; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/av1_dx_iface.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/av1_dx_iface.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_codec_iface = type { i8*, i32, i32, i32 (%struct.aom_codec_ctx*)*, i32 (%struct.aom_codec_alg_priv*)*, %struct.aom_codec_ctrl_fn_map*, %struct.aom_codec_dec_iface, %struct.aom_codec_enc_iface }
%struct.aom_codec_ctx = type { i8*, %struct.aom_codec_iface*, i32, i8*, i32, %union.anon, %struct.aom_codec_priv* }
%union.anon = type { %struct.aom_codec_dec_cfg* }
%struct.aom_codec_dec_cfg = type { i32, i32, i32, i32 }
%struct.aom_codec_priv = type { i8*, i32, %struct.anon }
%struct.anon = type { %struct.aom_fixed_buf, i32, i32, %struct.aom_codec_cx_pkt }
%struct.aom_fixed_buf = type { i8*, i32 }
%struct.aom_codec_cx_pkt = type { i32, %union.anon.0 }
%union.anon.0 = type { %struct.aom_psnr_pkt, [48 x i8] }
%struct.aom_psnr_pkt = type { [4 x i32], [4 x i64], [4 x double] }
%struct.aom_codec_alg_priv = type { %struct.aom_codec_priv, %struct.aom_codec_dec_cfg, %struct.aom_codec_stream_info, %struct.aom_image, i32, i32, i32, %struct.RefCntBuffer*, i32, i32, i32, i32, i32, i32, i32, i32, %struct.EXTERNAL_REFERENCES, i32, i32, i32, %struct.AVxWorker*, %struct.aom_image, [4 x %struct.aom_codec_frame_buffer], i32, i32, %struct.BufferPool*, i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)* }
%struct.aom_codec_stream_info = type { i32, i32, i32, i32, i32, i32 }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.yv12_buffer_config = type { %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, %union.anon.10, %union.anon.12, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i32, i32 }
%union.anon.10 = type { %struct.anon.11 }
%struct.anon.11 = type { i32, i32 }
%union.anon.12 = type { %struct.anon.13 }
%struct.anon.13 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.EXTERNAL_REFERENCES = type { [128 x %struct.yv12_buffer_config], i32 }
%struct.AVxWorker = type { %struct.AVxWorkerImpl*, i32, i8*, i32 (i8*, i8*)*, i8*, i8*, i32 }
%struct.AVxWorkerImpl = type opaque
%struct.aom_image = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.aom_metadata_array*, i8* }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.aom_codec_ctrl_fn_map = type { i32, i32 (%struct.aom_codec_alg_priv*, i8*)* }
%struct.aom_codec_dec_iface = type { i32 (i8*, i32, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_stream_info*)*, i32 (%struct.aom_codec_alg_priv*, i8*, i32, i8*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*, i8**)*, i32 (%struct.aom_codec_alg_priv*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, i8*)* }
%struct.aom_codec_enc_iface = type { i32, %struct.aom_codec_enc_cfg*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_image*, i64, i32, i32)*, %struct.aom_codec_cx_pkt* (%struct.aom_codec_alg_priv*, i8**)*, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_enc_cfg*)*, %struct.aom_fixed_buf* (%struct.aom_codec_alg_priv*)*, %struct.aom_image* (%struct.aom_codec_alg_priv*)* }
%struct.aom_codec_enc_cfg = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_rational, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_fixed_buf, %struct.aom_fixed_buf, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [64 x i32], [64 x i32], i32, [5 x i32], %struct.cfg_options }
%struct.aom_rational = type { i32, i32 }
%struct.cfg_options = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FrameWorkerData = type { %struct.AV1Decoder*, i8*, i8*, i32, i8*, i32, i32, i32 }
%struct.AV1Decoder = type { %struct.macroblockd, %struct.AV1Common, %struct.AVxWorker, %struct.AV1LfSyncData, %struct.AV1LrSyncData, %struct.AV1LrStruct, %struct.AVxWorker*, i32, %struct.DecWorkerData*, %struct.ThreadData, %struct.TileDataDec*, i32, [64 x [64 x %struct.TileBufferDec]], %struct.AV1DecTileMTData, i32, [4 x %struct.RefCntBuffer*], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.Accounting, i32, i32, i32, i32, i32, i32, i32, i32, %struct.DataBuffer, i32, i32, i32, i32, i32, i32, %struct.EXTERNAL_REFERENCES, %struct.yv12_buffer_config, %struct.CB_BUFFER*, i32, i32, %struct.AV1DecRowMTInfo, %struct.aom_metadata_array*, i32, i32, i32, i32, [8 x i32] }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }
%struct.AV1LfSyncData = type { [3 x i32*], i32, i32, %struct.LoopFilterWorkerData*, i32, %struct.AV1LfMTInfo*, i32, i32 }
%struct.LoopFilterWorkerData = type { %struct.yv12_buffer_config*, %struct.AV1Common*, [3 x %struct.macroblockd_plane], %struct.macroblockd* }
%struct.AV1LfMTInfo = type { i32, i32, i32 }
%struct.AV1LrSyncData = type { [3 x i32*], i32, i32, i32, i32, %struct.LoopRestorationWorkerData*, %struct.AV1LrMTInfo*, i32, i32 }
%struct.LoopRestorationWorkerData = type { i32*, i8*, i8* }
%struct.AV1LrMTInfo = type { i32, i32, i32, i32, i32, i32, i32 }
%struct.AV1LrStruct = type { void (%struct.RestorationTileLimits*, %struct.AV1PixelRect*, i32, i8*, i32*, %struct.RestorationLineBuffers*)*, [3 x %struct.FilterFrameCtxt], %struct.yv12_buffer_config*, %struct.yv12_buffer_config* }
%struct.RestorationTileLimits = type { i32, i32, i32, i32 }
%struct.AV1PixelRect = type { i32, i32, i32, i32 }
%struct.FilterFrameCtxt = type { %struct.RestorationInfo*, i32, i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.AV1PixelRect }
%struct.DecWorkerData = type { %struct.ThreadData*, i8*, %struct.aom_internal_error_info }
%struct.ThreadData = type { %struct.macroblockd, %struct.CB_BUFFER, %struct.aom_reader*, [2 x i8*], i32, i32, i16*, [2 x i8*], void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, %struct.aom_reader*, i32, i32, i32, i8)*, void (%struct.AV1Common*, %struct.macroblockd*, i8)*, void (%struct.AV1Common*, %struct.macroblockd*)*, [8 x i8] }
%struct.CB_BUFFER = type { [3 x [16384 x i32]], [3 x [1024 x %struct.eob_info]], [2 x [16384 x i8]] }
%struct.aom_reader = type { i8*, i8*, %struct.od_ec_dec, %struct.Accounting*, i8 }
%struct.od_ec_dec = type { i8*, i32, i8*, i8*, i32, i16, i16 }
%struct.TileDataDec = type { %struct.TileInfo, %struct.aom_reader, %struct.frame_contexts, %struct.AV1DecRowMTSyncData }
%struct.AV1DecRowMTSyncData = type { i32, i32*, i32, i32, i32, i32, i32, i32 }
%struct.TileBufferDec = type { i8*, i32 }
%struct.AV1DecTileMTData = type { %struct.TileJobsDec*, i32, i32, i32, i32 }
%struct.TileJobsDec = type { %struct.TileBufferDec*, %struct.TileDataDec* }
%struct.Accounting = type { %struct.AccountingSymbols, i32, [1021 x i16], %struct.AccountingSymbolContext, i32 }
%struct.AccountingSymbols = type { %struct.AccountingSymbol*, i32, i32, i32, %struct.AccountingDictionary }
%struct.AccountingSymbol = type { %struct.AccountingSymbolContext, i32, i32, i32 }
%struct.AccountingDictionary = type { [256 x i8*], i32 }
%struct.AccountingSymbolContext = type { i16, i16 }
%struct.DataBuffer = type { i8*, i32 }
%struct.AV1DecRowMTInfo = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.AVxWorkerInterface = type { void (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)* }
%struct.av1_ref_frame = type { i32, i32, %struct.aom_image }
%struct.av1_ext_ref_frame = type { %struct.aom_image*, i32 }
%struct.aom_tile_data = type { i32, i8*, i32 }
%struct.ObuHeader = type { i32, i8, i32, i32, i32, i32 }
%struct.aom_read_bit_buffer = type { i8*, i8*, i32, i8*, void (i8*)* }
%struct.AllocCbParam = type { %struct.BufferPool*, %struct.aom_codec_frame_buffer* }

@aom_codec_av1_dx_algo = hidden constant %struct.aom_codec_iface { i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i32 0, i32 0), i32 7, i32 2097153, i32 (%struct.aom_codec_ctx*)* @decoder_init, i32 (%struct.aom_codec_alg_priv*)* @decoder_destroy, %struct.aom_codec_ctrl_fn_map* getelementptr inbounds ([32 x %struct.aom_codec_ctrl_fn_map], [32 x %struct.aom_codec_ctrl_fn_map]* @decoder_ctrl_maps, i32 0, i32 0), %struct.aom_codec_dec_iface { i32 (i8*, i32, %struct.aom_codec_stream_info*)* @decoder_peek_si, i32 (%struct.aom_codec_alg_priv*, %struct.aom_codec_stream_info*)* @decoder_get_si, i32 (%struct.aom_codec_alg_priv*, i8*, i32, i8*)* @decoder_decode, %struct.aom_image* (%struct.aom_codec_alg_priv*, i8**)* @decoder_get_frame, i32 (%struct.aom_codec_alg_priv*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, i8*)* @decoder_set_fb_fn }, %struct.aom_codec_enc_iface zeroinitializer }, align 4
@.str = private unnamed_addr constant [35 x i8] c"AOMedia Project AV1 Decoder v2.0.0\00", align 1
@decoder_ctrl_maps = internal constant [32 x %struct.aom_codec_ctrl_fn_map] [%struct.aom_codec_ctrl_fn_map { i32 130, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_copy_reference }, %struct.aom_codec_ctrl_fn_map { i32 129, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_reference }, %struct.aom_codec_ctrl_fn_map { i32 266, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_invert_tile_order }, %struct.aom_codec_ctrl_fn_map { i32 265, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_byte_alignment }, %struct.aom_codec_ctrl_fn_map { i32 267, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_skip_loop_filter }, %struct.aom_codec_ctrl_fn_map { i32 270, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_decode_tile_row }, %struct.aom_codec_ctrl_fn_map { i32 271, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_decode_tile_col }, %struct.aom_codec_ctrl_fn_map { i32 272, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_tile_mode }, %struct.aom_codec_ctrl_fn_map { i32 278, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_is_annexb }, %struct.aom_codec_ctrl_fn_map { i32 279, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_operating_point }, %struct.aom_codec_ctrl_fn_map { i32 280, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_output_all_layers }, %struct.aom_codec_ctrl_fn_map { i32 281, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_inspection_callback }, %struct.aom_codec_ctrl_fn_map { i32 276, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_ext_tile_debug }, %struct.aom_codec_ctrl_fn_map { i32 277, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_row_mt }, %struct.aom_codec_ctrl_fn_map { i32 275, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_ext_ref_ptr }, %struct.aom_codec_ctrl_fn_map { i32 282, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_set_skip_film_grain }, %struct.aom_codec_ctrl_fn_map { i32 257, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_frame_corrupted }, %struct.aom_codec_ctrl_fn_map { i32 269, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_last_quantizer }, %struct.aom_codec_ctrl_fn_map { i32 256, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_last_ref_updates }, %struct.aom_codec_ctrl_fn_map { i32 261, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_bit_depth }, %struct.aom_codec_ctrl_fn_map { i32 262, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_img_format }, %struct.aom_codec_ctrl_fn_map { i32 263, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_tile_size }, %struct.aom_codec_ctrl_fn_map { i32 264, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_tile_count }, %struct.aom_codec_ctrl_fn_map { i32 260, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_render_size }, %struct.aom_codec_ctrl_fn_map { i32 259, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_frame_size }, %struct.aom_codec_ctrl_fn_map { i32 268, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_accounting }, %struct.aom_codec_ctrl_fn_map { i32 192, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_new_frame_image }, %struct.aom_codec_ctrl_fn_map { i32 193, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_copy_new_frame_image }, %struct.aom_codec_ctrl_fn_map { i32 128, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_reference }, %struct.aom_codec_ctrl_fn_map { i32 273, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_frame_header_info }, %struct.aom_codec_ctrl_fn_map { i32 274, i32 (%struct.aom_codec_alg_priv*, i8*)* @ctrl_get_tile_data }, %struct.aom_codec_ctrl_fn_map { i32 -1, i32 (%struct.aom_codec_alg_priv*, i8*)* null }], align 16
@.str.1 = private unnamed_addr constant [32 x i8] c"Failed to allocate frame_worker\00", align 1
@.str.2 = private unnamed_addr constant [16 x i8] c"aom frameworker\00", align 1
@.str.3 = private unnamed_addr constant [37 x i8] c"Failed to allocate frame_worker_data\00", align 1
@.str.4 = private unnamed_addr constant [44 x i8] c"Failed to initialize internal frame buffers\00", align 1
@.str.5 = private unnamed_addr constant [24 x i8] c"Grain systhesis failed\0A\00", align 1

; Function Attrs: nounwind
define hidden %struct.aom_codec_iface* @aom_codec_av1_dx() #0 {
entry:
  ret %struct.aom_codec_iface* @aom_codec_av1_dx_algo
}

; Function Attrs: nounwind
define internal i32 @decoder_init(%struct.aom_codec_ctx* %ctx) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_ctx*, align 4
  %priv1 = alloca %struct.aom_codec_alg_priv*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_ctx* %ctx, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %0, i32 0, i32 6
  %1 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv, align 4, !tbaa !6
  %tobool = icmp ne %struct.aom_codec_priv* %1, null
  br i1 %tobool, label %if.end15, label %if.then

if.then:                                          ; preds = %entry
  %2 = bitcast %struct.aom_codec_alg_priv** %priv1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %call = call i8* @aom_calloc(i32 1, i32 19544)
  %3 = bitcast i8* %call to %struct.aom_codec_alg_priv*
  store %struct.aom_codec_alg_priv* %3, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %cmp = icmp eq %struct.aom_codec_alg_priv* %4, null
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %5 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %6 = bitcast %struct.aom_codec_alg_priv* %5 to %struct.aom_codec_priv*
  %7 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv3 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %7, i32 0, i32 6
  store %struct.aom_codec_priv* %6, %struct.aom_codec_priv** %priv3, align 4, !tbaa !6
  %8 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %init_flags = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %8, i32 0, i32 4
  %9 = load i32, i32* %init_flags, align 4, !tbaa !9
  %10 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %priv4 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %10, i32 0, i32 6
  %11 = load %struct.aom_codec_priv*, %struct.aom_codec_priv** %priv4, align 4, !tbaa !6
  %init_flags5 = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %11, i32 0, i32 1
  store i32 %9, i32* %init_flags5, align 4, !tbaa !10
  %12 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %flushed = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %12, i32 0, i32 5
  store i32 0, i32* %flushed, align 4, !tbaa !16
  %13 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %cfg = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %13, i32 0, i32 1
  %allow_lowbitdepth = getelementptr inbounds %struct.aom_codec_dec_cfg, %struct.aom_codec_dec_cfg* %cfg, i32 0, i32 3
  store i32 1, i32* %allow_lowbitdepth, align 4, !tbaa !22
  %14 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %config = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %14, i32 0, i32 5
  %dec = bitcast %union.anon* %config to %struct.aom_codec_dec_cfg**
  %15 = load %struct.aom_codec_dec_cfg*, %struct.aom_codec_dec_cfg** %dec, align 4, !tbaa !23
  %tobool6 = icmp ne %struct.aom_codec_dec_cfg* %15, null
  br i1 %tobool6, label %if.then7, label %if.end14

if.then7:                                         ; preds = %if.end
  %16 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %cfg8 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %16, i32 0, i32 1
  %17 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %config9 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %17, i32 0, i32 5
  %dec10 = bitcast %union.anon* %config9 to %struct.aom_codec_dec_cfg**
  %18 = load %struct.aom_codec_dec_cfg*, %struct.aom_codec_dec_cfg** %dec10, align 4, !tbaa !23
  %19 = bitcast %struct.aom_codec_dec_cfg* %cfg8 to i8*
  %20 = bitcast %struct.aom_codec_dec_cfg* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %19, i8* align 4 %20, i32 16, i1 false), !tbaa.struct !24
  %21 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %cfg11 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %21, i32 0, i32 1
  %22 = load %struct.aom_codec_ctx*, %struct.aom_codec_ctx** %ctx.addr, align 4, !tbaa !2
  %config12 = getelementptr inbounds %struct.aom_codec_ctx, %struct.aom_codec_ctx* %22, i32 0, i32 5
  %dec13 = bitcast %union.anon* %config12 to %struct.aom_codec_dec_cfg**
  store %struct.aom_codec_dec_cfg* %cfg11, %struct.aom_codec_dec_cfg** %dec13, align 4, !tbaa !23
  br label %if.end14

if.end14:                                         ; preds = %if.then7, %if.end
  %23 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %num_grain_image_frame_buffers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %23, i32 0, i32 23
  store i32 0, i32* %num_grain_image_frame_buffers, align 4, !tbaa !26
  %24 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %row_mt = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %24, i32 0, i32 15
  store i32 1, i32* %row_mt, align 4, !tbaa !27
  %25 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %tile_mode = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %25, i32 0, i32 13
  store i32 0, i32* %tile_mode, align 4, !tbaa !28
  %26 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %decode_tile_row = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %26, i32 0, i32 11
  store i32 -1, i32* %decode_tile_row, align 4, !tbaa !29
  %27 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %priv1, align 4, !tbaa !2
  %decode_tile_col = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %27, i32 0, i32 12
  store i32 -1, i32* %decode_tile_col, align 8, !tbaa !30
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end14, %if.then2
  %28 = bitcast %struct.aom_codec_alg_priv** %priv1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end15

if.end15:                                         ; preds = %cleanup.cont, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end15, %cleanup
  %29 = load i32, i32* %retval, align 4
  ret i32 %29

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i32 @decoder_destroy(%struct.aom_codec_alg_priv* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %i = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %0, i32 0, i32 20
  %1 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %cmp = icmp ne %struct.AVxWorker* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker1 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %3, i32 0, i32 20
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker1, align 8, !tbaa !31
  store %struct.AVxWorker* %4, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %5 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %6, i32 0, i32 4
  %7 = load i8*, i8** %data1, align 4, !tbaa !32
  %8 = bitcast i8* %7 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %8, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %call = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  %end = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %call, i32 0, i32 5
  %9 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %end, align 4, !tbaa !34
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void %9(%struct.AVxWorker* %10)
  %11 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %11, i32 0, i32 0
  %12 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %12, i32 0, i32 1
  %tpl_mvs = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common, i32 0, i32 45
  %13 = load %struct.TPL_MV_REF*, %struct.TPL_MV_REF** %tpl_mvs, align 4, !tbaa !38
  %14 = bitcast %struct.TPL_MV_REF* %13 to i8*
  call void @aom_free(i8* %14)
  %15 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi2 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %15, i32 0, i32 0
  %16 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi2, align 4, !tbaa !36
  %common3 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %16, i32 0, i32 1
  %tpl_mvs4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common3, i32 0, i32 45
  store %struct.TPL_MV_REF* null, %struct.TPL_MV_REF** %tpl_mvs4, align 4, !tbaa !38
  %17 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi5 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %17, i32 0, i32 0
  %18 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi5, align 4, !tbaa !36
  %common6 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %18, i32 0, i32 1
  call void @av1_remove_common(%struct.AV1Common* %common6)
  %19 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi7 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %19, i32 0, i32 0
  %20 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi7, align 4, !tbaa !36
  %common8 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %20, i32 0, i32 1
  call void @av1_free_restoration_buffers(%struct.AV1Common* %common8)
  %21 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi9 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %21, i32 0, i32 0
  %22 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi9, align 4, !tbaa !36
  call void @av1_decoder_remove(%struct.AV1Decoder* %22)
  %23 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %24 = bitcast %struct.FrameWorkerData* %23 to i8*
  call void @aom_free(i8* %24)
  %25 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %27 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %27, i32 0, i32 25
  %28 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !78
  %tobool = icmp ne %struct.BufferPool* %28, null
  br i1 %tobool, label %if.then10, label %if.end17

if.then10:                                        ; preds = %if.end
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #5
  store i32 0, i32* %i, align 4, !tbaa !79
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then10
  %30 = load i32, i32* %i, align 4, !tbaa !79
  %31 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %num_grain_image_frame_buffers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %31, i32 0, i32 23
  %32 = load i32, i32* %num_grain_image_frame_buffers, align 4, !tbaa !26
  %cmp11 = icmp ult i32 %30, %32
  br i1 %cmp11, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %34 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool12 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %34, i32 0, i32 25
  %35 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool12, align 4, !tbaa !78
  %release_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %35, i32 0, i32 2
  %36 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !80
  %37 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool13 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %37, i32 0, i32 25
  %38 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool13, align 4, !tbaa !78
  %cb_priv = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %38, i32 0, i32 0
  %39 = load i8*, i8** %cb_priv, align 4, !tbaa !83
  %40 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %grain_image_frame_buffers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %40, i32 0, i32 22
  %41 = load i32, i32* %i, align 4, !tbaa !79
  %arrayidx = getelementptr inbounds [4 x %struct.aom_codec_frame_buffer], [4 x %struct.aom_codec_frame_buffer]* %grain_image_frame_buffers, i32 0, i32 %41
  %call14 = call i32 %36(i8* %39, %struct.aom_codec_frame_buffer* %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %42 = load i32, i32* %i, align 4, !tbaa !79
  %inc = add i32 %42, 1
  store i32 %inc, i32* %i, align 4, !tbaa !79
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %43 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool15 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %43, i32 0, i32 25
  %44 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool15, align 4, !tbaa !78
  call void @av1_free_ref_frame_buffers(%struct.BufferPool* %44)
  %45 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool16 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %45, i32 0, i32 25
  %46 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool16, align 4, !tbaa !78
  %int_frame_buffers = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %46, i32 0, i32 4
  call void @av1_free_internal_frame_buffers(%struct.InternalFrameBufferList* %int_frame_buffers)
  br label %if.end17

if.end17:                                         ; preds = %for.end, %if.end
  %47 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker18 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %47, i32 0, i32 20
  %48 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker18, align 8, !tbaa !31
  %49 = bitcast %struct.AVxWorker* %48 to i8*
  call void @aom_free(i8* %49)
  %50 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool19 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %50, i32 0, i32 25
  %51 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool19, align 4, !tbaa !78
  %52 = bitcast %struct.BufferPool* %51 to i8*
  call void @aom_free(i8* %52)
  %53 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %53, i32 0, i32 3
  call void @aom_img_free(%struct.aom_image* %img)
  %54 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %55 = bitcast %struct.aom_codec_alg_priv* %54 to i8*
  call void @aom_free(i8* %55)
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @decoder_peek_si(i8* %data, i32 %data_sz, %struct.aom_codec_stream_info* %si) #0 {
entry:
  %data.addr = alloca i8*, align 4
  %data_sz.addr = alloca i32, align 4
  %si.addr = alloca %struct.aom_codec_stream_info*, align 4
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %data_sz, i32* %data_sz.addr, align 4, !tbaa !79
  store %struct.aom_codec_stream_info* %si, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %1 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %2 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %call = call i32 @decoder_peek_si_internal(i8* %0, i32 %1, %struct.aom_codec_stream_info* %2, i32* null)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @decoder_get_si(%struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_stream_info* %si) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %si.addr = alloca %struct.aom_codec_stream_info*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_codec_stream_info* %si, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %1 = bitcast %struct.aom_codec_stream_info* %0 to i8*
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %si1 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 2
  %3 = bitcast %struct.aom_codec_stream_info* %si1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 8 %3, i32 24, i1 false)
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @decoder_decode(%struct.aom_codec_alg_priv* %ctx, i8* %data, i32 %data_sz, i8* %user_priv) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %data.addr = alloca i8*, align 4
  %data_sz.addr = alloca i32, align 4
  %user_priv.addr = alloca i8*, align 4
  %res = alloca i32, align 4
  %pool = alloca %struct.BufferPool*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %pbi = alloca %struct.AV1Decoder*, align 4
  %j = alloca i32, align 4
  %j4 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %data_start = alloca i8*, align 4
  %data_end = alloca i8*, align 4
  %length_of_size = alloca i32, align 4
  %temporal_unit_size = alloca i64, align 8
  %frame_size = alloca i64, align 8
  %length_of_size57 = alloca i32, align 4
  %marker = alloca i8, align 1
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %data_sz, i32* %data_sz.addr, align 4, !tbaa !79
  store i8* %user_priv, i8** %user_priv.addr, align 4, !tbaa !2
  %0 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %res, align 4, !tbaa !23
  %1 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %1, i32 0, i32 20
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool = icmp ne %struct.AVxWorker* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 25
  %5 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !78
  store %struct.BufferPool* %5, %struct.BufferPool** %pool, align 4, !tbaa !2
  %6 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @lock_buffer_pool(%struct.BufferPool* %6)
  %7 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker1 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %8, i32 0, i32 20
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker1, align 8, !tbaa !31
  store %struct.AVxWorker* %9, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %10 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %11, i32 0, i32 4
  %12 = load i8*, i8** %data1, align 4, !tbaa !32
  %13 = bitcast i8* %12 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %13, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %14 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi2 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %15, i32 0, i32 0
  %16 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi2, align 4, !tbaa !36
  store %struct.AV1Decoder* %16, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %17 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #5
  store i32 0, i32* %j, align 4, !tbaa !79
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %18 = load i32, i32* %j, align 4, !tbaa !79
  %19 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %num_output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %19, i32 0, i32 16
  %20 = load i32, i32* %num_output_frames, align 16, !tbaa !84
  %cmp = icmp ult i32 %18, %20
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %21 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %22 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %22, i32 0, i32 15
  %23 = load i32, i32* %j, align 4, !tbaa !79
  %arrayidx = getelementptr inbounds [4 x %struct.RefCntBuffer*], [4 x %struct.RefCntBuffer*]* %output_frames, i32 0, i32 %23
  %24 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  %25 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @decrease_ref_count(%struct.RefCntBuffer* %24, %struct.BufferPool* %25)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %j, align 4, !tbaa !79
  %inc = add i32 %26, 1
  store i32 %inc, i32* %j, align 4, !tbaa !79
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %27 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %num_output_frames3 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %27, i32 0, i32 16
  store i32 0, i32* %num_output_frames3, align 16, !tbaa !84
  %28 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  call void @unlock_buffer_pool(%struct.BufferPool* %28)
  %29 = bitcast i32* %j4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #5
  store i32 0, i32* %j4, align 4, !tbaa !79
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc17, %for.end
  %30 = load i32, i32* %j4, align 4, !tbaa !79
  %31 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %num_grain_image_frame_buffers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %31, i32 0, i32 23
  %32 = load i32, i32* %num_grain_image_frame_buffers, align 4, !tbaa !26
  %cmp6 = icmp ult i32 %30, %32
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  %33 = bitcast i32* %j4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  br label %for.end19

for.body8:                                        ; preds = %for.cond5
  %34 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %release_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %34, i32 0, i32 2
  %35 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !80
  %36 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %cb_priv = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %36, i32 0, i32 0
  %37 = load i8*, i8** %cb_priv, align 4, !tbaa !83
  %38 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %grain_image_frame_buffers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %38, i32 0, i32 22
  %39 = load i32, i32* %j4, align 4, !tbaa !79
  %arrayidx9 = getelementptr inbounds [4 x %struct.aom_codec_frame_buffer], [4 x %struct.aom_codec_frame_buffer]* %grain_image_frame_buffers, i32 0, i32 %39
  %call = call i32 %35(i8* %37, %struct.aom_codec_frame_buffer* %arrayidx9)
  %40 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %grain_image_frame_buffers10 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %40, i32 0, i32 22
  %41 = load i32, i32* %j4, align 4, !tbaa !79
  %arrayidx11 = getelementptr inbounds [4 x %struct.aom_codec_frame_buffer], [4 x %struct.aom_codec_frame_buffer]* %grain_image_frame_buffers10, i32 0, i32 %41
  %data12 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %arrayidx11, i32 0, i32 0
  store i8* null, i8** %data12, align 4, !tbaa !85
  %42 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %grain_image_frame_buffers13 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %42, i32 0, i32 22
  %43 = load i32, i32* %j4, align 4, !tbaa !79
  %arrayidx14 = getelementptr inbounds [4 x %struct.aom_codec_frame_buffer], [4 x %struct.aom_codec_frame_buffer]* %grain_image_frame_buffers13, i32 0, i32 %43
  %size = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %arrayidx14, i32 0, i32 1
  store i32 0, i32* %size, align 4, !tbaa !87
  %44 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %grain_image_frame_buffers15 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %44, i32 0, i32 22
  %45 = load i32, i32* %j4, align 4, !tbaa !79
  %arrayidx16 = getelementptr inbounds [4 x %struct.aom_codec_frame_buffer], [4 x %struct.aom_codec_frame_buffer]* %grain_image_frame_buffers15, i32 0, i32 %45
  %priv = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %arrayidx16, i32 0, i32 2
  store i8* null, i8** %priv, align 4, !tbaa !88
  br label %for.inc17

for.inc17:                                        ; preds = %for.body8
  %46 = load i32, i32* %j4, align 4, !tbaa !79
  %inc18 = add i32 %46, 1
  store i32 %inc18, i32* %j4, align 4, !tbaa !79
  br label %for.cond5

for.end19:                                        ; preds = %for.cond.cleanup7
  %47 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %num_grain_image_frame_buffers20 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %47, i32 0, i32 23
  store i32 0, i32* %num_grain_image_frame_buffers20, align 4, !tbaa !26
  %48 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %49 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  %50 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #5
  %51 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #5
  br label %if.end

if.end:                                           ; preds = %for.end19, %entry
  %52 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %cmp21 = icmp eq i8* %52, null
  br i1 %cmp21, label %land.lhs.true, label %if.end24

land.lhs.true:                                    ; preds = %if.end
  %53 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %cmp22 = icmp eq i32 %53, 0
  br i1 %cmp22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %land.lhs.true
  %54 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %flushed = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %54, i32 0, i32 5
  store i32 1, i32* %flushed, align 4, !tbaa !16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup106

if.end24:                                         ; preds = %land.lhs.true, %if.end
  %55 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %cmp25 = icmp eq i8* %55, null
  br i1 %cmp25, label %if.then27, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end24
  %56 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %cmp26 = icmp eq i32 %56, 0
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %lor.lhs.false, %if.end24
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup106

if.end28:                                         ; preds = %lor.lhs.false
  %57 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %flushed29 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %57, i32 0, i32 5
  store i32 0, i32* %flushed29, align 4, !tbaa !16
  %58 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker30 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %58, i32 0, i32 20
  %59 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker30, align 8, !tbaa !31
  %cmp31 = icmp eq %struct.AVxWorker* %59, null
  br i1 %cmp31, label %if.then32, label %if.end37

if.then32:                                        ; preds = %if.end28
  %60 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %call33 = call i32 @init_decoder(%struct.aom_codec_alg_priv* %60)
  store i32 %call33, i32* %res, align 4, !tbaa !23
  %61 = load i32, i32* %res, align 4, !tbaa !23
  %cmp34 = icmp ne i32 %61, 0
  br i1 %cmp34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.then32
  %62 = load i32, i32* %res, align 4, !tbaa !23
  store i32 %62, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup106

if.end36:                                         ; preds = %if.then32
  br label %if.end37

if.end37:                                         ; preds = %if.end36, %if.end28
  %63 = bitcast i8** %data_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #5
  %64 = load i8*, i8** %data.addr, align 4, !tbaa !2
  store i8* %64, i8** %data_start, align 4, !tbaa !2
  %65 = bitcast i8** %data_end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #5
  %66 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %67 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %add.ptr = getelementptr inbounds i8, i8* %66, i32 %67
  store i8* %add.ptr, i8** %data_end, align 4, !tbaa !2
  %68 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %is_annexb = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %68, i32 0, i32 17
  %69 = load i32, i32* %is_annexb, align 4, !tbaa !89
  %tobool38 = icmp ne i32 %69, 0
  br i1 %tobool38, label %if.then39, label %if.end51

if.then39:                                        ; preds = %if.end37
  %70 = bitcast i32* %length_of_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #5
  %71 = bitcast i64* %temporal_unit_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %71) #5
  %72 = load i8*, i8** %data_start, align 4, !tbaa !2
  %73 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %call40 = call i32 @aom_uleb_decode(i8* %72, i32 %73, i64* %temporal_unit_size, i32* %length_of_size)
  %cmp41 = icmp ne i32 %call40, 0
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.then39
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end43:                                         ; preds = %if.then39
  %74 = load i32, i32* %length_of_size, align 4, !tbaa !79
  %75 = load i8*, i8** %data_start, align 4, !tbaa !2
  %add.ptr44 = getelementptr inbounds i8, i8* %75, i32 %74
  store i8* %add.ptr44, i8** %data_start, align 4, !tbaa !2
  %76 = load i64, i64* %temporal_unit_size, align 8, !tbaa !90
  %77 = load i8*, i8** %data_end, align 4, !tbaa !2
  %78 = load i8*, i8** %data_start, align 4, !tbaa !2
  %sub.ptr.lhs.cast = ptrtoint i8* %77 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %78 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %conv = zext i32 %sub.ptr.sub to i64
  %cmp45 = icmp ugt i64 %76, %conv
  br i1 %cmp45, label %if.then47, label %if.end48

if.then47:                                        ; preds = %if.end43
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end48:                                         ; preds = %if.end43
  %79 = load i8*, i8** %data_start, align 4, !tbaa !2
  %80 = load i64, i64* %temporal_unit_size, align 8, !tbaa !90
  %idx.ext = trunc i64 %80 to i32
  %add.ptr49 = getelementptr inbounds i8, i8* %79, i32 %idx.ext
  store i8* %add.ptr49, i8** %data_end, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end48, %if.then47, %if.then42
  %81 = bitcast i64* %temporal_unit_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %81) #5
  %82 = bitcast i32* %length_of_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup104 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end51

if.end51:                                         ; preds = %cleanup.cont, %if.end37
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont102, %if.end51
  %83 = load i8*, i8** %data_start, align 4, !tbaa !2
  %84 = load i8*, i8** %data_end, align 4, !tbaa !2
  %cmp52 = icmp ult i8* %83, %84
  br i1 %cmp52, label %while.body, label %while.end103

while.body:                                       ; preds = %while.cond
  %85 = bitcast i64* %frame_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %85) #5
  %86 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %is_annexb54 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %86, i32 0, i32 17
  %87 = load i32, i32* %is_annexb54, align 4, !tbaa !89
  %tobool55 = icmp ne i32 %87, 0
  br i1 %tobool55, label %if.then56, label %if.else

if.then56:                                        ; preds = %while.body
  %88 = bitcast i32* %length_of_size57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #5
  %89 = load i8*, i8** %data_start, align 4, !tbaa !2
  %90 = load i8*, i8** %data_end, align 4, !tbaa !2
  %91 = load i8*, i8** %data_start, align 4, !tbaa !2
  %sub.ptr.lhs.cast58 = ptrtoint i8* %90 to i32
  %sub.ptr.rhs.cast59 = ptrtoint i8* %91 to i32
  %sub.ptr.sub60 = sub i32 %sub.ptr.lhs.cast58, %sub.ptr.rhs.cast59
  %call61 = call i32 @aom_uleb_decode(i8* %89, i32 %sub.ptr.sub60, i64* %frame_size, i32* %length_of_size57)
  %cmp62 = icmp ne i32 %call61, 0
  br i1 %cmp62, label %if.then64, label %if.end65

if.then64:                                        ; preds = %if.then56
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup75

if.end65:                                         ; preds = %if.then56
  %92 = load i32, i32* %length_of_size57, align 4, !tbaa !79
  %93 = load i8*, i8** %data_start, align 4, !tbaa !2
  %add.ptr66 = getelementptr inbounds i8, i8* %93, i32 %92
  store i8* %add.ptr66, i8** %data_start, align 4, !tbaa !2
  %94 = load i64, i64* %frame_size, align 8, !tbaa !90
  %95 = load i8*, i8** %data_end, align 4, !tbaa !2
  %96 = load i8*, i8** %data_start, align 4, !tbaa !2
  %sub.ptr.lhs.cast67 = ptrtoint i8* %95 to i32
  %sub.ptr.rhs.cast68 = ptrtoint i8* %96 to i32
  %sub.ptr.sub69 = sub i32 %sub.ptr.lhs.cast67, %sub.ptr.rhs.cast68
  %conv70 = zext i32 %sub.ptr.sub69 to i64
  %cmp71 = icmp ugt i64 %94, %conv70
  br i1 %cmp71, label %if.then73, label %if.end74

if.then73:                                        ; preds = %if.end65
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup75

if.end74:                                         ; preds = %if.end65
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup75

cleanup75:                                        ; preds = %if.end74, %if.then73, %if.then64
  %97 = bitcast i32* %length_of_size57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #5
  %cleanup.dest76 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest76, label %cleanup100 [
    i32 0, label %cleanup.cont77
  ]

cleanup.cont77:                                   ; preds = %cleanup75
  br label %if.end82

if.else:                                          ; preds = %while.body
  %98 = load i8*, i8** %data_end, align 4, !tbaa !2
  %99 = load i8*, i8** %data_start, align 4, !tbaa !2
  %sub.ptr.lhs.cast78 = ptrtoint i8* %98 to i32
  %sub.ptr.rhs.cast79 = ptrtoint i8* %99 to i32
  %sub.ptr.sub80 = sub i32 %sub.ptr.lhs.cast78, %sub.ptr.rhs.cast79
  %conv81 = sext i32 %sub.ptr.sub80 to i64
  store i64 %conv81, i64* %frame_size, align 8, !tbaa !90
  br label %if.end82

if.end82:                                         ; preds = %if.else, %cleanup.cont77
  %100 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %101 = load i64, i64* %frame_size, align 8, !tbaa !90
  %conv83 = trunc i64 %101 to i32
  %102 = load i8*, i8** %user_priv.addr, align 4, !tbaa !2
  %call84 = call i32 @decode_one(%struct.aom_codec_alg_priv* %100, i8** %data_start, i32 %conv83, i8* %102)
  store i32 %call84, i32* %res, align 4, !tbaa !23
  %103 = load i32, i32* %res, align 4, !tbaa !23
  %cmp85 = icmp ne i32 %103, 0
  br i1 %cmp85, label %if.then87, label %if.end88

if.then87:                                        ; preds = %if.end82
  %104 = load i32, i32* %res, align 4, !tbaa !23
  store i32 %104, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup100

if.end88:                                         ; preds = %if.end82
  br label %while.cond89

while.cond89:                                     ; preds = %cleanup.cont99, %if.end88
  %105 = load i8*, i8** %data_start, align 4, !tbaa !2
  %106 = load i8*, i8** %data_end, align 4, !tbaa !2
  %cmp90 = icmp ult i8* %105, %106
  br i1 %cmp90, label %while.body92, label %while.end

while.body92:                                     ; preds = %while.cond89
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %marker) #5
  %107 = load i8*, i8** %data_start, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i8, i8* %107, i32 0
  %108 = load i8, i8* %arrayidx93, align 1, !tbaa !23
  store i8 %108, i8* %marker, align 1, !tbaa !23
  %109 = load i8, i8* %marker, align 1, !tbaa !23
  %tobool94 = icmp ne i8 %109, 0
  br i1 %tobool94, label %if.then95, label %if.end96

if.then95:                                        ; preds = %while.body92
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup97

if.end96:                                         ; preds = %while.body92
  %110 = load i8*, i8** %data_start, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %110, i32 1
  store i8* %incdec.ptr, i8** %data_start, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup97

cleanup97:                                        ; preds = %if.end96, %if.then95
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %marker) #5
  %cleanup.dest98 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest98, label %unreachable [
    i32 0, label %cleanup.cont99
    i32 11, label %while.end
  ]

cleanup.cont99:                                   ; preds = %cleanup97
  br label %while.cond89

while.end:                                        ; preds = %cleanup97, %while.cond89
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup100

cleanup100:                                       ; preds = %while.end, %if.then87, %cleanup75
  %111 = bitcast i64* %frame_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %111) #5
  %cleanup.dest101 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest101, label %cleanup104 [
    i32 0, label %cleanup.cont102
  ]

cleanup.cont102:                                  ; preds = %cleanup100
  br label %while.cond

while.end103:                                     ; preds = %while.cond
  %112 = load i32, i32* %res, align 4, !tbaa !23
  store i32 %112, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup104

cleanup104:                                       ; preds = %while.end103, %cleanup100, %cleanup
  %113 = bitcast i8** %data_end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #5
  %114 = bitcast i8** %data_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #5
  br label %cleanup106

cleanup106:                                       ; preds = %cleanup104, %if.then35, %if.then27, %if.then23
  %115 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #5
  %116 = load i32, i32* %retval, align 4
  ret i32 %116

unreachable:                                      ; preds = %cleanup97
  unreachable
}

; Function Attrs: nounwind
define internal %struct.aom_image* @decoder_get_frame(%struct.aom_codec_alg_priv* %ctx, i8** %iter) #0 {
entry:
  %retval = alloca %struct.aom_image*, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %iter.addr = alloca i8**, align 4
  %img = alloca %struct.aom_image*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %index = alloca i32*, align 4
  %winterface = alloca %struct.AVxWorkerInterface*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %pbi = alloca %struct.AV1Decoder*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %tiles = alloca %struct.CommonTileParams*, align 4
  %sd = alloca %struct.yv12_buffer_config*, align 4
  %grain_params = alloca %struct.aom_film_grain_t*, align 4
  %output_frame_buf = alloca %struct.RefCntBuffer*, align 4
  %num_planes = alloca i32, align 4
  %tile_width = alloca i32, align 4
  %tile_height = alloca i32, align 4
  %tile_row = alloca i32, align 4
  %mi_row = alloca i32, align 4
  %ssy = alloca i32, align 4
  %plane = alloca i32, align 4
  %tile_width84 = alloca i32, align 4
  %tile_height85 = alloca i32, align 4
  %tile_col = alloca i32, align 4
  %mi_col = alloca i32, align 4
  %ssx = alloca i32, align 4
  %is_hbd = alloca i32, align 4
  %plane101 = alloca i32, align 4
  %res = alloca %struct.aom_image*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8** %iter, i8*** %iter.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_image** %img to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store %struct.aom_image* null, %struct.aom_image** %img, align 4, !tbaa !2
  %1 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  %tobool = icmp ne i8** %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.aom_image* null, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup169

if.end:                                           ; preds = %entry
  %2 = bitcast i32** %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i8**, i8*** %iter.addr, align 4, !tbaa !2
  %4 = bitcast i8** %3 to i32*
  store i32* %4, i32** %index, align 4, !tbaa !2
  %5 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %5, i32 0, i32 20
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %cmp = icmp ne %struct.AVxWorker* %6, null
  br i1 %cmp, label %if.then1, label %if.end167

if.then1:                                         ; preds = %if.end
  %7 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %call = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  store %struct.AVxWorkerInterface* %call, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %8 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker2 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %9, i32 0, i32 20
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker2, align 8, !tbaa !31
  store %struct.AVxWorker* %10, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %11 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %12, i32 0, i32 4
  %13 = load i8*, i8** %data1, align 4, !tbaa !32
  %14 = bitcast i8* %13 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %14, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %15 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi3 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %16, i32 0, i32 0
  %17 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi3, align 4, !tbaa !36
  store %struct.AV1Decoder* %17, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %18 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %19, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %20 = bitcast %struct.CommonTileParams** %tiles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #5
  %21 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %tiles4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %21, i32 0, i32 40
  store %struct.CommonTileParams* %tiles4, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %22 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %22, i32 0, i32 2
  %23 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %sync, align 4, !tbaa !92
  %24 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %call5 = call i32 %23(%struct.AVxWorker* %24)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then1
  %25 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %received_frame = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %25, i32 0, i32 5
  %26 = load i32, i32* %received_frame, align 4, !tbaa !93
  %cmp8 = icmp eq i32 %26, 1
  br i1 %cmp8, label %if.then9, label %if.end12

if.then9:                                         ; preds = %if.then7
  %27 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %received_frame10 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %27, i32 0, i32 5
  store i32 0, i32* %received_frame10, align 4, !tbaa !93
  %28 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %29 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi11 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %29, i32 0, i32 0
  %30 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi11, align 4, !tbaa !36
  call void @check_resync(%struct.aom_codec_alg_priv* %28, %struct.AV1Decoder* %30)
  br label %if.end12

if.end12:                                         ; preds = %if.then9, %if.then7
  %31 = bitcast %struct.yv12_buffer_config** %sd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #5
  %32 = bitcast %struct.aom_film_grain_t** %grain_params to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #5
  %33 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi13 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %33, i32 0, i32 0
  %34 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi13, align 4, !tbaa !36
  %35 = load i32*, i32** %index, align 4, !tbaa !2
  %36 = load i32, i32* %35, align 4, !tbaa !79
  %call14 = call i32 @av1_get_raw_frame(%struct.AV1Decoder* %34, i32 %36, %struct.yv12_buffer_config** %sd, %struct.aom_film_grain_t** %grain_params)
  %cmp15 = icmp eq i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end150

if.then16:                                        ; preds = %if.end12
  %37 = bitcast %struct.RefCntBuffer** %output_frame_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #5
  %38 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %38, i32 0, i32 15
  %39 = load i32*, i32** %index, align 4, !tbaa !2
  %40 = load i32, i32* %39, align 4, !tbaa !79
  %arrayidx = getelementptr inbounds [4 x %struct.RefCntBuffer*], [4 x %struct.RefCntBuffer*]* %output_frames, i32 0, i32 %40
  %41 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  store %struct.RefCntBuffer* %41, %struct.RefCntBuffer** %output_frame_buf, align 4, !tbaa !2
  %42 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %output_frame_buf, align 4, !tbaa !2
  %43 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %last_show_frame = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %43, i32 0, i32 7
  store %struct.RefCntBuffer* %42, %struct.RefCntBuffer** %last_show_frame, align 4, !tbaa !94
  %44 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %need_resync = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %44, i32 0, i32 24
  %45 = load i32, i32* %need_resync, align 8, !tbaa !95
  %tobool17 = icmp ne i32 %45, 0
  br i1 %tobool17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.then16
  store %struct.aom_image* null, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.then16
  %46 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img20 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %46, i32 0, i32 3
  call void @aom_img_remove_metadata(%struct.aom_image* %img20)
  %47 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img21 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %47, i32 0, i32 3
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %sd, align 4, !tbaa !2
  %49 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %user_priv = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %49, i32 0, i32 4
  %50 = load i8*, i8** %user_priv, align 4, !tbaa !96
  call void @yuvconfig2image(%struct.aom_image* %img21, %struct.yv12_buffer_config* %48, i8* %50)
  %51 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %52 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img22 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %52, i32 0, i32 3
  call void @move_decoder_metadata_to_img(%struct.AV1Decoder* %51, %struct.aom_image* %img22)
  %53 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %ext_tile_debug = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %53, i32 0, i32 42
  %54 = load i32, i32* %ext_tile_debug, align 4, !tbaa !97
  %tobool23 = icmp ne i32 %54, 0
  br i1 %tobool23, label %if.end30, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end19
  %55 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %large_scale = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %55, i32 0, i32 17
  %56 = load i32, i32* %large_scale, align 4, !tbaa !98
  %tobool24 = icmp ne i32 %56, 0
  br i1 %tobool24, label %if.then25, label %if.end30

if.then25:                                        ; preds = %land.lhs.true
  %57 = load i32*, i32** %index, align 4, !tbaa !2
  %58 = load i32, i32* %57, align 4, !tbaa !79
  %add = add i32 %58, 1
  store i32 %add, i32* %57, align 4, !tbaa !79
  %59 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img26 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %59, i32 0, i32 3
  call void @aom_img_remove_metadata(%struct.aom_image* %img26)
  %60 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img27 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %60, i32 0, i32 3
  %61 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %tile_list_outbuf = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %61, i32 0, i32 45
  call void @yuvconfig2image(%struct.aom_image* %img27, %struct.yv12_buffer_config* %tile_list_outbuf, i8* null)
  %62 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %63 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img28 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %63, i32 0, i32 3
  call void @move_decoder_metadata_to_img(%struct.AV1Decoder* %62, %struct.aom_image* %img28)
  %64 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img29 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %64, i32 0, i32 3
  store %struct.aom_image* %img29, %struct.aom_image** %img, align 4, !tbaa !2
  %65 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !2
  store %struct.aom_image* %65, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end30:                                         ; preds = %land.lhs.true, %if.end19
  %66 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #5
  %67 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %call31 = call i32 @av1_num_planes(%struct.AV1Common* %67)
  store i32 %call31, i32* %num_planes, align 4, !tbaa !25
  %68 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %ext_tile_debug32 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %68, i32 0, i32 42
  %69 = load i32, i32* %ext_tile_debug32, align 4, !tbaa !97
  %tobool33 = icmp ne i32 %69, 0
  br i1 %tobool33, label %land.lhs.true34, label %if.end75

land.lhs.true34:                                  ; preds = %if.end30
  %70 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %single_tile_decoding = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %70, i32 0, i32 18
  %71 = load i32, i32* %single_tile_decoding, align 4, !tbaa !99
  %tobool35 = icmp ne i32 %71, 0
  br i1 %tobool35, label %land.lhs.true36, label %if.end75

land.lhs.true36:                                  ; preds = %land.lhs.true34
  %72 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_row = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %72, i32 0, i32 25
  %73 = load i32, i32* %dec_tile_row, align 4, !tbaa !100
  %cmp37 = icmp sge i32 %73, 0
  br i1 %cmp37, label %if.then38, label %if.end75

if.then38:                                        ; preds = %land.lhs.true36
  %74 = bitcast i32* %tile_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #5
  %75 = bitcast i32* %tile_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #5
  %76 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  call void @av1_get_uniform_tile_size(%struct.AV1Common* %76, i32* %tile_width, i32* %tile_height)
  %77 = bitcast i32* %tile_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #5
  %78 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_row39 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %78, i32 0, i32 25
  %79 = load i32, i32* %dec_tile_row39, align 4, !tbaa !100
  %80 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %80, i32 0, i32 1
  %81 = load i32, i32* %rows, align 4, !tbaa !101
  %sub = sub nsw i32 %81, 1
  %cmp40 = icmp slt i32 %79, %sub
  br i1 %cmp40, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then38
  %82 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_row41 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %82, i32 0, i32 25
  %83 = load i32, i32* %dec_tile_row41, align 4, !tbaa !100
  br label %cond.end

cond.false:                                       ; preds = %if.then38
  %84 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %rows42 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %84, i32 0, i32 1
  %85 = load i32, i32* %rows42, align 4, !tbaa !101
  %sub43 = sub nsw i32 %85, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %83, %cond.true ], [ %sub43, %cond.false ]
  store i32 %cond, i32* %tile_row, align 4, !tbaa !25
  %86 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #5
  %87 = load i32, i32* %tile_row, align 4, !tbaa !25
  %88 = load i32, i32* %tile_height, align 4, !tbaa !25
  %mul = mul nsw i32 %87, %88
  store i32 %mul, i32* %mi_row, align 4, !tbaa !25
  %89 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #5
  %90 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img44 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %90, i32 0, i32 3
  %y_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img44, i32 0, i32 15
  %91 = load i32, i32* %y_chroma_shift, align 4, !tbaa !102
  store i32 %91, i32* %ssy, align 4, !tbaa !25
  %92 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #5
  %93 = load i32, i32* %mi_row, align 4, !tbaa !25
  %mul45 = mul nsw i32 %93, 4
  %94 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img46 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %94, i32 0, i32 3
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img46, i32 0, i32 17
  %arrayidx47 = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 0
  %95 = load i32, i32* %arrayidx47, align 4, !tbaa !25
  %mul48 = mul nsw i32 %mul45, %95
  %96 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img49 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %96, i32 0, i32 3
  %planes = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img49, i32 0, i32 16
  %arrayidx50 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes, i32 0, i32 0
  %97 = load i8*, i8** %arrayidx50, align 8, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %97, i32 %mul48
  store i8* %add.ptr, i8** %arrayidx50, align 8, !tbaa !2
  %98 = load i32, i32* %num_planes, align 4, !tbaa !25
  %cmp51 = icmp sgt i32 %98, 1
  br i1 %cmp51, label %if.then52, label %if.end63

if.then52:                                        ; preds = %cond.end
  store i32 1, i32* %plane, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then52
  %99 = load i32, i32* %plane, align 4, !tbaa !25
  %cmp53 = icmp slt i32 %99, 3
  br i1 %cmp53, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %100 = load i32, i32* %mi_row, align 4, !tbaa !25
  %101 = load i32, i32* %ssy, align 4, !tbaa !25
  %shr = ashr i32 4, %101
  %mul54 = mul nsw i32 %100, %shr
  %102 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img55 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %102, i32 0, i32 3
  %stride56 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img55, i32 0, i32 17
  %103 = load i32, i32* %plane, align 4, !tbaa !25
  %arrayidx57 = getelementptr inbounds [3 x i32], [3 x i32]* %stride56, i32 0, i32 %103
  %104 = load i32, i32* %arrayidx57, align 4, !tbaa !25
  %mul58 = mul nsw i32 %mul54, %104
  %105 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img59 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %105, i32 0, i32 3
  %planes60 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img59, i32 0, i32 16
  %106 = load i32, i32* %plane, align 4, !tbaa !25
  %arrayidx61 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes60, i32 0, i32 %106
  %107 = load i8*, i8** %arrayidx61, align 4, !tbaa !2
  %add.ptr62 = getelementptr inbounds i8, i8* %107, i32 %mul58
  store i8* %add.ptr62, i8** %arrayidx61, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %108 = load i32, i32* %plane, align 4, !tbaa !25
  %inc = add nsw i32 %108, 1
  store i32 %inc, i32* %plane, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end63

if.end63:                                         ; preds = %for.end, %cond.end
  %109 = load i32, i32* %tile_height, align 4, !tbaa !25
  %110 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %110, i32 0, i32 22
  %mi_rows = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params, i32 0, i32 3
  %111 = load i32, i32* %mi_rows, align 4, !tbaa !103
  %112 = load i32, i32* %mi_row, align 4, !tbaa !25
  %sub64 = sub nsw i32 %111, %112
  %cmp65 = icmp slt i32 %109, %sub64
  br i1 %cmp65, label %cond.true66, label %cond.false67

cond.true66:                                      ; preds = %if.end63
  %113 = load i32, i32* %tile_height, align 4, !tbaa !25
  br label %cond.end71

cond.false67:                                     ; preds = %if.end63
  %114 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params68 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %114, i32 0, i32 22
  %mi_rows69 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params68, i32 0, i32 3
  %115 = load i32, i32* %mi_rows69, align 4, !tbaa !103
  %116 = load i32, i32* %mi_row, align 4, !tbaa !25
  %sub70 = sub nsw i32 %115, %116
  br label %cond.end71

cond.end71:                                       ; preds = %cond.false67, %cond.true66
  %cond72 = phi i32 [ %113, %cond.true66 ], [ %sub70, %cond.false67 ]
  %mul73 = mul nsw i32 %cond72, 4
  %117 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img74 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %117, i32 0, i32 3
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img74, i32 0, i32 11
  store i32 %mul73, i32* %d_h, align 4, !tbaa !104
  %118 = bitcast i32* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %ssy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %mi_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast i32* %tile_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %122 = bitcast i32* %tile_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #5
  %123 = bitcast i32* %tile_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #5
  br label %if.end75

if.end75:                                         ; preds = %cond.end71, %land.lhs.true36, %land.lhs.true34, %if.end30
  %124 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %ext_tile_debug76 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %124, i32 0, i32 42
  %125 = load i32, i32* %ext_tile_debug76, align 4, !tbaa !97
  %tobool77 = icmp ne i32 %125, 0
  br i1 %tobool77, label %land.lhs.true78, label %if.end138

land.lhs.true78:                                  ; preds = %if.end75
  %126 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %single_tile_decoding79 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %126, i32 0, i32 18
  %127 = load i32, i32* %single_tile_decoding79, align 4, !tbaa !99
  %tobool80 = icmp ne i32 %127, 0
  br i1 %tobool80, label %land.lhs.true81, label %if.end138

land.lhs.true81:                                  ; preds = %land.lhs.true78
  %128 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_col = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %128, i32 0, i32 26
  %129 = load i32, i32* %dec_tile_col, align 8, !tbaa !105
  %cmp82 = icmp sge i32 %129, 0
  br i1 %cmp82, label %if.then83, label %if.end138

if.then83:                                        ; preds = %land.lhs.true81
  %130 = bitcast i32* %tile_width84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #5
  %131 = bitcast i32* %tile_height85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #5
  %132 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  call void @av1_get_uniform_tile_size(%struct.AV1Common* %132, i32* %tile_width84, i32* %tile_height85)
  %133 = bitcast i32* %tile_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %133) #5
  %134 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_col86 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %134, i32 0, i32 26
  %135 = load i32, i32* %dec_tile_col86, align 8, !tbaa !105
  %136 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %cols = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %136, i32 0, i32 0
  %137 = load i32, i32* %cols, align 4, !tbaa !106
  %sub87 = sub nsw i32 %137, 1
  %cmp88 = icmp slt i32 %135, %sub87
  br i1 %cmp88, label %cond.true89, label %cond.false91

cond.true89:                                      ; preds = %if.then83
  %138 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_col90 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %138, i32 0, i32 26
  %139 = load i32, i32* %dec_tile_col90, align 8, !tbaa !105
  br label %cond.end94

cond.false91:                                     ; preds = %if.then83
  %140 = load %struct.CommonTileParams*, %struct.CommonTileParams** %tiles, align 4, !tbaa !2
  %cols92 = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %140, i32 0, i32 0
  %141 = load i32, i32* %cols92, align 4, !tbaa !106
  %sub93 = sub nsw i32 %141, 1
  br label %cond.end94

cond.end94:                                       ; preds = %cond.false91, %cond.true89
  %cond95 = phi i32 [ %139, %cond.true89 ], [ %sub93, %cond.false91 ]
  store i32 %cond95, i32* %tile_col, align 4, !tbaa !25
  %142 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #5
  %143 = load i32, i32* %tile_col, align 4, !tbaa !25
  %144 = load i32, i32* %tile_width84, align 4, !tbaa !25
  %mul96 = mul nsw i32 %143, %144
  store i32 %mul96, i32* %mi_col, align 4, !tbaa !25
  %145 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #5
  %146 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img97 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %146, i32 0, i32 3
  %x_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img97, i32 0, i32 14
  %147 = load i32, i32* %x_chroma_shift, align 8, !tbaa !107
  store i32 %147, i32* %ssx, align 4, !tbaa !25
  %148 = bitcast i32* %is_hbd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #5
  %149 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img98 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %149, i32 0, i32 3
  %fmt = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img98, i32 0, i32 0
  %150 = load i32, i32* %fmt, align 8, !tbaa !108
  %and = and i32 %150, 2048
  %tobool99 = icmp ne i32 %and, 0
  %151 = zext i1 %tobool99 to i64
  %cond100 = select i1 %tobool99, i32 1, i32 0
  store i32 %cond100, i32* %is_hbd, align 4, !tbaa !25
  %152 = bitcast i32* %plane101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %152) #5
  %153 = load i32, i32* %mi_col, align 4, !tbaa !25
  %mul102 = mul nsw i32 %153, 4
  %154 = load i32, i32* %is_hbd, align 4, !tbaa !25
  %add103 = add nsw i32 1, %154
  %mul104 = mul nsw i32 %mul102, %add103
  %155 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img105 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %155, i32 0, i32 3
  %planes106 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img105, i32 0, i32 16
  %arrayidx107 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes106, i32 0, i32 0
  %156 = load i8*, i8** %arrayidx107, align 8, !tbaa !2
  %add.ptr108 = getelementptr inbounds i8, i8* %156, i32 %mul104
  store i8* %add.ptr108, i8** %arrayidx107, align 8, !tbaa !2
  %157 = load i32, i32* %num_planes, align 4, !tbaa !25
  %cmp109 = icmp sgt i32 %157, 1
  br i1 %cmp109, label %if.then110, label %if.end125

if.then110:                                       ; preds = %cond.end94
  store i32 1, i32* %plane101, align 4, !tbaa !25
  br label %for.cond111

for.cond111:                                      ; preds = %for.inc122, %if.then110
  %158 = load i32, i32* %plane101, align 4, !tbaa !25
  %cmp112 = icmp slt i32 %158, 3
  br i1 %cmp112, label %for.body113, label %for.end124

for.body113:                                      ; preds = %for.cond111
  %159 = load i32, i32* %mi_col, align 4, !tbaa !25
  %160 = load i32, i32* %ssx, align 4, !tbaa !25
  %shr114 = ashr i32 4, %160
  %mul115 = mul nsw i32 %159, %shr114
  %161 = load i32, i32* %is_hbd, align 4, !tbaa !25
  %add116 = add nsw i32 1, %161
  %mul117 = mul nsw i32 %mul115, %add116
  %162 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img118 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %162, i32 0, i32 3
  %planes119 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img118, i32 0, i32 16
  %163 = load i32, i32* %plane101, align 4, !tbaa !25
  %arrayidx120 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes119, i32 0, i32 %163
  %164 = load i8*, i8** %arrayidx120, align 4, !tbaa !2
  %add.ptr121 = getelementptr inbounds i8, i8* %164, i32 %mul117
  store i8* %add.ptr121, i8** %arrayidx120, align 4, !tbaa !2
  br label %for.inc122

for.inc122:                                       ; preds = %for.body113
  %165 = load i32, i32* %plane101, align 4, !tbaa !25
  %inc123 = add nsw i32 %165, 1
  store i32 %inc123, i32* %plane101, align 4, !tbaa !25
  br label %for.cond111

for.end124:                                       ; preds = %for.cond111
  br label %if.end125

if.end125:                                        ; preds = %for.end124, %cond.end94
  %166 = load i32, i32* %tile_width84, align 4, !tbaa !25
  %167 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params126 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %167, i32 0, i32 22
  %mi_cols = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params126, i32 0, i32 4
  %168 = load i32, i32* %mi_cols, align 4, !tbaa !109
  %169 = load i32, i32* %mi_col, align 4, !tbaa !25
  %sub127 = sub nsw i32 %168, %169
  %cmp128 = icmp slt i32 %166, %sub127
  br i1 %cmp128, label %cond.true129, label %cond.false130

cond.true129:                                     ; preds = %if.end125
  %170 = load i32, i32* %tile_width84, align 4, !tbaa !25
  br label %cond.end134

cond.false130:                                    ; preds = %if.end125
  %171 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %mi_params131 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %171, i32 0, i32 22
  %mi_cols132 = getelementptr inbounds %struct.CommonModeInfoParams, %struct.CommonModeInfoParams* %mi_params131, i32 0, i32 4
  %172 = load i32, i32* %mi_cols132, align 4, !tbaa !109
  %173 = load i32, i32* %mi_col, align 4, !tbaa !25
  %sub133 = sub nsw i32 %172, %173
  br label %cond.end134

cond.end134:                                      ; preds = %cond.false130, %cond.true129
  %cond135 = phi i32 [ %170, %cond.true129 ], [ %sub133, %cond.false130 ]
  %mul136 = mul nsw i32 %cond135, 4
  %174 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img137 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %174, i32 0, i32 3
  %d_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img137, i32 0, i32 10
  store i32 %mul136, i32* %d_w, align 8, !tbaa !110
  %175 = bitcast i32* %plane101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #5
  %176 = bitcast i32* %is_hbd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #5
  %177 = bitcast i32* %ssx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #5
  %178 = bitcast i32* %mi_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #5
  %179 = bitcast i32* %tile_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #5
  %180 = bitcast i32* %tile_height85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #5
  %181 = bitcast i32* %tile_width84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #5
  br label %if.end138

if.end138:                                        ; preds = %cond.end134, %land.lhs.true81, %land.lhs.true78, %if.end75
  %182 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %output_frame_buf, align 4, !tbaa !2
  %raw_frame_buffer = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %182, i32 0, i32 16
  %priv = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer, i32 0, i32 2
  %183 = load i8*, i8** %priv, align 4, !tbaa !111
  %184 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img139 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %184, i32 0, i32 3
  %fb_priv = getelementptr inbounds %struct.aom_image, %struct.aom_image* %img139, i32 0, i32 27
  store i8* %183, i8** %fb_priv, align 4, !tbaa !116
  %185 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %img140 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %185, i32 0, i32 3
  store %struct.aom_image* %img140, %struct.aom_image** %img, align 4, !tbaa !2
  %186 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %temporal_layer_id = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %186, i32 0, i32 50
  %187 = load i32, i32* %temporal_layer_id, align 16, !tbaa !117
  %188 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !2
  %temporal_id = getelementptr inbounds %struct.aom_image, %struct.aom_image* %188, i32 0, i32 20
  store i32 %187, i32* %temporal_id, align 4, !tbaa !118
  %189 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %spatial_layer_id = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %189, i32 0, i32 52
  %190 = load i32, i32* %spatial_layer_id, align 8, !tbaa !119
  %191 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !2
  %spatial_id = getelementptr inbounds %struct.aom_image, %struct.aom_image* %191, i32 0, i32 21
  store i32 %190, i32* %spatial_id, align 4, !tbaa !120
  %192 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %skip_film_grain = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %192, i32 0, i32 53
  %193 = load i32, i32* %skip_film_grain, align 4, !tbaa !121
  %tobool141 = icmp ne i32 %193, 0
  br i1 %tobool141, label %if.then142, label %if.end143

if.then142:                                       ; preds = %if.end138
  %194 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain_params, align 4, !tbaa !2
  %apply_grain = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %194, i32 0, i32 0
  store i32 0, i32* %apply_grain, align 4, !tbaa !122
  br label %if.end143

if.end143:                                        ; preds = %if.then142, %if.end138
  %195 = bitcast %struct.aom_image** %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %195) #5
  %196 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %197 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !2
  %198 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %image_with_grain = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %198, i32 0, i32 21
  %199 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain_params, align 4, !tbaa !2
  %call144 = call %struct.aom_image* @add_grain_if_needed(%struct.aom_codec_alg_priv* %196, %struct.aom_image* %197, %struct.aom_image* %image_with_grain, %struct.aom_film_grain_t* %199)
  store %struct.aom_image* %call144, %struct.aom_image** %res, align 4, !tbaa !2
  %200 = load %struct.aom_image*, %struct.aom_image** %res, align 4, !tbaa !2
  %tobool145 = icmp ne %struct.aom_image* %200, null
  br i1 %tobool145, label %if.end148, label %if.then146

if.then146:                                       ; preds = %if.end143
  %201 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %common147 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %201, i32 0, i32 1
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common147, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 7, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.5, i32 0, i32 0))
  br label %if.end148

if.end148:                                        ; preds = %if.then146, %if.end143
  %202 = load i32*, i32** %index, align 4, !tbaa !2
  %203 = load i32, i32* %202, align 4, !tbaa !79
  %add149 = add i32 %203, 1
  store i32 %add149, i32* %202, align 4, !tbaa !79
  %204 = load %struct.aom_image*, %struct.aom_image** %res, align 4, !tbaa !2
  store %struct.aom_image* %204, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %205 = bitcast %struct.aom_image** %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #5
  %206 = bitcast i32* %num_planes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #5
  br label %cleanup

cleanup:                                          ; preds = %if.end148, %if.then25, %if.then18
  %207 = bitcast %struct.RefCntBuffer** %output_frame_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #5
  br label %cleanup151

if.end150:                                        ; preds = %if.end12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup151

cleanup151:                                       ; preds = %if.end150, %cleanup
  %208 = bitcast %struct.aom_film_grain_t** %grain_params to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #5
  %209 = bitcast %struct.yv12_buffer_config** %sd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup159 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup151
  br label %if.end158

if.else:                                          ; preds = %if.then1
  %210 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %received_frame153 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %210, i32 0, i32 5
  store i32 0, i32* %received_frame153, align 4, !tbaa !93
  %211 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %need_resync154 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %211, i32 0, i32 24
  store i32 1, i32* %need_resync154, align 8, !tbaa !95
  %212 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %flushed = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %212, i32 0, i32 5
  %213 = load i32, i32* %flushed, align 4, !tbaa !16
  %cmp155 = icmp ne i32 %213, 1
  br i1 %cmp155, label %if.then156, label %if.end157

if.then156:                                       ; preds = %if.else
  store %struct.aom_image* null, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup159

if.end157:                                        ; preds = %if.else
  br label %if.end158

if.end158:                                        ; preds = %if.end157, %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup159

cleanup159:                                       ; preds = %if.end158, %if.then156, %cleanup151
  %214 = bitcast %struct.CommonTileParams** %tiles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #5
  %215 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #5
  %216 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #5
  %217 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #5
  %218 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #5
  %219 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #5
  %cleanup.dest165 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest165, label %cleanup168 [
    i32 0, label %cleanup.cont166
  ]

cleanup.cont166:                                  ; preds = %cleanup159
  br label %if.end167

if.end167:                                        ; preds = %cleanup.cont166, %if.end
  store %struct.aom_image* null, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup168

cleanup168:                                       ; preds = %if.end167, %cleanup159
  %220 = bitcast i32** %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #5
  br label %cleanup169

cleanup169:                                       ; preds = %cleanup168, %if.then
  %221 = bitcast %struct.aom_image** %img to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #5
  %222 = load %struct.aom_image*, %struct.aom_image** %retval, align 4
  ret %struct.aom_image* %222
}

; Function Attrs: nounwind
define internal i32 @decoder_set_fb_fn(%struct.aom_codec_alg_priv* %ctx, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %cb_get, i32 (i8*, %struct.aom_codec_frame_buffer*)* %cb_release, i8* %cb_priv) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %cb_get.addr = alloca i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, align 4
  %cb_release.addr = alloca i32 (i8*, %struct.aom_codec_frame_buffer*)*, align 4
  %cb_priv.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %cb_get, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb_get.addr, align 4, !tbaa !2
  store i32 (i8*, %struct.aom_codec_frame_buffer*)* %cb_release, i32 (i8*, %struct.aom_codec_frame_buffer*)** %cb_release.addr, align 4, !tbaa !2
  store i8* %cb_priv, i8** %cb_priv.addr, align 4, !tbaa !2
  %0 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb_get.addr, align 4, !tbaa !2
  %cmp = icmp eq i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %cb_release.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i32 (i8*, %struct.aom_codec_frame_buffer*)* %1, null
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 8, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %lor.lhs.false
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 20
  %3 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %cmp2 = icmp eq %struct.AVxWorker* %3, null
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %4 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb_get.addr, align 4, !tbaa !2
  %5 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %get_ext_fb_cb = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %5, i32 0, i32 27
  store i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %4, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %get_ext_fb_cb, align 4, !tbaa !123
  %6 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %cb_release.addr, align 4, !tbaa !2
  %7 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %release_ext_fb_cb = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %7, i32 0, i32 28
  store i32 (i8*, %struct.aom_codec_frame_buffer*)* %6, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_ext_fb_cb, align 8, !tbaa !124
  %8 = load i8*, i8** %cb_priv.addr, align 4, !tbaa !2
  %9 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_priv = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %9, i32 0, i32 26
  store i8* %8, i8** %ext_priv, align 8, !tbaa !125
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @aom_calloc(i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare %struct.AVxWorkerInterface* @aom_get_worker_interface() #2

declare void @aom_free(i8*) #2

declare void @av1_remove_common(%struct.AV1Common*) #2

declare void @av1_free_restoration_buffers(%struct.AV1Common*) #2

declare void @av1_decoder_remove(%struct.AV1Decoder*) #2

declare void @av1_free_ref_frame_buffers(%struct.BufferPool*) #2

declare void @av1_free_internal_frame_buffers(%struct.InternalFrameBufferList*) #2

declare void @aom_img_free(%struct.aom_image*) #2

; Function Attrs: nounwind
define internal i32 @ctrl_copy_reference(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %frame = alloca %struct.av1_ref_frame*, align 4
  %sd = alloca %struct.yv12_buffer_config, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast %struct.av1_ref_frame** %frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to %struct.av1_ref_frame**
  %2 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %1, align 4
  store %struct.av1_ref_frame* %2, %struct.av1_ref_frame** %frame, align 4, !tbaa !2
  %3 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %frame, align 4, !tbaa !2
  %tobool = icmp ne %struct.av1_ref_frame* %3, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast %struct.yv12_buffer_config* %sd to i8*
  call void @llvm.lifetime.start.p0i8(i64 148, i8* %4) #5
  %5 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %6, i32 0, i32 20
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %7, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %frame, align 4, !tbaa !2
  %img = getelementptr inbounds %struct.av1_ref_frame, %struct.av1_ref_frame* %12, i32 0, i32 2
  %call = call i32 @image2yuvconfig(%struct.aom_image* %img, %struct.yv12_buffer_config* %sd)
  %13 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %13, i32 0, i32 0
  %14 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %15 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %frame, align 4, !tbaa !2
  %idx = getelementptr inbounds %struct.av1_ref_frame, %struct.av1_ref_frame* %15, i32 0, i32 0
  %16 = load i32, i32* %idx, align 4, !tbaa !126
  %call1 = call i32 @av1_copy_reference_dec(%struct.AV1Decoder* %14, i32 %16, %struct.yv12_buffer_config* %sd)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %17 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast %struct.yv12_buffer_config* %sd to i8*
  call void @llvm.lifetime.end.p0i8(i64 148, i8* %19) #5
  br label %cleanup

if.else:                                          ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %20 = bitcast %struct.av1_ref_frame** %frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_reference(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %data = alloca %struct.av1_ref_frame*, align 4
  %frame = alloca %struct.av1_ref_frame*, align 4
  %sd = alloca %struct.yv12_buffer_config, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast %struct.av1_ref_frame** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to %struct.av1_ref_frame**
  %2 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %1, align 4
  store %struct.av1_ref_frame* %2, %struct.av1_ref_frame** %data, align 4, !tbaa !2
  %3 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %data, align 4, !tbaa !2
  %tobool = icmp ne %struct.av1_ref_frame* %3, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast %struct.av1_ref_frame** %frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %data, align 4, !tbaa !2
  store %struct.av1_ref_frame* %5, %struct.av1_ref_frame** %frame, align 4, !tbaa !2
  %6 = bitcast %struct.yv12_buffer_config* %sd to i8*
  call void @llvm.lifetime.start.p0i8(i64 148, i8* %6) #5
  %7 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %8, i32 0, i32 20
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %9, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %10 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %11, i32 0, i32 4
  %12 = load i8*, i8** %data1, align 4, !tbaa !32
  %13 = bitcast i8* %12 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %13, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %14 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %frame, align 4, !tbaa !2
  %img = getelementptr inbounds %struct.av1_ref_frame, %struct.av1_ref_frame* %14, i32 0, i32 2
  %call = call i32 @image2yuvconfig(%struct.aom_image* %img, %struct.yv12_buffer_config* %sd)
  %15 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %15, i32 0, i32 0
  %16 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %16, i32 0, i32 1
  %17 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %frame, align 4, !tbaa !2
  %idx = getelementptr inbounds %struct.av1_ref_frame, %struct.av1_ref_frame* %17, i32 0, i32 0
  %18 = load i32, i32* %idx, align 4, !tbaa !126
  %19 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %frame, align 4, !tbaa !2
  %use_external_ref = getelementptr inbounds %struct.av1_ref_frame, %struct.av1_ref_frame* %19, i32 0, i32 1
  %20 = load i32, i32* %use_external_ref, align 4, !tbaa !128
  %call1 = call i32 @av1_set_reference_dec(%struct.AV1Common* %common, i32 %18, i32 %20, %struct.yv12_buffer_config* %sd)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %21 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast %struct.yv12_buffer_config* %sd to i8*
  call void @llvm.lifetime.end.p0i8(i64 148, i8* %23) #5
  %24 = bitcast %struct.av1_ref_frame** %frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %cleanup

if.else:                                          ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then
  %25 = bitcast %struct.av1_ref_frame** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_invert_tile_order(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %invert_tile_order = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 6
  store i32 %1, i32* %invert_tile_order, align 8, !tbaa !129
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_byte_alignment(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %legacy_byte_alignment = alloca i32, align 4
  %min_byte_alignment = alloca i32, align 4
  %max_byte_alignment = alloca i32, align 4
  %byte_alignment = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32* %legacy_byte_alignment to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %legacy_byte_alignment, align 4, !tbaa !25
  %1 = bitcast i32* %min_byte_alignment to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 32, i32* %min_byte_alignment, align 4, !tbaa !25
  %2 = bitcast i32* %max_byte_alignment to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 1024, i32* %max_byte_alignment, align 4, !tbaa !25
  %3 = bitcast i32* %byte_alignment to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %4 = bitcast i8* %argp.cur to i32*
  %5 = load i32, i32* %4, align 4
  store i32 %5, i32* %byte_alignment, align 4, !tbaa !25
  %6 = load i32, i32* %byte_alignment, align 4, !tbaa !25
  %cmp = icmp ne i32 %6, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %7 = load i32, i32* %byte_alignment, align 4, !tbaa !25
  %cmp1 = icmp slt i32 %7, 32
  br i1 %cmp1, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %8 = load i32, i32* %byte_alignment, align 4, !tbaa !25
  %cmp2 = icmp sgt i32 %8, 1024
  br i1 %cmp2, label %if.then, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %9 = load i32, i32* %byte_alignment, align 4, !tbaa !25
  %10 = load i32, i32* %byte_alignment, align 4, !tbaa !25
  %sub = sub nsw i32 %10, 1
  %and = and i32 %9, %sub
  %cmp4 = icmp ne i32 %and, 0
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false3, %lor.lhs.false, %land.lhs.true
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false3, %entry
  %11 = load i32, i32* %byte_alignment, align 4, !tbaa !25
  %12 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %byte_alignment5 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %12, i32 0, i32 8
  store i32 %11, i32* %byte_alignment5, align 8, !tbaa !130
  %13 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %13, i32 0, i32 20
  %14 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool = icmp ne %struct.AVxWorker* %14, null
  br i1 %tobool, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %15 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker7 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %16, i32 0, i32 20
  %17 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker7, align 8, !tbaa !31
  store %struct.AVxWorker* %17, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %18 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %19, i32 0, i32 4
  %20 = load i8*, i8** %data1, align 4, !tbaa !32
  %21 = bitcast i8* %20 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %21, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %22 = load i32, i32* %byte_alignment, align 4, !tbaa !25
  %23 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %23, i32 0, i32 0
  %24 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %24, i32 0, i32 1
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common, i32 0, i32 21
  %byte_alignment8 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 15
  store i32 %22, i32* %byte_alignment8, align 4, !tbaa !131
  %25 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end9, %if.then
  %27 = bitcast i32* %byte_alignment to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast i32* %max_byte_alignment to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i32* %min_byte_alignment to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  %30 = bitcast i32* %legacy_byte_alignment to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_skip_loop_filter(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %skip_loop_filter = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 9
  store i32 %1, i32* %skip_loop_filter, align 4, !tbaa !132
  %3 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %3, i32 0, i32 20
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool = icmp ne %struct.AVxWorker* %4, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker1 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %6, i32 0, i32 20
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker1, align 8, !tbaa !31
  store %struct.AVxWorker* %7, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %skip_loop_filter2 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %12, i32 0, i32 9
  %13 = load i32, i32* %skip_loop_filter2, align 4, !tbaa !132
  %14 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %14, i32 0, i32 0
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %skip_loop_filter3 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %15, i32 0, i32 52
  store i32 %13, i32* %skip_loop_filter3, align 16, !tbaa !133
  %16 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_decode_tile_row(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %decode_tile_row = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 11
  store i32 %1, i32* %decode_tile_row, align 4, !tbaa !29
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_decode_tile_col(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %decode_tile_col = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 12
  store i32 %1, i32* %decode_tile_col, align 8, !tbaa !30
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_tile_mode(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %tile_mode = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 13
  store i32 %1, i32* %tile_mode, align 4, !tbaa !28
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_is_annexb(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %is_annexb = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 17
  store i32 %1, i32* %is_annexb, align 4, !tbaa !89
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_operating_point(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %operating_point = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 18
  store i32 %1, i32* %operating_point, align 8, !tbaa !134
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_output_all_layers(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %output_all_layers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 19
  store i32 %1, i32* %output_all_layers, align 4, !tbaa !135
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_inspection_callback(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %args.addr, align 4, !tbaa !2
  ret i32 4
}

; Function Attrs: nounwind
define internal i32 @ctrl_ext_tile_debug(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_tile_debug = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 14
  store i32 %1, i32* %ext_tile_debug, align 8, !tbaa !136
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_row_mt(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %row_mt = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 15
  store i32 %1, i32* %row_mt, align 4, !tbaa !27
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_ext_ref_ptr(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %data = alloca %struct.av1_ext_ref_frame*, align 4
  %ext_frames = alloca %struct.av1_ext_ref_frame*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast %struct.av1_ext_ref_frame** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to %struct.av1_ext_ref_frame**
  %2 = load %struct.av1_ext_ref_frame*, %struct.av1_ext_ref_frame** %1, align 4
  store %struct.av1_ext_ref_frame* %2, %struct.av1_ext_ref_frame** %data, align 4, !tbaa !2
  %3 = load %struct.av1_ext_ref_frame*, %struct.av1_ext_ref_frame** %data, align 4, !tbaa !2
  %tobool = icmp ne %struct.av1_ext_ref_frame* %3, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast %struct.av1_ext_ref_frame** %ext_frames to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.av1_ext_ref_frame*, %struct.av1_ext_ref_frame** %data, align 4, !tbaa !2
  store %struct.av1_ext_ref_frame* %5, %struct.av1_ext_ref_frame** %ext_frames, align 4, !tbaa !2
  %6 = load %struct.av1_ext_ref_frame*, %struct.av1_ext_ref_frame** %ext_frames, align 4, !tbaa !2
  %num = getelementptr inbounds %struct.av1_ext_ref_frame, %struct.av1_ext_ref_frame* %6, i32 0, i32 1
  %7 = load i32, i32* %num, align 4, !tbaa !137
  %8 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_refs = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %8, i32 0, i32 16
  %num1 = getelementptr inbounds %struct.EXTERNAL_REFERENCES, %struct.EXTERNAL_REFERENCES* %ext_refs, i32 0, i32 1
  store i32 %7, i32* %num1, align 8, !tbaa !139
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %10 = load i32, i32* %i, align 4, !tbaa !25
  %11 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_refs2 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %11, i32 0, i32 16
  %num3 = getelementptr inbounds %struct.EXTERNAL_REFERENCES, %struct.EXTERNAL_REFERENCES* %ext_refs2, i32 0, i32 1
  %12 = load i32, i32* %num3, align 8, !tbaa !139
  %cmp = icmp slt i32 %10, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %struct.av1_ext_ref_frame*, %struct.av1_ext_ref_frame** %ext_frames, align 4, !tbaa !2
  %img = getelementptr inbounds %struct.av1_ext_ref_frame, %struct.av1_ext_ref_frame* %14, i32 0, i32 0
  %15 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !140
  %incdec.ptr = getelementptr inbounds %struct.aom_image, %struct.aom_image* %15, i32 1
  store %struct.aom_image* %incdec.ptr, %struct.aom_image** %img, align 4, !tbaa !140
  %16 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_refs4 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %16, i32 0, i32 16
  %refs = getelementptr inbounds %struct.EXTERNAL_REFERENCES, %struct.EXTERNAL_REFERENCES* %ext_refs4, i32 0, i32 0
  %17 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [128 x %struct.yv12_buffer_config], [128 x %struct.yv12_buffer_config]* %refs, i32 0, i32 %17
  %call = call i32 @image2yuvconfig(%struct.aom_image* %15, %struct.yv12_buffer_config* %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %19 = bitcast %struct.av1_ext_ref_frame** %ext_frames to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  br label %cleanup

if.else:                                          ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %for.end
  %20 = bitcast %struct.av1_ext_ref_frame** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define internal i32 @ctrl_set_skip_film_grain(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %0 = bitcast i8* %argp.cur to i32*
  %1 = load i32, i32* %0, align 4
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %skip_film_grain = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 10
  store i32 %1, i32* %skip_film_grain, align 8, !tbaa !141
  %3 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %3, i32 0, i32 20
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool = icmp ne %struct.AVxWorker* %4, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker1 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %6, i32 0, i32 20
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker1, align 8, !tbaa !31
  store %struct.AVxWorker* %7, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %skip_film_grain2 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %12, i32 0, i32 10
  %13 = load i32, i32* %skip_film_grain2, align 8, !tbaa !141
  %14 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %14, i32 0, i32 0
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %skip_film_grain3 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %15, i32 0, i32 53
  store i32 %13, i32* %skip_film_grain3, align 4, !tbaa !121
  %16 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_frame_corrupted(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %corrupted = alloca i32*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %pbi = alloca %struct.AV1Decoder*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %corrupted to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %corrupted, align 4, !tbaa !2
  %3 = load i32*, i32** %corrupted, align 4, !tbaa !2
  %tobool = icmp ne i32* %3, null
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool1 = icmp ne %struct.AVxWorker* %5, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %6 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker3 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %7, i32 0, i32 20
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker3, align 8, !tbaa !31
  store %struct.AVxWorker* %8, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %9 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 4
  %11 = load i8*, i8** %data1, align 4, !tbaa !32
  %12 = bitcast i8* %11 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %12, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %13 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi4 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %14, i32 0, i32 0
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi4, align 4, !tbaa !36
  store %struct.AV1Decoder* %15, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %16 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %seen_frame_header = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %16, i32 0, i32 33
  %17 = load i32, i32* %seen_frame_header, align 4, !tbaa !142
  %tobool5 = icmp ne i32 %17, 0
  br i1 %tobool5, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then2
  %18 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %num_output_frames = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %18, i32 0, i32 16
  %19 = load i32, i32* %num_output_frames, align 16, !tbaa !84
  %cmp = icmp eq i32 %19, 0
  br i1 %cmp, label %if.then6, label %if.end

if.then6:                                         ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %if.then2
  %20 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %last_show_frame = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %20, i32 0, i32 7
  %21 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %last_show_frame, align 4, !tbaa !94
  %cmp7 = icmp ne %struct.RefCntBuffer* %21, null
  br i1 %cmp7, label %if.then8, label %if.end11

if.then8:                                         ; preds = %if.end
  %22 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %last_show_frame9 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %22, i32 0, i32 7
  %23 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %last_show_frame9, align 4, !tbaa !94
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %23, i32 0, i32 17
  %corrupted10 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %buf, i32 0, i32 25
  %24 = load i32, i32* %corrupted10, align 4, !tbaa !143
  %25 = load i32*, i32** %corrupted, align 4, !tbaa !2
  store i32 %24, i32* %25, align 4, !tbaa !25
  br label %if.end11

if.end11:                                         ; preds = %if.then8, %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end11, %if.then6
  %26 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  br label %cleanup15

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup15

if.end14:                                         ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup15

cleanup15:                                        ; preds = %if.end14, %if.else, %cleanup
  %29 = bitcast i32** %corrupted to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  %30 = load i32, i32* %retval, align 4
  ret i32 %30
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_last_quantizer(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %arg = alloca i32*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %arg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %arg, align 4, !tbaa !2
  %3 = load i32*, i32** %arg, align 4, !tbaa !2
  %cmp = icmp eq i32* %3, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %5, i32 0, i32 4
  %6 = load i8*, i8** %data1, align 4, !tbaa !32
  %7 = bitcast i8* %6 to %struct.FrameWorkerData*
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %7, i32 0, i32 0
  %8 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %8, i32 0, i32 1
  %quant_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common, i32 0, i32 23
  %base_qindex = getelementptr inbounds %struct.CommonQuantParams, %struct.CommonQuantParams* %quant_params, i32 0, i32 0
  %9 = load i32, i32* %base_qindex, align 4, !tbaa !144
  %10 = load i32*, i32** %arg, align 4, !tbaa !2
  store i32 %9, i32* %10, align 4, !tbaa !25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %11 = bitcast i32** %arg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #5
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_last_ref_updates(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %update_info = alloca i32*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %update_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %update_info, align 4, !tbaa !2
  %3 = load i32*, i32** %update_info, align 4, !tbaa !2
  %tobool = icmp ne i32* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool1 = icmp ne %struct.AVxWorker* %5, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %6 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker3 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %7, i32 0, i32 20
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker3, align 8, !tbaa !31
  store %struct.AVxWorker* %8, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %9 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 4
  %11 = load i8*, i8** %data1, align 4, !tbaa !32
  %12 = bitcast i8* %11 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %12, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %13 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %13, i32 0, i32 0
  %14 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %14, i32 0, i32 1
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common, i32 0, i32 0
  %refresh_frame_flags = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 6
  %15 = load i32, i32* %refresh_frame_flags, align 32, !tbaa !145
  %16 = load i32*, i32** %update_info, align 4, !tbaa !2
  store i32 %15, i32* %16, align 4, !tbaa !25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %17 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else, %if.then2
  %19 = bitcast i32** %update_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_bit_depth(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %bit_depth = alloca i32*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %bit_depth, align 4, !tbaa !2
  %3 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %5, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %6 = load i32*, i32** %bit_depth, align 4, !tbaa !2
  %tobool = icmp ne i32* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.AVxWorker* %7, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %13, i32 0, i32 0
  %14 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %14, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 37
  %bit_depth3 = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 25
  %16 = load i32, i32* %bit_depth3, align 8, !tbaa !146
  %17 = load i32*, i32** %bit_depth, align 4, !tbaa !2
  store i32 %16, i32* %17, align 4, !tbaa !25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %18 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else, %if.then2
  %20 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = bitcast i32** %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_img_format(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %img_fmt = alloca i32*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %img_fmt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %img_fmt, align 4, !tbaa !2
  %3 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %5, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %6 = load i32*, i32** %img_fmt, align 4, !tbaa !2
  %tobool = icmp ne i32* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.AVxWorker* %7, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %13, i32 0, i32 0
  %14 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %14, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 37
  %subsampling_x = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 32
  %16 = load i32, i32* %subsampling_x, align 16, !tbaa !147
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %seq_params3 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %17, i32 0, i32 37
  %subsampling_y = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params3, i32 0, i32 33
  %18 = load i32, i32* %subsampling_y, align 4, !tbaa !148
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %seq_params4 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %19, i32 0, i32 37
  %use_highbitdepth = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params4, i32 0, i32 26
  %20 = load i8, i8* %use_highbitdepth, align 4, !tbaa !149
  %conv = zext i8 %20 to i32
  %call = call i32 @get_img_format(i32 %16, i32 %18, i32 %conv)
  %21 = load i32*, i32** %img_fmt, align 4, !tbaa !2
  store i32 %call, i32* %21, align 4, !tbaa !23
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %22 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else, %if.then2
  %24 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  %25 = bitcast i32** %img_fmt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_tile_size(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %tile_size = alloca i32*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %tile_width = alloca i32, align 4
  %tile_height = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %tile_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %tile_size, align 4, !tbaa !2
  %3 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %5, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %6 = load i32*, i32** %tile_size, align 4, !tbaa !2
  %tobool = icmp ne i32* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.AVxWorker* %7, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %13, i32 0, i32 0
  %14 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %14, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %15 = bitcast i32* %tile_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  %16 = bitcast i32* %tile_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  %17 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  call void @av1_get_uniform_tile_size(%struct.AV1Common* %17, i32* %tile_width, i32* %tile_height)
  %18 = load i32, i32* %tile_width, align 4, !tbaa !25
  %mul = mul nsw i32 %18, 4
  %shl = shl i32 %mul, 16
  %19 = load i32, i32* %tile_height, align 4, !tbaa !25
  %mul3 = mul nsw i32 %19, 4
  %add = add nsw i32 %shl, %mul3
  %20 = load i32*, i32** %tile_size, align 4, !tbaa !2
  store i32 %add, i32* %20, align 4, !tbaa !25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %tile_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = bitcast i32* %tile_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  %24 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else, %if.then2
  %25 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast i32** %tile_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = load i32, i32* %retval, align 4
  ret i32 %27
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_tile_count(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %tile_count = alloca i32*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %tile_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %tile_count, align 4, !tbaa !2
  %3 = load i32*, i32** %tile_count, align 4, !tbaa !2
  %tobool = icmp ne i32* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %5, i32 0, i32 20
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %6, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.AVxWorker* %7, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %12, i32 0, i32 0
  %13 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %tile_count_minus_1 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %13, i32 0, i32 40
  %14 = load i32, i32* %tile_count_minus_1, align 4, !tbaa !150
  %add = add nsw i32 %14, 1
  %15 = load i32*, i32** %tile_count, align 4, !tbaa !2
  store i32 %add, i32* %15, align 4, !tbaa !25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %16 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then2
  %17 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  br label %cleanup3

if.end:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup3

cleanup3:                                         ; preds = %if.end, %cleanup
  %18 = bitcast i32** %tile_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_render_size(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %render_size = alloca i32*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %render_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %render_size, align 4, !tbaa !2
  %3 = load i32*, i32** %render_size, align 4, !tbaa !2
  %tobool = icmp ne i32* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool1 = icmp ne %struct.AVxWorker* %5, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %6 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker3 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %7, i32 0, i32 20
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker3, align 8, !tbaa !31
  store %struct.AVxWorker* %8, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %9 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 4
  %11 = load i8*, i8** %data1, align 4, !tbaa !32
  %12 = bitcast i8* %11 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %12, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %13 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %14, i32 0, i32 0
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %15, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %render_width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %16, i32 0, i32 4
  %17 = load i32, i32* %render_width, align 8, !tbaa !151
  %18 = load i32*, i32** %render_size, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %18, i32 0
  store i32 %17, i32* %arrayidx, align 4, !tbaa !25
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %render_height = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %19, i32 0, i32 5
  %20 = load i32, i32* %render_height, align 4, !tbaa !152
  %21 = load i32*, i32** %render_size, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %21, i32 1
  store i32 %20, i32* %arrayidx4, align 4, !tbaa !25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %22 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  %24 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else, %if.then2
  %25 = bitcast i32** %render_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_frame_size(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %frame_size = alloca i32*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast i32** %frame_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to i32**
  %2 = load i32*, i32** %1, align 4
  store i32* %2, i32** %frame_size, align 4, !tbaa !2
  %3 = load i32*, i32** %frame_size, align 4, !tbaa !2
  %tobool = icmp ne i32* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool1 = icmp ne %struct.AVxWorker* %5, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %6 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker3 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %7, i32 0, i32 20
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker3, align 8, !tbaa !31
  store %struct.AVxWorker* %8, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %9 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 4
  %11 = load i8*, i8** %data1, align 4, !tbaa !32
  %12 = bitcast i8* %11 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %12, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %13 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %14, i32 0, i32 0
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %15, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %16 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %16, i32 0, i32 2
  %17 = load i32, i32* %width, align 16, !tbaa !153
  %18 = load i32*, i32** %frame_size, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %18, i32 0
  store i32 %17, i32* %arrayidx, align 4, !tbaa !25
  %19 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %19, i32 0, i32 3
  %20 = load i32, i32* %height, align 4, !tbaa !154
  %21 = load i32*, i32** %frame_size, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %21, i32 1
  store i32 %20, i32* %arrayidx4, align 4, !tbaa !25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %22 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  %24 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else, %if.then2
  %25 = bitcast i32** %frame_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_accounting(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %pbi = alloca %struct.AV1Decoder*, align 4
  %acct = alloca %struct.Accounting**, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %0, i32 0, i32 20
  %1 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool = icmp ne %struct.AVxWorker* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker1 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %3, i32 0, i32 20
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker1, align 8, !tbaa !31
  store %struct.AVxWorker* %4, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %5 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %6, i32 0, i32 4
  %7 = load i8*, i8** %data1, align 4, !tbaa !32
  %8 = bitcast i8* %7 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %8, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %9 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi2 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %10, i32 0, i32 0
  %11 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi2, align 4, !tbaa !36
  store %struct.AV1Decoder* %11, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %12 = bitcast %struct.Accounting*** %acct to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %13 = bitcast i8* %argp.cur to %struct.Accounting***
  %14 = load %struct.Accounting**, %struct.Accounting*** %13, align 4
  store %struct.Accounting** %14, %struct.Accounting*** %acct, align 4, !tbaa !2
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %accounting = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %15, i32 0, i32 28
  %16 = load %struct.Accounting**, %struct.Accounting*** %acct, align 4, !tbaa !2
  store %struct.Accounting* %accounting, %struct.Accounting** %16, align 4, !tbaa !2
  store i32 0, i32* %retval, align 4
  %17 = bitcast %struct.Accounting*** %acct to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  br label %return

if.end:                                           ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_new_frame_image(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %new_img = alloca %struct.aom_image*, align 4
  %new_frame = alloca %struct.yv12_buffer_config, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_image** %new_img to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to %struct.aom_image**
  %2 = load %struct.aom_image*, %struct.aom_image** %1, align 4
  store %struct.aom_image* %2, %struct.aom_image** %new_img, align 4, !tbaa !2
  %3 = load %struct.aom_image*, %struct.aom_image** %new_img, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_image* %3, null
  br i1 %tobool, label %if.then, label %if.else4

if.then:                                          ; preds = %entry
  %4 = bitcast %struct.yv12_buffer_config* %new_frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 148, i8* %4) #5
  %5 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %6, i32 0, i32 20
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %7, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %12, i32 0, i32 0
  %13 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %call = call i32 @av1_get_frame_to_show(%struct.AV1Decoder* %13, %struct.yv12_buffer_config* %new_frame)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.then
  %14 = load %struct.aom_image*, %struct.aom_image** %new_img, align 4, !tbaa !2
  call void @yuvconfig2image(%struct.aom_image* %14, %struct.yv12_buffer_config* %new_frame, i8* null)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then1
  %15 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #5
  %16 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #5
  %17 = bitcast %struct.yv12_buffer_config* %new_frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 148, i8* %17) #5
  br label %cleanup5

if.else4:                                         ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup5

cleanup5:                                         ; preds = %if.else4, %cleanup
  %18 = bitcast %struct.aom_image** %new_img to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: nounwind
define internal i32 @ctrl_copy_new_frame_image(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %img = alloca %struct.aom_image*, align 4
  %new_frame = alloca %struct.yv12_buffer_config, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %sd = alloca %struct.yv12_buffer_config, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_image** %img to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to %struct.aom_image**
  %2 = load %struct.aom_image*, %struct.aom_image** %1, align 4
  store %struct.aom_image* %2, %struct.aom_image** %img, align 4, !tbaa !2
  %3 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_image* %3, null
  br i1 %tobool, label %if.then, label %if.else7

if.then:                                          ; preds = %entry
  %4 = bitcast %struct.yv12_buffer_config* %new_frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 148, i8* %4) #5
  %5 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %6, i32 0, i32 20
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %7, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %12, i32 0, i32 0
  %13 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %call = call i32 @av1_get_frame_to_show(%struct.AV1Decoder* %13, %struct.yv12_buffer_config* %new_frame)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.then
  %14 = bitcast %struct.yv12_buffer_config* %sd to i8*
  call void @llvm.lifetime.start.p0i8(i64 148, i8* %14) #5
  %15 = load %struct.aom_image*, %struct.aom_image** %img, align 4, !tbaa !2
  %call2 = call i32 @image2yuvconfig(%struct.aom_image* %15, %struct.yv12_buffer_config* %sd)
  %16 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi3 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %16, i32 0, i32 0
  %17 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi3, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %17, i32 0, i32 1
  %call4 = call i32 @av1_copy_new_frame_dec(%struct.AV1Common* %common, %struct.yv12_buffer_config* %new_frame, %struct.yv12_buffer_config* %sd)
  store i32 %call4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %18 = bitcast %struct.yv12_buffer_config* %sd to i8*
  call void @llvm.lifetime.end.p0i8(i64 148, i8* %18) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %if.then1
  %19 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = bitcast %struct.yv12_buffer_config* %new_frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 148, i8* %21) #5
  br label %cleanup8

if.else7:                                         ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup8

cleanup8:                                         ; preds = %if.else7, %cleanup
  %22 = bitcast %struct.aom_image** %img to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_reference(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %data = alloca %struct.av1_ref_frame*, align 4
  %fb = alloca %struct.yv12_buffer_config*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast %struct.av1_ref_frame** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to %struct.av1_ref_frame**
  %2 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %1, align 4
  store %struct.av1_ref_frame* %2, %struct.av1_ref_frame** %data, align 4, !tbaa !2
  %3 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %data, align 4, !tbaa !2
  %tobool = icmp ne %struct.av1_ref_frame* %3, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = bitcast %struct.yv12_buffer_config** %fb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %6, i32 0, i32 20
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %7, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %8 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %9, i32 0, i32 4
  %10 = load i8*, i8** %data1, align 4, !tbaa !32
  %11 = bitcast i8* %10 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %11, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %12 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %12, i32 0, i32 0
  %13 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %13, i32 0, i32 1
  %14 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %data, align 4, !tbaa !2
  %idx = getelementptr inbounds %struct.av1_ref_frame, %struct.av1_ref_frame* %14, i32 0, i32 0
  %15 = load i32, i32* %idx, align 4, !tbaa !126
  %call = call %struct.yv12_buffer_config* @get_ref_frame(%struct.AV1Common* %common, i32 %15)
  store %struct.yv12_buffer_config* %call, %struct.yv12_buffer_config** %fb, align 4, !tbaa !2
  %16 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %fb, align 4, !tbaa !2
  %cmp = icmp eq %struct.yv12_buffer_config* %16, null
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %17 = load %struct.av1_ref_frame*, %struct.av1_ref_frame** %data, align 4, !tbaa !2
  %img = getelementptr inbounds %struct.av1_ref_frame, %struct.av1_ref_frame* %17, i32 0, i32 2
  %18 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %fb, align 4, !tbaa !2
  call void @yuvconfig2image(%struct.aom_image* %img, %struct.yv12_buffer_config* %18, i8* null)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then1
  %19 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  %21 = bitcast %struct.yv12_buffer_config** %fb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  br label %cleanup4

if.else:                                          ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup4

cleanup4:                                         ; preds = %if.else, %cleanup
  %22 = bitcast %struct.av1_ref_frame** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_frame_header_info(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %frame_header_info = alloca %struct.aom_tile_data*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %pbi = alloca %struct.AV1Decoder*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_tile_data** %frame_header_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to %struct.aom_tile_data**
  %2 = load %struct.aom_tile_data*, %struct.aom_tile_data** %1, align 4
  store %struct.aom_tile_data* %2, %struct.aom_tile_data** %frame_header_info, align 4, !tbaa !2
  %3 = load %struct.aom_tile_data*, %struct.aom_tile_data** %frame_header_info, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_tile_data* %3, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool1 = icmp ne %struct.AVxWorker* %5, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %6 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker3 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %7, i32 0, i32 20
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker3, align 8, !tbaa !31
  store %struct.AVxWorker* %8, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %9 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 4
  %11 = load i8*, i8** %data1, align 4, !tbaa !32
  %12 = bitcast i8* %11 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %12, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %13 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi4 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %14, i32 0, i32 0
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi4, align 4, !tbaa !36
  store %struct.AV1Decoder* %15, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %16 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %obu_size_hdr = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %16, i32 0, i32 37
  %size = getelementptr inbounds %struct.DataBuffer, %struct.DataBuffer* %obu_size_hdr, i32 0, i32 1
  %17 = load i32, i32* %size, align 4, !tbaa !155
  %18 = load %struct.aom_tile_data*, %struct.aom_tile_data** %frame_header_info, align 4, !tbaa !2
  %coded_tile_data_size = getelementptr inbounds %struct.aom_tile_data, %struct.aom_tile_data* %18, i32 0, i32 0
  store i32 %17, i32* %coded_tile_data_size, align 4, !tbaa !156
  %19 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %obu_size_hdr5 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %19, i32 0, i32 37
  %data = getelementptr inbounds %struct.DataBuffer, %struct.DataBuffer* %obu_size_hdr5, i32 0, i32 0
  %20 = load i8*, i8** %data, align 4, !tbaa !158
  %21 = load %struct.aom_tile_data*, %struct.aom_tile_data** %frame_header_info, align 4, !tbaa !2
  %coded_tile_data = getelementptr inbounds %struct.aom_tile_data, %struct.aom_tile_data* %21, i32 0, i32 1
  store i8* %20, i8** %coded_tile_data, align 4, !tbaa !159
  %22 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %frame_header_size = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %22, i32 0, i32 36
  %23 = load i32, i32* %frame_header_size, align 8, !tbaa !160
  %24 = load %struct.aom_tile_data*, %struct.aom_tile_data** %frame_header_info, align 4, !tbaa !2
  %extra_size = getelementptr inbounds %struct.aom_tile_data, %struct.aom_tile_data* %24, i32 0, i32 2
  store i32 %23, i32* %extra_size, align 4, !tbaa !161
  %25 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #5
  %26 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  br label %if.end

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then2
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end6, %if.else
  %28 = bitcast %struct.aom_tile_data** %frame_header_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = load i32, i32* %retval, align 4
  ret i32 %29
}

; Function Attrs: nounwind
define internal i32 @ctrl_get_tile_data(%struct.aom_codec_alg_priv* %ctx, i8* %args) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %args.addr = alloca i8*, align 4
  %tile_data = alloca %struct.aom_tile_data*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %pbi = alloca %struct.AV1Decoder*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %args, i8** %args.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_tile_data** %tile_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %argp.cur = load i8*, i8** %args.addr, align 4
  %argp.next = getelementptr inbounds i8, i8* %argp.cur, i32 4
  store i8* %argp.next, i8** %args.addr, align 4
  %1 = bitcast i8* %argp.cur to %struct.aom_tile_data**
  %2 = load %struct.aom_tile_data*, %struct.aom_tile_data** %1, align 4
  store %struct.aom_tile_data* %2, %struct.aom_tile_data** %tile_data, align 4, !tbaa !2
  %3 = load %struct.aom_tile_data*, %struct.aom_tile_data** %tile_data, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_tile_data* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 20
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %tobool1 = icmp ne %struct.AVxWorker* %5, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %6 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker3 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %7, i32 0, i32 20
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker3, align 8, !tbaa !31
  store %struct.AVxWorker* %8, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %9 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %10, i32 0, i32 4
  %11 = load i8*, i8** %data1, align 4, !tbaa !32
  %12 = bitcast i8* %11 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %12, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %13 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi4 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %14, i32 0, i32 0
  %15 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi4, align 4, !tbaa !36
  store %struct.AV1Decoder* %15, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %16 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %tile_buffers = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %16, i32 0, i32 12
  %17 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_row = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %17, i32 0, i32 25
  %18 = load i32, i32* %dec_tile_row, align 4, !tbaa !100
  %arrayidx = getelementptr inbounds [64 x [64 x %struct.TileBufferDec]], [64 x [64 x %struct.TileBufferDec]]* %tile_buffers, i32 0, i32 %18
  %19 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_col = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %19, i32 0, i32 26
  %20 = load i32, i32* %dec_tile_col, align 8, !tbaa !105
  %arrayidx5 = getelementptr inbounds [64 x %struct.TileBufferDec], [64 x %struct.TileBufferDec]* %arrayidx, i32 0, i32 %20
  %size = getelementptr inbounds %struct.TileBufferDec, %struct.TileBufferDec* %arrayidx5, i32 0, i32 1
  %21 = load i32, i32* %size, align 4, !tbaa !162
  %22 = load %struct.aom_tile_data*, %struct.aom_tile_data** %tile_data, align 4, !tbaa !2
  %coded_tile_data_size = getelementptr inbounds %struct.aom_tile_data, %struct.aom_tile_data* %22, i32 0, i32 0
  store i32 %21, i32* %coded_tile_data_size, align 4, !tbaa !156
  %23 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %tile_buffers6 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %23, i32 0, i32 12
  %24 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_row7 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %24, i32 0, i32 25
  %25 = load i32, i32* %dec_tile_row7, align 4, !tbaa !100
  %arrayidx8 = getelementptr inbounds [64 x [64 x %struct.TileBufferDec]], [64 x [64 x %struct.TileBufferDec]]* %tile_buffers6, i32 0, i32 %25
  %26 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %dec_tile_col9 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %26, i32 0, i32 26
  %27 = load i32, i32* %dec_tile_col9, align 8, !tbaa !105
  %arrayidx10 = getelementptr inbounds [64 x %struct.TileBufferDec], [64 x %struct.TileBufferDec]* %arrayidx8, i32 0, i32 %27
  %data = getelementptr inbounds %struct.TileBufferDec, %struct.TileBufferDec* %arrayidx10, i32 0, i32 0
  %28 = load i8*, i8** %data, align 8, !tbaa !164
  %29 = load %struct.aom_tile_data*, %struct.aom_tile_data** %tile_data, align 4, !tbaa !2
  %coded_tile_data = getelementptr inbounds %struct.aom_tile_data, %struct.aom_tile_data* %29, i32 0, i32 1
  store i8* %28, i8** %coded_tile_data, align 4, !tbaa !159
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %30 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #5
  %31 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  %32 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #5
  br label %cleanup

if.else:                                          ; preds = %if.then
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.else, %if.then2
  %33 = bitcast %struct.aom_tile_data** %tile_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  %34 = load i32, i32* %retval, align 4
  ret i32 %34
}

; Function Attrs: nounwind
define internal i32 @image2yuvconfig(%struct.aom_image* %img, %struct.yv12_buffer_config* %yv12) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  %yv12.addr = alloca %struct.yv12_buffer_config*, align 4
  %border = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %yv12, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes = getelementptr inbounds %struct.aom_image, %struct.aom_image* %0, i32 0, i32 16
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes, i32 0, i32 0
  %1 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %2, i32 0, i32 5
  %4 = bitcast %union.anon.12* %3 to %struct.anon.13*
  %y_buffer = getelementptr inbounds %struct.anon.13, %struct.anon.13* %4, i32 0, i32 0
  store i8* %1, i8** %y_buffer, align 4, !tbaa !23
  %5 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes1 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %5, i32 0, i32 16
  %arrayidx2 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes1, i32 0, i32 1
  %6 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 5
  %9 = bitcast %union.anon.12* %8 to %struct.anon.13*
  %u_buffer = getelementptr inbounds %struct.anon.13, %struct.anon.13* %9, i32 0, i32 1
  store i8* %6, i8** %u_buffer, align 4, !tbaa !23
  %10 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes3 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %10, i32 0, i32 16
  %arrayidx4 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes3, i32 0, i32 2
  %11 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %13 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %12, i32 0, i32 5
  %14 = bitcast %union.anon.12* %13 to %struct.anon.13*
  %v_buffer = getelementptr inbounds %struct.anon.13, %struct.anon.13* %14, i32 0, i32 2
  store i8* %11, i8** %v_buffer, align 4, !tbaa !23
  %15 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %15, i32 0, i32 10
  %16 = load i32, i32* %d_w, align 4, !tbaa !165
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %18 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 2
  %19 = bitcast %union.anon.6* %18 to %struct.anon.7*
  %y_crop_width = getelementptr inbounds %struct.anon.7, %struct.anon.7* %19, i32 0, i32 0
  store i32 %16, i32* %y_crop_width, align 4, !tbaa !23
  %20 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %20, i32 0, i32 11
  %21 = load i32, i32* %d_h, align 4, !tbaa !166
  %22 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %23 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %22, i32 0, i32 3
  %24 = bitcast %union.anon.8* %23 to %struct.anon.9*
  %y_crop_height = getelementptr inbounds %struct.anon.9, %struct.anon.9* %24, i32 0, i32 0
  store i32 %21, i32* %y_crop_height, align 4, !tbaa !23
  %25 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %r_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %25, i32 0, i32 12
  %26 = load i32, i32* %r_w, align 4, !tbaa !167
  %27 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %render_width = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %27, i32 0, i32 23
  store i32 %26, i32* %render_width, align 4, !tbaa !168
  %28 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %r_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %28, i32 0, i32 13
  %29 = load i32, i32* %r_h, align 4, !tbaa !169
  %30 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %render_height = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %30, i32 0, i32 24
  store i32 %29, i32* %render_height, align 4, !tbaa !170
  %31 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %31, i32 0, i32 7
  %32 = load i32, i32* %w, align 4, !tbaa !171
  %33 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %34 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %33, i32 0, i32 0
  %35 = bitcast %union.anon.2* %34 to %struct.anon.3*
  %y_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %35, i32 0, i32 0
  store i32 %32, i32* %y_width, align 4, !tbaa !23
  %36 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %36, i32 0, i32 8
  %37 = load i32, i32* %h, align 4, !tbaa !172
  %38 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %39 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %38, i32 0, i32 1
  %40 = bitcast %union.anon.4* %39 to %struct.anon.5*
  %y_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %40, i32 0, i32 0
  store i32 %37, i32* %y_height, align 4, !tbaa !23
  %41 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %x_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %41, i32 0, i32 14
  %42 = load i32, i32* %x_chroma_shift, align 4, !tbaa !173
  %cmp = icmp eq i32 %42, 1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %43 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %44 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %43, i32 0, i32 0
  %45 = bitcast %union.anon.2* %44 to %struct.anon.3*
  %y_width5 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %45, i32 0, i32 0
  %46 = load i32, i32* %y_width5, align 4, !tbaa !23
  %add = add nsw i32 1, %46
  %div = sdiv i32 %add, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %47 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %48 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %47, i32 0, i32 0
  %49 = bitcast %union.anon.2* %48 to %struct.anon.3*
  %y_width6 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %49, i32 0, i32 0
  %50 = load i32, i32* %y_width6, align 4, !tbaa !23
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %div, %cond.true ], [ %50, %cond.false ]
  %51 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %52 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %51, i32 0, i32 0
  %53 = bitcast %union.anon.2* %52 to %struct.anon.3*
  %uv_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %53, i32 0, i32 1
  store i32 %cond, i32* %uv_width, align 4, !tbaa !23
  %54 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %54, i32 0, i32 15
  %55 = load i32, i32* %y_chroma_shift, align 4, !tbaa !174
  %cmp7 = icmp eq i32 %55, 1
  br i1 %cmp7, label %cond.true8, label %cond.false12

cond.true8:                                       ; preds = %cond.end
  %56 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %57 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %56, i32 0, i32 1
  %58 = bitcast %union.anon.4* %57 to %struct.anon.5*
  %y_height9 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %58, i32 0, i32 0
  %59 = load i32, i32* %y_height9, align 4, !tbaa !23
  %add10 = add nsw i32 1, %59
  %div11 = sdiv i32 %add10, 2
  br label %cond.end14

cond.false12:                                     ; preds = %cond.end
  %60 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %61 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %60, i32 0, i32 1
  %62 = bitcast %union.anon.4* %61 to %struct.anon.5*
  %y_height13 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %62, i32 0, i32 0
  %63 = load i32, i32* %y_height13, align 4, !tbaa !23
  br label %cond.end14

cond.end14:                                       ; preds = %cond.false12, %cond.true8
  %cond15 = phi i32 [ %div11, %cond.true8 ], [ %63, %cond.false12 ]
  %64 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %65 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %64, i32 0, i32 1
  %66 = bitcast %union.anon.4* %65 to %struct.anon.5*
  %uv_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %66, i32 0, i32 1
  store i32 %cond15, i32* %uv_height, align 4, !tbaa !23
  %67 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %68 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %67, i32 0, i32 0
  %69 = bitcast %union.anon.2* %68 to %struct.anon.3*
  %uv_width16 = getelementptr inbounds %struct.anon.3, %struct.anon.3* %69, i32 0, i32 1
  %70 = load i32, i32* %uv_width16, align 4, !tbaa !23
  %71 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %72 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %71, i32 0, i32 2
  %73 = bitcast %union.anon.6* %72 to %struct.anon.7*
  %uv_crop_width = getelementptr inbounds %struct.anon.7, %struct.anon.7* %73, i32 0, i32 1
  store i32 %70, i32* %uv_crop_width, align 4, !tbaa !23
  %74 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %75 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %74, i32 0, i32 1
  %76 = bitcast %union.anon.4* %75 to %struct.anon.5*
  %uv_height17 = getelementptr inbounds %struct.anon.5, %struct.anon.5* %76, i32 0, i32 1
  %77 = load i32, i32* %uv_height17, align 4, !tbaa !23
  %78 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %79 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %78, i32 0, i32 3
  %80 = bitcast %union.anon.8* %79 to %struct.anon.9*
  %uv_crop_height = getelementptr inbounds %struct.anon.9, %struct.anon.9* %80, i32 0, i32 1
  store i32 %77, i32* %uv_crop_height, align 4, !tbaa !23
  %81 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %81, i32 0, i32 17
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 0
  %82 = load i32, i32* %arrayidx18, align 4, !tbaa !25
  %83 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %84 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %83, i32 0, i32 4
  %85 = bitcast %union.anon.10* %84 to %struct.anon.11*
  %y_stride = getelementptr inbounds %struct.anon.11, %struct.anon.11* %85, i32 0, i32 0
  store i32 %82, i32* %y_stride, align 4, !tbaa !23
  %86 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride19 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %86, i32 0, i32 17
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %stride19, i32 0, i32 1
  %87 = load i32, i32* %arrayidx20, align 4, !tbaa !25
  %88 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %89 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %88, i32 0, i32 4
  %90 = bitcast %union.anon.10* %89 to %struct.anon.11*
  %uv_stride = getelementptr inbounds %struct.anon.11, %struct.anon.11* %90, i32 0, i32 1
  store i32 %87, i32* %uv_stride, align 4, !tbaa !23
  %91 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %cp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %91, i32 0, i32 1
  %92 = load i32, i32* %cp, align 4, !tbaa !175
  %93 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %color_primaries = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %93, i32 0, i32 17
  store i32 %92, i32* %color_primaries, align 4, !tbaa !176
  %94 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %94, i32 0, i32 2
  %95 = load i32, i32* %tc, align 4, !tbaa !177
  %96 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %transfer_characteristics = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %96, i32 0, i32 18
  store i32 %95, i32* %transfer_characteristics, align 4, !tbaa !178
  %97 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %mc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %97, i32 0, i32 3
  %98 = load i32, i32* %mc, align 4, !tbaa !179
  %99 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %matrix_coefficients = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %99, i32 0, i32 19
  store i32 %98, i32* %matrix_coefficients, align 4, !tbaa !180
  %100 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %monochrome = getelementptr inbounds %struct.aom_image, %struct.aom_image* %100, i32 0, i32 4
  %101 = load i32, i32* %monochrome, align 4, !tbaa !181
  %conv = trunc i32 %101 to i8
  %102 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %monochrome21 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %102, i32 0, i32 20
  store i8 %conv, i8* %monochrome21, align 4, !tbaa !182
  %103 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %csp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %103, i32 0, i32 5
  %104 = load i32, i32* %csp, align 4, !tbaa !183
  %105 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %chroma_sample_position = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %105, i32 0, i32 21
  store i32 %104, i32* %chroma_sample_position, align 4, !tbaa !184
  %106 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %range = getelementptr inbounds %struct.aom_image, %struct.aom_image* %106, i32 0, i32 6
  %107 = load i32, i32* %range, align 4, !tbaa !185
  %108 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %color_range = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %108, i32 0, i32 22
  store i32 %107, i32* %color_range, align 4, !tbaa !186
  %109 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt = getelementptr inbounds %struct.aom_image, %struct.aom_image* %109, i32 0, i32 0
  %110 = load i32, i32* %fmt, align 4, !tbaa !187
  %and = and i32 %110, 2048
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end14
  %111 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %112 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %111, i32 0, i32 5
  %113 = bitcast %union.anon.12* %112 to %struct.anon.13*
  %y_buffer22 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %113, i32 0, i32 0
  %114 = load i8*, i8** %y_buffer22, align 4, !tbaa !23
  %115 = ptrtoint i8* %114 to i32
  %shr = lshr i32 %115, 1
  %116 = inttoptr i32 %shr to i8*
  %117 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %118 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %117, i32 0, i32 5
  %119 = bitcast %union.anon.12* %118 to %struct.anon.13*
  %y_buffer23 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %119, i32 0, i32 0
  store i8* %116, i8** %y_buffer23, align 4, !tbaa !23
  %120 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %121 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %120, i32 0, i32 5
  %122 = bitcast %union.anon.12* %121 to %struct.anon.13*
  %u_buffer24 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %122, i32 0, i32 1
  %123 = load i8*, i8** %u_buffer24, align 4, !tbaa !23
  %124 = ptrtoint i8* %123 to i32
  %shr25 = lshr i32 %124, 1
  %125 = inttoptr i32 %shr25 to i8*
  %126 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %127 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %126, i32 0, i32 5
  %128 = bitcast %union.anon.12* %127 to %struct.anon.13*
  %u_buffer26 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %128, i32 0, i32 1
  store i8* %125, i8** %u_buffer26, align 4, !tbaa !23
  %129 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %130 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %129, i32 0, i32 5
  %131 = bitcast %union.anon.12* %130 to %struct.anon.13*
  %v_buffer27 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %131, i32 0, i32 2
  %132 = load i8*, i8** %v_buffer27, align 4, !tbaa !23
  %133 = ptrtoint i8* %132 to i32
  %shr28 = lshr i32 %133, 1
  %134 = inttoptr i32 %shr28 to i8*
  %135 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %136 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %135, i32 0, i32 5
  %137 = bitcast %union.anon.12* %136 to %struct.anon.13*
  %v_buffer29 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %137, i32 0, i32 2
  store i8* %134, i8** %v_buffer29, align 4, !tbaa !23
  %138 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %139 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %138, i32 0, i32 4
  %140 = bitcast %union.anon.10* %139 to %struct.anon.11*
  %y_stride30 = getelementptr inbounds %struct.anon.11, %struct.anon.11* %140, i32 0, i32 0
  %141 = load i32, i32* %y_stride30, align 4, !tbaa !23
  %shr31 = ashr i32 %141, 1
  store i32 %shr31, i32* %y_stride30, align 4, !tbaa !23
  %142 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %143 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %142, i32 0, i32 4
  %144 = bitcast %union.anon.10* %143 to %struct.anon.11*
  %uv_stride32 = getelementptr inbounds %struct.anon.11, %struct.anon.11* %144, i32 0, i32 1
  %145 = load i32, i32* %uv_stride32, align 4, !tbaa !23
  %shr33 = ashr i32 %145, 1
  store i32 %shr33, i32* %uv_stride32, align 4, !tbaa !23
  %146 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %146, i32 0, i32 26
  store i32 8, i32* %flags, align 4, !tbaa !188
  br label %if.end

if.else:                                          ; preds = %cond.end14
  %147 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %flags34 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %147, i32 0, i32 26
  store i32 0, i32* %flags34, align 4, !tbaa !188
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %148 = bitcast i32* %border to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #5
  %149 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %150 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %149, i32 0, i32 4
  %151 = bitcast %union.anon.10* %150 to %struct.anon.11*
  %y_stride35 = getelementptr inbounds %struct.anon.11, %struct.anon.11* %151, i32 0, i32 0
  %152 = load i32, i32* %y_stride35, align 4, !tbaa !23
  %153 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %w36 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %153, i32 0, i32 7
  %154 = load i32, i32* %w36, align 4, !tbaa !171
  %add37 = add i32 %154, 31
  %and38 = and i32 %add37, -32
  %sub = sub nsw i32 %152, %and38
  %div39 = sdiv i32 %sub, 2
  store i32 %div39, i32* %border, align 4, !tbaa !25
  %155 = load i32, i32* %border, align 4, !tbaa !25
  %cmp40 = icmp slt i32 %155, 0
  br i1 %cmp40, label %cond.true42, label %cond.false43

cond.true42:                                      ; preds = %if.end
  br label %cond.end44

cond.false43:                                     ; preds = %if.end
  %156 = load i32, i32* %border, align 4, !tbaa !25
  br label %cond.end44

cond.end44:                                       ; preds = %cond.false43, %cond.true42
  %cond45 = phi i32 [ 0, %cond.true42 ], [ %156, %cond.false43 ]
  %157 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %border46 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %157, i32 0, i32 12
  store i32 %cond45, i32* %border46, align 4, !tbaa !189
  %158 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %x_chroma_shift47 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %158, i32 0, i32 14
  %159 = load i32, i32* %x_chroma_shift47, align 4, !tbaa !173
  %160 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %160, i32 0, i32 14
  store i32 %159, i32* %subsampling_x, align 4, !tbaa !190
  %161 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift48 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %161, i32 0, i32 15
  %162 = load i32, i32* %y_chroma_shift48, align 4, !tbaa !174
  %163 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %163, i32 0, i32 15
  store i32 %162, i32* %subsampling_y, align 4, !tbaa !191
  %164 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.aom_image, %struct.aom_image* %164, i32 0, i32 26
  %165 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata, align 4, !tbaa !192
  %166 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %metadata49 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %166, i32 0, i32 27
  store %struct.aom_metadata_array* %165, %struct.aom_metadata_array** %metadata49, align 4, !tbaa !193
  %167 = bitcast i32* %border to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #5
  ret i32 0
}

declare i32 @av1_copy_reference_dec(%struct.AV1Decoder*, i32, %struct.yv12_buffer_config*) #2

declare i32 @av1_set_reference_dec(%struct.AV1Common*, i32, i32, %struct.yv12_buffer_config*) #2

; Function Attrs: nounwind
define internal i32 @get_img_format(i32 %subsampling_x, i32 %subsampling_y, i32 %use_highbitdepth) #0 {
entry:
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  %use_highbitdepth.addr = alloca i32, align 4
  %fmt = alloca i32, align 4
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !25
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !25
  store i32 %use_highbitdepth, i32* %use_highbitdepth.addr, align 4, !tbaa !25
  %0 = bitcast i32* %fmt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %fmt, align 4, !tbaa !23
  %1 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !25
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %2 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !25
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  store i32 262, i32* %fmt, align 4, !tbaa !23
  br label %if.end12

if.else:                                          ; preds = %land.lhs.true, %entry
  %3 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !25
  %cmp2 = icmp eq i32 %3, 1
  br i1 %cmp2, label %land.lhs.true3, label %if.else6

land.lhs.true3:                                   ; preds = %if.else
  %4 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !25
  %cmp4 = icmp eq i32 %4, 0
  br i1 %cmp4, label %if.then5, label %if.else6

if.then5:                                         ; preds = %land.lhs.true3
  store i32 261, i32* %fmt, align 4, !tbaa !23
  br label %if.end11

if.else6:                                         ; preds = %land.lhs.true3, %if.else
  %5 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !25
  %cmp7 = icmp eq i32 %5, 1
  br i1 %cmp7, label %land.lhs.true8, label %if.end

land.lhs.true8:                                   ; preds = %if.else6
  %6 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !25
  %cmp9 = icmp eq i32 %6, 1
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %land.lhs.true8
  store i32 258, i32* %fmt, align 4, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %if.then10, %land.lhs.true8, %if.else6
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then5
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.then
  %7 = load i32, i32* %use_highbitdepth.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.end12
  %8 = load i32, i32* %fmt, align 4, !tbaa !23
  %or = or i32 %8, 2048
  store i32 %or, i32* %fmt, align 4, !tbaa !23
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.end12
  %9 = load i32, i32* %fmt, align 4, !tbaa !23
  %10 = bitcast i32* %fmt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #5
  ret i32 %9
}

declare void @av1_get_uniform_tile_size(%struct.AV1Common*, i32*, i32*) #2

declare i32 @av1_get_frame_to_show(%struct.AV1Decoder*, %struct.yv12_buffer_config*) #2

; Function Attrs: nounwind
define internal void @yuvconfig2image(%struct.aom_image* %img, %struct.yv12_buffer_config* %yv12, i8* %user_priv) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  %yv12.addr = alloca %struct.yv12_buffer_config*, align 4
  %user_priv.addr = alloca i8*, align 4
  %bps = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store %struct.yv12_buffer_config* %yv12, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  store i8* %user_priv, i8** %user_priv.addr, align 4, !tbaa !2
  %0 = bitcast i32* %bps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 15
  %2 = load i32, i32* %subsampling_y, align 4, !tbaa !191
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.else4, label %if.then

if.then:                                          ; preds = %entry
  %3 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %3, i32 0, i32 14
  %4 = load i32, i32* %subsampling_x, align 4, !tbaa !190
  %tobool1 = icmp ne i32 %4, 0
  br i1 %tobool1, label %if.else, label %if.then2

if.then2:                                         ; preds = %if.then
  %5 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt = getelementptr inbounds %struct.aom_image, %struct.aom_image* %5, i32 0, i32 0
  store i32 262, i32* %fmt, align 4, !tbaa !187
  store i32 24, i32* %bps, align 4, !tbaa !25
  br label %if.end

if.else:                                          ; preds = %if.then
  %6 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt3 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %6, i32 0, i32 0
  store i32 261, i32* %fmt3, align 4, !tbaa !187
  store i32 16, i32* %bps, align 4, !tbaa !25
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then2
  br label %if.end6

if.else4:                                         ; preds = %entry
  %7 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt5 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %7, i32 0, i32 0
  store i32 258, i32* %fmt5, align 4, !tbaa !187
  store i32 12, i32* %bps, align 4, !tbaa !25
  br label %if.end6

if.end6:                                          ; preds = %if.else4, %if.end
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %color_primaries = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %8, i32 0, i32 17
  %9 = load i32, i32* %color_primaries, align 4, !tbaa !176
  %10 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %cp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %10, i32 0, i32 1
  store i32 %9, i32* %cp, align 4, !tbaa !175
  %11 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %transfer_characteristics = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %11, i32 0, i32 18
  %12 = load i32, i32* %transfer_characteristics, align 4, !tbaa !178
  %13 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %13, i32 0, i32 2
  store i32 %12, i32* %tc, align 4, !tbaa !177
  %14 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %matrix_coefficients = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %14, i32 0, i32 19
  %15 = load i32, i32* %matrix_coefficients, align 4, !tbaa !180
  %16 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %mc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %16, i32 0, i32 3
  store i32 %15, i32* %mc, align 4, !tbaa !179
  %17 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %monochrome = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %17, i32 0, i32 20
  %18 = load i8, i8* %monochrome, align 4, !tbaa !182
  %conv = zext i8 %18 to i32
  %19 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %monochrome7 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %19, i32 0, i32 4
  store i32 %conv, i32* %monochrome7, align 4, !tbaa !181
  %20 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %chroma_sample_position = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %20, i32 0, i32 21
  %21 = load i32, i32* %chroma_sample_position, align 4, !tbaa !184
  %22 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %csp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %22, i32 0, i32 5
  store i32 %21, i32* %csp, align 4, !tbaa !183
  %23 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %color_range = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %23, i32 0, i32 22
  %24 = load i32, i32* %color_range, align 4, !tbaa !186
  %25 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %range = getelementptr inbounds %struct.aom_image, %struct.aom_image* %25, i32 0, i32 6
  store i32 %24, i32* %range, align 4, !tbaa !185
  %26 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %bit_depth = getelementptr inbounds %struct.aom_image, %struct.aom_image* %26, i32 0, i32 9
  store i32 8, i32* %bit_depth, align 4, !tbaa !194
  %27 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %28 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %27, i32 0, i32 0
  %29 = bitcast %union.anon.2* %28 to %struct.anon.3*
  %y_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %29, i32 0, i32 0
  %30 = load i32, i32* %y_width, align 4, !tbaa !23
  %31 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %31, i32 0, i32 7
  store i32 %30, i32* %w, align 4, !tbaa !171
  %32 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %33 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %32, i32 0, i32 1
  %34 = bitcast %union.anon.4* %33 to %struct.anon.5*
  %y_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %34, i32 0, i32 0
  %35 = load i32, i32* %y_height, align 4, !tbaa !23
  %36 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %36, i32 0, i32 8
  store i32 %35, i32* %h, align 4, !tbaa !172
  %37 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %38 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %37, i32 0, i32 2
  %39 = bitcast %union.anon.6* %38 to %struct.anon.7*
  %y_crop_width = getelementptr inbounds %struct.anon.7, %struct.anon.7* %39, i32 0, i32 0
  %40 = load i32, i32* %y_crop_width, align 4, !tbaa !23
  %41 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %41, i32 0, i32 10
  store i32 %40, i32* %d_w, align 4, !tbaa !165
  %42 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %43 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %42, i32 0, i32 3
  %44 = bitcast %union.anon.8* %43 to %struct.anon.9*
  %y_crop_height = getelementptr inbounds %struct.anon.9, %struct.anon.9* %44, i32 0, i32 0
  %45 = load i32, i32* %y_crop_height, align 4, !tbaa !23
  %46 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %46, i32 0, i32 11
  store i32 %45, i32* %d_h, align 4, !tbaa !166
  %47 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %render_width = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %47, i32 0, i32 23
  %48 = load i32, i32* %render_width, align 4, !tbaa !168
  %49 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %r_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %49, i32 0, i32 12
  store i32 %48, i32* %r_w, align 4, !tbaa !167
  %50 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %render_height = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %50, i32 0, i32 24
  %51 = load i32, i32* %render_height, align 4, !tbaa !170
  %52 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %r_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %52, i32 0, i32 13
  store i32 %51, i32* %r_h, align 4, !tbaa !169
  %53 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %subsampling_x8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %53, i32 0, i32 14
  %54 = load i32, i32* %subsampling_x8, align 4, !tbaa !190
  %55 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %x_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %55, i32 0, i32 14
  store i32 %54, i32* %x_chroma_shift, align 4, !tbaa !173
  %56 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %subsampling_y9 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %56, i32 0, i32 15
  %57 = load i32, i32* %subsampling_y9, align 4, !tbaa !191
  %58 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %58, i32 0, i32 15
  store i32 %57, i32* %y_chroma_shift, align 4, !tbaa !174
  %59 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %60 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %59, i32 0, i32 5
  %61 = bitcast %union.anon.12* %60 to %struct.anon.13*
  %y_buffer = getelementptr inbounds %struct.anon.13, %struct.anon.13* %61, i32 0, i32 0
  %62 = load i8*, i8** %y_buffer, align 4, !tbaa !23
  %63 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes = getelementptr inbounds %struct.aom_image, %struct.aom_image* %63, i32 0, i32 16
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes, i32 0, i32 0
  store i8* %62, i8** %arrayidx, align 4, !tbaa !2
  %64 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %65 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %64, i32 0, i32 5
  %66 = bitcast %union.anon.12* %65 to %struct.anon.13*
  %u_buffer = getelementptr inbounds %struct.anon.13, %struct.anon.13* %66, i32 0, i32 1
  %67 = load i8*, i8** %u_buffer, align 4, !tbaa !23
  %68 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes10 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %68, i32 0, i32 16
  %arrayidx11 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes10, i32 0, i32 1
  store i8* %67, i8** %arrayidx11, align 4, !tbaa !2
  %69 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %70 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %69, i32 0, i32 5
  %71 = bitcast %union.anon.12* %70 to %struct.anon.13*
  %v_buffer = getelementptr inbounds %struct.anon.13, %struct.anon.13* %71, i32 0, i32 2
  %72 = load i8*, i8** %v_buffer, align 4, !tbaa !23
  %73 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes12 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %73, i32 0, i32 16
  %arrayidx13 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes12, i32 0, i32 2
  store i8* %72, i8** %arrayidx13, align 4, !tbaa !2
  %74 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %75 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %74, i32 0, i32 4
  %76 = bitcast %union.anon.10* %75 to %struct.anon.11*
  %y_stride = getelementptr inbounds %struct.anon.11, %struct.anon.11* %76, i32 0, i32 0
  %77 = load i32, i32* %y_stride, align 4, !tbaa !23
  %78 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %78, i32 0, i32 17
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 0
  store i32 %77, i32* %arrayidx14, align 4, !tbaa !25
  %79 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %80 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %79, i32 0, i32 4
  %81 = bitcast %union.anon.10* %80 to %struct.anon.11*
  %uv_stride = getelementptr inbounds %struct.anon.11, %struct.anon.11* %81, i32 0, i32 1
  %82 = load i32, i32* %uv_stride, align 4, !tbaa !23
  %83 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride15 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %83, i32 0, i32 17
  %arrayidx16 = getelementptr inbounds [3 x i32], [3 x i32]* %stride15, i32 0, i32 1
  store i32 %82, i32* %arrayidx16, align 4, !tbaa !25
  %84 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %85 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %84, i32 0, i32 4
  %86 = bitcast %union.anon.10* %85 to %struct.anon.11*
  %uv_stride17 = getelementptr inbounds %struct.anon.11, %struct.anon.11* %86, i32 0, i32 1
  %87 = load i32, i32* %uv_stride17, align 4, !tbaa !23
  %88 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride18 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %88, i32 0, i32 17
  %arrayidx19 = getelementptr inbounds [3 x i32], [3 x i32]* %stride18, i32 0, i32 2
  store i32 %87, i32* %arrayidx19, align 4, !tbaa !25
  %89 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %89, i32 0, i32 26
  %90 = load i32, i32* %flags, align 4, !tbaa !188
  %and = and i32 %90, 8
  %tobool20 = icmp ne i32 %and, 0
  br i1 %tobool20, label %if.then21, label %if.end49

if.then21:                                        ; preds = %if.end6
  %91 = load i32, i32* %bps, align 4, !tbaa !25
  %mul = mul nsw i32 %91, 2
  store i32 %mul, i32* %bps, align 4, !tbaa !25
  %92 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt22 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %92, i32 0, i32 0
  %93 = load i32, i32* %fmt22, align 4, !tbaa !187
  %or = or i32 %93, 2048
  %94 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt23 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %94, i32 0, i32 0
  store i32 %or, i32* %fmt23, align 4, !tbaa !187
  %95 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %bit_depth24 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %95, i32 0, i32 16
  %96 = load i32, i32* %bit_depth24, align 4, !tbaa !195
  %97 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %bit_depth25 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %97, i32 0, i32 9
  store i32 %96, i32* %bit_depth25, align 4, !tbaa !194
  %98 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %99 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %98, i32 0, i32 5
  %100 = bitcast %union.anon.12* %99 to %struct.anon.13*
  %y_buffer26 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %100, i32 0, i32 0
  %101 = load i8*, i8** %y_buffer26, align 4, !tbaa !23
  %102 = ptrtoint i8* %101 to i32
  %shl = shl i32 %102, 1
  %103 = inttoptr i32 %shl to i16*
  %104 = bitcast i16* %103 to i8*
  %105 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes27 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %105, i32 0, i32 16
  %arrayidx28 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes27, i32 0, i32 0
  store i8* %104, i8** %arrayidx28, align 4, !tbaa !2
  %106 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %107 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %106, i32 0, i32 5
  %108 = bitcast %union.anon.12* %107 to %struct.anon.13*
  %u_buffer29 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %108, i32 0, i32 1
  %109 = load i8*, i8** %u_buffer29, align 4, !tbaa !23
  %110 = ptrtoint i8* %109 to i32
  %shl30 = shl i32 %110, 1
  %111 = inttoptr i32 %shl30 to i16*
  %112 = bitcast i16* %111 to i8*
  %113 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes31 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %113, i32 0, i32 16
  %arrayidx32 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes31, i32 0, i32 1
  store i8* %112, i8** %arrayidx32, align 4, !tbaa !2
  %114 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %115 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %114, i32 0, i32 5
  %116 = bitcast %union.anon.12* %115 to %struct.anon.13*
  %v_buffer33 = getelementptr inbounds %struct.anon.13, %struct.anon.13* %116, i32 0, i32 2
  %117 = load i8*, i8** %v_buffer33, align 4, !tbaa !23
  %118 = ptrtoint i8* %117 to i32
  %shl34 = shl i32 %118, 1
  %119 = inttoptr i32 %shl34 to i16*
  %120 = bitcast i16* %119 to i8*
  %121 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes35 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %121, i32 0, i32 16
  %arrayidx36 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes35, i32 0, i32 2
  store i8* %120, i8** %arrayidx36, align 4, !tbaa !2
  %122 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %123 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %122, i32 0, i32 4
  %124 = bitcast %union.anon.10* %123 to %struct.anon.11*
  %y_stride37 = getelementptr inbounds %struct.anon.11, %struct.anon.11* %124, i32 0, i32 0
  %125 = load i32, i32* %y_stride37, align 4, !tbaa !23
  %mul38 = mul nsw i32 2, %125
  %126 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride39 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %126, i32 0, i32 17
  %arrayidx40 = getelementptr inbounds [3 x i32], [3 x i32]* %stride39, i32 0, i32 0
  store i32 %mul38, i32* %arrayidx40, align 4, !tbaa !25
  %127 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %128 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %127, i32 0, i32 4
  %129 = bitcast %union.anon.10* %128 to %struct.anon.11*
  %uv_stride41 = getelementptr inbounds %struct.anon.11, %struct.anon.11* %129, i32 0, i32 1
  %130 = load i32, i32* %uv_stride41, align 4, !tbaa !23
  %mul42 = mul nsw i32 2, %130
  %131 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride43 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %131, i32 0, i32 17
  %arrayidx44 = getelementptr inbounds [3 x i32], [3 x i32]* %stride43, i32 0, i32 1
  store i32 %mul42, i32* %arrayidx44, align 4, !tbaa !25
  %132 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %133 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %132, i32 0, i32 4
  %134 = bitcast %union.anon.10* %133 to %struct.anon.11*
  %uv_stride45 = getelementptr inbounds %struct.anon.11, %struct.anon.11* %134, i32 0, i32 1
  %135 = load i32, i32* %uv_stride45, align 4, !tbaa !23
  %mul46 = mul nsw i32 2, %135
  %136 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride47 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %136, i32 0, i32 17
  %arrayidx48 = getelementptr inbounds [3 x i32], [3 x i32]* %stride47, i32 0, i32 2
  store i32 %mul46, i32* %arrayidx48, align 4, !tbaa !25
  br label %if.end49

if.end49:                                         ; preds = %if.then21, %if.end6
  %137 = load i32, i32* %bps, align 4, !tbaa !25
  %138 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %bps50 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %138, i32 0, i32 19
  store i32 %137, i32* %bps50, align 4, !tbaa !196
  %139 = load i8*, i8** %user_priv.addr, align 4, !tbaa !2
  %140 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %user_priv51 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %140, i32 0, i32 22
  store i8* %139, i8** %user_priv51, align 4, !tbaa !197
  %141 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %buffer_alloc = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %141, i32 0, i32 10
  %142 = load i8*, i8** %buffer_alloc, align 4, !tbaa !198
  %143 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data = getelementptr inbounds %struct.aom_image, %struct.aom_image* %143, i32 0, i32 23
  store i8* %142, i8** %img_data, align 4, !tbaa !199
  %144 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data_owner = getelementptr inbounds %struct.aom_image, %struct.aom_image* %144, i32 0, i32 24
  store i32 0, i32* %img_data_owner, align 4, !tbaa !200
  %145 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %self_allocd = getelementptr inbounds %struct.aom_image, %struct.aom_image* %145, i32 0, i32 25
  store i32 0, i32* %self_allocd, align 4, !tbaa !201
  %146 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %yv12.addr, align 4, !tbaa !2
  %frame_size = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %146, i32 0, i32 13
  %147 = load i32, i32* %frame_size, align 4, !tbaa !202
  %148 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %sz = getelementptr inbounds %struct.aom_image, %struct.aom_image* %148, i32 0, i32 18
  store i32 %147, i32* %sz, align 4, !tbaa !203
  %149 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.aom_image, %struct.aom_image* %149, i32 0, i32 26
  store %struct.aom_metadata_array* null, %struct.aom_metadata_array** %metadata, align 4, !tbaa !192
  %150 = bitcast i32* %bps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #5
  ret void
}

declare i32 @av1_copy_new_frame_dec(%struct.AV1Common*, %struct.yv12_buffer_config*, %struct.yv12_buffer_config*) #2

; Function Attrs: inlinehint nounwind
define internal %struct.yv12_buffer_config* @get_ref_frame(%struct.AV1Common* %cm, i32 %index) #3 {
entry:
  %retval = alloca %struct.yv12_buffer_config*, align 4
  %cm.addr = alloca %struct.AV1Common*, align 4
  %index.addr = alloca i32, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %0 = load i32, i32* %index.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4, !tbaa !25
  %cmp1 = icmp sge i32 %1, 8
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %struct.yv12_buffer_config* null, %struct.yv12_buffer_config** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 17
  %3 = load i32, i32* %index.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map, i32 0, i32 %3
  %4 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx, align 4, !tbaa !2
  %cmp2 = icmp eq %struct.RefCntBuffer* %4, null
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store %struct.yv12_buffer_config* null, %struct.yv12_buffer_config** %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %5 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %ref_frame_map5 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %5, i32 0, i32 17
  %6 = load i32, i32* %index.addr, align 4, !tbaa !25
  %arrayidx6 = getelementptr inbounds [8 x %struct.RefCntBuffer*], [8 x %struct.RefCntBuffer*]* %ref_frame_map5, i32 0, i32 %6
  %7 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %arrayidx6, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %7, i32 0, i32 17
  store %struct.yv12_buffer_config* %buf, %struct.yv12_buffer_config** %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %retval, align 4
  ret %struct.yv12_buffer_config* %8
}

; Function Attrs: nounwind
define internal i32 @decoder_peek_si_internal(i8* %data, i32 %data_sz, %struct.aom_codec_stream_info* %si, i32* %is_intra_only) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  %data_sz.addr = alloca i32, align 4
  %si.addr = alloca %struct.aom_codec_stream_info*, align 4
  %is_intra_only.addr = alloca i32*, align 4
  %intra_only_flag = alloca i32, align 4
  %got_sequence_header = alloca i32, align 4
  %found_keyframe = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %obu_header = alloca %struct.ObuHeader, align 4
  %payload_size = alloca i32, align 4
  %bytes_read = alloca i32, align 4
  %reduced_still_picture_hdr = alloca i8, align 1
  %status = alloca i32, align 4
  %rb = alloca %struct.aom_read_bit_buffer, align 4
  %still_picture = alloca i8, align 1
  %num_bits_width = alloca i32, align 4
  %num_bits_height = alloca i32, align 4
  %max_frame_width = alloca i32, align 4
  %max_frame_height = alloca i32, align 4
  %rb84 = alloca %struct.aom_read_bit_buffer, align 4
  %show_existing_frame = alloca i32, align 4
  %frame_type = alloca i8, align 1
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %data_sz, i32* %data_sz.addr, align 4, !tbaa !79
  store %struct.aom_codec_stream_info* %si, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  store i32* %is_intra_only, i32** %is_intra_only.addr, align 4, !tbaa !2
  %0 = bitcast i32* %intra_only_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %intra_only_flag, align 4, !tbaa !25
  %1 = bitcast i32* %got_sequence_header to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  store i32 0, i32* %got_sequence_header, align 4, !tbaa !25
  %2 = bitcast i32* %found_keyframe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  store i32 0, i32* %found_keyframe, align 4, !tbaa !25
  %3 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %4 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %4
  %5 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %cmp = icmp ule i8* %add.ptr, %5
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %6 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %cmp1 = icmp ult i32 %6, 1
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup145

if.end:                                           ; preds = %lor.lhs.false
  %7 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %w = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %7, i32 0, i32 0
  store i32 0, i32* %w, align 4, !tbaa !204
  %8 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %h = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %8, i32 0, i32 1
  store i32 0, i32* %h, align 4, !tbaa !205
  %9 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %is_kf = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %9, i32 0, i32 2
  store i32 0, i32* %is_kf, align 4, !tbaa !206
  %10 = bitcast %struct.ObuHeader* %obu_header to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %10) #5
  %11 = bitcast %struct.ObuHeader* %obu_header to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %11, i8 0, i32 24, i1 false)
  %12 = bitcast i32* %payload_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  store i32 0, i32* %payload_size, align 4, !tbaa !79
  %13 = bitcast i32* %bytes_read to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  store i32 0, i32* %bytes_read, align 4, !tbaa !79
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %reduced_still_picture_hdr) #5
  store i8 0, i8* %reduced_still_picture_hdr, align 1, !tbaa !23
  %14 = bitcast i32* %status to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #5
  %15 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %16 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %17 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %is_annexb = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %17, i32 0, i32 5
  %18 = load i32, i32* %is_annexb, align 4, !tbaa !207
  %call = call i32 @aom_read_obu_header_and_size(i8* %15, i32 %16, i32 %18, %struct.ObuHeader* %obu_header, i32* %payload_size, i32* %bytes_read)
  store i32 %call, i32* %status, align 4, !tbaa !23
  %19 = load i32, i32* %status, align 4, !tbaa !23
  %cmp2 = icmp ne i32 %19, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %20 = load i32, i32* %status, align 4, !tbaa !23
  store i32 %20, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup140

if.end4:                                          ; preds = %if.end
  %type = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %obu_header, i32 0, i32 1
  %21 = load i8, i8* %type, align 4, !tbaa !208
  %conv = zext i8 %21 to i32
  %cmp5 = icmp eq i32 %conv, 2
  br i1 %cmp5, label %if.then7, label %if.end21

if.then7:                                         ; preds = %if.end4
  %22 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %23 = load i32, i32* %bytes_read, align 4, !tbaa !79
  %24 = load i32, i32* %payload_size, align 4, !tbaa !79
  %add = add i32 %23, %24
  %cmp8 = icmp ult i32 %22, %add
  br i1 %cmp8, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.then7
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup140

if.end11:                                         ; preds = %if.then7
  %25 = load i32, i32* %bytes_read, align 4, !tbaa !79
  %26 = load i32, i32* %payload_size, align 4, !tbaa !79
  %add12 = add i32 %25, %26
  %27 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %add.ptr13 = getelementptr inbounds i8, i8* %27, i32 %add12
  store i8* %add.ptr13, i8** %data.addr, align 4, !tbaa !2
  %28 = load i32, i32* %bytes_read, align 4, !tbaa !79
  %29 = load i32, i32* %payload_size, align 4, !tbaa !79
  %add14 = add i32 %28, %29
  %30 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %sub = sub i32 %30, %add14
  store i32 %sub, i32* %data_sz.addr, align 4, !tbaa !79
  %31 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %32 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %33 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %is_annexb15 = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %33, i32 0, i32 5
  %34 = load i32, i32* %is_annexb15, align 4, !tbaa !207
  %call16 = call i32 @aom_read_obu_header_and_size(i8* %31, i32 %32, i32 %34, %struct.ObuHeader* %obu_header, i32* %payload_size, i32* %bytes_read)
  store i32 %call16, i32* %status, align 4, !tbaa !23
  %35 = load i32, i32* %status, align 4, !tbaa !23
  %cmp17 = icmp ne i32 %35, 0
  br i1 %cmp17, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end11
  %36 = load i32, i32* %status, align 4, !tbaa !23
  store i32 %36, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup140

if.end20:                                         ; preds = %if.end11
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.end4
  br label %while.cond

while.cond:                                       ; preds = %if.end129, %if.end21
  br label %while.body

while.body:                                       ; preds = %while.cond
  %37 = load i32, i32* %bytes_read, align 4, !tbaa !79
  %38 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8, i8* %38, i32 %37
  store i8* %add.ptr22, i8** %data.addr, align 4, !tbaa !2
  %39 = load i32, i32* %bytes_read, align 4, !tbaa !79
  %40 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %sub23 = sub i32 %40, %39
  store i32 %sub23, i32* %data_sz.addr, align 4, !tbaa !79
  %41 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %42 = load i32, i32* %payload_size, align 4, !tbaa !79
  %cmp24 = icmp ult i32 %41, %42
  br i1 %cmp24, label %if.then26, label %if.end27

if.then26:                                        ; preds = %while.body
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup140

if.end27:                                         ; preds = %while.body
  %type28 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %obu_header, i32 0, i32 1
  %43 = load i8, i8* %type28, align 4, !tbaa !208
  %conv29 = zext i8 %43 to i32
  %cmp30 = icmp eq i32 %conv29, 1
  br i1 %cmp30, label %if.then32, label %if.else

if.then32:                                        ; preds = %if.end27
  %44 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %cmp33 = icmp ult i32 %44, 2
  br i1 %cmp33, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.then32
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup140

if.end36:                                         ; preds = %if.then32
  %45 = bitcast %struct.aom_read_bit_buffer* %rb to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %45) #5
  %bit_buffer = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 0
  %46 = load i8*, i8** %data.addr, align 4, !tbaa !2
  store i8* %46, i8** %bit_buffer, align 4, !tbaa !210
  %bit_buffer_end = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 1
  %47 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %48 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %add.ptr37 = getelementptr inbounds i8, i8* %47, i32 %48
  store i8* %add.ptr37, i8** %bit_buffer_end, align 4, !tbaa !212
  %bit_offset = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 2
  store i32 0, i32* %bit_offset, align 4, !tbaa !213
  %error_handler_data = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 3
  store i8* null, i8** %error_handler_data, align 4, !tbaa !214
  %error_handler = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb, i32 0, i32 4
  store void (i8*)* null, void (i8*)** %error_handler, align 4, !tbaa !215
  %call38 = call signext i8 @av1_read_profile(%struct.aom_read_bit_buffer* %rb)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %still_picture) #5
  %call39 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %rb)
  %conv40 = trunc i32 %call39 to i8
  store i8 %conv40, i8* %still_picture, align 1, !tbaa !23
  %call41 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %rb)
  %conv42 = trunc i32 %call41 to i8
  store i8 %conv42, i8* %reduced_still_picture_hdr, align 1, !tbaa !23
  %49 = load i8, i8* %still_picture, align 1, !tbaa !23
  %tobool = icmp ne i8 %49, 0
  br i1 %tobool, label %if.end46, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end36
  %50 = load i8, i8* %reduced_still_picture_hdr, align 1, !tbaa !23
  %conv43 = zext i8 %50 to i32
  %tobool44 = icmp ne i32 %conv43, 0
  br i1 %tobool44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %land.lhs.true
  store i32 5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %land.lhs.true, %if.end36
  %51 = load i8, i8* %reduced_still_picture_hdr, align 1, !tbaa !23
  %conv47 = zext i8 %51 to i32
  %52 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %call48 = call i32 @parse_operating_points(%struct.aom_read_bit_buffer* %rb, i32 %conv47, %struct.aom_codec_stream_info* %52)
  %cmp49 = icmp ne i32 %call48, 0
  br i1 %cmp49, label %if.then51, label %if.end52

if.then51:                                        ; preds = %if.end46
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end52:                                         ; preds = %if.end46
  %53 = bitcast i32* %num_bits_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #5
  %call53 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %rb, i32 4)
  %add54 = add nsw i32 %call53, 1
  store i32 %add54, i32* %num_bits_width, align 4, !tbaa !25
  %54 = bitcast i32* %num_bits_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #5
  %call55 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %rb, i32 4)
  %add56 = add nsw i32 %call55, 1
  store i32 %add56, i32* %num_bits_height, align 4, !tbaa !25
  %55 = bitcast i32* %max_frame_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #5
  %56 = load i32, i32* %num_bits_width, align 4, !tbaa !25
  %call57 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %rb, i32 %56)
  %add58 = add nsw i32 %call57, 1
  store i32 %add58, i32* %max_frame_width, align 4, !tbaa !25
  %57 = bitcast i32* %max_frame_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #5
  %58 = load i32, i32* %num_bits_height, align 4, !tbaa !25
  %call59 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %rb, i32 %58)
  %add60 = add nsw i32 %call59, 1
  store i32 %add60, i32* %max_frame_height, align 4, !tbaa !25
  %59 = load i32, i32* %max_frame_width, align 4, !tbaa !25
  %60 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %w61 = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %60, i32 0, i32 0
  store i32 %59, i32* %w61, align 4, !tbaa !204
  %61 = load i32, i32* %max_frame_height, align 4, !tbaa !25
  %62 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %h62 = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %62, i32 0, i32 1
  store i32 %61, i32* %h62, align 4, !tbaa !205
  store i32 1, i32* %got_sequence_header, align 4, !tbaa !25
  %63 = bitcast i32* %max_frame_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #5
  %64 = bitcast i32* %max_frame_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #5
  %65 = bitcast i32* %num_bits_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #5
  %66 = bitcast i32* %num_bits_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end52, %if.then51, %if.then45
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %still_picture) #5
  %67 = bitcast %struct.aom_read_bit_buffer* %rb to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %67) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup140 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end117

if.else:                                          ; preds = %if.end27
  %type64 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %obu_header, i32 0, i32 1
  %68 = load i8, i8* %type64, align 4, !tbaa !208
  %conv65 = zext i8 %68 to i32
  %cmp66 = icmp eq i32 %conv65, 3
  br i1 %cmp66, label %if.then73, label %lor.lhs.false68

lor.lhs.false68:                                  ; preds = %if.else
  %type69 = getelementptr inbounds %struct.ObuHeader, %struct.ObuHeader* %obu_header, i32 0, i32 1
  %69 = load i8, i8* %type69, align 4, !tbaa !208
  %conv70 = zext i8 %69 to i32
  %cmp71 = icmp eq i32 %conv70, 6
  br i1 %cmp71, label %if.then73, label %if.end116

if.then73:                                        ; preds = %lor.lhs.false68, %if.else
  %70 = load i32, i32* %got_sequence_header, align 4, !tbaa !25
  %tobool74 = icmp ne i32 %70, 0
  br i1 %tobool74, label %land.lhs.true75, label %if.else79

land.lhs.true75:                                  ; preds = %if.then73
  %71 = load i8, i8* %reduced_still_picture_hdr, align 1, !tbaa !23
  %conv76 = zext i8 %71 to i32
  %tobool77 = icmp ne i32 %conv76, 0
  br i1 %tobool77, label %if.then78, label %if.else79

if.then78:                                        ; preds = %land.lhs.true75
  store i32 1, i32* %found_keyframe, align 4, !tbaa !25
  br label %while.end

if.else79:                                        ; preds = %land.lhs.true75, %if.then73
  %72 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %cmp80 = icmp ult i32 %72, 1
  br i1 %cmp80, label %if.then82, label %if.end83

if.then82:                                        ; preds = %if.else79
  store i32 7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup140

if.end83:                                         ; preds = %if.else79
  %73 = bitcast %struct.aom_read_bit_buffer* %rb84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %73) #5
  %bit_buffer85 = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb84, i32 0, i32 0
  %74 = load i8*, i8** %data.addr, align 4, !tbaa !2
  store i8* %74, i8** %bit_buffer85, align 4, !tbaa !210
  %bit_buffer_end86 = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb84, i32 0, i32 1
  %75 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %76 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %add.ptr87 = getelementptr inbounds i8, i8* %75, i32 %76
  store i8* %add.ptr87, i8** %bit_buffer_end86, align 4, !tbaa !212
  %bit_offset88 = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb84, i32 0, i32 2
  store i32 0, i32* %bit_offset88, align 4, !tbaa !213
  %error_handler_data89 = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb84, i32 0, i32 3
  store i8* null, i8** %error_handler_data89, align 4, !tbaa !214
  %error_handler90 = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %rb84, i32 0, i32 4
  store void (i8*)* null, void (i8*)** %error_handler90, align 4, !tbaa !215
  %77 = bitcast i32* %show_existing_frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #5
  %call91 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %rb84)
  store i32 %call91, i32* %show_existing_frame, align 4, !tbaa !25
  %78 = load i32, i32* %show_existing_frame, align 4, !tbaa !25
  %tobool92 = icmp ne i32 %78, 0
  br i1 %tobool92, label %if.end110, label %if.then93

if.then93:                                        ; preds = %if.end83
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %frame_type) #5
  %call94 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %rb84, i32 2)
  %conv95 = trunc i32 %call94 to i8
  store i8 %conv95, i8* %frame_type, align 1, !tbaa !23
  %79 = load i8, i8* %frame_type, align 1, !tbaa !23
  %conv96 = zext i8 %79 to i32
  %cmp97 = icmp eq i32 %conv96, 0
  br i1 %cmp97, label %if.then99, label %if.else100

if.then99:                                        ; preds = %if.then93
  store i32 1, i32* %found_keyframe, align 4, !tbaa !25
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup107

if.else100:                                       ; preds = %if.then93
  %80 = load i8, i8* %frame_type, align 1, !tbaa !23
  %conv101 = zext i8 %80 to i32
  %cmp102 = icmp eq i32 %conv101, 2
  br i1 %cmp102, label %if.then104, label %if.end105

if.then104:                                       ; preds = %if.else100
  store i32 1, i32* %intra_only_flag, align 4, !tbaa !25
  br label %if.end105

if.end105:                                        ; preds = %if.then104, %if.else100
  br label %if.end106

if.end106:                                        ; preds = %if.end105
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup107

cleanup107:                                       ; preds = %if.end106, %if.then99
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %frame_type) #5
  %cleanup.dest108 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest108, label %cleanup111 [
    i32 0, label %cleanup.cont109
  ]

cleanup.cont109:                                  ; preds = %cleanup107
  br label %if.end110

if.end110:                                        ; preds = %cleanup.cont109, %if.end83
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

cleanup111:                                       ; preds = %if.end110, %cleanup107
  %81 = bitcast i32* %show_existing_frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #5
  %82 = bitcast %struct.aom_read_bit_buffer* %rb84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %82) #5
  %cleanup.dest113 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest113, label %unreachable [
    i32 0, label %cleanup.cont114
    i32 3, label %while.end
  ]

cleanup.cont114:                                  ; preds = %cleanup111
  br label %if.end115

if.end115:                                        ; preds = %cleanup.cont114
  br label %if.end116

if.end116:                                        ; preds = %if.end115, %lor.lhs.false68
  br label %if.end117

if.end117:                                        ; preds = %if.end116, %cleanup.cont
  %83 = load i32, i32* %payload_size, align 4, !tbaa !79
  %84 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %add.ptr118 = getelementptr inbounds i8, i8* %84, i32 %83
  store i8* %add.ptr118, i8** %data.addr, align 4, !tbaa !2
  %85 = load i32, i32* %payload_size, align 4, !tbaa !79
  %86 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %sub119 = sub i32 %86, %85
  store i32 %sub119, i32* %data_sz.addr, align 4, !tbaa !79
  %87 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %cmp120 = icmp eq i32 %87, 0
  br i1 %cmp120, label %if.then122, label %if.end123

if.then122:                                       ; preds = %if.end117
  br label %while.end

if.end123:                                        ; preds = %if.end117
  %88 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %89 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %90 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %is_annexb124 = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %90, i32 0, i32 5
  %91 = load i32, i32* %is_annexb124, align 4, !tbaa !207
  %call125 = call i32 @aom_read_obu_header_and_size(i8* %88, i32 %89, i32 %91, %struct.ObuHeader* %obu_header, i32* %payload_size, i32* %bytes_read)
  store i32 %call125, i32* %status, align 4, !tbaa !23
  %92 = load i32, i32* %status, align 4, !tbaa !23
  %cmp126 = icmp ne i32 %92, 0
  br i1 %cmp126, label %if.then128, label %if.end129

if.then128:                                       ; preds = %if.end123
  %93 = load i32, i32* %status, align 4, !tbaa !23
  store i32 %93, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup140

if.end129:                                        ; preds = %if.end123
  br label %while.cond

while.end:                                        ; preds = %if.then122, %cleanup111, %if.then78
  %94 = load i32, i32* %got_sequence_header, align 4, !tbaa !25
  %tobool130 = icmp ne i32 %94, 0
  br i1 %tobool130, label %land.lhs.true131, label %if.end135

land.lhs.true131:                                 ; preds = %while.end
  %95 = load i32, i32* %found_keyframe, align 4, !tbaa !25
  %tobool132 = icmp ne i32 %95, 0
  br i1 %tobool132, label %if.then133, label %if.end135

if.then133:                                       ; preds = %land.lhs.true131
  %96 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %is_kf134 = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %96, i32 0, i32 2
  store i32 1, i32* %is_kf134, align 4, !tbaa !206
  br label %if.end135

if.end135:                                        ; preds = %if.then133, %land.lhs.true131, %while.end
  %97 = load i32*, i32** %is_intra_only.addr, align 4, !tbaa !2
  %cmp136 = icmp ne i32* %97, null
  br i1 %cmp136, label %if.then138, label %if.end139

if.then138:                                       ; preds = %if.end135
  %98 = load i32, i32* %intra_only_flag, align 4, !tbaa !25
  %99 = load i32*, i32** %is_intra_only.addr, align 4, !tbaa !2
  store i32 %98, i32* %99, align 4, !tbaa !25
  br label %if.end139

if.end139:                                        ; preds = %if.then138, %if.end135
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup140

cleanup140:                                       ; preds = %if.end139, %if.then128, %if.then82, %cleanup, %if.then35, %if.then26, %if.then19, %if.then10, %if.then3
  %100 = bitcast i32* %status to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %reduced_still_picture_hdr) #5
  %101 = bitcast i32* %bytes_read to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #5
  %102 = bitcast i32* %payload_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #5
  %103 = bitcast %struct.ObuHeader* %obu_header to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %103) #5
  br label %cleanup145

cleanup145:                                       ; preds = %cleanup140, %if.then
  %104 = bitcast i32* %found_keyframe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #5
  %105 = bitcast i32* %got_sequence_header to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #5
  %106 = bitcast i32* %intra_only_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #5
  %107 = load i32, i32* %retval, align 4
  ret i32 %107

unreachable:                                      ; preds = %cleanup111
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

declare i32 @aom_read_obu_header_and_size(i8*, i32, i32, %struct.ObuHeader*, i32*, i32*) #2

declare signext i8 @av1_read_profile(%struct.aom_read_bit_buffer*) #2

declare i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer*) #2

; Function Attrs: nounwind
define internal i32 @parse_operating_points(%struct.aom_read_bit_buffer* %rb, i32 %is_reduced_header, %struct.aom_codec_stream_info* %si) #0 {
entry:
  %retval = alloca i32, align 4
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %is_reduced_header.addr = alloca i32, align 4
  %si.addr = alloca %struct.aom_codec_stream_info*, align 4
  %operating_point_idc0 = alloca i32, align 4
  %decoder_model_info_present_flag = alloca i8, align 1
  %buffer_delay_length_minus_1 = alloca i32, align 4
  %status = alloca i32, align 4
  %timing_info_present_flag = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %initial_display_delay_present_flag = alloca i8, align 1
  %operating_points_cnt_minus_1 = alloca i8, align 1
  %i = alloca i32, align 4
  %operating_point_idc = alloca i32, align 4
  %seq_level_idx = alloca i32, align 4
  %decoder_model_present_for_this_op = alloca i8, align 1
  %initial_display_delay_present_for_this_op = alloca i8, align 1
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i32 %is_reduced_header, i32* %is_reduced_header.addr, align 4, !tbaa !25
  store %struct.aom_codec_stream_info* %si, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %0 = bitcast i32* %operating_point_idc0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  store i32 0, i32* %operating_point_idc0, align 4, !tbaa !25
  %1 = load i32, i32* %is_reduced_header.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %2, i32 5)
  br label %if.end72

if.else:                                          ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %decoder_model_info_present_flag) #5
  store i8 0, i8* %decoder_model_info_present_flag, align 1, !tbaa !23
  %3 = bitcast i32* %buffer_delay_length_minus_1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %buffer_delay_length_minus_1, align 4, !tbaa !25
  %4 = bitcast i32* %status to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %timing_info_present_flag) #5
  %5 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call1 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %5)
  %conv = trunc i32 %call1 to i8
  store i8 %conv, i8* %timing_info_present_flag, align 1, !tbaa !23
  %6 = load i8, i8* %timing_info_present_flag, align 1, !tbaa !23
  %tobool2 = icmp ne i8 %6, 0
  br i1 %tobool2, label %if.then3, label %if.end17

if.then3:                                         ; preds = %if.else
  %7 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call4 = call i32 @parse_timing_info(%struct.aom_read_bit_buffer* %7)
  store i32 %call4, i32* %status, align 4, !tbaa !23
  %cmp = icmp ne i32 %call4, 0
  br i1 %cmp, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then3
  %8 = load i32, i32* %status, align 4, !tbaa !23
  store i32 %8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup66

if.end:                                           ; preds = %if.then3
  %9 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call7 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %9)
  %conv8 = trunc i32 %call7 to i8
  store i8 %conv8, i8* %decoder_model_info_present_flag, align 1, !tbaa !23
  %10 = load i8, i8* %decoder_model_info_present_flag, align 1, !tbaa !23
  %tobool9 = icmp ne i8 %10, 0
  br i1 %tobool9, label %if.then10, label %if.end16

if.then10:                                        ; preds = %if.end
  %11 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call11 = call i32 @parse_decoder_model_info(%struct.aom_read_bit_buffer* %11, i32* %buffer_delay_length_minus_1)
  store i32 %call11, i32* %status, align 4, !tbaa !23
  %cmp12 = icmp ne i32 %call11, 0
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then10
  %12 = load i32, i32* %status, align 4, !tbaa !23
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup66

if.end15:                                         ; preds = %if.then10
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %if.end
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.else
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %initial_display_delay_present_flag) #5
  %13 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call18 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %13)
  %conv19 = trunc i32 %call18 to i8
  store i8 %conv19, i8* %initial_display_delay_present_flag, align 1, !tbaa !23
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %operating_points_cnt_minus_1) #5
  %14 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call20 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %14, i32 5)
  %conv21 = trunc i32 %call20 to i8
  store i8 %conv21, i8* %operating_points_cnt_minus_1, align 1, !tbaa !23
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #5
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end17
  %16 = load i32, i32* %i, align 4, !tbaa !25
  %17 = load i8, i8* %operating_points_cnt_minus_1, align 1, !tbaa !23
  %conv22 = zext i8 %17 to i32
  %add = add nsw i32 %conv22, 1
  %cmp23 = icmp slt i32 %16, %add
  br i1 %cmp23, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup62

for.body:                                         ; preds = %for.cond
  %18 = bitcast i32* %operating_point_idc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #5
  %19 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call25 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %19, i32 12)
  store i32 %call25, i32* %operating_point_idc, align 4, !tbaa !25
  %20 = load i32, i32* %i, align 4, !tbaa !25
  %cmp26 = icmp eq i32 %20, 0
  br i1 %cmp26, label %if.then28, label %if.end29

if.then28:                                        ; preds = %for.body
  %21 = load i32, i32* %operating_point_idc, align 4, !tbaa !25
  store i32 %21, i32* %operating_point_idc0, align 4, !tbaa !25
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %for.body
  %22 = bitcast i32* %seq_level_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call30 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %23, i32 5)
  store i32 %call30, i32* %seq_level_idx, align 4, !tbaa !25
  %24 = load i32, i32* %seq_level_idx, align 4, !tbaa !25
  %cmp31 = icmp sgt i32 %24, 7
  br i1 %cmp31, label %if.then33, label %if.end35

if.then33:                                        ; preds = %if.end29
  %25 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call34 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %25)
  br label %if.end35

if.end35:                                         ; preds = %if.then33, %if.end29
  %26 = load i8, i8* %decoder_model_info_present_flag, align 1, !tbaa !23
  %tobool36 = icmp ne i8 %26, 0
  br i1 %tobool36, label %if.then37, label %if.end48

if.then37:                                        ; preds = %if.end35
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %decoder_model_present_for_this_op) #5
  %27 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call38 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %27)
  %conv39 = trunc i32 %call38 to i8
  store i8 %conv39, i8* %decoder_model_present_for_this_op, align 1, !tbaa !23
  %28 = load i8, i8* %decoder_model_present_for_this_op, align 1, !tbaa !23
  %tobool40 = icmp ne i8 %28, 0
  br i1 %tobool40, label %if.then41, label %if.end47

if.then41:                                        ; preds = %if.then37
  %29 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %30 = load i32, i32* %buffer_delay_length_minus_1, align 4, !tbaa !25
  %call42 = call i32 @parse_op_parameters_info(%struct.aom_read_bit_buffer* %29, i32 %30)
  store i32 %call42, i32* %status, align 4, !tbaa !23
  %cmp43 = icmp ne i32 %call42, 0
  br i1 %cmp43, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.then41
  %31 = load i32, i32* %status, align 4, !tbaa !23
  store i32 %31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %if.then41
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.then37
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end47, %if.then45
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %decoder_model_present_for_this_op) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup58 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end48

if.end48:                                         ; preds = %cleanup.cont, %if.end35
  %32 = load i8, i8* %initial_display_delay_present_flag, align 1, !tbaa !23
  %tobool49 = icmp ne i8 %32, 0
  br i1 %tobool49, label %if.then50, label %if.end57

if.then50:                                        ; preds = %if.end48
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %initial_display_delay_present_for_this_op) #5
  %33 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call51 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %33)
  %conv52 = trunc i32 %call51 to i8
  store i8 %conv52, i8* %initial_display_delay_present_for_this_op, align 1, !tbaa !23
  %34 = load i8, i8* %initial_display_delay_present_for_this_op, align 1, !tbaa !23
  %tobool53 = icmp ne i8 %34, 0
  br i1 %tobool53, label %if.then54, label %if.end56

if.then54:                                        ; preds = %if.then50
  %35 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call55 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %35, i32 4)
  br label %if.end56

if.end56:                                         ; preds = %if.then54, %if.then50
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %initial_display_delay_present_for_this_op) #5
  br label %if.end57

if.end57:                                         ; preds = %if.end56, %if.end48
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup58

cleanup58:                                        ; preds = %if.end57, %cleanup
  %36 = bitcast i32* %seq_level_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  %37 = bitcast i32* %operating_point_idc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #5
  %cleanup.dest60 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest60, label %cleanup62 [
    i32 0, label %cleanup.cont61
  ]

cleanup.cont61:                                   ; preds = %cleanup58
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont61
  %38 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %38, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

cleanup62:                                        ; preds = %cleanup58, %for.cond.cleanup
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #5
  %cleanup.dest63 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest63, label %cleanup64 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup62
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup64

cleanup64:                                        ; preds = %for.end, %cleanup62
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %operating_points_cnt_minus_1) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %initial_display_delay_present_flag) #5
  br label %cleanup66

cleanup66:                                        ; preds = %cleanup64, %if.then14, %if.then6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %timing_info_present_flag) #5
  %40 = bitcast i32* %status to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #5
  %41 = bitcast i32* %buffer_delay_length_minus_1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %decoder_model_info_present_flag) #5
  %cleanup.dest70 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest70, label %cleanup78 [
    i32 0, label %cleanup.cont71
  ]

cleanup.cont71:                                   ; preds = %cleanup66
  br label %if.end72

if.end72:                                         ; preds = %cleanup.cont71, %if.then
  %42 = load i32, i32* %operating_point_idc0, align 4, !tbaa !25
  %43 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %number_spatial_layers = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %43, i32 0, i32 3
  %44 = load %struct.aom_codec_stream_info*, %struct.aom_codec_stream_info** %si.addr, align 4, !tbaa !2
  %number_temporal_layers = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %44, i32 0, i32 4
  %call73 = call i32 @aom_get_num_layers_from_operating_point_idc(i32 %42, i32* %number_spatial_layers, i32* %number_temporal_layers)
  %cmp74 = icmp ne i32 %call73, 0
  br i1 %cmp74, label %if.then76, label %if.end77

if.then76:                                        ; preds = %if.end72
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup78

if.end77:                                         ; preds = %if.end72
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup78

cleanup78:                                        ; preds = %if.end77, %if.then76, %cleanup66
  %45 = bitcast i32* %operating_point_idc0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #5
  %46 = load i32, i32* %retval, align 4
  ret i32 %46
}

declare i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer*, i32) #2

; Function Attrs: nounwind
define internal i32 @parse_timing_info(%struct.aom_read_bit_buffer* %rb) #0 {
entry:
  %retval = alloca i32, align 4
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %num_units_in_display_tick = alloca i32, align 4
  %time_scale = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %equal_picture_interval = alloca i8, align 1
  %num_ticks_per_picture_minus_1 = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %0 = bitcast i32* %num_units_in_display_tick to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call = call i32 @aom_rb_read_unsigned_literal(%struct.aom_read_bit_buffer* %1, i32 32)
  store i32 %call, i32* %num_units_in_display_tick, align 4, !tbaa !25
  %2 = bitcast i32* %time_scale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call1 = call i32 @aom_rb_read_unsigned_literal(%struct.aom_read_bit_buffer* %3, i32 32)
  store i32 %call1, i32* %time_scale, align 4, !tbaa !25
  %4 = load i32, i32* %num_units_in_display_tick, align 4, !tbaa !25
  %cmp = icmp eq i32 %4, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %5 = load i32, i32* %time_scale, align 4, !tbaa !25
  %cmp2 = icmp eq i32 %5, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup12

if.end:                                           ; preds = %lor.lhs.false
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %equal_picture_interval) #5
  %6 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call3 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %6)
  %conv = trunc i32 %call3 to i8
  store i8 %conv, i8* %equal_picture_interval, align 1, !tbaa !23
  %7 = load i8, i8* %equal_picture_interval, align 1, !tbaa !23
  %tobool = icmp ne i8 %7, 0
  br i1 %tobool, label %if.then4, label %if.end10

if.then4:                                         ; preds = %if.end
  %8 = bitcast i32* %num_ticks_per_picture_minus_1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call5 = call i32 @aom_rb_read_uvlc(%struct.aom_read_bit_buffer* %9)
  store i32 %call5, i32* %num_ticks_per_picture_minus_1, align 4, !tbaa !25
  %10 = load i32, i32* %num_ticks_per_picture_minus_1, align 4, !tbaa !25
  %cmp6 = icmp eq i32 %10, -1
  br i1 %cmp6, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.then4
  store i32 5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.then4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end9, %if.then8
  %11 = bitcast i32* %num_ticks_per_picture_minus_1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup11 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end10

if.end10:                                         ; preds = %cleanup.cont, %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup11

cleanup11:                                        ; preds = %if.end10, %cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %equal_picture_interval) #5
  br label %cleanup12

cleanup12:                                        ; preds = %cleanup11, %if.then
  %12 = bitcast i32* %time_scale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  %13 = bitcast i32* %num_units_in_display_tick to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: nounwind
define internal i32 @parse_decoder_model_info(%struct.aom_read_bit_buffer* %rb, i32* %buffer_delay_length_minus_1) #0 {
entry:
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %buffer_delay_length_minus_1.addr = alloca i32*, align 4
  %num_units_in_decoding_tick = alloca i32, align 4
  %buffer_removal_time_length_minus_1 = alloca i8, align 1
  %frame_presentation_time_length_minus_1 = alloca i8, align 1
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i32* %buffer_delay_length_minus_1, i32** %buffer_delay_length_minus_1.addr, align 4, !tbaa !2
  %0 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %0, i32 5)
  %1 = load i32*, i32** %buffer_delay_length_minus_1.addr, align 4, !tbaa !2
  store i32 %call, i32* %1, align 4, !tbaa !25
  %2 = bitcast i32* %num_units_in_decoding_tick to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call1 = call i32 @aom_rb_read_unsigned_literal(%struct.aom_read_bit_buffer* %3, i32 32)
  store i32 %call1, i32* %num_units_in_decoding_tick, align 4, !tbaa !25
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %buffer_removal_time_length_minus_1) #5
  %4 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call2 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %4, i32 5)
  %conv = trunc i32 %call2 to i8
  store i8 %conv, i8* %buffer_removal_time_length_minus_1, align 1, !tbaa !23
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %frame_presentation_time_length_minus_1) #5
  %5 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call3 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %5, i32 5)
  %conv4 = trunc i32 %call3 to i8
  store i8 %conv4, i8* %frame_presentation_time_length_minus_1, align 1, !tbaa !23
  %6 = load i32, i32* %num_units_in_decoding_tick, align 4, !tbaa !25
  %7 = load i8, i8* %buffer_removal_time_length_minus_1, align 1, !tbaa !23
  %8 = load i8, i8* %frame_presentation_time_length_minus_1, align 1, !tbaa !23
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %frame_presentation_time_length_minus_1) #5
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %buffer_removal_time_length_minus_1) #5
  %9 = bitcast i32* %num_units_in_decoding_tick to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #5
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @parse_op_parameters_info(%struct.aom_read_bit_buffer* %rb, i32 %buffer_delay_length_minus_1) #0 {
entry:
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %buffer_delay_length_minus_1.addr = alloca i32, align 4
  %n = alloca i32, align 4
  %decoder_buffer_delay = alloca i32, align 4
  %encoder_buffer_delay = alloca i32, align 4
  %low_delay_mode_flag = alloca i8, align 1
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i32 %buffer_delay_length_minus_1, i32* %buffer_delay_length_minus_1.addr, align 4, !tbaa !25
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %buffer_delay_length_minus_1.addr, align 4, !tbaa !25
  %add = add nsw i32 %1, 1
  store i32 %add, i32* %n, align 4, !tbaa !25
  %2 = bitcast i32* %decoder_buffer_delay to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %4 = load i32, i32* %n, align 4, !tbaa !25
  %call = call i32 @aom_rb_read_unsigned_literal(%struct.aom_read_bit_buffer* %3, i32 %4)
  store i32 %call, i32* %decoder_buffer_delay, align 4, !tbaa !25
  %5 = bitcast i32* %encoder_buffer_delay to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %7 = load i32, i32* %n, align 4, !tbaa !25
  %call1 = call i32 @aom_rb_read_unsigned_literal(%struct.aom_read_bit_buffer* %6, i32 %7)
  store i32 %call1, i32* %encoder_buffer_delay, align 4, !tbaa !25
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %low_delay_mode_flag) #5
  %8 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call2 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %8)
  %conv = trunc i32 %call2 to i8
  store i8 %conv, i8* %low_delay_mode_flag, align 1, !tbaa !23
  %9 = load i32, i32* %decoder_buffer_delay, align 4, !tbaa !25
  %10 = load i32, i32* %encoder_buffer_delay, align 4, !tbaa !25
  %11 = load i8, i8* %low_delay_mode_flag, align 1, !tbaa !23
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %low_delay_mode_flag) #5
  %12 = bitcast i32* %encoder_buffer_delay to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  %13 = bitcast i32* %decoder_buffer_delay to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #5
  ret i32 0
}

declare i32 @aom_get_num_layers_from_operating_point_idc(i32, i32*, i32*) #2

declare i32 @aom_rb_read_unsigned_literal(%struct.aom_read_bit_buffer*, i32) #2

declare i32 @aom_rb_read_uvlc(%struct.aom_read_bit_buffer*) #2

; Function Attrs: nounwind
define internal void @lock_buffer_pool(%struct.BufferPool* %pool) #0 {
entry:
  %pool.addr = alloca %struct.BufferPool*, align 4
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %0 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @decrease_ref_count(%struct.RefCntBuffer* %buf, %struct.BufferPool* %pool) #3 {
entry:
  %buf.addr = alloca %struct.RefCntBuffer*, align 4
  %pool.addr = alloca %struct.BufferPool*, align 4
  store %struct.RefCntBuffer* %buf, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %0 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.RefCntBuffer* %0, null
  br i1 %cmp, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %1 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %ref_count = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %1, i32 0, i32 0
  %2 = load i32, i32* %ref_count, align 4, !tbaa !216
  %dec = add nsw i32 %2, -1
  store i32 %dec, i32* %ref_count, align 4, !tbaa !216
  %3 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %ref_count1 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %3, i32 0, i32 0
  %4 = load i32, i32* %ref_count1, align 4, !tbaa !216
  %cmp2 = icmp eq i32 %4, 0
  br i1 %cmp2, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %5 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %5, i32 0, i32 16
  %data = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer, i32 0, i32 0
  %6 = load i8*, i8** %data, align 4, !tbaa !217
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.then3, label %if.end

if.then3:                                         ; preds = %land.lhs.true
  %7 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %release_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %7, i32 0, i32 2
  %8 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !80
  %9 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %cb_priv = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %9, i32 0, i32 0
  %10 = load i8*, i8** %cb_priv, align 4, !tbaa !83
  %11 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer4 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %11, i32 0, i32 16
  %call = call i32 %8(i8* %10, %struct.aom_codec_frame_buffer* %raw_frame_buffer4)
  %12 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer5 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %12, i32 0, i32 16
  %data6 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer5, i32 0, i32 0
  store i8* null, i8** %data6, align 4, !tbaa !217
  %13 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer7 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %13, i32 0, i32 16
  %size = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer7, i32 0, i32 1
  store i32 0, i32* %size, align 4, !tbaa !218
  %14 = load %struct.RefCntBuffer*, %struct.RefCntBuffer** %buf.addr, align 4, !tbaa !2
  %raw_frame_buffer8 = getelementptr inbounds %struct.RefCntBuffer, %struct.RefCntBuffer* %14, i32 0, i32 16
  %priv = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %raw_frame_buffer8, i32 0, i32 2
  store i8* null, i8** %priv, align 4, !tbaa !111
  br label %if.end

if.end:                                           ; preds = %if.then3, %land.lhs.true, %if.then
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: nounwind
define internal void @unlock_buffer_pool(%struct.BufferPool* %pool) #0 {
entry:
  %pool.addr = alloca %struct.BufferPool*, align 4
  store %struct.BufferPool* %pool, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  %0 = load %struct.BufferPool*, %struct.BufferPool** %pool.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal i32 @init_decoder(%struct.aom_codec_alg_priv* %ctx) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %winterface = alloca %struct.AVxWorkerInterface*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  store %struct.AVxWorkerInterface* %call, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %1 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %last_show_frame = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %1, i32 0, i32 7
  store %struct.RefCntBuffer* null, %struct.RefCntBuffer** %last_show_frame, align 4, !tbaa !94
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %need_resync = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %2, i32 0, i32 24
  store i32 1, i32* %need_resync, align 8, !tbaa !95
  %3 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %flushed = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %3, i32 0, i32 5
  store i32 0, i32* %flushed, align 4, !tbaa !16
  %call1 = call i8* @aom_calloc(i32 1, i32 363668)
  %4 = bitcast i8* %call1 to %struct.BufferPool*
  %5 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %5, i32 0, i32 25
  store %struct.BufferPool* %4, %struct.BufferPool** %buffer_pool, align 4, !tbaa !78
  %6 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool2 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %6, i32 0, i32 25
  %7 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool2, align 4, !tbaa !78
  %cmp = icmp eq %struct.BufferPool* %7, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup40

if.end:                                           ; preds = %entry
  %call3 = call i8* @aom_malloc(i32 28)
  %8 = bitcast i8* %call3 to %struct.AVxWorker*
  %9 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %9, i32 0, i32 20
  store %struct.AVxWorker* %8, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  %10 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker4 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %10, i32 0, i32 20
  %11 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker4, align 8, !tbaa !31
  %cmp5 = icmp eq %struct.AVxWorker* %11, null
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  %12 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  call void @set_error_detail(%struct.aom_codec_alg_priv* %12, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.1, i32 0, i32 0))
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup40

if.end7:                                          ; preds = %if.end
  %13 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker8 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %14, i32 0, i32 20
  %15 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker8, align 8, !tbaa !31
  store %struct.AVxWorker* %15, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %16 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #5
  store %struct.FrameWorkerData* null, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %17 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %init = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %17, i32 0, i32 0
  %18 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %init, align 4, !tbaa !219
  %19 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void %18(%struct.AVxWorker* %19)
  %20 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %thread_name = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %20, i32 0, i32 2
  store i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.2, i32 0, i32 0), i8** %thread_name, align 4, !tbaa !220
  %call9 = call i8* @aom_memalign(i32 32, i32 32)
  %21 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %21, i32 0, i32 4
  store i8* %call9, i8** %data1, align 4, !tbaa !32
  %22 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data110 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %22, i32 0, i32 4
  %23 = load i8*, i8** %data110, align 4, !tbaa !32
  %cmp11 = icmp eq i8* %23, null
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end7
  %24 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  call void @set_error_detail(%struct.aom_codec_alg_priv* %24, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.3, i32 0, i32 0))
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.end7
  %25 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data114 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %25, i32 0, i32 4
  %26 = load i8*, i8** %data114, align 4, !tbaa !32
  %27 = bitcast i8* %26 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %27, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %28 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool15 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %28, i32 0, i32 25
  %29 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool15, align 4, !tbaa !78
  %call16 = call %struct.AV1Decoder* @av1_decoder_create(%struct.BufferPool* %29)
  %30 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %30, i32 0, i32 0
  store %struct.AV1Decoder* %call16, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %31 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi17 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %31, i32 0, i32 0
  %32 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi17, align 4, !tbaa !36
  %cmp18 = icmp eq %struct.AV1Decoder* %32, null
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end13
  %33 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  call void @set_error_detail(%struct.aom_codec_alg_priv* %33, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.3, i32 0, i32 0))
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %if.end13
  %34 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %frame_context_ready = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %34, i32 0, i32 6
  store i32 0, i32* %frame_context_ready, align 4, !tbaa !221
  %35 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %received_frame = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %35, i32 0, i32 5
  store i32 0, i32* %received_frame, align 4, !tbaa !93
  %36 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %cfg = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %36, i32 0, i32 1
  %allow_lowbitdepth = getelementptr inbounds %struct.aom_codec_dec_cfg, %struct.aom_codec_dec_cfg* %cfg, i32 0, i32 3
  %37 = load i32, i32* %allow_lowbitdepth, align 4, !tbaa !22
  %38 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi21 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %38, i32 0, i32 0
  %39 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi21, align 4, !tbaa !36
  %allow_lowbitdepth22 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %39, i32 0, i32 18
  store i32 %37, i32* %allow_lowbitdepth22, align 8, !tbaa !222
  %40 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %cfg23 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %40, i32 0, i32 1
  %threads = getelementptr inbounds %struct.aom_codec_dec_cfg, %struct.aom_codec_dec_cfg* %cfg23, i32 0, i32 0
  %41 = load i32, i32* %threads, align 8, !tbaa !223
  %42 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi24 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %42, i32 0, i32 0
  %43 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi24, align 4, !tbaa !36
  %max_threads = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %43, i32 0, i32 19
  store i32 %41, i32* %max_threads, align 4, !tbaa !224
  %44 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %invert_tile_order = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %44, i32 0, i32 6
  %45 = load i32, i32* %invert_tile_order, align 8, !tbaa !129
  %46 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi25 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %46, i32 0, i32 0
  %47 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi25, align 4, !tbaa !36
  %inv_tile_order = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %47, i32 0, i32 20
  store i32 %45, i32* %inv_tile_order, align 32, !tbaa !225
  %48 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %tile_mode = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %48, i32 0, i32 13
  %49 = load i32, i32* %tile_mode, align 4, !tbaa !28
  %50 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi26 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %50, i32 0, i32 0
  %51 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi26, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %51, i32 0, i32 1
  %tiles = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common, i32 0, i32 40
  %large_scale = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles, i32 0, i32 17
  store i32 %49, i32* %large_scale, align 4, !tbaa !226
  %52 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %is_annexb = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %52, i32 0, i32 17
  %53 = load i32, i32* %is_annexb, align 4, !tbaa !89
  %54 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi27 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %54, i32 0, i32 0
  %55 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi27, align 4, !tbaa !36
  %is_annexb28 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %55, i32 0, i32 54
  store i32 %53, i32* %is_annexb28, align 8, !tbaa !227
  %56 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %decode_tile_row = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %56, i32 0, i32 11
  %57 = load i32, i32* %decode_tile_row, align 4, !tbaa !29
  %58 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi29 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %58, i32 0, i32 0
  %59 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi29, align 4, !tbaa !36
  %dec_tile_row = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %59, i32 0, i32 25
  store i32 %57, i32* %dec_tile_row, align 4, !tbaa !100
  %60 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %decode_tile_col = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %60, i32 0, i32 12
  %61 = load i32, i32* %decode_tile_col, align 8, !tbaa !30
  %62 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi30 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %62, i32 0, i32 0
  %63 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi30, align 4, !tbaa !36
  %dec_tile_col = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %63, i32 0, i32 26
  store i32 %61, i32* %dec_tile_col, align 8, !tbaa !105
  %64 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %operating_point = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %64, i32 0, i32 18
  %65 = load i32, i32* %operating_point, align 8, !tbaa !134
  %66 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi31 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %66, i32 0, i32 0
  %67 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi31, align 4, !tbaa !36
  %operating_point32 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %67, i32 0, i32 31
  store i32 %65, i32* %operating_point32, align 4, !tbaa !228
  %68 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %output_all_layers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %68, i32 0, i32 19
  %69 = load i32, i32* %output_all_layers, align 4, !tbaa !135
  %70 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi33 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %70, i32 0, i32 0
  %71 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi33, align 4, !tbaa !36
  %output_all_layers34 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %71, i32 0, i32 14
  store i32 %69, i32* %output_all_layers34, align 4, !tbaa !229
  %72 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_tile_debug = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %72, i32 0, i32 14
  %73 = load i32, i32* %ext_tile_debug, align 8, !tbaa !136
  %74 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi35 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %74, i32 0, i32 0
  %75 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi35, align 4, !tbaa !36
  %ext_tile_debug36 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %75, i32 0, i32 42
  store i32 %73, i32* %ext_tile_debug36, align 4, !tbaa !97
  %76 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %row_mt = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %76, i32 0, i32 15
  %77 = load i32, i32* %row_mt, align 4, !tbaa !27
  %78 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi37 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %78, i32 0, i32 0
  %79 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi37, align 4, !tbaa !36
  %row_mt38 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %79, i32 0, i32 43
  store i32 %77, i32* %row_mt38, align 8, !tbaa !230
  %80 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %hook = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %80, i32 0, i32 3
  store i32 (i8*, i8*)* @frame_worker_hook, i32 (i8*, i8*)** %hook, align 4, !tbaa !231
  %81 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  call void @init_buffer_callbacks(%struct.aom_codec_alg_priv* %81)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.then19, %if.then12
  %82 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #5
  %83 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #5
  br label %cleanup40

cleanup40:                                        ; preds = %cleanup, %if.then6, %if.then
  %84 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #5
  %85 = load i32, i32* %retval, align 4
  ret i32 %85
}

declare i32 @aom_uleb_decode(i8*, i32, i64*, i32*) #2

; Function Attrs: nounwind
define internal i32 @decode_one(%struct.aom_codec_alg_priv* %ctx, i8** %data, i32 %data_sz, i8* %user_priv) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %data.addr = alloca i8**, align 4
  %data_sz.addr = alloca i32, align 4
  %user_priv.addr = alloca i8*, align 4
  %winterface = alloca %struct.AVxWorkerInterface*, align 4
  %is_intra_only = alloca i32, align 4
  %res = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8** %data, i8*** %data.addr, align 4, !tbaa !2
  store i32 %data_sz, i32* %data_sz.addr, align 4, !tbaa !79
  store i8* %user_priv, i8** %user_priv.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call %struct.AVxWorkerInterface* @aom_get_worker_interface()
  store %struct.AVxWorkerInterface* %call, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %1 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %si = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %1, i32 0, i32 2
  %h = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %si, i32 0, i32 1
  %2 = load i32, i32* %h, align 4, !tbaa !232
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.end12, label %if.then

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %is_intra_only to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %is_intra_only, align 4, !tbaa !25
  %4 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %is_annexb = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %4, i32 0, i32 17
  %5 = load i32, i32* %is_annexb, align 4, !tbaa !89
  %6 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %si1 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %6, i32 0, i32 2
  %is_annexb2 = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %si1, i32 0, i32 5
  store i32 %5, i32* %is_annexb2, align 4, !tbaa !233
  %7 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %8, align 4, !tbaa !2
  %10 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %11 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %si3 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %11, i32 0, i32 2
  %call4 = call i32 @decoder_peek_si_internal(i8* %9, i32 %10, %struct.aom_codec_stream_info* %si3, i32* %is_intra_only)
  store i32 %call4, i32* %res, align 4, !tbaa !23
  %12 = load i32, i32* %res, align 4, !tbaa !23
  %cmp = icmp ne i32 %12, 0
  br i1 %cmp, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  %13 = load i32, i32* %res, align 4, !tbaa !23
  store i32 %13, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %14 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %si6 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %14, i32 0, i32 2
  %is_kf = getelementptr inbounds %struct.aom_codec_stream_info, %struct.aom_codec_stream_info* %si6, i32 0, i32 2
  %15 = load i32, i32* %is_kf, align 8, !tbaa !234
  %tobool7 = icmp ne i32 %15, 0
  br i1 %tobool7, label %if.end10, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end
  %16 = load i32, i32* %is_intra_only, align 4, !tbaa !25
  %tobool8 = icmp ne i32 %16, 0
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %land.lhs.true, %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end10, %if.then9, %if.then5
  %17 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #5
  %18 = bitcast i32* %is_intra_only to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup36 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end12

if.end12:                                         ; preds = %cleanup.cont, %entry
  %19 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #5
  %20 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %20, i32 0, i32 20
  %21 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %21, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %22 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #5
  %23 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %23, i32 0, i32 4
  %24 = load i8*, i8** %data1, align 4, !tbaa !32
  %25 = bitcast i8* %24 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %25, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %26 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  %27 = load i8*, i8** %26, align 4, !tbaa !2
  %28 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %data13 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %28, i32 0, i32 1
  store i8* %27, i8** %data13, align 4, !tbaa !235
  %29 = load i32, i32* %data_sz.addr, align 4, !tbaa !79
  %30 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %data_size = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %30, i32 0, i32 3
  store i32 %29, i32* %data_size, align 4, !tbaa !236
  %31 = load i8*, i8** %user_priv.addr, align 4, !tbaa !2
  %32 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %user_priv14 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %32, i32 0, i32 4
  store i8* %31, i8** %user_priv14, align 4, !tbaa !96
  %33 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %received_frame = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %33, i32 0, i32 5
  store i32 1, i32* %received_frame, align 4, !tbaa !93
  %34 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %tile_mode = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %34, i32 0, i32 13
  %35 = load i32, i32* %tile_mode, align 4, !tbaa !28
  %36 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %36, i32 0, i32 0
  %37 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %37, i32 0, i32 1
  %tiles = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common, i32 0, i32 40
  %large_scale = getelementptr inbounds %struct.CommonTileParams, %struct.CommonTileParams* %tiles, i32 0, i32 17
  store i32 %35, i32* %large_scale, align 4, !tbaa !226
  %38 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %decode_tile_row = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %38, i32 0, i32 11
  %39 = load i32, i32* %decode_tile_row, align 4, !tbaa !29
  %40 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi15 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %40, i32 0, i32 0
  %41 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi15, align 4, !tbaa !36
  %dec_tile_row = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %41, i32 0, i32 25
  store i32 %39, i32* %dec_tile_row, align 4, !tbaa !100
  %42 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %decode_tile_col = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %42, i32 0, i32 12
  %43 = load i32, i32* %decode_tile_col, align 8, !tbaa !30
  %44 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi16 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %44, i32 0, i32 0
  %45 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi16, align 4, !tbaa !36
  %dec_tile_col = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %45, i32 0, i32 26
  store i32 %43, i32* %dec_tile_col, align 8, !tbaa !105
  %46 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_tile_debug = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %46, i32 0, i32 14
  %47 = load i32, i32* %ext_tile_debug, align 8, !tbaa !136
  %48 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi17 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %48, i32 0, i32 0
  %49 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi17, align 4, !tbaa !36
  %ext_tile_debug18 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %49, i32 0, i32 42
  store i32 %47, i32* %ext_tile_debug18, align 4, !tbaa !97
  %50 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %row_mt = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %50, i32 0, i32 15
  %51 = load i32, i32* %row_mt, align 4, !tbaa !27
  %52 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi19 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %52, i32 0, i32 0
  %53 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi19, align 4, !tbaa !36
  %row_mt20 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %53, i32 0, i32 43
  store i32 %51, i32* %row_mt20, align 8, !tbaa !230
  %54 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi21 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %54, i32 0, i32 0
  %55 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi21, align 4, !tbaa !36
  %ext_refs = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %55, i32 0, i32 44
  %56 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_refs22 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %56, i32 0, i32 16
  %57 = bitcast %struct.EXTERNAL_REFERENCES* %ext_refs to i8*
  %58 = bitcast %struct.EXTERNAL_REFERENCES* %ext_refs22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 8 %58, i32 18948, i1 false), !tbaa.struct !237
  %59 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %is_annexb23 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %59, i32 0, i32 17
  %60 = load i32, i32* %is_annexb23, align 4, !tbaa !89
  %61 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi24 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %61, i32 0, i32 0
  %62 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi24, align 4, !tbaa !36
  %is_annexb25 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %62, i32 0, i32 54
  store i32 %60, i32* %is_annexb25, align 8, !tbaa !227
  %63 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %had_error = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %63, i32 0, i32 6
  store i32 0, i32* %had_error, align 4, !tbaa !238
  %64 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface, align 4, !tbaa !2
  %execute = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %64, i32 0, i32 4
  %65 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %execute, align 4, !tbaa !239
  %66 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  call void %65(%struct.AVxWorker* %66)
  %67 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %data_end = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %67, i32 0, i32 2
  %68 = load i8*, i8** %data_end, align 4, !tbaa !240
  %69 = load i8**, i8*** %data.addr, align 4, !tbaa !2
  store i8* %68, i8** %69, align 4, !tbaa !2
  %70 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %had_error26 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %70, i32 0, i32 6
  %71 = load i32, i32* %had_error26, align 4, !tbaa !238
  %tobool27 = icmp ne i32 %71, 0
  br i1 %tobool27, label %if.then28, label %if.end32

if.then28:                                        ; preds = %if.end12
  %72 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %73 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi29 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %73, i32 0, i32 0
  %74 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi29, align 4, !tbaa !36
  %common30 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %74, i32 0, i32 1
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %common30, i32 0, i32 1
  %call31 = call i32 @update_error_state(%struct.aom_codec_alg_priv* %72, %struct.aom_internal_error_info* %error)
  store i32 %call31, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

if.end32:                                         ; preds = %if.end12
  %75 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %76 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi33 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %76, i32 0, i32 0
  %77 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi33, align 4, !tbaa !36
  call void @check_resync(%struct.aom_codec_alg_priv* %75, %struct.AV1Decoder* %77)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

cleanup34:                                        ; preds = %if.end32, %if.then28
  %78 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  %79 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #5
  br label %cleanup36

cleanup36:                                        ; preds = %cleanup34, %cleanup
  %80 = bitcast %struct.AVxWorkerInterface** %winterface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #5
  %81 = load i32, i32* %retval, align 4
  ret i32 %81
}

declare i8* @aom_malloc(i32) #2

; Function Attrs: nounwind
define internal void @set_error_detail(%struct.aom_codec_alg_priv* %ctx, i8* %error) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %error.addr = alloca i8*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store i8* %error, i8** %error.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %error.addr, align 4, !tbaa !2
  %1 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %base = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %1, i32 0, i32 0
  %err_detail = getelementptr inbounds %struct.aom_codec_priv, %struct.aom_codec_priv* %base, i32 0, i32 0
  store i8* %0, i8** %err_detail, align 8, !tbaa !241
  ret void
}

declare i8* @aom_memalign(i32, i32) #2

declare %struct.AV1Decoder* @av1_decoder_create(%struct.BufferPool*) #2

; Function Attrs: nounwind
define internal i32 @frame_worker_hook(i8* %arg1, i8* %arg2) #0 {
entry:
  %arg1.addr = alloca i8*, align 4
  %arg2.addr = alloca i8*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %data = alloca i8*, align 4
  %result = alloca i32, align 4
  store i8* %arg1, i8** %arg1.addr, align 4, !tbaa !2
  store i8* %arg2, i8** %arg2.addr, align 4, !tbaa !2
  %0 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %arg1.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %2, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %3 = bitcast i8** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %4, i32 0, i32 1
  %5 = load i8*, i8** %data1, align 4, !tbaa !235
  store i8* %5, i8** %data, align 4, !tbaa !2
  %6 = load i8*, i8** %arg2.addr, align 4, !tbaa !2
  %7 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %8, i32 0, i32 0
  %9 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !36
  %10 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %data_size = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %10, i32 0, i32 3
  %11 = load i32, i32* %data_size, align 4, !tbaa !236
  %call = call i32 @av1_receive_compressed_data(%struct.AV1Decoder* %9, i32 %11, i8** %data)
  store i32 %call, i32* %result, align 4, !tbaa !25
  %12 = load i8*, i8** %data, align 4, !tbaa !2
  %13 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %data_end = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %13, i32 0, i32 2
  store i8* %12, i8** %data_end, align 4, !tbaa !240
  %14 = load i32, i32* %result, align 4, !tbaa !25
  %cmp = icmp ne i32 %14, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %15 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi2 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %15, i32 0, i32 0
  %16 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi2, align 4, !tbaa !36
  %need_resync = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %16, i32 0, i32 21
  store i32 1, i32* %need_resync, align 4, !tbaa !242
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %17 = load i32, i32* %result, align 4, !tbaa !25
  %tobool = icmp ne i32 %17, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %18 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #5
  %19 = bitcast i8** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #5
  %20 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #5
  ret i32 %lnot.ext
}

; Function Attrs: nounwind
define internal void @init_buffer_callbacks(%struct.aom_codec_alg_priv* %ctx) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %worker = alloca %struct.AVxWorker*, align 4
  %frame_worker_data = alloca %struct.FrameWorkerData*, align 4
  %pbi = alloca %struct.AV1Decoder*, align 4
  %cm = alloca %struct.AV1Common*, align 4
  %pool = alloca %struct.BufferPool*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %0 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %frame_worker = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %1, i32 0, i32 20
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %frame_worker, align 8, !tbaa !31
  store %struct.AVxWorker* %2, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %3 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %worker, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %4, i32 0, i32 4
  %5 = load i8*, i8** %data1, align 4, !tbaa !32
  %6 = bitcast i8* %5 to %struct.FrameWorkerData*
  store %struct.FrameWorkerData* %6, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %7 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load %struct.FrameWorkerData*, %struct.FrameWorkerData** %frame_worker_data, align 4, !tbaa !2
  %pbi1 = getelementptr inbounds %struct.FrameWorkerData, %struct.FrameWorkerData* %8, i32 0, i32 0
  %9 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi1, align 4, !tbaa !36
  store %struct.AV1Decoder* %9, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %10 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %11, i32 0, i32 1
  store %struct.AV1Common* %common, %struct.AV1Common** %cm, align 4, !tbaa !2
  %12 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %13, i32 0, i32 41
  %14 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !243
  store %struct.BufferPool* %14, %struct.BufferPool** %pool, align 4, !tbaa !2
  %15 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %cur_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %15, i32 0, i32 13
  store %struct.RefCntBuffer* null, %struct.RefCntBuffer** %cur_frame, align 8, !tbaa !244
  %16 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %byte_alignment = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %16, i32 0, i32 8
  %17 = load i32, i32* %byte_alignment, align 8, !tbaa !130
  %18 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %features = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %18, i32 0, i32 21
  %byte_alignment2 = getelementptr inbounds %struct.FeatureFlags, %struct.FeatureFlags* %features, i32 0, i32 15
  store i32 %17, i32* %byte_alignment2, align 4, !tbaa !245
  %19 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %skip_loop_filter = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %19, i32 0, i32 9
  %20 = load i32, i32* %skip_loop_filter, align 4, !tbaa !132
  %21 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %skip_loop_filter3 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %21, i32 0, i32 52
  store i32 %20, i32* %skip_loop_filter3, align 16, !tbaa !133
  %22 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %skip_film_grain = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %22, i32 0, i32 10
  %23 = load i32, i32* %skip_film_grain, align 8, !tbaa !141
  %24 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi, align 4, !tbaa !2
  %skip_film_grain4 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %24, i32 0, i32 53
  store i32 %23, i32* %skip_film_grain4, align 4, !tbaa !121
  %25 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %get_ext_fb_cb = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %25, i32 0, i32 27
  %26 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %get_ext_fb_cb, align 4, !tbaa !123
  %cmp = icmp ne i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %26, null
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %27 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %release_ext_fb_cb = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %27, i32 0, i32 28
  %28 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_ext_fb_cb, align 8, !tbaa !124
  %cmp5 = icmp ne i32 (i8*, %struct.aom_codec_frame_buffer*)* %28, null
  br i1 %cmp5, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %29 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %get_ext_fb_cb6 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %29, i32 0, i32 27
  %30 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %get_ext_fb_cb6, align 4, !tbaa !123
  %31 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %get_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %31, i32 0, i32 1
  store i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %30, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %get_fb_cb, align 4, !tbaa !246
  %32 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %release_ext_fb_cb7 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %32, i32 0, i32 28
  %33 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_ext_fb_cb7, align 8, !tbaa !124
  %34 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %release_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %34, i32 0, i32 2
  store i32 (i8*, %struct.aom_codec_frame_buffer*)* %33, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !80
  %35 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %ext_priv = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %35, i32 0, i32 26
  %36 = load i8*, i8** %ext_priv, align 8, !tbaa !125
  %37 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %cb_priv = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %37, i32 0, i32 0
  store i8* %36, i8** %cb_priv, align 4, !tbaa !83
  br label %if.end13

if.else:                                          ; preds = %land.lhs.true, %entry
  %38 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %get_fb_cb8 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %38, i32 0, i32 1
  store i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* @av1_get_frame_buffer, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %get_fb_cb8, align 4, !tbaa !246
  %39 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %release_fb_cb9 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %39, i32 0, i32 2
  store i32 (i8*, %struct.aom_codec_frame_buffer*)* @av1_release_frame_buffer, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb9, align 4, !tbaa !80
  %40 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %int_frame_buffers = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %40, i32 0, i32 4
  %call = call i32 @av1_alloc_internal_frame_buffers(%struct.InternalFrameBufferList* %int_frame_buffers)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.else
  %41 = load %struct.AV1Common*, %struct.AV1Common** %cm, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %41, i32 0, i32 1
  call void (%struct.aom_internal_error_info*, i32, i8*, ...) @aom_internal_error(%struct.aom_internal_error_info* %error, i32 2, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.4, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.else
  %42 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %int_frame_buffers11 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %42, i32 0, i32 4
  %43 = bitcast %struct.InternalFrameBufferList* %int_frame_buffers11 to i8*
  %44 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %cb_priv12 = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %44, i32 0, i32 0
  store i8* %43, i8** %cb_priv12, align 4, !tbaa !83
  br label %if.end13

if.end13:                                         ; preds = %if.end, %if.then
  %45 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #5
  %46 = bitcast %struct.AV1Common** %cm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  %47 = bitcast %struct.AV1Decoder** %pbi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #5
  %48 = bitcast %struct.FrameWorkerData** %frame_worker_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  %49 = bitcast %struct.AVxWorker** %worker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #5
  ret void
}

declare i32 @av1_receive_compressed_data(%struct.AV1Decoder*, i32, i8**) #2

declare i32 @av1_get_frame_buffer(i8*, i32, %struct.aom_codec_frame_buffer*) #2

declare i32 @av1_release_frame_buffer(i8*, %struct.aom_codec_frame_buffer*) #2

declare i32 @av1_alloc_internal_frame_buffers(%struct.InternalFrameBufferList*) #2

declare void @aom_internal_error(%struct.aom_internal_error_info*, i32, i8*, ...) #2

; Function Attrs: nounwind
define internal i32 @update_error_state(%struct.aom_codec_alg_priv* %ctx, %struct.aom_internal_error_info* %error) #0 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %error.addr = alloca %struct.aom_internal_error_info*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_internal_error_info* %error, %struct.aom_internal_error_info** %error.addr, align 4, !tbaa !2
  %0 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error.addr, align 4, !tbaa !2
  %error_code = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %0, i32 0, i32 0
  %1 = load i32, i32* %error_code, align 4, !tbaa !247
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %3 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error.addr, align 4, !tbaa !2
  %has_detail = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %3, i32 0, i32 1
  %4 = load i32, i32* %has_detail, align 4, !tbaa !248
  %tobool1 = icmp ne i32 %4, 0
  br i1 %tobool1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %5 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error.addr, align 4, !tbaa !2
  %detail = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %5, i32 0, i32 2
  %arraydecay = getelementptr inbounds [80 x i8], [80 x i8]* %detail, i32 0, i32 0
  br label %cond.end

cond.false:                                       ; preds = %if.then
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %arraydecay, %cond.true ], [ null, %cond.false ]
  call void @set_error_detail(%struct.aom_codec_alg_priv* %2, i8* %cond)
  br label %if.end

if.end:                                           ; preds = %cond.end, %entry
  %6 = load %struct.aom_internal_error_info*, %struct.aom_internal_error_info** %error.addr, align 4, !tbaa !2
  %error_code2 = getelementptr inbounds %struct.aom_internal_error_info, %struct.aom_internal_error_info* %6, i32 0, i32 0
  %7 = load i32, i32* %error_code2, align 4, !tbaa !247
  ret i32 %7
}

; Function Attrs: inlinehint nounwind
define internal void @check_resync(%struct.aom_codec_alg_priv* %ctx, %struct.AV1Decoder* %pbi) #3 {
entry:
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %0 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %need_resync = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %0, i32 0, i32 24
  %1 = load i32, i32* %need_resync, align 8, !tbaa !95
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %need_resync1 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %2, i32 0, i32 21
  %3 = load i32, i32* %need_resync1, align 4, !tbaa !242
  %cmp2 = icmp eq i32 %3, 0
  br i1 %cmp2, label %land.lhs.true3, label %if.end

land.lhs.true3:                                   ; preds = %land.lhs.true
  %4 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %common = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %4, i32 0, i32 1
  %call = call i32 @frame_is_intra_only(%struct.AV1Common* %common)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true3
  %5 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %need_resync4 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %5, i32 0, i32 24
  store i32 0, i32* %need_resync4, align 8, !tbaa !95
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true3, %land.lhs.true, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @frame_is_intra_only(%struct.AV1Common* %cm) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 0
  %frame_type = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame, i32 0, i32 0
  %1 = load i8, i8* %frame_type, align 16, !tbaa !249
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %2 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %current_frame2 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %2, i32 0, i32 0
  %frame_type3 = getelementptr inbounds %struct.CurrentFrame, %struct.CurrentFrame* %current_frame2, i32 0, i32 0
  %3 = load i8, i8* %frame_type3, align 16, !tbaa !249
  %conv4 = zext i8 %3 to i32
  %cmp5 = icmp eq i32 %conv4, 2
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %4 = phi i1 [ true, %entry ], [ %cmp5, %lor.rhs ]
  %lor.ext = zext i1 %4 to i32
  ret i32 %lor.ext
}

declare i32 @av1_get_raw_frame(%struct.AV1Decoder*, i32, %struct.yv12_buffer_config**, %struct.aom_film_grain_t**) #2

declare void @aom_img_remove_metadata(%struct.aom_image*) #2

; Function Attrs: nounwind
define internal void @move_decoder_metadata_to_img(%struct.AV1Decoder* %pbi, %struct.aom_image* %img) #0 {
entry:
  %pbi.addr = alloca %struct.AV1Decoder*, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  store %struct.AV1Decoder* %pbi, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %0, i32 0, i32 50
  %1 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata, align 8, !tbaa !250
  %tobool = icmp ne %struct.aom_metadata_array* %1, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_image* %2, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %metadata2 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %3, i32 0, i32 50
  %4 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata2, align 8, !tbaa !250
  %5 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata3 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %5, i32 0, i32 26
  store %struct.aom_metadata_array* %4, %struct.aom_metadata_array** %metadata3, align 4, !tbaa !192
  %6 = load %struct.AV1Decoder*, %struct.AV1Decoder** %pbi.addr, align 4, !tbaa !2
  %metadata4 = getelementptr inbounds %struct.AV1Decoder, %struct.AV1Decoder* %6, i32 0, i32 50
  store %struct.aom_metadata_array* null, %struct.aom_metadata_array** %metadata4, align 8, !tbaa !250
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @av1_num_planes(%struct.AV1Common* %cm) #3 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %seq_params = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 37
  %monochrome = getelementptr inbounds %struct.SequenceHeader, %struct.SequenceHeader* %seq_params, i32 0, i32 27
  %1 = load i8, i8* %monochrome, align 1, !tbaa !251
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 3
  ret i32 %cond
}

; Function Attrs: nounwind
define internal %struct.aom_image* @add_grain_if_needed(%struct.aom_codec_alg_priv* %ctx, %struct.aom_image* %img, %struct.aom_image* %grain_img, %struct.aom_film_grain_t* %grain_params) #0 {
entry:
  %retval = alloca %struct.aom_image*, align 4
  %ctx.addr = alloca %struct.aom_codec_alg_priv*, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  %grain_img.addr = alloca %struct.aom_image*, align 4
  %grain_params.addr = alloca %struct.aom_film_grain_t*, align 4
  %w_even = alloca i32, align 4
  %h_even = alloca i32, align 4
  %pool = alloca %struct.BufferPool*, align 4
  %fb = alloca %struct.aom_codec_frame_buffer*, align 4
  %param = alloca %struct.AllocCbParam, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_codec_alg_priv* %ctx, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store %struct.aom_image* %grain_img, %struct.aom_image** %grain_img.addr, align 4, !tbaa !2
  store %struct.aom_film_grain_t* %grain_params, %struct.aom_film_grain_t** %grain_params.addr, align 4, !tbaa !2
  %0 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain_params.addr, align 4, !tbaa !2
  %apply_grain = getelementptr inbounds %struct.aom_film_grain_t, %struct.aom_film_grain_t* %0, i32 0, i32 0
  %1 = load i32, i32* %apply_grain, align 4, !tbaa !122
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %2 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store %struct.aom_image* %2, %struct.aom_image** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %w_even to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %4, i32 0, i32 10
  %5 = load i32, i32* %d_w, align 4, !tbaa !165
  %add = add i32 %5, 1
  %and = and i32 %add, -2
  store i32 %and, i32* %w_even, align 4, !tbaa !25
  %6 = bitcast i32* %h_even to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %7, i32 0, i32 11
  %8 = load i32, i32* %d_h, align 4, !tbaa !166
  %add1 = add i32 %8, 1
  %and2 = and i32 %add1, -2
  store i32 %and2, i32* %h_even, align 4, !tbaa !25
  %9 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %buffer_pool = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %10, i32 0, i32 25
  %11 = load %struct.BufferPool*, %struct.BufferPool** %buffer_pool, align 4, !tbaa !78
  store %struct.BufferPool* %11, %struct.BufferPool** %pool, align 4, !tbaa !2
  %12 = bitcast %struct.aom_codec_frame_buffer** %fb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %grain_image_frame_buffers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %13, i32 0, i32 22
  %14 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %num_grain_image_frame_buffers = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %14, i32 0, i32 23
  %15 = load i32, i32* %num_grain_image_frame_buffers, align 4, !tbaa !26
  %arrayidx = getelementptr inbounds [4 x %struct.aom_codec_frame_buffer], [4 x %struct.aom_codec_frame_buffer]* %grain_image_frame_buffers, i32 0, i32 %15
  store %struct.aom_codec_frame_buffer* %arrayidx, %struct.aom_codec_frame_buffer** %fb, align 4, !tbaa !2
  %16 = bitcast %struct.AllocCbParam* %param to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %16) #5
  %17 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %pool3 = getelementptr inbounds %struct.AllocCbParam, %struct.AllocCbParam* %param, i32 0, i32 0
  store %struct.BufferPool* %17, %struct.BufferPool** %pool3, align 4, !tbaa !252
  %18 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb, align 4, !tbaa !2
  %fb4 = getelementptr inbounds %struct.AllocCbParam, %struct.AllocCbParam* %param, i32 0, i32 1
  store %struct.aom_codec_frame_buffer* %18, %struct.aom_codec_frame_buffer** %fb4, align 4, !tbaa !254
  %19 = load %struct.aom_image*, %struct.aom_image** %grain_img.addr, align 4, !tbaa !2
  %20 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt = getelementptr inbounds %struct.aom_image, %struct.aom_image* %20, i32 0, i32 0
  %21 = load i32, i32* %fmt, align 4, !tbaa !187
  %22 = load i32, i32* %w_even, align 4, !tbaa !25
  %23 = load i32, i32* %h_even, align 4, !tbaa !25
  %24 = bitcast %struct.AllocCbParam* %param to i8*
  %call = call %struct.aom_image* @aom_img_alloc_with_cb(%struct.aom_image* %19, i32 %21, i32 %22, i32 %23, i32 16, i8* (i8*, i32)* @AllocWithGetFrameBufferCb, i8* %24)
  %tobool5 = icmp ne %struct.aom_image* %call, null
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %if.end
  store %struct.aom_image* null, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  %25 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %user_priv = getelementptr inbounds %struct.aom_image, %struct.aom_image* %25, i32 0, i32 22
  %26 = load i8*, i8** %user_priv, align 4, !tbaa !197
  %27 = load %struct.aom_image*, %struct.aom_image** %grain_img.addr, align 4, !tbaa !2
  %user_priv8 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %27, i32 0, i32 22
  store i8* %26, i8** %user_priv8, align 4, !tbaa !197
  %28 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb, align 4, !tbaa !2
  %priv = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %28, i32 0, i32 2
  %29 = load i8*, i8** %priv, align 4, !tbaa !88
  %30 = load %struct.aom_image*, %struct.aom_image** %grain_img.addr, align 4, !tbaa !2
  %fb_priv = getelementptr inbounds %struct.aom_image, %struct.aom_image* %30, i32 0, i32 27
  store i8* %29, i8** %fb_priv, align 4, !tbaa !255
  %31 = load %struct.aom_film_grain_t*, %struct.aom_film_grain_t** %grain_params.addr, align 4, !tbaa !2
  %32 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %33 = load %struct.aom_image*, %struct.aom_image** %grain_img.addr, align 4, !tbaa !2
  %call9 = call i32 @av1_add_film_grain(%struct.aom_film_grain_t* %31, %struct.aom_image* %32, %struct.aom_image* %33)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end7
  %34 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %release_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %34, i32 0, i32 2
  %35 = load i32 (i8*, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)** %release_fb_cb, align 4, !tbaa !80
  %36 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !2
  %cb_priv = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %36, i32 0, i32 0
  %37 = load i8*, i8** %cb_priv, align 4, !tbaa !83
  %38 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb, align 4, !tbaa !2
  %call12 = call i32 %35(i8* %37, %struct.aom_codec_frame_buffer* %38)
  store %struct.aom_image* null, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.end7
  %39 = load %struct.aom_codec_alg_priv*, %struct.aom_codec_alg_priv** %ctx.addr, align 4, !tbaa !2
  %num_grain_image_frame_buffers14 = getelementptr inbounds %struct.aom_codec_alg_priv, %struct.aom_codec_alg_priv* %39, i32 0, i32 23
  %40 = load i32, i32* %num_grain_image_frame_buffers14, align 4, !tbaa !26
  %inc = add i32 %40, 1
  store i32 %inc, i32* %num_grain_image_frame_buffers14, align 4, !tbaa !26
  %41 = load %struct.aom_image*, %struct.aom_image** %grain_img.addr, align 4, !tbaa !2
  store %struct.aom_image* %41, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end13, %if.then11, %if.then6
  %42 = bitcast %struct.AllocCbParam* %param to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %42) #5
  %43 = bitcast %struct.aom_codec_frame_buffer** %fb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #5
  %44 = bitcast %struct.BufferPool** %pool to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #5
  %45 = bitcast i32* %h_even to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #5
  %46 = bitcast i32* %w_even to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #5
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %47 = load %struct.aom_image*, %struct.aom_image** %retval, align 4
  ret %struct.aom_image* %47
}

declare %struct.aom_image* @aom_img_alloc_with_cb(%struct.aom_image*, i32, i32, i32, i32, i8* (i8*, i32)*, i8*) #2

; Function Attrs: nounwind
define internal i8* @AllocWithGetFrameBufferCb(i8* %priv, i32 %size) #0 {
entry:
  %retval = alloca i8*, align 4
  %priv.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %param = alloca %struct.AllocCbParam*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %priv, i8** %priv.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !79
  %0 = bitcast %struct.AllocCbParam** %param to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8*, i8** %priv.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.AllocCbParam*
  store %struct.AllocCbParam* %2, %struct.AllocCbParam** %param, align 4, !tbaa !2
  %3 = load %struct.AllocCbParam*, %struct.AllocCbParam** %param, align 4, !tbaa !2
  %pool = getelementptr inbounds %struct.AllocCbParam, %struct.AllocCbParam* %3, i32 0, i32 0
  %4 = load %struct.BufferPool*, %struct.BufferPool** %pool, align 4, !tbaa !252
  %get_fb_cb = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %4, i32 0, i32 1
  %5 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %get_fb_cb, align 4, !tbaa !246
  %6 = load %struct.AllocCbParam*, %struct.AllocCbParam** %param, align 4, !tbaa !2
  %pool1 = getelementptr inbounds %struct.AllocCbParam, %struct.AllocCbParam* %6, i32 0, i32 0
  %7 = load %struct.BufferPool*, %struct.BufferPool** %pool1, align 4, !tbaa !252
  %cb_priv = getelementptr inbounds %struct.BufferPool, %struct.BufferPool* %7, i32 0, i32 0
  %8 = load i8*, i8** %cb_priv, align 4, !tbaa !83
  %9 = load i32, i32* %size.addr, align 4, !tbaa !79
  %10 = load %struct.AllocCbParam*, %struct.AllocCbParam** %param, align 4, !tbaa !2
  %fb = getelementptr inbounds %struct.AllocCbParam, %struct.AllocCbParam* %10, i32 0, i32 1
  %11 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb, align 4, !tbaa !254
  %call = call i32 %5(i8* %8, i32 %9, %struct.aom_codec_frame_buffer* %11)
  %cmp = icmp slt i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %12 = load %struct.AllocCbParam*, %struct.AllocCbParam** %param, align 4, !tbaa !2
  %fb2 = getelementptr inbounds %struct.AllocCbParam, %struct.AllocCbParam* %12, i32 0, i32 1
  %13 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb2, align 4, !tbaa !254
  %data = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %13, i32 0, i32 0
  %14 = load i8*, i8** %data, align 4, !tbaa !85
  %cmp3 = icmp eq i8* %14, null
  br i1 %cmp3, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %15 = load %struct.AllocCbParam*, %struct.AllocCbParam** %param, align 4, !tbaa !2
  %fb4 = getelementptr inbounds %struct.AllocCbParam, %struct.AllocCbParam* %15, i32 0, i32 1
  %16 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb4, align 4, !tbaa !254
  %size5 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %16, i32 0, i32 1
  %17 = load i32, i32* %size5, align 4, !tbaa !87
  %18 = load i32, i32* %size.addr, align 4, !tbaa !79
  %cmp6 = icmp ult i32 %17, %18
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %lor.lhs.false, %if.end
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %lor.lhs.false
  %19 = load %struct.AllocCbParam*, %struct.AllocCbParam** %param, align 4, !tbaa !2
  %fb9 = getelementptr inbounds %struct.AllocCbParam, %struct.AllocCbParam* %19, i32 0, i32 1
  %20 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb9, align 4, !tbaa !254
  %data10 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %20, i32 0, i32 0
  %21 = load i8*, i8** %data10, align 4, !tbaa !85
  store i8* %21, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then7, %if.then
  %22 = bitcast %struct.AllocCbParam** %param to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = load i8*, i8** %retval, align 4
  ret i8* %23
}

declare i32 @av1_add_film_grain(%struct.aom_film_grain_t*, %struct.aom_image*, %struct.aom_image*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 24}
!7 = !{!"aom_codec_ctx", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 12, !8, i64 16, !4, i64 20, !3, i64 24}
!8 = !{!"long", !4, i64 0}
!9 = !{!7, !8, i64 16}
!10 = !{!11, !8, i64 4}
!11 = !{!"aom_codec_priv", !3, i64 0, !8, i64 4, !12, i64 8}
!12 = !{!"", !13, i64 0, !14, i64 8, !14, i64 12, !15, i64 16}
!13 = !{!"aom_fixed_buf", !3, i64 0, !8, i64 4}
!14 = !{!"int", !4, i64 0}
!15 = !{!"aom_codec_cx_pkt", !4, i64 0, !4, i64 8}
!16 = !{!17, !14, i64 332}
!17 = !{!"aom_codec_alg_priv", !11, i64 0, !18, i64 160, !19, i64 176, !20, i64 200, !14, i64 328, !14, i64 332, !14, i64 336, !3, i64 340, !14, i64 344, !14, i64 348, !14, i64 352, !14, i64 356, !14, i64 360, !14, i64 364, !14, i64 368, !14, i64 372, !21, i64 376, !14, i64 19324, !14, i64 19328, !14, i64 19332, !3, i64 19336, !20, i64 19340, !4, i64 19468, !8, i64 19516, !14, i64 19520, !3, i64 19524, !3, i64 19528, !3, i64 19532, !3, i64 19536}
!18 = !{!"aom_codec_dec_cfg", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12}
!19 = !{!"aom_codec_stream_info", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !14, i64 20}
!20 = !{!"aom_image", !4, i64 0, !4, i64 4, !4, i64 8, !4, i64 12, !14, i64 16, !4, i64 20, !4, i64 24, !14, i64 28, !14, i64 32, !14, i64 36, !14, i64 40, !14, i64 44, !14, i64 48, !14, i64 52, !14, i64 56, !14, i64 60, !4, i64 64, !4, i64 76, !8, i64 88, !14, i64 92, !14, i64 96, !14, i64 100, !3, i64 104, !3, i64 108, !14, i64 112, !14, i64 116, !3, i64 120, !3, i64 124}
!21 = !{!"EXTERNAL_REFERENCES", !4, i64 0, !14, i64 18944}
!22 = !{!17, !14, i64 172}
!23 = !{!4, !4, i64 0}
!24 = !{i64 0, i64 4, !25, i64 4, i64 4, !25, i64 8, i64 4, !25, i64 12, i64 4, !25}
!25 = !{!14, !14, i64 0}
!26 = !{!17, !8, i64 19516}
!27 = !{!17, !14, i64 372}
!28 = !{!17, !14, i64 364}
!29 = !{!17, !14, i64 356}
!30 = !{!17, !14, i64 360}
!31 = !{!17, !3, i64 19336}
!32 = !{!33, !3, i64 16}
!33 = !{!"", !3, i64 0, !4, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !14, i64 24}
!34 = !{!35, !3, i64 20}
!35 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!36 = !{!37, !3, i64 0}
!37 = !{!"FrameWorkerData", !3, i64 0, !3, i64 4, !3, i64 8, !8, i64 12, !3, i64 16, !14, i64 20, !14, i64 24, !14, i64 28}
!38 = !{!39, !3, i64 62828}
!39 = !{!"AV1Decoder", !40, i64 0, !45, i64 44032, !33, i64 62896, !66, i64 62924, !67, i64 62964, !68, i64 63008, !3, i64 63188, !14, i64 63192, !3, i64 63196, !69, i64 63200, !3, i64 348960, !14, i64 348964, !4, i64 348968, !71, i64 381736, !14, i64 381756, !4, i64 381760, !8, i64 381776, !14, i64 381780, !14, i64 381784, !14, i64 381788, !14, i64 381792, !14, i64 381796, !14, i64 381800, !14, i64 381804, !14, i64 381808, !14, i64 381812, !14, i64 381816, !14, i64 381820, !72, i64 381824, !14, i64 384924, !14, i64 384928, !14, i64 384932, !14, i64 384936, !14, i64 384940, !14, i64 384944, !14, i64 384948, !8, i64 384952, !76, i64 384956, !14, i64 384964, !14, i64 384968, !14, i64 384972, !14, i64 384976, !14, i64 384980, !14, i64 384984, !21, i64 384988, !56, i64 403936, !3, i64 404084, !14, i64 404088, !14, i64 404092, !77, i64 404096, !3, i64 404136, !14, i64 404140, !14, i64 404144, !14, i64 404148, !14, i64 404152, !4, i64 404156}
!40 = !{!"macroblockd", !14, i64 0, !14, i64 4, !14, i64 8, !41, i64 12, !4, i64 16, !42, i64 4060, !3, i64 4084, !41, i64 4088, !41, i64 4089, !41, i64 4090, !41, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !14, i64 4112, !14, i64 4116, !14, i64 4120, !14, i64 4124, !14, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !14, i64 6836, !4, i64 6840, !4, i64 6872, !14, i64 6904, !14, i64 6908, !3, i64 6912, !3, i64 6916, !14, i64 6920, !14, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !43, i64 39720, !44, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!41 = !{!"_Bool", !4, i64 0}
!42 = !{!"TileInfo", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !14, i64 20}
!43 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !14, i64 4104, !4, i64 4108, !14, i64 4236, !14, i64 4240, !14, i64 4244, !14, i64 4248, !14, i64 4252, !14, i64 4256}
!44 = !{!"dist_wtd_comp_params", !14, i64 0, !14, i64 4, !14, i64 8}
!45 = !{!"AV1Common", !46, i64 0, !48, i64 40, !14, i64 288, !14, i64 292, !14, i64 296, !14, i64 300, !14, i64 304, !14, i64 308, !4, i64 312, !41, i64 313, !4, i64 316, !14, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !49, i64 492, !4, i64 580, !4, i64 1284, !14, i64 1316, !14, i64 1320, !14, i64 1324, !50, i64 1328, !51, i64 1356, !52, i64 1420, !53, i64 10676, !3, i64 10848, !54, i64 10864, !55, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !56, i64 14880, !57, i64 15028, !58, i64 15168, !60, i64 15816, !4, i64 15836, !61, i64 16192, !3, i64 18128, !3, i64 18132, !64, i64 18136, !3, i64 18724, !65, i64 18728, !14, i64 18760, !4, i64 18764, !3, i64 18796, !14, i64 18800, !4, i64 18804, !4, i64 18836, !14, i64 18844, !14, i64 18848, !14, i64 18852, !14, i64 18856}
!46 = !{!"", !4, i64 0, !4, i64 1, !14, i64 4, !14, i64 8, !14, i64 12, !47, i64 16, !14, i64 32, !14, i64 36}
!47 = !{!"", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12}
!48 = !{!"aom_internal_error_info", !4, i64 0, !14, i64 4, !4, i64 8, !14, i64 88, !4, i64 92}
!49 = !{!"scale_factors", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!50 = !{!"", !41, i64 0, !41, i64 1, !41, i64 2, !41, i64 3, !41, i64 4, !41, i64 5, !41, i64 6, !41, i64 7, !41, i64 8, !41, i64 9, !41, i64 10, !41, i64 11, !4, i64 12, !4, i64 13, !14, i64 16, !14, i64 20, !4, i64 24}
!51 = !{!"CommonModeInfoParams", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !3, i64 20, !14, i64 24, !14, i64 28, !4, i64 32, !3, i64 36, !14, i64 40, !14, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!52 = !{!"CommonQuantParams", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !14, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !41, i64 9240, !14, i64 9244, !14, i64 9248, !14, i64 9252}
!53 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !14, i64 164, !4, i64 168}
!54 = !{!"", !4, i64 0, !4, i64 3072}
!55 = !{!"loopfilter", !4, i64 0, !14, i64 8, !14, i64 12, !14, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !14, i64 32}
!56 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !14, i64 52, !4, i64 56, !3, i64 68, !14, i64 72, !3, i64 76, !8, i64 80, !14, i64 84, !8, i64 88, !14, i64 92, !14, i64 96, !14, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !14, i64 128, !14, i64 132, !14, i64 136, !14, i64 140, !3, i64 144}
!57 = !{!"", !14, i64 0, !14, i64 4, !4, i64 8, !4, i64 72, !14, i64 136}
!58 = !{!"", !14, i64 0, !14, i64 4, !4, i64 8, !14, i64 120, !4, i64 124, !14, i64 204, !4, i64 208, !14, i64 288, !14, i64 292, !14, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !14, i64 596, !14, i64 600, !14, i64 604, !14, i64 608, !14, i64 612, !14, i64 616, !14, i64 620, !14, i64 624, !14, i64 628, !14, i64 632, !14, i64 636, !14, i64 640, !59, i64 644}
!59 = !{!"short", !4, i64 0}
!60 = !{!"", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16}
!61 = !{!"SequenceHeader", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !4, i64 16, !14, i64 20, !14, i64 24, !4, i64 28, !14, i64 32, !14, i64 36, !47, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !14, i64 92, !14, i64 96, !14, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !14, i64 112, !4, i64 116, !14, i64 244, !62, i64 248, !4, i64 264, !63, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!62 = !{!"aom_timing", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12}
!63 = !{!"aom_dec_model_info", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12}
!64 = !{!"CommonTileParams", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !14, i64 20, !14, i64 24, !14, i64 28, !14, i64 32, !14, i64 36, !14, i64 40, !14, i64 44, !14, i64 48, !14, i64 52, !14, i64 56, !4, i64 60, !4, i64 320, !14, i64 580, !14, i64 584}
!65 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !14, i64 20, !14, i64 24, !14, i64 28}
!66 = !{!"AV1LfSyncData", !4, i64 0, !14, i64 12, !14, i64 16, !3, i64 20, !14, i64 24, !3, i64 28, !14, i64 32, !14, i64 36}
!67 = !{!"AV1LrSyncData", !4, i64 0, !14, i64 12, !14, i64 16, !14, i64 20, !14, i64 24, !3, i64 28, !3, i64 32, !14, i64 36, !14, i64 40}
!68 = !{!"AV1LrStruct", !3, i64 0, !4, i64 4, !3, i64 172, !3, i64 176}
!69 = !{!"ThreadData", !40, i64 0, !70, i64 44032, !3, i64 285696, !4, i64 285700, !14, i64 285708, !14, i64 285712, !3, i64 285716, !4, i64 285720, !3, i64 285728, !3, i64 285732, !3, i64 285736, !3, i64 285740, !3, i64 285744, !3, i64 285748}
!70 = !{!"", !4, i64 0, !4, i64 196608, !4, i64 208896}
!71 = !{!"AV1DecTileMTData", !3, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16}
!72 = !{!"Accounting", !73, i64 0, !14, i64 1044, !4, i64 1048, !75, i64 3090, !14, i64 3096}
!73 = !{!"", !3, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !74, i64 16}
!74 = !{!"", !4, i64 0, !14, i64 1024}
!75 = !{!"", !59, i64 0, !59, i64 2}
!76 = !{!"DataBuffer", !3, i64 0, !8, i64 4}
!77 = !{!"AV1DecRowMTInfo", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !14, i64 20, !14, i64 24, !14, i64 28, !14, i64 32, !14, i64 36}
!78 = !{!17, !3, i64 19524}
!79 = !{!8, !8, i64 0}
!80 = !{!81, !3, i64 8}
!81 = !{!"BufferPool", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !82, i64 363660}
!82 = !{!"InternalFrameBufferList", !14, i64 0, !3, i64 4}
!83 = !{!81, !3, i64 0}
!84 = !{!39, !8, i64 381776}
!85 = !{!86, !3, i64 0}
!86 = !{!"aom_codec_frame_buffer", !3, i64 0, !8, i64 4, !3, i64 8}
!87 = !{!86, !8, i64 4}
!88 = !{!86, !3, i64 8}
!89 = !{!17, !14, i64 19324}
!90 = !{!91, !91, i64 0}
!91 = !{!"long long", !4, i64 0}
!92 = !{!35, !3, i64 8}
!93 = !{!37, !14, i64 20}
!94 = !{!17, !3, i64 340}
!95 = !{!17, !14, i64 19520}
!96 = !{!37, !3, i64 16}
!97 = !{!39, !14, i64 384980}
!98 = !{!64, !14, i64 580}
!99 = !{!64, !14, i64 584}
!100 = !{!39, !14, i64 381812}
!101 = !{!64, !14, i64 4}
!102 = !{!17, !14, i64 260}
!103 = !{!45, !14, i64 1368}
!104 = !{!17, !14, i64 244}
!105 = !{!39, !14, i64 381816}
!106 = !{!64, !14, i64 0}
!107 = !{!17, !14, i64 256}
!108 = !{!17, !4, i64 200}
!109 = !{!45, !14, i64 1372}
!110 = !{!17, !14, i64 240}
!111 = !{!112, !3, i64 1280}
!112 = !{!"RefCntBuffer", !14, i64 0, !14, i64 4, !4, i64 8, !14, i64 36, !4, i64 40, !3, i64 68, !3, i64 72, !53, i64 76, !14, i64 248, !14, i64 252, !14, i64 256, !14, i64 260, !4, i64 264, !14, i64 616, !4, i64 620, !58, i64 624, !86, i64 1272, !56, i64 1284, !4, i64 1432, !4, i64 1436, !4, i64 1452, !4, i64 1460, !113, i64 1464}
!113 = !{!"frame_contexts", !4, i64 0, !4, i64 390, !4, i64 930, !4, i64 966, !4, i64 1014, !4, i64 1070, !4, i64 1134, !4, i64 1206, !4, i64 1286, !4, i64 1374, !4, i64 1470, !4, i64 1790, !4, i64 5990, !4, i64 8090, !4, i64 8126, !4, i64 8138, !4, i64 8174, !4, i64 8192, !4, i64 8336, !4, i64 8468, !4, i64 9216, !4, i64 9240, !4, i64 9372, !4, i64 9412, !4, i64 9588, !4, i64 9720, !4, i64 9832, !4, i64 9944, !4, i64 10574, !4, i64 11204, !4, i64 11330, !4, i64 11342, !4, i64 11372, !4, i64 11480, !4, i64 11510, !4, i64 11564, !4, i64 11618, !4, i64 11654, !4, i64 11780, !4, i64 11816, !4, i64 11852, !4, i64 11870, !4, i64 11888, !114, i64 11912, !114, i64 12198, !4, i64 12484, !115, i64 12490, !4, i64 12580, !4, i64 12712, !4, i64 12724, !4, i64 12732, !4, i64 12738, !4, i64 12744, !4, i64 12856, !4, i64 13636, !4, i64 14076, !4, i64 14204, !4, i64 14904, !4, i64 15032, !4, i64 15128, !4, i64 15138, !4, i64 15178, !4, i64 15188, !4, i64 20492, !4, i64 21036, !4, i64 21054, !14, i64 21260}
!114 = !{!"", !4, i64 0, !4, i64 10}
!115 = !{!"segmentation_probs", !4, i64 0, !4, i64 18, !4, i64 36}
!116 = !{!17, !3, i64 324}
!117 = !{!45, !14, i64 18848}
!118 = !{!20, !14, i64 96}
!119 = !{!45, !14, i64 18856}
!120 = !{!20, !14, i64 100}
!121 = !{!39, !14, i64 404148}
!122 = !{!58, !14, i64 0}
!123 = !{!17, !3, i64 19532}
!124 = !{!17, !3, i64 19536}
!125 = !{!17, !3, i64 19528}
!126 = !{!127, !14, i64 0}
!127 = !{!"av1_ref_frame", !14, i64 0, !14, i64 4, !20, i64 8}
!128 = !{!127, !14, i64 4}
!129 = !{!17, !14, i64 336}
!130 = !{!17, !14, i64 344}
!131 = !{!39, !14, i64 45380}
!132 = !{!17, !14, i64 348}
!133 = !{!39, !14, i64 404144}
!134 = !{!17, !14, i64 19328}
!135 = !{!17, !14, i64 19332}
!136 = !{!17, !14, i64 368}
!137 = !{!138, !14, i64 4}
!138 = !{!"av1_ext_ref_frame", !3, i64 0, !14, i64 4}
!139 = !{!17, !14, i64 19320}
!140 = !{!138, !3, i64 0}
!141 = !{!17, !14, i64 352}
!142 = !{!39, !14, i64 384940}
!143 = !{!112, !14, i64 1420}
!144 = !{!39, !14, i64 45452}
!145 = !{!39, !14, i64 44064}
!146 = !{!45, !4, i64 16264}
!147 = !{!45, !14, i64 16288}
!148 = !{!45, !14, i64 16292}
!149 = !{!45, !4, i64 16268}
!150 = !{!39, !14, i64 384972}
!151 = !{!45, !14, i64 296}
!152 = !{!45, !14, i64 300}
!153 = !{!45, !14, i64 288}
!154 = !{!45, !14, i64 292}
!155 = !{!39, !8, i64 384960}
!156 = !{!157, !8, i64 0}
!157 = !{!"aom_tile_data", !8, i64 0, !3, i64 4, !8, i64 8}
!158 = !{!39, !3, i64 384956}
!159 = !{!157, !3, i64 4}
!160 = !{!39, !8, i64 384952}
!161 = !{!157, !8, i64 8}
!162 = !{!163, !8, i64 4}
!163 = !{!"TileBufferDec", !3, i64 0, !8, i64 4}
!164 = !{!163, !3, i64 0}
!165 = !{!20, !14, i64 40}
!166 = !{!20, !14, i64 44}
!167 = !{!20, !14, i64 48}
!168 = !{!56, !14, i64 128}
!169 = !{!20, !14, i64 52}
!170 = !{!56, !14, i64 132}
!171 = !{!20, !14, i64 28}
!172 = !{!20, !14, i64 32}
!173 = !{!20, !14, i64 56}
!174 = !{!20, !14, i64 60}
!175 = !{!20, !4, i64 4}
!176 = !{!56, !4, i64 104}
!177 = !{!20, !4, i64 8}
!178 = !{!56, !4, i64 108}
!179 = !{!20, !4, i64 12}
!180 = !{!56, !4, i64 112}
!181 = !{!20, !14, i64 16}
!182 = !{!56, !4, i64 116}
!183 = !{!20, !4, i64 20}
!184 = !{!56, !4, i64 120}
!185 = !{!20, !4, i64 24}
!186 = !{!56, !4, i64 124}
!187 = !{!20, !4, i64 0}
!188 = !{!56, !14, i64 140}
!189 = !{!56, !14, i64 84}
!190 = !{!56, !14, i64 92}
!191 = !{!56, !14, i64 96}
!192 = !{!20, !3, i64 120}
!193 = !{!56, !3, i64 144}
!194 = !{!20, !14, i64 36}
!195 = !{!56, !14, i64 100}
!196 = !{!20, !14, i64 92}
!197 = !{!20, !3, i64 104}
!198 = !{!56, !3, i64 76}
!199 = !{!20, !3, i64 108}
!200 = !{!20, !14, i64 112}
!201 = !{!20, !14, i64 116}
!202 = !{!56, !8, i64 88}
!203 = !{!20, !8, i64 88}
!204 = !{!19, !14, i64 0}
!205 = !{!19, !14, i64 4}
!206 = !{!19, !14, i64 8}
!207 = !{!19, !14, i64 20}
!208 = !{!209, !4, i64 4}
!209 = !{!"", !8, i64 0, !4, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !14, i64 20}
!210 = !{!211, !3, i64 0}
!211 = !{!"aom_read_bit_buffer", !3, i64 0, !3, i64 4, !14, i64 8, !3, i64 12, !3, i64 16}
!212 = !{!211, !3, i64 4}
!213 = !{!211, !14, i64 8}
!214 = !{!211, !3, i64 12}
!215 = !{!211, !3, i64 16}
!216 = !{!112, !14, i64 0}
!217 = !{!112, !3, i64 1272}
!218 = !{!112, !8, i64 1276}
!219 = !{!35, !3, i64 0}
!220 = !{!33, !3, i64 8}
!221 = !{!37, !14, i64 24}
!222 = !{!39, !14, i64 381784}
!223 = !{!17, !14, i64 160}
!224 = !{!39, !14, i64 381788}
!225 = !{!39, !14, i64 381792}
!226 = !{!39, !14, i64 62748}
!227 = !{!39, !14, i64 404152}
!228 = !{!39, !14, i64 384932}
!229 = !{!39, !14, i64 381756}
!230 = !{!39, !14, i64 384984}
!231 = !{!33, !3, i64 12}
!232 = !{!17, !14, i64 180}
!233 = !{!17, !14, i64 196}
!234 = !{!17, !14, i64 184}
!235 = !{!37, !3, i64 4}
!236 = !{!37, !8, i64 12}
!237 = !{i64 0, i64 18944, !23, i64 18944, i64 4, !25}
!238 = !{!33, !14, i64 24}
!239 = !{!35, !3, i64 16}
!240 = !{!37, !3, i64 8}
!241 = !{!17, !3, i64 0}
!242 = !{!39, !14, i64 381796}
!243 = !{!45, !3, i64 18724}
!244 = !{!45, !3, i64 456}
!245 = !{!45, !14, i64 1348}
!246 = !{!81, !3, i64 4}
!247 = !{!48, !4, i64 0}
!248 = !{!48, !14, i64 4}
!249 = !{!45, !4, i64 0}
!250 = !{!39, !3, i64 404136}
!251 = !{!45, !4, i64 16269}
!252 = !{!253, !3, i64 0}
!253 = !{!"", !3, i64 0, !3, i64 4}
!254 = !{!253, !3, i64 4}
!255 = !{!20, !3, i64 124}
