; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/entropymv.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/entropymv.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.AV1Common = type { %struct.CurrentFrame, %struct.aom_internal_error_info, i32, i32, i32, i32, i32, i32, i8, i8, [33 x i32], i32, %struct.RefCntBuffer*, %struct.RefCntBuffer*, [8 x i32], %struct.scale_factors, [8 x %struct.scale_factors], [8 x %struct.RefCntBuffer*], i32, i32, i32, %struct.FeatureFlags, %struct.CommonModeInfoParams, %struct.CommonQuantParams, %struct.segmentation, i8*, [12 x i8], %struct.loop_filter_info_n, %struct.loopfilter, [3 x %struct.RestorationInfo], i32*, %struct.RestorationLineBuffers*, %struct.yv12_buffer_config, %struct.CdefInfo, %struct.aom_film_grain_t, %struct.DeltaQInfo, [8 x %struct.WarpedMotionParams], %struct.SequenceHeader, %struct.frame_contexts*, %struct.frame_contexts*, %struct.CommonTileParams, %struct.BufferPool*, %struct.CommonContexts, i32, [8 x i32], %struct.TPL_MV_REF*, i32, [8 x i32], [8 x i8], i32, i32, i32, i32 }
%struct.CurrentFrame = type { i8, i8, i32, i32, i32, %struct.SkipModeInfo, i32, i32 }
%struct.SkipModeInfo = type { i32, i32, i32, i32 }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.RefCntBuffer = type { i32, i32, [7 x i32], i32, [7 x i32], %struct.MV_REF*, i8*, %struct.segmentation, i32, i32, i32, i32, [8 x %struct.WarpedMotionParams], i32, i8, %struct.aom_film_grain_t, %struct.aom_codec_frame_buffer, %struct.yv12_buffer_config, i8, [4 x i32], [8 x i8], [2 x i8], %struct.frame_contexts }
%struct.MV_REF = type { %union.int_mv, i8 }
%union.int_mv = type { i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.FeatureFlags = type { i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i32, i8 }
%struct.CommonModeInfoParams = type { i32, i32, i32, i32, i32, %struct.MB_MODE_INFO*, i32, i32, i8, %struct.MB_MODE_INFO**, i32, i32, i8*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*)*, void (%struct.CommonModeInfoParams*, i32, i32)* }
%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.CommonQuantParams = type { i32, i32, i32, i32, i32, i32, [8 x [2 x i16]], [8 x [2 x i16]], [8 x [2 x i16]], [16 x [3 x [19 x i8*]]], [16 x [3 x [19 x i8*]]], [8 x [19 x i8*]], [8 x [19 x i8*]], [8 x [19 x i8*]], i8, i32, i32, i32 }
%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }
%struct.loop_filter_info_n = type { [64 x %struct.loop_filter_thresh], [3 x [8 x [2 x [8 x [2 x i8]]]]] }
%struct.loop_filter_thresh = type { [16 x i8], [16 x i8], [16 x i8] }
%struct.loopfilter = type { [2 x i32], i32, i32, i32, i8, i8, [8 x i8], [2 x i8], i32 }
%struct.RestorationInfo = type { i8, i32, i32, i32, i32, %struct.RestorationUnitInfo*, %struct.RestorationStripeBoundaries, i32 }
%struct.RestorationUnitInfo = type { i8, [15 x i8], %struct.WienerInfo, %struct.SgrprojInfo, [4 x i8] }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.RestorationStripeBoundaries = type { i8*, i8*, i32, i32 }
%struct.RestorationLineBuffers = type { [3 x [392 x i16]], [3 x [392 x i16]] }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.CdefInfo = type { i32, i32, [16 x i32], [16 x i32], i32 }
%struct.aom_film_grain_t = type { i32, i32, [14 x [2 x i32]], i32, [10 x [2 x i32]], i32, [10 x [2 x i32]], i32, i32, i32, [24 x i32], [25 x i32], [25 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i16 }
%struct.DeltaQInfo = type { i32, i32, i32, i32, i32 }
%struct.SequenceHeader = type { i32, i32, i32, i32, i8, i32, i32, i8, i32, i32, %struct.OrderHintInfo, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i8, i32, i8, i8, i32, i32, i32, i32, i32, i32, i32, i8, i8, i32, [32 x i32], i32, %struct.aom_timing, i8, %struct.aom_dec_model_info, i8, [32 x i8], [32 x i8], [33 x %struct.aom_dec_model_op_parameters] }
%struct.OrderHintInfo = type { i32, i32, i32, i32 }
%struct.aom_timing = type { i32, i32, i32, i32 }
%struct.aom_dec_model_info = type { i32, i32, i32, i32 }
%struct.aom_dec_model_op_parameters = type { i32, i64, i64, i32, i32, i32, i32, i32 }
%struct.CommonTileParams = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [65 x i32], [65 x i32], i32, i32 }
%struct.BufferPool = type { i8*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, %struct.aom_codec_frame_buffer*)*, [16 x %struct.RefCntBuffer], %struct.InternalFrameBufferList }
%struct.InternalFrameBufferList = type { i32, %struct.InternalFrameBuffer* }
%struct.InternalFrameBuffer = type { i8*, i32, i32 }
%struct.CommonContexts = type { i8**, [3 x i8**], i8**, i32, i32, i32 }
%struct.TPL_MV_REF = type { %union.int_mv, i8 }

@default_nmv_context = internal constant %struct.nmv_context { [5 x i16] [i16 28672, i16 21504, i16 13440, i16 0, i16 0], [2 x %struct.nmv_component] [%struct.nmv_component { [12 x i16] [i16 4096, i16 1792, i16 910, i16 448, i16 217, i16 112, i16 28, i16 11, i16 6, i16 1, i16 0, i16 0], [2 x [5 x i16]] [[5 x i16] [i16 16384, i16 8192, i16 6144, i16 0, i16 0], [5 x i16] [i16 20480, i16 11520, i16 8640, i16 0, i16 0]], [5 x i16] [i16 24576, i16 15360, i16 11520, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 12288, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 5120, i16 0, i16 0], [10 x [3 x i16]] [[3 x i16] [i16 15360, i16 0, i16 0], [3 x i16] [i16 14848, i16 0, i16 0], [3 x i16] [i16 13824, i16 0, i16 0], [3 x i16] [i16 12288, i16 0, i16 0], [3 x i16] [i16 10240, i16 0, i16 0], [3 x i16] [i16 8192, i16 0, i16 0], [3 x i16] [i16 4096, i16 0, i16 0], [3 x i16] [i16 2816, i16 0, i16 0], [3 x i16] [i16 2816, i16 0, i16 0], [3 x i16] [i16 2048, i16 0, i16 0]] }, %struct.nmv_component { [12 x i16] [i16 4096, i16 1792, i16 910, i16 448, i16 217, i16 112, i16 28, i16 11, i16 6, i16 1, i16 0, i16 0], [2 x [5 x i16]] [[5 x i16] [i16 16384, i16 8192, i16 6144, i16 0, i16 0], [5 x i16] [i16 20480, i16 11520, i16 8640, i16 0, i16 0]], [5 x i16] [i16 24576, i16 15360, i16 11520, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 12288, i16 0, i16 0], [3 x i16] [i16 16384, i16 0, i16 0], [3 x i16] [i16 5120, i16 0, i16 0], [10 x [3 x i16]] [[3 x i16] [i16 15360, i16 0, i16 0], [3 x i16] [i16 14848, i16 0, i16 0], [3 x i16] [i16 13824, i16 0, i16 0], [3 x i16] [i16 12288, i16 0, i16 0], [3 x i16] [i16 10240, i16 0, i16 0], [3 x i16] [i16 8192, i16 0, i16 0], [3 x i16] [i16 4096, i16 0, i16 0], [3 x i16] [i16 2816, i16 0, i16 0], [3 x i16] [i16 2816, i16 0, i16 0], [3 x i16] [i16 2048, i16 0, i16 0]] }] }, align 2

; Function Attrs: nounwind
define hidden void @av1_init_mv_probs(%struct.AV1Common* %cm) #0 {
entry:
  %cm.addr = alloca %struct.AV1Common*, align 4
  store %struct.AV1Common* %cm, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %0 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %fc = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %0, i32 0, i32 38
  %1 = load %struct.frame_contexts*, %struct.frame_contexts** %fc, align 16, !tbaa !6
  %nmvc = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %1, i32 0, i32 43
  %2 = bitcast %struct.nmv_context* %nmvc to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 2 bitcast (%struct.nmv_context* @default_nmv_context to i8*), i32 286, i1 false), !tbaa.struct !31
  %3 = load %struct.AV1Common*, %struct.AV1Common** %cm.addr, align 4, !tbaa !2
  %fc1 = getelementptr inbounds %struct.AV1Common, %struct.AV1Common* %3, i32 0, i32 38
  %4 = load %struct.frame_contexts*, %struct.frame_contexts** %fc1, align 16, !tbaa !6
  %ndvc = getelementptr inbounds %struct.frame_contexts, %struct.frame_contexts* %4, i32 0, i32 44
  %5 = bitcast %struct.nmv_context* %ndvc to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %5, i8* align 2 bitcast (%struct.nmv_context* @default_nmv_context to i8*), i32 286, i1 false), !tbaa.struct !31
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 18128}
!7 = !{!"AV1Common", !8, i64 0, !11, i64 40, !9, i64 288, !9, i64 292, !9, i64 296, !9, i64 300, !9, i64 304, !9, i64 308, !4, i64 312, !12, i64 313, !4, i64 316, !9, i64 448, !3, i64 452, !3, i64 456, !4, i64 460, !13, i64 492, !4, i64 580, !4, i64 1284, !9, i64 1316, !9, i64 1320, !9, i64 1324, !14, i64 1328, !15, i64 1356, !16, i64 1420, !17, i64 10676, !3, i64 10848, !18, i64 10864, !19, i64 14704, !4, i64 14740, !3, i64 14872, !3, i64 14876, !20, i64 14880, !22, i64 15028, !23, i64 15168, !25, i64 15816, !4, i64 15836, !26, i64 16192, !3, i64 18128, !3, i64 18132, !29, i64 18136, !3, i64 18724, !30, i64 18728, !9, i64 18760, !4, i64 18764, !3, i64 18796, !9, i64 18800, !4, i64 18804, !4, i64 18836, !9, i64 18844, !9, i64 18848, !9, i64 18852, !9, i64 18856}
!8 = !{!"", !4, i64 0, !4, i64 1, !9, i64 4, !9, i64 8, !9, i64 12, !10, i64 16, !9, i64 32, !9, i64 36}
!9 = !{!"int", !4, i64 0}
!10 = !{!"", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!11 = !{!"aom_internal_error_info", !4, i64 0, !9, i64 4, !4, i64 8, !9, i64 88, !4, i64 92}
!12 = !{!"_Bool", !4, i64 0}
!13 = !{!"scale_factors", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !3, i64 16, !3, i64 20, !4, i64 24, !4, i64 56}
!14 = !{!"", !12, i64 0, !12, i64 1, !12, i64 2, !12, i64 3, !12, i64 4, !12, i64 5, !12, i64 6, !12, i64 7, !12, i64 8, !12, i64 9, !12, i64 10, !12, i64 11, !4, i64 12, !4, i64 13, !9, i64 16, !9, i64 20, !4, i64 24}
!15 = !{!"CommonModeInfoParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !3, i64 20, !9, i64 24, !9, i64 28, !4, i64 32, !3, i64 36, !9, i64 40, !9, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60}
!16 = !{!"CommonQuantParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !4, i64 24, !4, i64 56, !4, i64 88, !4, i64 120, !4, i64 3768, !4, i64 7416, !4, i64 8024, !4, i64 8632, !12, i64 9240, !9, i64 9244, !9, i64 9248, !9, i64 9252}
!17 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !9, i64 164, !4, i64 168}
!18 = !{!"", !4, i64 0, !4, i64 3072}
!19 = !{!"loopfilter", !4, i64 0, !9, i64 8, !9, i64 12, !9, i64 16, !4, i64 20, !4, i64 21, !4, i64 22, !4, i64 30, !9, i64 32}
!20 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !9, i64 52, !4, i64 56, !3, i64 68, !9, i64 72, !3, i64 76, !21, i64 80, !9, i64 84, !21, i64 88, !9, i64 92, !9, i64 96, !9, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !9, i64 128, !9, i64 132, !9, i64 136, !9, i64 140, !3, i64 144}
!21 = !{!"long", !4, i64 0}
!22 = !{!"", !9, i64 0, !9, i64 4, !4, i64 8, !4, i64 72, !9, i64 136}
!23 = !{!"", !9, i64 0, !9, i64 4, !4, i64 8, !9, i64 120, !4, i64 124, !9, i64 204, !4, i64 208, !9, i64 288, !9, i64 292, !9, i64 296, !4, i64 300, !4, i64 396, !4, i64 496, !9, i64 596, !9, i64 600, !9, i64 604, !9, i64 608, !9, i64 612, !9, i64 616, !9, i64 620, !9, i64 624, !9, i64 628, !9, i64 632, !9, i64 636, !9, i64 640, !24, i64 644}
!24 = !{!"short", !4, i64 0}
!25 = !{!"", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16}
!26 = !{!"SequenceHeader", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !4, i64 16, !9, i64 20, !9, i64 24, !4, i64 28, !9, i64 32, !9, i64 36, !10, i64 40, !4, i64 56, !4, i64 57, !4, i64 58, !4, i64 59, !4, i64 60, !4, i64 61, !4, i64 62, !4, i64 63, !4, i64 64, !4, i64 65, !4, i64 66, !4, i64 67, !4, i64 68, !4, i64 69, !4, i64 72, !4, i64 76, !4, i64 77, !4, i64 80, !4, i64 84, !4, i64 88, !9, i64 92, !9, i64 96, !9, i64 100, !4, i64 104, !4, i64 108, !4, i64 109, !9, i64 112, !4, i64 116, !9, i64 244, !27, i64 248, !4, i64 264, !28, i64 268, !4, i64 284, !4, i64 285, !4, i64 317, !4, i64 352}
!27 = !{!"aom_timing", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!28 = !{!"aom_dec_model_info", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!29 = !{!"CommonTileParams", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36, !9, i64 40, !9, i64 44, !9, i64 48, !9, i64 52, !9, i64 56, !4, i64 60, !4, i64 320, !9, i64 580, !9, i64 584}
!30 = !{!"CommonContexts", !3, i64 0, !4, i64 4, !3, i64 16, !9, i64 20, !9, i64 24, !9, i64 28}
!31 = !{i64 0, i64 10, !32, i64 10, i64 276, !32}
!32 = !{!4, !4, i64 0}
