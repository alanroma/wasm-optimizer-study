; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_scale/generic/yv12config.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_scale/generic/yv12config.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.aom_codec_frame_buffer = type { i8*, i32, i8* }

; Function Attrs: nounwind
define hidden i32 @aom_free_frame_buffer(%struct.yv12_buffer_config* %ybf) #0 {
entry:
  %retval = alloca i32, align 4
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.yv12_buffer_config* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc_sz = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 11
  %2 = load i32, i32* %buffer_alloc_sz, align 4, !tbaa !6
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %3 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %3, i32 0, i32 10
  %4 = load i8*, i8** %buffer_alloc, align 4, !tbaa !10
  call void @aom_free(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %5, i32 0, i32 8
  %6 = load i8*, i8** %y_buffer_8bit, align 4, !tbaa !11
  %tobool2 = icmp ne i8* %6, null
  br i1 %tobool2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit4 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 8
  %8 = load i8*, i8** %y_buffer_8bit4, align 4, !tbaa !11
  call void @aom_free(i8* %8)
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  %9 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  call void @aom_remove_metadata_from_frame_buffer(%struct.yv12_buffer_config* %9)
  %10 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %11 = bitcast %struct.yv12_buffer_config* %10 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %11, i8 0, i32 148, i1 false)
  store i32 0, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %entry
  store i32 2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.end5
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

declare void @aom_free(i8*) #1

; Function Attrs: nounwind
define hidden void @aom_remove_metadata_from_frame_buffer(%struct.yv12_buffer_config* %ybf) #0 {
entry:
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.yv12_buffer_config* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %1, i32 0, i32 27
  %2 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata, align 4, !tbaa !12
  %tobool1 = icmp ne %struct.aom_metadata_array* %2, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata2 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %3, i32 0, i32 27
  %4 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata2, align 4, !tbaa !12
  call void @aom_img_metadata_array_free(%struct.aom_metadata_array* %4)
  %5 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata3 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %5, i32 0, i32 27
  store %struct.aom_metadata_array* null, %struct.aom_metadata_array** %metadata3, align 4, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: nounwind
define hidden i32 @aom_realloc_frame_buffer(%struct.yv12_buffer_config* %ybf, i32 %width, i32 %height, i32 %ss_x, i32 %ss_y, i32 %use_highbitdepth, i32 %border, i32 %byte_alignment, %struct.aom_codec_frame_buffer* %fb, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %cb, i8* %cb_priv) #0 {
entry:
  %retval = alloca i32, align 4
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %ss_x.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %use_highbitdepth.addr = alloca i32, align 4
  %border.addr = alloca i32, align 4
  %byte_alignment.addr = alloca i32, align 4
  %fb.addr = alloca %struct.aom_codec_frame_buffer*, align 4
  %cb.addr = alloca i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, align 4
  %cb_priv.addr = alloca i8*, align 4
  %y_stride = alloca i32, align 4
  %uv_stride = alloca i32, align 4
  %yplane_size = alloca i64, align 8
  %uvplane_size = alloca i64, align 8
  %aligned_width = alloca i32, align 4
  %aligned_height = alloca i32, align 4
  %uv_width = alloca i32, align 4
  %uv_height = alloca i32, align 4
  %uv_border_w = alloca i32, align 4
  %uv_border_h = alloca i32, align 4
  %error = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i32 %height, i32* %height.addr, align 4, !tbaa !13
  store i32 %ss_x, i32* %ss_x.addr, align 4, !tbaa !13
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !13
  store i32 %use_highbitdepth, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  store i32 %border, i32* %border.addr, align 4, !tbaa !13
  store i32 %byte_alignment, i32* %byte_alignment.addr, align 4, !tbaa !13
  store %struct.aom_codec_frame_buffer* %fb, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  store i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %cb, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb.addr, align 4, !tbaa !2
  store i8* %cb_priv, i8** %cb_priv.addr, align 4, !tbaa !2
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.yv12_buffer_config* %0, null
  br i1 %tobool, label %if.then, label %if.end19

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %y_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %y_stride, align 4, !tbaa !13
  %2 = bitcast i32* %uv_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %uv_stride, align 4, !tbaa !13
  %3 = bitcast i64* %yplane_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #4
  store i64 0, i64* %yplane_size, align 8, !tbaa !14
  %4 = bitcast i64* %uvplane_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  store i64 0, i64* %uvplane_size, align 8, !tbaa !14
  %5 = bitcast i32* %aligned_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %width.addr, align 4, !tbaa !13
  %add = add nsw i32 %6, 7
  %and = and i32 %add, -8
  store i32 %and, i32* %aligned_width, align 4, !tbaa !13
  %7 = bitcast i32* %aligned_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %height.addr, align 4, !tbaa !13
  %add1 = add nsw i32 %8, 7
  %and2 = and i32 %add1, -8
  store i32 %and2, i32* %aligned_height, align 4, !tbaa !13
  %9 = bitcast i32* %uv_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load i32, i32* %aligned_width, align 4, !tbaa !13
  %11 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %shr = ashr i32 %10, %11
  store i32 %shr, i32* %uv_width, align 4, !tbaa !13
  %12 = bitcast i32* %uv_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %aligned_height, align 4, !tbaa !13
  %14 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %shr3 = ashr i32 %13, %14
  store i32 %shr3, i32* %uv_height, align 4, !tbaa !13
  %15 = bitcast i32* %uv_border_w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i32, i32* %border.addr, align 4, !tbaa !13
  %17 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %shr4 = ashr i32 %16, %17
  store i32 %shr4, i32* %uv_border_w, align 4, !tbaa !13
  %18 = bitcast i32* %uv_border_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load i32, i32* %border.addr, align 4, !tbaa !13
  %20 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %shr5 = ashr i32 %19, %20
  store i32 %shr5, i32* %uv_border_h, align 4, !tbaa !13
  %21 = bitcast i32* %error to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %23 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %24 = load i32, i32* %aligned_width, align 4, !tbaa !13
  %25 = load i32, i32* %aligned_height, align 4, !tbaa !13
  %26 = load i32, i32* %border.addr, align 4, !tbaa !13
  %27 = load i32, i32* %byte_alignment.addr, align 4, !tbaa !13
  %28 = load i32, i32* %uv_height, align 4, !tbaa !13
  %call = call i32 @calc_stride_and_planesize(i32 %22, i32 %23, i32 %24, i32 %25, i32 %26, i32 %27, i32* %y_stride, i32* %uv_stride, i64* %yplane_size, i64* %uvplane_size, i32 %28)
  store i32 %call, i32* %error, align 4, !tbaa !13
  %29 = load i32, i32* %error, align 4, !tbaa !13
  %tobool6 = icmp ne i32 %29, 0
  br i1 %tobool6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %30 = load i32, i32* %error, align 4, !tbaa !13
  store i32 %30, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %31 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %32 = load i32, i32* %width.addr, align 4, !tbaa !13
  %33 = load i32, i32* %height.addr, align 4, !tbaa !13
  %34 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %35 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %36 = load i32, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  %37 = load i32, i32* %border.addr, align 4, !tbaa !13
  %38 = load i32, i32* %byte_alignment.addr, align 4, !tbaa !13
  %39 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %40 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb.addr, align 4, !tbaa !2
  %41 = load i8*, i8** %cb_priv.addr, align 4, !tbaa !2
  %42 = load i32, i32* %y_stride, align 4, !tbaa !13
  %43 = load i64, i64* %yplane_size, align 8, !tbaa !14
  %44 = load i64, i64* %uvplane_size, align 8, !tbaa !14
  %45 = load i32, i32* %aligned_width, align 4, !tbaa !13
  %46 = load i32, i32* %aligned_height, align 4, !tbaa !13
  %47 = load i32, i32* %uv_width, align 4, !tbaa !13
  %48 = load i32, i32* %uv_height, align 4, !tbaa !13
  %49 = load i32, i32* %uv_stride, align 4, !tbaa !13
  %50 = load i32, i32* %uv_border_w, align 4, !tbaa !13
  %51 = load i32, i32* %uv_border_h, align 4, !tbaa !13
  %call8 = call i32 @realloc_frame_buffer_aligned(%struct.yv12_buffer_config* %31, i32 %32, i32 %33, i32 %34, i32 %35, i32 %36, i32 %37, i32 %38, %struct.aom_codec_frame_buffer* %39, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %40, i8* %41, i32 %42, i64 %43, i64 %44, i32 %45, i32 %46, i32 %47, i32 %48, i32 %49, i32 %50, i32 %51)
  store i32 %call8, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then7
  %52 = bitcast i32* %error to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast i32* %uv_border_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i32* %uv_border_w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast i32* %uv_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  %56 = bitcast i32* %uv_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i32* %aligned_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast i32* %aligned_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i64* %uvplane_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %59) #4
  %60 = bitcast i64* %yplane_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %60) #4
  %61 = bitcast i32* %uv_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %62 = bitcast i32* %y_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  br label %return

if.end19:                                         ; preds = %entry
  store i32 2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end19, %cleanup
  %63 = load i32, i32* %retval, align 4
  ret i32 %63
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define internal i32 @calc_stride_and_planesize(i32 %ss_x, i32 %ss_y, i32 %aligned_width, i32 %aligned_height, i32 %border, i32 %byte_alignment, i32* %y_stride, i32* %uv_stride, i64* %yplane_size, i64* %uvplane_size, i32 %uv_height) #0 {
entry:
  %retval = alloca i32, align 4
  %ss_x.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %aligned_width.addr = alloca i32, align 4
  %aligned_height.addr = alloca i32, align 4
  %border.addr = alloca i32, align 4
  %byte_alignment.addr = alloca i32, align 4
  %y_stride.addr = alloca i32*, align 4
  %uv_stride.addr = alloca i32*, align 4
  %yplane_size.addr = alloca i64*, align 4
  %uvplane_size.addr = alloca i64*, align 4
  %uv_height.addr = alloca i32, align 4
  store i32 %ss_x, i32* %ss_x.addr, align 4, !tbaa !13
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !13
  store i32 %aligned_width, i32* %aligned_width.addr, align 4, !tbaa !13
  store i32 %aligned_height, i32* %aligned_height.addr, align 4, !tbaa !13
  store i32 %border, i32* %border.addr, align 4, !tbaa !13
  store i32 %byte_alignment, i32* %byte_alignment.addr, align 4, !tbaa !13
  store i32* %y_stride, i32** %y_stride.addr, align 4, !tbaa !2
  store i32* %uv_stride, i32** %uv_stride.addr, align 4, !tbaa !2
  store i64* %yplane_size, i64** %yplane_size.addr, align 4, !tbaa !2
  store i64* %uvplane_size, i64** %uvplane_size.addr, align 4, !tbaa !2
  store i32 %uv_height, i32* %uv_height.addr, align 4, !tbaa !13
  %0 = load i32, i32* %border.addr, align 4, !tbaa !13
  %and = and i32 %0, 31
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %aligned_width.addr, align 4, !tbaa !13
  %2 = load i32, i32* %border.addr, align 4, !tbaa !13
  %mul = mul nsw i32 2, %2
  %add = add nsw i32 %1, %mul
  %add1 = add nsw i32 %add, 31
  %and2 = and i32 %add1, -32
  %3 = load i32*, i32** %y_stride.addr, align 4, !tbaa !2
  store i32 %and2, i32* %3, align 4, !tbaa !13
  %4 = load i32, i32* %aligned_height.addr, align 4, !tbaa !13
  %5 = load i32, i32* %border.addr, align 4, !tbaa !13
  %mul3 = mul nsw i32 2, %5
  %add4 = add nsw i32 %4, %mul3
  %conv = sext i32 %add4 to i64
  %6 = load i32*, i32** %y_stride.addr, align 4, !tbaa !2
  %7 = load i32, i32* %6, align 4, !tbaa !13
  %conv5 = sext i32 %7 to i64
  %mul6 = mul i64 %conv, %conv5
  %8 = load i32, i32* %byte_alignment.addr, align 4, !tbaa !13
  %conv7 = sext i32 %8 to i64
  %add8 = add i64 %mul6, %conv7
  %9 = load i64*, i64** %yplane_size.addr, align 4, !tbaa !2
  store i64 %add8, i64* %9, align 8, !tbaa !14
  %10 = load i32*, i32** %y_stride.addr, align 4, !tbaa !2
  %11 = load i32, i32* %10, align 4, !tbaa !13
  %12 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %shr = ashr i32 %11, %12
  %13 = load i32*, i32** %uv_stride.addr, align 4, !tbaa !2
  store i32 %shr, i32* %13, align 4, !tbaa !13
  %14 = load i32, i32* %uv_height.addr, align 4, !tbaa !13
  %15 = load i32, i32* %border.addr, align 4, !tbaa !13
  %16 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %shr9 = ashr i32 %15, %16
  %mul10 = mul nsw i32 2, %shr9
  %add11 = add nsw i32 %14, %mul10
  %conv12 = sext i32 %add11 to i64
  %17 = load i32*, i32** %uv_stride.addr, align 4, !tbaa !2
  %18 = load i32, i32* %17, align 4, !tbaa !13
  %conv13 = sext i32 %18 to i64
  %mul14 = mul i64 %conv12, %conv13
  %19 = load i32, i32* %byte_alignment.addr, align 4, !tbaa !13
  %conv15 = sext i32 %19 to i64
  %add16 = add i64 %mul14, %conv15
  %20 = load i64*, i64** %uvplane_size.addr, align 4, !tbaa !2
  store i64 %add16, i64* %20, align 8, !tbaa !14
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define internal i32 @realloc_frame_buffer_aligned(%struct.yv12_buffer_config* %ybf, i32 %width, i32 %height, i32 %ss_x, i32 %ss_y, i32 %use_highbitdepth, i32 %border, i32 %byte_alignment, %struct.aom_codec_frame_buffer* %fb, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %cb, i8* %cb_priv, i32 %y_stride, i64 %yplane_size, i64 %uvplane_size, i32 %aligned_width, i32 %aligned_height, i32 %uv_width, i32 %uv_height, i32 %uv_stride, i32 %uv_border_w, i32 %uv_border_h) #0 {
entry:
  %retval = alloca i32, align 4
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %ss_x.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %use_highbitdepth.addr = alloca i32, align 4
  %border.addr = alloca i32, align 4
  %byte_alignment.addr = alloca i32, align 4
  %fb.addr = alloca %struct.aom_codec_frame_buffer*, align 4
  %cb.addr = alloca i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, align 4
  %cb_priv.addr = alloca i8*, align 4
  %y_stride.addr = alloca i32, align 4
  %yplane_size.addr = alloca i64, align 8
  %uvplane_size.addr = alloca i64, align 8
  %aligned_width.addr = alloca i32, align 4
  %aligned_height.addr = alloca i32, align 4
  %uv_width.addr = alloca i32, align 4
  %uv_height.addr = alloca i32, align 4
  %uv_stride.addr = alloca i32, align 4
  %uv_border_w.addr = alloca i32, align 4
  %uv_border_h.addr = alloca i32, align 4
  %aom_byte_align = alloca i32, align 4
  %frame_size = alloca i64, align 8
  %buf = alloca i8*, align 4
  %alloc_size = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %align_addr_extra_size = alloca i32, align 4
  %external_frame_size = alloca i64, align 8
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i32 %height, i32* %height.addr, align 4, !tbaa !13
  store i32 %ss_x, i32* %ss_x.addr, align 4, !tbaa !13
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !13
  store i32 %use_highbitdepth, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  store i32 %border, i32* %border.addr, align 4, !tbaa !13
  store i32 %byte_alignment, i32* %byte_alignment.addr, align 4, !tbaa !13
  store %struct.aom_codec_frame_buffer* %fb, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  store i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %cb, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb.addr, align 4, !tbaa !2
  store i8* %cb_priv, i8** %cb_priv.addr, align 4, !tbaa !2
  store i32 %y_stride, i32* %y_stride.addr, align 4, !tbaa !13
  store i64 %yplane_size, i64* %yplane_size.addr, align 8, !tbaa !14
  store i64 %uvplane_size, i64* %uvplane_size.addr, align 8, !tbaa !14
  store i32 %aligned_width, i32* %aligned_width.addr, align 4, !tbaa !13
  store i32 %aligned_height, i32* %aligned_height.addr, align 4, !tbaa !13
  store i32 %uv_width, i32* %uv_width.addr, align 4, !tbaa !13
  store i32 %uv_height, i32* %uv_height.addr, align 4, !tbaa !13
  store i32 %uv_stride, i32* %uv_stride.addr, align 4, !tbaa !13
  store i32 %uv_border_w, i32* %uv_border_w.addr, align 4, !tbaa !13
  store i32 %uv_border_h, i32* %uv_border_h.addr, align 4, !tbaa !13
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.yv12_buffer_config* %0, null
  br i1 %tobool, label %if.then, label %if.end130

if.then:                                          ; preds = %entry
  %1 = bitcast i32* %aom_byte_align to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %byte_alignment.addr, align 4, !tbaa !13
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %3 = load i32, i32* %byte_alignment.addr, align 4, !tbaa !13
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %3, %cond.false ]
  store i32 %cond, i32* %aom_byte_align, align 4, !tbaa !13
  %4 = bitcast i64* %frame_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  %5 = load i32, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  %add = add nsw i32 1, %5
  %conv = sext i32 %add to i64
  %6 = load i64, i64* %yplane_size.addr, align 8, !tbaa !14
  %7 = load i64, i64* %uvplane_size.addr, align 8, !tbaa !14
  %mul = mul i64 2, %7
  %add1 = add i64 %6, %mul
  %mul2 = mul i64 %conv, %add1
  store i64 %mul2, i64* %frame_size, align 8, !tbaa !14
  %8 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i8* null, i8** %buf, align 4, !tbaa !2
  %9 = bitcast i64* %alloc_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %9) #4
  %10 = load i64, i64* %frame_size, align 8, !tbaa !14
  store i64 %10, i64* %alloc_size, align 8, !tbaa !14
  %11 = load i32, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  %tobool3 = icmp ne i32 %11, 0
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %cond.end
  %12 = load i64, i64* %yplane_size.addr, align 8, !tbaa !14
  %13 = load i64, i64* %alloc_size, align 8, !tbaa !14
  %add5 = add i64 %13, %12
  store i64 %add5, i64* %alloc_size, align 8, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then4, %cond.end
  %14 = load i64, i64* %alloc_size, align 8, !tbaa !14
  %cmp6 = icmp ugt i64 %14, 268427264
  br i1 %cmp6, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end9:                                          ; preds = %if.end
  %15 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb.addr, align 4, !tbaa !2
  %cmp10 = icmp ne i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* %15, null
  br i1 %cmp10, label %if.then12, label %if.else

if.then12:                                        ; preds = %if.end9
  %16 = bitcast i32* %align_addr_extra_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  store i32 31, i32* %align_addr_extra_size, align 4, !tbaa !13
  %17 = bitcast i64* %external_frame_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %17) #4
  %18 = load i64, i64* %frame_size, align 8, !tbaa !14
  %add13 = add i64 %18, 31
  store i64 %add13, i64* %external_frame_size, align 8, !tbaa !14
  %19 = load i64, i64* %external_frame_size, align 8, !tbaa !14
  %20 = load i64, i64* %external_frame_size, align 8, !tbaa !14
  %conv14 = trunc i64 %20 to i32
  %conv15 = zext i32 %conv14 to i64
  %cmp16 = icmp ne i64 %19, %conv15
  br i1 %cmp16, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.then12
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.then12
  %21 = load i32 (i8*, i32, %struct.aom_codec_frame_buffer*)*, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)** %cb.addr, align 4, !tbaa !2
  %22 = load i8*, i8** %cb_priv.addr, align 4, !tbaa !2
  %23 = load i64, i64* %external_frame_size, align 8, !tbaa !14
  %conv20 = trunc i64 %23 to i32
  %24 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %call = call i32 %21(i8* %22, i32 %conv20, %struct.aom_codec_frame_buffer* %24)
  %cmp21 = icmp slt i32 %call, 0
  br i1 %cmp21, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end19
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end24:                                         ; preds = %if.end19
  %25 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %25, i32 0, i32 0
  %26 = load i8*, i8** %data, align 4, !tbaa !16
  %cmp25 = icmp eq i8* %26, null
  br i1 %cmp25, label %if.then30, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end24
  %27 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %27, i32 0, i32 1
  %28 = load i32, i32* %size, align 4, !tbaa !18
  %conv27 = zext i32 %28 to i64
  %29 = load i64, i64* %external_frame_size, align 8, !tbaa !14
  %cmp28 = icmp ult i64 %conv27, %29
  br i1 %cmp28, label %if.then30, label %if.end31

if.then30:                                        ; preds = %lor.lhs.false, %if.end24
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %lor.lhs.false
  %30 = load %struct.aom_codec_frame_buffer*, %struct.aom_codec_frame_buffer** %fb.addr, align 4, !tbaa !2
  %data32 = getelementptr inbounds %struct.aom_codec_frame_buffer, %struct.aom_codec_frame_buffer* %30, i32 0, i32 0
  %31 = load i8*, i8** %data32, align 4, !tbaa !16
  %32 = ptrtoint i8* %31 to i32
  %add33 = add i32 %32, 31
  %and = and i32 %add33, -32
  %33 = inttoptr i32 %and to i8*
  %34 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %34, i32 0, i32 10
  store i8* %33, i8** %buffer_alloc, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end31, %if.then30, %if.then23, %if.then18
  %35 = bitcast i64* %external_frame_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %35) #4
  %36 = bitcast i32* %align_addr_extra_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup126 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end60

if.else:                                          ; preds = %if.end9
  %37 = load i64, i64* %frame_size, align 8, !tbaa !14
  %38 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc_sz = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %38, i32 0, i32 11
  %39 = load i32, i32* %buffer_alloc_sz, align 4, !tbaa !6
  %conv35 = zext i32 %39 to i64
  %cmp36 = icmp ugt i64 %37, %conv35
  br i1 %cmp36, label %if.then38, label %if.end59

if.then38:                                        ; preds = %if.else
  %40 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc39 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %40, i32 0, i32 10
  %41 = load i8*, i8** %buffer_alloc39, align 4, !tbaa !10
  call void @aom_free(i8* %41)
  %42 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc40 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %42, i32 0, i32 10
  store i8* null, i8** %buffer_alloc40, align 4, !tbaa !10
  %43 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc_sz41 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %43, i32 0, i32 11
  store i32 0, i32* %buffer_alloc_sz41, align 4, !tbaa !6
  %44 = load i64, i64* %frame_size, align 8, !tbaa !14
  %45 = load i64, i64* %frame_size, align 8, !tbaa !14
  %conv42 = trunc i64 %45 to i32
  %conv43 = zext i32 %conv42 to i64
  %cmp44 = icmp ne i64 %44, %conv43
  br i1 %cmp44, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.then38
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end47:                                         ; preds = %if.then38
  %46 = load i64, i64* %frame_size, align 8, !tbaa !14
  %conv48 = trunc i64 %46 to i32
  %call49 = call i8* @aom_memalign(i32 32, i32 %conv48)
  %47 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc50 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %47, i32 0, i32 10
  store i8* %call49, i8** %buffer_alloc50, align 4, !tbaa !10
  %48 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc51 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %48, i32 0, i32 10
  %49 = load i8*, i8** %buffer_alloc51, align 4, !tbaa !10
  %tobool52 = icmp ne i8* %49, null
  br i1 %tobool52, label %if.end54, label %if.then53

if.then53:                                        ; preds = %if.end47
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end54:                                         ; preds = %if.end47
  %50 = load i64, i64* %frame_size, align 8, !tbaa !14
  %conv55 = trunc i64 %50 to i32
  %51 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc_sz56 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %51, i32 0, i32 11
  store i32 %conv55, i32* %buffer_alloc_sz56, align 4, !tbaa !6
  %52 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc57 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %52, i32 0, i32 10
  %53 = load i8*, i8** %buffer_alloc57, align 4, !tbaa !10
  %54 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc_sz58 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %54, i32 0, i32 11
  %55 = load i32, i32* %buffer_alloc_sz58, align 4, !tbaa !6
  call void @llvm.memset.p0i8.i32(i8* align 1 %53, i8 0, i32 %55, i1 false)
  br label %if.end59

if.end59:                                         ; preds = %if.end54, %if.else
  br label %if.end60

if.end60:                                         ; preds = %if.end59, %cleanup.cont
  %56 = load i32, i32* %width.addr, align 4, !tbaa !13
  %57 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %58 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %57, i32 0, i32 2
  %59 = bitcast %union.anon.2* %58 to %struct.anon.3*
  %y_crop_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %59, i32 0, i32 0
  store i32 %56, i32* %y_crop_width, align 4, !tbaa !19
  %60 = load i32, i32* %height.addr, align 4, !tbaa !13
  %61 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %62 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %61, i32 0, i32 3
  %63 = bitcast %union.anon.4* %62 to %struct.anon.5*
  %y_crop_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %63, i32 0, i32 0
  store i32 %60, i32* %y_crop_height, align 4, !tbaa !19
  %64 = load i32, i32* %aligned_width.addr, align 4, !tbaa !13
  %65 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %66 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %65, i32 0, i32 0
  %67 = bitcast %union.anon* %66 to %struct.anon*
  %y_width = getelementptr inbounds %struct.anon, %struct.anon* %67, i32 0, i32 0
  store i32 %64, i32* %y_width, align 4, !tbaa !19
  %68 = load i32, i32* %aligned_height.addr, align 4, !tbaa !13
  %69 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %70 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %69, i32 0, i32 1
  %71 = bitcast %union.anon.0* %70 to %struct.anon.1*
  %y_height = getelementptr inbounds %struct.anon.1, %struct.anon.1* %71, i32 0, i32 0
  store i32 %68, i32* %y_height, align 4, !tbaa !19
  %72 = load i32, i32* %y_stride.addr, align 4, !tbaa !13
  %73 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %74 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %73, i32 0, i32 4
  %75 = bitcast %union.anon.6* %74 to %struct.anon.7*
  %y_stride61 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %75, i32 0, i32 0
  store i32 %72, i32* %y_stride61, align 4, !tbaa !19
  %76 = load i32, i32* %width.addr, align 4, !tbaa !13
  %77 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %add62 = add nsw i32 %76, %77
  %78 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %shr = ashr i32 %add62, %78
  %79 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %80 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %79, i32 0, i32 2
  %81 = bitcast %union.anon.2* %80 to %struct.anon.3*
  %uv_crop_width = getelementptr inbounds %struct.anon.3, %struct.anon.3* %81, i32 0, i32 1
  store i32 %shr, i32* %uv_crop_width, align 4, !tbaa !19
  %82 = load i32, i32* %height.addr, align 4, !tbaa !13
  %83 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %add63 = add nsw i32 %82, %83
  %84 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %shr64 = ashr i32 %add63, %84
  %85 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %86 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %85, i32 0, i32 3
  %87 = bitcast %union.anon.4* %86 to %struct.anon.5*
  %uv_crop_height = getelementptr inbounds %struct.anon.5, %struct.anon.5* %87, i32 0, i32 1
  store i32 %shr64, i32* %uv_crop_height, align 4, !tbaa !19
  %88 = load i32, i32* %uv_width.addr, align 4, !tbaa !13
  %89 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %90 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %89, i32 0, i32 0
  %91 = bitcast %union.anon* %90 to %struct.anon*
  %uv_width65 = getelementptr inbounds %struct.anon, %struct.anon* %91, i32 0, i32 1
  store i32 %88, i32* %uv_width65, align 4, !tbaa !19
  %92 = load i32, i32* %uv_height.addr, align 4, !tbaa !13
  %93 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %94 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %93, i32 0, i32 1
  %95 = bitcast %union.anon.0* %94 to %struct.anon.1*
  %uv_height66 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %95, i32 0, i32 1
  store i32 %92, i32* %uv_height66, align 4, !tbaa !19
  %96 = load i32, i32* %uv_stride.addr, align 4, !tbaa !13
  %97 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %98 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %97, i32 0, i32 4
  %99 = bitcast %union.anon.6* %98 to %struct.anon.7*
  %uv_stride67 = getelementptr inbounds %struct.anon.7, %struct.anon.7* %99, i32 0, i32 1
  store i32 %96, i32* %uv_stride67, align 4, !tbaa !19
  %100 = load i32, i32* %border.addr, align 4, !tbaa !13
  %101 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %border68 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %101, i32 0, i32 12
  store i32 %100, i32* %border68, align 4, !tbaa !20
  %102 = load i64, i64* %frame_size, align 8, !tbaa !14
  %conv69 = trunc i64 %102 to i32
  %103 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %frame_size70 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %103, i32 0, i32 13
  store i32 %conv69, i32* %frame_size70, align 4, !tbaa !21
  %104 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %105 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %105, i32 0, i32 14
  store i32 %104, i32* %subsampling_x, align 4, !tbaa !22
  %106 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %107 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %107, i32 0, i32 15
  store i32 %106, i32* %subsampling_y, align 4, !tbaa !23
  %108 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc71 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %108, i32 0, i32 10
  %109 = load i8*, i8** %buffer_alloc71, align 4, !tbaa !10
  store i8* %109, i8** %buf, align 4, !tbaa !2
  %110 = load i32, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  %tobool72 = icmp ne i32 %110, 0
  br i1 %tobool72, label %if.then73, label %if.else76

if.then73:                                        ; preds = %if.end60
  %111 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buffer_alloc74 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %111, i32 0, i32 10
  %112 = load i8*, i8** %buffer_alloc74, align 4, !tbaa !10
  %113 = ptrtoint i8* %112 to i32
  %shr75 = lshr i32 %113, 1
  %114 = inttoptr i32 %shr75 to i8*
  store i8* %114, i8** %buf, align 4, !tbaa !2
  %115 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %flags = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %115, i32 0, i32 26
  store i32 8, i32* %flags, align 4, !tbaa !24
  br label %if.end78

if.else76:                                        ; preds = %if.end60
  %116 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %flags77 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %116, i32 0, i32 26
  store i32 0, i32* %flags77, align 4, !tbaa !24
  br label %if.end78

if.end78:                                         ; preds = %if.else76, %if.then73
  %117 = load i8*, i8** %buf, align 4, !tbaa !2
  %118 = load i32, i32* %border.addr, align 4, !tbaa !13
  %119 = load i32, i32* %y_stride.addr, align 4, !tbaa !13
  %mul79 = mul nsw i32 %118, %119
  %add.ptr = getelementptr inbounds i8, i8* %117, i32 %mul79
  %120 = load i32, i32* %border.addr, align 4, !tbaa !13
  %add.ptr80 = getelementptr inbounds i8, i8* %add.ptr, i32 %120
  %121 = ptrtoint i8* %add.ptr80 to i32
  %122 = load i32, i32* %aom_byte_align, align 4, !tbaa !13
  %sub = sub nsw i32 %122, 1
  %add81 = add i32 %121, %sub
  %123 = load i32, i32* %aom_byte_align, align 4, !tbaa !13
  %sub82 = sub nsw i32 %123, 1
  %neg = xor i32 %sub82, -1
  %and83 = and i32 %add81, %neg
  %124 = inttoptr i32 %and83 to i8*
  %125 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %126 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %125, i32 0, i32 5
  %127 = bitcast %union.anon.8* %126 to %struct.anon.9*
  %y_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %127, i32 0, i32 0
  store i8* %124, i8** %y_buffer, align 4, !tbaa !19
  %128 = load i8*, i8** %buf, align 4, !tbaa !2
  %129 = load i64, i64* %yplane_size.addr, align 8, !tbaa !14
  %idx.ext = trunc i64 %129 to i32
  %add.ptr84 = getelementptr inbounds i8, i8* %128, i32 %idx.ext
  %130 = load i32, i32* %uv_border_h.addr, align 4, !tbaa !13
  %131 = load i32, i32* %uv_stride.addr, align 4, !tbaa !13
  %mul85 = mul nsw i32 %130, %131
  %add.ptr86 = getelementptr inbounds i8, i8* %add.ptr84, i32 %mul85
  %132 = load i32, i32* %uv_border_w.addr, align 4, !tbaa !13
  %add.ptr87 = getelementptr inbounds i8, i8* %add.ptr86, i32 %132
  %133 = ptrtoint i8* %add.ptr87 to i32
  %134 = load i32, i32* %aom_byte_align, align 4, !tbaa !13
  %sub88 = sub nsw i32 %134, 1
  %add89 = add i32 %133, %sub88
  %135 = load i32, i32* %aom_byte_align, align 4, !tbaa !13
  %sub90 = sub nsw i32 %135, 1
  %neg91 = xor i32 %sub90, -1
  %and92 = and i32 %add89, %neg91
  %136 = inttoptr i32 %and92 to i8*
  %137 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %138 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %137, i32 0, i32 5
  %139 = bitcast %union.anon.8* %138 to %struct.anon.9*
  %u_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %139, i32 0, i32 1
  store i8* %136, i8** %u_buffer, align 4, !tbaa !19
  %140 = load i8*, i8** %buf, align 4, !tbaa !2
  %141 = load i64, i64* %yplane_size.addr, align 8, !tbaa !14
  %idx.ext93 = trunc i64 %141 to i32
  %add.ptr94 = getelementptr inbounds i8, i8* %140, i32 %idx.ext93
  %142 = load i64, i64* %uvplane_size.addr, align 8, !tbaa !14
  %idx.ext95 = trunc i64 %142 to i32
  %add.ptr96 = getelementptr inbounds i8, i8* %add.ptr94, i32 %idx.ext95
  %143 = load i32, i32* %uv_border_h.addr, align 4, !tbaa !13
  %144 = load i32, i32* %uv_stride.addr, align 4, !tbaa !13
  %mul97 = mul nsw i32 %143, %144
  %add.ptr98 = getelementptr inbounds i8, i8* %add.ptr96, i32 %mul97
  %145 = load i32, i32* %uv_border_w.addr, align 4, !tbaa !13
  %add.ptr99 = getelementptr inbounds i8, i8* %add.ptr98, i32 %145
  %146 = ptrtoint i8* %add.ptr99 to i32
  %147 = load i32, i32* %aom_byte_align, align 4, !tbaa !13
  %sub100 = sub nsw i32 %147, 1
  %add101 = add i32 %146, %sub100
  %148 = load i32, i32* %aom_byte_align, align 4, !tbaa !13
  %sub102 = sub nsw i32 %148, 1
  %neg103 = xor i32 %sub102, -1
  %and104 = and i32 %add101, %neg103
  %149 = inttoptr i32 %and104 to i8*
  %150 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %151 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %150, i32 0, i32 5
  %152 = bitcast %union.anon.8* %151 to %struct.anon.9*
  %v_buffer = getelementptr inbounds %struct.anon.9, %struct.anon.9* %152, i32 0, i32 2
  store i8* %149, i8** %v_buffer, align 4, !tbaa !19
  %153 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %use_external_reference_buffers = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %153, i32 0, i32 6
  store i32 0, i32* %use_external_reference_buffers, align 4, !tbaa !25
  %154 = load i32, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  %tobool105 = icmp ne i32 %154, 0
  br i1 %tobool105, label %if.then106, label %if.else118

if.then106:                                       ; preds = %if.end78
  %155 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %155, i32 0, i32 8
  %156 = load i8*, i8** %y_buffer_8bit, align 4, !tbaa !11
  %tobool107 = icmp ne i8* %156, null
  br i1 %tobool107, label %if.then108, label %if.end110

if.then108:                                       ; preds = %if.then106
  %157 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit109 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %157, i32 0, i32 8
  %158 = load i8*, i8** %y_buffer_8bit109, align 4, !tbaa !11
  call void @aom_free(i8* %158)
  br label %if.end110

if.end110:                                        ; preds = %if.then108, %if.then106
  %159 = load i64, i64* %yplane_size.addr, align 8, !tbaa !14
  %conv111 = trunc i64 %159 to i32
  %call112 = call i8* @aom_memalign(i32 32, i32 %conv111)
  %160 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit113 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %160, i32 0, i32 8
  store i8* %call112, i8** %y_buffer_8bit113, align 4, !tbaa !11
  %161 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit114 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %161, i32 0, i32 8
  %162 = load i8*, i8** %y_buffer_8bit114, align 4, !tbaa !11
  %tobool115 = icmp ne i8* %162, null
  br i1 %tobool115, label %if.end117, label %if.then116

if.then116:                                       ; preds = %if.end110
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end117:                                        ; preds = %if.end110
  br label %if.end125

if.else118:                                       ; preds = %if.end78
  %163 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit119 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %163, i32 0, i32 8
  %164 = load i8*, i8** %y_buffer_8bit119, align 4, !tbaa !11
  %tobool120 = icmp ne i8* %164, null
  br i1 %tobool120, label %if.then121, label %if.end124

if.then121:                                       ; preds = %if.else118
  %165 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit122 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %165, i32 0, i32 8
  %166 = load i8*, i8** %y_buffer_8bit122, align 4, !tbaa !11
  call void @aom_free(i8* %166)
  %167 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %y_buffer_8bit123 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %167, i32 0, i32 8
  store i8* null, i8** %y_buffer_8bit123, align 4, !tbaa !11
  %168 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %buf_8bit_valid = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %168, i32 0, i32 9
  store i32 0, i32* %buf_8bit_valid, align 4, !tbaa !26
  br label %if.end124

if.end124:                                        ; preds = %if.then121, %if.else118
  br label %if.end125

if.end125:                                        ; preds = %if.end124, %if.end117
  %169 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %corrupted = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %169, i32 0, i32 25
  store i32 0, i32* %corrupted, align 4, !tbaa !27
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

cleanup126:                                       ; preds = %if.end125, %if.then116, %if.then53, %if.then46, %cleanup, %if.then8
  %170 = bitcast i64* %alloc_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %170) #4
  %171 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #4
  %172 = bitcast i64* %frame_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %172) #4
  %173 = bitcast i32* %aom_byte_align to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #4
  br label %return

if.end130:                                        ; preds = %entry
  store i32 2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end130, %cleanup126
  %174 = load i32, i32* %retval, align 4
  ret i32 %174
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define hidden i32 @aom_alloc_frame_buffer(%struct.yv12_buffer_config* %ybf, i32 %width, i32 %height, i32 %ss_x, i32 %ss_y, i32 %use_highbitdepth, i32 %border, i32 %byte_alignment) #0 {
entry:
  %retval = alloca i32, align 4
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %ss_x.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %use_highbitdepth.addr = alloca i32, align 4
  %border.addr = alloca i32, align 4
  %byte_alignment.addr = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i32 %height, i32* %height.addr, align 4, !tbaa !13
  store i32 %ss_x, i32* %ss_x.addr, align 4, !tbaa !13
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !13
  store i32 %use_highbitdepth, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  store i32 %border, i32* %border.addr, align 4, !tbaa !13
  store i32 %byte_alignment, i32* %byte_alignment.addr, align 4, !tbaa !13
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.yv12_buffer_config* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %call = call i32 @aom_free_frame_buffer(%struct.yv12_buffer_config* %1)
  %2 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %3 = load i32, i32* %width.addr, align 4, !tbaa !13
  %4 = load i32, i32* %height.addr, align 4, !tbaa !13
  %5 = load i32, i32* %ss_x.addr, align 4, !tbaa !13
  %6 = load i32, i32* %ss_y.addr, align 4, !tbaa !13
  %7 = load i32, i32* %use_highbitdepth.addr, align 4, !tbaa !13
  %8 = load i32, i32* %border.addr, align 4, !tbaa !13
  %9 = load i32, i32* %byte_alignment.addr, align 4, !tbaa !13
  %call1 = call i32 @aom_realloc_frame_buffer(%struct.yv12_buffer_config* %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i32 %8, i32 %9, %struct.aom_codec_frame_buffer* null, i32 (i8*, i32, %struct.aom_codec_frame_buffer*)* null, i8* null)
  store i32 %call1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

declare void @aom_img_metadata_array_free(%struct.aom_metadata_array*) #1

; Function Attrs: nounwind
define hidden i32 @aom_copy_metadata_to_frame_buffer(%struct.yv12_buffer_config* %ybf, %struct.aom_metadata_array* %arr) #0 {
entry:
  %retval = alloca i32, align 4
  %ybf.addr = alloca %struct.yv12_buffer_config*, align 4
  %arr.addr = alloca %struct.aom_metadata_array*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.yv12_buffer_config* %ybf, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  store %struct.aom_metadata_array* %arr, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %0 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.yv12_buffer_config* %0, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_metadata_array* %1, null
  br i1 %tobool1, label %lor.lhs.false2, label %if.then

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %2 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %metadata_array = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %2, i32 0, i32 1
  %3 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array, align 4, !tbaa !28
  %tobool3 = icmp ne %struct.aom_metadata** %3, null
  br i1 %tobool3, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false2
  %4 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  call void @aom_remove_metadata_from_frame_buffer(%struct.yv12_buffer_config* %4)
  %5 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %sz = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %5, i32 0, i32 0
  %6 = load i32, i32* %sz, align 4, !tbaa !30
  %call = call %struct.aom_metadata_array* @aom_img_metadata_array_alloc(i32 %6)
  %7 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %7, i32 0, i32 27
  store %struct.aom_metadata_array* %call, %struct.aom_metadata_array** %metadata, align 4, !tbaa !12
  %8 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata4 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %8, i32 0, i32 27
  %9 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata4, align 4, !tbaa !12
  %tobool5 = icmp ne %struct.aom_metadata_array* %9, null
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  store i32 0, i32* %i, align 4, !tbaa !31
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end7
  %11 = load i32, i32* %i, align 4, !tbaa !31
  %12 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata8 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %12, i32 0, i32 27
  %13 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata8, align 4, !tbaa !12
  %sz9 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %13, i32 0, i32 0
  %14 = load i32, i32* %sz9, align 4, !tbaa !30
  %cmp = icmp ult i32 %11, %14
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %15 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %metadata_array10 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %15, i32 0, i32 1
  %16 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array10, align 4, !tbaa !28
  %17 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %16, i32 %17
  %18 = load %struct.aom_metadata*, %struct.aom_metadata** %arrayidx, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %18, i32 0, i32 0
  %19 = load i32, i32* %type, align 4, !tbaa !32
  %20 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %metadata_array11 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %20, i32 0, i32 1
  %21 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array11, align 4, !tbaa !28
  %22 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx12 = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %21, i32 %22
  %23 = load %struct.aom_metadata*, %struct.aom_metadata** %arrayidx12, align 4, !tbaa !2
  %payload = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %23, i32 0, i32 1
  %24 = load i8*, i8** %payload, align 4, !tbaa !34
  %25 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %metadata_array13 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %25, i32 0, i32 1
  %26 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array13, align 4, !tbaa !28
  %27 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx14 = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %26, i32 %27
  %28 = load %struct.aom_metadata*, %struct.aom_metadata** %arrayidx14, align 4, !tbaa !2
  %sz15 = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %28, i32 0, i32 2
  %29 = load i32, i32* %sz15, align 4, !tbaa !35
  %30 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %metadata_array16 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %30, i32 0, i32 1
  %31 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array16, align 4, !tbaa !28
  %32 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx17 = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %31, i32 %32
  %33 = load %struct.aom_metadata*, %struct.aom_metadata** %arrayidx17, align 4, !tbaa !2
  %insert_flag = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %33, i32 0, i32 3
  %34 = load i32, i32* %insert_flag, align 4, !tbaa !36
  %call18 = call %struct.aom_metadata* @aom_img_metadata_alloc(i32 %19, i8* %24, i32 %29, i32 %34)
  %35 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata19 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %35, i32 0, i32 27
  %36 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata19, align 4, !tbaa !12
  %metadata_array20 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %36, i32 0, i32 1
  %37 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array20, align 4, !tbaa !28
  %38 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx21 = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %37, i32 %38
  store %struct.aom_metadata* %call18, %struct.aom_metadata** %arrayidx21, align 4, !tbaa !2
  %39 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata22 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %39, i32 0, i32 27
  %40 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata22, align 4, !tbaa !12
  %metadata_array23 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %40, i32 0, i32 1
  %41 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array23, align 4, !tbaa !28
  %42 = load i32, i32* %i, align 4, !tbaa !31
  %arrayidx24 = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %41, i32 %42
  %43 = load %struct.aom_metadata*, %struct.aom_metadata** %arrayidx24, align 4, !tbaa !2
  %cmp25 = icmp eq %struct.aom_metadata* %43, null
  br i1 %cmp25, label %if.then26, label %if.end29

if.then26:                                        ; preds = %for.body
  %44 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata27 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %44, i32 0, i32 27
  %45 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata27, align 4, !tbaa !12
  call void @aom_img_metadata_array_free(%struct.aom_metadata_array* %45)
  %46 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata28 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %46, i32 0, i32 27
  store %struct.aom_metadata_array* null, %struct.aom_metadata_array** %metadata28, align 4, !tbaa !12
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end29:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end29
  %47 = load i32, i32* %i, align 4, !tbaa !31
  %inc = add i32 %47, 1
  store i32 %inc, i32* %i, align 4, !tbaa !31
  br label %for.cond

cleanup:                                          ; preds = %if.then26, %for.cond.cleanup
  %48 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  %49 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %sz30 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %49, i32 0, i32 0
  %50 = load i32, i32* %sz30, align 4, !tbaa !30
  %51 = load %struct.yv12_buffer_config*, %struct.yv12_buffer_config** %ybf.addr, align 4, !tbaa !2
  %metadata31 = getelementptr inbounds %struct.yv12_buffer_config, %struct.yv12_buffer_config* %51, i32 0, i32 27
  %52 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata31, align 4, !tbaa !12
  %sz32 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %52, i32 0, i32 0
  store i32 %50, i32* %sz32, align 4, !tbaa !30
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup, %if.then6, %if.then
  %53 = load i32, i32* %retval, align 4
  ret i32 %53

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare %struct.aom_metadata_array* @aom_img_metadata_array_alloc(i32) #1

declare %struct.aom_metadata* @aom_img_metadata_alloc(i32, i8*, i32, i32) #1

declare i8* @aom_memalign(i32, i32) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !9, i64 80}
!7 = !{!"yv12_buffer_config", !4, i64 0, !4, i64 8, !4, i64 16, !4, i64 24, !4, i64 32, !4, i64 40, !8, i64 52, !4, i64 56, !3, i64 68, !8, i64 72, !3, i64 76, !9, i64 80, !8, i64 84, !9, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !4, i64 116, !4, i64 120, !4, i64 124, !8, i64 128, !8, i64 132, !8, i64 136, !8, i64 140, !3, i64 144}
!8 = !{!"int", !4, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!7, !3, i64 76}
!11 = !{!7, !3, i64 68}
!12 = !{!7, !3, i64 144}
!13 = !{!8, !8, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"long long", !4, i64 0}
!16 = !{!17, !3, i64 0}
!17 = !{!"aom_codec_frame_buffer", !3, i64 0, !9, i64 4, !3, i64 8}
!18 = !{!17, !9, i64 4}
!19 = !{!4, !4, i64 0}
!20 = !{!7, !8, i64 84}
!21 = !{!7, !9, i64 88}
!22 = !{!7, !8, i64 92}
!23 = !{!7, !8, i64 96}
!24 = !{!7, !8, i64 140}
!25 = !{!7, !8, i64 52}
!26 = !{!7, !8, i64 72}
!27 = !{!7, !8, i64 136}
!28 = !{!29, !3, i64 4}
!29 = !{!"aom_metadata_array", !9, i64 0, !3, i64 4}
!30 = !{!29, !9, i64 0}
!31 = !{!9, !9, i64 0}
!32 = !{!33, !8, i64 0}
!33 = !{!"aom_metadata", !8, i64 0, !3, i64 4, !9, i64 8, !4, i64 12}
!34 = !{!33, !3, i64 4}
!35 = !{!33, !9, i64 8}
!36 = !{!33, !4, i64 12}
