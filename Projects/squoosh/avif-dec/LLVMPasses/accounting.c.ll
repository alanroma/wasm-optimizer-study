; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/accounting.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/decoder/accounting.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Accounting = type { %struct.AccountingSymbols, i32, [1021 x i16], %struct.AccountingSymbolContext, i32 }
%struct.AccountingSymbols = type { %struct.AccountingSymbol*, i32, i32, i32, %struct.AccountingDictionary }
%struct.AccountingSymbol = type { %struct.AccountingSymbolContext, i32, i32, i32 }
%struct.AccountingDictionary = type { [256 x i8*], i32 }
%struct.AccountingSymbolContext = type { i16, i16 }

@.str = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.1 = private unnamed_addr constant [54 x i8] c"\0A----- Number of recorded syntax elements = %d -----\0A\00", align 1
@.str.2 = private unnamed_addr constant [59 x i8] c"----- Total number of symbol calls = %d (%d binary) -----\0A\00", align 1
@.str.3 = private unnamed_addr constant [38 x i8] c"%s x: %d, y: %d bits: %f samples: %d\0A\00", align 1

; Function Attrs: nounwind
define hidden i32 @aom_accounting_dictionary_lookup(%struct.Accounting* %accounting, i8* %str) #0 {
entry:
  %retval = alloca i32, align 4
  %accounting.addr = alloca %struct.Accounting*, align 4
  %str.addr = alloca i8*, align 4
  %hash = alloca i32, align 4
  %len = alloca i32, align 4
  %dictionary = alloca %struct.AccountingDictionary*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Accounting* %accounting, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  store i8* %str, i8** %str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast %struct.AccountingDictionary** %dictionary to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %3, i32 0, i32 0
  %dictionary1 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 4
  store %struct.AccountingDictionary* %dictionary1, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %4 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call = call i32 @accounting_hash(i8* %4)
  store i32 %call, i32* %hash, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end15, %entry
  %5 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %hash_dictionary = getelementptr inbounds %struct.Accounting, %struct.Accounting* %5, i32 0, i32 2
  %6 = load i32, i32* %hash, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [1021 x i16], [1021 x i16]* %hash_dictionary, i32 0, i32 %6
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !8
  %conv = sext i16 %7 to i32
  %cmp = icmp ne i32 %conv, -1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %strs = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %8, i32 0, i32 0
  %9 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %hash_dictionary3 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %9, i32 0, i32 2
  %10 = load i32, i32* %hash, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [1021 x i16], [1021 x i16]* %hash_dictionary3, i32 0, i32 %10
  %11 = load i16, i16* %arrayidx4, align 2, !tbaa !8
  %idxprom = sext i16 %11 to i32
  %arrayidx5 = getelementptr inbounds [256 x i8*], [256 x i8*]* %strs, i32 0, i32 %idxprom
  %12 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  %13 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call6 = call i32 @strcmp(i8* %12, i8* %13)
  %cmp7 = icmp eq i32 %call6, 0
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %14 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %hash_dictionary9 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %14, i32 0, i32 2
  %15 = load i32, i32* %hash, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds [1021 x i16], [1021 x i16]* %hash_dictionary9, i32 0, i32 %15
  %16 = load i16, i16* %arrayidx10, align 2, !tbaa !8
  %conv11 = sext i16 %16 to i32
  store i32 %conv11, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  %17 = load i32, i32* %hash, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %hash, align 4, !tbaa !6
  %18 = load i32, i32* %hash, align 4, !tbaa !6
  %cmp12 = icmp eq i32 %18, 1021
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end
  store i32 0, i32* %hash, align 4, !tbaa !6
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %if.end
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %19 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %num_strs = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %19, i32 0, i32 1
  %20 = load i32, i32* %num_strs, align 4, !tbaa !10
  %conv16 = trunc i32 %20 to i16
  %21 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %hash_dictionary17 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %21, i32 0, i32 2
  %22 = load i32, i32* %hash, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds [1021 x i16], [1021 x i16]* %hash_dictionary17, i32 0, i32 %22
  store i16 %conv16, i16* %arrayidx18, align 2, !tbaa !8
  %23 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call19 = call i32 @strlen(i8* %23)
  store i32 %call19, i32* %len, align 4, !tbaa !12
  %24 = load i32, i32* %len, align 4, !tbaa !12
  %add = add i32 %24, 1
  %call20 = call i8* @malloc(i32 %add)
  %25 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %strs21 = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %25, i32 0, i32 0
  %26 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %num_strs22 = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %26, i32 0, i32 1
  %27 = load i32, i32* %num_strs22, align 4, !tbaa !10
  %arrayidx23 = getelementptr inbounds [256 x i8*], [256 x i8*]* %strs21, i32 0, i32 %27
  store i8* %call20, i8** %arrayidx23, align 4, !tbaa !2
  %28 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %strs24 = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %28, i32 0, i32 0
  %29 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %num_strs25 = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %29, i32 0, i32 1
  %30 = load i32, i32* %num_strs25, align 4, !tbaa !10
  %arrayidx26 = getelementptr inbounds [256 x i8*], [256 x i8*]* %strs24, i32 0, i32 %30
  %31 = load i8*, i8** %arrayidx26, align 4, !tbaa !2
  %32 = load i32, i32* %len, align 4, !tbaa !12
  %add27 = add i32 %32, 1
  %33 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call28 = call i32 (i8*, i32, i8*, ...) @snprintf(i8* %31, i32 %add27, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0), i8* %33)
  %34 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %num_strs29 = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %34, i32 0, i32 1
  %35 = load i32, i32* %num_strs29, align 4, !tbaa !10
  %inc30 = add nsw i32 %35, 1
  store i32 %inc30, i32* %num_strs29, align 4, !tbaa !10
  %36 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %num_strs31 = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %36, i32 0, i32 1
  %37 = load i32, i32* %num_strs31, align 4, !tbaa !10
  %sub = sub nsw i32 %37, 1
  store i32 %sub, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %38 = bitcast %struct.AccountingDictionary** %dictionary to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  %39 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  %40 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  %41 = load i32, i32* %retval, align 4
  ret i32 %41
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @accounting_hash(i8* %str) #0 {
entry:
  %str.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %ustr = alloca i8*, align 4
  store i8* %str, i8** %str.addr, align 4, !tbaa !2
  %0 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i8** %ustr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  store i32 0, i32* %val, align 4, !tbaa !6
  %2 = load i8*, i8** %str.addr, align 4, !tbaa !2
  store i8* %2, i8** %ustr, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i8*, i8** %ustr, align 4, !tbaa !2
  %4 = load i8, i8* %3, align 1, !tbaa !14
  %tobool = icmp ne i8 %4, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i8*, i8** %ustr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %ustr, align 4, !tbaa !2
  %6 = load i8, i8* %5, align 1, !tbaa !14
  %conv = zext i8 %6 to i32
  %7 = load i32, i32* %val, align 4, !tbaa !6
  %add = add i32 %7, %conv
  store i32 %add, i32* %val, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %8 = load i32, i32* %val, align 4, !tbaa !6
  %rem = urem i32 %8, 1021
  %9 = bitcast i8** %ustr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  ret i32 %rem
}

declare i32 @strcmp(i8*, i8*) #2

declare i32 @strlen(i8*) #2

declare i8* @malloc(i32) #2

declare i32 @snprintf(i8*, i32, i8*, ...) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_accounting_init(%struct.Accounting* %accounting) #0 {
entry:
  %accounting.addr = alloca %struct.Accounting*, align 4
  %i = alloca i32, align 4
  store %struct.Accounting* %accounting, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %num_syms_allocated = getelementptr inbounds %struct.Accounting, %struct.Accounting* %1, i32 0, i32 1
  store i32 1000, i32* %num_syms_allocated, align 4, !tbaa !15
  %2 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %num_syms_allocated1 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %2, i32 0, i32 1
  %3 = load i32, i32* %num_syms_allocated1, align 4, !tbaa !15
  %mul = mul i32 16, %3
  %call = call i8* @malloc(i32 %mul)
  %4 = bitcast i8* %call to %struct.AccountingSymbol*
  %5 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %5, i32 0, i32 0
  %syms2 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 0
  store %struct.AccountingSymbol* %4, %struct.AccountingSymbol** %syms2, align 4, !tbaa !19
  %6 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms3 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %6, i32 0, i32 0
  %dictionary = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms3, i32 0, i32 4
  %num_strs = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %dictionary, i32 0, i32 1
  store i32 0, i32* %num_strs, align 4, !tbaa !20
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %7, 1021
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %hash_dictionary = getelementptr inbounds %struct.Accounting, %struct.Accounting* %8, i32 0, i32 2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [1021 x i16], [1021 x i16]* %hash_dictionary, i32 0, i32 %9
  store i16 -1, i16* %arrayidx, align 2, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  call void @aom_accounting_reset(%struct.Accounting* %11)
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #3
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_accounting_reset(%struct.Accounting* %accounting) #0 {
entry:
  %accounting.addr = alloca %struct.Accounting*, align 4
  store %struct.Accounting* %accounting, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %0 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %0, i32 0, i32 0
  %num_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 1
  store i32 0, i32* %num_syms, align 4, !tbaa !21
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms1 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %1, i32 0, i32 0
  %num_binary_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms1, i32 0, i32 3
  store i32 0, i32* %num_binary_syms, align 4, !tbaa !22
  %2 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms2 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %2, i32 0, i32 0
  %num_multi_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms2, i32 0, i32 2
  store i32 0, i32* %num_multi_syms, align 4, !tbaa !23
  %3 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %context = getelementptr inbounds %struct.Accounting, %struct.Accounting* %3, i32 0, i32 3
  %x = getelementptr inbounds %struct.AccountingSymbolContext, %struct.AccountingSymbolContext* %context, i32 0, i32 0
  store i16 -1, i16* %x, align 2, !tbaa !24
  %4 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %context3 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %4, i32 0, i32 3
  %y = getelementptr inbounds %struct.AccountingSymbolContext, %struct.AccountingSymbolContext* %context3, i32 0, i32 1
  store i16 -1, i16* %y, align 2, !tbaa !25
  %5 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %last_tell_frac = getelementptr inbounds %struct.Accounting, %struct.Accounting* %5, i32 0, i32 4
  store i32 0, i32* %last_tell_frac, align 4, !tbaa !26
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_accounting_clear(%struct.Accounting* %accounting) #0 {
entry:
  %accounting.addr = alloca %struct.Accounting*, align 4
  %i = alloca i32, align 4
  %dictionary = alloca %struct.AccountingDictionary*, align 4
  store %struct.Accounting* %accounting, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.AccountingDictionary** %dictionary to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %2, i32 0, i32 0
  %syms1 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 0
  %3 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %syms1, align 4, !tbaa !19
  %4 = bitcast %struct.AccountingSymbol* %3 to i8*
  call void @free(i8* %4)
  %5 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms2 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %5, i32 0, i32 0
  %dictionary3 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms2, i32 0, i32 4
  store %struct.AccountingDictionary* %dictionary3, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %7 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %num_strs = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %7, i32 0, i32 1
  %8 = load i32, i32* %num_strs, align 4, !tbaa !10
  %cmp = icmp slt i32 %6, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load %struct.AccountingDictionary*, %struct.AccountingDictionary** %dictionary, align 4, !tbaa !2
  %strs = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %9, i32 0, i32 0
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [256 x i8*], [256 x i8*]* %strs, i32 0, i32 %10
  %11 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  call void @free(i8* %11)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast %struct.AccountingDictionary** %dictionary to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  ret void
}

declare void @free(i8*) #2

; Function Attrs: nounwind
define hidden void @aom_accounting_set_context(%struct.Accounting* %accounting, i16 signext %x, i16 signext %y) #0 {
entry:
  %accounting.addr = alloca %struct.Accounting*, align 4
  %x.addr = alloca i16, align 2
  %y.addr = alloca i16, align 2
  store %struct.Accounting* %accounting, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  store i16 %x, i16* %x.addr, align 2, !tbaa !8
  store i16 %y, i16* %y.addr, align 2, !tbaa !8
  %0 = load i16, i16* %x.addr, align 2, !tbaa !8
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %context = getelementptr inbounds %struct.Accounting, %struct.Accounting* %1, i32 0, i32 3
  %x1 = getelementptr inbounds %struct.AccountingSymbolContext, %struct.AccountingSymbolContext* %context, i32 0, i32 0
  store i16 %0, i16* %x1, align 2, !tbaa !24
  %2 = load i16, i16* %y.addr, align 2, !tbaa !8
  %3 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %context2 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %3, i32 0, i32 3
  %y3 = getelementptr inbounds %struct.AccountingSymbolContext, %struct.AccountingSymbolContext* %context2, i32 0, i32 1
  store i16 %2, i16* %y3, align 2, !tbaa !25
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_accounting_record(%struct.Accounting* %accounting, i8* %str, i32 %bits) #0 {
entry:
  %accounting.addr = alloca %struct.Accounting*, align 4
  %str.addr = alloca i8*, align 4
  %bits.addr = alloca i32, align 4
  %sym = alloca %struct.AccountingSymbol, align 4
  %last_sym = alloca %struct.AccountingSymbol*, align 4
  %id = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.Accounting* %accounting, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  store i8* %str, i8** %str.addr, align 4, !tbaa !2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !6
  %0 = bitcast %struct.AccountingSymbol* %sym to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #3
  %1 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %1, i32 0, i32 0
  %num_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 1
  %2 = load i32, i32* %num_syms, align 4, !tbaa !21
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %3 = bitcast %struct.AccountingSymbol** %last_sym to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms1 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %4, i32 0, i32 0
  %syms2 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms1, i32 0, i32 0
  %5 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %syms2, align 4, !tbaa !19
  %6 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms3 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %6, i32 0, i32 0
  %num_syms4 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms3, i32 0, i32 1
  %7 = load i32, i32* %num_syms4, align 4, !tbaa !21
  %sub = sub nsw i32 %7, 1
  %arrayidx = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %5, i32 %sub
  store %struct.AccountingSymbol* %arrayidx, %struct.AccountingSymbol** %last_sym, align 4, !tbaa !2
  %8 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %last_sym, align 4, !tbaa !2
  %context = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %8, i32 0, i32 0
  %9 = bitcast %struct.AccountingSymbolContext* %context to i8*
  %10 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %context5 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %10, i32 0, i32 3
  %11 = bitcast %struct.AccountingSymbolContext* %context5 to i8*
  %call = call i32 @memcmp(i8* %9, i8* %11, i32 4)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then6, label %if.end12

if.then6:                                         ; preds = %if.then
  %12 = bitcast i32* %id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %14 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call7 = call i32 @aom_accounting_dictionary_lookup(%struct.Accounting* %13, i8* %14)
  store i32 %call7, i32* %id, align 4, !tbaa !6
  %15 = load i32, i32* %id, align 4, !tbaa !6
  %16 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %last_sym, align 4, !tbaa !2
  %id8 = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %16, i32 0, i32 1
  %17 = load i32, i32* %id8, align 4, !tbaa !27
  %cmp9 = icmp eq i32 %15, %17
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then6
  %18 = load i32, i32* %bits.addr, align 4, !tbaa !6
  %19 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %last_sym, align 4, !tbaa !2
  %bits11 = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %19, i32 0, i32 2
  %20 = load i32, i32* %bits11, align 4, !tbaa !29
  %add = add i32 %20, %18
  store i32 %add, i32* %bits11, align 4, !tbaa !29
  %21 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %last_sym, align 4, !tbaa !2
  %samples = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %21, i32 0, i32 3
  %22 = load i32, i32* %samples, align 4, !tbaa !30
  %inc = add i32 %22, 1
  store i32 %inc, i32* %samples, align 4, !tbaa !30
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then10
  %23 = bitcast i32* %id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup13 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end12

if.end12:                                         ; preds = %cleanup.cont, %if.then
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup13

cleanup13:                                        ; preds = %if.end12, %cleanup
  %24 = bitcast %struct.AccountingSymbol** %last_sym to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  %cleanup.dest14 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest14, label %cleanup42 [
    i32 0, label %cleanup.cont15
  ]

cleanup.cont15:                                   ; preds = %cleanup13
  br label %if.end16

if.end16:                                         ; preds = %cleanup.cont15, %entry
  %context17 = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %sym, i32 0, i32 0
  %25 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %context18 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %25, i32 0, i32 3
  %26 = bitcast %struct.AccountingSymbolContext* %context17 to i8*
  %27 = bitcast %struct.AccountingSymbolContext* %context18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 2 %27, i32 4, i1 false), !tbaa.struct !31
  %samples19 = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %sym, i32 0, i32 3
  store i32 1, i32* %samples19, align 4, !tbaa !30
  %28 = load i32, i32* %bits.addr, align 4, !tbaa !6
  %bits20 = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %sym, i32 0, i32 2
  store i32 %28, i32* %bits20, align 4, !tbaa !29
  %29 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %30 = load i8*, i8** %str.addr, align 4, !tbaa !2
  %call21 = call i32 @aom_accounting_dictionary_lookup(%struct.Accounting* %29, i8* %30)
  %id22 = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %sym, i32 0, i32 1
  store i32 %call21, i32* %id22, align 4, !tbaa !27
  %31 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms23 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %31, i32 0, i32 0
  %num_syms24 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms23, i32 0, i32 1
  %32 = load i32, i32* %num_syms24, align 4, !tbaa !21
  %33 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %num_syms_allocated = getelementptr inbounds %struct.Accounting, %struct.Accounting* %33, i32 0, i32 1
  %34 = load i32, i32* %num_syms_allocated, align 4, !tbaa !15
  %cmp25 = icmp eq i32 %32, %34
  br i1 %cmp25, label %if.then26, label %if.end35

if.then26:                                        ; preds = %if.end16
  %35 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %num_syms_allocated27 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %35, i32 0, i32 1
  %36 = load i32, i32* %num_syms_allocated27, align 4, !tbaa !15
  %mul = mul nsw i32 %36, 2
  store i32 %mul, i32* %num_syms_allocated27, align 4, !tbaa !15
  %37 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms28 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %37, i32 0, i32 0
  %syms29 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms28, i32 0, i32 0
  %38 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %syms29, align 4, !tbaa !19
  %39 = bitcast %struct.AccountingSymbol* %38 to i8*
  %40 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %num_syms_allocated30 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %40, i32 0, i32 1
  %41 = load i32, i32* %num_syms_allocated30, align 4, !tbaa !15
  %mul31 = mul i32 16, %41
  %call32 = call i8* @realloc(i8* %39, i32 %mul31)
  %42 = bitcast i8* %call32 to %struct.AccountingSymbol*
  %43 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms33 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %43, i32 0, i32 0
  %syms34 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms33, i32 0, i32 0
  store %struct.AccountingSymbol* %42, %struct.AccountingSymbol** %syms34, align 4, !tbaa !19
  br label %if.end35

if.end35:                                         ; preds = %if.then26, %if.end16
  %44 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms36 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %44, i32 0, i32 0
  %syms37 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms36, i32 0, i32 0
  %45 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %syms37, align 4, !tbaa !19
  %46 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms38 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %46, i32 0, i32 0
  %num_syms39 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms38, i32 0, i32 1
  %47 = load i32, i32* %num_syms39, align 4, !tbaa !21
  %inc40 = add nsw i32 %47, 1
  store i32 %inc40, i32* %num_syms39, align 4, !tbaa !21
  %arrayidx41 = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %45, i32 %47
  %48 = bitcast %struct.AccountingSymbol* %arrayidx41 to i8*
  %49 = bitcast %struct.AccountingSymbol* %sym to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false), !tbaa.struct !32
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup42

cleanup42:                                        ; preds = %if.end35, %cleanup13
  %50 = bitcast %struct.AccountingSymbol* %sym to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #3
  %cleanup.dest43 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest43, label %unreachable [
    i32 0, label %cleanup.cont44
    i32 1, label %cleanup.cont44
  ]

cleanup.cont44:                                   ; preds = %cleanup42, %cleanup42
  ret void

unreachable:                                      ; preds = %cleanup42
  unreachable
}

declare i32 @memcmp(i8*, i8*, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare i8* @realloc(i8*, i32) #2

; Function Attrs: nounwind
define hidden void @aom_accounting_dump(%struct.Accounting* %accounting) #0 {
entry:
  %accounting.addr = alloca %struct.Accounting*, align 4
  %i = alloca i32, align 4
  %sym = alloca %struct.AccountingSymbol*, align 4
  store %struct.Accounting* %accounting, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.AccountingSymbol** %sym to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms = getelementptr inbounds %struct.Accounting, %struct.Accounting* %2, i32 0, i32 0
  %num_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms, i32 0, i32 1
  %3 = load i32, i32* %num_syms, align 4, !tbaa !21
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([54 x i8], [54 x i8]* @.str.1, i32 0, i32 0), i32 %3)
  %4 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms1 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %4, i32 0, i32 0
  %num_multi_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms1, i32 0, i32 2
  %5 = load i32, i32* %num_multi_syms, align 4, !tbaa !23
  %6 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms2 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %6, i32 0, i32 0
  %num_binary_syms = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms2, i32 0, i32 3
  %7 = load i32, i32* %num_binary_syms, align 4, !tbaa !22
  %add = add nsw i32 %5, %7
  %8 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms3 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %8, i32 0, i32 0
  %num_binary_syms4 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms3, i32 0, i32 3
  %9 = load i32, i32* %num_binary_syms4, align 4, !tbaa !22
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.2, i32 0, i32 0), i32 %add, i32 %9)
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %11 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms6 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %11, i32 0, i32 0
  %num_syms7 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms6, i32 0, i32 1
  %12 = load i32, i32* %num_syms7, align 4, !tbaa !21
  %cmp = icmp slt i32 %10, %12
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms8 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %13, i32 0, i32 0
  %syms9 = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms8, i32 0, i32 0
  %14 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %syms9, align 4, !tbaa !19
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %14, i32 %15
  store %struct.AccountingSymbol* %arrayidx, %struct.AccountingSymbol** %sym, align 4, !tbaa !2
  %16 = load %struct.Accounting*, %struct.Accounting** %accounting.addr, align 4, !tbaa !2
  %syms10 = getelementptr inbounds %struct.Accounting, %struct.Accounting* %16, i32 0, i32 0
  %dictionary = getelementptr inbounds %struct.AccountingSymbols, %struct.AccountingSymbols* %syms10, i32 0, i32 4
  %strs = getelementptr inbounds %struct.AccountingDictionary, %struct.AccountingDictionary* %dictionary, i32 0, i32 0
  %17 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %sym, align 4, !tbaa !2
  %id = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %17, i32 0, i32 1
  %18 = load i32, i32* %id, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds [256 x i8*], [256 x i8*]* %strs, i32 0, i32 %18
  %19 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  %20 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %sym, align 4, !tbaa !2
  %context = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %20, i32 0, i32 0
  %x = getelementptr inbounds %struct.AccountingSymbolContext, %struct.AccountingSymbolContext* %context, i32 0, i32 0
  %21 = load i16, i16* %x, align 4, !tbaa !33
  %conv = sext i16 %21 to i32
  %22 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %sym, align 4, !tbaa !2
  %context12 = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %22, i32 0, i32 0
  %y = getelementptr inbounds %struct.AccountingSymbolContext, %struct.AccountingSymbolContext* %context12, i32 0, i32 1
  %23 = load i16, i16* %y, align 2, !tbaa !34
  %conv13 = sext i16 %23 to i32
  %24 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %sym, align 4, !tbaa !2
  %bits = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %24, i32 0, i32 2
  %25 = load i32, i32* %bits, align 4, !tbaa !29
  %conv14 = uitofp i32 %25 to float
  %conv15 = fpext float %conv14 to double
  %div = fdiv double %conv15, 8.000000e+00
  %26 = load %struct.AccountingSymbol*, %struct.AccountingSymbol** %sym, align 4, !tbaa !2
  %samples = getelementptr inbounds %struct.AccountingSymbol, %struct.AccountingSymbol* %26, i32 0, i32 3
  %27 = load i32, i32* %samples, align 4, !tbaa !30
  %call16 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.3, i32 0, i32 0), i8* %19, i32 %conv, i32 %conv13, double %div, i32 %27)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %29 = bitcast %struct.AccountingSymbol** %sym to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  ret void
}

declare i32 @printf(i8*, ...) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"short", !4, i64 0}
!10 = !{!11, !7, i64 1024}
!11 = !{!"", !4, i64 0, !7, i64 1024}
!12 = !{!13, !13, i64 0}
!13 = !{!"long", !4, i64 0}
!14 = !{!4, !4, i64 0}
!15 = !{!16, !7, i64 1044}
!16 = !{!"Accounting", !17, i64 0, !7, i64 1044, !4, i64 1048, !18, i64 3090, !7, i64 3096}
!17 = !{!"", !3, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !11, i64 16}
!18 = !{!"", !9, i64 0, !9, i64 2}
!19 = !{!16, !3, i64 0}
!20 = !{!16, !7, i64 1040}
!21 = !{!16, !7, i64 4}
!22 = !{!16, !7, i64 12}
!23 = !{!16, !7, i64 8}
!24 = !{!16, !9, i64 3090}
!25 = !{!16, !9, i64 3092}
!26 = !{!16, !7, i64 3096}
!27 = !{!28, !7, i64 4}
!28 = !{!"", !18, i64 0, !7, i64 4, !7, i64 8, !7, i64 12}
!29 = !{!28, !7, i64 8}
!30 = !{!28, !7, i64 12}
!31 = !{i64 0, i64 2, !8, i64 2, i64 2, !8}
!32 = !{i64 0, i64 2, !8, i64 2, i64 2, !8, i64 4, i64 4, !6, i64 8, i64 4, !6, i64 12, i64 4, !6}
!33 = !{!28, !9, i64 0}
!34 = !{!28, !9, i64 2}
