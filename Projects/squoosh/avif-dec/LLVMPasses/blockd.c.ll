; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/blockd.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/blockd.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.MB_MODE_INFO = type { %struct.INTERINTER_COMPOUND_DATA, %struct.WarpedMotionParams, [2 x %union.int_mv], i32, %union.int_interpfilters, %struct.PALETTE_MODE_INFO, i8, i8, i8, i8, i8, i8, [2 x i8], %struct.FILTER_INTRA_MODE_INFO, i8, [16 x i8], i8, i8, [4 x i8], i8, [2 x i8], i8, i8, i8, [2 x i8], i8, i8, i8 }
%struct.INTERINTER_COMPOUND_DATA = type { i8*, i8, i8, i8, i8 }
%struct.WarpedMotionParams = type { [8 x i32], i16, i16, i16, i16, i8, i8 }
%union.int_mv = type { i32 }
%union.int_interpfilters = type { i32 }
%struct.PALETTE_MODE_INFO = type { [24 x i16], [2 x i8] }
%struct.FILTER_INTRA_MODE_INFO = type { i8, i8 }
%struct.macroblockd = type { i32, i32, i32, i8, [3 x %struct.macroblockd_plane], %struct.TileInfo, %struct.MB_MODE_INFO**, i8, i8, i8, i8, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO*, i8*, i32, i32, i32, i32, i32, [2 x %struct.scale_factors*], %struct.yv12_buffer_config*, [3 x i8*], [3 x [32 x i8]], i8*, [32 x i8], i8*, i8*, [32 x i8], [8 x i8], [3 x %struct.WienerInfo], [3 x %struct.SgrprojInfo], i8, i8, [29 x i8], [29 x [8 x %struct.candidate_mv]], [29 x [8 x i16]], i8, [8 x i8], %struct.frame_contexts*, i32, [8 x i32], [8 x i32], i32, i32, %struct.aom_internal_error_info*, %struct.WarpedMotionParams*, i32, i32, i8, [4 x i8], [4 x i8], [7 x i8], [32768 x i8], [2 x i8*], %struct.cfl_ctx, %struct.dist_wtd_comp_params, [3 x i16], [3 x i16], [2 x i16], i16*, [2 x i8*], [12 x i8] }
%struct.macroblockd_plane = type { i32*, i32*, %struct.eob_info*, i8, i32, i32, %struct.buf_2d, [2 x %struct.buf_2d], i8*, i8*, [8 x [2 x i16]], i8*, i8, i8, [8 x [19 x i8*]], [8 x [19 x i8*]] }
%struct.eob_info = type { i16, i16 }
%struct.buf_2d = type { i8*, i8*, i32, i32, i32 }
%struct.TileInfo = type { i32, i32, i32, i32, i32, i32 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }
%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.yv12_buffer_config = type { %union.anon, %union.anon.0, %union.anon.2, %union.anon.4, %union.anon.6, %union.anon.8, i32, [3 x i8*], i8*, i32, i8*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i32, i32, i32, i32, i32, i32, %struct.aom_metadata_array* }
%union.anon = type { %struct.anon }
%struct.anon = type { i32, i32 }
%union.anon.0 = type { %struct.anon.1 }
%struct.anon.1 = type { i32, i32 }
%union.anon.2 = type { %struct.anon.3 }
%struct.anon.3 = type { i32, i32 }
%union.anon.4 = type { %struct.anon.5 }
%struct.anon.5 = type { i32, i32 }
%union.anon.6 = type { %struct.anon.7 }
%struct.anon.7 = type { i32, i32 }
%union.anon.8 = type { %struct.anon.9 }
%struct.anon.9 = type { i8*, i8*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }
%struct.WienerInfo = type { [8 x i16], [8 x i16] }
%struct.SgrprojInfo = type { i32, [2 x i32] }
%struct.candidate_mv = type { %union.int_mv, %union.int_mv }
%struct.frame_contexts = type { [5 x [13 x [3 x i16]]], [5 x [2 x [9 x [3 x i16]]]], [2 x [3 x [3 x i16]]], [2 x [2 x [6 x i16]]], [2 x [2 x [7 x i16]]], [2 x [2 x [8 x i16]]], [2 x [2 x [9 x i16]]], [2 x [2 x [10 x i16]]], [2 x [2 x [11 x i16]]], [2 x [2 x [12 x i16]]], [5 x [2 x [4 x [4 x i16]]]], [5 x [2 x [42 x [5 x i16]]]], [5 x [2 x [21 x [5 x i16]]]], [6 x [3 x i16]], [2 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [8 x [9 x i16]], [22 x [3 x i16]], [22 x [17 x i16]], [4 x [3 x i16]], [22 x [3 x i16]], [4 x [5 x i16]], [22 x [4 x i16]], [22 x [3 x i16]], [7 x [8 x i16]], [7 x [8 x i16]], [7 x [5 x [9 x i16]]], [7 x [5 x [9 x i16]]], [7 x [3 x [3 x i16]]], [2 x [3 x i16]], [5 x [3 x i16]], [3 x [6 x [3 x i16]]], [5 x [3 x i16]], [3 x [3 x [3 x i16]]], [3 x [3 x [3 x i16]]], [3 x [2 x [3 x i16]]], [21 x [3 x i16]], [6 x [3 x i16]], [6 x [3 x i16]], [3 x [3 x i16]], [3 x [3 x i16]], [4 x [3 x i16]], %struct.nmv_context, %struct.nmv_context, [3 x i16], %struct.segmentation_probs, [22 x [3 x i16]], [6 x i16], [4 x i16], [3 x i16], [3 x i16], [4 x [14 x i16]], [2 x [13 x [15 x i16]]], [20 x [11 x i16]], [16 x [4 x i16]], [5 x [5 x [14 x i16]]], [8 x [8 x i16]], [4 x [3 x [4 x i16]]], [5 x i16], [4 x [5 x i16]], [5 x i16], [3 x [4 x [13 x [17 x i16]]]], [4 x [4 x [17 x i16]]], [9 x i16], [6 x [17 x i16]], i32 }
%struct.nmv_context = type { [5 x i16], [2 x %struct.nmv_component] }
%struct.nmv_component = type { [12 x i16], [2 x [5 x i16]], [5 x i16], [3 x i16], [3 x i16], [3 x i16], [3 x i16], [10 x [3 x i16]] }
%struct.segmentation_probs = type { [9 x i16], [3 x [3 x i16]], [3 x [9 x i16]] }
%struct.aom_internal_error_info = type { i32, i32, [80 x i8], i32, [1 x %struct.__jmp_buf_tag] }
%struct.__jmp_buf_tag = type { [6 x i32], i32, [32 x i32] }
%struct.cfl_ctx = type { [1024 x i16], [1024 x i16], [2 x i32], i32, [2 x [32 x i16]], i32, i32, i32, i32, i32, i32 }
%struct.dist_wtd_comp_params = type { i32, i32, i32 }

@tx_size_wide_unit = internal constant [19 x i32] [i32 1, i32 2, i32 4, i32 8, i32 16, i32 1, i32 2, i32 2, i32 4, i32 4, i32 8, i32 8, i32 16, i32 1, i32 4, i32 2, i32 8, i32 4, i32 16], align 16
@tx_size_high_unit = internal constant [19 x i32] [i32 1, i32 2, i32 4, i32 8, i32 16, i32 2, i32 1, i32 4, i32 2, i32 8, i32 4, i32 16, i32 8, i32 4, i32 1, i32 8, i32 2, i32 16, i32 4], align 16
@mi_size_wide = internal constant [22 x i8] c"\01\01\02\02\02\04\04\04\08\08\08\10\10\10  \01\04\02\08\04\10", align 16
@mi_size_high = internal constant [22 x i8] c"\01\02\01\02\04\02\04\08\04\08\10\08\10 \10 \04\01\08\02\10\04", align 16
@block_size_wide = internal constant [22 x i8] c"\04\04\08\08\08\10\10\10   @@@\80\80\04\10\08 \10@", align 16
@block_size_high = internal constant [22 x i8] c"\04\08\04\08\10\08\10 \10 @ @\80@\80\10\04 \08@\10", align 16
@ss_size_lookup = internal constant [22 x [2 x [2 x i8]]] [[2 x [2 x i8]] zeroinitializer, [2 x [2 x i8]] [[2 x i8] c"\01\00", [2 x i8] c"\FF\00"], [2 x [2 x i8]] [[2 x i8] c"\02\FF", [2 x i8] zeroinitializer], [2 x [2 x i8]] [[2 x i8] c"\03\02", [2 x i8] c"\01\00"], [2 x [2 x i8]] [[2 x i8] c"\04\03", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\05\FF", [2 x i8] c"\03\02"], [2 x [2 x i8]] [[2 x i8] c"\06\05", [2 x i8] c"\04\03"], [2 x [2 x i8]] [[2 x i8] c"\07\06", [2 x i8] c"\FF\04"], [2 x [2 x i8]] [[2 x i8] c"\08\FF", [2 x i8] c"\06\05"], [2 x [2 x i8]] [[2 x i8] c"\09\08", [2 x i8] c"\07\06"], [2 x [2 x i8]] [[2 x i8] c"\0A\09", [2 x i8] c"\FF\07"], [2 x [2 x i8]] [[2 x i8] c"\0B\FF", [2 x i8] c"\09\08"], [2 x [2 x i8]] [[2 x i8] c"\0C\0B", [2 x i8] c"\0A\09"], [2 x [2 x i8]] [[2 x i8] c"\0D\0C", [2 x i8] c"\FF\0A"], [2 x [2 x i8]] [[2 x i8] c"\0E\FF", [2 x i8] c"\0C\0B"], [2 x [2 x i8]] [[2 x i8] c"\0F\0E", [2 x i8] c"\0D\0C"], [2 x [2 x i8]] [[2 x i8] c"\10\01", [2 x i8] c"\FF\01"], [2 x [2 x i8]] [[2 x i8] c"\11\FF", [2 x i8] c"\02\02"], [2 x [2 x i8]] [[2 x i8] c"\12\04", [2 x i8] c"\FF\10"], [2 x [2 x i8]] [[2 x i8] c"\13\FF", [2 x i8] c"\05\11"], [2 x [2 x i8]] [[2 x i8] c"\14\07", [2 x i8] c"\FF\12"], [2 x [2 x i8]] [[2 x i8] c"\15\FF", [2 x i8] c"\08\13"]], align 16

; Function Attrs: nounwind
define hidden zeroext i8 @av1_left_block_mode(%struct.MB_MODE_INFO* %left_mi) #0 {
entry:
  %retval = alloca i8, align 1
  %left_mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %left_mi, %struct.MB_MODE_INFO** %left_mi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.MB_MODE_INFO* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i8 0, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %left_mi.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 7
  %2 = load i8, i8* %mode, align 1, !tbaa !6
  store i8 %2, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8, i8* %retval, align 1
  ret i8 %3
}

; Function Attrs: nounwind
define hidden zeroext i8 @av1_above_block_mode(%struct.MB_MODE_INFO* %above_mi) #0 {
entry:
  %retval = alloca i8, align 1
  %above_mi.addr = alloca %struct.MB_MODE_INFO*, align 4
  store %struct.MB_MODE_INFO* %above_mi, %struct.MB_MODE_INFO** %above_mi.addr, align 4, !tbaa !2
  %0 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.MB_MODE_INFO* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i8 0, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.MB_MODE_INFO*, %struct.MB_MODE_INFO** %above_mi.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.MB_MODE_INFO, %struct.MB_MODE_INFO* %1, i32 0, i32 7
  %2 = load i8, i8* %mode, align 1, !tbaa !6
  store i8 %2, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8, i8* %retval, align 1
  ret i8 %3
}

; Function Attrs: nounwind
define hidden void @av1_set_entropy_contexts(%struct.macroblockd* %xd, %struct.macroblockd_plane* %pd, i32 %plane, i8 zeroext %plane_bsize, i8 zeroext %tx_size, i32 %has_eob, i32 %aoff, i32 %loff) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %pd.addr = alloca %struct.macroblockd_plane*, align 4
  %plane.addr = alloca i32, align 4
  %plane_bsize.addr = alloca i8, align 1
  %tx_size.addr = alloca i8, align 1
  %has_eob.addr = alloca i32, align 4
  %aoff.addr = alloca i32, align 4
  %loff.addr = alloca i32, align 4
  %a = alloca i8*, align 4
  %l = alloca i8*, align 4
  %txs_wide = alloca i32, align 4
  %txs_high = alloca i32, align 4
  %blocks_wide = alloca i32, align 4
  %above_contexts = alloca i32, align 4
  %blocks_high = alloca i32, align 4
  %left_contexts = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store %struct.macroblockd_plane* %pd, %struct.macroblockd_plane** %pd.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !14
  store i8 %plane_bsize, i8* %plane_bsize.addr, align 1, !tbaa !15
  store i8 %tx_size, i8* %tx_size.addr, align 1, !tbaa !15
  store i32 %has_eob, i32* %has_eob.addr, align 4, !tbaa !14
  store i32 %aoff, i32* %aoff.addr, align 4, !tbaa !14
  store i32 %loff, i32* %loff.addr, align 4, !tbaa !14
  %0 = bitcast i8** %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd.addr, align 4, !tbaa !2
  %above_entropy_context = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %1, i32 0, i32 8
  %2 = load i8*, i8** %above_entropy_context, align 4, !tbaa !16
  %3 = load i32, i32* %aoff.addr, align 4, !tbaa !14
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %3
  store i8* %add.ptr, i8** %a, align 4, !tbaa !2
  %4 = bitcast i8** %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd.addr, align 4, !tbaa !2
  %left_entropy_context = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %5, i32 0, i32 9
  %6 = load i8*, i8** %left_entropy_context, align 4, !tbaa !19
  %7 = load i32, i32* %loff.addr, align 4, !tbaa !14
  %add.ptr1 = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr1, i8** %l, align 4, !tbaa !2
  %8 = bitcast i32* %txs_wide to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i8, i8* %tx_size.addr, align 1, !tbaa !15
  %idxprom = zext i8 %9 to i32
  %arrayidx = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_wide_unit, i32 0, i32 %idxprom
  %10 = load i32, i32* %arrayidx, align 4, !tbaa !14
  store i32 %10, i32* %txs_wide, align 4, !tbaa !14
  %11 = bitcast i32* %txs_high to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i8, i8* %tx_size.addr, align 1, !tbaa !15
  %idxprom2 = zext i8 %12 to i32
  %arrayidx3 = getelementptr inbounds [19 x i32], [19 x i32]* @tx_size_high_unit, i32 0, i32 %idxprom2
  %13 = load i32, i32* %arrayidx3, align 4, !tbaa !14
  store i32 %13, i32* %txs_high, align 4, !tbaa !14
  %14 = load i32, i32* %has_eob.addr, align 4, !tbaa !14
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %15 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %15, i32 0, i32 18
  %16 = load i32, i32* %mb_to_right_edge, align 8, !tbaa !20
  %cmp = icmp slt i32 %16, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %17 = bitcast i32* %blocks_wide to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %19 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !15
  %20 = load i32, i32* %plane.addr, align 4, !tbaa !14
  %call = call i32 @max_block_wide(%struct.macroblockd* %18, i8 zeroext %19, i32 %20)
  store i32 %call, i32* %blocks_wide, align 4, !tbaa !14
  %21 = bitcast i32* %above_contexts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load i32, i32* %txs_wide, align 4, !tbaa !14
  %23 = load i32, i32* %blocks_wide, align 4, !tbaa !14
  %24 = load i32, i32* %aoff.addr, align 4, !tbaa !14
  %sub = sub nsw i32 %23, %24
  %cmp4 = icmp slt i32 %22, %sub
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %25 = load i32, i32* %txs_wide, align 4, !tbaa !14
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %26 = load i32, i32* %blocks_wide, align 4, !tbaa !14
  %27 = load i32, i32* %aoff.addr, align 4, !tbaa !14
  %sub5 = sub nsw i32 %26, %27
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %25, %cond.true ], [ %sub5, %cond.false ]
  store i32 %cond, i32* %above_contexts, align 4, !tbaa !14
  %28 = load i8*, i8** %a, align 4, !tbaa !2
  %29 = load i32, i32* %has_eob.addr, align 4, !tbaa !14
  %30 = trunc i32 %29 to i8
  %31 = load i32, i32* %above_contexts, align 4, !tbaa !14
  %mul = mul i32 1, %31
  call void @llvm.memset.p0i8.i32(i8* align 1 %28, i8 %30, i32 %mul, i1 false)
  %32 = load i8*, i8** %a, align 4, !tbaa !2
  %33 = load i32, i32* %above_contexts, align 4, !tbaa !14
  %add.ptr6 = getelementptr inbounds i8, i8* %32, i32 %33
  %34 = load i32, i32* %txs_wide, align 4, !tbaa !14
  %35 = load i32, i32* %above_contexts, align 4, !tbaa !14
  %sub7 = sub nsw i32 %34, %35
  %mul8 = mul i32 1, %sub7
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr6, i8 0, i32 %mul8, i1 false)
  %36 = bitcast i32* %above_contexts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %blocks_wide to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  %38 = load i8*, i8** %a, align 4, !tbaa !2
  %39 = load i32, i32* %has_eob.addr, align 4, !tbaa !14
  %40 = trunc i32 %39 to i8
  %41 = load i32, i32* %txs_wide, align 4, !tbaa !14
  %mul9 = mul i32 1, %41
  call void @llvm.memset.p0i8.i32(i8* align 1 %38, i8 %40, i32 %mul9, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %cond.end
  %42 = load i32, i32* %has_eob.addr, align 4, !tbaa !14
  %tobool10 = icmp ne i32 %42, 0
  br i1 %tobool10, label %land.lhs.true11, label %if.else26

land.lhs.true11:                                  ; preds = %if.end
  %43 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %43, i32 0, i32 20
  %44 = load i32, i32* %mb_to_bottom_edge, align 16, !tbaa !26
  %cmp12 = icmp slt i32 %44, 0
  br i1 %cmp12, label %if.then13, label %if.else26

if.then13:                                        ; preds = %land.lhs.true11
  %45 = bitcast i32* %blocks_high to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #4
  %46 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %47 = load i8, i8* %plane_bsize.addr, align 1, !tbaa !15
  %48 = load i32, i32* %plane.addr, align 4, !tbaa !14
  %call14 = call i32 @max_block_high(%struct.macroblockd* %46, i8 zeroext %47, i32 %48)
  store i32 %call14, i32* %blocks_high, align 4, !tbaa !14
  %49 = bitcast i32* %left_contexts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #4
  %50 = load i32, i32* %txs_high, align 4, !tbaa !14
  %51 = load i32, i32* %blocks_high, align 4, !tbaa !14
  %52 = load i32, i32* %loff.addr, align 4, !tbaa !14
  %sub15 = sub nsw i32 %51, %52
  %cmp16 = icmp slt i32 %50, %sub15
  br i1 %cmp16, label %cond.true17, label %cond.false18

cond.true17:                                      ; preds = %if.then13
  %53 = load i32, i32* %txs_high, align 4, !tbaa !14
  br label %cond.end20

cond.false18:                                     ; preds = %if.then13
  %54 = load i32, i32* %blocks_high, align 4, !tbaa !14
  %55 = load i32, i32* %loff.addr, align 4, !tbaa !14
  %sub19 = sub nsw i32 %54, %55
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false18, %cond.true17
  %cond21 = phi i32 [ %53, %cond.true17 ], [ %sub19, %cond.false18 ]
  store i32 %cond21, i32* %left_contexts, align 4, !tbaa !14
  %56 = load i8*, i8** %l, align 4, !tbaa !2
  %57 = load i32, i32* %has_eob.addr, align 4, !tbaa !14
  %58 = trunc i32 %57 to i8
  %59 = load i32, i32* %left_contexts, align 4, !tbaa !14
  %mul22 = mul i32 1, %59
  call void @llvm.memset.p0i8.i32(i8* align 1 %56, i8 %58, i32 %mul22, i1 false)
  %60 = load i8*, i8** %l, align 4, !tbaa !2
  %61 = load i32, i32* %left_contexts, align 4, !tbaa !14
  %add.ptr23 = getelementptr inbounds i8, i8* %60, i32 %61
  %62 = load i32, i32* %txs_high, align 4, !tbaa !14
  %63 = load i32, i32* %left_contexts, align 4, !tbaa !14
  %sub24 = sub nsw i32 %62, %63
  %mul25 = mul i32 1, %sub24
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr23, i8 0, i32 %mul25, i1 false)
  %64 = bitcast i32* %left_contexts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #4
  %65 = bitcast i32* %blocks_high to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  br label %if.end28

if.else26:                                        ; preds = %land.lhs.true11, %if.end
  %66 = load i8*, i8** %l, align 4, !tbaa !2
  %67 = load i32, i32* %has_eob.addr, align 4, !tbaa !14
  %68 = trunc i32 %67 to i8
  %69 = load i32, i32* %txs_high, align 4, !tbaa !14
  %mul27 = mul i32 1, %69
  call void @llvm.memset.p0i8.i32(i8* align 1 %66, i8 %68, i32 %mul27, i1 false)
  br label %if.end28

if.end28:                                         ; preds = %if.else26, %cond.end20
  %70 = bitcast i32* %txs_high to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %txs_wide to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %72 = bitcast i8** %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  %73 = bitcast i8** %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @max_block_wide(%struct.macroblockd* %xd, i8 zeroext %bsize, i32 %plane) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %max_blocks_wide = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !15
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !14
  %0 = bitcast i32* %max_blocks_wide to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !15
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_wide, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !15
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %max_blocks_wide, align 4, !tbaa !14
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 18
  %4 = load i32, i32* %mb_to_right_edge, align 8, !tbaa !20
  %cmp = icmp slt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 4
  %7 = load i32, i32* %plane.addr, align 4, !tbaa !14
  %arrayidx3 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane2, i32 0, i32 %7
  store %struct.macroblockd_plane* %arrayidx3, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_right_edge4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 18
  %9 = load i32, i32* %mb_to_right_edge4, align 8, !tbaa !20
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 4
  %11 = load i32, i32* %subsampling_x, align 4, !tbaa !27
  %add = add nsw i32 3, %11
  %shr = ashr i32 %9, %add
  %12 = load i32, i32* %max_blocks_wide, align 4, !tbaa !14
  %add5 = add nsw i32 %12, %shr
  store i32 %add5, i32* %max_blocks_wide, align 4, !tbaa !14
  %13 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load i32, i32* %max_blocks_wide, align 4, !tbaa !14
  %shr6 = ashr i32 %14, 2
  %15 = bitcast i32* %max_blocks_wide to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret i32 %shr6
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @max_block_high(%struct.macroblockd* %xd, i8 zeroext %bsize, i32 %plane) #2 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i8, align 1
  %plane.addr = alloca i32, align 4
  %max_blocks_high = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !15
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !14
  %0 = bitcast i32* %max_blocks_high to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %bsize.addr, align 1, !tbaa !15
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [22 x i8], [22 x i8]* @block_size_high, i32 0, i32 %idxprom
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !15
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %max_blocks_high, align 4, !tbaa !14
  %3 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %3, i32 0, i32 20
  %4 = load i32, i32* %mb_to_bottom_edge, align 16, !tbaa !26
  %cmp = icmp slt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane2 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 4
  %7 = load i32, i32* %plane.addr, align 4, !tbaa !14
  %arrayidx3 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane2, i32 0, i32 %7
  store %struct.macroblockd_plane* %arrayidx3, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %mb_to_bottom_edge4 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 20
  %9 = load i32, i32* %mb_to_bottom_edge4, align 16, !tbaa !26
  %10 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %10, i32 0, i32 5
  %11 = load i32, i32* %subsampling_y, align 4, !tbaa !28
  %add = add nsw i32 3, %11
  %shr = ashr i32 %9, %add
  %12 = load i32, i32* %max_blocks_high, align 4, !tbaa !14
  %add5 = add nsw i32 %12, %shr
  store i32 %add5, i32* %max_blocks_high, align 4, !tbaa !14
  %13 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load i32, i32* %max_blocks_high, align 4, !tbaa !14
  %shr6 = ashr i32 %14, 2
  %15 = bitcast i32* %max_blocks_high to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret i32 %shr6
}

; Function Attrs: nounwind
define hidden void @av1_reset_entropy_context(%struct.macroblockd* %xd, i8 zeroext %bsize, i32 %num_planes) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %bsize.addr = alloca i8, align 1
  %num_planes.addr = alloca i32, align 4
  %nplanes = alloca i32, align 4
  %i = alloca i32, align 4
  %pd = alloca %struct.macroblockd_plane*, align 4
  %plane_bsize = alloca i8, align 1
  %txs_wide = alloca i32, align 4
  %txs_high = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !15
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !14
  %0 = bitcast i32* %nplanes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %num_planes.addr, align 4, !tbaa !14
  %sub = sub nsw i32 %1, 1
  %2 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %is_chroma_ref = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %2, i32 0, i32 3
  %3 = load i8, i8* %is_chroma_ref, align 4, !tbaa !29, !range !30
  %tobool = trunc i8 %3 to i1
  %conv = zext i1 %tobool to i32
  %mul = mul nsw i32 %sub, %conv
  %add = add nsw i32 1, %mul
  store i32 %add, i32* %nplanes, align 4, !tbaa !14
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !14
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !14
  %6 = load i32, i32* %nplanes, align 4, !tbaa !14
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %8 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %9, i32 0, i32 4
  %10 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 %10
  store %struct.macroblockd_plane* %arrayidx, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %plane_bsize) #4
  %11 = load i8, i8* %bsize.addr, align 1, !tbaa !15
  %12 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %12, i32 0, i32 4
  %13 = load i32, i32* %subsampling_x, align 4, !tbaa !27
  %14 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %14, i32 0, i32 5
  %15 = load i32, i32* %subsampling_y, align 4, !tbaa !28
  %call = call zeroext i8 @get_plane_block_size(i8 zeroext %11, i32 %13, i32 %15)
  store i8 %call, i8* %plane_bsize, align 1, !tbaa !15
  %16 = bitcast i32* %txs_wide to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i8, i8* %plane_bsize, align 1, !tbaa !15
  %idxprom = zext i8 %17 to i32
  %arrayidx2 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_wide, i32 0, i32 %idxprom
  %18 = load i8, i8* %arrayidx2, align 1, !tbaa !15
  %conv3 = zext i8 %18 to i32
  store i32 %conv3, i32* %txs_wide, align 4, !tbaa !14
  %19 = bitcast i32* %txs_high to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load i8, i8* %plane_bsize, align 1, !tbaa !15
  %idxprom4 = zext i8 %20 to i32
  %arrayidx5 = getelementptr inbounds [22 x i8], [22 x i8]* @mi_size_high, i32 0, i32 %idxprom4
  %21 = load i8, i8* %arrayidx5, align 1, !tbaa !15
  %conv6 = zext i8 %21 to i32
  store i32 %conv6, i32* %txs_high, align 4, !tbaa !14
  %22 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %above_entropy_context = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %22, i32 0, i32 8
  %23 = load i8*, i8** %above_entropy_context, align 4, !tbaa !16
  %24 = load i32, i32* %txs_wide, align 4, !tbaa !14
  %mul7 = mul i32 1, %24
  call void @llvm.memset.p0i8.i32(i8* align 1 %23, i8 0, i32 %mul7, i1 false)
  %25 = load %struct.macroblockd_plane*, %struct.macroblockd_plane** %pd, align 4, !tbaa !2
  %left_entropy_context = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %25, i32 0, i32 9
  %26 = load i8*, i8** %left_entropy_context, align 4, !tbaa !19
  %27 = load i32, i32* %txs_high, align 4, !tbaa !14
  %mul8 = mul i32 1, %27
  call void @llvm.memset.p0i8.i32(i8* align 1 %26, i8 0, i32 %mul8, i1 false)
  %28 = bitcast i32* %txs_high to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  %29 = bitcast i32* %txs_wide to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %plane_bsize) #4
  %30 = bitcast %struct.macroblockd_plane** %pd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %31 = load i32, i32* %i, align 4, !tbaa !14
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %i, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %32 = bitcast i32* %nplanes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_plane_block_size(i8 zeroext %bsize, i32 %subsampling_x, i32 %subsampling_y) #2 {
entry:
  %bsize.addr = alloca i8, align 1
  %subsampling_x.addr = alloca i32, align 4
  %subsampling_y.addr = alloca i32, align 4
  store i8 %bsize, i8* %bsize.addr, align 1, !tbaa !15
  store i32 %subsampling_x, i32* %subsampling_x.addr, align 4, !tbaa !14
  store i32 %subsampling_y, i32* %subsampling_y.addr, align 4, !tbaa !14
  %0 = load i8, i8* %bsize.addr, align 1, !tbaa !15
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [22 x [2 x [2 x i8]]], [22 x [2 x [2 x i8]]]* @ss_size_lookup, i32 0, i32 %idxprom
  %1 = load i32, i32* %subsampling_x.addr, align 4, !tbaa !14
  %arrayidx1 = getelementptr inbounds [2 x [2 x i8]], [2 x [2 x i8]]* %arrayidx, i32 0, i32 %1
  %2 = load i32, i32* %subsampling_y.addr, align 4, !tbaa !14
  %arrayidx2 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx1, i32 0, i32 %2
  %3 = load i8, i8* %arrayidx2, align 1, !tbaa !15
  ret i8 %3
}

; Function Attrs: nounwind
define hidden void @av1_reset_loop_filter_delta(%struct.macroblockd* %xd, i32 %num_planes) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %num_planes.addr = alloca i32, align 4
  %frame_lf_count = alloca i32, align 4
  %lf_id = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !14
  %0 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %delta_lf_from_base = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %0, i32 0, i32 50
  store i8 0, i8* %delta_lf_from_base, align 16, !tbaa !31
  %1 = bitcast i32* %frame_lf_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %num_planes.addr, align 4, !tbaa !14
  %cmp = icmp sgt i32 %2, 1
  %3 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 4, i32 2
  store i32 %cond, i32* %frame_lf_count, align 4, !tbaa !14
  %4 = bitcast i32* %lf_id to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %lf_id, align 4, !tbaa !14
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %lf_id, align 4, !tbaa !14
  %6 = load i32, i32* %frame_lf_count, align 4, !tbaa !14
  %cmp1 = icmp slt i32 %5, %6
  br i1 %cmp1, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %lf_id to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %delta_lf = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 51
  %9 = load i32, i32* %lf_id, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %delta_lf, i32 0, i32 %9
  store i8 0, i8* %arrayidx, align 1, !tbaa !15
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %lf_id, align 4, !tbaa !14
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %lf_id, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %11 = bitcast i32* %frame_lf_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_reset_loop_restoration(%struct.macroblockd* %xd, i32 %num_planes) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %num_planes.addr = alloca i32, align 4
  %p = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !14
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %p, align 4, !tbaa !14
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %p, align 4, !tbaa !14
  %2 = load i32, i32* %num_planes.addr, align 4, !tbaa !14
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %wiener_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 31
  %arraydecay = getelementptr inbounds [3 x %struct.WienerInfo], [3 x %struct.WienerInfo]* %wiener_info, i32 0, i32 0
  %5 = load i32, i32* %p, align 4, !tbaa !14
  %add.ptr = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %arraydecay, i32 %5
  call void @set_default_wiener(%struct.WienerInfo* %add.ptr)
  %6 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %sgrproj_info = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %6, i32 0, i32 32
  %arraydecay1 = getelementptr inbounds [3 x %struct.SgrprojInfo], [3 x %struct.SgrprojInfo]* %sgrproj_info, i32 0, i32 0
  %7 = load i32, i32* %p, align 4, !tbaa !14
  %add.ptr2 = getelementptr inbounds %struct.SgrprojInfo, %struct.SgrprojInfo* %arraydecay1, i32 %7
  call void @set_default_sgrproj(%struct.SgrprojInfo* %add.ptr2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %p, align 4, !tbaa !14
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %p, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @set_default_wiener(%struct.WienerInfo* %wiener_info) #2 {
entry:
  %wiener_info.addr = alloca %struct.WienerInfo*, align 4
  store %struct.WienerInfo* %wiener_info, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %0 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %hfilter = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %0, i32 0, i32 1
  %arrayidx = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter, i32 0, i32 0
  store i16 3, i16* %arrayidx, align 16, !tbaa !32
  %1 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %vfilter = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %1, i32 0, i32 0
  %arrayidx1 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter, i32 0, i32 0
  store i16 3, i16* %arrayidx1, align 16, !tbaa !32
  %2 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %hfilter2 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %2, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter2, i32 0, i32 1
  store i16 -7, i16* %arrayidx3, align 2, !tbaa !32
  %3 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %vfilter4 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter4, i32 0, i32 1
  store i16 -7, i16* %arrayidx5, align 2, !tbaa !32
  %4 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %hfilter6 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %4, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter6, i32 0, i32 2
  store i16 15, i16* %arrayidx7, align 4, !tbaa !32
  %5 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %vfilter8 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %5, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter8, i32 0, i32 2
  store i16 15, i16* %arrayidx9, align 4, !tbaa !32
  %6 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %hfilter10 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %6, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter10, i32 0, i32 3
  store i16 -22, i16* %arrayidx11, align 2, !tbaa !32
  %7 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %vfilter12 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter12, i32 0, i32 3
  store i16 -22, i16* %arrayidx13, align 2, !tbaa !32
  %8 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %hfilter14 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %8, i32 0, i32 1
  %arrayidx15 = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter14, i32 0, i32 4
  store i16 15, i16* %arrayidx15, align 8, !tbaa !32
  %9 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %vfilter16 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %9, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter16, i32 0, i32 4
  store i16 15, i16* %arrayidx17, align 8, !tbaa !32
  %10 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %hfilter18 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %10, i32 0, i32 1
  %arrayidx19 = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter18, i32 0, i32 5
  store i16 -7, i16* %arrayidx19, align 2, !tbaa !32
  %11 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %vfilter20 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %11, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter20, i32 0, i32 5
  store i16 -7, i16* %arrayidx21, align 2, !tbaa !32
  %12 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %hfilter22 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %12, i32 0, i32 1
  %arrayidx23 = getelementptr inbounds [8 x i16], [8 x i16]* %hfilter22, i32 0, i32 6
  store i16 3, i16* %arrayidx23, align 4, !tbaa !32
  %13 = load %struct.WienerInfo*, %struct.WienerInfo** %wiener_info.addr, align 4, !tbaa !2
  %vfilter24 = getelementptr inbounds %struct.WienerInfo, %struct.WienerInfo* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [8 x i16], [8 x i16]* %vfilter24, i32 0, i32 6
  store i16 3, i16* %arrayidx25, align 4, !tbaa !32
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @set_default_sgrproj(%struct.SgrprojInfo* %sgrproj_info) #2 {
entry:
  %sgrproj_info.addr = alloca %struct.SgrprojInfo*, align 4
  store %struct.SgrprojInfo* %sgrproj_info, %struct.SgrprojInfo** %sgrproj_info.addr, align 4, !tbaa !2
  %0 = load %struct.SgrprojInfo*, %struct.SgrprojInfo** %sgrproj_info.addr, align 4, !tbaa !2
  %xqd = getelementptr inbounds %struct.SgrprojInfo, %struct.SgrprojInfo* %0, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %xqd, i32 0, i32 0
  store i32 -32, i32* %arrayidx, align 4, !tbaa !14
  %1 = load %struct.SgrprojInfo*, %struct.SgrprojInfo** %sgrproj_info.addr, align 4, !tbaa !2
  %xqd1 = getelementptr inbounds %struct.SgrprojInfo, %struct.SgrprojInfo* %1, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [2 x i32], [2 x i32]* %xqd1, i32 0, i32 1
  store i32 31, i32* %arrayidx2, align 4, !tbaa !14
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_setup_block_planes(%struct.macroblockd* %xd, i32 %ss_x, i32 %ss_y, i32 %num_planes) #0 {
entry:
  %xd.addr = alloca %struct.macroblockd*, align 4
  %ss_x.addr = alloca i32, align 4
  %ss_y.addr = alloca i32, align 4
  %num_planes.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.macroblockd* %xd, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  store i32 %ss_x, i32* %ss_x.addr, align 4, !tbaa !14
  store i32 %ss_y, i32* %ss_y.addr, align 4, !tbaa !14
  store i32 %num_planes, i32* %num_planes.addr, align 4, !tbaa !14
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !14
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !14
  %2 = load i32, i32* %num_planes.addr, align 4, !tbaa !14
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4, !tbaa !14
  %call = call zeroext i8 @get_plane_type(i32 %3)
  %4 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %4, i32 0, i32 4
  %5 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane, i32 0, i32 %5
  %plane_type = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx, i32 0, i32 3
  store i8 %call, i8* %plane_type, align 4, !tbaa !33
  %6 = load i32, i32* %i, align 4, !tbaa !14
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %7 = load i32, i32* %ss_x.addr, align 4, !tbaa !14
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %7, %cond.true ], [ 0, %cond.false ]
  %8 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane1 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %8, i32 0, i32 4
  %9 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx2 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane1, i32 0, i32 %9
  %subsampling_x = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx2, i32 0, i32 4
  store i32 %cond, i32* %subsampling_x, align 4, !tbaa !27
  %10 = load i32, i32* %i, align 4, !tbaa !14
  %tobool3 = icmp ne i32 %10, 0
  br i1 %tobool3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %11 = load i32, i32* %ss_y.addr, align 4, !tbaa !14
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi i32 [ %11, %cond.true4 ], [ 0, %cond.false5 ]
  %12 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane8 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %12, i32 0, i32 4
  %13 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx9 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane8, i32 0, i32 %13
  %subsampling_y = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx9, i32 0, i32 5
  store i32 %cond7, i32* %subsampling_y, align 4, !tbaa !28
  br label %for.inc

for.inc:                                          ; preds = %cond.end6
  %14 = load i32, i32* %i, align 4, !tbaa !14
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load i32, i32* %num_planes.addr, align 4, !tbaa !14
  store i32 %15, i32* %i, align 4, !tbaa !14
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc19, %for.end
  %16 = load i32, i32* %i, align 4, !tbaa !14
  %cmp11 = icmp slt i32 %16, 3
  br i1 %cmp11, label %for.body12, label %for.end21

for.body12:                                       ; preds = %for.cond10
  %17 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane13 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %17, i32 0, i32 4
  %18 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx14 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane13, i32 0, i32 %18
  %subsampling_x15 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx14, i32 0, i32 4
  store i32 1, i32* %subsampling_x15, align 4, !tbaa !27
  %19 = load %struct.macroblockd*, %struct.macroblockd** %xd.addr, align 4, !tbaa !2
  %plane16 = getelementptr inbounds %struct.macroblockd, %struct.macroblockd* %19, i32 0, i32 4
  %20 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx17 = getelementptr inbounds [3 x %struct.macroblockd_plane], [3 x %struct.macroblockd_plane]* %plane16, i32 0, i32 %20
  %subsampling_y18 = getelementptr inbounds %struct.macroblockd_plane, %struct.macroblockd_plane* %arrayidx17, i32 0, i32 5
  store i32 1, i32* %subsampling_y18, align 4, !tbaa !28
  br label %for.inc19

for.inc19:                                        ; preds = %for.body12
  %21 = load i32, i32* %i, align 4, !tbaa !14
  %inc20 = add nsw i32 %21, 1
  store i32 %inc20, i32* %i, align 4, !tbaa !14
  br label %for.cond10

for.end21:                                        ; preds = %for.cond10
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_plane_type(i32 %plane) #2 {
entry:
  %plane.addr = alloca i32, align 4
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !14
  %0 = load i32, i32* %plane.addr, align 4, !tbaa !14
  %cmp = icmp eq i32 %0, 0
  %1 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 0, i32 1
  %conv = trunc i32 %cond to i8
  ret i8 %conv
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !4, i64 119}
!7 = !{!"MB_MODE_INFO", !8, i64 0, !9, i64 8, !4, i64 52, !11, i64 60, !4, i64 64, !12, i64 68, !4, i64 118, !4, i64 119, !4, i64 120, !4, i64 121, !4, i64 122, !4, i64 123, !4, i64 124, !13, i64 126, !4, i64 128, !4, i64 129, !4, i64 145, !4, i64 146, !4, i64 147, !4, i64 151, !4, i64 152, !4, i64 154, !4, i64 155, !4, i64 156, !4, i64 157, !4, i64 159, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 160, !4, i64 161, !4, i64 161, !4, i64 161}
!8 = !{!"", !3, i64 0, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7}
!9 = !{!"", !4, i64 0, !10, i64 32, !10, i64 34, !10, i64 36, !10, i64 38, !4, i64 40, !4, i64 41}
!10 = !{!"short", !4, i64 0}
!11 = !{!"int", !4, i64 0}
!12 = !{!"", !4, i64 0, !4, i64 48}
!13 = !{!"", !4, i64 0, !4, i64 1}
!14 = !{!11, !11, i64 0}
!15 = !{!4, !4, i64 0}
!16 = !{!17, !3, i64 84}
!17 = !{!"macroblockd_plane", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12, !11, i64 16, !11, i64 20, !18, i64 24, !4, i64 44, !3, i64 84, !3, i64 88, !4, i64 92, !3, i64 124, !4, i64 128, !4, i64 129, !4, i64 132, !4, i64 740}
!18 = !{!"buf_2d", !3, i64 0, !3, i64 4, !11, i64 8, !11, i64 12, !11, i64 16}
!19 = !{!17, !3, i64 88}
!20 = !{!21, !11, i64 4120}
!21 = !{!"macroblockd", !11, i64 0, !11, i64 4, !11, i64 8, !22, i64 12, !4, i64 16, !23, i64 4060, !3, i64 4084, !22, i64 4088, !22, i64 4089, !22, i64 4090, !22, i64 4091, !3, i64 4092, !3, i64 4096, !3, i64 4100, !3, i64 4104, !3, i64 4108, !11, i64 4112, !11, i64 4116, !11, i64 4120, !11, i64 4124, !11, i64 4128, !4, i64 4132, !3, i64 4140, !4, i64 4144, !4, i64 4156, !3, i64 4252, !4, i64 4256, !3, i64 4288, !3, i64 4292, !4, i64 4296, !4, i64 4336, !4, i64 4432, !4, i64 4468, !4, i64 4469, !4, i64 4470, !4, i64 4500, !4, i64 6356, !4, i64 6820, !4, i64 6821, !3, i64 6832, !11, i64 6836, !4, i64 6840, !4, i64 6872, !11, i64 6904, !11, i64 6908, !3, i64 6912, !3, i64 6916, !11, i64 6920, !11, i64 6924, !4, i64 6928, !4, i64 6929, !4, i64 6933, !4, i64 6944, !4, i64 39712, !24, i64 39720, !25, i64 43980, !4, i64 43992, !4, i64 43998, !4, i64 44004, !3, i64 44008, !4, i64 44012}
!22 = !{!"_Bool", !4, i64 0}
!23 = !{!"TileInfo", !11, i64 0, !11, i64 4, !11, i64 8, !11, i64 12, !11, i64 16, !11, i64 20}
!24 = !{!"cfl_ctx", !4, i64 0, !4, i64 2048, !4, i64 4096, !11, i64 4104, !4, i64 4108, !11, i64 4236, !11, i64 4240, !11, i64 4244, !11, i64 4248, !11, i64 4252, !11, i64 4256}
!25 = !{!"dist_wtd_comp_params", !11, i64 0, !11, i64 4, !11, i64 8}
!26 = !{!21, !11, i64 4128}
!27 = !{!17, !11, i64 16}
!28 = !{!17, !11, i64 20}
!29 = !{!21, !22, i64 12}
!30 = !{i8 0, i8 2}
!31 = !{!21, !4, i64 6928}
!32 = !{!10, !10, i64 0}
!33 = !{!17, !4, i64 12}
