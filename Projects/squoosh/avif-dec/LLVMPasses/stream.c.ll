; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-dec/src/stream.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-dec/src/stream.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifROStream = type { %struct.avifROData*, i32 }
%struct.avifROData = type { i8*, i32 }
%struct.avifBoxHeader = type { i32, [4 x i8] }
%struct.avifRWStream = type { %struct.avifRWData*, i32 }
%struct.avifRWData = type { i8*, i32 }

@.str = private unnamed_addr constant [5 x i8] c"uuid\00", align 1

; Function Attrs: nounwind
define hidden i8* @avifROStreamCurrent(%struct.avifROStream* %stream) #0 {
entry:
  %stream.addr = alloca %struct.avifROStream*, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %0, i32 0, i32 0
  %1 = load %struct.avifROData*, %struct.avifROData** %raw, align 4, !tbaa !6
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %1, i32 0, i32 0
  %2 = load i8*, i8** %data, align 4, !tbaa !9
  %3 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %3, i32 0, i32 1
  %4 = load i32, i32* %offset, align 4, !tbaa !11
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %4
  ret i8* %add.ptr
}

; Function Attrs: nounwind
define hidden void @avifROStreamStart(%struct.avifROStream* %stream, %struct.avifROData* %raw) #0 {
entry:
  %stream.addr = alloca %struct.avifROStream*, align 4
  %raw.addr = alloca %struct.avifROData*, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store %struct.avifROData* %raw, %struct.avifROData** %raw.addr, align 4, !tbaa !2
  %0 = load %struct.avifROData*, %struct.avifROData** %raw.addr, align 4, !tbaa !2
  %1 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %raw1 = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %1, i32 0, i32 0
  store %struct.avifROData* %0, %struct.avifROData** %raw1, align 4, !tbaa !6
  %2 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %2, i32 0, i32 1
  store i32 0, i32* %offset, align 4, !tbaa !11
  ret void
}

; Function Attrs: nounwind
define hidden i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %stream, i32 %byteCount) #0 {
entry:
  %stream.addr = alloca %struct.avifROStream*, align 4
  %byteCount.addr = alloca i32, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i32 %byteCount, i32* %byteCount.addr, align 4, !tbaa !12
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %0, i32 0, i32 1
  %1 = load i32, i32* %offset, align 4, !tbaa !11
  %2 = load i32, i32* %byteCount.addr, align 4, !tbaa !12
  %add = add i32 %1, %2
  %3 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %3, i32 0, i32 0
  %4 = load %struct.avifROData*, %struct.avifROData** %raw, align 4, !tbaa !6
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %4, i32 0, i32 1
  %5 = load i32, i32* %size, align 4, !tbaa !13
  %cmp = icmp ule i32 %add, %5
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define hidden i32 @avifROStreamRemainingBytes(%struct.avifROStream* %stream) #0 {
entry:
  %stream.addr = alloca %struct.avifROStream*, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %0, i32 0, i32 0
  %1 = load %struct.avifROData*, %struct.avifROData** %raw, align 4, !tbaa !6
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %1, i32 0, i32 1
  %2 = load i32, i32* %size, align 4, !tbaa !13
  %3 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %3, i32 0, i32 1
  %4 = load i32, i32* %offset, align 4, !tbaa !11
  %sub = sub i32 %2, %4
  ret i32 %sub
}

; Function Attrs: nounwind
define hidden i32 @avifROStreamOffset(%struct.avifROStream* %stream) #0 {
entry:
  %stream.addr = alloca %struct.avifROStream*, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %0, i32 0, i32 1
  %1 = load i32, i32* %offset, align 4, !tbaa !11
  ret i32 %1
}

; Function Attrs: nounwind
define hidden void @avifROStreamSetOffset(%struct.avifROStream* %stream, i32 %offset) #0 {
entry:
  %stream.addr = alloca %struct.avifROStream*, align 4
  %offset.addr = alloca i32, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i32 %offset, i32* %offset.addr, align 4, !tbaa !12
  %0 = load i32, i32* %offset.addr, align 4, !tbaa !12
  %1 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset1 = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %1, i32 0, i32 1
  store i32 %0, i32* %offset1, align 4, !tbaa !11
  %2 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset2 = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %2, i32 0, i32 1
  %3 = load i32, i32* %offset2, align 4, !tbaa !11
  %4 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %4, i32 0, i32 0
  %5 = load %struct.avifROData*, %struct.avifROData** %raw, align 4, !tbaa !6
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %5, i32 0, i32 1
  %6 = load i32, i32* %size, align 4, !tbaa !13
  %cmp = icmp ugt i32 %3, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %raw3 = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %7, i32 0, i32 0
  %8 = load %struct.avifROData*, %struct.avifROData** %raw3, align 4, !tbaa !6
  %size4 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %8, i32 0, i32 1
  %9 = load i32, i32* %size4, align 4, !tbaa !13
  %10 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset5 = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %10, i32 0, i32 1
  store i32 %9, i32* %offset5, align 4, !tbaa !11
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define hidden i32 @avifROStreamSkip(%struct.avifROStream* %stream, i32 %byteCount) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %byteCount.addr = alloca i32, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i32 %byteCount, i32* %byteCount.addr, align 4, !tbaa !12
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %1 = load i32, i32* %byteCount.addr, align 4, !tbaa !12
  %call = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %0, i32 %1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %byteCount.addr, align 4, !tbaa !12
  %3 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %3, i32 0, i32 1
  %4 = load i32, i32* %offset, align 4, !tbaa !11
  %add = add i32 %4, %2
  store i32 %add, i32* %offset, align 4, !tbaa !11
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: nounwind
define hidden i32 @avifROStreamRead(%struct.avifROStream* %stream, i8* %data, i32 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %data.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !12
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %1 = load i32, i32* %size.addr, align 4, !tbaa !12
  %call = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %0, i32 %1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %3 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %3, i32 0, i32 0
  %4 = load %struct.avifROData*, %struct.avifROData** %raw, align 4, !tbaa !6
  %data1 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %4, i32 0, i32 0
  %5 = load i8*, i8** %data1, align 4, !tbaa !9
  %6 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %6, i32 0, i32 1
  %7 = load i32, i32* %offset, align 4, !tbaa !11
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %7
  %8 = load i32, i32* %size.addr, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 %8, i1 false)
  %9 = load i32, i32* %size.addr, align 4, !tbaa !12
  %10 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset2 = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %10, i32 0, i32 1
  %11 = load i32, i32* %offset2, align 4, !tbaa !11
  %add = add i32 %11, %9
  store i32 %add, i32* %offset2, align 4, !tbaa !11
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden i32 @avifROStreamReadUX8(%struct.avifROStream* %stream, i64* %v, i64 %factor) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %v.addr = alloca i64*, align 4
  %factor.addr = alloca i64, align 8
  %tmp = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %tmp8 = alloca i16, align 2
  %tmp24 = alloca i32, align 4
  %tmp40 = alloca i64, align 8
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i64* %v, i64** %v.addr, align 4, !tbaa !2
  store i64 %factor, i64* %factor.addr, align 8, !tbaa !14
  %0 = load i64, i64* %factor.addr, align 8, !tbaa !14
  %cmp = icmp eq i64 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i64*, i64** %v.addr, align 4, !tbaa !2
  store i64 0, i64* %1, align 8, !tbaa !14
  br label %if.end56

if.else:                                          ; preds = %entry
  %2 = load i64, i64* %factor.addr, align 8, !tbaa !14
  %cmp1 = icmp eq i64 %2, 1
  br i1 %cmp1, label %if.then2, label %if.else4

if.then2:                                         ; preds = %if.else
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tmp) #4
  br label %do.body

do.body:                                          ; preds = %if.then2
  %3 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %3, i8* %tmp, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = load i8, i8* %tmp, align 1, !tbaa !16
  %conv = zext i8 %4 to i64
  %5 = load i64*, i64** %v.addr, align 4, !tbaa !2
  store i64 %conv, i64* %5, align 8, !tbaa !14
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end, %if.then3
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tmp) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end55

if.else4:                                         ; preds = %if.else
  %6 = load i64, i64* %factor.addr, align 8, !tbaa !14
  %cmp5 = icmp eq i64 %6, 2
  br i1 %cmp5, label %if.then7, label %if.else20

if.then7:                                         ; preds = %if.else4
  %7 = bitcast i16* %tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #4
  br label %do.body9

do.body9:                                         ; preds = %if.then7
  %8 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call10 = call i32 @avifROStreamReadU16(%struct.avifROStream* %8, i16* %tmp8)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %do.body9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

if.end13:                                         ; preds = %do.body9
  br label %do.cond14

do.cond14:                                        ; preds = %if.end13
  br label %do.end15

do.end15:                                         ; preds = %do.cond14
  %9 = load i16, i16* %tmp8, align 2, !tbaa !17
  %conv16 = zext i16 %9 to i64
  %10 = load i64*, i64** %v.addr, align 4, !tbaa !2
  store i64 %conv16, i64* %10, align 8, !tbaa !14
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

cleanup17:                                        ; preds = %do.end15, %if.then12
  %11 = bitcast i16* %tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %11) #4
  %cleanup.dest18 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest18, label %unreachable [
    i32 0, label %cleanup.cont19
    i32 1, label %return
  ]

cleanup.cont19:                                   ; preds = %cleanup17
  br label %if.end54

if.else20:                                        ; preds = %if.else4
  %12 = load i64, i64* %factor.addr, align 8, !tbaa !14
  %cmp21 = icmp eq i64 %12, 4
  br i1 %cmp21, label %if.then23, label %if.else36

if.then23:                                        ; preds = %if.else20
  %13 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  br label %do.body25

do.body25:                                        ; preds = %if.then23
  %14 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call26 = call i32 @avifROStreamReadU32(%struct.avifROStream* %14, i32* %tmp24)
  %tobool27 = icmp ne i32 %call26, 0
  br i1 %tobool27, label %if.end29, label %if.then28

if.then28:                                        ; preds = %do.body25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

if.end29:                                         ; preds = %do.body25
  br label %do.cond30

do.cond30:                                        ; preds = %if.end29
  br label %do.end31

do.end31:                                         ; preds = %do.cond30
  %15 = load i32, i32* %tmp24, align 4, !tbaa !19
  %conv32 = zext i32 %15 to i64
  %16 = load i64*, i64** %v.addr, align 4, !tbaa !2
  store i64 %conv32, i64* %16, align 8, !tbaa !14
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

cleanup33:                                        ; preds = %do.end31, %if.then28
  %17 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  %cleanup.dest34 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest34, label %unreachable [
    i32 0, label %cleanup.cont35
    i32 1, label %return
  ]

cleanup.cont35:                                   ; preds = %cleanup33
  br label %if.end53

if.else36:                                        ; preds = %if.else20
  %18 = load i64, i64* %factor.addr, align 8, !tbaa !14
  %cmp37 = icmp eq i64 %18, 8
  br i1 %cmp37, label %if.then39, label %if.else51

if.then39:                                        ; preds = %if.else36
  %19 = bitcast i64* %tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %19) #4
  br label %do.body41

do.body41:                                        ; preds = %if.then39
  %20 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call42 = call i32 @avifROStreamReadU64(%struct.avifROStream* %20, i64* %tmp40)
  %tobool43 = icmp ne i32 %call42, 0
  br i1 %tobool43, label %if.end45, label %if.then44

if.then44:                                        ; preds = %do.body41
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup48

if.end45:                                         ; preds = %do.body41
  br label %do.cond46

do.cond46:                                        ; preds = %if.end45
  br label %do.end47

do.end47:                                         ; preds = %do.cond46
  %21 = load i64, i64* %tmp40, align 8, !tbaa !14
  %22 = load i64*, i64** %v.addr, align 4, !tbaa !2
  store i64 %21, i64* %22, align 8, !tbaa !14
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup48

cleanup48:                                        ; preds = %do.end47, %if.then44
  %23 = bitcast i64* %tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #4
  %cleanup.dest49 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest49, label %unreachable [
    i32 0, label %cleanup.cont50
    i32 1, label %return
  ]

cleanup.cont50:                                   ; preds = %cleanup48
  br label %if.end52

if.else51:                                        ; preds = %if.else36
  store i32 0, i32* %retval, align 4
  br label %return

if.end52:                                         ; preds = %cleanup.cont50
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %cleanup.cont35
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %cleanup.cont19
  br label %if.end55

if.end55:                                         ; preds = %if.end54, %cleanup.cont
  br label %if.end56

if.end56:                                         ; preds = %if.end55, %if.then
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end56, %if.else51, %cleanup48, %cleanup33, %cleanup17, %cleanup
  %24 = load i32, i32* %retval, align 4
  ret i32 %24

unreachable:                                      ; preds = %cleanup48, %cleanup33, %cleanup17, %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @avifROStreamReadU16(%struct.avifROStream* %stream, i16* %v) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %v.addr = alloca i16*, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i16* %v, i16** %v.addr, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %entry
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %1 = load i16*, i16** %v.addr, align 4, !tbaa !2
  %2 = bitcast i16* %1 to i8*
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %0, i8* %2, i32 2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %if.end
  %3 = load i16*, i16** %v.addr, align 4, !tbaa !2
  %4 = load i16, i16* %3, align 2, !tbaa !17
  %call1 = call zeroext i16 @avifNTOHS(i16 zeroext %4)
  %5 = load i16*, i16** %v.addr, align 4, !tbaa !2
  store i16 %call1, i16* %5, align 2, !tbaa !17
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %do.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: nounwind
define hidden i32 @avifROStreamReadU32(%struct.avifROStream* %stream, i32* %v) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %v.addr = alloca i32*, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i32* %v, i32** %v.addr, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %entry
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %1 = load i32*, i32** %v.addr, align 4, !tbaa !2
  %2 = bitcast i32* %1 to i8*
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %0, i8* %2, i32 4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %if.end
  %3 = load i32*, i32** %v.addr, align 4, !tbaa !2
  %4 = load i32, i32* %3, align 4, !tbaa !19
  %call1 = call i32 @avifNTOHL(i32 %4)
  %5 = load i32*, i32** %v.addr, align 4, !tbaa !2
  store i32 %call1, i32* %5, align 4, !tbaa !19
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %do.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: nounwind
define hidden i32 @avifROStreamReadU64(%struct.avifROStream* %stream, i64* %v) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %v.addr = alloca i64*, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i64* %v, i64** %v.addr, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %entry
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %1 = load i64*, i64** %v.addr, align 4, !tbaa !2
  %2 = bitcast i64* %1 to i8*
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %0, i8* %2, i32 8)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %if.end
  %3 = load i64*, i64** %v.addr, align 4, !tbaa !2
  %4 = load i64, i64* %3, align 8, !tbaa !14
  %call1 = call i64 @avifNTOH64(i64 %4)
  %5 = load i64*, i64** %v.addr, align 4, !tbaa !2
  store i64 %call1, i64* %5, align 8, !tbaa !14
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %do.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

declare zeroext i16 @avifNTOHS(i16 zeroext) #2

declare i32 @avifNTOHL(i32) #2

declare i64 @avifNTOH64(i64) #2

; Function Attrs: nounwind
define hidden i32 @avifROStreamReadString(%struct.avifROStream* %stream, i8* %output, i32 %outputSize) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %output.addr = alloca i8*, align 4
  %outputSize.addr = alloca i32, align 4
  %remainingBytes = alloca i32, align 4
  %p = alloca i8*, align 4
  %foundNullTerminator = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %streamString = alloca i8*, align 4
  %stringLen = alloca i32, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i8* %output, i8** %output.addr, align 4, !tbaa !2
  store i32 %outputSize, i32* %outputSize.addr, align 4, !tbaa !12
  %0 = bitcast i32* %remainingBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call = call i32 @avifROStreamRemainingBytes(%struct.avifROStream* %1)
  store i32 %call, i32* %remainingBytes, align 4, !tbaa !12
  %2 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call1 = call i8* @avifROStreamCurrent(%struct.avifROStream* %3)
  store i8* %call1, i8** %p, align 4, !tbaa !2
  %4 = bitcast i32* %foundNullTerminator to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %foundNullTerminator, align 4, !tbaa !19
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !12
  %7 = load i32, i32* %remainingBytes, align 4, !tbaa !12
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %8 = load i8*, i8** %p, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !16
  %conv = zext i8 %10 to i32
  %cmp2 = icmp eq i32 %conv, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 1, i32* %foundNullTerminator, align 4, !tbaa !19
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  br label %for.end

for.end:                                          ; preds = %cleanup
  %13 = load i32, i32* %foundNullTerminator, align 4, !tbaa !19
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.end5, label %if.then4

if.then4:                                         ; preds = %for.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

if.end5:                                          ; preds = %for.end
  %14 = bitcast i8** %streamString to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load i8*, i8** %p, align 4, !tbaa !2
  store i8* %15, i8** %streamString, align 4, !tbaa !2
  %16 = bitcast i32* %stringLen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i8*, i8** %streamString, align 4, !tbaa !2
  %call6 = call i32 @strlen(i8* %17)
  store i32 %call6, i32* %stringLen, align 4, !tbaa !12
  %18 = load i32, i32* %stringLen, align 4, !tbaa !12
  %add = add i32 %18, 1
  %19 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %19, i32 0, i32 1
  %20 = load i32, i32* %offset, align 4, !tbaa !11
  %add7 = add i32 %20, %add
  store i32 %add7, i32* %offset, align 4, !tbaa !11
  %21 = load i8*, i8** %output.addr, align 4, !tbaa !2
  %tobool8 = icmp ne i8* %21, null
  br i1 %tobool8, label %land.lhs.true, label %if.end16

land.lhs.true:                                    ; preds = %if.end5
  %22 = load i32, i32* %outputSize.addr, align 4, !tbaa !12
  %tobool9 = icmp ne i32 %22, 0
  br i1 %tobool9, label %if.then10, label %if.end16

if.then10:                                        ; preds = %land.lhs.true
  %23 = load i32, i32* %stringLen, align 4, !tbaa !12
  %24 = load i32, i32* %outputSize.addr, align 4, !tbaa !12
  %cmp11 = icmp uge i32 %23, %24
  br i1 %cmp11, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.then10
  %25 = load i32, i32* %outputSize.addr, align 4, !tbaa !12
  %sub = sub i32 %25, 1
  store i32 %sub, i32* %stringLen, align 4, !tbaa !12
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.then10
  %26 = load i8*, i8** %output.addr, align 4, !tbaa !2
  %27 = load i8*, i8** %streamString, align 4, !tbaa !2
  %28 = load i32, i32* %stringLen, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %26, i8* align 1 %27, i32 %28, i1 false)
  %29 = load i8*, i8** %output.addr, align 4, !tbaa !2
  %30 = load i32, i32* %stringLen, align 4, !tbaa !12
  %arrayidx15 = getelementptr inbounds i8, i8* %29, i32 %30
  store i8 0, i8* %arrayidx15, align 1, !tbaa !16
  br label %if.end16

if.end16:                                         ; preds = %if.end14, %land.lhs.true, %if.end5
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %31 = bitcast i32* %stringLen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #4
  %32 = bitcast i8** %streamString to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #4
  br label %cleanup19

cleanup19:                                        ; preds = %if.end16, %if.then4
  %33 = bitcast i32* %foundNullTerminator to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  %35 = bitcast i32* %remainingBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  %36 = load i32, i32* %retval, align 4
  ret i32 %36
}

declare i32 @strlen(i8*) #2

; Function Attrs: nounwind
define hidden i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %stream, %struct.avifBoxHeader* %header) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %header.addr = alloca %struct.avifBoxHeader*, align 4
  %startOffset = alloca i32, align 4
  %smallSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %size = alloca i64, align 8
  %bytesRead = alloca i32, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store %struct.avifBoxHeader* %header, %struct.avifBoxHeader** %header.addr, align 4, !tbaa !2
  %0 = bitcast i32* %startOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %1, i32 0, i32 1
  %2 = load i32, i32* %offset, align 4, !tbaa !11
  store i32 %2, i32* %startOffset, align 4, !tbaa !12
  %3 = bitcast i32* %smallSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %4 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call = call i32 @avifROStreamReadU32(%struct.avifROStream* %4, i32* %smallSize)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %5 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %6 = load %struct.avifBoxHeader*, %struct.avifBoxHeader** %header.addr, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %6, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call2 = call i32 @avifROStreamRead(%struct.avifROStream* %5, i8* %arraydecay, i32 4)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %7 = bitcast i64* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #4
  %8 = load i32, i32* %smallSize, align 4, !tbaa !19
  %conv = zext i32 %8 to i64
  store i64 %conv, i64* %size, align 8, !tbaa !14
  %9 = load i64, i64* %size, align 8, !tbaa !14
  %cmp = icmp eq i64 %9, 1
  br i1 %cmp, label %if.then9, label %if.end17

if.then9:                                         ; preds = %do.end7
  br label %do.body10

do.body10:                                        ; preds = %if.then9
  %10 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call11 = call i32 @avifROStreamReadU64(%struct.avifROStream* %10, i64* %size)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %do.body10
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup51

if.end14:                                         ; preds = %do.body10
  br label %do.cond15

do.cond15:                                        ; preds = %if.end14
  br label %do.end16

do.end16:                                         ; preds = %do.cond15
  br label %if.end17

if.end17:                                         ; preds = %do.end16, %do.end7
  %11 = load %struct.avifBoxHeader*, %struct.avifBoxHeader** %header.addr, align 4, !tbaa !2
  %type18 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %11, i32 0, i32 1
  %arraydecay19 = getelementptr inbounds [4 x i8], [4 x i8]* %type18, i32 0, i32 0
  %call20 = call i32 @memcmp(i8* %arraydecay19, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i32 4)
  %tobool21 = icmp ne i32 %call20, 0
  br i1 %tobool21, label %if.end30, label %if.then22

if.then22:                                        ; preds = %if.end17
  br label %do.body23

do.body23:                                        ; preds = %if.then22
  %12 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call24 = call i32 @avifROStreamSkip(%struct.avifROStream* %12, i32 16)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %do.body23
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup51

if.end27:                                         ; preds = %do.body23
  br label %do.cond28

do.cond28:                                        ; preds = %if.end27
  br label %do.end29

do.end29:                                         ; preds = %do.cond28
  br label %if.end30

if.end30:                                         ; preds = %do.end29, %if.end17
  %13 = bitcast i32* %bytesRead to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %offset31 = getelementptr inbounds %struct.avifROStream, %struct.avifROStream* %14, i32 0, i32 1
  %15 = load i32, i32* %offset31, align 4, !tbaa !11
  %16 = load i32, i32* %startOffset, align 4, !tbaa !12
  %sub = sub i32 %15, %16
  store i32 %sub, i32* %bytesRead, align 4, !tbaa !12
  %17 = load i64, i64* %size, align 8, !tbaa !14
  %18 = load i32, i32* %bytesRead, align 4, !tbaa !12
  %conv32 = zext i32 %18 to i64
  %cmp33 = icmp ult i64 %17, %conv32
  br i1 %cmp33, label %if.then39, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end30
  %19 = load i64, i64* %size, align 8, !tbaa !14
  %20 = load i32, i32* %bytesRead, align 4, !tbaa !12
  %conv35 = zext i32 %20 to i64
  %sub36 = sub i64 %19, %conv35
  %cmp37 = icmp ugt i64 %sub36, 4294967295
  br i1 %cmp37, label %if.then39, label %if.end40

if.then39:                                        ; preds = %lor.lhs.false, %if.end30
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end40:                                         ; preds = %lor.lhs.false
  %21 = load i64, i64* %size, align 8, !tbaa !14
  %22 = load i32, i32* %bytesRead, align 4, !tbaa !12
  %conv41 = zext i32 %22 to i64
  %sub42 = sub i64 %21, %conv41
  %conv43 = trunc i64 %sub42 to i32
  %23 = load %struct.avifBoxHeader*, %struct.avifBoxHeader** %header.addr, align 4, !tbaa !2
  %size44 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %23, i32 0, i32 0
  store i32 %conv43, i32* %size44, align 4, !tbaa !21
  %24 = load %struct.avifBoxHeader*, %struct.avifBoxHeader** %header.addr, align 4, !tbaa !2
  %size45 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %24, i32 0, i32 0
  %25 = load i32, i32* %size45, align 4, !tbaa !21
  %26 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call46 = call i32 @avifROStreamRemainingBytes(%struct.avifROStream* %26)
  %cmp47 = icmp ugt i32 %25, %call46
  br i1 %cmp47, label %if.then49, label %if.end50

if.then49:                                        ; preds = %if.end40
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end50:                                         ; preds = %if.end40
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end50, %if.then49, %if.then39
  %27 = bitcast i32* %bytesRead to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  br label %cleanup51

cleanup51:                                        ; preds = %cleanup, %if.then26, %if.then13
  %28 = bitcast i64* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %28) #4
  br label %cleanup52

cleanup52:                                        ; preds = %cleanup51, %if.then4, %if.then
  %29 = bitcast i32* %smallSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  %30 = bitcast i32* %startOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

declare i32 @memcmp(i8*, i8*, i32) #2

; Function Attrs: nounwind
define hidden i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %stream, i8* %version, i32* %flags) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %version.addr = alloca i8*, align 4
  %flags.addr = alloca i32*, align 4
  %versionAndFlags = alloca [4 x i8], align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i8* %version, i8** %version.addr, align 4, !tbaa !2
  store i32* %flags, i32** %flags.addr, align 4, !tbaa !2
  %0 = bitcast [4 x i8]* %versionAndFlags to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %1 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %versionAndFlags, i32 0, i32 0
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %1, i8* %arraydecay, i32 4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %2 = load i8*, i8** %version.addr, align 4, !tbaa !2
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %do.end
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %versionAndFlags, i32 0, i32 0
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !16
  %4 = load i8*, i8** %version.addr, align 4, !tbaa !2
  store i8 %3, i8* %4, align 1, !tbaa !16
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %do.end
  %5 = load i32*, i32** %flags.addr, align 4, !tbaa !2
  %tobool4 = icmp ne i32* %5, null
  br i1 %tobool4, label %if.then5, label %if.end14

if.then5:                                         ; preds = %if.end3
  %arrayidx6 = getelementptr inbounds [4 x i8], [4 x i8]* %versionAndFlags, i32 0, i32 1
  %6 = load i8, i8* %arrayidx6, align 1, !tbaa !16
  %conv = zext i8 %6 to i32
  %shl = shl i32 %conv, 16
  %arrayidx7 = getelementptr inbounds [4 x i8], [4 x i8]* %versionAndFlags, i32 0, i32 2
  %7 = load i8, i8* %arrayidx7, align 1, !tbaa !16
  %conv8 = zext i8 %7 to i32
  %shl9 = shl i32 %conv8, 8
  %add = add nsw i32 %shl, %shl9
  %arrayidx10 = getelementptr inbounds [4 x i8], [4 x i8]* %versionAndFlags, i32 0, i32 3
  %8 = load i8, i8* %arrayidx10, align 1, !tbaa !16
  %conv11 = zext i8 %8 to i32
  %shl12 = shl i32 %conv11, 0
  %add13 = add nsw i32 %add, %shl12
  %9 = load i32*, i32** %flags.addr, align 4, !tbaa !2
  store i32 %add13, i32* %9, align 4, !tbaa !19
  br label %if.end14

if.end14:                                         ; preds = %if.then5, %if.end3
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end14, %if.then
  %10 = bitcast [4 x i8]* %versionAndFlags to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: nounwind
define hidden i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %stream, i8 zeroext %enforcedVersion) #0 {
entry:
  %retval = alloca i32, align 4
  %stream.addr = alloca %struct.avifROStream*, align 4
  %enforcedVersion.addr = alloca i8, align 1
  %version = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifROStream* %stream, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  store i8 %enforcedVersion, i8* %enforcedVersion.addr, align 1, !tbaa !16
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %0 = load %struct.avifROStream*, %struct.avifROStream** %stream.addr, align 4, !tbaa !2
  %call = call i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %0, i8* %version, i32* null)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %1 = load i8, i8* %version, align 1, !tbaa !16
  %conv = zext i8 %1 to i32
  %2 = load i8, i8* %enforcedVersion.addr, align 1, !tbaa !16
  %conv1 = zext i8 %2 to i32
  %cmp = icmp eq i32 %conv, %conv1
  %conv2 = zext i1 %cmp to i32
  store i32 %conv2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define hidden void @avifRWStreamStart(%struct.avifRWStream* %stream, %struct.avifRWData* %raw) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %raw.addr = alloca %struct.avifRWData*, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store %struct.avifRWData* %raw, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %0 = load %struct.avifRWData*, %struct.avifRWData** %raw.addr, align 4, !tbaa !2
  %1 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw1 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %1, i32 0, i32 0
  store %struct.avifRWData* %0, %struct.avifRWData** %raw1, align 4, !tbaa !23
  %2 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %2, i32 0, i32 1
  store i32 0, i32* %offset, align 4, !tbaa !25
  ret void
}

; Function Attrs: nounwind
define hidden i32 @avifRWStreamOffset(%struct.avifRWStream* %stream) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %0 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %0, i32 0, i32 1
  %1 = load i32, i32* %offset, align 4, !tbaa !25
  ret i32 %1
}

; Function Attrs: nounwind
define hidden void @avifRWStreamSetOffset(%struct.avifRWStream* %stream, i32 %offset) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %offset.addr = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i32 %offset, i32* %offset.addr, align 4, !tbaa !12
  %0 = load i32, i32* %offset.addr, align 4, !tbaa !12
  %1 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset1 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %1, i32 0, i32 1
  store i32 %0, i32* %offset1, align 4, !tbaa !25
  %2 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset2 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %2, i32 0, i32 1
  %3 = load i32, i32* %offset2, align 4, !tbaa !25
  %4 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %4, i32 0, i32 0
  %5 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %5, i32 0, i32 1
  %6 = load i32, i32* %size, align 4, !tbaa !26
  %cmp = icmp ugt i32 %3, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw3 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %7, i32 0, i32 0
  %8 = load %struct.avifRWData*, %struct.avifRWData** %raw3, align 4, !tbaa !23
  %size4 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %8, i32 0, i32 1
  %9 = load i32, i32* %size4, align 4, !tbaa !26
  %10 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset5 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %10, i32 0, i32 1
  store i32 %9, i32* %offset5, align 4, !tbaa !25
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @avifRWStreamFinishWrite(%struct.avifRWStream* %stream) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %0 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %0, i32 0, i32 0
  %1 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %1, i32 0, i32 1
  %2 = load i32, i32* %size, align 4, !tbaa !26
  %3 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %3, i32 0, i32 1
  %4 = load i32, i32* %offset, align 4, !tbaa !25
  %cmp = icmp ne i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %5 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset1 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %5, i32 0, i32 1
  %6 = load i32, i32* %offset1, align 4, !tbaa !25
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %7 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset3 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %7, i32 0, i32 1
  %8 = load i32, i32* %offset3, align 4, !tbaa !25
  %9 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw4 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %9, i32 0, i32 0
  %10 = load %struct.avifRWData*, %struct.avifRWData** %raw4, align 4, !tbaa !23
  %size5 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %10, i32 0, i32 1
  store i32 %8, i32* %size5, align 4, !tbaa !26
  br label %if.end

if.else:                                          ; preds = %if.then
  %11 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw6 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %11, i32 0, i32 0
  %12 = load %struct.avifRWData*, %struct.avifRWData** %raw6, align 4, !tbaa !23
  call void @avifRWDataFree(%struct.avifRWData* %12)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then2
  br label %if.end7

if.end7:                                          ; preds = %if.end, %entry
  ret void
}

declare void @avifRWDataFree(%struct.avifRWData*) #2

; Function Attrs: nounwind
define hidden void @avifRWStreamWrite(%struct.avifRWStream* %stream, i8* %data, i32 %size) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %data.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !12
  %0 = load i32, i32* %size.addr, align 4, !tbaa !12
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %2 = load i32, i32* %size.addr, align 4, !tbaa !12
  call void @makeRoom(%struct.avifRWStream* %1, i32 %2)
  %3 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %3, i32 0, i32 0
  %4 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %data1 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %4, i32 0, i32 0
  %5 = load i8*, i8** %data1, align 4, !tbaa !28
  %6 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %6, i32 0, i32 1
  %7 = load i32, i32* %offset, align 4, !tbaa !25
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %7
  %8 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %9 = load i32, i32* %size.addr, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %8, i32 %9, i1 false)
  %10 = load i32, i32* %size.addr, align 4, !tbaa !12
  %11 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset2 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %11, i32 0, i32 1
  %12 = load i32, i32* %offset2, align 4, !tbaa !25
  %add = add i32 %12, %10
  store i32 %add, i32* %offset2, align 4, !tbaa !25
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @makeRoom(%struct.avifRWStream* %stream, i32 %size) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %size.addr = alloca i32, align 4
  %neededSize = alloca i32, align 4
  %newSize = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !12
  %0 = bitcast i32* %neededSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %1, i32 0, i32 1
  %2 = load i32, i32* %offset, align 4, !tbaa !25
  %3 = load i32, i32* %size.addr, align 4, !tbaa !12
  %add = add i32 %2, %3
  store i32 %add, i32* %neededSize, align 4, !tbaa !12
  %4 = bitcast i32* %newSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %5, i32 0, i32 0
  %6 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %size1 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %6, i32 0, i32 1
  %7 = load i32, i32* %size1, align 4, !tbaa !26
  store i32 %7, i32* %newSize, align 4, !tbaa !12
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load i32, i32* %newSize, align 4, !tbaa !12
  %9 = load i32, i32* %neededSize, align 4, !tbaa !12
  %cmp = icmp ult i32 %8, %9
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i32, i32* %newSize, align 4, !tbaa !12
  %add2 = add i32 %10, 1048576
  store i32 %add2, i32* %newSize, align 4, !tbaa !12
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %11 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw3 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %11, i32 0, i32 0
  %12 = load %struct.avifRWData*, %struct.avifRWData** %raw3, align 4, !tbaa !23
  %size4 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %12, i32 0, i32 1
  %13 = load i32, i32* %size4, align 4, !tbaa !26
  %14 = load i32, i32* %newSize, align 4, !tbaa !12
  %cmp5 = icmp ne i32 %13, %14
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %15 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw6 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %15, i32 0, i32 0
  %16 = load %struct.avifRWData*, %struct.avifRWData** %raw6, align 4, !tbaa !23
  %17 = load i32, i32* %newSize, align 4, !tbaa !12
  call void @avifRWDataRealloc(%struct.avifRWData* %16, i32 %17)
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end
  %18 = bitcast i32* %newSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %19 = bitcast i32* %neededSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @avifRWStreamWriteChars(%struct.avifRWStream* %stream, i8* %chars, i32 %size) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %chars.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i8* %chars, i8** %chars.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !12
  %0 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %chars.addr, align 4, !tbaa !2
  %2 = load i32, i32* %size.addr, align 4, !tbaa !12
  call void @avifRWStreamWrite(%struct.avifRWStream* %0, i8* %1, i32 %2)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %stream, i8* %type, i32 %contentSize, i32 %version, i32 %flags) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %type.addr = alloca i8*, align 4
  %contentSize.addr = alloca i32, align 4
  %version.addr = alloca i32, align 4
  %flags.addr = alloca i32, align 4
  %marker = alloca i32, align 4
  %headerSize = alloca i32, align 4
  %noSize = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i8* %type, i8** %type.addr, align 4, !tbaa !2
  store i32 %contentSize, i32* %contentSize.addr, align 4, !tbaa !12
  store i32 %version, i32* %version.addr, align 4, !tbaa !19
  store i32 %flags, i32* %flags.addr, align 4, !tbaa !19
  %0 = bitcast i32* %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %1, i32 0, i32 1
  %2 = load i32, i32* %offset, align 4, !tbaa !25
  store i32 %2, i32* %marker, align 4, !tbaa !12
  %3 = bitcast i32* %headerSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 8, i32* %headerSize, align 4, !tbaa !12
  %4 = load i32, i32* %version.addr, align 4, !tbaa !19
  %cmp = icmp ne i32 %4, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %headerSize, align 4, !tbaa !12
  %add = add i32 %5, 4
  store i32 %add, i32* %headerSize, align 4, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %7 = load i32, i32* %headerSize, align 4, !tbaa !12
  call void @makeRoom(%struct.avifRWStream* %6, i32 %7)
  %8 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %8, i32 0, i32 0
  %9 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %9, i32 0, i32 0
  %10 = load i8*, i8** %data, align 4, !tbaa !28
  %11 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset1 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %11, i32 0, i32 1
  %12 = load i32, i32* %offset1, align 4, !tbaa !25
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %12
  %13 = load i32, i32* %headerSize, align 4, !tbaa !12
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr, i8 0, i32 %13, i1 false)
  %14 = bitcast i32* %noSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load i32, i32* %headerSize, align 4, !tbaa !12
  %16 = load i32, i32* %contentSize.addr, align 4, !tbaa !12
  %add2 = add i32 %15, %16
  %call = call i32 @avifHTONL(i32 %add2)
  store i32 %call, i32* %noSize, align 4, !tbaa !19
  %17 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw3 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %17, i32 0, i32 0
  %18 = load %struct.avifRWData*, %struct.avifRWData** %raw3, align 4, !tbaa !23
  %data4 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %18, i32 0, i32 0
  %19 = load i8*, i8** %data4, align 4, !tbaa !28
  %20 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset5 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %20, i32 0, i32 1
  %21 = load i32, i32* %offset5, align 4, !tbaa !25
  %add.ptr6 = getelementptr inbounds i8, i8* %19, i32 %21
  %22 = bitcast i32* %noSize to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr6, i8* align 4 %22, i32 4, i1 false)
  %23 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw7 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %23, i32 0, i32 0
  %24 = load %struct.avifRWData*, %struct.avifRWData** %raw7, align 4, !tbaa !23
  %data8 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %24, i32 0, i32 0
  %25 = load i8*, i8** %data8, align 4, !tbaa !28
  %26 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset9 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %26, i32 0, i32 1
  %27 = load i32, i32* %offset9, align 4, !tbaa !25
  %add.ptr10 = getelementptr inbounds i8, i8* %25, i32 %27
  %add.ptr11 = getelementptr inbounds i8, i8* %add.ptr10, i32 4
  %28 = load i8*, i8** %type.addr, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr11, i8* align 1 %28, i32 4, i1 false)
  %29 = load i32, i32* %version.addr, align 4, !tbaa !19
  %cmp12 = icmp ne i32 %29, -1
  br i1 %cmp12, label %if.then13, label %if.end40

if.then13:                                        ; preds = %if.end
  %30 = load i32, i32* %version.addr, align 4, !tbaa !19
  %conv = trunc i32 %30 to i8
  %31 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw14 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %31, i32 0, i32 0
  %32 = load %struct.avifRWData*, %struct.avifRWData** %raw14, align 4, !tbaa !23
  %data15 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %32, i32 0, i32 0
  %33 = load i8*, i8** %data15, align 4, !tbaa !28
  %34 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset16 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %34, i32 0, i32 1
  %35 = load i32, i32* %offset16, align 4, !tbaa !25
  %add17 = add i32 %35, 8
  %arrayidx = getelementptr inbounds i8, i8* %33, i32 %add17
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !16
  %36 = load i32, i32* %flags.addr, align 4, !tbaa !19
  %shr = lshr i32 %36, 16
  %and = and i32 %shr, 255
  %conv18 = trunc i32 %and to i8
  %37 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw19 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %37, i32 0, i32 0
  %38 = load %struct.avifRWData*, %struct.avifRWData** %raw19, align 4, !tbaa !23
  %data20 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %38, i32 0, i32 0
  %39 = load i8*, i8** %data20, align 4, !tbaa !28
  %40 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset21 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %40, i32 0, i32 1
  %41 = load i32, i32* %offset21, align 4, !tbaa !25
  %add22 = add i32 %41, 9
  %arrayidx23 = getelementptr inbounds i8, i8* %39, i32 %add22
  store i8 %conv18, i8* %arrayidx23, align 1, !tbaa !16
  %42 = load i32, i32* %flags.addr, align 4, !tbaa !19
  %shr24 = lshr i32 %42, 8
  %and25 = and i32 %shr24, 255
  %conv26 = trunc i32 %and25 to i8
  %43 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw27 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %43, i32 0, i32 0
  %44 = load %struct.avifRWData*, %struct.avifRWData** %raw27, align 4, !tbaa !23
  %data28 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %44, i32 0, i32 0
  %45 = load i8*, i8** %data28, align 4, !tbaa !28
  %46 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset29 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %46, i32 0, i32 1
  %47 = load i32, i32* %offset29, align 4, !tbaa !25
  %add30 = add i32 %47, 10
  %arrayidx31 = getelementptr inbounds i8, i8* %45, i32 %add30
  store i8 %conv26, i8* %arrayidx31, align 1, !tbaa !16
  %48 = load i32, i32* %flags.addr, align 4, !tbaa !19
  %shr32 = lshr i32 %48, 0
  %and33 = and i32 %shr32, 255
  %conv34 = trunc i32 %and33 to i8
  %49 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw35 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %49, i32 0, i32 0
  %50 = load %struct.avifRWData*, %struct.avifRWData** %raw35, align 4, !tbaa !23
  %data36 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %50, i32 0, i32 0
  %51 = load i8*, i8** %data36, align 4, !tbaa !28
  %52 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset37 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %52, i32 0, i32 1
  %53 = load i32, i32* %offset37, align 4, !tbaa !25
  %add38 = add i32 %53, 11
  %arrayidx39 = getelementptr inbounds i8, i8* %51, i32 %add38
  store i8 %conv34, i8* %arrayidx39, align 1, !tbaa !16
  br label %if.end40

if.end40:                                         ; preds = %if.then13, %if.end
  %54 = load i32, i32* %headerSize, align 4, !tbaa !12
  %55 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset41 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %55, i32 0, i32 1
  %56 = load i32, i32* %offset41, align 4, !tbaa !25
  %add42 = add i32 %56, %54
  store i32 %add42, i32* %offset41, align 4, !tbaa !25
  %57 = load i32, i32* %marker, align 4, !tbaa !12
  %58 = bitcast i32* %noSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i32* %headerSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = bitcast i32* %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  ret i32 %57
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare i32 @avifHTONL(i32) #2

; Function Attrs: nounwind
define hidden i32 @avifRWStreamWriteBox(%struct.avifRWStream* %stream, i8* %type, i32 %contentSize) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %type.addr = alloca i8*, align 4
  %contentSize.addr = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i8* %type, i8** %type.addr, align 4, !tbaa !2
  store i32 %contentSize, i32* %contentSize.addr, align 4, !tbaa !12
  %0 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %type.addr, align 4, !tbaa !2
  %2 = load i32, i32* %contentSize.addr, align 4, !tbaa !12
  %call = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %0, i8* %1, i32 %2, i32 -1, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden void @avifRWStreamFinishBox(%struct.avifRWStream* %stream, i32 %marker) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %marker.addr = alloca i32, align 4
  %noSize = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i32 %marker, i32* %marker.addr, align 4, !tbaa !12
  %0 = bitcast i32* %noSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %1, i32 0, i32 1
  %2 = load i32, i32* %offset, align 4, !tbaa !25
  %3 = load i32, i32* %marker.addr, align 4, !tbaa !12
  %sub = sub i32 %2, %3
  %call = call i32 @avifNTOHL(i32 %sub)
  store i32 %call, i32* %noSize, align 4, !tbaa !19
  %4 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %4, i32 0, i32 0
  %5 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %5, i32 0, i32 0
  %6 = load i8*, i8** %data, align 4, !tbaa !28
  %7 = load i32, i32* %marker.addr, align 4, !tbaa !12
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = bitcast i32* %noSize to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 4 %8, i32 4, i1 false)
  %9 = bitcast i32* %noSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @avifRWStreamWriteU8(%struct.avifRWStream* %stream, i8 zeroext %v) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %v.addr = alloca i8, align 1
  %size = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i8 %v, i8* %v.addr, align 1, !tbaa !16
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 1, i32* %size, align 4, !tbaa !12
  %1 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %2 = load i32, i32* %size, align 4, !tbaa !12
  call void @makeRoom(%struct.avifRWStream* %1, i32 %2)
  %3 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %3, i32 0, i32 0
  %4 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %4, i32 0, i32 0
  %5 = load i8*, i8** %data, align 4, !tbaa !28
  %6 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %6, i32 0, i32 1
  %7 = load i32, i32* %offset, align 4, !tbaa !25
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %7
  %8 = load i32, i32* %size, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %v.addr, i32 %8, i1 false)
  %9 = load i32, i32* %size, align 4, !tbaa !12
  %10 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset1 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %10, i32 0, i32 1
  %11 = load i32, i32* %offset1, align 4, !tbaa !25
  %add = add i32 %11, %9
  store i32 %add, i32* %offset1, align 4, !tbaa !25
  %12 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @avifRWStreamWriteU16(%struct.avifRWStream* %stream, i16 zeroext %v) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %v.addr = alloca i16, align 2
  %size = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i16 %v, i16* %v.addr, align 2, !tbaa !17
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 2, i32* %size, align 4, !tbaa !12
  %1 = load i16, i16* %v.addr, align 2, !tbaa !17
  %call = call zeroext i16 @avifHTONS(i16 zeroext %1)
  store i16 %call, i16* %v.addr, align 2, !tbaa !17
  %2 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %3 = load i32, i32* %size, align 4, !tbaa !12
  call void @makeRoom(%struct.avifRWStream* %2, i32 %3)
  %4 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %4, i32 0, i32 0
  %5 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %5, i32 0, i32 0
  %6 = load i8*, i8** %data, align 4, !tbaa !28
  %7 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %7, i32 0, i32 1
  %8 = load i32, i32* %offset, align 4, !tbaa !25
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %8
  %9 = bitcast i16* %v.addr to i8*
  %10 = load i32, i32* %size, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 2 %9, i32 %10, i1 false)
  %11 = load i32, i32* %size, align 4, !tbaa !12
  %12 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset1 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %12, i32 0, i32 1
  %13 = load i32, i32* %offset1, align 4, !tbaa !25
  %add = add i32 %13, %11
  store i32 %add, i32* %offset1, align 4, !tbaa !25
  %14 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  ret void
}

declare zeroext i16 @avifHTONS(i16 zeroext) #2

; Function Attrs: nounwind
define hidden void @avifRWStreamWriteU32(%struct.avifRWStream* %stream, i32 %v) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %v.addr = alloca i32, align 4
  %size = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i32 %v, i32* %v.addr, align 4, !tbaa !19
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 4, i32* %size, align 4, !tbaa !12
  %1 = load i32, i32* %v.addr, align 4, !tbaa !19
  %call = call i32 @avifHTONL(i32 %1)
  store i32 %call, i32* %v.addr, align 4, !tbaa !19
  %2 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %3 = load i32, i32* %size, align 4, !tbaa !12
  call void @makeRoom(%struct.avifRWStream* %2, i32 %3)
  %4 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %4, i32 0, i32 0
  %5 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %5, i32 0, i32 0
  %6 = load i8*, i8** %data, align 4, !tbaa !28
  %7 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %7, i32 0, i32 1
  %8 = load i32, i32* %offset, align 4, !tbaa !25
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %8
  %9 = bitcast i32* %v.addr to i8*
  %10 = load i32, i32* %size, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 4 %9, i32 %10, i1 false)
  %11 = load i32, i32* %size, align 4, !tbaa !12
  %12 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset1 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %12, i32 0, i32 1
  %13 = load i32, i32* %offset1, align 4, !tbaa !25
  %add = add i32 %13, %11
  store i32 %add, i32* %offset1, align 4, !tbaa !25
  %14 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @avifRWStreamWriteU64(%struct.avifRWStream* %stream, i64 %v) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %v.addr = alloca i64, align 8
  %size = alloca i32, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i64 %v, i64* %v.addr, align 8, !tbaa !14
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 8, i32* %size, align 4, !tbaa !12
  %1 = load i64, i64* %v.addr, align 8, !tbaa !14
  %call = call i64 @avifHTON64(i64 %1)
  store i64 %call, i64* %v.addr, align 8, !tbaa !14
  %2 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %3 = load i32, i32* %size, align 4, !tbaa !12
  call void @makeRoom(%struct.avifRWStream* %2, i32 %3)
  %4 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %4, i32 0, i32 0
  %5 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %5, i32 0, i32 0
  %6 = load i8*, i8** %data, align 4, !tbaa !28
  %7 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %7, i32 0, i32 1
  %8 = load i32, i32* %offset, align 4, !tbaa !25
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %8
  %9 = bitcast i64* %v.addr to i8*
  %10 = load i32, i32* %size, align 4, !tbaa !12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 8 %9, i32 %10, i1 false)
  %11 = load i32, i32* %size, align 4, !tbaa !12
  %12 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset1 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %12, i32 0, i32 1
  %13 = load i32, i32* %offset1, align 4, !tbaa !25
  %add = add i32 %13, %11
  store i32 %add, i32* %offset1, align 4, !tbaa !25
  %14 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  ret void
}

declare i64 @avifHTON64(i64) #2

; Function Attrs: nounwind
define hidden void @avifRWStreamWriteZeros(%struct.avifRWStream* %stream, i32 %byteCount) #0 {
entry:
  %stream.addr = alloca %struct.avifRWStream*, align 4
  %byteCount.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %end = alloca i8*, align 4
  store %struct.avifRWStream* %stream, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  store i32 %byteCount, i32* %byteCount.addr, align 4, !tbaa !12
  %0 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %1 = load i32, i32* %byteCount.addr, align 4, !tbaa !12
  call void @makeRoom(%struct.avifRWStream* %0, i32 %1)
  %2 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %raw = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %3, i32 0, i32 0
  %4 = load %struct.avifRWData*, %struct.avifRWData** %raw, align 4, !tbaa !23
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %4, i32 0, i32 0
  %5 = load i8*, i8** %data, align 4, !tbaa !28
  %6 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %6, i32 0, i32 1
  %7 = load i32, i32* %offset, align 4, !tbaa !25
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %7
  store i8* %add.ptr, i8** %p, align 4, !tbaa !2
  %8 = bitcast i8** %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i8*, i8** %p, align 4, !tbaa !2
  %10 = load i32, i32* %byteCount.addr, align 4, !tbaa !12
  %add.ptr1 = getelementptr inbounds i8, i8* %9, i32 %10
  store i8* %add.ptr1, i8** %end, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %11 = load i8*, i8** %p, align 4, !tbaa !2
  %12 = load i8*, i8** %end, align 4, !tbaa !2
  %cmp = icmp ne i8* %11, %12
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %13 = load i8*, i8** %p, align 4, !tbaa !2
  store i8 0, i8* %13, align 1, !tbaa !16
  %14 = load i8*, i8** %p, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %14, i32 1
  store i8* %incdec.ptr, i8** %p, align 4, !tbaa !2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %15 = load i32, i32* %byteCount.addr, align 4, !tbaa !12
  %16 = load %struct.avifRWStream*, %struct.avifRWStream** %stream.addr, align 4, !tbaa !2
  %offset2 = getelementptr inbounds %struct.avifRWStream, %struct.avifRWStream* %16, i32 0, i32 1
  %17 = load i32, i32* %offset2, align 4, !tbaa !25
  %add = add i32 %17, %15
  store i32 %add, i32* %offset2, align 4, !tbaa !25
  %18 = bitcast i8** %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %19 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  ret void
}

declare void @avifRWDataRealloc(%struct.avifRWData*, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 0}
!7 = !{!"avifROStream", !3, i64 0, !8, i64 4}
!8 = !{!"long", !4, i64 0}
!9 = !{!10, !3, i64 0}
!10 = !{!"avifROData", !3, i64 0, !8, i64 4}
!11 = !{!7, !8, i64 4}
!12 = !{!8, !8, i64 0}
!13 = !{!10, !8, i64 4}
!14 = !{!15, !15, i64 0}
!15 = !{!"long long", !4, i64 0}
!16 = !{!4, !4, i64 0}
!17 = !{!18, !18, i64 0}
!18 = !{!"short", !4, i64 0}
!19 = !{!20, !20, i64 0}
!20 = !{!"int", !4, i64 0}
!21 = !{!22, !8, i64 0}
!22 = !{!"avifBoxHeader", !8, i64 0, !4, i64 4}
!23 = !{!24, !3, i64 0}
!24 = !{!"avifRWStream", !3, i64 0, !8, i64 4}
!25 = !{!24, !8, i64 4}
!26 = !{!27, !8, i64 4}
!27 = !{!"avifRWData", !3, i64 0, !8, i64 4}
!28 = !{!27, !3, i64 0}
