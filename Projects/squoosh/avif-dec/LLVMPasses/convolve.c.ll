; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/convolve.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/convolve.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.InterpFilterParams = type { i16*, i16, i16, i8 }
%struct.ConvolveParams = type { i32, i16*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.scale_factors = type { i32, i32, i32, i32, i32 (i32, %struct.scale_factors*)*, i32 (i32, %struct.scale_factors*)*, [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]] }

@av1_convolve_2d_sobel_y_c.sobel_a = internal constant [3 x i16] [i16 1, i16 0, i16 -1], align 256
@av1_convolve_2d_sobel_y_c.sobel_b = internal constant [3 x i16] [i16 1, i16 2, i16 1], align 256

; Function Attrs: nounwind
define hidden void @av1_convolve_horiz_rs_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, i16* %x_filters, i32 %x0_qn, i32 %x_step_qn) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %x_filters.addr = alloca i16*, align 4
  %x0_qn.addr = alloca i32, align 4
  %x_step_qn.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x_qn = alloca i32, align 4
  %x = alloca i32, align 4
  %src_x = alloca i8*, align 4
  %x_filter_idx = alloca i32, align 4
  %x_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i16* %x_filters, i16** %x_filters.addr, align 4, !tbaa !2
  store i32 %x0_qn, i32* %x0_qn.addr, align 4, !tbaa !6
  store i32 %x_step_qn, i32* %x_step_qn.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 -3
  store i8* %add.ptr, i8** %src.addr, align 4, !tbaa !2
  %1 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc24, %entry
  %2 = load i32, i32* %y, align 4, !tbaa !6
  %3 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  br label %for.end26

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %x_qn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %x0_qn.addr, align 4, !tbaa !6
  store i32 %6, i32* %x_qn, align 4, !tbaa !6
  %7 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc19, %for.body
  %8 = load i32, i32* %x, align 4, !tbaa !6
  %9 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %10 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  br label %for.end21

for.body4:                                        ; preds = %for.cond1
  %11 = bitcast i8** %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %13 = load i32, i32* %x_qn, align 4, !tbaa !6
  %shr = ashr i32 %13, 14
  %arrayidx = getelementptr inbounds i8, i8* %12, i32 %shr
  store i8* %arrayidx, i8** %src_x, align 4, !tbaa !2
  %14 = bitcast i32* %x_filter_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load i32, i32* %x_qn, align 4, !tbaa !6
  %and = and i32 %15, 16383
  %shr5 = ashr i32 %and, 8
  store i32 %shr5, i32* %x_filter_idx, align 4, !tbaa !6
  %16 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i16*, i16** %x_filters.addr, align 4, !tbaa !2
  %18 = load i32, i32* %x_filter_idx, align 4, !tbaa !6
  %mul = mul nsw i32 %18, 8
  %arrayidx6 = getelementptr inbounds i16, i16* %17, i32 %mul
  store i16* %arrayidx6, i16** %x_filter, align 4, !tbaa !2
  %19 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  store i32 0, i32* %sum, align 4, !tbaa !6
  %20 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body4
  %21 = load i32, i32* %k, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %21, 8
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  store i32 8, i32* %cleanup.dest.slot, align 4
  %22 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  br label %for.end

for.body10:                                       ; preds = %for.cond7
  %23 = load i8*, i8** %src_x, align 4, !tbaa !2
  %24 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i8, i8* %23, i32 %24
  %25 = load i8, i8* %arrayidx11, align 1, !tbaa !8
  %conv = zext i8 %25 to i32
  %26 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %27 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i16, i16* %26, i32 %27
  %28 = load i16, i16* %arrayidx12, align 2, !tbaa !9
  %conv13 = sext i16 %28 to i32
  %mul14 = mul nsw i32 %conv, %conv13
  %29 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %29, %mul14
  store i32 %add, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %30 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond.cleanup9
  %31 = load i32, i32* %sum, align 4, !tbaa !6
  %add15 = add nsw i32 %31, 64
  %shr16 = ashr i32 %add15, 7
  %call = call zeroext i8 @clip_pixel(i32 %shr16)
  %32 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %33 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds i8, i8* %32, i32 %33
  store i8 %call, i8* %arrayidx17, align 1, !tbaa !8
  %34 = load i32, i32* %x_step_qn.addr, align 4, !tbaa !6
  %35 = load i32, i32* %x_qn, align 4, !tbaa !6
  %add18 = add nsw i32 %35, %34
  store i32 %add18, i32* %x_qn, align 4, !tbaa !6
  %36 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = bitcast i32* %x_filter_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %39 = bitcast i8** %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  br label %for.inc19

for.inc19:                                        ; preds = %for.end
  %40 = load i32, i32* %x, align 4, !tbaa !6
  %inc20 = add nsw i32 %40, 1
  store i32 %inc20, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.end21:                                        ; preds = %for.cond.cleanup3
  %41 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %42 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8, i8* %42, i32 %41
  store i8* %add.ptr22, i8** %src.addr, align 4, !tbaa !2
  %43 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %44 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %add.ptr23 = getelementptr inbounds i8, i8* %44, i32 %43
  store i8* %add.ptr23, i8** %dst.addr, align 4, !tbaa !2
  %45 = bitcast i32* %x_qn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  br label %for.inc24

for.inc24:                                        ; preds = %for.end21
  %46 = load i32, i32* %y, align 4, !tbaa !6
  %inc25 = add nsw i32 %46, 1
  store i32 %inc25, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end26:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @clip_pixel(i32 %val) #2 {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  %0 = load i32, i32* %val.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %0, 255
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4, !tbaa !6
  %cmp1 = icmp slt i32 %1, 0
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %2 = load i32, i32* %val.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 0, %cond.true2 ], [ %2, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 255, %cond.true ], [ %cond, %cond.end ]
  %conv = trunc i32 %cond5 to i8
  ret i8 %conv
}

; Function Attrs: nounwind
define hidden void @av1_highbd_convolve_horiz_rs_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, i16* %x_filters, i32 %x0_qn, i32 %x_step_qn, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %x_filters.addr = alloca i16*, align 4
  %x0_qn.addr = alloca i32, align 4
  %x_step_qn.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x_qn = alloca i32, align 4
  %x = alloca i32, align 4
  %src_x = alloca i16*, align 4
  %x_filter_idx = alloca i32, align 4
  %x_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i16* %x_filters, i16** %x_filters.addr, align 4, !tbaa !2
  store i32 %x0_qn, i32* %x0_qn.addr, align 4, !tbaa !6
  store i32 %x_step_qn, i32* %x_step_qn.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %0, i32 -3
  store i16* %add.ptr, i16** %src.addr, align 4, !tbaa !2
  %1 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc24, %entry
  %2 = load i32, i32* %y, align 4, !tbaa !6
  %3 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  br label %for.end26

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %x_qn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %x0_qn.addr, align 4, !tbaa !6
  store i32 %6, i32* %x_qn, align 4, !tbaa !6
  %7 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc19, %for.body
  %8 = load i32, i32* %x, align 4, !tbaa !6
  %9 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %10 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  br label %for.end21

for.body4:                                        ; preds = %for.cond1
  %11 = bitcast i16** %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %13 = load i32, i32* %x_qn, align 4, !tbaa !6
  %shr = ashr i32 %13, 14
  %arrayidx = getelementptr inbounds i16, i16* %12, i32 %shr
  store i16* %arrayidx, i16** %src_x, align 4, !tbaa !2
  %14 = bitcast i32* %x_filter_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load i32, i32* %x_qn, align 4, !tbaa !6
  %and = and i32 %15, 16383
  %shr5 = ashr i32 %and, 8
  store i32 %shr5, i32* %x_filter_idx, align 4, !tbaa !6
  %16 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i16*, i16** %x_filters.addr, align 4, !tbaa !2
  %18 = load i32, i32* %x_filter_idx, align 4, !tbaa !6
  %mul = mul nsw i32 %18, 8
  %arrayidx6 = getelementptr inbounds i16, i16* %17, i32 %mul
  store i16* %arrayidx6, i16** %x_filter, align 4, !tbaa !2
  %19 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  store i32 0, i32* %sum, align 4, !tbaa !6
  %20 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body4
  %21 = load i32, i32* %k, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %21, 8
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  store i32 8, i32* %cleanup.dest.slot, align 4
  %22 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  br label %for.end

for.body10:                                       ; preds = %for.cond7
  %23 = load i16*, i16** %src_x, align 4, !tbaa !2
  %24 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i16, i16* %23, i32 %24
  %25 = load i16, i16* %arrayidx11, align 2, !tbaa !9
  %conv = zext i16 %25 to i32
  %26 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %27 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i16, i16* %26, i32 %27
  %28 = load i16, i16* %arrayidx12, align 2, !tbaa !9
  %conv13 = sext i16 %28 to i32
  %mul14 = mul nsw i32 %conv, %conv13
  %29 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %29, %mul14
  store i32 %add, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %30 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond.cleanup9
  %31 = load i32, i32* %sum, align 4, !tbaa !6
  %add15 = add nsw i32 %31, 64
  %shr16 = ashr i32 %add15, 7
  %32 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call zeroext i16 @clip_pixel_highbd(i32 %shr16, i32 %32)
  %33 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %34 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds i16, i16* %33, i32 %34
  store i16 %call, i16* %arrayidx17, align 2, !tbaa !9
  %35 = load i32, i32* %x_step_qn.addr, align 4, !tbaa !6
  %36 = load i32, i32* %x_qn, align 4, !tbaa !6
  %add18 = add nsw i32 %36, %35
  store i32 %add18, i32* %x_qn, align 4, !tbaa !6
  %37 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %39 = bitcast i32* %x_filter_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %40 = bitcast i16** %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.inc19

for.inc19:                                        ; preds = %for.end
  %41 = load i32, i32* %x, align 4, !tbaa !6
  %inc20 = add nsw i32 %41, 1
  store i32 %inc20, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.end21:                                        ; preds = %for.cond.cleanup3
  %42 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %43 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i16, i16* %43, i32 %42
  store i16* %add.ptr22, i16** %src.addr, align 4, !tbaa !2
  %44 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %45 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr23 = getelementptr inbounds i16, i16* %45, i32 %44
  store i16* %add.ptr23, i16** %dst.addr, align 4, !tbaa !2
  %46 = bitcast i32* %x_qn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %for.inc24

for.inc24:                                        ; preds = %for.end21
  %47 = load i32, i32* %y, align 4, !tbaa !6
  %inc25 = add nsw i32 %47, 1
  store i32 %inc25, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end26:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @clip_pixel_highbd(i32 %val, i32 %bd) #2 {
entry:
  %retval = alloca i16, align 2
  %val.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %bd.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 8, label %sw.bb
    i32 10, label %sw.bb1
    i32 12, label %sw.bb4
  ]

sw.bb:                                            ; preds = %entry
  br label %sw.default

sw.default:                                       ; preds = %entry, %sw.bb
  %1 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call = call i32 @clamp(i32 %1, i32 0, i32 255)
  %conv = trunc i32 %call to i16
  store i16 %conv, i16* %retval, align 2
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call2 = call i32 @clamp(i32 %2, i32 0, i32 1023)
  %conv3 = trunc i32 %call2 to i16
  store i16 %conv3, i16* %retval, align 2
  br label %return

sw.bb4:                                           ; preds = %entry
  %3 = load i32, i32* %val.addr, align 4, !tbaa !6
  %call5 = call i32 @clamp(i32 %3, i32 0, i32 4095)
  %conv6 = trunc i32 %call5 to i16
  store i16 %conv6, i16* %retval, align 2
  br label %return

return:                                           ; preds = %sw.bb4, %sw.bb1, %sw.default
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: nounwind
define hidden void @av1_convolve_2d_sobel_y_c(i8* %src, i32 %src_stride, double* %dst, i32 %dst_stride, i32 %w, i32 %h, i32 %dir, double %norm) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca double*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %dir.addr = alloca i32, align 4
  %norm.addr = alloca double, align 8
  %im_block = alloca [17280 x i16], align 16
  %taps = alloca i32, align 4
  %im_h = alloca i32, align 4
  %im_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %src_horiz = alloca i8*, align 4
  %x_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %sum = alloca i16, align 2
  %k = alloca i32, align 4
  %src_vert = alloca i16*, align 4
  %y_filter = alloca i16*, align 4
  %y32 = alloca i32, align 4
  %x38 = alloca i32, align 4
  %sum44 = alloca i16, align 2
  %k45 = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store double* %dst, double** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %dir, i32* %dir.addr, align 4, !tbaa !6
  store double %norm, double* %norm.addr, align 8, !tbaa !11
  %0 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 34560, i8* %0) #4
  %1 = bitcast i32* %taps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 3, i32* %taps, align 4, !tbaa !6
  %2 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i32, i32* %h.addr, align 4, !tbaa !6
  %add = add nsw i32 %3, 3
  %sub = sub nsw i32 %add, 1
  store i32 %sub, i32* %im_h, align 4, !tbaa !6
  %4 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i32, i32* %w.addr, align 4, !tbaa !6
  store i32 %5, i32* %im_stride, align 4, !tbaa !6
  %6 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  store i32 1, i32* %fo_vert, align 4, !tbaa !6
  %7 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  store i32 1, i32* %fo_horiz, align 4, !tbaa !6
  %8 = bitcast i8** %src_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %10 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 1, %10
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %idx.neg
  store i8* %add.ptr, i8** %src_horiz, align 4, !tbaa !2
  %11 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %12, 0
  %13 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i16* getelementptr inbounds ([3 x i16], [3 x i16]* @av1_convolve_2d_sobel_y_c.sobel_a, i32 0, i32 0), i16* getelementptr inbounds ([3 x i16], [3 x i16]* @av1_convolve_2d_sobel_y_c.sobel_b, i32 0, i32 0)
  store i16* %cond, i16** %x_filter, align 4, !tbaa !2
  %14 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %entry
  %15 = load i32, i32* %y, align 4, !tbaa !6
  %16 = load i32, i32* %im_h, align 4, !tbaa !6
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %for.end27

for.body:                                         ; preds = %for.cond
  %18 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc22, %for.body
  %19 = load i32, i32* %x, align 4, !tbaa !6
  %20 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %19, %20
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  br label %for.end24

for.body4:                                        ; preds = %for.cond1
  %22 = bitcast i16* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %22) #4
  store i16 0, i16* %sum, align 2, !tbaa !9
  %23 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body4
  %24 = load i32, i32* %k, align 4, !tbaa !6
  %cmp6 = icmp slt i32 %24, 3
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  store i32 8, i32* %cleanup.dest.slot, align 4
  %25 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  br label %for.end

for.body8:                                        ; preds = %for.cond5
  %26 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %27 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %26, i32 %27
  %28 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = sext i16 %28 to i32
  %29 = load i8*, i8** %src_horiz, align 4, !tbaa !2
  %30 = load i32, i32* %y, align 4, !tbaa !6
  %31 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul9 = mul nsw i32 %30, %31
  %32 = load i32, i32* %x, align 4, !tbaa !6
  %add10 = add nsw i32 %mul9, %32
  %sub11 = sub nsw i32 %add10, 1
  %33 = load i32, i32* %k, align 4, !tbaa !6
  %add12 = add nsw i32 %sub11, %33
  %arrayidx13 = getelementptr inbounds i8, i8* %29, i32 %add12
  %34 = load i8, i8* %arrayidx13, align 1, !tbaa !8
  %conv14 = zext i8 %34 to i32
  %mul15 = mul nsw i32 %conv, %conv14
  %35 = load i16, i16* %sum, align 2, !tbaa !9
  %conv16 = sext i16 %35 to i32
  %add17 = add nsw i32 %conv16, %mul15
  %conv18 = trunc i32 %add17 to i16
  store i16 %conv18, i16* %sum, align 2, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %36 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond5

for.end:                                          ; preds = %for.cond.cleanup7
  %37 = load i16, i16* %sum, align 2, !tbaa !9
  %38 = load i32, i32* %y, align 4, !tbaa !6
  %39 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul19 = mul nsw i32 %38, %39
  %40 = load i32, i32* %x, align 4, !tbaa !6
  %add20 = add nsw i32 %mul19, %40
  %arrayidx21 = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 %add20
  store i16 %37, i16* %arrayidx21, align 2, !tbaa !9
  %41 = bitcast i16* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %41) #4
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %42 = load i32, i32* %x, align 4, !tbaa !6
  %inc23 = add nsw i32 %42, 1
  store i32 %inc23, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.end24:                                        ; preds = %for.cond.cleanup3
  br label %for.inc25

for.inc25:                                        ; preds = %for.end24
  %43 = load i32, i32* %y, align 4, !tbaa !6
  %inc26 = add nsw i32 %43, 1
  store i32 %inc26, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end27:                                        ; preds = %for.cond.cleanup
  %44 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #4
  %arraydecay = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 0
  %45 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul28 = mul nsw i32 1, %45
  %add.ptr29 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul28
  store i16* %add.ptr29, i16** %src_vert, align 4, !tbaa !2
  %46 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #4
  %47 = load i32, i32* %dir.addr, align 4, !tbaa !6
  %tobool30 = icmp ne i32 %47, 0
  %48 = zext i1 %tobool30 to i64
  %cond31 = select i1 %tobool30, i16* getelementptr inbounds ([3 x i16], [3 x i16]* @av1_convolve_2d_sobel_y_c.sobel_b, i32 0, i32 0), i16* getelementptr inbounds ([3 x i16], [3 x i16]* @av1_convolve_2d_sobel_y_c.sobel_a, i32 0, i32 0)
  store i16* %cond31, i16** %y_filter, align 4, !tbaa !2
  %49 = bitcast i32* %y32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #4
  store i32 0, i32* %y32, align 4, !tbaa !6
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc75, %for.end27
  %50 = load i32, i32* %y32, align 4, !tbaa !6
  %51 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp34 = icmp slt i32 %50, %51
  br i1 %cmp34, label %for.body37, label %for.cond.cleanup36

for.cond.cleanup36:                               ; preds = %for.cond33
  store i32 11, i32* %cleanup.dest.slot, align 4
  %52 = bitcast i32* %y32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  br label %for.end77

for.body37:                                       ; preds = %for.cond33
  %53 = bitcast i32* %x38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #4
  store i32 0, i32* %x38, align 4, !tbaa !6
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc72, %for.body37
  %54 = load i32, i32* %x38, align 4, !tbaa !6
  %55 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp40 = icmp slt i32 %54, %55
  br i1 %cmp40, label %for.body43, label %for.cond.cleanup42

for.cond.cleanup42:                               ; preds = %for.cond39
  store i32 14, i32* %cleanup.dest.slot, align 4
  %56 = bitcast i32* %x38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  br label %for.end74

for.body43:                                       ; preds = %for.cond39
  %57 = bitcast i16* %sum44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %57) #4
  store i16 0, i16* %sum44, align 2, !tbaa !9
  %58 = bitcast i32* %k45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #4
  store i32 0, i32* %k45, align 4, !tbaa !6
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc63, %for.body43
  %59 = load i32, i32* %k45, align 4, !tbaa !6
  %cmp47 = icmp slt i32 %59, 3
  br i1 %cmp47, label %for.body50, label %for.cond.cleanup49

for.cond.cleanup49:                               ; preds = %for.cond46
  store i32 17, i32* %cleanup.dest.slot, align 4
  %60 = bitcast i32* %k45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  br label %for.end65

for.body50:                                       ; preds = %for.cond46
  %61 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %62 = load i32, i32* %k45, align 4, !tbaa !6
  %arrayidx51 = getelementptr inbounds i16, i16* %61, i32 %62
  %63 = load i16, i16* %arrayidx51, align 2, !tbaa !9
  %conv52 = sext i16 %63 to i32
  %64 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %65 = load i32, i32* %y32, align 4, !tbaa !6
  %sub53 = sub nsw i32 %65, 1
  %66 = load i32, i32* %k45, align 4, !tbaa !6
  %add54 = add nsw i32 %sub53, %66
  %67 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul55 = mul nsw i32 %add54, %67
  %68 = load i32, i32* %x38, align 4, !tbaa !6
  %add56 = add nsw i32 %mul55, %68
  %arrayidx57 = getelementptr inbounds i16, i16* %64, i32 %add56
  %69 = load i16, i16* %arrayidx57, align 2, !tbaa !9
  %conv58 = sext i16 %69 to i32
  %mul59 = mul nsw i32 %conv52, %conv58
  %70 = load i16, i16* %sum44, align 2, !tbaa !9
  %conv60 = sext i16 %70 to i32
  %add61 = add nsw i32 %conv60, %mul59
  %conv62 = trunc i32 %add61 to i16
  store i16 %conv62, i16* %sum44, align 2, !tbaa !9
  br label %for.inc63

for.inc63:                                        ; preds = %for.body50
  %71 = load i32, i32* %k45, align 4, !tbaa !6
  %inc64 = add nsw i32 %71, 1
  store i32 %inc64, i32* %k45, align 4, !tbaa !6
  br label %for.cond46

for.end65:                                        ; preds = %for.cond.cleanup49
  %72 = load i16, i16* %sum44, align 2, !tbaa !9
  %conv66 = sext i16 %72 to i32
  %conv67 = sitofp i32 %conv66 to double
  %73 = load double, double* %norm.addr, align 8, !tbaa !11
  %mul68 = fmul double %conv67, %73
  %74 = load double*, double** %dst.addr, align 4, !tbaa !2
  %75 = load i32, i32* %y32, align 4, !tbaa !6
  %76 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul69 = mul nsw i32 %75, %76
  %77 = load i32, i32* %x38, align 4, !tbaa !6
  %add70 = add nsw i32 %mul69, %77
  %arrayidx71 = getelementptr inbounds double, double* %74, i32 %add70
  store double %mul68, double* %arrayidx71, align 8, !tbaa !11
  %78 = bitcast i16* %sum44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %78) #4
  br label %for.inc72

for.inc72:                                        ; preds = %for.end65
  %79 = load i32, i32* %x38, align 4, !tbaa !6
  %inc73 = add nsw i32 %79, 1
  store i32 %inc73, i32* %x38, align 4, !tbaa !6
  br label %for.cond39

for.end74:                                        ; preds = %for.cond.cleanup42
  br label %for.inc75

for.inc75:                                        ; preds = %for.end74
  %80 = load i32, i32* %y32, align 4, !tbaa !6
  %inc76 = add nsw i32 %80, 1
  store i32 %inc76, i32* %y32, align 4, !tbaa !6
  br label %for.cond33

for.end77:                                        ; preds = %for.cond.cleanup36
  %81 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i8** %src_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %taps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 34560, i8* %90) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_convolve_2d_sr_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %im_block = alloca [17280 x i16], align 16
  %im_h = alloca i32, align 4
  %im_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %bd = alloca i32, align 4
  %bits = alloca i32, align 4
  %src_horiz = alloca i8*, align 4
  %x_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  %src_vert = alloca i16*, align 4
  %y_filter = alloca i16*, align 4
  %offset_bits = alloca i32, align 4
  %y52 = alloca i32, align 4
  %x58 = alloca i32, align 4
  %sum64 = alloca i32, align 4
  %k66 = alloca i32, align 4
  %res = alloca i16, align 2
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 34560, i8* %0) #4
  %1 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %h.addr, align 4, !tbaa !6
  %3 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %3, i32 0, i32 1
  %4 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %4 to i32
  %add = add nsw i32 %2, %conv
  %sub = sub nsw i32 %add, 1
  store i32 %sub, i32* %im_h, align 4, !tbaa !6
  %5 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %w.addr, align 4, !tbaa !6
  store i32 %6, i32* %im_stride, align 4, !tbaa !6
  %7 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps1 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %8, i32 0, i32 1
  %9 = load i16, i16* %taps1, align 4, !tbaa !13
  %conv2 = zext i16 %9 to i32
  %div = sdiv i32 %conv2, 2
  %sub3 = sub nsw i32 %div, 1
  store i32 %sub3, i32* %fo_vert, align 4, !tbaa !6
  %10 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps4 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %11, i32 0, i32 1
  %12 = load i16, i16* %taps4, align 4, !tbaa !13
  %conv5 = zext i16 %12 to i32
  %div6 = sdiv i32 %conv5, 2
  %sub7 = sub nsw i32 %div6, 1
  store i32 %sub7, i32* %fo_horiz, align 4, !tbaa !6
  %13 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  store i32 8, i32* %bd, align 4, !tbaa !6
  %14 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %15, i32 0, i32 3
  %16 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub8 = sub nsw i32 14, %16
  %17 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %17, i32 0, i32 4
  %18 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub9 = sub nsw i32 %sub8, %18
  store i32 %sub9, i32* %bits, align 4, !tbaa !6
  %19 = bitcast i8** %src_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %21 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %22 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %21, %22
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i8, i8* %20, i32 %idx.neg
  store i8* %add.ptr, i8** %src_horiz, align 4, !tbaa !2
  %23 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %25 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %and = and i32 %25, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %24, i32 %and)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %26 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc43, %entry
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %28 = load i32, i32* %im_h, align 4, !tbaa !6
  %cmp = icmp slt i32 %27, %28
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %29 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  br label %for.end45

for.body:                                         ; preds = %for.cond
  %30 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc40, %for.body
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %32 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp12 = icmp slt i32 %31, %32
  br i1 %cmp12, label %for.body15, label %for.cond.cleanup14

for.cond.cleanup14:                               ; preds = %for.cond11
  store i32 5, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  br label %for.end42

for.body15:                                       ; preds = %for.cond11
  %34 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  store i32 16384, i32* %sum, align 4, !tbaa !6
  %35 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc, %for.body15
  %36 = load i32, i32* %k, align 4, !tbaa !6
  %37 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps17 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %37, i32 0, i32 1
  %38 = load i16, i16* %taps17, align 4, !tbaa !13
  %conv18 = zext i16 %38 to i32
  %cmp19 = icmp slt i32 %36, %conv18
  br i1 %cmp19, label %for.body22, label %for.cond.cleanup21

for.cond.cleanup21:                               ; preds = %for.cond16
  store i32 8, i32* %cleanup.dest.slot, align 4
  %39 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  br label %for.end

for.body22:                                       ; preds = %for.cond16
  %40 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %41 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %40, i32 %41
  %42 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv23 = sext i16 %42 to i32
  %43 = load i8*, i8** %src_horiz, align 4, !tbaa !2
  %44 = load i32, i32* %y, align 4, !tbaa !6
  %45 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul24 = mul nsw i32 %44, %45
  %46 = load i32, i32* %x, align 4, !tbaa !6
  %add25 = add nsw i32 %mul24, %46
  %47 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub26 = sub nsw i32 %add25, %47
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %add27 = add nsw i32 %sub26, %48
  %arrayidx28 = getelementptr inbounds i8, i8* %43, i32 %add27
  %49 = load i8, i8* %arrayidx28, align 1, !tbaa !8
  %conv29 = zext i8 %49 to i32
  %mul30 = mul nsw i32 %conv23, %conv29
  %50 = load i32, i32* %sum, align 4, !tbaa !6
  %add31 = add nsw i32 %50, %mul30
  store i32 %add31, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body22
  %51 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond16

for.end:                                          ; preds = %for.cond.cleanup21
  %52 = load i32, i32* %sum, align 4, !tbaa !6
  %53 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_032 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %53, i32 0, i32 3
  %54 = load i32, i32* %round_032, align 4, !tbaa !15
  %shl = shl i32 1, %54
  %shr = ashr i32 %shl, 1
  %add33 = add nsw i32 %52, %shr
  %55 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_034 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %55, i32 0, i32 3
  %56 = load i32, i32* %round_034, align 4, !tbaa !15
  %shr35 = ashr i32 %add33, %56
  %conv36 = trunc i32 %shr35 to i16
  %57 = load i32, i32* %y, align 4, !tbaa !6
  %58 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul37 = mul nsw i32 %57, %58
  %59 = load i32, i32* %x, align 4, !tbaa !6
  %add38 = add nsw i32 %mul37, %59
  %arrayidx39 = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 %add38
  store i16 %conv36, i16* %arrayidx39, align 2, !tbaa !9
  %60 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  br label %for.inc40

for.inc40:                                        ; preds = %for.end
  %61 = load i32, i32* %x, align 4, !tbaa !6
  %inc41 = add nsw i32 %61, 1
  store i32 %inc41, i32* %x, align 4, !tbaa !6
  br label %for.cond11

for.end42:                                        ; preds = %for.cond.cleanup14
  br label %for.inc43

for.inc43:                                        ; preds = %for.end42
  %62 = load i32, i32* %y, align 4, !tbaa !6
  %inc44 = add nsw i32 %62, 1
  store i32 %inc44, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end45:                                        ; preds = %for.cond.cleanup
  %63 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #4
  %arraydecay = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 0
  %64 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %65 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul46 = mul nsw i32 %64, %65
  %add.ptr47 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul46
  store i16* %add.ptr47, i16** %src_vert, align 4, !tbaa !2
  %66 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #4
  %67 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %68 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %and48 = and i32 %68, 15
  %call49 = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %67, i32 %and48)
  store i16* %call49, i16** %y_filter, align 4, !tbaa !2
  %69 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %70 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_050 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %70, i32 0, i32 3
  %71 = load i32, i32* %round_050, align 4, !tbaa !15
  %sub51 = sub nsw i32 22, %71
  store i32 %sub51, i32* %offset_bits, align 4, !tbaa !6
  %72 = bitcast i32* %y52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #4
  store i32 0, i32* %y52, align 4, !tbaa !6
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc115, %for.end45
  %73 = load i32, i32* %y52, align 4, !tbaa !6
  %74 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp54 = icmp slt i32 %73, %74
  br i1 %cmp54, label %for.body57, label %for.cond.cleanup56

for.cond.cleanup56:                               ; preds = %for.cond53
  store i32 11, i32* %cleanup.dest.slot, align 4
  %75 = bitcast i32* %y52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  br label %for.end117

for.body57:                                       ; preds = %for.cond53
  %76 = bitcast i32* %x58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #4
  store i32 0, i32* %x58, align 4, !tbaa !6
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc112, %for.body57
  %77 = load i32, i32* %x58, align 4, !tbaa !6
  %78 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp60 = icmp slt i32 %77, %78
  br i1 %cmp60, label %for.body63, label %for.cond.cleanup62

for.cond.cleanup62:                               ; preds = %for.cond59
  store i32 14, i32* %cleanup.dest.slot, align 4
  %79 = bitcast i32* %x58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  br label %for.end114

for.body63:                                       ; preds = %for.cond59
  %80 = bitcast i32* %sum64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #4
  %81 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %shl65 = shl i32 1, %81
  store i32 %shl65, i32* %sum64, align 4, !tbaa !6
  %82 = bitcast i32* %k66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #4
  store i32 0, i32* %k66, align 4, !tbaa !6
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc84, %for.body63
  %83 = load i32, i32* %k66, align 4, !tbaa !6
  %84 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps68 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %84, i32 0, i32 1
  %85 = load i16, i16* %taps68, align 4, !tbaa !13
  %conv69 = zext i16 %85 to i32
  %cmp70 = icmp slt i32 %83, %conv69
  br i1 %cmp70, label %for.body73, label %for.cond.cleanup72

for.cond.cleanup72:                               ; preds = %for.cond67
  store i32 17, i32* %cleanup.dest.slot, align 4
  %86 = bitcast i32* %k66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  br label %for.end86

for.body73:                                       ; preds = %for.cond67
  %87 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %88 = load i32, i32* %k66, align 4, !tbaa !6
  %arrayidx74 = getelementptr inbounds i16, i16* %87, i32 %88
  %89 = load i16, i16* %arrayidx74, align 2, !tbaa !9
  %conv75 = sext i16 %89 to i32
  %90 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %91 = load i32, i32* %y52, align 4, !tbaa !6
  %92 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub76 = sub nsw i32 %91, %92
  %93 = load i32, i32* %k66, align 4, !tbaa !6
  %add77 = add nsw i32 %sub76, %93
  %94 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul78 = mul nsw i32 %add77, %94
  %95 = load i32, i32* %x58, align 4, !tbaa !6
  %add79 = add nsw i32 %mul78, %95
  %arrayidx80 = getelementptr inbounds i16, i16* %90, i32 %add79
  %96 = load i16, i16* %arrayidx80, align 2, !tbaa !9
  %conv81 = sext i16 %96 to i32
  %mul82 = mul nsw i32 %conv75, %conv81
  %97 = load i32, i32* %sum64, align 4, !tbaa !6
  %add83 = add nsw i32 %97, %mul82
  store i32 %add83, i32* %sum64, align 4, !tbaa !6
  br label %for.inc84

for.inc84:                                        ; preds = %for.body73
  %98 = load i32, i32* %k66, align 4, !tbaa !6
  %inc85 = add nsw i32 %98, 1
  store i32 %inc85, i32* %k66, align 4, !tbaa !6
  br label %for.cond67

for.end86:                                        ; preds = %for.cond.cleanup72
  %99 = bitcast i16* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %99) #4
  %100 = load i32, i32* %sum64, align 4, !tbaa !6
  %101 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_187 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %101, i32 0, i32 4
  %102 = load i32, i32* %round_187, align 4, !tbaa !17
  %shl88 = shl i32 1, %102
  %shr89 = ashr i32 %shl88, 1
  %add90 = add nsw i32 %100, %shr89
  %103 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_191 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %103, i32 0, i32 4
  %104 = load i32, i32* %round_191, align 4, !tbaa !17
  %shr92 = ashr i32 %add90, %104
  %105 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %106 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_193 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %106, i32 0, i32 4
  %107 = load i32, i32* %round_193, align 4, !tbaa !17
  %sub94 = sub nsw i32 %105, %107
  %shl95 = shl i32 1, %sub94
  %108 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %109 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_196 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %109, i32 0, i32 4
  %110 = load i32, i32* %round_196, align 4, !tbaa !17
  %sub97 = sub nsw i32 %108, %110
  %sub98 = sub nsw i32 %sub97, 1
  %shl99 = shl i32 1, %sub98
  %add100 = add nsw i32 %shl95, %shl99
  %sub101 = sub nsw i32 %shr92, %add100
  %conv102 = trunc i32 %sub101 to i16
  store i16 %conv102, i16* %res, align 2, !tbaa !9
  %111 = load i16, i16* %res, align 2, !tbaa !9
  %conv103 = sext i16 %111 to i32
  %112 = load i32, i32* %bits, align 4, !tbaa !6
  %shl104 = shl i32 1, %112
  %shr105 = ashr i32 %shl104, 1
  %add106 = add nsw i32 %conv103, %shr105
  %113 = load i32, i32* %bits, align 4, !tbaa !6
  %shr107 = ashr i32 %add106, %113
  %call108 = call zeroext i8 @clip_pixel(i32 %shr107)
  %114 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %115 = load i32, i32* %y52, align 4, !tbaa !6
  %116 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul109 = mul nsw i32 %115, %116
  %117 = load i32, i32* %x58, align 4, !tbaa !6
  %add110 = add nsw i32 %mul109, %117
  %arrayidx111 = getelementptr inbounds i8, i8* %114, i32 %add110
  store i8 %call108, i8* %arrayidx111, align 1, !tbaa !8
  %118 = bitcast i16* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %118) #4
  %119 = bitcast i32* %sum64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #4
  br label %for.inc112

for.inc112:                                       ; preds = %for.end86
  %120 = load i32, i32* %x58, align 4, !tbaa !6
  %inc113 = add nsw i32 %120, 1
  store i32 %inc113, i32* %x58, align 4, !tbaa !6
  br label %for.cond59

for.end114:                                       ; preds = %for.cond.cleanup62
  br label %for.inc115

for.inc115:                                       ; preds = %for.end114
  %121 = load i32, i32* %y52, align 4, !tbaa !6
  %inc116 = add nsw i32 %121, 1
  store i32 %inc116, i32* %y52, align 4, !tbaa !6
  br label %for.cond53

for.end117:                                       ; preds = %for.cond.cleanup56
  %122 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #4
  %123 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #4
  %124 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #4
  %125 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #4
  %126 = bitcast i8** %src_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #4
  %127 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #4
  %128 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #4
  %129 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #4
  %130 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #4
  %131 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #4
  %132 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #4
  %133 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 34560, i8* %133) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %filter_params, i32 %subpel) #2 {
entry:
  %filter_params.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel.addr = alloca i32, align 4
  store %struct.InterpFilterParams* %filter_params, %struct.InterpFilterParams** %filter_params.addr, align 4, !tbaa !2
  store i32 %subpel, i32* %subpel.addr, align 4, !tbaa !6
  %0 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params.addr, align 4, !tbaa !2
  %filter_ptr = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %0, i32 0, i32 0
  %1 = load i16*, i16** %filter_ptr, align 4, !tbaa !18
  %2 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %2, i32 0, i32 1
  %3 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %3 to i32
  %4 = load i32, i32* %subpel.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %4
  %add.ptr = getelementptr inbounds i16, i16* %1, i32 %mul
  ret i16* %add.ptr
}

; Function Attrs: nounwind
define hidden void @av1_convolve_y_sr_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %fo_vert = alloca i32, align 4
  %y_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i32, align 4
  %k = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %1, i32 0, i32 1
  %2 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %2 to i32
  %div = sdiv i32 %conv, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %fo_vert, align 4, !tbaa !6
  %3 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %4 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %5 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %6 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %8 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %and = and i32 %8, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %7, i32 %and)
  store i16* %call, i16** %y_filter, align 4, !tbaa !2
  %9 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc29, %entry
  %10 = load i32, i32* %y, align 4, !tbaa !6
  %11 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %12 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  br label %for.end31

for.body:                                         ; preds = %for.cond
  %13 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc26, %for.body
  %14 = load i32, i32* %x, align 4, !tbaa !6
  %15 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %14, %15
  br i1 %cmp3, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %16 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  br label %for.end28

for.body6:                                        ; preds = %for.cond2
  %17 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  store i32 0, i32* %res, align 4, !tbaa !6
  %18 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body6
  %19 = load i32, i32* %k, align 4, !tbaa !6
  %20 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps8 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %20, i32 0, i32 1
  %21 = load i16, i16* %taps8, align 4, !tbaa !13
  %conv9 = zext i16 %21 to i32
  %cmp10 = icmp slt i32 %19, %conv9
  br i1 %cmp10, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond7
  store i32 8, i32* %cleanup.dest.slot, align 4
  %22 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  br label %for.end

for.body13:                                       ; preds = %for.cond7
  %23 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %24 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %23, i32 %24
  %25 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv14 = sext i16 %25 to i32
  %26 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %28 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub15 = sub nsw i32 %27, %28
  %29 = load i32, i32* %k, align 4, !tbaa !6
  %add = add nsw i32 %sub15, %29
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %30
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %add16 = add nsw i32 %mul, %31
  %arrayidx17 = getelementptr inbounds i8, i8* %26, i32 %add16
  %32 = load i8, i8* %arrayidx17, align 1, !tbaa !8
  %conv18 = zext i8 %32 to i32
  %mul19 = mul nsw i32 %conv14, %conv18
  %33 = load i32, i32* %res, align 4, !tbaa !6
  %add20 = add nsw i32 %33, %mul19
  store i32 %add20, i32* %res, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %34 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond.cleanup12
  %35 = load i32, i32* %res, align 4, !tbaa !6
  %add21 = add nsw i32 %35, 64
  %shr = ashr i32 %add21, 7
  %call22 = call zeroext i8 @clip_pixel(i32 %shr)
  %36 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %37 = load i32, i32* %y, align 4, !tbaa !6
  %38 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 %37, %38
  %39 = load i32, i32* %x, align 4, !tbaa !6
  %add24 = add nsw i32 %mul23, %39
  %arrayidx25 = getelementptr inbounds i8, i8* %36, i32 %add24
  store i8 %call22, i8* %arrayidx25, align 1, !tbaa !8
  %40 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.inc26

for.inc26:                                        ; preds = %for.end
  %41 = load i32, i32* %x, align 4, !tbaa !6
  %inc27 = add nsw i32 %41, 1
  store i32 %inc27, i32* %x, align 4, !tbaa !6
  br label %for.cond2

for.end28:                                        ; preds = %for.cond.cleanup5
  br label %for.inc29

for.inc29:                                        ; preds = %for.end28
  %42 = load i32, i32* %y, align 4, !tbaa !6
  %inc30 = add nsw i32 %42, 1
  store i32 %inc30, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end31:                                        ; preds = %for.cond.cleanup
  %43 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_convolve_x_sr_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %fo_horiz = alloca i32, align 4
  %bits = alloca i32, align 4
  %x_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i32, align 4
  %k = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %1, i32 0, i32 1
  %2 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %2 to i32
  %div = sdiv i32 %conv, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %fo_horiz, align 4, !tbaa !6
  %3 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 3
  %5 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub1 = sub nsw i32 7, %5
  store i32 %sub1, i32* %bits, align 4, !tbaa !6
  %6 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %7 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %8 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %9 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %11 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %and = and i32 %11, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %10, i32 %and)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %12 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc37, %entry
  %13 = load i32, i32* %y, align 4, !tbaa !6
  %14 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %13, %14
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %15 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  br label %for.end39

for.body:                                         ; preds = %for.cond
  %16 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc34, %for.body
  %17 = load i32, i32* %x, align 4, !tbaa !6
  %18 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %17, %18
  br i1 %cmp4, label %for.body7, label %for.cond.cleanup6

for.cond.cleanup6:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  br label %for.end36

for.body7:                                        ; preds = %for.cond3
  %20 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  store i32 0, i32* %res, align 4, !tbaa !6
  %21 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body7
  %22 = load i32, i32* %k, align 4, !tbaa !6
  %23 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps9 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %23, i32 0, i32 1
  %24 = load i16, i16* %taps9, align 4, !tbaa !13
  %conv10 = zext i16 %24 to i32
  %cmp11 = icmp slt i32 %22, %conv10
  br i1 %cmp11, label %for.body14, label %for.cond.cleanup13

for.cond.cleanup13:                               ; preds = %for.cond8
  store i32 8, i32* %cleanup.dest.slot, align 4
  %25 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  br label %for.end

for.body14:                                       ; preds = %for.cond8
  %26 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %27 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %26, i32 %27
  %28 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv15 = sext i16 %28 to i32
  %29 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %30 = load i32, i32* %y, align 4, !tbaa !6
  %31 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %30, %31
  %32 = load i32, i32* %x, align 4, !tbaa !6
  %add = add nsw i32 %mul, %32
  %33 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub16 = sub nsw i32 %add, %33
  %34 = load i32, i32* %k, align 4, !tbaa !6
  %add17 = add nsw i32 %sub16, %34
  %arrayidx18 = getelementptr inbounds i8, i8* %29, i32 %add17
  %35 = load i8, i8* %arrayidx18, align 1, !tbaa !8
  %conv19 = zext i8 %35 to i32
  %mul20 = mul nsw i32 %conv15, %conv19
  %36 = load i32, i32* %res, align 4, !tbaa !6
  %add21 = add nsw i32 %36, %mul20
  store i32 %add21, i32* %res, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %37 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond8

for.end:                                          ; preds = %for.cond.cleanup13
  %38 = load i32, i32* %res, align 4, !tbaa !6
  %39 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_022 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %39, i32 0, i32 3
  %40 = load i32, i32* %round_022, align 4, !tbaa !15
  %shl = shl i32 1, %40
  %shr = ashr i32 %shl, 1
  %add23 = add nsw i32 %38, %shr
  %41 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_024 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %41, i32 0, i32 3
  %42 = load i32, i32* %round_024, align 4, !tbaa !15
  %shr25 = ashr i32 %add23, %42
  store i32 %shr25, i32* %res, align 4, !tbaa !6
  %43 = load i32, i32* %res, align 4, !tbaa !6
  %44 = load i32, i32* %bits, align 4, !tbaa !6
  %shl26 = shl i32 1, %44
  %shr27 = ashr i32 %shl26, 1
  %add28 = add nsw i32 %43, %shr27
  %45 = load i32, i32* %bits, align 4, !tbaa !6
  %shr29 = ashr i32 %add28, %45
  %call30 = call zeroext i8 @clip_pixel(i32 %shr29)
  %46 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %47 = load i32, i32* %y, align 4, !tbaa !6
  %48 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul31 = mul nsw i32 %47, %48
  %49 = load i32, i32* %x, align 4, !tbaa !6
  %add32 = add nsw i32 %mul31, %49
  %arrayidx33 = getelementptr inbounds i8, i8* %46, i32 %add32
  store i8 %call30, i8* %arrayidx33, align 1, !tbaa !8
  %50 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  br label %for.inc34

for.inc34:                                        ; preds = %for.end
  %51 = load i32, i32* %x, align 4, !tbaa !6
  %inc35 = add nsw i32 %51, 1
  store i32 %inc35, i32* %x, align 4, !tbaa !6
  br label %for.cond3

for.end36:                                        ; preds = %for.cond.cleanup6
  br label %for.inc37

for.inc37:                                        ; preds = %for.end36
  %52 = load i32, i32* %y, align 4, !tbaa !6
  %inc38 = add nsw i32 %52, 1
  store i32 %inc38, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end39:                                        ; preds = %for.cond.cleanup
  %53 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_convolve_2d_copy_sr_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %y = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %1 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %2 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %3 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %5 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %y, align 4, !tbaa !6
  %7 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %10 = load i32, i32* %y, align 4, !tbaa !6
  %11 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %10, %11
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %mul
  %12 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %13 = load i32, i32* %y, align 4, !tbaa !6
  %14 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 %13, %14
  %add.ptr2 = getelementptr inbounds i8, i8* %12, i32 %mul1
  %15 = load i32, i32* %w.addr, align 4, !tbaa !6
  %mul3 = mul i32 %15, 1
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %add.ptr2, i32 %mul3, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %y, align 4, !tbaa !6
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @av1_dist_wtd_convolve_2d_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %im_block = alloca [17280 x i16], align 16
  %im_h = alloca i32, align 4
  %im_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %bd = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %src_horiz = alloca i8*, align 4
  %x_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  %src_vert = alloca i16*, align 4
  %y_filter = alloca i16*, align 4
  %offset_bits = alloca i32, align 4
  %y54 = alloca i32, align 4
  %x60 = alloca i32, align 4
  %sum66 = alloca i32, align 4
  %k68 = alloca i32, align 4
  %res = alloca i16, align 2
  %tmp = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %1, i32 0, i32 1
  %2 = load i16*, i16** %dst1, align 4, !tbaa !19
  store i16* %2, i16** %dst16, align 4, !tbaa !2
  %3 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 2
  %5 = load i32, i32* %dst_stride2, align 4, !tbaa !20
  store i32 %5, i32* %dst16_stride, align 4, !tbaa !6
  %6 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 34560, i8* %6) #4
  %7 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %h.addr, align 4, !tbaa !6
  %9 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %9, i32 0, i32 1
  %10 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %10 to i32
  %add = add nsw i32 %8, %conv
  %sub = sub nsw i32 %add, 1
  store i32 %sub, i32* %im_h, align 4, !tbaa !6
  %11 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i32, i32* %w.addr, align 4, !tbaa !6
  store i32 %12, i32* %im_stride, align 4, !tbaa !6
  %13 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps3 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %14, i32 0, i32 1
  %15 = load i16, i16* %taps3, align 4, !tbaa !13
  %conv4 = zext i16 %15 to i32
  %div = sdiv i32 %conv4, 2
  %sub5 = sub nsw i32 %div, 1
  store i32 %sub5, i32* %fo_vert, align 4, !tbaa !6
  %16 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps6 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %17, i32 0, i32 1
  %18 = load i16, i16* %taps6, align 4, !tbaa !13
  %conv7 = zext i16 %18 to i32
  %div8 = sdiv i32 %conv7, 2
  %sub9 = sub nsw i32 %div8, 1
  store i32 %sub9, i32* %fo_horiz, align 4, !tbaa !6
  %19 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  store i32 8, i32* %bd, align 4, !tbaa !6
  %20 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %21, i32 0, i32 3
  %22 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub10 = sub nsw i32 14, %22
  %23 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %23, i32 0, i32 4
  %24 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub11 = sub nsw i32 %sub10, %24
  store i32 %sub11, i32* %round_bits, align 4, !tbaa !6
  %25 = bitcast i8** %src_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  %26 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %27 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %28 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %27, %28
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i8, i8* %26, i32 %idx.neg
  store i8* %add.ptr, i8** %src_horiz, align 4, !tbaa !2
  %29 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %31 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %and = and i32 %31, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %30, i32 %and)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %32 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc45, %entry
  %33 = load i32, i32* %y, align 4, !tbaa !6
  %34 = load i32, i32* %im_h, align 4, !tbaa !6
  %cmp = icmp slt i32 %33, %34
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %35 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  br label %for.end47

for.body:                                         ; preds = %for.cond
  %36 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc42, %for.body
  %37 = load i32, i32* %x, align 4, !tbaa !6
  %38 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %37, %38
  br i1 %cmp14, label %for.body17, label %for.cond.cleanup16

for.cond.cleanup16:                               ; preds = %for.cond13
  store i32 5, i32* %cleanup.dest.slot, align 4
  %39 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  br label %for.end44

for.body17:                                       ; preds = %for.cond13
  %40 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #4
  store i32 16384, i32* %sum, align 4, !tbaa !6
  %41 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %for.body17
  %42 = load i32, i32* %k, align 4, !tbaa !6
  %43 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps19 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %43, i32 0, i32 1
  %44 = load i16, i16* %taps19, align 4, !tbaa !13
  %conv20 = zext i16 %44 to i32
  %cmp21 = icmp slt i32 %42, %conv20
  br i1 %cmp21, label %for.body24, label %for.cond.cleanup23

for.cond.cleanup23:                               ; preds = %for.cond18
  store i32 8, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  br label %for.end

for.body24:                                       ; preds = %for.cond18
  %46 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %47 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %46, i32 %47
  %48 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv25 = sext i16 %48 to i32
  %49 = load i8*, i8** %src_horiz, align 4, !tbaa !2
  %50 = load i32, i32* %y, align 4, !tbaa !6
  %51 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %50, %51
  %52 = load i32, i32* %x, align 4, !tbaa !6
  %add27 = add nsw i32 %mul26, %52
  %53 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub28 = sub nsw i32 %add27, %53
  %54 = load i32, i32* %k, align 4, !tbaa !6
  %add29 = add nsw i32 %sub28, %54
  %arrayidx30 = getelementptr inbounds i8, i8* %49, i32 %add29
  %55 = load i8, i8* %arrayidx30, align 1, !tbaa !8
  %conv31 = zext i8 %55 to i32
  %mul32 = mul nsw i32 %conv25, %conv31
  %56 = load i32, i32* %sum, align 4, !tbaa !6
  %add33 = add nsw i32 %56, %mul32
  store i32 %add33, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body24
  %57 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %57, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond18

for.end:                                          ; preds = %for.cond.cleanup23
  %58 = load i32, i32* %sum, align 4, !tbaa !6
  %59 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_034 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %59, i32 0, i32 3
  %60 = load i32, i32* %round_034, align 4, !tbaa !15
  %shl = shl i32 1, %60
  %shr = ashr i32 %shl, 1
  %add35 = add nsw i32 %58, %shr
  %61 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_036 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %61, i32 0, i32 3
  %62 = load i32, i32* %round_036, align 4, !tbaa !15
  %shr37 = ashr i32 %add35, %62
  %conv38 = trunc i32 %shr37 to i16
  %63 = load i32, i32* %y, align 4, !tbaa !6
  %64 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul39 = mul nsw i32 %63, %64
  %65 = load i32, i32* %x, align 4, !tbaa !6
  %add40 = add nsw i32 %mul39, %65
  %arrayidx41 = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 %add40
  store i16 %conv38, i16* %arrayidx41, align 2, !tbaa !9
  %66 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #4
  br label %for.inc42

for.inc42:                                        ; preds = %for.end
  %67 = load i32, i32* %x, align 4, !tbaa !6
  %inc43 = add nsw i32 %67, 1
  store i32 %inc43, i32* %x, align 4, !tbaa !6
  br label %for.cond13

for.end44:                                        ; preds = %for.cond.cleanup16
  br label %for.inc45

for.inc45:                                        ; preds = %for.end44
  %68 = load i32, i32* %y, align 4, !tbaa !6
  %inc46 = add nsw i32 %68, 1
  store i32 %inc46, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end47:                                        ; preds = %for.cond.cleanup
  %69 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %arraydecay = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 0
  %70 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %71 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul48 = mul nsw i32 %70, %71
  %add.ptr49 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul48
  store i16* %add.ptr49, i16** %src_vert, align 4, !tbaa !2
  %72 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #4
  %73 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %74 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %and50 = and i32 %74, 15
  %call51 = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %73, i32 %and50)
  store i16* %call51, i16** %y_filter, align 4, !tbaa !2
  %75 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #4
  %76 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_052 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %76, i32 0, i32 3
  %77 = load i32, i32* %round_052, align 4, !tbaa !15
  %sub53 = sub nsw i32 22, %77
  store i32 %sub53, i32* %offset_bits, align 4, !tbaa !6
  %78 = bitcast i32* %y54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #4
  store i32 0, i32* %y54, align 4, !tbaa !6
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc135, %for.end47
  %79 = load i32, i32* %y54, align 4, !tbaa !6
  %80 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp56 = icmp slt i32 %79, %80
  br i1 %cmp56, label %for.body59, label %for.cond.cleanup58

for.cond.cleanup58:                               ; preds = %for.cond55
  store i32 11, i32* %cleanup.dest.slot, align 4
  %81 = bitcast i32* %y54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  br label %for.end137

for.body59:                                       ; preds = %for.cond55
  %82 = bitcast i32* %x60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #4
  store i32 0, i32* %x60, align 4, !tbaa !6
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc132, %for.body59
  %83 = load i32, i32* %x60, align 4, !tbaa !6
  %84 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp62 = icmp slt i32 %83, %84
  br i1 %cmp62, label %for.body65, label %for.cond.cleanup64

for.cond.cleanup64:                               ; preds = %for.cond61
  store i32 14, i32* %cleanup.dest.slot, align 4
  %85 = bitcast i32* %x60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  br label %for.end134

for.body65:                                       ; preds = %for.cond61
  %86 = bitcast i32* %sum66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #4
  %87 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %shl67 = shl i32 1, %87
  store i32 %shl67, i32* %sum66, align 4, !tbaa !6
  %88 = bitcast i32* %k68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #4
  store i32 0, i32* %k68, align 4, !tbaa !6
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc86, %for.body65
  %89 = load i32, i32* %k68, align 4, !tbaa !6
  %90 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps70 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %90, i32 0, i32 1
  %91 = load i16, i16* %taps70, align 4, !tbaa !13
  %conv71 = zext i16 %91 to i32
  %cmp72 = icmp slt i32 %89, %conv71
  br i1 %cmp72, label %for.body75, label %for.cond.cleanup74

for.cond.cleanup74:                               ; preds = %for.cond69
  store i32 17, i32* %cleanup.dest.slot, align 4
  %92 = bitcast i32* %k68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  br label %for.end88

for.body75:                                       ; preds = %for.cond69
  %93 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %94 = load i32, i32* %k68, align 4, !tbaa !6
  %arrayidx76 = getelementptr inbounds i16, i16* %93, i32 %94
  %95 = load i16, i16* %arrayidx76, align 2, !tbaa !9
  %conv77 = sext i16 %95 to i32
  %96 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %97 = load i32, i32* %y54, align 4, !tbaa !6
  %98 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub78 = sub nsw i32 %97, %98
  %99 = load i32, i32* %k68, align 4, !tbaa !6
  %add79 = add nsw i32 %sub78, %99
  %100 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul80 = mul nsw i32 %add79, %100
  %101 = load i32, i32* %x60, align 4, !tbaa !6
  %add81 = add nsw i32 %mul80, %101
  %arrayidx82 = getelementptr inbounds i16, i16* %96, i32 %add81
  %102 = load i16, i16* %arrayidx82, align 2, !tbaa !9
  %conv83 = sext i16 %102 to i32
  %mul84 = mul nsw i32 %conv77, %conv83
  %103 = load i32, i32* %sum66, align 4, !tbaa !6
  %add85 = add nsw i32 %103, %mul84
  store i32 %add85, i32* %sum66, align 4, !tbaa !6
  br label %for.inc86

for.inc86:                                        ; preds = %for.body75
  %104 = load i32, i32* %k68, align 4, !tbaa !6
  %inc87 = add nsw i32 %104, 1
  store i32 %inc87, i32* %k68, align 4, !tbaa !6
  br label %for.cond69

for.end88:                                        ; preds = %for.cond.cleanup74
  %105 = bitcast i16* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %105) #4
  %106 = load i32, i32* %sum66, align 4, !tbaa !6
  %107 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_189 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %107, i32 0, i32 4
  %108 = load i32, i32* %round_189, align 4, !tbaa !17
  %shl90 = shl i32 1, %108
  %shr91 = ashr i32 %shl90, 1
  %add92 = add nsw i32 %106, %shr91
  %109 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_193 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %109, i32 0, i32 4
  %110 = load i32, i32* %round_193, align 4, !tbaa !17
  %shr94 = ashr i32 %add92, %110
  %conv95 = trunc i32 %shr94 to i16
  store i16 %conv95, i16* %res, align 2, !tbaa !9
  %111 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %111, i32 0, i32 0
  %112 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool = icmp ne i32 %112, 0
  br i1 %tobool, label %if.then, label %if.else127

if.then:                                          ; preds = %for.end88
  %113 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #4
  %114 = load i16*, i16** %dst16, align 4, !tbaa !2
  %115 = load i32, i32* %y54, align 4, !tbaa !6
  %116 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul96 = mul nsw i32 %115, %116
  %117 = load i32, i32* %x60, align 4, !tbaa !6
  %add97 = add nsw i32 %mul96, %117
  %arrayidx98 = getelementptr inbounds i16, i16* %114, i32 %add97
  %118 = load i16, i16* %arrayidx98, align 2, !tbaa !9
  %conv99 = zext i16 %118 to i32
  store i32 %conv99, i32* %tmp, align 4, !tbaa !6
  %119 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %119, i32 0, i32 8
  %120 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool100 = icmp ne i32 %120, 0
  br i1 %tobool100, label %if.then101, label %if.else

if.then101:                                       ; preds = %if.then
  %121 = load i32, i32* %tmp, align 4, !tbaa !6
  %122 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %122, i32 0, i32 9
  %123 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul102 = mul nsw i32 %121, %123
  %124 = load i16, i16* %res, align 2, !tbaa !9
  %conv103 = zext i16 %124 to i32
  %125 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %125, i32 0, i32 10
  %126 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul104 = mul nsw i32 %conv103, %126
  %add105 = add nsw i32 %mul102, %mul104
  store i32 %add105, i32* %tmp, align 4, !tbaa !6
  %127 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr106 = ashr i32 %127, 4
  store i32 %shr106, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  %128 = load i16, i16* %res, align 2, !tbaa !9
  %conv107 = zext i16 %128 to i32
  %129 = load i32, i32* %tmp, align 4, !tbaa !6
  %add108 = add nsw i32 %129, %conv107
  store i32 %add108, i32* %tmp, align 4, !tbaa !6
  %130 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr109 = ashr i32 %130, 1
  store i32 %shr109, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then101
  %131 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %132 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1110 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %132, i32 0, i32 4
  %133 = load i32, i32* %round_1110, align 4, !tbaa !17
  %sub111 = sub nsw i32 %131, %133
  %shl112 = shl i32 1, %sub111
  %134 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %135 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1113 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %135, i32 0, i32 4
  %136 = load i32, i32* %round_1113, align 4, !tbaa !17
  %sub114 = sub nsw i32 %134, %136
  %sub115 = sub nsw i32 %sub114, 1
  %shl116 = shl i32 1, %sub115
  %add117 = add nsw i32 %shl112, %shl116
  %137 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub118 = sub nsw i32 %137, %add117
  store i32 %sub118, i32* %tmp, align 4, !tbaa !6
  %138 = load i32, i32* %tmp, align 4, !tbaa !6
  %139 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl119 = shl i32 1, %139
  %shr120 = ashr i32 %shl119, 1
  %add121 = add nsw i32 %138, %shr120
  %140 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr122 = ashr i32 %add121, %140
  %call123 = call zeroext i8 @clip_pixel(i32 %shr122)
  %141 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %142 = load i32, i32* %y54, align 4, !tbaa !6
  %143 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul124 = mul nsw i32 %142, %143
  %144 = load i32, i32* %x60, align 4, !tbaa !6
  %add125 = add nsw i32 %mul124, %144
  %arrayidx126 = getelementptr inbounds i8, i8* %141, i32 %add125
  store i8 %call123, i8* %arrayidx126, align 1, !tbaa !8
  %145 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  br label %if.end131

if.else127:                                       ; preds = %for.end88
  %146 = load i16, i16* %res, align 2, !tbaa !9
  %147 = load i16*, i16** %dst16, align 4, !tbaa !2
  %148 = load i32, i32* %y54, align 4, !tbaa !6
  %149 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul128 = mul nsw i32 %148, %149
  %150 = load i32, i32* %x60, align 4, !tbaa !6
  %add129 = add nsw i32 %mul128, %150
  %arrayidx130 = getelementptr inbounds i16, i16* %147, i32 %add129
  store i16 %146, i16* %arrayidx130, align 2, !tbaa !9
  br label %if.end131

if.end131:                                        ; preds = %if.else127, %if.end
  %151 = bitcast i16* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %151) #4
  %152 = bitcast i32* %sum66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  br label %for.inc132

for.inc132:                                       ; preds = %if.end131
  %153 = load i32, i32* %x60, align 4, !tbaa !6
  %inc133 = add nsw i32 %153, 1
  store i32 %inc133, i32* %x60, align 4, !tbaa !6
  br label %for.cond61

for.end134:                                       ; preds = %for.cond.cleanup64
  br label %for.inc135

for.inc135:                                       ; preds = %for.end134
  %154 = load i32, i32* %y54, align 4, !tbaa !6
  %inc136 = add nsw i32 %154, 1
  store i32 %inc136, i32* %y54, align 4, !tbaa !6
  br label %for.cond55

for.end137:                                       ; preds = %for.cond.cleanup58
  %155 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #4
  %156 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #4
  %157 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %158 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  %159 = bitcast i8** %src_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #4
  %160 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #4
  %161 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #4
  %162 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #4
  %163 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #4
  %164 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #4
  %165 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #4
  %166 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 34560, i8* %166) #4
  %167 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  %168 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_dist_wtd_convolve_y_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %bits = alloca i32, align 4
  %bd = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %round_offset = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %y_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i32, align 4
  %k = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %1, i32 0, i32 1
  %2 = load i16*, i16** %dst1, align 4, !tbaa !19
  store i16* %2, i16** %dst16, align 4, !tbaa !2
  %3 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 2
  %5 = load i32, i32* %dst_stride2, align 4, !tbaa !20
  store i32 %5, i32* %dst16_stride, align 4, !tbaa !6
  %6 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %7, i32 0, i32 1
  %8 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %8 to i32
  %div = sdiv i32 %conv, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %fo_vert, align 4, !tbaa !6
  %9 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %10, i32 0, i32 3
  %11 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub3 = sub nsw i32 7, %11
  store i32 %sub3, i32* %bits, align 4, !tbaa !6
  %12 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  store i32 8, i32* %bd, align 4, !tbaa !6
  %13 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_04 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %14, i32 0, i32 3
  %15 = load i32, i32* %round_04, align 4, !tbaa !15
  %sub5 = sub nsw i32 22, %15
  store i32 %sub5, i32* %offset_bits, align 4, !tbaa !6
  %16 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %18 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %18, i32 0, i32 4
  %19 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub6 = sub nsw i32 %17, %19
  %shl = shl i32 1, %sub6
  %20 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %21 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_17 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %21, i32 0, i32 4
  %22 = load i32, i32* %round_17, align 4, !tbaa !17
  %sub8 = sub nsw i32 %20, %22
  %sub9 = sub nsw i32 %sub8, 1
  %shl10 = shl i32 1, %sub9
  %add = add nsw i32 %shl, %shl10
  store i32 %add, i32* %round_offset, align 4, !tbaa !6
  %23 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_011 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %24, i32 0, i32 3
  %25 = load i32, i32* %round_011, align 4, !tbaa !15
  %sub12 = sub nsw i32 14, %25
  %26 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_113 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %26, i32 0, i32 4
  %27 = load i32, i32* %round_113, align 4, !tbaa !17
  %sub14 = sub nsw i32 %sub12, %27
  store i32 %sub14, i32* %round_bits, align 4, !tbaa !6
  %28 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %29 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %30 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %32 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %and = and i32 %32, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %31, i32 %and)
  store i16* %call, i16** %y_filter, align 4, !tbaa !2
  %33 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc74, %entry
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %35 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  br label %for.end76

for.body:                                         ; preds = %for.cond
  %37 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc71, %for.body
  %38 = load i32, i32* %x, align 4, !tbaa !6
  %39 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %38, %39
  br i1 %cmp17, label %for.body20, label %for.cond.cleanup19

for.cond.cleanup19:                               ; preds = %for.cond16
  store i32 5, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.end73

for.body20:                                       ; preds = %for.cond16
  %41 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  store i32 0, i32* %res, align 4, !tbaa !6
  %42 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %for.body20
  %43 = load i32, i32* %k, align 4, !tbaa !6
  %44 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps22 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %44, i32 0, i32 1
  %45 = load i16, i16* %taps22, align 4, !tbaa !13
  %conv23 = zext i16 %45 to i32
  %cmp24 = icmp slt i32 %43, %conv23
  br i1 %cmp24, label %for.body27, label %for.cond.cleanup26

for.cond.cleanup26:                               ; preds = %for.cond21
  store i32 8, i32* %cleanup.dest.slot, align 4
  %46 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %for.end

for.body27:                                       ; preds = %for.cond21
  %47 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %47, i32 %48
  %49 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv28 = sext i16 %49 to i32
  %50 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %51 = load i32, i32* %y, align 4, !tbaa !6
  %52 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub29 = sub nsw i32 %51, %52
  %53 = load i32, i32* %k, align 4, !tbaa !6
  %add30 = add nsw i32 %sub29, %53
  %54 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add30, %54
  %55 = load i32, i32* %x, align 4, !tbaa !6
  %add31 = add nsw i32 %mul, %55
  %arrayidx32 = getelementptr inbounds i8, i8* %50, i32 %add31
  %56 = load i8, i8* %arrayidx32, align 1, !tbaa !8
  %conv33 = zext i8 %56 to i32
  %mul34 = mul nsw i32 %conv28, %conv33
  %57 = load i32, i32* %res, align 4, !tbaa !6
  %add35 = add nsw i32 %57, %mul34
  store i32 %add35, i32* %res, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body27
  %58 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond21

for.end:                                          ; preds = %for.cond.cleanup26
  %59 = load i32, i32* %bits, align 4, !tbaa !6
  %shl36 = shl i32 1, %59
  %60 = load i32, i32* %res, align 4, !tbaa !6
  %mul37 = mul nsw i32 %60, %shl36
  store i32 %mul37, i32* %res, align 4, !tbaa !6
  %61 = load i32, i32* %res, align 4, !tbaa !6
  %62 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_138 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %62, i32 0, i32 4
  %63 = load i32, i32* %round_138, align 4, !tbaa !17
  %shl39 = shl i32 1, %63
  %shr = ashr i32 %shl39, 1
  %add40 = add nsw i32 %61, %shr
  %64 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_141 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %64, i32 0, i32 4
  %65 = load i32, i32* %round_141, align 4, !tbaa !17
  %shr42 = ashr i32 %add40, %65
  %66 = load i32, i32* %round_offset, align 4, !tbaa !6
  %add43 = add nsw i32 %shr42, %66
  store i32 %add43, i32* %res, align 4, !tbaa !6
  %67 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %67, i32 0, i32 0
  %68 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool = icmp ne i32 %68, 0
  br i1 %tobool, label %if.then, label %if.else65

if.then:                                          ; preds = %for.end
  %69 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %70 = load i16*, i16** %dst16, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !6
  %72 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul44 = mul nsw i32 %71, %72
  %73 = load i32, i32* %x, align 4, !tbaa !6
  %add45 = add nsw i32 %mul44, %73
  %arrayidx46 = getelementptr inbounds i16, i16* %70, i32 %add45
  %74 = load i16, i16* %arrayidx46, align 2, !tbaa !9
  %conv47 = zext i16 %74 to i32
  store i32 %conv47, i32* %tmp, align 4, !tbaa !6
  %75 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %75, i32 0, i32 8
  %76 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool48 = icmp ne i32 %76, 0
  br i1 %tobool48, label %if.then49, label %if.else

if.then49:                                        ; preds = %if.then
  %77 = load i32, i32* %tmp, align 4, !tbaa !6
  %78 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %78, i32 0, i32 9
  %79 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul50 = mul nsw i32 %77, %79
  %80 = load i32, i32* %res, align 4, !tbaa !6
  %81 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %81, i32 0, i32 10
  %82 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul51 = mul nsw i32 %80, %82
  %add52 = add nsw i32 %mul50, %mul51
  store i32 %add52, i32* %tmp, align 4, !tbaa !6
  %83 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr53 = ashr i32 %83, 4
  store i32 %shr53, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  %84 = load i32, i32* %res, align 4, !tbaa !6
  %85 = load i32, i32* %tmp, align 4, !tbaa !6
  %add54 = add nsw i32 %85, %84
  store i32 %add54, i32* %tmp, align 4, !tbaa !6
  %86 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr55 = ashr i32 %86, 1
  store i32 %shr55, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then49
  %87 = load i32, i32* %round_offset, align 4, !tbaa !6
  %88 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub56 = sub nsw i32 %88, %87
  store i32 %sub56, i32* %tmp, align 4, !tbaa !6
  %89 = load i32, i32* %tmp, align 4, !tbaa !6
  %90 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl57 = shl i32 1, %90
  %shr58 = ashr i32 %shl57, 1
  %add59 = add nsw i32 %89, %shr58
  %91 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr60 = ashr i32 %add59, %91
  %call61 = call zeroext i8 @clip_pixel(i32 %shr60)
  %92 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %93 = load i32, i32* %y, align 4, !tbaa !6
  %94 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul62 = mul nsw i32 %93, %94
  %95 = load i32, i32* %x, align 4, !tbaa !6
  %add63 = add nsw i32 %mul62, %95
  %arrayidx64 = getelementptr inbounds i8, i8* %92, i32 %add63
  store i8 %call61, i8* %arrayidx64, align 1, !tbaa !8
  %96 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  br label %if.end70

if.else65:                                        ; preds = %for.end
  %97 = load i32, i32* %res, align 4, !tbaa !6
  %conv66 = trunc i32 %97 to i16
  %98 = load i16*, i16** %dst16, align 4, !tbaa !2
  %99 = load i32, i32* %y, align 4, !tbaa !6
  %100 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul67 = mul nsw i32 %99, %100
  %101 = load i32, i32* %x, align 4, !tbaa !6
  %add68 = add nsw i32 %mul67, %101
  %arrayidx69 = getelementptr inbounds i16, i16* %98, i32 %add68
  store i16 %conv66, i16* %arrayidx69, align 2, !tbaa !9
  br label %if.end70

if.end70:                                         ; preds = %if.else65, %if.end
  %102 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #4
  br label %for.inc71

for.inc71:                                        ; preds = %if.end70
  %103 = load i32, i32* %x, align 4, !tbaa !6
  %inc72 = add nsw i32 %103, 1
  store i32 %inc72, i32* %x, align 4, !tbaa !6
  br label %for.cond16

for.end73:                                        ; preds = %for.cond.cleanup19
  br label %for.inc74

for.inc74:                                        ; preds = %for.end73
  %104 = load i32, i32* %y, align 4, !tbaa !6
  %inc75 = add nsw i32 %104, 1
  store i32 %inc75, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end76:                                        ; preds = %for.cond.cleanup
  %105 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #4
  %106 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  %107 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  %108 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #4
  %109 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_dist_wtd_convolve_x_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %bits = alloca i32, align 4
  %bd = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %round_offset = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %x_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i32, align 4
  %k = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %1, i32 0, i32 1
  %2 = load i16*, i16** %dst1, align 4, !tbaa !19
  store i16* %2, i16** %dst16, align 4, !tbaa !2
  %3 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 2
  %5 = load i32, i32* %dst_stride2, align 4, !tbaa !20
  store i32 %5, i32* %dst16_stride, align 4, !tbaa !6
  %6 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %7, i32 0, i32 1
  %8 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %8 to i32
  %div = sdiv i32 %conv, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %fo_horiz, align 4, !tbaa !6
  %9 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %10, i32 0, i32 4
  %11 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub3 = sub nsw i32 7, %11
  store i32 %sub3, i32* %bits, align 4, !tbaa !6
  %12 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  store i32 8, i32* %bd, align 4, !tbaa !6
  %13 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %14, i32 0, i32 3
  %15 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub4 = sub nsw i32 22, %15
  store i32 %sub4, i32* %offset_bits, align 4, !tbaa !6
  %16 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %18 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_15 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %18, i32 0, i32 4
  %19 = load i32, i32* %round_15, align 4, !tbaa !17
  %sub6 = sub nsw i32 %17, %19
  %shl = shl i32 1, %sub6
  %20 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %21 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_17 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %21, i32 0, i32 4
  %22 = load i32, i32* %round_17, align 4, !tbaa !17
  %sub8 = sub nsw i32 %20, %22
  %sub9 = sub nsw i32 %sub8, 1
  %shl10 = shl i32 1, %sub9
  %add = add nsw i32 %shl, %shl10
  store i32 %add, i32* %round_offset, align 4, !tbaa !6
  %23 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_011 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %24, i32 0, i32 3
  %25 = load i32, i32* %round_011, align 4, !tbaa !15
  %sub12 = sub nsw i32 14, %25
  %26 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_113 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %26, i32 0, i32 4
  %27 = load i32, i32* %round_113, align 4, !tbaa !17
  %sub14 = sub nsw i32 %sub12, %27
  store i32 %sub14, i32* %round_bits, align 4, !tbaa !6
  %28 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %29 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %30 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %32 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %and = and i32 %32, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %31, i32 %and)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %33 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc74, %entry
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %35 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  br label %for.end76

for.body:                                         ; preds = %for.cond
  %37 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc71, %for.body
  %38 = load i32, i32* %x, align 4, !tbaa !6
  %39 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %38, %39
  br i1 %cmp17, label %for.body20, label %for.cond.cleanup19

for.cond.cleanup19:                               ; preds = %for.cond16
  store i32 5, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.end73

for.body20:                                       ; preds = %for.cond16
  %41 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  store i32 0, i32* %res, align 4, !tbaa !6
  %42 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %for.body20
  %43 = load i32, i32* %k, align 4, !tbaa !6
  %44 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps22 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %44, i32 0, i32 1
  %45 = load i16, i16* %taps22, align 4, !tbaa !13
  %conv23 = zext i16 %45 to i32
  %cmp24 = icmp slt i32 %43, %conv23
  br i1 %cmp24, label %for.body27, label %for.cond.cleanup26

for.cond.cleanup26:                               ; preds = %for.cond21
  store i32 8, i32* %cleanup.dest.slot, align 4
  %46 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %for.end

for.body27:                                       ; preds = %for.cond21
  %47 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %47, i32 %48
  %49 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv28 = sext i16 %49 to i32
  %50 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %51 = load i32, i32* %y, align 4, !tbaa !6
  %52 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %51, %52
  %53 = load i32, i32* %x, align 4, !tbaa !6
  %add29 = add nsw i32 %mul, %53
  %54 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub30 = sub nsw i32 %add29, %54
  %55 = load i32, i32* %k, align 4, !tbaa !6
  %add31 = add nsw i32 %sub30, %55
  %arrayidx32 = getelementptr inbounds i8, i8* %50, i32 %add31
  %56 = load i8, i8* %arrayidx32, align 1, !tbaa !8
  %conv33 = zext i8 %56 to i32
  %mul34 = mul nsw i32 %conv28, %conv33
  %57 = load i32, i32* %res, align 4, !tbaa !6
  %add35 = add nsw i32 %57, %mul34
  store i32 %add35, i32* %res, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body27
  %58 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond21

for.end:                                          ; preds = %for.cond.cleanup26
  %59 = load i32, i32* %bits, align 4, !tbaa !6
  %shl36 = shl i32 1, %59
  %60 = load i32, i32* %res, align 4, !tbaa !6
  %61 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_037 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %61, i32 0, i32 3
  %62 = load i32, i32* %round_037, align 4, !tbaa !15
  %shl38 = shl i32 1, %62
  %shr = ashr i32 %shl38, 1
  %add39 = add nsw i32 %60, %shr
  %63 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_040 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %63, i32 0, i32 3
  %64 = load i32, i32* %round_040, align 4, !tbaa !15
  %shr41 = ashr i32 %add39, %64
  %mul42 = mul nsw i32 %shl36, %shr41
  store i32 %mul42, i32* %res, align 4, !tbaa !6
  %65 = load i32, i32* %round_offset, align 4, !tbaa !6
  %66 = load i32, i32* %res, align 4, !tbaa !6
  %add43 = add nsw i32 %66, %65
  store i32 %add43, i32* %res, align 4, !tbaa !6
  %67 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %67, i32 0, i32 0
  %68 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool = icmp ne i32 %68, 0
  br i1 %tobool, label %if.then, label %if.else65

if.then:                                          ; preds = %for.end
  %69 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %70 = load i16*, i16** %dst16, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !6
  %72 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul44 = mul nsw i32 %71, %72
  %73 = load i32, i32* %x, align 4, !tbaa !6
  %add45 = add nsw i32 %mul44, %73
  %arrayidx46 = getelementptr inbounds i16, i16* %70, i32 %add45
  %74 = load i16, i16* %arrayidx46, align 2, !tbaa !9
  %conv47 = zext i16 %74 to i32
  store i32 %conv47, i32* %tmp, align 4, !tbaa !6
  %75 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %75, i32 0, i32 8
  %76 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool48 = icmp ne i32 %76, 0
  br i1 %tobool48, label %if.then49, label %if.else

if.then49:                                        ; preds = %if.then
  %77 = load i32, i32* %tmp, align 4, !tbaa !6
  %78 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %78, i32 0, i32 9
  %79 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul50 = mul nsw i32 %77, %79
  %80 = load i32, i32* %res, align 4, !tbaa !6
  %81 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %81, i32 0, i32 10
  %82 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul51 = mul nsw i32 %80, %82
  %add52 = add nsw i32 %mul50, %mul51
  store i32 %add52, i32* %tmp, align 4, !tbaa !6
  %83 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr53 = ashr i32 %83, 4
  store i32 %shr53, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  %84 = load i32, i32* %res, align 4, !tbaa !6
  %85 = load i32, i32* %tmp, align 4, !tbaa !6
  %add54 = add nsw i32 %85, %84
  store i32 %add54, i32* %tmp, align 4, !tbaa !6
  %86 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr55 = ashr i32 %86, 1
  store i32 %shr55, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then49
  %87 = load i32, i32* %round_offset, align 4, !tbaa !6
  %88 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub56 = sub nsw i32 %88, %87
  store i32 %sub56, i32* %tmp, align 4, !tbaa !6
  %89 = load i32, i32* %tmp, align 4, !tbaa !6
  %90 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl57 = shl i32 1, %90
  %shr58 = ashr i32 %shl57, 1
  %add59 = add nsw i32 %89, %shr58
  %91 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr60 = ashr i32 %add59, %91
  %call61 = call zeroext i8 @clip_pixel(i32 %shr60)
  %92 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %93 = load i32, i32* %y, align 4, !tbaa !6
  %94 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul62 = mul nsw i32 %93, %94
  %95 = load i32, i32* %x, align 4, !tbaa !6
  %add63 = add nsw i32 %mul62, %95
  %arrayidx64 = getelementptr inbounds i8, i8* %92, i32 %add63
  store i8 %call61, i8* %arrayidx64, align 1, !tbaa !8
  %96 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  br label %if.end70

if.else65:                                        ; preds = %for.end
  %97 = load i32, i32* %res, align 4, !tbaa !6
  %conv66 = trunc i32 %97 to i16
  %98 = load i16*, i16** %dst16, align 4, !tbaa !2
  %99 = load i32, i32* %y, align 4, !tbaa !6
  %100 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul67 = mul nsw i32 %99, %100
  %101 = load i32, i32* %x, align 4, !tbaa !6
  %add68 = add nsw i32 %mul67, %101
  %arrayidx69 = getelementptr inbounds i16, i16* %98, i32 %add68
  store i16 %conv66, i16* %arrayidx69, align 2, !tbaa !9
  br label %if.end70

if.end70:                                         ; preds = %if.else65, %if.end
  %102 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #4
  br label %for.inc71

for.inc71:                                        ; preds = %if.end70
  %103 = load i32, i32* %x, align 4, !tbaa !6
  %inc72 = add nsw i32 %103, 1
  store i32 %inc72, i32* %x, align 4, !tbaa !6
  br label %for.cond16

for.end73:                                        ; preds = %for.cond.cleanup19
  br label %for.inc74

for.inc74:                                        ; preds = %for.end73
  %104 = load i32, i32* %y, align 4, !tbaa !6
  %inc75 = add nsw i32 %104, 1
  store i32 %inc75, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end76:                                        ; preds = %for.cond.cleanup
  %105 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #4
  %106 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  %107 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  %108 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #4
  %109 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_dist_wtd_convolve_2d_copy_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %bits = alloca i32, align 4
  %bd = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %round_offset = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i16, align 2
  %tmp = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %1, i32 0, i32 1
  %2 = load i16*, i16** %dst1, align 4, !tbaa !19
  store i16* %2, i16** %dst16, align 4, !tbaa !2
  %3 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 2
  %5 = load i32, i32* %dst_stride2, align 4, !tbaa !20
  store i32 %5, i32* %dst16_stride, align 4, !tbaa !6
  %6 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %7, i32 0, i32 4
  %8 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub = sub nsw i32 14, %8
  %9 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %9, i32 0, i32 3
  %10 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub3 = sub nsw i32 %sub, %10
  store i32 %sub3, i32* %bits, align 4, !tbaa !6
  %11 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  store i32 8, i32* %bd, align 4, !tbaa !6
  %12 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_04 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %13, i32 0, i32 3
  %14 = load i32, i32* %round_04, align 4, !tbaa !15
  %sub5 = sub nsw i32 22, %14
  store i32 %sub5, i32* %offset_bits, align 4, !tbaa !6
  %15 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %17 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_16 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %17, i32 0, i32 4
  %18 = load i32, i32* %round_16, align 4, !tbaa !17
  %sub7 = sub nsw i32 %16, %18
  %shl = shl i32 1, %sub7
  %19 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %20 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_18 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %20, i32 0, i32 4
  %21 = load i32, i32* %round_18, align 4, !tbaa !17
  %sub9 = sub nsw i32 %19, %21
  %sub10 = sub nsw i32 %sub9, 1
  %shl11 = shl i32 1, %sub10
  %add = add nsw i32 %shl, %shl11
  store i32 %add, i32* %round_offset, align 4, !tbaa !6
  %22 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %23 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %24 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %25 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %26 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc48, %entry
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %28 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %27, %28
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %29 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  br label %for.end50

for.body:                                         ; preds = %for.cond
  %30 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %for.body
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %32 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %31, %32
  br i1 %cmp13, label %for.body15, label %for.cond.cleanup14

for.cond.cleanup14:                               ; preds = %for.cond12
  store i32 5, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  br label %for.end

for.body15:                                       ; preds = %for.cond12
  %34 = bitcast i16* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %34) #4
  %35 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %36 = load i32, i32* %y, align 4, !tbaa !6
  %37 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %36, %37
  %38 = load i32, i32* %x, align 4, !tbaa !6
  %add16 = add nsw i32 %mul, %38
  %arrayidx = getelementptr inbounds i8, i8* %35, i32 %add16
  %39 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %39 to i32
  %40 = load i32, i32* %bits, align 4, !tbaa !6
  %shl17 = shl i32 %conv, %40
  %conv18 = trunc i32 %shl17 to i16
  store i16 %conv18, i16* %res, align 2, !tbaa !9
  %41 = load i32, i32* %round_offset, align 4, !tbaa !6
  %42 = load i16, i16* %res, align 2, !tbaa !9
  %conv19 = zext i16 %42 to i32
  %add20 = add nsw i32 %conv19, %41
  %conv21 = trunc i32 %add20 to i16
  store i16 %conv21, i16* %res, align 2, !tbaa !9
  %43 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %43, i32 0, i32 0
  %44 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool = icmp ne i32 %44, 0
  br i1 %tobool, label %if.then, label %if.else43

if.then:                                          ; preds = %for.body15
  %45 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #4
  %46 = load i16*, i16** %dst16, align 4, !tbaa !2
  %47 = load i32, i32* %y, align 4, !tbaa !6
  %48 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul22 = mul nsw i32 %47, %48
  %49 = load i32, i32* %x, align 4, !tbaa !6
  %add23 = add nsw i32 %mul22, %49
  %arrayidx24 = getelementptr inbounds i16, i16* %46, i32 %add23
  %50 = load i16, i16* %arrayidx24, align 2, !tbaa !9
  %conv25 = zext i16 %50 to i32
  store i32 %conv25, i32* %tmp, align 4, !tbaa !6
  %51 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %51, i32 0, i32 8
  %52 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool26 = icmp ne i32 %52, 0
  br i1 %tobool26, label %if.then27, label %if.else

if.then27:                                        ; preds = %if.then
  %53 = load i32, i32* %tmp, align 4, !tbaa !6
  %54 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %54, i32 0, i32 9
  %55 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul28 = mul nsw i32 %53, %55
  %56 = load i16, i16* %res, align 2, !tbaa !9
  %conv29 = zext i16 %56 to i32
  %57 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %57, i32 0, i32 10
  %58 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul30 = mul nsw i32 %conv29, %58
  %add31 = add nsw i32 %mul28, %mul30
  store i32 %add31, i32* %tmp, align 4, !tbaa !6
  %59 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr = ashr i32 %59, 4
  store i32 %shr, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  %60 = load i16, i16* %res, align 2, !tbaa !9
  %conv32 = zext i16 %60 to i32
  %61 = load i32, i32* %tmp, align 4, !tbaa !6
  %add33 = add nsw i32 %61, %conv32
  store i32 %add33, i32* %tmp, align 4, !tbaa !6
  %62 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr34 = ashr i32 %62, 1
  store i32 %shr34, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then27
  %63 = load i32, i32* %round_offset, align 4, !tbaa !6
  %64 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub35 = sub nsw i32 %64, %63
  store i32 %sub35, i32* %tmp, align 4, !tbaa !6
  %65 = load i32, i32* %tmp, align 4, !tbaa !6
  %66 = load i32, i32* %bits, align 4, !tbaa !6
  %shl36 = shl i32 1, %66
  %shr37 = ashr i32 %shl36, 1
  %add38 = add nsw i32 %65, %shr37
  %67 = load i32, i32* %bits, align 4, !tbaa !6
  %shr39 = ashr i32 %add38, %67
  %call = call zeroext i8 @clip_pixel(i32 %shr39)
  %68 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %69 = load i32, i32* %y, align 4, !tbaa !6
  %70 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul40 = mul nsw i32 %69, %70
  %71 = load i32, i32* %x, align 4, !tbaa !6
  %add41 = add nsw i32 %mul40, %71
  %arrayidx42 = getelementptr inbounds i8, i8* %68, i32 %add41
  store i8 %call, i8* %arrayidx42, align 1, !tbaa !8
  %72 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  br label %if.end47

if.else43:                                        ; preds = %for.body15
  %73 = load i16, i16* %res, align 2, !tbaa !9
  %74 = load i16*, i16** %dst16, align 4, !tbaa !2
  %75 = load i32, i32* %y, align 4, !tbaa !6
  %76 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul44 = mul nsw i32 %75, %76
  %77 = load i32, i32* %x, align 4, !tbaa !6
  %add45 = add nsw i32 %mul44, %77
  %arrayidx46 = getelementptr inbounds i16, i16* %74, i32 %add45
  store i16 %73, i16* %arrayidx46, align 2, !tbaa !9
  br label %if.end47

if.end47:                                         ; preds = %if.else43, %if.end
  %78 = bitcast i16* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %78) #4
  br label %for.inc

for.inc:                                          ; preds = %if.end47
  %79 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %79, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond12

for.end:                                          ; preds = %for.cond.cleanup14
  br label %for.inc48

for.inc48:                                        ; preds = %for.end
  %80 = load i32, i32* %y, align 4, !tbaa !6
  %inc49 = add nsw i32 %80, 1
  store i32 %inc49, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end50:                                        ; preds = %for.cond.cleanup
  %81 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_convolve_2d_scale_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %x_step_qn, i32 %subpel_y_qn, i32 %y_step_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %x_step_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %y_step_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %im_block = alloca [33792 x i16], align 16
  %im_h = alloca i32, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %bits = alloca i32, align 4
  %im_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %bd = alloca i32, align 4
  %src_horiz = alloca i8*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x_qn = alloca i32, align 4
  %x = alloca i32, align 4
  %src_x = alloca i8*, align 4
  %x_filter_idx = alloca i32, align 4
  %x_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  %src_vert = alloca i16*, align 4
  %offset_bits = alloca i32, align 4
  %x57 = alloca i32, align 4
  %y_qn = alloca i32, align 4
  %y63 = alloca i32, align 4
  %src_y = alloca i16*, align 4
  %y_filter_idx = alloca i32, align 4
  %y_filter = alloca i16*, align 4
  %sum75 = alloca i32, align 4
  %k77 = alloca i32, align 4
  %res = alloca i16, align 2
  %tmp = alloca i32, align 4
  %tmp142 = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %x_step_qn, i32* %x_step_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store i32 %y_step_qn, i32* %y_step_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast [33792 x i16]* %im_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 67584, i8* %0) #4
  %1 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %h.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 1
  %3 = load i32, i32* %y_step_qn.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %sub, %3
  %4 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %add = add nsw i32 %mul, %4
  %shr = ashr i32 %add, 10
  %5 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %5, i32 0, i32 1
  %6 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %6 to i32
  %add1 = add nsw i32 %shr, %conv
  store i32 %add1, i32* %im_h, align 4, !tbaa !6
  %7 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %8, i32 0, i32 1
  %9 = load i16*, i16** %dst2, align 4, !tbaa !19
  store i16* %9, i16** %dst16, align 4, !tbaa !2
  %10 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride3 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %11, i32 0, i32 2
  %12 = load i32, i32* %dst_stride3, align 4, !tbaa !20
  store i32 %12, i32* %dst16_stride, align 4, !tbaa !6
  %13 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %14, i32 0, i32 3
  %15 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub4 = sub nsw i32 14, %15
  %16 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %16, i32 0, i32 4
  %17 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub5 = sub nsw i32 %sub4, %17
  store i32 %sub5, i32* %bits, align 4, !tbaa !6
  %18 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load i32, i32* %w.addr, align 4, !tbaa !6
  store i32 %19, i32* %im_stride, align 4, !tbaa !6
  %20 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps6 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %21, i32 0, i32 1
  %22 = load i16, i16* %taps6, align 4, !tbaa !13
  %conv7 = zext i16 %22 to i32
  %div = sdiv i32 %conv7, 2
  %sub8 = sub nsw i32 %div, 1
  store i32 %sub8, i32* %fo_vert, align 4, !tbaa !6
  %23 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps9 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %24, i32 0, i32 1
  %25 = load i16, i16* %taps9, align 4, !tbaa !13
  %conv10 = zext i16 %25 to i32
  %div11 = sdiv i32 %conv10, 2
  %sub12 = sub nsw i32 %div11, 1
  store i32 %sub12, i32* %fo_horiz, align 4, !tbaa !6
  %26 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  store i32 8, i32* %bd, align 4, !tbaa !6
  %27 = bitcast i8** %src_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %29 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 %29, %30
  %idx.neg = sub i32 0, %mul13
  %add.ptr = getelementptr inbounds i8, i8* %28, i32 %idx.neg
  store i8* %add.ptr, i8** %src_horiz, align 4, !tbaa !2
  %31 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc50, %entry
  %32 = load i32, i32* %y, align 4, !tbaa !6
  %33 = load i32, i32* %im_h, align 4, !tbaa !6
  %cmp = icmp slt i32 %32, %33
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %34 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  br label %for.end52

for.body:                                         ; preds = %for.cond
  %35 = bitcast i32* %x_qn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %36, i32* %x_qn, align 4, !tbaa !6
  %37 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc45, %for.body
  %38 = load i32, i32* %x, align 4, !tbaa !6
  %39 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %38, %39
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  store i32 5, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.end48

for.body19:                                       ; preds = %for.cond15
  %41 = bitcast i8** %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  %42 = load i8*, i8** %src_horiz, align 4, !tbaa !2
  %43 = load i32, i32* %x_qn, align 4, !tbaa !6
  %shr20 = ashr i32 %43, 10
  %arrayidx = getelementptr inbounds i8, i8* %42, i32 %shr20
  store i8* %arrayidx, i8** %src_x, align 4, !tbaa !2
  %44 = bitcast i32* %x_filter_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #4
  %45 = load i32, i32* %x_qn, align 4, !tbaa !6
  %and = and i32 %45, 1023
  %shr21 = ashr i32 %and, 6
  store i32 %shr21, i32* %x_filter_idx, align 4, !tbaa !6
  %46 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #4
  %47 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %48 = load i32, i32* %x_filter_idx, align 4, !tbaa !6
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %47, i32 %48)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %49 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #4
  store i32 16384, i32* %sum, align 4, !tbaa !6
  %50 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %for.body19
  %51 = load i32, i32* %k, align 4, !tbaa !6
  %52 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps23 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %52, i32 0, i32 1
  %53 = load i16, i16* %taps23, align 4, !tbaa !13
  %conv24 = zext i16 %53 to i32
  %cmp25 = icmp slt i32 %51, %conv24
  br i1 %cmp25, label %for.body28, label %for.cond.cleanup27

for.cond.cleanup27:                               ; preds = %for.cond22
  store i32 8, i32* %cleanup.dest.slot, align 4
  %54 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  br label %for.end

for.body28:                                       ; preds = %for.cond22
  %55 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %56 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds i16, i16* %55, i32 %56
  %57 = load i16, i16* %arrayidx29, align 2, !tbaa !9
  %conv30 = sext i16 %57 to i32
  %58 = load i8*, i8** %src_x, align 4, !tbaa !2
  %59 = load i32, i32* %k, align 4, !tbaa !6
  %60 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub31 = sub nsw i32 %59, %60
  %arrayidx32 = getelementptr inbounds i8, i8* %58, i32 %sub31
  %61 = load i8, i8* %arrayidx32, align 1, !tbaa !8
  %conv33 = zext i8 %61 to i32
  %mul34 = mul nsw i32 %conv30, %conv33
  %62 = load i32, i32* %sum, align 4, !tbaa !6
  %add35 = add nsw i32 %62, %mul34
  store i32 %add35, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body28
  %63 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %63, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond22

for.end:                                          ; preds = %for.cond.cleanup27
  %64 = load i32, i32* %sum, align 4, !tbaa !6
  %65 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_036 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %65, i32 0, i32 3
  %66 = load i32, i32* %round_036, align 4, !tbaa !15
  %shl = shl i32 1, %66
  %shr37 = ashr i32 %shl, 1
  %add38 = add nsw i32 %64, %shr37
  %67 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_039 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %67, i32 0, i32 3
  %68 = load i32, i32* %round_039, align 4, !tbaa !15
  %shr40 = ashr i32 %add38, %68
  %conv41 = trunc i32 %shr40 to i16
  %69 = load i32, i32* %y, align 4, !tbaa !6
  %70 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul42 = mul nsw i32 %69, %70
  %71 = load i32, i32* %x, align 4, !tbaa !6
  %add43 = add nsw i32 %mul42, %71
  %arrayidx44 = getelementptr inbounds [33792 x i16], [33792 x i16]* %im_block, i32 0, i32 %add43
  store i16 %conv41, i16* %arrayidx44, align 2, !tbaa !9
  %72 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  %73 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  %74 = bitcast i32* %x_filter_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  %75 = bitcast i8** %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  br label %for.inc45

for.inc45:                                        ; preds = %for.end
  %76 = load i32, i32* %x, align 4, !tbaa !6
  %inc46 = add nsw i32 %76, 1
  store i32 %inc46, i32* %x, align 4, !tbaa !6
  %77 = load i32, i32* %x_step_qn.addr, align 4, !tbaa !6
  %78 = load i32, i32* %x_qn, align 4, !tbaa !6
  %add47 = add nsw i32 %78, %77
  store i32 %add47, i32* %x_qn, align 4, !tbaa !6
  br label %for.cond15

for.end48:                                        ; preds = %for.cond.cleanup18
  %79 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %80 = load i8*, i8** %src_horiz, align 4, !tbaa !2
  %add.ptr49 = getelementptr inbounds i8, i8* %80, i32 %79
  store i8* %add.ptr49, i8** %src_horiz, align 4, !tbaa !2
  %81 = bitcast i32* %x_qn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  br label %for.inc50

for.inc50:                                        ; preds = %for.end48
  %82 = load i32, i32* %y, align 4, !tbaa !6
  %inc51 = add nsw i32 %82, 1
  store i32 %inc51, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end52:                                        ; preds = %for.cond.cleanup
  %83 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #4
  %arraydecay = getelementptr inbounds [33792 x i16], [33792 x i16]* %im_block, i32 0, i32 0
  %84 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %85 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul53 = mul nsw i32 %84, %85
  %add.ptr54 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul53
  store i16* %add.ptr54, i16** %src_vert, align 4, !tbaa !2
  %86 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #4
  %87 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_055 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %87, i32 0, i32 3
  %88 = load i32, i32* %round_055, align 4, !tbaa !15
  %sub56 = sub nsw i32 22, %88
  store i32 %sub56, i32* %offset_bits, align 4, !tbaa !6
  %89 = bitcast i32* %x57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #4
  store i32 0, i32* %x57, align 4, !tbaa !6
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc166, %for.end52
  %90 = load i32, i32* %x57, align 4, !tbaa !6
  %91 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp59 = icmp slt i32 %90, %91
  br i1 %cmp59, label %for.body62, label %for.cond.cleanup61

for.cond.cleanup61:                               ; preds = %for.cond58
  store i32 11, i32* %cleanup.dest.slot, align 4
  %92 = bitcast i32* %x57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  br label %for.end168

for.body62:                                       ; preds = %for.cond58
  %93 = bitcast i32* %y_qn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #4
  %94 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store i32 %94, i32* %y_qn, align 4, !tbaa !6
  %95 = bitcast i32* %y63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #4
  store i32 0, i32* %y63, align 4, !tbaa !6
  br label %for.cond64

for.cond64:                                       ; preds = %for.inc162, %for.body62
  %96 = load i32, i32* %y63, align 4, !tbaa !6
  %97 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp65 = icmp slt i32 %96, %97
  br i1 %cmp65, label %for.body68, label %for.cond.cleanup67

for.cond.cleanup67:                               ; preds = %for.cond64
  store i32 14, i32* %cleanup.dest.slot, align 4
  %98 = bitcast i32* %y63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  br label %for.end165

for.body68:                                       ; preds = %for.cond64
  %99 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #4
  %100 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %101 = load i32, i32* %y_qn, align 4, !tbaa !6
  %shr69 = ashr i32 %101, 10
  %102 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul70 = mul nsw i32 %shr69, %102
  %arrayidx71 = getelementptr inbounds i16, i16* %100, i32 %mul70
  store i16* %arrayidx71, i16** %src_y, align 4, !tbaa !2
  %103 = bitcast i32* %y_filter_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #4
  %104 = load i32, i32* %y_qn, align 4, !tbaa !6
  %and72 = and i32 %104, 1023
  %shr73 = ashr i32 %and72, 6
  store i32 %shr73, i32* %y_filter_idx, align 4, !tbaa !6
  %105 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #4
  %106 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %107 = load i32, i32* %y_filter_idx, align 4, !tbaa !6
  %call74 = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %106, i32 %107)
  store i16* %call74, i16** %y_filter, align 4, !tbaa !2
  %108 = bitcast i32* %sum75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #4
  %109 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %shl76 = shl i32 1, %109
  store i32 %shl76, i32* %sum75, align 4, !tbaa !6
  %110 = bitcast i32* %k77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #4
  store i32 0, i32* %k77, align 4, !tbaa !6
  br label %for.cond78

for.cond78:                                       ; preds = %for.inc93, %for.body68
  %111 = load i32, i32* %k77, align 4, !tbaa !6
  %112 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps79 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %112, i32 0, i32 1
  %113 = load i16, i16* %taps79, align 4, !tbaa !13
  %conv80 = zext i16 %113 to i32
  %cmp81 = icmp slt i32 %111, %conv80
  br i1 %cmp81, label %for.body84, label %for.cond.cleanup83

for.cond.cleanup83:                               ; preds = %for.cond78
  store i32 17, i32* %cleanup.dest.slot, align 4
  %114 = bitcast i32* %k77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #4
  br label %for.end95

for.body84:                                       ; preds = %for.cond78
  %115 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %116 = load i32, i32* %k77, align 4, !tbaa !6
  %arrayidx85 = getelementptr inbounds i16, i16* %115, i32 %116
  %117 = load i16, i16* %arrayidx85, align 2, !tbaa !9
  %conv86 = sext i16 %117 to i32
  %118 = load i16*, i16** %src_y, align 4, !tbaa !2
  %119 = load i32, i32* %k77, align 4, !tbaa !6
  %120 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub87 = sub nsw i32 %119, %120
  %121 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul88 = mul nsw i32 %sub87, %121
  %arrayidx89 = getelementptr inbounds i16, i16* %118, i32 %mul88
  %122 = load i16, i16* %arrayidx89, align 2, !tbaa !9
  %conv90 = sext i16 %122 to i32
  %mul91 = mul nsw i32 %conv86, %conv90
  %123 = load i32, i32* %sum75, align 4, !tbaa !6
  %add92 = add nsw i32 %123, %mul91
  store i32 %add92, i32* %sum75, align 4, !tbaa !6
  br label %for.inc93

for.inc93:                                        ; preds = %for.body84
  %124 = load i32, i32* %k77, align 4, !tbaa !6
  %inc94 = add nsw i32 %124, 1
  store i32 %inc94, i32* %k77, align 4, !tbaa !6
  br label %for.cond78

for.end95:                                        ; preds = %for.cond.cleanup83
  %125 = bitcast i16* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %125) #4
  %126 = load i32, i32* %sum75, align 4, !tbaa !6
  %127 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_196 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %127, i32 0, i32 4
  %128 = load i32, i32* %round_196, align 4, !tbaa !17
  %shl97 = shl i32 1, %128
  %shr98 = ashr i32 %shl97, 1
  %add99 = add nsw i32 %126, %shr98
  %129 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1100 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %129, i32 0, i32 4
  %130 = load i32, i32* %round_1100, align 4, !tbaa !17
  %shr101 = ashr i32 %add99, %130
  %conv102 = trunc i32 %shr101 to i16
  store i16 %conv102, i16* %res, align 2, !tbaa !9
  %131 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %131, i32 0, i32 6
  %132 = load i32, i32* %is_compound, align 4, !tbaa !25
  %tobool = icmp ne i32 %132, 0
  br i1 %tobool, label %if.then, label %if.else141

if.then:                                          ; preds = %for.end95
  %133 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %133, i32 0, i32 0
  %134 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool103 = icmp ne i32 %134, 0
  br i1 %tobool103, label %if.then104, label %if.else136

if.then104:                                       ; preds = %if.then
  %135 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #4
  %136 = load i16*, i16** %dst16, align 4, !tbaa !2
  %137 = load i32, i32* %y63, align 4, !tbaa !6
  %138 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul105 = mul nsw i32 %137, %138
  %139 = load i32, i32* %x57, align 4, !tbaa !6
  %add106 = add nsw i32 %mul105, %139
  %arrayidx107 = getelementptr inbounds i16, i16* %136, i32 %add106
  %140 = load i16, i16* %arrayidx107, align 2, !tbaa !9
  %conv108 = zext i16 %140 to i32
  store i32 %conv108, i32* %tmp, align 4, !tbaa !6
  %141 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %141, i32 0, i32 8
  %142 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool109 = icmp ne i32 %142, 0
  br i1 %tobool109, label %if.then110, label %if.else

if.then110:                                       ; preds = %if.then104
  %143 = load i32, i32* %tmp, align 4, !tbaa !6
  %144 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %144, i32 0, i32 9
  %145 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul111 = mul nsw i32 %143, %145
  %146 = load i16, i16* %res, align 2, !tbaa !9
  %conv112 = zext i16 %146 to i32
  %147 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %147, i32 0, i32 10
  %148 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul113 = mul nsw i32 %conv112, %148
  %add114 = add nsw i32 %mul111, %mul113
  store i32 %add114, i32* %tmp, align 4, !tbaa !6
  %149 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr115 = ashr i32 %149, 4
  store i32 %shr115, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then104
  %150 = load i16, i16* %res, align 2, !tbaa !9
  %conv116 = zext i16 %150 to i32
  %151 = load i32, i32* %tmp, align 4, !tbaa !6
  %add117 = add nsw i32 %151, %conv116
  store i32 %add117, i32* %tmp, align 4, !tbaa !6
  %152 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr118 = ashr i32 %152, 1
  store i32 %shr118, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then110
  %153 = load i32, i32* %tmp, align 4, !tbaa !6
  %154 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %155 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1119 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %155, i32 0, i32 4
  %156 = load i32, i32* %round_1119, align 4, !tbaa !17
  %sub120 = sub nsw i32 %154, %156
  %shl121 = shl i32 1, %sub120
  %157 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %158 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1122 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %158, i32 0, i32 4
  %159 = load i32, i32* %round_1122, align 4, !tbaa !17
  %sub123 = sub nsw i32 %157, %159
  %sub124 = sub nsw i32 %sub123, 1
  %shl125 = shl i32 1, %sub124
  %add126 = add nsw i32 %shl121, %shl125
  %sub127 = sub nsw i32 %153, %add126
  store i32 %sub127, i32* %tmp, align 4, !tbaa !6
  %160 = load i32, i32* %tmp, align 4, !tbaa !6
  %161 = load i32, i32* %bits, align 4, !tbaa !6
  %shl128 = shl i32 1, %161
  %shr129 = ashr i32 %shl128, 1
  %add130 = add nsw i32 %160, %shr129
  %162 = load i32, i32* %bits, align 4, !tbaa !6
  %shr131 = ashr i32 %add130, %162
  %call132 = call zeroext i8 @clip_pixel(i32 %shr131)
  %163 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %164 = load i32, i32* %y63, align 4, !tbaa !6
  %165 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul133 = mul nsw i32 %164, %165
  %166 = load i32, i32* %x57, align 4, !tbaa !6
  %add134 = add nsw i32 %mul133, %166
  %arrayidx135 = getelementptr inbounds i8, i8* %163, i32 %add134
  store i8 %call132, i8* %arrayidx135, align 1, !tbaa !8
  %167 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  br label %if.end140

if.else136:                                       ; preds = %if.then
  %168 = load i16, i16* %res, align 2, !tbaa !9
  %169 = load i16*, i16** %dst16, align 4, !tbaa !2
  %170 = load i32, i32* %y63, align 4, !tbaa !6
  %171 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul137 = mul nsw i32 %170, %171
  %172 = load i32, i32* %x57, align 4, !tbaa !6
  %add138 = add nsw i32 %mul137, %172
  %arrayidx139 = getelementptr inbounds i16, i16* %169, i32 %add138
  store i16 %168, i16* %arrayidx139, align 2, !tbaa !9
  br label %if.end140

if.end140:                                        ; preds = %if.else136, %if.end
  br label %if.end161

if.else141:                                       ; preds = %for.end95
  %173 = bitcast i32* %tmp142 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #4
  %174 = load i16, i16* %res, align 2, !tbaa !9
  %conv143 = zext i16 %174 to i32
  %175 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %176 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1144 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %176, i32 0, i32 4
  %177 = load i32, i32* %round_1144, align 4, !tbaa !17
  %sub145 = sub nsw i32 %175, %177
  %shl146 = shl i32 1, %sub145
  %178 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %179 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1147 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %179, i32 0, i32 4
  %180 = load i32, i32* %round_1147, align 4, !tbaa !17
  %sub148 = sub nsw i32 %178, %180
  %sub149 = sub nsw i32 %sub148, 1
  %shl150 = shl i32 1, %sub149
  %add151 = add nsw i32 %shl146, %shl150
  %sub152 = sub nsw i32 %conv143, %add151
  store i32 %sub152, i32* %tmp142, align 4, !tbaa !6
  %181 = load i32, i32* %tmp142, align 4, !tbaa !6
  %182 = load i32, i32* %bits, align 4, !tbaa !6
  %shl153 = shl i32 1, %182
  %shr154 = ashr i32 %shl153, 1
  %add155 = add nsw i32 %181, %shr154
  %183 = load i32, i32* %bits, align 4, !tbaa !6
  %shr156 = ashr i32 %add155, %183
  %call157 = call zeroext i8 @clip_pixel(i32 %shr156)
  %184 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %185 = load i32, i32* %y63, align 4, !tbaa !6
  %186 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul158 = mul nsw i32 %185, %186
  %187 = load i32, i32* %x57, align 4, !tbaa !6
  %add159 = add nsw i32 %mul158, %187
  %arrayidx160 = getelementptr inbounds i8, i8* %184, i32 %add159
  store i8 %call157, i8* %arrayidx160, align 1, !tbaa !8
  %188 = bitcast i32* %tmp142 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #4
  br label %if.end161

if.end161:                                        ; preds = %if.else141, %if.end140
  %189 = bitcast i16* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %189) #4
  %190 = bitcast i32* %sum75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #4
  %191 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #4
  %192 = bitcast i32* %y_filter_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #4
  %193 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #4
  br label %for.inc162

for.inc162:                                       ; preds = %if.end161
  %194 = load i32, i32* %y63, align 4, !tbaa !6
  %inc163 = add nsw i32 %194, 1
  store i32 %inc163, i32* %y63, align 4, !tbaa !6
  %195 = load i32, i32* %y_step_qn.addr, align 4, !tbaa !6
  %196 = load i32, i32* %y_qn, align 4, !tbaa !6
  %add164 = add nsw i32 %196, %195
  store i32 %add164, i32* %y_qn, align 4, !tbaa !6
  br label %for.cond64

for.end165:                                       ; preds = %for.cond.cleanup67
  %197 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %197, i32 1
  store i16* %incdec.ptr, i16** %src_vert, align 4, !tbaa !2
  %198 = bitcast i32* %y_qn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  br label %for.inc166

for.inc166:                                       ; preds = %for.end165
  %199 = load i32, i32* %x57, align 4, !tbaa !6
  %inc167 = add nsw i32 %199, 1
  store i32 %inc167, i32* %x57, align 4, !tbaa !6
  br label %for.cond58

for.end168:                                       ; preds = %for.cond.cleanup61
  %200 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #4
  %201 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #4
  %202 = bitcast i8** %src_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast [33792 x i16]* %im_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 67584, i8* %211) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_convolve_2d_facade(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams** %interp_filters, i32 %subpel_x_qn, i32 %x_step_q4, i32 %subpel_y_qn, i32 %y_step_q4, i32 %scaled, %struct.ConvolveParams* %conv_params, %struct.scale_factors* %sf) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %interp_filters.addr = alloca %struct.InterpFilterParams**, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %x_step_q4.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %y_step_q4.addr = alloca i32, align 4
  %scaled.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %filter_params_x = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y = alloca %struct.InterpFilterParams*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams** %interp_filters, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !6
  store i32 %scaled, i32* %scaled.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %0 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !6
  %1 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %2 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %3 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %4 = bitcast %struct.InterpFilterParams** %filter_params_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.InterpFilterParams**, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.InterpFilterParams*, %struct.InterpFilterParams** %5, i32 0
  %6 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %arrayidx, align 4, !tbaa !2
  store %struct.InterpFilterParams* %6, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %7 = bitcast %struct.InterpFilterParams** %filter_params_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.InterpFilterParams**, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds %struct.InterpFilterParams*, %struct.InterpFilterParams** %8, i32 1
  %9 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %arrayidx1, align 4, !tbaa !2
  store %struct.InterpFilterParams* %9, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %10 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %10, i32 0, i32 1
  %11 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %11 to i32
  %cmp = icmp eq i32 %conv, 2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %12 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %taps3 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %12, i32 0, i32 1
  %13 = load i16, i16* %taps3, align 4, !tbaa !13
  %conv4 = zext i16 %13 to i32
  %cmp5 = icmp eq i32 %conv4, 2
  br i1 %cmp5, label %if.then, label %if.end16

if.then:                                          ; preds = %lor.lhs.false, %entry
  %14 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then
  %15 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %tobool7 = icmp ne i32 %15, 0
  br i1 %tobool7, label %if.then8, label %if.else

if.then8:                                         ; preds = %land.lhs.true
  %16 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %17 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %18 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %19 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %20 = load i32, i32* %w.addr, align 4, !tbaa !6
  %21 = load i32, i32* %h.addr, align 4, !tbaa !6
  %22 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %23 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %24 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %25 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %26 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  call void @av1_convolve_2d_sr_c(i8* %16, i32 %17, i8* %18, i32 %19, i32 %20, i32 %21, %struct.InterpFilterParams* %22, %struct.InterpFilterParams* %23, i32 %24, i32 %25, %struct.ConvolveParams* %26)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %land.lhs.true, %if.then
  %27 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %tobool9 = icmp ne i32 %27, 0
  br i1 %tobool9, label %if.then10, label %if.else11

if.then10:                                        ; preds = %if.else
  %28 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %29 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %30 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %31 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %32 = load i32, i32* %w.addr, align 4, !tbaa !6
  %33 = load i32, i32* %h.addr, align 4, !tbaa !6
  %34 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %35 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %36 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %37 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %38 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  call void @av1_convolve_x_sr_c(i8* %28, i32 %29, i8* %30, i32 %31, i32 %32, i32 %33, %struct.InterpFilterParams* %34, %struct.InterpFilterParams* %35, i32 %36, i32 %37, %struct.ConvolveParams* %38)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else11:                                        ; preds = %if.else
  %39 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %tobool12 = icmp ne i32 %39, 0
  br i1 %tobool12, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.else11
  %40 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %41 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %42 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %43 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %44 = load i32, i32* %w.addr, align 4, !tbaa !6
  %45 = load i32, i32* %h.addr, align 4, !tbaa !6
  %46 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %47 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %48 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %49 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %50 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  call void @av1_convolve_y_sr_c(i8* %40, i32 %41, i8* %42, i32 %43, i32 %44, i32 %45, %struct.InterpFilterParams* %46, %struct.InterpFilterParams* %47, i32 %48, i32 %49, %struct.ConvolveParams* %50)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.else11
  br label %if.end14

if.end14:                                         ; preds = %if.end
  br label %if.end15

if.end15:                                         ; preds = %if.end14
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %lor.lhs.false
  %51 = load i32, i32* %scaled.addr, align 4, !tbaa !6
  %tobool17 = icmp ne i32 %51, 0
  br i1 %tobool17, label %if.then18, label %if.else19

if.then18:                                        ; preds = %if.end16
  %52 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %53 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %54 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %55 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %56 = load i32, i32* %w.addr, align 4, !tbaa !6
  %57 = load i32, i32* %h.addr, align 4, !tbaa !6
  %58 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %59 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %60 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %61 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !6
  %62 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %63 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %64 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  call void @convolve_2d_scale_wrapper(i8* %52, i32 %53, i8* %54, i32 %55, i32 %56, i32 %57, %struct.InterpFilterParams* %58, %struct.InterpFilterParams* %59, i32 %60, i32 %61, i32 %62, i32 %63, %struct.ConvolveParams* %64)
  br label %if.end27

if.else19:                                        ; preds = %if.end16
  %65 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %convolve = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %65, i32 0, i32 6
  %66 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %cmp20 = icmp ne i32 %66, 0
  %conv21 = zext i1 %cmp20 to i32
  %arrayidx22 = getelementptr inbounds [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]], [2 x [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]]* %convolve, i32 0, i32 %conv21
  %67 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %cmp23 = icmp ne i32 %67, 0
  %conv24 = zext i1 %cmp23 to i32
  %arrayidx25 = getelementptr inbounds [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]], [2 x [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]]* %arrayidx22, i32 0, i32 %conv24
  %68 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %68, i32 0, i32 6
  %69 = load i32, i32* %is_compound, align 4, !tbaa !25
  %arrayidx26 = getelementptr inbounds [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*], [2 x void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*]* %arrayidx25, i32 0, i32 %69
  %70 = load void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)*, void (i8*, i32, i8*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*)** %arrayidx26, align 4, !tbaa !2
  %71 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %72 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %73 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %74 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %75 = load i32, i32* %w.addr, align 4, !tbaa !6
  %76 = load i32, i32* %h.addr, align 4, !tbaa !6
  %77 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %78 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %79 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %80 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %81 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  call void %70(i8* %71, i32 %72, i8* %73, i32 %74, i32 %75, i32 %76, %struct.InterpFilterParams* %77, %struct.InterpFilterParams* %78, i32 %79, i32 %80, %struct.ConvolveParams* %81)
  br label %if.end27

if.end27:                                         ; preds = %if.else19, %if.then18
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end27, %if.then13, %if.then10, %if.then8
  %82 = bitcast %struct.InterpFilterParams** %filter_params_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast %struct.InterpFilterParams** %filter_params_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @convolve_2d_scale_wrapper(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %x_step_qn, i32 %subpel_y_qn, i32 %y_step_qn, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %x_step_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %y_step_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %x_step_qn, i32* %x_step_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store i32 %y_step_qn, i32* %y_step_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %0, i32 0, i32 6
  %1 = load i32, i32* %is_compound, align 4, !tbaa !25
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %3 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %4 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %5 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %6 = load i32, i32* %w.addr, align 4, !tbaa !6
  %7 = load i32, i32* %h.addr, align 4, !tbaa !6
  %8 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %9 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %10 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %11 = load i32, i32* %x_step_qn.addr, align 4, !tbaa !6
  %12 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %13 = load i32, i32* %y_step_qn.addr, align 4, !tbaa !6
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  call void @av1_convolve_2d_scale_c(i8* %2, i32 %3, i8* %4, i32 %5, i32 %6, i32 %7, %struct.InterpFilterParams* %8, %struct.InterpFilterParams* %9, i32 %10, i32 %11, i32 %12, i32 %13, %struct.ConvolveParams* %14)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_convolve_2d_copy_sr_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %y = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %1 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %2 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %3 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %5 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %6 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %y, align 4, !tbaa !6
  %8 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %7, %8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %9 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %11 = load i32, i32* %y, align 4, !tbaa !6
  %12 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %11, %12
  %add.ptr = getelementptr inbounds i16, i16* %10, i32 %mul
  %13 = bitcast i16* %add.ptr to i8*
  %14 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %15 = load i32, i32* %y, align 4, !tbaa !6
  %16 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul1 = mul nsw i32 %15, %16
  %add.ptr2 = getelementptr inbounds i16, i16* %14, i32 %mul1
  %17 = bitcast i16* %add.ptr2 to i8*
  %18 = load i32, i32* %w.addr, align 4, !tbaa !6
  %mul3 = mul i32 %18, 2
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 2 %13, i8* align 2 %17, i32 %mul3, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %y, align 4, !tbaa !6
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_convolve_x_sr_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %bits = alloca i32, align 4
  %x_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i32, align 4
  %k = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %1, i32 0, i32 1
  %2 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %2 to i32
  %div = sdiv i32 %conv, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %fo_horiz, align 4, !tbaa !6
  %3 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 3
  %5 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub1 = sub nsw i32 7, %5
  store i32 %sub1, i32* %bits, align 4, !tbaa !6
  %6 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %7 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %8 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %10 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %and = and i32 %10, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %9, i32 %and)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %11 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc37, %entry
  %12 = load i32, i32* %y, align 4, !tbaa !6
  %13 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %12, %13
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %14 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  br label %for.end39

for.body:                                         ; preds = %for.cond
  %15 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc34, %for.body
  %16 = load i32, i32* %x, align 4, !tbaa !6
  %17 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %16, %17
  br i1 %cmp4, label %for.body7, label %for.cond.cleanup6

for.cond.cleanup6:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  %18 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  br label %for.end36

for.body7:                                        ; preds = %for.cond3
  %19 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  store i32 0, i32* %res, align 4, !tbaa !6
  %20 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body7
  %21 = load i32, i32* %k, align 4, !tbaa !6
  %22 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps9 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %22, i32 0, i32 1
  %23 = load i16, i16* %taps9, align 4, !tbaa !13
  %conv10 = zext i16 %23 to i32
  %cmp11 = icmp slt i32 %21, %conv10
  br i1 %cmp11, label %for.body14, label %for.cond.cleanup13

for.cond.cleanup13:                               ; preds = %for.cond8
  store i32 8, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  br label %for.end

for.body14:                                       ; preds = %for.cond8
  %25 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %26 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %25, i32 %26
  %27 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv15 = sext i16 %27 to i32
  %28 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %29 = load i32, i32* %y, align 4, !tbaa !6
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %29, %30
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %add = add nsw i32 %mul, %31
  %32 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub16 = sub nsw i32 %add, %32
  %33 = load i32, i32* %k, align 4, !tbaa !6
  %add17 = add nsw i32 %sub16, %33
  %arrayidx18 = getelementptr inbounds i16, i16* %28, i32 %add17
  %34 = load i16, i16* %arrayidx18, align 2, !tbaa !9
  %conv19 = zext i16 %34 to i32
  %mul20 = mul nsw i32 %conv15, %conv19
  %35 = load i32, i32* %res, align 4, !tbaa !6
  %add21 = add nsw i32 %35, %mul20
  store i32 %add21, i32* %res, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %36 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond8

for.end:                                          ; preds = %for.cond.cleanup13
  %37 = load i32, i32* %res, align 4, !tbaa !6
  %38 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_022 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %38, i32 0, i32 3
  %39 = load i32, i32* %round_022, align 4, !tbaa !15
  %shl = shl i32 1, %39
  %shr = ashr i32 %shl, 1
  %add23 = add nsw i32 %37, %shr
  %40 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_024 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %40, i32 0, i32 3
  %41 = load i32, i32* %round_024, align 4, !tbaa !15
  %shr25 = ashr i32 %add23, %41
  store i32 %shr25, i32* %res, align 4, !tbaa !6
  %42 = load i32, i32* %res, align 4, !tbaa !6
  %43 = load i32, i32* %bits, align 4, !tbaa !6
  %shl26 = shl i32 1, %43
  %shr27 = ashr i32 %shl26, 1
  %add28 = add nsw i32 %42, %shr27
  %44 = load i32, i32* %bits, align 4, !tbaa !6
  %shr29 = ashr i32 %add28, %44
  %45 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call30 = call zeroext i16 @clip_pixel_highbd(i32 %shr29, i32 %45)
  %46 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %47 = load i32, i32* %y, align 4, !tbaa !6
  %48 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul31 = mul nsw i32 %47, %48
  %49 = load i32, i32* %x, align 4, !tbaa !6
  %add32 = add nsw i32 %mul31, %49
  %arrayidx33 = getelementptr inbounds i16, i16* %46, i32 %add32
  store i16 %call30, i16* %arrayidx33, align 2, !tbaa !9
  %50 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  br label %for.inc34

for.inc34:                                        ; preds = %for.end
  %51 = load i32, i32* %x, align 4, !tbaa !6
  %inc35 = add nsw i32 %51, 1
  store i32 %inc35, i32* %x, align 4, !tbaa !6
  br label %for.cond3

for.end36:                                        ; preds = %for.cond.cleanup6
  br label %for.inc37

for.inc37:                                        ; preds = %for.end36
  %52 = load i32, i32* %y, align 4, !tbaa !6
  %inc38 = add nsw i32 %52, 1
  store i32 %inc38, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end39:                                        ; preds = %for.cond.cleanup
  %53 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_convolve_y_sr_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %y_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i32, align 4
  %k = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %1, i32 0, i32 1
  %2 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %2 to i32
  %div = sdiv i32 %conv, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %fo_vert, align 4, !tbaa !6
  %3 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %4 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %5 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %6 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %8 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %and = and i32 %8, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %7, i32 %and)
  store i16* %call, i16** %y_filter, align 4, !tbaa !2
  %9 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc29, %entry
  %10 = load i32, i32* %y, align 4, !tbaa !6
  %11 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %12 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  br label %for.end31

for.body:                                         ; preds = %for.cond
  %13 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc26, %for.body
  %14 = load i32, i32* %x, align 4, !tbaa !6
  %15 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp3 = icmp slt i32 %14, %15
  br i1 %cmp3, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %16 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  br label %for.end28

for.body6:                                        ; preds = %for.cond2
  %17 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  store i32 0, i32* %res, align 4, !tbaa !6
  %18 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body6
  %19 = load i32, i32* %k, align 4, !tbaa !6
  %20 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps8 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %20, i32 0, i32 1
  %21 = load i16, i16* %taps8, align 4, !tbaa !13
  %conv9 = zext i16 %21 to i32
  %cmp10 = icmp slt i32 %19, %conv9
  br i1 %cmp10, label %for.body13, label %for.cond.cleanup12

for.cond.cleanup12:                               ; preds = %for.cond7
  store i32 8, i32* %cleanup.dest.slot, align 4
  %22 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  br label %for.end

for.body13:                                       ; preds = %for.cond7
  %23 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %24 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %23, i32 %24
  %25 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv14 = sext i16 %25 to i32
  %26 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %28 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub15 = sub nsw i32 %27, %28
  %29 = load i32, i32* %k, align 4, !tbaa !6
  %add = add nsw i32 %sub15, %29
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add, %30
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %add16 = add nsw i32 %mul, %31
  %arrayidx17 = getelementptr inbounds i16, i16* %26, i32 %add16
  %32 = load i16, i16* %arrayidx17, align 2, !tbaa !9
  %conv18 = zext i16 %32 to i32
  %mul19 = mul nsw i32 %conv14, %conv18
  %33 = load i32, i32* %res, align 4, !tbaa !6
  %add20 = add nsw i32 %33, %mul19
  store i32 %add20, i32* %res, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %34 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond.cleanup12
  %35 = load i32, i32* %res, align 4, !tbaa !6
  %add21 = add nsw i32 %35, 64
  %shr = ashr i32 %add21, 7
  %36 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call22 = call zeroext i16 @clip_pixel_highbd(i32 %shr, i32 %36)
  %37 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %38 = load i32, i32* %y, align 4, !tbaa !6
  %39 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul23 = mul nsw i32 %38, %39
  %40 = load i32, i32* %x, align 4, !tbaa !6
  %add24 = add nsw i32 %mul23, %40
  %arrayidx25 = getelementptr inbounds i16, i16* %37, i32 %add24
  store i16 %call22, i16* %arrayidx25, align 2, !tbaa !9
  %41 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  br label %for.inc26

for.inc26:                                        ; preds = %for.end
  %42 = load i32, i32* %x, align 4, !tbaa !6
  %inc27 = add nsw i32 %42, 1
  store i32 %inc27, i32* %x, align 4, !tbaa !6
  br label %for.cond2

for.end28:                                        ; preds = %for.cond.cleanup5
  br label %for.inc29

for.inc29:                                        ; preds = %for.end28
  %43 = load i32, i32* %y, align 4, !tbaa !6
  %inc30 = add nsw i32 %43, 1
  store i32 %inc30, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end31:                                        ; preds = %for.cond.cleanup
  %44 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_convolve_2d_sr_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %im_block = alloca [17280 x i16], align 16
  %im_h = alloca i32, align 4
  %im_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %bits = alloca i32, align 4
  %src_horiz = alloca i16*, align 4
  %x_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  %src_vert = alloca i16*, align 4
  %y_filter = alloca i16*, align 4
  %offset_bits = alloca i32, align 4
  %y56 = alloca i32, align 4
  %x62 = alloca i32, align 4
  %sum68 = alloca i32, align 4
  %k70 = alloca i32, align 4
  %res = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 34560, i8* %0) #4
  %1 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %h.addr, align 4, !tbaa !6
  %3 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %3, i32 0, i32 1
  %4 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %4 to i32
  %add = add nsw i32 %2, %conv
  %sub = sub nsw i32 %add, 1
  store i32 %sub, i32* %im_h, align 4, !tbaa !6
  %5 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %w.addr, align 4, !tbaa !6
  store i32 %6, i32* %im_stride, align 4, !tbaa !6
  %7 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps1 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %8, i32 0, i32 1
  %9 = load i16, i16* %taps1, align 4, !tbaa !13
  %conv2 = zext i16 %9 to i32
  %div = sdiv i32 %conv2, 2
  %sub3 = sub nsw i32 %div, 1
  store i32 %sub3, i32* %fo_vert, align 4, !tbaa !6
  %10 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps4 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %11, i32 0, i32 1
  %12 = load i16, i16* %taps4, align 4, !tbaa !13
  %conv5 = zext i16 %12 to i32
  %div6 = sdiv i32 %conv5, 2
  %sub7 = sub nsw i32 %div6, 1
  store i32 %sub7, i32* %fo_horiz, align 4, !tbaa !6
  %13 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %14, i32 0, i32 3
  %15 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub8 = sub nsw i32 14, %15
  %16 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %16, i32 0, i32 4
  %17 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub9 = sub nsw i32 %sub8, %17
  store i32 %sub9, i32* %bits, align 4, !tbaa !6
  %18 = bitcast i16** %src_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %20 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %21 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %20, %21
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i16, i16* %19, i32 %idx.neg
  store i16* %add.ptr, i16** %src_horiz, align 4, !tbaa !2
  %22 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %24 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %and = and i32 %24, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %23, i32 %and)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %25 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc46, %entry
  %26 = load i32, i32* %y, align 4, !tbaa !6
  %27 = load i32, i32* %im_h, align 4, !tbaa !6
  %cmp = icmp slt i32 %26, %27
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  br label %for.end48

for.body:                                         ; preds = %for.cond
  %29 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc43, %for.body
  %30 = load i32, i32* %x, align 4, !tbaa !6
  %31 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp12 = icmp slt i32 %30, %31
  br i1 %cmp12, label %for.body15, label %for.cond.cleanup14

for.cond.cleanup14:                               ; preds = %for.cond11
  store i32 5, i32* %cleanup.dest.slot, align 4
  %32 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #4
  br label %for.end45

for.body15:                                       ; preds = %for.cond11
  %33 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add16 = add nsw i32 %34, 7
  %sub17 = sub nsw i32 %add16, 1
  %shl = shl i32 1, %sub17
  store i32 %shl, i32* %sum, align 4, !tbaa !6
  %35 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %for.body15
  %36 = load i32, i32* %k, align 4, !tbaa !6
  %37 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps19 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %37, i32 0, i32 1
  %38 = load i16, i16* %taps19, align 4, !tbaa !13
  %conv20 = zext i16 %38 to i32
  %cmp21 = icmp slt i32 %36, %conv20
  br i1 %cmp21, label %for.body24, label %for.cond.cleanup23

for.cond.cleanup23:                               ; preds = %for.cond18
  store i32 8, i32* %cleanup.dest.slot, align 4
  %39 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  br label %for.end

for.body24:                                       ; preds = %for.cond18
  %40 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %41 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %40, i32 %41
  %42 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv25 = sext i16 %42 to i32
  %43 = load i16*, i16** %src_horiz, align 4, !tbaa !2
  %44 = load i32, i32* %y, align 4, !tbaa !6
  %45 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %44, %45
  %46 = load i32, i32* %x, align 4, !tbaa !6
  %add27 = add nsw i32 %mul26, %46
  %47 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub28 = sub nsw i32 %add27, %47
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %add29 = add nsw i32 %sub28, %48
  %arrayidx30 = getelementptr inbounds i16, i16* %43, i32 %add29
  %49 = load i16, i16* %arrayidx30, align 2, !tbaa !9
  %conv31 = zext i16 %49 to i32
  %mul32 = mul nsw i32 %conv25, %conv31
  %50 = load i32, i32* %sum, align 4, !tbaa !6
  %add33 = add nsw i32 %50, %mul32
  store i32 %add33, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body24
  %51 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond18

for.end:                                          ; preds = %for.cond.cleanup23
  %52 = load i32, i32* %sum, align 4, !tbaa !6
  %53 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_034 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %53, i32 0, i32 3
  %54 = load i32, i32* %round_034, align 4, !tbaa !15
  %shl35 = shl i32 1, %54
  %shr = ashr i32 %shl35, 1
  %add36 = add nsw i32 %52, %shr
  %55 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_037 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %55, i32 0, i32 3
  %56 = load i32, i32* %round_037, align 4, !tbaa !15
  %shr38 = ashr i32 %add36, %56
  %conv39 = trunc i32 %shr38 to i16
  %57 = load i32, i32* %y, align 4, !tbaa !6
  %58 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul40 = mul nsw i32 %57, %58
  %59 = load i32, i32* %x, align 4, !tbaa !6
  %add41 = add nsw i32 %mul40, %59
  %arrayidx42 = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 %add41
  store i16 %conv39, i16* %arrayidx42, align 2, !tbaa !9
  %60 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  br label %for.inc43

for.inc43:                                        ; preds = %for.end
  %61 = load i32, i32* %x, align 4, !tbaa !6
  %inc44 = add nsw i32 %61, 1
  store i32 %inc44, i32* %x, align 4, !tbaa !6
  br label %for.cond11

for.end45:                                        ; preds = %for.cond.cleanup14
  br label %for.inc46

for.inc46:                                        ; preds = %for.end45
  %62 = load i32, i32* %y, align 4, !tbaa !6
  %inc47 = add nsw i32 %62, 1
  store i32 %inc47, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end48:                                        ; preds = %for.cond.cleanup
  %63 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #4
  %arraydecay = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 0
  %64 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %65 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul49 = mul nsw i32 %64, %65
  %add.ptr50 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul49
  store i16* %add.ptr50, i16** %src_vert, align 4, !tbaa !2
  %66 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #4
  %67 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %68 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %and51 = and i32 %68, 15
  %call52 = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %67, i32 %and51)
  store i16* %call52, i16** %y_filter, align 4, !tbaa !2
  %69 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %70 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add53 = add nsw i32 %70, 14
  %71 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_054 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %71, i32 0, i32 3
  %72 = load i32, i32* %round_054, align 4, !tbaa !15
  %sub55 = sub nsw i32 %add53, %72
  store i32 %sub55, i32* %offset_bits, align 4, !tbaa !6
  %73 = bitcast i32* %y56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #4
  store i32 0, i32* %y56, align 4, !tbaa !6
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc117, %for.end48
  %74 = load i32, i32* %y56, align 4, !tbaa !6
  %75 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp58 = icmp slt i32 %74, %75
  br i1 %cmp58, label %for.body61, label %for.cond.cleanup60

for.cond.cleanup60:                               ; preds = %for.cond57
  store i32 11, i32* %cleanup.dest.slot, align 4
  %76 = bitcast i32* %y56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  br label %for.end119

for.body61:                                       ; preds = %for.cond57
  %77 = bitcast i32* %x62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #4
  store i32 0, i32* %x62, align 4, !tbaa !6
  br label %for.cond63

for.cond63:                                       ; preds = %for.inc114, %for.body61
  %78 = load i32, i32* %x62, align 4, !tbaa !6
  %79 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp64 = icmp slt i32 %78, %79
  br i1 %cmp64, label %for.body67, label %for.cond.cleanup66

for.cond.cleanup66:                               ; preds = %for.cond63
  store i32 14, i32* %cleanup.dest.slot, align 4
  %80 = bitcast i32* %x62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  br label %for.end116

for.body67:                                       ; preds = %for.cond63
  %81 = bitcast i32* %sum68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #4
  %82 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %shl69 = shl i32 1, %82
  store i32 %shl69, i32* %sum68, align 4, !tbaa !6
  %83 = bitcast i32* %k70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #4
  store i32 0, i32* %k70, align 4, !tbaa !6
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc88, %for.body67
  %84 = load i32, i32* %k70, align 4, !tbaa !6
  %85 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps72 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %85, i32 0, i32 1
  %86 = load i16, i16* %taps72, align 4, !tbaa !13
  %conv73 = zext i16 %86 to i32
  %cmp74 = icmp slt i32 %84, %conv73
  br i1 %cmp74, label %for.body77, label %for.cond.cleanup76

for.cond.cleanup76:                               ; preds = %for.cond71
  store i32 17, i32* %cleanup.dest.slot, align 4
  %87 = bitcast i32* %k70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  br label %for.end90

for.body77:                                       ; preds = %for.cond71
  %88 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %89 = load i32, i32* %k70, align 4, !tbaa !6
  %arrayidx78 = getelementptr inbounds i16, i16* %88, i32 %89
  %90 = load i16, i16* %arrayidx78, align 2, !tbaa !9
  %conv79 = sext i16 %90 to i32
  %91 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %92 = load i32, i32* %y56, align 4, !tbaa !6
  %93 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub80 = sub nsw i32 %92, %93
  %94 = load i32, i32* %k70, align 4, !tbaa !6
  %add81 = add nsw i32 %sub80, %94
  %95 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul82 = mul nsw i32 %add81, %95
  %96 = load i32, i32* %x62, align 4, !tbaa !6
  %add83 = add nsw i32 %mul82, %96
  %arrayidx84 = getelementptr inbounds i16, i16* %91, i32 %add83
  %97 = load i16, i16* %arrayidx84, align 2, !tbaa !9
  %conv85 = sext i16 %97 to i32
  %mul86 = mul nsw i32 %conv79, %conv85
  %98 = load i32, i32* %sum68, align 4, !tbaa !6
  %add87 = add nsw i32 %98, %mul86
  store i32 %add87, i32* %sum68, align 4, !tbaa !6
  br label %for.inc88

for.inc88:                                        ; preds = %for.body77
  %99 = load i32, i32* %k70, align 4, !tbaa !6
  %inc89 = add nsw i32 %99, 1
  store i32 %inc89, i32* %k70, align 4, !tbaa !6
  br label %for.cond71

for.end90:                                        ; preds = %for.cond.cleanup76
  %100 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #4
  %101 = load i32, i32* %sum68, align 4, !tbaa !6
  %102 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_191 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %102, i32 0, i32 4
  %103 = load i32, i32* %round_191, align 4, !tbaa !17
  %shl92 = shl i32 1, %103
  %shr93 = ashr i32 %shl92, 1
  %add94 = add nsw i32 %101, %shr93
  %104 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_195 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %104, i32 0, i32 4
  %105 = load i32, i32* %round_195, align 4, !tbaa !17
  %shr96 = ashr i32 %add94, %105
  %106 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %107 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_197 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %107, i32 0, i32 4
  %108 = load i32, i32* %round_197, align 4, !tbaa !17
  %sub98 = sub nsw i32 %106, %108
  %shl99 = shl i32 1, %sub98
  %109 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %110 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1100 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %110, i32 0, i32 4
  %111 = load i32, i32* %round_1100, align 4, !tbaa !17
  %sub101 = sub nsw i32 %109, %111
  %sub102 = sub nsw i32 %sub101, 1
  %shl103 = shl i32 1, %sub102
  %add104 = add nsw i32 %shl99, %shl103
  %sub105 = sub nsw i32 %shr96, %add104
  store i32 %sub105, i32* %res, align 4, !tbaa !6
  %112 = load i32, i32* %res, align 4, !tbaa !6
  %113 = load i32, i32* %bits, align 4, !tbaa !6
  %shl106 = shl i32 1, %113
  %shr107 = ashr i32 %shl106, 1
  %add108 = add nsw i32 %112, %shr107
  %114 = load i32, i32* %bits, align 4, !tbaa !6
  %shr109 = ashr i32 %add108, %114
  %115 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call110 = call zeroext i16 @clip_pixel_highbd(i32 %shr109, i32 %115)
  %116 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %117 = load i32, i32* %y56, align 4, !tbaa !6
  %118 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul111 = mul nsw i32 %117, %118
  %119 = load i32, i32* %x62, align 4, !tbaa !6
  %add112 = add nsw i32 %mul111, %119
  %arrayidx113 = getelementptr inbounds i16, i16* %116, i32 %add112
  store i16 %call110, i16* %arrayidx113, align 2, !tbaa !9
  %120 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #4
  %121 = bitcast i32* %sum68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #4
  br label %for.inc114

for.inc114:                                       ; preds = %for.end90
  %122 = load i32, i32* %x62, align 4, !tbaa !6
  %inc115 = add nsw i32 %122, 1
  store i32 %inc115, i32* %x62, align 4, !tbaa !6
  br label %for.cond63

for.end116:                                       ; preds = %for.cond.cleanup66
  br label %for.inc117

for.inc117:                                       ; preds = %for.end116
  %123 = load i32, i32* %y56, align 4, !tbaa !6
  %inc118 = add nsw i32 %123, 1
  store i32 %inc118, i32* %y56, align 4, !tbaa !6
  br label %for.cond57

for.end119:                                       ; preds = %for.cond.cleanup60
  %124 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #4
  %125 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #4
  %126 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #4
  %127 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #4
  %128 = bitcast i16** %src_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #4
  %129 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #4
  %130 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #4
  %131 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #4
  %132 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #4
  %133 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #4
  %134 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 34560, i8* %134) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_dist_wtd_convolve_2d_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %k = alloca i32, align 4
  %im_block = alloca [17280 x i16], align 16
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %im_h = alloca i32, align 4
  %im_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %src_horiz = alloca i16*, align 4
  %x_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  %src_vert = alloca i16*, align 4
  %offset_bits = alloca i32, align 4
  %y_filter = alloca i16*, align 4
  %sum64 = alloca i32, align 4
  %res = alloca i16, align 2
  %tmp = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 34560, i8* %3) #4
  %4 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %5, i32 0, i32 1
  %6 = load i16*, i16** %dst1, align 4, !tbaa !19
  store i16* %6, i16** %dst16, align 4, !tbaa !2
  %7 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %8, i32 0, i32 2
  %9 = load i32, i32* %dst_stride2, align 4, !tbaa !20
  store i32 %9, i32* %dst16_stride, align 4, !tbaa !6
  %10 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load i32, i32* %h.addr, align 4, !tbaa !6
  %12 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %12, i32 0, i32 1
  %13 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %13 to i32
  %add = add nsw i32 %11, %conv
  %sub = sub nsw i32 %add, 1
  store i32 %sub, i32* %im_h, align 4, !tbaa !6
  %14 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load i32, i32* %w.addr, align 4, !tbaa !6
  store i32 %15, i32* %im_stride, align 4, !tbaa !6
  %16 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps3 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %17, i32 0, i32 1
  %18 = load i16, i16* %taps3, align 4, !tbaa !13
  %conv4 = zext i16 %18 to i32
  %div = sdiv i32 %conv4, 2
  %sub5 = sub nsw i32 %div, 1
  store i32 %sub5, i32* %fo_vert, align 4, !tbaa !6
  %19 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps6 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %20, i32 0, i32 1
  %21 = load i16, i16* %taps6, align 4, !tbaa !13
  %conv7 = zext i16 %21 to i32
  %div8 = sdiv i32 %conv7, 2
  %sub9 = sub nsw i32 %div8, 1
  store i32 %sub9, i32* %fo_horiz, align 4, !tbaa !6
  %22 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %23, i32 0, i32 3
  %24 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub10 = sub nsw i32 14, %24
  %25 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %25, i32 0, i32 4
  %26 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub11 = sub nsw i32 %sub10, %26
  store i32 %sub11, i32* %round_bits, align 4, !tbaa !6
  %27 = bitcast i16** %src_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %29 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %30 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %29, %30
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i16, i16* %28, i32 %idx.neg
  store i16* %add.ptr, i16** %src_horiz, align 4, !tbaa !2
  %31 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %33 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %and = and i32 %33, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %32, i32 %and)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc46, %entry
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %35 = load i32, i32* %im_h, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.end48

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc43, %for.body
  %36 = load i32, i32* %x, align 4, !tbaa !6
  %37 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %36, %37
  br i1 %cmp14, label %for.body16, label %for.end45

for.body16:                                       ; preds = %for.cond13
  %38 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #4
  %39 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add17 = add nsw i32 %39, 7
  %sub18 = sub nsw i32 %add17, 1
  %shl = shl i32 1, %sub18
  store i32 %shl, i32* %sum, align 4, !tbaa !6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc, %for.body16
  %40 = load i32, i32* %k, align 4, !tbaa !6
  %41 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps20 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %41, i32 0, i32 1
  %42 = load i16, i16* %taps20, align 4, !tbaa !13
  %conv21 = zext i16 %42 to i32
  %cmp22 = icmp slt i32 %40, %conv21
  br i1 %cmp22, label %for.body24, label %for.end

for.body24:                                       ; preds = %for.cond19
  %43 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %44 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %43, i32 %44
  %45 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv25 = sext i16 %45 to i32
  %46 = load i16*, i16** %src_horiz, align 4, !tbaa !2
  %47 = load i32, i32* %y, align 4, !tbaa !6
  %48 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul26 = mul nsw i32 %47, %48
  %49 = load i32, i32* %x, align 4, !tbaa !6
  %add27 = add nsw i32 %mul26, %49
  %50 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub28 = sub nsw i32 %add27, %50
  %51 = load i32, i32* %k, align 4, !tbaa !6
  %add29 = add nsw i32 %sub28, %51
  %arrayidx30 = getelementptr inbounds i16, i16* %46, i32 %add29
  %52 = load i16, i16* %arrayidx30, align 2, !tbaa !9
  %conv31 = zext i16 %52 to i32
  %mul32 = mul nsw i32 %conv25, %conv31
  %53 = load i32, i32* %sum, align 4, !tbaa !6
  %add33 = add nsw i32 %53, %mul32
  store i32 %add33, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body24
  %54 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond19

for.end:                                          ; preds = %for.cond19
  %55 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %56 = load i32, i32* %sum, align 4, !tbaa !6
  %57 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_034 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %57, i32 0, i32 3
  %58 = load i32, i32* %round_034, align 4, !tbaa !15
  %shl35 = shl i32 1, %58
  %shr = ashr i32 %shl35, 1
  %add36 = add nsw i32 %56, %shr
  %59 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_037 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %59, i32 0, i32 3
  %60 = load i32, i32* %round_037, align 4, !tbaa !15
  %shr38 = ashr i32 %add36, %60
  %conv39 = trunc i32 %shr38 to i16
  %61 = load i32, i32* %y, align 4, !tbaa !6
  %62 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul40 = mul nsw i32 %61, %62
  %63 = load i32, i32* %x, align 4, !tbaa !6
  %add41 = add nsw i32 %mul40, %63
  %arrayidx42 = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 %add41
  store i16 %conv39, i16* %arrayidx42, align 2, !tbaa !9
  %64 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #4
  br label %for.inc43

for.inc43:                                        ; preds = %for.end
  %65 = load i32, i32* %x, align 4, !tbaa !6
  %inc44 = add nsw i32 %65, 1
  store i32 %inc44, i32* %x, align 4, !tbaa !6
  br label %for.cond13

for.end45:                                        ; preds = %for.cond13
  br label %for.inc46

for.inc46:                                        ; preds = %for.end45
  %66 = load i32, i32* %y, align 4, !tbaa !6
  %inc47 = add nsw i32 %66, 1
  store i32 %inc47, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end48:                                        ; preds = %for.cond
  %67 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #4
  %arraydecay = getelementptr inbounds [17280 x i16], [17280 x i16]* %im_block, i32 0, i32 0
  %68 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %69 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul49 = mul nsw i32 %68, %69
  %add.ptr50 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul49
  store i16* %add.ptr50, i16** %src_vert, align 4, !tbaa !2
  %70 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #4
  %71 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add51 = add nsw i32 %71, 14
  %72 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_052 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %72, i32 0, i32 3
  %73 = load i32, i32* %round_052, align 4, !tbaa !15
  %sub53 = sub nsw i32 %add51, %73
  store i32 %sub53, i32* %offset_bits, align 4, !tbaa !6
  %74 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #4
  %75 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %76 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %and54 = and i32 %76, 15
  %call55 = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %75, i32 %and54)
  store i16* %call55, i16** %y_filter, align 4, !tbaa !2
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc131, %for.end48
  %77 = load i32, i32* %y, align 4, !tbaa !6
  %78 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp57 = icmp slt i32 %77, %78
  br i1 %cmp57, label %for.body59, label %for.end133

for.body59:                                       ; preds = %for.cond56
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc128, %for.body59
  %79 = load i32, i32* %x, align 4, !tbaa !6
  %80 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp61 = icmp slt i32 %79, %80
  br i1 %cmp61, label %for.body63, label %for.end130

for.body63:                                       ; preds = %for.cond60
  %81 = bitcast i32* %sum64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #4
  %82 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %shl65 = shl i32 1, %82
  store i32 %shl65, i32* %sum64, align 4, !tbaa !6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc82, %for.body63
  %83 = load i32, i32* %k, align 4, !tbaa !6
  %84 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps67 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %84, i32 0, i32 1
  %85 = load i16, i16* %taps67, align 4, !tbaa !13
  %conv68 = zext i16 %85 to i32
  %cmp69 = icmp slt i32 %83, %conv68
  br i1 %cmp69, label %for.body71, label %for.end84

for.body71:                                       ; preds = %for.cond66
  %86 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %87 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx72 = getelementptr inbounds i16, i16* %86, i32 %87
  %88 = load i16, i16* %arrayidx72, align 2, !tbaa !9
  %conv73 = sext i16 %88 to i32
  %89 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %90 = load i32, i32* %y, align 4, !tbaa !6
  %91 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub74 = sub nsw i32 %90, %91
  %92 = load i32, i32* %k, align 4, !tbaa !6
  %add75 = add nsw i32 %sub74, %92
  %93 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul76 = mul nsw i32 %add75, %93
  %94 = load i32, i32* %x, align 4, !tbaa !6
  %add77 = add nsw i32 %mul76, %94
  %arrayidx78 = getelementptr inbounds i16, i16* %89, i32 %add77
  %95 = load i16, i16* %arrayidx78, align 2, !tbaa !9
  %conv79 = sext i16 %95 to i32
  %mul80 = mul nsw i32 %conv73, %conv79
  %96 = load i32, i32* %sum64, align 4, !tbaa !6
  %add81 = add nsw i32 %96, %mul80
  store i32 %add81, i32* %sum64, align 4, !tbaa !6
  br label %for.inc82

for.inc82:                                        ; preds = %for.body71
  %97 = load i32, i32* %k, align 4, !tbaa !6
  %inc83 = add nsw i32 %97, 1
  store i32 %inc83, i32* %k, align 4, !tbaa !6
  br label %for.cond66

for.end84:                                        ; preds = %for.cond66
  %98 = bitcast i16* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %98) #4
  %99 = load i32, i32* %sum64, align 4, !tbaa !6
  %100 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_185 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %100, i32 0, i32 4
  %101 = load i32, i32* %round_185, align 4, !tbaa !17
  %shl86 = shl i32 1, %101
  %shr87 = ashr i32 %shl86, 1
  %add88 = add nsw i32 %99, %shr87
  %102 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_189 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %102, i32 0, i32 4
  %103 = load i32, i32* %round_189, align 4, !tbaa !17
  %shr90 = ashr i32 %add88, %103
  %conv91 = trunc i32 %shr90 to i16
  store i16 %conv91, i16* %res, align 2, !tbaa !9
  %104 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %104, i32 0, i32 0
  %105 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool = icmp ne i32 %105, 0
  br i1 %tobool, label %if.then, label %if.else123

if.then:                                          ; preds = %for.end84
  %106 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #4
  %107 = load i16*, i16** %dst16, align 4, !tbaa !2
  %108 = load i32, i32* %y, align 4, !tbaa !6
  %109 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul92 = mul nsw i32 %108, %109
  %110 = load i32, i32* %x, align 4, !tbaa !6
  %add93 = add nsw i32 %mul92, %110
  %arrayidx94 = getelementptr inbounds i16, i16* %107, i32 %add93
  %111 = load i16, i16* %arrayidx94, align 2, !tbaa !9
  %conv95 = zext i16 %111 to i32
  store i32 %conv95, i32* %tmp, align 4, !tbaa !6
  %112 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %112, i32 0, i32 8
  %113 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool96 = icmp ne i32 %113, 0
  br i1 %tobool96, label %if.then97, label %if.else

if.then97:                                        ; preds = %if.then
  %114 = load i32, i32* %tmp, align 4, !tbaa !6
  %115 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %115, i32 0, i32 9
  %116 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul98 = mul nsw i32 %114, %116
  %117 = load i16, i16* %res, align 2, !tbaa !9
  %conv99 = zext i16 %117 to i32
  %118 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %118, i32 0, i32 10
  %119 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul100 = mul nsw i32 %conv99, %119
  %add101 = add nsw i32 %mul98, %mul100
  store i32 %add101, i32* %tmp, align 4, !tbaa !6
  %120 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr102 = ashr i32 %120, 4
  store i32 %shr102, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  %121 = load i16, i16* %res, align 2, !tbaa !9
  %conv103 = zext i16 %121 to i32
  %122 = load i32, i32* %tmp, align 4, !tbaa !6
  %add104 = add nsw i32 %122, %conv103
  store i32 %add104, i32* %tmp, align 4, !tbaa !6
  %123 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr105 = ashr i32 %123, 1
  store i32 %shr105, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then97
  %124 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %125 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1106 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %125, i32 0, i32 4
  %126 = load i32, i32* %round_1106, align 4, !tbaa !17
  %sub107 = sub nsw i32 %124, %126
  %shl108 = shl i32 1, %sub107
  %127 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %128 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1109 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %128, i32 0, i32 4
  %129 = load i32, i32* %round_1109, align 4, !tbaa !17
  %sub110 = sub nsw i32 %127, %129
  %sub111 = sub nsw i32 %sub110, 1
  %shl112 = shl i32 1, %sub111
  %add113 = add nsw i32 %shl108, %shl112
  %130 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub114 = sub nsw i32 %130, %add113
  store i32 %sub114, i32* %tmp, align 4, !tbaa !6
  %131 = load i32, i32* %tmp, align 4, !tbaa !6
  %132 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl115 = shl i32 1, %132
  %shr116 = ashr i32 %shl115, 1
  %add117 = add nsw i32 %131, %shr116
  %133 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr118 = ashr i32 %add117, %133
  %134 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call119 = call zeroext i16 @clip_pixel_highbd(i32 %shr118, i32 %134)
  %135 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %136 = load i32, i32* %y, align 4, !tbaa !6
  %137 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul120 = mul nsw i32 %136, %137
  %138 = load i32, i32* %x, align 4, !tbaa !6
  %add121 = add nsw i32 %mul120, %138
  %arrayidx122 = getelementptr inbounds i16, i16* %135, i32 %add121
  store i16 %call119, i16* %arrayidx122, align 2, !tbaa !9
  %139 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  br label %if.end127

if.else123:                                       ; preds = %for.end84
  %140 = load i16, i16* %res, align 2, !tbaa !9
  %141 = load i16*, i16** %dst16, align 4, !tbaa !2
  %142 = load i32, i32* %y, align 4, !tbaa !6
  %143 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul124 = mul nsw i32 %142, %143
  %144 = load i32, i32* %x, align 4, !tbaa !6
  %add125 = add nsw i32 %mul124, %144
  %arrayidx126 = getelementptr inbounds i16, i16* %141, i32 %add125
  store i16 %140, i16* %arrayidx126, align 2, !tbaa !9
  br label %if.end127

if.end127:                                        ; preds = %if.else123, %if.end
  %145 = bitcast i16* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %145) #4
  %146 = bitcast i32* %sum64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  br label %for.inc128

for.inc128:                                       ; preds = %if.end127
  %147 = load i32, i32* %x, align 4, !tbaa !6
  %inc129 = add nsw i32 %147, 1
  store i32 %inc129, i32* %x, align 4, !tbaa !6
  br label %for.cond60

for.end130:                                       ; preds = %for.cond60
  br label %for.inc131

for.inc131:                                       ; preds = %for.end130
  %148 = load i32, i32* %y, align 4, !tbaa !6
  %inc132 = add nsw i32 %148, 1
  store i32 %inc132, i32* %y, align 4, !tbaa !6
  br label %for.cond56

for.end133:                                       ; preds = %for.cond56
  %149 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  %152 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %153 = bitcast i16** %src_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  %154 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  %155 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #4
  %156 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #4
  %157 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %158 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  %159 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #4
  %160 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #4
  %161 = bitcast [17280 x i16]* %im_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 34560, i8* %161) #4
  %162 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #4
  %163 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #4
  %164 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_dist_wtd_convolve_x_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %bits = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %round_offset = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %x_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i32, align 4
  %k = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %1, i32 0, i32 1
  %2 = load i16*, i16** %dst1, align 4, !tbaa !19
  store i16* %2, i16** %dst16, align 4, !tbaa !2
  %3 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 2
  %5 = load i32, i32* %dst_stride2, align 4, !tbaa !20
  store i32 %5, i32* %dst16_stride, align 4, !tbaa !6
  %6 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %7, i32 0, i32 1
  %8 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %8 to i32
  %div = sdiv i32 %conv, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %fo_horiz, align 4, !tbaa !6
  %9 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %10, i32 0, i32 4
  %11 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub3 = sub nsw i32 7, %11
  store i32 %sub3, i32* %bits, align 4, !tbaa !6
  %12 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %13, 14
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %14, i32 0, i32 3
  %15 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub4 = sub nsw i32 %add, %15
  store i32 %sub4, i32* %offset_bits, align 4, !tbaa !6
  %16 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %18 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_15 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %18, i32 0, i32 4
  %19 = load i32, i32* %round_15, align 4, !tbaa !17
  %sub6 = sub nsw i32 %17, %19
  %shl = shl i32 1, %sub6
  %20 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %21 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_17 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %21, i32 0, i32 4
  %22 = load i32, i32* %round_17, align 4, !tbaa !17
  %sub8 = sub nsw i32 %20, %22
  %sub9 = sub nsw i32 %sub8, 1
  %shl10 = shl i32 1, %sub9
  %add11 = add nsw i32 %shl, %shl10
  store i32 %add11, i32* %round_offset, align 4, !tbaa !6
  %23 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_012 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %24, i32 0, i32 3
  %25 = load i32, i32* %round_012, align 4, !tbaa !15
  %sub13 = sub nsw i32 14, %25
  %26 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_114 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %26, i32 0, i32 4
  %27 = load i32, i32* %round_114, align 4, !tbaa !17
  %sub15 = sub nsw i32 %sub13, %27
  store i32 %sub15, i32* %round_bits, align 4, !tbaa !6
  %28 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %29 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %30 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %32 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %and = and i32 %32, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %31, i32 %and)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %33 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %entry
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %35 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  br label %for.end77

for.body:                                         ; preds = %for.cond
  %37 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc72, %for.body
  %38 = load i32, i32* %x, align 4, !tbaa !6
  %39 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp18 = icmp slt i32 %38, %39
  br i1 %cmp18, label %for.body21, label %for.cond.cleanup20

for.cond.cleanup20:                               ; preds = %for.cond17
  store i32 5, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.end74

for.body21:                                       ; preds = %for.cond17
  %41 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  store i32 0, i32* %res, align 4, !tbaa !6
  %42 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %for.body21
  %43 = load i32, i32* %k, align 4, !tbaa !6
  %44 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps23 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %44, i32 0, i32 1
  %45 = load i16, i16* %taps23, align 4, !tbaa !13
  %conv24 = zext i16 %45 to i32
  %cmp25 = icmp slt i32 %43, %conv24
  br i1 %cmp25, label %for.body28, label %for.cond.cleanup27

for.cond.cleanup27:                               ; preds = %for.cond22
  store i32 8, i32* %cleanup.dest.slot, align 4
  %46 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %for.end

for.body28:                                       ; preds = %for.cond22
  %47 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %47, i32 %48
  %49 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv29 = sext i16 %49 to i32
  %50 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %51 = load i32, i32* %y, align 4, !tbaa !6
  %52 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %51, %52
  %53 = load i32, i32* %x, align 4, !tbaa !6
  %add30 = add nsw i32 %mul, %53
  %54 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub31 = sub nsw i32 %add30, %54
  %55 = load i32, i32* %k, align 4, !tbaa !6
  %add32 = add nsw i32 %sub31, %55
  %arrayidx33 = getelementptr inbounds i16, i16* %50, i32 %add32
  %56 = load i16, i16* %arrayidx33, align 2, !tbaa !9
  %conv34 = zext i16 %56 to i32
  %mul35 = mul nsw i32 %conv29, %conv34
  %57 = load i32, i32* %res, align 4, !tbaa !6
  %add36 = add nsw i32 %57, %mul35
  store i32 %add36, i32* %res, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body28
  %58 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond22

for.end:                                          ; preds = %for.cond.cleanup27
  %59 = load i32, i32* %bits, align 4, !tbaa !6
  %shl37 = shl i32 1, %59
  %60 = load i32, i32* %res, align 4, !tbaa !6
  %61 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_038 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %61, i32 0, i32 3
  %62 = load i32, i32* %round_038, align 4, !tbaa !15
  %shl39 = shl i32 1, %62
  %shr = ashr i32 %shl39, 1
  %add40 = add nsw i32 %60, %shr
  %63 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_041 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %63, i32 0, i32 3
  %64 = load i32, i32* %round_041, align 4, !tbaa !15
  %shr42 = ashr i32 %add40, %64
  %mul43 = mul nsw i32 %shl37, %shr42
  store i32 %mul43, i32* %res, align 4, !tbaa !6
  %65 = load i32, i32* %round_offset, align 4, !tbaa !6
  %66 = load i32, i32* %res, align 4, !tbaa !6
  %add44 = add nsw i32 %66, %65
  store i32 %add44, i32* %res, align 4, !tbaa !6
  %67 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %67, i32 0, i32 0
  %68 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool = icmp ne i32 %68, 0
  br i1 %tobool, label %if.then, label %if.else66

if.then:                                          ; preds = %for.end
  %69 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %70 = load i16*, i16** %dst16, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !6
  %72 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul45 = mul nsw i32 %71, %72
  %73 = load i32, i32* %x, align 4, !tbaa !6
  %add46 = add nsw i32 %mul45, %73
  %arrayidx47 = getelementptr inbounds i16, i16* %70, i32 %add46
  %74 = load i16, i16* %arrayidx47, align 2, !tbaa !9
  %conv48 = zext i16 %74 to i32
  store i32 %conv48, i32* %tmp, align 4, !tbaa !6
  %75 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %75, i32 0, i32 8
  %76 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool49 = icmp ne i32 %76, 0
  br i1 %tobool49, label %if.then50, label %if.else

if.then50:                                        ; preds = %if.then
  %77 = load i32, i32* %tmp, align 4, !tbaa !6
  %78 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %78, i32 0, i32 9
  %79 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul51 = mul nsw i32 %77, %79
  %80 = load i32, i32* %res, align 4, !tbaa !6
  %81 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %81, i32 0, i32 10
  %82 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul52 = mul nsw i32 %80, %82
  %add53 = add nsw i32 %mul51, %mul52
  store i32 %add53, i32* %tmp, align 4, !tbaa !6
  %83 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr54 = ashr i32 %83, 4
  store i32 %shr54, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  %84 = load i32, i32* %res, align 4, !tbaa !6
  %85 = load i32, i32* %tmp, align 4, !tbaa !6
  %add55 = add nsw i32 %85, %84
  store i32 %add55, i32* %tmp, align 4, !tbaa !6
  %86 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr56 = ashr i32 %86, 1
  store i32 %shr56, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then50
  %87 = load i32, i32* %round_offset, align 4, !tbaa !6
  %88 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub57 = sub nsw i32 %88, %87
  store i32 %sub57, i32* %tmp, align 4, !tbaa !6
  %89 = load i32, i32* %tmp, align 4, !tbaa !6
  %90 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl58 = shl i32 1, %90
  %shr59 = ashr i32 %shl58, 1
  %add60 = add nsw i32 %89, %shr59
  %91 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr61 = ashr i32 %add60, %91
  %92 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call62 = call zeroext i16 @clip_pixel_highbd(i32 %shr61, i32 %92)
  %93 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %94 = load i32, i32* %y, align 4, !tbaa !6
  %95 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul63 = mul nsw i32 %94, %95
  %96 = load i32, i32* %x, align 4, !tbaa !6
  %add64 = add nsw i32 %mul63, %96
  %arrayidx65 = getelementptr inbounds i16, i16* %93, i32 %add64
  store i16 %call62, i16* %arrayidx65, align 2, !tbaa !9
  %97 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  br label %if.end71

if.else66:                                        ; preds = %for.end
  %98 = load i32, i32* %res, align 4, !tbaa !6
  %conv67 = trunc i32 %98 to i16
  %99 = load i16*, i16** %dst16, align 4, !tbaa !2
  %100 = load i32, i32* %y, align 4, !tbaa !6
  %101 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul68 = mul nsw i32 %100, %101
  %102 = load i32, i32* %x, align 4, !tbaa !6
  %add69 = add nsw i32 %mul68, %102
  %arrayidx70 = getelementptr inbounds i16, i16* %99, i32 %add69
  store i16 %conv67, i16* %arrayidx70, align 2, !tbaa !9
  br label %if.end71

if.end71:                                         ; preds = %if.else66, %if.end
  %103 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #4
  br label %for.inc72

for.inc72:                                        ; preds = %if.end71
  %104 = load i32, i32* %x, align 4, !tbaa !6
  %inc73 = add nsw i32 %104, 1
  store i32 %inc73, i32* %x, align 4, !tbaa !6
  br label %for.cond17

for.end74:                                        ; preds = %for.cond.cleanup20
  br label %for.inc75

for.inc75:                                        ; preds = %for.end74
  %105 = load i32, i32* %y, align 4, !tbaa !6
  %inc76 = add nsw i32 %105, 1
  store i32 %inc76, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end77:                                        ; preds = %for.cond.cleanup
  %106 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  %107 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  %108 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #4
  %109 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_dist_wtd_convolve_y_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %bits = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %round_offset = alloca i32, align 4
  %round_bits = alloca i32, align 4
  %y_filter = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i32, align 4
  %k = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %1, i32 0, i32 1
  %2 = load i16*, i16** %dst1, align 4, !tbaa !19
  store i16* %2, i16** %dst16, align 4, !tbaa !2
  %3 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 2
  %5 = load i32, i32* %dst_stride2, align 4, !tbaa !20
  store i32 %5, i32* %dst16_stride, align 4, !tbaa !6
  %6 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %7, i32 0, i32 1
  %8 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %8 to i32
  %div = sdiv i32 %conv, 2
  %sub = sub nsw i32 %div, 1
  store i32 %sub, i32* %fo_vert, align 4, !tbaa !6
  %9 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %10, i32 0, i32 3
  %11 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub3 = sub nsw i32 7, %11
  store i32 %sub3, i32* %bits, align 4, !tbaa !6
  %12 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %13, 14
  %14 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_04 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %14, i32 0, i32 3
  %15 = load i32, i32* %round_04, align 4, !tbaa !15
  %sub5 = sub nsw i32 %add, %15
  store i32 %sub5, i32* %offset_bits, align 4, !tbaa !6
  %16 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %18 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %18, i32 0, i32 4
  %19 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub6 = sub nsw i32 %17, %19
  %shl = shl i32 1, %sub6
  %20 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %21 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_17 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %21, i32 0, i32 4
  %22 = load i32, i32* %round_17, align 4, !tbaa !17
  %sub8 = sub nsw i32 %20, %22
  %sub9 = sub nsw i32 %sub8, 1
  %shl10 = shl i32 1, %sub9
  %add11 = add nsw i32 %shl, %shl10
  store i32 %add11, i32* %round_offset, align 4, !tbaa !6
  %23 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_012 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %24, i32 0, i32 3
  %25 = load i32, i32* %round_012, align 4, !tbaa !15
  %sub13 = sub nsw i32 14, %25
  %26 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_114 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %26, i32 0, i32 4
  %27 = load i32, i32* %round_114, align 4, !tbaa !17
  %sub15 = sub nsw i32 %sub13, %27
  store i32 %sub15, i32* %round_bits, align 4, !tbaa !6
  %28 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %29 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %30 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %32 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %and = and i32 %32, 15
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %31, i32 %and)
  store i16* %call, i16** %y_filter, align 4, !tbaa !2
  %33 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %entry
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %35 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %34, %35
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %36 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  br label %for.end77

for.body:                                         ; preds = %for.cond
  %37 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc72, %for.body
  %38 = load i32, i32* %x, align 4, !tbaa !6
  %39 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp18 = icmp slt i32 %38, %39
  br i1 %cmp18, label %for.body21, label %for.cond.cleanup20

for.cond.cleanup20:                               ; preds = %for.cond17
  store i32 5, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.end74

for.body21:                                       ; preds = %for.cond17
  %41 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  store i32 0, i32* %res, align 4, !tbaa !6
  %42 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %for.body21
  %43 = load i32, i32* %k, align 4, !tbaa !6
  %44 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps23 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %44, i32 0, i32 1
  %45 = load i16, i16* %taps23, align 4, !tbaa !13
  %conv24 = zext i16 %45 to i32
  %cmp25 = icmp slt i32 %43, %conv24
  br i1 %cmp25, label %for.body28, label %for.cond.cleanup27

for.cond.cleanup27:                               ; preds = %for.cond22
  store i32 8, i32* %cleanup.dest.slot, align 4
  %46 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %for.end

for.body28:                                       ; preds = %for.cond22
  %47 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %47, i32 %48
  %49 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv29 = sext i16 %49 to i32
  %50 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %51 = load i32, i32* %y, align 4, !tbaa !6
  %52 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub30 = sub nsw i32 %51, %52
  %53 = load i32, i32* %k, align 4, !tbaa !6
  %add31 = add nsw i32 %sub30, %53
  %54 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %add31, %54
  %55 = load i32, i32* %x, align 4, !tbaa !6
  %add32 = add nsw i32 %mul, %55
  %arrayidx33 = getelementptr inbounds i16, i16* %50, i32 %add32
  %56 = load i16, i16* %arrayidx33, align 2, !tbaa !9
  %conv34 = zext i16 %56 to i32
  %mul35 = mul nsw i32 %conv29, %conv34
  %57 = load i32, i32* %res, align 4, !tbaa !6
  %add36 = add nsw i32 %57, %mul35
  store i32 %add36, i32* %res, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body28
  %58 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond22

for.end:                                          ; preds = %for.cond.cleanup27
  %59 = load i32, i32* %bits, align 4, !tbaa !6
  %shl37 = shl i32 1, %59
  %60 = load i32, i32* %res, align 4, !tbaa !6
  %mul38 = mul nsw i32 %60, %shl37
  store i32 %mul38, i32* %res, align 4, !tbaa !6
  %61 = load i32, i32* %res, align 4, !tbaa !6
  %62 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_139 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %62, i32 0, i32 4
  %63 = load i32, i32* %round_139, align 4, !tbaa !17
  %shl40 = shl i32 1, %63
  %shr = ashr i32 %shl40, 1
  %add41 = add nsw i32 %61, %shr
  %64 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_142 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %64, i32 0, i32 4
  %65 = load i32, i32* %round_142, align 4, !tbaa !17
  %shr43 = ashr i32 %add41, %65
  %66 = load i32, i32* %round_offset, align 4, !tbaa !6
  %add44 = add nsw i32 %shr43, %66
  store i32 %add44, i32* %res, align 4, !tbaa !6
  %67 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %67, i32 0, i32 0
  %68 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool = icmp ne i32 %68, 0
  br i1 %tobool, label %if.then, label %if.else66

if.then:                                          ; preds = %for.end
  %69 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %70 = load i16*, i16** %dst16, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !6
  %72 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul45 = mul nsw i32 %71, %72
  %73 = load i32, i32* %x, align 4, !tbaa !6
  %add46 = add nsw i32 %mul45, %73
  %arrayidx47 = getelementptr inbounds i16, i16* %70, i32 %add46
  %74 = load i16, i16* %arrayidx47, align 2, !tbaa !9
  %conv48 = zext i16 %74 to i32
  store i32 %conv48, i32* %tmp, align 4, !tbaa !6
  %75 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %75, i32 0, i32 8
  %76 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool49 = icmp ne i32 %76, 0
  br i1 %tobool49, label %if.then50, label %if.else

if.then50:                                        ; preds = %if.then
  %77 = load i32, i32* %tmp, align 4, !tbaa !6
  %78 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %78, i32 0, i32 9
  %79 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul51 = mul nsw i32 %77, %79
  %80 = load i32, i32* %res, align 4, !tbaa !6
  %81 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %81, i32 0, i32 10
  %82 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul52 = mul nsw i32 %80, %82
  %add53 = add nsw i32 %mul51, %mul52
  store i32 %add53, i32* %tmp, align 4, !tbaa !6
  %83 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr54 = ashr i32 %83, 4
  store i32 %shr54, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  %84 = load i32, i32* %res, align 4, !tbaa !6
  %85 = load i32, i32* %tmp, align 4, !tbaa !6
  %add55 = add nsw i32 %85, %84
  store i32 %add55, i32* %tmp, align 4, !tbaa !6
  %86 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr56 = ashr i32 %86, 1
  store i32 %shr56, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then50
  %87 = load i32, i32* %round_offset, align 4, !tbaa !6
  %88 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub57 = sub nsw i32 %88, %87
  store i32 %sub57, i32* %tmp, align 4, !tbaa !6
  %89 = load i32, i32* %tmp, align 4, !tbaa !6
  %90 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shl58 = shl i32 1, %90
  %shr59 = ashr i32 %shl58, 1
  %add60 = add nsw i32 %89, %shr59
  %91 = load i32, i32* %round_bits, align 4, !tbaa !6
  %shr61 = ashr i32 %add60, %91
  %92 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call62 = call zeroext i16 @clip_pixel_highbd(i32 %shr61, i32 %92)
  %93 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %94 = load i32, i32* %y, align 4, !tbaa !6
  %95 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul63 = mul nsw i32 %94, %95
  %96 = load i32, i32* %x, align 4, !tbaa !6
  %add64 = add nsw i32 %mul63, %96
  %arrayidx65 = getelementptr inbounds i16, i16* %93, i32 %add64
  store i16 %call62, i16* %arrayidx65, align 2, !tbaa !9
  %97 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  br label %if.end71

if.else66:                                        ; preds = %for.end
  %98 = load i32, i32* %res, align 4, !tbaa !6
  %conv67 = trunc i32 %98 to i16
  %99 = load i16*, i16** %dst16, align 4, !tbaa !2
  %100 = load i32, i32* %y, align 4, !tbaa !6
  %101 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul68 = mul nsw i32 %100, %101
  %102 = load i32, i32* %x, align 4, !tbaa !6
  %add69 = add nsw i32 %mul68, %102
  %arrayidx70 = getelementptr inbounds i16, i16* %99, i32 %add69
  store i16 %conv67, i16* %arrayidx70, align 2, !tbaa !9
  br label %if.end71

if.end71:                                         ; preds = %if.else66, %if.end
  %103 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #4
  br label %for.inc72

for.inc72:                                        ; preds = %if.end71
  %104 = load i32, i32* %x, align 4, !tbaa !6
  %inc73 = add nsw i32 %104, 1
  store i32 %inc73, i32* %x, align 4, !tbaa !6
  br label %for.cond17

for.end74:                                        ; preds = %for.cond.cleanup20
  br label %for.inc75

for.inc75:                                        ; preds = %for.end74
  %105 = load i32, i32* %y, align 4, !tbaa !6
  %inc76 = add nsw i32 %105, 1
  store i32 %inc76, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end77:                                        ; preds = %for.cond.cleanup
  %106 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  %107 = bitcast i32* %round_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  %108 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #4
  %109 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_dist_wtd_convolve_2d_copy_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %subpel_y_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %bits = alloca i32, align 4
  %offset_bits = alloca i32, align 4
  %round_offset = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x = alloca i32, align 4
  %res = alloca i16, align 2
  %tmp = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %1, i32 0, i32 1
  %2 = load i16*, i16** %dst1, align 4, !tbaa !19
  store i16* %2, i16** %dst16, align 4, !tbaa !2
  %3 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride2 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %4, i32 0, i32 2
  %5 = load i32, i32* %dst_stride2, align 4, !tbaa !20
  store i32 %5, i32* %dst16_stride, align 4, !tbaa !6
  %6 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %7, i32 0, i32 4
  %8 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub = sub nsw i32 14, %8
  %9 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %9, i32 0, i32 3
  %10 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub3 = sub nsw i32 %sub, %10
  store i32 %sub3, i32* %bits, align 4, !tbaa !6
  %11 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %12, 14
  %13 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_04 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %13, i32 0, i32 3
  %14 = load i32, i32* %round_04, align 4, !tbaa !15
  %sub5 = sub nsw i32 %add, %14
  store i32 %sub5, i32* %offset_bits, align 4, !tbaa !6
  %15 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %17 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_16 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %17, i32 0, i32 4
  %18 = load i32, i32* %round_16, align 4, !tbaa !17
  %sub7 = sub nsw i32 %16, %18
  %shl = shl i32 1, %sub7
  %19 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %20 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_18 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %20, i32 0, i32 4
  %21 = load i32, i32* %round_18, align 4, !tbaa !17
  %sub9 = sub nsw i32 %19, %21
  %sub10 = sub nsw i32 %sub9, 1
  %shl11 = shl i32 1, %sub10
  %add12 = add nsw i32 %shl, %shl11
  store i32 %add12, i32* %round_offset, align 4, !tbaa !6
  %22 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %23 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %24 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %25 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %26 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc49, %entry
  %27 = load i32, i32* %y, align 4, !tbaa !6
  %28 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %27, %28
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %29 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  br label %for.end51

for.body:                                         ; preds = %for.cond
  %30 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc, %for.body
  %31 = load i32, i32* %x, align 4, !tbaa !6
  %32 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp14 = icmp slt i32 %31, %32
  br i1 %cmp14, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond13
  store i32 5, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  br label %for.end

for.body16:                                       ; preds = %for.cond13
  %34 = bitcast i16* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %34) #4
  %35 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %36 = load i32, i32* %y, align 4, !tbaa !6
  %37 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %36, %37
  %38 = load i32, i32* %x, align 4, !tbaa !6
  %add17 = add nsw i32 %mul, %38
  %arrayidx = getelementptr inbounds i16, i16* %35, i32 %add17
  %39 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = zext i16 %39 to i32
  %40 = load i32, i32* %bits, align 4, !tbaa !6
  %shl18 = shl i32 %conv, %40
  %conv19 = trunc i32 %shl18 to i16
  store i16 %conv19, i16* %res, align 2, !tbaa !9
  %41 = load i32, i32* %round_offset, align 4, !tbaa !6
  %42 = load i16, i16* %res, align 2, !tbaa !9
  %conv20 = zext i16 %42 to i32
  %add21 = add nsw i32 %conv20, %41
  %conv22 = trunc i32 %add21 to i16
  store i16 %conv22, i16* %res, align 2, !tbaa !9
  %43 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %43, i32 0, i32 0
  %44 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool = icmp ne i32 %44, 0
  br i1 %tobool, label %if.then, label %if.else44

if.then:                                          ; preds = %for.body16
  %45 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #4
  %46 = load i16*, i16** %dst16, align 4, !tbaa !2
  %47 = load i32, i32* %y, align 4, !tbaa !6
  %48 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul23 = mul nsw i32 %47, %48
  %49 = load i32, i32* %x, align 4, !tbaa !6
  %add24 = add nsw i32 %mul23, %49
  %arrayidx25 = getelementptr inbounds i16, i16* %46, i32 %add24
  %50 = load i16, i16* %arrayidx25, align 2, !tbaa !9
  %conv26 = zext i16 %50 to i32
  store i32 %conv26, i32* %tmp, align 4, !tbaa !6
  %51 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %51, i32 0, i32 8
  %52 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool27 = icmp ne i32 %52, 0
  br i1 %tobool27, label %if.then28, label %if.else

if.then28:                                        ; preds = %if.then
  %53 = load i32, i32* %tmp, align 4, !tbaa !6
  %54 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %54, i32 0, i32 9
  %55 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul29 = mul nsw i32 %53, %55
  %56 = load i16, i16* %res, align 2, !tbaa !9
  %conv30 = zext i16 %56 to i32
  %57 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %57, i32 0, i32 10
  %58 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul31 = mul nsw i32 %conv30, %58
  %add32 = add nsw i32 %mul29, %mul31
  store i32 %add32, i32* %tmp, align 4, !tbaa !6
  %59 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr = ashr i32 %59, 4
  store i32 %shr, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then
  %60 = load i16, i16* %res, align 2, !tbaa !9
  %conv33 = zext i16 %60 to i32
  %61 = load i32, i32* %tmp, align 4, !tbaa !6
  %add34 = add nsw i32 %61, %conv33
  store i32 %add34, i32* %tmp, align 4, !tbaa !6
  %62 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr35 = ashr i32 %62, 1
  store i32 %shr35, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then28
  %63 = load i32, i32* %round_offset, align 4, !tbaa !6
  %64 = load i32, i32* %tmp, align 4, !tbaa !6
  %sub36 = sub nsw i32 %64, %63
  store i32 %sub36, i32* %tmp, align 4, !tbaa !6
  %65 = load i32, i32* %tmp, align 4, !tbaa !6
  %66 = load i32, i32* %bits, align 4, !tbaa !6
  %shl37 = shl i32 1, %66
  %shr38 = ashr i32 %shl37, 1
  %add39 = add nsw i32 %65, %shr38
  %67 = load i32, i32* %bits, align 4, !tbaa !6
  %shr40 = ashr i32 %add39, %67
  %68 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call = call zeroext i16 @clip_pixel_highbd(i32 %shr40, i32 %68)
  %69 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %70 = load i32, i32* %y, align 4, !tbaa !6
  %71 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul41 = mul nsw i32 %70, %71
  %72 = load i32, i32* %x, align 4, !tbaa !6
  %add42 = add nsw i32 %mul41, %72
  %arrayidx43 = getelementptr inbounds i16, i16* %69, i32 %add42
  store i16 %call, i16* %arrayidx43, align 2, !tbaa !9
  %73 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  br label %if.end48

if.else44:                                        ; preds = %for.body16
  %74 = load i16, i16* %res, align 2, !tbaa !9
  %75 = load i16*, i16** %dst16, align 4, !tbaa !2
  %76 = load i32, i32* %y, align 4, !tbaa !6
  %77 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul45 = mul nsw i32 %76, %77
  %78 = load i32, i32* %x, align 4, !tbaa !6
  %add46 = add nsw i32 %mul45, %78
  %arrayidx47 = getelementptr inbounds i16, i16* %75, i32 %add46
  store i16 %74, i16* %arrayidx47, align 2, !tbaa !9
  br label %if.end48

if.end48:                                         ; preds = %if.else44, %if.end
  %79 = bitcast i16* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %79) #4
  br label %for.inc

for.inc:                                          ; preds = %if.end48
  %80 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %80, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond13

for.end:                                          ; preds = %for.cond.cleanup15
  br label %for.inc49

for.inc49:                                        ; preds = %for.end
  %81 = load i32, i32* %y, align 4, !tbaa !6
  %inc50 = add nsw i32 %81, 1
  store i32 %inc50, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end51:                                        ; preds = %for.cond.cleanup
  %82 = bitcast i32* %round_offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_convolve_2d_scale_c(i16* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams* %filter_params_y, i32 %subpel_x_qn, i32 %x_step_qn, i32 %subpel_y_qn, i32 %y_step_qn, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %filter_params_x.addr = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y.addr = alloca %struct.InterpFilterParams*, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %x_step_qn.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %y_step_qn.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %im_block = alloca [33792 x i16], align 16
  %im_h = alloca i32, align 4
  %im_stride = alloca i32, align 4
  %fo_vert = alloca i32, align 4
  %fo_horiz = alloca i32, align 4
  %dst16 = alloca i16*, align 4
  %dst16_stride = alloca i32, align 4
  %bits = alloca i32, align 4
  %src_horiz = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x_qn = alloca i32, align 4
  %x = alloca i32, align 4
  %src_x = alloca i16*, align 4
  %x_filter_idx = alloca i32, align 4
  %x_filter = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  %src_vert = alloca i16*, align 4
  %offset_bits = alloca i32, align 4
  %x61 = alloca i32, align 4
  %y_qn = alloca i32, align 4
  %y67 = alloca i32, align 4
  %src_y = alloca i16*, align 4
  %y_filter_idx = alloca i32, align 4
  %y_filter = alloca i16*, align 4
  %sum79 = alloca i32, align 4
  %k81 = alloca i32, align 4
  %res = alloca i16, align 2
  %tmp = alloca i32, align 4
  %tmp146 = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams* %filter_params_x, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  store %struct.InterpFilterParams* %filter_params_y, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %x_step_qn, i32* %x_step_qn.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store i32 %y_step_qn, i32* %y_step_qn.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [33792 x i16]* %im_block to i8*
  call void @llvm.lifetime.start.p0i8(i64 67584, i8* %0) #4
  %1 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %h.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %2, 1
  %3 = load i32, i32* %y_step_qn.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %sub, %3
  %4 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %add = add nsw i32 %mul, %4
  %shr = ashr i32 %add, 10
  %5 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %5, i32 0, i32 1
  %6 = load i16, i16* %taps, align 4, !tbaa !13
  %conv = zext i16 %6 to i32
  %add1 = add nsw i32 %shr, %conv
  store i32 %add1, i32* %im_h, align 4, !tbaa !6
  %7 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %w.addr, align 4, !tbaa !6
  store i32 %8, i32* %im_stride, align 4, !tbaa !6
  %9 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps2 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %10, i32 0, i32 1
  %11 = load i16, i16* %taps2, align 4, !tbaa !13
  %conv3 = zext i16 %11 to i32
  %div = sdiv i32 %conv3, 2
  %sub4 = sub nsw i32 %div, 1
  store i32 %sub4, i32* %fo_vert, align 4, !tbaa !6
  %12 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps5 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %13, i32 0, i32 1
  %14 = load i16, i16* %taps5, align 4, !tbaa !13
  %conv6 = zext i16 %14 to i32
  %div7 = sdiv i32 %conv6, 2
  %sub8 = sub nsw i32 %div7, 1
  store i32 %sub8, i32* %fo_horiz, align 4, !tbaa !6
  %15 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst9 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %16, i32 0, i32 1
  %17 = load i16*, i16** %dst9, align 4, !tbaa !19
  store i16* %17, i16** %dst16, align 4, !tbaa !2
  %18 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %dst_stride10 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %19, i32 0, i32 2
  %20 = load i32, i32* %dst_stride10, align 4, !tbaa !20
  store i32 %20, i32* %dst16_stride, align 4, !tbaa !6
  %21 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %22, i32 0, i32 3
  %23 = load i32, i32* %round_0, align 4, !tbaa !15
  %sub11 = sub nsw i32 14, %23
  %24 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %24, i32 0, i32 4
  %25 = load i32, i32* %round_1, align 4, !tbaa !17
  %sub12 = sub nsw i32 %sub11, %25
  store i32 %sub12, i32* %bits, align 4, !tbaa !6
  %26 = bitcast i16** %src_horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %28 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %29 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %mul13 = mul nsw i32 %28, %29
  %idx.neg = sub i32 0, %mul13
  %add.ptr = getelementptr inbounds i16, i16* %27, i32 %idx.neg
  store i16* %add.ptr, i16** %src_horiz, align 4, !tbaa !2
  %30 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc53, %entry
  %31 = load i32, i32* %y, align 4, !tbaa !6
  %32 = load i32, i32* %im_h, align 4, !tbaa !6
  %cmp = icmp slt i32 %31, %32
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  br label %for.end55

for.body:                                         ; preds = %for.cond
  %34 = bitcast i32* %x_qn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %35, i32* %x_qn, align 4, !tbaa !6
  %36 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc48, %for.body
  %37 = load i32, i32* %x, align 4, !tbaa !6
  %38 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %37, %38
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  store i32 5, i32* %cleanup.dest.slot, align 4
  %39 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  br label %for.end51

for.body19:                                       ; preds = %for.cond15
  %40 = bitcast i16** %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #4
  %41 = load i16*, i16** %src_horiz, align 4, !tbaa !2
  %42 = load i32, i32* %x_qn, align 4, !tbaa !6
  %shr20 = ashr i32 %42, 10
  %arrayidx = getelementptr inbounds i16, i16* %41, i32 %shr20
  store i16* %arrayidx, i16** %src_x, align 4, !tbaa !2
  %43 = bitcast i32* %x_filter_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  %44 = load i32, i32* %x_qn, align 4, !tbaa !6
  %and = and i32 %44, 1023
  %shr21 = ashr i32 %and, 6
  store i32 %shr21, i32* %x_filter_idx, align 4, !tbaa !6
  %45 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #4
  %46 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %47 = load i32, i32* %x_filter_idx, align 4, !tbaa !6
  %call = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %46, i32 %47)
  store i16* %call, i16** %x_filter, align 4, !tbaa !2
  %48 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #4
  %49 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add22 = add nsw i32 %49, 7
  %sub23 = sub nsw i32 %add22, 1
  %shl = shl i32 1, %sub23
  store i32 %shl, i32* %sum, align 4, !tbaa !6
  %50 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc, %for.body19
  %51 = load i32, i32* %k, align 4, !tbaa !6
  %52 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x.addr, align 4, !tbaa !2
  %taps25 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %52, i32 0, i32 1
  %53 = load i16, i16* %taps25, align 4, !tbaa !13
  %conv26 = zext i16 %53 to i32
  %cmp27 = icmp slt i32 %51, %conv26
  br i1 %cmp27, label %for.body30, label %for.cond.cleanup29

for.cond.cleanup29:                               ; preds = %for.cond24
  store i32 8, i32* %cleanup.dest.slot, align 4
  %54 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  br label %for.end

for.body30:                                       ; preds = %for.cond24
  %55 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %56 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds i16, i16* %55, i32 %56
  %57 = load i16, i16* %arrayidx31, align 2, !tbaa !9
  %conv32 = sext i16 %57 to i32
  %58 = load i16*, i16** %src_x, align 4, !tbaa !2
  %59 = load i32, i32* %k, align 4, !tbaa !6
  %60 = load i32, i32* %fo_horiz, align 4, !tbaa !6
  %sub33 = sub nsw i32 %59, %60
  %arrayidx34 = getelementptr inbounds i16, i16* %58, i32 %sub33
  %61 = load i16, i16* %arrayidx34, align 2, !tbaa !9
  %conv35 = zext i16 %61 to i32
  %mul36 = mul nsw i32 %conv32, %conv35
  %62 = load i32, i32* %sum, align 4, !tbaa !6
  %add37 = add nsw i32 %62, %mul36
  store i32 %add37, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body30
  %63 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %63, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond24

for.end:                                          ; preds = %for.cond.cleanup29
  %64 = load i32, i32* %sum, align 4, !tbaa !6
  %65 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_038 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %65, i32 0, i32 3
  %66 = load i32, i32* %round_038, align 4, !tbaa !15
  %shl39 = shl i32 1, %66
  %shr40 = ashr i32 %shl39, 1
  %add41 = add nsw i32 %64, %shr40
  %67 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_042 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %67, i32 0, i32 3
  %68 = load i32, i32* %round_042, align 4, !tbaa !15
  %shr43 = ashr i32 %add41, %68
  %conv44 = trunc i32 %shr43 to i16
  %69 = load i32, i32* %y, align 4, !tbaa !6
  %70 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul45 = mul nsw i32 %69, %70
  %71 = load i32, i32* %x, align 4, !tbaa !6
  %add46 = add nsw i32 %mul45, %71
  %arrayidx47 = getelementptr inbounds [33792 x i16], [33792 x i16]* %im_block, i32 0, i32 %add46
  store i16 %conv44, i16* %arrayidx47, align 2, !tbaa !9
  %72 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  %73 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  %74 = bitcast i32* %x_filter_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  %75 = bitcast i16** %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #4
  br label %for.inc48

for.inc48:                                        ; preds = %for.end
  %76 = load i32, i32* %x, align 4, !tbaa !6
  %inc49 = add nsw i32 %76, 1
  store i32 %inc49, i32* %x, align 4, !tbaa !6
  %77 = load i32, i32* %x_step_qn.addr, align 4, !tbaa !6
  %78 = load i32, i32* %x_qn, align 4, !tbaa !6
  %add50 = add nsw i32 %78, %77
  store i32 %add50, i32* %x_qn, align 4, !tbaa !6
  br label %for.cond15

for.end51:                                        ; preds = %for.cond.cleanup18
  %79 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %80 = load i16*, i16** %src_horiz, align 4, !tbaa !2
  %add.ptr52 = getelementptr inbounds i16, i16* %80, i32 %79
  store i16* %add.ptr52, i16** %src_horiz, align 4, !tbaa !2
  %81 = bitcast i32* %x_qn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  br label %for.inc53

for.inc53:                                        ; preds = %for.end51
  %82 = load i32, i32* %y, align 4, !tbaa !6
  %inc54 = add nsw i32 %82, 1
  store i32 %inc54, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end55:                                        ; preds = %for.cond.cleanup
  %83 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #4
  %arraydecay = getelementptr inbounds [33792 x i16], [33792 x i16]* %im_block, i32 0, i32 0
  %84 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %85 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul56 = mul nsw i32 %84, %85
  %add.ptr57 = getelementptr inbounds i16, i16* %arraydecay, i32 %mul56
  store i16* %add.ptr57, i16** %src_vert, align 4, !tbaa !2
  %86 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #4
  %87 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add58 = add nsw i32 %87, 14
  %88 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_059 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %88, i32 0, i32 3
  %89 = load i32, i32* %round_059, align 4, !tbaa !15
  %sub60 = sub nsw i32 %add58, %89
  store i32 %sub60, i32* %offset_bits, align 4, !tbaa !6
  %90 = bitcast i32* %x61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #4
  store i32 0, i32* %x61, align 4, !tbaa !6
  br label %for.cond62

for.cond62:                                       ; preds = %for.inc170, %for.end55
  %91 = load i32, i32* %x61, align 4, !tbaa !6
  %92 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp63 = icmp slt i32 %91, %92
  br i1 %cmp63, label %for.body66, label %for.cond.cleanup65

for.cond.cleanup65:                               ; preds = %for.cond62
  store i32 11, i32* %cleanup.dest.slot, align 4
  %93 = bitcast i32* %x61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  br label %for.end172

for.body66:                                       ; preds = %for.cond62
  %94 = bitcast i32* %y_qn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #4
  %95 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store i32 %95, i32* %y_qn, align 4, !tbaa !6
  %96 = bitcast i32* %y67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #4
  store i32 0, i32* %y67, align 4, !tbaa !6
  br label %for.cond68

for.cond68:                                       ; preds = %for.inc166, %for.body66
  %97 = load i32, i32* %y67, align 4, !tbaa !6
  %98 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp69 = icmp slt i32 %97, %98
  br i1 %cmp69, label %for.body72, label %for.cond.cleanup71

for.cond.cleanup71:                               ; preds = %for.cond68
  store i32 14, i32* %cleanup.dest.slot, align 4
  %99 = bitcast i32* %y67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #4
  br label %for.end169

for.body72:                                       ; preds = %for.cond68
  %100 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #4
  %101 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %102 = load i32, i32* %y_qn, align 4, !tbaa !6
  %shr73 = ashr i32 %102, 10
  %103 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul74 = mul nsw i32 %shr73, %103
  %arrayidx75 = getelementptr inbounds i16, i16* %101, i32 %mul74
  store i16* %arrayidx75, i16** %src_y, align 4, !tbaa !2
  %104 = bitcast i32* %y_filter_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #4
  %105 = load i32, i32* %y_qn, align 4, !tbaa !6
  %and76 = and i32 %105, 1023
  %shr77 = ashr i32 %and76, 6
  store i32 %shr77, i32* %y_filter_idx, align 4, !tbaa !6
  %106 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #4
  %107 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %108 = load i32, i32* %y_filter_idx, align 4, !tbaa !6
  %call78 = call i16* @av1_get_interp_filter_subpel_kernel(%struct.InterpFilterParams* %107, i32 %108)
  store i16* %call78, i16** %y_filter, align 4, !tbaa !2
  %109 = bitcast i32* %sum79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #4
  %110 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %shl80 = shl i32 1, %110
  store i32 %shl80, i32* %sum79, align 4, !tbaa !6
  %111 = bitcast i32* %k81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #4
  store i32 0, i32* %k81, align 4, !tbaa !6
  br label %for.cond82

for.cond82:                                       ; preds = %for.inc97, %for.body72
  %112 = load i32, i32* %k81, align 4, !tbaa !6
  %113 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y.addr, align 4, !tbaa !2
  %taps83 = getelementptr inbounds %struct.InterpFilterParams, %struct.InterpFilterParams* %113, i32 0, i32 1
  %114 = load i16, i16* %taps83, align 4, !tbaa !13
  %conv84 = zext i16 %114 to i32
  %cmp85 = icmp slt i32 %112, %conv84
  br i1 %cmp85, label %for.body88, label %for.cond.cleanup87

for.cond.cleanup87:                               ; preds = %for.cond82
  store i32 17, i32* %cleanup.dest.slot, align 4
  %115 = bitcast i32* %k81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #4
  br label %for.end99

for.body88:                                       ; preds = %for.cond82
  %116 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %117 = load i32, i32* %k81, align 4, !tbaa !6
  %arrayidx89 = getelementptr inbounds i16, i16* %116, i32 %117
  %118 = load i16, i16* %arrayidx89, align 2, !tbaa !9
  %conv90 = sext i16 %118 to i32
  %119 = load i16*, i16** %src_y, align 4, !tbaa !2
  %120 = load i32, i32* %k81, align 4, !tbaa !6
  %121 = load i32, i32* %fo_vert, align 4, !tbaa !6
  %sub91 = sub nsw i32 %120, %121
  %122 = load i32, i32* %im_stride, align 4, !tbaa !6
  %mul92 = mul nsw i32 %sub91, %122
  %arrayidx93 = getelementptr inbounds i16, i16* %119, i32 %mul92
  %123 = load i16, i16* %arrayidx93, align 2, !tbaa !9
  %conv94 = sext i16 %123 to i32
  %mul95 = mul nsw i32 %conv90, %conv94
  %124 = load i32, i32* %sum79, align 4, !tbaa !6
  %add96 = add nsw i32 %124, %mul95
  store i32 %add96, i32* %sum79, align 4, !tbaa !6
  br label %for.inc97

for.inc97:                                        ; preds = %for.body88
  %125 = load i32, i32* %k81, align 4, !tbaa !6
  %inc98 = add nsw i32 %125, 1
  store i32 %inc98, i32* %k81, align 4, !tbaa !6
  br label %for.cond82

for.end99:                                        ; preds = %for.cond.cleanup87
  %126 = bitcast i16* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %126) #4
  %127 = load i32, i32* %sum79, align 4, !tbaa !6
  %128 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1100 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %128, i32 0, i32 4
  %129 = load i32, i32* %round_1100, align 4, !tbaa !17
  %shl101 = shl i32 1, %129
  %shr102 = ashr i32 %shl101, 1
  %add103 = add nsw i32 %127, %shr102
  %130 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1104 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %130, i32 0, i32 4
  %131 = load i32, i32* %round_1104, align 4, !tbaa !17
  %shr105 = ashr i32 %add103, %131
  %conv106 = trunc i32 %shr105 to i16
  store i16 %conv106, i16* %res, align 2, !tbaa !9
  %132 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %132, i32 0, i32 6
  %133 = load i32, i32* %is_compound, align 4, !tbaa !25
  %tobool = icmp ne i32 %133, 0
  br i1 %tobool, label %if.then, label %if.else145

if.then:                                          ; preds = %for.end99
  %134 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %do_average = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %134, i32 0, i32 0
  %135 = load i32, i32* %do_average, align 4, !tbaa !21
  %tobool107 = icmp ne i32 %135, 0
  br i1 %tobool107, label %if.then108, label %if.else140

if.then108:                                       ; preds = %if.then
  %136 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #4
  %137 = load i16*, i16** %dst16, align 4, !tbaa !2
  %138 = load i32, i32* %y67, align 4, !tbaa !6
  %139 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul109 = mul nsw i32 %138, %139
  %140 = load i32, i32* %x61, align 4, !tbaa !6
  %add110 = add nsw i32 %mul109, %140
  %arrayidx111 = getelementptr inbounds i16, i16* %137, i32 %add110
  %141 = load i16, i16* %arrayidx111, align 2, !tbaa !9
  %conv112 = zext i16 %141 to i32
  store i32 %conv112, i32* %tmp, align 4, !tbaa !6
  %142 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %use_dist_wtd_comp_avg = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %142, i32 0, i32 8
  %143 = load i32, i32* %use_dist_wtd_comp_avg, align 4, !tbaa !22
  %tobool113 = icmp ne i32 %143, 0
  br i1 %tobool113, label %if.then114, label %if.else

if.then114:                                       ; preds = %if.then108
  %144 = load i32, i32* %tmp, align 4, !tbaa !6
  %145 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %fwd_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %145, i32 0, i32 9
  %146 = load i32, i32* %fwd_offset, align 4, !tbaa !23
  %mul115 = mul nsw i32 %144, %146
  %147 = load i16, i16* %res, align 2, !tbaa !9
  %conv116 = zext i16 %147 to i32
  %148 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %bck_offset = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %148, i32 0, i32 10
  %149 = load i32, i32* %bck_offset, align 4, !tbaa !24
  %mul117 = mul nsw i32 %conv116, %149
  %add118 = add nsw i32 %mul115, %mul117
  store i32 %add118, i32* %tmp, align 4, !tbaa !6
  %150 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr119 = ashr i32 %150, 4
  store i32 %shr119, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %if.then108
  %151 = load i16, i16* %res, align 2, !tbaa !9
  %conv120 = zext i16 %151 to i32
  %152 = load i32, i32* %tmp, align 4, !tbaa !6
  %add121 = add nsw i32 %152, %conv120
  store i32 %add121, i32* %tmp, align 4, !tbaa !6
  %153 = load i32, i32* %tmp, align 4, !tbaa !6
  %shr122 = ashr i32 %153, 1
  store i32 %shr122, i32* %tmp, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then114
  %154 = load i32, i32* %tmp, align 4, !tbaa !6
  %155 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %156 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1123 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %156, i32 0, i32 4
  %157 = load i32, i32* %round_1123, align 4, !tbaa !17
  %sub124 = sub nsw i32 %155, %157
  %shl125 = shl i32 1, %sub124
  %158 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %159 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1126 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %159, i32 0, i32 4
  %160 = load i32, i32* %round_1126, align 4, !tbaa !17
  %sub127 = sub nsw i32 %158, %160
  %sub128 = sub nsw i32 %sub127, 1
  %shl129 = shl i32 1, %sub128
  %add130 = add nsw i32 %shl125, %shl129
  %sub131 = sub nsw i32 %154, %add130
  store i32 %sub131, i32* %tmp, align 4, !tbaa !6
  %161 = load i32, i32* %tmp, align 4, !tbaa !6
  %162 = load i32, i32* %bits, align 4, !tbaa !6
  %shl132 = shl i32 1, %162
  %shr133 = ashr i32 %shl132, 1
  %add134 = add nsw i32 %161, %shr133
  %163 = load i32, i32* %bits, align 4, !tbaa !6
  %shr135 = ashr i32 %add134, %163
  %164 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call136 = call zeroext i16 @clip_pixel_highbd(i32 %shr135, i32 %164)
  %165 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %166 = load i32, i32* %y67, align 4, !tbaa !6
  %167 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul137 = mul nsw i32 %166, %167
  %168 = load i32, i32* %x61, align 4, !tbaa !6
  %add138 = add nsw i32 %mul137, %168
  %arrayidx139 = getelementptr inbounds i16, i16* %165, i32 %add138
  store i16 %call136, i16* %arrayidx139, align 2, !tbaa !9
  %169 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  br label %if.end144

if.else140:                                       ; preds = %if.then
  %170 = load i16, i16* %res, align 2, !tbaa !9
  %171 = load i16*, i16** %dst16, align 4, !tbaa !2
  %172 = load i32, i32* %y67, align 4, !tbaa !6
  %173 = load i32, i32* %dst16_stride, align 4, !tbaa !6
  %mul141 = mul nsw i32 %172, %173
  %174 = load i32, i32* %x61, align 4, !tbaa !6
  %add142 = add nsw i32 %mul141, %174
  %arrayidx143 = getelementptr inbounds i16, i16* %171, i32 %add142
  store i16 %170, i16* %arrayidx143, align 2, !tbaa !9
  br label %if.end144

if.end144:                                        ; preds = %if.else140, %if.end
  br label %if.end165

if.else145:                                       ; preds = %for.end99
  %175 = bitcast i32* %tmp146 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %175) #4
  %176 = load i16, i16* %res, align 2, !tbaa !9
  %conv147 = zext i16 %176 to i32
  %177 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %178 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1148 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %178, i32 0, i32 4
  %179 = load i32, i32* %round_1148, align 4, !tbaa !17
  %sub149 = sub nsw i32 %177, %179
  %shl150 = shl i32 1, %sub149
  %180 = load i32, i32* %offset_bits, align 4, !tbaa !6
  %181 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1151 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %181, i32 0, i32 4
  %182 = load i32, i32* %round_1151, align 4, !tbaa !17
  %sub152 = sub nsw i32 %180, %182
  %sub153 = sub nsw i32 %sub152, 1
  %shl154 = shl i32 1, %sub153
  %add155 = add nsw i32 %shl150, %shl154
  %sub156 = sub nsw i32 %conv147, %add155
  store i32 %sub156, i32* %tmp146, align 4, !tbaa !6
  %183 = load i32, i32* %tmp146, align 4, !tbaa !6
  %184 = load i32, i32* %bits, align 4, !tbaa !6
  %shl157 = shl i32 1, %184
  %shr158 = ashr i32 %shl157, 1
  %add159 = add nsw i32 %183, %shr158
  %185 = load i32, i32* %bits, align 4, !tbaa !6
  %shr160 = ashr i32 %add159, %185
  %186 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call161 = call zeroext i16 @clip_pixel_highbd(i32 %shr160, i32 %186)
  %187 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %188 = load i32, i32* %y67, align 4, !tbaa !6
  %189 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %mul162 = mul nsw i32 %188, %189
  %190 = load i32, i32* %x61, align 4, !tbaa !6
  %add163 = add nsw i32 %mul162, %190
  %arrayidx164 = getelementptr inbounds i16, i16* %187, i32 %add163
  store i16 %call161, i16* %arrayidx164, align 2, !tbaa !9
  %191 = bitcast i32* %tmp146 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #4
  br label %if.end165

if.end165:                                        ; preds = %if.else145, %if.end144
  %192 = bitcast i16* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %192) #4
  %193 = bitcast i32* %sum79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #4
  %194 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #4
  %195 = bitcast i32* %y_filter_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #4
  %196 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  br label %for.inc166

for.inc166:                                       ; preds = %if.end165
  %197 = load i32, i32* %y67, align 4, !tbaa !6
  %inc167 = add nsw i32 %197, 1
  store i32 %inc167, i32* %y67, align 4, !tbaa !6
  %198 = load i32, i32* %y_step_qn.addr, align 4, !tbaa !6
  %199 = load i32, i32* %y_qn, align 4, !tbaa !6
  %add168 = add nsw i32 %199, %198
  store i32 %add168, i32* %y_qn, align 4, !tbaa !6
  br label %for.cond68

for.end169:                                       ; preds = %for.cond.cleanup71
  %200 = load i16*, i16** %src_vert, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %200, i32 1
  store i16* %incdec.ptr, i16** %src_vert, align 4, !tbaa !2
  %201 = bitcast i32* %y_qn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #4
  br label %for.inc170

for.inc170:                                       ; preds = %for.end169
  %202 = load i32, i32* %x61, align 4, !tbaa !6
  %inc171 = add nsw i32 %202, 1
  store i32 %inc171, i32* %x61, align 4, !tbaa !6
  br label %for.cond62

for.end172:                                       ; preds = %for.cond.cleanup65
  %203 = bitcast i32* %offset_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i16** %src_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i16** %src_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i32* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i32* %dst16_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i16** %dst16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i32* %fo_horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i32* %fo_vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i32* %im_stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i32* %im_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  %213 = bitcast [33792 x i16]* %im_block to i8*
  call void @llvm.lifetime.end.p0i8(i64 67584, i8* %213) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_convolve_2d_facade(i8* %src8, i32 %src_stride, i8* %dst8, i32 %dst_stride, i32 %w, i32 %h, %struct.InterpFilterParams** %interp_filters, i32 %subpel_x_qn, i32 %x_step_q4, i32 %subpel_y_qn, i32 %y_step_q4, i32 %scaled, %struct.ConvolveParams* %conv_params, %struct.scale_factors* %sf, i32 %bd) #0 {
entry:
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %interp_filters.addr = alloca %struct.InterpFilterParams**, align 4
  %subpel_x_qn.addr = alloca i32, align 4
  %x_step_q4.addr = alloca i32, align 4
  %subpel_y_qn.addr = alloca i32, align 4
  %y_step_q4.addr = alloca i32, align 4
  %scaled.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %sf.addr = alloca %struct.scale_factors*, align 4
  %bd.addr = alloca i32, align 4
  %src = alloca i16*, align 4
  %need_filter_params_x = alloca i32, align 4
  %need_filter_params_y = alloca i32, align 4
  %filter_params_x = alloca %struct.InterpFilterParams*, align 4
  %filter_params_y = alloca %struct.InterpFilterParams*, align 4
  %dst = alloca i16*, align 4
  %dst14 = alloca i16*, align 4
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !6
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.InterpFilterParams** %interp_filters, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  store i32 %subpel_x_qn, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !6
  store i32 %subpel_y_qn, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !6
  store i32 %scaled, i32* %scaled.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store %struct.scale_factors* %sf, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !6
  %1 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %2 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %3 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %5 = ptrtoint i8* %4 to i32
  %shl = shl i32 %5, 1
  %6 = inttoptr i32 %shl to i16*
  store i16* %6, i16** %src, align 4, !tbaa !2
  %7 = bitcast i32* %need_filter_params_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %8, 0
  %conv = zext i1 %cmp to i32
  %9 = load i32, i32* %scaled.addr, align 4, !tbaa !6
  %or = or i32 %conv, %9
  store i32 %or, i32* %need_filter_params_x, align 4, !tbaa !6
  %10 = bitcast i32* %need_filter_params_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %cmp1 = icmp ne i32 %11, 0
  %conv2 = zext i1 %cmp1 to i32
  %12 = load i32, i32* %scaled.addr, align 4, !tbaa !6
  %or3 = or i32 %conv2, %12
  store i32 %or3, i32* %need_filter_params_y, align 4, !tbaa !6
  %13 = bitcast %struct.InterpFilterParams** %filter_params_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i32, i32* %need_filter_params_x, align 4, !tbaa !6
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %15 = load %struct.InterpFilterParams**, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.InterpFilterParams*, %struct.InterpFilterParams** %15, i32 0
  %16 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %arrayidx, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.InterpFilterParams* [ %16, %cond.true ], [ null, %cond.false ]
  store %struct.InterpFilterParams* %cond, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %17 = bitcast %struct.InterpFilterParams** %filter_params_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load i32, i32* %need_filter_params_y, align 4, !tbaa !6
  %tobool4 = icmp ne i32 %18, 0
  br i1 %tobool4, label %cond.true5, label %cond.false7

cond.true5:                                       ; preds = %cond.end
  %19 = load %struct.InterpFilterParams**, %struct.InterpFilterParams*** %interp_filters.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds %struct.InterpFilterParams*, %struct.InterpFilterParams** %19, i32 1
  %20 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %arrayidx6, align 4, !tbaa !2
  br label %cond.end8

cond.false7:                                      ; preds = %cond.end
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true5
  %cond9 = phi %struct.InterpFilterParams* [ %20, %cond.true5 ], [ null, %cond.false7 ]
  store %struct.InterpFilterParams* %cond9, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %21 = load i32, i32* %scaled.addr, align 4, !tbaa !6
  %tobool10 = icmp ne i32 %21, 0
  br i1 %tobool10, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end8
  %22 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %24 = ptrtoint i8* %23 to i32
  %shl11 = shl i32 %24, 1
  %25 = inttoptr i32 %shl11 to i16*
  store i16* %25, i16** %dst, align 4, !tbaa !2
  %26 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %26, i32 0, i32 6
  %27 = load i32, i32* %is_compound, align 4, !tbaa !25
  %tobool12 = icmp ne i32 %27, 0
  br i1 %tobool12, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.then
  br label %if.end

if.end:                                           ; preds = %if.then13, %if.then
  %28 = load i16*, i16** %src, align 4, !tbaa !2
  %29 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %30 = load i16*, i16** %dst, align 4, !tbaa !2
  %31 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %32 = load i32, i32* %w.addr, align 4, !tbaa !6
  %33 = load i32, i32* %h.addr, align 4, !tbaa !6
  %34 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %35 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %36 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %37 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !6
  %38 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %39 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %40 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %41 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @av1_highbd_convolve_2d_scale_c(i16* %28, i32 %29, i16* %30, i32 %31, i32 %32, i32 %33, %struct.InterpFilterParams* %34, %struct.InterpFilterParams* %35, i32 %36, i32 %37, i32 %38, i32 %39, %struct.ConvolveParams* %40, i32 %41)
  %42 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  br label %if.end24

if.else:                                          ; preds = %cond.end8
  %43 = bitcast i16** %dst14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  %44 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %45 = ptrtoint i8* %44 to i32
  %shl15 = shl i32 %45, 1
  %46 = inttoptr i32 %shl15 to i16*
  store i16* %46, i16** %dst14, align 4, !tbaa !2
  %47 = load %struct.scale_factors*, %struct.scale_factors** %sf.addr, align 4, !tbaa !2
  %highbd_convolve = getelementptr inbounds %struct.scale_factors, %struct.scale_factors* %47, i32 0, i32 7
  %48 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %cmp16 = icmp ne i32 %48, 0
  %conv17 = zext i1 %cmp16 to i32
  %arrayidx18 = getelementptr inbounds [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]], [2 x [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]]* %highbd_convolve, i32 0, i32 %conv17
  %49 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %cmp19 = icmp ne i32 %49, 0
  %conv20 = zext i1 %cmp19 to i32
  %arrayidx21 = getelementptr inbounds [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]], [2 x [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]]* %arrayidx18, i32 0, i32 %conv20
  %50 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %is_compound22 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %50, i32 0, i32 6
  %51 = load i32, i32* %is_compound22, align 4, !tbaa !25
  %arrayidx23 = getelementptr inbounds [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*], [2 x void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*]* %arrayidx21, i32 0, i32 %51
  %52 = load void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)*, void (i16*, i32, i16*, i32, i32, i32, %struct.InterpFilterParams*, %struct.InterpFilterParams*, i32, i32, %struct.ConvolveParams*, i32)** %arrayidx23, align 4, !tbaa !2
  %53 = load i16*, i16** %src, align 4, !tbaa !2
  %54 = load i32, i32* %src_stride.addr, align 4, !tbaa !6
  %55 = load i16*, i16** %dst14, align 4, !tbaa !2
  %56 = load i32, i32* %dst_stride.addr, align 4, !tbaa !6
  %57 = load i32, i32* %w.addr, align 4, !tbaa !6
  %58 = load i32, i32* %h.addr, align 4, !tbaa !6
  %59 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_x, align 4, !tbaa !2
  %60 = load %struct.InterpFilterParams*, %struct.InterpFilterParams** %filter_params_y, align 4, !tbaa !2
  %61 = load i32, i32* %subpel_x_qn.addr, align 4, !tbaa !6
  %62 = load i32, i32* %subpel_y_qn.addr, align 4, !tbaa !6
  %63 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %64 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void %52(i16* %53, i32 %54, i16* %55, i32 %56, i32 %57, i32 %58, %struct.InterpFilterParams* %59, %struct.InterpFilterParams* %60, i32 %61, i32 %62, %struct.ConvolveParams* %63, i32 %64)
  %65 = bitcast i16** %dst14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  br label %if.end24

if.end24:                                         ; preds = %if.else, %if.end
  %66 = bitcast %struct.InterpFilterParams** %filter_params_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #4
  %67 = bitcast %struct.InterpFilterParams** %filter_params_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  %68 = bitcast i32* %need_filter_params_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #4
  %69 = bitcast i32* %need_filter_params_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %70 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_wiener_convolve_add_src_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i16* %filter_x, i32 %x_step_q4, i16* %filter_y, i32 %y_step_q4, i32 %w, i32 %h, %struct.ConvolveParams* %conv_params) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %filter_x.addr = alloca i16*, align 4
  %x_step_q4.addr = alloca i32, align 4
  %filter_y.addr = alloca i16*, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %filters_x = alloca [8 x i16]*, align 4
  %x0_q4 = alloca i32, align 4
  %filters_y = alloca [8 x i16]*, align 4
  %y0_q4 = alloca i32, align 4
  %temp = alloca [33664 x i16], align 16
  %intermediate_height = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !26
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !26
  store i16* %filter_x, i16** %filter_x.addr, align 4, !tbaa !2
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !6
  store i16* %filter_y, i16** %filter_y.addr, align 4, !tbaa !2
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %0 = bitcast [8 x i16]** %filters_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @get_filter_base(i16* %1)
  store [8 x i16]* %call, [8 x i16]** %filters_x, align 4, !tbaa !2
  %2 = bitcast i32* %x0_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %4 = load [8 x i16]*, [8 x i16]** %filters_x, align 4, !tbaa !2
  %call1 = call i32 @get_filter_offset(i16* %3, [8 x i16]* %4)
  store i32 %call1, i32* %x0_q4, align 4, !tbaa !6
  %5 = bitcast [8 x i16]** %filters_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %call2 = call [8 x i16]* @get_filter_base(i16* %6)
  store [8 x i16]* %call2, [8 x i16]** %filters_y, align 4, !tbaa !2
  %7 = bitcast i32* %y0_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %9 = load [8 x i16]*, [8 x i16]** %filters_y, align 4, !tbaa !2
  %call3 = call i32 @get_filter_offset(i16* %8, [8 x i16]* %9)
  store i32 %call3, i32* %y0_q4, align 4, !tbaa !6
  %10 = bitcast [33664 x i16]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 67328, i8* %10) #4
  %11 = bitcast i32* %intermediate_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i32, i32* %h.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %12, 1
  %13 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %sub, %13
  %14 = load i32, i32* %y0_q4, align 4, !tbaa !6
  %add = add nsw i32 %mul, %14
  %shr = ashr i32 %add, 4
  %add4 = add nsw i32 %shr, 8
  %sub5 = sub nsw i32 %add4, 1
  store i32 %sub5, i32* %intermediate_height, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [33664 x i16], [33664 x i16]* %temp, i32 0, i32 0
  %15 = load i32, i32* %intermediate_height, align 4, !tbaa !6
  %mul6 = mul nsw i32 %15, 128
  %add.ptr = getelementptr inbounds i16, i16* %arraydecay, i32 %mul6
  %16 = bitcast i16* %add.ptr to i8*
  call void @llvm.memset.p0i8.i32(i8* align 2 %16, i8 0, i32 128, i1 false)
  %17 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %18 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %mul7 = mul nsw i32 %18, 3
  %idx.neg = sub i32 0, %mul7
  %add.ptr8 = getelementptr inbounds i8, i8* %17, i32 %idx.neg
  %19 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %arraydecay9 = getelementptr inbounds [33664 x i16], [33664 x i16]* %temp, i32 0, i32 0
  %20 = load [8 x i16]*, [8 x i16]** %filters_x, align 4, !tbaa !2
  %21 = load i32, i32* %x0_q4, align 4, !tbaa !6
  %22 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !6
  %23 = load i32, i32* %w.addr, align 4, !tbaa !6
  %24 = load i32, i32* %intermediate_height, align 4, !tbaa !6
  %25 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %25, i32 0, i32 3
  %26 = load i32, i32* %round_0, align 4, !tbaa !15
  call void @convolve_add_src_horiz_hip(i8* %add.ptr8, i32 %19, i16* %arraydecay9, i32 128, [8 x i16]* %20, i32 %21, i32 %22, i32 %23, i32 %24, i32 %26)
  %arraydecay10 = getelementptr inbounds [33664 x i16], [33664 x i16]* %temp, i32 0, i32 0
  %add.ptr11 = getelementptr inbounds i16, i16* %arraydecay10, i32 384
  %27 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %28 = load i32, i32* %dst_stride.addr, align 4, !tbaa !26
  %29 = load [8 x i16]*, [8 x i16]** %filters_y, align 4, !tbaa !2
  %30 = load i32, i32* %y0_q4, align 4, !tbaa !6
  %31 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %32 = load i32, i32* %w.addr, align 4, !tbaa !6
  %33 = load i32, i32* %h.addr, align 4, !tbaa !6
  %34 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %34, i32 0, i32 4
  %35 = load i32, i32* %round_1, align 4, !tbaa !17
  call void @convolve_add_src_vert_hip(i16* %add.ptr11, i32 128, i8* %27, i32 %28, [8 x i16]* %29, i32 %30, i32 %31, i32 %32, i32 %33, i32 %35)
  %36 = bitcast i32* %intermediate_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast [33664 x i16]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 67328, i8* %37) #4
  %38 = bitcast i32* %y0_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %39 = bitcast [8 x i16]** %filters_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %40 = bitcast i32* %x0_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast [8 x i16]** %filters_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  ret void
}

; Function Attrs: nounwind
define internal [8 x i16]* @get_filter_base(i16* %filter) #0 {
entry:
  %filter.addr = alloca i16*, align 4
  store i16* %filter, i16** %filter.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %filter.addr, align 4, !tbaa !2
  %1 = ptrtoint i16* %0 to i32
  %and = and i32 %1, -256
  %2 = inttoptr i32 %and to [8 x i16]*
  ret [8 x i16]* %2
}

; Function Attrs: nounwind
define internal i32 @get_filter_offset(i16* %f, [8 x i16]* %base) #0 {
entry:
  %f.addr = alloca i16*, align 4
  %base.addr = alloca [8 x i16]*, align 4
  store i16* %f, i16** %f.addr, align 4, !tbaa !2
  store [8 x i16]* %base, [8 x i16]** %base.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %f.addr, align 4, !tbaa !2
  %1 = ptrtoint i16* %0 to i32
  %2 = inttoptr i32 %1 to [8 x i16]*
  %3 = load [8 x i16]*, [8 x i16]** %base.addr, align 4, !tbaa !2
  %sub.ptr.lhs.cast = ptrtoint [8 x i16]* %2 to i32
  %sub.ptr.rhs.cast = ptrtoint [8 x i16]* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 16
  ret i32 %sub.ptr.div
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define internal void @convolve_add_src_horiz_hip(i8* %src, i32 %src_stride, i16* %dst, i32 %dst_stride, [8 x i16]* %x_filters, i32 %x0_q4, i32 %x_step_q4, i32 %w, i32 %h, i32 %round0_bits) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %x_filters.addr = alloca [8 x i16]*, align 4
  %x0_q4.addr = alloca i32, align 4
  %x_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %round0_bits.addr = alloca i32, align 4
  %bd = alloca i32, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x_q4 = alloca i32, align 4
  %x = alloca i32, align 4
  %src_x = alloca i8*, align 4
  %x_filter = alloca i16*, align 4
  %rounding = alloca i32, align 4
  %sum = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !26
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !26
  store [8 x i16]* %x_filters, [8 x i16]** %x_filters.addr, align 4, !tbaa !2
  store i32 %x0_q4, i32* %x0_q4.addr, align 4, !tbaa !6
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %round0_bits, i32* %round0_bits.addr, align 4, !tbaa !6
  %0 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 8, i32* %bd, align 4, !tbaa !6
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 -3
  store i8* %add.ptr, i8** %src.addr, align 4, !tbaa !2
  %2 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc20, %entry
  %3 = load i32, i32* %y, align 4, !tbaa !6
  %4 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %5 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  br label %for.end22

for.body:                                         ; preds = %for.cond
  %6 = bitcast i32* %x_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %x0_q4.addr, align 4, !tbaa !6
  store i32 %7, i32* %x_q4, align 4, !tbaa !6
  %8 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %x, align 4, !tbaa !6
  %10 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %9, %10
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %12 = bitcast i8** %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %14 = load i32, i32* %x_q4, align 4, !tbaa !6
  %shr = ashr i32 %14, 4
  %arrayidx = getelementptr inbounds i8, i8* %13, i32 %shr
  store i8* %arrayidx, i8** %src_x, align 4, !tbaa !2
  %15 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load [8 x i16]*, [8 x i16]** %x_filters.addr, align 4, !tbaa !2
  %17 = load i32, i32* %x_q4, align 4, !tbaa !6
  %and = and i32 %17, 15
  %arrayidx5 = getelementptr inbounds [8 x i16], [8 x i16]* %16, i32 %and
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx5, i32 0, i32 0
  store i16* %arraydecay, i16** %x_filter, align 4, !tbaa !2
  %18 = bitcast i32* %rounding to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load i8*, i8** %src_x, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %19, i32 3
  %20 = load i8, i8* %arrayidx6, align 1, !tbaa !8
  %conv = zext i8 %20 to i32
  %shl = shl i32 %conv, 7
  %add = add nsw i32 %shl, 16384
  store i32 %add, i32* %rounding, align 4, !tbaa !6
  %21 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load i8*, i8** %src_x, align 4, !tbaa !2
  %23 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %call = call i32 @horz_scalar_product(i8* %22, i16* %23)
  %24 = load i32, i32* %rounding, align 4, !tbaa !6
  %add7 = add nsw i32 %call, %24
  store i32 %add7, i32* %sum, align 4, !tbaa !6
  %25 = load i32, i32* %sum, align 4, !tbaa !6
  %26 = load i32, i32* %round0_bits.addr, align 4, !tbaa !6
  %shl8 = shl i32 1, %26
  %shr9 = ashr i32 %shl8, 1
  %add10 = add nsw i32 %25, %shr9
  %27 = load i32, i32* %round0_bits.addr, align 4, !tbaa !6
  %shr11 = ashr i32 %add10, %27
  %28 = load i32, i32* %round0_bits.addr, align 4, !tbaa !6
  %sub = sub nsw i32 16, %28
  %shl12 = shl i32 1, %sub
  %sub13 = sub nsw i32 %shl12, 1
  %call14 = call i32 @clamp(i32 %shr11, i32 0, i32 %sub13)
  %conv15 = trunc i32 %call14 to i16
  %29 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %30 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds i16, i16* %29, i32 %30
  store i16 %conv15, i16* %arrayidx16, align 2, !tbaa !9
  %31 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !6
  %32 = load i32, i32* %x_q4, align 4, !tbaa !6
  %add17 = add nsw i32 %32, %31
  store i32 %add17, i32* %x_q4, align 4, !tbaa !6
  %33 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i32* %rounding to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  %35 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i8** %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %37 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %38 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %39 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %add.ptr18 = getelementptr inbounds i8, i8* %39, i32 %38
  store i8* %add.ptr18, i8** %src.addr, align 4, !tbaa !2
  %40 = load i32, i32* %dst_stride.addr, align 4, !tbaa !26
  %41 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr19 = getelementptr inbounds i16, i16* %41, i32 %40
  store i16* %add.ptr19, i16** %dst.addr, align 4, !tbaa !2
  %42 = bitcast i32* %x_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  br label %for.inc20

for.inc20:                                        ; preds = %for.end
  %43 = load i32, i32* %y, align 4, !tbaa !6
  %inc21 = add nsw i32 %43, 1
  store i32 %inc21, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end22:                                        ; preds = %for.cond.cleanup
  %44 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  ret void
}

; Function Attrs: nounwind
define internal void @convolve_add_src_vert_hip(i16* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, [8 x i16]* %y_filters, i32 %y0_q4, i32 %y_step_q4, i32 %w, i32 %h, i32 %round1_bits) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %y_filters.addr = alloca [8 x i16]*, align 4
  %y0_q4.addr = alloca i32, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %round1_bits.addr = alloca i32, align 4
  %bd = alloca i32, align 4
  %x = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y_q4 = alloca i32, align 4
  %y = alloca i32, align 4
  %src_y = alloca i16*, align 4
  %y_filter = alloca i16*, align 4
  %rounding = alloca i32, align 4
  %sum = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !26
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !26
  store [8 x i16]* %y_filters, [8 x i16]** %y_filters.addr, align 4, !tbaa !2
  store i32 %y0_q4, i32* %y0_q4.addr, align 4, !tbaa !6
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %round1_bits, i32* %round1_bits.addr, align 4, !tbaa !6
  %0 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 8, i32* %bd, align 4, !tbaa !6
  %1 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %mul = mul nsw i32 %1, 3
  %2 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i16, i16* %2, i32 %idx.neg
  store i16* %add.ptr, i16** %src.addr, align 4, !tbaa !2
  %3 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %entry
  %4 = load i32, i32* %x, align 4, !tbaa !6
  %5 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %6 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  br label %for.end23

for.body:                                         ; preds = %for.cond
  %7 = bitcast i32* %y_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %y0_q4.addr, align 4, !tbaa !6
  store i32 %8, i32* %y_q4, align 4, !tbaa !6
  %9 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %10 = load i32, i32* %y, align 4, !tbaa !6
  %11 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %10, %11
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %12 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %13 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %15 = load i32, i32* %y_q4, align 4, !tbaa !6
  %shr = ashr i32 %15, 4
  %16 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %mul5 = mul nsw i32 %shr, %16
  %arrayidx = getelementptr inbounds i16, i16* %14, i32 %mul5
  store i16* %arrayidx, i16** %src_y, align 4, !tbaa !2
  %17 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load [8 x i16]*, [8 x i16]** %y_filters.addr, align 4, !tbaa !2
  %19 = load i32, i32* %y_q4, align 4, !tbaa !6
  %and = and i32 %19, 15
  %arrayidx6 = getelementptr inbounds [8 x i16], [8 x i16]* %18, i32 %and
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx6, i32 0, i32 0
  store i16* %arraydecay, i16** %y_filter, align 4, !tbaa !2
  %20 = bitcast i32* %rounding to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load i16*, i16** %src_y, align 4, !tbaa !2
  %22 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %mul7 = mul nsw i32 3, %22
  %arrayidx8 = getelementptr inbounds i16, i16* %21, i32 %mul7
  %23 = load i16, i16* %arrayidx8, align 2, !tbaa !9
  %conv = zext i16 %23 to i32
  %shl = shl i32 %conv, 7
  %24 = load i32, i32* %round1_bits.addr, align 4, !tbaa !6
  %add = add nsw i32 8, %24
  %sub = sub nsw i32 %add, 1
  %shl9 = shl i32 1, %sub
  %sub10 = sub nsw i32 %shl, %shl9
  store i32 %sub10, i32* %rounding, align 4, !tbaa !6
  %25 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  %26 = load i16*, i16** %src_y, align 4, !tbaa !2
  %27 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %28 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %call = call i32 @highbd_vert_scalar_product(i16* %26, i32 %27, i16* %28)
  %29 = load i32, i32* %rounding, align 4, !tbaa !6
  %add11 = add nsw i32 %call, %29
  store i32 %add11, i32* %sum, align 4, !tbaa !6
  %30 = load i32, i32* %sum, align 4, !tbaa !6
  %31 = load i32, i32* %round1_bits.addr, align 4, !tbaa !6
  %shl12 = shl i32 1, %31
  %shr13 = ashr i32 %shl12, 1
  %add14 = add nsw i32 %30, %shr13
  %32 = load i32, i32* %round1_bits.addr, align 4, !tbaa !6
  %shr15 = ashr i32 %add14, %32
  %call16 = call zeroext i8 @clip_pixel(i32 %shr15)
  %33 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %34 = load i32, i32* %y, align 4, !tbaa !6
  %35 = load i32, i32* %dst_stride.addr, align 4, !tbaa !26
  %mul17 = mul nsw i32 %34, %35
  %arrayidx18 = getelementptr inbounds i8, i8* %33, i32 %mul17
  store i8 %call16, i8* %arrayidx18, align 1, !tbaa !8
  %36 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %37 = load i32, i32* %y_q4, align 4, !tbaa !6
  %add19 = add nsw i32 %37, %36
  store i32 %add19, i32* %y_q4, align 4, !tbaa !6
  %38 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %39 = bitcast i32* %rounding to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %40 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %42 = load i32, i32* %y, align 4, !tbaa !6
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %y, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %43 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %43, i32 1
  store i16* %incdec.ptr, i16** %src.addr, align 4, !tbaa !2
  %44 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %44, i32 1
  store i8* %incdec.ptr20, i8** %dst.addr, align 4, !tbaa !2
  %45 = bitcast i32* %y_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  br label %for.inc21

for.inc21:                                        ; preds = %for.end
  %46 = load i32, i32* %x, align 4, !tbaa !6
  %inc22 = add nsw i32 %46, 1
  store i32 %inc22, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.end23:                                        ; preds = %for.cond.cleanup
  %47 = bitcast i32* %bd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_highbd_wiener_convolve_add_src_c(i8* %src, i32 %src_stride, i8* %dst, i32 %dst_stride, i16* %filter_x, i32 %x_step_q4, i16* %filter_y, i32 %y_step_q4, i32 %w, i32 %h, %struct.ConvolveParams* %conv_params, i32 %bd) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %filter_x.addr = alloca i16*, align 4
  %x_step_q4.addr = alloca i32, align 4
  %filter_y.addr = alloca i16*, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %conv_params.addr = alloca %struct.ConvolveParams*, align 4
  %bd.addr = alloca i32, align 4
  %filters_x = alloca [8 x i16]*, align 4
  %x0_q4 = alloca i32, align 4
  %filters_y = alloca [8 x i16]*, align 4
  %y0_q4 = alloca i32, align 4
  %temp = alloca [33664 x i16], align 16
  %intermediate_height = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !26
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !26
  store i16* %filter_x, i16** %filter_x.addr, align 4, !tbaa !2
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !6
  store i16* %filter_y, i16** %filter_y.addr, align 4, !tbaa !2
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store %struct.ConvolveParams* %conv_params, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast [8 x i16]** %filters_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %call = call [8 x i16]* @get_filter_base(i16* %1)
  store [8 x i16]* %call, [8 x i16]** %filters_x, align 4, !tbaa !2
  %2 = bitcast i32* %x0_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i16*, i16** %filter_x.addr, align 4, !tbaa !2
  %4 = load [8 x i16]*, [8 x i16]** %filters_x, align 4, !tbaa !2
  %call1 = call i32 @get_filter_offset(i16* %3, [8 x i16]* %4)
  store i32 %call1, i32* %x0_q4, align 4, !tbaa !6
  %5 = bitcast [8 x i16]** %filters_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %call2 = call [8 x i16]* @get_filter_base(i16* %6)
  store [8 x i16]* %call2, [8 x i16]** %filters_y, align 4, !tbaa !2
  %7 = bitcast i32* %y0_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i16*, i16** %filter_y.addr, align 4, !tbaa !2
  %9 = load [8 x i16]*, [8 x i16]** %filters_y, align 4, !tbaa !2
  %call3 = call i32 @get_filter_offset(i16* %8, [8 x i16]* %9)
  store i32 %call3, i32* %y0_q4, align 4, !tbaa !6
  %10 = bitcast [33664 x i16]* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 67328, i8* %10) #4
  %11 = bitcast i32* %intermediate_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i32, i32* %h.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %12, 1
  %13 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %sub, %13
  %14 = load i32, i32* %y0_q4, align 4, !tbaa !6
  %add = add nsw i32 %mul, %14
  %shr = ashr i32 %add, 4
  %add4 = add nsw i32 %shr, 8
  store i32 %add4, i32* %intermediate_height, align 4, !tbaa !6
  %15 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %16 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %mul5 = mul nsw i32 %16, 3
  %idx.neg = sub i32 0, %mul5
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 %idx.neg
  %17 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %arraydecay = getelementptr inbounds [33664 x i16], [33664 x i16]* %temp, i32 0, i32 0
  %18 = load [8 x i16]*, [8 x i16]** %filters_x, align 4, !tbaa !2
  %19 = load i32, i32* %x0_q4, align 4, !tbaa !6
  %20 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !6
  %21 = load i32, i32* %w.addr, align 4, !tbaa !6
  %22 = load i32, i32* %intermediate_height, align 4, !tbaa !6
  %23 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_0 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %23, i32 0, i32 3
  %24 = load i32, i32* %round_0, align 4, !tbaa !15
  %25 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_convolve_add_src_horiz_hip(i8* %add.ptr, i32 %17, i16* %arraydecay, i32 128, [8 x i16]* %18, i32 %19, i32 %20, i32 %21, i32 %22, i32 %24, i32 %25)
  %arraydecay6 = getelementptr inbounds [33664 x i16], [33664 x i16]* %temp, i32 0, i32 0
  %add.ptr7 = getelementptr inbounds i16, i16* %arraydecay6, i32 384
  %26 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %27 = load i32, i32* %dst_stride.addr, align 4, !tbaa !26
  %28 = load [8 x i16]*, [8 x i16]** %filters_y, align 4, !tbaa !2
  %29 = load i32, i32* %y0_q4, align 4, !tbaa !6
  %30 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %31 = load i32, i32* %w.addr, align 4, !tbaa !6
  %32 = load i32, i32* %h.addr, align 4, !tbaa !6
  %33 = load %struct.ConvolveParams*, %struct.ConvolveParams** %conv_params.addr, align 4, !tbaa !2
  %round_1 = getelementptr inbounds %struct.ConvolveParams, %struct.ConvolveParams* %33, i32 0, i32 4
  %34 = load i32, i32* %round_1, align 4, !tbaa !17
  %35 = load i32, i32* %bd.addr, align 4, !tbaa !6
  call void @highbd_convolve_add_src_vert_hip(i16* %add.ptr7, i32 128, i8* %26, i32 %27, [8 x i16]* %28, i32 %29, i32 %30, i32 %31, i32 %32, i32 %34, i32 %35)
  %36 = bitcast i32* %intermediate_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast [33664 x i16]* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 67328, i8* %37) #4
  %38 = bitcast i32* %y0_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %39 = bitcast [8 x i16]** %filters_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %40 = bitcast i32* %x0_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast [8 x i16]** %filters_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_convolve_add_src_horiz_hip(i8* %src8, i32 %src_stride, i16* %dst, i32 %dst_stride, [8 x i16]* %x_filters, i32 %x0_q4, i32 %x_step_q4, i32 %w, i32 %h, i32 %round0_bits, i32 %bd) #0 {
entry:
  %src8.addr = alloca i8*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst.addr = alloca i16*, align 4
  %dst_stride.addr = alloca i32, align 4
  %x_filters.addr = alloca [8 x i16]*, align 4
  %x0_q4.addr = alloca i32, align 4
  %x_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %round0_bits.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %extraprec_clamp_limit = alloca i32, align 4
  %src = alloca i16*, align 4
  %y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %x_q4 = alloca i32, align 4
  %x = alloca i32, align 4
  %src_x = alloca i16*, align 4
  %x_filter = alloca i16*, align 4
  %rounding = alloca i32, align 4
  %sum = alloca i32, align 4
  store i8* %src8, i8** %src8.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !26
  store i16* %dst, i16** %dst.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !26
  store [8 x i16]* %x_filters, [8 x i16]** %x_filters.addr, align 4, !tbaa !2
  store i32 %x0_q4, i32* %x0_q4.addr, align 4, !tbaa !6
  store i32 %x_step_q4, i32* %x_step_q4.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %round0_bits, i32* %round0_bits.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i32* %extraprec_clamp_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add = add nsw i32 %1, 1
  %add1 = add nsw i32 %add, 7
  %2 = load i32, i32* %round0_bits.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %add1, %2
  %shl = shl i32 1, %sub
  store i32 %shl, i32* %extraprec_clamp_limit, align 4, !tbaa !6
  %3 = bitcast i16** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i8*, i8** %src8.addr, align 4, !tbaa !2
  %5 = ptrtoint i8* %4 to i32
  %shl2 = shl i32 %5, 1
  %6 = inttoptr i32 %shl2 to i16*
  store i16* %6, i16** %src, align 4, !tbaa !2
  %7 = load i16*, i16** %src, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %7, i32 -3
  store i16* %add.ptr, i16** %src, align 4, !tbaa !2
  %8 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc26, %entry
  %9 = load i32, i32* %y, align 4, !tbaa !6
  %10 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  br label %for.end28

for.body:                                         ; preds = %for.cond
  %12 = bitcast i32* %x_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i32, i32* %x0_q4.addr, align 4, !tbaa !6
  store i32 %13, i32* %x_q4, align 4, !tbaa !6
  %14 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %15 = load i32, i32* %x, align 4, !tbaa !6
  %16 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp4 = icmp slt i32 %15, %16
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %for.end

for.body6:                                        ; preds = %for.cond3
  %18 = bitcast i16** %src_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load i16*, i16** %src, align 4, !tbaa !2
  %20 = load i32, i32* %x_q4, align 4, !tbaa !6
  %shr = ashr i32 %20, 4
  %arrayidx = getelementptr inbounds i16, i16* %19, i32 %shr
  store i16* %arrayidx, i16** %src_x, align 4, !tbaa !2
  %21 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load [8 x i16]*, [8 x i16]** %x_filters.addr, align 4, !tbaa !2
  %23 = load i32, i32* %x_q4, align 4, !tbaa !6
  %and = and i32 %23, 15
  %arrayidx7 = getelementptr inbounds [8 x i16], [8 x i16]* %22, i32 %and
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx7, i32 0, i32 0
  store i16* %arraydecay, i16** %x_filter, align 4, !tbaa !2
  %24 = bitcast i32* %rounding to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load i16*, i16** %src_x, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i16, i16* %25, i32 3
  %26 = load i16, i16* %arrayidx8, align 2, !tbaa !9
  %conv = zext i16 %26 to i32
  %shl9 = shl i32 %conv, 7
  %27 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %add10 = add nsw i32 %27, 7
  %sub11 = sub nsw i32 %add10, 1
  %shl12 = shl i32 1, %sub11
  %add13 = add nsw i32 %shl9, %shl12
  store i32 %add13, i32* %rounding, align 4, !tbaa !6
  %28 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #4
  %29 = load i16*, i16** %src_x, align 4, !tbaa !2
  %30 = load i16*, i16** %x_filter, align 4, !tbaa !2
  %call = call i32 @highbd_horz_scalar_product(i16* %29, i16* %30)
  %31 = load i32, i32* %rounding, align 4, !tbaa !6
  %add14 = add nsw i32 %call, %31
  store i32 %add14, i32* %sum, align 4, !tbaa !6
  %32 = load i32, i32* %sum, align 4, !tbaa !6
  %33 = load i32, i32* %round0_bits.addr, align 4, !tbaa !6
  %shl15 = shl i32 1, %33
  %shr16 = ashr i32 %shl15, 1
  %add17 = add nsw i32 %32, %shr16
  %34 = load i32, i32* %round0_bits.addr, align 4, !tbaa !6
  %shr18 = ashr i32 %add17, %34
  %35 = load i32, i32* %extraprec_clamp_limit, align 4, !tbaa !6
  %sub19 = sub nsw i32 %35, 1
  %call20 = call i32 @clamp(i32 %shr18, i32 0, i32 %sub19)
  %conv21 = trunc i32 %call20 to i16
  %36 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %37 = load i32, i32* %x, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds i16, i16* %36, i32 %37
  store i16 %conv21, i16* %arrayidx22, align 2, !tbaa !9
  %38 = load i32, i32* %x_step_q4.addr, align 4, !tbaa !6
  %39 = load i32, i32* %x_q4, align 4, !tbaa !6
  %add23 = add nsw i32 %39, %38
  store i32 %add23, i32* %x_q4, align 4, !tbaa !6
  %40 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %rounding to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i16** %x_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i16** %src_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %44 = load i32, i32* %x, align 4, !tbaa !6
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %x, align 4, !tbaa !6
  br label %for.cond3

for.end:                                          ; preds = %for.cond.cleanup5
  %45 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %46 = load i16*, i16** %src, align 4, !tbaa !2
  %add.ptr24 = getelementptr inbounds i16, i16* %46, i32 %45
  store i16* %add.ptr24, i16** %src, align 4, !tbaa !2
  %47 = load i32, i32* %dst_stride.addr, align 4, !tbaa !26
  %48 = load i16*, i16** %dst.addr, align 4, !tbaa !2
  %add.ptr25 = getelementptr inbounds i16, i16* %48, i32 %47
  store i16* %add.ptr25, i16** %dst.addr, align 4, !tbaa !2
  %49 = bitcast i32* %x_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  br label %for.inc26

for.inc26:                                        ; preds = %for.end
  %50 = load i32, i32* %y, align 4, !tbaa !6
  %inc27 = add nsw i32 %50, 1
  store i32 %inc27, i32* %y, align 4, !tbaa !6
  br label %for.cond

for.end28:                                        ; preds = %for.cond.cleanup
  %51 = bitcast i16** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %extraprec_clamp_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  ret void
}

; Function Attrs: nounwind
define internal void @highbd_convolve_add_src_vert_hip(i16* %src, i32 %src_stride, i8* %dst8, i32 %dst_stride, [8 x i16]* %y_filters, i32 %y0_q4, i32 %y_step_q4, i32 %w, i32 %h, i32 %round1_bits, i32 %bd) #0 {
entry:
  %src.addr = alloca i16*, align 4
  %src_stride.addr = alloca i32, align 4
  %dst8.addr = alloca i8*, align 4
  %dst_stride.addr = alloca i32, align 4
  %y_filters.addr = alloca [8 x i16]*, align 4
  %y0_q4.addr = alloca i32, align 4
  %y_step_q4.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %round1_bits.addr = alloca i32, align 4
  %bd.addr = alloca i32, align 4
  %dst = alloca i16*, align 4
  %x = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %y_q4 = alloca i32, align 4
  %y = alloca i32, align 4
  %src_y = alloca i16*, align 4
  %y_filter = alloca i16*, align 4
  %rounding = alloca i32, align 4
  %sum = alloca i32, align 4
  store i16* %src, i16** %src.addr, align 4, !tbaa !2
  store i32 %src_stride, i32* %src_stride.addr, align 4, !tbaa !26
  store i8* %dst8, i8** %dst8.addr, align 4, !tbaa !2
  store i32 %dst_stride, i32* %dst_stride.addr, align 4, !tbaa !26
  store [8 x i16]* %y_filters, [8 x i16]** %y_filters.addr, align 4, !tbaa !2
  store i32 %y0_q4, i32* %y0_q4.addr, align 4, !tbaa !6
  store i32 %y_step_q4, i32* %y_step_q4.addr, align 4, !tbaa !6
  store i32 %w, i32* %w.addr, align 4, !tbaa !6
  store i32 %h, i32* %h.addr, align 4, !tbaa !6
  store i32 %round1_bits, i32* %round1_bits.addr, align 4, !tbaa !6
  store i32 %bd, i32* %bd.addr, align 4, !tbaa !6
  %0 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8*, i8** %dst8.addr, align 4, !tbaa !2
  %2 = ptrtoint i8* %1 to i32
  %shl = shl i32 %2, 1
  %3 = inttoptr i32 %shl to i16*
  store i16* %3, i16** %dst, align 4, !tbaa !2
  %4 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %mul = mul nsw i32 %4, 3
  %5 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %idx.neg = sub i32 0, %mul
  %add.ptr = getelementptr inbounds i16, i16* %5, i32 %idx.neg
  store i16* %add.ptr, i16** %src.addr, align 4, !tbaa !2
  %6 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  store i32 0, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc22, %entry
  %7 = load i32, i32* %x, align 4, !tbaa !6
  %8 = load i32, i32* %w.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %7, %8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %9 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  br label %for.end24

for.body:                                         ; preds = %for.cond
  %10 = bitcast i32* %y_q4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load i32, i32* %y0_q4.addr, align 4, !tbaa !6
  store i32 %11, i32* %y_q4, align 4, !tbaa !6
  %12 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  store i32 0, i32* %y, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %y, align 4, !tbaa !6
  %14 = load i32, i32* %h.addr, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %13, %14
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %15 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %16 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %18 = load i32, i32* %y_q4, align 4, !tbaa !6
  %shr = ashr i32 %18, 4
  %19 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %mul5 = mul nsw i32 %shr, %19
  %arrayidx = getelementptr inbounds i16, i16* %17, i32 %mul5
  store i16* %arrayidx, i16** %src_y, align 4, !tbaa !2
  %20 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load [8 x i16]*, [8 x i16]** %y_filters.addr, align 4, !tbaa !2
  %22 = load i32, i32* %y_q4, align 4, !tbaa !6
  %and = and i32 %22, 15
  %arrayidx6 = getelementptr inbounds [8 x i16], [8 x i16]* %21, i32 %and
  %arraydecay = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx6, i32 0, i32 0
  store i16* %arraydecay, i16** %y_filter, align 4, !tbaa !2
  %23 = bitcast i32* %rounding to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load i16*, i16** %src_y, align 4, !tbaa !2
  %25 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %mul7 = mul nsw i32 3, %25
  %arrayidx8 = getelementptr inbounds i16, i16* %24, i32 %mul7
  %26 = load i16, i16* %arrayidx8, align 2, !tbaa !9
  %conv = zext i16 %26 to i32
  %shl9 = shl i32 %conv, 7
  %27 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %28 = load i32, i32* %round1_bits.addr, align 4, !tbaa !6
  %add = add nsw i32 %27, %28
  %sub = sub nsw i32 %add, 1
  %shl10 = shl i32 1, %sub
  %sub11 = sub nsw i32 %shl9, %shl10
  store i32 %sub11, i32* %rounding, align 4, !tbaa !6
  %29 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load i16*, i16** %src_y, align 4, !tbaa !2
  %31 = load i32, i32* %src_stride.addr, align 4, !tbaa !26
  %32 = load i16*, i16** %y_filter, align 4, !tbaa !2
  %call = call i32 @highbd_vert_scalar_product(i16* %30, i32 %31, i16* %32)
  %33 = load i32, i32* %rounding, align 4, !tbaa !6
  %add12 = add nsw i32 %call, %33
  store i32 %add12, i32* %sum, align 4, !tbaa !6
  %34 = load i32, i32* %sum, align 4, !tbaa !6
  %35 = load i32, i32* %round1_bits.addr, align 4, !tbaa !6
  %shl13 = shl i32 1, %35
  %shr14 = ashr i32 %shl13, 1
  %add15 = add nsw i32 %34, %shr14
  %36 = load i32, i32* %round1_bits.addr, align 4, !tbaa !6
  %shr16 = ashr i32 %add15, %36
  %37 = load i32, i32* %bd.addr, align 4, !tbaa !6
  %call17 = call zeroext i16 @clip_pixel_highbd(i32 %shr16, i32 %37)
  %38 = load i16*, i16** %dst, align 4, !tbaa !2
  %39 = load i32, i32* %y, align 4, !tbaa !6
  %40 = load i32, i32* %dst_stride.addr, align 4, !tbaa !26
  %mul18 = mul nsw i32 %39, %40
  %arrayidx19 = getelementptr inbounds i16, i16* %38, i32 %mul18
  store i16 %call17, i16* %arrayidx19, align 2, !tbaa !9
  %41 = load i32, i32* %y_step_q4.addr, align 4, !tbaa !6
  %42 = load i32, i32* %y_q4, align 4, !tbaa !6
  %add20 = add nsw i32 %42, %41
  store i32 %add20, i32* %y_q4, align 4, !tbaa !6
  %43 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i32* %rounding to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i16** %y_filter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i16** %src_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %47 = load i32, i32* %y, align 4, !tbaa !6
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %y, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %48 = load i16*, i16** %src.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %48, i32 1
  store i16* %incdec.ptr, i16** %src.addr, align 4, !tbaa !2
  %49 = load i16*, i16** %dst, align 4, !tbaa !2
  %incdec.ptr21 = getelementptr inbounds i16, i16* %49, i32 1
  store i16* %incdec.ptr21, i16** %dst, align 4, !tbaa !2
  %50 = bitcast i32* %y_q4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %51 = load i32, i32* %x, align 4, !tbaa !6
  %inc23 = add nsw i32 %51, 1
  store i32 %inc23, i32* %x, align 4, !tbaa !6
  br label %for.cond

for.end24:                                        ; preds = %for.cond.cleanup
  %52 = bitcast i16** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !6
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %high, i32* %high.addr, align 4, !tbaa !6
  %0 = load i32, i32* %value.addr, align 4, !tbaa !6
  %1 = load i32, i32* %low.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !6
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !6
  %4 = load i32, i32* %high.addr, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !6
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal i32 @horz_scalar_product(i8* %a, i16* %b) #2 {
entry:
  %a.addr = alloca i8*, align 4
  %b.addr = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i8* %a, i8** %a.addr, align 4, !tbaa !2
  store i16* %b, i16** %b.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %sum, align 4, !tbaa !6
  %1 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %k, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %a.addr, align 4, !tbaa !2
  %5 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  %6 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %6 to i32
  %7 = load i16*, i16** %b.addr, align 4, !tbaa !2
  %8 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds i16, i16* %7, i32 %8
  %9 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  %conv2 = sext i16 %9 to i32
  %mul = mul nsw i32 %conv, %conv2
  %10 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %10, %mul
  store i32 %add, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %12 = load i32, i32* %sum, align 4, !tbaa !6
  %13 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  ret i32 %12
}

; Function Attrs: inlinehint nounwind
define internal i32 @highbd_vert_scalar_product(i16* %a, i32 %a_stride, i16* %b) #2 {
entry:
  %a.addr = alloca i16*, align 4
  %a_stride.addr = alloca i32, align 4
  %b.addr = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i16* %a, i16** %a.addr, align 4, !tbaa !2
  store i32 %a_stride, i32* %a_stride.addr, align 4, !tbaa !26
  store i16* %b, i16** %b.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %sum, align 4, !tbaa !6
  %1 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %k, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i16*, i16** %a.addr, align 4, !tbaa !2
  %5 = load i32, i32* %k, align 4, !tbaa !6
  %6 = load i32, i32* %a_stride.addr, align 4, !tbaa !26
  %mul = mul nsw i32 %5, %6
  %arrayidx = getelementptr inbounds i16, i16* %4, i32 %mul
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = zext i16 %7 to i32
  %8 = load i16*, i16** %b.addr, align 4, !tbaa !2
  %9 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds i16, i16* %8, i32 %9
  %10 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  %conv2 = sext i16 %10 to i32
  %mul3 = mul nsw i32 %conv, %conv2
  %11 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %11, %mul3
  store i32 %add, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %13 = load i32, i32* %sum, align 4, !tbaa !6
  %14 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  ret i32 %13
}

; Function Attrs: inlinehint nounwind
define internal i32 @highbd_horz_scalar_product(i16* %a, i16* %b) #2 {
entry:
  %a.addr = alloca i16*, align 4
  %b.addr = alloca i16*, align 4
  %sum = alloca i32, align 4
  %k = alloca i32, align 4
  store i16* %a, i16** %a.addr, align 4, !tbaa !2
  store i16* %b, i16** %b.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %sum, align 4, !tbaa !6
  %1 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %k, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i16*, i16** %a.addr, align 4, !tbaa !2
  %5 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %4, i32 %5
  %6 = load i16, i16* %arrayidx, align 2, !tbaa !9
  %conv = zext i16 %6 to i32
  %7 = load i16*, i16** %b.addr, align 4, !tbaa !2
  %8 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds i16, i16* %7, i32 %8
  %9 = load i16, i16* %arrayidx1, align 2, !tbaa !9
  %conv2 = sext i16 %9 to i32
  %mul = mul nsw i32 %conv, %conv2
  %10 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add nsw i32 %10, %mul
  store i32 %add, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %12 = load i32, i32* %sum, align 4, !tbaa !6
  %13 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  ret i32 %12
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !12, i64 0}
!12 = !{!"double", !4, i64 0}
!13 = !{!14, !10, i64 4}
!14 = !{!"InterpFilterParams", !3, i64 0, !10, i64 4, !10, i64 6, !4, i64 8}
!15 = !{!16, !7, i64 12}
!16 = !{!"ConvolveParams", !7, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40}
!17 = !{!16, !7, i64 16}
!18 = !{!14, !3, i64 0}
!19 = !{!16, !3, i64 4}
!20 = !{!16, !7, i64 8}
!21 = !{!16, !7, i64 0}
!22 = !{!16, !7, i64 32}
!23 = !{!16, !7, i64 36}
!24 = !{!16, !7, i64 40}
!25 = !{!16, !7, i64 24}
!26 = !{!27, !27, i64 0}
!27 = !{!"long", !4, i64 0}
