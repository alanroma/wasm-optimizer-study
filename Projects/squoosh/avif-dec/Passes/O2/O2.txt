[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.0204352 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000731545 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0460773 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0404364 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00700417 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.042282 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0100344 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.029659 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0125869 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0900986 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0606043 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0125036 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0351967 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0537901 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.13544 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0610129 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0122076 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0552737 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0121293 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0581359 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0373186 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0371714 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0067717 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0356773 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.028351 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0371537 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0426002 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0583698 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0940241 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.117439 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0040039 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   5.3459e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00481521 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.0070887 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      3.2934e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00640012 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0582618 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 1.37117 seconds.
[PassRunner] (final validation)
