[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    5.2677e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00417911 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    5.1004e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00612209 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0057646 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000963219 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0461567 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0407843 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00706772 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0425629 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.153704 seconds.
[PassRunner] (final validation)
