[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    6.5971e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00906661 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    7.1646e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.0146186 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0121561 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.00151246 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.112855 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.105711 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0167126 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0960101 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0234178 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.071228 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.0155049 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0312657 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.234259 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.145557 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0285458 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0898284 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.185778 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.344744 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.147329 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.028248 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.192428 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0280041 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.138949 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0799416 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.091778 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0162631 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0775179 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0688285 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0895437 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.165509 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.138674 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.30605 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.118365 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0109781 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   0.00010248 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.0103457 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 3.24776 seconds.
[PassRunner] (final validation)
