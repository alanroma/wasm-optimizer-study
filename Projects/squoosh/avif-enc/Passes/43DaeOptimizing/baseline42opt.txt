[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    7.0518e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.0091224 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    9.1291e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.0147485 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0124814 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.00144331 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.111933 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.105401 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0169384 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0965388 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0233304 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0712383 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.0154323 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0310981 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.232006 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.142917 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0284542 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0885565 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.184181 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.338799 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.145732 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0282039 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.188311 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0280767 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.139766 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.080512 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0924796 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0164584 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0779046 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0695414 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0900345 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.16674 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.138776 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 2.78732 seconds.
[PassRunner] (final validation)
