[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    9.3612e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00910846 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    9.0126e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.0146937 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0120813 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.00154434 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.112012 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.105396 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0166528 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0968491 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0233444 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0711519 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.015406 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0308061 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.230999 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.143221 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0292135 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0890561 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.184966 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.340408 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.145994 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0281418 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.188461 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0280124 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.138987 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0796794 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0925268 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0161324 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0779212 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0706241 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0886764 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.163651 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.137224 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.298969 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.128423 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0107594 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   9.4452e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.0102162 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.0162377 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      5.1751e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.0161264 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.13314 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-debug...                    0.0472878 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-producers...                7.3188e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 3.4445 seconds.
[PassRunner] (final validation)
