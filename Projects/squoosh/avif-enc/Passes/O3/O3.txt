[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.0439767 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.00170676 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.314882 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.1136 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.104968 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0167206 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.100458 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0236047 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.464849 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0393721 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.250682 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.150586 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0356983 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.09005 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                   0.256972 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.239664 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.340852 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.150118 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0291533 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.188904 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0289378 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.143248 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                   0.0319496 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.085252 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.094364 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0165257 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0809382 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.547688 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.09276 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.168499 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.141731 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.454271 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.42723 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0090038 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   9.7171e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.0104888 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.0163301 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      4.8714e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.0144009 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.409867 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 5.73045 seconds.
[PassRunner] (final validation)
