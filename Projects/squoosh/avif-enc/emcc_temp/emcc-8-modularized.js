
var avif_enc = (function() {
  var _scriptDir = typeof document !== 'undefined' && document.currentScript ? document.currentScript.src : undefined;
  if (typeof __filename !== 'undefined') _scriptDir = _scriptDir || __filename;
  return (
function(avif_enc) {
  avif_enc = avif_enc || {};

/*

 Copyright 2010 The Emscripten Authors
 SPDX-License-Identifier: MIT

 Copyright 2019 The Emscripten Authors
 SPDX-License-Identifier: MIT

 Copyright 2017 The Emscripten Authors
 SPDX-License-Identifier: MIT

 Copyright 2020 The Emscripten Authors
 SPDX-License-Identifier: MIT

 Copyright 2015 The Emscripten Authors
 SPDX-License-Identifier: MIT
*/
var b;
b || (b = {__EMSCRIPTEN_PRIVATE_MODULE_EXPORT_NAME_SUBSTITUTION__:1});
var aa;
b.ready = new Promise(function(a) {
  aa = a;
});
Object.getOwnPropertyDescriptor(b.ready, "_main") || (Object.defineProperty(b.ready, "_main", {configurable:!0, get:function() {
  f("You are getting _main on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_main", {configurable:!0, set:function() {
  f("You are setting _main on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_malloc") || (Object.defineProperty(b.ready, "_malloc", {configurable:!0, get:function() {
  f("You are getting _malloc on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_malloc", {configurable:!0, set:function() {
  f("You are setting _malloc on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_free") || (Object.defineProperty(b.ready, "_free", {configurable:!0, get:function() {
  f("You are getting _free on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_free", {configurable:!0, set:function() {
  f("You are setting _free on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_stackSave") || (Object.defineProperty(b.ready, "_stackSave", {configurable:!0, get:function() {
  f("You are getting _stackSave on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_stackSave", {configurable:!0, set:function() {
  f("You are setting _stackSave on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_stackRestore") || (Object.defineProperty(b.ready, "_stackRestore", {configurable:!0, get:function() {
  f("You are getting _stackRestore on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_stackRestore", {configurable:!0, set:function() {
  f("You are setting _stackRestore on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_stackAlloc") || (Object.defineProperty(b.ready, "_stackAlloc", {configurable:!0, get:function() {
  f("You are getting _stackAlloc on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_stackAlloc", {configurable:!0, set:function() {
  f("You are setting _stackAlloc on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "___data_end") || (Object.defineProperty(b.ready, "___data_end", {configurable:!0, get:function() {
  f("You are getting ___data_end on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "___data_end", {configurable:!0, set:function() {
  f("You are setting ___data_end on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "___wasm_call_ctors") || (Object.defineProperty(b.ready, "___wasm_call_ctors", {configurable:!0, get:function() {
  f("You are getting ___wasm_call_ctors on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "___wasm_call_ctors", {configurable:!0, set:function() {
  f("You are setting ___wasm_call_ctors on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_fflush") || (Object.defineProperty(b.ready, "_fflush", {configurable:!0, get:function() {
  f("You are getting _fflush on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_fflush", {configurable:!0, set:function() {
  f("You are setting _fflush on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "___errno_location") || (Object.defineProperty(b.ready, "___errno_location", {configurable:!0, get:function() {
  f("You are getting ___errno_location on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "___errno_location", {configurable:!0, set:function() {
  f("You are setting ___errno_location on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_setThrew") || (Object.defineProperty(b.ready, "_setThrew", {configurable:!0, get:function() {
  f("You are getting _setThrew on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_setThrew", {configurable:!0, set:function() {
  f("You are setting _setThrew on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_realloc") || (Object.defineProperty(b.ready, "_realloc", {configurable:!0, get:function() {
  f("You are getting _realloc on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_realloc", {configurable:!0, set:function() {
  f("You are setting _realloc on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_testSetjmp") || (Object.defineProperty(b.ready, "_testSetjmp", {configurable:!0, get:function() {
  f("You are getting _testSetjmp on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_testSetjmp", {configurable:!0, set:function() {
  f("You are setting _testSetjmp on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "_saveSetjmp") || (Object.defineProperty(b.ready, "_saveSetjmp", {configurable:!0, get:function() {
  f("You are getting _saveSetjmp on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "_saveSetjmp", {configurable:!0, set:function() {
  f("You are setting _saveSetjmp on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
Object.getOwnPropertyDescriptor(b.ready, "onRuntimeInitialized") || (Object.defineProperty(b.ready, "onRuntimeInitialized", {configurable:!0, get:function() {
  f("You are getting onRuntimeInitialized on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}), Object.defineProperty(b.ready, "onRuntimeInitialized", {configurable:!0, set:function() {
  f("You are setting onRuntimeInitialized on the Promise object, instead of the instance. Use .then() to get called back with the instance, see the MODULARIZE docs in src/settings.js");
}}));
var ba = {}, n;
for (n in b) {
  b.hasOwnProperty(n) && (ba[n] = b[n]);
}
var ca = !1, r = !1, da = !1, ea = !1;
ca = "object" === typeof window;
r = "function" === typeof importScripts;
da = "object" === typeof process && "object" === typeof process.versions && "string" === typeof process.versions.node;
ea = !ca && !da && !r;
if (b.ENVIRONMENT) {
  throw Error("Module.ENVIRONMENT has been deprecated. To force the environment, use the ENVIRONMENT compile-time option (for example, -s ENVIRONMENT=web or -s ENVIRONMENT=node)");
}
var u = "", fa, ha, ia, ja;
if (da) {
  u = r ? require("path").dirname(u) + "/" : __dirname + "/", fa = function(a, c) {
    ia || (ia = require("fs"));
    ja || (ja = require("path"));
    a = ja.normalize(a);
    return ia.readFileSync(a, c ? null : "utf8");
  }, ha = function(a) {
    a = fa(a, !0);
    a.buffer || (a = new Uint8Array(a));
    assert(a.buffer);
    return a;
  }, 1 < process.argv.length && process.argv[1].replace(/\\/g, "/"), process.argv.slice(2), process.on("uncaughtException", function(a) {
    throw a;
  }), process.on("unhandledRejection", f), b.inspect = function() {
    return "[Emscripten Module object]";
  };
} else {
  if (ea) {
    "undefined" != typeof read && (fa = function(a) {
      return read(a);
    }), ha = function(a) {
      if ("function" === typeof readbuffer) {
        return new Uint8Array(readbuffer(a));
      }
      a = read(a, "binary");
      assert("object" === typeof a);
      return a;
    }, "undefined" !== typeof print && ("undefined" === typeof console && (console = {}), console.log = print, console.warn = console.error = "undefined" !== typeof printErr ? printErr : print);
  } else {
    if (ca || r) {
      r ? u = self.location.href : document.currentScript && (u = document.currentScript.src), _scriptDir && (u = _scriptDir), 0 !== u.indexOf("blob:") ? u = u.substr(0, u.lastIndexOf("/") + 1) : u = "", fa = function(a) {
        var c = new XMLHttpRequest;
        c.open("GET", a, !1);
        c.send(null);
        return c.responseText;
      }, r && (ha = function(a) {
        var c = new XMLHttpRequest;
        c.open("GET", a, !1);
        c.responseType = "arraybuffer";
        c.send(null);
        return new Uint8Array(c.response);
      });
    } else {
      throw Error("environment detection error");
    }
  }
}
var ka = b.print || console.log.bind(console), w = b.printErr || console.warn.bind(console);
for (n in ba) {
  ba.hasOwnProperty(n) && (b[n] = ba[n]);
}
ba = null;
Object.getOwnPropertyDescriptor(b, "arguments") || Object.defineProperty(b, "arguments", {configurable:!0, get:function() {
  f("Module.arguments has been replaced with plain arguments_ (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
Object.getOwnPropertyDescriptor(b, "thisProgram") || Object.defineProperty(b, "thisProgram", {configurable:!0, get:function() {
  f("Module.thisProgram has been replaced with plain thisProgram (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
Object.getOwnPropertyDescriptor(b, "quit") || Object.defineProperty(b, "quit", {configurable:!0, get:function() {
  f("Module.quit has been replaced with plain quit_ (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
assert("undefined" === typeof b.memoryInitializerPrefixURL, "Module.memoryInitializerPrefixURL option was removed, use Module.locateFile instead");
assert("undefined" === typeof b.pthreadMainPrefixURL, "Module.pthreadMainPrefixURL option was removed, use Module.locateFile instead");
assert("undefined" === typeof b.cdInitializerPrefixURL, "Module.cdInitializerPrefixURL option was removed, use Module.locateFile instead");
assert("undefined" === typeof b.filePackagePrefixURL, "Module.filePackagePrefixURL option was removed, use Module.locateFile instead");
assert("undefined" === typeof b.read, "Module.read option was removed (modify read_ in JS)");
assert("undefined" === typeof b.readAsync, "Module.readAsync option was removed (modify readAsync in JS)");
assert("undefined" === typeof b.readBinary, "Module.readBinary option was removed (modify readBinary in JS)");
assert("undefined" === typeof b.setWindowTitle, "Module.setWindowTitle option was removed (modify setWindowTitle in JS)");
assert("undefined" === typeof b.TOTAL_MEMORY, "Module.TOTAL_MEMORY has been renamed Module.INITIAL_MEMORY");
Object.getOwnPropertyDescriptor(b, "read") || Object.defineProperty(b, "read", {configurable:!0, get:function() {
  f("Module.read has been replaced with plain read_ (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
Object.getOwnPropertyDescriptor(b, "readAsync") || Object.defineProperty(b, "readAsync", {configurable:!0, get:function() {
  f("Module.readAsync has been replaced with plain readAsync (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
Object.getOwnPropertyDescriptor(b, "readBinary") || Object.defineProperty(b, "readBinary", {configurable:!0, get:function() {
  f("Module.readBinary has been replaced with plain readBinary (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
Object.getOwnPropertyDescriptor(b, "setWindowTitle") || Object.defineProperty(b, "setWindowTitle", {configurable:!0, get:function() {
  f("Module.setWindowTitle has been replaced with plain setWindowTitle (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
function la(a) {
  ma || (ma = {});
  ma[a] || (ma[a] = 1, w(a));
}
var ma, na = 0, oa;
b.wasmBinary && (oa = b.wasmBinary);
Object.getOwnPropertyDescriptor(b, "wasmBinary") || Object.defineProperty(b, "wasmBinary", {configurable:!0, get:function() {
  f("Module.wasmBinary has been replaced with plain wasmBinary (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
var noExitRuntime;
b.noExitRuntime && (noExitRuntime = b.noExitRuntime);
Object.getOwnPropertyDescriptor(b, "noExitRuntime") || Object.defineProperty(b, "noExitRuntime", {configurable:!0, get:function() {
  f("Module.noExitRuntime has been replaced with plain noExitRuntime (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
"object" !== typeof WebAssembly && f("No WebAssembly support found. Build with -s WASM=0 to target JavaScript instead.");
var y, pa = new WebAssembly.Table({initial:899, maximum:899, element:"anyfunc"}), qa = !1;
function assert(a, c) {
  a || f("Assertion failed: " + c);
}
var ra = "undefined" !== typeof TextDecoder ? new TextDecoder("utf8") : void 0;
function sa(a, c, d) {
  var e = c + d;
  for (d = c; a[d] && !(d >= e);) {
    ++d;
  }
  if (16 < d - c && a.subarray && ra) {
    return ra.decode(a.subarray(c, d));
  }
  for (e = ""; c < d;) {
    var g = a[c++];
    if (g & 128) {
      var h = a[c++] & 63;
      if (192 == (g & 224)) {
        e += String.fromCharCode((g & 31) << 6 | h);
      } else {
        var k = a[c++] & 63;
        224 == (g & 240) ? g = (g & 15) << 12 | h << 6 | k : (240 != (g & 248) && la("Invalid UTF-8 leading byte 0x" + g.toString(16) + " encountered when deserializing a UTF-8 string on the asm.js/wasm heap to a JS string!"), g = (g & 7) << 18 | h << 12 | k << 6 | a[c++] & 63);
        65536 > g ? e += String.fromCharCode(g) : (g -= 65536, e += String.fromCharCode(55296 | g >> 10, 56320 | g & 1023));
      }
    } else {
      e += String.fromCharCode(g);
    }
  }
  return e;
}
function ta(a, c, d, e) {
  if (!(0 < e)) {
    return 0;
  }
  var g = d;
  e = d + e - 1;
  for (var h = 0; h < a.length; ++h) {
    var k = a.charCodeAt(h);
    if (55296 <= k && 57343 >= k) {
      var l = a.charCodeAt(++h);
      k = 65536 + ((k & 1023) << 10) | l & 1023;
    }
    if (127 >= k) {
      if (d >= e) {
        break;
      }
      c[d++] = k;
    } else {
      if (2047 >= k) {
        if (d + 1 >= e) {
          break;
        }
        c[d++] = 192 | k >> 6;
      } else {
        if (65535 >= k) {
          if (d + 2 >= e) {
            break;
          }
          c[d++] = 224 | k >> 12;
        } else {
          if (d + 3 >= e) {
            break;
          }
          2097152 <= k && la("Invalid Unicode code point 0x" + k.toString(16) + " encountered when serializing a JS string to an UTF-8 string on the asm.js/wasm heap! (Valid unicode code points should be in range 0-0x1FFFFF).");
          c[d++] = 240 | k >> 18;
          c[d++] = 128 | k >> 12 & 63;
        }
        c[d++] = 128 | k >> 6 & 63;
      }
      c[d++] = 128 | k & 63;
    }
  }
  c[d] = 0;
  return d - g;
}
function ua(a, c, d) {
  assert("number" == typeof d, "stringToUTF8(str, outPtr, maxBytesToWrite) is missing the third parameter that specifies the length of the output buffer!");
  ta(a, B, c, d);
}
function va(a) {
  for (var c = 0, d = 0; d < a.length; ++d) {
    var e = a.charCodeAt(d);
    55296 <= e && 57343 >= e && (e = 65536 + ((e & 1023) << 10) | a.charCodeAt(++d) & 1023);
    127 >= e ? ++c : c = 2047 >= e ? c + 2 : 65535 >= e ? c + 3 : c + 4;
  }
  return c;
}
var xa = "undefined" !== typeof TextDecoder ? new TextDecoder("utf-16le") : void 0;
function ya(a, c) {
  assert(0 == a % 2, "Pointer passed to UTF16ToString must be aligned to two bytes!");
  var d = a >> 1;
  for (var e = d + c / 2; !(d >= e) && za[d];) {
    ++d;
  }
  d <<= 1;
  if (32 < d - a && xa) {
    return xa.decode(B.subarray(a, d));
  }
  d = 0;
  for (e = "";;) {
    var g = C[a + 2 * d >> 1];
    if (0 == g || d == c / 2) {
      return e;
    }
    ++d;
    e += String.fromCharCode(g);
  }
}
function Aa(a, c, d) {
  assert(0 == c % 2, "Pointer passed to stringToUTF16 must be aligned to two bytes!");
  assert("number" == typeof d, "stringToUTF16(str, outPtr, maxBytesToWrite) is missing the third parameter that specifies the length of the output buffer!");
  void 0 === d && (d = 2147483647);
  if (2 > d) {
    return 0;
  }
  d -= 2;
  var e = c;
  d = d < 2 * a.length ? d / 2 : a.length;
  for (var g = 0; g < d; ++g) {
    C[c >> 1] = a.charCodeAt(g), c += 2;
  }
  C[c >> 1] = 0;
  return c - e;
}
function Ba(a) {
  return 2 * a.length;
}
function Ca(a, c) {
  assert(0 == a % 4, "Pointer passed to UTF32ToString must be aligned to four bytes!");
  for (var d = 0, e = ""; !(d >= c / 4);) {
    var g = D[a + 4 * d >> 2];
    if (0 == g) {
      break;
    }
    ++d;
    65536 <= g ? (g -= 65536, e += String.fromCharCode(55296 | g >> 10, 56320 | g & 1023)) : e += String.fromCharCode(g);
  }
  return e;
}
function Da(a, c, d) {
  assert(0 == c % 4, "Pointer passed to stringToUTF32 must be aligned to four bytes!");
  assert("number" == typeof d, "stringToUTF32(str, outPtr, maxBytesToWrite) is missing the third parameter that specifies the length of the output buffer!");
  void 0 === d && (d = 2147483647);
  if (4 > d) {
    return 0;
  }
  var e = c;
  d = e + d - 4;
  for (var g = 0; g < a.length; ++g) {
    var h = a.charCodeAt(g);
    if (55296 <= h && 57343 >= h) {
      var k = a.charCodeAt(++g);
      h = 65536 + ((h & 1023) << 10) | k & 1023;
    }
    D[c >> 2] = h;
    c += 4;
    if (c + 4 > d) {
      break;
    }
  }
  D[c >> 2] = 0;
  return c - e;
}
function Ea(a) {
  for (var c = 0, d = 0; d < a.length; ++d) {
    var e = a.charCodeAt(d);
    55296 <= e && 57343 >= e && ++d;
    c += 4;
  }
  return c;
}
var E, F, B, C, za, D, G, Fa, Ga;
function Ha(a) {
  E = a;
  b.HEAP8 = F = new Int8Array(a);
  b.HEAP16 = C = new Int16Array(a);
  b.HEAP32 = D = new Int32Array(a);
  b.HEAPU8 = B = new Uint8Array(a);
  b.HEAPU16 = za = new Uint16Array(a);
  b.HEAPU32 = G = new Uint32Array(a);
  b.HEAPF32 = Fa = new Float32Array(a);
  b.HEAPF64 = Ga = new Float64Array(a);
}
assert(!0, "stack must start aligned");
assert(!0, "heap must start aligned");
b.TOTAL_STACK && assert(5242880 === b.TOTAL_STACK, "the stack size can no longer be determined at runtime");
var Ia = b.INITIAL_MEMORY || 16777216;
Object.getOwnPropertyDescriptor(b, "INITIAL_MEMORY") || Object.defineProperty(b, "INITIAL_MEMORY", {configurable:!0, get:function() {
  f("Module.INITIAL_MEMORY has been replaced with plain INITIAL_INITIAL_MEMORY (the initial value can be provided on Module, but after startup the value is only looked for on a local variable of that name)");
}});
assert(5242880 <= Ia, "INITIAL_MEMORY should be larger than TOTAL_STACK, was " + Ia + "! (TOTAL_STACK=5242880)");
assert("undefined" !== typeof Int32Array && "undefined" !== typeof Float64Array && void 0 !== Int32Array.prototype.subarray && void 0 !== Int32Array.prototype.set, "JS engine does not provide full typed array support");
b.wasmMemory ? y = b.wasmMemory : y = new WebAssembly.Memory({initial:Ia / 65536, maximum:32768});
y && (E = y.buffer);
Ia = E.byteLength;
assert(0 === Ia % 65536);
assert(!0);
Ha(E);
D[223320] = 6136320;
function Ja() {
  assert(!0);
  G[223361] = 34821223;
  G[223362] = 2310721022;
  D[0] = 1668509029;
}
function Ka() {
  var a = G[223361], c = G[223362];
  34821223 == a && 2310721022 == c || f("Stack overflow! Stack cookie has been overwritten, expected hex dwords 0x89BACDFE and 0x2135467, but received 0x" + c.toString(16) + " " + a.toString(16));
  1668509029 !== D[0] && f("Runtime error: The application has corrupted its heap memory area (address zero)!");
}
var La = new Int16Array(1), Ma = new Int8Array(La.buffer);
La[0] = 25459;
if (115 !== Ma[0] || 99 !== Ma[1]) {
  throw "Runtime error: expected the system to be little-endian!";
}
function Na(a) {
  for (; 0 < a.length;) {
    var c = a.shift();
    if ("function" == typeof c) {
      c(b);
    } else {
      var d = c.O;
      "number" === typeof d ? void 0 === c.A ? b.dynCall_v(d) : b.dynCall_vi(d, c.A) : d(void 0 === c.A ? null : c.A);
    }
  }
}
var Oa = [], Pa = [], Qa = [], Ra = [], Sa = [], Ta = !1;
function Ua() {
  var a = b.preRun.shift();
  Oa.unshift(a);
}
assert(Math.imul, "This browser does not support Math.imul(), build with LEGACY_VM_SUPPORT or POLYFILL_OLD_MATH_FUNCTIONS to add in a polyfill");
assert(Math.fround, "This browser does not support Math.fround(), build with LEGACY_VM_SUPPORT or POLYFILL_OLD_MATH_FUNCTIONS to add in a polyfill");
assert(Math.clz32, "This browser does not support Math.clz32(), build with LEGACY_VM_SUPPORT or POLYFILL_OLD_MATH_FUNCTIONS to add in a polyfill");
assert(Math.trunc, "This browser does not support Math.trunc(), build with LEGACY_VM_SUPPORT or POLYFILL_OLD_MATH_FUNCTIONS to add in a polyfill");
var Va = Math.abs, Wa = Math.ceil, Xa = Math.floor, Ya = Math.min, I = 0, J = null, Za = null, $a = {};
function ab() {
  I++;
  b.monitorRunDependencies && b.monitorRunDependencies(I);
  assert(!$a["wasm-instantiate"]);
  $a["wasm-instantiate"] = 1;
  null === J && "undefined" !== typeof setInterval && (J = setInterval(function() {
    if (qa) {
      clearInterval(J), J = null;
    } else {
      var a = !1, c;
      for (c in $a) {
        a || (a = !0, w("still waiting on run dependencies:")), w("dependency: " + c);
      }
      a && w("(end of list)");
    }
  }, 10000));
}
b.preloadedImages = {};
b.preloadedAudios = {};
function f(a) {
  if (b.onAbort) {
    b.onAbort(a);
  }
  ka(a);
  w(a);
  qa = !0;
  a = "abort(" + a + ") at ";
  a: {
    var c = Error();
    if (!c.stack) {
      try {
        throw Error();
      } catch (d) {
        c = d;
      }
      if (!c.stack) {
        c = "(no stack trace available)";
        break a;
      }
    }
    c = c.stack.toString();
  }
  b.extraStackTrace && (c += "\n" + b.extraStackTrace());
  c = bb(c);
  throw new WebAssembly.RuntimeError(a + c);
}
function cb(a) {
  var c = K;
  return String.prototype.startsWith ? c.startsWith(a) : 0 === c.indexOf(a);
}
function db() {
  return cb("data:application/octet-stream;base64,");
}
function L(a) {
  return function() {
    var c = b.asm;
    assert(Ta, "native function `" + a + "` called before runtime initialization");
    assert(!0, "native function `" + a + "` called after runtime exit (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
    c[a] || assert(c[a], "exported native function `" + a + "` not found");
    return c[a].apply(null, arguments);
  };
}
var K = "avif_enc.wasm";
if (!db()) {
  var eb = K;
  K = b.locateFile ? b.locateFile(eb, u) : u + eb;
}
function fb() {
  try {
    if (oa) {
      return new Uint8Array(oa);
    }
    if (ha) {
      return ha(K);
    }
    throw "both async and sync fetching of the wasm failed";
  } catch (a) {
    f(a);
  }
}
function gb() {
  return oa || !ca && !r || "function" !== typeof fetch || cb("file://") ? new Promise(function(a) {
    a(fb());
  }) : fetch(K, {credentials:"same-origin"}).then(function(a) {
    if (!a.ok) {
      throw "failed to load wasm binary file at '" + K + "'";
    }
    return a.arrayBuffer();
  }).catch(function() {
    return fb();
  });
}
var hb, ib;
Pa.push({O:function() {
  jb();
}});
function bb(a) {
  return a.replace(/\b_Z[\w\d_]+/g, function(c) {
    la("warning: build with  -s DEMANGLE_SUPPORT=1  to link in libcxxabi demangling");
    return c === c ? c : c + " [" + c + "]";
  });
}
function kb(a, c) {
  for (var d = 0, e = a.length - 1; 0 <= e; e--) {
    var g = a[e];
    "." === g ? a.splice(e, 1) : ".." === g ? (a.splice(e, 1), d++) : d && (a.splice(e, 1), d--);
  }
  if (c) {
    for (; d; d--) {
      a.unshift("..");
    }
  }
  return a;
}
function lb(a) {
  var c = "/" === a.charAt(0), d = "/" === a.substr(-1);
  (a = kb(a.split("/").filter(function(e) {
    return !!e;
  }), !c).join("/")) || c || (a = ".");
  a && d && (a += "/");
  return (c ? "/" : "") + a;
}
function mb(a) {
  var c = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/.exec(a).slice(1);
  a = c[0];
  c = c[1];
  if (!a && !c) {
    return ".";
  }
  c && (c = c.substr(0, c.length - 1));
  return a + c;
}
function nb(a) {
  if ("/" === a) {
    return "/";
  }
  var c = a.lastIndexOf("/");
  return -1 === c ? a : a.substr(c + 1);
}
function ob() {
  for (var a = "", c = !1, d = arguments.length - 1; -1 <= d && !c; d--) {
    c = 0 <= d ? arguments[d] : "/";
    if ("string" !== typeof c) {
      throw new TypeError("Arguments to path.resolve must be strings");
    }
    if (!c) {
      return "";
    }
    a = c + "/" + a;
    c = "/" === c.charAt(0);
  }
  a = kb(a.split("/").filter(function(e) {
    return !!e;
  }), !c).join("/");
  return (c ? "/" : "") + a || ".";
}
var pb = [];
function qb(a, c) {
  pb[a] = {input:[], output:[], v:c};
  rb(a, sb);
}
var sb = {open:function(a) {
  var c = pb[a.node.rdev];
  if (!c) {
    throw new M(43);
  }
  a.tty = c;
  a.seekable = !1;
}, close:function(a) {
  a.tty.v.flush(a.tty);
}, flush:function(a) {
  a.tty.v.flush(a.tty);
}, read:function(a, c, d, e) {
  if (!a.tty || !a.tty.v.P) {
    throw new M(60);
  }
  for (var g = 0, h = 0; h < e; h++) {
    try {
      var k = a.tty.v.P(a.tty);
    } catch (l) {
      throw new M(29);
    }
    if (void 0 === k && 0 === g) {
      throw new M(6);
    }
    if (null === k || void 0 === k) {
      break;
    }
    g++;
    c[d + h] = k;
  }
  g && (a.node.timestamp = Date.now());
  return g;
}, write:function(a, c, d, e) {
  if (!a.tty || !a.tty.v.F) {
    throw new M(60);
  }
  try {
    for (var g = 0; g < e; g++) {
      a.tty.v.F(a.tty, c[d + g]);
    }
  } catch (h) {
    throw new M(29);
  }
  e && (a.node.timestamp = Date.now());
  return g;
}}, tb = {P:function(a) {
  if (!a.input.length) {
    var c = null;
    if (da) {
      var d = Buffer.V ? Buffer.V(256) : new Buffer(256), e = 0;
      try {
        e = ia.readSync(process.stdin.fd, d, 0, 256, null);
      } catch (g) {
        if (-1 != g.toString().indexOf("EOF")) {
          e = 0;
        } else {
          throw g;
        }
      }
      0 < e ? c = d.slice(0, e).toString("utf-8") : c = null;
    } else {
      "undefined" != typeof window && "function" == typeof window.prompt ? (c = window.prompt("Input: "), null !== c && (c += "\n")) : "function" == typeof readline && (c = readline(), null !== c && (c += "\n"));
    }
    if (!c) {
      return null;
    }
    d = Array(va(c) + 1);
    c = ta(c, d, 0, d.length);
    d.length = c;
    a.input = d;
  }
  return a.input.shift();
}, F:function(a, c) {
  null === c || 10 === c ? (ka(sa(a.output, 0)), a.output = []) : 0 != c && a.output.push(c);
}, flush:function(a) {
  a.output && 0 < a.output.length && (ka(sa(a.output, 0)), a.output = []);
}}, ub = {F:function(a, c) {
  null === c || 10 === c ? (w(sa(a.output, 0)), a.output = []) : 0 != c && a.output.push(c);
}, flush:function(a) {
  a.output && 0 < a.output.length && (w(sa(a.output, 0)), a.output = []);
}}, N = {h:null, m:function() {
  return N.createNode(null, "/", 16895, 0);
}, createNode:function(a, c, d, e) {
  if (24576 === (d & 61440) || 4096 === (d & 61440)) {
    throw new M(63);
  }
  N.h || (N.h = {dir:{node:{o:N.c.o, j:N.c.j, lookup:N.c.lookup, B:N.c.B, rename:N.c.rename, unlink:N.c.unlink, rmdir:N.c.rmdir, readdir:N.c.readdir, symlink:N.c.symlink}, stream:{u:N.f.u}}, file:{node:{o:N.c.o, j:N.c.j}, stream:{u:N.f.u, read:N.f.read, write:N.f.write, I:N.f.I, R:N.f.R, T:N.f.T}}, link:{node:{o:N.c.o, j:N.c.j, readlink:N.c.readlink}, stream:{}}, J:{node:{o:N.c.o, j:N.c.j}, stream:vb}});
  d = wb(a, c, d, e);
  16384 === (d.mode & 61440) ? (d.c = N.h.dir.node, d.f = N.h.dir.stream, d.b = {}) : 32768 === (d.mode & 61440) ? (d.c = N.h.file.node, d.f = N.h.file.stream, d.g = 0, d.b = null) : 40960 === (d.mode & 61440) ? (d.c = N.h.link.node, d.f = N.h.link.stream) : 8192 === (d.mode & 61440) && (d.c = N.h.J.node, d.f = N.h.J.stream);
  d.timestamp = Date.now();
  a && (a.b[c] = d);
  return d;
}, Gc:function(a) {
  if (a.b && a.b.subarray) {
    for (var c = [], d = 0; d < a.g; ++d) {
      c.push(a.b[d]);
    }
    return c;
  }
  return a.b;
}, Hc:function(a) {
  return a.b ? a.b.subarray ? a.b.subarray(0, a.g) : new Uint8Array(a.b) : new Uint8Array(0);
}, K:function(a, c) {
  var d = a.b ? a.b.length : 0;
  d >= c || (c = Math.max(c, d * (1048576 > d ? 2.0 : 1.125) >>> 0), 0 != d && (c = Math.max(c, 256)), d = a.b, a.b = new Uint8Array(c), 0 < a.g && a.b.set(d.subarray(0, a.g), 0));
}, ha:function(a, c) {
  if (a.g != c) {
    if (0 == c) {
      a.b = null, a.g = 0;
    } else {
      if (!a.b || a.b.subarray) {
        var d = a.b;
        a.b = new Uint8Array(c);
        d && a.b.set(d.subarray(0, Math.min(c, a.g)));
      } else {
        if (a.b || (a.b = []), a.b.length > c) {
          a.b.length = c;
        } else {
          for (; a.b.length < c;) {
            a.b.push(0);
          }
        }
      }
      a.g = c;
    }
  }
}, c:{o:function(a) {
  var c = {};
  c.dev = 8192 === (a.mode & 61440) ? a.id : 1;
  c.ino = a.id;
  c.mode = a.mode;
  c.nlink = 1;
  c.uid = 0;
  c.gid = 0;
  c.rdev = a.rdev;
  16384 === (a.mode & 61440) ? c.size = 4096 : 32768 === (a.mode & 61440) ? c.size = a.g : 40960 === (a.mode & 61440) ? c.size = a.link.length : c.size = 0;
  c.atime = new Date(a.timestamp);
  c.mtime = new Date(a.timestamp);
  c.ctime = new Date(a.timestamp);
  c.W = 4096;
  c.blocks = Math.ceil(c.size / c.W);
  return c;
}, j:function(a, c) {
  void 0 !== c.mode && (a.mode = c.mode);
  void 0 !== c.timestamp && (a.timestamp = c.timestamp);
  void 0 !== c.size && N.ha(a, c.size);
}, lookup:function() {
  throw xb[44];
}, B:function(a, c, d, e) {
  return N.createNode(a, c, d, e);
}, rename:function(a, c, d) {
  if (16384 === (a.mode & 61440)) {
    try {
      var e = yb(c, d);
    } catch (h) {
    }
    if (e) {
      for (var g in e.b) {
        throw new M(55);
      }
    }
  }
  delete a.parent.b[a.name];
  a.name = d;
  c.b[d] = a;
  a.parent = c;
}, unlink:function(a, c) {
  delete a.b[c];
}, rmdir:function(a, c) {
  var d = yb(a, c), e;
  for (e in d.b) {
    throw new M(55);
  }
  delete a.b[c];
}, readdir:function(a) {
  var c = [".", ".."], d;
  for (d in a.b) {
    a.b.hasOwnProperty(d) && c.push(d);
  }
  return c;
}, symlink:function(a, c, d) {
  a = N.createNode(a, c, 41471, 0);
  a.link = d;
  return a;
}, readlink:function(a) {
  if (40960 !== (a.mode & 61440)) {
    throw new M(28);
  }
  return a.link;
}}, f:{read:function(a, c, d, e, g) {
  var h = a.node.b;
  if (g >= a.node.g) {
    return 0;
  }
  a = Math.min(a.node.g - g, e);
  assert(0 <= a);
  if (8 < a && h.subarray) {
    c.set(h.subarray(g, g + a), d);
  } else {
    for (e = 0; e < a; e++) {
      c[d + e] = h[g + e];
    }
  }
  return a;
}, write:function(a, c, d, e, g, h) {
  assert(!(c instanceof ArrayBuffer));
  c.buffer === F.buffer && (h && la("file packager has copied file data into memory, but in memory growth we are forced to copy it again (see --no-heap-copy)"), h = !1);
  if (!e) {
    return 0;
  }
  a = a.node;
  a.timestamp = Date.now();
  if (c.subarray && (!a.b || a.b.subarray)) {
    if (h) {
      return assert(0 === g, "canOwn must imply no weird position inside the file"), a.b = c.subarray(d, d + e), a.g = e;
    }
    if (0 === a.g && 0 === g) {
      return a.b = c.slice(d, d + e), a.g = e;
    }
    if (g + e <= a.g) {
      return a.b.set(c.subarray(d, d + e), g), e;
    }
  }
  N.K(a, g + e);
  if (a.b.subarray && c.subarray) {
    a.b.set(c.subarray(d, d + e), g);
  } else {
    for (h = 0; h < e; h++) {
      a.b[g + h] = c[d + h];
    }
  }
  a.g = Math.max(a.g, g + e);
  return e;
}, u:function(a, c, d) {
  1 === d ? c += a.position : 2 === d && 32768 === (a.node.mode & 61440) && (c += a.node.g);
  if (0 > c) {
    throw new M(28);
  }
  return c;
}, I:function(a, c, d) {
  N.K(a.node, c + d);
  a.node.g = Math.max(a.node.g, c + d);
}, R:function(a, c, d, e, g, h) {
  assert(0 === c);
  if (32768 !== (a.node.mode & 61440)) {
    throw new M(43);
  }
  a = a.node.b;
  if (h & 2 || a.buffer !== E) {
    if (0 < e || e + d < a.length) {
      a.subarray ? a = a.subarray(e, e + d) : a = Array.prototype.slice.call(a, e, e + d);
    }
    e = !0;
    d = zb(d);
    if (!d) {
      throw new M(48);
    }
    F.set(a, d);
  } else {
    e = !1, d = a.byteOffset;
  }
  return {Lc:d, Fc:e};
}, T:function(a, c, d, e, g) {
  if (32768 !== (a.node.mode & 61440)) {
    throw new M(43);
  }
  if (g & 2) {
    return 0;
  }
  N.f.write(a, c, 0, e, d, !1);
  return 0;
}}}, Ab = {0:"Success", 1:"Arg list too long", 2:"Permission denied", 3:"Address already in use", 4:"Address not available", 5:"Address family not supported by protocol family", 6:"No more processes", 7:"Socket already connected", 8:"Bad file number", 9:"Trying to read unreadable message", 10:"Mount device busy", 11:"Operation canceled", 12:"No children", 13:"Connection aborted", 14:"Connection refused", 15:"Connection reset by peer", 16:"File locking deadlock error", 17:"Destination address required", 
18:"Math arg out of domain of func", 19:"Quota exceeded", 20:"File exists", 21:"Bad address", 22:"File too large", 23:"Host is unreachable", 24:"Identifier removed", 25:"Illegal byte sequence", 26:"Connection already in progress", 27:"Interrupted system call", 28:"Invalid argument", 29:"I/O error", 30:"Socket is already connected", 31:"Is a directory", 32:"Too many symbolic links", 33:"Too many open files", 34:"Too many links", 35:"Message too long", 36:"Multihop attempted", 37:"File or path name too long", 
38:"Network interface is not configured", 39:"Connection reset by network", 40:"Network is unreachable", 41:"Too many open files in system", 42:"No buffer space available", 43:"No such device", 44:"No such file or directory", 45:"Exec format error", 46:"No record locks available", 47:"The link has been severed", 48:"Not enough core", 49:"No message of desired type", 50:"Protocol not available", 51:"No space left on device", 52:"Function not implemented", 53:"Socket is not connected", 54:"Not a directory", 
55:"Directory not empty", 56:"State not recoverable", 57:"Socket operation on non-socket", 59:"Not a typewriter", 60:"No such device or address", 61:"Value too large for defined data type", 62:"Previous owner died", 63:"Not super-user", 64:"Broken pipe", 65:"Protocol error", 66:"Unknown protocol", 67:"Protocol wrong type for socket", 68:"Math result not representable", 69:"Read only file system", 70:"Illegal seek", 71:"No such process", 72:"Stale file handle", 73:"Connection timed out", 74:"Text file busy", 
75:"Cross-device link", 100:"Device not a stream", 101:"Bad font file fmt", 102:"Invalid slot", 103:"Invalid request code", 104:"No anode", 105:"Block device required", 106:"Channel number out of range", 107:"Level 3 halted", 108:"Level 3 reset", 109:"Link number out of range", 110:"Protocol driver not attached", 111:"No CSI structure available", 112:"Level 2 halted", 113:"Invalid exchange", 114:"Invalid request descriptor", 115:"Exchange full", 116:"No data (for no delay io)", 117:"Timer expired", 
118:"Out of streams resources", 119:"Machine is not on the network", 120:"Package not installed", 121:"The object is remote", 122:"Advertise error", 123:"Srmount error", 124:"Communication error on send", 125:"Cross mount point (not really error)", 126:"Given log. name not unique", 127:"f.d. invalid for this operation", 128:"Remote address changed", 129:"Can   access a needed shared lib", 130:"Accessing a corrupted shared lib", 131:".lib section in a.out corrupted", 132:"Attempting to link in too many libs", 
133:"Attempting to exec a shared library", 135:"Streams pipe error", 136:"Too many users", 137:"Socket type not supported", 138:"Not supported", 139:"Protocol family not supported", 140:"Can't send after socket shutdown", 141:"Too many references", 142:"Host is down", 148:"No medium (in tape drive)", 156:"Level 2 not synchronized"}, Bb = {ec:63, Eb:44, sc:71, $a:27, bb:29, ac:60, na:1, Fb:45, wa:8, Ga:12, ta:6, Cc:6, Jb:48, oa:2, Ta:21, Sb:105, Ea:10, Sa:20, Dc:75, Db:43, Ub:54, eb:31, ab:28, yb:41, 
qb:33, Zb:59, zc:74, Ua:22, Ob:51, rc:70, oc:69, rb:34, hc:64, Pa:18, lc:68, Kb:49, Xa:24, Ha:106, gb:156, hb:107, ib:108, ob:109, Ac:110, Bb:111, fb:112, Ma:16, Gb:46, va:113, Aa:114, Ec:115, zb:104, Ba:103, Ca:102, Na:16, Da:101, Qb:100, Cb:116, wc:117, Pb:118, Lb:119, Mb:120, nc:121, Hb:47, ra:122, tc:123, Ia:124, ic:65, tb:36, Qa:125, za:9, $b:126, ya:127, mc:128, jb:129, kb:130, nb:131, mb:132, lb:133, Rb:52, Vb:55, ub:37, pb:32, bc:138, fc:139, La:15, Ab:42, sa:5, kc:67, Xb:57, Nb:50, pc:140, 
Ka:14, pa:3, Ja:13, xb:40, vb:38, xc:73, Va:142, Wa:23, Za:26, ua:7, Oa:17, sb:35, jc:66, qc:137, qa:4, wb:39, cb:30, Tb:53, yc:141, Bc:136, Ra:19, uc:72, Yb:138, Ib:148, Ya:25, cc:61, Fa:11, Wb:56, dc:62, vc:135}, Cb = null, Db = {}, Eb = [], Fb = 1, Gb = null, Hb = !0, Ib = {}, M = null, xb = {};
function O(a, c) {
  a = ob("/", a);
  c = c || {};
  if (!a) {
    return {path:"", node:null};
  }
  var d = {N:!0, G:0}, e;
  for (e in d) {
    void 0 === c[e] && (c[e] = d[e]);
  }
  if (8 < c.G) {
    throw new M(32);
  }
  a = kb(a.split("/").filter(function(k) {
    return !!k;
  }), !1);
  var g = Cb;
  d = "/";
  for (e = 0; e < a.length; e++) {
    var h = e === a.length - 1;
    if (h && c.parent) {
      break;
    }
    g = yb(g, a[e]);
    d = lb(d + "/" + a[e]);
    g.C && (!h || h && c.N) && (g = g.C.root);
    if (!h || c.M) {
      for (h = 0; 40960 === (g.mode & 61440);) {
        if (g = Jb(d), d = ob(mb(d), g), g = O(d, {G:c.G}).node, 40 < h++) {
          throw new M(32);
        }
      }
    }
  }
  return {path:d, node:g};
}
function Kb(a) {
  for (var c;;) {
    if (a === a.parent) {
      return a = a.m.S, c ? "/" !== a[a.length - 1] ? a + "/" + c : a + c : a;
    }
    c = c ? a.name + "/" + c : a.name;
    a = a.parent;
  }
}
function Lb(a, c) {
  for (var d = 0, e = 0; e < c.length; e++) {
    d = (d << 5) - d + c.charCodeAt(e) | 0;
  }
  return (a + d >>> 0) % Gb.length;
}
function yb(a, c) {
  var d;
  if (d = (d = Mb(a, "x")) ? d : a.c.lookup ? 0 : 2) {
    throw new M(d, a);
  }
  for (d = Gb[Lb(a.id, c)]; d; d = d.ea) {
    var e = d.name;
    if (d.parent.id === a.id && e === c) {
      return d;
    }
  }
  return a.c.lookup(a, c);
}
function wb(a, c, d, e) {
  a = new Nb(a, c, d, e);
  c = Lb(a.parent.id, a.name);
  a.ea = Gb[c];
  return Gb[c] = a;
}
var Ob = {r:0, rs:1052672, "r+":2, w:577, wx:705, xw:705, "w+":578, "wx+":706, "xw+":706, a:1089, ax:1217, xa:1217, "a+":1090, "ax+":1218, "xa+":1218};
function Pb(a) {
  var c = ["r", "w", "rw"][a & 3];
  a & 512 && (c += "w");
  return c;
}
function Mb(a, c) {
  if (Hb) {
    return 0;
  }
  if (-1 === c.indexOf("r") || a.mode & 292) {
    if (-1 !== c.indexOf("w") && !(a.mode & 146) || -1 !== c.indexOf("x") && !(a.mode & 73)) {
      return 2;
    }
  } else {
    return 2;
  }
  return 0;
}
function Qb(a, c) {
  try {
    return yb(a, c), 20;
  } catch (d) {
  }
  return Mb(a, "wx");
}
function Rb(a) {
  var c = 4096;
  for (a = a || 0; a <= c; a++) {
    if (!Eb[a]) {
      return a;
    }
  }
  throw new M(33);
}
function Sb(a, c) {
  Tb || (Tb = function() {
  }, Tb.prototype = {});
  var d = new Tb, e;
  for (e in a) {
    d[e] = a[e];
  }
  a = d;
  c = Rb(c);
  a.fd = c;
  return Eb[c] = a;
}
var vb = {open:function(a) {
  a.f = Db[a.node.rdev].f;
  a.f.open && a.f.open(a);
}, u:function() {
  throw new M(70);
}};
function rb(a, c) {
  Db[a] = {f:c};
}
function Ub(a, c) {
  if ("string" === typeof a) {
    throw a;
  }
  var d = "/" === c, e = !c;
  if (d && Cb) {
    throw new M(10);
  }
  if (!d && !e) {
    var g = O(c, {N:!1});
    c = g.path;
    g = g.node;
    if (g.C) {
      throw new M(10);
    }
    if (16384 !== (g.mode & 61440)) {
      throw new M(54);
    }
  }
  c = {type:a, Kc:{}, S:c, da:[]};
  a = a.m(c);
  a.m = c;
  c.root = a;
  d ? Cb = a : g && (g.C = c, g.m && g.m.da.push(c));
}
function Vb(a, c, d) {
  var e = O(a, {parent:!0}).node;
  a = nb(a);
  if (!a || "." === a || ".." === a) {
    throw new M(28);
  }
  var g = Qb(e, a);
  if (g) {
    throw new M(g);
  }
  if (!e.c.B) {
    throw new M(63);
  }
  return e.c.B(e, a, c, d);
}
function P(a) {
  Vb(a, 16895, 0);
}
function Wb(a, c, d) {
  "undefined" === typeof d && (d = c, c = 438);
  Vb(a, c | 8192, d);
}
function Xb(a, c) {
  if (!ob(a)) {
    throw new M(44);
  }
  var d = O(c, {parent:!0}).node;
  if (!d) {
    throw new M(44);
  }
  c = nb(c);
  var e = Qb(d, c);
  if (e) {
    throw new M(e);
  }
  if (!d.c.symlink) {
    throw new M(63);
  }
  d.c.symlink(d, c, a);
}
function Jb(a) {
  a = O(a).node;
  if (!a) {
    throw new M(44);
  }
  if (!a.c.readlink) {
    throw new M(28);
  }
  return ob(Kb(a.parent), a.c.readlink(a));
}
function Yb(a, c, d, e) {
  if ("" === a) {
    throw new M(44);
  }
  if ("string" === typeof c) {
    var g = Ob[c];
    if ("undefined" === typeof g) {
      throw Error("Unknown file open mode: " + c);
    }
    c = g;
  }
  d = c & 64 ? ("undefined" === typeof d ? 438 : d) & 4095 | 32768 : 0;
  if ("object" === typeof a) {
    var h = a;
  } else {
    a = lb(a);
    try {
      h = O(a, {M:!(c & 131072)}).node;
    } catch (l) {
    }
  }
  g = !1;
  if (c & 64) {
    if (h) {
      if (c & 128) {
        throw new M(20);
      }
    } else {
      h = Vb(a, d, 0), g = !0;
    }
  }
  if (!h) {
    throw new M(44);
  }
  8192 === (h.mode & 61440) && (c &= -513);
  if (c & 65536 && 16384 !== (h.mode & 61440)) {
    throw new M(54);
  }
  if (!g && (d = h ? 40960 === (h.mode & 61440) ? 32 : 16384 === (h.mode & 61440) && ("r" !== Pb(c) || c & 512) ? 31 : Mb(h, Pb(c)) : 44)) {
    throw new M(d);
  }
  if (c & 512) {
    d = h;
    var k;
    "string" === typeof d ? k = O(d, {M:!0}).node : k = d;
    if (!k.c.j) {
      throw new M(63);
    }
    if (16384 === (k.mode & 61440)) {
      throw new M(31);
    }
    if (32768 !== (k.mode & 61440)) {
      throw new M(28);
    }
    if (d = Mb(k, "w")) {
      throw new M(d);
    }
    k.c.j(k, {size:0, timestamp:Date.now()});
  }
  c &= -131713;
  e = Sb({node:h, path:Kb(h), flags:c, seekable:!0, position:0, f:h.f, ma:[], error:!1}, e);
  e.f.open && e.f.open(e);
  !b.logReadFiles || c & 1 || (Zb || (Zb = {}), a in Zb || (Zb[a] = 1, w("FS.trackingDelegate error on read file: " + a)));
  try {
    Ib.onOpenFile && (h = 0, 1 !== (c & 2097155) && (h |= 1), 0 !== (c & 2097155) && (h |= 2), Ib.onOpenFile(a, h));
  } catch (l) {
    w("FS.trackingDelegate['onOpenFile']('" + a + "', flags) threw an exception: " + l.message);
  }
  return e;
}
function $b(a, c, d) {
  if (null === a.fd) {
    throw new M(8);
  }
  if (!a.seekable || !a.f.u) {
    throw new M(70);
  }
  if (0 != d && 1 != d && 2 != d) {
    throw new M(28);
  }
  a.position = a.f.u(a, c, d);
  a.ma = [];
}
function ac() {
  M || (M = function(a, c) {
    this.node = c;
    this.ia = function(d) {
      this.s = d;
      for (var e in Bb) {
        if (Bb[e] === d) {
          this.code = e;
          break;
        }
      }
    };
    this.ia(a);
    this.message = Ab[a];
    this.stack && (Object.defineProperty(this, "stack", {value:Error().stack, writable:!0}), this.stack = bb(this.stack));
  }, M.prototype = Error(), M.prototype.constructor = M, [44].forEach(function(a) {
    xb[a] = new M(a);
    xb[a].stack = "<generic error, no stack>";
  }));
}
var bc;
function cc(a, c) {
  var d = 0;
  a && (d |= 365);
  c && (d |= 146);
  return d;
}
function dc(a, c, d) {
  a = lb("/dev/" + a);
  var e = cc(!!c, !!d);
  ec || (ec = 64);
  var g = ec++ << 8 | 0;
  rb(g, {open:function(h) {
    h.seekable = !1;
  }, close:function() {
    d && d.buffer && d.buffer.length && d(10);
  }, read:function(h, k, l, m) {
    for (var p = 0, q = 0; q < m; q++) {
      try {
        var v = c();
      } catch (x) {
        throw new M(29);
      }
      if (void 0 === v && 0 === p) {
        throw new M(6);
      }
      if (null === v || void 0 === v) {
        break;
      }
      p++;
      k[l + q] = v;
    }
    p && (h.node.timestamp = Date.now());
    return p;
  }, write:function(h, k, l, m) {
    for (var p = 0; p < m; p++) {
      try {
        d(k[l + p]);
      } catch (q) {
        throw new M(29);
      }
    }
    m && (h.node.timestamp = Date.now());
    return p;
  }});
  Wb(a, e, g);
}
var ec, R = {}, Tb, Zb, fc = void 0;
function hc() {
  assert(void 0 != fc);
  fc += 4;
  return D[fc - 4 >> 2];
}
function ic(a) {
  a = Eb[a];
  if (!a) {
    throw new M(8);
  }
  return a;
}
var jc = {};
function kc(a) {
  for (; a.length;) {
    var c = a.pop();
    a.pop()(c);
  }
}
function lc(a) {
  return this.fromWireType(G[a >> 2]);
}
var mc = {}, nc = {}, oc = {};
function pc(a) {
  if (void 0 === a) {
    return "_unknown";
  }
  a = a.replace(/[^a-zA-Z0-9_]/g, "$");
  var c = a.charCodeAt(0);
  return 48 <= c && 57 >= c ? "_" + a : a;
}
function qc(a, c) {
  a = pc(a);
  return (new Function("body", "return function " + a + '() {\n    "use strict";    return body.apply(this, arguments);\n};\n'))(c);
}
function rc(a) {
  var c = Error, d = qc(a, function(e) {
    this.name = a;
    this.message = e;
    e = Error(e).stack;
    void 0 !== e && (this.stack = this.toString() + "\n" + e.replace(/^Error(:[^\n]*)?\n/, ""));
  });
  d.prototype = Object.create(c.prototype);
  d.prototype.constructor = d;
  d.prototype.toString = function() {
    return void 0 === this.message ? this.name : this.name + ": " + this.message;
  };
  return d;
}
var sc = void 0;
function tc(a, c, d) {
  function e(l) {
    l = d(l);
    if (l.length !== a.length) {
      throw new sc("Mismatched type converter count");
    }
    for (var m = 0; m < a.length; ++m) {
      S(a[m], l[m]);
    }
  }
  a.forEach(function(l) {
    oc[l] = c;
  });
  var g = Array(c.length), h = [], k = 0;
  c.forEach(function(l, m) {
    nc.hasOwnProperty(l) ? g[m] = nc[l] : (h.push(l), mc.hasOwnProperty(l) || (mc[l] = []), mc[l].push(function() {
      g[m] = nc[l];
      ++k;
      k === h.length && e(g);
    }));
  });
  0 === h.length && e(g);
}
function uc(a) {
  switch(a) {
    case 1:
      return 0;
    case 2:
      return 1;
    case 4:
      return 2;
    case 8:
      return 3;
    default:
      throw new TypeError("Unknown type size: " + a);
  }
}
var vc = void 0;
function T(a) {
  for (var c = ""; B[a];) {
    c += vc[B[a++]];
  }
  return c;
}
var wc = void 0;
function U(a) {
  throw new wc(a);
}
function S(a, c, d) {
  d = d || {};
  if (!("argPackAdvance" in c)) {
    throw new TypeError("registerType registeredInstance requires argPackAdvance");
  }
  var e = c.name;
  a || U('type "' + e + '" must have a positive integer typeid pointer');
  if (nc.hasOwnProperty(a)) {
    if (d.aa) {
      return;
    }
    U("Cannot register type '" + e + "' twice");
  }
  nc[a] = c;
  delete oc[a];
  mc.hasOwnProperty(a) && (c = mc[a], delete mc[a], c.forEach(function(g) {
    g();
  }));
}
var xc = [], V = [{}, {value:void 0}, {value:null}, {value:!0}, {value:!1}];
function yc(a) {
  4 < a && 0 === --V[a].H && (V[a] = void 0, xc.push(a));
}
function zc(a) {
  switch(a) {
    case void 0:
      return 1;
    case null:
      return 2;
    case !0:
      return 3;
    case !1:
      return 4;
    default:
      var c = xc.length ? xc.pop() : V.length;
      V[c] = {H:1, value:a};
      return c;
  }
}
function Ac(a) {
  if (null === a) {
    return "null";
  }
  var c = typeof a;
  return "object" === c || "array" === c || "function" === c ? a.toString() : "" + a;
}
function Bc(a, c) {
  switch(c) {
    case 2:
      return function(d) {
        return this.fromWireType(Fa[d >> 2]);
      };
    case 3:
      return function(d) {
        return this.fromWireType(Ga[d >> 3]);
      };
    default:
      throw new TypeError("Unknown float type: " + a);
  }
}
function Cc(a) {
  var c = Function;
  if (!(c instanceof Function)) {
    throw new TypeError("new_ called with constructor type " + typeof c + " which is not a function");
  }
  var d = qc(c.name || "unknownFunctionName", function() {
  });
  d.prototype = c.prototype;
  d = new d;
  a = c.apply(d, a);
  return a instanceof Object ? a : d;
}
function Dc(a, c) {
  var d = b;
  if (void 0 === d[a].i) {
    var e = d[a];
    d[a] = function() {
      d[a].i.hasOwnProperty(arguments.length) || U("Function '" + c + "' called with an invalid number of arguments (" + arguments.length + ") - expects one of (" + d[a].i + ")!");
      return d[a].i[arguments.length].apply(this, arguments);
    };
    d[a].i = [];
    d[a].i[e.U] = e;
  }
}
function Ec(a, c, d) {
  b.hasOwnProperty(a) ? ((void 0 === d || void 0 !== b[a].i && void 0 !== b[a].i[d]) && U("Cannot register public name '" + a + "' twice"), Dc(a, a), b.hasOwnProperty(d) && U("Cannot register multiple overloads of a function with the same number of arguments (" + d + ")!"), b[a].i[d] = c) : (b[a] = c, void 0 !== d && (b[a].Jc = d));
}
function Fc(a, c) {
  for (var d = [], e = 0; e < a; e++) {
    d.push(D[(c >> 2) + e]);
  }
  return d;
}
function Gc(a, c) {
  a = T(a);
  var d = b["dynCall_" + a];
  for (var e = [], g = 1; g < a.length; ++g) {
    e.push("a" + g);
  }
  g = "return function dynCall_" + (a + "_" + c) + "(" + e.join(", ") + ") {\n";
  g += "    return dynCall(rawFunction" + (e.length ? ", " : "") + e.join(", ") + ");\n";
  d = (new Function("dynCall", "rawFunction", g + "};\n"))(d, c);
  "function" !== typeof d && U("unknown function pointer with signature " + a + ": " + c);
  return d;
}
var Hc = void 0;
function Ic(a) {
  a = Jc(a);
  var c = T(a);
  W(a);
  return c;
}
function Kc(a, c) {
  function d(h) {
    g[h] || nc[h] || (oc[h] ? oc[h].forEach(d) : (e.push(h), g[h] = !0));
  }
  var e = [], g = {};
  c.forEach(d);
  throw new Hc(a + ": " + e.map(Ic).join([", "]));
}
function Lc(a, c, d) {
  switch(c) {
    case 0:
      return d ? function(e) {
        return F[e];
      } : function(e) {
        return B[e];
      };
    case 1:
      return d ? function(e) {
        return C[e >> 1];
      } : function(e) {
        return za[e >> 1];
      };
    case 2:
      return d ? function(e) {
        return D[e >> 2];
      } : function(e) {
        return G[e >> 2];
      };
    default:
      throw new TypeError("Unknown integer type: " + a);
  }
}
var Mc = {};
function Nc() {
  return "object" === typeof globalThis ? globalThis : Function("return this")();
}
function Oc(a, c) {
  var d = nc[a];
  void 0 === d && U(c + " has unknown type " + Ic(a));
  return d;
}
var Pc = {};
function Nb(a, c, d, e) {
  a || (a = this);
  this.parent = a;
  this.m = a.m;
  this.C = null;
  this.id = Fb++;
  this.name = c;
  this.mode = d;
  this.c = {};
  this.f = {};
  this.rdev = e;
}
Object.defineProperties(Nb.prototype, {read:{get:function() {
  return 365 === (this.mode & 365);
}, set:function(a) {
  a ? this.mode |= 365 : this.mode &= -366;
}}, write:{get:function() {
  return 146 === (this.mode & 146);
}, set:function(a) {
  a ? this.mode |= 146 : this.mode &= -147;
}}});
ac();
Gb = Array(4096);
Ub(N, "/");
P("/tmp");
P("/home");
P("/home/web_user");
(function() {
  P("/dev");
  rb(259, {read:function() {
    return 0;
  }, write:function(e, g, h, k) {
    return k;
  }});
  Wb("/dev/null", 259);
  qb(1280, tb);
  qb(1536, ub);
  Wb("/dev/tty", 1280);
  Wb("/dev/tty1", 1536);
  if ("object" === typeof crypto && "function" === typeof crypto.getRandomValues) {
    var a = new Uint8Array(1);
    var c = function() {
      crypto.getRandomValues(a);
      return a[0];
    };
  } else {
    if (da) {
      try {
        var d = require("crypto");
        c = function() {
          return d.randomBytes(1)[0];
        };
      } catch (e) {
      }
    }
  }
  c || (c = function() {
    f("no cryptographic support found for random_device. consider polyfilling it if you want to use something insecure like Math.random(), e.g. put this in a --pre-js: var crypto = { getRandomValues: function(array) { for (var i = 0; i < array.length; i++) array[i] = (Math.random()*256)|0 } };");
  });
  dc("random", c);
  dc("urandom", c);
  P("/dev/shm");
  P("/dev/shm/tmp");
})();
P("/proc");
P("/proc/self");
P("/proc/self/fd");
Ub({m:function() {
  var a = wb("/proc/self", "fd", 16895, 73);
  a.c = {lookup:function(c, d) {
    var e = Eb[+d];
    if (!e) {
      throw new M(8);
    }
    c = {parent:null, m:{S:"fake"}, c:{readlink:function() {
      return e.path;
    }}};
    return c.parent = c;
  }};
  return a;
}}, "/proc/self/fd");
sc = b.InternalError = rc("InternalError");
for (var Qc = Array(256), Rc = 0; 256 > Rc; ++Rc) {
  Qc[Rc] = String.fromCharCode(Rc);
}
vc = Qc;
wc = b.BindingError = rc("BindingError");
b.count_emval_handles = function() {
  for (var a = 0, c = 5; c < V.length; ++c) {
    void 0 !== V[c] && ++a;
  }
  return a;
};
b.get_first_emval = function() {
  for (var a = 5; a < V.length; ++a) {
    if (void 0 !== V[a]) {
      return V[a];
    }
  }
  return null;
};
Hc = b.UnboundTypeError = rc("UnboundTypeError");
var fd = {__cxa_thread_atexit:function(a, c) {
  la("atexit() called, but EXIT_RUNTIME is not set, so atexits() will not be called. set EXIT_RUNTIME to 1 (see the FAQ)");
  Ra.unshift({O:a, A:c});
}, __handle_stack_overflow:function() {
  f("stack overflow");
}, __sys_fcntl64:function(a, c, d) {
  fc = d;
  try {
    var e = ic(a);
    switch(c) {
      case 0:
        var g = hc();
        return 0 > g ? -28 : Yb(e.path, e.flags, 0, g).fd;
      case 1:
      case 2:
        return 0;
      case 3:
        return e.flags;
      case 4:
        return g = hc(), e.flags |= g, 0;
      case 12:
        return g = hc(), C[g + 0 >> 1] = 2, 0;
      case 13:
      case 14:
        return 0;
      case 16:
      case 8:
        return -28;
      case 9:
        return D[Sc() >> 2] = 28, -1;
      default:
        return -28;
    }
  } catch (h) {
    return "undefined" !== typeof R && h instanceof M || f(h), -h.s;
  }
}, __sys_ioctl:function(a, c, d) {
  fc = d;
  try {
    var e = ic(a);
    switch(c) {
      case 21509:
      case 21505:
        return e.tty ? 0 : -59;
      case 21510:
      case 21511:
      case 21512:
      case 21506:
      case 21507:
      case 21508:
        return e.tty ? 0 : -59;
      case 21519:
        if (!e.tty) {
          return -59;
        }
        var g = hc();
        return D[g >> 2] = 0;
      case 21520:
        return e.tty ? -28 : -59;
      case 21531:
        a = g = hc();
        if (!e.f.ba) {
          throw new M(59);
        }
        return e.f.ba(e, c, a);
      case 21523:
        return e.tty ? 0 : -59;
      case 21524:
        return e.tty ? 0 : -59;
      default:
        f("bad ioctl syscall " + c);
    }
  } catch (h) {
    return "undefined" !== typeof R && h instanceof M || f(h), -h.s;
  }
}, __sys_open:function(a, c, d) {
  fc = d;
  try {
    var e = a ? sa(B, a, void 0) : "", g = hc();
    return Yb(e, c, g).fd;
  } catch (h) {
    return "undefined" !== typeof R && h instanceof M || f(h), -h.s;
  }
}, _embind_finalize_value_object:function(a) {
  var c = jc[a];
  delete jc[a];
  var d = c.fa, e = c.ga, g = c.L, h = g.map(function(k) {
    return k.$;
  }).concat(g.map(function(k) {
    return k.ka;
  }));
  tc([a], h, function(k) {
    var l = {};
    g.forEach(function(m, p) {
      var q = k[p], v = m.Y, x = m.Z, z = k[p + g.length], t = m.ja, H = m.la;
      l[m.X] = {read:function(A) {
        return q.fromWireType(v(x, A));
      }, write:function(A, Q) {
        var wa = [];
        t(H, A, z.toWireType(wa, Q));
        kc(wa);
      }};
    });
    return [{name:c.name, fromWireType:function(m) {
      var p = {}, q;
      for (q in l) {
        p[q] = l[q].read(m);
      }
      e(m);
      return p;
    }, toWireType:function(m, p) {
      for (var q in l) {
        if (!(q in p)) {
          throw new TypeError('Missing field:  "' + q + '"');
        }
      }
      var v = d();
      for (q in l) {
        l[q].write(v, p[q]);
      }
      null !== m && m.push(e, v);
      return v;
    }, argPackAdvance:8, readValueFromPointer:lc, l:e}];
  });
}, _embind_register_bool:function(a, c, d, e, g) {
  var h = uc(d);
  c = T(c);
  S(a, {name:c, fromWireType:function(k) {
    return !!k;
  }, toWireType:function(k, l) {
    return l ? e : g;
  }, argPackAdvance:8, readValueFromPointer:function(k) {
    if (1 === d) {
      var l = F;
    } else {
      if (2 === d) {
        l = C;
      } else {
        if (4 === d) {
          l = D;
        } else {
          throw new TypeError("Unknown boolean type size: " + c);
        }
      }
    }
    return this.fromWireType(l[k >> h]);
  }, l:null});
}, _embind_register_emval:function(a, c) {
  c = T(c);
  S(a, {name:c, fromWireType:function(d) {
    var e = V[d].value;
    yc(d);
    return e;
  }, toWireType:function(d, e) {
    return zc(e);
  }, argPackAdvance:8, readValueFromPointer:lc, l:null});
}, _embind_register_float:function(a, c, d) {
  d = uc(d);
  c = T(c);
  S(a, {name:c, fromWireType:function(e) {
    return e;
  }, toWireType:function(e, g) {
    if ("number" !== typeof g && "boolean" !== typeof g) {
      throw new TypeError('Cannot convert "' + Ac(g) + '" to ' + this.name);
    }
    return g;
  }, argPackAdvance:8, readValueFromPointer:Bc(c, d), l:null});
}, _embind_register_function:function(a, c, d, e, g, h) {
  var k = Fc(c, d);
  a = T(a);
  g = Gc(e, g);
  Ec(a, function() {
    Kc("Cannot call " + a + " due to unbound types", k);
  }, c - 1);
  tc([], k, function(l) {
    var m = [l[0], null].concat(l.slice(1)), p = l = a, q = g, v = m.length;
    2 > v && U("argTypes array size mismatch! Must at least get return value and 'this' types!");
    for (var x = null !== m[1] && !1, z = !1, t = 1; t < m.length; ++t) {
      if (null !== m[t] && void 0 === m[t].l) {
        z = !0;
        break;
      }
    }
    var H = "void" !== m[0].name, A = "", Q = "";
    for (t = 0; t < v - 2; ++t) {
      A += (0 !== t ? ", " : "") + "arg" + t, Q += (0 !== t ? ", " : "") + "arg" + t + "Wired";
    }
    p = "return function " + pc(p) + "(" + A + ") {\nif (arguments.length !== " + (v - 2) + ") {\nthrowBindingError('function " + p + " called with ' + arguments.length + ' arguments, expected " + (v - 2) + " args!');\n}\n";
    z && (p += "var destructors = [];\n");
    var wa = z ? "destructors" : "null";
    A = "throwBindingError invoker fn runDestructors retType classParam".split(" ");
    q = [U, q, h, kc, m[0], m[1]];
    x && (p += "var thisWired = classParam.toWireType(" + wa + ", this);\n");
    for (t = 0; t < v - 2; ++t) {
      p += "var arg" + t + "Wired = argType" + t + ".toWireType(" + wa + ", arg" + t + "); // " + m[t + 2].name + "\n", A.push("argType" + t), q.push(m[t + 2]);
    }
    x && (Q = "thisWired" + (0 < Q.length ? ", " : "") + Q);
    p += (H ? "var rv = " : "") + "invoker(fn" + (0 < Q.length ? ", " : "") + Q + ");\n";
    if (z) {
      p += "runDestructors(destructors);\n";
    } else {
      for (t = x ? 1 : 2; t < m.length; ++t) {
        v = 1 === t ? "thisWired" : "arg" + (t - 2) + "Wired", null !== m[t].l && (p += v + "_dtor(" + v + "); // " + m[t].name + "\n", A.push(v + "_dtor"), q.push(m[t].l));
      }
    }
    H && (p += "var ret = retType.fromWireType(rv);\nreturn ret;\n");
    A.push(p + "}\n");
    m = Cc(A).apply(null, q);
    t = c - 1;
    if (!b.hasOwnProperty(l)) {
      throw new sc("Replacing nonexistant public symbol");
    }
    void 0 !== b[l].i && void 0 !== t ? b[l].i[t] = m : (b[l] = m, b[l].U = t);
    return [];
  });
}, _embind_register_integer:function(a, c, d, e, g) {
  function h(p) {
    return p;
  }
  c = T(c);
  -1 === g && (g = 4294967295);
  var k = uc(d);
  if (0 === e) {
    var l = 32 - 8 * d;
    h = function(p) {
      return p << l >>> l;
    };
  }
  var m = -1 != c.indexOf("unsigned");
  S(a, {name:c, fromWireType:h, toWireType:function(p, q) {
    if ("number" !== typeof q && "boolean" !== typeof q) {
      throw new TypeError('Cannot convert "' + Ac(q) + '" to ' + this.name);
    }
    if (q < e || q > g) {
      throw new TypeError('Passing a number "' + Ac(q) + '" from JS side to C/C++ side to an argument of type "' + c + '", which is outside the valid range [' + e + ", " + g + "]!");
    }
    return m ? q >>> 0 : q | 0;
  }, argPackAdvance:8, readValueFromPointer:Lc(c, k, 0 !== e), l:null});
}, _embind_register_memory_view:function(a, c, d) {
  function e(h) {
    h >>= 2;
    var k = G;
    return new g(E, k[h + 1], k[h]);
  }
  var g = [Int8Array, Uint8Array, Int16Array, Uint16Array, Int32Array, Uint32Array, Float32Array, Float64Array][c];
  d = T(d);
  S(a, {name:d, fromWireType:e, argPackAdvance:8, readValueFromPointer:e}, {aa:!0});
}, _embind_register_std_string:function(a, c) {
  c = T(c);
  var d = "std::string" === c;
  S(a, {name:c, fromWireType:function(e) {
    var g = G[e >> 2];
    if (d) {
      for (var h = e + 4, k = 0; k <= g; ++k) {
        var l = e + 4 + k;
        if (0 == B[l] || k == g) {
          h = h ? sa(B, h, l - h) : "";
          if (void 0 === m) {
            var m = h;
          } else {
            m += String.fromCharCode(0), m += h;
          }
          h = l + 1;
        }
      }
    } else {
      m = Array(g);
      for (k = 0; k < g; ++k) {
        m[k] = String.fromCharCode(B[e + 4 + k]);
      }
      m = m.join("");
    }
    W(e);
    return m;
  }, toWireType:function(e, g) {
    g instanceof ArrayBuffer && (g = new Uint8Array(g));
    var h = "string" === typeof g;
    h || g instanceof Uint8Array || g instanceof Uint8ClampedArray || g instanceof Int8Array || U("Cannot pass non-string to std::string");
    var k = (d && h ? function() {
      return va(g);
    } : function() {
      return g.length;
    })(), l = zb(4 + k + 1);
    G[l >> 2] = k;
    if (d && h) {
      ua(g, l + 4, k + 1);
    } else {
      if (h) {
        for (h = 0; h < k; ++h) {
          var m = g.charCodeAt(h);
          255 < m && (W(l), U("String has UTF-16 code units that do not fit in 8 bits"));
          B[l + 4 + h] = m;
        }
      } else {
        for (h = 0; h < k; ++h) {
          B[l + 4 + h] = g[h];
        }
      }
    }
    null !== e && e.push(W, l);
    return l;
  }, argPackAdvance:8, readValueFromPointer:lc, l:function(e) {
    W(e);
  }});
}, _embind_register_std_wstring:function(a, c, d) {
  d = T(d);
  if (2 === c) {
    var e = ya;
    var g = Aa;
    var h = Ba;
    var k = function() {
      return za;
    };
    var l = 1;
  } else {
    4 === c && (e = Ca, g = Da, h = Ea, k = function() {
      return G;
    }, l = 2);
  }
  S(a, {name:d, fromWireType:function(m) {
    for (var p = G[m >> 2], q = k(), v, x = m + 4, z = 0; z <= p; ++z) {
      var t = m + 4 + z * c;
      if (0 == q[t >> l] || z == p) {
        x = e(x, t - x), void 0 === v ? v = x : (v += String.fromCharCode(0), v += x), x = t + c;
      }
    }
    W(m);
    return v;
  }, toWireType:function(m, p) {
    "string" !== typeof p && U("Cannot pass non-string to C++ string type " + d);
    var q = h(p), v = zb(4 + q + c);
    G[v >> 2] = q >> l;
    g(p, v + 4, q + c);
    null !== m && m.push(W, v);
    return v;
  }, argPackAdvance:8, readValueFromPointer:lc, l:function(m) {
    W(m);
  }});
}, _embind_register_value_object:function(a, c, d, e, g, h) {
  jc[a] = {name:T(c), fa:Gc(d, e), ga:Gc(g, h), L:[]};
}, _embind_register_value_object_field:function(a, c, d, e, g, h, k, l, m, p) {
  jc[a].L.push({X:T(c), $:d, Y:Gc(e, g), Z:h, ka:k, ja:Gc(l, m), la:p});
}, _embind_register_void:function(a, c) {
  c = T(c);
  S(a, {Ic:!0, name:c, argPackAdvance:0, fromWireType:function() {
  }, toWireType:function() {
  }});
}, _emval_decref:yc, _emval_get_global:function(a) {
  if (0 === a) {
    return zc(Nc());
  }
  var c = Mc[a];
  a = void 0 === c ? T(a) : c;
  return zc(Nc()[a]);
}, _emval_incref:function(a) {
  4 < a && (V[a].H += 1);
}, _emval_new:function(a, c, d, e) {
  a || U("Cannot use deleted val. handle = " + a);
  a = V[a].value;
  var g = Pc[c];
  if (!g) {
    g = "";
    for (var h = 0; h < c; ++h) {
      g += (0 !== h ? ", " : "") + "arg" + h;
    }
    var k = "return function emval_allocator_" + c + "(constructor, argTypes, args) {\n";
    for (h = 0; h < c; ++h) {
      k += "var argType" + h + " = requireRegisteredType(Module['HEAP32'][(argTypes >>> 2) + " + h + '], "parameter ' + h + '");\nvar arg' + h + " = argType" + h + ".readValueFromPointer(args);\nargs += argType" + h + "['argPackAdvance'];\n";
    }
    g = (new Function("requireRegisteredType", "Module", "__emval_register", k + ("var obj = new constructor(" + g + ");\nreturn __emval_register(obj);\n}\n")))(Oc, b, zc);
    Pc[c] = g;
  }
  return g(a, d, e);
}, abort:function() {
  f();
}, aom_codec_av1_dx:function() {
  w("missing function: aom_codec_av1_dx");
  f(-1);
}, emscripten_get_sbrk_ptr:function() {
  return 893280;
}, emscripten_longjmp:function(a, c) {
  X(a, c || 1);
  throw "longjmp";
}, emscripten_memcpy_big:function(a, c, d) {
  B.copyWithin(a, c, c + d);
}, emscripten_resize_heap:function(a) {
  a >>>= 0;
  var c = B.length;
  assert(a > c);
  if (2147483648 < a) {
    return w("Cannot enlarge memory, asked to go up to " + a + " bytes, but the limit is 2147483648 bytes!"), !1;
  }
  for (var d = 1; 4 >= d; d *= 2) {
    var e = c * (1 + 0.2 / d);
    e = Math.min(e, a + 100663296);
    e = Math.max(16777216, a, e);
    0 < e % 65536 && (e += 65536 - e % 65536);
    e = Math.min(2147483648, e);
    a: {
      var g = e;
      try {
        y.grow(g - E.byteLength + 65535 >>> 16);
        Ha(y.buffer);
        var h = 1;
        break a;
      } catch (k) {
        console.error("emscripten_realloc_buffer: Attempted to grow heap from " + E.byteLength + " bytes to " + g + " bytes, but got error: " + k);
      }
      h = void 0;
    }
    if (h) {
      return !0;
    }
  }
  w("Failed to grow the heap from " + c + " bytes to " + e + " bytes, not enough memory!");
  return !1;
}, fd_close:function(a) {
  try {
    var c = ic(a);
    if (null === c.fd) {
      throw new M(8);
    }
    c.D && (c.D = null);
    try {
      c.f.close && c.f.close(c);
    } catch (d) {
      throw d;
    } finally {
      Eb[c.fd] = null;
    }
    c.fd = null;
    return 0;
  } catch (d) {
    return "undefined" !== typeof R && d instanceof M || f(d), d.s;
  }
}, fd_read:function(a, c, d, e) {
  try {
    a: {
      for (var g = ic(a), h = a = 0; h < d; h++) {
        var k = D[c + (8 * h + 4) >> 2], l = g, m = D[c + 8 * h >> 2], p = k, q = void 0, v = F;
        if (0 > p || 0 > q) {
          throw new M(28);
        }
        if (null === l.fd) {
          throw new M(8);
        }
        if (1 === (l.flags & 2097155)) {
          throw new M(8);
        }
        if (16384 === (l.node.mode & 61440)) {
          throw new M(31);
        }
        if (!l.f.read) {
          throw new M(28);
        }
        var x = "undefined" !== typeof q;
        if (!x) {
          q = l.position;
        } else {
          if (!l.seekable) {
            throw new M(70);
          }
        }
        var z = l.f.read(l, v, m, p, q);
        x || (l.position += z);
        var t = z;
        if (0 > t) {
          var H = -1;
          break a;
        }
        a += t;
        if (t < k) {
          break;
        }
      }
      H = a;
    }
    D[e >> 2] = H;
    return 0;
  } catch (A) {
    return "undefined" !== typeof R && A instanceof M || f(A), A.s;
  }
}, fd_seek:function(a, c, d, e, g) {
  try {
    var h = ic(a);
    a = 4294967296 * d + (c >>> 0);
    if (-9007199254740992 >= a || 9007199254740992 <= a) {
      return -61;
    }
    $b(h, a, e);
    ib = [h.position >>> 0, (hb = h.position, 1.0 <= +Va(hb) ? 0.0 < hb ? (Ya(+Xa(hb / 4294967296.0), 4294967295.0) | 0) >>> 0 : ~~+Wa((hb - +(~~hb >>> 0)) / 4294967296.0) >>> 0 : 0)];
    D[g >> 2] = ib[0];
    D[g + 4 >> 2] = ib[1];
    h.D && 0 === a && 0 === e && (h.D = null);
    return 0;
  } catch (k) {
    return "undefined" !== typeof R && k instanceof M || f(k), k.s;
  }
}, fd_write:function(a, c, d, e) {
  try {
    a: {
      for (var g = ic(a), h = a = 0; h < d; h++) {
        var k = g, l = D[c + 8 * h >> 2], m = D[c + (8 * h + 4) >> 2], p = void 0, q = F;
        if (0 > m || 0 > p) {
          throw new M(28);
        }
        if (null === k.fd) {
          throw new M(8);
        }
        if (0 === (k.flags & 2097155)) {
          throw new M(8);
        }
        if (16384 === (k.node.mode & 61440)) {
          throw new M(31);
        }
        if (!k.f.write) {
          throw new M(28);
        }
        k.seekable && k.flags & 1024 && $b(k, 0, 2);
        var v = "undefined" !== typeof p;
        if (!v) {
          p = k.position;
        } else {
          if (!k.seekable) {
            throw new M(70);
          }
        }
        var x = k.f.write(k, q, l, m, p, void 0);
        v || (k.position += x);
        try {
          if (k.path && Ib.onWriteToFile) {
            Ib.onWriteToFile(k.path);
          }
        } catch (H) {
          w("FS.trackingDelegate['onWriteToFile']('" + k.path + "') threw an exception: " + H.message);
        }
        var z = x;
        if (0 > z) {
          var t = -1;
          break a;
        }
        a += z;
      }
      t = a;
    }
    D[e >> 2] = t;
    return 0;
  } catch (H) {
    return "undefined" !== typeof R && H instanceof M || f(H), H.s;
  }
}, getTempRet0:function() {
  return na | 0;
}, invoke_ii:Tc, invoke_iii:Uc, invoke_iiii:Vc, invoke_iiiii:Wc, invoke_iiiiiiiii:Xc, invoke_iiiiiiiiii:Yc, invoke_iiiijj:Zc, invoke_ij:$c, invoke_v:ad, invoke_vi:bd, invoke_vii:cd, invoke_viii:dd, invoke_viiii:ed, memory:y, round:function(a) {
  a = +a;
  return 0 <= a ? +Xa(a + 0.5) : +Wa(a - 0.5);
}, roundf:function(a) {
  a = +a;
  return 0 <= a ? +Xa(a + 0.5) : +Wa(a - 0.5);
}, setTempRet0:function(a) {
  na = a | 0;
}, table:pa, time:function(a) {
  var c = Date.now() / 1000 | 0;
  a && (D[a >> 2] = c);
  return c;
}};
(function() {
  function a(h) {
    b.asm = h.exports;
    I--;
    b.monitorRunDependencies && b.monitorRunDependencies(I);
    assert($a["wasm-instantiate"]);
    delete $a["wasm-instantiate"];
    0 == I && (null !== J && (clearInterval(J), J = null), Za && (h = Za, Za = null, h()));
  }
  function c(h) {
    assert(b === g, "the Module object should not be replaced during async compilation - perhaps the order of HTML elements is wrong?");
    g = null;
    a(h.instance);
  }
  function d(h) {
    return gb().then(function(k) {
      return WebAssembly.instantiate(k, e);
    }).then(h, function(k) {
      w("failed to asynchronously prepare wasm: " + k);
      f(k);
    });
  }
  var e = {env:fd, wasi_snapshot_preview1:fd};
  ab();
  var g = b;
  if (b.instantiateWasm) {
    try {
      return b.instantiateWasm(e, a);
    } catch (h) {
      return w("Module.instantiateWasm callback failed with error: " + h), !1;
    }
  }
  (function() {
    if (oa || "function" !== typeof WebAssembly.instantiateStreaming || db() || cb("file://") || "function" !== typeof fetch) {
      return d(c);
    }
    fetch(K, {credentials:"same-origin"}).then(function(h) {
      return WebAssembly.instantiateStreaming(h, e).then(c, function(k) {
        w("wasm streaming compile failed: " + k);
        w("falling back to ArrayBuffer instantiation");
        return d(c);
      });
    });
  })();
  return {};
})();
var jb = b.___wasm_call_ctors = L("__wasm_call_ctors"), zb = b._malloc = L("malloc"), W = b._free = L("free");
b._realloc = L("realloc");
b._testSetjmp = L("testSetjmp");
b._saveSetjmp = L("saveSetjmp");
var Jc = b.___getTypeName = L("__getTypeName");
b.___embind_register_native_and_builtin_types = L("__embind_register_native_and_builtin_types");
b._fflush = L("fflush");
var Sc = b.___errno_location = L("__errno_location"), X = b._setThrew = L("setThrew"), Y = b.stackSave = L("stackSave"), Z = b.stackRestore = L("stackRestore");
b.stackAlloc = L("stackAlloc");
var gd = b.dynCall_v = L("dynCall_v"), hd = b.dynCall_vi = L("dynCall_vi"), id = b.dynCall_vii = L("dynCall_vii"), jd = b.dynCall_viii = L("dynCall_viii"), kd = b.dynCall_viiii = L("dynCall_viiii"), ld = b.dynCall_ii = L("dynCall_ii"), md = b.dynCall_iii = L("dynCall_iii"), nd = b.dynCall_iiii = L("dynCall_iiii"), od = b.dynCall_iiiii = L("dynCall_iiiii"), pd = b.dynCall_iiiiiiiii = L("dynCall_iiiiiiiii"), qd = b.dynCall_iiiiiiiiii = L("dynCall_iiiiiiiiii"), rd = b.dynCall_iiiijj = L("dynCall_iiiijj"), 
sd = b.dynCall_ij = L("dynCall_ij");
b.___set_stack_limit = L("__set_stack_limit");
b.__growWasmMemory = L("__growWasmMemory");
b.dynCall_viiiii = L("dynCall_viiiii");
b.dynCall_i = L("dynCall_i");
b.dynCall_iiiiii = L("dynCall_iiiiii");
b.dynCall_viiiiii = L("dynCall_viiiiii");
b.dynCall_viiiiiiiiii = L("dynCall_viiiiiiiiii");
b.dynCall_viiiiiii = L("dynCall_viiiiiii");
b.dynCall_viiiiiiii = L("dynCall_viiiiiiii");
b.dynCall_viiiiiiiii = L("dynCall_viiiiiiiii");
b.dynCall_viiiiiiiiiii = L("dynCall_viiiiiiiiiii");
b.dynCall_viiiiiiiiiiiii = L("dynCall_viiiiiiiiiiiii");
b.dynCall_jiiiiiiii = L("dynCall_jiiiiiiii");
b.dynCall_ff = L("dynCall_ff");
b.dynCall_jiiiiiiiii = L("dynCall_jiiiiiiiii");
b.dynCall_jiiiiii = L("dynCall_jiiiiii");
b.dynCall_jiiiii = L("dynCall_jiiiii");
b.dynCall_iiiiiiii = L("dynCall_iiiiiiii");
b.dynCall_iiiiiii = L("dynCall_iiiiiii");
b.dynCall_iiiiiiiiiiii = L("dynCall_iiiiiiiiiiii");
b.dynCall_iiijii = L("dynCall_iiijii");
b.dynCall_iidiiii = L("dynCall_iidiiii");
b.dynCall_jiji = L("dynCall_jiji");
function Uc(a, c, d) {
  var e = Y();
  try {
    return md(a, c, d);
  } catch (g) {
    Z(e);
    if (g !== g + 0 && "longjmp" !== g) {
      throw g;
    }
    X(1, 0);
  }
}
function bd(a, c) {
  var d = Y();
  try {
    hd(a, c);
  } catch (e) {
    Z(d);
    if (e !== e + 0 && "longjmp" !== e) {
      throw e;
    }
    X(1, 0);
  }
}
function ed(a, c, d, e, g) {
  var h = Y();
  try {
    kd(a, c, d, e, g);
  } catch (k) {
    Z(h);
    if (k !== k + 0 && "longjmp" !== k) {
      throw k;
    }
    X(1, 0);
  }
}
function cd(a, c, d) {
  var e = Y();
  try {
    id(a, c, d);
  } catch (g) {
    Z(e);
    if (g !== g + 0 && "longjmp" !== g) {
      throw g;
    }
    X(1, 0);
  }
}
function dd(a, c, d, e) {
  var g = Y();
  try {
    jd(a, c, d, e);
  } catch (h) {
    Z(g);
    if (h !== h + 0 && "longjmp" !== h) {
      throw h;
    }
    X(1, 0);
  }
}
function Tc(a, c) {
  var d = Y();
  try {
    return ld(a, c);
  } catch (e) {
    Z(d);
    if (e !== e + 0 && "longjmp" !== e) {
      throw e;
    }
    X(1, 0);
  }
}
function Xc(a, c, d, e, g, h, k, l, m) {
  var p = Y();
  try {
    return pd(a, c, d, e, g, h, k, l, m);
  } catch (q) {
    Z(p);
    if (q !== q + 0 && "longjmp" !== q) {
      throw q;
    }
    X(1, 0);
  }
}
function ad(a) {
  var c = Y();
  try {
    gd(a);
  } catch (d) {
    Z(c);
    if (d !== d + 0 && "longjmp" !== d) {
      throw d;
    }
    X(1, 0);
  }
}
function Yc(a, c, d, e, g, h, k, l, m, p) {
  var q = Y();
  try {
    return qd(a, c, d, e, g, h, k, l, m, p);
  } catch (v) {
    Z(q);
    if (v !== v + 0 && "longjmp" !== v) {
      throw v;
    }
    X(1, 0);
  }
}
function Wc(a, c, d, e, g) {
  var h = Y();
  try {
    return od(a, c, d, e, g);
  } catch (k) {
    Z(h);
    if (k !== k + 0 && "longjmp" !== k) {
      throw k;
    }
    X(1, 0);
  }
}
function Vc(a, c, d, e) {
  var g = Y();
  try {
    return nd(a, c, d, e);
  } catch (h) {
    Z(g);
    if (h !== h + 0 && "longjmp" !== h) {
      throw h;
    }
    X(1, 0);
  }
}
function Zc(a, c, d, e, g, h, k, l) {
  var m = Y();
  try {
    return rd(a, c, d, e, g, h, k, l);
  } catch (p) {
    Z(m);
    if (p !== p + 0 && "longjmp" !== p) {
      throw p;
    }
    X(1, 0);
  }
}
function $c(a, c, d) {
  var e = Y();
  try {
    return sd(a, c, d);
  } catch (g) {
    Z(e);
    if (g !== g + 0 && "longjmp" !== g) {
      throw g;
    }
    X(1, 0);
  }
}
Object.getOwnPropertyDescriptor(b, "intArrayFromString") || (b.intArrayFromString = function() {
  f("'intArrayFromString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "intArrayToString") || (b.intArrayToString = function() {
  f("'intArrayToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ccall") || (b.ccall = function() {
  f("'ccall' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "cwrap") || (b.cwrap = function() {
  f("'cwrap' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "setValue") || (b.setValue = function() {
  f("'setValue' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getValue") || (b.getValue = function() {
  f("'getValue' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "allocate") || (b.allocate = function() {
  f("'allocate' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getMemory") || (b.getMemory = function() {
  f("'getMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "UTF8ArrayToString") || (b.UTF8ArrayToString = function() {
  f("'UTF8ArrayToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "UTF8ToString") || (b.UTF8ToString = function() {
  f("'UTF8ToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stringToUTF8Array") || (b.stringToUTF8Array = function() {
  f("'stringToUTF8Array' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stringToUTF8") || (b.stringToUTF8 = function() {
  f("'stringToUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "lengthBytesUTF8") || (b.lengthBytesUTF8 = function() {
  f("'lengthBytesUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stackTrace") || (b.stackTrace = function() {
  f("'stackTrace' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "addOnPreRun") || (b.addOnPreRun = function() {
  f("'addOnPreRun' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "addOnInit") || (b.addOnInit = function() {
  f("'addOnInit' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "addOnPreMain") || (b.addOnPreMain = function() {
  f("'addOnPreMain' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "addOnExit") || (b.addOnExit = function() {
  f("'addOnExit' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "addOnPostRun") || (b.addOnPostRun = function() {
  f("'addOnPostRun' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "writeStringToMemory") || (b.writeStringToMemory = function() {
  f("'writeStringToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "writeArrayToMemory") || (b.writeArrayToMemory = function() {
  f("'writeArrayToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "writeAsciiToMemory") || (b.writeAsciiToMemory = function() {
  f("'writeAsciiToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "addRunDependency") || (b.addRunDependency = function() {
  f("'addRunDependency' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "removeRunDependency") || (b.removeRunDependency = function() {
  f("'removeRunDependency' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "FS_createFolder") || (b.FS_createFolder = function() {
  f("'FS_createFolder' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "FS_createPath") || (b.FS_createPath = function() {
  f("'FS_createPath' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "FS_createDataFile") || (b.FS_createDataFile = function() {
  f("'FS_createDataFile' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "FS_createPreloadedFile") || (b.FS_createPreloadedFile = function() {
  f("'FS_createPreloadedFile' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "FS_createLazyFile") || (b.FS_createLazyFile = function() {
  f("'FS_createLazyFile' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "FS_createLink") || (b.FS_createLink = function() {
  f("'FS_createLink' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "FS_createDevice") || (b.FS_createDevice = function() {
  f("'FS_createDevice' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "FS_unlink") || (b.FS_unlink = function() {
  f("'FS_unlink' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
});
Object.getOwnPropertyDescriptor(b, "dynamicAlloc") || (b.dynamicAlloc = function() {
  f("'dynamicAlloc' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "loadDynamicLibrary") || (b.loadDynamicLibrary = function() {
  f("'loadDynamicLibrary' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "loadWebAssemblyModule") || (b.loadWebAssemblyModule = function() {
  f("'loadWebAssemblyModule' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getLEB") || (b.getLEB = function() {
  f("'getLEB' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getFunctionTables") || (b.getFunctionTables = function() {
  f("'getFunctionTables' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "alignFunctionTables") || (b.alignFunctionTables = function() {
  f("'alignFunctionTables' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "registerFunctions") || (b.registerFunctions = function() {
  f("'registerFunctions' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "addFunction") || (b.addFunction = function() {
  f("'addFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "removeFunction") || (b.removeFunction = function() {
  f("'removeFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getFuncWrapper") || (b.getFuncWrapper = function() {
  f("'getFuncWrapper' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "prettyPrint") || (b.prettyPrint = function() {
  f("'prettyPrint' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "makeBigInt") || (b.makeBigInt = function() {
  f("'makeBigInt' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "dynCall") || (b.dynCall = function() {
  f("'dynCall' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getCompilerSetting") || (b.getCompilerSetting = function() {
  f("'getCompilerSetting' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "print") || (b.print = function() {
  f("'print' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "printErr") || (b.printErr = function() {
  f("'printErr' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getTempRet0") || (b.getTempRet0 = function() {
  f("'getTempRet0' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "setTempRet0") || (b.setTempRet0 = function() {
  f("'setTempRet0' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "callMain") || (b.callMain = function() {
  f("'callMain' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "abort") || (b.abort = function() {
  f("'abort' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stringToNewUTF8") || (b.stringToNewUTF8 = function() {
  f("'stringToNewUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emscripten_realloc_buffer") || (b.emscripten_realloc_buffer = function() {
  f("'emscripten_realloc_buffer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ENV") || (b.ENV = function() {
  f("'ENV' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ERRNO_CODES") || (b.ERRNO_CODES = function() {
  f("'ERRNO_CODES' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ERRNO_MESSAGES") || (b.ERRNO_MESSAGES = function() {
  f("'ERRNO_MESSAGES' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "setErrNo") || (b.setErrNo = function() {
  f("'setErrNo' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "DNS") || (b.DNS = function() {
  f("'DNS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "GAI_ERRNO_MESSAGES") || (b.GAI_ERRNO_MESSAGES = function() {
  f("'GAI_ERRNO_MESSAGES' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "Protocols") || (b.Protocols = function() {
  f("'Protocols' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "Sockets") || (b.Sockets = function() {
  f("'Sockets' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "UNWIND_CACHE") || (b.UNWIND_CACHE = function() {
  f("'UNWIND_CACHE' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "readAsmConstArgs") || (b.readAsmConstArgs = function() {
  f("'readAsmConstArgs' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "jstoi_q") || (b.jstoi_q = function() {
  f("'jstoi_q' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "jstoi_s") || (b.jstoi_s = function() {
  f("'jstoi_s' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "abortStackOverflow") || (b.abortStackOverflow = function() {
  f("'abortStackOverflow' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "reallyNegative") || (b.reallyNegative = function() {
  f("'reallyNegative' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "formatString") || (b.formatString = function() {
  f("'formatString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "PATH") || (b.PATH = function() {
  f("'PATH' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "PATH_FS") || (b.PATH_FS = function() {
  f("'PATH_FS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "SYSCALLS") || (b.SYSCALLS = function() {
  f("'SYSCALLS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "syscallMmap2") || (b.syscallMmap2 = function() {
  f("'syscallMmap2' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "syscallMunmap") || (b.syscallMunmap = function() {
  f("'syscallMunmap' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "JSEvents") || (b.JSEvents = function() {
  f("'JSEvents' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "specialHTMLTargets") || (b.specialHTMLTargets = function() {
  f("'specialHTMLTargets' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "demangle") || (b.demangle = function() {
  f("'demangle' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "demangleAll") || (b.demangleAll = function() {
  f("'demangleAll' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "jsStackTrace") || (b.jsStackTrace = function() {
  f("'jsStackTrace' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stackTrace") || (b.stackTrace = function() {
  f("'stackTrace' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getEnvStrings") || (b.getEnvStrings = function() {
  f("'getEnvStrings' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "checkWasiClock") || (b.checkWasiClock = function() {
  f("'checkWasiClock' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "writeI53ToI64") || (b.writeI53ToI64 = function() {
  f("'writeI53ToI64' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "writeI53ToI64Clamped") || (b.writeI53ToI64Clamped = function() {
  f("'writeI53ToI64Clamped' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "writeI53ToI64Signaling") || (b.writeI53ToI64Signaling = function() {
  f("'writeI53ToI64Signaling' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "writeI53ToU64Clamped") || (b.writeI53ToU64Clamped = function() {
  f("'writeI53ToU64Clamped' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "writeI53ToU64Signaling") || (b.writeI53ToU64Signaling = function() {
  f("'writeI53ToU64Signaling' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "readI53FromI64") || (b.readI53FromI64 = function() {
  f("'readI53FromI64' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "readI53FromU64") || (b.readI53FromU64 = function() {
  f("'readI53FromU64' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "convertI32PairToI53") || (b.convertI32PairToI53 = function() {
  f("'convertI32PairToI53' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "convertU32PairToI53") || (b.convertU32PairToI53 = function() {
  f("'convertU32PairToI53' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "Browser") || (b.Browser = function() {
  f("'Browser' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "FS") || (b.FS = function() {
  f("'FS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "MEMFS") || (b.MEMFS = function() {
  f("'MEMFS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "TTY") || (b.TTY = function() {
  f("'TTY' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "PIPEFS") || (b.PIPEFS = function() {
  f("'PIPEFS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "SOCKFS") || (b.SOCKFS = function() {
  f("'SOCKFS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "GL") || (b.GL = function() {
  f("'GL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emscriptenWebGLGet") || (b.emscriptenWebGLGet = function() {
  f("'emscriptenWebGLGet' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emscriptenWebGLGetTexPixelData") || (b.emscriptenWebGLGetTexPixelData = function() {
  f("'emscriptenWebGLGetTexPixelData' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emscriptenWebGLGetUniform") || (b.emscriptenWebGLGetUniform = function() {
  f("'emscriptenWebGLGetUniform' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emscriptenWebGLGetVertexAttrib") || (b.emscriptenWebGLGetVertexAttrib = function() {
  f("'emscriptenWebGLGetVertexAttrib' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "AL") || (b.AL = function() {
  f("'AL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "SDL_unicode") || (b.SDL_unicode = function() {
  f("'SDL_unicode' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "SDL_ttfContext") || (b.SDL_ttfContext = function() {
  f("'SDL_ttfContext' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "SDL_audio") || (b.SDL_audio = function() {
  f("'SDL_audio' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "SDL") || (b.SDL = function() {
  f("'SDL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "SDL_gfx") || (b.SDL_gfx = function() {
  f("'SDL_gfx' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "GLUT") || (b.GLUT = function() {
  f("'GLUT' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "EGL") || (b.EGL = function() {
  f("'EGL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "GLFW_Window") || (b.GLFW_Window = function() {
  f("'GLFW_Window' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "GLFW") || (b.GLFW = function() {
  f("'GLFW' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "GLEW") || (b.GLEW = function() {
  f("'GLEW' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "IDBStore") || (b.IDBStore = function() {
  f("'IDBStore' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "runAndAbortIfError") || (b.runAndAbortIfError = function() {
  f("'runAndAbortIfError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emval_handle_array") || (b.emval_handle_array = function() {
  f("'emval_handle_array' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emval_free_list") || (b.emval_free_list = function() {
  f("'emval_free_list' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emval_symbols") || (b.emval_symbols = function() {
  f("'emval_symbols' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "init_emval") || (b.init_emval = function() {
  f("'init_emval' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "count_emval_handles") || (b.count_emval_handles = function() {
  f("'count_emval_handles' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "get_first_emval") || (b.get_first_emval = function() {
  f("'get_first_emval' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getStringOrSymbol") || (b.getStringOrSymbol = function() {
  f("'getStringOrSymbol' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "requireHandle") || (b.requireHandle = function() {
  f("'requireHandle' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emval_newers") || (b.emval_newers = function() {
  f("'emval_newers' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "craftEmvalAllocator") || (b.craftEmvalAllocator = function() {
  f("'craftEmvalAllocator' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emval_get_global") || (b.emval_get_global = function() {
  f("'emval_get_global' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "emval_methodCallers") || (b.emval_methodCallers = function() {
  f("'emval_methodCallers' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "InternalError") || (b.InternalError = function() {
  f("'InternalError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "BindingError") || (b.BindingError = function() {
  f("'BindingError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "UnboundTypeError") || (b.UnboundTypeError = function() {
  f("'UnboundTypeError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "PureVirtualError") || (b.PureVirtualError = function() {
  f("'PureVirtualError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "init_embind") || (b.init_embind = function() {
  f("'init_embind' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "throwInternalError") || (b.throwInternalError = function() {
  f("'throwInternalError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "throwBindingError") || (b.throwBindingError = function() {
  f("'throwBindingError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "throwUnboundTypeError") || (b.throwUnboundTypeError = function() {
  f("'throwUnboundTypeError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ensureOverloadTable") || (b.ensureOverloadTable = function() {
  f("'ensureOverloadTable' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "exposePublicSymbol") || (b.exposePublicSymbol = function() {
  f("'exposePublicSymbol' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "replacePublicSymbol") || (b.replacePublicSymbol = function() {
  f("'replacePublicSymbol' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "extendError") || (b.extendError = function() {
  f("'extendError' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "createNamedFunction") || (b.createNamedFunction = function() {
  f("'createNamedFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "registeredInstances") || (b.registeredInstances = function() {
  f("'registeredInstances' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getBasestPointer") || (b.getBasestPointer = function() {
  f("'getBasestPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "registerInheritedInstance") || (b.registerInheritedInstance = function() {
  f("'registerInheritedInstance' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "unregisterInheritedInstance") || (b.unregisterInheritedInstance = function() {
  f("'unregisterInheritedInstance' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getInheritedInstance") || (b.getInheritedInstance = function() {
  f("'getInheritedInstance' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getInheritedInstanceCount") || (b.getInheritedInstanceCount = function() {
  f("'getInheritedInstanceCount' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getLiveInheritedInstances") || (b.getLiveInheritedInstances = function() {
  f("'getLiveInheritedInstances' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "registeredTypes") || (b.registeredTypes = function() {
  f("'registeredTypes' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "awaitingDependencies") || (b.awaitingDependencies = function() {
  f("'awaitingDependencies' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "typeDependencies") || (b.typeDependencies = function() {
  f("'typeDependencies' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "registeredPointers") || (b.registeredPointers = function() {
  f("'registeredPointers' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "registerType") || (b.registerType = function() {
  f("'registerType' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "whenDependentTypesAreResolved") || (b.whenDependentTypesAreResolved = function() {
  f("'whenDependentTypesAreResolved' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "embind_charCodes") || (b.embind_charCodes = function() {
  f("'embind_charCodes' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "embind_init_charCodes") || (b.embind_init_charCodes = function() {
  f("'embind_init_charCodes' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "readLatin1String") || (b.readLatin1String = function() {
  f("'readLatin1String' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getTypeName") || (b.getTypeName = function() {
  f("'getTypeName' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "heap32VectorToArray") || (b.heap32VectorToArray = function() {
  f("'heap32VectorToArray' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "requireRegisteredType") || (b.requireRegisteredType = function() {
  f("'requireRegisteredType' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "getShiftFromSize") || (b.getShiftFromSize = function() {
  f("'getShiftFromSize' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "integerReadValueFromPointer") || (b.integerReadValueFromPointer = function() {
  f("'integerReadValueFromPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "enumReadValueFromPointer") || (b.enumReadValueFromPointer = function() {
  f("'enumReadValueFromPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "floatReadValueFromPointer") || (b.floatReadValueFromPointer = function() {
  f("'floatReadValueFromPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "simpleReadValueFromPointer") || (b.simpleReadValueFromPointer = function() {
  f("'simpleReadValueFromPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "runDestructors") || (b.runDestructors = function() {
  f("'runDestructors' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "new_") || (b.new_ = function() {
  f("'new_' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "craftInvokerFunction") || (b.craftInvokerFunction = function() {
  f("'craftInvokerFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "embind__requireFunction") || (b.embind__requireFunction = function() {
  f("'embind__requireFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "tupleRegistrations") || (b.tupleRegistrations = function() {
  f("'tupleRegistrations' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "structRegistrations") || (b.structRegistrations = function() {
  f("'structRegistrations' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "genericPointerToWireType") || (b.genericPointerToWireType = function() {
  f("'genericPointerToWireType' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "constNoSmartPtrRawPointerToWireType") || (b.constNoSmartPtrRawPointerToWireType = function() {
  f("'constNoSmartPtrRawPointerToWireType' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "nonConstNoSmartPtrRawPointerToWireType") || (b.nonConstNoSmartPtrRawPointerToWireType = function() {
  f("'nonConstNoSmartPtrRawPointerToWireType' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "init_RegisteredPointer") || (b.init_RegisteredPointer = function() {
  f("'init_RegisteredPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "RegisteredPointer") || (b.RegisteredPointer = function() {
  f("'RegisteredPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "RegisteredPointer_getPointee") || (b.RegisteredPointer_getPointee = function() {
  f("'RegisteredPointer_getPointee' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "RegisteredPointer_destructor") || (b.RegisteredPointer_destructor = function() {
  f("'RegisteredPointer_destructor' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "RegisteredPointer_deleteObject") || (b.RegisteredPointer_deleteObject = function() {
  f("'RegisteredPointer_deleteObject' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "RegisteredPointer_fromWireType") || (b.RegisteredPointer_fromWireType = function() {
  f("'RegisteredPointer_fromWireType' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "runDestructor") || (b.runDestructor = function() {
  f("'runDestructor' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "releaseClassHandle") || (b.releaseClassHandle = function() {
  f("'releaseClassHandle' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "finalizationGroup") || (b.finalizationGroup = function() {
  f("'finalizationGroup' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "detachFinalizer_deps") || (b.detachFinalizer_deps = function() {
  f("'detachFinalizer_deps' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "detachFinalizer") || (b.detachFinalizer = function() {
  f("'detachFinalizer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "attachFinalizer") || (b.attachFinalizer = function() {
  f("'attachFinalizer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "makeClassHandle") || (b.makeClassHandle = function() {
  f("'makeClassHandle' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "init_ClassHandle") || (b.init_ClassHandle = function() {
  f("'init_ClassHandle' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ClassHandle") || (b.ClassHandle = function() {
  f("'ClassHandle' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ClassHandle_isAliasOf") || (b.ClassHandle_isAliasOf = function() {
  f("'ClassHandle_isAliasOf' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "throwInstanceAlreadyDeleted") || (b.throwInstanceAlreadyDeleted = function() {
  f("'throwInstanceAlreadyDeleted' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ClassHandle_clone") || (b.ClassHandle_clone = function() {
  f("'ClassHandle_clone' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ClassHandle_delete") || (b.ClassHandle_delete = function() {
  f("'ClassHandle_delete' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "deletionQueue") || (b.deletionQueue = function() {
  f("'deletionQueue' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ClassHandle_isDeleted") || (b.ClassHandle_isDeleted = function() {
  f("'ClassHandle_isDeleted' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "ClassHandle_deleteLater") || (b.ClassHandle_deleteLater = function() {
  f("'ClassHandle_deleteLater' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "flushPendingDeletes") || (b.flushPendingDeletes = function() {
  f("'flushPendingDeletes' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "delayFunction") || (b.delayFunction = function() {
  f("'delayFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "setDelayFunction") || (b.setDelayFunction = function() {
  f("'setDelayFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "RegisteredClass") || (b.RegisteredClass = function() {
  f("'RegisteredClass' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "shallowCopyInternalPointer") || (b.shallowCopyInternalPointer = function() {
  f("'shallowCopyInternalPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "downcastPointer") || (b.downcastPointer = function() {
  f("'downcastPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "upcastPointer") || (b.upcastPointer = function() {
  f("'upcastPointer' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "validateThis") || (b.validateThis = function() {
  f("'validateThis' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "char_0") || (b.char_0 = function() {
  f("'char_0' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "char_9") || (b.char_9 = function() {
  f("'char_9' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "makeLegalFunctionName") || (b.makeLegalFunctionName = function() {
  f("'makeLegalFunctionName' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "warnOnce") || (b.warnOnce = function() {
  f("'warnOnce' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stackSave") || (b.stackSave = function() {
  f("'stackSave' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stackRestore") || (b.stackRestore = function() {
  f("'stackRestore' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stackAlloc") || (b.stackAlloc = function() {
  f("'stackAlloc' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "AsciiToString") || (b.AsciiToString = function() {
  f("'AsciiToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stringToAscii") || (b.stringToAscii = function() {
  f("'stringToAscii' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "UTF16ToString") || (b.UTF16ToString = function() {
  f("'UTF16ToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stringToUTF16") || (b.stringToUTF16 = function() {
  f("'stringToUTF16' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "lengthBytesUTF16") || (b.lengthBytesUTF16 = function() {
  f("'lengthBytesUTF16' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "UTF32ToString") || (b.UTF32ToString = function() {
  f("'UTF32ToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "stringToUTF32") || (b.stringToUTF32 = function() {
  f("'stringToUTF32' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "lengthBytesUTF32") || (b.lengthBytesUTF32 = function() {
  f("'lengthBytesUTF32' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "allocateUTF8") || (b.allocateUTF8 = function() {
  f("'allocateUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
Object.getOwnPropertyDescriptor(b, "allocateUTF8OnStack") || (b.allocateUTF8OnStack = function() {
  f("'allocateUTF8OnStack' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
});
b.writeStackCookie = Ja;
b.checkStackCookie = Ka;
Object.getOwnPropertyDescriptor(b, "ALLOC_NORMAL") || Object.defineProperty(b, "ALLOC_NORMAL", {configurable:!0, get:function() {
  f("'ALLOC_NORMAL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
}});
Object.getOwnPropertyDescriptor(b, "ALLOC_STACK") || Object.defineProperty(b, "ALLOC_STACK", {configurable:!0, get:function() {
  f("'ALLOC_STACK' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
}});
Object.getOwnPropertyDescriptor(b, "ALLOC_DYNAMIC") || Object.defineProperty(b, "ALLOC_DYNAMIC", {configurable:!0, get:function() {
  f("'ALLOC_DYNAMIC' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
}});
Object.getOwnPropertyDescriptor(b, "ALLOC_NONE") || Object.defineProperty(b, "ALLOC_NONE", {configurable:!0, get:function() {
  f("'ALLOC_NONE' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
}});
var td;
Za = function ud() {
  td || vd();
  td || (Za = ud);
};
function vd() {
  function a() {
    if (!td && (td = !0, b.calledRun = !0, !qa)) {
      Ka();
      assert(!Ta);
      Ta = !0;
      if (!b.noFSInit && !bc) {
        assert(!bc, "FS.init was previously called. If you want to initialize later with custom parameters, remove any earlier calls (note that one is automatically added to the generated code)");
        bc = !0;
        ac();
        b.stdin = b.stdin;
        b.stdout = b.stdout;
        b.stderr = b.stderr;
        b.stdin ? dc("stdin", b.stdin) : Xb("/dev/tty", "/dev/stdin");
        b.stdout ? dc("stdout", null, b.stdout) : Xb("/dev/tty", "/dev/stdout");
        b.stderr ? dc("stderr", null, b.stderr) : Xb("/dev/tty1", "/dev/stderr");
        var c = Yb("/dev/stdin", "r"), d = Yb("/dev/stdout", "w"), e = Yb("/dev/stderr", "w");
        assert(0 === c.fd, "invalid handle for stdin (" + c.fd + ")");
        assert(1 === d.fd, "invalid handle for stdout (" + d.fd + ")");
        assert(2 === e.fd, "invalid handle for stderr (" + e.fd + ")");
      }
      Na(Pa);
      Ka();
      Hb = !1;
      Na(Qa);
      aa(b);
      if (b.onRuntimeInitialized) {
        b.onRuntimeInitialized();
      }
      assert(!b._main, 'compiled without a main, but one is present. if you added it from JS, use Module["onRuntimeInitialized"]');
      Ka();
      if (b.postRun) {
        for ("function" == typeof b.postRun && (b.postRun = [b.postRun]); b.postRun.length;) {
          c = b.postRun.shift(), Sa.unshift(c);
        }
      }
      Na(Sa);
    }
  }
  if (!(0 < I)) {
    Ja();
    if (b.preRun) {
      for ("function" == typeof b.preRun && (b.preRun = [b.preRun]); b.preRun.length;) {
        Ua();
      }
    }
    Na(Oa);
    0 < I || (b.setStatus ? (b.setStatus("Running..."), setTimeout(function() {
      setTimeout(function() {
        b.setStatus("");
      }, 1);
      a();
    }, 1)) : a(), Ka());
  }
}
b.run = vd;
if (b.preInit) {
  for ("function" == typeof b.preInit && (b.preInit = [b.preInit]); 0 < b.preInit.length;) {
    b.preInit.pop()();
  }
}
noExitRuntime = !0;
vd();



  return avif_enc.ready
}
);
})();
if (typeof exports === 'object' && typeof module === 'object')
      module.exports = avif_enc;
    else if (typeof define === 'function' && define['amd'])
      define([], function() { return avif_enc; });
    else if (typeof exports === 'object')
      exports["avif_enc"] = avif_enc;
    