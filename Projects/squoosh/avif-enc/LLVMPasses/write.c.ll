; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/write.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/write.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifCodecEncodeOutput = type { %struct.avifEncodeSampleArray }
%struct.avifEncodeSampleArray = type { %struct.avifEncodeSample*, i32, i32, i32 }
%struct.avifEncodeSample = type { %struct.avifRWData, i32 }
%struct.avifRWData = type { i8*, i32 }
%struct.avifEncoder = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i64, %struct.avifIOStats, %struct.avifEncoderData* }
%struct.avifIOStats = type { i32, i32 }
%struct.avifEncoderData = type { %struct.avifEncoderItemArray, %struct.avifEncoderFrameArray, %struct.avifImage*, %struct.avifEncoderItem*, %struct.avifEncoderItem*, i16, i16 }
%struct.avifEncoderItemArray = type { %struct.avifEncoderItem*, i32, i32, i32 }
%struct.avifEncoderFrameArray = type { %struct.avifEncoderFrame*, i32, i32, i32 }
%struct.avifEncoderFrame = type { i64 }
%struct.avifImage = type { i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i8*, i32, i32, %struct.avifRWData, i32, i32, i32, i32, %struct.avifPixelAspectRatioBox, %struct.avifCleanApertureBox, %struct.avifImageRotation, %struct.avifImageMirror, %struct.avifRWData, %struct.avifRWData }
%struct.avifPixelAspectRatioBox = type { i32, i32 }
%struct.avifCleanApertureBox = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifImageRotation = type { i8 }
%struct.avifImageMirror = type { i8 }
%struct.avifEncoderItem = type { i16, [4 x i8], %struct.avifCodec*, %struct.avifCodecEncodeOutput*, %struct.avifRWData, i32, i8*, i32, i8*, i32, %struct.avifOffsetFixupArray, i16, i8*, %struct.ipmaArray }
%struct.avifCodec = type { %struct.avifCodecDecodeInput*, %struct.avifCodecConfigurationBox, %struct.avifCodecInternal*, i32 (%struct.avifCodec*, i32)*, i32 (%struct.avifCodec*, %struct.avifImage*)*, i32 (%struct.avifCodec*, %struct.avifEncoder*, %struct.avifImage*, i32, i32, %struct.avifCodecEncodeOutput*)*, i32 (%struct.avifCodec*, %struct.avifCodecEncodeOutput*)*, void (%struct.avifCodec*)* }
%struct.avifCodecDecodeInput = type { %struct.avifDecodeSampleArray, i32 }
%struct.avifDecodeSampleArray = type { %struct.avifDecodeSample*, i32, i32, i32 }
%struct.avifDecodeSample = type { %struct.avifROData, i32 }
%struct.avifROData = type { i8*, i32 }
%struct.avifCodecConfigurationBox = type { i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.avifCodecInternal = type opaque
%struct.avifOffsetFixupArray = type { %struct.avifOffsetFixup*, i32, i32, i32 }
%struct.avifOffsetFixup = type { i32 }
%struct.ipmaArray = type { [16 x i8], [16 x i32], i8 }
%struct.avifPixelFormatInfo = type { i32, i32, i32 }
%struct.avifRWStream = type { %struct.avifRWData*, i32 }

@.str = private unnamed_addr constant [5 x i8] c"av01\00", align 1
@.str.1 = private unnamed_addr constant [6 x i8] c"Color\00", align 1
@.str.2 = private unnamed_addr constant [6 x i8] c"Alpha\00", align 1
@.str.3 = private unnamed_addr constant [5 x i8] c"auxl\00", align 1
@__const.avifEncoderAddImage.tiffHeaderBE = private unnamed_addr constant [4 x i8] c"MM\00*", align 1
@__const.avifEncoderAddImage.tiffHeaderLE = private unnamed_addr constant [4 x i8] c"II*\00", align 1
@.str.4 = private unnamed_addr constant [5 x i8] c"Exif\00", align 1
@.str.5 = private unnamed_addr constant [5 x i8] c"cdsc\00", align 1
@.str.6 = private unnamed_addr constant [5 x i8] c"mime\00", align 1
@.str.7 = private unnamed_addr constant [4 x i8] c"XMP\00", align 1
@xmpContentType = internal constant [20 x i8] c"application/rdf+xml\00", align 16
@.str.8 = private unnamed_addr constant [5 x i8] c"avif\00", align 1
@.str.9 = private unnamed_addr constant [5 x i8] c"avis\00", align 1
@.str.10 = private unnamed_addr constant [5 x i8] c"ftyp\00", align 1
@.str.11 = private unnamed_addr constant [5 x i8] c"msf1\00", align 1
@.str.12 = private unnamed_addr constant [5 x i8] c"mif1\00", align 1
@.str.13 = private unnamed_addr constant [5 x i8] c"miaf\00", align 1
@.str.14 = private unnamed_addr constant [5 x i8] c"MA1B\00", align 1
@.str.15 = private unnamed_addr constant [5 x i8] c"MA1A\00", align 1
@.str.16 = private unnamed_addr constant [5 x i8] c"meta\00", align 1
@.str.17 = private unnamed_addr constant [5 x i8] c"hdlr\00", align 1
@.str.18 = private unnamed_addr constant [5 x i8] c"pict\00", align 1
@.str.19 = private unnamed_addr constant [8 x i8] c"libavif\00", align 1
@.str.20 = private unnamed_addr constant [5 x i8] c"pitm\00", align 1
@.str.21 = private unnamed_addr constant [5 x i8] c"iloc\00", align 1
@.str.22 = private unnamed_addr constant [5 x i8] c"iinf\00", align 1
@.str.23 = private unnamed_addr constant [5 x i8] c"infe\00", align 1
@.str.24 = private unnamed_addr constant [5 x i8] c"iref\00", align 1
@.str.25 = private unnamed_addr constant [5 x i8] c"iprp\00", align 1
@.str.26 = private unnamed_addr constant [5 x i8] c"ipco\00", align 1
@.str.27 = private unnamed_addr constant [5 x i8] c"ispe\00", align 1
@.str.28 = private unnamed_addr constant [5 x i8] c"pixi\00", align 1
@.str.29 = private unnamed_addr constant [5 x i8] c"auxC\00", align 1
@alphaURN = internal constant [44 x i8] c"urn:mpeg:mpegB:cicp:systems:auxiliary:alpha\00", align 16
@.str.30 = private unnamed_addr constant [5 x i8] c"ipma\00", align 1
@avifEncoderFinish.unityMatrix = internal constant [9 x i32] [i32 65536, i32 0, i32 0, i32 0, i32 65536, i32 0, i32 0, i32 0, i32 1073741824], align 16
@.str.31 = private unnamed_addr constant [5 x i8] c"moov\00", align 1
@.str.32 = private unnamed_addr constant [5 x i8] c"mvhd\00", align 1
@.str.33 = private unnamed_addr constant [5 x i8] c"trak\00", align 1
@.str.34 = private unnamed_addr constant [5 x i8] c"tkhd\00", align 1
@.str.35 = private unnamed_addr constant [5 x i8] c"tref\00", align 1
@.str.36 = private unnamed_addr constant [5 x i8] c"mdia\00", align 1
@.str.37 = private unnamed_addr constant [5 x i8] c"mdhd\00", align 1
@.str.38 = private unnamed_addr constant [5 x i8] c"minf\00", align 1
@.str.39 = private unnamed_addr constant [5 x i8] c"vmhd\00", align 1
@.str.40 = private unnamed_addr constant [5 x i8] c"dinf\00", align 1
@.str.41 = private unnamed_addr constant [5 x i8] c"dref\00", align 1
@.str.42 = private unnamed_addr constant [5 x i8] c"url \00", align 1
@.str.43 = private unnamed_addr constant [5 x i8] c"stbl\00", align 1
@.str.44 = private unnamed_addr constant [5 x i8] c"stco\00", align 1
@.str.45 = private unnamed_addr constant [5 x i8] c"stsc\00", align 1
@.str.46 = private unnamed_addr constant [5 x i8] c"stsz\00", align 1
@.str.47 = private unnamed_addr constant [5 x i8] c"stss\00", align 1
@.str.48 = private unnamed_addr constant [5 x i8] c"stts\00", align 1
@.str.49 = private unnamed_addr constant [5 x i8] c"stsd\00", align 1
@.str.50 = private unnamed_addr constant [12 x i8] c"\0AAOM Coding\00", align 1
@.str.51 = private unnamed_addr constant [5 x i8] c"mdat\00", align 1
@.str.52 = private unnamed_addr constant [5 x i8] c"colr\00", align 1
@.str.53 = private unnamed_addr constant [5 x i8] c"prof\00", align 1
@.str.54 = private unnamed_addr constant [5 x i8] c"nclx\00", align 1
@.str.55 = private unnamed_addr constant [5 x i8] c"pasp\00", align 1
@.str.56 = private unnamed_addr constant [5 x i8] c"clap\00", align 1
@.str.57 = private unnamed_addr constant [5 x i8] c"irot\00", align 1
@.str.58 = private unnamed_addr constant [5 x i8] c"imir\00", align 1
@.str.59 = private unnamed_addr constant [5 x i8] c"av1C\00", align 1

; Function Attrs: nounwind
define hidden %struct.avifCodecEncodeOutput* @avifCodecEncodeOutputCreate() #0 {
entry:
  %encodeOutput = alloca %struct.avifCodecEncodeOutput*, align 4
  %0 = bitcast %struct.avifCodecEncodeOutput** %encodeOutput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 16)
  %1 = bitcast i8* %call to %struct.avifCodecEncodeOutput*
  store %struct.avifCodecEncodeOutput* %1, %struct.avifCodecEncodeOutput** %encodeOutput, align 4, !tbaa !2
  %2 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput, align 4, !tbaa !2
  %3 = bitcast %struct.avifCodecEncodeOutput* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 16, i1 false)
  %4 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput, align 4, !tbaa !2
  %samples = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %4, i32 0, i32 0
  %5 = bitcast %struct.avifEncodeSampleArray* %samples to i8*
  call void @avifArrayCreate(i8* %5, i32 12, i32 1)
  %6 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput, align 4, !tbaa !2
  %7 = bitcast %struct.avifCodecEncodeOutput** %encodeOutput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret %struct.avifCodecEncodeOutput* %6
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @avifAlloc(i32) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare void @avifArrayCreate(i8*, i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @avifCodecEncodeOutputAddSample(%struct.avifCodecEncodeOutput* %encodeOutput, i8* %data, i32 %len, i32 %sync) #0 {
entry:
  %encodeOutput.addr = alloca %struct.avifCodecEncodeOutput*, align 4
  %data.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %sync.addr = alloca i32, align 4
  %sample = alloca %struct.avifEncodeSample*, align 4
  store %struct.avifCodecEncodeOutput* %encodeOutput, %struct.avifCodecEncodeOutput** %encodeOutput.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !6
  store i32 %sync, i32* %sync.addr, align 4, !tbaa !8
  %0 = bitcast %struct.avifEncodeSample** %sample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput.addr, align 4, !tbaa !2
  %samples = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %1, i32 0, i32 0
  %2 = bitcast %struct.avifEncodeSampleArray* %samples to i8*
  %call = call i8* @avifArrayPushPtr(i8* %2)
  %3 = bitcast i8* %call to %struct.avifEncodeSample*
  store %struct.avifEncodeSample* %3, %struct.avifEncodeSample** %sample, align 4, !tbaa !2
  %4 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %4, i32 0, i32 0
  %5 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !6
  call void @avifRWDataSet(%struct.avifRWData* %data1, i8* %5, i32 %6)
  %7 = load i32, i32* %sync.addr, align 4, !tbaa !8
  %8 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample, align 4, !tbaa !2
  %sync2 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %8, i32 0, i32 1
  store i32 %7, i32* %sync2, align 4, !tbaa !10
  %9 = bitcast %struct.avifEncodeSample** %sample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  ret void
}

declare i8* @avifArrayPushPtr(i8*) #2

declare void @avifRWDataSet(%struct.avifRWData*, i8*, i32) #2

; Function Attrs: nounwind
define hidden void @avifCodecEncodeOutputDestroy(%struct.avifCodecEncodeOutput* %encodeOutput) #0 {
entry:
  %encodeOutput.addr = alloca %struct.avifCodecEncodeOutput*, align 4
  %sampleIndex = alloca i32, align 4
  store %struct.avifCodecEncodeOutput* %encodeOutput, %struct.avifCodecEncodeOutput** %encodeOutput.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sampleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %sampleIndex, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %sampleIndex, align 4, !tbaa !8
  %2 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput.addr, align 4, !tbaa !2
  %samples = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %2, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !13
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %sampleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput.addr, align 4, !tbaa !2
  %samples1 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %5, i32 0, i32 0
  %sample = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples1, i32 0, i32 0
  %6 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample, align 4, !tbaa !16
  %7 = load i32, i32* %sampleIndex, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %6, i32 %7
  %data = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %arrayidx, i32 0, i32 0
  call void @avifRWDataFree(%struct.avifRWData* %data)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %sampleIndex, align 4, !tbaa !8
  %inc = add i32 %8, 1
  store i32 %inc, i32* %sampleIndex, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %9 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput.addr, align 4, !tbaa !2
  %samples2 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %9, i32 0, i32 0
  %10 = bitcast %struct.avifEncodeSampleArray* %samples2 to i8*
  call void @avifArrayDestroy(i8* %10)
  %11 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput.addr, align 4, !tbaa !2
  %12 = bitcast %struct.avifCodecEncodeOutput* %11 to i8*
  call void @avifFree(i8* %12)
  ret void
}

declare void @avifRWDataFree(%struct.avifRWData*) #2

declare void @avifArrayDestroy(i8*) #2

declare void @avifFree(i8*) #2

; Function Attrs: nounwind
define hidden %struct.avifEncoder* @avifEncoderCreate() #0 {
entry:
  %encoder = alloca %struct.avifEncoder*, align 4
  %0 = bitcast %struct.avifEncoder** %encoder to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 64)
  %1 = bitcast i8* %call to %struct.avifEncoder*
  store %struct.avifEncoder* %1, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %2 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %3 = bitcast %struct.avifEncoder* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %3, i8 0, i32 64, i1 false)
  %4 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %maxThreads = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %4, i32 0, i32 1
  store i32 1, i32* %maxThreads, align 4, !tbaa !17
  %5 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %minQuantizer = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %5, i32 0, i32 2
  store i32 0, i32* %minQuantizer, align 8, !tbaa !21
  %6 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %maxQuantizer = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %6, i32 0, i32 3
  store i32 0, i32* %maxQuantizer, align 4, !tbaa !22
  %7 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %minQuantizerAlpha = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %7, i32 0, i32 4
  store i32 0, i32* %minQuantizerAlpha, align 8, !tbaa !23
  %8 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %maxQuantizerAlpha = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %8, i32 0, i32 5
  store i32 0, i32* %maxQuantizerAlpha, align 4, !tbaa !24
  %9 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %tileRowsLog2 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %9, i32 0, i32 6
  store i32 0, i32* %tileRowsLog2, align 8, !tbaa !25
  %10 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %tileColsLog2 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %10, i32 0, i32 7
  store i32 0, i32* %tileColsLog2, align 4, !tbaa !26
  %11 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %speed = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %11, i32 0, i32 8
  store i32 -1, i32* %speed, align 8, !tbaa !27
  %call1 = call %struct.avifEncoderData* @avifEncoderDataCreate()
  %12 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %12, i32 0, i32 12
  store %struct.avifEncoderData* %call1, %struct.avifEncoderData** %data, align 8, !tbaa !28
  %13 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %timescale = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %13, i32 0, i32 10
  store i64 1, i64* %timescale, align 8, !tbaa !29
  %14 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %keyframeInterval = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %14, i32 0, i32 9
  store i32 0, i32* %keyframeInterval, align 4, !tbaa !30
  %15 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4, !tbaa !2
  %16 = bitcast %struct.avifEncoder** %encoder to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  ret %struct.avifEncoder* %15
}

; Function Attrs: nounwind
define hidden void @avifEncoderDestroy(%struct.avifEncoder* %encoder) #0 {
entry:
  %encoder.addr = alloca %struct.avifEncoder*, align 4
  store %struct.avifEncoder* %encoder, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %0 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %0, i32 0, i32 12
  %1 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 8, !tbaa !28
  call void @avifEncoderDataDestroy(%struct.avifEncoderData* %1)
  %2 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %3 = bitcast %struct.avifEncoder* %2 to i8*
  call void @avifFree(i8* %3)
  ret void
}

; Function Attrs: nounwind
define internal void @avifEncoderDataDestroy(%struct.avifEncoderData* %data) #0 {
entry:
  %data.addr = alloca %struct.avifEncoderData*, align 4
  %i = alloca i32, align 4
  %item = alloca %struct.avifEncoderItem*, align 4
  store %struct.avifEncoderData* %data, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !8
  %2 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %items = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %2, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !31
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %items1 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %6, i32 0, i32 0
  %item2 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items1, i32 0, i32 0
  %7 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item2, align 4, !tbaa !36
  %8 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %7, i32 %8
  store %struct.avifEncoderItem* %arrayidx, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %9 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %codec = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %9, i32 0, i32 2
  %10 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !37
  %tobool = icmp ne %struct.avifCodec* %10, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %11 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %codec3 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %11, i32 0, i32 2
  %12 = load %struct.avifCodec*, %struct.avifCodec** %codec3, align 4, !tbaa !37
  call void @avifCodecDestroy(%struct.avifCodec* %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %13 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %encodeOutput = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %13, i32 0, i32 3
  %14 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput, align 4, !tbaa !41
  call void @avifCodecEncodeOutputDestroy(%struct.avifCodecEncodeOutput* %14)
  %15 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %metadataPayload = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %15, i32 0, i32 4
  call void @avifRWDataFree(%struct.avifRWData* %metadataPayload)
  %16 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %mdatFixups = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %16, i32 0, i32 10
  %17 = bitcast %struct.avifOffsetFixupArray* %mdatFixups to i8*
  call void @avifArrayDestroy(i8* %17)
  %18 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %imageMetadata = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %20, i32 0, i32 2
  %21 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !42
  call void @avifImageDestroy(%struct.avifImage* %21)
  %22 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %items4 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %22, i32 0, i32 0
  %23 = bitcast %struct.avifEncoderItemArray* %items4 to i8*
  call void @avifArrayDestroy(i8* %23)
  %24 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %frames = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %24, i32 0, i32 1
  %25 = bitcast %struct.avifEncoderFrameArray* %frames to i8*
  call void @avifArrayDestroy(i8* %25)
  %26 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %27 = bitcast %struct.avifEncoderData* %26 to i8*
  call void @avifFree(i8* %27)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @avifEncoderAddImage(%struct.avifEncoder* %encoder, %struct.avifImage* %image, i64 %durationInTimescales, i32 %addImageFlags) #0 {
entry:
  %retval = alloca i32, align 4
  %encoder.addr = alloca %struct.avifEncoder*, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %durationInTimescales.addr = alloca i64, align 8
  %addImageFlags.addr = alloca i32, align 4
  %needsAlpha = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %exifTiffHeaderOffset = alloca i32, align 4
  %tiffHeaderBE = alloca [4 x i8], align 1
  %tiffHeaderLE = alloca [4 x i8], align 1
  %exifItem = alloca %struct.avifEncoderItem*, align 4
  %xmpItem = alloca %struct.avifEncoderItem*, align 4
  %itemIndex = alloca i32, align 4
  %item = alloca %struct.avifEncoderItem*, align 4
  %itemIndex178 = alloca i32, align 4
  %item187 = alloca %struct.avifEncoderItem*, align 4
  %frame = alloca %struct.avifEncoderFrame*, align 4
  store %struct.avifEncoder* %encoder, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store i64 %durationInTimescales, i64* %durationInTimescales.addr, align 8, !tbaa !43
  store i32 %addImageFlags, i32* %addImageFlags.addr, align 4, !tbaa !8
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 2
  %1 = load i32, i32* %depth, align 4, !tbaa !44
  %cmp = icmp ne i32 %1, 8
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %2, i32 0, i32 2
  %3 = load i32, i32* %depth1, align 4, !tbaa !44
  %cmp2 = icmp ne i32 %3, 10
  br i1 %cmp2, label %land.lhs.true3, label %if.end

land.lhs.true3:                                   ; preds = %land.lhs.true
  %4 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth4 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %4, i32 0, i32 2
  %5 = load i32, i32* %depth4, align 4, !tbaa !44
  %cmp5 = icmp ne i32 %5, 12
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true3
  store i32 6, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true3, %land.lhs.true, %entry
  %6 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %6, i32 0, i32 0
  %7 = load i32, i32* %width, align 4, !tbaa !50
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then9

lor.lhs.false:                                    ; preds = %if.end
  %8 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %8, i32 0, i32 1
  %9 = load i32, i32* %height, align 4, !tbaa !51
  %tobool6 = icmp ne i32 %9, 0
  br i1 %tobool6, label %lor.lhs.false7, label %if.then9

lor.lhs.false7:                                   ; preds = %lor.lhs.false
  %10 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %10, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %11 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %tobool8 = icmp ne i8* %11, null
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %lor.lhs.false7, %lor.lhs.false, %if.end
  store i32 3, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %lor.lhs.false7
  %12 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 3
  %13 = load i32, i32* %yuvFormat, align 4, !tbaa !52
  %cmp11 = icmp eq i32 %13, 0
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end10
  store i32 4, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %if.end10
  %14 = load i64, i64* %durationInTimescales.addr, align 8, !tbaa !43
  %cmp14 = icmp eq i64 %14, 0
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end13
  store i64 1, i64* %durationInTimescales.addr, align 8, !tbaa !43
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %if.end13
  %15 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %15, i32 0, i32 12
  %16 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 8, !tbaa !28
  %items = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %16, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items, i32 0, i32 2
  %17 = load i32, i32* %count, align 4, !tbaa !31
  %cmp17 = icmp eq i32 %17, 0
  br i1 %cmp17, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.end16
  %18 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data19 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %18, i32 0, i32 12
  %19 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data19, align 8, !tbaa !28
  %imageMetadata = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %19, i32 0, i32 2
  %20 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !42
  %21 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  call void @avifImageCopy(%struct.avifImage* %20, %struct.avifImage* %21, i32 0)
  %22 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data20 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %22, i32 0, i32 12
  %23 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data20, align 8, !tbaa !28
  %call = call %struct.avifEncoderItem* @avifEncoderDataCreateItem(%struct.avifEncoderData* %23, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.1, i32 0, i32 0), i32 6)
  %24 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data21 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %24, i32 0, i32 12
  %25 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data21, align 8, !tbaa !28
  %colorItem = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %25, i32 0, i32 3
  store %struct.avifEncoderItem* %call, %struct.avifEncoderItem** %colorItem, align 4, !tbaa !53
  %26 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %codecChoice = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %26, i32 0, i32 0
  %27 = load i32, i32* %codecChoice, align 8, !tbaa !54
  %call22 = call %struct.avifCodec* @avifCodecCreate(i32 %27, i32 2)
  %28 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data23 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %28, i32 0, i32 12
  %29 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data23, align 8, !tbaa !28
  %colorItem24 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %29, i32 0, i32 3
  %30 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %colorItem24, align 4, !tbaa !53
  %codec = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %30, i32 0, i32 2
  store %struct.avifCodec* %call22, %struct.avifCodec** %codec, align 4, !tbaa !37
  %31 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data25 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %31, i32 0, i32 12
  %32 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data25, align 8, !tbaa !28
  %colorItem26 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %32, i32 0, i32 3
  %33 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %colorItem26, align 4, !tbaa !53
  %codec27 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %33, i32 0, i32 2
  %34 = load %struct.avifCodec*, %struct.avifCodec** %codec27, align 4, !tbaa !37
  %tobool28 = icmp ne %struct.avifCodec* %34, null
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %if.then18
  store i32 15, i32* %retval, align 4
  br label %return

if.end30:                                         ; preds = %if.then18
  %35 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data31 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %35, i32 0, i32 12
  %36 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data31, align 8, !tbaa !28
  %colorItem32 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %36, i32 0, i32 3
  %37 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %colorItem32, align 4, !tbaa !53
  %id = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %37, i32 0, i32 0
  %38 = load i16, i16* %id, align 4, !tbaa !55
  %39 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data33 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %39, i32 0, i32 12
  %40 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data33, align 8, !tbaa !28
  %primaryItemID = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %40, i32 0, i32 6
  store i16 %38, i16* %primaryItemID, align 2, !tbaa !56
  %41 = bitcast i32* %needsAlpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  %42 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %42, i32 0, i32 10
  %43 = load i8*, i8** %alphaPlane, align 4, !tbaa !57
  %cmp34 = icmp ne i8* %43, null
  %conv = zext i1 %cmp34 to i32
  store i32 %conv, i32* %needsAlpha, align 4, !tbaa !8
  %44 = load i32, i32* %addImageFlags.addr, align 4, !tbaa !8
  %and = and i32 %44, 2
  %tobool35 = icmp ne i32 %and, 0
  br i1 %tobool35, label %if.then36, label %if.end39

if.then36:                                        ; preds = %if.end30
  %45 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %call37 = call i32 @avifImageIsOpaque(%struct.avifImage* %45)
  %tobool38 = icmp ne i32 %call37, 0
  %lnot = xor i1 %tobool38, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %needsAlpha, align 4, !tbaa !8
  br label %if.end39

if.end39:                                         ; preds = %if.then36, %if.end30
  %46 = load i32, i32* %needsAlpha, align 4, !tbaa !8
  %tobool40 = icmp ne i32 %46, 0
  br i1 %tobool40, label %if.then41, label %if.end64

if.then41:                                        ; preds = %if.end39
  %47 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data42 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %47, i32 0, i32 12
  %48 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data42, align 8, !tbaa !28
  %call43 = call %struct.avifEncoderItem* @avifEncoderDataCreateItem(%struct.avifEncoderData* %48, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i32 0, i32 0), i32 6)
  %49 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data44 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %49, i32 0, i32 12
  %50 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data44, align 8, !tbaa !28
  %alphaItem = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %50, i32 0, i32 4
  store %struct.avifEncoderItem* %call43, %struct.avifEncoderItem** %alphaItem, align 4, !tbaa !58
  %51 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %codecChoice45 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %51, i32 0, i32 0
  %52 = load i32, i32* %codecChoice45, align 8, !tbaa !54
  %call46 = call %struct.avifCodec* @avifCodecCreate(i32 %52, i32 2)
  %53 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data47 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %53, i32 0, i32 12
  %54 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data47, align 8, !tbaa !28
  %alphaItem48 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %54, i32 0, i32 4
  %55 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %alphaItem48, align 4, !tbaa !58
  %codec49 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %55, i32 0, i32 2
  store %struct.avifCodec* %call46, %struct.avifCodec** %codec49, align 4, !tbaa !37
  %56 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data50 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %56, i32 0, i32 12
  %57 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data50, align 8, !tbaa !28
  %alphaItem51 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %57, i32 0, i32 4
  %58 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %alphaItem51, align 4, !tbaa !58
  %codec52 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %58, i32 0, i32 2
  %59 = load %struct.avifCodec*, %struct.avifCodec** %codec52, align 4, !tbaa !37
  %tobool53 = icmp ne %struct.avifCodec* %59, null
  br i1 %tobool53, label %if.end55, label %if.then54

if.then54:                                        ; preds = %if.then41
  store i32 15, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup157

if.end55:                                         ; preds = %if.then41
  %60 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data56 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %60, i32 0, i32 12
  %61 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data56, align 8, !tbaa !28
  %alphaItem57 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %61, i32 0, i32 4
  %62 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %alphaItem57, align 4, !tbaa !58
  %alpha = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %62, i32 0, i32 5
  store i32 1, i32* %alpha, align 4, !tbaa !59
  %63 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data58 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %63, i32 0, i32 12
  %64 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data58, align 8, !tbaa !28
  %primaryItemID59 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %64, i32 0, i32 6
  %65 = load i16, i16* %primaryItemID59, align 2, !tbaa !56
  %66 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data60 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %66, i32 0, i32 12
  %67 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data60, align 8, !tbaa !28
  %alphaItem61 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %67, i32 0, i32 4
  %68 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %alphaItem61, align 4, !tbaa !58
  %irefToID = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %68, i32 0, i32 11
  store i16 %65, i16* %irefToID, align 4, !tbaa !60
  %69 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data62 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %69, i32 0, i32 12
  %70 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data62, align 8, !tbaa !28
  %alphaItem63 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %70, i32 0, i32 4
  %71 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %alphaItem63, align 4, !tbaa !58
  %irefType = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %71, i32 0, i32 12
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0), i8** %irefType, align 4, !tbaa !61
  br label %if.end64

if.end64:                                         ; preds = %if.end55, %if.end39
  %72 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif = getelementptr inbounds %struct.avifImage, %struct.avifImage* %72, i32 0, i32 22
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif, i32 0, i32 1
  %73 = load i32, i32* %size, align 4, !tbaa !62
  %cmp65 = icmp ugt i32 %73, 0
  br i1 %cmp65, label %if.then67, label %if.end119

if.then67:                                        ; preds = %if.end64
  %74 = bitcast i32* %exifTiffHeaderOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #4
  store i32 0, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  %75 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif68 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %75, i32 0, i32 22
  %size69 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif68, i32 0, i32 1
  %76 = load i32, i32* %size69, align 4, !tbaa !62
  %cmp70 = icmp ult i32 %76, 4
  br i1 %cmp70, label %if.then72, label %if.end73

if.then72:                                        ; preds = %if.then67
  store i32 17, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

if.end73:                                         ; preds = %if.then67
  %77 = bitcast [4 x i8]* %tiffHeaderBE to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #4
  %78 = bitcast [4 x i8]* %tiffHeaderBE to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %78, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @__const.avifEncoderAddImage.tiffHeaderBE, i32 0, i32 0), i32 4, i1 false)
  %79 = bitcast [4 x i8]* %tiffHeaderLE to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #4
  %80 = bitcast [4 x i8]* %tiffHeaderLE to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %80, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @__const.avifEncoderAddImage.tiffHeaderLE, i32 0, i32 0), i32 4, i1 false)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end73
  %81 = load i32, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  %82 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif74 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %82, i32 0, i32 22
  %size75 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif74, i32 0, i32 1
  %83 = load i32, i32* %size75, align 4, !tbaa !62
  %sub = sub i32 %83, 4
  %cmp76 = icmp ult i32 %81, %sub
  br i1 %cmp76, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %84 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif78 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %84, i32 0, i32 22
  %data79 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif78, i32 0, i32 0
  %85 = load i8*, i8** %data79, align 4, !tbaa !63
  %86 = load i32, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  %arrayidx80 = getelementptr inbounds i8, i8* %85, i32 %86
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %tiffHeaderBE, i32 0, i32 0
  %call81 = call i32 @memcmp(i8* %arrayidx80, i8* %arraydecay, i32 4)
  %tobool82 = icmp ne i32 %call81, 0
  br i1 %tobool82, label %if.end84, label %if.then83

if.then83:                                        ; preds = %for.body
  br label %for.end

if.end84:                                         ; preds = %for.body
  %87 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif85 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %87, i32 0, i32 22
  %data86 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif85, i32 0, i32 0
  %88 = load i8*, i8** %data86, align 4, !tbaa !63
  %89 = load i32, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  %arrayidx87 = getelementptr inbounds i8, i8* %88, i32 %89
  %arraydecay88 = getelementptr inbounds [4 x i8], [4 x i8]* %tiffHeaderLE, i32 0, i32 0
  %call89 = call i32 @memcmp(i8* %arrayidx87, i8* %arraydecay88, i32 4)
  %tobool90 = icmp ne i32 %call89, 0
  br i1 %tobool90, label %if.end92, label %if.then91

if.then91:                                        ; preds = %if.end84
  br label %for.end

if.end92:                                         ; preds = %if.end84
  br label %for.inc

for.inc:                                          ; preds = %if.end92
  %90 = load i32, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  %inc = add i32 %90, 1
  store i32 %inc, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %if.then91, %if.then83, %for.cond
  %91 = load i32, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  %92 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif93 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %92, i32 0, i32 22
  %size94 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif93, i32 0, i32 1
  %93 = load i32, i32* %size94, align 4, !tbaa !62
  %sub95 = sub i32 %93, 4
  %cmp96 = icmp uge i32 %91, %sub95
  br i1 %cmp96, label %if.then98, label %if.end99

if.then98:                                        ; preds = %for.end
  store i32 17, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end99:                                         ; preds = %for.end
  %94 = bitcast %struct.avifEncoderItem** %exifItem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #4
  %95 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data100 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %95, i32 0, i32 12
  %96 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data100, align 8, !tbaa !28
  %call101 = call %struct.avifEncoderItem* @avifEncoderDataCreateItem(%struct.avifEncoderData* %96, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0), i32 5)
  store %struct.avifEncoderItem* %call101, %struct.avifEncoderItem** %exifItem, align 4, !tbaa !2
  %97 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data102 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %97, i32 0, i32 12
  %98 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data102, align 8, !tbaa !28
  %primaryItemID103 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %98, i32 0, i32 6
  %99 = load i16, i16* %primaryItemID103, align 2, !tbaa !56
  %100 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %exifItem, align 4, !tbaa !2
  %irefToID104 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %100, i32 0, i32 11
  store i16 %99, i16* %irefToID104, align 4, !tbaa !60
  %101 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %exifItem, align 4, !tbaa !2
  %irefType105 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %101, i32 0, i32 12
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0), i8** %irefType105, align 4, !tbaa !61
  %102 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %exifItem, align 4, !tbaa !2
  %metadataPayload = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %102, i32 0, i32 4
  %103 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif106 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %103, i32 0, i32 22
  %size107 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif106, i32 0, i32 1
  %104 = load i32, i32* %size107, align 4, !tbaa !62
  %add = add i32 4, %104
  call void @avifRWDataRealloc(%struct.avifRWData* %metadataPayload, i32 %add)
  %105 = load i32, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  %call108 = call i32 @avifHTONL(i32 %105)
  store i32 %call108, i32* %exifTiffHeaderOffset, align 4, !tbaa !8
  %106 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %exifItem, align 4, !tbaa !2
  %metadataPayload109 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %106, i32 0, i32 4
  %data110 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %metadataPayload109, i32 0, i32 0
  %107 = load i8*, i8** %data110, align 4, !tbaa !64
  %108 = bitcast i32* %exifTiffHeaderOffset to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %107, i8* align 4 %108, i32 4, i1 false)
  %109 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %exifItem, align 4, !tbaa !2
  %metadataPayload111 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %109, i32 0, i32 4
  %data112 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %metadataPayload111, i32 0, i32 0
  %110 = load i8*, i8** %data112, align 4, !tbaa !64
  %add.ptr = getelementptr inbounds i8, i8* %110, i32 4
  %111 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif113 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %111, i32 0, i32 22
  %data114 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif113, i32 0, i32 0
  %112 = load i8*, i8** %data114, align 4, !tbaa !63
  %113 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %exif115 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %113, i32 0, i32 22
  %size116 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %exif115, i32 0, i32 1
  %114 = load i32, i32* %size116, align 4, !tbaa !62
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %112, i32 %114, i1 false)
  %115 = bitcast %struct.avifEncoderItem** %exifItem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end99, %if.then98
  %116 = bitcast [4 x i8]* %tiffHeaderLE to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #4
  %117 = bitcast [4 x i8]* %tiffHeaderBE to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #4
  br label %cleanup118

cleanup118:                                       ; preds = %cleanup, %if.then72
  %118 = bitcast i32* %exifTiffHeaderOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup157 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup118
  br label %if.end119

if.end119:                                        ; preds = %cleanup.cont, %if.end64
  %119 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %xmp = getelementptr inbounds %struct.avifImage, %struct.avifImage* %119, i32 0, i32 23
  %size120 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %xmp, i32 0, i32 1
  %120 = load i32, i32* %size120, align 4, !tbaa !65
  %cmp121 = icmp ugt i32 %120, 0
  br i1 %cmp121, label %if.then123, label %if.end135

if.then123:                                       ; preds = %if.end119
  %121 = bitcast %struct.avifEncoderItem** %xmpItem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #4
  %122 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data124 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %122, i32 0, i32 12
  %123 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data124, align 8, !tbaa !28
  %call125 = call %struct.avifEncoderItem* @avifEncoderDataCreateItem(%struct.avifEncoderData* %123, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.6, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.7, i32 0, i32 0), i32 4)
  store %struct.avifEncoderItem* %call125, %struct.avifEncoderItem** %xmpItem, align 4, !tbaa !2
  %124 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data126 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %124, i32 0, i32 12
  %125 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data126, align 8, !tbaa !28
  %primaryItemID127 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %125, i32 0, i32 6
  %126 = load i16, i16* %primaryItemID127, align 2, !tbaa !56
  %127 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %xmpItem, align 4, !tbaa !2
  %irefToID128 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %127, i32 0, i32 11
  store i16 %126, i16* %irefToID128, align 4, !tbaa !60
  %128 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %xmpItem, align 4, !tbaa !2
  %irefType129 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %128, i32 0, i32 12
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0), i8** %irefType129, align 4, !tbaa !61
  %129 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %xmpItem, align 4, !tbaa !2
  %infeContentType = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %129, i32 0, i32 8
  store i8* getelementptr inbounds ([20 x i8], [20 x i8]* @xmpContentType, i32 0, i32 0), i8** %infeContentType, align 4, !tbaa !66
  %130 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %xmpItem, align 4, !tbaa !2
  %infeContentTypeSize = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %130, i32 0, i32 9
  store i32 20, i32* %infeContentTypeSize, align 4, !tbaa !67
  %131 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %xmpItem, align 4, !tbaa !2
  %metadataPayload130 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %131, i32 0, i32 4
  %132 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %xmp131 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %132, i32 0, i32 23
  %data132 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %xmp131, i32 0, i32 0
  %133 = load i8*, i8** %data132, align 4, !tbaa !68
  %134 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %xmp133 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %134, i32 0, i32 23
  %size134 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %xmp133, i32 0, i32 1
  %135 = load i32, i32* %size134, align 4, !tbaa !65
  call void @avifRWDataSet(%struct.avifRWData* %metadataPayload130, i8* %133, i32 %135)
  %136 = bitcast %struct.avifEncoderItem** %xmpItem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  br label %if.end135

if.end135:                                        ; preds = %if.then123, %if.end119
  %137 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #4
  store i32 0, i32* %itemIndex, align 4, !tbaa !8
  br label %for.cond136

for.cond136:                                      ; preds = %for.inc153, %if.end135
  %138 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %139 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data137 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %139, i32 0, i32 12
  %140 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data137, align 8, !tbaa !28
  %items138 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %140, i32 0, i32 0
  %count139 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items138, i32 0, i32 2
  %141 = load i32, i32* %count139, align 4, !tbaa !31
  %cmp140 = icmp ult i32 %138, %141
  br i1 %cmp140, label %for.body142, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond136
  store i32 5, i32* %cleanup.dest.slot, align 4
  %142 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  br label %for.end156

for.body142:                                      ; preds = %for.cond136
  %143 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %143) #4
  %144 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data143 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %144, i32 0, i32 12
  %145 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data143, align 8, !tbaa !28
  %items144 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %145, i32 0, i32 0
  %item145 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items144, i32 0, i32 0
  %146 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !36
  %147 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %arrayidx146 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %146, i32 %147
  store %struct.avifEncoderItem* %arrayidx146, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %148 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %codec147 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %148, i32 0, i32 2
  %149 = load %struct.avifCodec*, %struct.avifCodec** %codec147, align 4, !tbaa !37
  %tobool148 = icmp ne %struct.avifCodec* %149, null
  br i1 %tobool148, label %if.then149, label %if.end152

if.then149:                                       ; preds = %for.body142
  %150 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %codec150 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %150, i32 0, i32 2
  %151 = load %struct.avifCodec*, %struct.avifCodec** %codec150, align 4, !tbaa !37
  %152 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %153 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %alpha151 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %153, i32 0, i32 5
  %154 = load i32, i32* %alpha151, align 4, !tbaa !59
  call void @fillConfigBox(%struct.avifCodec* %151, %struct.avifImage* %152, i32 %154)
  br label %if.end152

if.end152:                                        ; preds = %if.then149, %for.body142
  %155 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #4
  br label %for.inc153

for.inc153:                                       ; preds = %if.end152
  %156 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %inc154 = add i32 %156, 1
  store i32 %inc154, i32* %itemIndex, align 4, !tbaa !8
  br label %for.cond136

for.end156:                                       ; preds = %for.cond.cleanup
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup157

cleanup157:                                       ; preds = %for.end156, %cleanup118, %if.then54
  %157 = bitcast i32* %needsAlpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %cleanup.dest158 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest158, label %unreachable [
    i32 0, label %cleanup.cont159
    i32 1, label %return
  ]

cleanup.cont159:                                  ; preds = %cleanup157
  br label %if.end168

if.else:                                          ; preds = %if.end16
  %158 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data160 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %158, i32 0, i32 12
  %159 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data160, align 8, !tbaa !28
  %alphaItem161 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %159, i32 0, i32 4
  %160 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %alphaItem161, align 4, !tbaa !58
  %tobool162 = icmp ne %struct.avifEncoderItem* %160, null
  br i1 %tobool162, label %land.lhs.true163, label %if.end167

land.lhs.true163:                                 ; preds = %if.else
  %161 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane164 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %161, i32 0, i32 10
  %162 = load i8*, i8** %alphaPlane164, align 4, !tbaa !57
  %tobool165 = icmp ne i8* %162, null
  br i1 %tobool165, label %if.end167, label %if.then166

if.then166:                                       ; preds = %land.lhs.true163
  store i32 8, i32* %retval, align 4
  br label %return

if.end167:                                        ; preds = %land.lhs.true163, %if.else
  br label %if.end168

if.end168:                                        ; preds = %if.end167, %cleanup.cont159
  %163 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %keyframeInterval = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %163, i32 0, i32 9
  %164 = load i32, i32* %keyframeInterval, align 4, !tbaa !30
  %tobool169 = icmp ne i32 %164, 0
  br i1 %tobool169, label %land.lhs.true170, label %if.end177

land.lhs.true170:                                 ; preds = %if.end168
  %165 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data171 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %165, i32 0, i32 12
  %166 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data171, align 8, !tbaa !28
  %frames = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %166, i32 0, i32 1
  %count172 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames, i32 0, i32 2
  %167 = load i32, i32* %count172, align 4, !tbaa !69
  %168 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %keyframeInterval173 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %168, i32 0, i32 9
  %169 = load i32, i32* %keyframeInterval173, align 4, !tbaa !30
  %rem = urem i32 %167, %169
  %cmp174 = icmp eq i32 %rem, 0
  br i1 %cmp174, label %if.then176, label %if.end177

if.then176:                                       ; preds = %land.lhs.true170
  %170 = load i32, i32* %addImageFlags.addr, align 4, !tbaa !8
  %or = or i32 %170, 1
  store i32 %or, i32* %addImageFlags.addr, align 4, !tbaa !8
  br label %if.end177

if.end177:                                        ; preds = %if.then176, %land.lhs.true170, %if.end168
  %171 = bitcast i32* %itemIndex178 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %171) #4
  store i32 0, i32* %itemIndex178, align 4, !tbaa !8
  br label %for.cond179

for.cond179:                                      ; preds = %for.inc208, %if.end177
  %172 = load i32, i32* %itemIndex178, align 4, !tbaa !8
  %173 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data180 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %173, i32 0, i32 12
  %174 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data180, align 8, !tbaa !28
  %items181 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %174, i32 0, i32 0
  %count182 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items181, i32 0, i32 2
  %175 = load i32, i32* %count182, align 4, !tbaa !31
  %cmp183 = icmp ult i32 %172, %175
  br i1 %cmp183, label %for.body186, label %for.cond.cleanup185

for.cond.cleanup185:                              ; preds = %for.cond179
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup210

for.body186:                                      ; preds = %for.cond179
  %176 = bitcast %struct.avifEncoderItem** %item187 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %176) #4
  %177 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data188 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %177, i32 0, i32 12
  %178 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data188, align 8, !tbaa !28
  %items189 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %178, i32 0, i32 0
  %item190 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items189, i32 0, i32 0
  %179 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item190, align 4, !tbaa !36
  %180 = load i32, i32* %itemIndex178, align 4, !tbaa !8
  %arrayidx191 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %179, i32 %180
  store %struct.avifEncoderItem* %arrayidx191, %struct.avifEncoderItem** %item187, align 4, !tbaa !2
  %181 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item187, align 4, !tbaa !2
  %codec192 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %181, i32 0, i32 2
  %182 = load %struct.avifCodec*, %struct.avifCodec** %codec192, align 4, !tbaa !37
  %tobool193 = icmp ne %struct.avifCodec* %182, null
  br i1 %tobool193, label %if.then194, label %if.end204

if.then194:                                       ; preds = %for.body186
  %183 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item187, align 4, !tbaa !2
  %codec195 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %183, i32 0, i32 2
  %184 = load %struct.avifCodec*, %struct.avifCodec** %codec195, align 4, !tbaa !37
  %encodeImage = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %184, i32 0, i32 5
  %185 = load i32 (%struct.avifCodec*, %struct.avifEncoder*, %struct.avifImage*, i32, i32, %struct.avifCodecEncodeOutput*)*, i32 (%struct.avifCodec*, %struct.avifEncoder*, %struct.avifImage*, i32, i32, %struct.avifCodecEncodeOutput*)** %encodeImage, align 4, !tbaa !70
  %186 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item187, align 4, !tbaa !2
  %codec196 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %186, i32 0, i32 2
  %187 = load %struct.avifCodec*, %struct.avifCodec** %codec196, align 4, !tbaa !37
  %188 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %189 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %190 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item187, align 4, !tbaa !2
  %alpha197 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %190, i32 0, i32 5
  %191 = load i32, i32* %alpha197, align 4, !tbaa !59
  %192 = load i32, i32* %addImageFlags.addr, align 4, !tbaa !8
  %193 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item187, align 4, !tbaa !2
  %encodeOutput = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %193, i32 0, i32 3
  %194 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput, align 4, !tbaa !41
  %call198 = call i32 %185(%struct.avifCodec* %187, %struct.avifEncoder* %188, %struct.avifImage* %189, i32 %191, i32 %192, %struct.avifCodecEncodeOutput* %194)
  %tobool199 = icmp ne i32 %call198, 0
  br i1 %tobool199, label %if.end203, label %if.then200

if.then200:                                       ; preds = %if.then194
  %195 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item187, align 4, !tbaa !2
  %alpha201 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %195, i32 0, i32 5
  %196 = load i32, i32* %alpha201, align 4, !tbaa !59
  %tobool202 = icmp ne i32 %196, 0
  %197 = zext i1 %tobool202 to i64
  %cond = select i1 %tobool202, i32 8, i32 7
  store i32 %cond, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup205

if.end203:                                        ; preds = %if.then194
  br label %if.end204

if.end204:                                        ; preds = %if.end203, %for.body186
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup205

cleanup205:                                       ; preds = %if.end204, %if.then200
  %198 = bitcast %struct.avifEncoderItem** %item187 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  %cleanup.dest206 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest206, label %cleanup210 [
    i32 0, label %cleanup.cont207
  ]

cleanup.cont207:                                  ; preds = %cleanup205
  br label %for.inc208

for.inc208:                                       ; preds = %cleanup.cont207
  %199 = load i32, i32* %itemIndex178, align 4, !tbaa !8
  %inc209 = add i32 %199, 1
  store i32 %inc209, i32* %itemIndex178, align 4, !tbaa !8
  br label %for.cond179

cleanup210:                                       ; preds = %cleanup205, %for.cond.cleanup185
  %200 = bitcast i32* %itemIndex178 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #4
  %cleanup.dest211 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest211, label %unreachable [
    i32 8, label %for.end212
    i32 1, label %return
  ]

for.end212:                                       ; preds = %cleanup210
  %201 = bitcast %struct.avifEncoderFrame** %frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %201) #4
  %202 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data213 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %202, i32 0, i32 12
  %203 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data213, align 8, !tbaa !28
  %frames214 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %203, i32 0, i32 1
  %204 = bitcast %struct.avifEncoderFrameArray* %frames214 to i8*
  %call215 = call i8* @avifArrayPushPtr(i8* %204)
  %205 = bitcast i8* %call215 to %struct.avifEncoderFrame*
  store %struct.avifEncoderFrame* %205, %struct.avifEncoderFrame** %frame, align 4, !tbaa !2
  %206 = load i64, i64* %durationInTimescales.addr, align 8, !tbaa !43
  %207 = load %struct.avifEncoderFrame*, %struct.avifEncoderFrame** %frame, align 4, !tbaa !2
  %durationInTimescales216 = getelementptr inbounds %struct.avifEncoderFrame, %struct.avifEncoderFrame* %207, i32 0, i32 0
  store i64 %206, i64* %durationInTimescales216, align 8, !tbaa !73
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %208 = bitcast %struct.avifEncoderFrame** %frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  br label %return

return:                                           ; preds = %for.end212, %cleanup210, %if.then166, %cleanup157, %if.then29, %if.then12, %if.then9, %if.then
  %209 = load i32, i32* %retval, align 4
  ret i32 %209

unreachable:                                      ; preds = %cleanup210, %cleanup157
  unreachable
}

declare void @avifImageCopy(%struct.avifImage*, %struct.avifImage*, i32) #2

; Function Attrs: nounwind
define internal %struct.avifEncoderItem* @avifEncoderDataCreateItem(%struct.avifEncoderData* %data, i8* %type, i8* %infeName, i32 %infeNameSize) #0 {
entry:
  %data.addr = alloca %struct.avifEncoderData*, align 4
  %type.addr = alloca i8*, align 4
  %infeName.addr = alloca i8*, align 4
  %infeNameSize.addr = alloca i32, align 4
  %item = alloca %struct.avifEncoderItem*, align 4
  store %struct.avifEncoderData* %data, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  store i8* %type, i8** %type.addr, align 4, !tbaa !2
  store i8* %infeName, i8** %infeName.addr, align 4, !tbaa !2
  store i32 %infeNameSize, i32* %infeNameSize.addr, align 4, !tbaa !6
  %0 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %items = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %1, i32 0, i32 0
  %2 = bitcast %struct.avifEncoderItemArray* %items to i8*
  %call = call i8* @avifArrayPushPtr(i8* %2)
  %3 = bitcast i8* %call to %struct.avifEncoderItem*
  store %struct.avifEncoderItem* %3, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %4 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %lastItemID = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %4, i32 0, i32 5
  %5 = load i16, i16* %lastItemID, align 4, !tbaa !75
  %inc = add i16 %5, 1
  store i16 %inc, i16* %lastItemID, align 4, !tbaa !75
  %6 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data.addr, align 4, !tbaa !2
  %lastItemID1 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %6, i32 0, i32 5
  %7 = load i16, i16* %lastItemID1, align 4, !tbaa !75
  %8 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %id = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %8, i32 0, i32 0
  store i16 %7, i16* %id, align 4, !tbaa !55
  %9 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %type2 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %9, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type2, i32 0, i32 0
  %10 = load i8*, i8** %type.addr, align 4, !tbaa !2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %arraydecay, i8* align 1 %10, i32 4, i1 false)
  %11 = load i8*, i8** %infeName.addr, align 4, !tbaa !2
  %12 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %infeName3 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %12, i32 0, i32 6
  store i8* %11, i8** %infeName3, align 4, !tbaa !76
  %13 = load i32, i32* %infeNameSize.addr, align 4, !tbaa !6
  %14 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %infeNameSize4 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %14, i32 0, i32 7
  store i32 %13, i32* %infeNameSize4, align 4, !tbaa !77
  %call5 = call %struct.avifCodecEncodeOutput* @avifCodecEncodeOutputCreate()
  %15 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %encodeOutput = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %15, i32 0, i32 3
  store %struct.avifCodecEncodeOutput* %call5, %struct.avifCodecEncodeOutput** %encodeOutput, align 4, !tbaa !41
  %16 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %mdatFixups = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %16, i32 0, i32 10
  %17 = bitcast %struct.avifOffsetFixupArray* %mdatFixups to i8*
  call void @avifArrayCreate(i8* %17, i32 4, i32 4)
  %18 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %19 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  ret %struct.avifEncoderItem* %18
}

declare %struct.avifCodec* @avifCodecCreate(i32, i32) #2

; Function Attrs: nounwind
define internal i32 @avifImageIsOpaque(%struct.avifImage* %image) #0 {
entry:
  %retval = alloca i32, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %maxChannel = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %p = alloca i16*, align 4
  %j20 = alloca i32, align 4
  %i27 = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 10
  %1 = load i8*, i8** %alphaPlane, align 4, !tbaa !57
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32* %maxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 2
  %4 = load i32, i32* %depth, align 4, !tbaa !44
  %shl = shl i32 1, %4
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %maxChannel, align 4, !tbaa !8
  %5 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %call = call i32 @avifImageUsesU16(%struct.avifImage* %5)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %6 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  store i32 0, i32* %j, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %if.then2
  %7 = load i32, i32* %j, align 4, !tbaa !8
  %8 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %8, i32 0, i32 1
  %9 = load i32, i32* %height, align 4, !tbaa !51
  %cmp = icmp ult i32 %7, %9
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

for.body:                                         ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !8
  %12 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 0
  %13 = load i32, i32* %width, align 4, !tbaa !50
  %cmp4 = icmp ult i32 %11, %13
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup13

for.body6:                                        ; preds = %for.cond3
  %14 = bitcast i16** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane7 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %15, i32 0, i32 10
  %16 = load i8*, i8** %alphaPlane7, align 4, !tbaa !57
  %17 = load i32, i32* %i, align 4, !tbaa !8
  %mul = mul i32 %17, 2
  %18 = load i32, i32* %j, align 4, !tbaa !8
  %19 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %19, i32 0, i32 11
  %20 = load i32, i32* %alphaRowBytes, align 4, !tbaa !78
  %mul8 = mul i32 %18, %20
  %add = add i32 %mul, %mul8
  %arrayidx = getelementptr inbounds i8, i8* %16, i32 %add
  %21 = bitcast i8* %arrayidx to i16*
  store i16* %21, i16** %p, align 4, !tbaa !2
  %22 = load i16*, i16** %p, align 4, !tbaa !2
  %23 = load i16, i16* %22, align 2, !tbaa !79
  %conv = zext i16 %23 to i32
  %24 = load i32, i32* %maxChannel, align 4, !tbaa !8
  %cmp9 = icmp ne i32 %conv, %24
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %for.body6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %for.body6
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end12, %if.then11
  %25 = bitcast i16** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup13 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %26 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add i32 %26, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond3

cleanup13:                                        ; preds = %cleanup, %for.cond.cleanup5
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  %cleanup.dest14 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest14, label %cleanup17 [
    i32 5, label %for.end
  ]

for.end:                                          ; preds = %cleanup13
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %28 = load i32, i32* %j, align 4, !tbaa !8
  %inc16 = add i32 %28, 1
  store i32 %inc16, i32* %j, align 4, !tbaa !8
  br label %for.cond

cleanup17:                                        ; preds = %cleanup13, %for.cond.cleanup
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  %cleanup.dest18 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest18, label %cleanup55 [
    i32 2, label %for.end19
  ]

for.end19:                                        ; preds = %cleanup17
  br label %if.end54

if.else:                                          ; preds = %if.end
  %30 = bitcast i32* %j20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store i32 0, i32* %j20, align 4, !tbaa !8
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc49, %if.else
  %31 = load i32, i32* %j20, align 4, !tbaa !8
  %32 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height22 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 1
  %33 = load i32, i32* %height22, align 4, !tbaa !51
  %cmp23 = icmp ult i32 %31, %33
  br i1 %cmp23, label %for.body26, label %for.cond.cleanup25

for.cond.cleanup25:                               ; preds = %for.cond21
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup51

for.body26:                                       ; preds = %for.cond21
  %34 = bitcast i32* %i27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  store i32 0, i32* %i27, align 4, !tbaa !8
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc44, %for.body26
  %35 = load i32, i32* %i27, align 4, !tbaa !8
  %36 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width29 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %36, i32 0, i32 0
  %37 = load i32, i32* %width29, align 4, !tbaa !50
  %cmp30 = icmp ult i32 %35, %37
  br i1 %cmp30, label %for.body33, label %for.cond.cleanup32

for.cond.cleanup32:                               ; preds = %for.cond28
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

for.body33:                                       ; preds = %for.cond28
  %38 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane34 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %38, i32 0, i32 10
  %39 = load i8*, i8** %alphaPlane34, align 4, !tbaa !57
  %40 = load i32, i32* %i27, align 4, !tbaa !8
  %41 = load i32, i32* %j20, align 4, !tbaa !8
  %42 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes35 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %42, i32 0, i32 11
  %43 = load i32, i32* %alphaRowBytes35, align 4, !tbaa !78
  %mul36 = mul i32 %41, %43
  %add37 = add i32 %40, %mul36
  %arrayidx38 = getelementptr inbounds i8, i8* %39, i32 %add37
  %44 = load i8, i8* %arrayidx38, align 1, !tbaa !80
  %conv39 = zext i8 %44 to i32
  %45 = load i32, i32* %maxChannel, align 4, !tbaa !8
  %cmp40 = icmp ne i32 %conv39, %45
  br i1 %cmp40, label %if.then42, label %if.end43

if.then42:                                        ; preds = %for.body33
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

if.end43:                                         ; preds = %for.body33
  br label %for.inc44

for.inc44:                                        ; preds = %if.end43
  %46 = load i32, i32* %i27, align 4, !tbaa !8
  %inc45 = add i32 %46, 1
  store i32 %inc45, i32* %i27, align 4, !tbaa !8
  br label %for.cond28

cleanup46:                                        ; preds = %if.then42, %for.cond.cleanup32
  %47 = bitcast i32* %i27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  %cleanup.dest47 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest47, label %cleanup51 [
    i32 11, label %for.end48
  ]

for.end48:                                        ; preds = %cleanup46
  br label %for.inc49

for.inc49:                                        ; preds = %for.end48
  %48 = load i32, i32* %j20, align 4, !tbaa !8
  %inc50 = add i32 %48, 1
  store i32 %inc50, i32* %j20, align 4, !tbaa !8
  br label %for.cond21

cleanup51:                                        ; preds = %cleanup46, %for.cond.cleanup25
  %49 = bitcast i32* %j20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %cleanup.dest52 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest52, label %cleanup55 [
    i32 8, label %for.end53
  ]

for.end53:                                        ; preds = %cleanup51
  br label %if.end54

if.end54:                                         ; preds = %for.end53, %for.end19
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup55

cleanup55:                                        ; preds = %if.end54, %cleanup51, %cleanup17
  %50 = bitcast i32* %maxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  br label %return

return:                                           ; preds = %cleanup55, %if.then
  %51 = load i32, i32* %retval, align 4
  ret i32 %51
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare i32 @memcmp(i8*, i8*, i32) #2

declare void @avifRWDataRealloc(%struct.avifRWData*, i32) #2

declare i32 @avifHTONL(i32) #2

; Function Attrs: nounwind
define internal void @fillConfigBox(%struct.avifCodec* %codec, %struct.avifImage* %image, i32 %alpha) #0 {
entry:
  %codec.addr = alloca %struct.avifCodec*, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %alpha.addr = alloca i32, align 4
  %formatInfo = alloca %struct.avifPixelFormatInfo, align 4
  %seqProfile = alloca i8, align 1
  %seqLevelIdx0 = alloca i8, align 1
  store %struct.avifCodec* %codec, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store i32 %alpha, i32* %alpha.addr, align 4, !tbaa !8
  %0 = bitcast %struct.avifPixelFormatInfo* %formatInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #4
  %1 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %1, i32 0, i32 3
  %2 = load i32, i32* %yuvFormat, align 4, !tbaa !52
  call void @avifGetPixelFormatInfo(i32 %2, %struct.avifPixelFormatInfo* %formatInfo)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %seqProfile) #4
  store i8 0, i8* %seqProfile, align 1, !tbaa !80
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %3, i32 0, i32 2
  %4 = load i32, i32* %depth, align 4, !tbaa !44
  %cmp = icmp eq i32 %4, 12
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i8 2, i8* %seqProfile, align 1, !tbaa !80
  br label %if.end8

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %alpha.addr, align 4, !tbaa !8
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then1, label %if.else2

if.then1:                                         ; preds = %if.else
  store i8 0, i8* %seqProfile, align 1, !tbaa !80
  br label %if.end

if.else2:                                         ; preds = %if.else
  %6 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat3 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %6, i32 0, i32 3
  %7 = load i32, i32* %yuvFormat3, align 4, !tbaa !52
  switch i32 %7, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb4
    i32 3, label %sw.bb5
    i32 4, label %sw.bb6
    i32 0, label %sw.bb7
  ]

sw.bb:                                            ; preds = %if.else2
  store i8 1, i8* %seqProfile, align 1, !tbaa !80
  br label %sw.epilog

sw.bb4:                                           ; preds = %if.else2
  store i8 2, i8* %seqProfile, align 1, !tbaa !80
  br label %sw.epilog

sw.bb5:                                           ; preds = %if.else2
  store i8 0, i8* %seqProfile, align 1, !tbaa !80
  br label %sw.epilog

sw.bb6:                                           ; preds = %if.else2
  store i8 0, i8* %seqProfile, align 1, !tbaa !80
  br label %sw.epilog

sw.bb7:                                           ; preds = %if.else2
  br label %sw.default

sw.default:                                       ; preds = %if.else2, %sw.bb7
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb
  br label %if.end

if.end:                                           ; preds = %sw.epilog, %if.then1
  br label %if.end8

if.end8:                                          ; preds = %if.end, %if.then
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %seqLevelIdx0) #4
  store i8 31, i8* %seqLevelIdx0, align 1, !tbaa !80
  %8 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %8, i32 0, i32 0
  %9 = load i32, i32* %width, align 4, !tbaa !50
  %cmp9 = icmp ule i32 %9, 8192
  br i1 %cmp9, label %land.lhs.true, label %if.end16

land.lhs.true:                                    ; preds = %if.end8
  %10 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %10, i32 0, i32 1
  %11 = load i32, i32* %height, align 4, !tbaa !51
  %cmp10 = icmp ule i32 %11, 4352
  br i1 %cmp10, label %land.lhs.true11, label %if.end16

land.lhs.true11:                                  ; preds = %land.lhs.true
  %12 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width12 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 0
  %13 = load i32, i32* %width12, align 4, !tbaa !50
  %14 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height13 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %14, i32 0, i32 1
  %15 = load i32, i32* %height13, align 4, !tbaa !51
  %mul = mul i32 %13, %15
  %cmp14 = icmp ule i32 %mul, 8912896
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %land.lhs.true11
  store i8 13, i8* %seqLevelIdx0, align 1, !tbaa !80
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %land.lhs.true11, %land.lhs.true, %if.end8
  %16 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %16, i32 0, i32 1
  %17 = bitcast %struct.avifCodecConfigurationBox* %configBox to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %17, i8 0, i32 9, i1 false)
  %18 = load i8, i8* %seqProfile, align 1, !tbaa !80
  %19 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox17 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %19, i32 0, i32 1
  %seqProfile18 = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox17, i32 0, i32 0
  store i8 %18, i8* %seqProfile18, align 4, !tbaa !81
  %20 = load i8, i8* %seqLevelIdx0, align 1, !tbaa !80
  %21 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox19 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %21, i32 0, i32 1
  %seqLevelIdx020 = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox19, i32 0, i32 1
  store i8 %20, i8* %seqLevelIdx020, align 1, !tbaa !82
  %22 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox21 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %22, i32 0, i32 1
  %seqTier0 = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox21, i32 0, i32 2
  store i8 0, i8* %seqTier0, align 2, !tbaa !83
  %23 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth22 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 2
  %24 = load i32, i32* %depth22, align 4, !tbaa !44
  %cmp23 = icmp ugt i32 %24, 8
  %conv = zext i1 %cmp23 to i32
  %conv24 = trunc i32 %conv to i8
  %25 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox25 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %25, i32 0, i32 1
  %highBitdepth = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox25, i32 0, i32 3
  store i8 %conv24, i8* %highBitdepth, align 1, !tbaa !84
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth26 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 2
  %27 = load i32, i32* %depth26, align 4, !tbaa !44
  %cmp27 = icmp eq i32 %27, 12
  %conv28 = zext i1 %cmp27 to i32
  %conv29 = trunc i32 %conv28 to i8
  %28 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox30 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %28, i32 0, i32 1
  %twelveBit = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox30, i32 0, i32 4
  store i8 %conv29, i8* %twelveBit, align 4, !tbaa !85
  %29 = load i32, i32* %alpha.addr, align 4, !tbaa !8
  %tobool31 = icmp ne i32 %29, 0
  br i1 %tobool31, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end16
  %30 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat32 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %30, i32 0, i32 3
  %31 = load i32, i32* %yuvFormat32, align 4, !tbaa !52
  %cmp33 = icmp eq i32 %31, 4
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.end16
  %32 = phi i1 [ true, %if.end16 ], [ %cmp33, %lor.rhs ]
  %lor.ext = zext i1 %32 to i32
  %conv35 = trunc i32 %lor.ext to i8
  %33 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox36 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %33, i32 0, i32 1
  %monochrome = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox36, i32 0, i32 5
  store i8 %conv35, i8* %monochrome, align 1, !tbaa !86
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 1
  %34 = load i32, i32* %chromaShiftX, align 4, !tbaa !87
  %conv37 = trunc i32 %34 to i8
  %35 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox38 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %35, i32 0, i32 1
  %chromaSubsamplingX = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox38, i32 0, i32 6
  store i8 %conv37, i8* %chromaSubsamplingX, align 2, !tbaa !89
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %36 = load i32, i32* %chromaShiftY, align 4, !tbaa !90
  %conv39 = trunc i32 %36 to i8
  %37 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox40 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %37, i32 0, i32 1
  %chromaSubsamplingY = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox40, i32 0, i32 7
  store i8 %conv39, i8* %chromaSubsamplingY, align 1, !tbaa !91
  %38 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvChromaSamplePosition = getelementptr inbounds %struct.avifImage, %struct.avifImage* %38, i32 0, i32 5
  %39 = load i32, i32* %yuvChromaSamplePosition, align 4, !tbaa !92
  %conv41 = trunc i32 %39 to i8
  %40 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox42 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %40, i32 0, i32 1
  %chromaSamplePosition = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox42, i32 0, i32 8
  store i8 %conv41, i8* %chromaSamplePosition, align 4, !tbaa !93
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %seqLevelIdx0) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %seqProfile) #4
  %41 = bitcast %struct.avifPixelFormatInfo* %formatInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %41) #4
  ret void
}

; Function Attrs: nounwind
define hidden i32 @avifEncoderFinish(%struct.avifEncoder* %encoder, %struct.avifRWData* %output) #0 {
entry:
  %retval = alloca i32, align 4
  %encoder.addr = alloca %struct.avifEncoder*, align 4
  %output.addr = alloca %struct.avifRWData*, align 4
  %itemIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %item = alloca %struct.avifEncoderItem*, align 4
  %obuSize = alloca i32, align 4
  %sampleIndex = alloca i32, align 4
  %imageMetadata = alloca %struct.avifImage*, align 4
  %now = alloca i64, align 8
  %s = alloca %struct.avifRWStream, align 4
  %majorBrand = alloca i8*, align 4
  %ftyp = alloca i32, align 4
  %meta = alloca i32, align 4
  %hdlr = alloca i32, align 4
  %iloc = alloca i32, align 4
  %offsetSizeAndLengthSize = alloca i8, align 1
  %itemIndex99 = alloca i32, align 4
  %item108 = alloca %struct.avifEncoderItem*, align 4
  %contentSize = alloca i32, align 4
  %iinf = alloca i32, align 4
  %itemIndex136 = alloca i32, align 4
  %item145 = alloca %struct.avifEncoderItem*, align 4
  %infe = alloca i32, align 4
  %iref = alloca i32, align 4
  %itemIndex162 = alloca i32, align 4
  %item171 = alloca %struct.avifEncoderItem*, align 4
  %refType = alloca i32, align 4
  %iprp = alloca i32, align 4
  %itemPropertyIndex = alloca i8, align 1
  %ipco = alloca i32, align 4
  %itemIndex197 = alloca i32, align 4
  %item206 = alloca %struct.avifEncoderItem*, align 4
  %ispe = alloca i32, align 4
  %channelCount = alloca i8, align 1
  %pixi = alloca i32, align 4
  %chan = alloca i8, align 1
  %auxC = alloca i32, align 4
  %ipma260 = alloca i32, align 4
  %ipmaCount = alloca i32, align 4
  %itemIndex262 = alloca i32, align 4
  %item271 = alloca %struct.avifEncoderItem*, align 4
  %itemIndex288 = alloca i32, align 4
  %item297 = alloca %struct.avifEncoderItem*, align 4
  %i = alloca i32, align 4
  %essentialAndIndex = alloca i8, align 1
  %durationInTimescales = alloca i64, align 8
  %frameIndex = alloca i32, align 4
  %frame = alloca %struct.avifEncoderFrame*, align 4
  %moov = alloca i32, align 4
  %mvhd = alloca i32, align 4
  %itemIndex370 = alloca i32, align 4
  %item379 = alloca %struct.avifEncoderItem*, align 4
  %syncSamplesCount = alloca i32, align 4
  %sampleIndex391 = alloca i32, align 4
  %sample400 = alloca %struct.avifEncodeSample*, align 4
  %trak = alloca i32, align 4
  %tkhd = alloca i32, align 4
  %tref = alloca i32, align 4
  %refType425 = alloca i32, align 4
  %mdia = alloca i32, align 4
  %mdhd = alloca i32, align 4
  %hdlrTrak = alloca i32, align 4
  %minf = alloca i32, align 4
  %vmhd = alloca i32, align 4
  %dinf = alloca i32, align 4
  %dref = alloca i32, align 4
  %stbl = alloca i32, align 4
  %stco = alloca i32, align 4
  %stsc = alloca i32, align 4
  %stsz = alloca i32, align 4
  %sampleIndex455 = alloca i32, align 4
  %sample464 = alloca %struct.avifEncodeSample*, align 4
  %stss = alloca i32, align 4
  %sampleIndex476 = alloca i32, align 4
  %sample485 = alloca %struct.avifEncodeSample*, align 4
  %stts = alloca i32, align 4
  %sttsEntryCountOffset = alloca i32, align 4
  %sttsEntryCount = alloca i32, align 4
  %sampleCount = alloca i32, align 4
  %frameIndex501 = alloca i32, align 4
  %frame510 = alloca %struct.avifEncoderFrame*, align 4
  %nextFrame = alloca %struct.avifEncoderFrame*, align 4
  %prevOffset = alloca i32, align 4
  %stsd = alloca i32, align 4
  %av01 = alloca i32, align 4
  %mdat = alloca i32, align 4
  %itemIndex570 = alloca i32, align 4
  %item579 = alloca %struct.avifEncoderItem*, align 4
  %chunkOffset = alloca i32, align 4
  %sampleIndex603 = alloca i32, align 4
  %sample612 = alloca %struct.avifEncodeSample*, align 4
  %fixupIndex = alloca i32, align 4
  %fixup = alloca %struct.avifOffsetFixup*, align 4
  %prevOffset640 = alloca i32, align 4
  store %struct.avifEncoder* %encoder, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  store %struct.avifRWData* %output, %struct.avifRWData** %output.addr, align 4, !tbaa !2
  %0 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %0, i32 0, i32 12
  %1 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 8, !tbaa !28
  %items = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %1, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items, i32 0, i32 2
  %2 = load i32, i32* %count, align 4, !tbaa !31
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 0, i32* %itemIndex, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc42, %if.end
  %4 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %5 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %5, i32 0, i32 12
  %6 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data1, align 8, !tbaa !28
  %items2 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %6, i32 0, i32 0
  %count3 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items2, i32 0, i32 2
  %7 = load i32, i32* %count3, align 4, !tbaa !31
  %cmp4 = icmp ult i32 %4, %7
  br i1 %cmp4, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup44

for.body:                                         ; preds = %for.cond
  %8 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data5 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %9, i32 0, i32 12
  %10 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data5, align 8, !tbaa !28
  %items6 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %10, i32 0, i32 0
  %item7 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items6, i32 0, i32 0
  %11 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item7, align 4, !tbaa !36
  %12 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %11, i32 %12
  store %struct.avifEncoderItem* %arrayidx, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %13 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %codec = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %13, i32 0, i32 2
  %14 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !37
  %tobool = icmp ne %struct.avifCodec* %14, null
  br i1 %tobool, label %if.then8, label %if.end41

if.then8:                                         ; preds = %for.body
  %15 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %codec9 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %15, i32 0, i32 2
  %16 = load %struct.avifCodec*, %struct.avifCodec** %codec9, align 4, !tbaa !37
  %encodeFinish = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %16, i32 0, i32 6
  %17 = load i32 (%struct.avifCodec*, %struct.avifCodecEncodeOutput*)*, i32 (%struct.avifCodec*, %struct.avifCodecEncodeOutput*)** %encodeFinish, align 4, !tbaa !94
  %18 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %codec10 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %18, i32 0, i32 2
  %19 = load %struct.avifCodec*, %struct.avifCodec** %codec10, align 4, !tbaa !37
  %20 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %encodeOutput = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %20, i32 0, i32 3
  %21 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput, align 4, !tbaa !41
  %call = call i32 %17(%struct.avifCodec* %19, %struct.avifCodecEncodeOutput* %21)
  %tobool11 = icmp ne i32 %call, 0
  br i1 %tobool11, label %if.end14, label %if.then12

if.then12:                                        ; preds = %if.then8
  %22 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %alpha = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %22, i32 0, i32 5
  %23 = load i32, i32* %alpha, align 4, !tbaa !59
  %tobool13 = icmp ne i32 %23, 0
  %24 = zext i1 %tobool13 to i64
  %cond = select i1 %tobool13, i32 8, i32 7
  store i32 %cond, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %if.then8
  %25 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %encodeOutput15 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %25, i32 0, i32 3
  %26 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput15, align 4, !tbaa !41
  %samples = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %26, i32 0, i32 0
  %count16 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples, i32 0, i32 2
  %27 = load i32, i32* %count16, align 4, !tbaa !13
  %28 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data17 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %28, i32 0, i32 12
  %29 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data17, align 8, !tbaa !28
  %frames = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %29, i32 0, i32 1
  %count18 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames, i32 0, i32 2
  %30 = load i32, i32* %count18, align 4, !tbaa !69
  %cmp19 = icmp ne i32 %27, %30
  br i1 %cmp19, label %if.then20, label %if.end24

if.then20:                                        ; preds = %if.end14
  %31 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %alpha21 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %31, i32 0, i32 5
  %32 = load i32, i32* %alpha21, align 4, !tbaa !59
  %tobool22 = icmp ne i32 %32, 0
  %33 = zext i1 %tobool22 to i64
  %cond23 = select i1 %tobool22, i32 8, i32 7
  store i32 %cond23, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end24:                                         ; preds = %if.end14
  %34 = bitcast i32* %obuSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  store i32 0, i32* %obuSize, align 4, !tbaa !6
  %35 = bitcast i32* %sampleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  store i32 0, i32* %sampleIndex, align 4, !tbaa !8
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc, %if.end24
  %36 = load i32, i32* %sampleIndex, align 4, !tbaa !8
  %37 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %encodeOutput26 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %37, i32 0, i32 3
  %38 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput26, align 4, !tbaa !41
  %samples27 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %38, i32 0, i32 0
  %count28 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples27, i32 0, i32 2
  %39 = load i32, i32* %count28, align 4, !tbaa !13
  %cmp29 = icmp ult i32 %36, %39
  br i1 %cmp29, label %for.body31, label %for.cond.cleanup30

for.cond.cleanup30:                               ; preds = %for.cond25
  store i32 5, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %sampleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  br label %for.end

for.body31:                                       ; preds = %for.cond25
  %41 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %encodeOutput32 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %41, i32 0, i32 3
  %42 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput32, align 4, !tbaa !41
  %samples33 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %42, i32 0, i32 0
  %sample = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples33, i32 0, i32 0
  %43 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample, align 4, !tbaa !16
  %44 = load i32, i32* %sampleIndex, align 4, !tbaa !8
  %arrayidx34 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %43, i32 %44
  %data35 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %arrayidx34, i32 0, i32 0
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %data35, i32 0, i32 1
  %45 = load i32, i32* %size, align 4, !tbaa !95
  %46 = load i32, i32* %obuSize, align 4, !tbaa !6
  %add = add i32 %46, %45
  store i32 %add, i32* %obuSize, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body31
  %47 = load i32, i32* %sampleIndex, align 4, !tbaa !8
  %inc = add i32 %47, 1
  store i32 %inc, i32* %sampleIndex, align 4, !tbaa !8
  br label %for.cond25

for.end:                                          ; preds = %for.cond.cleanup30
  %48 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %alpha36 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %48, i32 0, i32 5
  %49 = load i32, i32* %alpha36, align 4, !tbaa !59
  %tobool37 = icmp ne i32 %49, 0
  br i1 %tobool37, label %if.then38, label %if.else

if.then38:                                        ; preds = %for.end
  %50 = load i32, i32* %obuSize, align 4, !tbaa !6
  %51 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %ioStats = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %51, i32 0, i32 11
  %alphaOBUSize = getelementptr inbounds %struct.avifIOStats, %struct.avifIOStats* %ioStats, i32 0, i32 1
  store i32 %50, i32* %alphaOBUSize, align 4, !tbaa !96
  br label %if.end40

if.else:                                          ; preds = %for.end
  %52 = load i32, i32* %obuSize, align 4, !tbaa !6
  %53 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %ioStats39 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %53, i32 0, i32 11
  %colorOBUSize = getelementptr inbounds %struct.avifIOStats, %struct.avifIOStats* %ioStats39, i32 0, i32 0
  store i32 %52, i32* %colorOBUSize, align 8, !tbaa !97
  br label %if.end40

if.end40:                                         ; preds = %if.else, %if.then38
  %54 = bitcast i32* %obuSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end41, %if.then20, %if.then12
  %55 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup44 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc42

for.inc42:                                        ; preds = %cleanup.cont
  %56 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %inc43 = add i32 %56, 1
  store i32 %inc43, i32* %itemIndex, align 4, !tbaa !8
  br label %for.cond

cleanup44:                                        ; preds = %cleanup, %for.cond.cleanup
  %57 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %cleanup.dest45 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest45, label %unreachable [
    i32 2, label %for.end46
    i32 1, label %return
  ]

for.end46:                                        ; preds = %cleanup44
  %58 = bitcast %struct.avifImage** %imageMetadata to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #4
  %59 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data47 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %59, i32 0, i32 12
  %60 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data47, align 8, !tbaa !28
  %imageMetadata48 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %60, i32 0, i32 2
  %61 = load %struct.avifImage*, %struct.avifImage** %imageMetadata48, align 4, !tbaa !42
  store %struct.avifImage* %61, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %62 = bitcast i64* %now to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %62) #4
  %call49 = call i32 @time(i32* null)
  %conv = sext i32 %call49 to i64
  %add50 = add i64 %conv, 2082844800
  store i64 %add50, i64* %now, align 8, !tbaa !43
  %63 = bitcast %struct.avifRWStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %63) #4
  %64 = load %struct.avifRWData*, %struct.avifRWData** %output.addr, align 4, !tbaa !2
  call void @avifRWStreamStart(%struct.avifRWStream* %s, %struct.avifRWData* %64)
  %65 = bitcast i8** %majorBrand to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #4
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i32 0, i32 0), i8** %majorBrand, align 4, !tbaa !2
  %66 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data51 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %66, i32 0, i32 12
  %67 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data51, align 8, !tbaa !28
  %frames52 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %67, i32 0, i32 1
  %count53 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames52, i32 0, i32 2
  %68 = load i32, i32* %count53, align 4, !tbaa !69
  %cmp54 = icmp ugt i32 %68, 1
  br i1 %cmp54, label %if.then56, label %if.end57

if.then56:                                        ; preds = %for.end46
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0), i8** %majorBrand, align 4, !tbaa !2
  br label %if.end57

if.end57:                                         ; preds = %if.then56, %for.end46
  %69 = bitcast i32* %ftyp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #4
  %call58 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i32 0, i32 0), i32 0)
  store i32 %call58, i32* %ftyp, align 4, !tbaa !6
  %70 = load i8*, i8** %majorBrand, align 4, !tbaa !2
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* %70, i32 4)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 0)
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i32 0, i32 0), i32 4)
  %71 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data59 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %71, i32 0, i32 12
  %72 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data59, align 8, !tbaa !28
  %frames60 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %72, i32 0, i32 1
  %count61 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames60, i32 0, i32 2
  %73 = load i32, i32* %count61, align 4, !tbaa !69
  %cmp62 = icmp ugt i32 %73, 1
  br i1 %cmp62, label %if.then64, label %if.end65

if.then64:                                        ; preds = %if.end57
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0), i32 4)
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.11, i32 0, i32 0), i32 4)
  br label %if.end65

if.end65:                                         ; preds = %if.then64, %if.end57
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.12, i32 0, i32 0), i32 4)
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.13, i32 0, i32 0), i32 4)
  %74 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %74, i32 0, i32 2
  %75 = load i32, i32* %depth, align 4, !tbaa !44
  %cmp66 = icmp eq i32 %75, 8
  br i1 %cmp66, label %if.then71, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end65
  %76 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %depth68 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %76, i32 0, i32 2
  %77 = load i32, i32* %depth68, align 4, !tbaa !44
  %cmp69 = icmp eq i32 %77, 10
  br i1 %cmp69, label %if.then71, label %if.end82

if.then71:                                        ; preds = %lor.lhs.false, %if.end65
  %78 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %78, i32 0, i32 3
  %79 = load i32, i32* %yuvFormat, align 4, !tbaa !52
  %cmp72 = icmp eq i32 %79, 3
  br i1 %cmp72, label %if.then74, label %if.else75

if.then74:                                        ; preds = %if.then71
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.14, i32 0, i32 0), i32 4)
  br label %if.end81

if.else75:                                        ; preds = %if.then71
  %80 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %yuvFormat76 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %80, i32 0, i32 3
  %81 = load i32, i32* %yuvFormat76, align 4, !tbaa !52
  %cmp77 = icmp eq i32 %81, 1
  br i1 %cmp77, label %if.then79, label %if.end80

if.then79:                                        ; preds = %if.else75
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.15, i32 0, i32 0), i32 4)
  br label %if.end80

if.end80:                                         ; preds = %if.then79, %if.else75
  br label %if.end81

if.end81:                                         ; preds = %if.end80, %if.then74
  br label %if.end82

if.end82:                                         ; preds = %if.end81, %lor.lhs.false
  %82 = load i32, i32* %ftyp, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %82)
  %83 = bitcast i32* %meta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #4
  %call83 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.16, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call83, i32* %meta, align 4, !tbaa !6
  %84 = bitcast i32* %hdlr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #4
  %call84 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.17, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call84, i32* %hdlr, align 4, !tbaa !6
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 0)
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.18, i32 0, i32 0), i32 4)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 12)
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.19, i32 0, i32 0), i32 8)
  %85 = load i32, i32* %hdlr, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %85)
  %86 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data85 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %86, i32 0, i32 12
  %87 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data85, align 8, !tbaa !28
  %primaryItemID = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %87, i32 0, i32 6
  %88 = load i16, i16* %primaryItemID, align 2, !tbaa !56
  %conv86 = zext i16 %88 to i32
  %cmp87 = icmp ne i32 %conv86, 0
  br i1 %cmp87, label %if.then89, label %if.end93

if.then89:                                        ; preds = %if.end82
  %call90 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.20, i32 0, i32 0), i32 2, i32 0, i32 0)
  %89 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data91 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %89, i32 0, i32 12
  %90 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data91, align 8, !tbaa !28
  %primaryItemID92 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %90, i32 0, i32 6
  %91 = load i16, i16* %primaryItemID92, align 2, !tbaa !56
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %91)
  br label %if.end93

if.end93:                                         ; preds = %if.then89, %if.end82
  %92 = bitcast i32* %iloc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #4
  %call94 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.21, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call94, i32* %iloc, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %offsetSizeAndLengthSize) #4
  store i8 68, i8* %offsetSizeAndLengthSize, align 1, !tbaa !80
  call void @avifRWStreamWrite(%struct.avifRWStream* %s, i8* %offsetSizeAndLengthSize, i32 1)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 1)
  %93 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data95 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %93, i32 0, i32 12
  %94 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data95, align 8, !tbaa !28
  %items96 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %94, i32 0, i32 0
  %count97 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items96, i32 0, i32 2
  %95 = load i32, i32* %count97, align 4, !tbaa !31
  %conv98 = trunc i32 %95 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %conv98)
  %96 = bitcast i32* %itemIndex99 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #4
  store i32 0, i32* %itemIndex99, align 4, !tbaa !8
  br label %for.cond100

for.cond100:                                      ; preds = %for.inc127, %if.end93
  %97 = load i32, i32* %itemIndex99, align 4, !tbaa !8
  %98 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data101 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %98, i32 0, i32 12
  %99 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data101, align 8, !tbaa !28
  %items102 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %99, i32 0, i32 0
  %count103 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items102, i32 0, i32 2
  %100 = load i32, i32* %count103, align 4, !tbaa !31
  %cmp104 = icmp ult i32 %97, %100
  br i1 %cmp104, label %for.body107, label %for.cond.cleanup106

for.cond.cleanup106:                              ; preds = %for.cond100
  store i32 8, i32* %cleanup.dest.slot, align 4
  %101 = bitcast i32* %itemIndex99 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #4
  br label %for.end130

for.body107:                                      ; preds = %for.cond100
  %102 = bitcast %struct.avifEncoderItem** %item108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #4
  %103 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data109 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %103, i32 0, i32 12
  %104 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data109, align 8, !tbaa !28
  %items110 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %104, i32 0, i32 0
  %item111 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items110, i32 0, i32 0
  %105 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item111, align 4, !tbaa !36
  %106 = load i32, i32* %itemIndex99, align 4, !tbaa !8
  %arrayidx112 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %105, i32 %106
  store %struct.avifEncoderItem* %arrayidx112, %struct.avifEncoderItem** %item108, align 4, !tbaa !2
  %107 = bitcast i32* %contentSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #4
  %108 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item108, align 4, !tbaa !2
  %metadataPayload = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %108, i32 0, i32 4
  %size113 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %metadataPayload, i32 0, i32 1
  %109 = load i32, i32* %size113, align 4, !tbaa !98
  store i32 %109, i32* %contentSize, align 4, !tbaa !8
  %110 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item108, align 4, !tbaa !2
  %encodeOutput114 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %110, i32 0, i32 3
  %111 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput114, align 4, !tbaa !41
  %samples115 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %111, i32 0, i32 0
  %count116 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples115, i32 0, i32 2
  %112 = load i32, i32* %count116, align 4, !tbaa !13
  %cmp117 = icmp ugt i32 %112, 0
  br i1 %cmp117, label %if.then119, label %if.end126

if.then119:                                       ; preds = %for.body107
  %113 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item108, align 4, !tbaa !2
  %encodeOutput120 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %113, i32 0, i32 3
  %114 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput120, align 4, !tbaa !41
  %samples121 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %114, i32 0, i32 0
  %sample122 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples121, i32 0, i32 0
  %115 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample122, align 4, !tbaa !16
  %arrayidx123 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %115, i32 0
  %data124 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %arrayidx123, i32 0, i32 0
  %size125 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %data124, i32 0, i32 1
  %116 = load i32, i32* %size125, align 4, !tbaa !95
  store i32 %116, i32* %contentSize, align 4, !tbaa !8
  br label %if.end126

if.end126:                                        ; preds = %if.then119, %for.body107
  %117 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item108, align 4, !tbaa !2
  %id = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %117, i32 0, i32 0
  %118 = load i16, i16* %id, align 4, !tbaa !55
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %118)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 1)
  %119 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item108, align 4, !tbaa !2
  call void @avifEncoderItemAddMdatFixup(%struct.avifEncoderItem* %119, %struct.avifRWStream* %s)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 0)
  %120 = load i32, i32* %contentSize, align 4, !tbaa !8
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %120)
  %121 = bitcast i32* %contentSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #4
  %122 = bitcast %struct.avifEncoderItem** %item108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #4
  br label %for.inc127

for.inc127:                                       ; preds = %if.end126
  %123 = load i32, i32* %itemIndex99, align 4, !tbaa !8
  %inc128 = add i32 %123, 1
  store i32 %inc128, i32* %itemIndex99, align 4, !tbaa !8
  br label %for.cond100

for.end130:                                       ; preds = %for.cond.cleanup106
  %124 = load i32, i32* %iloc, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %124)
  %125 = bitcast i32* %iinf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #4
  %call131 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.22, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call131, i32* %iinf, align 4, !tbaa !6
  %126 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data132 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %126, i32 0, i32 12
  %127 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data132, align 8, !tbaa !28
  %items133 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %127, i32 0, i32 0
  %count134 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items133, i32 0, i32 2
  %128 = load i32, i32* %count134, align 4, !tbaa !31
  %conv135 = trunc i32 %128 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %conv135)
  %129 = bitcast i32* %itemIndex136 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #4
  store i32 0, i32* %itemIndex136, align 4, !tbaa !8
  br label %for.cond137

for.cond137:                                      ; preds = %for.inc158, %for.end130
  %130 = load i32, i32* %itemIndex136, align 4, !tbaa !8
  %131 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data138 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %131, i32 0, i32 12
  %132 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data138, align 8, !tbaa !28
  %items139 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %132, i32 0, i32 0
  %count140 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items139, i32 0, i32 2
  %133 = load i32, i32* %count140, align 4, !tbaa !31
  %cmp141 = icmp ult i32 %130, %133
  br i1 %cmp141, label %for.body144, label %for.cond.cleanup143

for.cond.cleanup143:                              ; preds = %for.cond137
  store i32 11, i32* %cleanup.dest.slot, align 4
  %134 = bitcast i32* %itemIndex136 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #4
  br label %for.end161

for.body144:                                      ; preds = %for.cond137
  %135 = bitcast %struct.avifEncoderItem** %item145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #4
  %136 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data146 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %136, i32 0, i32 12
  %137 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data146, align 8, !tbaa !28
  %items147 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %137, i32 0, i32 0
  %item148 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items147, i32 0, i32 0
  %138 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item148, align 4, !tbaa !36
  %139 = load i32, i32* %itemIndex136, align 4, !tbaa !8
  %arrayidx149 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %138, i32 %139
  store %struct.avifEncoderItem* %arrayidx149, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %140 = bitcast i32* %infe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #4
  %call150 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.23, i32 0, i32 0), i32 0, i32 2, i32 0)
  store i32 %call150, i32* %infe, align 4, !tbaa !6
  %141 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %id151 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %141, i32 0, i32 0
  %142 = load i16, i16* %id151, align 4, !tbaa !55
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %142)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  %143 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %143, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  call void @avifRWStreamWrite(%struct.avifRWStream* %s, i8* %arraydecay, i32 4)
  %144 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %infeName = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %144, i32 0, i32 6
  %145 = load i8*, i8** %infeName, align 4, !tbaa !76
  %146 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %infeNameSize = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %146, i32 0, i32 7
  %147 = load i32, i32* %infeNameSize, align 4, !tbaa !77
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* %145, i32 %147)
  %148 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %infeContentType = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %148, i32 0, i32 8
  %149 = load i8*, i8** %infeContentType, align 4, !tbaa !66
  %tobool152 = icmp ne i8* %149, null
  br i1 %tobool152, label %land.lhs.true, label %if.end157

land.lhs.true:                                    ; preds = %for.body144
  %150 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %infeContentTypeSize = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %150, i32 0, i32 9
  %151 = load i32, i32* %infeContentTypeSize, align 4, !tbaa !67
  %tobool153 = icmp ne i32 %151, 0
  br i1 %tobool153, label %if.then154, label %if.end157

if.then154:                                       ; preds = %land.lhs.true
  %152 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %infeContentType155 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %152, i32 0, i32 8
  %153 = load i8*, i8** %infeContentType155, align 4, !tbaa !66
  %154 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item145, align 4, !tbaa !2
  %infeContentTypeSize156 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %154, i32 0, i32 9
  %155 = load i32, i32* %infeContentTypeSize156, align 4, !tbaa !67
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* %153, i32 %155)
  br label %if.end157

if.end157:                                        ; preds = %if.then154, %land.lhs.true, %for.body144
  %156 = load i32, i32* %infe, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %156)
  %157 = bitcast i32* %infe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %158 = bitcast %struct.avifEncoderItem** %item145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  br label %for.inc158

for.inc158:                                       ; preds = %if.end157
  %159 = load i32, i32* %itemIndex136, align 4, !tbaa !8
  %inc159 = add i32 %159, 1
  store i32 %inc159, i32* %itemIndex136, align 4, !tbaa !8
  br label %for.cond137

for.end161:                                       ; preds = %for.cond.cleanup143
  %160 = load i32, i32* %iinf, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %160)
  %161 = bitcast i32* %iref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %161) #4
  store i32 0, i32* %iref, align 4, !tbaa !6
  %162 = bitcast i32* %itemIndex162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #4
  store i32 0, i32* %itemIndex162, align 4, !tbaa !8
  br label %for.cond163

for.cond163:                                      ; preds = %for.inc188, %for.end161
  %163 = load i32, i32* %itemIndex162, align 4, !tbaa !8
  %164 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data164 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %164, i32 0, i32 12
  %165 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data164, align 8, !tbaa !28
  %items165 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %165, i32 0, i32 0
  %count166 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items165, i32 0, i32 2
  %166 = load i32, i32* %count166, align 4, !tbaa !31
  %cmp167 = icmp ult i32 %163, %166
  br i1 %cmp167, label %for.body170, label %for.cond.cleanup169

for.cond.cleanup169:                              ; preds = %for.cond163
  store i32 14, i32* %cleanup.dest.slot, align 4
  %167 = bitcast i32* %itemIndex162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  br label %for.end191

for.body170:                                      ; preds = %for.cond163
  %168 = bitcast %struct.avifEncoderItem** %item171 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %168) #4
  %169 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data172 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %169, i32 0, i32 12
  %170 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data172, align 8, !tbaa !28
  %items173 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %170, i32 0, i32 0
  %item174 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items173, i32 0, i32 0
  %171 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item174, align 4, !tbaa !36
  %172 = load i32, i32* %itemIndex162, align 4, !tbaa !8
  %arrayidx175 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %171, i32 %172
  store %struct.avifEncoderItem* %arrayidx175, %struct.avifEncoderItem** %item171, align 4, !tbaa !2
  %173 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item171, align 4, !tbaa !2
  %irefToID = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %173, i32 0, i32 11
  %174 = load i16, i16* %irefToID, align 4, !tbaa !60
  %conv176 = zext i16 %174 to i32
  %cmp177 = icmp ne i32 %conv176, 0
  br i1 %cmp177, label %if.then179, label %if.end187

if.then179:                                       ; preds = %for.body170
  %175 = load i32, i32* %iref, align 4, !tbaa !6
  %tobool180 = icmp ne i32 %175, 0
  br i1 %tobool180, label %if.end183, label %if.then181

if.then181:                                       ; preds = %if.then179
  %call182 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.24, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call182, i32* %iref, align 4, !tbaa !6
  br label %if.end183

if.end183:                                        ; preds = %if.then181, %if.then179
  %176 = bitcast i32* %refType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %176) #4
  %177 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item171, align 4, !tbaa !2
  %irefType = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %177, i32 0, i32 12
  %178 = load i8*, i8** %irefType, align 4, !tbaa !61
  %call184 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* %178, i32 0)
  store i32 %call184, i32* %refType, align 4, !tbaa !6
  %179 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item171, align 4, !tbaa !2
  %id185 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %179, i32 0, i32 0
  %180 = load i16, i16* %id185, align 4, !tbaa !55
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %180)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 1)
  %181 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item171, align 4, !tbaa !2
  %irefToID186 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %181, i32 0, i32 11
  %182 = load i16, i16* %irefToID186, align 4, !tbaa !60
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %182)
  %183 = load i32, i32* %refType, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %183)
  %184 = bitcast i32* %refType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #4
  br label %if.end187

if.end187:                                        ; preds = %if.end183, %for.body170
  %185 = bitcast %struct.avifEncoderItem** %item171 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #4
  br label %for.inc188

for.inc188:                                       ; preds = %if.end187
  %186 = load i32, i32* %itemIndex162, align 4, !tbaa !8
  %inc189 = add i32 %186, 1
  store i32 %inc189, i32* %itemIndex162, align 4, !tbaa !8
  br label %for.cond163

for.end191:                                       ; preds = %for.cond.cleanup169
  %187 = load i32, i32* %iref, align 4, !tbaa !6
  %tobool192 = icmp ne i32 %187, 0
  br i1 %tobool192, label %if.then193, label %if.end194

if.then193:                                       ; preds = %for.end191
  %188 = load i32, i32* %iref, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %188)
  br label %if.end194

if.end194:                                        ; preds = %if.then193, %for.end191
  %189 = bitcast i32* %iprp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %189) #4
  %call195 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.25, i32 0, i32 0), i32 0)
  store i32 %call195, i32* %iprp, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %itemPropertyIndex) #4
  store i8 0, i8* %itemPropertyIndex, align 1, !tbaa !80
  %190 = bitcast i32* %ipco to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %190) #4
  %call196 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.26, i32 0, i32 0), i32 0)
  store i32 %call196, i32* %ipco, align 4, !tbaa !6
  %191 = bitcast i32* %itemIndex197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %191) #4
  store i32 0, i32* %itemIndex197, align 4, !tbaa !8
  br label %for.cond198

for.cond198:                                      ; preds = %for.inc256, %if.end194
  %192 = load i32, i32* %itemIndex197, align 4, !tbaa !8
  %193 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data199 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %193, i32 0, i32 12
  %194 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data199, align 8, !tbaa !28
  %items200 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %194, i32 0, i32 0
  %count201 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items200, i32 0, i32 2
  %195 = load i32, i32* %count201, align 4, !tbaa !31
  %cmp202 = icmp ult i32 %192, %195
  br i1 %cmp202, label %for.body205, label %for.cond.cleanup204

for.cond.cleanup204:                              ; preds = %for.cond198
  store i32 17, i32* %cleanup.dest.slot, align 4
  %196 = bitcast i32* %itemIndex197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  br label %for.end259

for.body205:                                      ; preds = %for.cond198
  %197 = bitcast %struct.avifEncoderItem** %item206 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #4
  %198 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data207 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %198, i32 0, i32 12
  %199 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data207, align 8, !tbaa !28
  %items208 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %199, i32 0, i32 0
  %item209 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items208, i32 0, i32 0
  %200 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item209, align 4, !tbaa !36
  %201 = load i32, i32* %itemIndex197, align 4, !tbaa !8
  %arrayidx210 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %200, i32 %201
  store %struct.avifEncoderItem* %arrayidx210, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %202 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %ipma = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %202, i32 0, i32 13
  %203 = bitcast %struct.ipmaArray* %ipma to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %203, i8 0, i32 84, i1 false)
  %204 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %codec211 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %204, i32 0, i32 2
  %205 = load %struct.avifCodec*, %struct.avifCodec** %codec211, align 4, !tbaa !37
  %tobool212 = icmp ne %struct.avifCodec* %205, null
  br i1 %tobool212, label %if.end214, label %if.then213

if.then213:                                       ; preds = %for.body205
  store i32 19, i32* %cleanup.dest.slot, align 4
  br label %cleanup253

if.end214:                                        ; preds = %for.body205
  %206 = bitcast i32* %ispe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %206) #4
  %call215 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.27, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call215, i32* %ispe, align 4, !tbaa !6
  %207 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %207, i32 0, i32 0
  %208 = load i32, i32* %width, align 4, !tbaa !50
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %208)
  %209 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %209, i32 0, i32 1
  %210 = load i32, i32* %height, align 4, !tbaa !51
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %210)
  %211 = load i32, i32* %ispe, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %211)
  %212 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %ipma216 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %212, i32 0, i32 13
  %213 = load i8, i8* %itemPropertyIndex, align 1, !tbaa !80
  %inc217 = add i8 %213, 1
  store i8 %inc217, i8* %itemPropertyIndex, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %ipma216, i8 zeroext %inc217, i32 0)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %channelCount) #4
  %214 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %alpha218 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %214, i32 0, i32 5
  %215 = load i32, i32* %alpha218, align 4, !tbaa !59
  %tobool219 = icmp ne i32 %215, 0
  br i1 %tobool219, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end214
  %216 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %yuvFormat220 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %216, i32 0, i32 3
  %217 = load i32, i32* %yuvFormat220, align 4, !tbaa !52
  %cmp221 = icmp eq i32 %217, 4
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.end214
  %218 = phi i1 [ true, %if.end214 ], [ %cmp221, %lor.rhs ]
  %219 = zext i1 %218 to i64
  %cond223 = select i1 %218, i32 1, i32 3
  %conv224 = trunc i32 %cond223 to i8
  store i8 %conv224, i8* %channelCount, align 1, !tbaa !80
  %220 = bitcast i32* %pixi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %220) #4
  %call225 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.28, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call225, i32* %pixi, align 4, !tbaa !6
  %221 = load i8, i8* %channelCount, align 1, !tbaa !80
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %s, i8 zeroext %221)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %chan) #4
  store i8 0, i8* %chan, align 1, !tbaa !80
  br label %for.cond226

for.cond226:                                      ; preds = %for.inc235, %lor.end
  %222 = load i8, i8* %chan, align 1, !tbaa !80
  %conv227 = zext i8 %222 to i32
  %223 = load i8, i8* %channelCount, align 1, !tbaa !80
  %conv228 = zext i8 %223 to i32
  %cmp229 = icmp slt i32 %conv227, %conv228
  br i1 %cmp229, label %for.body232, label %for.cond.cleanup231

for.cond.cleanup231:                              ; preds = %for.cond226
  store i32 20, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %chan) #4
  br label %for.end238

for.body232:                                      ; preds = %for.cond226
  %224 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %depth233 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %224, i32 0, i32 2
  %225 = load i32, i32* %depth233, align 4, !tbaa !44
  %conv234 = trunc i32 %225 to i8
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %s, i8 zeroext %conv234)
  br label %for.inc235

for.inc235:                                       ; preds = %for.body232
  %226 = load i8, i8* %chan, align 1, !tbaa !80
  %inc236 = add i8 %226, 1
  store i8 %inc236, i8* %chan, align 1, !tbaa !80
  br label %for.cond226

for.end238:                                       ; preds = %for.cond.cleanup231
  %227 = load i32, i32* %pixi, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %227)
  %228 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %ipma239 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %228, i32 0, i32 13
  %229 = load i8, i8* %itemPropertyIndex, align 1, !tbaa !80
  %inc240 = add i8 %229, 1
  store i8 %inc240, i8* %itemPropertyIndex, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %ipma239, i8 zeroext %inc240, i32 0)
  %230 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %codec241 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %230, i32 0, i32 2
  %231 = load %struct.avifCodec*, %struct.avifCodec** %codec241, align 4, !tbaa !37
  %configBox = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %231, i32 0, i32 1
  call void @writeConfigBox(%struct.avifRWStream* %s, %struct.avifCodecConfigurationBox* %configBox)
  %232 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %ipma242 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %232, i32 0, i32 13
  %233 = load i8, i8* %itemPropertyIndex, align 1, !tbaa !80
  %inc243 = add i8 %233, 1
  store i8 %inc243, i8* %itemPropertyIndex, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %ipma242, i8 zeroext %inc243, i32 1)
  %234 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %alpha244 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %234, i32 0, i32 5
  %235 = load i32, i32* %alpha244, align 4, !tbaa !59
  %tobool245 = icmp ne i32 %235, 0
  br i1 %tobool245, label %if.then246, label %if.else250

if.then246:                                       ; preds = %for.end238
  %236 = bitcast i32* %auxC to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %236) #4
  %call247 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.29, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call247, i32* %auxC, align 4, !tbaa !6
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @alphaURN, i32 0, i32 0), i32 44)
  %237 = load i32, i32* %auxC, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %237)
  %238 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %ipma248 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %238, i32 0, i32 13
  %239 = load i8, i8* %itemPropertyIndex, align 1, !tbaa !80
  %inc249 = add i8 %239, 1
  store i8 %inc249, i8* %itemPropertyIndex, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %ipma248, i8 zeroext %inc249, i32 0)
  %240 = bitcast i32* %auxC to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #4
  br label %if.end252

if.else250:                                       ; preds = %for.end238
  %241 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %242 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item206, align 4, !tbaa !2
  %ipma251 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %242, i32 0, i32 13
  call void @avifEncoderWriteColorProperties(%struct.avifRWStream* %s, %struct.avifImage* %241, %struct.ipmaArray* %ipma251, i8* %itemPropertyIndex)
  br label %if.end252

if.end252:                                        ; preds = %if.else250, %if.then246
  %243 = bitcast i32* %pixi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %channelCount) #4
  %244 = bitcast i32* %ispe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup253

cleanup253:                                       ; preds = %if.end252, %if.then213
  %245 = bitcast %struct.avifEncoderItem** %item206 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #4
  %cleanup.dest254 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest254, label %unreachable [
    i32 0, label %cleanup.cont255
    i32 19, label %for.inc256
  ]

cleanup.cont255:                                  ; preds = %cleanup253
  br label %for.inc256

for.inc256:                                       ; preds = %cleanup.cont255, %cleanup253
  %246 = load i32, i32* %itemIndex197, align 4, !tbaa !8
  %inc257 = add i32 %246, 1
  store i32 %inc257, i32* %itemIndex197, align 4, !tbaa !8
  br label %for.cond198

for.end259:                                       ; preds = %for.cond.cleanup204
  %247 = load i32, i32* %ipco, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %247)
  %248 = bitcast i32* %ipma260 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %248) #4
  %call261 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.30, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call261, i32* %ipma260, align 4, !tbaa !6
  %249 = bitcast i32* %ipmaCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %249) #4
  store i32 0, i32* %ipmaCount, align 4, !tbaa !8
  %250 = bitcast i32* %itemIndex262 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %250) #4
  store i32 0, i32* %itemIndex262, align 4, !tbaa !8
  br label %for.cond263

for.cond263:                                      ; preds = %for.inc284, %for.end259
  %251 = load i32, i32* %itemIndex262, align 4, !tbaa !8
  %252 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data264 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %252, i32 0, i32 12
  %253 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data264, align 8, !tbaa !28
  %items265 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %253, i32 0, i32 0
  %count266 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items265, i32 0, i32 2
  %254 = load i32, i32* %count266, align 4, !tbaa !31
  %cmp267 = icmp ult i32 %251, %254
  br i1 %cmp267, label %for.body270, label %for.cond.cleanup269

for.cond.cleanup269:                              ; preds = %for.cond263
  store i32 23, i32* %cleanup.dest.slot, align 4
  %255 = bitcast i32* %itemIndex262 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #4
  br label %for.end287

for.body270:                                      ; preds = %for.cond263
  %256 = bitcast %struct.avifEncoderItem** %item271 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %256) #4
  %257 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data272 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %257, i32 0, i32 12
  %258 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data272, align 8, !tbaa !28
  %items273 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %258, i32 0, i32 0
  %item274 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items273, i32 0, i32 0
  %259 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item274, align 4, !tbaa !36
  %260 = load i32, i32* %itemIndex262, align 4, !tbaa !8
  %arrayidx275 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %259, i32 %260
  store %struct.avifEncoderItem* %arrayidx275, %struct.avifEncoderItem** %item271, align 4, !tbaa !2
  %261 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item271, align 4, !tbaa !2
  %ipma276 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %261, i32 0, i32 13
  %count277 = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %ipma276, i32 0, i32 2
  %262 = load i8, i8* %count277, align 4, !tbaa !99
  %conv278 = zext i8 %262 to i32
  %cmp279 = icmp sgt i32 %conv278, 0
  br i1 %cmp279, label %if.then281, label %if.end283

if.then281:                                       ; preds = %for.body270
  %263 = load i32, i32* %ipmaCount, align 4, !tbaa !8
  %inc282 = add nsw i32 %263, 1
  store i32 %inc282, i32* %ipmaCount, align 4, !tbaa !8
  br label %if.end283

if.end283:                                        ; preds = %if.then281, %for.body270
  %264 = bitcast %struct.avifEncoderItem** %item271 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #4
  br label %for.inc284

for.inc284:                                       ; preds = %if.end283
  %265 = load i32, i32* %itemIndex262, align 4, !tbaa !8
  %inc285 = add i32 %265, 1
  store i32 %inc285, i32* %itemIndex262, align 4, !tbaa !8
  br label %for.cond263

for.end287:                                       ; preds = %for.cond.cleanup269
  %266 = load i32, i32* %ipmaCount, align 4, !tbaa !8
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %266)
  %267 = bitcast i32* %itemIndex288 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %267) #4
  store i32 0, i32* %itemIndex288, align 4, !tbaa !8
  br label %for.cond289

for.cond289:                                      ; preds = %for.inc336, %for.end287
  %268 = load i32, i32* %itemIndex288, align 4, !tbaa !8
  %269 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data290 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %269, i32 0, i32 12
  %270 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data290, align 8, !tbaa !28
  %items291 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %270, i32 0, i32 0
  %count292 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items291, i32 0, i32 2
  %271 = load i32, i32* %count292, align 4, !tbaa !31
  %cmp293 = icmp ult i32 %268, %271
  br i1 %cmp293, label %for.body296, label %for.cond.cleanup295

for.cond.cleanup295:                              ; preds = %for.cond289
  store i32 26, i32* %cleanup.dest.slot, align 4
  %272 = bitcast i32* %itemIndex288 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #4
  br label %for.end339

for.body296:                                      ; preds = %for.cond289
  %273 = bitcast %struct.avifEncoderItem** %item297 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %273) #4
  %274 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data298 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %274, i32 0, i32 12
  %275 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data298, align 8, !tbaa !28
  %items299 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %275, i32 0, i32 0
  %item300 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items299, i32 0, i32 0
  %276 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item300, align 4, !tbaa !36
  %277 = load i32, i32* %itemIndex288, align 4, !tbaa !8
  %arrayidx301 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %276, i32 %277
  store %struct.avifEncoderItem* %arrayidx301, %struct.avifEncoderItem** %item297, align 4, !tbaa !2
  %278 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item297, align 4, !tbaa !2
  %ipma302 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %278, i32 0, i32 13
  %count303 = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %ipma302, i32 0, i32 2
  %279 = load i8, i8* %count303, align 4, !tbaa !99
  %conv304 = zext i8 %279 to i32
  %cmp305 = icmp eq i32 %conv304, 0
  br i1 %cmp305, label %if.then307, label %if.end308

if.then307:                                       ; preds = %for.body296
  store i32 28, i32* %cleanup.dest.slot, align 4
  br label %cleanup333

if.end308:                                        ; preds = %for.body296
  %280 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item297, align 4, !tbaa !2
  %id309 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %280, i32 0, i32 0
  %281 = load i16, i16* %id309, align 4, !tbaa !55
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %281)
  %282 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item297, align 4, !tbaa !2
  %ipma310 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %282, i32 0, i32 13
  %count311 = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %ipma310, i32 0, i32 2
  %283 = load i8, i8* %count311, align 4, !tbaa !99
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %s, i8 zeroext %283)
  %284 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %284) #4
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond312

for.cond312:                                      ; preds = %for.inc329, %if.end308
  %285 = load i32, i32* %i, align 4, !tbaa !8
  %286 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item297, align 4, !tbaa !2
  %ipma313 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %286, i32 0, i32 13
  %count314 = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %ipma313, i32 0, i32 2
  %287 = load i8, i8* %count314, align 4, !tbaa !99
  %conv315 = zext i8 %287 to i32
  %cmp316 = icmp slt i32 %285, %conv315
  br i1 %cmp316, label %for.body319, label %for.cond.cleanup318

for.cond.cleanup318:                              ; preds = %for.cond312
  store i32 29, i32* %cleanup.dest.slot, align 4
  %288 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #4
  br label %for.end332

for.body319:                                      ; preds = %for.cond312
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %essentialAndIndex) #4
  %289 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item297, align 4, !tbaa !2
  %ipma320 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %289, i32 0, i32 13
  %associations = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %ipma320, i32 0, i32 0
  %290 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx321 = getelementptr inbounds [16 x i8], [16 x i8]* %associations, i32 0, i32 %290
  %291 = load i8, i8* %arrayidx321, align 1, !tbaa !80
  store i8 %291, i8* %essentialAndIndex, align 1, !tbaa !80
  %292 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item297, align 4, !tbaa !2
  %ipma322 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %292, i32 0, i32 13
  %essential = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %ipma322, i32 0, i32 1
  %293 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx323 = getelementptr inbounds [16 x i32], [16 x i32]* %essential, i32 0, i32 %293
  %294 = load i32, i32* %arrayidx323, align 4, !tbaa !8
  %tobool324 = icmp ne i32 %294, 0
  br i1 %tobool324, label %if.then325, label %if.end328

if.then325:                                       ; preds = %for.body319
  %295 = load i8, i8* %essentialAndIndex, align 1, !tbaa !80
  %conv326 = zext i8 %295 to i32
  %or = or i32 %conv326, 128
  %conv327 = trunc i32 %or to i8
  store i8 %conv327, i8* %essentialAndIndex, align 1, !tbaa !80
  br label %if.end328

if.end328:                                        ; preds = %if.then325, %for.body319
  %296 = load i8, i8* %essentialAndIndex, align 1, !tbaa !80
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %s, i8 zeroext %296)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %essentialAndIndex) #4
  br label %for.inc329

for.inc329:                                       ; preds = %if.end328
  %297 = load i32, i32* %i, align 4, !tbaa !8
  %inc330 = add nsw i32 %297, 1
  store i32 %inc330, i32* %i, align 4, !tbaa !8
  br label %for.cond312

for.end332:                                       ; preds = %for.cond.cleanup318
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup333

cleanup333:                                       ; preds = %for.end332, %if.then307
  %298 = bitcast %struct.avifEncoderItem** %item297 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #4
  %cleanup.dest334 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest334, label %unreachable [
    i32 0, label %cleanup.cont335
    i32 28, label %for.inc336
  ]

cleanup.cont335:                                  ; preds = %cleanup333
  br label %for.inc336

for.inc336:                                       ; preds = %cleanup.cont335, %cleanup333
  %299 = load i32, i32* %itemIndex288, align 4, !tbaa !8
  %inc337 = add i32 %299, 1
  store i32 %inc337, i32* %itemIndex288, align 4, !tbaa !8
  br label %for.cond289

for.end339:                                       ; preds = %for.cond.cleanup295
  %300 = bitcast i32* %ipmaCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %300) #4
  %301 = load i32, i32* %ipma260, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %301)
  %302 = load i32, i32* %iprp, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %302)
  %303 = load i32, i32* %meta, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %303)
  %304 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data340 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %304, i32 0, i32 12
  %305 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data340, align 8, !tbaa !28
  %frames341 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %305, i32 0, i32 1
  %count342 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames341, i32 0, i32 2
  %306 = load i32, i32* %count342, align 4, !tbaa !69
  %cmp343 = icmp ugt i32 %306, 1
  br i1 %cmp343, label %if.then345, label %if.end568

if.then345:                                       ; preds = %for.end339
  %307 = bitcast i64* %durationInTimescales to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %307) #4
  store i64 0, i64* %durationInTimescales, align 8, !tbaa !43
  %308 = bitcast i32* %frameIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %308) #4
  store i32 0, i32* %frameIndex, align 4, !tbaa !8
  br label %for.cond346

for.cond346:                                      ; preds = %for.inc360, %if.then345
  %309 = load i32, i32* %frameIndex, align 4, !tbaa !8
  %310 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data347 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %310, i32 0, i32 12
  %311 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data347, align 8, !tbaa !28
  %frames348 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %311, i32 0, i32 1
  %count349 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames348, i32 0, i32 2
  %312 = load i32, i32* %count349, align 4, !tbaa !69
  %cmp350 = icmp ult i32 %309, %312
  br i1 %cmp350, label %for.body353, label %for.cond.cleanup352

for.cond.cleanup352:                              ; preds = %for.cond346
  store i32 32, i32* %cleanup.dest.slot, align 4
  %313 = bitcast i32* %frameIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #4
  br label %for.end363

for.body353:                                      ; preds = %for.cond346
  %314 = bitcast %struct.avifEncoderFrame** %frame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %314) #4
  %315 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data354 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %315, i32 0, i32 12
  %316 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data354, align 8, !tbaa !28
  %frames355 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %316, i32 0, i32 1
  %frame356 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames355, i32 0, i32 0
  %317 = load %struct.avifEncoderFrame*, %struct.avifEncoderFrame** %frame356, align 4, !tbaa !100
  %318 = load i32, i32* %frameIndex, align 4, !tbaa !8
  %arrayidx357 = getelementptr inbounds %struct.avifEncoderFrame, %struct.avifEncoderFrame* %317, i32 %318
  store %struct.avifEncoderFrame* %arrayidx357, %struct.avifEncoderFrame** %frame, align 4, !tbaa !2
  %319 = load %struct.avifEncoderFrame*, %struct.avifEncoderFrame** %frame, align 4, !tbaa !2
  %durationInTimescales358 = getelementptr inbounds %struct.avifEncoderFrame, %struct.avifEncoderFrame* %319, i32 0, i32 0
  %320 = load i64, i64* %durationInTimescales358, align 8, !tbaa !73
  %321 = load i64, i64* %durationInTimescales, align 8, !tbaa !43
  %add359 = add i64 %321, %320
  store i64 %add359, i64* %durationInTimescales, align 8, !tbaa !43
  %322 = bitcast %struct.avifEncoderFrame** %frame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %322) #4
  br label %for.inc360

for.inc360:                                       ; preds = %for.body353
  %323 = load i32, i32* %frameIndex, align 4, !tbaa !8
  %inc361 = add i32 %323, 1
  store i32 %inc361, i32* %frameIndex, align 4, !tbaa !8
  br label %for.cond346

for.end363:                                       ; preds = %for.cond.cleanup352
  %324 = bitcast i32* %moov to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %324) #4
  %call364 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.31, i32 0, i32 0), i32 0)
  store i32 %call364, i32* %moov, align 4, !tbaa !6
  %325 = bitcast i32* %mvhd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %325) #4
  %call365 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.32, i32 0, i32 0), i32 0, i32 1, i32 0)
  store i32 %call365, i32* %mvhd, align 4, !tbaa !6
  %326 = load i64, i64* %now, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %326)
  %327 = load i64, i64* %now, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %327)
  %328 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %timescale = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %328, i32 0, i32 10
  %329 = load i64, i64* %timescale, align 8, !tbaa !29
  %conv366 = trunc i64 %329 to i32
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %conv366)
  %330 = load i64, i64* %durationInTimescales, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %330)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 65536)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 256)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 8)
  call void @avifRWStreamWrite(%struct.avifRWStream* %s, i8* bitcast ([9 x i32]* @avifEncoderFinish.unityMatrix to i8*), i32 36)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 24)
  %331 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data367 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %331, i32 0, i32 12
  %332 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data367, align 8, !tbaa !28
  %items368 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %332, i32 0, i32 0
  %count369 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items368, i32 0, i32 2
  %333 = load i32, i32* %count369, align 4, !tbaa !31
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %333)
  %334 = load i32, i32* %mvhd, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %334)
  %335 = bitcast i32* %itemIndex370 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %335) #4
  store i32 0, i32* %itemIndex370, align 4, !tbaa !8
  br label %for.cond371

for.cond371:                                      ; preds = %for.inc564, %for.end363
  %336 = load i32, i32* %itemIndex370, align 4, !tbaa !8
  %337 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data372 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %337, i32 0, i32 12
  %338 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data372, align 8, !tbaa !28
  %items373 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %338, i32 0, i32 0
  %count374 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items373, i32 0, i32 2
  %339 = load i32, i32* %count374, align 4, !tbaa !31
  %cmp375 = icmp ult i32 %336, %339
  br i1 %cmp375, label %for.body378, label %for.cond.cleanup377

for.cond.cleanup377:                              ; preds = %for.cond371
  store i32 35, i32* %cleanup.dest.slot, align 4
  %340 = bitcast i32* %itemIndex370 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #4
  br label %for.end567

for.body378:                                      ; preds = %for.cond371
  %341 = bitcast %struct.avifEncoderItem** %item379 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %341) #4
  %342 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data380 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %342, i32 0, i32 12
  %343 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data380, align 8, !tbaa !28
  %items381 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %343, i32 0, i32 0
  %item382 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items381, i32 0, i32 0
  %344 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item382, align 4, !tbaa !36
  %345 = load i32, i32* %itemIndex370, align 4, !tbaa !8
  %arrayidx383 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %344, i32 %345
  store %struct.avifEncoderItem* %arrayidx383, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %346 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput384 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %346, i32 0, i32 3
  %347 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput384, align 4, !tbaa !41
  %samples385 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %347, i32 0, i32 0
  %count386 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples385, i32 0, i32 2
  %348 = load i32, i32* %count386, align 4, !tbaa !13
  %cmp387 = icmp eq i32 %348, 0
  br i1 %cmp387, label %if.then389, label %if.end390

if.then389:                                       ; preds = %for.body378
  store i32 37, i32* %cleanup.dest.slot, align 4
  br label %cleanup561

if.end390:                                        ; preds = %for.body378
  %349 = bitcast i32* %syncSamplesCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %349) #4
  store i32 0, i32* %syncSamplesCount, align 4, !tbaa !8
  %350 = bitcast i32* %sampleIndex391 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %350) #4
  store i32 0, i32* %sampleIndex391, align 4, !tbaa !8
  br label %for.cond392

for.cond392:                                      ; preds = %for.inc409, %if.end390
  %351 = load i32, i32* %sampleIndex391, align 4, !tbaa !8
  %352 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput393 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %352, i32 0, i32 3
  %353 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput393, align 4, !tbaa !41
  %samples394 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %353, i32 0, i32 0
  %count395 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples394, i32 0, i32 2
  %354 = load i32, i32* %count395, align 4, !tbaa !13
  %cmp396 = icmp ult i32 %351, %354
  br i1 %cmp396, label %for.body399, label %for.cond.cleanup398

for.cond.cleanup398:                              ; preds = %for.cond392
  store i32 38, i32* %cleanup.dest.slot, align 4
  %355 = bitcast i32* %sampleIndex391 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %355) #4
  br label %for.end412

for.body399:                                      ; preds = %for.cond392
  %356 = bitcast %struct.avifEncodeSample** %sample400 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %356) #4
  %357 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput401 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %357, i32 0, i32 3
  %358 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput401, align 4, !tbaa !41
  %samples402 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %358, i32 0, i32 0
  %sample403 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples402, i32 0, i32 0
  %359 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample403, align 4, !tbaa !16
  %360 = load i32, i32* %sampleIndex391, align 4, !tbaa !8
  %arrayidx404 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %359, i32 %360
  store %struct.avifEncodeSample* %arrayidx404, %struct.avifEncodeSample** %sample400, align 4, !tbaa !2
  %361 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample400, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %361, i32 0, i32 1
  %362 = load i32, i32* %sync, align 4, !tbaa !10
  %tobool405 = icmp ne i32 %362, 0
  br i1 %tobool405, label %if.then406, label %if.end408

if.then406:                                       ; preds = %for.body399
  %363 = load i32, i32* %syncSamplesCount, align 4, !tbaa !8
  %inc407 = add i32 %363, 1
  store i32 %inc407, i32* %syncSamplesCount, align 4, !tbaa !8
  br label %if.end408

if.end408:                                        ; preds = %if.then406, %for.body399
  %364 = bitcast %struct.avifEncodeSample** %sample400 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %364) #4
  br label %for.inc409

for.inc409:                                       ; preds = %if.end408
  %365 = load i32, i32* %sampleIndex391, align 4, !tbaa !8
  %inc410 = add i32 %365, 1
  store i32 %inc410, i32* %sampleIndex391, align 4, !tbaa !8
  br label %for.cond392

for.end412:                                       ; preds = %for.cond.cleanup398
  %366 = bitcast i32* %trak to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %366) #4
  %call413 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.33, i32 0, i32 0), i32 0)
  store i32 %call413, i32* %trak, align 4, !tbaa !6
  %367 = bitcast i32* %tkhd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %367) #4
  %call414 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.34, i32 0, i32 0), i32 0, i32 1, i32 1)
  store i32 %call414, i32* %tkhd, align 4, !tbaa !6
  %368 = load i64, i64* %now, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %368)
  %369 = load i64, i64* %now, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %369)
  %370 = load i32, i32* %itemIndex370, align 4, !tbaa !8
  %add415 = add i32 %370, 1
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %add415)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 0)
  %371 = load i64, i64* %durationInTimescales, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %371)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 8)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWrite(%struct.avifRWStream* %s, i8* bitcast ([9 x i32]* @avifEncoderFinish.unityMatrix to i8*), i32 36)
  %372 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %width416 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %372, i32 0, i32 0
  %373 = load i32, i32* %width416, align 4, !tbaa !50
  %shl = shl i32 %373, 16
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %shl)
  %374 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %height417 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %374, i32 0, i32 1
  %375 = load i32, i32* %height417, align 4, !tbaa !51
  %shl418 = shl i32 %375, 16
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %shl418)
  %376 = load i32, i32* %tkhd, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %376)
  %377 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %irefToID419 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %377, i32 0, i32 11
  %378 = load i16, i16* %irefToID419, align 4, !tbaa !60
  %conv420 = zext i16 %378 to i32
  %cmp421 = icmp ne i32 %conv420, 0
  br i1 %cmp421, label %if.then423, label %if.end430

if.then423:                                       ; preds = %for.end412
  %379 = bitcast i32* %tref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %379) #4
  %call424 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.35, i32 0, i32 0), i32 0)
  store i32 %call424, i32* %tref, align 4, !tbaa !6
  %380 = bitcast i32* %refType425 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %380) #4
  %381 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %irefType426 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %381, i32 0, i32 12
  %382 = load i8*, i8** %irefType426, align 4, !tbaa !61
  %call427 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* %382, i32 0)
  store i32 %call427, i32* %refType425, align 4, !tbaa !6
  %383 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %irefToID428 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %383, i32 0, i32 11
  %384 = load i16, i16* %irefToID428, align 4, !tbaa !60
  %conv429 = zext i16 %384 to i32
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %conv429)
  %385 = load i32, i32* %refType425, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %385)
  %386 = load i32, i32* %tref, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %386)
  %387 = bitcast i32* %refType425 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #4
  %388 = bitcast i32* %tref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %388) #4
  br label %if.end430

if.end430:                                        ; preds = %if.then423, %for.end412
  %389 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %alpha431 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %389, i32 0, i32 5
  %390 = load i32, i32* %alpha431, align 4, !tbaa !59
  %tobool432 = icmp ne i32 %390, 0
  br i1 %tobool432, label %if.end434, label %if.then433

if.then433:                                       ; preds = %if.end430
  %391 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  call void @avifEncoderWriteTrackMetaBox(%struct.avifEncoder* %391, %struct.avifRWStream* %s)
  br label %if.end434

if.end434:                                        ; preds = %if.then433, %if.end430
  %392 = bitcast i32* %mdia to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %392) #4
  %call435 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.36, i32 0, i32 0), i32 0)
  store i32 %call435, i32* %mdia, align 4, !tbaa !6
  %393 = bitcast i32* %mdhd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %393) #4
  %call436 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.37, i32 0, i32 0), i32 0, i32 1, i32 0)
  store i32 %call436, i32* %mdhd, align 4, !tbaa !6
  %394 = load i64, i64* %now, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %394)
  %395 = load i64, i64* %now, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %395)
  %396 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %timescale437 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %396, i32 0, i32 10
  %397 = load i64, i64* %timescale437, align 8, !tbaa !29
  %conv438 = trunc i64 %397 to i32
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %conv438)
  %398 = load i64, i64* %durationInTimescales, align 8, !tbaa !43
  call void @avifRWStreamWriteU64(%struct.avifRWStream* %s, i64 %398)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 21956)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  %399 = load i32, i32* %mdhd, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %399)
  %400 = bitcast i32* %hdlrTrak to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %400) #4
  %call439 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.17, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call439, i32* %hdlrTrak, align 4, !tbaa !6
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 0)
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.18, i32 0, i32 0), i32 4)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 12)
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.19, i32 0, i32 0), i32 8)
  %401 = load i32, i32* %hdlrTrak, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %401)
  %402 = bitcast i32* %minf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %402) #4
  %call440 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.38, i32 0, i32 0), i32 0)
  store i32 %call440, i32* %minf, align 4, !tbaa !6
  %403 = bitcast i32* %vmhd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %403) #4
  %call441 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.39, i32 0, i32 0), i32 0, i32 0, i32 1)
  store i32 %call441, i32* %vmhd, align 4, !tbaa !6
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 6)
  %404 = load i32, i32* %vmhd, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %404)
  %405 = bitcast i32* %dinf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %405) #4
  %call442 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.40, i32 0, i32 0), i32 0)
  store i32 %call442, i32* %dinf, align 4, !tbaa !6
  %406 = bitcast i32* %dref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %406) #4
  %call443 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.41, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call443, i32* %dref, align 4, !tbaa !6
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 1)
  %call444 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.42, i32 0, i32 0), i32 0, i32 0, i32 1)
  %407 = load i32, i32* %dref, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %407)
  %408 = load i32, i32* %dinf, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %408)
  %409 = bitcast i32* %stbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %409) #4
  %call445 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.43, i32 0, i32 0), i32 0)
  store i32 %call445, i32* %stbl, align 4, !tbaa !6
  %410 = bitcast i32* %stco to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %410) #4
  %call446 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.44, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call446, i32* %stco, align 4, !tbaa !6
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 1)
  %411 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  call void @avifEncoderItemAddMdatFixup(%struct.avifEncoderItem* %411, %struct.avifRWStream* %s)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 1)
  %412 = load i32, i32* %stco, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %412)
  %413 = bitcast i32* %stsc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %413) #4
  %call447 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.45, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call447, i32* %stsc, align 4, !tbaa !6
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 1)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 1)
  %414 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput448 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %414, i32 0, i32 3
  %415 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput448, align 4, !tbaa !41
  %samples449 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %415, i32 0, i32 0
  %count450 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples449, i32 0, i32 2
  %416 = load i32, i32* %count450, align 4, !tbaa !13
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %416)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 1)
  %417 = load i32, i32* %stsc, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %417)
  %418 = bitcast i32* %stsz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %418) #4
  %call451 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.46, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call451, i32* %stsz, align 4, !tbaa !6
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 0)
  %419 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput452 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %419, i32 0, i32 3
  %420 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput452, align 4, !tbaa !41
  %samples453 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %420, i32 0, i32 0
  %count454 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples453, i32 0, i32 2
  %421 = load i32, i32* %count454, align 4, !tbaa !13
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %421)
  %422 = bitcast i32* %sampleIndex455 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %422) #4
  store i32 0, i32* %sampleIndex455, align 4, !tbaa !8
  br label %for.cond456

for.cond456:                                      ; preds = %for.inc471, %if.end434
  %423 = load i32, i32* %sampleIndex455, align 4, !tbaa !8
  %424 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput457 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %424, i32 0, i32 3
  %425 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput457, align 4, !tbaa !41
  %samples458 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %425, i32 0, i32 0
  %count459 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples458, i32 0, i32 2
  %426 = load i32, i32* %count459, align 4, !tbaa !13
  %cmp460 = icmp ult i32 %423, %426
  br i1 %cmp460, label %for.body463, label %for.cond.cleanup462

for.cond.cleanup462:                              ; preds = %for.cond456
  store i32 41, i32* %cleanup.dest.slot, align 4
  %427 = bitcast i32* %sampleIndex455 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %427) #4
  br label %for.end474

for.body463:                                      ; preds = %for.cond456
  %428 = bitcast %struct.avifEncodeSample** %sample464 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %428) #4
  %429 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput465 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %429, i32 0, i32 3
  %430 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput465, align 4, !tbaa !41
  %samples466 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %430, i32 0, i32 0
  %sample467 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples466, i32 0, i32 0
  %431 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample467, align 4, !tbaa !16
  %432 = load i32, i32* %sampleIndex455, align 4, !tbaa !8
  %arrayidx468 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %431, i32 %432
  store %struct.avifEncodeSample* %arrayidx468, %struct.avifEncodeSample** %sample464, align 4, !tbaa !2
  %433 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample464, align 4, !tbaa !2
  %data469 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %433, i32 0, i32 0
  %size470 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %data469, i32 0, i32 1
  %434 = load i32, i32* %size470, align 4, !tbaa !95
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %434)
  %435 = bitcast %struct.avifEncodeSample** %sample464 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %435) #4
  br label %for.inc471

for.inc471:                                       ; preds = %for.body463
  %436 = load i32, i32* %sampleIndex455, align 4, !tbaa !8
  %inc472 = add i32 %436, 1
  store i32 %inc472, i32* %sampleIndex455, align 4, !tbaa !8
  br label %for.cond456

for.end474:                                       ; preds = %for.cond.cleanup462
  %437 = load i32, i32* %stsz, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %437)
  %438 = bitcast i32* %stss to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %438) #4
  %call475 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.47, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call475, i32* %stss, align 4, !tbaa !6
  %439 = load i32, i32* %syncSamplesCount, align 4, !tbaa !8
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %439)
  %440 = bitcast i32* %sampleIndex476 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %440) #4
  store i32 0, i32* %sampleIndex476, align 4, !tbaa !8
  br label %for.cond477

for.cond477:                                      ; preds = %for.inc495, %for.end474
  %441 = load i32, i32* %sampleIndex476, align 4, !tbaa !8
  %442 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput478 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %442, i32 0, i32 3
  %443 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput478, align 4, !tbaa !41
  %samples479 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %443, i32 0, i32 0
  %count480 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples479, i32 0, i32 2
  %444 = load i32, i32* %count480, align 4, !tbaa !13
  %cmp481 = icmp ult i32 %441, %444
  br i1 %cmp481, label %for.body484, label %for.cond.cleanup483

for.cond.cleanup483:                              ; preds = %for.cond477
  store i32 44, i32* %cleanup.dest.slot, align 4
  %445 = bitcast i32* %sampleIndex476 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %445) #4
  br label %for.end498

for.body484:                                      ; preds = %for.cond477
  %446 = bitcast %struct.avifEncodeSample** %sample485 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %446) #4
  %447 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %encodeOutput486 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %447, i32 0, i32 3
  %448 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput486, align 4, !tbaa !41
  %samples487 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %448, i32 0, i32 0
  %sample488 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples487, i32 0, i32 0
  %449 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample488, align 4, !tbaa !16
  %450 = load i32, i32* %sampleIndex476, align 4, !tbaa !8
  %arrayidx489 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %449, i32 %450
  store %struct.avifEncodeSample* %arrayidx489, %struct.avifEncodeSample** %sample485, align 4, !tbaa !2
  %451 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample485, align 4, !tbaa !2
  %sync490 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %451, i32 0, i32 1
  %452 = load i32, i32* %sync490, align 4, !tbaa !10
  %tobool491 = icmp ne i32 %452, 0
  br i1 %tobool491, label %if.then492, label %if.end494

if.then492:                                       ; preds = %for.body484
  %453 = load i32, i32* %sampleIndex476, align 4, !tbaa !8
  %add493 = add i32 %453, 1
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %add493)
  br label %if.end494

if.end494:                                        ; preds = %if.then492, %for.body484
  %454 = bitcast %struct.avifEncodeSample** %sample485 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %454) #4
  br label %for.inc495

for.inc495:                                       ; preds = %if.end494
  %455 = load i32, i32* %sampleIndex476, align 4, !tbaa !8
  %inc496 = add i32 %455, 1
  store i32 %inc496, i32* %sampleIndex476, align 4, !tbaa !8
  br label %for.cond477

for.end498:                                       ; preds = %for.cond.cleanup483
  %456 = load i32, i32* %stss, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %456)
  %457 = bitcast i32* %stts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %457) #4
  %call499 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.48, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call499, i32* %stts, align 4, !tbaa !6
  %458 = bitcast i32* %sttsEntryCountOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %458) #4
  %call500 = call i32 @avifRWStreamOffset(%struct.avifRWStream* %s)
  store i32 %call500, i32* %sttsEntryCountOffset, align 4, !tbaa !6
  %459 = bitcast i32* %sttsEntryCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %459) #4
  store i32 0, i32* %sttsEntryCount, align 4, !tbaa !8
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 0)
  %460 = bitcast i32* %sampleCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %460) #4
  store i32 0, i32* %sampleCount, align 4, !tbaa !8
  %461 = bitcast i32* %frameIndex501 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %461) #4
  store i32 0, i32* %frameIndex501, align 4, !tbaa !8
  br label %for.cond502

for.cond502:                                      ; preds = %for.inc543, %for.end498
  %462 = load i32, i32* %frameIndex501, align 4, !tbaa !8
  %463 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data503 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %463, i32 0, i32 12
  %464 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data503, align 8, !tbaa !28
  %frames504 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %464, i32 0, i32 1
  %count505 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames504, i32 0, i32 2
  %465 = load i32, i32* %count505, align 4, !tbaa !69
  %cmp506 = icmp ult i32 %462, %465
  br i1 %cmp506, label %for.body509, label %for.cond.cleanup508

for.cond.cleanup508:                              ; preds = %for.cond502
  store i32 47, i32* %cleanup.dest.slot, align 4
  %466 = bitcast i32* %frameIndex501 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %466) #4
  %467 = bitcast i32* %sampleCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %467) #4
  br label %for.end547

for.body509:                                      ; preds = %for.cond502
  %468 = bitcast %struct.avifEncoderFrame** %frame510 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %468) #4
  %469 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data511 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %469, i32 0, i32 12
  %470 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data511, align 8, !tbaa !28
  %frames512 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %470, i32 0, i32 1
  %frame513 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames512, i32 0, i32 0
  %471 = load %struct.avifEncoderFrame*, %struct.avifEncoderFrame** %frame513, align 4, !tbaa !100
  %472 = load i32, i32* %frameIndex501, align 4, !tbaa !8
  %arrayidx514 = getelementptr inbounds %struct.avifEncoderFrame, %struct.avifEncoderFrame* %471, i32 %472
  store %struct.avifEncoderFrame* %arrayidx514, %struct.avifEncoderFrame** %frame510, align 4, !tbaa !2
  %473 = load i32, i32* %sampleCount, align 4, !tbaa !8
  %inc515 = add i32 %473, 1
  store i32 %inc515, i32* %sampleCount, align 4, !tbaa !8
  %474 = load i32, i32* %frameIndex501, align 4, !tbaa !8
  %475 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data516 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %475, i32 0, i32 12
  %476 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data516, align 8, !tbaa !28
  %frames517 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %476, i32 0, i32 1
  %count518 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames517, i32 0, i32 2
  %477 = load i32, i32* %count518, align 4, !tbaa !69
  %sub = sub i32 %477, 1
  %cmp519 = icmp ult i32 %474, %sub
  br i1 %cmp519, label %if.then521, label %if.end536

if.then521:                                       ; preds = %for.body509
  %478 = bitcast %struct.avifEncoderFrame** %nextFrame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %478) #4
  %479 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data522 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %479, i32 0, i32 12
  %480 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data522, align 8, !tbaa !28
  %frames523 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %480, i32 0, i32 1
  %frame524 = getelementptr inbounds %struct.avifEncoderFrameArray, %struct.avifEncoderFrameArray* %frames523, i32 0, i32 0
  %481 = load %struct.avifEncoderFrame*, %struct.avifEncoderFrame** %frame524, align 4, !tbaa !100
  %482 = load i32, i32* %frameIndex501, align 4, !tbaa !8
  %add525 = add i32 %482, 1
  %arrayidx526 = getelementptr inbounds %struct.avifEncoderFrame, %struct.avifEncoderFrame* %481, i32 %add525
  store %struct.avifEncoderFrame* %arrayidx526, %struct.avifEncoderFrame** %nextFrame, align 4, !tbaa !2
  %483 = load %struct.avifEncoderFrame*, %struct.avifEncoderFrame** %frame510, align 4, !tbaa !2
  %durationInTimescales527 = getelementptr inbounds %struct.avifEncoderFrame, %struct.avifEncoderFrame* %483, i32 0, i32 0
  %484 = load i64, i64* %durationInTimescales527, align 8, !tbaa !73
  %485 = load %struct.avifEncoderFrame*, %struct.avifEncoderFrame** %nextFrame, align 4, !tbaa !2
  %durationInTimescales528 = getelementptr inbounds %struct.avifEncoderFrame, %struct.avifEncoderFrame* %485, i32 0, i32 0
  %486 = load i64, i64* %durationInTimescales528, align 8, !tbaa !73
  %cmp529 = icmp eq i64 %484, %486
  br i1 %cmp529, label %if.then531, label %if.end532

if.then531:                                       ; preds = %if.then521
  store i32 49, i32* %cleanup.dest.slot, align 4
  br label %cleanup533

if.end532:                                        ; preds = %if.then521
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup533

cleanup533:                                       ; preds = %if.end532, %if.then531
  %487 = bitcast %struct.avifEncoderFrame** %nextFrame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %487) #4
  %cleanup.dest534 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest534, label %cleanup540 [
    i32 0, label %cleanup.cont535
  ]

cleanup.cont535:                                  ; preds = %cleanup533
  br label %if.end536

if.end536:                                        ; preds = %cleanup.cont535, %for.body509
  %488 = load i32, i32* %sampleCount, align 4, !tbaa !8
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %488)
  %489 = load %struct.avifEncoderFrame*, %struct.avifEncoderFrame** %frame510, align 4, !tbaa !2
  %durationInTimescales537 = getelementptr inbounds %struct.avifEncoderFrame, %struct.avifEncoderFrame* %489, i32 0, i32 0
  %490 = load i64, i64* %durationInTimescales537, align 8, !tbaa !73
  %conv538 = trunc i64 %490 to i32
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %conv538)
  store i32 0, i32* %sampleCount, align 4, !tbaa !8
  %491 = load i32, i32* %sttsEntryCount, align 4, !tbaa !8
  %inc539 = add i32 %491, 1
  store i32 %inc539, i32* %sttsEntryCount, align 4, !tbaa !8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup540

cleanup540:                                       ; preds = %if.end536, %cleanup533
  %492 = bitcast %struct.avifEncoderFrame** %frame510 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %492) #4
  %cleanup.dest541 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest541, label %unreachable [
    i32 0, label %cleanup.cont542
    i32 49, label %for.inc543
  ]

cleanup.cont542:                                  ; preds = %cleanup540
  br label %for.inc543

for.inc543:                                       ; preds = %cleanup.cont542, %cleanup540
  %493 = load i32, i32* %frameIndex501, align 4, !tbaa !8
  %inc544 = add i32 %493, 1
  store i32 %inc544, i32* %frameIndex501, align 4, !tbaa !8
  br label %for.cond502

for.end547:                                       ; preds = %for.cond.cleanup508
  %494 = bitcast i32* %prevOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %494) #4
  %call548 = call i32 @avifRWStreamOffset(%struct.avifRWStream* %s)
  store i32 %call548, i32* %prevOffset, align 4, !tbaa !6
  %495 = load i32, i32* %sttsEntryCountOffset, align 4, !tbaa !6
  call void @avifRWStreamSetOffset(%struct.avifRWStream* %s, i32 %495)
  %496 = load i32, i32* %sttsEntryCount, align 4, !tbaa !8
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %496)
  %497 = load i32, i32* %prevOffset, align 4, !tbaa !6
  call void @avifRWStreamSetOffset(%struct.avifRWStream* %s, i32 %497)
  %498 = load i32, i32* %stts, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %498)
  %499 = bitcast i32* %stsd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %499) #4
  %call549 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call549, i32* %stsd, align 4, !tbaa !6
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 1)
  %500 = bitcast i32* %av01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %500) #4
  %call550 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i32 0)
  store i32 %call550, i32* %av01, align 4, !tbaa !6
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 6)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 1)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 0)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 12)
  %501 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %width551 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %501, i32 0, i32 0
  %502 = load i32, i32* %width551, align 4, !tbaa !50
  %conv552 = trunc i32 %502 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %conv552)
  %503 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  %height553 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %503, i32 0, i32 1
  %504 = load i32, i32* %height553, align 4, !tbaa !51
  %conv554 = trunc i32 %504 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext %conv554)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 4718592)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 4718592)
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 0)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 1)
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %s, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.50, i32 0, i32 0), i32 11)
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %s, i32 21)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext 24)
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %s, i16 zeroext -1)
  %505 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %codec555 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %505, i32 0, i32 2
  %506 = load %struct.avifCodec*, %struct.avifCodec** %codec555, align 4, !tbaa !37
  %configBox556 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %506, i32 0, i32 1
  call void @writeConfigBox(%struct.avifRWStream* %s, %struct.avifCodecConfigurationBox* %configBox556)
  %507 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item379, align 4, !tbaa !2
  %alpha557 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %507, i32 0, i32 5
  %508 = load i32, i32* %alpha557, align 4, !tbaa !59
  %tobool558 = icmp ne i32 %508, 0
  br i1 %tobool558, label %if.end560, label %if.then559

if.then559:                                       ; preds = %for.end547
  %509 = load %struct.avifImage*, %struct.avifImage** %imageMetadata, align 4, !tbaa !2
  call void @avifEncoderWriteColorProperties(%struct.avifRWStream* %s, %struct.avifImage* %509, %struct.ipmaArray* null, i8* null)
  br label %if.end560

if.end560:                                        ; preds = %if.then559, %for.end547
  %510 = load i32, i32* %av01, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %510)
  %511 = load i32, i32* %stsd, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %511)
  %512 = load i32, i32* %stbl, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %512)
  %513 = load i32, i32* %minf, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %513)
  %514 = load i32, i32* %mdia, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %514)
  %515 = load i32, i32* %trak, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %515)
  %516 = bitcast i32* %av01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %516) #4
  %517 = bitcast i32* %stsd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %517) #4
  %518 = bitcast i32* %prevOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %518) #4
  %519 = bitcast i32* %sttsEntryCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %519) #4
  %520 = bitcast i32* %sttsEntryCountOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %520) #4
  %521 = bitcast i32* %stts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %521) #4
  %522 = bitcast i32* %stss to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %522) #4
  %523 = bitcast i32* %stsz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %523) #4
  %524 = bitcast i32* %stsc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %524) #4
  %525 = bitcast i32* %stco to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %525) #4
  %526 = bitcast i32* %stbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %526) #4
  %527 = bitcast i32* %dref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %527) #4
  %528 = bitcast i32* %dinf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %528) #4
  %529 = bitcast i32* %vmhd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %529) #4
  %530 = bitcast i32* %minf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %530) #4
  %531 = bitcast i32* %hdlrTrak to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %531) #4
  %532 = bitcast i32* %mdhd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %532) #4
  %533 = bitcast i32* %mdia to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %533) #4
  %534 = bitcast i32* %tkhd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %534) #4
  %535 = bitcast i32* %trak to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %535) #4
  %536 = bitcast i32* %syncSamplesCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %536) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup561

cleanup561:                                       ; preds = %if.end560, %if.then389
  %537 = bitcast %struct.avifEncoderItem** %item379 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %537) #4
  %cleanup.dest562 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest562, label %unreachable [
    i32 0, label %cleanup.cont563
    i32 37, label %for.inc564
  ]

cleanup.cont563:                                  ; preds = %cleanup561
  br label %for.inc564

for.inc564:                                       ; preds = %cleanup.cont563, %cleanup561
  %538 = load i32, i32* %itemIndex370, align 4, !tbaa !8
  %inc565 = add i32 %538, 1
  store i32 %inc565, i32* %itemIndex370, align 4, !tbaa !8
  br label %for.cond371

for.end567:                                       ; preds = %for.cond.cleanup377
  %539 = load i32, i32* %moov, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %539)
  %540 = bitcast i32* %mvhd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %540) #4
  %541 = bitcast i32* %moov to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %541) #4
  %542 = bitcast i64* %durationInTimescales to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %542) #4
  br label %if.end568

if.end568:                                        ; preds = %for.end567, %for.end339
  %543 = bitcast i32* %mdat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %543) #4
  %call569 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %s, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.51, i32 0, i32 0), i32 0)
  store i32 %call569, i32* %mdat, align 4, !tbaa !6
  %544 = bitcast i32* %itemIndex570 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %544) #4
  store i32 0, i32* %itemIndex570, align 4, !tbaa !8
  br label %for.cond571

for.cond571:                                      ; preds = %for.inc649, %if.end568
  %545 = load i32, i32* %itemIndex570, align 4, !tbaa !8
  %546 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data572 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %546, i32 0, i32 12
  %547 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data572, align 8, !tbaa !28
  %items573 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %547, i32 0, i32 0
  %count574 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items573, i32 0, i32 2
  %548 = load i32, i32* %count574, align 4, !tbaa !31
  %cmp575 = icmp ult i32 %545, %548
  br i1 %cmp575, label %for.body578, label %for.cond.cleanup577

for.cond.cleanup577:                              ; preds = %for.cond571
  store i32 50, i32* %cleanup.dest.slot, align 4
  %549 = bitcast i32* %itemIndex570 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %549) #4
  br label %for.end652

for.body578:                                      ; preds = %for.cond571
  %550 = bitcast %struct.avifEncoderItem** %item579 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %550) #4
  %551 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data580 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %551, i32 0, i32 12
  %552 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data580, align 8, !tbaa !28
  %items581 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %552, i32 0, i32 0
  %item582 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items581, i32 0, i32 0
  %553 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item582, align 4, !tbaa !36
  %554 = load i32, i32* %itemIndex570, align 4, !tbaa !8
  %arrayidx583 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %553, i32 %554
  store %struct.avifEncoderItem* %arrayidx583, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %555 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %metadataPayload584 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %555, i32 0, i32 4
  %size585 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %metadataPayload584, i32 0, i32 1
  %556 = load i32, i32* %size585, align 4, !tbaa !98
  %cmp586 = icmp eq i32 %556, 0
  br i1 %cmp586, label %land.lhs.true588, label %if.end595

land.lhs.true588:                                 ; preds = %for.body578
  %557 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %encodeOutput589 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %557, i32 0, i32 3
  %558 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput589, align 4, !tbaa !41
  %samples590 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %558, i32 0, i32 0
  %count591 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples590, i32 0, i32 2
  %559 = load i32, i32* %count591, align 4, !tbaa !13
  %cmp592 = icmp eq i32 %559, 0
  br i1 %cmp592, label %if.then594, label %if.end595

if.then594:                                       ; preds = %land.lhs.true588
  store i32 52, i32* %cleanup.dest.slot, align 4
  br label %cleanup646

if.end595:                                        ; preds = %land.lhs.true588, %for.body578
  %560 = bitcast i32* %chunkOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %560) #4
  %call596 = call i32 @avifRWStreamOffset(%struct.avifRWStream* %s)
  store i32 %call596, i32* %chunkOffset, align 4, !tbaa !8
  %561 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %encodeOutput597 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %561, i32 0, i32 3
  %562 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput597, align 4, !tbaa !41
  %samples598 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %562, i32 0, i32 0
  %count599 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples598, i32 0, i32 2
  %563 = load i32, i32* %count599, align 4, !tbaa !13
  %cmp600 = icmp ugt i32 %563, 0
  br i1 %cmp600, label %if.then602, label %if.else625

if.then602:                                       ; preds = %if.end595
  %564 = bitcast i32* %sampleIndex603 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %564) #4
  store i32 0, i32* %sampleIndex603, align 4, !tbaa !8
  br label %for.cond604

for.cond604:                                      ; preds = %for.inc621, %if.then602
  %565 = load i32, i32* %sampleIndex603, align 4, !tbaa !8
  %566 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %encodeOutput605 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %566, i32 0, i32 3
  %567 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput605, align 4, !tbaa !41
  %samples606 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %567, i32 0, i32 0
  %count607 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples606, i32 0, i32 2
  %568 = load i32, i32* %count607, align 4, !tbaa !13
  %cmp608 = icmp ult i32 %565, %568
  br i1 %cmp608, label %for.body611, label %for.cond.cleanup610

for.cond.cleanup610:                              ; preds = %for.cond604
  store i32 53, i32* %cleanup.dest.slot, align 4
  %569 = bitcast i32* %sampleIndex603 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %569) #4
  br label %for.end624

for.body611:                                      ; preds = %for.cond604
  %570 = bitcast %struct.avifEncodeSample** %sample612 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %570) #4
  %571 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %encodeOutput613 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %571, i32 0, i32 3
  %572 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %encodeOutput613, align 4, !tbaa !41
  %samples614 = getelementptr inbounds %struct.avifCodecEncodeOutput, %struct.avifCodecEncodeOutput* %572, i32 0, i32 0
  %sample615 = getelementptr inbounds %struct.avifEncodeSampleArray, %struct.avifEncodeSampleArray* %samples614, i32 0, i32 0
  %573 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample615, align 4, !tbaa !16
  %574 = load i32, i32* %sampleIndex603, align 4, !tbaa !8
  %arrayidx616 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %573, i32 %574
  store %struct.avifEncodeSample* %arrayidx616, %struct.avifEncodeSample** %sample612, align 4, !tbaa !2
  %575 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample612, align 4, !tbaa !2
  %data617 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %575, i32 0, i32 0
  %data618 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %data617, i32 0, i32 0
  %576 = load i8*, i8** %data618, align 4, !tbaa !101
  %577 = load %struct.avifEncodeSample*, %struct.avifEncodeSample** %sample612, align 4, !tbaa !2
  %data619 = getelementptr inbounds %struct.avifEncodeSample, %struct.avifEncodeSample* %577, i32 0, i32 0
  %size620 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %data619, i32 0, i32 1
  %578 = load i32, i32* %size620, align 4, !tbaa !95
  call void @avifRWStreamWrite(%struct.avifRWStream* %s, i8* %576, i32 %578)
  %579 = bitcast %struct.avifEncodeSample** %sample612 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %579) #4
  br label %for.inc621

for.inc621:                                       ; preds = %for.body611
  %580 = load i32, i32* %sampleIndex603, align 4, !tbaa !8
  %inc622 = add i32 %580, 1
  store i32 %inc622, i32* %sampleIndex603, align 4, !tbaa !8
  br label %for.cond604

for.end624:                                       ; preds = %for.cond.cleanup610
  br label %if.end630

if.else625:                                       ; preds = %if.end595
  %581 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %metadataPayload626 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %581, i32 0, i32 4
  %data627 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %metadataPayload626, i32 0, i32 0
  %582 = load i8*, i8** %data627, align 4, !tbaa !64
  %583 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %metadataPayload628 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %583, i32 0, i32 4
  %size629 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %metadataPayload628, i32 0, i32 1
  %584 = load i32, i32* %size629, align 4, !tbaa !98
  call void @avifRWStreamWrite(%struct.avifRWStream* %s, i8* %582, i32 %584)
  br label %if.end630

if.end630:                                        ; preds = %if.else625, %for.end624
  %585 = bitcast i32* %fixupIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %585) #4
  store i32 0, i32* %fixupIndex, align 4, !tbaa !8
  br label %for.cond631

for.cond631:                                      ; preds = %for.inc642, %if.end630
  %586 = load i32, i32* %fixupIndex, align 4, !tbaa !8
  %587 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %mdatFixups = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %587, i32 0, i32 10
  %count632 = getelementptr inbounds %struct.avifOffsetFixupArray, %struct.avifOffsetFixupArray* %mdatFixups, i32 0, i32 2
  %588 = load i32, i32* %count632, align 4, !tbaa !102
  %cmp633 = icmp ult i32 %586, %588
  br i1 %cmp633, label %for.body636, label %for.cond.cleanup635

for.cond.cleanup635:                              ; preds = %for.cond631
  store i32 56, i32* %cleanup.dest.slot, align 4
  %589 = bitcast i32* %fixupIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %589) #4
  br label %for.end645

for.body636:                                      ; preds = %for.cond631
  %590 = bitcast %struct.avifOffsetFixup** %fixup to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %590) #4
  %591 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item579, align 4, !tbaa !2
  %mdatFixups637 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %591, i32 0, i32 10
  %fixup638 = getelementptr inbounds %struct.avifOffsetFixupArray, %struct.avifOffsetFixupArray* %mdatFixups637, i32 0, i32 0
  %592 = load %struct.avifOffsetFixup*, %struct.avifOffsetFixup** %fixup638, align 4, !tbaa !103
  %593 = load i32, i32* %fixupIndex, align 4, !tbaa !8
  %arrayidx639 = getelementptr inbounds %struct.avifOffsetFixup, %struct.avifOffsetFixup* %592, i32 %593
  store %struct.avifOffsetFixup* %arrayidx639, %struct.avifOffsetFixup** %fixup, align 4, !tbaa !2
  %594 = bitcast i32* %prevOffset640 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %594) #4
  %call641 = call i32 @avifRWStreamOffset(%struct.avifRWStream* %s)
  store i32 %call641, i32* %prevOffset640, align 4, !tbaa !6
  %595 = load %struct.avifOffsetFixup*, %struct.avifOffsetFixup** %fixup, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifOffsetFixup, %struct.avifOffsetFixup* %595, i32 0, i32 0
  %596 = load i32, i32* %offset, align 4, !tbaa !104
  call void @avifRWStreamSetOffset(%struct.avifRWStream* %s, i32 %596)
  %597 = load i32, i32* %chunkOffset, align 4, !tbaa !8
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %s, i32 %597)
  %598 = load i32, i32* %prevOffset640, align 4, !tbaa !6
  call void @avifRWStreamSetOffset(%struct.avifRWStream* %s, i32 %598)
  %599 = bitcast i32* %prevOffset640 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %599) #4
  %600 = bitcast %struct.avifOffsetFixup** %fixup to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %600) #4
  br label %for.inc642

for.inc642:                                       ; preds = %for.body636
  %601 = load i32, i32* %fixupIndex, align 4, !tbaa !8
  %inc643 = add i32 %601, 1
  store i32 %inc643, i32* %fixupIndex, align 4, !tbaa !8
  br label %for.cond631

for.end645:                                       ; preds = %for.cond.cleanup635
  %602 = bitcast i32* %chunkOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %602) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup646

cleanup646:                                       ; preds = %for.end645, %if.then594
  %603 = bitcast %struct.avifEncoderItem** %item579 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %603) #4
  %cleanup.dest647 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest647, label %unreachable [
    i32 0, label %cleanup.cont648
    i32 52, label %for.inc649
  ]

cleanup.cont648:                                  ; preds = %cleanup646
  br label %for.inc649

for.inc649:                                       ; preds = %cleanup.cont648, %cleanup646
  %604 = load i32, i32* %itemIndex570, align 4, !tbaa !8
  %inc650 = add i32 %604, 1
  store i32 %inc650, i32* %itemIndex570, align 4, !tbaa !8
  br label %for.cond571

for.end652:                                       ; preds = %for.cond.cleanup577
  %605 = load i32, i32* %mdat, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %s, i32 %605)
  call void @avifRWStreamFinishWrite(%struct.avifRWStream* %s)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %606 = bitcast i32* %mdat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %606) #4
  %607 = bitcast i32* %ipma260 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %607) #4
  %608 = bitcast i32* %ipco to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %608) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %itemPropertyIndex) #4
  %609 = bitcast i32* %iprp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %609) #4
  %610 = bitcast i32* %iref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %610) #4
  %611 = bitcast i32* %iinf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %611) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %offsetSizeAndLengthSize) #4
  %612 = bitcast i32* %iloc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %612) #4
  %613 = bitcast i32* %hdlr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %613) #4
  %614 = bitcast i32* %meta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %614) #4
  %615 = bitcast i32* %ftyp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %615) #4
  %616 = bitcast i8** %majorBrand to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %616) #4
  %617 = bitcast %struct.avifRWStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %617) #4
  %618 = bitcast i64* %now to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %618) #4
  %619 = bitcast %struct.avifImage** %imageMetadata to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %619) #4
  br label %return

return:                                           ; preds = %for.end652, %cleanup44, %if.then
  %620 = load i32, i32* %retval, align 4
  ret i32 %620

unreachable:                                      ; preds = %cleanup646, %cleanup561, %cleanup540, %cleanup333, %cleanup253, %cleanup44
  unreachable
}

declare i32 @time(i32*) #2

declare void @avifRWStreamStart(%struct.avifRWStream*, %struct.avifRWData*) #2

declare i32 @avifRWStreamWriteBox(%struct.avifRWStream*, i8*, i32) #2

declare void @avifRWStreamWriteChars(%struct.avifRWStream*, i8*, i32) #2

declare void @avifRWStreamWriteU32(%struct.avifRWStream*, i32) #2

declare void @avifRWStreamFinishBox(%struct.avifRWStream*, i32) #2

declare i32 @avifRWStreamWriteFullBox(%struct.avifRWStream*, i8*, i32, i32, i32) #2

declare void @avifRWStreamWriteZeros(%struct.avifRWStream*, i32) #2

declare void @avifRWStreamWriteU16(%struct.avifRWStream*, i16 zeroext) #2

declare void @avifRWStreamWrite(%struct.avifRWStream*, i8*, i32) #2

; Function Attrs: nounwind
define internal void @avifEncoderItemAddMdatFixup(%struct.avifEncoderItem* %item, %struct.avifRWStream* %s) #0 {
entry:
  %item.addr = alloca %struct.avifEncoderItem*, align 4
  %s.addr = alloca %struct.avifRWStream*, align 4
  %fixup = alloca %struct.avifOffsetFixup*, align 4
  store %struct.avifEncoderItem* %item, %struct.avifEncoderItem** %item.addr, align 4, !tbaa !2
  store %struct.avifRWStream* %s, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifOffsetFixup** %fixup to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item.addr, align 4, !tbaa !2
  %mdatFixups = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %1, i32 0, i32 10
  %2 = bitcast %struct.avifOffsetFixupArray* %mdatFixups to i8*
  %call = call i8* @avifArrayPushPtr(i8* %2)
  %3 = bitcast i8* %call to %struct.avifOffsetFixup*
  store %struct.avifOffsetFixup* %3, %struct.avifOffsetFixup** %fixup, align 4, !tbaa !2
  %4 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call1 = call i32 @avifRWStreamOffset(%struct.avifRWStream* %4)
  %5 = load %struct.avifOffsetFixup*, %struct.avifOffsetFixup** %fixup, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifOffsetFixup, %struct.avifOffsetFixup* %5, i32 0, i32 0
  store i32 %call1, i32* %offset, align 4, !tbaa !104
  %6 = bitcast %struct.avifOffsetFixup** %fixup to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  ret void
}

; Function Attrs: nounwind
define internal void @ipmaPush(%struct.ipmaArray* %ipma, i8 zeroext %assoc, i32 %essential) #0 {
entry:
  %ipma.addr = alloca %struct.ipmaArray*, align 4
  %assoc.addr = alloca i8, align 1
  %essential.addr = alloca i32, align 4
  store %struct.ipmaArray* %ipma, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  store i8 %assoc, i8* %assoc.addr, align 1, !tbaa !80
  store i32 %essential, i32* %essential.addr, align 4, !tbaa !8
  %0 = load i8, i8* %assoc.addr, align 1, !tbaa !80
  %1 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %associations = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %1, i32 0, i32 0
  %2 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %count = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %2, i32 0, i32 2
  %3 = load i8, i8* %count, align 4, !tbaa !106
  %idxprom = zext i8 %3 to i32
  %arrayidx = getelementptr inbounds [16 x i8], [16 x i8]* %associations, i32 0, i32 %idxprom
  store i8 %0, i8* %arrayidx, align 1, !tbaa !80
  %4 = load i32, i32* %essential.addr, align 4, !tbaa !8
  %5 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %essential1 = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %5, i32 0, i32 1
  %6 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %count2 = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %6, i32 0, i32 2
  %7 = load i8, i8* %count2, align 4, !tbaa !106
  %idxprom3 = zext i8 %7 to i32
  %arrayidx4 = getelementptr inbounds [16 x i32], [16 x i32]* %essential1, i32 0, i32 %idxprom3
  store i32 %4, i32* %arrayidx4, align 4, !tbaa !8
  %8 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %count5 = getelementptr inbounds %struct.ipmaArray, %struct.ipmaArray* %8, i32 0, i32 2
  %9 = load i8, i8* %count5, align 4, !tbaa !106
  %inc = add i8 %9, 1
  store i8 %inc, i8* %count5, align 4, !tbaa !106
  ret void
}

declare void @avifRWStreamWriteU8(%struct.avifRWStream*, i8 zeroext) #2

; Function Attrs: nounwind
define internal void @writeConfigBox(%struct.avifRWStream* %s, %struct.avifCodecConfigurationBox* %cfg) #0 {
entry:
  %s.addr = alloca %struct.avifRWStream*, align 4
  %cfg.addr = alloca %struct.avifCodecConfigurationBox*, align 4
  %av1C = alloca i32, align 4
  %bits = alloca i8, align 1
  store %struct.avifRWStream* %s, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  store %struct.avifCodecConfigurationBox* %cfg, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %0 = bitcast i32* %av1C to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %1, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.59, i32 0, i32 0), i32 0)
  store i32 %call, i32* %av1C, align 4, !tbaa !6
  %2 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %2, i8 zeroext -127)
  %3 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %4 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %seqProfile = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %4, i32 0, i32 0
  %5 = load i8, i8* %seqProfile, align 1, !tbaa !107
  %conv = zext i8 %5 to i32
  %and = and i32 %conv, 7
  %shl = shl i32 %and, 5
  %conv1 = trunc i32 %shl to i8
  %conv2 = zext i8 %conv1 to i32
  %6 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %seqLevelIdx0 = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %6, i32 0, i32 1
  %7 = load i8, i8* %seqLevelIdx0, align 1, !tbaa !108
  %conv3 = zext i8 %7 to i32
  %and4 = and i32 %conv3, 31
  %conv5 = trunc i32 %and4 to i8
  %conv6 = zext i8 %conv5 to i32
  %or = or i32 %conv2, %conv6
  %conv7 = trunc i32 %or to i8
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %3, i8 zeroext %conv7)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %bits) #4
  store i8 0, i8* %bits, align 1, !tbaa !80
  %8 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %seqTier0 = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %8, i32 0, i32 2
  %9 = load i8, i8* %seqTier0, align 1, !tbaa !109
  %conv8 = zext i8 %9 to i32
  %and9 = and i32 %conv8, 1
  %shl10 = shl i32 %and9, 7
  %10 = load i8, i8* %bits, align 1, !tbaa !80
  %conv11 = zext i8 %10 to i32
  %or12 = or i32 %conv11, %shl10
  %conv13 = trunc i32 %or12 to i8
  store i8 %conv13, i8* %bits, align 1, !tbaa !80
  %11 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %highBitdepth = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %11, i32 0, i32 3
  %12 = load i8, i8* %highBitdepth, align 1, !tbaa !110
  %conv14 = zext i8 %12 to i32
  %and15 = and i32 %conv14, 1
  %shl16 = shl i32 %and15, 6
  %13 = load i8, i8* %bits, align 1, !tbaa !80
  %conv17 = zext i8 %13 to i32
  %or18 = or i32 %conv17, %shl16
  %conv19 = trunc i32 %or18 to i8
  store i8 %conv19, i8* %bits, align 1, !tbaa !80
  %14 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %twelveBit = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %14, i32 0, i32 4
  %15 = load i8, i8* %twelveBit, align 1, !tbaa !111
  %conv20 = zext i8 %15 to i32
  %and21 = and i32 %conv20, 1
  %shl22 = shl i32 %and21, 5
  %16 = load i8, i8* %bits, align 1, !tbaa !80
  %conv23 = zext i8 %16 to i32
  %or24 = or i32 %conv23, %shl22
  %conv25 = trunc i32 %or24 to i8
  store i8 %conv25, i8* %bits, align 1, !tbaa !80
  %17 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %monochrome = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %17, i32 0, i32 5
  %18 = load i8, i8* %monochrome, align 1, !tbaa !112
  %conv26 = zext i8 %18 to i32
  %and27 = and i32 %conv26, 1
  %shl28 = shl i32 %and27, 4
  %19 = load i8, i8* %bits, align 1, !tbaa !80
  %conv29 = zext i8 %19 to i32
  %or30 = or i32 %conv29, %shl28
  %conv31 = trunc i32 %or30 to i8
  store i8 %conv31, i8* %bits, align 1, !tbaa !80
  %20 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %chromaSubsamplingX = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %20, i32 0, i32 6
  %21 = load i8, i8* %chromaSubsamplingX, align 1, !tbaa !113
  %conv32 = zext i8 %21 to i32
  %and33 = and i32 %conv32, 1
  %shl34 = shl i32 %and33, 3
  %22 = load i8, i8* %bits, align 1, !tbaa !80
  %conv35 = zext i8 %22 to i32
  %or36 = or i32 %conv35, %shl34
  %conv37 = trunc i32 %or36 to i8
  store i8 %conv37, i8* %bits, align 1, !tbaa !80
  %23 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %chromaSubsamplingY = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %23, i32 0, i32 7
  %24 = load i8, i8* %chromaSubsamplingY, align 1, !tbaa !114
  %conv38 = zext i8 %24 to i32
  %and39 = and i32 %conv38, 1
  %shl40 = shl i32 %and39, 2
  %25 = load i8, i8* %bits, align 1, !tbaa !80
  %conv41 = zext i8 %25 to i32
  %or42 = or i32 %conv41, %shl40
  %conv43 = trunc i32 %or42 to i8
  store i8 %conv43, i8* %bits, align 1, !tbaa !80
  %26 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %cfg.addr, align 4, !tbaa !2
  %chromaSamplePosition = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %26, i32 0, i32 8
  %27 = load i8, i8* %chromaSamplePosition, align 1, !tbaa !115
  %conv44 = zext i8 %27 to i32
  %and45 = and i32 %conv44, 3
  %28 = load i8, i8* %bits, align 1, !tbaa !80
  %conv46 = zext i8 %28 to i32
  %or47 = or i32 %conv46, %and45
  %conv48 = trunc i32 %or47 to i8
  store i8 %conv48, i8* %bits, align 1, !tbaa !80
  %29 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %30 = load i8, i8* %bits, align 1, !tbaa !80
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %29, i8 zeroext %30)
  %31 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %31, i8 zeroext 0)
  %32 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %33 = load i32, i32* %av1C, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %32, i32 %33)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %bits) #4
  %34 = bitcast i32* %av1C to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #4
  ret void
}

; Function Attrs: nounwind
define internal void @avifEncoderWriteColorProperties(%struct.avifRWStream* %s, %struct.avifImage* %imageMetadata, %struct.ipmaArray* %ipma, i8* %itemPropertyIndex) #0 {
entry:
  %s.addr = alloca %struct.avifRWStream*, align 4
  %imageMetadata.addr = alloca %struct.avifImage*, align 4
  %ipma.addr = alloca %struct.ipmaArray*, align 4
  %itemPropertyIndex.addr = alloca i8*, align 4
  %colr = alloca i32, align 4
  %colr6 = alloca i32, align 4
  %pasp = alloca i32, align 4
  %clap = alloca i32, align 4
  %irot = alloca i32, align 4
  %angle = alloca i8, align 1
  %imir = alloca i32, align 4
  %axis = alloca i8, align 1
  store %struct.avifRWStream* %s, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  store %struct.avifImage* %imageMetadata, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  store %struct.ipmaArray* %ipma, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  store i8* %itemPropertyIndex, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %0 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %icc = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 13
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %icc, i32 0, i32 1
  %1 = load i32, i32* %size, align 4, !tbaa !116
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %colr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %3, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.52, i32 0, i32 0), i32 0)
  store i32 %call, i32* %colr, align 4, !tbaa !6
  %4 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %4, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.53, i32 0, i32 0), i32 4)
  %5 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %6 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %icc1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %6, i32 0, i32 13
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %icc1, i32 0, i32 0
  %7 = load i8*, i8** %data, align 4, !tbaa !117
  %8 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %icc2 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %8, i32 0, i32 13
  %size3 = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %icc2, i32 0, i32 1
  %9 = load i32, i32* %size3, align 4, !tbaa !116
  call void @avifRWStreamWrite(%struct.avifRWStream* %5, i8* %7, i32 %9)
  %10 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %11 = load i32, i32* %colr, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %10, i32 %11)
  %12 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.ipmaArray* %12, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %13 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %tobool4 = icmp ne i8* %13, null
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %land.lhs.true
  %14 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %15 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %16 = load i8, i8* %15, align 1, !tbaa !80
  %inc = add i8 %16, 1
  store i8 %inc, i8* %15, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %14, i8 zeroext %inc, i32 0)
  br label %if.end

if.end:                                           ; preds = %if.then5, %land.lhs.true, %if.then
  %17 = bitcast i32* %colr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %if.end19

if.else:                                          ; preds = %entry
  %18 = bitcast i32* %colr6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call7 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %19, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.52, i32 0, i32 0), i32 0)
  store i32 %call7, i32* %colr6, align 4, !tbaa !6
  %20 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %20, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.54, i32 0, i32 0), i32 4)
  %21 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %22 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %colorPrimaries = getelementptr inbounds %struct.avifImage, %struct.avifImage* %22, i32 0, i32 14
  %23 = load i32, i32* %colorPrimaries, align 4, !tbaa !118
  %conv = trunc i32 %23 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %21, i16 zeroext %conv)
  %24 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %25 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %transferCharacteristics = getelementptr inbounds %struct.avifImage, %struct.avifImage* %25, i32 0, i32 15
  %26 = load i32, i32* %transferCharacteristics, align 4, !tbaa !119
  %conv8 = trunc i32 %26 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %24, i16 zeroext %conv8)
  %27 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %28 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %28, i32 0, i32 16
  %29 = load i32, i32* %matrixCoefficients, align 4, !tbaa !120
  %conv9 = trunc i32 %29 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %27, i16 zeroext %conv9)
  %30 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %31 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %31, i32 0, i32 4
  %32 = load i32, i32* %yuvRange, align 4, !tbaa !121
  %cmp10 = icmp eq i32 %32, 1
  %33 = zext i1 %cmp10 to i64
  %cond = select i1 %cmp10, i32 128, i32 0
  %conv12 = trunc i32 %cond to i8
  call void @avifRWStreamWriteU8(%struct.avifRWStream* %30, i8 zeroext %conv12)
  %34 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %35 = load i32, i32* %colr6, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %34, i32 %35)
  %36 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %tobool13 = icmp ne %struct.ipmaArray* %36, null
  br i1 %tobool13, label %land.lhs.true14, label %if.end18

land.lhs.true14:                                  ; preds = %if.else
  %37 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %tobool15 = icmp ne i8* %37, null
  br i1 %tobool15, label %if.then16, label %if.end18

if.then16:                                        ; preds = %land.lhs.true14
  %38 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %39 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %40 = load i8, i8* %39, align 1, !tbaa !80
  %inc17 = add i8 %40, 1
  store i8 %inc17, i8* %39, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %38, i8 zeroext %inc17, i32 0)
  br label %if.end18

if.end18:                                         ; preds = %if.then16, %land.lhs.true14, %if.else
  %41 = bitcast i32* %colr6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.end
  %42 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %transformFlags = getelementptr inbounds %struct.avifImage, %struct.avifImage* %42, i32 0, i32 17
  %43 = load i32, i32* %transformFlags, align 4, !tbaa !122
  %and = and i32 %43, 1
  %tobool20 = icmp ne i32 %and, 0
  br i1 %tobool20, label %if.then21, label %if.end31

if.then21:                                        ; preds = %if.end19
  %44 = bitcast i32* %pasp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #4
  %45 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call22 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %45, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.55, i32 0, i32 0), i32 0)
  store i32 %call22, i32* %pasp, align 4, !tbaa !6
  %46 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %47 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %pasp23 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %47, i32 0, i32 18
  %hSpacing = getelementptr inbounds %struct.avifPixelAspectRatioBox, %struct.avifPixelAspectRatioBox* %pasp23, i32 0, i32 0
  %48 = load i32, i32* %hSpacing, align 4, !tbaa !123
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %46, i32 %48)
  %49 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %50 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %pasp24 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %50, i32 0, i32 18
  %vSpacing = getelementptr inbounds %struct.avifPixelAspectRatioBox, %struct.avifPixelAspectRatioBox* %pasp24, i32 0, i32 1
  %51 = load i32, i32* %vSpacing, align 4, !tbaa !124
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %49, i32 %51)
  %52 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %53 = load i32, i32* %pasp, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %52, i32 %53)
  %54 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %tobool25 = icmp ne %struct.ipmaArray* %54, null
  br i1 %tobool25, label %land.lhs.true26, label %if.end30

land.lhs.true26:                                  ; preds = %if.then21
  %55 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %tobool27 = icmp ne i8* %55, null
  br i1 %tobool27, label %if.then28, label %if.end30

if.then28:                                        ; preds = %land.lhs.true26
  %56 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %57 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !80
  %inc29 = add i8 %58, 1
  store i8 %inc29, i8* %57, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %56, i8 zeroext %inc29, i32 0)
  br label %if.end30

if.end30:                                         ; preds = %if.then28, %land.lhs.true26, %if.then21
  %59 = bitcast i32* %pasp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.end19
  %60 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %transformFlags32 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %60, i32 0, i32 17
  %61 = load i32, i32* %transformFlags32, align 4, !tbaa !122
  %and33 = and i32 %61, 2
  %tobool34 = icmp ne i32 %and33, 0
  br i1 %tobool34, label %if.then35, label %if.end51

if.then35:                                        ; preds = %if.end31
  %62 = bitcast i32* %clap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #4
  %63 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call36 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %63, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.56, i32 0, i32 0), i32 0)
  store i32 %call36, i32* %clap, align 4, !tbaa !6
  %64 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %65 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %clap37 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %65, i32 0, i32 19
  %widthN = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %clap37, i32 0, i32 0
  %66 = load i32, i32* %widthN, align 4, !tbaa !125
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %64, i32 %66)
  %67 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %68 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %clap38 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %68, i32 0, i32 19
  %widthD = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %clap38, i32 0, i32 1
  %69 = load i32, i32* %widthD, align 4, !tbaa !126
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %67, i32 %69)
  %70 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %71 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %clap39 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %71, i32 0, i32 19
  %heightN = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %clap39, i32 0, i32 2
  %72 = load i32, i32* %heightN, align 4, !tbaa !127
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %70, i32 %72)
  %73 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %74 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %clap40 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %74, i32 0, i32 19
  %heightD = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %clap40, i32 0, i32 3
  %75 = load i32, i32* %heightD, align 4, !tbaa !128
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %73, i32 %75)
  %76 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %77 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %clap41 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %77, i32 0, i32 19
  %horizOffN = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %clap41, i32 0, i32 4
  %78 = load i32, i32* %horizOffN, align 4, !tbaa !129
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %76, i32 %78)
  %79 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %80 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %clap42 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %80, i32 0, i32 19
  %horizOffD = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %clap42, i32 0, i32 5
  %81 = load i32, i32* %horizOffD, align 4, !tbaa !130
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %79, i32 %81)
  %82 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %83 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %clap43 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %83, i32 0, i32 19
  %vertOffN = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %clap43, i32 0, i32 6
  %84 = load i32, i32* %vertOffN, align 4, !tbaa !131
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %82, i32 %84)
  %85 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %86 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %clap44 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %86, i32 0, i32 19
  %vertOffD = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %clap44, i32 0, i32 7
  %87 = load i32, i32* %vertOffD, align 4, !tbaa !132
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %85, i32 %87)
  %88 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %89 = load i32, i32* %clap, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %88, i32 %89)
  %90 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %tobool45 = icmp ne %struct.ipmaArray* %90, null
  br i1 %tobool45, label %land.lhs.true46, label %if.end50

land.lhs.true46:                                  ; preds = %if.then35
  %91 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %tobool47 = icmp ne i8* %91, null
  br i1 %tobool47, label %if.then48, label %if.end50

if.then48:                                        ; preds = %land.lhs.true46
  %92 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %93 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %94 = load i8, i8* %93, align 1, !tbaa !80
  %inc49 = add i8 %94, 1
  store i8 %inc49, i8* %93, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %92, i8 zeroext %inc49, i32 1)
  br label %if.end50

if.end50:                                         ; preds = %if.then48, %land.lhs.true46, %if.then35
  %95 = bitcast i32* %clap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.end31
  %96 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %transformFlags52 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %96, i32 0, i32 17
  %97 = load i32, i32* %transformFlags52, align 4, !tbaa !122
  %and53 = and i32 %97, 4
  %tobool54 = icmp ne i32 %and53, 0
  br i1 %tobool54, label %if.then55, label %if.end68

if.then55:                                        ; preds = %if.end51
  %98 = bitcast i32* %irot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #4
  %99 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call56 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %99, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.57, i32 0, i32 0), i32 0)
  store i32 %call56, i32* %irot, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %angle) #4
  %100 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %irot57 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %100, i32 0, i32 20
  %angle58 = getelementptr inbounds %struct.avifImageRotation, %struct.avifImageRotation* %irot57, i32 0, i32 0
  %101 = load i8, i8* %angle58, align 4, !tbaa !133
  %conv59 = zext i8 %101 to i32
  %and60 = and i32 %conv59, 3
  %conv61 = trunc i32 %and60 to i8
  store i8 %conv61, i8* %angle, align 1, !tbaa !80
  %102 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWrite(%struct.avifRWStream* %102, i8* %angle, i32 1)
  %103 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %104 = load i32, i32* %irot, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %103, i32 %104)
  %105 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %tobool62 = icmp ne %struct.ipmaArray* %105, null
  br i1 %tobool62, label %land.lhs.true63, label %if.end67

land.lhs.true63:                                  ; preds = %if.then55
  %106 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %tobool64 = icmp ne i8* %106, null
  br i1 %tobool64, label %if.then65, label %if.end67

if.then65:                                        ; preds = %land.lhs.true63
  %107 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %108 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %109 = load i8, i8* %108, align 1, !tbaa !80
  %inc66 = add i8 %109, 1
  store i8 %inc66, i8* %108, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %107, i8 zeroext %inc66, i32 1)
  br label %if.end67

if.end67:                                         ; preds = %if.then65, %land.lhs.true63, %if.then55
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %angle) #4
  %110 = bitcast i32* %irot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %if.end51
  %111 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %transformFlags69 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %111, i32 0, i32 17
  %112 = load i32, i32* %transformFlags69, align 4, !tbaa !122
  %and70 = and i32 %112, 8
  %tobool71 = icmp ne i32 %and70, 0
  br i1 %tobool71, label %if.then72, label %if.end85

if.then72:                                        ; preds = %if.end68
  %113 = bitcast i32* %imir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #4
  %114 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call73 = call i32 @avifRWStreamWriteBox(%struct.avifRWStream* %114, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.58, i32 0, i32 0), i32 0)
  store i32 %call73, i32* %imir, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %axis) #4
  %115 = load %struct.avifImage*, %struct.avifImage** %imageMetadata.addr, align 4, !tbaa !2
  %imir74 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %115, i32 0, i32 21
  %axis75 = getelementptr inbounds %struct.avifImageMirror, %struct.avifImageMirror* %imir74, i32 0, i32 0
  %116 = load i8, i8* %axis75, align 1, !tbaa !134
  %conv76 = zext i8 %116 to i32
  %and77 = and i32 %conv76, 1
  %conv78 = trunc i32 %and77 to i8
  store i8 %conv78, i8* %axis, align 1, !tbaa !80
  %117 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWrite(%struct.avifRWStream* %117, i8* %axis, i32 1)
  %118 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %119 = load i32, i32* %imir, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %118, i32 %119)
  %120 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %tobool79 = icmp ne %struct.ipmaArray* %120, null
  br i1 %tobool79, label %land.lhs.true80, label %if.end84

land.lhs.true80:                                  ; preds = %if.then72
  %121 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %tobool81 = icmp ne i8* %121, null
  br i1 %tobool81, label %if.then82, label %if.end84

if.then82:                                        ; preds = %land.lhs.true80
  %122 = load %struct.ipmaArray*, %struct.ipmaArray** %ipma.addr, align 4, !tbaa !2
  %123 = load i8*, i8** %itemPropertyIndex.addr, align 4, !tbaa !2
  %124 = load i8, i8* %123, align 1, !tbaa !80
  %inc83 = add i8 %124, 1
  store i8 %inc83, i8* %123, align 1, !tbaa !80
  call void @ipmaPush(%struct.ipmaArray* %122, i8 zeroext %inc83, i32 1)
  br label %if.end84

if.end84:                                         ; preds = %if.then82, %land.lhs.true80, %if.then72
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %axis) #4
  %125 = bitcast i32* %imir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #4
  br label %if.end85

if.end85:                                         ; preds = %if.end84, %if.end68
  ret void
}

declare void @avifRWStreamWriteU64(%struct.avifRWStream*, i64) #2

; Function Attrs: nounwind
define internal void @avifEncoderWriteTrackMetaBox(%struct.avifEncoder* %encoder, %struct.avifRWStream* %s) #0 {
entry:
  %encoder.addr = alloca %struct.avifEncoder*, align 4
  %s.addr = alloca %struct.avifRWStream*, align 4
  %metadataItemCount = alloca i32, align 4
  %itemIndex = alloca i32, align 4
  %item = alloca %struct.avifEncoderItem*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %meta = alloca i32, align 4
  %hdlr = alloca i32, align 4
  %iloc = alloca i32, align 4
  %offsetSizeAndLengthSize = alloca i8, align 1
  %trakItemIndex = alloca i32, align 4
  %item20 = alloca %struct.avifEncoderItem*, align 4
  %iinf = alloca i32, align 4
  %trakItemIndex38 = alloca i32, align 4
  %item47 = alloca %struct.avifEncoderItem*, align 4
  %infe = alloca i32, align 4
  store %struct.avifEncoder* %encoder, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  store %struct.avifRWStream* %s, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %0 = bitcast i32* %metadataItemCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %metadataItemCount, align 4, !tbaa !8
  %1 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %itemIndex, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %3 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %3, i32 0, i32 12
  %4 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 8, !tbaa !28
  %items = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %4, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items, i32 0, i32 2
  %5 = load i32, i32* %count, align 4, !tbaa !31
  %cmp = icmp ult i32 %2, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %8, i32 0, i32 12
  %9 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data1, align 8, !tbaa !28
  %items2 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %9, i32 0, i32 0
  %item3 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items2, i32 0, i32 0
  %10 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item3, align 4, !tbaa !36
  %11 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %10, i32 %11
  store %struct.avifEncoderItem* %arrayidx, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %12 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %12, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i32 4)
  %cmp4 = icmp ne i32 %call, 0
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %13 = load i32, i32* %metadataItemCount, align 4, !tbaa !8
  %inc = add i32 %13, 1
  store i32 %inc, i32* %metadataItemCount, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %14 = bitcast %struct.avifEncoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %15 = load i32, i32* %itemIndex, align 4, !tbaa !8
  %inc5 = add i32 %15, 1
  store i32 %inc5, i32* %itemIndex, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %16 = load i32, i32* %metadataItemCount, align 4, !tbaa !8
  %cmp6 = icmp eq i32 %16, 0
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %for.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup75

if.end8:                                          ; preds = %for.end
  %17 = bitcast i32* %meta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call9 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %18, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.16, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call9, i32* %meta, align 4, !tbaa !6
  %19 = bitcast i32* %hdlr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call10 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %20, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.17, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call10, i32* %hdlr, align 4, !tbaa !6
  %21 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %21, i32 0)
  %22 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %22, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.18, i32 0, i32 0), i32 4)
  %23 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %23, i32 12)
  %24 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %24, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.19, i32 0, i32 0), i32 8)
  %25 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %26 = load i32, i32* %hdlr, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %25, i32 %26)
  %27 = bitcast i32* %iloc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call11 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %28, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.21, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call11, i32* %iloc, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %offsetSizeAndLengthSize) #4
  store i8 68, i8* %offsetSizeAndLengthSize, align 1, !tbaa !80
  %29 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWrite(%struct.avifRWStream* %29, i8* %offsetSizeAndLengthSize, i32 1)
  %30 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteZeros(%struct.avifRWStream* %30, i32 1)
  %31 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %32 = load i32, i32* %metadataItemCount, align 4, !tbaa !8
  %conv = trunc i32 %32 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %31, i16 zeroext %conv)
  %33 = bitcast i32* %trakItemIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  store i32 0, i32* %trakItemIndex, align 4, !tbaa !8
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc32, %if.end8
  %34 = load i32, i32* %trakItemIndex, align 4, !tbaa !8
  %35 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data13 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %35, i32 0, i32 12
  %36 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data13, align 8, !tbaa !28
  %items14 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %36, i32 0, i32 0
  %count15 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items14, i32 0, i32 2
  %37 = load i32, i32* %count15, align 4, !tbaa !31
  %cmp16 = icmp ult i32 %34, %37
  br i1 %cmp16, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond12
  store i32 5, i32* %cleanup.dest.slot, align 4
  %38 = bitcast i32* %trakItemIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  br label %for.end35

for.body19:                                       ; preds = %for.cond12
  %39 = bitcast %struct.avifEncoderItem** %item20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #4
  %40 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data21 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %40, i32 0, i32 12
  %41 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data21, align 8, !tbaa !28
  %items22 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %41, i32 0, i32 0
  %item23 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items22, i32 0, i32 0
  %42 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item23, align 4, !tbaa !36
  %43 = load i32, i32* %trakItemIndex, align 4, !tbaa !8
  %arrayidx24 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %42, i32 %43
  store %struct.avifEncoderItem* %arrayidx24, %struct.avifEncoderItem** %item20, align 4, !tbaa !2
  %44 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item20, align 4, !tbaa !2
  %type25 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %44, i32 0, i32 1
  %arraydecay26 = getelementptr inbounds [4 x i8], [4 x i8]* %type25, i32 0, i32 0
  %call27 = call i32 @memcmp(i8* %arraydecay26, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i32 4)
  %cmp28 = icmp eq i32 %call27, 0
  br i1 %cmp28, label %if.then30, label %if.end31

if.then30:                                        ; preds = %for.body19
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %for.body19
  %45 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %46 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item20, align 4, !tbaa !2
  %id = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %46, i32 0, i32 0
  %47 = load i16, i16* %id, align 4, !tbaa !55
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %45, i16 zeroext %47)
  %48 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %48, i16 zeroext 0)
  %49 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %49, i16 zeroext 1)
  %50 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item20, align 4, !tbaa !2
  %51 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifEncoderItemAddMdatFixup(%struct.avifEncoderItem* %50, %struct.avifRWStream* %51)
  %52 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %52, i32 0)
  %53 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %54 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item20, align 4, !tbaa !2
  %metadataPayload = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %54, i32 0, i32 4
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %metadataPayload, i32 0, i32 1
  %55 = load i32, i32* %size, align 4, !tbaa !98
  call void @avifRWStreamWriteU32(%struct.avifRWStream* %53, i32 %55)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end31, %if.then30
  %56 = bitcast %struct.avifEncoderItem** %item20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc32
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc32

for.inc32:                                        ; preds = %cleanup.cont, %cleanup
  %57 = load i32, i32* %trakItemIndex, align 4, !tbaa !8
  %inc33 = add i32 %57, 1
  store i32 %inc33, i32* %trakItemIndex, align 4, !tbaa !8
  br label %for.cond12

for.end35:                                        ; preds = %for.cond.cleanup18
  %58 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %59 = load i32, i32* %iloc, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %58, i32 %59)
  %60 = bitcast i32* %iinf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #4
  %61 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call36 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %61, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.22, i32 0, i32 0), i32 0, i32 0, i32 0)
  store i32 %call36, i32* %iinf, align 4, !tbaa !6
  %62 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %63 = load i32, i32* %metadataItemCount, align 4, !tbaa !8
  %conv37 = trunc i32 %63 to i16
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %62, i16 zeroext %conv37)
  %64 = bitcast i32* %trakItemIndex38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #4
  store i32 0, i32* %trakItemIndex38, align 4, !tbaa !8
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc71, %for.end35
  %65 = load i32, i32* %trakItemIndex38, align 4, !tbaa !8
  %66 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data40 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %66, i32 0, i32 12
  %67 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data40, align 8, !tbaa !28
  %items41 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %67, i32 0, i32 0
  %count42 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items41, i32 0, i32 2
  %68 = load i32, i32* %count42, align 4, !tbaa !31
  %cmp43 = icmp ult i32 %65, %68
  br i1 %cmp43, label %for.body46, label %for.cond.cleanup45

for.cond.cleanup45:                               ; preds = %for.cond39
  store i32 8, i32* %cleanup.dest.slot, align 4
  %69 = bitcast i32* %trakItemIndex38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  br label %for.end74

for.body46:                                       ; preds = %for.cond39
  %70 = bitcast %struct.avifEncoderItem** %item47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #4
  %71 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %data48 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %71, i32 0, i32 12
  %72 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data48, align 8, !tbaa !28
  %items49 = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %72, i32 0, i32 0
  %item50 = getelementptr inbounds %struct.avifEncoderItemArray, %struct.avifEncoderItemArray* %items49, i32 0, i32 0
  %73 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item50, align 4, !tbaa !36
  %74 = load i32, i32* %trakItemIndex38, align 4, !tbaa !8
  %arrayidx51 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %73, i32 %74
  store %struct.avifEncoderItem* %arrayidx51, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %75 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %type52 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %75, i32 0, i32 1
  %arraydecay53 = getelementptr inbounds [4 x i8], [4 x i8]* %type52, i32 0, i32 0
  %call54 = call i32 @memcmp(i8* %arraydecay53, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i32 4)
  %cmp55 = icmp eq i32 %call54, 0
  br i1 %cmp55, label %if.then57, label %if.end58

if.then57:                                        ; preds = %for.body46
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup68

if.end58:                                         ; preds = %for.body46
  %76 = bitcast i32* %infe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #4
  %77 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %call59 = call i32 @avifRWStreamWriteFullBox(%struct.avifRWStream* %77, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.23, i32 0, i32 0), i32 0, i32 2, i32 0)
  store i32 %call59, i32* %infe, align 4, !tbaa !6
  %78 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %79 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %id60 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %79, i32 0, i32 0
  %80 = load i16, i16* %id60, align 4, !tbaa !55
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %78, i16 zeroext %80)
  %81 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  call void @avifRWStreamWriteU16(%struct.avifRWStream* %81, i16 zeroext 0)
  %82 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %83 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %type61 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %83, i32 0, i32 1
  %arraydecay62 = getelementptr inbounds [4 x i8], [4 x i8]* %type61, i32 0, i32 0
  call void @avifRWStreamWrite(%struct.avifRWStream* %82, i8* %arraydecay62, i32 4)
  %84 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %85 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %infeName = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %85, i32 0, i32 6
  %86 = load i8*, i8** %infeName, align 4, !tbaa !76
  %87 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %infeNameSize = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %87, i32 0, i32 7
  %88 = load i32, i32* %infeNameSize, align 4, !tbaa !77
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %84, i8* %86, i32 %88)
  %89 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %infeContentType = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %89, i32 0, i32 8
  %90 = load i8*, i8** %infeContentType, align 4, !tbaa !66
  %tobool = icmp ne i8* %90, null
  br i1 %tobool, label %land.lhs.true, label %if.end67

land.lhs.true:                                    ; preds = %if.end58
  %91 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %infeContentTypeSize = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %91, i32 0, i32 9
  %92 = load i32, i32* %infeContentTypeSize, align 4, !tbaa !67
  %tobool63 = icmp ne i32 %92, 0
  br i1 %tobool63, label %if.then64, label %if.end67

if.then64:                                        ; preds = %land.lhs.true
  %93 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %94 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %infeContentType65 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %94, i32 0, i32 8
  %95 = load i8*, i8** %infeContentType65, align 4, !tbaa !66
  %96 = load %struct.avifEncoderItem*, %struct.avifEncoderItem** %item47, align 4, !tbaa !2
  %infeContentTypeSize66 = getelementptr inbounds %struct.avifEncoderItem, %struct.avifEncoderItem* %96, i32 0, i32 9
  %97 = load i32, i32* %infeContentTypeSize66, align 4, !tbaa !67
  call void @avifRWStreamWriteChars(%struct.avifRWStream* %93, i8* %95, i32 %97)
  br label %if.end67

if.end67:                                         ; preds = %if.then64, %land.lhs.true, %if.end58
  %98 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %99 = load i32, i32* %infe, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %98, i32 %99)
  %100 = bitcast i32* %infe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup68

cleanup68:                                        ; preds = %if.end67, %if.then57
  %101 = bitcast %struct.avifEncoderItem** %item47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #4
  %cleanup.dest69 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest69, label %unreachable [
    i32 0, label %cleanup.cont70
    i32 10, label %for.inc71
  ]

cleanup.cont70:                                   ; preds = %cleanup68
  br label %for.inc71

for.inc71:                                        ; preds = %cleanup.cont70, %cleanup68
  %102 = load i32, i32* %trakItemIndex38, align 4, !tbaa !8
  %inc72 = add i32 %102, 1
  store i32 %inc72, i32* %trakItemIndex38, align 4, !tbaa !8
  br label %for.cond39

for.end74:                                        ; preds = %for.cond.cleanup45
  %103 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %104 = load i32, i32* %iinf, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %103, i32 %104)
  %105 = load %struct.avifRWStream*, %struct.avifRWStream** %s.addr, align 4, !tbaa !2
  %106 = load i32, i32* %meta, align 4, !tbaa !6
  call void @avifRWStreamFinishBox(%struct.avifRWStream* %105, i32 %106)
  %107 = bitcast i32* %iinf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %offsetSizeAndLengthSize) #4
  %108 = bitcast i32* %iloc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #4
  %109 = bitcast i32* %hdlr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %meta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup75

cleanup75:                                        ; preds = %for.end74, %if.then7
  %111 = bitcast i32* %metadataItemCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %cleanup.dest76 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest76, label %unreachable [
    i32 0, label %cleanup.cont77
    i32 1, label %cleanup.cont77
  ]

cleanup.cont77:                                   ; preds = %cleanup75, %cleanup75
  ret void

unreachable:                                      ; preds = %cleanup75, %cleanup68, %cleanup
  unreachable
}

declare i32 @avifRWStreamOffset(%struct.avifRWStream*) #2

declare void @avifRWStreamSetOffset(%struct.avifRWStream*, i32) #2

declare void @avifRWStreamFinishWrite(%struct.avifRWStream*) #2

; Function Attrs: nounwind
define hidden i32 @avifEncoderWrite(%struct.avifEncoder* %encoder, %struct.avifImage* %image, %struct.avifRWData* %output) #0 {
entry:
  %retval = alloca i32, align 4
  %encoder.addr = alloca %struct.avifEncoder*, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %output.addr = alloca %struct.avifRWData*, align 4
  %addImageResult = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifEncoder* %encoder, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRWData* %output, %struct.avifRWData** %output.addr, align 4, !tbaa !2
  %0 = bitcast i32* %addImageResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %2 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %call = call i32 @avifEncoderAddImage(%struct.avifEncoder* %1, %struct.avifImage* %2, i64 1, i32 2)
  store i32 %call, i32* %addImageResult, align 4, !tbaa !80
  %3 = load i32, i32* %addImageResult, align 4, !tbaa !80
  %cmp = icmp ne i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %addImageResult, align 4, !tbaa !80
  store i32 %4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %6 = load %struct.avifRWData*, %struct.avifRWData** %output.addr, align 4, !tbaa !2
  %call1 = call i32 @avifEncoderFinish(%struct.avifEncoder* %5, %struct.avifRWData* %6)
  store i32 %call1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %7 = bitcast i32* %addImageResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: nounwind
define internal %struct.avifEncoderData* @avifEncoderDataCreate() #0 {
entry:
  %data = alloca %struct.avifEncoderData*, align 4
  %0 = bitcast %struct.avifEncoderData** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 48)
  %1 = bitcast i8* %call to %struct.avifEncoderData*
  store %struct.avifEncoderData* %1, %struct.avifEncoderData** %data, align 4, !tbaa !2
  %2 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 4, !tbaa !2
  %3 = bitcast %struct.avifEncoderData* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 48, i1 false)
  %call1 = call %struct.avifImage* @avifImageCreateEmpty()
  %4 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 4, !tbaa !2
  %imageMetadata = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %4, i32 0, i32 2
  store %struct.avifImage* %call1, %struct.avifImage** %imageMetadata, align 4, !tbaa !42
  %5 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 4, !tbaa !2
  %items = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %5, i32 0, i32 0
  %6 = bitcast %struct.avifEncoderItemArray* %items to i8*
  call void @avifArrayCreate(i8* %6, i32 152, i32 8)
  %7 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 4, !tbaa !2
  %frames = getelementptr inbounds %struct.avifEncoderData, %struct.avifEncoderData* %7, i32 0, i32 1
  %8 = bitcast %struct.avifEncoderFrameArray* %frames to i8*
  call void @avifArrayCreate(i8* %8, i32 8, i32 1)
  %9 = load %struct.avifEncoderData*, %struct.avifEncoderData** %data, align 4, !tbaa !2
  %10 = bitcast %struct.avifEncoderData** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  ret %struct.avifEncoderData* %9
}

declare %struct.avifImage* @avifImageCreateEmpty() #2

declare void @avifCodecDestroy(%struct.avifCodec*) #2

declare void @avifImageDestroy(%struct.avifImage*) #2

declare i32 @avifImageUsesU16(%struct.avifImage*) #2

declare void @avifGetPixelFormatInfo(i32, %struct.avifPixelFormatInfo*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
!10 = !{!11, !9, i64 8}
!11 = !{!"avifEncodeSample", !12, i64 0, !9, i64 8}
!12 = !{!"avifRWData", !3, i64 0, !7, i64 4}
!13 = !{!14, !9, i64 8}
!14 = !{!"avifCodecEncodeOutput", !15, i64 0}
!15 = !{!"avifEncodeSampleArray", !3, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!16 = !{!14, !3, i64 0}
!17 = !{!18, !9, i64 4}
!18 = !{!"avifEncoder", !4, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36, !19, i64 40, !20, i64 48, !3, i64 56}
!19 = !{!"long long", !4, i64 0}
!20 = !{!"avifIOStats", !7, i64 0, !7, i64 4}
!21 = !{!18, !9, i64 8}
!22 = !{!18, !9, i64 12}
!23 = !{!18, !9, i64 16}
!24 = !{!18, !9, i64 20}
!25 = !{!18, !9, i64 24}
!26 = !{!18, !9, i64 28}
!27 = !{!18, !9, i64 32}
!28 = !{!18, !3, i64 56}
!29 = !{!18, !19, i64 40}
!30 = !{!18, !9, i64 36}
!31 = !{!32, !9, i64 8}
!32 = !{!"avifEncoderData", !33, i64 0, !34, i64 16, !3, i64 32, !3, i64 36, !3, i64 40, !35, i64 44, !35, i64 46}
!33 = !{!"avifEncoderItemArray", !3, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!34 = !{!"avifEncoderFrameArray", !3, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!35 = !{!"short", !4, i64 0}
!36 = !{!32, !3, i64 0}
!37 = !{!38, !3, i64 8}
!38 = !{!"avifEncoderItem", !35, i64 0, !4, i64 2, !3, i64 8, !3, i64 12, !12, i64 16, !9, i64 24, !3, i64 28, !7, i64 32, !3, i64 36, !7, i64 40, !39, i64 44, !35, i64 60, !3, i64 64, !40, i64 68}
!39 = !{!"avifOffsetFixupArray", !3, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!40 = !{!"ipmaArray", !4, i64 0, !4, i64 16, !4, i64 80}
!41 = !{!38, !3, i64 12}
!42 = !{!32, !3, i64 32}
!43 = !{!19, !19, i64 0}
!44 = !{!45, !9, i64 8}
!45 = !{!"avifImage", !9, i64 0, !9, i64 4, !9, i64 8, !4, i64 12, !4, i64 16, !4, i64 20, !4, i64 24, !4, i64 36, !9, i64 48, !4, i64 52, !3, i64 56, !9, i64 60, !9, i64 64, !12, i64 68, !4, i64 76, !4, i64 80, !4, i64 84, !9, i64 88, !46, i64 92, !47, i64 100, !48, i64 132, !49, i64 133, !12, i64 136, !12, i64 144}
!46 = !{!"avifPixelAspectRatioBox", !9, i64 0, !9, i64 4}
!47 = !{!"avifCleanApertureBox", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !9, i64 20, !9, i64 24, !9, i64 28}
!48 = !{!"avifImageRotation", !4, i64 0}
!49 = !{!"avifImageMirror", !4, i64 0}
!50 = !{!45, !9, i64 0}
!51 = !{!45, !9, i64 4}
!52 = !{!45, !4, i64 12}
!53 = !{!32, !3, i64 36}
!54 = !{!18, !4, i64 0}
!55 = !{!38, !35, i64 0}
!56 = !{!32, !35, i64 46}
!57 = !{!45, !3, i64 56}
!58 = !{!32, !3, i64 40}
!59 = !{!38, !9, i64 24}
!60 = !{!38, !35, i64 60}
!61 = !{!38, !3, i64 64}
!62 = !{!45, !7, i64 140}
!63 = !{!45, !3, i64 136}
!64 = !{!38, !3, i64 16}
!65 = !{!45, !7, i64 148}
!66 = !{!38, !3, i64 36}
!67 = !{!38, !7, i64 40}
!68 = !{!45, !3, i64 144}
!69 = !{!32, !9, i64 24}
!70 = !{!71, !3, i64 28}
!71 = !{!"avifCodec", !3, i64 0, !72, i64 4, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36}
!72 = !{!"avifCodecConfigurationBox", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7, !4, i64 8}
!73 = !{!74, !19, i64 0}
!74 = !{!"avifEncoderFrame", !19, i64 0}
!75 = !{!32, !35, i64 44}
!76 = !{!38, !3, i64 28}
!77 = !{!38, !7, i64 32}
!78 = !{!45, !9, i64 60}
!79 = !{!35, !35, i64 0}
!80 = !{!4, !4, i64 0}
!81 = !{!71, !4, i64 4}
!82 = !{!71, !4, i64 5}
!83 = !{!71, !4, i64 6}
!84 = !{!71, !4, i64 7}
!85 = !{!71, !4, i64 8}
!86 = !{!71, !4, i64 9}
!87 = !{!88, !9, i64 4}
!88 = !{!"avifPixelFormatInfo", !9, i64 0, !9, i64 4, !9, i64 8}
!89 = !{!71, !4, i64 10}
!90 = !{!88, !9, i64 8}
!91 = !{!71, !4, i64 11}
!92 = !{!45, !4, i64 20}
!93 = !{!71, !4, i64 12}
!94 = !{!71, !3, i64 32}
!95 = !{!11, !7, i64 4}
!96 = !{!18, !7, i64 52}
!97 = !{!18, !7, i64 48}
!98 = !{!38, !7, i64 20}
!99 = !{!38, !4, i64 148}
!100 = !{!32, !3, i64 16}
!101 = !{!11, !3, i64 0}
!102 = !{!38, !9, i64 52}
!103 = !{!38, !3, i64 44}
!104 = !{!105, !7, i64 0}
!105 = !{!"avifOffsetFixup", !7, i64 0}
!106 = !{!40, !4, i64 80}
!107 = !{!72, !4, i64 0}
!108 = !{!72, !4, i64 1}
!109 = !{!72, !4, i64 2}
!110 = !{!72, !4, i64 3}
!111 = !{!72, !4, i64 4}
!112 = !{!72, !4, i64 5}
!113 = !{!72, !4, i64 6}
!114 = !{!72, !4, i64 7}
!115 = !{!72, !4, i64 8}
!116 = !{!45, !7, i64 72}
!117 = !{!45, !3, i64 68}
!118 = !{!45, !4, i64 76}
!119 = !{!45, !4, i64 80}
!120 = !{!45, !4, i64 84}
!121 = !{!45, !4, i64 16}
!122 = !{!45, !9, i64 88}
!123 = !{!45, !9, i64 92}
!124 = !{!45, !9, i64 96}
!125 = !{!45, !9, i64 100}
!126 = !{!45, !9, i64 104}
!127 = !{!45, !9, i64 108}
!128 = !{!45, !9, i64 112}
!129 = !{!45, !9, i64 116}
!130 = !{!45, !9, i64 120}
!131 = !{!45, !9, i64 124}
!132 = !{!45, !9, i64 128}
!133 = !{!45, !4, i64 132}
!134 = !{!45, !4, i64 133}
