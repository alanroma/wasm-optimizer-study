; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/corner_detect.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/corner_detect.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.xy = type { i32, i32 }

; Function Attrs: nounwind
define hidden i32 @av1_fast_corner_detect(i8* %buf, i32 %width, i32 %height, i32 %stride, i32* %points, i32 %max_points) #0 {
entry:
  %retval = alloca i32, align 4
  %buf.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %points.addr = alloca i32*, align 4
  %max_points.addr = alloca i32, align 4
  %num_points = alloca i32, align 4
  %frm_corners_xy = alloca %struct.xy*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i8* %buf, i8** %buf.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32* %points, i32** %points.addr, align 4, !tbaa !2
  store i32 %max_points, i32* %max_points.addr, align 4, !tbaa !6
  %0 = bitcast i32* %num_points to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.xy** %frm_corners_xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  %3 = load i32, i32* %width.addr, align 4, !tbaa !6
  %4 = load i32, i32* %height.addr, align 4, !tbaa !6
  %5 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %call = call %struct.xy* @aom_fast9_detect_nonmax(i8* %2, i32 %3, i32 %4, i32 %5, i32 18, i32* %num_points)
  store %struct.xy* %call, %struct.xy** %frm_corners_xy, align 4, !tbaa !2
  %6 = load i32, i32* %num_points, align 4, !tbaa !6
  %7 = load i32, i32* %max_points.addr, align 4, !tbaa !6
  %cmp = icmp sle i32 %6, %7
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load i32, i32* %num_points, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %9 = load i32, i32* %max_points.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %8, %cond.true ], [ %9, %cond.false ]
  store i32 %cond, i32* %num_points, align 4, !tbaa !6
  %10 = load i32, i32* %num_points, align 4, !tbaa !6
  %cmp1 = icmp sgt i32 %10, 0
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end
  %11 = load %struct.xy*, %struct.xy** %frm_corners_xy, align 4, !tbaa !2
  %tobool = icmp ne %struct.xy* %11, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %12 = load i32*, i32** %points.addr, align 4, !tbaa !2
  %13 = bitcast i32* %12 to i8*
  %14 = load %struct.xy*, %struct.xy** %frm_corners_xy, align 4, !tbaa !2
  %15 = bitcast %struct.xy* %14 to i8*
  %16 = load i32, i32* %num_points, align 4, !tbaa !6
  %mul = mul i32 8, %16
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %15, i32 %mul, i1 false)
  %17 = load %struct.xy*, %struct.xy** %frm_corners_xy, align 4, !tbaa !2
  %18 = bitcast %struct.xy* %17 to i8*
  call void @free(i8* %18)
  %19 = load i32, i32* %num_points, align 4, !tbaa !6
  store i32 %19, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %cond.end
  %20 = load %struct.xy*, %struct.xy** %frm_corners_xy, align 4, !tbaa !2
  %21 = bitcast %struct.xy* %20 to i8*
  call void @free(i8* %21)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %22 = bitcast %struct.xy** %frm_corners_xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #3
  %23 = bitcast i32* %num_points to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare %struct.xy* @aom_fast9_detect_nonmax(i8*, i32, i32, i32, i32, i32*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @free(i8*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
