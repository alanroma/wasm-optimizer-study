; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/seg_common.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/common/seg_common.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.segmentation = type { i8, i8, i8, i8, [8 x [8 x i16]], [8 x i32], i32, i8 }

@seg_feature_data_max = internal constant [8 x i32] [i32 255, i32 63, i32 63, i32 63, i32 63, i32 7, i32 0, i32 0], align 16
@seg_feature_data_signed = internal constant [8 x i32] [i32 1, i32 1, i32 1, i32 1, i32 1, i32 0, i32 0, i32 0], align 16

; Function Attrs: nounwind
define hidden void @av1_clearall_segfeatures(%struct.segmentation* %seg) #0 {
entry:
  %seg.addr = alloca %struct.segmentation*, align 4
  store %struct.segmentation* %seg, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %0 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %feature_data = getelementptr inbounds %struct.segmentation, %struct.segmentation* %0, i32 0, i32 4
  %1 = bitcast [8 x [8 x i16]]* %feature_data to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 128, i1 false)
  %2 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %feature_mask = getelementptr inbounds %struct.segmentation, %struct.segmentation* %2, i32 0, i32 5
  %3 = bitcast [8 x i32]* %feature_mask to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 32, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @av1_calculate_segdata(%struct.segmentation* %seg) #0 {
entry:
  %seg.addr = alloca %struct.segmentation*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.segmentation* %seg, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %0 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %segid_preskip = getelementptr inbounds %struct.segmentation, %struct.segmentation* %0, i32 0, i32 7
  store i8 0, i8* %segid_preskip, align 4, !tbaa !6
  %1 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %last_active_segid = getelementptr inbounds %struct.segmentation, %struct.segmentation* %1, i32 0, i32 6
  store i32 0, i32* %last_active_segid, align 4, !tbaa !9
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !10
  %cmp = icmp slt i32 %3, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #3
  br label %for.end12

for.body:                                         ; preds = %for.cond
  %5 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  store i32 0, i32* %j, align 4, !tbaa !10
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %j, align 4, !tbaa !10
  %cmp2 = icmp slt i32 %6, 8
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #3
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %8 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %feature_mask = getelementptr inbounds %struct.segmentation, %struct.segmentation* %8, i32 0, i32 5
  %9 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %feature_mask, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx, align 4, !tbaa !10
  %11 = load i32, i32* %j, align 4, !tbaa !10
  %shl = shl i32 1, %11
  %and = and i32 %10, %shl
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  %12 = load i32, i32* %j, align 4, !tbaa !10
  %cmp5 = icmp sge i32 %12, 5
  %conv = zext i1 %cmp5 to i32
  %13 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %segid_preskip6 = getelementptr inbounds %struct.segmentation, %struct.segmentation* %13, i32 0, i32 7
  %14 = load i8, i8* %segid_preskip6, align 4, !tbaa !6
  %conv7 = zext i8 %14 to i32
  %or = or i32 %conv7, %conv
  %conv8 = trunc i32 %or to i8
  store i8 %conv8, i8* %segid_preskip6, align 4, !tbaa !6
  %15 = load i32, i32* %i, align 4, !tbaa !10
  %16 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %last_active_segid9 = getelementptr inbounds %struct.segmentation, %struct.segmentation* %16, i32 0, i32 6
  store i32 %15, i32* %last_active_segid9, align 4, !tbaa !9
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %17 = load i32, i32* %j, align 4, !tbaa !10
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %j, align 4, !tbaa !10
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  br label %for.inc10

for.inc10:                                        ; preds = %for.end
  %18 = load i32, i32* %i, align 4, !tbaa !10
  %inc11 = add nsw i32 %18, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end12:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden void @av1_enable_segfeature(%struct.segmentation* %seg, i32 %segment_id, i8 zeroext %feature_id) #0 {
entry:
  %seg.addr = alloca %struct.segmentation*, align 4
  %segment_id.addr = alloca i32, align 4
  %feature_id.addr = alloca i8, align 1
  store %struct.segmentation* %seg, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !10
  store i8 %feature_id, i8* %feature_id.addr, align 1, !tbaa !11
  %0 = load i8, i8* %feature_id.addr, align 1, !tbaa !11
  %conv = zext i8 %0 to i32
  %shl = shl i32 1, %conv
  %1 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %feature_mask = getelementptr inbounds %struct.segmentation, %struct.segmentation* %1, i32 0, i32 5
  %2 = load i32, i32* %segment_id.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %feature_mask, i32 0, i32 %2
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !10
  %or = or i32 %3, %shl
  store i32 %or, i32* %arrayidx, align 4, !tbaa !10
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_seg_feature_data_max(i8 zeroext %feature_id) #0 {
entry:
  %feature_id.addr = alloca i8, align 1
  store i8 %feature_id, i8* %feature_id.addr, align 1, !tbaa !11
  %0 = load i8, i8* %feature_id.addr, align 1, !tbaa !11
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* @seg_feature_data_max, i32 0, i32 %idxprom
  %1 = load i32, i32* %arrayidx, align 4, !tbaa !10
  ret i32 %1
}

; Function Attrs: nounwind
define hidden i32 @av1_is_segfeature_signed(i8 zeroext %feature_id) #0 {
entry:
  %feature_id.addr = alloca i8, align 1
  store i8 %feature_id, i8* %feature_id.addr, align 1, !tbaa !11
  %0 = load i8, i8* %feature_id.addr, align 1, !tbaa !11
  %idxprom = zext i8 %0 to i32
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* @seg_feature_data_signed, i32 0, i32 %idxprom
  %1 = load i32, i32* %arrayidx, align 4, !tbaa !10
  ret i32 %1
}

; Function Attrs: nounwind
define hidden void @av1_set_segdata(%struct.segmentation* %seg, i32 %segment_id, i8 zeroext %feature_id, i32 %seg_data) #0 {
entry:
  %seg.addr = alloca %struct.segmentation*, align 4
  %segment_id.addr = alloca i32, align 4
  %feature_id.addr = alloca i8, align 1
  %seg_data.addr = alloca i32, align 4
  store %struct.segmentation* %seg, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  store i32 %segment_id, i32* %segment_id.addr, align 4, !tbaa !10
  store i8 %feature_id, i8* %feature_id.addr, align 1, !tbaa !11
  store i32 %seg_data, i32* %seg_data.addr, align 4, !tbaa !10
  %0 = load i32, i32* %seg_data.addr, align 4, !tbaa !10
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %1 = load i32, i32* %seg_data.addr, align 4, !tbaa !10
  %conv = trunc i32 %1 to i16
  %2 = load %struct.segmentation*, %struct.segmentation** %seg.addr, align 4, !tbaa !2
  %feature_data = getelementptr inbounds %struct.segmentation, %struct.segmentation* %2, i32 0, i32 4
  %3 = load i32, i32* %segment_id.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [8 x [8 x i16]], [8 x [8 x i16]]* %feature_data, i32 0, i32 %3
  %4 = load i8, i8* %feature_id.addr, align 1, !tbaa !11
  %idxprom = zext i8 %4 to i32
  %arrayidx1 = getelementptr inbounds [8 x i16], [8 x i16]* %arrayidx, i32 0, i32 %idxprom
  store i16 %conv, i16* %arrayidx1, align 2, !tbaa !12
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !4, i64 168}
!7 = !{!"segmentation", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 132, !8, i64 164, !4, i64 168}
!8 = !{!"int", !4, i64 0}
!9 = !{!7, !8, i64 164}
!10 = !{!8, !8, i64 0}
!11 = !{!4, !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"short", !4, i64 0}
