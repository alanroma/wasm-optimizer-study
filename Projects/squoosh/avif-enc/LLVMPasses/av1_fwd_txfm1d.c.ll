; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/av1_fwd_txfm1d.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/av1_fwd_txfm1d.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@av1_cospi_arr_data = external constant [7 x [64 x i32]], align 16
@av1_sinpi_arr_data = external constant [7 x [5 x i32]], align 16

; Function Attrs: nounwind
define hidden void @av1_fdct4(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %size = alloca i32, align 4
  %cospi = alloca i32*, align 4
  %stage = alloca i32, align 4
  %bf0 = alloca i32*, align 4
  %bf1 = alloca i32*, align 4
  %step = alloca [4 x i32], align 16
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 4, i32* %size, align 4, !tbaa !7
  %1 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %stage, align 4, !tbaa !7
  %3 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast [4 x i32]* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #4
  %6 = load i32, i32* %stage, align 4, !tbaa !7
  %7 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %8 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %6, i32* %7, i32* %8, i32 4, i8 signext %11)
  %12 = load i32, i32* %stage, align 4, !tbaa !7
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %stage, align 4, !tbaa !7
  %13 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %13, i32** %bf1, align 4, !tbaa !2
  %14 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %14, i32 0
  %15 = load i32, i32* %arrayidx1, align 4, !tbaa !7
  %16 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %16, i32 3
  %17 = load i32, i32* %arrayidx2, align 4, !tbaa !7
  %add = add nsw i32 %15, %17
  %18 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %18, i32 0
  store i32 %add, i32* %arrayidx3, align 4, !tbaa !7
  %19 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %19, i32 1
  %20 = load i32, i32* %arrayidx4, align 4, !tbaa !7
  %21 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 2
  %22 = load i32, i32* %arrayidx5, align 4, !tbaa !7
  %add6 = add nsw i32 %20, %22
  %23 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %23, i32 1
  store i32 %add6, i32* %arrayidx7, align 4, !tbaa !7
  %24 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx8, align 4, !tbaa !7
  %sub = sub nsw i32 0, %25
  %26 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %26, i32 1
  %27 = load i32, i32* %arrayidx9, align 4, !tbaa !7
  %add10 = add nsw i32 %sub, %27
  %28 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %28, i32 2
  store i32 %add10, i32* %arrayidx11, align 4, !tbaa !7
  %29 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %29, i32 3
  %30 = load i32, i32* %arrayidx12, align 4, !tbaa !7
  %sub13 = sub nsw i32 0, %30
  %31 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %31, i32 0
  %32 = load i32, i32* %arrayidx14, align 4, !tbaa !7
  %add15 = add nsw i32 %sub13, %32
  %33 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %33, i32 3
  store i32 %add15, i32* %arrayidx16, align 4, !tbaa !7
  %34 = load i32, i32* %stage, align 4, !tbaa !7
  %35 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %36 = load i32*, i32** %bf1, align 4, !tbaa !2
  %37 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %38 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx17 = getelementptr inbounds i8, i8* %37, i32 %38
  %39 = load i8, i8* %arrayidx17, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %34, i32* %35, i32* %36, i32 4, i8 signext %39)
  %40 = load i32, i32* %stage, align 4, !tbaa !7
  %inc18 = add nsw i32 %40, 1
  store i32 %inc18, i32* %stage, align 4, !tbaa !7
  %41 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv = sext i8 %41 to i32
  %call = call i32* @cospi_arr(i32 %conv)
  store i32* %call, i32** %cospi, align 4, !tbaa !2
  %42 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %42, i32** %bf0, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay, i32** %bf1, align 4, !tbaa !2
  %43 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %43, i32 32
  %44 = load i32, i32* %arrayidx19, align 4, !tbaa !7
  %45 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %45, i32 0
  %46 = load i32, i32* %arrayidx20, align 4, !tbaa !7
  %47 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %47, i32 32
  %48 = load i32, i32* %arrayidx21, align 4, !tbaa !7
  %49 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i32, i32* %49, i32 1
  %50 = load i32, i32* %arrayidx22, align 4, !tbaa !7
  %51 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv23 = sext i8 %51 to i32
  %call24 = call i32 @half_btf(i32 %44, i32 %46, i32 %48, i32 %50, i32 %conv23)
  %52 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %52, i32 0
  store i32 %call24, i32* %arrayidx25, align 4, !tbaa !7
  %53 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i32, i32* %53, i32 32
  %54 = load i32, i32* %arrayidx26, align 4, !tbaa !7
  %sub27 = sub nsw i32 0, %54
  %55 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i32, i32* %55, i32 1
  %56 = load i32, i32* %arrayidx28, align 4, !tbaa !7
  %57 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %57, i32 32
  %58 = load i32, i32* %arrayidx29, align 4, !tbaa !7
  %59 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %59, i32 0
  %60 = load i32, i32* %arrayidx30, align 4, !tbaa !7
  %61 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv31 = sext i8 %61 to i32
  %call32 = call i32 @half_btf(i32 %sub27, i32 %56, i32 %58, i32 %60, i32 %conv31)
  %62 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %62, i32 1
  store i32 %call32, i32* %arrayidx33, align 4, !tbaa !7
  %63 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %63, i32 48
  %64 = load i32, i32* %arrayidx34, align 4, !tbaa !7
  %65 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %65, i32 2
  %66 = load i32, i32* %arrayidx35, align 4, !tbaa !7
  %67 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %67, i32 16
  %68 = load i32, i32* %arrayidx36, align 4, !tbaa !7
  %69 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %69, i32 3
  %70 = load i32, i32* %arrayidx37, align 4, !tbaa !7
  %71 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv38 = sext i8 %71 to i32
  %call39 = call i32 @half_btf(i32 %64, i32 %66, i32 %68, i32 %70, i32 %conv38)
  %72 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %72, i32 2
  store i32 %call39, i32* %arrayidx40, align 4, !tbaa !7
  %73 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %73, i32 48
  %74 = load i32, i32* %arrayidx41, align 4, !tbaa !7
  %75 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %75, i32 3
  %76 = load i32, i32* %arrayidx42, align 4, !tbaa !7
  %77 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %77, i32 16
  %78 = load i32, i32* %arrayidx43, align 4, !tbaa !7
  %sub44 = sub nsw i32 0, %78
  %79 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %79, i32 2
  %80 = load i32, i32* %arrayidx45, align 4, !tbaa !7
  %81 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv46 = sext i8 %81 to i32
  %call47 = call i32 @half_btf(i32 %74, i32 %76, i32 %sub44, i32 %80, i32 %conv46)
  %82 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %82, i32 3
  store i32 %call47, i32* %arrayidx48, align 4, !tbaa !7
  %83 = load i32, i32* %stage, align 4, !tbaa !7
  %84 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %85 = load i32*, i32** %bf1, align 4, !tbaa !2
  %86 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %87 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx49 = getelementptr inbounds i8, i8* %86, i32 %87
  %88 = load i8, i8* %arrayidx49, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %83, i32* %84, i32* %85, i32 4, i8 signext %88)
  %89 = load i32, i32* %stage, align 4, !tbaa !7
  %inc50 = add nsw i32 %89, 1
  store i32 %inc50, i32* %stage, align 4, !tbaa !7
  %arraydecay51 = getelementptr inbounds [4 x i32], [4 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay51, i32** %bf0, align 4, !tbaa !2
  %90 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %90, i32** %bf1, align 4, !tbaa !2
  %91 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %91, i32 0
  %92 = load i32, i32* %arrayidx52, align 4, !tbaa !7
  %93 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i32, i32* %93, i32 0
  store i32 %92, i32* %arrayidx53, align 4, !tbaa !7
  %94 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds i32, i32* %94, i32 2
  %95 = load i32, i32* %arrayidx54, align 4, !tbaa !7
  %96 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %96, i32 1
  store i32 %95, i32* %arrayidx55, align 4, !tbaa !7
  %97 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i32, i32* %97, i32 1
  %98 = load i32, i32* %arrayidx56, align 4, !tbaa !7
  %99 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i32, i32* %99, i32 2
  store i32 %98, i32* %arrayidx57, align 4, !tbaa !7
  %100 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i32, i32* %100, i32 3
  %101 = load i32, i32* %arrayidx58, align 4, !tbaa !7
  %102 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %102, i32 3
  store i32 %101, i32* %arrayidx59, align 4, !tbaa !7
  %103 = load i32, i32* %stage, align 4, !tbaa !7
  %104 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %105 = load i32*, i32** %bf1, align 4, !tbaa !2
  %106 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %107 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx60 = getelementptr inbounds i8, i8* %106, i32 %107
  %108 = load i8, i8* %arrayidx60, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %103, i32* %104, i32* %105, i32 4, i8 signext %108)
  %109 = bitcast [4 x i32]* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #4
  %110 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  %114 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare void @av1_range_check_buf(i32, i32*, i32*, i32, i8 signext) #2

; Function Attrs: inlinehint nounwind
define internal i32* @cospi_arr(i32 %n) #3 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !7
  %0 = load i32, i32* %n.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %0, 10
  %arrayidx = getelementptr inbounds [7 x [64 x i32]], [7 x [64 x i32]]* @av1_cospi_arr_data, i32 0, i32 %sub
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx, i32 0, i32 0
  ret i32* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i32 @half_btf(i32 %w0, i32 %in0, i32 %w1, i32 %in1, i32 %bit) #3 {
entry:
  %w0.addr = alloca i32, align 4
  %in0.addr = alloca i32, align 4
  %w1.addr = alloca i32, align 4
  %in1.addr = alloca i32, align 4
  %bit.addr = alloca i32, align 4
  %result_64 = alloca i64, align 8
  %intermediate = alloca i64, align 8
  store i32 %w0, i32* %w0.addr, align 4, !tbaa !7
  store i32 %in0, i32* %in0.addr, align 4, !tbaa !7
  store i32 %w1, i32* %w1.addr, align 4, !tbaa !7
  store i32 %in1, i32* %in1.addr, align 4, !tbaa !7
  store i32 %bit, i32* %bit.addr, align 4, !tbaa !7
  %0 = bitcast i64* %result_64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = load i32, i32* %w0.addr, align 4, !tbaa !7
  %2 = load i32, i32* %in0.addr, align 4, !tbaa !7
  %mul = mul nsw i32 %1, %2
  %conv = sext i32 %mul to i64
  %3 = load i32, i32* %w1.addr, align 4, !tbaa !7
  %4 = load i32, i32* %in1.addr, align 4, !tbaa !7
  %mul1 = mul nsw i32 %3, %4
  %conv2 = sext i32 %mul1 to i64
  %add = add nsw i64 %conv, %conv2
  store i64 %add, i64* %result_64, align 8, !tbaa !9
  %5 = bitcast i64* %intermediate to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #4
  %6 = load i64, i64* %result_64, align 8, !tbaa !9
  %7 = load i32, i32* %bit.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %7, 1
  %sh_prom = zext i32 %sub to i64
  %shl = shl i64 1, %sh_prom
  %add3 = add nsw i64 %6, %shl
  store i64 %add3, i64* %intermediate, align 8, !tbaa !9
  %8 = load i64, i64* %intermediate, align 8, !tbaa !9
  %9 = load i32, i32* %bit.addr, align 4, !tbaa !7
  %sh_prom4 = zext i32 %9 to i64
  %shr = ashr i64 %8, %sh_prom4
  %conv5 = trunc i64 %shr to i32
  %10 = bitcast i64* %intermediate to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #4
  %11 = bitcast i64* %result_64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  ret i32 %conv5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @av1_fdct8(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %size = alloca i32, align 4
  %cospi = alloca i32*, align 4
  %stage = alloca i32, align 4
  %bf0 = alloca i32*, align 4
  %bf1 = alloca i32*, align 4
  %step = alloca [8 x i32], align 16
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 8, i32* %size, align 4, !tbaa !7
  %1 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %stage, align 4, !tbaa !7
  %3 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast [8 x i32]* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %5) #4
  %6 = load i32, i32* %stage, align 4, !tbaa !7
  %7 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %8 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %6, i32* %7, i32* %8, i32 8, i8 signext %11)
  %12 = load i32, i32* %stage, align 4, !tbaa !7
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %stage, align 4, !tbaa !7
  %13 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %13, i32** %bf1, align 4, !tbaa !2
  %14 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %14, i32 0
  %15 = load i32, i32* %arrayidx1, align 4, !tbaa !7
  %16 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %16, i32 7
  %17 = load i32, i32* %arrayidx2, align 4, !tbaa !7
  %add = add nsw i32 %15, %17
  %18 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %18, i32 0
  store i32 %add, i32* %arrayidx3, align 4, !tbaa !7
  %19 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %19, i32 1
  %20 = load i32, i32* %arrayidx4, align 4, !tbaa !7
  %21 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 6
  %22 = load i32, i32* %arrayidx5, align 4, !tbaa !7
  %add6 = add nsw i32 %20, %22
  %23 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %23, i32 1
  store i32 %add6, i32* %arrayidx7, align 4, !tbaa !7
  %24 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx8, align 4, !tbaa !7
  %26 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %26, i32 5
  %27 = load i32, i32* %arrayidx9, align 4, !tbaa !7
  %add10 = add nsw i32 %25, %27
  %28 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %28, i32 2
  store i32 %add10, i32* %arrayidx11, align 4, !tbaa !7
  %29 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %29, i32 3
  %30 = load i32, i32* %arrayidx12, align 4, !tbaa !7
  %31 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %31, i32 4
  %32 = load i32, i32* %arrayidx13, align 4, !tbaa !7
  %add14 = add nsw i32 %30, %32
  %33 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i32, i32* %33, i32 3
  store i32 %add14, i32* %arrayidx15, align 4, !tbaa !7
  %34 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %34, i32 4
  %35 = load i32, i32* %arrayidx16, align 4, !tbaa !7
  %sub = sub nsw i32 0, %35
  %36 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %36, i32 3
  %37 = load i32, i32* %arrayidx17, align 4, !tbaa !7
  %add18 = add nsw i32 %sub, %37
  %38 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %38, i32 4
  store i32 %add18, i32* %arrayidx19, align 4, !tbaa !7
  %39 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %39, i32 5
  %40 = load i32, i32* %arrayidx20, align 4, !tbaa !7
  %sub21 = sub nsw i32 0, %40
  %41 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i32, i32* %41, i32 2
  %42 = load i32, i32* %arrayidx22, align 4, !tbaa !7
  %add23 = add nsw i32 %sub21, %42
  %43 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %43, i32 5
  store i32 %add23, i32* %arrayidx24, align 4, !tbaa !7
  %44 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %44, i32 6
  %45 = load i32, i32* %arrayidx25, align 4, !tbaa !7
  %sub26 = sub nsw i32 0, %45
  %46 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %46, i32 1
  %47 = load i32, i32* %arrayidx27, align 4, !tbaa !7
  %add28 = add nsw i32 %sub26, %47
  %48 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %48, i32 6
  store i32 %add28, i32* %arrayidx29, align 4, !tbaa !7
  %49 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %49, i32 7
  %50 = load i32, i32* %arrayidx30, align 4, !tbaa !7
  %sub31 = sub nsw i32 0, %50
  %51 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %51, i32 0
  %52 = load i32, i32* %arrayidx32, align 4, !tbaa !7
  %add33 = add nsw i32 %sub31, %52
  %53 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %53, i32 7
  store i32 %add33, i32* %arrayidx34, align 4, !tbaa !7
  %54 = load i32, i32* %stage, align 4, !tbaa !7
  %55 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %56 = load i32*, i32** %bf1, align 4, !tbaa !2
  %57 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %58 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx35 = getelementptr inbounds i8, i8* %57, i32 %58
  %59 = load i8, i8* %arrayidx35, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %54, i32* %55, i32* %56, i32 8, i8 signext %59)
  %60 = load i32, i32* %stage, align 4, !tbaa !7
  %inc36 = add nsw i32 %60, 1
  store i32 %inc36, i32* %stage, align 4, !tbaa !7
  %61 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv = sext i8 %61 to i32
  %call = call i32* @cospi_arr(i32 %conv)
  store i32* %call, i32** %cospi, align 4, !tbaa !2
  %62 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %62, i32** %bf0, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay, i32** %bf1, align 4, !tbaa !2
  %63 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %63, i32 0
  %64 = load i32, i32* %arrayidx37, align 4, !tbaa !7
  %65 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %65, i32 3
  %66 = load i32, i32* %arrayidx38, align 4, !tbaa !7
  %add39 = add nsw i32 %64, %66
  %67 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %67, i32 0
  store i32 %add39, i32* %arrayidx40, align 4, !tbaa !7
  %68 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %68, i32 1
  %69 = load i32, i32* %arrayidx41, align 4, !tbaa !7
  %70 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %70, i32 2
  %71 = load i32, i32* %arrayidx42, align 4, !tbaa !7
  %add43 = add nsw i32 %69, %71
  %72 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %72, i32 1
  store i32 %add43, i32* %arrayidx44, align 4, !tbaa !7
  %73 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %73, i32 2
  %74 = load i32, i32* %arrayidx45, align 4, !tbaa !7
  %sub46 = sub nsw i32 0, %74
  %75 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %75, i32 1
  %76 = load i32, i32* %arrayidx47, align 4, !tbaa !7
  %add48 = add nsw i32 %sub46, %76
  %77 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i32, i32* %77, i32 2
  store i32 %add48, i32* %arrayidx49, align 4, !tbaa !7
  %78 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i32, i32* %78, i32 3
  %79 = load i32, i32* %arrayidx50, align 4, !tbaa !7
  %sub51 = sub nsw i32 0, %79
  %80 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %80, i32 0
  %81 = load i32, i32* %arrayidx52, align 4, !tbaa !7
  %add53 = add nsw i32 %sub51, %81
  %82 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds i32, i32* %82, i32 3
  store i32 %add53, i32* %arrayidx54, align 4, !tbaa !7
  %83 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %83, i32 4
  %84 = load i32, i32* %arrayidx55, align 4, !tbaa !7
  %85 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i32, i32* %85, i32 4
  store i32 %84, i32* %arrayidx56, align 4, !tbaa !7
  %86 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i32, i32* %86, i32 32
  %87 = load i32, i32* %arrayidx57, align 4, !tbaa !7
  %sub58 = sub nsw i32 0, %87
  %88 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %88, i32 5
  %89 = load i32, i32* %arrayidx59, align 4, !tbaa !7
  %90 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %90, i32 32
  %91 = load i32, i32* %arrayidx60, align 4, !tbaa !7
  %92 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %92, i32 6
  %93 = load i32, i32* %arrayidx61, align 4, !tbaa !7
  %94 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv62 = sext i8 %94 to i32
  %call63 = call i32 @half_btf(i32 %sub58, i32 %89, i32 %91, i32 %93, i32 %conv62)
  %95 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %95, i32 5
  store i32 %call63, i32* %arrayidx64, align 4, !tbaa !7
  %96 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %96, i32 32
  %97 = load i32, i32* %arrayidx65, align 4, !tbaa !7
  %98 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %98, i32 6
  %99 = load i32, i32* %arrayidx66, align 4, !tbaa !7
  %100 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %100, i32 32
  %101 = load i32, i32* %arrayidx67, align 4, !tbaa !7
  %102 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %102, i32 5
  %103 = load i32, i32* %arrayidx68, align 4, !tbaa !7
  %104 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv69 = sext i8 %104 to i32
  %call70 = call i32 @half_btf(i32 %97, i32 %99, i32 %101, i32 %103, i32 %conv69)
  %105 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %105, i32 6
  store i32 %call70, i32* %arrayidx71, align 4, !tbaa !7
  %106 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds i32, i32* %106, i32 7
  %107 = load i32, i32* %arrayidx72, align 4, !tbaa !7
  %108 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %108, i32 7
  store i32 %107, i32* %arrayidx73, align 4, !tbaa !7
  %109 = load i32, i32* %stage, align 4, !tbaa !7
  %110 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %111 = load i32*, i32** %bf1, align 4, !tbaa !2
  %112 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %113 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx74 = getelementptr inbounds i8, i8* %112, i32 %113
  %114 = load i8, i8* %arrayidx74, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %109, i32* %110, i32* %111, i32 8, i8 signext %114)
  %115 = load i32, i32* %stage, align 4, !tbaa !7
  %inc75 = add nsw i32 %115, 1
  store i32 %inc75, i32* %stage, align 4, !tbaa !7
  %116 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv76 = sext i8 %116 to i32
  %call77 = call i32* @cospi_arr(i32 %conv76)
  store i32* %call77, i32** %cospi, align 4, !tbaa !2
  %arraydecay78 = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay78, i32** %bf0, align 4, !tbaa !2
  %117 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %117, i32** %bf1, align 4, !tbaa !2
  %118 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %118, i32 32
  %119 = load i32, i32* %arrayidx79, align 4, !tbaa !7
  %120 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds i32, i32* %120, i32 0
  %121 = load i32, i32* %arrayidx80, align 4, !tbaa !7
  %122 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i32, i32* %122, i32 32
  %123 = load i32, i32* %arrayidx81, align 4, !tbaa !7
  %124 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %124, i32 1
  %125 = load i32, i32* %arrayidx82, align 4, !tbaa !7
  %126 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv83 = sext i8 %126 to i32
  %call84 = call i32 @half_btf(i32 %119, i32 %121, i32 %123, i32 %125, i32 %conv83)
  %127 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %127, i32 0
  store i32 %call84, i32* %arrayidx85, align 4, !tbaa !7
  %128 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i32, i32* %128, i32 32
  %129 = load i32, i32* %arrayidx86, align 4, !tbaa !7
  %sub87 = sub nsw i32 0, %129
  %130 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i32, i32* %130, i32 1
  %131 = load i32, i32* %arrayidx88, align 4, !tbaa !7
  %132 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i32, i32* %132, i32 32
  %133 = load i32, i32* %arrayidx89, align 4, !tbaa !7
  %134 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i32, i32* %134, i32 0
  %135 = load i32, i32* %arrayidx90, align 4, !tbaa !7
  %136 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv91 = sext i8 %136 to i32
  %call92 = call i32 @half_btf(i32 %sub87, i32 %131, i32 %133, i32 %135, i32 %conv91)
  %137 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %137, i32 1
  store i32 %call92, i32* %arrayidx93, align 4, !tbaa !7
  %138 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i32, i32* %138, i32 48
  %139 = load i32, i32* %arrayidx94, align 4, !tbaa !7
  %140 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i32, i32* %140, i32 2
  %141 = load i32, i32* %arrayidx95, align 4, !tbaa !7
  %142 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i32, i32* %142, i32 16
  %143 = load i32, i32* %arrayidx96, align 4, !tbaa !7
  %144 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i32, i32* %144, i32 3
  %145 = load i32, i32* %arrayidx97, align 4, !tbaa !7
  %146 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv98 = sext i8 %146 to i32
  %call99 = call i32 @half_btf(i32 %139, i32 %141, i32 %143, i32 %145, i32 %conv98)
  %147 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i32, i32* %147, i32 2
  store i32 %call99, i32* %arrayidx100, align 4, !tbaa !7
  %148 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i32, i32* %148, i32 48
  %149 = load i32, i32* %arrayidx101, align 4, !tbaa !7
  %150 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %150, i32 3
  %151 = load i32, i32* %arrayidx102, align 4, !tbaa !7
  %152 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %152, i32 16
  %153 = load i32, i32* %arrayidx103, align 4, !tbaa !7
  %sub104 = sub nsw i32 0, %153
  %154 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i32, i32* %154, i32 2
  %155 = load i32, i32* %arrayidx105, align 4, !tbaa !7
  %156 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv106 = sext i8 %156 to i32
  %call107 = call i32 @half_btf(i32 %149, i32 %151, i32 %sub104, i32 %155, i32 %conv106)
  %157 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %157, i32 3
  store i32 %call107, i32* %arrayidx108, align 4, !tbaa !7
  %158 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %158, i32 4
  %159 = load i32, i32* %arrayidx109, align 4, !tbaa !7
  %160 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds i32, i32* %160, i32 5
  %161 = load i32, i32* %arrayidx110, align 4, !tbaa !7
  %add111 = add nsw i32 %159, %161
  %162 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %162, i32 4
  store i32 %add111, i32* %arrayidx112, align 4, !tbaa !7
  %163 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds i32, i32* %163, i32 5
  %164 = load i32, i32* %arrayidx113, align 4, !tbaa !7
  %sub114 = sub nsw i32 0, %164
  %165 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %165, i32 4
  %166 = load i32, i32* %arrayidx115, align 4, !tbaa !7
  %add116 = add nsw i32 %sub114, %166
  %167 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds i32, i32* %167, i32 5
  store i32 %add116, i32* %arrayidx117, align 4, !tbaa !7
  %168 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %168, i32 6
  %169 = load i32, i32* %arrayidx118, align 4, !tbaa !7
  %sub119 = sub nsw i32 0, %169
  %170 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i32, i32* %170, i32 7
  %171 = load i32, i32* %arrayidx120, align 4, !tbaa !7
  %add121 = add nsw i32 %sub119, %171
  %172 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %172, i32 6
  store i32 %add121, i32* %arrayidx122, align 4, !tbaa !7
  %173 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %173, i32 7
  %174 = load i32, i32* %arrayidx123, align 4, !tbaa !7
  %175 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %175, i32 6
  %176 = load i32, i32* %arrayidx124, align 4, !tbaa !7
  %add125 = add nsw i32 %174, %176
  %177 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx126 = getelementptr inbounds i32, i32* %177, i32 7
  store i32 %add125, i32* %arrayidx126, align 4, !tbaa !7
  %178 = load i32, i32* %stage, align 4, !tbaa !7
  %179 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %180 = load i32*, i32** %bf1, align 4, !tbaa !2
  %181 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %182 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx127 = getelementptr inbounds i8, i8* %181, i32 %182
  %183 = load i8, i8* %arrayidx127, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %178, i32* %179, i32* %180, i32 8, i8 signext %183)
  %184 = load i32, i32* %stage, align 4, !tbaa !7
  %inc128 = add nsw i32 %184, 1
  store i32 %inc128, i32* %stage, align 4, !tbaa !7
  %185 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv129 = sext i8 %185 to i32
  %call130 = call i32* @cospi_arr(i32 %conv129)
  store i32* %call130, i32** %cospi, align 4, !tbaa !2
  %186 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %186, i32** %bf0, align 4, !tbaa !2
  %arraydecay131 = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay131, i32** %bf1, align 4, !tbaa !2
  %187 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %187, i32 0
  %188 = load i32, i32* %arrayidx132, align 4, !tbaa !7
  %189 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i32, i32* %189, i32 0
  store i32 %188, i32* %arrayidx133, align 4, !tbaa !7
  %190 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx134 = getelementptr inbounds i32, i32* %190, i32 1
  %191 = load i32, i32* %arrayidx134, align 4, !tbaa !7
  %192 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i32, i32* %192, i32 1
  store i32 %191, i32* %arrayidx135, align 4, !tbaa !7
  %193 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %193, i32 2
  %194 = load i32, i32* %arrayidx136, align 4, !tbaa !7
  %195 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds i32, i32* %195, i32 2
  store i32 %194, i32* %arrayidx137, align 4, !tbaa !7
  %196 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx138 = getelementptr inbounds i32, i32* %196, i32 3
  %197 = load i32, i32* %arrayidx138, align 4, !tbaa !7
  %198 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i32, i32* %198, i32 3
  store i32 %197, i32* %arrayidx139, align 4, !tbaa !7
  %199 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds i32, i32* %199, i32 56
  %200 = load i32, i32* %arrayidx140, align 4, !tbaa !7
  %201 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx141 = getelementptr inbounds i32, i32* %201, i32 4
  %202 = load i32, i32* %arrayidx141, align 4, !tbaa !7
  %203 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx142 = getelementptr inbounds i32, i32* %203, i32 8
  %204 = load i32, i32* %arrayidx142, align 4, !tbaa !7
  %205 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds i32, i32* %205, i32 7
  %206 = load i32, i32* %arrayidx143, align 4, !tbaa !7
  %207 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv144 = sext i8 %207 to i32
  %call145 = call i32 @half_btf(i32 %200, i32 %202, i32 %204, i32 %206, i32 %conv144)
  %208 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i32, i32* %208, i32 4
  store i32 %call145, i32* %arrayidx146, align 4, !tbaa !7
  %209 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx147 = getelementptr inbounds i32, i32* %209, i32 24
  %210 = load i32, i32* %arrayidx147, align 4, !tbaa !7
  %211 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx148 = getelementptr inbounds i32, i32* %211, i32 5
  %212 = load i32, i32* %arrayidx148, align 4, !tbaa !7
  %213 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx149 = getelementptr inbounds i32, i32* %213, i32 40
  %214 = load i32, i32* %arrayidx149, align 4, !tbaa !7
  %215 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx150 = getelementptr inbounds i32, i32* %215, i32 6
  %216 = load i32, i32* %arrayidx150, align 4, !tbaa !7
  %217 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv151 = sext i8 %217 to i32
  %call152 = call i32 @half_btf(i32 %210, i32 %212, i32 %214, i32 %216, i32 %conv151)
  %218 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %218, i32 5
  store i32 %call152, i32* %arrayidx153, align 4, !tbaa !7
  %219 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds i32, i32* %219, i32 24
  %220 = load i32, i32* %arrayidx154, align 4, !tbaa !7
  %221 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds i32, i32* %221, i32 6
  %222 = load i32, i32* %arrayidx155, align 4, !tbaa !7
  %223 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds i32, i32* %223, i32 40
  %224 = load i32, i32* %arrayidx156, align 4, !tbaa !7
  %sub157 = sub nsw i32 0, %224
  %225 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx158 = getelementptr inbounds i32, i32* %225, i32 5
  %226 = load i32, i32* %arrayidx158, align 4, !tbaa !7
  %227 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv159 = sext i8 %227 to i32
  %call160 = call i32 @half_btf(i32 %220, i32 %222, i32 %sub157, i32 %226, i32 %conv159)
  %228 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds i32, i32* %228, i32 6
  store i32 %call160, i32* %arrayidx161, align 4, !tbaa !7
  %229 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx162 = getelementptr inbounds i32, i32* %229, i32 56
  %230 = load i32, i32* %arrayidx162, align 4, !tbaa !7
  %231 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds i32, i32* %231, i32 7
  %232 = load i32, i32* %arrayidx163, align 4, !tbaa !7
  %233 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i32, i32* %233, i32 8
  %234 = load i32, i32* %arrayidx164, align 4, !tbaa !7
  %sub165 = sub nsw i32 0, %234
  %235 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i32, i32* %235, i32 4
  %236 = load i32, i32* %arrayidx166, align 4, !tbaa !7
  %237 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv167 = sext i8 %237 to i32
  %call168 = call i32 @half_btf(i32 %230, i32 %232, i32 %sub165, i32 %236, i32 %conv167)
  %238 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds i32, i32* %238, i32 7
  store i32 %call168, i32* %arrayidx169, align 4, !tbaa !7
  %239 = load i32, i32* %stage, align 4, !tbaa !7
  %240 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %241 = load i32*, i32** %bf1, align 4, !tbaa !2
  %242 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %243 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx170 = getelementptr inbounds i8, i8* %242, i32 %243
  %244 = load i8, i8* %arrayidx170, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %239, i32* %240, i32* %241, i32 8, i8 signext %244)
  %245 = load i32, i32* %stage, align 4, !tbaa !7
  %inc171 = add nsw i32 %245, 1
  store i32 %inc171, i32* %stage, align 4, !tbaa !7
  %arraydecay172 = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay172, i32** %bf0, align 4, !tbaa !2
  %246 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %246, i32** %bf1, align 4, !tbaa !2
  %247 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx173 = getelementptr inbounds i32, i32* %247, i32 0
  %248 = load i32, i32* %arrayidx173, align 4, !tbaa !7
  %249 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i32, i32* %249, i32 0
  store i32 %248, i32* %arrayidx174, align 4, !tbaa !7
  %250 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx175 = getelementptr inbounds i32, i32* %250, i32 4
  %251 = load i32, i32* %arrayidx175, align 4, !tbaa !7
  %252 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds i32, i32* %252, i32 1
  store i32 %251, i32* %arrayidx176, align 4, !tbaa !7
  %253 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx177 = getelementptr inbounds i32, i32* %253, i32 2
  %254 = load i32, i32* %arrayidx177, align 4, !tbaa !7
  %255 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx178 = getelementptr inbounds i32, i32* %255, i32 2
  store i32 %254, i32* %arrayidx178, align 4, !tbaa !7
  %256 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds i32, i32* %256, i32 6
  %257 = load i32, i32* %arrayidx179, align 4, !tbaa !7
  %258 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx180 = getelementptr inbounds i32, i32* %258, i32 3
  store i32 %257, i32* %arrayidx180, align 4, !tbaa !7
  %259 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx181 = getelementptr inbounds i32, i32* %259, i32 1
  %260 = load i32, i32* %arrayidx181, align 4, !tbaa !7
  %261 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx182 = getelementptr inbounds i32, i32* %261, i32 4
  store i32 %260, i32* %arrayidx182, align 4, !tbaa !7
  %262 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx183 = getelementptr inbounds i32, i32* %262, i32 5
  %263 = load i32, i32* %arrayidx183, align 4, !tbaa !7
  %264 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx184 = getelementptr inbounds i32, i32* %264, i32 5
  store i32 %263, i32* %arrayidx184, align 4, !tbaa !7
  %265 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx185 = getelementptr inbounds i32, i32* %265, i32 3
  %266 = load i32, i32* %arrayidx185, align 4, !tbaa !7
  %267 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds i32, i32* %267, i32 6
  store i32 %266, i32* %arrayidx186, align 4, !tbaa !7
  %268 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx187 = getelementptr inbounds i32, i32* %268, i32 7
  %269 = load i32, i32* %arrayidx187, align 4, !tbaa !7
  %270 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx188 = getelementptr inbounds i32, i32* %270, i32 7
  store i32 %269, i32* %arrayidx188, align 4, !tbaa !7
  %271 = load i32, i32* %stage, align 4, !tbaa !7
  %272 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %273 = load i32*, i32** %bf1, align 4, !tbaa !2
  %274 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %275 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx189 = getelementptr inbounds i8, i8* %274, i32 %275
  %276 = load i8, i8* %arrayidx189, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %271, i32* %272, i32* %273, i32 8, i8 signext %276)
  %277 = bitcast [8 x i32]* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %277) #4
  %278 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #4
  %279 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #4
  %280 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #4
  %281 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #4
  %282 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fdct16(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %size = alloca i32, align 4
  %cospi = alloca i32*, align 4
  %stage = alloca i32, align 4
  %bf0 = alloca i32*, align 4
  %bf1 = alloca i32*, align 4
  %step = alloca [16 x i32], align 16
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 16, i32* %size, align 4, !tbaa !7
  %1 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %stage, align 4, !tbaa !7
  %3 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast [16 x i32]* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %5) #4
  %6 = load i32, i32* %stage, align 4, !tbaa !7
  %7 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %8 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %6, i32* %7, i32* %8, i32 16, i8 signext %11)
  %12 = load i32, i32* %stage, align 4, !tbaa !7
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %stage, align 4, !tbaa !7
  %13 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %13, i32** %bf1, align 4, !tbaa !2
  %14 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %14, i32 0
  %15 = load i32, i32* %arrayidx1, align 4, !tbaa !7
  %16 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %16, i32 15
  %17 = load i32, i32* %arrayidx2, align 4, !tbaa !7
  %add = add nsw i32 %15, %17
  %18 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %18, i32 0
  store i32 %add, i32* %arrayidx3, align 4, !tbaa !7
  %19 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %19, i32 1
  %20 = load i32, i32* %arrayidx4, align 4, !tbaa !7
  %21 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 14
  %22 = load i32, i32* %arrayidx5, align 4, !tbaa !7
  %add6 = add nsw i32 %20, %22
  %23 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %23, i32 1
  store i32 %add6, i32* %arrayidx7, align 4, !tbaa !7
  %24 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx8, align 4, !tbaa !7
  %26 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %26, i32 13
  %27 = load i32, i32* %arrayidx9, align 4, !tbaa !7
  %add10 = add nsw i32 %25, %27
  %28 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %28, i32 2
  store i32 %add10, i32* %arrayidx11, align 4, !tbaa !7
  %29 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %29, i32 3
  %30 = load i32, i32* %arrayidx12, align 4, !tbaa !7
  %31 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %31, i32 12
  %32 = load i32, i32* %arrayidx13, align 4, !tbaa !7
  %add14 = add nsw i32 %30, %32
  %33 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i32, i32* %33, i32 3
  store i32 %add14, i32* %arrayidx15, align 4, !tbaa !7
  %34 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %34, i32 4
  %35 = load i32, i32* %arrayidx16, align 4, !tbaa !7
  %36 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %36, i32 11
  %37 = load i32, i32* %arrayidx17, align 4, !tbaa !7
  %add18 = add nsw i32 %35, %37
  %38 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %38, i32 4
  store i32 %add18, i32* %arrayidx19, align 4, !tbaa !7
  %39 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %39, i32 5
  %40 = load i32, i32* %arrayidx20, align 4, !tbaa !7
  %41 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %41, i32 10
  %42 = load i32, i32* %arrayidx21, align 4, !tbaa !7
  %add22 = add nsw i32 %40, %42
  %43 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %43, i32 5
  store i32 %add22, i32* %arrayidx23, align 4, !tbaa !7
  %44 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %44, i32 6
  %45 = load i32, i32* %arrayidx24, align 4, !tbaa !7
  %46 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %46, i32 9
  %47 = load i32, i32* %arrayidx25, align 4, !tbaa !7
  %add26 = add nsw i32 %45, %47
  %48 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %48, i32 6
  store i32 %add26, i32* %arrayidx27, align 4, !tbaa !7
  %49 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i32, i32* %49, i32 7
  %50 = load i32, i32* %arrayidx28, align 4, !tbaa !7
  %51 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %51, i32 8
  %52 = load i32, i32* %arrayidx29, align 4, !tbaa !7
  %add30 = add nsw i32 %50, %52
  %53 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i32, i32* %53, i32 7
  store i32 %add30, i32* %arrayidx31, align 4, !tbaa !7
  %54 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %54, i32 8
  %55 = load i32, i32* %arrayidx32, align 4, !tbaa !7
  %sub = sub nsw i32 0, %55
  %56 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %56, i32 7
  %57 = load i32, i32* %arrayidx33, align 4, !tbaa !7
  %add34 = add nsw i32 %sub, %57
  %58 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %58, i32 8
  store i32 %add34, i32* %arrayidx35, align 4, !tbaa !7
  %59 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %59, i32 9
  %60 = load i32, i32* %arrayidx36, align 4, !tbaa !7
  %sub37 = sub nsw i32 0, %60
  %61 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %61, i32 6
  %62 = load i32, i32* %arrayidx38, align 4, !tbaa !7
  %add39 = add nsw i32 %sub37, %62
  %63 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %63, i32 9
  store i32 %add39, i32* %arrayidx40, align 4, !tbaa !7
  %64 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %64, i32 10
  %65 = load i32, i32* %arrayidx41, align 4, !tbaa !7
  %sub42 = sub nsw i32 0, %65
  %66 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %66, i32 5
  %67 = load i32, i32* %arrayidx43, align 4, !tbaa !7
  %add44 = add nsw i32 %sub42, %67
  %68 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %68, i32 10
  store i32 %add44, i32* %arrayidx45, align 4, !tbaa !7
  %69 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i32, i32* %69, i32 11
  %70 = load i32, i32* %arrayidx46, align 4, !tbaa !7
  %sub47 = sub nsw i32 0, %70
  %71 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %71, i32 4
  %72 = load i32, i32* %arrayidx48, align 4, !tbaa !7
  %add49 = add nsw i32 %sub47, %72
  %73 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i32, i32* %73, i32 11
  store i32 %add49, i32* %arrayidx50, align 4, !tbaa !7
  %74 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %74, i32 12
  %75 = load i32, i32* %arrayidx51, align 4, !tbaa !7
  %sub52 = sub nsw i32 0, %75
  %76 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i32, i32* %76, i32 3
  %77 = load i32, i32* %arrayidx53, align 4, !tbaa !7
  %add54 = add nsw i32 %sub52, %77
  %78 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %78, i32 12
  store i32 %add54, i32* %arrayidx55, align 4, !tbaa !7
  %79 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i32, i32* %79, i32 13
  %80 = load i32, i32* %arrayidx56, align 4, !tbaa !7
  %sub57 = sub nsw i32 0, %80
  %81 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i32, i32* %81, i32 2
  %82 = load i32, i32* %arrayidx58, align 4, !tbaa !7
  %add59 = add nsw i32 %sub57, %82
  %83 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %83, i32 13
  store i32 %add59, i32* %arrayidx60, align 4, !tbaa !7
  %84 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %84, i32 14
  %85 = load i32, i32* %arrayidx61, align 4, !tbaa !7
  %sub62 = sub nsw i32 0, %85
  %86 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %86, i32 1
  %87 = load i32, i32* %arrayidx63, align 4, !tbaa !7
  %add64 = add nsw i32 %sub62, %87
  %88 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %88, i32 14
  store i32 %add64, i32* %arrayidx65, align 4, !tbaa !7
  %89 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %89, i32 15
  %90 = load i32, i32* %arrayidx66, align 4, !tbaa !7
  %sub67 = sub nsw i32 0, %90
  %91 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %91, i32 0
  %92 = load i32, i32* %arrayidx68, align 4, !tbaa !7
  %add69 = add nsw i32 %sub67, %92
  %93 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i32, i32* %93, i32 15
  store i32 %add69, i32* %arrayidx70, align 4, !tbaa !7
  %94 = load i32, i32* %stage, align 4, !tbaa !7
  %95 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %96 = load i32*, i32** %bf1, align 4, !tbaa !2
  %97 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %98 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx71 = getelementptr inbounds i8, i8* %97, i32 %98
  %99 = load i8, i8* %arrayidx71, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %94, i32* %95, i32* %96, i32 16, i8 signext %99)
  %100 = load i32, i32* %stage, align 4, !tbaa !7
  %inc72 = add nsw i32 %100, 1
  store i32 %inc72, i32* %stage, align 4, !tbaa !7
  %101 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv = sext i8 %101 to i32
  %call = call i32* @cospi_arr(i32 %conv)
  store i32* %call, i32** %cospi, align 4, !tbaa !2
  %102 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %102, i32** %bf0, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay, i32** %bf1, align 4, !tbaa !2
  %103 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %103, i32 0
  %104 = load i32, i32* %arrayidx73, align 4, !tbaa !7
  %105 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i32, i32* %105, i32 7
  %106 = load i32, i32* %arrayidx74, align 4, !tbaa !7
  %add75 = add nsw i32 %104, %106
  %107 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %107, i32 0
  store i32 %add75, i32* %arrayidx76, align 4, !tbaa !7
  %108 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %108, i32 1
  %109 = load i32, i32* %arrayidx77, align 4, !tbaa !7
  %110 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %110, i32 6
  %111 = load i32, i32* %arrayidx78, align 4, !tbaa !7
  %add79 = add nsw i32 %109, %111
  %112 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds i32, i32* %112, i32 1
  store i32 %add79, i32* %arrayidx80, align 4, !tbaa !7
  %113 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i32, i32* %113, i32 2
  %114 = load i32, i32* %arrayidx81, align 4, !tbaa !7
  %115 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %115, i32 5
  %116 = load i32, i32* %arrayidx82, align 4, !tbaa !7
  %add83 = add nsw i32 %114, %116
  %117 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i32, i32* %117, i32 2
  store i32 %add83, i32* %arrayidx84, align 4, !tbaa !7
  %118 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %118, i32 3
  %119 = load i32, i32* %arrayidx85, align 4, !tbaa !7
  %120 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i32, i32* %120, i32 4
  %121 = load i32, i32* %arrayidx86, align 4, !tbaa !7
  %add87 = add nsw i32 %119, %121
  %122 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i32, i32* %122, i32 3
  store i32 %add87, i32* %arrayidx88, align 4, !tbaa !7
  %123 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i32, i32* %123, i32 4
  %124 = load i32, i32* %arrayidx89, align 4, !tbaa !7
  %sub90 = sub nsw i32 0, %124
  %125 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i32, i32* %125, i32 3
  %126 = load i32, i32* %arrayidx91, align 4, !tbaa !7
  %add92 = add nsw i32 %sub90, %126
  %127 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %127, i32 4
  store i32 %add92, i32* %arrayidx93, align 4, !tbaa !7
  %128 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i32, i32* %128, i32 5
  %129 = load i32, i32* %arrayidx94, align 4, !tbaa !7
  %sub95 = sub nsw i32 0, %129
  %130 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i32, i32* %130, i32 2
  %131 = load i32, i32* %arrayidx96, align 4, !tbaa !7
  %add97 = add nsw i32 %sub95, %131
  %132 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i32, i32* %132, i32 5
  store i32 %add97, i32* %arrayidx98, align 4, !tbaa !7
  %133 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %133, i32 6
  %134 = load i32, i32* %arrayidx99, align 4, !tbaa !7
  %sub100 = sub nsw i32 0, %134
  %135 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i32, i32* %135, i32 1
  %136 = load i32, i32* %arrayidx101, align 4, !tbaa !7
  %add102 = add nsw i32 %sub100, %136
  %137 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %137, i32 6
  store i32 %add102, i32* %arrayidx103, align 4, !tbaa !7
  %138 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx104 = getelementptr inbounds i32, i32* %138, i32 7
  %139 = load i32, i32* %arrayidx104, align 4, !tbaa !7
  %sub105 = sub nsw i32 0, %139
  %140 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i32, i32* %140, i32 0
  %141 = load i32, i32* %arrayidx106, align 4, !tbaa !7
  %add107 = add nsw i32 %sub105, %141
  %142 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %142, i32 7
  store i32 %add107, i32* %arrayidx108, align 4, !tbaa !7
  %143 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %143, i32 8
  %144 = load i32, i32* %arrayidx109, align 4, !tbaa !7
  %145 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds i32, i32* %145, i32 8
  store i32 %144, i32* %arrayidx110, align 4, !tbaa !7
  %146 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i32, i32* %146, i32 9
  %147 = load i32, i32* %arrayidx111, align 4, !tbaa !7
  %148 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %148, i32 9
  store i32 %147, i32* %arrayidx112, align 4, !tbaa !7
  %149 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds i32, i32* %149, i32 32
  %150 = load i32, i32* %arrayidx113, align 4, !tbaa !7
  %sub114 = sub nsw i32 0, %150
  %151 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %151, i32 10
  %152 = load i32, i32* %arrayidx115, align 4, !tbaa !7
  %153 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds i32, i32* %153, i32 32
  %154 = load i32, i32* %arrayidx116, align 4, !tbaa !7
  %155 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds i32, i32* %155, i32 13
  %156 = load i32, i32* %arrayidx117, align 4, !tbaa !7
  %157 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv118 = sext i8 %157 to i32
  %call119 = call i32 @half_btf(i32 %sub114, i32 %152, i32 %154, i32 %156, i32 %conv118)
  %158 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i32, i32* %158, i32 10
  store i32 %call119, i32* %arrayidx120, align 4, !tbaa !7
  %159 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i32, i32* %159, i32 32
  %160 = load i32, i32* %arrayidx121, align 4, !tbaa !7
  %sub122 = sub nsw i32 0, %160
  %161 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %161, i32 11
  %162 = load i32, i32* %arrayidx123, align 4, !tbaa !7
  %163 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %163, i32 32
  %164 = load i32, i32* %arrayidx124, align 4, !tbaa !7
  %165 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %165, i32 12
  %166 = load i32, i32* %arrayidx125, align 4, !tbaa !7
  %167 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv126 = sext i8 %167 to i32
  %call127 = call i32 @half_btf(i32 %sub122, i32 %162, i32 %164, i32 %166, i32 %conv126)
  %168 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx128 = getelementptr inbounds i32, i32* %168, i32 11
  store i32 %call127, i32* %arrayidx128, align 4, !tbaa !7
  %169 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds i32, i32* %169, i32 32
  %170 = load i32, i32* %arrayidx129, align 4, !tbaa !7
  %171 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i32, i32* %171, i32 12
  %172 = load i32, i32* %arrayidx130, align 4, !tbaa !7
  %173 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i32, i32* %173, i32 32
  %174 = load i32, i32* %arrayidx131, align 4, !tbaa !7
  %175 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %175, i32 11
  %176 = load i32, i32* %arrayidx132, align 4, !tbaa !7
  %177 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv133 = sext i8 %177 to i32
  %call134 = call i32 @half_btf(i32 %170, i32 %172, i32 %174, i32 %176, i32 %conv133)
  %178 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i32, i32* %178, i32 12
  store i32 %call134, i32* %arrayidx135, align 4, !tbaa !7
  %179 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %179, i32 32
  %180 = load i32, i32* %arrayidx136, align 4, !tbaa !7
  %181 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds i32, i32* %181, i32 13
  %182 = load i32, i32* %arrayidx137, align 4, !tbaa !7
  %183 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx138 = getelementptr inbounds i32, i32* %183, i32 32
  %184 = load i32, i32* %arrayidx138, align 4, !tbaa !7
  %185 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i32, i32* %185, i32 10
  %186 = load i32, i32* %arrayidx139, align 4, !tbaa !7
  %187 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv140 = sext i8 %187 to i32
  %call141 = call i32 @half_btf(i32 %180, i32 %182, i32 %184, i32 %186, i32 %conv140)
  %188 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx142 = getelementptr inbounds i32, i32* %188, i32 13
  store i32 %call141, i32* %arrayidx142, align 4, !tbaa !7
  %189 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds i32, i32* %189, i32 14
  %190 = load i32, i32* %arrayidx143, align 4, !tbaa !7
  %191 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds i32, i32* %191, i32 14
  store i32 %190, i32* %arrayidx144, align 4, !tbaa !7
  %192 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds i32, i32* %192, i32 15
  %193 = load i32, i32* %arrayidx145, align 4, !tbaa !7
  %194 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i32, i32* %194, i32 15
  store i32 %193, i32* %arrayidx146, align 4, !tbaa !7
  %195 = load i32, i32* %stage, align 4, !tbaa !7
  %196 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %197 = load i32*, i32** %bf1, align 4, !tbaa !2
  %198 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %199 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx147 = getelementptr inbounds i8, i8* %198, i32 %199
  %200 = load i8, i8* %arrayidx147, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %195, i32* %196, i32* %197, i32 16, i8 signext %200)
  %201 = load i32, i32* %stage, align 4, !tbaa !7
  %inc148 = add nsw i32 %201, 1
  store i32 %inc148, i32* %stage, align 4, !tbaa !7
  %202 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv149 = sext i8 %202 to i32
  %call150 = call i32* @cospi_arr(i32 %conv149)
  store i32* %call150, i32** %cospi, align 4, !tbaa !2
  %arraydecay151 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay151, i32** %bf0, align 4, !tbaa !2
  %203 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %203, i32** %bf1, align 4, !tbaa !2
  %204 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds i32, i32* %204, i32 0
  %205 = load i32, i32* %arrayidx152, align 4, !tbaa !7
  %206 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %206, i32 3
  %207 = load i32, i32* %arrayidx153, align 4, !tbaa !7
  %add154 = add nsw i32 %205, %207
  %208 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds i32, i32* %208, i32 0
  store i32 %add154, i32* %arrayidx155, align 4, !tbaa !7
  %209 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds i32, i32* %209, i32 1
  %210 = load i32, i32* %arrayidx156, align 4, !tbaa !7
  %211 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx157 = getelementptr inbounds i32, i32* %211, i32 2
  %212 = load i32, i32* %arrayidx157, align 4, !tbaa !7
  %add158 = add nsw i32 %210, %212
  %213 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx159 = getelementptr inbounds i32, i32* %213, i32 1
  store i32 %add158, i32* %arrayidx159, align 4, !tbaa !7
  %214 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx160 = getelementptr inbounds i32, i32* %214, i32 2
  %215 = load i32, i32* %arrayidx160, align 4, !tbaa !7
  %sub161 = sub nsw i32 0, %215
  %216 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx162 = getelementptr inbounds i32, i32* %216, i32 1
  %217 = load i32, i32* %arrayidx162, align 4, !tbaa !7
  %add163 = add nsw i32 %sub161, %217
  %218 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i32, i32* %218, i32 2
  store i32 %add163, i32* %arrayidx164, align 4, !tbaa !7
  %219 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx165 = getelementptr inbounds i32, i32* %219, i32 3
  %220 = load i32, i32* %arrayidx165, align 4, !tbaa !7
  %sub166 = sub nsw i32 0, %220
  %221 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds i32, i32* %221, i32 0
  %222 = load i32, i32* %arrayidx167, align 4, !tbaa !7
  %add168 = add nsw i32 %sub166, %222
  %223 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds i32, i32* %223, i32 3
  store i32 %add168, i32* %arrayidx169, align 4, !tbaa !7
  %224 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx170 = getelementptr inbounds i32, i32* %224, i32 4
  %225 = load i32, i32* %arrayidx170, align 4, !tbaa !7
  %226 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx171 = getelementptr inbounds i32, i32* %226, i32 4
  store i32 %225, i32* %arrayidx171, align 4, !tbaa !7
  %227 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx172 = getelementptr inbounds i32, i32* %227, i32 32
  %228 = load i32, i32* %arrayidx172, align 4, !tbaa !7
  %sub173 = sub nsw i32 0, %228
  %229 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i32, i32* %229, i32 5
  %230 = load i32, i32* %arrayidx174, align 4, !tbaa !7
  %231 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx175 = getelementptr inbounds i32, i32* %231, i32 32
  %232 = load i32, i32* %arrayidx175, align 4, !tbaa !7
  %233 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds i32, i32* %233, i32 6
  %234 = load i32, i32* %arrayidx176, align 4, !tbaa !7
  %235 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv177 = sext i8 %235 to i32
  %call178 = call i32 @half_btf(i32 %sub173, i32 %230, i32 %232, i32 %234, i32 %conv177)
  %236 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds i32, i32* %236, i32 5
  store i32 %call178, i32* %arrayidx179, align 4, !tbaa !7
  %237 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx180 = getelementptr inbounds i32, i32* %237, i32 32
  %238 = load i32, i32* %arrayidx180, align 4, !tbaa !7
  %239 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx181 = getelementptr inbounds i32, i32* %239, i32 6
  %240 = load i32, i32* %arrayidx181, align 4, !tbaa !7
  %241 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx182 = getelementptr inbounds i32, i32* %241, i32 32
  %242 = load i32, i32* %arrayidx182, align 4, !tbaa !7
  %243 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx183 = getelementptr inbounds i32, i32* %243, i32 5
  %244 = load i32, i32* %arrayidx183, align 4, !tbaa !7
  %245 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv184 = sext i8 %245 to i32
  %call185 = call i32 @half_btf(i32 %238, i32 %240, i32 %242, i32 %244, i32 %conv184)
  %246 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds i32, i32* %246, i32 6
  store i32 %call185, i32* %arrayidx186, align 4, !tbaa !7
  %247 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx187 = getelementptr inbounds i32, i32* %247, i32 7
  %248 = load i32, i32* %arrayidx187, align 4, !tbaa !7
  %249 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx188 = getelementptr inbounds i32, i32* %249, i32 7
  store i32 %248, i32* %arrayidx188, align 4, !tbaa !7
  %250 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx189 = getelementptr inbounds i32, i32* %250, i32 8
  %251 = load i32, i32* %arrayidx189, align 4, !tbaa !7
  %252 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx190 = getelementptr inbounds i32, i32* %252, i32 11
  %253 = load i32, i32* %arrayidx190, align 4, !tbaa !7
  %add191 = add nsw i32 %251, %253
  %254 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds i32, i32* %254, i32 8
  store i32 %add191, i32* %arrayidx192, align 4, !tbaa !7
  %255 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx193 = getelementptr inbounds i32, i32* %255, i32 9
  %256 = load i32, i32* %arrayidx193, align 4, !tbaa !7
  %257 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds i32, i32* %257, i32 10
  %258 = load i32, i32* %arrayidx194, align 4, !tbaa !7
  %add195 = add nsw i32 %256, %258
  %259 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx196 = getelementptr inbounds i32, i32* %259, i32 9
  store i32 %add195, i32* %arrayidx196, align 4, !tbaa !7
  %260 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx197 = getelementptr inbounds i32, i32* %260, i32 10
  %261 = load i32, i32* %arrayidx197, align 4, !tbaa !7
  %sub198 = sub nsw i32 0, %261
  %262 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx199 = getelementptr inbounds i32, i32* %262, i32 9
  %263 = load i32, i32* %arrayidx199, align 4, !tbaa !7
  %add200 = add nsw i32 %sub198, %263
  %264 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx201 = getelementptr inbounds i32, i32* %264, i32 10
  store i32 %add200, i32* %arrayidx201, align 4, !tbaa !7
  %265 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx202 = getelementptr inbounds i32, i32* %265, i32 11
  %266 = load i32, i32* %arrayidx202, align 4, !tbaa !7
  %sub203 = sub nsw i32 0, %266
  %267 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx204 = getelementptr inbounds i32, i32* %267, i32 8
  %268 = load i32, i32* %arrayidx204, align 4, !tbaa !7
  %add205 = add nsw i32 %sub203, %268
  %269 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx206 = getelementptr inbounds i32, i32* %269, i32 11
  store i32 %add205, i32* %arrayidx206, align 4, !tbaa !7
  %270 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx207 = getelementptr inbounds i32, i32* %270, i32 12
  %271 = load i32, i32* %arrayidx207, align 4, !tbaa !7
  %sub208 = sub nsw i32 0, %271
  %272 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx209 = getelementptr inbounds i32, i32* %272, i32 15
  %273 = load i32, i32* %arrayidx209, align 4, !tbaa !7
  %add210 = add nsw i32 %sub208, %273
  %274 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx211 = getelementptr inbounds i32, i32* %274, i32 12
  store i32 %add210, i32* %arrayidx211, align 4, !tbaa !7
  %275 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx212 = getelementptr inbounds i32, i32* %275, i32 13
  %276 = load i32, i32* %arrayidx212, align 4, !tbaa !7
  %sub213 = sub nsw i32 0, %276
  %277 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx214 = getelementptr inbounds i32, i32* %277, i32 14
  %278 = load i32, i32* %arrayidx214, align 4, !tbaa !7
  %add215 = add nsw i32 %sub213, %278
  %279 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx216 = getelementptr inbounds i32, i32* %279, i32 13
  store i32 %add215, i32* %arrayidx216, align 4, !tbaa !7
  %280 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx217 = getelementptr inbounds i32, i32* %280, i32 14
  %281 = load i32, i32* %arrayidx217, align 4, !tbaa !7
  %282 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx218 = getelementptr inbounds i32, i32* %282, i32 13
  %283 = load i32, i32* %arrayidx218, align 4, !tbaa !7
  %add219 = add nsw i32 %281, %283
  %284 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx220 = getelementptr inbounds i32, i32* %284, i32 14
  store i32 %add219, i32* %arrayidx220, align 4, !tbaa !7
  %285 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx221 = getelementptr inbounds i32, i32* %285, i32 15
  %286 = load i32, i32* %arrayidx221, align 4, !tbaa !7
  %287 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx222 = getelementptr inbounds i32, i32* %287, i32 12
  %288 = load i32, i32* %arrayidx222, align 4, !tbaa !7
  %add223 = add nsw i32 %286, %288
  %289 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds i32, i32* %289, i32 15
  store i32 %add223, i32* %arrayidx224, align 4, !tbaa !7
  %290 = load i32, i32* %stage, align 4, !tbaa !7
  %291 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %292 = load i32*, i32** %bf1, align 4, !tbaa !2
  %293 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %294 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx225 = getelementptr inbounds i8, i8* %293, i32 %294
  %295 = load i8, i8* %arrayidx225, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %290, i32* %291, i32* %292, i32 16, i8 signext %295)
  %296 = load i32, i32* %stage, align 4, !tbaa !7
  %inc226 = add nsw i32 %296, 1
  store i32 %inc226, i32* %stage, align 4, !tbaa !7
  %297 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv227 = sext i8 %297 to i32
  %call228 = call i32* @cospi_arr(i32 %conv227)
  store i32* %call228, i32** %cospi, align 4, !tbaa !2
  %298 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %298, i32** %bf0, align 4, !tbaa !2
  %arraydecay229 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay229, i32** %bf1, align 4, !tbaa !2
  %299 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx230 = getelementptr inbounds i32, i32* %299, i32 32
  %300 = load i32, i32* %arrayidx230, align 4, !tbaa !7
  %301 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx231 = getelementptr inbounds i32, i32* %301, i32 0
  %302 = load i32, i32* %arrayidx231, align 4, !tbaa !7
  %303 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx232 = getelementptr inbounds i32, i32* %303, i32 32
  %304 = load i32, i32* %arrayidx232, align 4, !tbaa !7
  %305 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx233 = getelementptr inbounds i32, i32* %305, i32 1
  %306 = load i32, i32* %arrayidx233, align 4, !tbaa !7
  %307 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv234 = sext i8 %307 to i32
  %call235 = call i32 @half_btf(i32 %300, i32 %302, i32 %304, i32 %306, i32 %conv234)
  %308 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds i32, i32* %308, i32 0
  store i32 %call235, i32* %arrayidx236, align 4, !tbaa !7
  %309 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx237 = getelementptr inbounds i32, i32* %309, i32 32
  %310 = load i32, i32* %arrayidx237, align 4, !tbaa !7
  %sub238 = sub nsw i32 0, %310
  %311 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx239 = getelementptr inbounds i32, i32* %311, i32 1
  %312 = load i32, i32* %arrayidx239, align 4, !tbaa !7
  %313 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx240 = getelementptr inbounds i32, i32* %313, i32 32
  %314 = load i32, i32* %arrayidx240, align 4, !tbaa !7
  %315 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx241 = getelementptr inbounds i32, i32* %315, i32 0
  %316 = load i32, i32* %arrayidx241, align 4, !tbaa !7
  %317 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv242 = sext i8 %317 to i32
  %call243 = call i32 @half_btf(i32 %sub238, i32 %312, i32 %314, i32 %316, i32 %conv242)
  %318 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx244 = getelementptr inbounds i32, i32* %318, i32 1
  store i32 %call243, i32* %arrayidx244, align 4, !tbaa !7
  %319 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx245 = getelementptr inbounds i32, i32* %319, i32 48
  %320 = load i32, i32* %arrayidx245, align 4, !tbaa !7
  %321 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx246 = getelementptr inbounds i32, i32* %321, i32 2
  %322 = load i32, i32* %arrayidx246, align 4, !tbaa !7
  %323 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx247 = getelementptr inbounds i32, i32* %323, i32 16
  %324 = load i32, i32* %arrayidx247, align 4, !tbaa !7
  %325 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx248 = getelementptr inbounds i32, i32* %325, i32 3
  %326 = load i32, i32* %arrayidx248, align 4, !tbaa !7
  %327 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv249 = sext i8 %327 to i32
  %call250 = call i32 @half_btf(i32 %320, i32 %322, i32 %324, i32 %326, i32 %conv249)
  %328 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx251 = getelementptr inbounds i32, i32* %328, i32 2
  store i32 %call250, i32* %arrayidx251, align 4, !tbaa !7
  %329 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx252 = getelementptr inbounds i32, i32* %329, i32 48
  %330 = load i32, i32* %arrayidx252, align 4, !tbaa !7
  %331 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx253 = getelementptr inbounds i32, i32* %331, i32 3
  %332 = load i32, i32* %arrayidx253, align 4, !tbaa !7
  %333 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx254 = getelementptr inbounds i32, i32* %333, i32 16
  %334 = load i32, i32* %arrayidx254, align 4, !tbaa !7
  %sub255 = sub nsw i32 0, %334
  %335 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx256 = getelementptr inbounds i32, i32* %335, i32 2
  %336 = load i32, i32* %arrayidx256, align 4, !tbaa !7
  %337 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv257 = sext i8 %337 to i32
  %call258 = call i32 @half_btf(i32 %330, i32 %332, i32 %sub255, i32 %336, i32 %conv257)
  %338 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx259 = getelementptr inbounds i32, i32* %338, i32 3
  store i32 %call258, i32* %arrayidx259, align 4, !tbaa !7
  %339 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx260 = getelementptr inbounds i32, i32* %339, i32 4
  %340 = load i32, i32* %arrayidx260, align 4, !tbaa !7
  %341 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx261 = getelementptr inbounds i32, i32* %341, i32 5
  %342 = load i32, i32* %arrayidx261, align 4, !tbaa !7
  %add262 = add nsw i32 %340, %342
  %343 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx263 = getelementptr inbounds i32, i32* %343, i32 4
  store i32 %add262, i32* %arrayidx263, align 4, !tbaa !7
  %344 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx264 = getelementptr inbounds i32, i32* %344, i32 5
  %345 = load i32, i32* %arrayidx264, align 4, !tbaa !7
  %sub265 = sub nsw i32 0, %345
  %346 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx266 = getelementptr inbounds i32, i32* %346, i32 4
  %347 = load i32, i32* %arrayidx266, align 4, !tbaa !7
  %add267 = add nsw i32 %sub265, %347
  %348 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx268 = getelementptr inbounds i32, i32* %348, i32 5
  store i32 %add267, i32* %arrayidx268, align 4, !tbaa !7
  %349 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx269 = getelementptr inbounds i32, i32* %349, i32 6
  %350 = load i32, i32* %arrayidx269, align 4, !tbaa !7
  %sub270 = sub nsw i32 0, %350
  %351 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx271 = getelementptr inbounds i32, i32* %351, i32 7
  %352 = load i32, i32* %arrayidx271, align 4, !tbaa !7
  %add272 = add nsw i32 %sub270, %352
  %353 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx273 = getelementptr inbounds i32, i32* %353, i32 6
  store i32 %add272, i32* %arrayidx273, align 4, !tbaa !7
  %354 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx274 = getelementptr inbounds i32, i32* %354, i32 7
  %355 = load i32, i32* %arrayidx274, align 4, !tbaa !7
  %356 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx275 = getelementptr inbounds i32, i32* %356, i32 6
  %357 = load i32, i32* %arrayidx275, align 4, !tbaa !7
  %add276 = add nsw i32 %355, %357
  %358 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx277 = getelementptr inbounds i32, i32* %358, i32 7
  store i32 %add276, i32* %arrayidx277, align 4, !tbaa !7
  %359 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx278 = getelementptr inbounds i32, i32* %359, i32 8
  %360 = load i32, i32* %arrayidx278, align 4, !tbaa !7
  %361 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx279 = getelementptr inbounds i32, i32* %361, i32 8
  store i32 %360, i32* %arrayidx279, align 4, !tbaa !7
  %362 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx280 = getelementptr inbounds i32, i32* %362, i32 16
  %363 = load i32, i32* %arrayidx280, align 4, !tbaa !7
  %sub281 = sub nsw i32 0, %363
  %364 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx282 = getelementptr inbounds i32, i32* %364, i32 9
  %365 = load i32, i32* %arrayidx282, align 4, !tbaa !7
  %366 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx283 = getelementptr inbounds i32, i32* %366, i32 48
  %367 = load i32, i32* %arrayidx283, align 4, !tbaa !7
  %368 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx284 = getelementptr inbounds i32, i32* %368, i32 14
  %369 = load i32, i32* %arrayidx284, align 4, !tbaa !7
  %370 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv285 = sext i8 %370 to i32
  %call286 = call i32 @half_btf(i32 %sub281, i32 %365, i32 %367, i32 %369, i32 %conv285)
  %371 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx287 = getelementptr inbounds i32, i32* %371, i32 9
  store i32 %call286, i32* %arrayidx287, align 4, !tbaa !7
  %372 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx288 = getelementptr inbounds i32, i32* %372, i32 48
  %373 = load i32, i32* %arrayidx288, align 4, !tbaa !7
  %sub289 = sub nsw i32 0, %373
  %374 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx290 = getelementptr inbounds i32, i32* %374, i32 10
  %375 = load i32, i32* %arrayidx290, align 4, !tbaa !7
  %376 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx291 = getelementptr inbounds i32, i32* %376, i32 16
  %377 = load i32, i32* %arrayidx291, align 4, !tbaa !7
  %sub292 = sub nsw i32 0, %377
  %378 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx293 = getelementptr inbounds i32, i32* %378, i32 13
  %379 = load i32, i32* %arrayidx293, align 4, !tbaa !7
  %380 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv294 = sext i8 %380 to i32
  %call295 = call i32 @half_btf(i32 %sub289, i32 %375, i32 %sub292, i32 %379, i32 %conv294)
  %381 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx296 = getelementptr inbounds i32, i32* %381, i32 10
  store i32 %call295, i32* %arrayidx296, align 4, !tbaa !7
  %382 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx297 = getelementptr inbounds i32, i32* %382, i32 11
  %383 = load i32, i32* %arrayidx297, align 4, !tbaa !7
  %384 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx298 = getelementptr inbounds i32, i32* %384, i32 11
  store i32 %383, i32* %arrayidx298, align 4, !tbaa !7
  %385 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx299 = getelementptr inbounds i32, i32* %385, i32 12
  %386 = load i32, i32* %arrayidx299, align 4, !tbaa !7
  %387 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx300 = getelementptr inbounds i32, i32* %387, i32 12
  store i32 %386, i32* %arrayidx300, align 4, !tbaa !7
  %388 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx301 = getelementptr inbounds i32, i32* %388, i32 48
  %389 = load i32, i32* %arrayidx301, align 4, !tbaa !7
  %390 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx302 = getelementptr inbounds i32, i32* %390, i32 13
  %391 = load i32, i32* %arrayidx302, align 4, !tbaa !7
  %392 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx303 = getelementptr inbounds i32, i32* %392, i32 16
  %393 = load i32, i32* %arrayidx303, align 4, !tbaa !7
  %sub304 = sub nsw i32 0, %393
  %394 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx305 = getelementptr inbounds i32, i32* %394, i32 10
  %395 = load i32, i32* %arrayidx305, align 4, !tbaa !7
  %396 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv306 = sext i8 %396 to i32
  %call307 = call i32 @half_btf(i32 %389, i32 %391, i32 %sub304, i32 %395, i32 %conv306)
  %397 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx308 = getelementptr inbounds i32, i32* %397, i32 13
  store i32 %call307, i32* %arrayidx308, align 4, !tbaa !7
  %398 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx309 = getelementptr inbounds i32, i32* %398, i32 16
  %399 = load i32, i32* %arrayidx309, align 4, !tbaa !7
  %400 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx310 = getelementptr inbounds i32, i32* %400, i32 14
  %401 = load i32, i32* %arrayidx310, align 4, !tbaa !7
  %402 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx311 = getelementptr inbounds i32, i32* %402, i32 48
  %403 = load i32, i32* %arrayidx311, align 4, !tbaa !7
  %404 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx312 = getelementptr inbounds i32, i32* %404, i32 9
  %405 = load i32, i32* %arrayidx312, align 4, !tbaa !7
  %406 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv313 = sext i8 %406 to i32
  %call314 = call i32 @half_btf(i32 %399, i32 %401, i32 %403, i32 %405, i32 %conv313)
  %407 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx315 = getelementptr inbounds i32, i32* %407, i32 14
  store i32 %call314, i32* %arrayidx315, align 4, !tbaa !7
  %408 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx316 = getelementptr inbounds i32, i32* %408, i32 15
  %409 = load i32, i32* %arrayidx316, align 4, !tbaa !7
  %410 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx317 = getelementptr inbounds i32, i32* %410, i32 15
  store i32 %409, i32* %arrayidx317, align 4, !tbaa !7
  %411 = load i32, i32* %stage, align 4, !tbaa !7
  %412 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %413 = load i32*, i32** %bf1, align 4, !tbaa !2
  %414 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %415 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx318 = getelementptr inbounds i8, i8* %414, i32 %415
  %416 = load i8, i8* %arrayidx318, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %411, i32* %412, i32* %413, i32 16, i8 signext %416)
  %417 = load i32, i32* %stage, align 4, !tbaa !7
  %inc319 = add nsw i32 %417, 1
  store i32 %inc319, i32* %stage, align 4, !tbaa !7
  %418 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv320 = sext i8 %418 to i32
  %call321 = call i32* @cospi_arr(i32 %conv320)
  store i32* %call321, i32** %cospi, align 4, !tbaa !2
  %arraydecay322 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay322, i32** %bf0, align 4, !tbaa !2
  %419 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %419, i32** %bf1, align 4, !tbaa !2
  %420 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx323 = getelementptr inbounds i32, i32* %420, i32 0
  %421 = load i32, i32* %arrayidx323, align 4, !tbaa !7
  %422 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx324 = getelementptr inbounds i32, i32* %422, i32 0
  store i32 %421, i32* %arrayidx324, align 4, !tbaa !7
  %423 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx325 = getelementptr inbounds i32, i32* %423, i32 1
  %424 = load i32, i32* %arrayidx325, align 4, !tbaa !7
  %425 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx326 = getelementptr inbounds i32, i32* %425, i32 1
  store i32 %424, i32* %arrayidx326, align 4, !tbaa !7
  %426 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx327 = getelementptr inbounds i32, i32* %426, i32 2
  %427 = load i32, i32* %arrayidx327, align 4, !tbaa !7
  %428 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx328 = getelementptr inbounds i32, i32* %428, i32 2
  store i32 %427, i32* %arrayidx328, align 4, !tbaa !7
  %429 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx329 = getelementptr inbounds i32, i32* %429, i32 3
  %430 = load i32, i32* %arrayidx329, align 4, !tbaa !7
  %431 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx330 = getelementptr inbounds i32, i32* %431, i32 3
  store i32 %430, i32* %arrayidx330, align 4, !tbaa !7
  %432 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx331 = getelementptr inbounds i32, i32* %432, i32 56
  %433 = load i32, i32* %arrayidx331, align 4, !tbaa !7
  %434 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx332 = getelementptr inbounds i32, i32* %434, i32 4
  %435 = load i32, i32* %arrayidx332, align 4, !tbaa !7
  %436 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx333 = getelementptr inbounds i32, i32* %436, i32 8
  %437 = load i32, i32* %arrayidx333, align 4, !tbaa !7
  %438 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx334 = getelementptr inbounds i32, i32* %438, i32 7
  %439 = load i32, i32* %arrayidx334, align 4, !tbaa !7
  %440 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv335 = sext i8 %440 to i32
  %call336 = call i32 @half_btf(i32 %433, i32 %435, i32 %437, i32 %439, i32 %conv335)
  %441 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx337 = getelementptr inbounds i32, i32* %441, i32 4
  store i32 %call336, i32* %arrayidx337, align 4, !tbaa !7
  %442 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx338 = getelementptr inbounds i32, i32* %442, i32 24
  %443 = load i32, i32* %arrayidx338, align 4, !tbaa !7
  %444 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx339 = getelementptr inbounds i32, i32* %444, i32 5
  %445 = load i32, i32* %arrayidx339, align 4, !tbaa !7
  %446 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx340 = getelementptr inbounds i32, i32* %446, i32 40
  %447 = load i32, i32* %arrayidx340, align 4, !tbaa !7
  %448 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx341 = getelementptr inbounds i32, i32* %448, i32 6
  %449 = load i32, i32* %arrayidx341, align 4, !tbaa !7
  %450 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv342 = sext i8 %450 to i32
  %call343 = call i32 @half_btf(i32 %443, i32 %445, i32 %447, i32 %449, i32 %conv342)
  %451 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx344 = getelementptr inbounds i32, i32* %451, i32 5
  store i32 %call343, i32* %arrayidx344, align 4, !tbaa !7
  %452 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx345 = getelementptr inbounds i32, i32* %452, i32 24
  %453 = load i32, i32* %arrayidx345, align 4, !tbaa !7
  %454 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx346 = getelementptr inbounds i32, i32* %454, i32 6
  %455 = load i32, i32* %arrayidx346, align 4, !tbaa !7
  %456 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx347 = getelementptr inbounds i32, i32* %456, i32 40
  %457 = load i32, i32* %arrayidx347, align 4, !tbaa !7
  %sub348 = sub nsw i32 0, %457
  %458 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx349 = getelementptr inbounds i32, i32* %458, i32 5
  %459 = load i32, i32* %arrayidx349, align 4, !tbaa !7
  %460 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv350 = sext i8 %460 to i32
  %call351 = call i32 @half_btf(i32 %453, i32 %455, i32 %sub348, i32 %459, i32 %conv350)
  %461 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx352 = getelementptr inbounds i32, i32* %461, i32 6
  store i32 %call351, i32* %arrayidx352, align 4, !tbaa !7
  %462 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx353 = getelementptr inbounds i32, i32* %462, i32 56
  %463 = load i32, i32* %arrayidx353, align 4, !tbaa !7
  %464 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx354 = getelementptr inbounds i32, i32* %464, i32 7
  %465 = load i32, i32* %arrayidx354, align 4, !tbaa !7
  %466 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx355 = getelementptr inbounds i32, i32* %466, i32 8
  %467 = load i32, i32* %arrayidx355, align 4, !tbaa !7
  %sub356 = sub nsw i32 0, %467
  %468 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx357 = getelementptr inbounds i32, i32* %468, i32 4
  %469 = load i32, i32* %arrayidx357, align 4, !tbaa !7
  %470 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv358 = sext i8 %470 to i32
  %call359 = call i32 @half_btf(i32 %463, i32 %465, i32 %sub356, i32 %469, i32 %conv358)
  %471 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx360 = getelementptr inbounds i32, i32* %471, i32 7
  store i32 %call359, i32* %arrayidx360, align 4, !tbaa !7
  %472 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx361 = getelementptr inbounds i32, i32* %472, i32 8
  %473 = load i32, i32* %arrayidx361, align 4, !tbaa !7
  %474 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx362 = getelementptr inbounds i32, i32* %474, i32 9
  %475 = load i32, i32* %arrayidx362, align 4, !tbaa !7
  %add363 = add nsw i32 %473, %475
  %476 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx364 = getelementptr inbounds i32, i32* %476, i32 8
  store i32 %add363, i32* %arrayidx364, align 4, !tbaa !7
  %477 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx365 = getelementptr inbounds i32, i32* %477, i32 9
  %478 = load i32, i32* %arrayidx365, align 4, !tbaa !7
  %sub366 = sub nsw i32 0, %478
  %479 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx367 = getelementptr inbounds i32, i32* %479, i32 8
  %480 = load i32, i32* %arrayidx367, align 4, !tbaa !7
  %add368 = add nsw i32 %sub366, %480
  %481 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx369 = getelementptr inbounds i32, i32* %481, i32 9
  store i32 %add368, i32* %arrayidx369, align 4, !tbaa !7
  %482 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx370 = getelementptr inbounds i32, i32* %482, i32 10
  %483 = load i32, i32* %arrayidx370, align 4, !tbaa !7
  %sub371 = sub nsw i32 0, %483
  %484 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx372 = getelementptr inbounds i32, i32* %484, i32 11
  %485 = load i32, i32* %arrayidx372, align 4, !tbaa !7
  %add373 = add nsw i32 %sub371, %485
  %486 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx374 = getelementptr inbounds i32, i32* %486, i32 10
  store i32 %add373, i32* %arrayidx374, align 4, !tbaa !7
  %487 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx375 = getelementptr inbounds i32, i32* %487, i32 11
  %488 = load i32, i32* %arrayidx375, align 4, !tbaa !7
  %489 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx376 = getelementptr inbounds i32, i32* %489, i32 10
  %490 = load i32, i32* %arrayidx376, align 4, !tbaa !7
  %add377 = add nsw i32 %488, %490
  %491 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx378 = getelementptr inbounds i32, i32* %491, i32 11
  store i32 %add377, i32* %arrayidx378, align 4, !tbaa !7
  %492 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx379 = getelementptr inbounds i32, i32* %492, i32 12
  %493 = load i32, i32* %arrayidx379, align 4, !tbaa !7
  %494 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx380 = getelementptr inbounds i32, i32* %494, i32 13
  %495 = load i32, i32* %arrayidx380, align 4, !tbaa !7
  %add381 = add nsw i32 %493, %495
  %496 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx382 = getelementptr inbounds i32, i32* %496, i32 12
  store i32 %add381, i32* %arrayidx382, align 4, !tbaa !7
  %497 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx383 = getelementptr inbounds i32, i32* %497, i32 13
  %498 = load i32, i32* %arrayidx383, align 4, !tbaa !7
  %sub384 = sub nsw i32 0, %498
  %499 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx385 = getelementptr inbounds i32, i32* %499, i32 12
  %500 = load i32, i32* %arrayidx385, align 4, !tbaa !7
  %add386 = add nsw i32 %sub384, %500
  %501 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx387 = getelementptr inbounds i32, i32* %501, i32 13
  store i32 %add386, i32* %arrayidx387, align 4, !tbaa !7
  %502 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx388 = getelementptr inbounds i32, i32* %502, i32 14
  %503 = load i32, i32* %arrayidx388, align 4, !tbaa !7
  %sub389 = sub nsw i32 0, %503
  %504 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx390 = getelementptr inbounds i32, i32* %504, i32 15
  %505 = load i32, i32* %arrayidx390, align 4, !tbaa !7
  %add391 = add nsw i32 %sub389, %505
  %506 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx392 = getelementptr inbounds i32, i32* %506, i32 14
  store i32 %add391, i32* %arrayidx392, align 4, !tbaa !7
  %507 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx393 = getelementptr inbounds i32, i32* %507, i32 15
  %508 = load i32, i32* %arrayidx393, align 4, !tbaa !7
  %509 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx394 = getelementptr inbounds i32, i32* %509, i32 14
  %510 = load i32, i32* %arrayidx394, align 4, !tbaa !7
  %add395 = add nsw i32 %508, %510
  %511 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx396 = getelementptr inbounds i32, i32* %511, i32 15
  store i32 %add395, i32* %arrayidx396, align 4, !tbaa !7
  %512 = load i32, i32* %stage, align 4, !tbaa !7
  %513 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %514 = load i32*, i32** %bf1, align 4, !tbaa !2
  %515 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %516 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx397 = getelementptr inbounds i8, i8* %515, i32 %516
  %517 = load i8, i8* %arrayidx397, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %512, i32* %513, i32* %514, i32 16, i8 signext %517)
  %518 = load i32, i32* %stage, align 4, !tbaa !7
  %inc398 = add nsw i32 %518, 1
  store i32 %inc398, i32* %stage, align 4, !tbaa !7
  %519 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv399 = sext i8 %519 to i32
  %call400 = call i32* @cospi_arr(i32 %conv399)
  store i32* %call400, i32** %cospi, align 4, !tbaa !2
  %520 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %520, i32** %bf0, align 4, !tbaa !2
  %arraydecay401 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay401, i32** %bf1, align 4, !tbaa !2
  %521 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx402 = getelementptr inbounds i32, i32* %521, i32 0
  %522 = load i32, i32* %arrayidx402, align 4, !tbaa !7
  %523 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx403 = getelementptr inbounds i32, i32* %523, i32 0
  store i32 %522, i32* %arrayidx403, align 4, !tbaa !7
  %524 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx404 = getelementptr inbounds i32, i32* %524, i32 1
  %525 = load i32, i32* %arrayidx404, align 4, !tbaa !7
  %526 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx405 = getelementptr inbounds i32, i32* %526, i32 1
  store i32 %525, i32* %arrayidx405, align 4, !tbaa !7
  %527 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx406 = getelementptr inbounds i32, i32* %527, i32 2
  %528 = load i32, i32* %arrayidx406, align 4, !tbaa !7
  %529 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx407 = getelementptr inbounds i32, i32* %529, i32 2
  store i32 %528, i32* %arrayidx407, align 4, !tbaa !7
  %530 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx408 = getelementptr inbounds i32, i32* %530, i32 3
  %531 = load i32, i32* %arrayidx408, align 4, !tbaa !7
  %532 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx409 = getelementptr inbounds i32, i32* %532, i32 3
  store i32 %531, i32* %arrayidx409, align 4, !tbaa !7
  %533 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx410 = getelementptr inbounds i32, i32* %533, i32 4
  %534 = load i32, i32* %arrayidx410, align 4, !tbaa !7
  %535 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx411 = getelementptr inbounds i32, i32* %535, i32 4
  store i32 %534, i32* %arrayidx411, align 4, !tbaa !7
  %536 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx412 = getelementptr inbounds i32, i32* %536, i32 5
  %537 = load i32, i32* %arrayidx412, align 4, !tbaa !7
  %538 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx413 = getelementptr inbounds i32, i32* %538, i32 5
  store i32 %537, i32* %arrayidx413, align 4, !tbaa !7
  %539 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx414 = getelementptr inbounds i32, i32* %539, i32 6
  %540 = load i32, i32* %arrayidx414, align 4, !tbaa !7
  %541 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx415 = getelementptr inbounds i32, i32* %541, i32 6
  store i32 %540, i32* %arrayidx415, align 4, !tbaa !7
  %542 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx416 = getelementptr inbounds i32, i32* %542, i32 7
  %543 = load i32, i32* %arrayidx416, align 4, !tbaa !7
  %544 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx417 = getelementptr inbounds i32, i32* %544, i32 7
  store i32 %543, i32* %arrayidx417, align 4, !tbaa !7
  %545 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx418 = getelementptr inbounds i32, i32* %545, i32 60
  %546 = load i32, i32* %arrayidx418, align 4, !tbaa !7
  %547 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx419 = getelementptr inbounds i32, i32* %547, i32 8
  %548 = load i32, i32* %arrayidx419, align 4, !tbaa !7
  %549 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx420 = getelementptr inbounds i32, i32* %549, i32 4
  %550 = load i32, i32* %arrayidx420, align 4, !tbaa !7
  %551 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx421 = getelementptr inbounds i32, i32* %551, i32 15
  %552 = load i32, i32* %arrayidx421, align 4, !tbaa !7
  %553 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv422 = sext i8 %553 to i32
  %call423 = call i32 @half_btf(i32 %546, i32 %548, i32 %550, i32 %552, i32 %conv422)
  %554 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx424 = getelementptr inbounds i32, i32* %554, i32 8
  store i32 %call423, i32* %arrayidx424, align 4, !tbaa !7
  %555 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx425 = getelementptr inbounds i32, i32* %555, i32 28
  %556 = load i32, i32* %arrayidx425, align 4, !tbaa !7
  %557 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx426 = getelementptr inbounds i32, i32* %557, i32 9
  %558 = load i32, i32* %arrayidx426, align 4, !tbaa !7
  %559 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx427 = getelementptr inbounds i32, i32* %559, i32 36
  %560 = load i32, i32* %arrayidx427, align 4, !tbaa !7
  %561 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx428 = getelementptr inbounds i32, i32* %561, i32 14
  %562 = load i32, i32* %arrayidx428, align 4, !tbaa !7
  %563 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv429 = sext i8 %563 to i32
  %call430 = call i32 @half_btf(i32 %556, i32 %558, i32 %560, i32 %562, i32 %conv429)
  %564 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx431 = getelementptr inbounds i32, i32* %564, i32 9
  store i32 %call430, i32* %arrayidx431, align 4, !tbaa !7
  %565 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx432 = getelementptr inbounds i32, i32* %565, i32 44
  %566 = load i32, i32* %arrayidx432, align 4, !tbaa !7
  %567 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx433 = getelementptr inbounds i32, i32* %567, i32 10
  %568 = load i32, i32* %arrayidx433, align 4, !tbaa !7
  %569 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx434 = getelementptr inbounds i32, i32* %569, i32 20
  %570 = load i32, i32* %arrayidx434, align 4, !tbaa !7
  %571 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx435 = getelementptr inbounds i32, i32* %571, i32 13
  %572 = load i32, i32* %arrayidx435, align 4, !tbaa !7
  %573 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv436 = sext i8 %573 to i32
  %call437 = call i32 @half_btf(i32 %566, i32 %568, i32 %570, i32 %572, i32 %conv436)
  %574 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx438 = getelementptr inbounds i32, i32* %574, i32 10
  store i32 %call437, i32* %arrayidx438, align 4, !tbaa !7
  %575 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx439 = getelementptr inbounds i32, i32* %575, i32 12
  %576 = load i32, i32* %arrayidx439, align 4, !tbaa !7
  %577 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx440 = getelementptr inbounds i32, i32* %577, i32 11
  %578 = load i32, i32* %arrayidx440, align 4, !tbaa !7
  %579 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx441 = getelementptr inbounds i32, i32* %579, i32 52
  %580 = load i32, i32* %arrayidx441, align 4, !tbaa !7
  %581 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx442 = getelementptr inbounds i32, i32* %581, i32 12
  %582 = load i32, i32* %arrayidx442, align 4, !tbaa !7
  %583 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv443 = sext i8 %583 to i32
  %call444 = call i32 @half_btf(i32 %576, i32 %578, i32 %580, i32 %582, i32 %conv443)
  %584 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx445 = getelementptr inbounds i32, i32* %584, i32 11
  store i32 %call444, i32* %arrayidx445, align 4, !tbaa !7
  %585 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx446 = getelementptr inbounds i32, i32* %585, i32 12
  %586 = load i32, i32* %arrayidx446, align 4, !tbaa !7
  %587 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx447 = getelementptr inbounds i32, i32* %587, i32 12
  %588 = load i32, i32* %arrayidx447, align 4, !tbaa !7
  %589 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx448 = getelementptr inbounds i32, i32* %589, i32 52
  %590 = load i32, i32* %arrayidx448, align 4, !tbaa !7
  %sub449 = sub nsw i32 0, %590
  %591 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx450 = getelementptr inbounds i32, i32* %591, i32 11
  %592 = load i32, i32* %arrayidx450, align 4, !tbaa !7
  %593 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv451 = sext i8 %593 to i32
  %call452 = call i32 @half_btf(i32 %586, i32 %588, i32 %sub449, i32 %592, i32 %conv451)
  %594 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx453 = getelementptr inbounds i32, i32* %594, i32 12
  store i32 %call452, i32* %arrayidx453, align 4, !tbaa !7
  %595 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx454 = getelementptr inbounds i32, i32* %595, i32 44
  %596 = load i32, i32* %arrayidx454, align 4, !tbaa !7
  %597 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx455 = getelementptr inbounds i32, i32* %597, i32 13
  %598 = load i32, i32* %arrayidx455, align 4, !tbaa !7
  %599 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx456 = getelementptr inbounds i32, i32* %599, i32 20
  %600 = load i32, i32* %arrayidx456, align 4, !tbaa !7
  %sub457 = sub nsw i32 0, %600
  %601 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx458 = getelementptr inbounds i32, i32* %601, i32 10
  %602 = load i32, i32* %arrayidx458, align 4, !tbaa !7
  %603 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv459 = sext i8 %603 to i32
  %call460 = call i32 @half_btf(i32 %596, i32 %598, i32 %sub457, i32 %602, i32 %conv459)
  %604 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx461 = getelementptr inbounds i32, i32* %604, i32 13
  store i32 %call460, i32* %arrayidx461, align 4, !tbaa !7
  %605 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx462 = getelementptr inbounds i32, i32* %605, i32 28
  %606 = load i32, i32* %arrayidx462, align 4, !tbaa !7
  %607 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx463 = getelementptr inbounds i32, i32* %607, i32 14
  %608 = load i32, i32* %arrayidx463, align 4, !tbaa !7
  %609 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx464 = getelementptr inbounds i32, i32* %609, i32 36
  %610 = load i32, i32* %arrayidx464, align 4, !tbaa !7
  %sub465 = sub nsw i32 0, %610
  %611 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx466 = getelementptr inbounds i32, i32* %611, i32 9
  %612 = load i32, i32* %arrayidx466, align 4, !tbaa !7
  %613 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv467 = sext i8 %613 to i32
  %call468 = call i32 @half_btf(i32 %606, i32 %608, i32 %sub465, i32 %612, i32 %conv467)
  %614 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx469 = getelementptr inbounds i32, i32* %614, i32 14
  store i32 %call468, i32* %arrayidx469, align 4, !tbaa !7
  %615 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx470 = getelementptr inbounds i32, i32* %615, i32 60
  %616 = load i32, i32* %arrayidx470, align 4, !tbaa !7
  %617 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx471 = getelementptr inbounds i32, i32* %617, i32 15
  %618 = load i32, i32* %arrayidx471, align 4, !tbaa !7
  %619 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx472 = getelementptr inbounds i32, i32* %619, i32 4
  %620 = load i32, i32* %arrayidx472, align 4, !tbaa !7
  %sub473 = sub nsw i32 0, %620
  %621 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx474 = getelementptr inbounds i32, i32* %621, i32 8
  %622 = load i32, i32* %arrayidx474, align 4, !tbaa !7
  %623 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv475 = sext i8 %623 to i32
  %call476 = call i32 @half_btf(i32 %616, i32 %618, i32 %sub473, i32 %622, i32 %conv475)
  %624 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx477 = getelementptr inbounds i32, i32* %624, i32 15
  store i32 %call476, i32* %arrayidx477, align 4, !tbaa !7
  %625 = load i32, i32* %stage, align 4, !tbaa !7
  %626 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %627 = load i32*, i32** %bf1, align 4, !tbaa !2
  %628 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %629 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx478 = getelementptr inbounds i8, i8* %628, i32 %629
  %630 = load i8, i8* %arrayidx478, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %625, i32* %626, i32* %627, i32 16, i8 signext %630)
  %631 = load i32, i32* %stage, align 4, !tbaa !7
  %inc479 = add nsw i32 %631, 1
  store i32 %inc479, i32* %stage, align 4, !tbaa !7
  %arraydecay480 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay480, i32** %bf0, align 4, !tbaa !2
  %632 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %632, i32** %bf1, align 4, !tbaa !2
  %633 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx481 = getelementptr inbounds i32, i32* %633, i32 0
  %634 = load i32, i32* %arrayidx481, align 4, !tbaa !7
  %635 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx482 = getelementptr inbounds i32, i32* %635, i32 0
  store i32 %634, i32* %arrayidx482, align 4, !tbaa !7
  %636 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx483 = getelementptr inbounds i32, i32* %636, i32 8
  %637 = load i32, i32* %arrayidx483, align 4, !tbaa !7
  %638 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx484 = getelementptr inbounds i32, i32* %638, i32 1
  store i32 %637, i32* %arrayidx484, align 4, !tbaa !7
  %639 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx485 = getelementptr inbounds i32, i32* %639, i32 4
  %640 = load i32, i32* %arrayidx485, align 4, !tbaa !7
  %641 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx486 = getelementptr inbounds i32, i32* %641, i32 2
  store i32 %640, i32* %arrayidx486, align 4, !tbaa !7
  %642 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx487 = getelementptr inbounds i32, i32* %642, i32 12
  %643 = load i32, i32* %arrayidx487, align 4, !tbaa !7
  %644 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx488 = getelementptr inbounds i32, i32* %644, i32 3
  store i32 %643, i32* %arrayidx488, align 4, !tbaa !7
  %645 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx489 = getelementptr inbounds i32, i32* %645, i32 2
  %646 = load i32, i32* %arrayidx489, align 4, !tbaa !7
  %647 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx490 = getelementptr inbounds i32, i32* %647, i32 4
  store i32 %646, i32* %arrayidx490, align 4, !tbaa !7
  %648 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx491 = getelementptr inbounds i32, i32* %648, i32 10
  %649 = load i32, i32* %arrayidx491, align 4, !tbaa !7
  %650 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx492 = getelementptr inbounds i32, i32* %650, i32 5
  store i32 %649, i32* %arrayidx492, align 4, !tbaa !7
  %651 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx493 = getelementptr inbounds i32, i32* %651, i32 6
  %652 = load i32, i32* %arrayidx493, align 4, !tbaa !7
  %653 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx494 = getelementptr inbounds i32, i32* %653, i32 6
  store i32 %652, i32* %arrayidx494, align 4, !tbaa !7
  %654 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx495 = getelementptr inbounds i32, i32* %654, i32 14
  %655 = load i32, i32* %arrayidx495, align 4, !tbaa !7
  %656 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx496 = getelementptr inbounds i32, i32* %656, i32 7
  store i32 %655, i32* %arrayidx496, align 4, !tbaa !7
  %657 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx497 = getelementptr inbounds i32, i32* %657, i32 1
  %658 = load i32, i32* %arrayidx497, align 4, !tbaa !7
  %659 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx498 = getelementptr inbounds i32, i32* %659, i32 8
  store i32 %658, i32* %arrayidx498, align 4, !tbaa !7
  %660 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx499 = getelementptr inbounds i32, i32* %660, i32 9
  %661 = load i32, i32* %arrayidx499, align 4, !tbaa !7
  %662 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx500 = getelementptr inbounds i32, i32* %662, i32 9
  store i32 %661, i32* %arrayidx500, align 4, !tbaa !7
  %663 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx501 = getelementptr inbounds i32, i32* %663, i32 5
  %664 = load i32, i32* %arrayidx501, align 4, !tbaa !7
  %665 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx502 = getelementptr inbounds i32, i32* %665, i32 10
  store i32 %664, i32* %arrayidx502, align 4, !tbaa !7
  %666 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx503 = getelementptr inbounds i32, i32* %666, i32 13
  %667 = load i32, i32* %arrayidx503, align 4, !tbaa !7
  %668 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx504 = getelementptr inbounds i32, i32* %668, i32 11
  store i32 %667, i32* %arrayidx504, align 4, !tbaa !7
  %669 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx505 = getelementptr inbounds i32, i32* %669, i32 3
  %670 = load i32, i32* %arrayidx505, align 4, !tbaa !7
  %671 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx506 = getelementptr inbounds i32, i32* %671, i32 12
  store i32 %670, i32* %arrayidx506, align 4, !tbaa !7
  %672 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx507 = getelementptr inbounds i32, i32* %672, i32 11
  %673 = load i32, i32* %arrayidx507, align 4, !tbaa !7
  %674 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx508 = getelementptr inbounds i32, i32* %674, i32 13
  store i32 %673, i32* %arrayidx508, align 4, !tbaa !7
  %675 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx509 = getelementptr inbounds i32, i32* %675, i32 7
  %676 = load i32, i32* %arrayidx509, align 4, !tbaa !7
  %677 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx510 = getelementptr inbounds i32, i32* %677, i32 14
  store i32 %676, i32* %arrayidx510, align 4, !tbaa !7
  %678 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx511 = getelementptr inbounds i32, i32* %678, i32 15
  %679 = load i32, i32* %arrayidx511, align 4, !tbaa !7
  %680 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx512 = getelementptr inbounds i32, i32* %680, i32 15
  store i32 %679, i32* %arrayidx512, align 4, !tbaa !7
  %681 = load i32, i32* %stage, align 4, !tbaa !7
  %682 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %683 = load i32*, i32** %bf1, align 4, !tbaa !2
  %684 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %685 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx513 = getelementptr inbounds i8, i8* %684, i32 %685
  %686 = load i8, i8* %arrayidx513, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %681, i32* %682, i32* %683, i32 16, i8 signext %686)
  %687 = bitcast [16 x i32]* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %687) #4
  %688 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %688) #4
  %689 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %689) #4
  %690 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %690) #4
  %691 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %691) #4
  %692 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %692) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fdct32(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %size = alloca i32, align 4
  %cospi = alloca i32*, align 4
  %stage = alloca i32, align 4
  %bf0 = alloca i32*, align 4
  %bf1 = alloca i32*, align 4
  %step = alloca [32 x i32], align 16
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 32, i32* %size, align 4, !tbaa !7
  %1 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %stage, align 4, !tbaa !7
  %3 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast [32 x i32]* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %5) #4
  %6 = load i32, i32* %stage, align 4, !tbaa !7
  %7 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %8 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %6, i32* %7, i32* %8, i32 32, i8 signext %11)
  %12 = load i32, i32* %stage, align 4, !tbaa !7
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %stage, align 4, !tbaa !7
  %13 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %13, i32** %bf1, align 4, !tbaa !2
  %14 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %14, i32 0
  %15 = load i32, i32* %arrayidx1, align 4, !tbaa !7
  %16 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %16, i32 31
  %17 = load i32, i32* %arrayidx2, align 4, !tbaa !7
  %add = add nsw i32 %15, %17
  %18 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %18, i32 0
  store i32 %add, i32* %arrayidx3, align 4, !tbaa !7
  %19 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %19, i32 1
  %20 = load i32, i32* %arrayidx4, align 4, !tbaa !7
  %21 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 30
  %22 = load i32, i32* %arrayidx5, align 4, !tbaa !7
  %add6 = add nsw i32 %20, %22
  %23 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %23, i32 1
  store i32 %add6, i32* %arrayidx7, align 4, !tbaa !7
  %24 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx8, align 4, !tbaa !7
  %26 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %26, i32 29
  %27 = load i32, i32* %arrayidx9, align 4, !tbaa !7
  %add10 = add nsw i32 %25, %27
  %28 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %28, i32 2
  store i32 %add10, i32* %arrayidx11, align 4, !tbaa !7
  %29 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %29, i32 3
  %30 = load i32, i32* %arrayidx12, align 4, !tbaa !7
  %31 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %31, i32 28
  %32 = load i32, i32* %arrayidx13, align 4, !tbaa !7
  %add14 = add nsw i32 %30, %32
  %33 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i32, i32* %33, i32 3
  store i32 %add14, i32* %arrayidx15, align 4, !tbaa !7
  %34 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %34, i32 4
  %35 = load i32, i32* %arrayidx16, align 4, !tbaa !7
  %36 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %36, i32 27
  %37 = load i32, i32* %arrayidx17, align 4, !tbaa !7
  %add18 = add nsw i32 %35, %37
  %38 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %38, i32 4
  store i32 %add18, i32* %arrayidx19, align 4, !tbaa !7
  %39 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %39, i32 5
  %40 = load i32, i32* %arrayidx20, align 4, !tbaa !7
  %41 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %41, i32 26
  %42 = load i32, i32* %arrayidx21, align 4, !tbaa !7
  %add22 = add nsw i32 %40, %42
  %43 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %43, i32 5
  store i32 %add22, i32* %arrayidx23, align 4, !tbaa !7
  %44 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %44, i32 6
  %45 = load i32, i32* %arrayidx24, align 4, !tbaa !7
  %46 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %46, i32 25
  %47 = load i32, i32* %arrayidx25, align 4, !tbaa !7
  %add26 = add nsw i32 %45, %47
  %48 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %48, i32 6
  store i32 %add26, i32* %arrayidx27, align 4, !tbaa !7
  %49 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i32, i32* %49, i32 7
  %50 = load i32, i32* %arrayidx28, align 4, !tbaa !7
  %51 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %51, i32 24
  %52 = load i32, i32* %arrayidx29, align 4, !tbaa !7
  %add30 = add nsw i32 %50, %52
  %53 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i32, i32* %53, i32 7
  store i32 %add30, i32* %arrayidx31, align 4, !tbaa !7
  %54 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %54, i32 8
  %55 = load i32, i32* %arrayidx32, align 4, !tbaa !7
  %56 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %56, i32 23
  %57 = load i32, i32* %arrayidx33, align 4, !tbaa !7
  %add34 = add nsw i32 %55, %57
  %58 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %58, i32 8
  store i32 %add34, i32* %arrayidx35, align 4, !tbaa !7
  %59 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %59, i32 9
  %60 = load i32, i32* %arrayidx36, align 4, !tbaa !7
  %61 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %61, i32 22
  %62 = load i32, i32* %arrayidx37, align 4, !tbaa !7
  %add38 = add nsw i32 %60, %62
  %63 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %63, i32 9
  store i32 %add38, i32* %arrayidx39, align 4, !tbaa !7
  %64 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %64, i32 10
  %65 = load i32, i32* %arrayidx40, align 4, !tbaa !7
  %66 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %66, i32 21
  %67 = load i32, i32* %arrayidx41, align 4, !tbaa !7
  %add42 = add nsw i32 %65, %67
  %68 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %68, i32 10
  store i32 %add42, i32* %arrayidx43, align 4, !tbaa !7
  %69 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %69, i32 11
  %70 = load i32, i32* %arrayidx44, align 4, !tbaa !7
  %71 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %71, i32 20
  %72 = load i32, i32* %arrayidx45, align 4, !tbaa !7
  %add46 = add nsw i32 %70, %72
  %73 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %73, i32 11
  store i32 %add46, i32* %arrayidx47, align 4, !tbaa !7
  %74 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %74, i32 12
  %75 = load i32, i32* %arrayidx48, align 4, !tbaa !7
  %76 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i32, i32* %76, i32 19
  %77 = load i32, i32* %arrayidx49, align 4, !tbaa !7
  %add50 = add nsw i32 %75, %77
  %78 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %78, i32 12
  store i32 %add50, i32* %arrayidx51, align 4, !tbaa !7
  %79 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %79, i32 13
  %80 = load i32, i32* %arrayidx52, align 4, !tbaa !7
  %81 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i32, i32* %81, i32 18
  %82 = load i32, i32* %arrayidx53, align 4, !tbaa !7
  %add54 = add nsw i32 %80, %82
  %83 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %83, i32 13
  store i32 %add54, i32* %arrayidx55, align 4, !tbaa !7
  %84 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i32, i32* %84, i32 14
  %85 = load i32, i32* %arrayidx56, align 4, !tbaa !7
  %86 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i32, i32* %86, i32 17
  %87 = load i32, i32* %arrayidx57, align 4, !tbaa !7
  %add58 = add nsw i32 %85, %87
  %88 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %88, i32 14
  store i32 %add58, i32* %arrayidx59, align 4, !tbaa !7
  %89 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %89, i32 15
  %90 = load i32, i32* %arrayidx60, align 4, !tbaa !7
  %91 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %91, i32 16
  %92 = load i32, i32* %arrayidx61, align 4, !tbaa !7
  %add62 = add nsw i32 %90, %92
  %93 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %93, i32 15
  store i32 %add62, i32* %arrayidx63, align 4, !tbaa !7
  %94 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %94, i32 16
  %95 = load i32, i32* %arrayidx64, align 4, !tbaa !7
  %sub = sub nsw i32 0, %95
  %96 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %96, i32 15
  %97 = load i32, i32* %arrayidx65, align 4, !tbaa !7
  %add66 = add nsw i32 %sub, %97
  %98 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %98, i32 16
  store i32 %add66, i32* %arrayidx67, align 4, !tbaa !7
  %99 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %99, i32 17
  %100 = load i32, i32* %arrayidx68, align 4, !tbaa !7
  %sub69 = sub nsw i32 0, %100
  %101 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i32, i32* %101, i32 14
  %102 = load i32, i32* %arrayidx70, align 4, !tbaa !7
  %add71 = add nsw i32 %sub69, %102
  %103 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds i32, i32* %103, i32 17
  store i32 %add71, i32* %arrayidx72, align 4, !tbaa !7
  %104 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %104, i32 18
  %105 = load i32, i32* %arrayidx73, align 4, !tbaa !7
  %sub74 = sub nsw i32 0, %105
  %106 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds i32, i32* %106, i32 13
  %107 = load i32, i32* %arrayidx75, align 4, !tbaa !7
  %add76 = add nsw i32 %sub74, %107
  %108 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %108, i32 18
  store i32 %add76, i32* %arrayidx77, align 4, !tbaa !7
  %109 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %109, i32 19
  %110 = load i32, i32* %arrayidx78, align 4, !tbaa !7
  %sub79 = sub nsw i32 0, %110
  %111 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds i32, i32* %111, i32 12
  %112 = load i32, i32* %arrayidx80, align 4, !tbaa !7
  %add81 = add nsw i32 %sub79, %112
  %113 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %113, i32 19
  store i32 %add81, i32* %arrayidx82, align 4, !tbaa !7
  %114 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %114, i32 20
  %115 = load i32, i32* %arrayidx83, align 4, !tbaa !7
  %sub84 = sub nsw i32 0, %115
  %116 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %116, i32 11
  %117 = load i32, i32* %arrayidx85, align 4, !tbaa !7
  %add86 = add nsw i32 %sub84, %117
  %118 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx87 = getelementptr inbounds i32, i32* %118, i32 20
  store i32 %add86, i32* %arrayidx87, align 4, !tbaa !7
  %119 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i32, i32* %119, i32 21
  %120 = load i32, i32* %arrayidx88, align 4, !tbaa !7
  %sub89 = sub nsw i32 0, %120
  %121 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i32, i32* %121, i32 10
  %122 = load i32, i32* %arrayidx90, align 4, !tbaa !7
  %add91 = add nsw i32 %sub89, %122
  %123 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i32, i32* %123, i32 21
  store i32 %add91, i32* %arrayidx92, align 4, !tbaa !7
  %124 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %124, i32 22
  %125 = load i32, i32* %arrayidx93, align 4, !tbaa !7
  %sub94 = sub nsw i32 0, %125
  %126 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i32, i32* %126, i32 9
  %127 = load i32, i32* %arrayidx95, align 4, !tbaa !7
  %add96 = add nsw i32 %sub94, %127
  %128 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i32, i32* %128, i32 22
  store i32 %add96, i32* %arrayidx97, align 4, !tbaa !7
  %129 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i32, i32* %129, i32 23
  %130 = load i32, i32* %arrayidx98, align 4, !tbaa !7
  %sub99 = sub nsw i32 0, %130
  %131 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i32, i32* %131, i32 8
  %132 = load i32, i32* %arrayidx100, align 4, !tbaa !7
  %add101 = add nsw i32 %sub99, %132
  %133 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %133, i32 23
  store i32 %add101, i32* %arrayidx102, align 4, !tbaa !7
  %134 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %134, i32 24
  %135 = load i32, i32* %arrayidx103, align 4, !tbaa !7
  %sub104 = sub nsw i32 0, %135
  %136 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i32, i32* %136, i32 7
  %137 = load i32, i32* %arrayidx105, align 4, !tbaa !7
  %add106 = add nsw i32 %sub104, %137
  %138 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx107 = getelementptr inbounds i32, i32* %138, i32 24
  store i32 %add106, i32* %arrayidx107, align 4, !tbaa !7
  %139 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %139, i32 25
  %140 = load i32, i32* %arrayidx108, align 4, !tbaa !7
  %sub109 = sub nsw i32 0, %140
  %141 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds i32, i32* %141, i32 6
  %142 = load i32, i32* %arrayidx110, align 4, !tbaa !7
  %add111 = add nsw i32 %sub109, %142
  %143 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %143, i32 25
  store i32 %add111, i32* %arrayidx112, align 4, !tbaa !7
  %144 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds i32, i32* %144, i32 26
  %145 = load i32, i32* %arrayidx113, align 4, !tbaa !7
  %sub114 = sub nsw i32 0, %145
  %146 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %146, i32 5
  %147 = load i32, i32* %arrayidx115, align 4, !tbaa !7
  %add116 = add nsw i32 %sub114, %147
  %148 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds i32, i32* %148, i32 26
  store i32 %add116, i32* %arrayidx117, align 4, !tbaa !7
  %149 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %149, i32 27
  %150 = load i32, i32* %arrayidx118, align 4, !tbaa !7
  %sub119 = sub nsw i32 0, %150
  %151 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i32, i32* %151, i32 4
  %152 = load i32, i32* %arrayidx120, align 4, !tbaa !7
  %add121 = add nsw i32 %sub119, %152
  %153 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %153, i32 27
  store i32 %add121, i32* %arrayidx122, align 4, !tbaa !7
  %154 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %154, i32 28
  %155 = load i32, i32* %arrayidx123, align 4, !tbaa !7
  %sub124 = sub nsw i32 0, %155
  %156 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %156, i32 3
  %157 = load i32, i32* %arrayidx125, align 4, !tbaa !7
  %add126 = add nsw i32 %sub124, %157
  %158 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %158, i32 28
  store i32 %add126, i32* %arrayidx127, align 4, !tbaa !7
  %159 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx128 = getelementptr inbounds i32, i32* %159, i32 29
  %160 = load i32, i32* %arrayidx128, align 4, !tbaa !7
  %sub129 = sub nsw i32 0, %160
  %161 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i32, i32* %161, i32 2
  %162 = load i32, i32* %arrayidx130, align 4, !tbaa !7
  %add131 = add nsw i32 %sub129, %162
  %163 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %163, i32 29
  store i32 %add131, i32* %arrayidx132, align 4, !tbaa !7
  %164 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i32, i32* %164, i32 30
  %165 = load i32, i32* %arrayidx133, align 4, !tbaa !7
  %sub134 = sub nsw i32 0, %165
  %166 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i32, i32* %166, i32 1
  %167 = load i32, i32* %arrayidx135, align 4, !tbaa !7
  %add136 = add nsw i32 %sub134, %167
  %168 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds i32, i32* %168, i32 30
  store i32 %add136, i32* %arrayidx137, align 4, !tbaa !7
  %169 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx138 = getelementptr inbounds i32, i32* %169, i32 31
  %170 = load i32, i32* %arrayidx138, align 4, !tbaa !7
  %sub139 = sub nsw i32 0, %170
  %171 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds i32, i32* %171, i32 0
  %172 = load i32, i32* %arrayidx140, align 4, !tbaa !7
  %add141 = add nsw i32 %sub139, %172
  %173 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx142 = getelementptr inbounds i32, i32* %173, i32 31
  store i32 %add141, i32* %arrayidx142, align 4, !tbaa !7
  %174 = load i32, i32* %stage, align 4, !tbaa !7
  %175 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %176 = load i32*, i32** %bf1, align 4, !tbaa !2
  %177 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %178 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx143 = getelementptr inbounds i8, i8* %177, i32 %178
  %179 = load i8, i8* %arrayidx143, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %174, i32* %175, i32* %176, i32 32, i8 signext %179)
  %180 = load i32, i32* %stage, align 4, !tbaa !7
  %inc144 = add nsw i32 %180, 1
  store i32 %inc144, i32* %stage, align 4, !tbaa !7
  %181 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv = sext i8 %181 to i32
  %call = call i32* @cospi_arr(i32 %conv)
  store i32* %call, i32** %cospi, align 4, !tbaa !2
  %182 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %182, i32** %bf0, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [32 x i32], [32 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay, i32** %bf1, align 4, !tbaa !2
  %183 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds i32, i32* %183, i32 0
  %184 = load i32, i32* %arrayidx145, align 4, !tbaa !7
  %185 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i32, i32* %185, i32 15
  %186 = load i32, i32* %arrayidx146, align 4, !tbaa !7
  %add147 = add nsw i32 %184, %186
  %187 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx148 = getelementptr inbounds i32, i32* %187, i32 0
  store i32 %add147, i32* %arrayidx148, align 4, !tbaa !7
  %188 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx149 = getelementptr inbounds i32, i32* %188, i32 1
  %189 = load i32, i32* %arrayidx149, align 4, !tbaa !7
  %190 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx150 = getelementptr inbounds i32, i32* %190, i32 14
  %191 = load i32, i32* %arrayidx150, align 4, !tbaa !7
  %add151 = add nsw i32 %189, %191
  %192 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds i32, i32* %192, i32 1
  store i32 %add151, i32* %arrayidx152, align 4, !tbaa !7
  %193 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %193, i32 2
  %194 = load i32, i32* %arrayidx153, align 4, !tbaa !7
  %195 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds i32, i32* %195, i32 13
  %196 = load i32, i32* %arrayidx154, align 4, !tbaa !7
  %add155 = add nsw i32 %194, %196
  %197 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds i32, i32* %197, i32 2
  store i32 %add155, i32* %arrayidx156, align 4, !tbaa !7
  %198 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx157 = getelementptr inbounds i32, i32* %198, i32 3
  %199 = load i32, i32* %arrayidx157, align 4, !tbaa !7
  %200 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx158 = getelementptr inbounds i32, i32* %200, i32 12
  %201 = load i32, i32* %arrayidx158, align 4, !tbaa !7
  %add159 = add nsw i32 %199, %201
  %202 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx160 = getelementptr inbounds i32, i32* %202, i32 3
  store i32 %add159, i32* %arrayidx160, align 4, !tbaa !7
  %203 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds i32, i32* %203, i32 4
  %204 = load i32, i32* %arrayidx161, align 4, !tbaa !7
  %205 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx162 = getelementptr inbounds i32, i32* %205, i32 11
  %206 = load i32, i32* %arrayidx162, align 4, !tbaa !7
  %add163 = add nsw i32 %204, %206
  %207 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i32, i32* %207, i32 4
  store i32 %add163, i32* %arrayidx164, align 4, !tbaa !7
  %208 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx165 = getelementptr inbounds i32, i32* %208, i32 5
  %209 = load i32, i32* %arrayidx165, align 4, !tbaa !7
  %210 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i32, i32* %210, i32 10
  %211 = load i32, i32* %arrayidx166, align 4, !tbaa !7
  %add167 = add nsw i32 %209, %211
  %212 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx168 = getelementptr inbounds i32, i32* %212, i32 5
  store i32 %add167, i32* %arrayidx168, align 4, !tbaa !7
  %213 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds i32, i32* %213, i32 6
  %214 = load i32, i32* %arrayidx169, align 4, !tbaa !7
  %215 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx170 = getelementptr inbounds i32, i32* %215, i32 9
  %216 = load i32, i32* %arrayidx170, align 4, !tbaa !7
  %add171 = add nsw i32 %214, %216
  %217 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx172 = getelementptr inbounds i32, i32* %217, i32 6
  store i32 %add171, i32* %arrayidx172, align 4, !tbaa !7
  %218 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx173 = getelementptr inbounds i32, i32* %218, i32 7
  %219 = load i32, i32* %arrayidx173, align 4, !tbaa !7
  %220 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i32, i32* %220, i32 8
  %221 = load i32, i32* %arrayidx174, align 4, !tbaa !7
  %add175 = add nsw i32 %219, %221
  %222 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds i32, i32* %222, i32 7
  store i32 %add175, i32* %arrayidx176, align 4, !tbaa !7
  %223 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx177 = getelementptr inbounds i32, i32* %223, i32 8
  %224 = load i32, i32* %arrayidx177, align 4, !tbaa !7
  %sub178 = sub nsw i32 0, %224
  %225 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds i32, i32* %225, i32 7
  %226 = load i32, i32* %arrayidx179, align 4, !tbaa !7
  %add180 = add nsw i32 %sub178, %226
  %227 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx181 = getelementptr inbounds i32, i32* %227, i32 8
  store i32 %add180, i32* %arrayidx181, align 4, !tbaa !7
  %228 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx182 = getelementptr inbounds i32, i32* %228, i32 9
  %229 = load i32, i32* %arrayidx182, align 4, !tbaa !7
  %sub183 = sub nsw i32 0, %229
  %230 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx184 = getelementptr inbounds i32, i32* %230, i32 6
  %231 = load i32, i32* %arrayidx184, align 4, !tbaa !7
  %add185 = add nsw i32 %sub183, %231
  %232 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds i32, i32* %232, i32 9
  store i32 %add185, i32* %arrayidx186, align 4, !tbaa !7
  %233 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx187 = getelementptr inbounds i32, i32* %233, i32 10
  %234 = load i32, i32* %arrayidx187, align 4, !tbaa !7
  %sub188 = sub nsw i32 0, %234
  %235 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx189 = getelementptr inbounds i32, i32* %235, i32 5
  %236 = load i32, i32* %arrayidx189, align 4, !tbaa !7
  %add190 = add nsw i32 %sub188, %236
  %237 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx191 = getelementptr inbounds i32, i32* %237, i32 10
  store i32 %add190, i32* %arrayidx191, align 4, !tbaa !7
  %238 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds i32, i32* %238, i32 11
  %239 = load i32, i32* %arrayidx192, align 4, !tbaa !7
  %sub193 = sub nsw i32 0, %239
  %240 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds i32, i32* %240, i32 4
  %241 = load i32, i32* %arrayidx194, align 4, !tbaa !7
  %add195 = add nsw i32 %sub193, %241
  %242 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx196 = getelementptr inbounds i32, i32* %242, i32 11
  store i32 %add195, i32* %arrayidx196, align 4, !tbaa !7
  %243 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx197 = getelementptr inbounds i32, i32* %243, i32 12
  %244 = load i32, i32* %arrayidx197, align 4, !tbaa !7
  %sub198 = sub nsw i32 0, %244
  %245 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx199 = getelementptr inbounds i32, i32* %245, i32 3
  %246 = load i32, i32* %arrayidx199, align 4, !tbaa !7
  %add200 = add nsw i32 %sub198, %246
  %247 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx201 = getelementptr inbounds i32, i32* %247, i32 12
  store i32 %add200, i32* %arrayidx201, align 4, !tbaa !7
  %248 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx202 = getelementptr inbounds i32, i32* %248, i32 13
  %249 = load i32, i32* %arrayidx202, align 4, !tbaa !7
  %sub203 = sub nsw i32 0, %249
  %250 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx204 = getelementptr inbounds i32, i32* %250, i32 2
  %251 = load i32, i32* %arrayidx204, align 4, !tbaa !7
  %add205 = add nsw i32 %sub203, %251
  %252 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx206 = getelementptr inbounds i32, i32* %252, i32 13
  store i32 %add205, i32* %arrayidx206, align 4, !tbaa !7
  %253 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx207 = getelementptr inbounds i32, i32* %253, i32 14
  %254 = load i32, i32* %arrayidx207, align 4, !tbaa !7
  %sub208 = sub nsw i32 0, %254
  %255 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx209 = getelementptr inbounds i32, i32* %255, i32 1
  %256 = load i32, i32* %arrayidx209, align 4, !tbaa !7
  %add210 = add nsw i32 %sub208, %256
  %257 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx211 = getelementptr inbounds i32, i32* %257, i32 14
  store i32 %add210, i32* %arrayidx211, align 4, !tbaa !7
  %258 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx212 = getelementptr inbounds i32, i32* %258, i32 15
  %259 = load i32, i32* %arrayidx212, align 4, !tbaa !7
  %sub213 = sub nsw i32 0, %259
  %260 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx214 = getelementptr inbounds i32, i32* %260, i32 0
  %261 = load i32, i32* %arrayidx214, align 4, !tbaa !7
  %add215 = add nsw i32 %sub213, %261
  %262 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx216 = getelementptr inbounds i32, i32* %262, i32 15
  store i32 %add215, i32* %arrayidx216, align 4, !tbaa !7
  %263 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx217 = getelementptr inbounds i32, i32* %263, i32 16
  %264 = load i32, i32* %arrayidx217, align 4, !tbaa !7
  %265 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx218 = getelementptr inbounds i32, i32* %265, i32 16
  store i32 %264, i32* %arrayidx218, align 4, !tbaa !7
  %266 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx219 = getelementptr inbounds i32, i32* %266, i32 17
  %267 = load i32, i32* %arrayidx219, align 4, !tbaa !7
  %268 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx220 = getelementptr inbounds i32, i32* %268, i32 17
  store i32 %267, i32* %arrayidx220, align 4, !tbaa !7
  %269 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx221 = getelementptr inbounds i32, i32* %269, i32 18
  %270 = load i32, i32* %arrayidx221, align 4, !tbaa !7
  %271 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx222 = getelementptr inbounds i32, i32* %271, i32 18
  store i32 %270, i32* %arrayidx222, align 4, !tbaa !7
  %272 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx223 = getelementptr inbounds i32, i32* %272, i32 19
  %273 = load i32, i32* %arrayidx223, align 4, !tbaa !7
  %274 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds i32, i32* %274, i32 19
  store i32 %273, i32* %arrayidx224, align 4, !tbaa !7
  %275 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx225 = getelementptr inbounds i32, i32* %275, i32 32
  %276 = load i32, i32* %arrayidx225, align 4, !tbaa !7
  %sub226 = sub nsw i32 0, %276
  %277 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx227 = getelementptr inbounds i32, i32* %277, i32 20
  %278 = load i32, i32* %arrayidx227, align 4, !tbaa !7
  %279 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx228 = getelementptr inbounds i32, i32* %279, i32 32
  %280 = load i32, i32* %arrayidx228, align 4, !tbaa !7
  %281 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx229 = getelementptr inbounds i32, i32* %281, i32 27
  %282 = load i32, i32* %arrayidx229, align 4, !tbaa !7
  %283 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv230 = sext i8 %283 to i32
  %call231 = call i32 @half_btf(i32 %sub226, i32 %278, i32 %280, i32 %282, i32 %conv230)
  %284 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx232 = getelementptr inbounds i32, i32* %284, i32 20
  store i32 %call231, i32* %arrayidx232, align 4, !tbaa !7
  %285 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx233 = getelementptr inbounds i32, i32* %285, i32 32
  %286 = load i32, i32* %arrayidx233, align 4, !tbaa !7
  %sub234 = sub nsw i32 0, %286
  %287 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx235 = getelementptr inbounds i32, i32* %287, i32 21
  %288 = load i32, i32* %arrayidx235, align 4, !tbaa !7
  %289 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds i32, i32* %289, i32 32
  %290 = load i32, i32* %arrayidx236, align 4, !tbaa !7
  %291 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx237 = getelementptr inbounds i32, i32* %291, i32 26
  %292 = load i32, i32* %arrayidx237, align 4, !tbaa !7
  %293 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv238 = sext i8 %293 to i32
  %call239 = call i32 @half_btf(i32 %sub234, i32 %288, i32 %290, i32 %292, i32 %conv238)
  %294 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx240 = getelementptr inbounds i32, i32* %294, i32 21
  store i32 %call239, i32* %arrayidx240, align 4, !tbaa !7
  %295 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx241 = getelementptr inbounds i32, i32* %295, i32 32
  %296 = load i32, i32* %arrayidx241, align 4, !tbaa !7
  %sub242 = sub nsw i32 0, %296
  %297 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx243 = getelementptr inbounds i32, i32* %297, i32 22
  %298 = load i32, i32* %arrayidx243, align 4, !tbaa !7
  %299 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx244 = getelementptr inbounds i32, i32* %299, i32 32
  %300 = load i32, i32* %arrayidx244, align 4, !tbaa !7
  %301 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx245 = getelementptr inbounds i32, i32* %301, i32 25
  %302 = load i32, i32* %arrayidx245, align 4, !tbaa !7
  %303 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv246 = sext i8 %303 to i32
  %call247 = call i32 @half_btf(i32 %sub242, i32 %298, i32 %300, i32 %302, i32 %conv246)
  %304 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx248 = getelementptr inbounds i32, i32* %304, i32 22
  store i32 %call247, i32* %arrayidx248, align 4, !tbaa !7
  %305 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx249 = getelementptr inbounds i32, i32* %305, i32 32
  %306 = load i32, i32* %arrayidx249, align 4, !tbaa !7
  %sub250 = sub nsw i32 0, %306
  %307 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx251 = getelementptr inbounds i32, i32* %307, i32 23
  %308 = load i32, i32* %arrayidx251, align 4, !tbaa !7
  %309 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx252 = getelementptr inbounds i32, i32* %309, i32 32
  %310 = load i32, i32* %arrayidx252, align 4, !tbaa !7
  %311 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx253 = getelementptr inbounds i32, i32* %311, i32 24
  %312 = load i32, i32* %arrayidx253, align 4, !tbaa !7
  %313 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv254 = sext i8 %313 to i32
  %call255 = call i32 @half_btf(i32 %sub250, i32 %308, i32 %310, i32 %312, i32 %conv254)
  %314 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx256 = getelementptr inbounds i32, i32* %314, i32 23
  store i32 %call255, i32* %arrayidx256, align 4, !tbaa !7
  %315 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx257 = getelementptr inbounds i32, i32* %315, i32 32
  %316 = load i32, i32* %arrayidx257, align 4, !tbaa !7
  %317 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx258 = getelementptr inbounds i32, i32* %317, i32 24
  %318 = load i32, i32* %arrayidx258, align 4, !tbaa !7
  %319 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx259 = getelementptr inbounds i32, i32* %319, i32 32
  %320 = load i32, i32* %arrayidx259, align 4, !tbaa !7
  %321 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx260 = getelementptr inbounds i32, i32* %321, i32 23
  %322 = load i32, i32* %arrayidx260, align 4, !tbaa !7
  %323 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv261 = sext i8 %323 to i32
  %call262 = call i32 @half_btf(i32 %316, i32 %318, i32 %320, i32 %322, i32 %conv261)
  %324 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx263 = getelementptr inbounds i32, i32* %324, i32 24
  store i32 %call262, i32* %arrayidx263, align 4, !tbaa !7
  %325 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx264 = getelementptr inbounds i32, i32* %325, i32 32
  %326 = load i32, i32* %arrayidx264, align 4, !tbaa !7
  %327 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx265 = getelementptr inbounds i32, i32* %327, i32 25
  %328 = load i32, i32* %arrayidx265, align 4, !tbaa !7
  %329 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx266 = getelementptr inbounds i32, i32* %329, i32 32
  %330 = load i32, i32* %arrayidx266, align 4, !tbaa !7
  %331 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx267 = getelementptr inbounds i32, i32* %331, i32 22
  %332 = load i32, i32* %arrayidx267, align 4, !tbaa !7
  %333 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv268 = sext i8 %333 to i32
  %call269 = call i32 @half_btf(i32 %326, i32 %328, i32 %330, i32 %332, i32 %conv268)
  %334 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx270 = getelementptr inbounds i32, i32* %334, i32 25
  store i32 %call269, i32* %arrayidx270, align 4, !tbaa !7
  %335 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx271 = getelementptr inbounds i32, i32* %335, i32 32
  %336 = load i32, i32* %arrayidx271, align 4, !tbaa !7
  %337 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx272 = getelementptr inbounds i32, i32* %337, i32 26
  %338 = load i32, i32* %arrayidx272, align 4, !tbaa !7
  %339 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx273 = getelementptr inbounds i32, i32* %339, i32 32
  %340 = load i32, i32* %arrayidx273, align 4, !tbaa !7
  %341 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx274 = getelementptr inbounds i32, i32* %341, i32 21
  %342 = load i32, i32* %arrayidx274, align 4, !tbaa !7
  %343 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv275 = sext i8 %343 to i32
  %call276 = call i32 @half_btf(i32 %336, i32 %338, i32 %340, i32 %342, i32 %conv275)
  %344 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx277 = getelementptr inbounds i32, i32* %344, i32 26
  store i32 %call276, i32* %arrayidx277, align 4, !tbaa !7
  %345 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx278 = getelementptr inbounds i32, i32* %345, i32 32
  %346 = load i32, i32* %arrayidx278, align 4, !tbaa !7
  %347 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx279 = getelementptr inbounds i32, i32* %347, i32 27
  %348 = load i32, i32* %arrayidx279, align 4, !tbaa !7
  %349 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx280 = getelementptr inbounds i32, i32* %349, i32 32
  %350 = load i32, i32* %arrayidx280, align 4, !tbaa !7
  %351 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx281 = getelementptr inbounds i32, i32* %351, i32 20
  %352 = load i32, i32* %arrayidx281, align 4, !tbaa !7
  %353 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv282 = sext i8 %353 to i32
  %call283 = call i32 @half_btf(i32 %346, i32 %348, i32 %350, i32 %352, i32 %conv282)
  %354 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx284 = getelementptr inbounds i32, i32* %354, i32 27
  store i32 %call283, i32* %arrayidx284, align 4, !tbaa !7
  %355 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx285 = getelementptr inbounds i32, i32* %355, i32 28
  %356 = load i32, i32* %arrayidx285, align 4, !tbaa !7
  %357 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx286 = getelementptr inbounds i32, i32* %357, i32 28
  store i32 %356, i32* %arrayidx286, align 4, !tbaa !7
  %358 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx287 = getelementptr inbounds i32, i32* %358, i32 29
  %359 = load i32, i32* %arrayidx287, align 4, !tbaa !7
  %360 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx288 = getelementptr inbounds i32, i32* %360, i32 29
  store i32 %359, i32* %arrayidx288, align 4, !tbaa !7
  %361 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx289 = getelementptr inbounds i32, i32* %361, i32 30
  %362 = load i32, i32* %arrayidx289, align 4, !tbaa !7
  %363 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx290 = getelementptr inbounds i32, i32* %363, i32 30
  store i32 %362, i32* %arrayidx290, align 4, !tbaa !7
  %364 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx291 = getelementptr inbounds i32, i32* %364, i32 31
  %365 = load i32, i32* %arrayidx291, align 4, !tbaa !7
  %366 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx292 = getelementptr inbounds i32, i32* %366, i32 31
  store i32 %365, i32* %arrayidx292, align 4, !tbaa !7
  %367 = load i32, i32* %stage, align 4, !tbaa !7
  %368 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %369 = load i32*, i32** %bf1, align 4, !tbaa !2
  %370 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %371 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx293 = getelementptr inbounds i8, i8* %370, i32 %371
  %372 = load i8, i8* %arrayidx293, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %367, i32* %368, i32* %369, i32 32, i8 signext %372)
  %373 = load i32, i32* %stage, align 4, !tbaa !7
  %inc294 = add nsw i32 %373, 1
  store i32 %inc294, i32* %stage, align 4, !tbaa !7
  %374 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv295 = sext i8 %374 to i32
  %call296 = call i32* @cospi_arr(i32 %conv295)
  store i32* %call296, i32** %cospi, align 4, !tbaa !2
  %arraydecay297 = getelementptr inbounds [32 x i32], [32 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay297, i32** %bf0, align 4, !tbaa !2
  %375 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %375, i32** %bf1, align 4, !tbaa !2
  %376 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx298 = getelementptr inbounds i32, i32* %376, i32 0
  %377 = load i32, i32* %arrayidx298, align 4, !tbaa !7
  %378 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx299 = getelementptr inbounds i32, i32* %378, i32 7
  %379 = load i32, i32* %arrayidx299, align 4, !tbaa !7
  %add300 = add nsw i32 %377, %379
  %380 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx301 = getelementptr inbounds i32, i32* %380, i32 0
  store i32 %add300, i32* %arrayidx301, align 4, !tbaa !7
  %381 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx302 = getelementptr inbounds i32, i32* %381, i32 1
  %382 = load i32, i32* %arrayidx302, align 4, !tbaa !7
  %383 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx303 = getelementptr inbounds i32, i32* %383, i32 6
  %384 = load i32, i32* %arrayidx303, align 4, !tbaa !7
  %add304 = add nsw i32 %382, %384
  %385 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx305 = getelementptr inbounds i32, i32* %385, i32 1
  store i32 %add304, i32* %arrayidx305, align 4, !tbaa !7
  %386 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx306 = getelementptr inbounds i32, i32* %386, i32 2
  %387 = load i32, i32* %arrayidx306, align 4, !tbaa !7
  %388 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx307 = getelementptr inbounds i32, i32* %388, i32 5
  %389 = load i32, i32* %arrayidx307, align 4, !tbaa !7
  %add308 = add nsw i32 %387, %389
  %390 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx309 = getelementptr inbounds i32, i32* %390, i32 2
  store i32 %add308, i32* %arrayidx309, align 4, !tbaa !7
  %391 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx310 = getelementptr inbounds i32, i32* %391, i32 3
  %392 = load i32, i32* %arrayidx310, align 4, !tbaa !7
  %393 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx311 = getelementptr inbounds i32, i32* %393, i32 4
  %394 = load i32, i32* %arrayidx311, align 4, !tbaa !7
  %add312 = add nsw i32 %392, %394
  %395 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx313 = getelementptr inbounds i32, i32* %395, i32 3
  store i32 %add312, i32* %arrayidx313, align 4, !tbaa !7
  %396 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx314 = getelementptr inbounds i32, i32* %396, i32 4
  %397 = load i32, i32* %arrayidx314, align 4, !tbaa !7
  %sub315 = sub nsw i32 0, %397
  %398 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx316 = getelementptr inbounds i32, i32* %398, i32 3
  %399 = load i32, i32* %arrayidx316, align 4, !tbaa !7
  %add317 = add nsw i32 %sub315, %399
  %400 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx318 = getelementptr inbounds i32, i32* %400, i32 4
  store i32 %add317, i32* %arrayidx318, align 4, !tbaa !7
  %401 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx319 = getelementptr inbounds i32, i32* %401, i32 5
  %402 = load i32, i32* %arrayidx319, align 4, !tbaa !7
  %sub320 = sub nsw i32 0, %402
  %403 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx321 = getelementptr inbounds i32, i32* %403, i32 2
  %404 = load i32, i32* %arrayidx321, align 4, !tbaa !7
  %add322 = add nsw i32 %sub320, %404
  %405 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx323 = getelementptr inbounds i32, i32* %405, i32 5
  store i32 %add322, i32* %arrayidx323, align 4, !tbaa !7
  %406 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx324 = getelementptr inbounds i32, i32* %406, i32 6
  %407 = load i32, i32* %arrayidx324, align 4, !tbaa !7
  %sub325 = sub nsw i32 0, %407
  %408 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx326 = getelementptr inbounds i32, i32* %408, i32 1
  %409 = load i32, i32* %arrayidx326, align 4, !tbaa !7
  %add327 = add nsw i32 %sub325, %409
  %410 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx328 = getelementptr inbounds i32, i32* %410, i32 6
  store i32 %add327, i32* %arrayidx328, align 4, !tbaa !7
  %411 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx329 = getelementptr inbounds i32, i32* %411, i32 7
  %412 = load i32, i32* %arrayidx329, align 4, !tbaa !7
  %sub330 = sub nsw i32 0, %412
  %413 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx331 = getelementptr inbounds i32, i32* %413, i32 0
  %414 = load i32, i32* %arrayidx331, align 4, !tbaa !7
  %add332 = add nsw i32 %sub330, %414
  %415 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx333 = getelementptr inbounds i32, i32* %415, i32 7
  store i32 %add332, i32* %arrayidx333, align 4, !tbaa !7
  %416 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx334 = getelementptr inbounds i32, i32* %416, i32 8
  %417 = load i32, i32* %arrayidx334, align 4, !tbaa !7
  %418 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx335 = getelementptr inbounds i32, i32* %418, i32 8
  store i32 %417, i32* %arrayidx335, align 4, !tbaa !7
  %419 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx336 = getelementptr inbounds i32, i32* %419, i32 9
  %420 = load i32, i32* %arrayidx336, align 4, !tbaa !7
  %421 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx337 = getelementptr inbounds i32, i32* %421, i32 9
  store i32 %420, i32* %arrayidx337, align 4, !tbaa !7
  %422 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx338 = getelementptr inbounds i32, i32* %422, i32 32
  %423 = load i32, i32* %arrayidx338, align 4, !tbaa !7
  %sub339 = sub nsw i32 0, %423
  %424 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx340 = getelementptr inbounds i32, i32* %424, i32 10
  %425 = load i32, i32* %arrayidx340, align 4, !tbaa !7
  %426 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx341 = getelementptr inbounds i32, i32* %426, i32 32
  %427 = load i32, i32* %arrayidx341, align 4, !tbaa !7
  %428 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx342 = getelementptr inbounds i32, i32* %428, i32 13
  %429 = load i32, i32* %arrayidx342, align 4, !tbaa !7
  %430 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv343 = sext i8 %430 to i32
  %call344 = call i32 @half_btf(i32 %sub339, i32 %425, i32 %427, i32 %429, i32 %conv343)
  %431 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx345 = getelementptr inbounds i32, i32* %431, i32 10
  store i32 %call344, i32* %arrayidx345, align 4, !tbaa !7
  %432 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx346 = getelementptr inbounds i32, i32* %432, i32 32
  %433 = load i32, i32* %arrayidx346, align 4, !tbaa !7
  %sub347 = sub nsw i32 0, %433
  %434 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx348 = getelementptr inbounds i32, i32* %434, i32 11
  %435 = load i32, i32* %arrayidx348, align 4, !tbaa !7
  %436 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx349 = getelementptr inbounds i32, i32* %436, i32 32
  %437 = load i32, i32* %arrayidx349, align 4, !tbaa !7
  %438 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx350 = getelementptr inbounds i32, i32* %438, i32 12
  %439 = load i32, i32* %arrayidx350, align 4, !tbaa !7
  %440 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv351 = sext i8 %440 to i32
  %call352 = call i32 @half_btf(i32 %sub347, i32 %435, i32 %437, i32 %439, i32 %conv351)
  %441 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx353 = getelementptr inbounds i32, i32* %441, i32 11
  store i32 %call352, i32* %arrayidx353, align 4, !tbaa !7
  %442 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx354 = getelementptr inbounds i32, i32* %442, i32 32
  %443 = load i32, i32* %arrayidx354, align 4, !tbaa !7
  %444 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx355 = getelementptr inbounds i32, i32* %444, i32 12
  %445 = load i32, i32* %arrayidx355, align 4, !tbaa !7
  %446 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx356 = getelementptr inbounds i32, i32* %446, i32 32
  %447 = load i32, i32* %arrayidx356, align 4, !tbaa !7
  %448 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx357 = getelementptr inbounds i32, i32* %448, i32 11
  %449 = load i32, i32* %arrayidx357, align 4, !tbaa !7
  %450 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv358 = sext i8 %450 to i32
  %call359 = call i32 @half_btf(i32 %443, i32 %445, i32 %447, i32 %449, i32 %conv358)
  %451 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx360 = getelementptr inbounds i32, i32* %451, i32 12
  store i32 %call359, i32* %arrayidx360, align 4, !tbaa !7
  %452 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx361 = getelementptr inbounds i32, i32* %452, i32 32
  %453 = load i32, i32* %arrayidx361, align 4, !tbaa !7
  %454 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx362 = getelementptr inbounds i32, i32* %454, i32 13
  %455 = load i32, i32* %arrayidx362, align 4, !tbaa !7
  %456 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx363 = getelementptr inbounds i32, i32* %456, i32 32
  %457 = load i32, i32* %arrayidx363, align 4, !tbaa !7
  %458 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx364 = getelementptr inbounds i32, i32* %458, i32 10
  %459 = load i32, i32* %arrayidx364, align 4, !tbaa !7
  %460 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv365 = sext i8 %460 to i32
  %call366 = call i32 @half_btf(i32 %453, i32 %455, i32 %457, i32 %459, i32 %conv365)
  %461 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx367 = getelementptr inbounds i32, i32* %461, i32 13
  store i32 %call366, i32* %arrayidx367, align 4, !tbaa !7
  %462 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx368 = getelementptr inbounds i32, i32* %462, i32 14
  %463 = load i32, i32* %arrayidx368, align 4, !tbaa !7
  %464 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx369 = getelementptr inbounds i32, i32* %464, i32 14
  store i32 %463, i32* %arrayidx369, align 4, !tbaa !7
  %465 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx370 = getelementptr inbounds i32, i32* %465, i32 15
  %466 = load i32, i32* %arrayidx370, align 4, !tbaa !7
  %467 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx371 = getelementptr inbounds i32, i32* %467, i32 15
  store i32 %466, i32* %arrayidx371, align 4, !tbaa !7
  %468 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx372 = getelementptr inbounds i32, i32* %468, i32 16
  %469 = load i32, i32* %arrayidx372, align 4, !tbaa !7
  %470 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx373 = getelementptr inbounds i32, i32* %470, i32 23
  %471 = load i32, i32* %arrayidx373, align 4, !tbaa !7
  %add374 = add nsw i32 %469, %471
  %472 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx375 = getelementptr inbounds i32, i32* %472, i32 16
  store i32 %add374, i32* %arrayidx375, align 4, !tbaa !7
  %473 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx376 = getelementptr inbounds i32, i32* %473, i32 17
  %474 = load i32, i32* %arrayidx376, align 4, !tbaa !7
  %475 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx377 = getelementptr inbounds i32, i32* %475, i32 22
  %476 = load i32, i32* %arrayidx377, align 4, !tbaa !7
  %add378 = add nsw i32 %474, %476
  %477 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx379 = getelementptr inbounds i32, i32* %477, i32 17
  store i32 %add378, i32* %arrayidx379, align 4, !tbaa !7
  %478 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx380 = getelementptr inbounds i32, i32* %478, i32 18
  %479 = load i32, i32* %arrayidx380, align 4, !tbaa !7
  %480 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx381 = getelementptr inbounds i32, i32* %480, i32 21
  %481 = load i32, i32* %arrayidx381, align 4, !tbaa !7
  %add382 = add nsw i32 %479, %481
  %482 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx383 = getelementptr inbounds i32, i32* %482, i32 18
  store i32 %add382, i32* %arrayidx383, align 4, !tbaa !7
  %483 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx384 = getelementptr inbounds i32, i32* %483, i32 19
  %484 = load i32, i32* %arrayidx384, align 4, !tbaa !7
  %485 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx385 = getelementptr inbounds i32, i32* %485, i32 20
  %486 = load i32, i32* %arrayidx385, align 4, !tbaa !7
  %add386 = add nsw i32 %484, %486
  %487 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx387 = getelementptr inbounds i32, i32* %487, i32 19
  store i32 %add386, i32* %arrayidx387, align 4, !tbaa !7
  %488 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx388 = getelementptr inbounds i32, i32* %488, i32 20
  %489 = load i32, i32* %arrayidx388, align 4, !tbaa !7
  %sub389 = sub nsw i32 0, %489
  %490 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx390 = getelementptr inbounds i32, i32* %490, i32 19
  %491 = load i32, i32* %arrayidx390, align 4, !tbaa !7
  %add391 = add nsw i32 %sub389, %491
  %492 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx392 = getelementptr inbounds i32, i32* %492, i32 20
  store i32 %add391, i32* %arrayidx392, align 4, !tbaa !7
  %493 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx393 = getelementptr inbounds i32, i32* %493, i32 21
  %494 = load i32, i32* %arrayidx393, align 4, !tbaa !7
  %sub394 = sub nsw i32 0, %494
  %495 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx395 = getelementptr inbounds i32, i32* %495, i32 18
  %496 = load i32, i32* %arrayidx395, align 4, !tbaa !7
  %add396 = add nsw i32 %sub394, %496
  %497 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx397 = getelementptr inbounds i32, i32* %497, i32 21
  store i32 %add396, i32* %arrayidx397, align 4, !tbaa !7
  %498 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx398 = getelementptr inbounds i32, i32* %498, i32 22
  %499 = load i32, i32* %arrayidx398, align 4, !tbaa !7
  %sub399 = sub nsw i32 0, %499
  %500 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx400 = getelementptr inbounds i32, i32* %500, i32 17
  %501 = load i32, i32* %arrayidx400, align 4, !tbaa !7
  %add401 = add nsw i32 %sub399, %501
  %502 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx402 = getelementptr inbounds i32, i32* %502, i32 22
  store i32 %add401, i32* %arrayidx402, align 4, !tbaa !7
  %503 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx403 = getelementptr inbounds i32, i32* %503, i32 23
  %504 = load i32, i32* %arrayidx403, align 4, !tbaa !7
  %sub404 = sub nsw i32 0, %504
  %505 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx405 = getelementptr inbounds i32, i32* %505, i32 16
  %506 = load i32, i32* %arrayidx405, align 4, !tbaa !7
  %add406 = add nsw i32 %sub404, %506
  %507 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx407 = getelementptr inbounds i32, i32* %507, i32 23
  store i32 %add406, i32* %arrayidx407, align 4, !tbaa !7
  %508 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx408 = getelementptr inbounds i32, i32* %508, i32 24
  %509 = load i32, i32* %arrayidx408, align 4, !tbaa !7
  %sub409 = sub nsw i32 0, %509
  %510 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx410 = getelementptr inbounds i32, i32* %510, i32 31
  %511 = load i32, i32* %arrayidx410, align 4, !tbaa !7
  %add411 = add nsw i32 %sub409, %511
  %512 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx412 = getelementptr inbounds i32, i32* %512, i32 24
  store i32 %add411, i32* %arrayidx412, align 4, !tbaa !7
  %513 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx413 = getelementptr inbounds i32, i32* %513, i32 25
  %514 = load i32, i32* %arrayidx413, align 4, !tbaa !7
  %sub414 = sub nsw i32 0, %514
  %515 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx415 = getelementptr inbounds i32, i32* %515, i32 30
  %516 = load i32, i32* %arrayidx415, align 4, !tbaa !7
  %add416 = add nsw i32 %sub414, %516
  %517 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx417 = getelementptr inbounds i32, i32* %517, i32 25
  store i32 %add416, i32* %arrayidx417, align 4, !tbaa !7
  %518 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx418 = getelementptr inbounds i32, i32* %518, i32 26
  %519 = load i32, i32* %arrayidx418, align 4, !tbaa !7
  %sub419 = sub nsw i32 0, %519
  %520 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx420 = getelementptr inbounds i32, i32* %520, i32 29
  %521 = load i32, i32* %arrayidx420, align 4, !tbaa !7
  %add421 = add nsw i32 %sub419, %521
  %522 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx422 = getelementptr inbounds i32, i32* %522, i32 26
  store i32 %add421, i32* %arrayidx422, align 4, !tbaa !7
  %523 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx423 = getelementptr inbounds i32, i32* %523, i32 27
  %524 = load i32, i32* %arrayidx423, align 4, !tbaa !7
  %sub424 = sub nsw i32 0, %524
  %525 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx425 = getelementptr inbounds i32, i32* %525, i32 28
  %526 = load i32, i32* %arrayidx425, align 4, !tbaa !7
  %add426 = add nsw i32 %sub424, %526
  %527 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx427 = getelementptr inbounds i32, i32* %527, i32 27
  store i32 %add426, i32* %arrayidx427, align 4, !tbaa !7
  %528 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx428 = getelementptr inbounds i32, i32* %528, i32 28
  %529 = load i32, i32* %arrayidx428, align 4, !tbaa !7
  %530 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx429 = getelementptr inbounds i32, i32* %530, i32 27
  %531 = load i32, i32* %arrayidx429, align 4, !tbaa !7
  %add430 = add nsw i32 %529, %531
  %532 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx431 = getelementptr inbounds i32, i32* %532, i32 28
  store i32 %add430, i32* %arrayidx431, align 4, !tbaa !7
  %533 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx432 = getelementptr inbounds i32, i32* %533, i32 29
  %534 = load i32, i32* %arrayidx432, align 4, !tbaa !7
  %535 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx433 = getelementptr inbounds i32, i32* %535, i32 26
  %536 = load i32, i32* %arrayidx433, align 4, !tbaa !7
  %add434 = add nsw i32 %534, %536
  %537 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx435 = getelementptr inbounds i32, i32* %537, i32 29
  store i32 %add434, i32* %arrayidx435, align 4, !tbaa !7
  %538 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx436 = getelementptr inbounds i32, i32* %538, i32 30
  %539 = load i32, i32* %arrayidx436, align 4, !tbaa !7
  %540 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx437 = getelementptr inbounds i32, i32* %540, i32 25
  %541 = load i32, i32* %arrayidx437, align 4, !tbaa !7
  %add438 = add nsw i32 %539, %541
  %542 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx439 = getelementptr inbounds i32, i32* %542, i32 30
  store i32 %add438, i32* %arrayidx439, align 4, !tbaa !7
  %543 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx440 = getelementptr inbounds i32, i32* %543, i32 31
  %544 = load i32, i32* %arrayidx440, align 4, !tbaa !7
  %545 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx441 = getelementptr inbounds i32, i32* %545, i32 24
  %546 = load i32, i32* %arrayidx441, align 4, !tbaa !7
  %add442 = add nsw i32 %544, %546
  %547 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx443 = getelementptr inbounds i32, i32* %547, i32 31
  store i32 %add442, i32* %arrayidx443, align 4, !tbaa !7
  %548 = load i32, i32* %stage, align 4, !tbaa !7
  %549 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %550 = load i32*, i32** %bf1, align 4, !tbaa !2
  %551 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %552 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx444 = getelementptr inbounds i8, i8* %551, i32 %552
  %553 = load i8, i8* %arrayidx444, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %548, i32* %549, i32* %550, i32 32, i8 signext %553)
  %554 = load i32, i32* %stage, align 4, !tbaa !7
  %inc445 = add nsw i32 %554, 1
  store i32 %inc445, i32* %stage, align 4, !tbaa !7
  %555 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv446 = sext i8 %555 to i32
  %call447 = call i32* @cospi_arr(i32 %conv446)
  store i32* %call447, i32** %cospi, align 4, !tbaa !2
  %556 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %556, i32** %bf0, align 4, !tbaa !2
  %arraydecay448 = getelementptr inbounds [32 x i32], [32 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay448, i32** %bf1, align 4, !tbaa !2
  %557 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx449 = getelementptr inbounds i32, i32* %557, i32 0
  %558 = load i32, i32* %arrayidx449, align 4, !tbaa !7
  %559 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx450 = getelementptr inbounds i32, i32* %559, i32 3
  %560 = load i32, i32* %arrayidx450, align 4, !tbaa !7
  %add451 = add nsw i32 %558, %560
  %561 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx452 = getelementptr inbounds i32, i32* %561, i32 0
  store i32 %add451, i32* %arrayidx452, align 4, !tbaa !7
  %562 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx453 = getelementptr inbounds i32, i32* %562, i32 1
  %563 = load i32, i32* %arrayidx453, align 4, !tbaa !7
  %564 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx454 = getelementptr inbounds i32, i32* %564, i32 2
  %565 = load i32, i32* %arrayidx454, align 4, !tbaa !7
  %add455 = add nsw i32 %563, %565
  %566 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx456 = getelementptr inbounds i32, i32* %566, i32 1
  store i32 %add455, i32* %arrayidx456, align 4, !tbaa !7
  %567 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx457 = getelementptr inbounds i32, i32* %567, i32 2
  %568 = load i32, i32* %arrayidx457, align 4, !tbaa !7
  %sub458 = sub nsw i32 0, %568
  %569 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx459 = getelementptr inbounds i32, i32* %569, i32 1
  %570 = load i32, i32* %arrayidx459, align 4, !tbaa !7
  %add460 = add nsw i32 %sub458, %570
  %571 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx461 = getelementptr inbounds i32, i32* %571, i32 2
  store i32 %add460, i32* %arrayidx461, align 4, !tbaa !7
  %572 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx462 = getelementptr inbounds i32, i32* %572, i32 3
  %573 = load i32, i32* %arrayidx462, align 4, !tbaa !7
  %sub463 = sub nsw i32 0, %573
  %574 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx464 = getelementptr inbounds i32, i32* %574, i32 0
  %575 = load i32, i32* %arrayidx464, align 4, !tbaa !7
  %add465 = add nsw i32 %sub463, %575
  %576 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx466 = getelementptr inbounds i32, i32* %576, i32 3
  store i32 %add465, i32* %arrayidx466, align 4, !tbaa !7
  %577 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx467 = getelementptr inbounds i32, i32* %577, i32 4
  %578 = load i32, i32* %arrayidx467, align 4, !tbaa !7
  %579 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx468 = getelementptr inbounds i32, i32* %579, i32 4
  store i32 %578, i32* %arrayidx468, align 4, !tbaa !7
  %580 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx469 = getelementptr inbounds i32, i32* %580, i32 32
  %581 = load i32, i32* %arrayidx469, align 4, !tbaa !7
  %sub470 = sub nsw i32 0, %581
  %582 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx471 = getelementptr inbounds i32, i32* %582, i32 5
  %583 = load i32, i32* %arrayidx471, align 4, !tbaa !7
  %584 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx472 = getelementptr inbounds i32, i32* %584, i32 32
  %585 = load i32, i32* %arrayidx472, align 4, !tbaa !7
  %586 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx473 = getelementptr inbounds i32, i32* %586, i32 6
  %587 = load i32, i32* %arrayidx473, align 4, !tbaa !7
  %588 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv474 = sext i8 %588 to i32
  %call475 = call i32 @half_btf(i32 %sub470, i32 %583, i32 %585, i32 %587, i32 %conv474)
  %589 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx476 = getelementptr inbounds i32, i32* %589, i32 5
  store i32 %call475, i32* %arrayidx476, align 4, !tbaa !7
  %590 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx477 = getelementptr inbounds i32, i32* %590, i32 32
  %591 = load i32, i32* %arrayidx477, align 4, !tbaa !7
  %592 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx478 = getelementptr inbounds i32, i32* %592, i32 6
  %593 = load i32, i32* %arrayidx478, align 4, !tbaa !7
  %594 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx479 = getelementptr inbounds i32, i32* %594, i32 32
  %595 = load i32, i32* %arrayidx479, align 4, !tbaa !7
  %596 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx480 = getelementptr inbounds i32, i32* %596, i32 5
  %597 = load i32, i32* %arrayidx480, align 4, !tbaa !7
  %598 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv481 = sext i8 %598 to i32
  %call482 = call i32 @half_btf(i32 %591, i32 %593, i32 %595, i32 %597, i32 %conv481)
  %599 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx483 = getelementptr inbounds i32, i32* %599, i32 6
  store i32 %call482, i32* %arrayidx483, align 4, !tbaa !7
  %600 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx484 = getelementptr inbounds i32, i32* %600, i32 7
  %601 = load i32, i32* %arrayidx484, align 4, !tbaa !7
  %602 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx485 = getelementptr inbounds i32, i32* %602, i32 7
  store i32 %601, i32* %arrayidx485, align 4, !tbaa !7
  %603 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx486 = getelementptr inbounds i32, i32* %603, i32 8
  %604 = load i32, i32* %arrayidx486, align 4, !tbaa !7
  %605 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx487 = getelementptr inbounds i32, i32* %605, i32 11
  %606 = load i32, i32* %arrayidx487, align 4, !tbaa !7
  %add488 = add nsw i32 %604, %606
  %607 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx489 = getelementptr inbounds i32, i32* %607, i32 8
  store i32 %add488, i32* %arrayidx489, align 4, !tbaa !7
  %608 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx490 = getelementptr inbounds i32, i32* %608, i32 9
  %609 = load i32, i32* %arrayidx490, align 4, !tbaa !7
  %610 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx491 = getelementptr inbounds i32, i32* %610, i32 10
  %611 = load i32, i32* %arrayidx491, align 4, !tbaa !7
  %add492 = add nsw i32 %609, %611
  %612 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx493 = getelementptr inbounds i32, i32* %612, i32 9
  store i32 %add492, i32* %arrayidx493, align 4, !tbaa !7
  %613 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx494 = getelementptr inbounds i32, i32* %613, i32 10
  %614 = load i32, i32* %arrayidx494, align 4, !tbaa !7
  %sub495 = sub nsw i32 0, %614
  %615 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx496 = getelementptr inbounds i32, i32* %615, i32 9
  %616 = load i32, i32* %arrayidx496, align 4, !tbaa !7
  %add497 = add nsw i32 %sub495, %616
  %617 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx498 = getelementptr inbounds i32, i32* %617, i32 10
  store i32 %add497, i32* %arrayidx498, align 4, !tbaa !7
  %618 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx499 = getelementptr inbounds i32, i32* %618, i32 11
  %619 = load i32, i32* %arrayidx499, align 4, !tbaa !7
  %sub500 = sub nsw i32 0, %619
  %620 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx501 = getelementptr inbounds i32, i32* %620, i32 8
  %621 = load i32, i32* %arrayidx501, align 4, !tbaa !7
  %add502 = add nsw i32 %sub500, %621
  %622 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx503 = getelementptr inbounds i32, i32* %622, i32 11
  store i32 %add502, i32* %arrayidx503, align 4, !tbaa !7
  %623 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx504 = getelementptr inbounds i32, i32* %623, i32 12
  %624 = load i32, i32* %arrayidx504, align 4, !tbaa !7
  %sub505 = sub nsw i32 0, %624
  %625 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx506 = getelementptr inbounds i32, i32* %625, i32 15
  %626 = load i32, i32* %arrayidx506, align 4, !tbaa !7
  %add507 = add nsw i32 %sub505, %626
  %627 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx508 = getelementptr inbounds i32, i32* %627, i32 12
  store i32 %add507, i32* %arrayidx508, align 4, !tbaa !7
  %628 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx509 = getelementptr inbounds i32, i32* %628, i32 13
  %629 = load i32, i32* %arrayidx509, align 4, !tbaa !7
  %sub510 = sub nsw i32 0, %629
  %630 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx511 = getelementptr inbounds i32, i32* %630, i32 14
  %631 = load i32, i32* %arrayidx511, align 4, !tbaa !7
  %add512 = add nsw i32 %sub510, %631
  %632 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx513 = getelementptr inbounds i32, i32* %632, i32 13
  store i32 %add512, i32* %arrayidx513, align 4, !tbaa !7
  %633 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx514 = getelementptr inbounds i32, i32* %633, i32 14
  %634 = load i32, i32* %arrayidx514, align 4, !tbaa !7
  %635 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx515 = getelementptr inbounds i32, i32* %635, i32 13
  %636 = load i32, i32* %arrayidx515, align 4, !tbaa !7
  %add516 = add nsw i32 %634, %636
  %637 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx517 = getelementptr inbounds i32, i32* %637, i32 14
  store i32 %add516, i32* %arrayidx517, align 4, !tbaa !7
  %638 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx518 = getelementptr inbounds i32, i32* %638, i32 15
  %639 = load i32, i32* %arrayidx518, align 4, !tbaa !7
  %640 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx519 = getelementptr inbounds i32, i32* %640, i32 12
  %641 = load i32, i32* %arrayidx519, align 4, !tbaa !7
  %add520 = add nsw i32 %639, %641
  %642 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx521 = getelementptr inbounds i32, i32* %642, i32 15
  store i32 %add520, i32* %arrayidx521, align 4, !tbaa !7
  %643 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx522 = getelementptr inbounds i32, i32* %643, i32 16
  %644 = load i32, i32* %arrayidx522, align 4, !tbaa !7
  %645 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx523 = getelementptr inbounds i32, i32* %645, i32 16
  store i32 %644, i32* %arrayidx523, align 4, !tbaa !7
  %646 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx524 = getelementptr inbounds i32, i32* %646, i32 17
  %647 = load i32, i32* %arrayidx524, align 4, !tbaa !7
  %648 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx525 = getelementptr inbounds i32, i32* %648, i32 17
  store i32 %647, i32* %arrayidx525, align 4, !tbaa !7
  %649 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx526 = getelementptr inbounds i32, i32* %649, i32 16
  %650 = load i32, i32* %arrayidx526, align 4, !tbaa !7
  %sub527 = sub nsw i32 0, %650
  %651 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx528 = getelementptr inbounds i32, i32* %651, i32 18
  %652 = load i32, i32* %arrayidx528, align 4, !tbaa !7
  %653 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx529 = getelementptr inbounds i32, i32* %653, i32 48
  %654 = load i32, i32* %arrayidx529, align 4, !tbaa !7
  %655 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx530 = getelementptr inbounds i32, i32* %655, i32 29
  %656 = load i32, i32* %arrayidx530, align 4, !tbaa !7
  %657 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv531 = sext i8 %657 to i32
  %call532 = call i32 @half_btf(i32 %sub527, i32 %652, i32 %654, i32 %656, i32 %conv531)
  %658 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx533 = getelementptr inbounds i32, i32* %658, i32 18
  store i32 %call532, i32* %arrayidx533, align 4, !tbaa !7
  %659 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx534 = getelementptr inbounds i32, i32* %659, i32 16
  %660 = load i32, i32* %arrayidx534, align 4, !tbaa !7
  %sub535 = sub nsw i32 0, %660
  %661 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx536 = getelementptr inbounds i32, i32* %661, i32 19
  %662 = load i32, i32* %arrayidx536, align 4, !tbaa !7
  %663 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx537 = getelementptr inbounds i32, i32* %663, i32 48
  %664 = load i32, i32* %arrayidx537, align 4, !tbaa !7
  %665 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx538 = getelementptr inbounds i32, i32* %665, i32 28
  %666 = load i32, i32* %arrayidx538, align 4, !tbaa !7
  %667 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv539 = sext i8 %667 to i32
  %call540 = call i32 @half_btf(i32 %sub535, i32 %662, i32 %664, i32 %666, i32 %conv539)
  %668 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx541 = getelementptr inbounds i32, i32* %668, i32 19
  store i32 %call540, i32* %arrayidx541, align 4, !tbaa !7
  %669 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx542 = getelementptr inbounds i32, i32* %669, i32 48
  %670 = load i32, i32* %arrayidx542, align 4, !tbaa !7
  %sub543 = sub nsw i32 0, %670
  %671 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx544 = getelementptr inbounds i32, i32* %671, i32 20
  %672 = load i32, i32* %arrayidx544, align 4, !tbaa !7
  %673 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx545 = getelementptr inbounds i32, i32* %673, i32 16
  %674 = load i32, i32* %arrayidx545, align 4, !tbaa !7
  %sub546 = sub nsw i32 0, %674
  %675 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx547 = getelementptr inbounds i32, i32* %675, i32 27
  %676 = load i32, i32* %arrayidx547, align 4, !tbaa !7
  %677 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv548 = sext i8 %677 to i32
  %call549 = call i32 @half_btf(i32 %sub543, i32 %672, i32 %sub546, i32 %676, i32 %conv548)
  %678 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx550 = getelementptr inbounds i32, i32* %678, i32 20
  store i32 %call549, i32* %arrayidx550, align 4, !tbaa !7
  %679 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx551 = getelementptr inbounds i32, i32* %679, i32 48
  %680 = load i32, i32* %arrayidx551, align 4, !tbaa !7
  %sub552 = sub nsw i32 0, %680
  %681 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx553 = getelementptr inbounds i32, i32* %681, i32 21
  %682 = load i32, i32* %arrayidx553, align 4, !tbaa !7
  %683 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx554 = getelementptr inbounds i32, i32* %683, i32 16
  %684 = load i32, i32* %arrayidx554, align 4, !tbaa !7
  %sub555 = sub nsw i32 0, %684
  %685 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx556 = getelementptr inbounds i32, i32* %685, i32 26
  %686 = load i32, i32* %arrayidx556, align 4, !tbaa !7
  %687 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv557 = sext i8 %687 to i32
  %call558 = call i32 @half_btf(i32 %sub552, i32 %682, i32 %sub555, i32 %686, i32 %conv557)
  %688 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx559 = getelementptr inbounds i32, i32* %688, i32 21
  store i32 %call558, i32* %arrayidx559, align 4, !tbaa !7
  %689 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx560 = getelementptr inbounds i32, i32* %689, i32 22
  %690 = load i32, i32* %arrayidx560, align 4, !tbaa !7
  %691 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx561 = getelementptr inbounds i32, i32* %691, i32 22
  store i32 %690, i32* %arrayidx561, align 4, !tbaa !7
  %692 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx562 = getelementptr inbounds i32, i32* %692, i32 23
  %693 = load i32, i32* %arrayidx562, align 4, !tbaa !7
  %694 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx563 = getelementptr inbounds i32, i32* %694, i32 23
  store i32 %693, i32* %arrayidx563, align 4, !tbaa !7
  %695 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx564 = getelementptr inbounds i32, i32* %695, i32 24
  %696 = load i32, i32* %arrayidx564, align 4, !tbaa !7
  %697 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx565 = getelementptr inbounds i32, i32* %697, i32 24
  store i32 %696, i32* %arrayidx565, align 4, !tbaa !7
  %698 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx566 = getelementptr inbounds i32, i32* %698, i32 25
  %699 = load i32, i32* %arrayidx566, align 4, !tbaa !7
  %700 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx567 = getelementptr inbounds i32, i32* %700, i32 25
  store i32 %699, i32* %arrayidx567, align 4, !tbaa !7
  %701 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx568 = getelementptr inbounds i32, i32* %701, i32 48
  %702 = load i32, i32* %arrayidx568, align 4, !tbaa !7
  %703 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx569 = getelementptr inbounds i32, i32* %703, i32 26
  %704 = load i32, i32* %arrayidx569, align 4, !tbaa !7
  %705 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx570 = getelementptr inbounds i32, i32* %705, i32 16
  %706 = load i32, i32* %arrayidx570, align 4, !tbaa !7
  %sub571 = sub nsw i32 0, %706
  %707 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx572 = getelementptr inbounds i32, i32* %707, i32 21
  %708 = load i32, i32* %arrayidx572, align 4, !tbaa !7
  %709 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv573 = sext i8 %709 to i32
  %call574 = call i32 @half_btf(i32 %702, i32 %704, i32 %sub571, i32 %708, i32 %conv573)
  %710 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx575 = getelementptr inbounds i32, i32* %710, i32 26
  store i32 %call574, i32* %arrayidx575, align 4, !tbaa !7
  %711 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx576 = getelementptr inbounds i32, i32* %711, i32 48
  %712 = load i32, i32* %arrayidx576, align 4, !tbaa !7
  %713 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx577 = getelementptr inbounds i32, i32* %713, i32 27
  %714 = load i32, i32* %arrayidx577, align 4, !tbaa !7
  %715 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx578 = getelementptr inbounds i32, i32* %715, i32 16
  %716 = load i32, i32* %arrayidx578, align 4, !tbaa !7
  %sub579 = sub nsw i32 0, %716
  %717 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx580 = getelementptr inbounds i32, i32* %717, i32 20
  %718 = load i32, i32* %arrayidx580, align 4, !tbaa !7
  %719 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv581 = sext i8 %719 to i32
  %call582 = call i32 @half_btf(i32 %712, i32 %714, i32 %sub579, i32 %718, i32 %conv581)
  %720 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx583 = getelementptr inbounds i32, i32* %720, i32 27
  store i32 %call582, i32* %arrayidx583, align 4, !tbaa !7
  %721 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx584 = getelementptr inbounds i32, i32* %721, i32 16
  %722 = load i32, i32* %arrayidx584, align 4, !tbaa !7
  %723 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx585 = getelementptr inbounds i32, i32* %723, i32 28
  %724 = load i32, i32* %arrayidx585, align 4, !tbaa !7
  %725 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx586 = getelementptr inbounds i32, i32* %725, i32 48
  %726 = load i32, i32* %arrayidx586, align 4, !tbaa !7
  %727 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx587 = getelementptr inbounds i32, i32* %727, i32 19
  %728 = load i32, i32* %arrayidx587, align 4, !tbaa !7
  %729 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv588 = sext i8 %729 to i32
  %call589 = call i32 @half_btf(i32 %722, i32 %724, i32 %726, i32 %728, i32 %conv588)
  %730 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx590 = getelementptr inbounds i32, i32* %730, i32 28
  store i32 %call589, i32* %arrayidx590, align 4, !tbaa !7
  %731 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx591 = getelementptr inbounds i32, i32* %731, i32 16
  %732 = load i32, i32* %arrayidx591, align 4, !tbaa !7
  %733 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx592 = getelementptr inbounds i32, i32* %733, i32 29
  %734 = load i32, i32* %arrayidx592, align 4, !tbaa !7
  %735 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx593 = getelementptr inbounds i32, i32* %735, i32 48
  %736 = load i32, i32* %arrayidx593, align 4, !tbaa !7
  %737 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx594 = getelementptr inbounds i32, i32* %737, i32 18
  %738 = load i32, i32* %arrayidx594, align 4, !tbaa !7
  %739 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv595 = sext i8 %739 to i32
  %call596 = call i32 @half_btf(i32 %732, i32 %734, i32 %736, i32 %738, i32 %conv595)
  %740 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx597 = getelementptr inbounds i32, i32* %740, i32 29
  store i32 %call596, i32* %arrayidx597, align 4, !tbaa !7
  %741 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx598 = getelementptr inbounds i32, i32* %741, i32 30
  %742 = load i32, i32* %arrayidx598, align 4, !tbaa !7
  %743 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx599 = getelementptr inbounds i32, i32* %743, i32 30
  store i32 %742, i32* %arrayidx599, align 4, !tbaa !7
  %744 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx600 = getelementptr inbounds i32, i32* %744, i32 31
  %745 = load i32, i32* %arrayidx600, align 4, !tbaa !7
  %746 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx601 = getelementptr inbounds i32, i32* %746, i32 31
  store i32 %745, i32* %arrayidx601, align 4, !tbaa !7
  %747 = load i32, i32* %stage, align 4, !tbaa !7
  %748 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %749 = load i32*, i32** %bf1, align 4, !tbaa !2
  %750 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %751 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx602 = getelementptr inbounds i8, i8* %750, i32 %751
  %752 = load i8, i8* %arrayidx602, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %747, i32* %748, i32* %749, i32 32, i8 signext %752)
  %753 = load i32, i32* %stage, align 4, !tbaa !7
  %inc603 = add nsw i32 %753, 1
  store i32 %inc603, i32* %stage, align 4, !tbaa !7
  %754 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv604 = sext i8 %754 to i32
  %call605 = call i32* @cospi_arr(i32 %conv604)
  store i32* %call605, i32** %cospi, align 4, !tbaa !2
  %arraydecay606 = getelementptr inbounds [32 x i32], [32 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay606, i32** %bf0, align 4, !tbaa !2
  %755 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %755, i32** %bf1, align 4, !tbaa !2
  %756 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx607 = getelementptr inbounds i32, i32* %756, i32 32
  %757 = load i32, i32* %arrayidx607, align 4, !tbaa !7
  %758 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx608 = getelementptr inbounds i32, i32* %758, i32 0
  %759 = load i32, i32* %arrayidx608, align 4, !tbaa !7
  %760 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx609 = getelementptr inbounds i32, i32* %760, i32 32
  %761 = load i32, i32* %arrayidx609, align 4, !tbaa !7
  %762 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx610 = getelementptr inbounds i32, i32* %762, i32 1
  %763 = load i32, i32* %arrayidx610, align 4, !tbaa !7
  %764 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv611 = sext i8 %764 to i32
  %call612 = call i32 @half_btf(i32 %757, i32 %759, i32 %761, i32 %763, i32 %conv611)
  %765 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx613 = getelementptr inbounds i32, i32* %765, i32 0
  store i32 %call612, i32* %arrayidx613, align 4, !tbaa !7
  %766 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx614 = getelementptr inbounds i32, i32* %766, i32 32
  %767 = load i32, i32* %arrayidx614, align 4, !tbaa !7
  %sub615 = sub nsw i32 0, %767
  %768 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx616 = getelementptr inbounds i32, i32* %768, i32 1
  %769 = load i32, i32* %arrayidx616, align 4, !tbaa !7
  %770 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx617 = getelementptr inbounds i32, i32* %770, i32 32
  %771 = load i32, i32* %arrayidx617, align 4, !tbaa !7
  %772 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx618 = getelementptr inbounds i32, i32* %772, i32 0
  %773 = load i32, i32* %arrayidx618, align 4, !tbaa !7
  %774 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv619 = sext i8 %774 to i32
  %call620 = call i32 @half_btf(i32 %sub615, i32 %769, i32 %771, i32 %773, i32 %conv619)
  %775 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx621 = getelementptr inbounds i32, i32* %775, i32 1
  store i32 %call620, i32* %arrayidx621, align 4, !tbaa !7
  %776 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx622 = getelementptr inbounds i32, i32* %776, i32 48
  %777 = load i32, i32* %arrayidx622, align 4, !tbaa !7
  %778 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx623 = getelementptr inbounds i32, i32* %778, i32 2
  %779 = load i32, i32* %arrayidx623, align 4, !tbaa !7
  %780 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx624 = getelementptr inbounds i32, i32* %780, i32 16
  %781 = load i32, i32* %arrayidx624, align 4, !tbaa !7
  %782 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx625 = getelementptr inbounds i32, i32* %782, i32 3
  %783 = load i32, i32* %arrayidx625, align 4, !tbaa !7
  %784 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv626 = sext i8 %784 to i32
  %call627 = call i32 @half_btf(i32 %777, i32 %779, i32 %781, i32 %783, i32 %conv626)
  %785 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx628 = getelementptr inbounds i32, i32* %785, i32 2
  store i32 %call627, i32* %arrayidx628, align 4, !tbaa !7
  %786 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx629 = getelementptr inbounds i32, i32* %786, i32 48
  %787 = load i32, i32* %arrayidx629, align 4, !tbaa !7
  %788 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx630 = getelementptr inbounds i32, i32* %788, i32 3
  %789 = load i32, i32* %arrayidx630, align 4, !tbaa !7
  %790 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx631 = getelementptr inbounds i32, i32* %790, i32 16
  %791 = load i32, i32* %arrayidx631, align 4, !tbaa !7
  %sub632 = sub nsw i32 0, %791
  %792 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx633 = getelementptr inbounds i32, i32* %792, i32 2
  %793 = load i32, i32* %arrayidx633, align 4, !tbaa !7
  %794 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv634 = sext i8 %794 to i32
  %call635 = call i32 @half_btf(i32 %787, i32 %789, i32 %sub632, i32 %793, i32 %conv634)
  %795 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx636 = getelementptr inbounds i32, i32* %795, i32 3
  store i32 %call635, i32* %arrayidx636, align 4, !tbaa !7
  %796 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx637 = getelementptr inbounds i32, i32* %796, i32 4
  %797 = load i32, i32* %arrayidx637, align 4, !tbaa !7
  %798 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx638 = getelementptr inbounds i32, i32* %798, i32 5
  %799 = load i32, i32* %arrayidx638, align 4, !tbaa !7
  %add639 = add nsw i32 %797, %799
  %800 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx640 = getelementptr inbounds i32, i32* %800, i32 4
  store i32 %add639, i32* %arrayidx640, align 4, !tbaa !7
  %801 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx641 = getelementptr inbounds i32, i32* %801, i32 5
  %802 = load i32, i32* %arrayidx641, align 4, !tbaa !7
  %sub642 = sub nsw i32 0, %802
  %803 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx643 = getelementptr inbounds i32, i32* %803, i32 4
  %804 = load i32, i32* %arrayidx643, align 4, !tbaa !7
  %add644 = add nsw i32 %sub642, %804
  %805 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx645 = getelementptr inbounds i32, i32* %805, i32 5
  store i32 %add644, i32* %arrayidx645, align 4, !tbaa !7
  %806 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx646 = getelementptr inbounds i32, i32* %806, i32 6
  %807 = load i32, i32* %arrayidx646, align 4, !tbaa !7
  %sub647 = sub nsw i32 0, %807
  %808 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx648 = getelementptr inbounds i32, i32* %808, i32 7
  %809 = load i32, i32* %arrayidx648, align 4, !tbaa !7
  %add649 = add nsw i32 %sub647, %809
  %810 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx650 = getelementptr inbounds i32, i32* %810, i32 6
  store i32 %add649, i32* %arrayidx650, align 4, !tbaa !7
  %811 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx651 = getelementptr inbounds i32, i32* %811, i32 7
  %812 = load i32, i32* %arrayidx651, align 4, !tbaa !7
  %813 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx652 = getelementptr inbounds i32, i32* %813, i32 6
  %814 = load i32, i32* %arrayidx652, align 4, !tbaa !7
  %add653 = add nsw i32 %812, %814
  %815 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx654 = getelementptr inbounds i32, i32* %815, i32 7
  store i32 %add653, i32* %arrayidx654, align 4, !tbaa !7
  %816 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx655 = getelementptr inbounds i32, i32* %816, i32 8
  %817 = load i32, i32* %arrayidx655, align 4, !tbaa !7
  %818 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx656 = getelementptr inbounds i32, i32* %818, i32 8
  store i32 %817, i32* %arrayidx656, align 4, !tbaa !7
  %819 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx657 = getelementptr inbounds i32, i32* %819, i32 16
  %820 = load i32, i32* %arrayidx657, align 4, !tbaa !7
  %sub658 = sub nsw i32 0, %820
  %821 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx659 = getelementptr inbounds i32, i32* %821, i32 9
  %822 = load i32, i32* %arrayidx659, align 4, !tbaa !7
  %823 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx660 = getelementptr inbounds i32, i32* %823, i32 48
  %824 = load i32, i32* %arrayidx660, align 4, !tbaa !7
  %825 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx661 = getelementptr inbounds i32, i32* %825, i32 14
  %826 = load i32, i32* %arrayidx661, align 4, !tbaa !7
  %827 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv662 = sext i8 %827 to i32
  %call663 = call i32 @half_btf(i32 %sub658, i32 %822, i32 %824, i32 %826, i32 %conv662)
  %828 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx664 = getelementptr inbounds i32, i32* %828, i32 9
  store i32 %call663, i32* %arrayidx664, align 4, !tbaa !7
  %829 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx665 = getelementptr inbounds i32, i32* %829, i32 48
  %830 = load i32, i32* %arrayidx665, align 4, !tbaa !7
  %sub666 = sub nsw i32 0, %830
  %831 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx667 = getelementptr inbounds i32, i32* %831, i32 10
  %832 = load i32, i32* %arrayidx667, align 4, !tbaa !7
  %833 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx668 = getelementptr inbounds i32, i32* %833, i32 16
  %834 = load i32, i32* %arrayidx668, align 4, !tbaa !7
  %sub669 = sub nsw i32 0, %834
  %835 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx670 = getelementptr inbounds i32, i32* %835, i32 13
  %836 = load i32, i32* %arrayidx670, align 4, !tbaa !7
  %837 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv671 = sext i8 %837 to i32
  %call672 = call i32 @half_btf(i32 %sub666, i32 %832, i32 %sub669, i32 %836, i32 %conv671)
  %838 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx673 = getelementptr inbounds i32, i32* %838, i32 10
  store i32 %call672, i32* %arrayidx673, align 4, !tbaa !7
  %839 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx674 = getelementptr inbounds i32, i32* %839, i32 11
  %840 = load i32, i32* %arrayidx674, align 4, !tbaa !7
  %841 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx675 = getelementptr inbounds i32, i32* %841, i32 11
  store i32 %840, i32* %arrayidx675, align 4, !tbaa !7
  %842 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx676 = getelementptr inbounds i32, i32* %842, i32 12
  %843 = load i32, i32* %arrayidx676, align 4, !tbaa !7
  %844 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx677 = getelementptr inbounds i32, i32* %844, i32 12
  store i32 %843, i32* %arrayidx677, align 4, !tbaa !7
  %845 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx678 = getelementptr inbounds i32, i32* %845, i32 48
  %846 = load i32, i32* %arrayidx678, align 4, !tbaa !7
  %847 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx679 = getelementptr inbounds i32, i32* %847, i32 13
  %848 = load i32, i32* %arrayidx679, align 4, !tbaa !7
  %849 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx680 = getelementptr inbounds i32, i32* %849, i32 16
  %850 = load i32, i32* %arrayidx680, align 4, !tbaa !7
  %sub681 = sub nsw i32 0, %850
  %851 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx682 = getelementptr inbounds i32, i32* %851, i32 10
  %852 = load i32, i32* %arrayidx682, align 4, !tbaa !7
  %853 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv683 = sext i8 %853 to i32
  %call684 = call i32 @half_btf(i32 %846, i32 %848, i32 %sub681, i32 %852, i32 %conv683)
  %854 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx685 = getelementptr inbounds i32, i32* %854, i32 13
  store i32 %call684, i32* %arrayidx685, align 4, !tbaa !7
  %855 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx686 = getelementptr inbounds i32, i32* %855, i32 16
  %856 = load i32, i32* %arrayidx686, align 4, !tbaa !7
  %857 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx687 = getelementptr inbounds i32, i32* %857, i32 14
  %858 = load i32, i32* %arrayidx687, align 4, !tbaa !7
  %859 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx688 = getelementptr inbounds i32, i32* %859, i32 48
  %860 = load i32, i32* %arrayidx688, align 4, !tbaa !7
  %861 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx689 = getelementptr inbounds i32, i32* %861, i32 9
  %862 = load i32, i32* %arrayidx689, align 4, !tbaa !7
  %863 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv690 = sext i8 %863 to i32
  %call691 = call i32 @half_btf(i32 %856, i32 %858, i32 %860, i32 %862, i32 %conv690)
  %864 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx692 = getelementptr inbounds i32, i32* %864, i32 14
  store i32 %call691, i32* %arrayidx692, align 4, !tbaa !7
  %865 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx693 = getelementptr inbounds i32, i32* %865, i32 15
  %866 = load i32, i32* %arrayidx693, align 4, !tbaa !7
  %867 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx694 = getelementptr inbounds i32, i32* %867, i32 15
  store i32 %866, i32* %arrayidx694, align 4, !tbaa !7
  %868 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx695 = getelementptr inbounds i32, i32* %868, i32 16
  %869 = load i32, i32* %arrayidx695, align 4, !tbaa !7
  %870 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx696 = getelementptr inbounds i32, i32* %870, i32 19
  %871 = load i32, i32* %arrayidx696, align 4, !tbaa !7
  %add697 = add nsw i32 %869, %871
  %872 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx698 = getelementptr inbounds i32, i32* %872, i32 16
  store i32 %add697, i32* %arrayidx698, align 4, !tbaa !7
  %873 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx699 = getelementptr inbounds i32, i32* %873, i32 17
  %874 = load i32, i32* %arrayidx699, align 4, !tbaa !7
  %875 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx700 = getelementptr inbounds i32, i32* %875, i32 18
  %876 = load i32, i32* %arrayidx700, align 4, !tbaa !7
  %add701 = add nsw i32 %874, %876
  %877 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx702 = getelementptr inbounds i32, i32* %877, i32 17
  store i32 %add701, i32* %arrayidx702, align 4, !tbaa !7
  %878 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx703 = getelementptr inbounds i32, i32* %878, i32 18
  %879 = load i32, i32* %arrayidx703, align 4, !tbaa !7
  %sub704 = sub nsw i32 0, %879
  %880 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx705 = getelementptr inbounds i32, i32* %880, i32 17
  %881 = load i32, i32* %arrayidx705, align 4, !tbaa !7
  %add706 = add nsw i32 %sub704, %881
  %882 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx707 = getelementptr inbounds i32, i32* %882, i32 18
  store i32 %add706, i32* %arrayidx707, align 4, !tbaa !7
  %883 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx708 = getelementptr inbounds i32, i32* %883, i32 19
  %884 = load i32, i32* %arrayidx708, align 4, !tbaa !7
  %sub709 = sub nsw i32 0, %884
  %885 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx710 = getelementptr inbounds i32, i32* %885, i32 16
  %886 = load i32, i32* %arrayidx710, align 4, !tbaa !7
  %add711 = add nsw i32 %sub709, %886
  %887 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx712 = getelementptr inbounds i32, i32* %887, i32 19
  store i32 %add711, i32* %arrayidx712, align 4, !tbaa !7
  %888 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx713 = getelementptr inbounds i32, i32* %888, i32 20
  %889 = load i32, i32* %arrayidx713, align 4, !tbaa !7
  %sub714 = sub nsw i32 0, %889
  %890 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx715 = getelementptr inbounds i32, i32* %890, i32 23
  %891 = load i32, i32* %arrayidx715, align 4, !tbaa !7
  %add716 = add nsw i32 %sub714, %891
  %892 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx717 = getelementptr inbounds i32, i32* %892, i32 20
  store i32 %add716, i32* %arrayidx717, align 4, !tbaa !7
  %893 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx718 = getelementptr inbounds i32, i32* %893, i32 21
  %894 = load i32, i32* %arrayidx718, align 4, !tbaa !7
  %sub719 = sub nsw i32 0, %894
  %895 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx720 = getelementptr inbounds i32, i32* %895, i32 22
  %896 = load i32, i32* %arrayidx720, align 4, !tbaa !7
  %add721 = add nsw i32 %sub719, %896
  %897 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx722 = getelementptr inbounds i32, i32* %897, i32 21
  store i32 %add721, i32* %arrayidx722, align 4, !tbaa !7
  %898 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx723 = getelementptr inbounds i32, i32* %898, i32 22
  %899 = load i32, i32* %arrayidx723, align 4, !tbaa !7
  %900 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx724 = getelementptr inbounds i32, i32* %900, i32 21
  %901 = load i32, i32* %arrayidx724, align 4, !tbaa !7
  %add725 = add nsw i32 %899, %901
  %902 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx726 = getelementptr inbounds i32, i32* %902, i32 22
  store i32 %add725, i32* %arrayidx726, align 4, !tbaa !7
  %903 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx727 = getelementptr inbounds i32, i32* %903, i32 23
  %904 = load i32, i32* %arrayidx727, align 4, !tbaa !7
  %905 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx728 = getelementptr inbounds i32, i32* %905, i32 20
  %906 = load i32, i32* %arrayidx728, align 4, !tbaa !7
  %add729 = add nsw i32 %904, %906
  %907 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx730 = getelementptr inbounds i32, i32* %907, i32 23
  store i32 %add729, i32* %arrayidx730, align 4, !tbaa !7
  %908 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx731 = getelementptr inbounds i32, i32* %908, i32 24
  %909 = load i32, i32* %arrayidx731, align 4, !tbaa !7
  %910 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx732 = getelementptr inbounds i32, i32* %910, i32 27
  %911 = load i32, i32* %arrayidx732, align 4, !tbaa !7
  %add733 = add nsw i32 %909, %911
  %912 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx734 = getelementptr inbounds i32, i32* %912, i32 24
  store i32 %add733, i32* %arrayidx734, align 4, !tbaa !7
  %913 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx735 = getelementptr inbounds i32, i32* %913, i32 25
  %914 = load i32, i32* %arrayidx735, align 4, !tbaa !7
  %915 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx736 = getelementptr inbounds i32, i32* %915, i32 26
  %916 = load i32, i32* %arrayidx736, align 4, !tbaa !7
  %add737 = add nsw i32 %914, %916
  %917 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx738 = getelementptr inbounds i32, i32* %917, i32 25
  store i32 %add737, i32* %arrayidx738, align 4, !tbaa !7
  %918 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx739 = getelementptr inbounds i32, i32* %918, i32 26
  %919 = load i32, i32* %arrayidx739, align 4, !tbaa !7
  %sub740 = sub nsw i32 0, %919
  %920 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx741 = getelementptr inbounds i32, i32* %920, i32 25
  %921 = load i32, i32* %arrayidx741, align 4, !tbaa !7
  %add742 = add nsw i32 %sub740, %921
  %922 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx743 = getelementptr inbounds i32, i32* %922, i32 26
  store i32 %add742, i32* %arrayidx743, align 4, !tbaa !7
  %923 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx744 = getelementptr inbounds i32, i32* %923, i32 27
  %924 = load i32, i32* %arrayidx744, align 4, !tbaa !7
  %sub745 = sub nsw i32 0, %924
  %925 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx746 = getelementptr inbounds i32, i32* %925, i32 24
  %926 = load i32, i32* %arrayidx746, align 4, !tbaa !7
  %add747 = add nsw i32 %sub745, %926
  %927 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx748 = getelementptr inbounds i32, i32* %927, i32 27
  store i32 %add747, i32* %arrayidx748, align 4, !tbaa !7
  %928 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx749 = getelementptr inbounds i32, i32* %928, i32 28
  %929 = load i32, i32* %arrayidx749, align 4, !tbaa !7
  %sub750 = sub nsw i32 0, %929
  %930 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx751 = getelementptr inbounds i32, i32* %930, i32 31
  %931 = load i32, i32* %arrayidx751, align 4, !tbaa !7
  %add752 = add nsw i32 %sub750, %931
  %932 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx753 = getelementptr inbounds i32, i32* %932, i32 28
  store i32 %add752, i32* %arrayidx753, align 4, !tbaa !7
  %933 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx754 = getelementptr inbounds i32, i32* %933, i32 29
  %934 = load i32, i32* %arrayidx754, align 4, !tbaa !7
  %sub755 = sub nsw i32 0, %934
  %935 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx756 = getelementptr inbounds i32, i32* %935, i32 30
  %936 = load i32, i32* %arrayidx756, align 4, !tbaa !7
  %add757 = add nsw i32 %sub755, %936
  %937 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx758 = getelementptr inbounds i32, i32* %937, i32 29
  store i32 %add757, i32* %arrayidx758, align 4, !tbaa !7
  %938 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx759 = getelementptr inbounds i32, i32* %938, i32 30
  %939 = load i32, i32* %arrayidx759, align 4, !tbaa !7
  %940 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx760 = getelementptr inbounds i32, i32* %940, i32 29
  %941 = load i32, i32* %arrayidx760, align 4, !tbaa !7
  %add761 = add nsw i32 %939, %941
  %942 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx762 = getelementptr inbounds i32, i32* %942, i32 30
  store i32 %add761, i32* %arrayidx762, align 4, !tbaa !7
  %943 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx763 = getelementptr inbounds i32, i32* %943, i32 31
  %944 = load i32, i32* %arrayidx763, align 4, !tbaa !7
  %945 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx764 = getelementptr inbounds i32, i32* %945, i32 28
  %946 = load i32, i32* %arrayidx764, align 4, !tbaa !7
  %add765 = add nsw i32 %944, %946
  %947 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx766 = getelementptr inbounds i32, i32* %947, i32 31
  store i32 %add765, i32* %arrayidx766, align 4, !tbaa !7
  %948 = load i32, i32* %stage, align 4, !tbaa !7
  %949 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %950 = load i32*, i32** %bf1, align 4, !tbaa !2
  %951 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %952 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx767 = getelementptr inbounds i8, i8* %951, i32 %952
  %953 = load i8, i8* %arrayidx767, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %948, i32* %949, i32* %950, i32 32, i8 signext %953)
  %954 = load i32, i32* %stage, align 4, !tbaa !7
  %inc768 = add nsw i32 %954, 1
  store i32 %inc768, i32* %stage, align 4, !tbaa !7
  %955 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv769 = sext i8 %955 to i32
  %call770 = call i32* @cospi_arr(i32 %conv769)
  store i32* %call770, i32** %cospi, align 4, !tbaa !2
  %956 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %956, i32** %bf0, align 4, !tbaa !2
  %arraydecay771 = getelementptr inbounds [32 x i32], [32 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay771, i32** %bf1, align 4, !tbaa !2
  %957 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx772 = getelementptr inbounds i32, i32* %957, i32 0
  %958 = load i32, i32* %arrayidx772, align 4, !tbaa !7
  %959 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx773 = getelementptr inbounds i32, i32* %959, i32 0
  store i32 %958, i32* %arrayidx773, align 4, !tbaa !7
  %960 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx774 = getelementptr inbounds i32, i32* %960, i32 1
  %961 = load i32, i32* %arrayidx774, align 4, !tbaa !7
  %962 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx775 = getelementptr inbounds i32, i32* %962, i32 1
  store i32 %961, i32* %arrayidx775, align 4, !tbaa !7
  %963 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx776 = getelementptr inbounds i32, i32* %963, i32 2
  %964 = load i32, i32* %arrayidx776, align 4, !tbaa !7
  %965 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx777 = getelementptr inbounds i32, i32* %965, i32 2
  store i32 %964, i32* %arrayidx777, align 4, !tbaa !7
  %966 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx778 = getelementptr inbounds i32, i32* %966, i32 3
  %967 = load i32, i32* %arrayidx778, align 4, !tbaa !7
  %968 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx779 = getelementptr inbounds i32, i32* %968, i32 3
  store i32 %967, i32* %arrayidx779, align 4, !tbaa !7
  %969 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx780 = getelementptr inbounds i32, i32* %969, i32 56
  %970 = load i32, i32* %arrayidx780, align 4, !tbaa !7
  %971 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx781 = getelementptr inbounds i32, i32* %971, i32 4
  %972 = load i32, i32* %arrayidx781, align 4, !tbaa !7
  %973 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx782 = getelementptr inbounds i32, i32* %973, i32 8
  %974 = load i32, i32* %arrayidx782, align 4, !tbaa !7
  %975 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx783 = getelementptr inbounds i32, i32* %975, i32 7
  %976 = load i32, i32* %arrayidx783, align 4, !tbaa !7
  %977 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv784 = sext i8 %977 to i32
  %call785 = call i32 @half_btf(i32 %970, i32 %972, i32 %974, i32 %976, i32 %conv784)
  %978 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx786 = getelementptr inbounds i32, i32* %978, i32 4
  store i32 %call785, i32* %arrayidx786, align 4, !tbaa !7
  %979 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx787 = getelementptr inbounds i32, i32* %979, i32 24
  %980 = load i32, i32* %arrayidx787, align 4, !tbaa !7
  %981 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx788 = getelementptr inbounds i32, i32* %981, i32 5
  %982 = load i32, i32* %arrayidx788, align 4, !tbaa !7
  %983 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx789 = getelementptr inbounds i32, i32* %983, i32 40
  %984 = load i32, i32* %arrayidx789, align 4, !tbaa !7
  %985 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx790 = getelementptr inbounds i32, i32* %985, i32 6
  %986 = load i32, i32* %arrayidx790, align 4, !tbaa !7
  %987 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv791 = sext i8 %987 to i32
  %call792 = call i32 @half_btf(i32 %980, i32 %982, i32 %984, i32 %986, i32 %conv791)
  %988 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx793 = getelementptr inbounds i32, i32* %988, i32 5
  store i32 %call792, i32* %arrayidx793, align 4, !tbaa !7
  %989 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx794 = getelementptr inbounds i32, i32* %989, i32 24
  %990 = load i32, i32* %arrayidx794, align 4, !tbaa !7
  %991 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx795 = getelementptr inbounds i32, i32* %991, i32 6
  %992 = load i32, i32* %arrayidx795, align 4, !tbaa !7
  %993 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx796 = getelementptr inbounds i32, i32* %993, i32 40
  %994 = load i32, i32* %arrayidx796, align 4, !tbaa !7
  %sub797 = sub nsw i32 0, %994
  %995 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx798 = getelementptr inbounds i32, i32* %995, i32 5
  %996 = load i32, i32* %arrayidx798, align 4, !tbaa !7
  %997 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv799 = sext i8 %997 to i32
  %call800 = call i32 @half_btf(i32 %990, i32 %992, i32 %sub797, i32 %996, i32 %conv799)
  %998 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx801 = getelementptr inbounds i32, i32* %998, i32 6
  store i32 %call800, i32* %arrayidx801, align 4, !tbaa !7
  %999 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx802 = getelementptr inbounds i32, i32* %999, i32 56
  %1000 = load i32, i32* %arrayidx802, align 4, !tbaa !7
  %1001 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx803 = getelementptr inbounds i32, i32* %1001, i32 7
  %1002 = load i32, i32* %arrayidx803, align 4, !tbaa !7
  %1003 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx804 = getelementptr inbounds i32, i32* %1003, i32 8
  %1004 = load i32, i32* %arrayidx804, align 4, !tbaa !7
  %sub805 = sub nsw i32 0, %1004
  %1005 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx806 = getelementptr inbounds i32, i32* %1005, i32 4
  %1006 = load i32, i32* %arrayidx806, align 4, !tbaa !7
  %1007 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv807 = sext i8 %1007 to i32
  %call808 = call i32 @half_btf(i32 %1000, i32 %1002, i32 %sub805, i32 %1006, i32 %conv807)
  %1008 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx809 = getelementptr inbounds i32, i32* %1008, i32 7
  store i32 %call808, i32* %arrayidx809, align 4, !tbaa !7
  %1009 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx810 = getelementptr inbounds i32, i32* %1009, i32 8
  %1010 = load i32, i32* %arrayidx810, align 4, !tbaa !7
  %1011 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx811 = getelementptr inbounds i32, i32* %1011, i32 9
  %1012 = load i32, i32* %arrayidx811, align 4, !tbaa !7
  %add812 = add nsw i32 %1010, %1012
  %1013 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx813 = getelementptr inbounds i32, i32* %1013, i32 8
  store i32 %add812, i32* %arrayidx813, align 4, !tbaa !7
  %1014 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx814 = getelementptr inbounds i32, i32* %1014, i32 9
  %1015 = load i32, i32* %arrayidx814, align 4, !tbaa !7
  %sub815 = sub nsw i32 0, %1015
  %1016 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx816 = getelementptr inbounds i32, i32* %1016, i32 8
  %1017 = load i32, i32* %arrayidx816, align 4, !tbaa !7
  %add817 = add nsw i32 %sub815, %1017
  %1018 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx818 = getelementptr inbounds i32, i32* %1018, i32 9
  store i32 %add817, i32* %arrayidx818, align 4, !tbaa !7
  %1019 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx819 = getelementptr inbounds i32, i32* %1019, i32 10
  %1020 = load i32, i32* %arrayidx819, align 4, !tbaa !7
  %sub820 = sub nsw i32 0, %1020
  %1021 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx821 = getelementptr inbounds i32, i32* %1021, i32 11
  %1022 = load i32, i32* %arrayidx821, align 4, !tbaa !7
  %add822 = add nsw i32 %sub820, %1022
  %1023 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx823 = getelementptr inbounds i32, i32* %1023, i32 10
  store i32 %add822, i32* %arrayidx823, align 4, !tbaa !7
  %1024 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx824 = getelementptr inbounds i32, i32* %1024, i32 11
  %1025 = load i32, i32* %arrayidx824, align 4, !tbaa !7
  %1026 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx825 = getelementptr inbounds i32, i32* %1026, i32 10
  %1027 = load i32, i32* %arrayidx825, align 4, !tbaa !7
  %add826 = add nsw i32 %1025, %1027
  %1028 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx827 = getelementptr inbounds i32, i32* %1028, i32 11
  store i32 %add826, i32* %arrayidx827, align 4, !tbaa !7
  %1029 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx828 = getelementptr inbounds i32, i32* %1029, i32 12
  %1030 = load i32, i32* %arrayidx828, align 4, !tbaa !7
  %1031 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx829 = getelementptr inbounds i32, i32* %1031, i32 13
  %1032 = load i32, i32* %arrayidx829, align 4, !tbaa !7
  %add830 = add nsw i32 %1030, %1032
  %1033 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx831 = getelementptr inbounds i32, i32* %1033, i32 12
  store i32 %add830, i32* %arrayidx831, align 4, !tbaa !7
  %1034 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx832 = getelementptr inbounds i32, i32* %1034, i32 13
  %1035 = load i32, i32* %arrayidx832, align 4, !tbaa !7
  %sub833 = sub nsw i32 0, %1035
  %1036 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx834 = getelementptr inbounds i32, i32* %1036, i32 12
  %1037 = load i32, i32* %arrayidx834, align 4, !tbaa !7
  %add835 = add nsw i32 %sub833, %1037
  %1038 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx836 = getelementptr inbounds i32, i32* %1038, i32 13
  store i32 %add835, i32* %arrayidx836, align 4, !tbaa !7
  %1039 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx837 = getelementptr inbounds i32, i32* %1039, i32 14
  %1040 = load i32, i32* %arrayidx837, align 4, !tbaa !7
  %sub838 = sub nsw i32 0, %1040
  %1041 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx839 = getelementptr inbounds i32, i32* %1041, i32 15
  %1042 = load i32, i32* %arrayidx839, align 4, !tbaa !7
  %add840 = add nsw i32 %sub838, %1042
  %1043 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx841 = getelementptr inbounds i32, i32* %1043, i32 14
  store i32 %add840, i32* %arrayidx841, align 4, !tbaa !7
  %1044 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx842 = getelementptr inbounds i32, i32* %1044, i32 15
  %1045 = load i32, i32* %arrayidx842, align 4, !tbaa !7
  %1046 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx843 = getelementptr inbounds i32, i32* %1046, i32 14
  %1047 = load i32, i32* %arrayidx843, align 4, !tbaa !7
  %add844 = add nsw i32 %1045, %1047
  %1048 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx845 = getelementptr inbounds i32, i32* %1048, i32 15
  store i32 %add844, i32* %arrayidx845, align 4, !tbaa !7
  %1049 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx846 = getelementptr inbounds i32, i32* %1049, i32 16
  %1050 = load i32, i32* %arrayidx846, align 4, !tbaa !7
  %1051 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx847 = getelementptr inbounds i32, i32* %1051, i32 16
  store i32 %1050, i32* %arrayidx847, align 4, !tbaa !7
  %1052 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx848 = getelementptr inbounds i32, i32* %1052, i32 8
  %1053 = load i32, i32* %arrayidx848, align 4, !tbaa !7
  %sub849 = sub nsw i32 0, %1053
  %1054 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx850 = getelementptr inbounds i32, i32* %1054, i32 17
  %1055 = load i32, i32* %arrayidx850, align 4, !tbaa !7
  %1056 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx851 = getelementptr inbounds i32, i32* %1056, i32 56
  %1057 = load i32, i32* %arrayidx851, align 4, !tbaa !7
  %1058 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx852 = getelementptr inbounds i32, i32* %1058, i32 30
  %1059 = load i32, i32* %arrayidx852, align 4, !tbaa !7
  %1060 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv853 = sext i8 %1060 to i32
  %call854 = call i32 @half_btf(i32 %sub849, i32 %1055, i32 %1057, i32 %1059, i32 %conv853)
  %1061 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx855 = getelementptr inbounds i32, i32* %1061, i32 17
  store i32 %call854, i32* %arrayidx855, align 4, !tbaa !7
  %1062 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx856 = getelementptr inbounds i32, i32* %1062, i32 56
  %1063 = load i32, i32* %arrayidx856, align 4, !tbaa !7
  %sub857 = sub nsw i32 0, %1063
  %1064 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx858 = getelementptr inbounds i32, i32* %1064, i32 18
  %1065 = load i32, i32* %arrayidx858, align 4, !tbaa !7
  %1066 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx859 = getelementptr inbounds i32, i32* %1066, i32 8
  %1067 = load i32, i32* %arrayidx859, align 4, !tbaa !7
  %sub860 = sub nsw i32 0, %1067
  %1068 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx861 = getelementptr inbounds i32, i32* %1068, i32 29
  %1069 = load i32, i32* %arrayidx861, align 4, !tbaa !7
  %1070 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv862 = sext i8 %1070 to i32
  %call863 = call i32 @half_btf(i32 %sub857, i32 %1065, i32 %sub860, i32 %1069, i32 %conv862)
  %1071 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx864 = getelementptr inbounds i32, i32* %1071, i32 18
  store i32 %call863, i32* %arrayidx864, align 4, !tbaa !7
  %1072 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx865 = getelementptr inbounds i32, i32* %1072, i32 19
  %1073 = load i32, i32* %arrayidx865, align 4, !tbaa !7
  %1074 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx866 = getelementptr inbounds i32, i32* %1074, i32 19
  store i32 %1073, i32* %arrayidx866, align 4, !tbaa !7
  %1075 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx867 = getelementptr inbounds i32, i32* %1075, i32 20
  %1076 = load i32, i32* %arrayidx867, align 4, !tbaa !7
  %1077 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx868 = getelementptr inbounds i32, i32* %1077, i32 20
  store i32 %1076, i32* %arrayidx868, align 4, !tbaa !7
  %1078 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx869 = getelementptr inbounds i32, i32* %1078, i32 40
  %1079 = load i32, i32* %arrayidx869, align 4, !tbaa !7
  %sub870 = sub nsw i32 0, %1079
  %1080 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx871 = getelementptr inbounds i32, i32* %1080, i32 21
  %1081 = load i32, i32* %arrayidx871, align 4, !tbaa !7
  %1082 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx872 = getelementptr inbounds i32, i32* %1082, i32 24
  %1083 = load i32, i32* %arrayidx872, align 4, !tbaa !7
  %1084 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx873 = getelementptr inbounds i32, i32* %1084, i32 26
  %1085 = load i32, i32* %arrayidx873, align 4, !tbaa !7
  %1086 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv874 = sext i8 %1086 to i32
  %call875 = call i32 @half_btf(i32 %sub870, i32 %1081, i32 %1083, i32 %1085, i32 %conv874)
  %1087 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx876 = getelementptr inbounds i32, i32* %1087, i32 21
  store i32 %call875, i32* %arrayidx876, align 4, !tbaa !7
  %1088 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx877 = getelementptr inbounds i32, i32* %1088, i32 24
  %1089 = load i32, i32* %arrayidx877, align 4, !tbaa !7
  %sub878 = sub nsw i32 0, %1089
  %1090 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx879 = getelementptr inbounds i32, i32* %1090, i32 22
  %1091 = load i32, i32* %arrayidx879, align 4, !tbaa !7
  %1092 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx880 = getelementptr inbounds i32, i32* %1092, i32 40
  %1093 = load i32, i32* %arrayidx880, align 4, !tbaa !7
  %sub881 = sub nsw i32 0, %1093
  %1094 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx882 = getelementptr inbounds i32, i32* %1094, i32 25
  %1095 = load i32, i32* %arrayidx882, align 4, !tbaa !7
  %1096 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv883 = sext i8 %1096 to i32
  %call884 = call i32 @half_btf(i32 %sub878, i32 %1091, i32 %sub881, i32 %1095, i32 %conv883)
  %1097 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx885 = getelementptr inbounds i32, i32* %1097, i32 22
  store i32 %call884, i32* %arrayidx885, align 4, !tbaa !7
  %1098 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx886 = getelementptr inbounds i32, i32* %1098, i32 23
  %1099 = load i32, i32* %arrayidx886, align 4, !tbaa !7
  %1100 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx887 = getelementptr inbounds i32, i32* %1100, i32 23
  store i32 %1099, i32* %arrayidx887, align 4, !tbaa !7
  %1101 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx888 = getelementptr inbounds i32, i32* %1101, i32 24
  %1102 = load i32, i32* %arrayidx888, align 4, !tbaa !7
  %1103 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx889 = getelementptr inbounds i32, i32* %1103, i32 24
  store i32 %1102, i32* %arrayidx889, align 4, !tbaa !7
  %1104 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx890 = getelementptr inbounds i32, i32* %1104, i32 24
  %1105 = load i32, i32* %arrayidx890, align 4, !tbaa !7
  %1106 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx891 = getelementptr inbounds i32, i32* %1106, i32 25
  %1107 = load i32, i32* %arrayidx891, align 4, !tbaa !7
  %1108 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx892 = getelementptr inbounds i32, i32* %1108, i32 40
  %1109 = load i32, i32* %arrayidx892, align 4, !tbaa !7
  %sub893 = sub nsw i32 0, %1109
  %1110 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx894 = getelementptr inbounds i32, i32* %1110, i32 22
  %1111 = load i32, i32* %arrayidx894, align 4, !tbaa !7
  %1112 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv895 = sext i8 %1112 to i32
  %call896 = call i32 @half_btf(i32 %1105, i32 %1107, i32 %sub893, i32 %1111, i32 %conv895)
  %1113 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx897 = getelementptr inbounds i32, i32* %1113, i32 25
  store i32 %call896, i32* %arrayidx897, align 4, !tbaa !7
  %1114 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx898 = getelementptr inbounds i32, i32* %1114, i32 40
  %1115 = load i32, i32* %arrayidx898, align 4, !tbaa !7
  %1116 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx899 = getelementptr inbounds i32, i32* %1116, i32 26
  %1117 = load i32, i32* %arrayidx899, align 4, !tbaa !7
  %1118 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx900 = getelementptr inbounds i32, i32* %1118, i32 24
  %1119 = load i32, i32* %arrayidx900, align 4, !tbaa !7
  %1120 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx901 = getelementptr inbounds i32, i32* %1120, i32 21
  %1121 = load i32, i32* %arrayidx901, align 4, !tbaa !7
  %1122 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv902 = sext i8 %1122 to i32
  %call903 = call i32 @half_btf(i32 %1115, i32 %1117, i32 %1119, i32 %1121, i32 %conv902)
  %1123 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx904 = getelementptr inbounds i32, i32* %1123, i32 26
  store i32 %call903, i32* %arrayidx904, align 4, !tbaa !7
  %1124 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx905 = getelementptr inbounds i32, i32* %1124, i32 27
  %1125 = load i32, i32* %arrayidx905, align 4, !tbaa !7
  %1126 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx906 = getelementptr inbounds i32, i32* %1126, i32 27
  store i32 %1125, i32* %arrayidx906, align 4, !tbaa !7
  %1127 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx907 = getelementptr inbounds i32, i32* %1127, i32 28
  %1128 = load i32, i32* %arrayidx907, align 4, !tbaa !7
  %1129 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx908 = getelementptr inbounds i32, i32* %1129, i32 28
  store i32 %1128, i32* %arrayidx908, align 4, !tbaa !7
  %1130 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx909 = getelementptr inbounds i32, i32* %1130, i32 56
  %1131 = load i32, i32* %arrayidx909, align 4, !tbaa !7
  %1132 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx910 = getelementptr inbounds i32, i32* %1132, i32 29
  %1133 = load i32, i32* %arrayidx910, align 4, !tbaa !7
  %1134 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx911 = getelementptr inbounds i32, i32* %1134, i32 8
  %1135 = load i32, i32* %arrayidx911, align 4, !tbaa !7
  %sub912 = sub nsw i32 0, %1135
  %1136 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx913 = getelementptr inbounds i32, i32* %1136, i32 18
  %1137 = load i32, i32* %arrayidx913, align 4, !tbaa !7
  %1138 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv914 = sext i8 %1138 to i32
  %call915 = call i32 @half_btf(i32 %1131, i32 %1133, i32 %sub912, i32 %1137, i32 %conv914)
  %1139 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx916 = getelementptr inbounds i32, i32* %1139, i32 29
  store i32 %call915, i32* %arrayidx916, align 4, !tbaa !7
  %1140 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx917 = getelementptr inbounds i32, i32* %1140, i32 8
  %1141 = load i32, i32* %arrayidx917, align 4, !tbaa !7
  %1142 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx918 = getelementptr inbounds i32, i32* %1142, i32 30
  %1143 = load i32, i32* %arrayidx918, align 4, !tbaa !7
  %1144 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx919 = getelementptr inbounds i32, i32* %1144, i32 56
  %1145 = load i32, i32* %arrayidx919, align 4, !tbaa !7
  %1146 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx920 = getelementptr inbounds i32, i32* %1146, i32 17
  %1147 = load i32, i32* %arrayidx920, align 4, !tbaa !7
  %1148 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv921 = sext i8 %1148 to i32
  %call922 = call i32 @half_btf(i32 %1141, i32 %1143, i32 %1145, i32 %1147, i32 %conv921)
  %1149 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx923 = getelementptr inbounds i32, i32* %1149, i32 30
  store i32 %call922, i32* %arrayidx923, align 4, !tbaa !7
  %1150 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx924 = getelementptr inbounds i32, i32* %1150, i32 31
  %1151 = load i32, i32* %arrayidx924, align 4, !tbaa !7
  %1152 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx925 = getelementptr inbounds i32, i32* %1152, i32 31
  store i32 %1151, i32* %arrayidx925, align 4, !tbaa !7
  %1153 = load i32, i32* %stage, align 4, !tbaa !7
  %1154 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %1155 = load i32*, i32** %bf1, align 4, !tbaa !2
  %1156 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %1157 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx926 = getelementptr inbounds i8, i8* %1156, i32 %1157
  %1158 = load i8, i8* %arrayidx926, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %1153, i32* %1154, i32* %1155, i32 32, i8 signext %1158)
  %1159 = load i32, i32* %stage, align 4, !tbaa !7
  %inc927 = add nsw i32 %1159, 1
  store i32 %inc927, i32* %stage, align 4, !tbaa !7
  %1160 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv928 = sext i8 %1160 to i32
  %call929 = call i32* @cospi_arr(i32 %conv928)
  store i32* %call929, i32** %cospi, align 4, !tbaa !2
  %arraydecay930 = getelementptr inbounds [32 x i32], [32 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay930, i32** %bf0, align 4, !tbaa !2
  %1161 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %1161, i32** %bf1, align 4, !tbaa !2
  %1162 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx931 = getelementptr inbounds i32, i32* %1162, i32 0
  %1163 = load i32, i32* %arrayidx931, align 4, !tbaa !7
  %1164 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx932 = getelementptr inbounds i32, i32* %1164, i32 0
  store i32 %1163, i32* %arrayidx932, align 4, !tbaa !7
  %1165 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx933 = getelementptr inbounds i32, i32* %1165, i32 1
  %1166 = load i32, i32* %arrayidx933, align 4, !tbaa !7
  %1167 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx934 = getelementptr inbounds i32, i32* %1167, i32 1
  store i32 %1166, i32* %arrayidx934, align 4, !tbaa !7
  %1168 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx935 = getelementptr inbounds i32, i32* %1168, i32 2
  %1169 = load i32, i32* %arrayidx935, align 4, !tbaa !7
  %1170 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx936 = getelementptr inbounds i32, i32* %1170, i32 2
  store i32 %1169, i32* %arrayidx936, align 4, !tbaa !7
  %1171 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx937 = getelementptr inbounds i32, i32* %1171, i32 3
  %1172 = load i32, i32* %arrayidx937, align 4, !tbaa !7
  %1173 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx938 = getelementptr inbounds i32, i32* %1173, i32 3
  store i32 %1172, i32* %arrayidx938, align 4, !tbaa !7
  %1174 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx939 = getelementptr inbounds i32, i32* %1174, i32 4
  %1175 = load i32, i32* %arrayidx939, align 4, !tbaa !7
  %1176 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx940 = getelementptr inbounds i32, i32* %1176, i32 4
  store i32 %1175, i32* %arrayidx940, align 4, !tbaa !7
  %1177 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx941 = getelementptr inbounds i32, i32* %1177, i32 5
  %1178 = load i32, i32* %arrayidx941, align 4, !tbaa !7
  %1179 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx942 = getelementptr inbounds i32, i32* %1179, i32 5
  store i32 %1178, i32* %arrayidx942, align 4, !tbaa !7
  %1180 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx943 = getelementptr inbounds i32, i32* %1180, i32 6
  %1181 = load i32, i32* %arrayidx943, align 4, !tbaa !7
  %1182 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx944 = getelementptr inbounds i32, i32* %1182, i32 6
  store i32 %1181, i32* %arrayidx944, align 4, !tbaa !7
  %1183 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx945 = getelementptr inbounds i32, i32* %1183, i32 7
  %1184 = load i32, i32* %arrayidx945, align 4, !tbaa !7
  %1185 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx946 = getelementptr inbounds i32, i32* %1185, i32 7
  store i32 %1184, i32* %arrayidx946, align 4, !tbaa !7
  %1186 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx947 = getelementptr inbounds i32, i32* %1186, i32 60
  %1187 = load i32, i32* %arrayidx947, align 4, !tbaa !7
  %1188 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx948 = getelementptr inbounds i32, i32* %1188, i32 8
  %1189 = load i32, i32* %arrayidx948, align 4, !tbaa !7
  %1190 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx949 = getelementptr inbounds i32, i32* %1190, i32 4
  %1191 = load i32, i32* %arrayidx949, align 4, !tbaa !7
  %1192 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx950 = getelementptr inbounds i32, i32* %1192, i32 15
  %1193 = load i32, i32* %arrayidx950, align 4, !tbaa !7
  %1194 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv951 = sext i8 %1194 to i32
  %call952 = call i32 @half_btf(i32 %1187, i32 %1189, i32 %1191, i32 %1193, i32 %conv951)
  %1195 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx953 = getelementptr inbounds i32, i32* %1195, i32 8
  store i32 %call952, i32* %arrayidx953, align 4, !tbaa !7
  %1196 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx954 = getelementptr inbounds i32, i32* %1196, i32 28
  %1197 = load i32, i32* %arrayidx954, align 4, !tbaa !7
  %1198 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx955 = getelementptr inbounds i32, i32* %1198, i32 9
  %1199 = load i32, i32* %arrayidx955, align 4, !tbaa !7
  %1200 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx956 = getelementptr inbounds i32, i32* %1200, i32 36
  %1201 = load i32, i32* %arrayidx956, align 4, !tbaa !7
  %1202 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx957 = getelementptr inbounds i32, i32* %1202, i32 14
  %1203 = load i32, i32* %arrayidx957, align 4, !tbaa !7
  %1204 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv958 = sext i8 %1204 to i32
  %call959 = call i32 @half_btf(i32 %1197, i32 %1199, i32 %1201, i32 %1203, i32 %conv958)
  %1205 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx960 = getelementptr inbounds i32, i32* %1205, i32 9
  store i32 %call959, i32* %arrayidx960, align 4, !tbaa !7
  %1206 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx961 = getelementptr inbounds i32, i32* %1206, i32 44
  %1207 = load i32, i32* %arrayidx961, align 4, !tbaa !7
  %1208 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx962 = getelementptr inbounds i32, i32* %1208, i32 10
  %1209 = load i32, i32* %arrayidx962, align 4, !tbaa !7
  %1210 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx963 = getelementptr inbounds i32, i32* %1210, i32 20
  %1211 = load i32, i32* %arrayidx963, align 4, !tbaa !7
  %1212 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx964 = getelementptr inbounds i32, i32* %1212, i32 13
  %1213 = load i32, i32* %arrayidx964, align 4, !tbaa !7
  %1214 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv965 = sext i8 %1214 to i32
  %call966 = call i32 @half_btf(i32 %1207, i32 %1209, i32 %1211, i32 %1213, i32 %conv965)
  %1215 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx967 = getelementptr inbounds i32, i32* %1215, i32 10
  store i32 %call966, i32* %arrayidx967, align 4, !tbaa !7
  %1216 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx968 = getelementptr inbounds i32, i32* %1216, i32 12
  %1217 = load i32, i32* %arrayidx968, align 4, !tbaa !7
  %1218 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx969 = getelementptr inbounds i32, i32* %1218, i32 11
  %1219 = load i32, i32* %arrayidx969, align 4, !tbaa !7
  %1220 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx970 = getelementptr inbounds i32, i32* %1220, i32 52
  %1221 = load i32, i32* %arrayidx970, align 4, !tbaa !7
  %1222 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx971 = getelementptr inbounds i32, i32* %1222, i32 12
  %1223 = load i32, i32* %arrayidx971, align 4, !tbaa !7
  %1224 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv972 = sext i8 %1224 to i32
  %call973 = call i32 @half_btf(i32 %1217, i32 %1219, i32 %1221, i32 %1223, i32 %conv972)
  %1225 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx974 = getelementptr inbounds i32, i32* %1225, i32 11
  store i32 %call973, i32* %arrayidx974, align 4, !tbaa !7
  %1226 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx975 = getelementptr inbounds i32, i32* %1226, i32 12
  %1227 = load i32, i32* %arrayidx975, align 4, !tbaa !7
  %1228 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx976 = getelementptr inbounds i32, i32* %1228, i32 12
  %1229 = load i32, i32* %arrayidx976, align 4, !tbaa !7
  %1230 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx977 = getelementptr inbounds i32, i32* %1230, i32 52
  %1231 = load i32, i32* %arrayidx977, align 4, !tbaa !7
  %sub978 = sub nsw i32 0, %1231
  %1232 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx979 = getelementptr inbounds i32, i32* %1232, i32 11
  %1233 = load i32, i32* %arrayidx979, align 4, !tbaa !7
  %1234 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv980 = sext i8 %1234 to i32
  %call981 = call i32 @half_btf(i32 %1227, i32 %1229, i32 %sub978, i32 %1233, i32 %conv980)
  %1235 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx982 = getelementptr inbounds i32, i32* %1235, i32 12
  store i32 %call981, i32* %arrayidx982, align 4, !tbaa !7
  %1236 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx983 = getelementptr inbounds i32, i32* %1236, i32 44
  %1237 = load i32, i32* %arrayidx983, align 4, !tbaa !7
  %1238 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx984 = getelementptr inbounds i32, i32* %1238, i32 13
  %1239 = load i32, i32* %arrayidx984, align 4, !tbaa !7
  %1240 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx985 = getelementptr inbounds i32, i32* %1240, i32 20
  %1241 = load i32, i32* %arrayidx985, align 4, !tbaa !7
  %sub986 = sub nsw i32 0, %1241
  %1242 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx987 = getelementptr inbounds i32, i32* %1242, i32 10
  %1243 = load i32, i32* %arrayidx987, align 4, !tbaa !7
  %1244 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv988 = sext i8 %1244 to i32
  %call989 = call i32 @half_btf(i32 %1237, i32 %1239, i32 %sub986, i32 %1243, i32 %conv988)
  %1245 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx990 = getelementptr inbounds i32, i32* %1245, i32 13
  store i32 %call989, i32* %arrayidx990, align 4, !tbaa !7
  %1246 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx991 = getelementptr inbounds i32, i32* %1246, i32 28
  %1247 = load i32, i32* %arrayidx991, align 4, !tbaa !7
  %1248 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx992 = getelementptr inbounds i32, i32* %1248, i32 14
  %1249 = load i32, i32* %arrayidx992, align 4, !tbaa !7
  %1250 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx993 = getelementptr inbounds i32, i32* %1250, i32 36
  %1251 = load i32, i32* %arrayidx993, align 4, !tbaa !7
  %sub994 = sub nsw i32 0, %1251
  %1252 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx995 = getelementptr inbounds i32, i32* %1252, i32 9
  %1253 = load i32, i32* %arrayidx995, align 4, !tbaa !7
  %1254 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv996 = sext i8 %1254 to i32
  %call997 = call i32 @half_btf(i32 %1247, i32 %1249, i32 %sub994, i32 %1253, i32 %conv996)
  %1255 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx998 = getelementptr inbounds i32, i32* %1255, i32 14
  store i32 %call997, i32* %arrayidx998, align 4, !tbaa !7
  %1256 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx999 = getelementptr inbounds i32, i32* %1256, i32 60
  %1257 = load i32, i32* %arrayidx999, align 4, !tbaa !7
  %1258 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1000 = getelementptr inbounds i32, i32* %1258, i32 15
  %1259 = load i32, i32* %arrayidx1000, align 4, !tbaa !7
  %1260 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1001 = getelementptr inbounds i32, i32* %1260, i32 4
  %1261 = load i32, i32* %arrayidx1001, align 4, !tbaa !7
  %sub1002 = sub nsw i32 0, %1261
  %1262 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1003 = getelementptr inbounds i32, i32* %1262, i32 8
  %1263 = load i32, i32* %arrayidx1003, align 4, !tbaa !7
  %1264 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1004 = sext i8 %1264 to i32
  %call1005 = call i32 @half_btf(i32 %1257, i32 %1259, i32 %sub1002, i32 %1263, i32 %conv1004)
  %1265 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1006 = getelementptr inbounds i32, i32* %1265, i32 15
  store i32 %call1005, i32* %arrayidx1006, align 4, !tbaa !7
  %1266 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1007 = getelementptr inbounds i32, i32* %1266, i32 16
  %1267 = load i32, i32* %arrayidx1007, align 4, !tbaa !7
  %1268 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1008 = getelementptr inbounds i32, i32* %1268, i32 17
  %1269 = load i32, i32* %arrayidx1008, align 4, !tbaa !7
  %add1009 = add nsw i32 %1267, %1269
  %1270 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1010 = getelementptr inbounds i32, i32* %1270, i32 16
  store i32 %add1009, i32* %arrayidx1010, align 4, !tbaa !7
  %1271 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1011 = getelementptr inbounds i32, i32* %1271, i32 17
  %1272 = load i32, i32* %arrayidx1011, align 4, !tbaa !7
  %sub1012 = sub nsw i32 0, %1272
  %1273 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1013 = getelementptr inbounds i32, i32* %1273, i32 16
  %1274 = load i32, i32* %arrayidx1013, align 4, !tbaa !7
  %add1014 = add nsw i32 %sub1012, %1274
  %1275 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1015 = getelementptr inbounds i32, i32* %1275, i32 17
  store i32 %add1014, i32* %arrayidx1015, align 4, !tbaa !7
  %1276 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1016 = getelementptr inbounds i32, i32* %1276, i32 18
  %1277 = load i32, i32* %arrayidx1016, align 4, !tbaa !7
  %sub1017 = sub nsw i32 0, %1277
  %1278 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1018 = getelementptr inbounds i32, i32* %1278, i32 19
  %1279 = load i32, i32* %arrayidx1018, align 4, !tbaa !7
  %add1019 = add nsw i32 %sub1017, %1279
  %1280 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1020 = getelementptr inbounds i32, i32* %1280, i32 18
  store i32 %add1019, i32* %arrayidx1020, align 4, !tbaa !7
  %1281 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1021 = getelementptr inbounds i32, i32* %1281, i32 19
  %1282 = load i32, i32* %arrayidx1021, align 4, !tbaa !7
  %1283 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1022 = getelementptr inbounds i32, i32* %1283, i32 18
  %1284 = load i32, i32* %arrayidx1022, align 4, !tbaa !7
  %add1023 = add nsw i32 %1282, %1284
  %1285 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1024 = getelementptr inbounds i32, i32* %1285, i32 19
  store i32 %add1023, i32* %arrayidx1024, align 4, !tbaa !7
  %1286 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1025 = getelementptr inbounds i32, i32* %1286, i32 20
  %1287 = load i32, i32* %arrayidx1025, align 4, !tbaa !7
  %1288 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1026 = getelementptr inbounds i32, i32* %1288, i32 21
  %1289 = load i32, i32* %arrayidx1026, align 4, !tbaa !7
  %add1027 = add nsw i32 %1287, %1289
  %1290 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1028 = getelementptr inbounds i32, i32* %1290, i32 20
  store i32 %add1027, i32* %arrayidx1028, align 4, !tbaa !7
  %1291 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1029 = getelementptr inbounds i32, i32* %1291, i32 21
  %1292 = load i32, i32* %arrayidx1029, align 4, !tbaa !7
  %sub1030 = sub nsw i32 0, %1292
  %1293 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1031 = getelementptr inbounds i32, i32* %1293, i32 20
  %1294 = load i32, i32* %arrayidx1031, align 4, !tbaa !7
  %add1032 = add nsw i32 %sub1030, %1294
  %1295 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1033 = getelementptr inbounds i32, i32* %1295, i32 21
  store i32 %add1032, i32* %arrayidx1033, align 4, !tbaa !7
  %1296 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1034 = getelementptr inbounds i32, i32* %1296, i32 22
  %1297 = load i32, i32* %arrayidx1034, align 4, !tbaa !7
  %sub1035 = sub nsw i32 0, %1297
  %1298 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1036 = getelementptr inbounds i32, i32* %1298, i32 23
  %1299 = load i32, i32* %arrayidx1036, align 4, !tbaa !7
  %add1037 = add nsw i32 %sub1035, %1299
  %1300 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1038 = getelementptr inbounds i32, i32* %1300, i32 22
  store i32 %add1037, i32* %arrayidx1038, align 4, !tbaa !7
  %1301 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1039 = getelementptr inbounds i32, i32* %1301, i32 23
  %1302 = load i32, i32* %arrayidx1039, align 4, !tbaa !7
  %1303 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1040 = getelementptr inbounds i32, i32* %1303, i32 22
  %1304 = load i32, i32* %arrayidx1040, align 4, !tbaa !7
  %add1041 = add nsw i32 %1302, %1304
  %1305 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1042 = getelementptr inbounds i32, i32* %1305, i32 23
  store i32 %add1041, i32* %arrayidx1042, align 4, !tbaa !7
  %1306 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1043 = getelementptr inbounds i32, i32* %1306, i32 24
  %1307 = load i32, i32* %arrayidx1043, align 4, !tbaa !7
  %1308 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1044 = getelementptr inbounds i32, i32* %1308, i32 25
  %1309 = load i32, i32* %arrayidx1044, align 4, !tbaa !7
  %add1045 = add nsw i32 %1307, %1309
  %1310 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1046 = getelementptr inbounds i32, i32* %1310, i32 24
  store i32 %add1045, i32* %arrayidx1046, align 4, !tbaa !7
  %1311 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1047 = getelementptr inbounds i32, i32* %1311, i32 25
  %1312 = load i32, i32* %arrayidx1047, align 4, !tbaa !7
  %sub1048 = sub nsw i32 0, %1312
  %1313 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1049 = getelementptr inbounds i32, i32* %1313, i32 24
  %1314 = load i32, i32* %arrayidx1049, align 4, !tbaa !7
  %add1050 = add nsw i32 %sub1048, %1314
  %1315 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1051 = getelementptr inbounds i32, i32* %1315, i32 25
  store i32 %add1050, i32* %arrayidx1051, align 4, !tbaa !7
  %1316 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1052 = getelementptr inbounds i32, i32* %1316, i32 26
  %1317 = load i32, i32* %arrayidx1052, align 4, !tbaa !7
  %sub1053 = sub nsw i32 0, %1317
  %1318 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1054 = getelementptr inbounds i32, i32* %1318, i32 27
  %1319 = load i32, i32* %arrayidx1054, align 4, !tbaa !7
  %add1055 = add nsw i32 %sub1053, %1319
  %1320 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1056 = getelementptr inbounds i32, i32* %1320, i32 26
  store i32 %add1055, i32* %arrayidx1056, align 4, !tbaa !7
  %1321 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1057 = getelementptr inbounds i32, i32* %1321, i32 27
  %1322 = load i32, i32* %arrayidx1057, align 4, !tbaa !7
  %1323 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1058 = getelementptr inbounds i32, i32* %1323, i32 26
  %1324 = load i32, i32* %arrayidx1058, align 4, !tbaa !7
  %add1059 = add nsw i32 %1322, %1324
  %1325 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1060 = getelementptr inbounds i32, i32* %1325, i32 27
  store i32 %add1059, i32* %arrayidx1060, align 4, !tbaa !7
  %1326 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1061 = getelementptr inbounds i32, i32* %1326, i32 28
  %1327 = load i32, i32* %arrayidx1061, align 4, !tbaa !7
  %1328 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1062 = getelementptr inbounds i32, i32* %1328, i32 29
  %1329 = load i32, i32* %arrayidx1062, align 4, !tbaa !7
  %add1063 = add nsw i32 %1327, %1329
  %1330 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1064 = getelementptr inbounds i32, i32* %1330, i32 28
  store i32 %add1063, i32* %arrayidx1064, align 4, !tbaa !7
  %1331 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1065 = getelementptr inbounds i32, i32* %1331, i32 29
  %1332 = load i32, i32* %arrayidx1065, align 4, !tbaa !7
  %sub1066 = sub nsw i32 0, %1332
  %1333 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1067 = getelementptr inbounds i32, i32* %1333, i32 28
  %1334 = load i32, i32* %arrayidx1067, align 4, !tbaa !7
  %add1068 = add nsw i32 %sub1066, %1334
  %1335 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1069 = getelementptr inbounds i32, i32* %1335, i32 29
  store i32 %add1068, i32* %arrayidx1069, align 4, !tbaa !7
  %1336 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1070 = getelementptr inbounds i32, i32* %1336, i32 30
  %1337 = load i32, i32* %arrayidx1070, align 4, !tbaa !7
  %sub1071 = sub nsw i32 0, %1337
  %1338 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1072 = getelementptr inbounds i32, i32* %1338, i32 31
  %1339 = load i32, i32* %arrayidx1072, align 4, !tbaa !7
  %add1073 = add nsw i32 %sub1071, %1339
  %1340 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1074 = getelementptr inbounds i32, i32* %1340, i32 30
  store i32 %add1073, i32* %arrayidx1074, align 4, !tbaa !7
  %1341 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1075 = getelementptr inbounds i32, i32* %1341, i32 31
  %1342 = load i32, i32* %arrayidx1075, align 4, !tbaa !7
  %1343 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1076 = getelementptr inbounds i32, i32* %1343, i32 30
  %1344 = load i32, i32* %arrayidx1076, align 4, !tbaa !7
  %add1077 = add nsw i32 %1342, %1344
  %1345 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1078 = getelementptr inbounds i32, i32* %1345, i32 31
  store i32 %add1077, i32* %arrayidx1078, align 4, !tbaa !7
  %1346 = load i32, i32* %stage, align 4, !tbaa !7
  %1347 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %1348 = load i32*, i32** %bf1, align 4, !tbaa !2
  %1349 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %1350 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx1079 = getelementptr inbounds i8, i8* %1349, i32 %1350
  %1351 = load i8, i8* %arrayidx1079, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %1346, i32* %1347, i32* %1348, i32 32, i8 signext %1351)
  %1352 = load i32, i32* %stage, align 4, !tbaa !7
  %inc1080 = add nsw i32 %1352, 1
  store i32 %inc1080, i32* %stage, align 4, !tbaa !7
  %1353 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1081 = sext i8 %1353 to i32
  %call1082 = call i32* @cospi_arr(i32 %conv1081)
  store i32* %call1082, i32** %cospi, align 4, !tbaa !2
  %1354 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %1354, i32** %bf0, align 4, !tbaa !2
  %arraydecay1083 = getelementptr inbounds [32 x i32], [32 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay1083, i32** %bf1, align 4, !tbaa !2
  %1355 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1084 = getelementptr inbounds i32, i32* %1355, i32 0
  %1356 = load i32, i32* %arrayidx1084, align 4, !tbaa !7
  %1357 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1085 = getelementptr inbounds i32, i32* %1357, i32 0
  store i32 %1356, i32* %arrayidx1085, align 4, !tbaa !7
  %1358 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1086 = getelementptr inbounds i32, i32* %1358, i32 1
  %1359 = load i32, i32* %arrayidx1086, align 4, !tbaa !7
  %1360 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1087 = getelementptr inbounds i32, i32* %1360, i32 1
  store i32 %1359, i32* %arrayidx1087, align 4, !tbaa !7
  %1361 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1088 = getelementptr inbounds i32, i32* %1361, i32 2
  %1362 = load i32, i32* %arrayidx1088, align 4, !tbaa !7
  %1363 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1089 = getelementptr inbounds i32, i32* %1363, i32 2
  store i32 %1362, i32* %arrayidx1089, align 4, !tbaa !7
  %1364 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1090 = getelementptr inbounds i32, i32* %1364, i32 3
  %1365 = load i32, i32* %arrayidx1090, align 4, !tbaa !7
  %1366 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1091 = getelementptr inbounds i32, i32* %1366, i32 3
  store i32 %1365, i32* %arrayidx1091, align 4, !tbaa !7
  %1367 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1092 = getelementptr inbounds i32, i32* %1367, i32 4
  %1368 = load i32, i32* %arrayidx1092, align 4, !tbaa !7
  %1369 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1093 = getelementptr inbounds i32, i32* %1369, i32 4
  store i32 %1368, i32* %arrayidx1093, align 4, !tbaa !7
  %1370 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1094 = getelementptr inbounds i32, i32* %1370, i32 5
  %1371 = load i32, i32* %arrayidx1094, align 4, !tbaa !7
  %1372 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1095 = getelementptr inbounds i32, i32* %1372, i32 5
  store i32 %1371, i32* %arrayidx1095, align 4, !tbaa !7
  %1373 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1096 = getelementptr inbounds i32, i32* %1373, i32 6
  %1374 = load i32, i32* %arrayidx1096, align 4, !tbaa !7
  %1375 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1097 = getelementptr inbounds i32, i32* %1375, i32 6
  store i32 %1374, i32* %arrayidx1097, align 4, !tbaa !7
  %1376 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1098 = getelementptr inbounds i32, i32* %1376, i32 7
  %1377 = load i32, i32* %arrayidx1098, align 4, !tbaa !7
  %1378 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1099 = getelementptr inbounds i32, i32* %1378, i32 7
  store i32 %1377, i32* %arrayidx1099, align 4, !tbaa !7
  %1379 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1100 = getelementptr inbounds i32, i32* %1379, i32 8
  %1380 = load i32, i32* %arrayidx1100, align 4, !tbaa !7
  %1381 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1101 = getelementptr inbounds i32, i32* %1381, i32 8
  store i32 %1380, i32* %arrayidx1101, align 4, !tbaa !7
  %1382 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1102 = getelementptr inbounds i32, i32* %1382, i32 9
  %1383 = load i32, i32* %arrayidx1102, align 4, !tbaa !7
  %1384 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1103 = getelementptr inbounds i32, i32* %1384, i32 9
  store i32 %1383, i32* %arrayidx1103, align 4, !tbaa !7
  %1385 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1104 = getelementptr inbounds i32, i32* %1385, i32 10
  %1386 = load i32, i32* %arrayidx1104, align 4, !tbaa !7
  %1387 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1105 = getelementptr inbounds i32, i32* %1387, i32 10
  store i32 %1386, i32* %arrayidx1105, align 4, !tbaa !7
  %1388 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1106 = getelementptr inbounds i32, i32* %1388, i32 11
  %1389 = load i32, i32* %arrayidx1106, align 4, !tbaa !7
  %1390 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1107 = getelementptr inbounds i32, i32* %1390, i32 11
  store i32 %1389, i32* %arrayidx1107, align 4, !tbaa !7
  %1391 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1108 = getelementptr inbounds i32, i32* %1391, i32 12
  %1392 = load i32, i32* %arrayidx1108, align 4, !tbaa !7
  %1393 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1109 = getelementptr inbounds i32, i32* %1393, i32 12
  store i32 %1392, i32* %arrayidx1109, align 4, !tbaa !7
  %1394 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1110 = getelementptr inbounds i32, i32* %1394, i32 13
  %1395 = load i32, i32* %arrayidx1110, align 4, !tbaa !7
  %1396 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1111 = getelementptr inbounds i32, i32* %1396, i32 13
  store i32 %1395, i32* %arrayidx1111, align 4, !tbaa !7
  %1397 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1112 = getelementptr inbounds i32, i32* %1397, i32 14
  %1398 = load i32, i32* %arrayidx1112, align 4, !tbaa !7
  %1399 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1113 = getelementptr inbounds i32, i32* %1399, i32 14
  store i32 %1398, i32* %arrayidx1113, align 4, !tbaa !7
  %1400 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1114 = getelementptr inbounds i32, i32* %1400, i32 15
  %1401 = load i32, i32* %arrayidx1114, align 4, !tbaa !7
  %1402 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1115 = getelementptr inbounds i32, i32* %1402, i32 15
  store i32 %1401, i32* %arrayidx1115, align 4, !tbaa !7
  %1403 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1116 = getelementptr inbounds i32, i32* %1403, i32 62
  %1404 = load i32, i32* %arrayidx1116, align 4, !tbaa !7
  %1405 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1117 = getelementptr inbounds i32, i32* %1405, i32 16
  %1406 = load i32, i32* %arrayidx1117, align 4, !tbaa !7
  %1407 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1118 = getelementptr inbounds i32, i32* %1407, i32 2
  %1408 = load i32, i32* %arrayidx1118, align 4, !tbaa !7
  %1409 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1119 = getelementptr inbounds i32, i32* %1409, i32 31
  %1410 = load i32, i32* %arrayidx1119, align 4, !tbaa !7
  %1411 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1120 = sext i8 %1411 to i32
  %call1121 = call i32 @half_btf(i32 %1404, i32 %1406, i32 %1408, i32 %1410, i32 %conv1120)
  %1412 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1122 = getelementptr inbounds i32, i32* %1412, i32 16
  store i32 %call1121, i32* %arrayidx1122, align 4, !tbaa !7
  %1413 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1123 = getelementptr inbounds i32, i32* %1413, i32 30
  %1414 = load i32, i32* %arrayidx1123, align 4, !tbaa !7
  %1415 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1124 = getelementptr inbounds i32, i32* %1415, i32 17
  %1416 = load i32, i32* %arrayidx1124, align 4, !tbaa !7
  %1417 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1125 = getelementptr inbounds i32, i32* %1417, i32 34
  %1418 = load i32, i32* %arrayidx1125, align 4, !tbaa !7
  %1419 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1126 = getelementptr inbounds i32, i32* %1419, i32 30
  %1420 = load i32, i32* %arrayidx1126, align 4, !tbaa !7
  %1421 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1127 = sext i8 %1421 to i32
  %call1128 = call i32 @half_btf(i32 %1414, i32 %1416, i32 %1418, i32 %1420, i32 %conv1127)
  %1422 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1129 = getelementptr inbounds i32, i32* %1422, i32 17
  store i32 %call1128, i32* %arrayidx1129, align 4, !tbaa !7
  %1423 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1130 = getelementptr inbounds i32, i32* %1423, i32 46
  %1424 = load i32, i32* %arrayidx1130, align 4, !tbaa !7
  %1425 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1131 = getelementptr inbounds i32, i32* %1425, i32 18
  %1426 = load i32, i32* %arrayidx1131, align 4, !tbaa !7
  %1427 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1132 = getelementptr inbounds i32, i32* %1427, i32 18
  %1428 = load i32, i32* %arrayidx1132, align 4, !tbaa !7
  %1429 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1133 = getelementptr inbounds i32, i32* %1429, i32 29
  %1430 = load i32, i32* %arrayidx1133, align 4, !tbaa !7
  %1431 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1134 = sext i8 %1431 to i32
  %call1135 = call i32 @half_btf(i32 %1424, i32 %1426, i32 %1428, i32 %1430, i32 %conv1134)
  %1432 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1136 = getelementptr inbounds i32, i32* %1432, i32 18
  store i32 %call1135, i32* %arrayidx1136, align 4, !tbaa !7
  %1433 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1137 = getelementptr inbounds i32, i32* %1433, i32 14
  %1434 = load i32, i32* %arrayidx1137, align 4, !tbaa !7
  %1435 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1138 = getelementptr inbounds i32, i32* %1435, i32 19
  %1436 = load i32, i32* %arrayidx1138, align 4, !tbaa !7
  %1437 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1139 = getelementptr inbounds i32, i32* %1437, i32 50
  %1438 = load i32, i32* %arrayidx1139, align 4, !tbaa !7
  %1439 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1140 = getelementptr inbounds i32, i32* %1439, i32 28
  %1440 = load i32, i32* %arrayidx1140, align 4, !tbaa !7
  %1441 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1141 = sext i8 %1441 to i32
  %call1142 = call i32 @half_btf(i32 %1434, i32 %1436, i32 %1438, i32 %1440, i32 %conv1141)
  %1442 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1143 = getelementptr inbounds i32, i32* %1442, i32 19
  store i32 %call1142, i32* %arrayidx1143, align 4, !tbaa !7
  %1443 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1144 = getelementptr inbounds i32, i32* %1443, i32 54
  %1444 = load i32, i32* %arrayidx1144, align 4, !tbaa !7
  %1445 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1145 = getelementptr inbounds i32, i32* %1445, i32 20
  %1446 = load i32, i32* %arrayidx1145, align 4, !tbaa !7
  %1447 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1146 = getelementptr inbounds i32, i32* %1447, i32 10
  %1448 = load i32, i32* %arrayidx1146, align 4, !tbaa !7
  %1449 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1147 = getelementptr inbounds i32, i32* %1449, i32 27
  %1450 = load i32, i32* %arrayidx1147, align 4, !tbaa !7
  %1451 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1148 = sext i8 %1451 to i32
  %call1149 = call i32 @half_btf(i32 %1444, i32 %1446, i32 %1448, i32 %1450, i32 %conv1148)
  %1452 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1150 = getelementptr inbounds i32, i32* %1452, i32 20
  store i32 %call1149, i32* %arrayidx1150, align 4, !tbaa !7
  %1453 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1151 = getelementptr inbounds i32, i32* %1453, i32 22
  %1454 = load i32, i32* %arrayidx1151, align 4, !tbaa !7
  %1455 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1152 = getelementptr inbounds i32, i32* %1455, i32 21
  %1456 = load i32, i32* %arrayidx1152, align 4, !tbaa !7
  %1457 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1153 = getelementptr inbounds i32, i32* %1457, i32 42
  %1458 = load i32, i32* %arrayidx1153, align 4, !tbaa !7
  %1459 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1154 = getelementptr inbounds i32, i32* %1459, i32 26
  %1460 = load i32, i32* %arrayidx1154, align 4, !tbaa !7
  %1461 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1155 = sext i8 %1461 to i32
  %call1156 = call i32 @half_btf(i32 %1454, i32 %1456, i32 %1458, i32 %1460, i32 %conv1155)
  %1462 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1157 = getelementptr inbounds i32, i32* %1462, i32 21
  store i32 %call1156, i32* %arrayidx1157, align 4, !tbaa !7
  %1463 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1158 = getelementptr inbounds i32, i32* %1463, i32 38
  %1464 = load i32, i32* %arrayidx1158, align 4, !tbaa !7
  %1465 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1159 = getelementptr inbounds i32, i32* %1465, i32 22
  %1466 = load i32, i32* %arrayidx1159, align 4, !tbaa !7
  %1467 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1160 = getelementptr inbounds i32, i32* %1467, i32 26
  %1468 = load i32, i32* %arrayidx1160, align 4, !tbaa !7
  %1469 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1161 = getelementptr inbounds i32, i32* %1469, i32 25
  %1470 = load i32, i32* %arrayidx1161, align 4, !tbaa !7
  %1471 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1162 = sext i8 %1471 to i32
  %call1163 = call i32 @half_btf(i32 %1464, i32 %1466, i32 %1468, i32 %1470, i32 %conv1162)
  %1472 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1164 = getelementptr inbounds i32, i32* %1472, i32 22
  store i32 %call1163, i32* %arrayidx1164, align 4, !tbaa !7
  %1473 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1165 = getelementptr inbounds i32, i32* %1473, i32 6
  %1474 = load i32, i32* %arrayidx1165, align 4, !tbaa !7
  %1475 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1166 = getelementptr inbounds i32, i32* %1475, i32 23
  %1476 = load i32, i32* %arrayidx1166, align 4, !tbaa !7
  %1477 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1167 = getelementptr inbounds i32, i32* %1477, i32 58
  %1478 = load i32, i32* %arrayidx1167, align 4, !tbaa !7
  %1479 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1168 = getelementptr inbounds i32, i32* %1479, i32 24
  %1480 = load i32, i32* %arrayidx1168, align 4, !tbaa !7
  %1481 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1169 = sext i8 %1481 to i32
  %call1170 = call i32 @half_btf(i32 %1474, i32 %1476, i32 %1478, i32 %1480, i32 %conv1169)
  %1482 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1171 = getelementptr inbounds i32, i32* %1482, i32 23
  store i32 %call1170, i32* %arrayidx1171, align 4, !tbaa !7
  %1483 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1172 = getelementptr inbounds i32, i32* %1483, i32 6
  %1484 = load i32, i32* %arrayidx1172, align 4, !tbaa !7
  %1485 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1173 = getelementptr inbounds i32, i32* %1485, i32 24
  %1486 = load i32, i32* %arrayidx1173, align 4, !tbaa !7
  %1487 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1174 = getelementptr inbounds i32, i32* %1487, i32 58
  %1488 = load i32, i32* %arrayidx1174, align 4, !tbaa !7
  %sub1175 = sub nsw i32 0, %1488
  %1489 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1176 = getelementptr inbounds i32, i32* %1489, i32 23
  %1490 = load i32, i32* %arrayidx1176, align 4, !tbaa !7
  %1491 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1177 = sext i8 %1491 to i32
  %call1178 = call i32 @half_btf(i32 %1484, i32 %1486, i32 %sub1175, i32 %1490, i32 %conv1177)
  %1492 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1179 = getelementptr inbounds i32, i32* %1492, i32 24
  store i32 %call1178, i32* %arrayidx1179, align 4, !tbaa !7
  %1493 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1180 = getelementptr inbounds i32, i32* %1493, i32 38
  %1494 = load i32, i32* %arrayidx1180, align 4, !tbaa !7
  %1495 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1181 = getelementptr inbounds i32, i32* %1495, i32 25
  %1496 = load i32, i32* %arrayidx1181, align 4, !tbaa !7
  %1497 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1182 = getelementptr inbounds i32, i32* %1497, i32 26
  %1498 = load i32, i32* %arrayidx1182, align 4, !tbaa !7
  %sub1183 = sub nsw i32 0, %1498
  %1499 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1184 = getelementptr inbounds i32, i32* %1499, i32 22
  %1500 = load i32, i32* %arrayidx1184, align 4, !tbaa !7
  %1501 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1185 = sext i8 %1501 to i32
  %call1186 = call i32 @half_btf(i32 %1494, i32 %1496, i32 %sub1183, i32 %1500, i32 %conv1185)
  %1502 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1187 = getelementptr inbounds i32, i32* %1502, i32 25
  store i32 %call1186, i32* %arrayidx1187, align 4, !tbaa !7
  %1503 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1188 = getelementptr inbounds i32, i32* %1503, i32 22
  %1504 = load i32, i32* %arrayidx1188, align 4, !tbaa !7
  %1505 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1189 = getelementptr inbounds i32, i32* %1505, i32 26
  %1506 = load i32, i32* %arrayidx1189, align 4, !tbaa !7
  %1507 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1190 = getelementptr inbounds i32, i32* %1507, i32 42
  %1508 = load i32, i32* %arrayidx1190, align 4, !tbaa !7
  %sub1191 = sub nsw i32 0, %1508
  %1509 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1192 = getelementptr inbounds i32, i32* %1509, i32 21
  %1510 = load i32, i32* %arrayidx1192, align 4, !tbaa !7
  %1511 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1193 = sext i8 %1511 to i32
  %call1194 = call i32 @half_btf(i32 %1504, i32 %1506, i32 %sub1191, i32 %1510, i32 %conv1193)
  %1512 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1195 = getelementptr inbounds i32, i32* %1512, i32 26
  store i32 %call1194, i32* %arrayidx1195, align 4, !tbaa !7
  %1513 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1196 = getelementptr inbounds i32, i32* %1513, i32 54
  %1514 = load i32, i32* %arrayidx1196, align 4, !tbaa !7
  %1515 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1197 = getelementptr inbounds i32, i32* %1515, i32 27
  %1516 = load i32, i32* %arrayidx1197, align 4, !tbaa !7
  %1517 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1198 = getelementptr inbounds i32, i32* %1517, i32 10
  %1518 = load i32, i32* %arrayidx1198, align 4, !tbaa !7
  %sub1199 = sub nsw i32 0, %1518
  %1519 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1200 = getelementptr inbounds i32, i32* %1519, i32 20
  %1520 = load i32, i32* %arrayidx1200, align 4, !tbaa !7
  %1521 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1201 = sext i8 %1521 to i32
  %call1202 = call i32 @half_btf(i32 %1514, i32 %1516, i32 %sub1199, i32 %1520, i32 %conv1201)
  %1522 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1203 = getelementptr inbounds i32, i32* %1522, i32 27
  store i32 %call1202, i32* %arrayidx1203, align 4, !tbaa !7
  %1523 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1204 = getelementptr inbounds i32, i32* %1523, i32 14
  %1524 = load i32, i32* %arrayidx1204, align 4, !tbaa !7
  %1525 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1205 = getelementptr inbounds i32, i32* %1525, i32 28
  %1526 = load i32, i32* %arrayidx1205, align 4, !tbaa !7
  %1527 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1206 = getelementptr inbounds i32, i32* %1527, i32 50
  %1528 = load i32, i32* %arrayidx1206, align 4, !tbaa !7
  %sub1207 = sub nsw i32 0, %1528
  %1529 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1208 = getelementptr inbounds i32, i32* %1529, i32 19
  %1530 = load i32, i32* %arrayidx1208, align 4, !tbaa !7
  %1531 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1209 = sext i8 %1531 to i32
  %call1210 = call i32 @half_btf(i32 %1524, i32 %1526, i32 %sub1207, i32 %1530, i32 %conv1209)
  %1532 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1211 = getelementptr inbounds i32, i32* %1532, i32 28
  store i32 %call1210, i32* %arrayidx1211, align 4, !tbaa !7
  %1533 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1212 = getelementptr inbounds i32, i32* %1533, i32 46
  %1534 = load i32, i32* %arrayidx1212, align 4, !tbaa !7
  %1535 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1213 = getelementptr inbounds i32, i32* %1535, i32 29
  %1536 = load i32, i32* %arrayidx1213, align 4, !tbaa !7
  %1537 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1214 = getelementptr inbounds i32, i32* %1537, i32 18
  %1538 = load i32, i32* %arrayidx1214, align 4, !tbaa !7
  %sub1215 = sub nsw i32 0, %1538
  %1539 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1216 = getelementptr inbounds i32, i32* %1539, i32 18
  %1540 = load i32, i32* %arrayidx1216, align 4, !tbaa !7
  %1541 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1217 = sext i8 %1541 to i32
  %call1218 = call i32 @half_btf(i32 %1534, i32 %1536, i32 %sub1215, i32 %1540, i32 %conv1217)
  %1542 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1219 = getelementptr inbounds i32, i32* %1542, i32 29
  store i32 %call1218, i32* %arrayidx1219, align 4, !tbaa !7
  %1543 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1220 = getelementptr inbounds i32, i32* %1543, i32 30
  %1544 = load i32, i32* %arrayidx1220, align 4, !tbaa !7
  %1545 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1221 = getelementptr inbounds i32, i32* %1545, i32 30
  %1546 = load i32, i32* %arrayidx1221, align 4, !tbaa !7
  %1547 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1222 = getelementptr inbounds i32, i32* %1547, i32 34
  %1548 = load i32, i32* %arrayidx1222, align 4, !tbaa !7
  %sub1223 = sub nsw i32 0, %1548
  %1549 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1224 = getelementptr inbounds i32, i32* %1549, i32 17
  %1550 = load i32, i32* %arrayidx1224, align 4, !tbaa !7
  %1551 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1225 = sext i8 %1551 to i32
  %call1226 = call i32 @half_btf(i32 %1544, i32 %1546, i32 %sub1223, i32 %1550, i32 %conv1225)
  %1552 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1227 = getelementptr inbounds i32, i32* %1552, i32 30
  store i32 %call1226, i32* %arrayidx1227, align 4, !tbaa !7
  %1553 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1228 = getelementptr inbounds i32, i32* %1553, i32 62
  %1554 = load i32, i32* %arrayidx1228, align 4, !tbaa !7
  %1555 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1229 = getelementptr inbounds i32, i32* %1555, i32 31
  %1556 = load i32, i32* %arrayidx1229, align 4, !tbaa !7
  %1557 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1230 = getelementptr inbounds i32, i32* %1557, i32 2
  %1558 = load i32, i32* %arrayidx1230, align 4, !tbaa !7
  %sub1231 = sub nsw i32 0, %1558
  %1559 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1232 = getelementptr inbounds i32, i32* %1559, i32 16
  %1560 = load i32, i32* %arrayidx1232, align 4, !tbaa !7
  %1561 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1233 = sext i8 %1561 to i32
  %call1234 = call i32 @half_btf(i32 %1554, i32 %1556, i32 %sub1231, i32 %1560, i32 %conv1233)
  %1562 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1235 = getelementptr inbounds i32, i32* %1562, i32 31
  store i32 %call1234, i32* %arrayidx1235, align 4, !tbaa !7
  %1563 = load i32, i32* %stage, align 4, !tbaa !7
  %1564 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %1565 = load i32*, i32** %bf1, align 4, !tbaa !2
  %1566 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %1567 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx1236 = getelementptr inbounds i8, i8* %1566, i32 %1567
  %1568 = load i8, i8* %arrayidx1236, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %1563, i32* %1564, i32* %1565, i32 32, i8 signext %1568)
  %1569 = load i32, i32* %stage, align 4, !tbaa !7
  %inc1237 = add nsw i32 %1569, 1
  store i32 %inc1237, i32* %stage, align 4, !tbaa !7
  %arraydecay1238 = getelementptr inbounds [32 x i32], [32 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay1238, i32** %bf0, align 4, !tbaa !2
  %1570 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %1570, i32** %bf1, align 4, !tbaa !2
  %1571 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1239 = getelementptr inbounds i32, i32* %1571, i32 0
  %1572 = load i32, i32* %arrayidx1239, align 4, !tbaa !7
  %1573 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1240 = getelementptr inbounds i32, i32* %1573, i32 0
  store i32 %1572, i32* %arrayidx1240, align 4, !tbaa !7
  %1574 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1241 = getelementptr inbounds i32, i32* %1574, i32 16
  %1575 = load i32, i32* %arrayidx1241, align 4, !tbaa !7
  %1576 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1242 = getelementptr inbounds i32, i32* %1576, i32 1
  store i32 %1575, i32* %arrayidx1242, align 4, !tbaa !7
  %1577 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1243 = getelementptr inbounds i32, i32* %1577, i32 8
  %1578 = load i32, i32* %arrayidx1243, align 4, !tbaa !7
  %1579 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1244 = getelementptr inbounds i32, i32* %1579, i32 2
  store i32 %1578, i32* %arrayidx1244, align 4, !tbaa !7
  %1580 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1245 = getelementptr inbounds i32, i32* %1580, i32 24
  %1581 = load i32, i32* %arrayidx1245, align 4, !tbaa !7
  %1582 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1246 = getelementptr inbounds i32, i32* %1582, i32 3
  store i32 %1581, i32* %arrayidx1246, align 4, !tbaa !7
  %1583 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1247 = getelementptr inbounds i32, i32* %1583, i32 4
  %1584 = load i32, i32* %arrayidx1247, align 4, !tbaa !7
  %1585 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1248 = getelementptr inbounds i32, i32* %1585, i32 4
  store i32 %1584, i32* %arrayidx1248, align 4, !tbaa !7
  %1586 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1249 = getelementptr inbounds i32, i32* %1586, i32 20
  %1587 = load i32, i32* %arrayidx1249, align 4, !tbaa !7
  %1588 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1250 = getelementptr inbounds i32, i32* %1588, i32 5
  store i32 %1587, i32* %arrayidx1250, align 4, !tbaa !7
  %1589 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1251 = getelementptr inbounds i32, i32* %1589, i32 12
  %1590 = load i32, i32* %arrayidx1251, align 4, !tbaa !7
  %1591 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1252 = getelementptr inbounds i32, i32* %1591, i32 6
  store i32 %1590, i32* %arrayidx1252, align 4, !tbaa !7
  %1592 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1253 = getelementptr inbounds i32, i32* %1592, i32 28
  %1593 = load i32, i32* %arrayidx1253, align 4, !tbaa !7
  %1594 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1254 = getelementptr inbounds i32, i32* %1594, i32 7
  store i32 %1593, i32* %arrayidx1254, align 4, !tbaa !7
  %1595 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1255 = getelementptr inbounds i32, i32* %1595, i32 2
  %1596 = load i32, i32* %arrayidx1255, align 4, !tbaa !7
  %1597 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1256 = getelementptr inbounds i32, i32* %1597, i32 8
  store i32 %1596, i32* %arrayidx1256, align 4, !tbaa !7
  %1598 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1257 = getelementptr inbounds i32, i32* %1598, i32 18
  %1599 = load i32, i32* %arrayidx1257, align 4, !tbaa !7
  %1600 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1258 = getelementptr inbounds i32, i32* %1600, i32 9
  store i32 %1599, i32* %arrayidx1258, align 4, !tbaa !7
  %1601 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1259 = getelementptr inbounds i32, i32* %1601, i32 10
  %1602 = load i32, i32* %arrayidx1259, align 4, !tbaa !7
  %1603 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1260 = getelementptr inbounds i32, i32* %1603, i32 10
  store i32 %1602, i32* %arrayidx1260, align 4, !tbaa !7
  %1604 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1261 = getelementptr inbounds i32, i32* %1604, i32 26
  %1605 = load i32, i32* %arrayidx1261, align 4, !tbaa !7
  %1606 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1262 = getelementptr inbounds i32, i32* %1606, i32 11
  store i32 %1605, i32* %arrayidx1262, align 4, !tbaa !7
  %1607 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1263 = getelementptr inbounds i32, i32* %1607, i32 6
  %1608 = load i32, i32* %arrayidx1263, align 4, !tbaa !7
  %1609 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1264 = getelementptr inbounds i32, i32* %1609, i32 12
  store i32 %1608, i32* %arrayidx1264, align 4, !tbaa !7
  %1610 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1265 = getelementptr inbounds i32, i32* %1610, i32 22
  %1611 = load i32, i32* %arrayidx1265, align 4, !tbaa !7
  %1612 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1266 = getelementptr inbounds i32, i32* %1612, i32 13
  store i32 %1611, i32* %arrayidx1266, align 4, !tbaa !7
  %1613 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1267 = getelementptr inbounds i32, i32* %1613, i32 14
  %1614 = load i32, i32* %arrayidx1267, align 4, !tbaa !7
  %1615 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1268 = getelementptr inbounds i32, i32* %1615, i32 14
  store i32 %1614, i32* %arrayidx1268, align 4, !tbaa !7
  %1616 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1269 = getelementptr inbounds i32, i32* %1616, i32 30
  %1617 = load i32, i32* %arrayidx1269, align 4, !tbaa !7
  %1618 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1270 = getelementptr inbounds i32, i32* %1618, i32 15
  store i32 %1617, i32* %arrayidx1270, align 4, !tbaa !7
  %1619 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1271 = getelementptr inbounds i32, i32* %1619, i32 1
  %1620 = load i32, i32* %arrayidx1271, align 4, !tbaa !7
  %1621 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1272 = getelementptr inbounds i32, i32* %1621, i32 16
  store i32 %1620, i32* %arrayidx1272, align 4, !tbaa !7
  %1622 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1273 = getelementptr inbounds i32, i32* %1622, i32 17
  %1623 = load i32, i32* %arrayidx1273, align 4, !tbaa !7
  %1624 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1274 = getelementptr inbounds i32, i32* %1624, i32 17
  store i32 %1623, i32* %arrayidx1274, align 4, !tbaa !7
  %1625 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1275 = getelementptr inbounds i32, i32* %1625, i32 9
  %1626 = load i32, i32* %arrayidx1275, align 4, !tbaa !7
  %1627 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1276 = getelementptr inbounds i32, i32* %1627, i32 18
  store i32 %1626, i32* %arrayidx1276, align 4, !tbaa !7
  %1628 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1277 = getelementptr inbounds i32, i32* %1628, i32 25
  %1629 = load i32, i32* %arrayidx1277, align 4, !tbaa !7
  %1630 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1278 = getelementptr inbounds i32, i32* %1630, i32 19
  store i32 %1629, i32* %arrayidx1278, align 4, !tbaa !7
  %1631 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1279 = getelementptr inbounds i32, i32* %1631, i32 5
  %1632 = load i32, i32* %arrayidx1279, align 4, !tbaa !7
  %1633 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1280 = getelementptr inbounds i32, i32* %1633, i32 20
  store i32 %1632, i32* %arrayidx1280, align 4, !tbaa !7
  %1634 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1281 = getelementptr inbounds i32, i32* %1634, i32 21
  %1635 = load i32, i32* %arrayidx1281, align 4, !tbaa !7
  %1636 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1282 = getelementptr inbounds i32, i32* %1636, i32 21
  store i32 %1635, i32* %arrayidx1282, align 4, !tbaa !7
  %1637 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1283 = getelementptr inbounds i32, i32* %1637, i32 13
  %1638 = load i32, i32* %arrayidx1283, align 4, !tbaa !7
  %1639 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1284 = getelementptr inbounds i32, i32* %1639, i32 22
  store i32 %1638, i32* %arrayidx1284, align 4, !tbaa !7
  %1640 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1285 = getelementptr inbounds i32, i32* %1640, i32 29
  %1641 = load i32, i32* %arrayidx1285, align 4, !tbaa !7
  %1642 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1286 = getelementptr inbounds i32, i32* %1642, i32 23
  store i32 %1641, i32* %arrayidx1286, align 4, !tbaa !7
  %1643 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1287 = getelementptr inbounds i32, i32* %1643, i32 3
  %1644 = load i32, i32* %arrayidx1287, align 4, !tbaa !7
  %1645 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1288 = getelementptr inbounds i32, i32* %1645, i32 24
  store i32 %1644, i32* %arrayidx1288, align 4, !tbaa !7
  %1646 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1289 = getelementptr inbounds i32, i32* %1646, i32 19
  %1647 = load i32, i32* %arrayidx1289, align 4, !tbaa !7
  %1648 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1290 = getelementptr inbounds i32, i32* %1648, i32 25
  store i32 %1647, i32* %arrayidx1290, align 4, !tbaa !7
  %1649 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1291 = getelementptr inbounds i32, i32* %1649, i32 11
  %1650 = load i32, i32* %arrayidx1291, align 4, !tbaa !7
  %1651 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1292 = getelementptr inbounds i32, i32* %1651, i32 26
  store i32 %1650, i32* %arrayidx1292, align 4, !tbaa !7
  %1652 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1293 = getelementptr inbounds i32, i32* %1652, i32 27
  %1653 = load i32, i32* %arrayidx1293, align 4, !tbaa !7
  %1654 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1294 = getelementptr inbounds i32, i32* %1654, i32 27
  store i32 %1653, i32* %arrayidx1294, align 4, !tbaa !7
  %1655 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1295 = getelementptr inbounds i32, i32* %1655, i32 7
  %1656 = load i32, i32* %arrayidx1295, align 4, !tbaa !7
  %1657 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1296 = getelementptr inbounds i32, i32* %1657, i32 28
  store i32 %1656, i32* %arrayidx1296, align 4, !tbaa !7
  %1658 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1297 = getelementptr inbounds i32, i32* %1658, i32 23
  %1659 = load i32, i32* %arrayidx1297, align 4, !tbaa !7
  %1660 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1298 = getelementptr inbounds i32, i32* %1660, i32 29
  store i32 %1659, i32* %arrayidx1298, align 4, !tbaa !7
  %1661 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1299 = getelementptr inbounds i32, i32* %1661, i32 15
  %1662 = load i32, i32* %arrayidx1299, align 4, !tbaa !7
  %1663 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1300 = getelementptr inbounds i32, i32* %1663, i32 30
  store i32 %1662, i32* %arrayidx1300, align 4, !tbaa !7
  %1664 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1301 = getelementptr inbounds i32, i32* %1664, i32 31
  %1665 = load i32, i32* %arrayidx1301, align 4, !tbaa !7
  %1666 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1302 = getelementptr inbounds i32, i32* %1666, i32 31
  store i32 %1665, i32* %arrayidx1302, align 4, !tbaa !7
  %1667 = load i32, i32* %stage, align 4, !tbaa !7
  %1668 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %1669 = load i32*, i32** %bf1, align 4, !tbaa !2
  %1670 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %1671 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx1303 = getelementptr inbounds i8, i8* %1670, i32 %1671
  %1672 = load i8, i8* %arrayidx1303, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %1667, i32* %1668, i32* %1669, i32 32, i8 signext %1672)
  %1673 = bitcast [32 x i32]* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %1673) #4
  %1674 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1674) #4
  %1675 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1675) #4
  %1676 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1676) #4
  %1677 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1677) #4
  %1678 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1678) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fadst4(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %bit = alloca i32, align 4
  %sinpi = alloca i32*, align 4
  %x0 = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %x3 = alloca i32, align 4
  %s0 = alloca i32, align 4
  %s1 = alloca i32, align 4
  %s2 = alloca i32, align 4
  %s3 = alloca i32, align 4
  %s4 = alloca i32, align 4
  %s5 = alloca i32, align 4
  %s6 = alloca i32, align 4
  %s7 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv = sext i8 %1 to i32
  store i32 %conv, i32* %bit, align 4, !tbaa !7
  %2 = bitcast i32** %sinpi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i32, i32* %bit, align 4, !tbaa !7
  %call = call i32* @sinpi_arr(i32 %3)
  store i32* %call, i32** %sinpi, align 4, !tbaa !2
  %4 = bitcast i32* %x0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %x1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %x3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %s0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %s1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %s2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %s3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %s4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %s5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %s6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %s7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %17 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %18 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %18, i32 0
  %19 = load i8, i8* %arrayidx, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 0, i32* %16, i32* %17, i32 4, i8 signext %19)
  %20 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %20, i32 0
  %21 = load i32, i32* %arrayidx1, align 4, !tbaa !7
  store i32 %21, i32* %x0, align 4, !tbaa !7
  %22 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %22, i32 1
  %23 = load i32, i32* %arrayidx2, align 4, !tbaa !7
  store i32 %23, i32* %x1, align 4, !tbaa !7
  %24 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx3, align 4, !tbaa !7
  store i32 %25, i32* %x2, align 4, !tbaa !7
  %26 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %26, i32 3
  %27 = load i32, i32* %arrayidx4, align 4, !tbaa !7
  store i32 %27, i32* %x3, align 4, !tbaa !7
  %28 = load i32, i32* %x0, align 4, !tbaa !7
  %29 = load i32, i32* %x1, align 4, !tbaa !7
  %or = or i32 %28, %29
  %30 = load i32, i32* %x2, align 4, !tbaa !7
  %or5 = or i32 %or, %30
  %31 = load i32, i32* %x3, align 4, !tbaa !7
  %or6 = or i32 %or5, %31
  %tobool = icmp ne i32 %or6, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %32 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %32, i32 3
  store i32 0, i32* %arrayidx7, align 4, !tbaa !7
  %33 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %33, i32 2
  store i32 0, i32* %arrayidx8, align 4, !tbaa !7
  %34 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %34, i32 1
  store i32 0, i32* %arrayidx9, align 4, !tbaa !7
  %35 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %35, i32 0
  store i32 0, i32* %arrayidx10, align 4, !tbaa !7
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %36 = load i32*, i32** %sinpi, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 1
  %37 = load i32, i32* %arrayidx11, align 4, !tbaa !7
  %38 = load i32, i32* %x0, align 4, !tbaa !7
  %mul = mul nsw i32 %37, %38
  %39 = load i32, i32* %bit, align 4, !tbaa !7
  %40 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %40, i32 1
  %41 = load i8, i8* %arrayidx12, align 1, !tbaa !6
  %conv13 = sext i8 %41 to i32
  %add = add nsw i32 %39, %conv13
  %conv14 = trunc i32 %add to i8
  %call15 = call i32 @range_check_value(i32 %mul, i8 signext %conv14)
  store i32 %call15, i32* %s0, align 4, !tbaa !7
  %42 = load i32*, i32** %sinpi, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %42, i32 4
  %43 = load i32, i32* %arrayidx16, align 4, !tbaa !7
  %44 = load i32, i32* %x0, align 4, !tbaa !7
  %mul17 = mul nsw i32 %43, %44
  %45 = load i32, i32* %bit, align 4, !tbaa !7
  %46 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i8, i8* %46, i32 1
  %47 = load i8, i8* %arrayidx18, align 1, !tbaa !6
  %conv19 = sext i8 %47 to i32
  %add20 = add nsw i32 %45, %conv19
  %conv21 = trunc i32 %add20 to i8
  %call22 = call i32 @range_check_value(i32 %mul17, i8 signext %conv21)
  store i32 %call22, i32* %s1, align 4, !tbaa !7
  %48 = load i32*, i32** %sinpi, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %48, i32 2
  %49 = load i32, i32* %arrayidx23, align 4, !tbaa !7
  %50 = load i32, i32* %x1, align 4, !tbaa !7
  %mul24 = mul nsw i32 %49, %50
  %51 = load i32, i32* %bit, align 4, !tbaa !7
  %52 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %52, i32 1
  %53 = load i8, i8* %arrayidx25, align 1, !tbaa !6
  %conv26 = sext i8 %53 to i32
  %add27 = add nsw i32 %51, %conv26
  %conv28 = trunc i32 %add27 to i8
  %call29 = call i32 @range_check_value(i32 %mul24, i8 signext %conv28)
  store i32 %call29, i32* %s2, align 4, !tbaa !7
  %54 = load i32*, i32** %sinpi, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %54, i32 1
  %55 = load i32, i32* %arrayidx30, align 4, !tbaa !7
  %56 = load i32, i32* %x1, align 4, !tbaa !7
  %mul31 = mul nsw i32 %55, %56
  %57 = load i32, i32* %bit, align 4, !tbaa !7
  %58 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i8, i8* %58, i32 1
  %59 = load i8, i8* %arrayidx32, align 1, !tbaa !6
  %conv33 = sext i8 %59 to i32
  %add34 = add nsw i32 %57, %conv33
  %conv35 = trunc i32 %add34 to i8
  %call36 = call i32 @range_check_value(i32 %mul31, i8 signext %conv35)
  store i32 %call36, i32* %s3, align 4, !tbaa !7
  %60 = load i32*, i32** %sinpi, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %60, i32 3
  %61 = load i32, i32* %arrayidx37, align 4, !tbaa !7
  %62 = load i32, i32* %x2, align 4, !tbaa !7
  %mul38 = mul nsw i32 %61, %62
  %63 = load i32, i32* %bit, align 4, !tbaa !7
  %64 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i8, i8* %64, i32 1
  %65 = load i8, i8* %arrayidx39, align 1, !tbaa !6
  %conv40 = sext i8 %65 to i32
  %add41 = add nsw i32 %63, %conv40
  %conv42 = trunc i32 %add41 to i8
  %call43 = call i32 @range_check_value(i32 %mul38, i8 signext %conv42)
  store i32 %call43, i32* %s4, align 4, !tbaa !7
  %66 = load i32*, i32** %sinpi, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %66, i32 4
  %67 = load i32, i32* %arrayidx44, align 4, !tbaa !7
  %68 = load i32, i32* %x3, align 4, !tbaa !7
  %mul45 = mul nsw i32 %67, %68
  %69 = load i32, i32* %bit, align 4, !tbaa !7
  %70 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i8, i8* %70, i32 1
  %71 = load i8, i8* %arrayidx46, align 1, !tbaa !6
  %conv47 = sext i8 %71 to i32
  %add48 = add nsw i32 %69, %conv47
  %conv49 = trunc i32 %add48 to i8
  %call50 = call i32 @range_check_value(i32 %mul45, i8 signext %conv49)
  store i32 %call50, i32* %s5, align 4, !tbaa !7
  %72 = load i32*, i32** %sinpi, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %72, i32 2
  %73 = load i32, i32* %arrayidx51, align 4, !tbaa !7
  %74 = load i32, i32* %x3, align 4, !tbaa !7
  %mul52 = mul nsw i32 %73, %74
  %75 = load i32, i32* %bit, align 4, !tbaa !7
  %76 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i8, i8* %76, i32 1
  %77 = load i8, i8* %arrayidx53, align 1, !tbaa !6
  %conv54 = sext i8 %77 to i32
  %add55 = add nsw i32 %75, %conv54
  %conv56 = trunc i32 %add55 to i8
  %call57 = call i32 @range_check_value(i32 %mul52, i8 signext %conv56)
  store i32 %call57, i32* %s6, align 4, !tbaa !7
  %78 = load i32, i32* %x0, align 4, !tbaa !7
  %79 = load i32, i32* %x1, align 4, !tbaa !7
  %add58 = add nsw i32 %78, %79
  %80 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i8, i8* %80, i32 1
  %81 = load i8, i8* %arrayidx59, align 1, !tbaa !6
  %call60 = call i32 @range_check_value(i32 %add58, i8 signext %81)
  store i32 %call60, i32* %s7, align 4, !tbaa !7
  %82 = load i32, i32* %s7, align 4, !tbaa !7
  %83 = load i32, i32* %x3, align 4, !tbaa !7
  %sub = sub nsw i32 %82, %83
  %84 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i8, i8* %84, i32 2
  %85 = load i8, i8* %arrayidx61, align 1, !tbaa !6
  %call62 = call i32 @range_check_value(i32 %sub, i8 signext %85)
  store i32 %call62, i32* %s7, align 4, !tbaa !7
  %86 = load i32, i32* %s0, align 4, !tbaa !7
  %87 = load i32, i32* %s2, align 4, !tbaa !7
  %add63 = add nsw i32 %86, %87
  %88 = load i32, i32* %bit, align 4, !tbaa !7
  %89 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i8, i8* %89, i32 3
  %90 = load i8, i8* %arrayidx64, align 1, !tbaa !6
  %conv65 = sext i8 %90 to i32
  %add66 = add nsw i32 %88, %conv65
  %conv67 = trunc i32 %add66 to i8
  %call68 = call i32 @range_check_value(i32 %add63, i8 signext %conv67)
  store i32 %call68, i32* %x0, align 4, !tbaa !7
  %91 = load i32*, i32** %sinpi, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i32, i32* %91, i32 3
  %92 = load i32, i32* %arrayidx69, align 4, !tbaa !7
  %93 = load i32, i32* %s7, align 4, !tbaa !7
  %mul70 = mul nsw i32 %92, %93
  %94 = load i32, i32* %bit, align 4, !tbaa !7
  %95 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i8, i8* %95, i32 3
  %96 = load i8, i8* %arrayidx71, align 1, !tbaa !6
  %conv72 = sext i8 %96 to i32
  %add73 = add nsw i32 %94, %conv72
  %conv74 = trunc i32 %add73 to i8
  %call75 = call i32 @range_check_value(i32 %mul70, i8 signext %conv74)
  store i32 %call75, i32* %x1, align 4, !tbaa !7
  %97 = load i32, i32* %s1, align 4, !tbaa !7
  %98 = load i32, i32* %s3, align 4, !tbaa !7
  %sub76 = sub nsw i32 %97, %98
  %99 = load i32, i32* %bit, align 4, !tbaa !7
  %100 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i8, i8* %100, i32 3
  %101 = load i8, i8* %arrayidx77, align 1, !tbaa !6
  %conv78 = sext i8 %101 to i32
  %add79 = add nsw i32 %99, %conv78
  %conv80 = trunc i32 %add79 to i8
  %call81 = call i32 @range_check_value(i32 %sub76, i8 signext %conv80)
  store i32 %call81, i32* %x2, align 4, !tbaa !7
  %102 = load i32, i32* %s4, align 4, !tbaa !7
  %103 = load i32, i32* %bit, align 4, !tbaa !7
  %104 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i8, i8* %104, i32 3
  %105 = load i8, i8* %arrayidx82, align 1, !tbaa !6
  %conv83 = sext i8 %105 to i32
  %add84 = add nsw i32 %103, %conv83
  %conv85 = trunc i32 %add84 to i8
  %call86 = call i32 @range_check_value(i32 %102, i8 signext %conv85)
  store i32 %call86, i32* %x3, align 4, !tbaa !7
  %106 = load i32, i32* %x0, align 4, !tbaa !7
  %107 = load i32, i32* %s5, align 4, !tbaa !7
  %add87 = add nsw i32 %106, %107
  %108 = load i32, i32* %bit, align 4, !tbaa !7
  %109 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i8, i8* %109, i32 4
  %110 = load i8, i8* %arrayidx88, align 1, !tbaa !6
  %conv89 = sext i8 %110 to i32
  %add90 = add nsw i32 %108, %conv89
  %conv91 = trunc i32 %add90 to i8
  %call92 = call i32 @range_check_value(i32 %add87, i8 signext %conv91)
  store i32 %call92, i32* %x0, align 4, !tbaa !7
  %111 = load i32, i32* %x2, align 4, !tbaa !7
  %112 = load i32, i32* %s6, align 4, !tbaa !7
  %add93 = add nsw i32 %111, %112
  %113 = load i32, i32* %bit, align 4, !tbaa !7
  %114 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i8, i8* %114, i32 4
  %115 = load i8, i8* %arrayidx94, align 1, !tbaa !6
  %conv95 = sext i8 %115 to i32
  %add96 = add nsw i32 %113, %conv95
  %conv97 = trunc i32 %add96 to i8
  %call98 = call i32 @range_check_value(i32 %add93, i8 signext %conv97)
  store i32 %call98, i32* %x2, align 4, !tbaa !7
  %116 = load i32, i32* %x0, align 4, !tbaa !7
  %117 = load i32, i32* %x3, align 4, !tbaa !7
  %add99 = add nsw i32 %116, %117
  %118 = load i32, i32* %bit, align 4, !tbaa !7
  %119 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i8, i8* %119, i32 5
  %120 = load i8, i8* %arrayidx100, align 1, !tbaa !6
  %conv101 = sext i8 %120 to i32
  %add102 = add nsw i32 %118, %conv101
  %conv103 = trunc i32 %add102 to i8
  %call104 = call i32 @range_check_value(i32 %add99, i8 signext %conv103)
  store i32 %call104, i32* %s0, align 4, !tbaa !7
  %121 = load i32, i32* %x1, align 4, !tbaa !7
  %122 = load i32, i32* %bit, align 4, !tbaa !7
  %123 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i8, i8* %123, i32 5
  %124 = load i8, i8* %arrayidx105, align 1, !tbaa !6
  %conv106 = sext i8 %124 to i32
  %add107 = add nsw i32 %122, %conv106
  %conv108 = trunc i32 %add107 to i8
  %call109 = call i32 @range_check_value(i32 %121, i8 signext %conv108)
  store i32 %call109, i32* %s1, align 4, !tbaa !7
  %125 = load i32, i32* %x2, align 4, !tbaa !7
  %126 = load i32, i32* %x3, align 4, !tbaa !7
  %sub110 = sub nsw i32 %125, %126
  %127 = load i32, i32* %bit, align 4, !tbaa !7
  %128 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i8, i8* %128, i32 5
  %129 = load i8, i8* %arrayidx111, align 1, !tbaa !6
  %conv112 = sext i8 %129 to i32
  %add113 = add nsw i32 %127, %conv112
  %conv114 = trunc i32 %add113 to i8
  %call115 = call i32 @range_check_value(i32 %sub110, i8 signext %conv114)
  store i32 %call115, i32* %s2, align 4, !tbaa !7
  %130 = load i32, i32* %x2, align 4, !tbaa !7
  %131 = load i32, i32* %x0, align 4, !tbaa !7
  %sub116 = sub nsw i32 %130, %131
  %132 = load i32, i32* %bit, align 4, !tbaa !7
  %133 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds i8, i8* %133, i32 5
  %134 = load i8, i8* %arrayidx117, align 1, !tbaa !6
  %conv118 = sext i8 %134 to i32
  %add119 = add nsw i32 %132, %conv118
  %conv120 = trunc i32 %add119 to i8
  %call121 = call i32 @range_check_value(i32 %sub116, i8 signext %conv120)
  store i32 %call121, i32* %s3, align 4, !tbaa !7
  %135 = load i32, i32* %s3, align 4, !tbaa !7
  %136 = load i32, i32* %x3, align 4, !tbaa !7
  %add122 = add nsw i32 %135, %136
  %137 = load i32, i32* %bit, align 4, !tbaa !7
  %138 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i8, i8* %138, i32 6
  %139 = load i8, i8* %arrayidx123, align 1, !tbaa !6
  %conv124 = sext i8 %139 to i32
  %add125 = add nsw i32 %137, %conv124
  %conv126 = trunc i32 %add125 to i8
  %call127 = call i32 @range_check_value(i32 %add122, i8 signext %conv126)
  store i32 %call127, i32* %s3, align 4, !tbaa !7
  %140 = load i32, i32* %s0, align 4, !tbaa !7
  %conv128 = sext i32 %140 to i64
  %141 = load i32, i32* %bit, align 4, !tbaa !7
  %call129 = call i32 @round_shift(i64 %conv128, i32 %141)
  %142 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i32, i32* %142, i32 0
  store i32 %call129, i32* %arrayidx130, align 4, !tbaa !7
  %143 = load i32, i32* %s1, align 4, !tbaa !7
  %conv131 = sext i32 %143 to i64
  %144 = load i32, i32* %bit, align 4, !tbaa !7
  %call132 = call i32 @round_shift(i64 %conv131, i32 %144)
  %145 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i32, i32* %145, i32 1
  store i32 %call132, i32* %arrayidx133, align 4, !tbaa !7
  %146 = load i32, i32* %s2, align 4, !tbaa !7
  %conv134 = sext i32 %146 to i64
  %147 = load i32, i32* %bit, align 4, !tbaa !7
  %call135 = call i32 @round_shift(i64 %conv134, i32 %147)
  %148 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %148, i32 2
  store i32 %call135, i32* %arrayidx136, align 4, !tbaa !7
  %149 = load i32, i32* %s3, align 4, !tbaa !7
  %conv137 = sext i32 %149 to i64
  %150 = load i32, i32* %bit, align 4, !tbaa !7
  %call138 = call i32 @round_shift(i64 %conv137, i32 %150)
  %151 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i32, i32* %151, i32 3
  store i32 %call138, i32* %arrayidx139, align 4, !tbaa !7
  %152 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %153 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %154 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds i8, i8* %154, i32 6
  %155 = load i8, i8* %arrayidx140, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 6, i32* %152, i32* %153, i32 4, i8 signext %155)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %156 = bitcast i32* %s7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #4
  %157 = bitcast i32* %s6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %158 = bitcast i32* %s5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  %159 = bitcast i32* %s4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #4
  %160 = bitcast i32* %s3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #4
  %161 = bitcast i32* %s2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #4
  %162 = bitcast i32* %s1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #4
  %163 = bitcast i32* %s0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #4
  %164 = bitcast i32* %x3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #4
  %165 = bitcast i32* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #4
  %166 = bitcast i32* %x1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #4
  %167 = bitcast i32* %x0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  %168 = bitcast i32** %sinpi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  %169 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define internal i32* @sinpi_arr(i32 %n) #3 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !7
  %0 = load i32, i32* %n.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %0, 10
  %arrayidx = getelementptr inbounds [7 x [5 x i32]], [7 x [5 x i32]]* @av1_sinpi_arr_data, i32 0, i32 %sub
  %arraydecay = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx, i32 0, i32 0
  ret i32* %arraydecay
}

; Function Attrs: inlinehint nounwind
define internal i32 @range_check_value(i32 %value, i8 signext %bit) #3 {
entry:
  %value.addr = alloca i32, align 4
  %bit.addr = alloca i8, align 1
  store i32 %value, i32* %value.addr, align 4, !tbaa !7
  store i8 %bit, i8* %bit.addr, align 1, !tbaa !6
  %0 = load i8, i8* %bit.addr, align 1, !tbaa !6
  %1 = load i32, i32* %value.addr, align 4, !tbaa !7
  ret i32 %1
}

; Function Attrs: inlinehint nounwind
define internal i32 @round_shift(i64 %value, i32 %bit) #3 {
entry:
  %value.addr = alloca i64, align 8
  %bit.addr = alloca i32, align 4
  store i64 %value, i64* %value.addr, align 8, !tbaa !9
  store i32 %bit, i32* %bit.addr, align 4, !tbaa !7
  %0 = load i64, i64* %value.addr, align 8, !tbaa !9
  %1 = load i32, i32* %bit.addr, align 4, !tbaa !7
  %sub = sub nsw i32 %1, 1
  %sh_prom = zext i32 %sub to i64
  %shl = shl i64 1, %sh_prom
  %add = add nsw i64 %0, %shl
  %2 = load i32, i32* %bit.addr, align 4, !tbaa !7
  %sh_prom1 = zext i32 %2 to i64
  %shr = ashr i64 %add, %sh_prom1
  %conv = trunc i64 %shr to i32
  ret i32 %conv
}

; Function Attrs: nounwind
define hidden void @av1_fadst8(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %size = alloca i32, align 4
  %cospi = alloca i32*, align 4
  %stage = alloca i32, align 4
  %bf0 = alloca i32*, align 4
  %bf1 = alloca i32*, align 4
  %step = alloca [8 x i32], align 16
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 8, i32* %size, align 4, !tbaa !7
  %1 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %stage, align 4, !tbaa !7
  %3 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast [8 x i32]* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %5) #4
  %6 = load i32, i32* %stage, align 4, !tbaa !7
  %7 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %8 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %6, i32* %7, i32* %8, i32 8, i8 signext %11)
  %12 = load i32, i32* %stage, align 4, !tbaa !7
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %stage, align 4, !tbaa !7
  %13 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %13, i32** %bf1, align 4, !tbaa !2
  %14 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %14, i32 0
  %15 = load i32, i32* %arrayidx1, align 4, !tbaa !7
  %16 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %16, i32 0
  store i32 %15, i32* %arrayidx2, align 4, !tbaa !7
  %17 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %17, i32 7
  %18 = load i32, i32* %arrayidx3, align 4, !tbaa !7
  %sub = sub nsw i32 0, %18
  %19 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %19, i32 1
  store i32 %sub, i32* %arrayidx4, align 4, !tbaa !7
  %20 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %20, i32 3
  %21 = load i32, i32* %arrayidx5, align 4, !tbaa !7
  %sub6 = sub nsw i32 0, %21
  %22 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %22, i32 2
  store i32 %sub6, i32* %arrayidx7, align 4, !tbaa !7
  %23 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %23, i32 4
  %24 = load i32, i32* %arrayidx8, align 4, !tbaa !7
  %25 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %25, i32 3
  store i32 %24, i32* %arrayidx9, align 4, !tbaa !7
  %26 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %26, i32 1
  %27 = load i32, i32* %arrayidx10, align 4, !tbaa !7
  %sub11 = sub nsw i32 0, %27
  %28 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %28, i32 4
  store i32 %sub11, i32* %arrayidx12, align 4, !tbaa !7
  %29 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %29, i32 6
  %30 = load i32, i32* %arrayidx13, align 4, !tbaa !7
  %31 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %31, i32 5
  store i32 %30, i32* %arrayidx14, align 4, !tbaa !7
  %32 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i32, i32* %32, i32 2
  %33 = load i32, i32* %arrayidx15, align 4, !tbaa !7
  %34 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %34, i32 6
  store i32 %33, i32* %arrayidx16, align 4, !tbaa !7
  %35 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %35, i32 5
  %36 = load i32, i32* %arrayidx17, align 4, !tbaa !7
  %sub18 = sub nsw i32 0, %36
  %37 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %37, i32 7
  store i32 %sub18, i32* %arrayidx19, align 4, !tbaa !7
  %38 = load i32, i32* %stage, align 4, !tbaa !7
  %39 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %40 = load i32*, i32** %bf1, align 4, !tbaa !2
  %41 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %42 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx20 = getelementptr inbounds i8, i8* %41, i32 %42
  %43 = load i8, i8* %arrayidx20, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %38, i32* %39, i32* %40, i32 8, i8 signext %43)
  %44 = load i32, i32* %stage, align 4, !tbaa !7
  %inc21 = add nsw i32 %44, 1
  store i32 %inc21, i32* %stage, align 4, !tbaa !7
  %45 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv = sext i8 %45 to i32
  %call = call i32* @cospi_arr(i32 %conv)
  store i32* %call, i32** %cospi, align 4, !tbaa !2
  %46 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %46, i32** %bf0, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay, i32** %bf1, align 4, !tbaa !2
  %47 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i32, i32* %47, i32 0
  %48 = load i32, i32* %arrayidx22, align 4, !tbaa !7
  %49 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %49, i32 0
  store i32 %48, i32* %arrayidx23, align 4, !tbaa !7
  %50 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %50, i32 1
  %51 = load i32, i32* %arrayidx24, align 4, !tbaa !7
  %52 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %52, i32 1
  store i32 %51, i32* %arrayidx25, align 4, !tbaa !7
  %53 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i32, i32* %53, i32 32
  %54 = load i32, i32* %arrayidx26, align 4, !tbaa !7
  %55 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %55, i32 2
  %56 = load i32, i32* %arrayidx27, align 4, !tbaa !7
  %57 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i32, i32* %57, i32 32
  %58 = load i32, i32* %arrayidx28, align 4, !tbaa !7
  %59 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %59, i32 3
  %60 = load i32, i32* %arrayidx29, align 4, !tbaa !7
  %61 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv30 = sext i8 %61 to i32
  %call31 = call i32 @half_btf(i32 %54, i32 %56, i32 %58, i32 %60, i32 %conv30)
  %62 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %62, i32 2
  store i32 %call31, i32* %arrayidx32, align 4, !tbaa !7
  %63 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %63, i32 32
  %64 = load i32, i32* %arrayidx33, align 4, !tbaa !7
  %65 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %65, i32 2
  %66 = load i32, i32* %arrayidx34, align 4, !tbaa !7
  %67 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %67, i32 32
  %68 = load i32, i32* %arrayidx35, align 4, !tbaa !7
  %sub36 = sub nsw i32 0, %68
  %69 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %69, i32 3
  %70 = load i32, i32* %arrayidx37, align 4, !tbaa !7
  %71 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv38 = sext i8 %71 to i32
  %call39 = call i32 @half_btf(i32 %64, i32 %66, i32 %sub36, i32 %70, i32 %conv38)
  %72 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %72, i32 3
  store i32 %call39, i32* %arrayidx40, align 4, !tbaa !7
  %73 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %73, i32 4
  %74 = load i32, i32* %arrayidx41, align 4, !tbaa !7
  %75 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %75, i32 4
  store i32 %74, i32* %arrayidx42, align 4, !tbaa !7
  %76 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %76, i32 5
  %77 = load i32, i32* %arrayidx43, align 4, !tbaa !7
  %78 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %78, i32 5
  store i32 %77, i32* %arrayidx44, align 4, !tbaa !7
  %79 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %79, i32 32
  %80 = load i32, i32* %arrayidx45, align 4, !tbaa !7
  %81 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i32, i32* %81, i32 6
  %82 = load i32, i32* %arrayidx46, align 4, !tbaa !7
  %83 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %83, i32 32
  %84 = load i32, i32* %arrayidx47, align 4, !tbaa !7
  %85 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %85, i32 7
  %86 = load i32, i32* %arrayidx48, align 4, !tbaa !7
  %87 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv49 = sext i8 %87 to i32
  %call50 = call i32 @half_btf(i32 %80, i32 %82, i32 %84, i32 %86, i32 %conv49)
  %88 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %88, i32 6
  store i32 %call50, i32* %arrayidx51, align 4, !tbaa !7
  %89 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %89, i32 32
  %90 = load i32, i32* %arrayidx52, align 4, !tbaa !7
  %91 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i32, i32* %91, i32 6
  %92 = load i32, i32* %arrayidx53, align 4, !tbaa !7
  %93 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds i32, i32* %93, i32 32
  %94 = load i32, i32* %arrayidx54, align 4, !tbaa !7
  %sub55 = sub nsw i32 0, %94
  %95 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i32, i32* %95, i32 7
  %96 = load i32, i32* %arrayidx56, align 4, !tbaa !7
  %97 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv57 = sext i8 %97 to i32
  %call58 = call i32 @half_btf(i32 %90, i32 %92, i32 %sub55, i32 %96, i32 %conv57)
  %98 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %98, i32 7
  store i32 %call58, i32* %arrayidx59, align 4, !tbaa !7
  %99 = load i32, i32* %stage, align 4, !tbaa !7
  %100 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %101 = load i32*, i32** %bf1, align 4, !tbaa !2
  %102 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %103 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx60 = getelementptr inbounds i8, i8* %102, i32 %103
  %104 = load i8, i8* %arrayidx60, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %99, i32* %100, i32* %101, i32 8, i8 signext %104)
  %105 = load i32, i32* %stage, align 4, !tbaa !7
  %inc61 = add nsw i32 %105, 1
  store i32 %inc61, i32* %stage, align 4, !tbaa !7
  %arraydecay62 = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay62, i32** %bf0, align 4, !tbaa !2
  %106 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %106, i32** %bf1, align 4, !tbaa !2
  %107 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %107, i32 0
  %108 = load i32, i32* %arrayidx63, align 4, !tbaa !7
  %109 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %109, i32 2
  %110 = load i32, i32* %arrayidx64, align 4, !tbaa !7
  %add = add nsw i32 %108, %110
  %111 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %111, i32 0
  store i32 %add, i32* %arrayidx65, align 4, !tbaa !7
  %112 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %112, i32 1
  %113 = load i32, i32* %arrayidx66, align 4, !tbaa !7
  %114 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %114, i32 3
  %115 = load i32, i32* %arrayidx67, align 4, !tbaa !7
  %add68 = add nsw i32 %113, %115
  %116 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i32, i32* %116, i32 1
  store i32 %add68, i32* %arrayidx69, align 4, !tbaa !7
  %117 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i32, i32* %117, i32 0
  %118 = load i32, i32* %arrayidx70, align 4, !tbaa !7
  %119 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %119, i32 2
  %120 = load i32, i32* %arrayidx71, align 4, !tbaa !7
  %sub72 = sub nsw i32 %118, %120
  %121 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %121, i32 2
  store i32 %sub72, i32* %arrayidx73, align 4, !tbaa !7
  %122 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i32, i32* %122, i32 1
  %123 = load i32, i32* %arrayidx74, align 4, !tbaa !7
  %124 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds i32, i32* %124, i32 3
  %125 = load i32, i32* %arrayidx75, align 4, !tbaa !7
  %sub76 = sub nsw i32 %123, %125
  %126 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %126, i32 3
  store i32 %sub76, i32* %arrayidx77, align 4, !tbaa !7
  %127 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %127, i32 4
  %128 = load i32, i32* %arrayidx78, align 4, !tbaa !7
  %129 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %129, i32 6
  %130 = load i32, i32* %arrayidx79, align 4, !tbaa !7
  %add80 = add nsw i32 %128, %130
  %131 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i32, i32* %131, i32 4
  store i32 %add80, i32* %arrayidx81, align 4, !tbaa !7
  %132 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %132, i32 5
  %133 = load i32, i32* %arrayidx82, align 4, !tbaa !7
  %134 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %134, i32 7
  %135 = load i32, i32* %arrayidx83, align 4, !tbaa !7
  %add84 = add nsw i32 %133, %135
  %136 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %136, i32 5
  store i32 %add84, i32* %arrayidx85, align 4, !tbaa !7
  %137 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i32, i32* %137, i32 4
  %138 = load i32, i32* %arrayidx86, align 4, !tbaa !7
  %139 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx87 = getelementptr inbounds i32, i32* %139, i32 6
  %140 = load i32, i32* %arrayidx87, align 4, !tbaa !7
  %sub88 = sub nsw i32 %138, %140
  %141 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i32, i32* %141, i32 6
  store i32 %sub88, i32* %arrayidx89, align 4, !tbaa !7
  %142 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i32, i32* %142, i32 5
  %143 = load i32, i32* %arrayidx90, align 4, !tbaa !7
  %144 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i32, i32* %144, i32 7
  %145 = load i32, i32* %arrayidx91, align 4, !tbaa !7
  %sub92 = sub nsw i32 %143, %145
  %146 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %146, i32 7
  store i32 %sub92, i32* %arrayidx93, align 4, !tbaa !7
  %147 = load i32, i32* %stage, align 4, !tbaa !7
  %148 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %149 = load i32*, i32** %bf1, align 4, !tbaa !2
  %150 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %151 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx94 = getelementptr inbounds i8, i8* %150, i32 %151
  %152 = load i8, i8* %arrayidx94, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %147, i32* %148, i32* %149, i32 8, i8 signext %152)
  %153 = load i32, i32* %stage, align 4, !tbaa !7
  %inc95 = add nsw i32 %153, 1
  store i32 %inc95, i32* %stage, align 4, !tbaa !7
  %154 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv96 = sext i8 %154 to i32
  %call97 = call i32* @cospi_arr(i32 %conv96)
  store i32* %call97, i32** %cospi, align 4, !tbaa !2
  %155 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %155, i32** %bf0, align 4, !tbaa !2
  %arraydecay98 = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay98, i32** %bf1, align 4, !tbaa !2
  %156 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %156, i32 0
  %157 = load i32, i32* %arrayidx99, align 4, !tbaa !7
  %158 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i32, i32* %158, i32 0
  store i32 %157, i32* %arrayidx100, align 4, !tbaa !7
  %159 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i32, i32* %159, i32 1
  %160 = load i32, i32* %arrayidx101, align 4, !tbaa !7
  %161 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %161, i32 1
  store i32 %160, i32* %arrayidx102, align 4, !tbaa !7
  %162 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %162, i32 2
  %163 = load i32, i32* %arrayidx103, align 4, !tbaa !7
  %164 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx104 = getelementptr inbounds i32, i32* %164, i32 2
  store i32 %163, i32* %arrayidx104, align 4, !tbaa !7
  %165 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i32, i32* %165, i32 3
  %166 = load i32, i32* %arrayidx105, align 4, !tbaa !7
  %167 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i32, i32* %167, i32 3
  store i32 %166, i32* %arrayidx106, align 4, !tbaa !7
  %168 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx107 = getelementptr inbounds i32, i32* %168, i32 16
  %169 = load i32, i32* %arrayidx107, align 4, !tbaa !7
  %170 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %170, i32 4
  %171 = load i32, i32* %arrayidx108, align 4, !tbaa !7
  %172 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %172, i32 48
  %173 = load i32, i32* %arrayidx109, align 4, !tbaa !7
  %174 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds i32, i32* %174, i32 5
  %175 = load i32, i32* %arrayidx110, align 4, !tbaa !7
  %176 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv111 = sext i8 %176 to i32
  %call112 = call i32 @half_btf(i32 %169, i32 %171, i32 %173, i32 %175, i32 %conv111)
  %177 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds i32, i32* %177, i32 4
  store i32 %call112, i32* %arrayidx113, align 4, !tbaa !7
  %178 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx114 = getelementptr inbounds i32, i32* %178, i32 48
  %179 = load i32, i32* %arrayidx114, align 4, !tbaa !7
  %180 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %180, i32 4
  %181 = load i32, i32* %arrayidx115, align 4, !tbaa !7
  %182 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds i32, i32* %182, i32 16
  %183 = load i32, i32* %arrayidx116, align 4, !tbaa !7
  %sub117 = sub nsw i32 0, %183
  %184 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %184, i32 5
  %185 = load i32, i32* %arrayidx118, align 4, !tbaa !7
  %186 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv119 = sext i8 %186 to i32
  %call120 = call i32 @half_btf(i32 %179, i32 %181, i32 %sub117, i32 %185, i32 %conv119)
  %187 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i32, i32* %187, i32 5
  store i32 %call120, i32* %arrayidx121, align 4, !tbaa !7
  %188 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %188, i32 48
  %189 = load i32, i32* %arrayidx122, align 4, !tbaa !7
  %sub123 = sub nsw i32 0, %189
  %190 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %190, i32 6
  %191 = load i32, i32* %arrayidx124, align 4, !tbaa !7
  %192 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %192, i32 16
  %193 = load i32, i32* %arrayidx125, align 4, !tbaa !7
  %194 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx126 = getelementptr inbounds i32, i32* %194, i32 7
  %195 = load i32, i32* %arrayidx126, align 4, !tbaa !7
  %196 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv127 = sext i8 %196 to i32
  %call128 = call i32 @half_btf(i32 %sub123, i32 %191, i32 %193, i32 %195, i32 %conv127)
  %197 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds i32, i32* %197, i32 6
  store i32 %call128, i32* %arrayidx129, align 4, !tbaa !7
  %198 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i32, i32* %198, i32 16
  %199 = load i32, i32* %arrayidx130, align 4, !tbaa !7
  %200 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i32, i32* %200, i32 6
  %201 = load i32, i32* %arrayidx131, align 4, !tbaa !7
  %202 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %202, i32 48
  %203 = load i32, i32* %arrayidx132, align 4, !tbaa !7
  %204 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i32, i32* %204, i32 7
  %205 = load i32, i32* %arrayidx133, align 4, !tbaa !7
  %206 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv134 = sext i8 %206 to i32
  %call135 = call i32 @half_btf(i32 %199, i32 %201, i32 %203, i32 %205, i32 %conv134)
  %207 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %207, i32 7
  store i32 %call135, i32* %arrayidx136, align 4, !tbaa !7
  %208 = load i32, i32* %stage, align 4, !tbaa !7
  %209 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %210 = load i32*, i32** %bf1, align 4, !tbaa !2
  %211 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %212 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx137 = getelementptr inbounds i8, i8* %211, i32 %212
  %213 = load i8, i8* %arrayidx137, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %208, i32* %209, i32* %210, i32 8, i8 signext %213)
  %214 = load i32, i32* %stage, align 4, !tbaa !7
  %inc138 = add nsw i32 %214, 1
  store i32 %inc138, i32* %stage, align 4, !tbaa !7
  %arraydecay139 = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay139, i32** %bf0, align 4, !tbaa !2
  %215 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %215, i32** %bf1, align 4, !tbaa !2
  %216 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds i32, i32* %216, i32 0
  %217 = load i32, i32* %arrayidx140, align 4, !tbaa !7
  %218 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx141 = getelementptr inbounds i32, i32* %218, i32 4
  %219 = load i32, i32* %arrayidx141, align 4, !tbaa !7
  %add142 = add nsw i32 %217, %219
  %220 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds i32, i32* %220, i32 0
  store i32 %add142, i32* %arrayidx143, align 4, !tbaa !7
  %221 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds i32, i32* %221, i32 1
  %222 = load i32, i32* %arrayidx144, align 4, !tbaa !7
  %223 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds i32, i32* %223, i32 5
  %224 = load i32, i32* %arrayidx145, align 4, !tbaa !7
  %add146 = add nsw i32 %222, %224
  %225 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx147 = getelementptr inbounds i32, i32* %225, i32 1
  store i32 %add146, i32* %arrayidx147, align 4, !tbaa !7
  %226 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx148 = getelementptr inbounds i32, i32* %226, i32 2
  %227 = load i32, i32* %arrayidx148, align 4, !tbaa !7
  %228 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx149 = getelementptr inbounds i32, i32* %228, i32 6
  %229 = load i32, i32* %arrayidx149, align 4, !tbaa !7
  %add150 = add nsw i32 %227, %229
  %230 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %230, i32 2
  store i32 %add150, i32* %arrayidx151, align 4, !tbaa !7
  %231 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds i32, i32* %231, i32 3
  %232 = load i32, i32* %arrayidx152, align 4, !tbaa !7
  %233 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %233, i32 7
  %234 = load i32, i32* %arrayidx153, align 4, !tbaa !7
  %add154 = add nsw i32 %232, %234
  %235 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds i32, i32* %235, i32 3
  store i32 %add154, i32* %arrayidx155, align 4, !tbaa !7
  %236 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds i32, i32* %236, i32 0
  %237 = load i32, i32* %arrayidx156, align 4, !tbaa !7
  %238 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx157 = getelementptr inbounds i32, i32* %238, i32 4
  %239 = load i32, i32* %arrayidx157, align 4, !tbaa !7
  %sub158 = sub nsw i32 %237, %239
  %240 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx159 = getelementptr inbounds i32, i32* %240, i32 4
  store i32 %sub158, i32* %arrayidx159, align 4, !tbaa !7
  %241 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx160 = getelementptr inbounds i32, i32* %241, i32 1
  %242 = load i32, i32* %arrayidx160, align 4, !tbaa !7
  %243 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds i32, i32* %243, i32 5
  %244 = load i32, i32* %arrayidx161, align 4, !tbaa !7
  %sub162 = sub nsw i32 %242, %244
  %245 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds i32, i32* %245, i32 5
  store i32 %sub162, i32* %arrayidx163, align 4, !tbaa !7
  %246 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i32, i32* %246, i32 2
  %247 = load i32, i32* %arrayidx164, align 4, !tbaa !7
  %248 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx165 = getelementptr inbounds i32, i32* %248, i32 6
  %249 = load i32, i32* %arrayidx165, align 4, !tbaa !7
  %sub166 = sub nsw i32 %247, %249
  %250 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds i32, i32* %250, i32 6
  store i32 %sub166, i32* %arrayidx167, align 4, !tbaa !7
  %251 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx168 = getelementptr inbounds i32, i32* %251, i32 3
  %252 = load i32, i32* %arrayidx168, align 4, !tbaa !7
  %253 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds i32, i32* %253, i32 7
  %254 = load i32, i32* %arrayidx169, align 4, !tbaa !7
  %sub170 = sub nsw i32 %252, %254
  %255 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx171 = getelementptr inbounds i32, i32* %255, i32 7
  store i32 %sub170, i32* %arrayidx171, align 4, !tbaa !7
  %256 = load i32, i32* %stage, align 4, !tbaa !7
  %257 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %258 = load i32*, i32** %bf1, align 4, !tbaa !2
  %259 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %260 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx172 = getelementptr inbounds i8, i8* %259, i32 %260
  %261 = load i8, i8* %arrayidx172, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %256, i32* %257, i32* %258, i32 8, i8 signext %261)
  %262 = load i32, i32* %stage, align 4, !tbaa !7
  %inc173 = add nsw i32 %262, 1
  store i32 %inc173, i32* %stage, align 4, !tbaa !7
  %263 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv174 = sext i8 %263 to i32
  %call175 = call i32* @cospi_arr(i32 %conv174)
  store i32* %call175, i32** %cospi, align 4, !tbaa !2
  %264 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %264, i32** %bf0, align 4, !tbaa !2
  %arraydecay176 = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay176, i32** %bf1, align 4, !tbaa !2
  %265 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx177 = getelementptr inbounds i32, i32* %265, i32 4
  %266 = load i32, i32* %arrayidx177, align 4, !tbaa !7
  %267 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx178 = getelementptr inbounds i32, i32* %267, i32 0
  %268 = load i32, i32* %arrayidx178, align 4, !tbaa !7
  %269 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds i32, i32* %269, i32 60
  %270 = load i32, i32* %arrayidx179, align 4, !tbaa !7
  %271 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx180 = getelementptr inbounds i32, i32* %271, i32 1
  %272 = load i32, i32* %arrayidx180, align 4, !tbaa !7
  %273 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv181 = sext i8 %273 to i32
  %call182 = call i32 @half_btf(i32 %266, i32 %268, i32 %270, i32 %272, i32 %conv181)
  %274 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx183 = getelementptr inbounds i32, i32* %274, i32 0
  store i32 %call182, i32* %arrayidx183, align 4, !tbaa !7
  %275 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx184 = getelementptr inbounds i32, i32* %275, i32 60
  %276 = load i32, i32* %arrayidx184, align 4, !tbaa !7
  %277 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx185 = getelementptr inbounds i32, i32* %277, i32 0
  %278 = load i32, i32* %arrayidx185, align 4, !tbaa !7
  %279 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds i32, i32* %279, i32 4
  %280 = load i32, i32* %arrayidx186, align 4, !tbaa !7
  %sub187 = sub nsw i32 0, %280
  %281 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx188 = getelementptr inbounds i32, i32* %281, i32 1
  %282 = load i32, i32* %arrayidx188, align 4, !tbaa !7
  %283 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv189 = sext i8 %283 to i32
  %call190 = call i32 @half_btf(i32 %276, i32 %278, i32 %sub187, i32 %282, i32 %conv189)
  %284 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx191 = getelementptr inbounds i32, i32* %284, i32 1
  store i32 %call190, i32* %arrayidx191, align 4, !tbaa !7
  %285 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds i32, i32* %285, i32 20
  %286 = load i32, i32* %arrayidx192, align 4, !tbaa !7
  %287 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx193 = getelementptr inbounds i32, i32* %287, i32 2
  %288 = load i32, i32* %arrayidx193, align 4, !tbaa !7
  %289 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds i32, i32* %289, i32 44
  %290 = load i32, i32* %arrayidx194, align 4, !tbaa !7
  %291 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx195 = getelementptr inbounds i32, i32* %291, i32 3
  %292 = load i32, i32* %arrayidx195, align 4, !tbaa !7
  %293 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv196 = sext i8 %293 to i32
  %call197 = call i32 @half_btf(i32 %286, i32 %288, i32 %290, i32 %292, i32 %conv196)
  %294 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx198 = getelementptr inbounds i32, i32* %294, i32 2
  store i32 %call197, i32* %arrayidx198, align 4, !tbaa !7
  %295 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx199 = getelementptr inbounds i32, i32* %295, i32 44
  %296 = load i32, i32* %arrayidx199, align 4, !tbaa !7
  %297 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx200 = getelementptr inbounds i32, i32* %297, i32 2
  %298 = load i32, i32* %arrayidx200, align 4, !tbaa !7
  %299 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx201 = getelementptr inbounds i32, i32* %299, i32 20
  %300 = load i32, i32* %arrayidx201, align 4, !tbaa !7
  %sub202 = sub nsw i32 0, %300
  %301 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx203 = getelementptr inbounds i32, i32* %301, i32 3
  %302 = load i32, i32* %arrayidx203, align 4, !tbaa !7
  %303 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv204 = sext i8 %303 to i32
  %call205 = call i32 @half_btf(i32 %296, i32 %298, i32 %sub202, i32 %302, i32 %conv204)
  %304 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx206 = getelementptr inbounds i32, i32* %304, i32 3
  store i32 %call205, i32* %arrayidx206, align 4, !tbaa !7
  %305 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx207 = getelementptr inbounds i32, i32* %305, i32 36
  %306 = load i32, i32* %arrayidx207, align 4, !tbaa !7
  %307 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx208 = getelementptr inbounds i32, i32* %307, i32 4
  %308 = load i32, i32* %arrayidx208, align 4, !tbaa !7
  %309 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx209 = getelementptr inbounds i32, i32* %309, i32 28
  %310 = load i32, i32* %arrayidx209, align 4, !tbaa !7
  %311 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx210 = getelementptr inbounds i32, i32* %311, i32 5
  %312 = load i32, i32* %arrayidx210, align 4, !tbaa !7
  %313 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv211 = sext i8 %313 to i32
  %call212 = call i32 @half_btf(i32 %306, i32 %308, i32 %310, i32 %312, i32 %conv211)
  %314 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx213 = getelementptr inbounds i32, i32* %314, i32 4
  store i32 %call212, i32* %arrayidx213, align 4, !tbaa !7
  %315 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx214 = getelementptr inbounds i32, i32* %315, i32 28
  %316 = load i32, i32* %arrayidx214, align 4, !tbaa !7
  %317 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx215 = getelementptr inbounds i32, i32* %317, i32 4
  %318 = load i32, i32* %arrayidx215, align 4, !tbaa !7
  %319 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx216 = getelementptr inbounds i32, i32* %319, i32 36
  %320 = load i32, i32* %arrayidx216, align 4, !tbaa !7
  %sub217 = sub nsw i32 0, %320
  %321 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx218 = getelementptr inbounds i32, i32* %321, i32 5
  %322 = load i32, i32* %arrayidx218, align 4, !tbaa !7
  %323 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv219 = sext i8 %323 to i32
  %call220 = call i32 @half_btf(i32 %316, i32 %318, i32 %sub217, i32 %322, i32 %conv219)
  %324 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx221 = getelementptr inbounds i32, i32* %324, i32 5
  store i32 %call220, i32* %arrayidx221, align 4, !tbaa !7
  %325 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx222 = getelementptr inbounds i32, i32* %325, i32 52
  %326 = load i32, i32* %arrayidx222, align 4, !tbaa !7
  %327 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx223 = getelementptr inbounds i32, i32* %327, i32 6
  %328 = load i32, i32* %arrayidx223, align 4, !tbaa !7
  %329 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds i32, i32* %329, i32 12
  %330 = load i32, i32* %arrayidx224, align 4, !tbaa !7
  %331 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx225 = getelementptr inbounds i32, i32* %331, i32 7
  %332 = load i32, i32* %arrayidx225, align 4, !tbaa !7
  %333 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv226 = sext i8 %333 to i32
  %call227 = call i32 @half_btf(i32 %326, i32 %328, i32 %330, i32 %332, i32 %conv226)
  %334 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx228 = getelementptr inbounds i32, i32* %334, i32 6
  store i32 %call227, i32* %arrayidx228, align 4, !tbaa !7
  %335 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx229 = getelementptr inbounds i32, i32* %335, i32 12
  %336 = load i32, i32* %arrayidx229, align 4, !tbaa !7
  %337 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx230 = getelementptr inbounds i32, i32* %337, i32 6
  %338 = load i32, i32* %arrayidx230, align 4, !tbaa !7
  %339 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx231 = getelementptr inbounds i32, i32* %339, i32 52
  %340 = load i32, i32* %arrayidx231, align 4, !tbaa !7
  %sub232 = sub nsw i32 0, %340
  %341 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx233 = getelementptr inbounds i32, i32* %341, i32 7
  %342 = load i32, i32* %arrayidx233, align 4, !tbaa !7
  %343 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv234 = sext i8 %343 to i32
  %call235 = call i32 @half_btf(i32 %336, i32 %338, i32 %sub232, i32 %342, i32 %conv234)
  %344 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds i32, i32* %344, i32 7
  store i32 %call235, i32* %arrayidx236, align 4, !tbaa !7
  %345 = load i32, i32* %stage, align 4, !tbaa !7
  %346 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %347 = load i32*, i32** %bf1, align 4, !tbaa !2
  %348 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %349 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx237 = getelementptr inbounds i8, i8* %348, i32 %349
  %350 = load i8, i8* %arrayidx237, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %345, i32* %346, i32* %347, i32 8, i8 signext %350)
  %351 = load i32, i32* %stage, align 4, !tbaa !7
  %inc238 = add nsw i32 %351, 1
  store i32 %inc238, i32* %stage, align 4, !tbaa !7
  %arraydecay239 = getelementptr inbounds [8 x i32], [8 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay239, i32** %bf0, align 4, !tbaa !2
  %352 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %352, i32** %bf1, align 4, !tbaa !2
  %353 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx240 = getelementptr inbounds i32, i32* %353, i32 1
  %354 = load i32, i32* %arrayidx240, align 4, !tbaa !7
  %355 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx241 = getelementptr inbounds i32, i32* %355, i32 0
  store i32 %354, i32* %arrayidx241, align 4, !tbaa !7
  %356 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx242 = getelementptr inbounds i32, i32* %356, i32 6
  %357 = load i32, i32* %arrayidx242, align 4, !tbaa !7
  %358 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx243 = getelementptr inbounds i32, i32* %358, i32 1
  store i32 %357, i32* %arrayidx243, align 4, !tbaa !7
  %359 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx244 = getelementptr inbounds i32, i32* %359, i32 3
  %360 = load i32, i32* %arrayidx244, align 4, !tbaa !7
  %361 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx245 = getelementptr inbounds i32, i32* %361, i32 2
  store i32 %360, i32* %arrayidx245, align 4, !tbaa !7
  %362 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx246 = getelementptr inbounds i32, i32* %362, i32 4
  %363 = load i32, i32* %arrayidx246, align 4, !tbaa !7
  %364 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx247 = getelementptr inbounds i32, i32* %364, i32 3
  store i32 %363, i32* %arrayidx247, align 4, !tbaa !7
  %365 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx248 = getelementptr inbounds i32, i32* %365, i32 5
  %366 = load i32, i32* %arrayidx248, align 4, !tbaa !7
  %367 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx249 = getelementptr inbounds i32, i32* %367, i32 4
  store i32 %366, i32* %arrayidx249, align 4, !tbaa !7
  %368 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx250 = getelementptr inbounds i32, i32* %368, i32 2
  %369 = load i32, i32* %arrayidx250, align 4, !tbaa !7
  %370 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx251 = getelementptr inbounds i32, i32* %370, i32 5
  store i32 %369, i32* %arrayidx251, align 4, !tbaa !7
  %371 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx252 = getelementptr inbounds i32, i32* %371, i32 7
  %372 = load i32, i32* %arrayidx252, align 4, !tbaa !7
  %373 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx253 = getelementptr inbounds i32, i32* %373, i32 6
  store i32 %372, i32* %arrayidx253, align 4, !tbaa !7
  %374 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx254 = getelementptr inbounds i32, i32* %374, i32 0
  %375 = load i32, i32* %arrayidx254, align 4, !tbaa !7
  %376 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx255 = getelementptr inbounds i32, i32* %376, i32 7
  store i32 %375, i32* %arrayidx255, align 4, !tbaa !7
  %377 = load i32, i32* %stage, align 4, !tbaa !7
  %378 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %379 = load i32*, i32** %bf1, align 4, !tbaa !2
  %380 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %381 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx256 = getelementptr inbounds i8, i8* %380, i32 %381
  %382 = load i8, i8* %arrayidx256, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %377, i32* %378, i32* %379, i32 8, i8 signext %382)
  %383 = bitcast [8 x i32]* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %383) #4
  %384 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %384) #4
  %385 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %385) #4
  %386 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %386) #4
  %387 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #4
  %388 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %388) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fadst16(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %size = alloca i32, align 4
  %cospi = alloca i32*, align 4
  %stage = alloca i32, align 4
  %bf0 = alloca i32*, align 4
  %bf1 = alloca i32*, align 4
  %step = alloca [16 x i32], align 16
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 16, i32* %size, align 4, !tbaa !7
  %1 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %stage, align 4, !tbaa !7
  %3 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast [16 x i32]* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %5) #4
  %6 = load i32, i32* %stage, align 4, !tbaa !7
  %7 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %8 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %6, i32* %7, i32* %8, i32 16, i8 signext %11)
  %12 = load i32, i32* %stage, align 4, !tbaa !7
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %stage, align 4, !tbaa !7
  %13 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %13, i32** %bf1, align 4, !tbaa !2
  %14 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %14, i32 0
  %15 = load i32, i32* %arrayidx1, align 4, !tbaa !7
  %16 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %16, i32 0
  store i32 %15, i32* %arrayidx2, align 4, !tbaa !7
  %17 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %17, i32 15
  %18 = load i32, i32* %arrayidx3, align 4, !tbaa !7
  %sub = sub nsw i32 0, %18
  %19 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %19, i32 1
  store i32 %sub, i32* %arrayidx4, align 4, !tbaa !7
  %20 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %20, i32 7
  %21 = load i32, i32* %arrayidx5, align 4, !tbaa !7
  %sub6 = sub nsw i32 0, %21
  %22 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %22, i32 2
  store i32 %sub6, i32* %arrayidx7, align 4, !tbaa !7
  %23 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %23, i32 8
  %24 = load i32, i32* %arrayidx8, align 4, !tbaa !7
  %25 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %25, i32 3
  store i32 %24, i32* %arrayidx9, align 4, !tbaa !7
  %26 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %26, i32 3
  %27 = load i32, i32* %arrayidx10, align 4, !tbaa !7
  %sub11 = sub nsw i32 0, %27
  %28 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %28, i32 4
  store i32 %sub11, i32* %arrayidx12, align 4, !tbaa !7
  %29 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %29, i32 12
  %30 = load i32, i32* %arrayidx13, align 4, !tbaa !7
  %31 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %31, i32 5
  store i32 %30, i32* %arrayidx14, align 4, !tbaa !7
  %32 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i32, i32* %32, i32 4
  %33 = load i32, i32* %arrayidx15, align 4, !tbaa !7
  %34 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %34, i32 6
  store i32 %33, i32* %arrayidx16, align 4, !tbaa !7
  %35 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %35, i32 11
  %36 = load i32, i32* %arrayidx17, align 4, !tbaa !7
  %sub18 = sub nsw i32 0, %36
  %37 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %37, i32 7
  store i32 %sub18, i32* %arrayidx19, align 4, !tbaa !7
  %38 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %38, i32 1
  %39 = load i32, i32* %arrayidx20, align 4, !tbaa !7
  %sub21 = sub nsw i32 0, %39
  %40 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i32, i32* %40, i32 8
  store i32 %sub21, i32* %arrayidx22, align 4, !tbaa !7
  %41 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %41, i32 14
  %42 = load i32, i32* %arrayidx23, align 4, !tbaa !7
  %43 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %43, i32 9
  store i32 %42, i32* %arrayidx24, align 4, !tbaa !7
  %44 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %44, i32 6
  %45 = load i32, i32* %arrayidx25, align 4, !tbaa !7
  %46 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i32, i32* %46, i32 10
  store i32 %45, i32* %arrayidx26, align 4, !tbaa !7
  %47 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %47, i32 9
  %48 = load i32, i32* %arrayidx27, align 4, !tbaa !7
  %sub28 = sub nsw i32 0, %48
  %49 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %49, i32 11
  store i32 %sub28, i32* %arrayidx29, align 4, !tbaa !7
  %50 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %50, i32 2
  %51 = load i32, i32* %arrayidx30, align 4, !tbaa !7
  %52 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i32, i32* %52, i32 12
  store i32 %51, i32* %arrayidx31, align 4, !tbaa !7
  %53 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %53, i32 13
  %54 = load i32, i32* %arrayidx32, align 4, !tbaa !7
  %sub33 = sub nsw i32 0, %54
  %55 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %55, i32 13
  store i32 %sub33, i32* %arrayidx34, align 4, !tbaa !7
  %56 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %56, i32 5
  %57 = load i32, i32* %arrayidx35, align 4, !tbaa !7
  %sub36 = sub nsw i32 0, %57
  %58 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %58, i32 14
  store i32 %sub36, i32* %arrayidx37, align 4, !tbaa !7
  %59 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %59, i32 10
  %60 = load i32, i32* %arrayidx38, align 4, !tbaa !7
  %61 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %61, i32 15
  store i32 %60, i32* %arrayidx39, align 4, !tbaa !7
  %62 = load i32, i32* %stage, align 4, !tbaa !7
  %63 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %64 = load i32*, i32** %bf1, align 4, !tbaa !2
  %65 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %66 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx40 = getelementptr inbounds i8, i8* %65, i32 %66
  %67 = load i8, i8* %arrayidx40, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %62, i32* %63, i32* %64, i32 16, i8 signext %67)
  %68 = load i32, i32* %stage, align 4, !tbaa !7
  %inc41 = add nsw i32 %68, 1
  store i32 %inc41, i32* %stage, align 4, !tbaa !7
  %69 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv = sext i8 %69 to i32
  %call = call i32* @cospi_arr(i32 %conv)
  store i32* %call, i32** %cospi, align 4, !tbaa !2
  %70 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %70, i32** %bf0, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay, i32** %bf1, align 4, !tbaa !2
  %71 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %71, i32 0
  %72 = load i32, i32* %arrayidx42, align 4, !tbaa !7
  %73 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %73, i32 0
  store i32 %72, i32* %arrayidx43, align 4, !tbaa !7
  %74 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %74, i32 1
  %75 = load i32, i32* %arrayidx44, align 4, !tbaa !7
  %76 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %76, i32 1
  store i32 %75, i32* %arrayidx45, align 4, !tbaa !7
  %77 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i32, i32* %77, i32 32
  %78 = load i32, i32* %arrayidx46, align 4, !tbaa !7
  %79 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %79, i32 2
  %80 = load i32, i32* %arrayidx47, align 4, !tbaa !7
  %81 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %81, i32 32
  %82 = load i32, i32* %arrayidx48, align 4, !tbaa !7
  %83 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i32, i32* %83, i32 3
  %84 = load i32, i32* %arrayidx49, align 4, !tbaa !7
  %85 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv50 = sext i8 %85 to i32
  %call51 = call i32 @half_btf(i32 %78, i32 %80, i32 %82, i32 %84, i32 %conv50)
  %86 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %86, i32 2
  store i32 %call51, i32* %arrayidx52, align 4, !tbaa !7
  %87 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i32, i32* %87, i32 32
  %88 = load i32, i32* %arrayidx53, align 4, !tbaa !7
  %89 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds i32, i32* %89, i32 2
  %90 = load i32, i32* %arrayidx54, align 4, !tbaa !7
  %91 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %91, i32 32
  %92 = load i32, i32* %arrayidx55, align 4, !tbaa !7
  %sub56 = sub nsw i32 0, %92
  %93 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i32, i32* %93, i32 3
  %94 = load i32, i32* %arrayidx57, align 4, !tbaa !7
  %95 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv58 = sext i8 %95 to i32
  %call59 = call i32 @half_btf(i32 %88, i32 %90, i32 %sub56, i32 %94, i32 %conv58)
  %96 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %96, i32 3
  store i32 %call59, i32* %arrayidx60, align 4, !tbaa !7
  %97 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %97, i32 4
  %98 = load i32, i32* %arrayidx61, align 4, !tbaa !7
  %99 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i32, i32* %99, i32 4
  store i32 %98, i32* %arrayidx62, align 4, !tbaa !7
  %100 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %100, i32 5
  %101 = load i32, i32* %arrayidx63, align 4, !tbaa !7
  %102 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %102, i32 5
  store i32 %101, i32* %arrayidx64, align 4, !tbaa !7
  %103 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %103, i32 32
  %104 = load i32, i32* %arrayidx65, align 4, !tbaa !7
  %105 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %105, i32 6
  %106 = load i32, i32* %arrayidx66, align 4, !tbaa !7
  %107 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %107, i32 32
  %108 = load i32, i32* %arrayidx67, align 4, !tbaa !7
  %109 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %109, i32 7
  %110 = load i32, i32* %arrayidx68, align 4, !tbaa !7
  %111 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv69 = sext i8 %111 to i32
  %call70 = call i32 @half_btf(i32 %104, i32 %106, i32 %108, i32 %110, i32 %conv69)
  %112 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %112, i32 6
  store i32 %call70, i32* %arrayidx71, align 4, !tbaa !7
  %113 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds i32, i32* %113, i32 32
  %114 = load i32, i32* %arrayidx72, align 4, !tbaa !7
  %115 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %115, i32 6
  %116 = load i32, i32* %arrayidx73, align 4, !tbaa !7
  %117 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i32, i32* %117, i32 32
  %118 = load i32, i32* %arrayidx74, align 4, !tbaa !7
  %sub75 = sub nsw i32 0, %118
  %119 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %119, i32 7
  %120 = load i32, i32* %arrayidx76, align 4, !tbaa !7
  %121 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv77 = sext i8 %121 to i32
  %call78 = call i32 @half_btf(i32 %114, i32 %116, i32 %sub75, i32 %120, i32 %conv77)
  %122 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %122, i32 7
  store i32 %call78, i32* %arrayidx79, align 4, !tbaa !7
  %123 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds i32, i32* %123, i32 8
  %124 = load i32, i32* %arrayidx80, align 4, !tbaa !7
  %125 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i32, i32* %125, i32 8
  store i32 %124, i32* %arrayidx81, align 4, !tbaa !7
  %126 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %126, i32 9
  %127 = load i32, i32* %arrayidx82, align 4, !tbaa !7
  %128 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %128, i32 9
  store i32 %127, i32* %arrayidx83, align 4, !tbaa !7
  %129 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i32, i32* %129, i32 32
  %130 = load i32, i32* %arrayidx84, align 4, !tbaa !7
  %131 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %131, i32 10
  %132 = load i32, i32* %arrayidx85, align 4, !tbaa !7
  %133 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i32, i32* %133, i32 32
  %134 = load i32, i32* %arrayidx86, align 4, !tbaa !7
  %135 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx87 = getelementptr inbounds i32, i32* %135, i32 11
  %136 = load i32, i32* %arrayidx87, align 4, !tbaa !7
  %137 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv88 = sext i8 %137 to i32
  %call89 = call i32 @half_btf(i32 %130, i32 %132, i32 %134, i32 %136, i32 %conv88)
  %138 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i32, i32* %138, i32 10
  store i32 %call89, i32* %arrayidx90, align 4, !tbaa !7
  %139 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i32, i32* %139, i32 32
  %140 = load i32, i32* %arrayidx91, align 4, !tbaa !7
  %141 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i32, i32* %141, i32 10
  %142 = load i32, i32* %arrayidx92, align 4, !tbaa !7
  %143 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %143, i32 32
  %144 = load i32, i32* %arrayidx93, align 4, !tbaa !7
  %sub94 = sub nsw i32 0, %144
  %145 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i32, i32* %145, i32 11
  %146 = load i32, i32* %arrayidx95, align 4, !tbaa !7
  %147 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv96 = sext i8 %147 to i32
  %call97 = call i32 @half_btf(i32 %140, i32 %142, i32 %sub94, i32 %146, i32 %conv96)
  %148 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i32, i32* %148, i32 11
  store i32 %call97, i32* %arrayidx98, align 4, !tbaa !7
  %149 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %149, i32 12
  %150 = load i32, i32* %arrayidx99, align 4, !tbaa !7
  %151 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i32, i32* %151, i32 12
  store i32 %150, i32* %arrayidx100, align 4, !tbaa !7
  %152 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i32, i32* %152, i32 13
  %153 = load i32, i32* %arrayidx101, align 4, !tbaa !7
  %154 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %154, i32 13
  store i32 %153, i32* %arrayidx102, align 4, !tbaa !7
  %155 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %155, i32 32
  %156 = load i32, i32* %arrayidx103, align 4, !tbaa !7
  %157 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx104 = getelementptr inbounds i32, i32* %157, i32 14
  %158 = load i32, i32* %arrayidx104, align 4, !tbaa !7
  %159 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i32, i32* %159, i32 32
  %160 = load i32, i32* %arrayidx105, align 4, !tbaa !7
  %161 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i32, i32* %161, i32 15
  %162 = load i32, i32* %arrayidx106, align 4, !tbaa !7
  %163 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv107 = sext i8 %163 to i32
  %call108 = call i32 @half_btf(i32 %156, i32 %158, i32 %160, i32 %162, i32 %conv107)
  %164 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %164, i32 14
  store i32 %call108, i32* %arrayidx109, align 4, !tbaa !7
  %165 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds i32, i32* %165, i32 32
  %166 = load i32, i32* %arrayidx110, align 4, !tbaa !7
  %167 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i32, i32* %167, i32 14
  %168 = load i32, i32* %arrayidx111, align 4, !tbaa !7
  %169 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %169, i32 32
  %170 = load i32, i32* %arrayidx112, align 4, !tbaa !7
  %sub113 = sub nsw i32 0, %170
  %171 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx114 = getelementptr inbounds i32, i32* %171, i32 15
  %172 = load i32, i32* %arrayidx114, align 4, !tbaa !7
  %173 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv115 = sext i8 %173 to i32
  %call116 = call i32 @half_btf(i32 %166, i32 %168, i32 %sub113, i32 %172, i32 %conv115)
  %174 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds i32, i32* %174, i32 15
  store i32 %call116, i32* %arrayidx117, align 4, !tbaa !7
  %175 = load i32, i32* %stage, align 4, !tbaa !7
  %176 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %177 = load i32*, i32** %bf1, align 4, !tbaa !2
  %178 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %179 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx118 = getelementptr inbounds i8, i8* %178, i32 %179
  %180 = load i8, i8* %arrayidx118, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %175, i32* %176, i32* %177, i32 16, i8 signext %180)
  %181 = load i32, i32* %stage, align 4, !tbaa !7
  %inc119 = add nsw i32 %181, 1
  store i32 %inc119, i32* %stage, align 4, !tbaa !7
  %arraydecay120 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay120, i32** %bf0, align 4, !tbaa !2
  %182 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %182, i32** %bf1, align 4, !tbaa !2
  %183 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i32, i32* %183, i32 0
  %184 = load i32, i32* %arrayidx121, align 4, !tbaa !7
  %185 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %185, i32 2
  %186 = load i32, i32* %arrayidx122, align 4, !tbaa !7
  %add = add nsw i32 %184, %186
  %187 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %187, i32 0
  store i32 %add, i32* %arrayidx123, align 4, !tbaa !7
  %188 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %188, i32 1
  %189 = load i32, i32* %arrayidx124, align 4, !tbaa !7
  %190 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %190, i32 3
  %191 = load i32, i32* %arrayidx125, align 4, !tbaa !7
  %add126 = add nsw i32 %189, %191
  %192 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %192, i32 1
  store i32 %add126, i32* %arrayidx127, align 4, !tbaa !7
  %193 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx128 = getelementptr inbounds i32, i32* %193, i32 0
  %194 = load i32, i32* %arrayidx128, align 4, !tbaa !7
  %195 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds i32, i32* %195, i32 2
  %196 = load i32, i32* %arrayidx129, align 4, !tbaa !7
  %sub130 = sub nsw i32 %194, %196
  %197 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i32, i32* %197, i32 2
  store i32 %sub130, i32* %arrayidx131, align 4, !tbaa !7
  %198 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %198, i32 1
  %199 = load i32, i32* %arrayidx132, align 4, !tbaa !7
  %200 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i32, i32* %200, i32 3
  %201 = load i32, i32* %arrayidx133, align 4, !tbaa !7
  %sub134 = sub nsw i32 %199, %201
  %202 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i32, i32* %202, i32 3
  store i32 %sub134, i32* %arrayidx135, align 4, !tbaa !7
  %203 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %203, i32 4
  %204 = load i32, i32* %arrayidx136, align 4, !tbaa !7
  %205 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds i32, i32* %205, i32 6
  %206 = load i32, i32* %arrayidx137, align 4, !tbaa !7
  %add138 = add nsw i32 %204, %206
  %207 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i32, i32* %207, i32 4
  store i32 %add138, i32* %arrayidx139, align 4, !tbaa !7
  %208 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds i32, i32* %208, i32 5
  %209 = load i32, i32* %arrayidx140, align 4, !tbaa !7
  %210 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx141 = getelementptr inbounds i32, i32* %210, i32 7
  %211 = load i32, i32* %arrayidx141, align 4, !tbaa !7
  %add142 = add nsw i32 %209, %211
  %212 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds i32, i32* %212, i32 5
  store i32 %add142, i32* %arrayidx143, align 4, !tbaa !7
  %213 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds i32, i32* %213, i32 4
  %214 = load i32, i32* %arrayidx144, align 4, !tbaa !7
  %215 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds i32, i32* %215, i32 6
  %216 = load i32, i32* %arrayidx145, align 4, !tbaa !7
  %sub146 = sub nsw i32 %214, %216
  %217 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx147 = getelementptr inbounds i32, i32* %217, i32 6
  store i32 %sub146, i32* %arrayidx147, align 4, !tbaa !7
  %218 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx148 = getelementptr inbounds i32, i32* %218, i32 5
  %219 = load i32, i32* %arrayidx148, align 4, !tbaa !7
  %220 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx149 = getelementptr inbounds i32, i32* %220, i32 7
  %221 = load i32, i32* %arrayidx149, align 4, !tbaa !7
  %sub150 = sub nsw i32 %219, %221
  %222 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %222, i32 7
  store i32 %sub150, i32* %arrayidx151, align 4, !tbaa !7
  %223 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds i32, i32* %223, i32 8
  %224 = load i32, i32* %arrayidx152, align 4, !tbaa !7
  %225 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %225, i32 10
  %226 = load i32, i32* %arrayidx153, align 4, !tbaa !7
  %add154 = add nsw i32 %224, %226
  %227 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds i32, i32* %227, i32 8
  store i32 %add154, i32* %arrayidx155, align 4, !tbaa !7
  %228 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds i32, i32* %228, i32 9
  %229 = load i32, i32* %arrayidx156, align 4, !tbaa !7
  %230 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx157 = getelementptr inbounds i32, i32* %230, i32 11
  %231 = load i32, i32* %arrayidx157, align 4, !tbaa !7
  %add158 = add nsw i32 %229, %231
  %232 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx159 = getelementptr inbounds i32, i32* %232, i32 9
  store i32 %add158, i32* %arrayidx159, align 4, !tbaa !7
  %233 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx160 = getelementptr inbounds i32, i32* %233, i32 8
  %234 = load i32, i32* %arrayidx160, align 4, !tbaa !7
  %235 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds i32, i32* %235, i32 10
  %236 = load i32, i32* %arrayidx161, align 4, !tbaa !7
  %sub162 = sub nsw i32 %234, %236
  %237 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds i32, i32* %237, i32 10
  store i32 %sub162, i32* %arrayidx163, align 4, !tbaa !7
  %238 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i32, i32* %238, i32 9
  %239 = load i32, i32* %arrayidx164, align 4, !tbaa !7
  %240 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx165 = getelementptr inbounds i32, i32* %240, i32 11
  %241 = load i32, i32* %arrayidx165, align 4, !tbaa !7
  %sub166 = sub nsw i32 %239, %241
  %242 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds i32, i32* %242, i32 11
  store i32 %sub166, i32* %arrayidx167, align 4, !tbaa !7
  %243 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx168 = getelementptr inbounds i32, i32* %243, i32 12
  %244 = load i32, i32* %arrayidx168, align 4, !tbaa !7
  %245 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds i32, i32* %245, i32 14
  %246 = load i32, i32* %arrayidx169, align 4, !tbaa !7
  %add170 = add nsw i32 %244, %246
  %247 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx171 = getelementptr inbounds i32, i32* %247, i32 12
  store i32 %add170, i32* %arrayidx171, align 4, !tbaa !7
  %248 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx172 = getelementptr inbounds i32, i32* %248, i32 13
  %249 = load i32, i32* %arrayidx172, align 4, !tbaa !7
  %250 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx173 = getelementptr inbounds i32, i32* %250, i32 15
  %251 = load i32, i32* %arrayidx173, align 4, !tbaa !7
  %add174 = add nsw i32 %249, %251
  %252 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx175 = getelementptr inbounds i32, i32* %252, i32 13
  store i32 %add174, i32* %arrayidx175, align 4, !tbaa !7
  %253 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds i32, i32* %253, i32 12
  %254 = load i32, i32* %arrayidx176, align 4, !tbaa !7
  %255 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx177 = getelementptr inbounds i32, i32* %255, i32 14
  %256 = load i32, i32* %arrayidx177, align 4, !tbaa !7
  %sub178 = sub nsw i32 %254, %256
  %257 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds i32, i32* %257, i32 14
  store i32 %sub178, i32* %arrayidx179, align 4, !tbaa !7
  %258 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx180 = getelementptr inbounds i32, i32* %258, i32 13
  %259 = load i32, i32* %arrayidx180, align 4, !tbaa !7
  %260 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx181 = getelementptr inbounds i32, i32* %260, i32 15
  %261 = load i32, i32* %arrayidx181, align 4, !tbaa !7
  %sub182 = sub nsw i32 %259, %261
  %262 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx183 = getelementptr inbounds i32, i32* %262, i32 15
  store i32 %sub182, i32* %arrayidx183, align 4, !tbaa !7
  %263 = load i32, i32* %stage, align 4, !tbaa !7
  %264 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %265 = load i32*, i32** %bf1, align 4, !tbaa !2
  %266 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %267 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx184 = getelementptr inbounds i8, i8* %266, i32 %267
  %268 = load i8, i8* %arrayidx184, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %263, i32* %264, i32* %265, i32 16, i8 signext %268)
  %269 = load i32, i32* %stage, align 4, !tbaa !7
  %inc185 = add nsw i32 %269, 1
  store i32 %inc185, i32* %stage, align 4, !tbaa !7
  %270 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv186 = sext i8 %270 to i32
  %call187 = call i32* @cospi_arr(i32 %conv186)
  store i32* %call187, i32** %cospi, align 4, !tbaa !2
  %271 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %271, i32** %bf0, align 4, !tbaa !2
  %arraydecay188 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay188, i32** %bf1, align 4, !tbaa !2
  %272 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx189 = getelementptr inbounds i32, i32* %272, i32 0
  %273 = load i32, i32* %arrayidx189, align 4, !tbaa !7
  %274 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx190 = getelementptr inbounds i32, i32* %274, i32 0
  store i32 %273, i32* %arrayidx190, align 4, !tbaa !7
  %275 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx191 = getelementptr inbounds i32, i32* %275, i32 1
  %276 = load i32, i32* %arrayidx191, align 4, !tbaa !7
  %277 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds i32, i32* %277, i32 1
  store i32 %276, i32* %arrayidx192, align 4, !tbaa !7
  %278 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx193 = getelementptr inbounds i32, i32* %278, i32 2
  %279 = load i32, i32* %arrayidx193, align 4, !tbaa !7
  %280 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds i32, i32* %280, i32 2
  store i32 %279, i32* %arrayidx194, align 4, !tbaa !7
  %281 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx195 = getelementptr inbounds i32, i32* %281, i32 3
  %282 = load i32, i32* %arrayidx195, align 4, !tbaa !7
  %283 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx196 = getelementptr inbounds i32, i32* %283, i32 3
  store i32 %282, i32* %arrayidx196, align 4, !tbaa !7
  %284 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx197 = getelementptr inbounds i32, i32* %284, i32 16
  %285 = load i32, i32* %arrayidx197, align 4, !tbaa !7
  %286 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx198 = getelementptr inbounds i32, i32* %286, i32 4
  %287 = load i32, i32* %arrayidx198, align 4, !tbaa !7
  %288 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx199 = getelementptr inbounds i32, i32* %288, i32 48
  %289 = load i32, i32* %arrayidx199, align 4, !tbaa !7
  %290 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx200 = getelementptr inbounds i32, i32* %290, i32 5
  %291 = load i32, i32* %arrayidx200, align 4, !tbaa !7
  %292 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv201 = sext i8 %292 to i32
  %call202 = call i32 @half_btf(i32 %285, i32 %287, i32 %289, i32 %291, i32 %conv201)
  %293 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx203 = getelementptr inbounds i32, i32* %293, i32 4
  store i32 %call202, i32* %arrayidx203, align 4, !tbaa !7
  %294 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx204 = getelementptr inbounds i32, i32* %294, i32 48
  %295 = load i32, i32* %arrayidx204, align 4, !tbaa !7
  %296 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx205 = getelementptr inbounds i32, i32* %296, i32 4
  %297 = load i32, i32* %arrayidx205, align 4, !tbaa !7
  %298 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx206 = getelementptr inbounds i32, i32* %298, i32 16
  %299 = load i32, i32* %arrayidx206, align 4, !tbaa !7
  %sub207 = sub nsw i32 0, %299
  %300 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx208 = getelementptr inbounds i32, i32* %300, i32 5
  %301 = load i32, i32* %arrayidx208, align 4, !tbaa !7
  %302 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv209 = sext i8 %302 to i32
  %call210 = call i32 @half_btf(i32 %295, i32 %297, i32 %sub207, i32 %301, i32 %conv209)
  %303 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx211 = getelementptr inbounds i32, i32* %303, i32 5
  store i32 %call210, i32* %arrayidx211, align 4, !tbaa !7
  %304 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx212 = getelementptr inbounds i32, i32* %304, i32 48
  %305 = load i32, i32* %arrayidx212, align 4, !tbaa !7
  %sub213 = sub nsw i32 0, %305
  %306 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx214 = getelementptr inbounds i32, i32* %306, i32 6
  %307 = load i32, i32* %arrayidx214, align 4, !tbaa !7
  %308 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx215 = getelementptr inbounds i32, i32* %308, i32 16
  %309 = load i32, i32* %arrayidx215, align 4, !tbaa !7
  %310 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx216 = getelementptr inbounds i32, i32* %310, i32 7
  %311 = load i32, i32* %arrayidx216, align 4, !tbaa !7
  %312 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv217 = sext i8 %312 to i32
  %call218 = call i32 @half_btf(i32 %sub213, i32 %307, i32 %309, i32 %311, i32 %conv217)
  %313 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx219 = getelementptr inbounds i32, i32* %313, i32 6
  store i32 %call218, i32* %arrayidx219, align 4, !tbaa !7
  %314 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx220 = getelementptr inbounds i32, i32* %314, i32 16
  %315 = load i32, i32* %arrayidx220, align 4, !tbaa !7
  %316 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx221 = getelementptr inbounds i32, i32* %316, i32 6
  %317 = load i32, i32* %arrayidx221, align 4, !tbaa !7
  %318 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx222 = getelementptr inbounds i32, i32* %318, i32 48
  %319 = load i32, i32* %arrayidx222, align 4, !tbaa !7
  %320 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx223 = getelementptr inbounds i32, i32* %320, i32 7
  %321 = load i32, i32* %arrayidx223, align 4, !tbaa !7
  %322 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv224 = sext i8 %322 to i32
  %call225 = call i32 @half_btf(i32 %315, i32 %317, i32 %319, i32 %321, i32 %conv224)
  %323 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx226 = getelementptr inbounds i32, i32* %323, i32 7
  store i32 %call225, i32* %arrayidx226, align 4, !tbaa !7
  %324 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx227 = getelementptr inbounds i32, i32* %324, i32 8
  %325 = load i32, i32* %arrayidx227, align 4, !tbaa !7
  %326 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx228 = getelementptr inbounds i32, i32* %326, i32 8
  store i32 %325, i32* %arrayidx228, align 4, !tbaa !7
  %327 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx229 = getelementptr inbounds i32, i32* %327, i32 9
  %328 = load i32, i32* %arrayidx229, align 4, !tbaa !7
  %329 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx230 = getelementptr inbounds i32, i32* %329, i32 9
  store i32 %328, i32* %arrayidx230, align 4, !tbaa !7
  %330 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx231 = getelementptr inbounds i32, i32* %330, i32 10
  %331 = load i32, i32* %arrayidx231, align 4, !tbaa !7
  %332 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx232 = getelementptr inbounds i32, i32* %332, i32 10
  store i32 %331, i32* %arrayidx232, align 4, !tbaa !7
  %333 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx233 = getelementptr inbounds i32, i32* %333, i32 11
  %334 = load i32, i32* %arrayidx233, align 4, !tbaa !7
  %335 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds i32, i32* %335, i32 11
  store i32 %334, i32* %arrayidx234, align 4, !tbaa !7
  %336 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx235 = getelementptr inbounds i32, i32* %336, i32 16
  %337 = load i32, i32* %arrayidx235, align 4, !tbaa !7
  %338 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds i32, i32* %338, i32 12
  %339 = load i32, i32* %arrayidx236, align 4, !tbaa !7
  %340 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx237 = getelementptr inbounds i32, i32* %340, i32 48
  %341 = load i32, i32* %arrayidx237, align 4, !tbaa !7
  %342 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx238 = getelementptr inbounds i32, i32* %342, i32 13
  %343 = load i32, i32* %arrayidx238, align 4, !tbaa !7
  %344 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv239 = sext i8 %344 to i32
  %call240 = call i32 @half_btf(i32 %337, i32 %339, i32 %341, i32 %343, i32 %conv239)
  %345 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx241 = getelementptr inbounds i32, i32* %345, i32 12
  store i32 %call240, i32* %arrayidx241, align 4, !tbaa !7
  %346 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx242 = getelementptr inbounds i32, i32* %346, i32 48
  %347 = load i32, i32* %arrayidx242, align 4, !tbaa !7
  %348 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx243 = getelementptr inbounds i32, i32* %348, i32 12
  %349 = load i32, i32* %arrayidx243, align 4, !tbaa !7
  %350 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx244 = getelementptr inbounds i32, i32* %350, i32 16
  %351 = load i32, i32* %arrayidx244, align 4, !tbaa !7
  %sub245 = sub nsw i32 0, %351
  %352 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx246 = getelementptr inbounds i32, i32* %352, i32 13
  %353 = load i32, i32* %arrayidx246, align 4, !tbaa !7
  %354 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv247 = sext i8 %354 to i32
  %call248 = call i32 @half_btf(i32 %347, i32 %349, i32 %sub245, i32 %353, i32 %conv247)
  %355 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx249 = getelementptr inbounds i32, i32* %355, i32 13
  store i32 %call248, i32* %arrayidx249, align 4, !tbaa !7
  %356 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx250 = getelementptr inbounds i32, i32* %356, i32 48
  %357 = load i32, i32* %arrayidx250, align 4, !tbaa !7
  %sub251 = sub nsw i32 0, %357
  %358 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx252 = getelementptr inbounds i32, i32* %358, i32 14
  %359 = load i32, i32* %arrayidx252, align 4, !tbaa !7
  %360 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx253 = getelementptr inbounds i32, i32* %360, i32 16
  %361 = load i32, i32* %arrayidx253, align 4, !tbaa !7
  %362 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx254 = getelementptr inbounds i32, i32* %362, i32 15
  %363 = load i32, i32* %arrayidx254, align 4, !tbaa !7
  %364 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv255 = sext i8 %364 to i32
  %call256 = call i32 @half_btf(i32 %sub251, i32 %359, i32 %361, i32 %363, i32 %conv255)
  %365 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx257 = getelementptr inbounds i32, i32* %365, i32 14
  store i32 %call256, i32* %arrayidx257, align 4, !tbaa !7
  %366 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx258 = getelementptr inbounds i32, i32* %366, i32 16
  %367 = load i32, i32* %arrayidx258, align 4, !tbaa !7
  %368 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx259 = getelementptr inbounds i32, i32* %368, i32 14
  %369 = load i32, i32* %arrayidx259, align 4, !tbaa !7
  %370 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx260 = getelementptr inbounds i32, i32* %370, i32 48
  %371 = load i32, i32* %arrayidx260, align 4, !tbaa !7
  %372 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx261 = getelementptr inbounds i32, i32* %372, i32 15
  %373 = load i32, i32* %arrayidx261, align 4, !tbaa !7
  %374 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv262 = sext i8 %374 to i32
  %call263 = call i32 @half_btf(i32 %367, i32 %369, i32 %371, i32 %373, i32 %conv262)
  %375 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx264 = getelementptr inbounds i32, i32* %375, i32 15
  store i32 %call263, i32* %arrayidx264, align 4, !tbaa !7
  %376 = load i32, i32* %stage, align 4, !tbaa !7
  %377 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %378 = load i32*, i32** %bf1, align 4, !tbaa !2
  %379 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %380 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx265 = getelementptr inbounds i8, i8* %379, i32 %380
  %381 = load i8, i8* %arrayidx265, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %376, i32* %377, i32* %378, i32 16, i8 signext %381)
  %382 = load i32, i32* %stage, align 4, !tbaa !7
  %inc266 = add nsw i32 %382, 1
  store i32 %inc266, i32* %stage, align 4, !tbaa !7
  %arraydecay267 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay267, i32** %bf0, align 4, !tbaa !2
  %383 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %383, i32** %bf1, align 4, !tbaa !2
  %384 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx268 = getelementptr inbounds i32, i32* %384, i32 0
  %385 = load i32, i32* %arrayidx268, align 4, !tbaa !7
  %386 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx269 = getelementptr inbounds i32, i32* %386, i32 4
  %387 = load i32, i32* %arrayidx269, align 4, !tbaa !7
  %add270 = add nsw i32 %385, %387
  %388 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx271 = getelementptr inbounds i32, i32* %388, i32 0
  store i32 %add270, i32* %arrayidx271, align 4, !tbaa !7
  %389 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx272 = getelementptr inbounds i32, i32* %389, i32 1
  %390 = load i32, i32* %arrayidx272, align 4, !tbaa !7
  %391 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx273 = getelementptr inbounds i32, i32* %391, i32 5
  %392 = load i32, i32* %arrayidx273, align 4, !tbaa !7
  %add274 = add nsw i32 %390, %392
  %393 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx275 = getelementptr inbounds i32, i32* %393, i32 1
  store i32 %add274, i32* %arrayidx275, align 4, !tbaa !7
  %394 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx276 = getelementptr inbounds i32, i32* %394, i32 2
  %395 = load i32, i32* %arrayidx276, align 4, !tbaa !7
  %396 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx277 = getelementptr inbounds i32, i32* %396, i32 6
  %397 = load i32, i32* %arrayidx277, align 4, !tbaa !7
  %add278 = add nsw i32 %395, %397
  %398 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx279 = getelementptr inbounds i32, i32* %398, i32 2
  store i32 %add278, i32* %arrayidx279, align 4, !tbaa !7
  %399 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx280 = getelementptr inbounds i32, i32* %399, i32 3
  %400 = load i32, i32* %arrayidx280, align 4, !tbaa !7
  %401 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx281 = getelementptr inbounds i32, i32* %401, i32 7
  %402 = load i32, i32* %arrayidx281, align 4, !tbaa !7
  %add282 = add nsw i32 %400, %402
  %403 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx283 = getelementptr inbounds i32, i32* %403, i32 3
  store i32 %add282, i32* %arrayidx283, align 4, !tbaa !7
  %404 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx284 = getelementptr inbounds i32, i32* %404, i32 0
  %405 = load i32, i32* %arrayidx284, align 4, !tbaa !7
  %406 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx285 = getelementptr inbounds i32, i32* %406, i32 4
  %407 = load i32, i32* %arrayidx285, align 4, !tbaa !7
  %sub286 = sub nsw i32 %405, %407
  %408 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx287 = getelementptr inbounds i32, i32* %408, i32 4
  store i32 %sub286, i32* %arrayidx287, align 4, !tbaa !7
  %409 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx288 = getelementptr inbounds i32, i32* %409, i32 1
  %410 = load i32, i32* %arrayidx288, align 4, !tbaa !7
  %411 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx289 = getelementptr inbounds i32, i32* %411, i32 5
  %412 = load i32, i32* %arrayidx289, align 4, !tbaa !7
  %sub290 = sub nsw i32 %410, %412
  %413 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx291 = getelementptr inbounds i32, i32* %413, i32 5
  store i32 %sub290, i32* %arrayidx291, align 4, !tbaa !7
  %414 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx292 = getelementptr inbounds i32, i32* %414, i32 2
  %415 = load i32, i32* %arrayidx292, align 4, !tbaa !7
  %416 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx293 = getelementptr inbounds i32, i32* %416, i32 6
  %417 = load i32, i32* %arrayidx293, align 4, !tbaa !7
  %sub294 = sub nsw i32 %415, %417
  %418 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx295 = getelementptr inbounds i32, i32* %418, i32 6
  store i32 %sub294, i32* %arrayidx295, align 4, !tbaa !7
  %419 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx296 = getelementptr inbounds i32, i32* %419, i32 3
  %420 = load i32, i32* %arrayidx296, align 4, !tbaa !7
  %421 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx297 = getelementptr inbounds i32, i32* %421, i32 7
  %422 = load i32, i32* %arrayidx297, align 4, !tbaa !7
  %sub298 = sub nsw i32 %420, %422
  %423 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx299 = getelementptr inbounds i32, i32* %423, i32 7
  store i32 %sub298, i32* %arrayidx299, align 4, !tbaa !7
  %424 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx300 = getelementptr inbounds i32, i32* %424, i32 8
  %425 = load i32, i32* %arrayidx300, align 4, !tbaa !7
  %426 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx301 = getelementptr inbounds i32, i32* %426, i32 12
  %427 = load i32, i32* %arrayidx301, align 4, !tbaa !7
  %add302 = add nsw i32 %425, %427
  %428 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx303 = getelementptr inbounds i32, i32* %428, i32 8
  store i32 %add302, i32* %arrayidx303, align 4, !tbaa !7
  %429 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx304 = getelementptr inbounds i32, i32* %429, i32 9
  %430 = load i32, i32* %arrayidx304, align 4, !tbaa !7
  %431 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx305 = getelementptr inbounds i32, i32* %431, i32 13
  %432 = load i32, i32* %arrayidx305, align 4, !tbaa !7
  %add306 = add nsw i32 %430, %432
  %433 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx307 = getelementptr inbounds i32, i32* %433, i32 9
  store i32 %add306, i32* %arrayidx307, align 4, !tbaa !7
  %434 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx308 = getelementptr inbounds i32, i32* %434, i32 10
  %435 = load i32, i32* %arrayidx308, align 4, !tbaa !7
  %436 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx309 = getelementptr inbounds i32, i32* %436, i32 14
  %437 = load i32, i32* %arrayidx309, align 4, !tbaa !7
  %add310 = add nsw i32 %435, %437
  %438 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx311 = getelementptr inbounds i32, i32* %438, i32 10
  store i32 %add310, i32* %arrayidx311, align 4, !tbaa !7
  %439 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx312 = getelementptr inbounds i32, i32* %439, i32 11
  %440 = load i32, i32* %arrayidx312, align 4, !tbaa !7
  %441 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx313 = getelementptr inbounds i32, i32* %441, i32 15
  %442 = load i32, i32* %arrayidx313, align 4, !tbaa !7
  %add314 = add nsw i32 %440, %442
  %443 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx315 = getelementptr inbounds i32, i32* %443, i32 11
  store i32 %add314, i32* %arrayidx315, align 4, !tbaa !7
  %444 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx316 = getelementptr inbounds i32, i32* %444, i32 8
  %445 = load i32, i32* %arrayidx316, align 4, !tbaa !7
  %446 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx317 = getelementptr inbounds i32, i32* %446, i32 12
  %447 = load i32, i32* %arrayidx317, align 4, !tbaa !7
  %sub318 = sub nsw i32 %445, %447
  %448 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx319 = getelementptr inbounds i32, i32* %448, i32 12
  store i32 %sub318, i32* %arrayidx319, align 4, !tbaa !7
  %449 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx320 = getelementptr inbounds i32, i32* %449, i32 9
  %450 = load i32, i32* %arrayidx320, align 4, !tbaa !7
  %451 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx321 = getelementptr inbounds i32, i32* %451, i32 13
  %452 = load i32, i32* %arrayidx321, align 4, !tbaa !7
  %sub322 = sub nsw i32 %450, %452
  %453 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx323 = getelementptr inbounds i32, i32* %453, i32 13
  store i32 %sub322, i32* %arrayidx323, align 4, !tbaa !7
  %454 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx324 = getelementptr inbounds i32, i32* %454, i32 10
  %455 = load i32, i32* %arrayidx324, align 4, !tbaa !7
  %456 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx325 = getelementptr inbounds i32, i32* %456, i32 14
  %457 = load i32, i32* %arrayidx325, align 4, !tbaa !7
  %sub326 = sub nsw i32 %455, %457
  %458 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx327 = getelementptr inbounds i32, i32* %458, i32 14
  store i32 %sub326, i32* %arrayidx327, align 4, !tbaa !7
  %459 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx328 = getelementptr inbounds i32, i32* %459, i32 11
  %460 = load i32, i32* %arrayidx328, align 4, !tbaa !7
  %461 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx329 = getelementptr inbounds i32, i32* %461, i32 15
  %462 = load i32, i32* %arrayidx329, align 4, !tbaa !7
  %sub330 = sub nsw i32 %460, %462
  %463 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx331 = getelementptr inbounds i32, i32* %463, i32 15
  store i32 %sub330, i32* %arrayidx331, align 4, !tbaa !7
  %464 = load i32, i32* %stage, align 4, !tbaa !7
  %465 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %466 = load i32*, i32** %bf1, align 4, !tbaa !2
  %467 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %468 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx332 = getelementptr inbounds i8, i8* %467, i32 %468
  %469 = load i8, i8* %arrayidx332, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %464, i32* %465, i32* %466, i32 16, i8 signext %469)
  %470 = load i32, i32* %stage, align 4, !tbaa !7
  %inc333 = add nsw i32 %470, 1
  store i32 %inc333, i32* %stage, align 4, !tbaa !7
  %471 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv334 = sext i8 %471 to i32
  %call335 = call i32* @cospi_arr(i32 %conv334)
  store i32* %call335, i32** %cospi, align 4, !tbaa !2
  %472 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %472, i32** %bf0, align 4, !tbaa !2
  %arraydecay336 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay336, i32** %bf1, align 4, !tbaa !2
  %473 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx337 = getelementptr inbounds i32, i32* %473, i32 0
  %474 = load i32, i32* %arrayidx337, align 4, !tbaa !7
  %475 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx338 = getelementptr inbounds i32, i32* %475, i32 0
  store i32 %474, i32* %arrayidx338, align 4, !tbaa !7
  %476 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx339 = getelementptr inbounds i32, i32* %476, i32 1
  %477 = load i32, i32* %arrayidx339, align 4, !tbaa !7
  %478 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx340 = getelementptr inbounds i32, i32* %478, i32 1
  store i32 %477, i32* %arrayidx340, align 4, !tbaa !7
  %479 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx341 = getelementptr inbounds i32, i32* %479, i32 2
  %480 = load i32, i32* %arrayidx341, align 4, !tbaa !7
  %481 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx342 = getelementptr inbounds i32, i32* %481, i32 2
  store i32 %480, i32* %arrayidx342, align 4, !tbaa !7
  %482 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx343 = getelementptr inbounds i32, i32* %482, i32 3
  %483 = load i32, i32* %arrayidx343, align 4, !tbaa !7
  %484 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx344 = getelementptr inbounds i32, i32* %484, i32 3
  store i32 %483, i32* %arrayidx344, align 4, !tbaa !7
  %485 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx345 = getelementptr inbounds i32, i32* %485, i32 4
  %486 = load i32, i32* %arrayidx345, align 4, !tbaa !7
  %487 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx346 = getelementptr inbounds i32, i32* %487, i32 4
  store i32 %486, i32* %arrayidx346, align 4, !tbaa !7
  %488 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx347 = getelementptr inbounds i32, i32* %488, i32 5
  %489 = load i32, i32* %arrayidx347, align 4, !tbaa !7
  %490 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx348 = getelementptr inbounds i32, i32* %490, i32 5
  store i32 %489, i32* %arrayidx348, align 4, !tbaa !7
  %491 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx349 = getelementptr inbounds i32, i32* %491, i32 6
  %492 = load i32, i32* %arrayidx349, align 4, !tbaa !7
  %493 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx350 = getelementptr inbounds i32, i32* %493, i32 6
  store i32 %492, i32* %arrayidx350, align 4, !tbaa !7
  %494 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx351 = getelementptr inbounds i32, i32* %494, i32 7
  %495 = load i32, i32* %arrayidx351, align 4, !tbaa !7
  %496 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx352 = getelementptr inbounds i32, i32* %496, i32 7
  store i32 %495, i32* %arrayidx352, align 4, !tbaa !7
  %497 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx353 = getelementptr inbounds i32, i32* %497, i32 8
  %498 = load i32, i32* %arrayidx353, align 4, !tbaa !7
  %499 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx354 = getelementptr inbounds i32, i32* %499, i32 8
  %500 = load i32, i32* %arrayidx354, align 4, !tbaa !7
  %501 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx355 = getelementptr inbounds i32, i32* %501, i32 56
  %502 = load i32, i32* %arrayidx355, align 4, !tbaa !7
  %503 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx356 = getelementptr inbounds i32, i32* %503, i32 9
  %504 = load i32, i32* %arrayidx356, align 4, !tbaa !7
  %505 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv357 = sext i8 %505 to i32
  %call358 = call i32 @half_btf(i32 %498, i32 %500, i32 %502, i32 %504, i32 %conv357)
  %506 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx359 = getelementptr inbounds i32, i32* %506, i32 8
  store i32 %call358, i32* %arrayidx359, align 4, !tbaa !7
  %507 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx360 = getelementptr inbounds i32, i32* %507, i32 56
  %508 = load i32, i32* %arrayidx360, align 4, !tbaa !7
  %509 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx361 = getelementptr inbounds i32, i32* %509, i32 8
  %510 = load i32, i32* %arrayidx361, align 4, !tbaa !7
  %511 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx362 = getelementptr inbounds i32, i32* %511, i32 8
  %512 = load i32, i32* %arrayidx362, align 4, !tbaa !7
  %sub363 = sub nsw i32 0, %512
  %513 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx364 = getelementptr inbounds i32, i32* %513, i32 9
  %514 = load i32, i32* %arrayidx364, align 4, !tbaa !7
  %515 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv365 = sext i8 %515 to i32
  %call366 = call i32 @half_btf(i32 %508, i32 %510, i32 %sub363, i32 %514, i32 %conv365)
  %516 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx367 = getelementptr inbounds i32, i32* %516, i32 9
  store i32 %call366, i32* %arrayidx367, align 4, !tbaa !7
  %517 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx368 = getelementptr inbounds i32, i32* %517, i32 40
  %518 = load i32, i32* %arrayidx368, align 4, !tbaa !7
  %519 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx369 = getelementptr inbounds i32, i32* %519, i32 10
  %520 = load i32, i32* %arrayidx369, align 4, !tbaa !7
  %521 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx370 = getelementptr inbounds i32, i32* %521, i32 24
  %522 = load i32, i32* %arrayidx370, align 4, !tbaa !7
  %523 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx371 = getelementptr inbounds i32, i32* %523, i32 11
  %524 = load i32, i32* %arrayidx371, align 4, !tbaa !7
  %525 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv372 = sext i8 %525 to i32
  %call373 = call i32 @half_btf(i32 %518, i32 %520, i32 %522, i32 %524, i32 %conv372)
  %526 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx374 = getelementptr inbounds i32, i32* %526, i32 10
  store i32 %call373, i32* %arrayidx374, align 4, !tbaa !7
  %527 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx375 = getelementptr inbounds i32, i32* %527, i32 24
  %528 = load i32, i32* %arrayidx375, align 4, !tbaa !7
  %529 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx376 = getelementptr inbounds i32, i32* %529, i32 10
  %530 = load i32, i32* %arrayidx376, align 4, !tbaa !7
  %531 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx377 = getelementptr inbounds i32, i32* %531, i32 40
  %532 = load i32, i32* %arrayidx377, align 4, !tbaa !7
  %sub378 = sub nsw i32 0, %532
  %533 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx379 = getelementptr inbounds i32, i32* %533, i32 11
  %534 = load i32, i32* %arrayidx379, align 4, !tbaa !7
  %535 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv380 = sext i8 %535 to i32
  %call381 = call i32 @half_btf(i32 %528, i32 %530, i32 %sub378, i32 %534, i32 %conv380)
  %536 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx382 = getelementptr inbounds i32, i32* %536, i32 11
  store i32 %call381, i32* %arrayidx382, align 4, !tbaa !7
  %537 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx383 = getelementptr inbounds i32, i32* %537, i32 56
  %538 = load i32, i32* %arrayidx383, align 4, !tbaa !7
  %sub384 = sub nsw i32 0, %538
  %539 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx385 = getelementptr inbounds i32, i32* %539, i32 12
  %540 = load i32, i32* %arrayidx385, align 4, !tbaa !7
  %541 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx386 = getelementptr inbounds i32, i32* %541, i32 8
  %542 = load i32, i32* %arrayidx386, align 4, !tbaa !7
  %543 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx387 = getelementptr inbounds i32, i32* %543, i32 13
  %544 = load i32, i32* %arrayidx387, align 4, !tbaa !7
  %545 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv388 = sext i8 %545 to i32
  %call389 = call i32 @half_btf(i32 %sub384, i32 %540, i32 %542, i32 %544, i32 %conv388)
  %546 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx390 = getelementptr inbounds i32, i32* %546, i32 12
  store i32 %call389, i32* %arrayidx390, align 4, !tbaa !7
  %547 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx391 = getelementptr inbounds i32, i32* %547, i32 8
  %548 = load i32, i32* %arrayidx391, align 4, !tbaa !7
  %549 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx392 = getelementptr inbounds i32, i32* %549, i32 12
  %550 = load i32, i32* %arrayidx392, align 4, !tbaa !7
  %551 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx393 = getelementptr inbounds i32, i32* %551, i32 56
  %552 = load i32, i32* %arrayidx393, align 4, !tbaa !7
  %553 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx394 = getelementptr inbounds i32, i32* %553, i32 13
  %554 = load i32, i32* %arrayidx394, align 4, !tbaa !7
  %555 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv395 = sext i8 %555 to i32
  %call396 = call i32 @half_btf(i32 %548, i32 %550, i32 %552, i32 %554, i32 %conv395)
  %556 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx397 = getelementptr inbounds i32, i32* %556, i32 13
  store i32 %call396, i32* %arrayidx397, align 4, !tbaa !7
  %557 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx398 = getelementptr inbounds i32, i32* %557, i32 24
  %558 = load i32, i32* %arrayidx398, align 4, !tbaa !7
  %sub399 = sub nsw i32 0, %558
  %559 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx400 = getelementptr inbounds i32, i32* %559, i32 14
  %560 = load i32, i32* %arrayidx400, align 4, !tbaa !7
  %561 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx401 = getelementptr inbounds i32, i32* %561, i32 40
  %562 = load i32, i32* %arrayidx401, align 4, !tbaa !7
  %563 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx402 = getelementptr inbounds i32, i32* %563, i32 15
  %564 = load i32, i32* %arrayidx402, align 4, !tbaa !7
  %565 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv403 = sext i8 %565 to i32
  %call404 = call i32 @half_btf(i32 %sub399, i32 %560, i32 %562, i32 %564, i32 %conv403)
  %566 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx405 = getelementptr inbounds i32, i32* %566, i32 14
  store i32 %call404, i32* %arrayidx405, align 4, !tbaa !7
  %567 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx406 = getelementptr inbounds i32, i32* %567, i32 40
  %568 = load i32, i32* %arrayidx406, align 4, !tbaa !7
  %569 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx407 = getelementptr inbounds i32, i32* %569, i32 14
  %570 = load i32, i32* %arrayidx407, align 4, !tbaa !7
  %571 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx408 = getelementptr inbounds i32, i32* %571, i32 24
  %572 = load i32, i32* %arrayidx408, align 4, !tbaa !7
  %573 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx409 = getelementptr inbounds i32, i32* %573, i32 15
  %574 = load i32, i32* %arrayidx409, align 4, !tbaa !7
  %575 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv410 = sext i8 %575 to i32
  %call411 = call i32 @half_btf(i32 %568, i32 %570, i32 %572, i32 %574, i32 %conv410)
  %576 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx412 = getelementptr inbounds i32, i32* %576, i32 15
  store i32 %call411, i32* %arrayidx412, align 4, !tbaa !7
  %577 = load i32, i32* %stage, align 4, !tbaa !7
  %578 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %579 = load i32*, i32** %bf1, align 4, !tbaa !2
  %580 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %581 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx413 = getelementptr inbounds i8, i8* %580, i32 %581
  %582 = load i8, i8* %arrayidx413, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %577, i32* %578, i32* %579, i32 16, i8 signext %582)
  %583 = load i32, i32* %stage, align 4, !tbaa !7
  %inc414 = add nsw i32 %583, 1
  store i32 %inc414, i32* %stage, align 4, !tbaa !7
  %arraydecay415 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay415, i32** %bf0, align 4, !tbaa !2
  %584 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %584, i32** %bf1, align 4, !tbaa !2
  %585 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx416 = getelementptr inbounds i32, i32* %585, i32 0
  %586 = load i32, i32* %arrayidx416, align 4, !tbaa !7
  %587 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx417 = getelementptr inbounds i32, i32* %587, i32 8
  %588 = load i32, i32* %arrayidx417, align 4, !tbaa !7
  %add418 = add nsw i32 %586, %588
  %589 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx419 = getelementptr inbounds i32, i32* %589, i32 0
  store i32 %add418, i32* %arrayidx419, align 4, !tbaa !7
  %590 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx420 = getelementptr inbounds i32, i32* %590, i32 1
  %591 = load i32, i32* %arrayidx420, align 4, !tbaa !7
  %592 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx421 = getelementptr inbounds i32, i32* %592, i32 9
  %593 = load i32, i32* %arrayidx421, align 4, !tbaa !7
  %add422 = add nsw i32 %591, %593
  %594 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx423 = getelementptr inbounds i32, i32* %594, i32 1
  store i32 %add422, i32* %arrayidx423, align 4, !tbaa !7
  %595 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx424 = getelementptr inbounds i32, i32* %595, i32 2
  %596 = load i32, i32* %arrayidx424, align 4, !tbaa !7
  %597 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx425 = getelementptr inbounds i32, i32* %597, i32 10
  %598 = load i32, i32* %arrayidx425, align 4, !tbaa !7
  %add426 = add nsw i32 %596, %598
  %599 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx427 = getelementptr inbounds i32, i32* %599, i32 2
  store i32 %add426, i32* %arrayidx427, align 4, !tbaa !7
  %600 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx428 = getelementptr inbounds i32, i32* %600, i32 3
  %601 = load i32, i32* %arrayidx428, align 4, !tbaa !7
  %602 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx429 = getelementptr inbounds i32, i32* %602, i32 11
  %603 = load i32, i32* %arrayidx429, align 4, !tbaa !7
  %add430 = add nsw i32 %601, %603
  %604 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx431 = getelementptr inbounds i32, i32* %604, i32 3
  store i32 %add430, i32* %arrayidx431, align 4, !tbaa !7
  %605 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx432 = getelementptr inbounds i32, i32* %605, i32 4
  %606 = load i32, i32* %arrayidx432, align 4, !tbaa !7
  %607 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx433 = getelementptr inbounds i32, i32* %607, i32 12
  %608 = load i32, i32* %arrayidx433, align 4, !tbaa !7
  %add434 = add nsw i32 %606, %608
  %609 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx435 = getelementptr inbounds i32, i32* %609, i32 4
  store i32 %add434, i32* %arrayidx435, align 4, !tbaa !7
  %610 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx436 = getelementptr inbounds i32, i32* %610, i32 5
  %611 = load i32, i32* %arrayidx436, align 4, !tbaa !7
  %612 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx437 = getelementptr inbounds i32, i32* %612, i32 13
  %613 = load i32, i32* %arrayidx437, align 4, !tbaa !7
  %add438 = add nsw i32 %611, %613
  %614 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx439 = getelementptr inbounds i32, i32* %614, i32 5
  store i32 %add438, i32* %arrayidx439, align 4, !tbaa !7
  %615 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx440 = getelementptr inbounds i32, i32* %615, i32 6
  %616 = load i32, i32* %arrayidx440, align 4, !tbaa !7
  %617 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx441 = getelementptr inbounds i32, i32* %617, i32 14
  %618 = load i32, i32* %arrayidx441, align 4, !tbaa !7
  %add442 = add nsw i32 %616, %618
  %619 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx443 = getelementptr inbounds i32, i32* %619, i32 6
  store i32 %add442, i32* %arrayidx443, align 4, !tbaa !7
  %620 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx444 = getelementptr inbounds i32, i32* %620, i32 7
  %621 = load i32, i32* %arrayidx444, align 4, !tbaa !7
  %622 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx445 = getelementptr inbounds i32, i32* %622, i32 15
  %623 = load i32, i32* %arrayidx445, align 4, !tbaa !7
  %add446 = add nsw i32 %621, %623
  %624 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx447 = getelementptr inbounds i32, i32* %624, i32 7
  store i32 %add446, i32* %arrayidx447, align 4, !tbaa !7
  %625 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx448 = getelementptr inbounds i32, i32* %625, i32 0
  %626 = load i32, i32* %arrayidx448, align 4, !tbaa !7
  %627 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx449 = getelementptr inbounds i32, i32* %627, i32 8
  %628 = load i32, i32* %arrayidx449, align 4, !tbaa !7
  %sub450 = sub nsw i32 %626, %628
  %629 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx451 = getelementptr inbounds i32, i32* %629, i32 8
  store i32 %sub450, i32* %arrayidx451, align 4, !tbaa !7
  %630 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx452 = getelementptr inbounds i32, i32* %630, i32 1
  %631 = load i32, i32* %arrayidx452, align 4, !tbaa !7
  %632 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx453 = getelementptr inbounds i32, i32* %632, i32 9
  %633 = load i32, i32* %arrayidx453, align 4, !tbaa !7
  %sub454 = sub nsw i32 %631, %633
  %634 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx455 = getelementptr inbounds i32, i32* %634, i32 9
  store i32 %sub454, i32* %arrayidx455, align 4, !tbaa !7
  %635 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx456 = getelementptr inbounds i32, i32* %635, i32 2
  %636 = load i32, i32* %arrayidx456, align 4, !tbaa !7
  %637 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx457 = getelementptr inbounds i32, i32* %637, i32 10
  %638 = load i32, i32* %arrayidx457, align 4, !tbaa !7
  %sub458 = sub nsw i32 %636, %638
  %639 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx459 = getelementptr inbounds i32, i32* %639, i32 10
  store i32 %sub458, i32* %arrayidx459, align 4, !tbaa !7
  %640 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx460 = getelementptr inbounds i32, i32* %640, i32 3
  %641 = load i32, i32* %arrayidx460, align 4, !tbaa !7
  %642 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx461 = getelementptr inbounds i32, i32* %642, i32 11
  %643 = load i32, i32* %arrayidx461, align 4, !tbaa !7
  %sub462 = sub nsw i32 %641, %643
  %644 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx463 = getelementptr inbounds i32, i32* %644, i32 11
  store i32 %sub462, i32* %arrayidx463, align 4, !tbaa !7
  %645 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx464 = getelementptr inbounds i32, i32* %645, i32 4
  %646 = load i32, i32* %arrayidx464, align 4, !tbaa !7
  %647 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx465 = getelementptr inbounds i32, i32* %647, i32 12
  %648 = load i32, i32* %arrayidx465, align 4, !tbaa !7
  %sub466 = sub nsw i32 %646, %648
  %649 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx467 = getelementptr inbounds i32, i32* %649, i32 12
  store i32 %sub466, i32* %arrayidx467, align 4, !tbaa !7
  %650 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx468 = getelementptr inbounds i32, i32* %650, i32 5
  %651 = load i32, i32* %arrayidx468, align 4, !tbaa !7
  %652 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx469 = getelementptr inbounds i32, i32* %652, i32 13
  %653 = load i32, i32* %arrayidx469, align 4, !tbaa !7
  %sub470 = sub nsw i32 %651, %653
  %654 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx471 = getelementptr inbounds i32, i32* %654, i32 13
  store i32 %sub470, i32* %arrayidx471, align 4, !tbaa !7
  %655 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx472 = getelementptr inbounds i32, i32* %655, i32 6
  %656 = load i32, i32* %arrayidx472, align 4, !tbaa !7
  %657 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx473 = getelementptr inbounds i32, i32* %657, i32 14
  %658 = load i32, i32* %arrayidx473, align 4, !tbaa !7
  %sub474 = sub nsw i32 %656, %658
  %659 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx475 = getelementptr inbounds i32, i32* %659, i32 14
  store i32 %sub474, i32* %arrayidx475, align 4, !tbaa !7
  %660 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx476 = getelementptr inbounds i32, i32* %660, i32 7
  %661 = load i32, i32* %arrayidx476, align 4, !tbaa !7
  %662 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx477 = getelementptr inbounds i32, i32* %662, i32 15
  %663 = load i32, i32* %arrayidx477, align 4, !tbaa !7
  %sub478 = sub nsw i32 %661, %663
  %664 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx479 = getelementptr inbounds i32, i32* %664, i32 15
  store i32 %sub478, i32* %arrayidx479, align 4, !tbaa !7
  %665 = load i32, i32* %stage, align 4, !tbaa !7
  %666 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %667 = load i32*, i32** %bf1, align 4, !tbaa !2
  %668 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %669 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx480 = getelementptr inbounds i8, i8* %668, i32 %669
  %670 = load i8, i8* %arrayidx480, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %665, i32* %666, i32* %667, i32 16, i8 signext %670)
  %671 = load i32, i32* %stage, align 4, !tbaa !7
  %inc481 = add nsw i32 %671, 1
  store i32 %inc481, i32* %stage, align 4, !tbaa !7
  %672 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv482 = sext i8 %672 to i32
  %call483 = call i32* @cospi_arr(i32 %conv482)
  store i32* %call483, i32** %cospi, align 4, !tbaa !2
  %673 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %673, i32** %bf0, align 4, !tbaa !2
  %arraydecay484 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay484, i32** %bf1, align 4, !tbaa !2
  %674 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx485 = getelementptr inbounds i32, i32* %674, i32 2
  %675 = load i32, i32* %arrayidx485, align 4, !tbaa !7
  %676 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx486 = getelementptr inbounds i32, i32* %676, i32 0
  %677 = load i32, i32* %arrayidx486, align 4, !tbaa !7
  %678 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx487 = getelementptr inbounds i32, i32* %678, i32 62
  %679 = load i32, i32* %arrayidx487, align 4, !tbaa !7
  %680 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx488 = getelementptr inbounds i32, i32* %680, i32 1
  %681 = load i32, i32* %arrayidx488, align 4, !tbaa !7
  %682 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv489 = sext i8 %682 to i32
  %call490 = call i32 @half_btf(i32 %675, i32 %677, i32 %679, i32 %681, i32 %conv489)
  %683 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx491 = getelementptr inbounds i32, i32* %683, i32 0
  store i32 %call490, i32* %arrayidx491, align 4, !tbaa !7
  %684 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx492 = getelementptr inbounds i32, i32* %684, i32 62
  %685 = load i32, i32* %arrayidx492, align 4, !tbaa !7
  %686 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx493 = getelementptr inbounds i32, i32* %686, i32 0
  %687 = load i32, i32* %arrayidx493, align 4, !tbaa !7
  %688 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx494 = getelementptr inbounds i32, i32* %688, i32 2
  %689 = load i32, i32* %arrayidx494, align 4, !tbaa !7
  %sub495 = sub nsw i32 0, %689
  %690 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx496 = getelementptr inbounds i32, i32* %690, i32 1
  %691 = load i32, i32* %arrayidx496, align 4, !tbaa !7
  %692 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv497 = sext i8 %692 to i32
  %call498 = call i32 @half_btf(i32 %685, i32 %687, i32 %sub495, i32 %691, i32 %conv497)
  %693 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx499 = getelementptr inbounds i32, i32* %693, i32 1
  store i32 %call498, i32* %arrayidx499, align 4, !tbaa !7
  %694 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx500 = getelementptr inbounds i32, i32* %694, i32 10
  %695 = load i32, i32* %arrayidx500, align 4, !tbaa !7
  %696 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx501 = getelementptr inbounds i32, i32* %696, i32 2
  %697 = load i32, i32* %arrayidx501, align 4, !tbaa !7
  %698 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx502 = getelementptr inbounds i32, i32* %698, i32 54
  %699 = load i32, i32* %arrayidx502, align 4, !tbaa !7
  %700 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx503 = getelementptr inbounds i32, i32* %700, i32 3
  %701 = load i32, i32* %arrayidx503, align 4, !tbaa !7
  %702 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv504 = sext i8 %702 to i32
  %call505 = call i32 @half_btf(i32 %695, i32 %697, i32 %699, i32 %701, i32 %conv504)
  %703 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx506 = getelementptr inbounds i32, i32* %703, i32 2
  store i32 %call505, i32* %arrayidx506, align 4, !tbaa !7
  %704 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx507 = getelementptr inbounds i32, i32* %704, i32 54
  %705 = load i32, i32* %arrayidx507, align 4, !tbaa !7
  %706 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx508 = getelementptr inbounds i32, i32* %706, i32 2
  %707 = load i32, i32* %arrayidx508, align 4, !tbaa !7
  %708 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx509 = getelementptr inbounds i32, i32* %708, i32 10
  %709 = load i32, i32* %arrayidx509, align 4, !tbaa !7
  %sub510 = sub nsw i32 0, %709
  %710 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx511 = getelementptr inbounds i32, i32* %710, i32 3
  %711 = load i32, i32* %arrayidx511, align 4, !tbaa !7
  %712 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv512 = sext i8 %712 to i32
  %call513 = call i32 @half_btf(i32 %705, i32 %707, i32 %sub510, i32 %711, i32 %conv512)
  %713 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx514 = getelementptr inbounds i32, i32* %713, i32 3
  store i32 %call513, i32* %arrayidx514, align 4, !tbaa !7
  %714 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx515 = getelementptr inbounds i32, i32* %714, i32 18
  %715 = load i32, i32* %arrayidx515, align 4, !tbaa !7
  %716 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx516 = getelementptr inbounds i32, i32* %716, i32 4
  %717 = load i32, i32* %arrayidx516, align 4, !tbaa !7
  %718 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx517 = getelementptr inbounds i32, i32* %718, i32 46
  %719 = load i32, i32* %arrayidx517, align 4, !tbaa !7
  %720 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx518 = getelementptr inbounds i32, i32* %720, i32 5
  %721 = load i32, i32* %arrayidx518, align 4, !tbaa !7
  %722 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv519 = sext i8 %722 to i32
  %call520 = call i32 @half_btf(i32 %715, i32 %717, i32 %719, i32 %721, i32 %conv519)
  %723 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx521 = getelementptr inbounds i32, i32* %723, i32 4
  store i32 %call520, i32* %arrayidx521, align 4, !tbaa !7
  %724 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx522 = getelementptr inbounds i32, i32* %724, i32 46
  %725 = load i32, i32* %arrayidx522, align 4, !tbaa !7
  %726 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx523 = getelementptr inbounds i32, i32* %726, i32 4
  %727 = load i32, i32* %arrayidx523, align 4, !tbaa !7
  %728 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx524 = getelementptr inbounds i32, i32* %728, i32 18
  %729 = load i32, i32* %arrayidx524, align 4, !tbaa !7
  %sub525 = sub nsw i32 0, %729
  %730 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx526 = getelementptr inbounds i32, i32* %730, i32 5
  %731 = load i32, i32* %arrayidx526, align 4, !tbaa !7
  %732 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv527 = sext i8 %732 to i32
  %call528 = call i32 @half_btf(i32 %725, i32 %727, i32 %sub525, i32 %731, i32 %conv527)
  %733 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx529 = getelementptr inbounds i32, i32* %733, i32 5
  store i32 %call528, i32* %arrayidx529, align 4, !tbaa !7
  %734 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx530 = getelementptr inbounds i32, i32* %734, i32 26
  %735 = load i32, i32* %arrayidx530, align 4, !tbaa !7
  %736 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx531 = getelementptr inbounds i32, i32* %736, i32 6
  %737 = load i32, i32* %arrayidx531, align 4, !tbaa !7
  %738 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx532 = getelementptr inbounds i32, i32* %738, i32 38
  %739 = load i32, i32* %arrayidx532, align 4, !tbaa !7
  %740 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx533 = getelementptr inbounds i32, i32* %740, i32 7
  %741 = load i32, i32* %arrayidx533, align 4, !tbaa !7
  %742 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv534 = sext i8 %742 to i32
  %call535 = call i32 @half_btf(i32 %735, i32 %737, i32 %739, i32 %741, i32 %conv534)
  %743 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx536 = getelementptr inbounds i32, i32* %743, i32 6
  store i32 %call535, i32* %arrayidx536, align 4, !tbaa !7
  %744 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx537 = getelementptr inbounds i32, i32* %744, i32 38
  %745 = load i32, i32* %arrayidx537, align 4, !tbaa !7
  %746 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx538 = getelementptr inbounds i32, i32* %746, i32 6
  %747 = load i32, i32* %arrayidx538, align 4, !tbaa !7
  %748 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx539 = getelementptr inbounds i32, i32* %748, i32 26
  %749 = load i32, i32* %arrayidx539, align 4, !tbaa !7
  %sub540 = sub nsw i32 0, %749
  %750 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx541 = getelementptr inbounds i32, i32* %750, i32 7
  %751 = load i32, i32* %arrayidx541, align 4, !tbaa !7
  %752 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv542 = sext i8 %752 to i32
  %call543 = call i32 @half_btf(i32 %745, i32 %747, i32 %sub540, i32 %751, i32 %conv542)
  %753 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx544 = getelementptr inbounds i32, i32* %753, i32 7
  store i32 %call543, i32* %arrayidx544, align 4, !tbaa !7
  %754 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx545 = getelementptr inbounds i32, i32* %754, i32 34
  %755 = load i32, i32* %arrayidx545, align 4, !tbaa !7
  %756 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx546 = getelementptr inbounds i32, i32* %756, i32 8
  %757 = load i32, i32* %arrayidx546, align 4, !tbaa !7
  %758 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx547 = getelementptr inbounds i32, i32* %758, i32 30
  %759 = load i32, i32* %arrayidx547, align 4, !tbaa !7
  %760 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx548 = getelementptr inbounds i32, i32* %760, i32 9
  %761 = load i32, i32* %arrayidx548, align 4, !tbaa !7
  %762 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv549 = sext i8 %762 to i32
  %call550 = call i32 @half_btf(i32 %755, i32 %757, i32 %759, i32 %761, i32 %conv549)
  %763 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx551 = getelementptr inbounds i32, i32* %763, i32 8
  store i32 %call550, i32* %arrayidx551, align 4, !tbaa !7
  %764 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx552 = getelementptr inbounds i32, i32* %764, i32 30
  %765 = load i32, i32* %arrayidx552, align 4, !tbaa !7
  %766 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx553 = getelementptr inbounds i32, i32* %766, i32 8
  %767 = load i32, i32* %arrayidx553, align 4, !tbaa !7
  %768 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx554 = getelementptr inbounds i32, i32* %768, i32 34
  %769 = load i32, i32* %arrayidx554, align 4, !tbaa !7
  %sub555 = sub nsw i32 0, %769
  %770 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx556 = getelementptr inbounds i32, i32* %770, i32 9
  %771 = load i32, i32* %arrayidx556, align 4, !tbaa !7
  %772 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv557 = sext i8 %772 to i32
  %call558 = call i32 @half_btf(i32 %765, i32 %767, i32 %sub555, i32 %771, i32 %conv557)
  %773 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx559 = getelementptr inbounds i32, i32* %773, i32 9
  store i32 %call558, i32* %arrayidx559, align 4, !tbaa !7
  %774 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx560 = getelementptr inbounds i32, i32* %774, i32 42
  %775 = load i32, i32* %arrayidx560, align 4, !tbaa !7
  %776 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx561 = getelementptr inbounds i32, i32* %776, i32 10
  %777 = load i32, i32* %arrayidx561, align 4, !tbaa !7
  %778 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx562 = getelementptr inbounds i32, i32* %778, i32 22
  %779 = load i32, i32* %arrayidx562, align 4, !tbaa !7
  %780 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx563 = getelementptr inbounds i32, i32* %780, i32 11
  %781 = load i32, i32* %arrayidx563, align 4, !tbaa !7
  %782 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv564 = sext i8 %782 to i32
  %call565 = call i32 @half_btf(i32 %775, i32 %777, i32 %779, i32 %781, i32 %conv564)
  %783 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx566 = getelementptr inbounds i32, i32* %783, i32 10
  store i32 %call565, i32* %arrayidx566, align 4, !tbaa !7
  %784 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx567 = getelementptr inbounds i32, i32* %784, i32 22
  %785 = load i32, i32* %arrayidx567, align 4, !tbaa !7
  %786 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx568 = getelementptr inbounds i32, i32* %786, i32 10
  %787 = load i32, i32* %arrayidx568, align 4, !tbaa !7
  %788 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx569 = getelementptr inbounds i32, i32* %788, i32 42
  %789 = load i32, i32* %arrayidx569, align 4, !tbaa !7
  %sub570 = sub nsw i32 0, %789
  %790 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx571 = getelementptr inbounds i32, i32* %790, i32 11
  %791 = load i32, i32* %arrayidx571, align 4, !tbaa !7
  %792 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv572 = sext i8 %792 to i32
  %call573 = call i32 @half_btf(i32 %785, i32 %787, i32 %sub570, i32 %791, i32 %conv572)
  %793 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx574 = getelementptr inbounds i32, i32* %793, i32 11
  store i32 %call573, i32* %arrayidx574, align 4, !tbaa !7
  %794 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx575 = getelementptr inbounds i32, i32* %794, i32 50
  %795 = load i32, i32* %arrayidx575, align 4, !tbaa !7
  %796 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx576 = getelementptr inbounds i32, i32* %796, i32 12
  %797 = load i32, i32* %arrayidx576, align 4, !tbaa !7
  %798 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx577 = getelementptr inbounds i32, i32* %798, i32 14
  %799 = load i32, i32* %arrayidx577, align 4, !tbaa !7
  %800 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx578 = getelementptr inbounds i32, i32* %800, i32 13
  %801 = load i32, i32* %arrayidx578, align 4, !tbaa !7
  %802 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv579 = sext i8 %802 to i32
  %call580 = call i32 @half_btf(i32 %795, i32 %797, i32 %799, i32 %801, i32 %conv579)
  %803 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx581 = getelementptr inbounds i32, i32* %803, i32 12
  store i32 %call580, i32* %arrayidx581, align 4, !tbaa !7
  %804 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx582 = getelementptr inbounds i32, i32* %804, i32 14
  %805 = load i32, i32* %arrayidx582, align 4, !tbaa !7
  %806 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx583 = getelementptr inbounds i32, i32* %806, i32 12
  %807 = load i32, i32* %arrayidx583, align 4, !tbaa !7
  %808 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx584 = getelementptr inbounds i32, i32* %808, i32 50
  %809 = load i32, i32* %arrayidx584, align 4, !tbaa !7
  %sub585 = sub nsw i32 0, %809
  %810 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx586 = getelementptr inbounds i32, i32* %810, i32 13
  %811 = load i32, i32* %arrayidx586, align 4, !tbaa !7
  %812 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv587 = sext i8 %812 to i32
  %call588 = call i32 @half_btf(i32 %805, i32 %807, i32 %sub585, i32 %811, i32 %conv587)
  %813 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx589 = getelementptr inbounds i32, i32* %813, i32 13
  store i32 %call588, i32* %arrayidx589, align 4, !tbaa !7
  %814 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx590 = getelementptr inbounds i32, i32* %814, i32 58
  %815 = load i32, i32* %arrayidx590, align 4, !tbaa !7
  %816 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx591 = getelementptr inbounds i32, i32* %816, i32 14
  %817 = load i32, i32* %arrayidx591, align 4, !tbaa !7
  %818 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx592 = getelementptr inbounds i32, i32* %818, i32 6
  %819 = load i32, i32* %arrayidx592, align 4, !tbaa !7
  %820 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx593 = getelementptr inbounds i32, i32* %820, i32 15
  %821 = load i32, i32* %arrayidx593, align 4, !tbaa !7
  %822 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv594 = sext i8 %822 to i32
  %call595 = call i32 @half_btf(i32 %815, i32 %817, i32 %819, i32 %821, i32 %conv594)
  %823 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx596 = getelementptr inbounds i32, i32* %823, i32 14
  store i32 %call595, i32* %arrayidx596, align 4, !tbaa !7
  %824 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx597 = getelementptr inbounds i32, i32* %824, i32 6
  %825 = load i32, i32* %arrayidx597, align 4, !tbaa !7
  %826 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx598 = getelementptr inbounds i32, i32* %826, i32 14
  %827 = load i32, i32* %arrayidx598, align 4, !tbaa !7
  %828 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx599 = getelementptr inbounds i32, i32* %828, i32 58
  %829 = load i32, i32* %arrayidx599, align 4, !tbaa !7
  %sub600 = sub nsw i32 0, %829
  %830 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx601 = getelementptr inbounds i32, i32* %830, i32 15
  %831 = load i32, i32* %arrayidx601, align 4, !tbaa !7
  %832 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv602 = sext i8 %832 to i32
  %call603 = call i32 @half_btf(i32 %825, i32 %827, i32 %sub600, i32 %831, i32 %conv602)
  %833 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx604 = getelementptr inbounds i32, i32* %833, i32 15
  store i32 %call603, i32* %arrayidx604, align 4, !tbaa !7
  %834 = load i32, i32* %stage, align 4, !tbaa !7
  %835 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %836 = load i32*, i32** %bf1, align 4, !tbaa !2
  %837 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %838 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx605 = getelementptr inbounds i8, i8* %837, i32 %838
  %839 = load i8, i8* %arrayidx605, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %834, i32* %835, i32* %836, i32 16, i8 signext %839)
  %840 = load i32, i32* %stage, align 4, !tbaa !7
  %inc606 = add nsw i32 %840, 1
  store i32 %inc606, i32* %stage, align 4, !tbaa !7
  %arraydecay607 = getelementptr inbounds [16 x i32], [16 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay607, i32** %bf0, align 4, !tbaa !2
  %841 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %841, i32** %bf1, align 4, !tbaa !2
  %842 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx608 = getelementptr inbounds i32, i32* %842, i32 1
  %843 = load i32, i32* %arrayidx608, align 4, !tbaa !7
  %844 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx609 = getelementptr inbounds i32, i32* %844, i32 0
  store i32 %843, i32* %arrayidx609, align 4, !tbaa !7
  %845 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx610 = getelementptr inbounds i32, i32* %845, i32 14
  %846 = load i32, i32* %arrayidx610, align 4, !tbaa !7
  %847 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx611 = getelementptr inbounds i32, i32* %847, i32 1
  store i32 %846, i32* %arrayidx611, align 4, !tbaa !7
  %848 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx612 = getelementptr inbounds i32, i32* %848, i32 3
  %849 = load i32, i32* %arrayidx612, align 4, !tbaa !7
  %850 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx613 = getelementptr inbounds i32, i32* %850, i32 2
  store i32 %849, i32* %arrayidx613, align 4, !tbaa !7
  %851 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx614 = getelementptr inbounds i32, i32* %851, i32 12
  %852 = load i32, i32* %arrayidx614, align 4, !tbaa !7
  %853 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx615 = getelementptr inbounds i32, i32* %853, i32 3
  store i32 %852, i32* %arrayidx615, align 4, !tbaa !7
  %854 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx616 = getelementptr inbounds i32, i32* %854, i32 5
  %855 = load i32, i32* %arrayidx616, align 4, !tbaa !7
  %856 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx617 = getelementptr inbounds i32, i32* %856, i32 4
  store i32 %855, i32* %arrayidx617, align 4, !tbaa !7
  %857 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx618 = getelementptr inbounds i32, i32* %857, i32 10
  %858 = load i32, i32* %arrayidx618, align 4, !tbaa !7
  %859 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx619 = getelementptr inbounds i32, i32* %859, i32 5
  store i32 %858, i32* %arrayidx619, align 4, !tbaa !7
  %860 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx620 = getelementptr inbounds i32, i32* %860, i32 7
  %861 = load i32, i32* %arrayidx620, align 4, !tbaa !7
  %862 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx621 = getelementptr inbounds i32, i32* %862, i32 6
  store i32 %861, i32* %arrayidx621, align 4, !tbaa !7
  %863 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx622 = getelementptr inbounds i32, i32* %863, i32 8
  %864 = load i32, i32* %arrayidx622, align 4, !tbaa !7
  %865 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx623 = getelementptr inbounds i32, i32* %865, i32 7
  store i32 %864, i32* %arrayidx623, align 4, !tbaa !7
  %866 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx624 = getelementptr inbounds i32, i32* %866, i32 9
  %867 = load i32, i32* %arrayidx624, align 4, !tbaa !7
  %868 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx625 = getelementptr inbounds i32, i32* %868, i32 8
  store i32 %867, i32* %arrayidx625, align 4, !tbaa !7
  %869 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx626 = getelementptr inbounds i32, i32* %869, i32 6
  %870 = load i32, i32* %arrayidx626, align 4, !tbaa !7
  %871 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx627 = getelementptr inbounds i32, i32* %871, i32 9
  store i32 %870, i32* %arrayidx627, align 4, !tbaa !7
  %872 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx628 = getelementptr inbounds i32, i32* %872, i32 11
  %873 = load i32, i32* %arrayidx628, align 4, !tbaa !7
  %874 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx629 = getelementptr inbounds i32, i32* %874, i32 10
  store i32 %873, i32* %arrayidx629, align 4, !tbaa !7
  %875 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx630 = getelementptr inbounds i32, i32* %875, i32 4
  %876 = load i32, i32* %arrayidx630, align 4, !tbaa !7
  %877 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx631 = getelementptr inbounds i32, i32* %877, i32 11
  store i32 %876, i32* %arrayidx631, align 4, !tbaa !7
  %878 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx632 = getelementptr inbounds i32, i32* %878, i32 13
  %879 = load i32, i32* %arrayidx632, align 4, !tbaa !7
  %880 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx633 = getelementptr inbounds i32, i32* %880, i32 12
  store i32 %879, i32* %arrayidx633, align 4, !tbaa !7
  %881 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx634 = getelementptr inbounds i32, i32* %881, i32 2
  %882 = load i32, i32* %arrayidx634, align 4, !tbaa !7
  %883 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx635 = getelementptr inbounds i32, i32* %883, i32 13
  store i32 %882, i32* %arrayidx635, align 4, !tbaa !7
  %884 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx636 = getelementptr inbounds i32, i32* %884, i32 15
  %885 = load i32, i32* %arrayidx636, align 4, !tbaa !7
  %886 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx637 = getelementptr inbounds i32, i32* %886, i32 14
  store i32 %885, i32* %arrayidx637, align 4, !tbaa !7
  %887 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx638 = getelementptr inbounds i32, i32* %887, i32 0
  %888 = load i32, i32* %arrayidx638, align 4, !tbaa !7
  %889 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx639 = getelementptr inbounds i32, i32* %889, i32 15
  store i32 %888, i32* %arrayidx639, align 4, !tbaa !7
  %890 = load i32, i32* %stage, align 4, !tbaa !7
  %891 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %892 = load i32*, i32** %bf1, align 4, !tbaa !2
  %893 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %894 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx640 = getelementptr inbounds i8, i8* %893, i32 %894
  %895 = load i8, i8* %arrayidx640, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %890, i32* %891, i32* %892, i32 16, i8 signext %895)
  %896 = bitcast [16 x i32]* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %896) #4
  %897 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %897) #4
  %898 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %898) #4
  %899 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %899) #4
  %900 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %900) #4
  %901 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %901) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fidentity4_c(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp slt i32 %2, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !7
  %conv = sext i32 %6 to i64
  %mul = mul nsw i64 %conv, 5793
  %call = call i32 @round_shift(i64 %mul, i32 12)
  %7 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx1 = getelementptr inbounds i32, i32* %7, i32 %8
  store i32 %call, i32* %arrayidx1, align 4, !tbaa !7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %10 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %11 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %12, i32 0
  %13 = load i8, i8* %arrayidx2, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 0, i32* %10, i32* %11, i32 4, i8 signext %13)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fidentity8_c(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp slt i32 %2, 8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !7
  %mul = mul nsw i32 %6, 2
  %7 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx1 = getelementptr inbounds i32, i32* %7, i32 %8
  store i32 %mul, i32* %arrayidx1, align 4, !tbaa !7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %10 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %11 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %12, i32 0
  %13 = load i8, i8* %arrayidx2, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 0, i32* %10, i32* %11, i32 8, i8 signext %13)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fidentity16_c(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp slt i32 %2, 16
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !7
  %conv = sext i32 %6 to i64
  %mul = mul nsw i64 %conv, 2
  %mul1 = mul nsw i64 %mul, 5793
  %call = call i32 @round_shift(i64 %mul1, i32 12)
  %7 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  store i32 %call, i32* %arrayidx2, align 4, !tbaa !7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %10 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %11 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %12, i32 0
  %13 = load i8, i8* %arrayidx3, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 0, i32* %10, i32* %11, i32 16, i8 signext %13)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fidentity32_c(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !7
  %cmp = icmp slt i32 %2, 32
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4, !tbaa !7
  %mul = mul nsw i32 %6, 4
  %7 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !7
  %arrayidx1 = getelementptr inbounds i32, i32* %7, i32 %8
  store i32 %mul, i32* %arrayidx1, align 4, !tbaa !7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4, !tbaa !7
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !7
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %10 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %11 = load i32*, i32** %output.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %12, i32 0
  %13 = load i8, i8* %arrayidx2, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 0, i32* %10, i32* %11, i32 32, i8 signext %13)
  ret void
}

; Function Attrs: nounwind
define hidden void @av1_fdct64(i32* %input, i32* %output, i8 signext %cos_bit, i8* %stage_range) #0 {
entry:
  %input.addr = alloca i32*, align 4
  %output.addr = alloca i32*, align 4
  %cos_bit.addr = alloca i8, align 1
  %stage_range.addr = alloca i8*, align 4
  %size = alloca i32, align 4
  %cospi = alloca i32*, align 4
  %stage = alloca i32, align 4
  %bf0 = alloca i32*, align 4
  %bf1 = alloca i32*, align 4
  %step = alloca [64 x i32], align 16
  store i32* %input, i32** %input.addr, align 4, !tbaa !2
  store i32* %output, i32** %output.addr, align 4, !tbaa !2
  store i8 %cos_bit, i8* %cos_bit.addr, align 1, !tbaa !6
  store i8* %stage_range, i8** %stage_range.addr, align 4, !tbaa !2
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 64, i32* %size, align 4, !tbaa !7
  %1 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %stage, align 4, !tbaa !7
  %3 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast [64 x i32]* %step to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %5) #4
  %6 = load i32, i32* %stage, align 4, !tbaa !7
  %7 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %8 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %9 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %6, i32* %7, i32* %8, i32 64, i8 signext %11)
  %12 = load i32, i32* %stage, align 4, !tbaa !7
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %stage, align 4, !tbaa !7
  %13 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %13, i32** %bf1, align 4, !tbaa !2
  %14 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %14, i32 0
  %15 = load i32, i32* %arrayidx1, align 4, !tbaa !7
  %16 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %16, i32 63
  %17 = load i32, i32* %arrayidx2, align 4, !tbaa !7
  %add = add nsw i32 %15, %17
  %18 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %18, i32 0
  store i32 %add, i32* %arrayidx3, align 4, !tbaa !7
  %19 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %19, i32 1
  %20 = load i32, i32* %arrayidx4, align 4, !tbaa !7
  %21 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %21, i32 62
  %22 = load i32, i32* %arrayidx5, align 4, !tbaa !7
  %add6 = add nsw i32 %20, %22
  %23 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %23, i32 1
  store i32 %add6, i32* %arrayidx7, align 4, !tbaa !7
  %24 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx8, align 4, !tbaa !7
  %26 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %26, i32 61
  %27 = load i32, i32* %arrayidx9, align 4, !tbaa !7
  %add10 = add nsw i32 %25, %27
  %28 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %28, i32 2
  store i32 %add10, i32* %arrayidx11, align 4, !tbaa !7
  %29 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %29, i32 3
  %30 = load i32, i32* %arrayidx12, align 4, !tbaa !7
  %31 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %31, i32 60
  %32 = load i32, i32* %arrayidx13, align 4, !tbaa !7
  %add14 = add nsw i32 %30, %32
  %33 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i32, i32* %33, i32 3
  store i32 %add14, i32* %arrayidx15, align 4, !tbaa !7
  %34 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %34, i32 4
  %35 = load i32, i32* %arrayidx16, align 4, !tbaa !7
  %36 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %36, i32 59
  %37 = load i32, i32* %arrayidx17, align 4, !tbaa !7
  %add18 = add nsw i32 %35, %37
  %38 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %38, i32 4
  store i32 %add18, i32* %arrayidx19, align 4, !tbaa !7
  %39 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %39, i32 5
  %40 = load i32, i32* %arrayidx20, align 4, !tbaa !7
  %41 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %41, i32 58
  %42 = load i32, i32* %arrayidx21, align 4, !tbaa !7
  %add22 = add nsw i32 %40, %42
  %43 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i32, i32* %43, i32 5
  store i32 %add22, i32* %arrayidx23, align 4, !tbaa !7
  %44 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %44, i32 6
  %45 = load i32, i32* %arrayidx24, align 4, !tbaa !7
  %46 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %46, i32 57
  %47 = load i32, i32* %arrayidx25, align 4, !tbaa !7
  %add26 = add nsw i32 %45, %47
  %48 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %48, i32 6
  store i32 %add26, i32* %arrayidx27, align 4, !tbaa !7
  %49 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i32, i32* %49, i32 7
  %50 = load i32, i32* %arrayidx28, align 4, !tbaa !7
  %51 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i32, i32* %51, i32 56
  %52 = load i32, i32* %arrayidx29, align 4, !tbaa !7
  %add30 = add nsw i32 %50, %52
  %53 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i32, i32* %53, i32 7
  store i32 %add30, i32* %arrayidx31, align 4, !tbaa !7
  %54 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %54, i32 8
  %55 = load i32, i32* %arrayidx32, align 4, !tbaa !7
  %56 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %56, i32 55
  %57 = load i32, i32* %arrayidx33, align 4, !tbaa !7
  %add34 = add nsw i32 %55, %57
  %58 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %58, i32 8
  store i32 %add34, i32* %arrayidx35, align 4, !tbaa !7
  %59 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %59, i32 9
  %60 = load i32, i32* %arrayidx36, align 4, !tbaa !7
  %61 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %61, i32 54
  %62 = load i32, i32* %arrayidx37, align 4, !tbaa !7
  %add38 = add nsw i32 %60, %62
  %63 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %63, i32 9
  store i32 %add38, i32* %arrayidx39, align 4, !tbaa !7
  %64 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %64, i32 10
  %65 = load i32, i32* %arrayidx40, align 4, !tbaa !7
  %66 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %66, i32 53
  %67 = load i32, i32* %arrayidx41, align 4, !tbaa !7
  %add42 = add nsw i32 %65, %67
  %68 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %68, i32 10
  store i32 %add42, i32* %arrayidx43, align 4, !tbaa !7
  %69 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %69, i32 11
  %70 = load i32, i32* %arrayidx44, align 4, !tbaa !7
  %71 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %71, i32 52
  %72 = load i32, i32* %arrayidx45, align 4, !tbaa !7
  %add46 = add nsw i32 %70, %72
  %73 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %73, i32 11
  store i32 %add46, i32* %arrayidx47, align 4, !tbaa !7
  %74 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %74, i32 12
  %75 = load i32, i32* %arrayidx48, align 4, !tbaa !7
  %76 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i32, i32* %76, i32 51
  %77 = load i32, i32* %arrayidx49, align 4, !tbaa !7
  %add50 = add nsw i32 %75, %77
  %78 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %78, i32 12
  store i32 %add50, i32* %arrayidx51, align 4, !tbaa !7
  %79 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %79, i32 13
  %80 = load i32, i32* %arrayidx52, align 4, !tbaa !7
  %81 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i32, i32* %81, i32 50
  %82 = load i32, i32* %arrayidx53, align 4, !tbaa !7
  %add54 = add nsw i32 %80, %82
  %83 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %83, i32 13
  store i32 %add54, i32* %arrayidx55, align 4, !tbaa !7
  %84 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i32, i32* %84, i32 14
  %85 = load i32, i32* %arrayidx56, align 4, !tbaa !7
  %86 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i32, i32* %86, i32 49
  %87 = load i32, i32* %arrayidx57, align 4, !tbaa !7
  %add58 = add nsw i32 %85, %87
  %88 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %88, i32 14
  store i32 %add58, i32* %arrayidx59, align 4, !tbaa !7
  %89 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %89, i32 15
  %90 = load i32, i32* %arrayidx60, align 4, !tbaa !7
  %91 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %91, i32 48
  %92 = load i32, i32* %arrayidx61, align 4, !tbaa !7
  %add62 = add nsw i32 %90, %92
  %93 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds i32, i32* %93, i32 15
  store i32 %add62, i32* %arrayidx63, align 4, !tbaa !7
  %94 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %94, i32 16
  %95 = load i32, i32* %arrayidx64, align 4, !tbaa !7
  %96 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %96, i32 47
  %97 = load i32, i32* %arrayidx65, align 4, !tbaa !7
  %add66 = add nsw i32 %95, %97
  %98 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %98, i32 16
  store i32 %add66, i32* %arrayidx67, align 4, !tbaa !7
  %99 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %99, i32 17
  %100 = load i32, i32* %arrayidx68, align 4, !tbaa !7
  %101 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i32, i32* %101, i32 46
  %102 = load i32, i32* %arrayidx69, align 4, !tbaa !7
  %add70 = add nsw i32 %100, %102
  %103 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %103, i32 17
  store i32 %add70, i32* %arrayidx71, align 4, !tbaa !7
  %104 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds i32, i32* %104, i32 18
  %105 = load i32, i32* %arrayidx72, align 4, !tbaa !7
  %106 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %106, i32 45
  %107 = load i32, i32* %arrayidx73, align 4, !tbaa !7
  %add74 = add nsw i32 %105, %107
  %108 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds i32, i32* %108, i32 18
  store i32 %add74, i32* %arrayidx75, align 4, !tbaa !7
  %109 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %109, i32 19
  %110 = load i32, i32* %arrayidx76, align 4, !tbaa !7
  %111 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %111, i32 44
  %112 = load i32, i32* %arrayidx77, align 4, !tbaa !7
  %add78 = add nsw i32 %110, %112
  %113 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %113, i32 19
  store i32 %add78, i32* %arrayidx79, align 4, !tbaa !7
  %114 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds i32, i32* %114, i32 20
  %115 = load i32, i32* %arrayidx80, align 4, !tbaa !7
  %116 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i32, i32* %116, i32 43
  %117 = load i32, i32* %arrayidx81, align 4, !tbaa !7
  %add82 = add nsw i32 %115, %117
  %118 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %118, i32 20
  store i32 %add82, i32* %arrayidx83, align 4, !tbaa !7
  %119 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i32, i32* %119, i32 21
  %120 = load i32, i32* %arrayidx84, align 4, !tbaa !7
  %121 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %121, i32 42
  %122 = load i32, i32* %arrayidx85, align 4, !tbaa !7
  %add86 = add nsw i32 %120, %122
  %123 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx87 = getelementptr inbounds i32, i32* %123, i32 21
  store i32 %add86, i32* %arrayidx87, align 4, !tbaa !7
  %124 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i32, i32* %124, i32 22
  %125 = load i32, i32* %arrayidx88, align 4, !tbaa !7
  %126 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i32, i32* %126, i32 41
  %127 = load i32, i32* %arrayidx89, align 4, !tbaa !7
  %add90 = add nsw i32 %125, %127
  %128 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i32, i32* %128, i32 22
  store i32 %add90, i32* %arrayidx91, align 4, !tbaa !7
  %129 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i32, i32* %129, i32 23
  %130 = load i32, i32* %arrayidx92, align 4, !tbaa !7
  %131 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %131, i32 40
  %132 = load i32, i32* %arrayidx93, align 4, !tbaa !7
  %add94 = add nsw i32 %130, %132
  %133 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i32, i32* %133, i32 23
  store i32 %add94, i32* %arrayidx95, align 4, !tbaa !7
  %134 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i32, i32* %134, i32 24
  %135 = load i32, i32* %arrayidx96, align 4, !tbaa !7
  %136 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i32, i32* %136, i32 39
  %137 = load i32, i32* %arrayidx97, align 4, !tbaa !7
  %add98 = add nsw i32 %135, %137
  %138 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %138, i32 24
  store i32 %add98, i32* %arrayidx99, align 4, !tbaa !7
  %139 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i32, i32* %139, i32 25
  %140 = load i32, i32* %arrayidx100, align 4, !tbaa !7
  %141 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i32, i32* %141, i32 38
  %142 = load i32, i32* %arrayidx101, align 4, !tbaa !7
  %add102 = add nsw i32 %140, %142
  %143 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %143, i32 25
  store i32 %add102, i32* %arrayidx103, align 4, !tbaa !7
  %144 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx104 = getelementptr inbounds i32, i32* %144, i32 26
  %145 = load i32, i32* %arrayidx104, align 4, !tbaa !7
  %146 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i32, i32* %146, i32 37
  %147 = load i32, i32* %arrayidx105, align 4, !tbaa !7
  %add106 = add nsw i32 %145, %147
  %148 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx107 = getelementptr inbounds i32, i32* %148, i32 26
  store i32 %add106, i32* %arrayidx107, align 4, !tbaa !7
  %149 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %149, i32 27
  %150 = load i32, i32* %arrayidx108, align 4, !tbaa !7
  %151 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %151, i32 36
  %152 = load i32, i32* %arrayidx109, align 4, !tbaa !7
  %add110 = add nsw i32 %150, %152
  %153 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i32, i32* %153, i32 27
  store i32 %add110, i32* %arrayidx111, align 4, !tbaa !7
  %154 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %154, i32 28
  %155 = load i32, i32* %arrayidx112, align 4, !tbaa !7
  %156 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds i32, i32* %156, i32 35
  %157 = load i32, i32* %arrayidx113, align 4, !tbaa !7
  %add114 = add nsw i32 %155, %157
  %158 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %158, i32 28
  store i32 %add114, i32* %arrayidx115, align 4, !tbaa !7
  %159 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds i32, i32* %159, i32 29
  %160 = load i32, i32* %arrayidx116, align 4, !tbaa !7
  %161 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds i32, i32* %161, i32 34
  %162 = load i32, i32* %arrayidx117, align 4, !tbaa !7
  %add118 = add nsw i32 %160, %162
  %163 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i32, i32* %163, i32 29
  store i32 %add118, i32* %arrayidx119, align 4, !tbaa !7
  %164 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i32, i32* %164, i32 30
  %165 = load i32, i32* %arrayidx120, align 4, !tbaa !7
  %166 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i32, i32* %166, i32 33
  %167 = load i32, i32* %arrayidx121, align 4, !tbaa !7
  %add122 = add nsw i32 %165, %167
  %168 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %168, i32 30
  store i32 %add122, i32* %arrayidx123, align 4, !tbaa !7
  %169 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %169, i32 31
  %170 = load i32, i32* %arrayidx124, align 4, !tbaa !7
  %171 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %171, i32 32
  %172 = load i32, i32* %arrayidx125, align 4, !tbaa !7
  %add126 = add nsw i32 %170, %172
  %173 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %173, i32 31
  store i32 %add126, i32* %arrayidx127, align 4, !tbaa !7
  %174 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx128 = getelementptr inbounds i32, i32* %174, i32 32
  %175 = load i32, i32* %arrayidx128, align 4, !tbaa !7
  %sub = sub nsw i32 0, %175
  %176 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds i32, i32* %176, i32 31
  %177 = load i32, i32* %arrayidx129, align 4, !tbaa !7
  %add130 = add nsw i32 %sub, %177
  %178 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i32, i32* %178, i32 32
  store i32 %add130, i32* %arrayidx131, align 4, !tbaa !7
  %179 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %179, i32 33
  %180 = load i32, i32* %arrayidx132, align 4, !tbaa !7
  %sub133 = sub nsw i32 0, %180
  %181 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx134 = getelementptr inbounds i32, i32* %181, i32 30
  %182 = load i32, i32* %arrayidx134, align 4, !tbaa !7
  %add135 = add nsw i32 %sub133, %182
  %183 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %183, i32 33
  store i32 %add135, i32* %arrayidx136, align 4, !tbaa !7
  %184 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds i32, i32* %184, i32 34
  %185 = load i32, i32* %arrayidx137, align 4, !tbaa !7
  %sub138 = sub nsw i32 0, %185
  %186 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i32, i32* %186, i32 29
  %187 = load i32, i32* %arrayidx139, align 4, !tbaa !7
  %add140 = add nsw i32 %sub138, %187
  %188 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx141 = getelementptr inbounds i32, i32* %188, i32 34
  store i32 %add140, i32* %arrayidx141, align 4, !tbaa !7
  %189 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx142 = getelementptr inbounds i32, i32* %189, i32 35
  %190 = load i32, i32* %arrayidx142, align 4, !tbaa !7
  %sub143 = sub nsw i32 0, %190
  %191 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds i32, i32* %191, i32 28
  %192 = load i32, i32* %arrayidx144, align 4, !tbaa !7
  %add145 = add nsw i32 %sub143, %192
  %193 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i32, i32* %193, i32 35
  store i32 %add145, i32* %arrayidx146, align 4, !tbaa !7
  %194 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx147 = getelementptr inbounds i32, i32* %194, i32 36
  %195 = load i32, i32* %arrayidx147, align 4, !tbaa !7
  %sub148 = sub nsw i32 0, %195
  %196 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx149 = getelementptr inbounds i32, i32* %196, i32 27
  %197 = load i32, i32* %arrayidx149, align 4, !tbaa !7
  %add150 = add nsw i32 %sub148, %197
  %198 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %198, i32 36
  store i32 %add150, i32* %arrayidx151, align 4, !tbaa !7
  %199 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds i32, i32* %199, i32 37
  %200 = load i32, i32* %arrayidx152, align 4, !tbaa !7
  %sub153 = sub nsw i32 0, %200
  %201 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds i32, i32* %201, i32 26
  %202 = load i32, i32* %arrayidx154, align 4, !tbaa !7
  %add155 = add nsw i32 %sub153, %202
  %203 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds i32, i32* %203, i32 37
  store i32 %add155, i32* %arrayidx156, align 4, !tbaa !7
  %204 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx157 = getelementptr inbounds i32, i32* %204, i32 38
  %205 = load i32, i32* %arrayidx157, align 4, !tbaa !7
  %sub158 = sub nsw i32 0, %205
  %206 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx159 = getelementptr inbounds i32, i32* %206, i32 25
  %207 = load i32, i32* %arrayidx159, align 4, !tbaa !7
  %add160 = add nsw i32 %sub158, %207
  %208 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds i32, i32* %208, i32 38
  store i32 %add160, i32* %arrayidx161, align 4, !tbaa !7
  %209 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx162 = getelementptr inbounds i32, i32* %209, i32 39
  %210 = load i32, i32* %arrayidx162, align 4, !tbaa !7
  %sub163 = sub nsw i32 0, %210
  %211 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i32, i32* %211, i32 24
  %212 = load i32, i32* %arrayidx164, align 4, !tbaa !7
  %add165 = add nsw i32 %sub163, %212
  %213 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i32, i32* %213, i32 39
  store i32 %add165, i32* %arrayidx166, align 4, !tbaa !7
  %214 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds i32, i32* %214, i32 40
  %215 = load i32, i32* %arrayidx167, align 4, !tbaa !7
  %sub168 = sub nsw i32 0, %215
  %216 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds i32, i32* %216, i32 23
  %217 = load i32, i32* %arrayidx169, align 4, !tbaa !7
  %add170 = add nsw i32 %sub168, %217
  %218 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx171 = getelementptr inbounds i32, i32* %218, i32 40
  store i32 %add170, i32* %arrayidx171, align 4, !tbaa !7
  %219 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx172 = getelementptr inbounds i32, i32* %219, i32 41
  %220 = load i32, i32* %arrayidx172, align 4, !tbaa !7
  %sub173 = sub nsw i32 0, %220
  %221 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i32, i32* %221, i32 22
  %222 = load i32, i32* %arrayidx174, align 4, !tbaa !7
  %add175 = add nsw i32 %sub173, %222
  %223 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds i32, i32* %223, i32 41
  store i32 %add175, i32* %arrayidx176, align 4, !tbaa !7
  %224 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx177 = getelementptr inbounds i32, i32* %224, i32 42
  %225 = load i32, i32* %arrayidx177, align 4, !tbaa !7
  %sub178 = sub nsw i32 0, %225
  %226 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx179 = getelementptr inbounds i32, i32* %226, i32 21
  %227 = load i32, i32* %arrayidx179, align 4, !tbaa !7
  %add180 = add nsw i32 %sub178, %227
  %228 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx181 = getelementptr inbounds i32, i32* %228, i32 42
  store i32 %add180, i32* %arrayidx181, align 4, !tbaa !7
  %229 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx182 = getelementptr inbounds i32, i32* %229, i32 43
  %230 = load i32, i32* %arrayidx182, align 4, !tbaa !7
  %sub183 = sub nsw i32 0, %230
  %231 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx184 = getelementptr inbounds i32, i32* %231, i32 20
  %232 = load i32, i32* %arrayidx184, align 4, !tbaa !7
  %add185 = add nsw i32 %sub183, %232
  %233 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds i32, i32* %233, i32 43
  store i32 %add185, i32* %arrayidx186, align 4, !tbaa !7
  %234 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx187 = getelementptr inbounds i32, i32* %234, i32 44
  %235 = load i32, i32* %arrayidx187, align 4, !tbaa !7
  %sub188 = sub nsw i32 0, %235
  %236 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx189 = getelementptr inbounds i32, i32* %236, i32 19
  %237 = load i32, i32* %arrayidx189, align 4, !tbaa !7
  %add190 = add nsw i32 %sub188, %237
  %238 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx191 = getelementptr inbounds i32, i32* %238, i32 44
  store i32 %add190, i32* %arrayidx191, align 4, !tbaa !7
  %239 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds i32, i32* %239, i32 45
  %240 = load i32, i32* %arrayidx192, align 4, !tbaa !7
  %sub193 = sub nsw i32 0, %240
  %241 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds i32, i32* %241, i32 18
  %242 = load i32, i32* %arrayidx194, align 4, !tbaa !7
  %add195 = add nsw i32 %sub193, %242
  %243 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx196 = getelementptr inbounds i32, i32* %243, i32 45
  store i32 %add195, i32* %arrayidx196, align 4, !tbaa !7
  %244 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx197 = getelementptr inbounds i32, i32* %244, i32 46
  %245 = load i32, i32* %arrayidx197, align 4, !tbaa !7
  %sub198 = sub nsw i32 0, %245
  %246 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx199 = getelementptr inbounds i32, i32* %246, i32 17
  %247 = load i32, i32* %arrayidx199, align 4, !tbaa !7
  %add200 = add nsw i32 %sub198, %247
  %248 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx201 = getelementptr inbounds i32, i32* %248, i32 46
  store i32 %add200, i32* %arrayidx201, align 4, !tbaa !7
  %249 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx202 = getelementptr inbounds i32, i32* %249, i32 47
  %250 = load i32, i32* %arrayidx202, align 4, !tbaa !7
  %sub203 = sub nsw i32 0, %250
  %251 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx204 = getelementptr inbounds i32, i32* %251, i32 16
  %252 = load i32, i32* %arrayidx204, align 4, !tbaa !7
  %add205 = add nsw i32 %sub203, %252
  %253 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx206 = getelementptr inbounds i32, i32* %253, i32 47
  store i32 %add205, i32* %arrayidx206, align 4, !tbaa !7
  %254 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx207 = getelementptr inbounds i32, i32* %254, i32 48
  %255 = load i32, i32* %arrayidx207, align 4, !tbaa !7
  %sub208 = sub nsw i32 0, %255
  %256 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx209 = getelementptr inbounds i32, i32* %256, i32 15
  %257 = load i32, i32* %arrayidx209, align 4, !tbaa !7
  %add210 = add nsw i32 %sub208, %257
  %258 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx211 = getelementptr inbounds i32, i32* %258, i32 48
  store i32 %add210, i32* %arrayidx211, align 4, !tbaa !7
  %259 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx212 = getelementptr inbounds i32, i32* %259, i32 49
  %260 = load i32, i32* %arrayidx212, align 4, !tbaa !7
  %sub213 = sub nsw i32 0, %260
  %261 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx214 = getelementptr inbounds i32, i32* %261, i32 14
  %262 = load i32, i32* %arrayidx214, align 4, !tbaa !7
  %add215 = add nsw i32 %sub213, %262
  %263 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx216 = getelementptr inbounds i32, i32* %263, i32 49
  store i32 %add215, i32* %arrayidx216, align 4, !tbaa !7
  %264 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx217 = getelementptr inbounds i32, i32* %264, i32 50
  %265 = load i32, i32* %arrayidx217, align 4, !tbaa !7
  %sub218 = sub nsw i32 0, %265
  %266 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx219 = getelementptr inbounds i32, i32* %266, i32 13
  %267 = load i32, i32* %arrayidx219, align 4, !tbaa !7
  %add220 = add nsw i32 %sub218, %267
  %268 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx221 = getelementptr inbounds i32, i32* %268, i32 50
  store i32 %add220, i32* %arrayidx221, align 4, !tbaa !7
  %269 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx222 = getelementptr inbounds i32, i32* %269, i32 51
  %270 = load i32, i32* %arrayidx222, align 4, !tbaa !7
  %sub223 = sub nsw i32 0, %270
  %271 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds i32, i32* %271, i32 12
  %272 = load i32, i32* %arrayidx224, align 4, !tbaa !7
  %add225 = add nsw i32 %sub223, %272
  %273 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx226 = getelementptr inbounds i32, i32* %273, i32 51
  store i32 %add225, i32* %arrayidx226, align 4, !tbaa !7
  %274 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx227 = getelementptr inbounds i32, i32* %274, i32 52
  %275 = load i32, i32* %arrayidx227, align 4, !tbaa !7
  %sub228 = sub nsw i32 0, %275
  %276 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx229 = getelementptr inbounds i32, i32* %276, i32 11
  %277 = load i32, i32* %arrayidx229, align 4, !tbaa !7
  %add230 = add nsw i32 %sub228, %277
  %278 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx231 = getelementptr inbounds i32, i32* %278, i32 52
  store i32 %add230, i32* %arrayidx231, align 4, !tbaa !7
  %279 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx232 = getelementptr inbounds i32, i32* %279, i32 53
  %280 = load i32, i32* %arrayidx232, align 4, !tbaa !7
  %sub233 = sub nsw i32 0, %280
  %281 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds i32, i32* %281, i32 10
  %282 = load i32, i32* %arrayidx234, align 4, !tbaa !7
  %add235 = add nsw i32 %sub233, %282
  %283 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds i32, i32* %283, i32 53
  store i32 %add235, i32* %arrayidx236, align 4, !tbaa !7
  %284 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx237 = getelementptr inbounds i32, i32* %284, i32 54
  %285 = load i32, i32* %arrayidx237, align 4, !tbaa !7
  %sub238 = sub nsw i32 0, %285
  %286 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx239 = getelementptr inbounds i32, i32* %286, i32 9
  %287 = load i32, i32* %arrayidx239, align 4, !tbaa !7
  %add240 = add nsw i32 %sub238, %287
  %288 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx241 = getelementptr inbounds i32, i32* %288, i32 54
  store i32 %add240, i32* %arrayidx241, align 4, !tbaa !7
  %289 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx242 = getelementptr inbounds i32, i32* %289, i32 55
  %290 = load i32, i32* %arrayidx242, align 4, !tbaa !7
  %sub243 = sub nsw i32 0, %290
  %291 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx244 = getelementptr inbounds i32, i32* %291, i32 8
  %292 = load i32, i32* %arrayidx244, align 4, !tbaa !7
  %add245 = add nsw i32 %sub243, %292
  %293 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx246 = getelementptr inbounds i32, i32* %293, i32 55
  store i32 %add245, i32* %arrayidx246, align 4, !tbaa !7
  %294 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx247 = getelementptr inbounds i32, i32* %294, i32 56
  %295 = load i32, i32* %arrayidx247, align 4, !tbaa !7
  %sub248 = sub nsw i32 0, %295
  %296 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx249 = getelementptr inbounds i32, i32* %296, i32 7
  %297 = load i32, i32* %arrayidx249, align 4, !tbaa !7
  %add250 = add nsw i32 %sub248, %297
  %298 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx251 = getelementptr inbounds i32, i32* %298, i32 56
  store i32 %add250, i32* %arrayidx251, align 4, !tbaa !7
  %299 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx252 = getelementptr inbounds i32, i32* %299, i32 57
  %300 = load i32, i32* %arrayidx252, align 4, !tbaa !7
  %sub253 = sub nsw i32 0, %300
  %301 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx254 = getelementptr inbounds i32, i32* %301, i32 6
  %302 = load i32, i32* %arrayidx254, align 4, !tbaa !7
  %add255 = add nsw i32 %sub253, %302
  %303 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx256 = getelementptr inbounds i32, i32* %303, i32 57
  store i32 %add255, i32* %arrayidx256, align 4, !tbaa !7
  %304 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx257 = getelementptr inbounds i32, i32* %304, i32 58
  %305 = load i32, i32* %arrayidx257, align 4, !tbaa !7
  %sub258 = sub nsw i32 0, %305
  %306 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx259 = getelementptr inbounds i32, i32* %306, i32 5
  %307 = load i32, i32* %arrayidx259, align 4, !tbaa !7
  %add260 = add nsw i32 %sub258, %307
  %308 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx261 = getelementptr inbounds i32, i32* %308, i32 58
  store i32 %add260, i32* %arrayidx261, align 4, !tbaa !7
  %309 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx262 = getelementptr inbounds i32, i32* %309, i32 59
  %310 = load i32, i32* %arrayidx262, align 4, !tbaa !7
  %sub263 = sub nsw i32 0, %310
  %311 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx264 = getelementptr inbounds i32, i32* %311, i32 4
  %312 = load i32, i32* %arrayidx264, align 4, !tbaa !7
  %add265 = add nsw i32 %sub263, %312
  %313 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx266 = getelementptr inbounds i32, i32* %313, i32 59
  store i32 %add265, i32* %arrayidx266, align 4, !tbaa !7
  %314 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx267 = getelementptr inbounds i32, i32* %314, i32 60
  %315 = load i32, i32* %arrayidx267, align 4, !tbaa !7
  %sub268 = sub nsw i32 0, %315
  %316 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx269 = getelementptr inbounds i32, i32* %316, i32 3
  %317 = load i32, i32* %arrayidx269, align 4, !tbaa !7
  %add270 = add nsw i32 %sub268, %317
  %318 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx271 = getelementptr inbounds i32, i32* %318, i32 60
  store i32 %add270, i32* %arrayidx271, align 4, !tbaa !7
  %319 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx272 = getelementptr inbounds i32, i32* %319, i32 61
  %320 = load i32, i32* %arrayidx272, align 4, !tbaa !7
  %sub273 = sub nsw i32 0, %320
  %321 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx274 = getelementptr inbounds i32, i32* %321, i32 2
  %322 = load i32, i32* %arrayidx274, align 4, !tbaa !7
  %add275 = add nsw i32 %sub273, %322
  %323 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx276 = getelementptr inbounds i32, i32* %323, i32 61
  store i32 %add275, i32* %arrayidx276, align 4, !tbaa !7
  %324 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx277 = getelementptr inbounds i32, i32* %324, i32 62
  %325 = load i32, i32* %arrayidx277, align 4, !tbaa !7
  %sub278 = sub nsw i32 0, %325
  %326 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx279 = getelementptr inbounds i32, i32* %326, i32 1
  %327 = load i32, i32* %arrayidx279, align 4, !tbaa !7
  %add280 = add nsw i32 %sub278, %327
  %328 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx281 = getelementptr inbounds i32, i32* %328, i32 62
  store i32 %add280, i32* %arrayidx281, align 4, !tbaa !7
  %329 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx282 = getelementptr inbounds i32, i32* %329, i32 63
  %330 = load i32, i32* %arrayidx282, align 4, !tbaa !7
  %sub283 = sub nsw i32 0, %330
  %331 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %arrayidx284 = getelementptr inbounds i32, i32* %331, i32 0
  %332 = load i32, i32* %arrayidx284, align 4, !tbaa !7
  %add285 = add nsw i32 %sub283, %332
  %333 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx286 = getelementptr inbounds i32, i32* %333, i32 63
  store i32 %add285, i32* %arrayidx286, align 4, !tbaa !7
  %334 = load i32, i32* %stage, align 4, !tbaa !7
  %335 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %336 = load i32*, i32** %bf1, align 4, !tbaa !2
  %337 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %338 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx287 = getelementptr inbounds i8, i8* %337, i32 %338
  %339 = load i8, i8* %arrayidx287, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %334, i32* %335, i32* %336, i32 64, i8 signext %339)
  %340 = load i32, i32* %stage, align 4, !tbaa !7
  %inc288 = add nsw i32 %340, 1
  store i32 %inc288, i32* %stage, align 4, !tbaa !7
  %341 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv = sext i8 %341 to i32
  %call = call i32* @cospi_arr(i32 %conv)
  store i32* %call, i32** %cospi, align 4, !tbaa !2
  %342 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %342, i32** %bf0, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay, i32** %bf1, align 4, !tbaa !2
  %343 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx289 = getelementptr inbounds i32, i32* %343, i32 0
  %344 = load i32, i32* %arrayidx289, align 4, !tbaa !7
  %345 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx290 = getelementptr inbounds i32, i32* %345, i32 31
  %346 = load i32, i32* %arrayidx290, align 4, !tbaa !7
  %add291 = add nsw i32 %344, %346
  %347 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx292 = getelementptr inbounds i32, i32* %347, i32 0
  store i32 %add291, i32* %arrayidx292, align 4, !tbaa !7
  %348 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx293 = getelementptr inbounds i32, i32* %348, i32 1
  %349 = load i32, i32* %arrayidx293, align 4, !tbaa !7
  %350 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx294 = getelementptr inbounds i32, i32* %350, i32 30
  %351 = load i32, i32* %arrayidx294, align 4, !tbaa !7
  %add295 = add nsw i32 %349, %351
  %352 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx296 = getelementptr inbounds i32, i32* %352, i32 1
  store i32 %add295, i32* %arrayidx296, align 4, !tbaa !7
  %353 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx297 = getelementptr inbounds i32, i32* %353, i32 2
  %354 = load i32, i32* %arrayidx297, align 4, !tbaa !7
  %355 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx298 = getelementptr inbounds i32, i32* %355, i32 29
  %356 = load i32, i32* %arrayidx298, align 4, !tbaa !7
  %add299 = add nsw i32 %354, %356
  %357 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx300 = getelementptr inbounds i32, i32* %357, i32 2
  store i32 %add299, i32* %arrayidx300, align 4, !tbaa !7
  %358 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx301 = getelementptr inbounds i32, i32* %358, i32 3
  %359 = load i32, i32* %arrayidx301, align 4, !tbaa !7
  %360 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx302 = getelementptr inbounds i32, i32* %360, i32 28
  %361 = load i32, i32* %arrayidx302, align 4, !tbaa !7
  %add303 = add nsw i32 %359, %361
  %362 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx304 = getelementptr inbounds i32, i32* %362, i32 3
  store i32 %add303, i32* %arrayidx304, align 4, !tbaa !7
  %363 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx305 = getelementptr inbounds i32, i32* %363, i32 4
  %364 = load i32, i32* %arrayidx305, align 4, !tbaa !7
  %365 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx306 = getelementptr inbounds i32, i32* %365, i32 27
  %366 = load i32, i32* %arrayidx306, align 4, !tbaa !7
  %add307 = add nsw i32 %364, %366
  %367 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx308 = getelementptr inbounds i32, i32* %367, i32 4
  store i32 %add307, i32* %arrayidx308, align 4, !tbaa !7
  %368 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx309 = getelementptr inbounds i32, i32* %368, i32 5
  %369 = load i32, i32* %arrayidx309, align 4, !tbaa !7
  %370 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx310 = getelementptr inbounds i32, i32* %370, i32 26
  %371 = load i32, i32* %arrayidx310, align 4, !tbaa !7
  %add311 = add nsw i32 %369, %371
  %372 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx312 = getelementptr inbounds i32, i32* %372, i32 5
  store i32 %add311, i32* %arrayidx312, align 4, !tbaa !7
  %373 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx313 = getelementptr inbounds i32, i32* %373, i32 6
  %374 = load i32, i32* %arrayidx313, align 4, !tbaa !7
  %375 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx314 = getelementptr inbounds i32, i32* %375, i32 25
  %376 = load i32, i32* %arrayidx314, align 4, !tbaa !7
  %add315 = add nsw i32 %374, %376
  %377 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx316 = getelementptr inbounds i32, i32* %377, i32 6
  store i32 %add315, i32* %arrayidx316, align 4, !tbaa !7
  %378 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx317 = getelementptr inbounds i32, i32* %378, i32 7
  %379 = load i32, i32* %arrayidx317, align 4, !tbaa !7
  %380 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx318 = getelementptr inbounds i32, i32* %380, i32 24
  %381 = load i32, i32* %arrayidx318, align 4, !tbaa !7
  %add319 = add nsw i32 %379, %381
  %382 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx320 = getelementptr inbounds i32, i32* %382, i32 7
  store i32 %add319, i32* %arrayidx320, align 4, !tbaa !7
  %383 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx321 = getelementptr inbounds i32, i32* %383, i32 8
  %384 = load i32, i32* %arrayidx321, align 4, !tbaa !7
  %385 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx322 = getelementptr inbounds i32, i32* %385, i32 23
  %386 = load i32, i32* %arrayidx322, align 4, !tbaa !7
  %add323 = add nsw i32 %384, %386
  %387 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx324 = getelementptr inbounds i32, i32* %387, i32 8
  store i32 %add323, i32* %arrayidx324, align 4, !tbaa !7
  %388 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx325 = getelementptr inbounds i32, i32* %388, i32 9
  %389 = load i32, i32* %arrayidx325, align 4, !tbaa !7
  %390 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx326 = getelementptr inbounds i32, i32* %390, i32 22
  %391 = load i32, i32* %arrayidx326, align 4, !tbaa !7
  %add327 = add nsw i32 %389, %391
  %392 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx328 = getelementptr inbounds i32, i32* %392, i32 9
  store i32 %add327, i32* %arrayidx328, align 4, !tbaa !7
  %393 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx329 = getelementptr inbounds i32, i32* %393, i32 10
  %394 = load i32, i32* %arrayidx329, align 4, !tbaa !7
  %395 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx330 = getelementptr inbounds i32, i32* %395, i32 21
  %396 = load i32, i32* %arrayidx330, align 4, !tbaa !7
  %add331 = add nsw i32 %394, %396
  %397 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx332 = getelementptr inbounds i32, i32* %397, i32 10
  store i32 %add331, i32* %arrayidx332, align 4, !tbaa !7
  %398 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx333 = getelementptr inbounds i32, i32* %398, i32 11
  %399 = load i32, i32* %arrayidx333, align 4, !tbaa !7
  %400 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx334 = getelementptr inbounds i32, i32* %400, i32 20
  %401 = load i32, i32* %arrayidx334, align 4, !tbaa !7
  %add335 = add nsw i32 %399, %401
  %402 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx336 = getelementptr inbounds i32, i32* %402, i32 11
  store i32 %add335, i32* %arrayidx336, align 4, !tbaa !7
  %403 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx337 = getelementptr inbounds i32, i32* %403, i32 12
  %404 = load i32, i32* %arrayidx337, align 4, !tbaa !7
  %405 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx338 = getelementptr inbounds i32, i32* %405, i32 19
  %406 = load i32, i32* %arrayidx338, align 4, !tbaa !7
  %add339 = add nsw i32 %404, %406
  %407 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx340 = getelementptr inbounds i32, i32* %407, i32 12
  store i32 %add339, i32* %arrayidx340, align 4, !tbaa !7
  %408 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx341 = getelementptr inbounds i32, i32* %408, i32 13
  %409 = load i32, i32* %arrayidx341, align 4, !tbaa !7
  %410 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx342 = getelementptr inbounds i32, i32* %410, i32 18
  %411 = load i32, i32* %arrayidx342, align 4, !tbaa !7
  %add343 = add nsw i32 %409, %411
  %412 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx344 = getelementptr inbounds i32, i32* %412, i32 13
  store i32 %add343, i32* %arrayidx344, align 4, !tbaa !7
  %413 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx345 = getelementptr inbounds i32, i32* %413, i32 14
  %414 = load i32, i32* %arrayidx345, align 4, !tbaa !7
  %415 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx346 = getelementptr inbounds i32, i32* %415, i32 17
  %416 = load i32, i32* %arrayidx346, align 4, !tbaa !7
  %add347 = add nsw i32 %414, %416
  %417 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx348 = getelementptr inbounds i32, i32* %417, i32 14
  store i32 %add347, i32* %arrayidx348, align 4, !tbaa !7
  %418 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx349 = getelementptr inbounds i32, i32* %418, i32 15
  %419 = load i32, i32* %arrayidx349, align 4, !tbaa !7
  %420 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx350 = getelementptr inbounds i32, i32* %420, i32 16
  %421 = load i32, i32* %arrayidx350, align 4, !tbaa !7
  %add351 = add nsw i32 %419, %421
  %422 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx352 = getelementptr inbounds i32, i32* %422, i32 15
  store i32 %add351, i32* %arrayidx352, align 4, !tbaa !7
  %423 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx353 = getelementptr inbounds i32, i32* %423, i32 16
  %424 = load i32, i32* %arrayidx353, align 4, !tbaa !7
  %sub354 = sub nsw i32 0, %424
  %425 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx355 = getelementptr inbounds i32, i32* %425, i32 15
  %426 = load i32, i32* %arrayidx355, align 4, !tbaa !7
  %add356 = add nsw i32 %sub354, %426
  %427 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx357 = getelementptr inbounds i32, i32* %427, i32 16
  store i32 %add356, i32* %arrayidx357, align 4, !tbaa !7
  %428 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx358 = getelementptr inbounds i32, i32* %428, i32 17
  %429 = load i32, i32* %arrayidx358, align 4, !tbaa !7
  %sub359 = sub nsw i32 0, %429
  %430 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx360 = getelementptr inbounds i32, i32* %430, i32 14
  %431 = load i32, i32* %arrayidx360, align 4, !tbaa !7
  %add361 = add nsw i32 %sub359, %431
  %432 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx362 = getelementptr inbounds i32, i32* %432, i32 17
  store i32 %add361, i32* %arrayidx362, align 4, !tbaa !7
  %433 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx363 = getelementptr inbounds i32, i32* %433, i32 18
  %434 = load i32, i32* %arrayidx363, align 4, !tbaa !7
  %sub364 = sub nsw i32 0, %434
  %435 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx365 = getelementptr inbounds i32, i32* %435, i32 13
  %436 = load i32, i32* %arrayidx365, align 4, !tbaa !7
  %add366 = add nsw i32 %sub364, %436
  %437 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx367 = getelementptr inbounds i32, i32* %437, i32 18
  store i32 %add366, i32* %arrayidx367, align 4, !tbaa !7
  %438 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx368 = getelementptr inbounds i32, i32* %438, i32 19
  %439 = load i32, i32* %arrayidx368, align 4, !tbaa !7
  %sub369 = sub nsw i32 0, %439
  %440 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx370 = getelementptr inbounds i32, i32* %440, i32 12
  %441 = load i32, i32* %arrayidx370, align 4, !tbaa !7
  %add371 = add nsw i32 %sub369, %441
  %442 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx372 = getelementptr inbounds i32, i32* %442, i32 19
  store i32 %add371, i32* %arrayidx372, align 4, !tbaa !7
  %443 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx373 = getelementptr inbounds i32, i32* %443, i32 20
  %444 = load i32, i32* %arrayidx373, align 4, !tbaa !7
  %sub374 = sub nsw i32 0, %444
  %445 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx375 = getelementptr inbounds i32, i32* %445, i32 11
  %446 = load i32, i32* %arrayidx375, align 4, !tbaa !7
  %add376 = add nsw i32 %sub374, %446
  %447 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx377 = getelementptr inbounds i32, i32* %447, i32 20
  store i32 %add376, i32* %arrayidx377, align 4, !tbaa !7
  %448 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx378 = getelementptr inbounds i32, i32* %448, i32 21
  %449 = load i32, i32* %arrayidx378, align 4, !tbaa !7
  %sub379 = sub nsw i32 0, %449
  %450 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx380 = getelementptr inbounds i32, i32* %450, i32 10
  %451 = load i32, i32* %arrayidx380, align 4, !tbaa !7
  %add381 = add nsw i32 %sub379, %451
  %452 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx382 = getelementptr inbounds i32, i32* %452, i32 21
  store i32 %add381, i32* %arrayidx382, align 4, !tbaa !7
  %453 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx383 = getelementptr inbounds i32, i32* %453, i32 22
  %454 = load i32, i32* %arrayidx383, align 4, !tbaa !7
  %sub384 = sub nsw i32 0, %454
  %455 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx385 = getelementptr inbounds i32, i32* %455, i32 9
  %456 = load i32, i32* %arrayidx385, align 4, !tbaa !7
  %add386 = add nsw i32 %sub384, %456
  %457 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx387 = getelementptr inbounds i32, i32* %457, i32 22
  store i32 %add386, i32* %arrayidx387, align 4, !tbaa !7
  %458 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx388 = getelementptr inbounds i32, i32* %458, i32 23
  %459 = load i32, i32* %arrayidx388, align 4, !tbaa !7
  %sub389 = sub nsw i32 0, %459
  %460 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx390 = getelementptr inbounds i32, i32* %460, i32 8
  %461 = load i32, i32* %arrayidx390, align 4, !tbaa !7
  %add391 = add nsw i32 %sub389, %461
  %462 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx392 = getelementptr inbounds i32, i32* %462, i32 23
  store i32 %add391, i32* %arrayidx392, align 4, !tbaa !7
  %463 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx393 = getelementptr inbounds i32, i32* %463, i32 24
  %464 = load i32, i32* %arrayidx393, align 4, !tbaa !7
  %sub394 = sub nsw i32 0, %464
  %465 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx395 = getelementptr inbounds i32, i32* %465, i32 7
  %466 = load i32, i32* %arrayidx395, align 4, !tbaa !7
  %add396 = add nsw i32 %sub394, %466
  %467 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx397 = getelementptr inbounds i32, i32* %467, i32 24
  store i32 %add396, i32* %arrayidx397, align 4, !tbaa !7
  %468 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx398 = getelementptr inbounds i32, i32* %468, i32 25
  %469 = load i32, i32* %arrayidx398, align 4, !tbaa !7
  %sub399 = sub nsw i32 0, %469
  %470 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx400 = getelementptr inbounds i32, i32* %470, i32 6
  %471 = load i32, i32* %arrayidx400, align 4, !tbaa !7
  %add401 = add nsw i32 %sub399, %471
  %472 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx402 = getelementptr inbounds i32, i32* %472, i32 25
  store i32 %add401, i32* %arrayidx402, align 4, !tbaa !7
  %473 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx403 = getelementptr inbounds i32, i32* %473, i32 26
  %474 = load i32, i32* %arrayidx403, align 4, !tbaa !7
  %sub404 = sub nsw i32 0, %474
  %475 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx405 = getelementptr inbounds i32, i32* %475, i32 5
  %476 = load i32, i32* %arrayidx405, align 4, !tbaa !7
  %add406 = add nsw i32 %sub404, %476
  %477 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx407 = getelementptr inbounds i32, i32* %477, i32 26
  store i32 %add406, i32* %arrayidx407, align 4, !tbaa !7
  %478 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx408 = getelementptr inbounds i32, i32* %478, i32 27
  %479 = load i32, i32* %arrayidx408, align 4, !tbaa !7
  %sub409 = sub nsw i32 0, %479
  %480 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx410 = getelementptr inbounds i32, i32* %480, i32 4
  %481 = load i32, i32* %arrayidx410, align 4, !tbaa !7
  %add411 = add nsw i32 %sub409, %481
  %482 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx412 = getelementptr inbounds i32, i32* %482, i32 27
  store i32 %add411, i32* %arrayidx412, align 4, !tbaa !7
  %483 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx413 = getelementptr inbounds i32, i32* %483, i32 28
  %484 = load i32, i32* %arrayidx413, align 4, !tbaa !7
  %sub414 = sub nsw i32 0, %484
  %485 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx415 = getelementptr inbounds i32, i32* %485, i32 3
  %486 = load i32, i32* %arrayidx415, align 4, !tbaa !7
  %add416 = add nsw i32 %sub414, %486
  %487 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx417 = getelementptr inbounds i32, i32* %487, i32 28
  store i32 %add416, i32* %arrayidx417, align 4, !tbaa !7
  %488 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx418 = getelementptr inbounds i32, i32* %488, i32 29
  %489 = load i32, i32* %arrayidx418, align 4, !tbaa !7
  %sub419 = sub nsw i32 0, %489
  %490 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx420 = getelementptr inbounds i32, i32* %490, i32 2
  %491 = load i32, i32* %arrayidx420, align 4, !tbaa !7
  %add421 = add nsw i32 %sub419, %491
  %492 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx422 = getelementptr inbounds i32, i32* %492, i32 29
  store i32 %add421, i32* %arrayidx422, align 4, !tbaa !7
  %493 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx423 = getelementptr inbounds i32, i32* %493, i32 30
  %494 = load i32, i32* %arrayidx423, align 4, !tbaa !7
  %sub424 = sub nsw i32 0, %494
  %495 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx425 = getelementptr inbounds i32, i32* %495, i32 1
  %496 = load i32, i32* %arrayidx425, align 4, !tbaa !7
  %add426 = add nsw i32 %sub424, %496
  %497 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx427 = getelementptr inbounds i32, i32* %497, i32 30
  store i32 %add426, i32* %arrayidx427, align 4, !tbaa !7
  %498 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx428 = getelementptr inbounds i32, i32* %498, i32 31
  %499 = load i32, i32* %arrayidx428, align 4, !tbaa !7
  %sub429 = sub nsw i32 0, %499
  %500 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx430 = getelementptr inbounds i32, i32* %500, i32 0
  %501 = load i32, i32* %arrayidx430, align 4, !tbaa !7
  %add431 = add nsw i32 %sub429, %501
  %502 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx432 = getelementptr inbounds i32, i32* %502, i32 31
  store i32 %add431, i32* %arrayidx432, align 4, !tbaa !7
  %503 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx433 = getelementptr inbounds i32, i32* %503, i32 32
  %504 = load i32, i32* %arrayidx433, align 4, !tbaa !7
  %505 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx434 = getelementptr inbounds i32, i32* %505, i32 32
  store i32 %504, i32* %arrayidx434, align 4, !tbaa !7
  %506 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx435 = getelementptr inbounds i32, i32* %506, i32 33
  %507 = load i32, i32* %arrayidx435, align 4, !tbaa !7
  %508 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx436 = getelementptr inbounds i32, i32* %508, i32 33
  store i32 %507, i32* %arrayidx436, align 4, !tbaa !7
  %509 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx437 = getelementptr inbounds i32, i32* %509, i32 34
  %510 = load i32, i32* %arrayidx437, align 4, !tbaa !7
  %511 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx438 = getelementptr inbounds i32, i32* %511, i32 34
  store i32 %510, i32* %arrayidx438, align 4, !tbaa !7
  %512 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx439 = getelementptr inbounds i32, i32* %512, i32 35
  %513 = load i32, i32* %arrayidx439, align 4, !tbaa !7
  %514 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx440 = getelementptr inbounds i32, i32* %514, i32 35
  store i32 %513, i32* %arrayidx440, align 4, !tbaa !7
  %515 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx441 = getelementptr inbounds i32, i32* %515, i32 36
  %516 = load i32, i32* %arrayidx441, align 4, !tbaa !7
  %517 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx442 = getelementptr inbounds i32, i32* %517, i32 36
  store i32 %516, i32* %arrayidx442, align 4, !tbaa !7
  %518 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx443 = getelementptr inbounds i32, i32* %518, i32 37
  %519 = load i32, i32* %arrayidx443, align 4, !tbaa !7
  %520 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx444 = getelementptr inbounds i32, i32* %520, i32 37
  store i32 %519, i32* %arrayidx444, align 4, !tbaa !7
  %521 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx445 = getelementptr inbounds i32, i32* %521, i32 38
  %522 = load i32, i32* %arrayidx445, align 4, !tbaa !7
  %523 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx446 = getelementptr inbounds i32, i32* %523, i32 38
  store i32 %522, i32* %arrayidx446, align 4, !tbaa !7
  %524 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx447 = getelementptr inbounds i32, i32* %524, i32 39
  %525 = load i32, i32* %arrayidx447, align 4, !tbaa !7
  %526 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx448 = getelementptr inbounds i32, i32* %526, i32 39
  store i32 %525, i32* %arrayidx448, align 4, !tbaa !7
  %527 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx449 = getelementptr inbounds i32, i32* %527, i32 32
  %528 = load i32, i32* %arrayidx449, align 4, !tbaa !7
  %sub450 = sub nsw i32 0, %528
  %529 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx451 = getelementptr inbounds i32, i32* %529, i32 40
  %530 = load i32, i32* %arrayidx451, align 4, !tbaa !7
  %531 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx452 = getelementptr inbounds i32, i32* %531, i32 32
  %532 = load i32, i32* %arrayidx452, align 4, !tbaa !7
  %533 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx453 = getelementptr inbounds i32, i32* %533, i32 55
  %534 = load i32, i32* %arrayidx453, align 4, !tbaa !7
  %535 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv454 = sext i8 %535 to i32
  %call455 = call i32 @half_btf(i32 %sub450, i32 %530, i32 %532, i32 %534, i32 %conv454)
  %536 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx456 = getelementptr inbounds i32, i32* %536, i32 40
  store i32 %call455, i32* %arrayidx456, align 4, !tbaa !7
  %537 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx457 = getelementptr inbounds i32, i32* %537, i32 32
  %538 = load i32, i32* %arrayidx457, align 4, !tbaa !7
  %sub458 = sub nsw i32 0, %538
  %539 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx459 = getelementptr inbounds i32, i32* %539, i32 41
  %540 = load i32, i32* %arrayidx459, align 4, !tbaa !7
  %541 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx460 = getelementptr inbounds i32, i32* %541, i32 32
  %542 = load i32, i32* %arrayidx460, align 4, !tbaa !7
  %543 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx461 = getelementptr inbounds i32, i32* %543, i32 54
  %544 = load i32, i32* %arrayidx461, align 4, !tbaa !7
  %545 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv462 = sext i8 %545 to i32
  %call463 = call i32 @half_btf(i32 %sub458, i32 %540, i32 %542, i32 %544, i32 %conv462)
  %546 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx464 = getelementptr inbounds i32, i32* %546, i32 41
  store i32 %call463, i32* %arrayidx464, align 4, !tbaa !7
  %547 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx465 = getelementptr inbounds i32, i32* %547, i32 32
  %548 = load i32, i32* %arrayidx465, align 4, !tbaa !7
  %sub466 = sub nsw i32 0, %548
  %549 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx467 = getelementptr inbounds i32, i32* %549, i32 42
  %550 = load i32, i32* %arrayidx467, align 4, !tbaa !7
  %551 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx468 = getelementptr inbounds i32, i32* %551, i32 32
  %552 = load i32, i32* %arrayidx468, align 4, !tbaa !7
  %553 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx469 = getelementptr inbounds i32, i32* %553, i32 53
  %554 = load i32, i32* %arrayidx469, align 4, !tbaa !7
  %555 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv470 = sext i8 %555 to i32
  %call471 = call i32 @half_btf(i32 %sub466, i32 %550, i32 %552, i32 %554, i32 %conv470)
  %556 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx472 = getelementptr inbounds i32, i32* %556, i32 42
  store i32 %call471, i32* %arrayidx472, align 4, !tbaa !7
  %557 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx473 = getelementptr inbounds i32, i32* %557, i32 32
  %558 = load i32, i32* %arrayidx473, align 4, !tbaa !7
  %sub474 = sub nsw i32 0, %558
  %559 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx475 = getelementptr inbounds i32, i32* %559, i32 43
  %560 = load i32, i32* %arrayidx475, align 4, !tbaa !7
  %561 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx476 = getelementptr inbounds i32, i32* %561, i32 32
  %562 = load i32, i32* %arrayidx476, align 4, !tbaa !7
  %563 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx477 = getelementptr inbounds i32, i32* %563, i32 52
  %564 = load i32, i32* %arrayidx477, align 4, !tbaa !7
  %565 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv478 = sext i8 %565 to i32
  %call479 = call i32 @half_btf(i32 %sub474, i32 %560, i32 %562, i32 %564, i32 %conv478)
  %566 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx480 = getelementptr inbounds i32, i32* %566, i32 43
  store i32 %call479, i32* %arrayidx480, align 4, !tbaa !7
  %567 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx481 = getelementptr inbounds i32, i32* %567, i32 32
  %568 = load i32, i32* %arrayidx481, align 4, !tbaa !7
  %sub482 = sub nsw i32 0, %568
  %569 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx483 = getelementptr inbounds i32, i32* %569, i32 44
  %570 = load i32, i32* %arrayidx483, align 4, !tbaa !7
  %571 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx484 = getelementptr inbounds i32, i32* %571, i32 32
  %572 = load i32, i32* %arrayidx484, align 4, !tbaa !7
  %573 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx485 = getelementptr inbounds i32, i32* %573, i32 51
  %574 = load i32, i32* %arrayidx485, align 4, !tbaa !7
  %575 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv486 = sext i8 %575 to i32
  %call487 = call i32 @half_btf(i32 %sub482, i32 %570, i32 %572, i32 %574, i32 %conv486)
  %576 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx488 = getelementptr inbounds i32, i32* %576, i32 44
  store i32 %call487, i32* %arrayidx488, align 4, !tbaa !7
  %577 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx489 = getelementptr inbounds i32, i32* %577, i32 32
  %578 = load i32, i32* %arrayidx489, align 4, !tbaa !7
  %sub490 = sub nsw i32 0, %578
  %579 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx491 = getelementptr inbounds i32, i32* %579, i32 45
  %580 = load i32, i32* %arrayidx491, align 4, !tbaa !7
  %581 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx492 = getelementptr inbounds i32, i32* %581, i32 32
  %582 = load i32, i32* %arrayidx492, align 4, !tbaa !7
  %583 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx493 = getelementptr inbounds i32, i32* %583, i32 50
  %584 = load i32, i32* %arrayidx493, align 4, !tbaa !7
  %585 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv494 = sext i8 %585 to i32
  %call495 = call i32 @half_btf(i32 %sub490, i32 %580, i32 %582, i32 %584, i32 %conv494)
  %586 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx496 = getelementptr inbounds i32, i32* %586, i32 45
  store i32 %call495, i32* %arrayidx496, align 4, !tbaa !7
  %587 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx497 = getelementptr inbounds i32, i32* %587, i32 32
  %588 = load i32, i32* %arrayidx497, align 4, !tbaa !7
  %sub498 = sub nsw i32 0, %588
  %589 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx499 = getelementptr inbounds i32, i32* %589, i32 46
  %590 = load i32, i32* %arrayidx499, align 4, !tbaa !7
  %591 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx500 = getelementptr inbounds i32, i32* %591, i32 32
  %592 = load i32, i32* %arrayidx500, align 4, !tbaa !7
  %593 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx501 = getelementptr inbounds i32, i32* %593, i32 49
  %594 = load i32, i32* %arrayidx501, align 4, !tbaa !7
  %595 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv502 = sext i8 %595 to i32
  %call503 = call i32 @half_btf(i32 %sub498, i32 %590, i32 %592, i32 %594, i32 %conv502)
  %596 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx504 = getelementptr inbounds i32, i32* %596, i32 46
  store i32 %call503, i32* %arrayidx504, align 4, !tbaa !7
  %597 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx505 = getelementptr inbounds i32, i32* %597, i32 32
  %598 = load i32, i32* %arrayidx505, align 4, !tbaa !7
  %sub506 = sub nsw i32 0, %598
  %599 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx507 = getelementptr inbounds i32, i32* %599, i32 47
  %600 = load i32, i32* %arrayidx507, align 4, !tbaa !7
  %601 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx508 = getelementptr inbounds i32, i32* %601, i32 32
  %602 = load i32, i32* %arrayidx508, align 4, !tbaa !7
  %603 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx509 = getelementptr inbounds i32, i32* %603, i32 48
  %604 = load i32, i32* %arrayidx509, align 4, !tbaa !7
  %605 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv510 = sext i8 %605 to i32
  %call511 = call i32 @half_btf(i32 %sub506, i32 %600, i32 %602, i32 %604, i32 %conv510)
  %606 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx512 = getelementptr inbounds i32, i32* %606, i32 47
  store i32 %call511, i32* %arrayidx512, align 4, !tbaa !7
  %607 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx513 = getelementptr inbounds i32, i32* %607, i32 32
  %608 = load i32, i32* %arrayidx513, align 4, !tbaa !7
  %609 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx514 = getelementptr inbounds i32, i32* %609, i32 48
  %610 = load i32, i32* %arrayidx514, align 4, !tbaa !7
  %611 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx515 = getelementptr inbounds i32, i32* %611, i32 32
  %612 = load i32, i32* %arrayidx515, align 4, !tbaa !7
  %613 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx516 = getelementptr inbounds i32, i32* %613, i32 47
  %614 = load i32, i32* %arrayidx516, align 4, !tbaa !7
  %615 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv517 = sext i8 %615 to i32
  %call518 = call i32 @half_btf(i32 %608, i32 %610, i32 %612, i32 %614, i32 %conv517)
  %616 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx519 = getelementptr inbounds i32, i32* %616, i32 48
  store i32 %call518, i32* %arrayidx519, align 4, !tbaa !7
  %617 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx520 = getelementptr inbounds i32, i32* %617, i32 32
  %618 = load i32, i32* %arrayidx520, align 4, !tbaa !7
  %619 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx521 = getelementptr inbounds i32, i32* %619, i32 49
  %620 = load i32, i32* %arrayidx521, align 4, !tbaa !7
  %621 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx522 = getelementptr inbounds i32, i32* %621, i32 32
  %622 = load i32, i32* %arrayidx522, align 4, !tbaa !7
  %623 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx523 = getelementptr inbounds i32, i32* %623, i32 46
  %624 = load i32, i32* %arrayidx523, align 4, !tbaa !7
  %625 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv524 = sext i8 %625 to i32
  %call525 = call i32 @half_btf(i32 %618, i32 %620, i32 %622, i32 %624, i32 %conv524)
  %626 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx526 = getelementptr inbounds i32, i32* %626, i32 49
  store i32 %call525, i32* %arrayidx526, align 4, !tbaa !7
  %627 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx527 = getelementptr inbounds i32, i32* %627, i32 32
  %628 = load i32, i32* %arrayidx527, align 4, !tbaa !7
  %629 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx528 = getelementptr inbounds i32, i32* %629, i32 50
  %630 = load i32, i32* %arrayidx528, align 4, !tbaa !7
  %631 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx529 = getelementptr inbounds i32, i32* %631, i32 32
  %632 = load i32, i32* %arrayidx529, align 4, !tbaa !7
  %633 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx530 = getelementptr inbounds i32, i32* %633, i32 45
  %634 = load i32, i32* %arrayidx530, align 4, !tbaa !7
  %635 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv531 = sext i8 %635 to i32
  %call532 = call i32 @half_btf(i32 %628, i32 %630, i32 %632, i32 %634, i32 %conv531)
  %636 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx533 = getelementptr inbounds i32, i32* %636, i32 50
  store i32 %call532, i32* %arrayidx533, align 4, !tbaa !7
  %637 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx534 = getelementptr inbounds i32, i32* %637, i32 32
  %638 = load i32, i32* %arrayidx534, align 4, !tbaa !7
  %639 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx535 = getelementptr inbounds i32, i32* %639, i32 51
  %640 = load i32, i32* %arrayidx535, align 4, !tbaa !7
  %641 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx536 = getelementptr inbounds i32, i32* %641, i32 32
  %642 = load i32, i32* %arrayidx536, align 4, !tbaa !7
  %643 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx537 = getelementptr inbounds i32, i32* %643, i32 44
  %644 = load i32, i32* %arrayidx537, align 4, !tbaa !7
  %645 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv538 = sext i8 %645 to i32
  %call539 = call i32 @half_btf(i32 %638, i32 %640, i32 %642, i32 %644, i32 %conv538)
  %646 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx540 = getelementptr inbounds i32, i32* %646, i32 51
  store i32 %call539, i32* %arrayidx540, align 4, !tbaa !7
  %647 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx541 = getelementptr inbounds i32, i32* %647, i32 32
  %648 = load i32, i32* %arrayidx541, align 4, !tbaa !7
  %649 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx542 = getelementptr inbounds i32, i32* %649, i32 52
  %650 = load i32, i32* %arrayidx542, align 4, !tbaa !7
  %651 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx543 = getelementptr inbounds i32, i32* %651, i32 32
  %652 = load i32, i32* %arrayidx543, align 4, !tbaa !7
  %653 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx544 = getelementptr inbounds i32, i32* %653, i32 43
  %654 = load i32, i32* %arrayidx544, align 4, !tbaa !7
  %655 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv545 = sext i8 %655 to i32
  %call546 = call i32 @half_btf(i32 %648, i32 %650, i32 %652, i32 %654, i32 %conv545)
  %656 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx547 = getelementptr inbounds i32, i32* %656, i32 52
  store i32 %call546, i32* %arrayidx547, align 4, !tbaa !7
  %657 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx548 = getelementptr inbounds i32, i32* %657, i32 32
  %658 = load i32, i32* %arrayidx548, align 4, !tbaa !7
  %659 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx549 = getelementptr inbounds i32, i32* %659, i32 53
  %660 = load i32, i32* %arrayidx549, align 4, !tbaa !7
  %661 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx550 = getelementptr inbounds i32, i32* %661, i32 32
  %662 = load i32, i32* %arrayidx550, align 4, !tbaa !7
  %663 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx551 = getelementptr inbounds i32, i32* %663, i32 42
  %664 = load i32, i32* %arrayidx551, align 4, !tbaa !7
  %665 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv552 = sext i8 %665 to i32
  %call553 = call i32 @half_btf(i32 %658, i32 %660, i32 %662, i32 %664, i32 %conv552)
  %666 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx554 = getelementptr inbounds i32, i32* %666, i32 53
  store i32 %call553, i32* %arrayidx554, align 4, !tbaa !7
  %667 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx555 = getelementptr inbounds i32, i32* %667, i32 32
  %668 = load i32, i32* %arrayidx555, align 4, !tbaa !7
  %669 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx556 = getelementptr inbounds i32, i32* %669, i32 54
  %670 = load i32, i32* %arrayidx556, align 4, !tbaa !7
  %671 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx557 = getelementptr inbounds i32, i32* %671, i32 32
  %672 = load i32, i32* %arrayidx557, align 4, !tbaa !7
  %673 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx558 = getelementptr inbounds i32, i32* %673, i32 41
  %674 = load i32, i32* %arrayidx558, align 4, !tbaa !7
  %675 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv559 = sext i8 %675 to i32
  %call560 = call i32 @half_btf(i32 %668, i32 %670, i32 %672, i32 %674, i32 %conv559)
  %676 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx561 = getelementptr inbounds i32, i32* %676, i32 54
  store i32 %call560, i32* %arrayidx561, align 4, !tbaa !7
  %677 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx562 = getelementptr inbounds i32, i32* %677, i32 32
  %678 = load i32, i32* %arrayidx562, align 4, !tbaa !7
  %679 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx563 = getelementptr inbounds i32, i32* %679, i32 55
  %680 = load i32, i32* %arrayidx563, align 4, !tbaa !7
  %681 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx564 = getelementptr inbounds i32, i32* %681, i32 32
  %682 = load i32, i32* %arrayidx564, align 4, !tbaa !7
  %683 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx565 = getelementptr inbounds i32, i32* %683, i32 40
  %684 = load i32, i32* %arrayidx565, align 4, !tbaa !7
  %685 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv566 = sext i8 %685 to i32
  %call567 = call i32 @half_btf(i32 %678, i32 %680, i32 %682, i32 %684, i32 %conv566)
  %686 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx568 = getelementptr inbounds i32, i32* %686, i32 55
  store i32 %call567, i32* %arrayidx568, align 4, !tbaa !7
  %687 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx569 = getelementptr inbounds i32, i32* %687, i32 56
  %688 = load i32, i32* %arrayidx569, align 4, !tbaa !7
  %689 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx570 = getelementptr inbounds i32, i32* %689, i32 56
  store i32 %688, i32* %arrayidx570, align 4, !tbaa !7
  %690 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx571 = getelementptr inbounds i32, i32* %690, i32 57
  %691 = load i32, i32* %arrayidx571, align 4, !tbaa !7
  %692 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx572 = getelementptr inbounds i32, i32* %692, i32 57
  store i32 %691, i32* %arrayidx572, align 4, !tbaa !7
  %693 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx573 = getelementptr inbounds i32, i32* %693, i32 58
  %694 = load i32, i32* %arrayidx573, align 4, !tbaa !7
  %695 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx574 = getelementptr inbounds i32, i32* %695, i32 58
  store i32 %694, i32* %arrayidx574, align 4, !tbaa !7
  %696 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx575 = getelementptr inbounds i32, i32* %696, i32 59
  %697 = load i32, i32* %arrayidx575, align 4, !tbaa !7
  %698 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx576 = getelementptr inbounds i32, i32* %698, i32 59
  store i32 %697, i32* %arrayidx576, align 4, !tbaa !7
  %699 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx577 = getelementptr inbounds i32, i32* %699, i32 60
  %700 = load i32, i32* %arrayidx577, align 4, !tbaa !7
  %701 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx578 = getelementptr inbounds i32, i32* %701, i32 60
  store i32 %700, i32* %arrayidx578, align 4, !tbaa !7
  %702 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx579 = getelementptr inbounds i32, i32* %702, i32 61
  %703 = load i32, i32* %arrayidx579, align 4, !tbaa !7
  %704 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx580 = getelementptr inbounds i32, i32* %704, i32 61
  store i32 %703, i32* %arrayidx580, align 4, !tbaa !7
  %705 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx581 = getelementptr inbounds i32, i32* %705, i32 62
  %706 = load i32, i32* %arrayidx581, align 4, !tbaa !7
  %707 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx582 = getelementptr inbounds i32, i32* %707, i32 62
  store i32 %706, i32* %arrayidx582, align 4, !tbaa !7
  %708 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx583 = getelementptr inbounds i32, i32* %708, i32 63
  %709 = load i32, i32* %arrayidx583, align 4, !tbaa !7
  %710 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx584 = getelementptr inbounds i32, i32* %710, i32 63
  store i32 %709, i32* %arrayidx584, align 4, !tbaa !7
  %711 = load i32, i32* %stage, align 4, !tbaa !7
  %712 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %713 = load i32*, i32** %bf1, align 4, !tbaa !2
  %714 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %715 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx585 = getelementptr inbounds i8, i8* %714, i32 %715
  %716 = load i8, i8* %arrayidx585, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %711, i32* %712, i32* %713, i32 64, i8 signext %716)
  %717 = load i32, i32* %stage, align 4, !tbaa !7
  %inc586 = add nsw i32 %717, 1
  store i32 %inc586, i32* %stage, align 4, !tbaa !7
  %718 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv587 = sext i8 %718 to i32
  %call588 = call i32* @cospi_arr(i32 %conv587)
  store i32* %call588, i32** %cospi, align 4, !tbaa !2
  %arraydecay589 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay589, i32** %bf0, align 4, !tbaa !2
  %719 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %719, i32** %bf1, align 4, !tbaa !2
  %720 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx590 = getelementptr inbounds i32, i32* %720, i32 0
  %721 = load i32, i32* %arrayidx590, align 4, !tbaa !7
  %722 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx591 = getelementptr inbounds i32, i32* %722, i32 15
  %723 = load i32, i32* %arrayidx591, align 4, !tbaa !7
  %add592 = add nsw i32 %721, %723
  %724 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx593 = getelementptr inbounds i32, i32* %724, i32 0
  store i32 %add592, i32* %arrayidx593, align 4, !tbaa !7
  %725 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx594 = getelementptr inbounds i32, i32* %725, i32 1
  %726 = load i32, i32* %arrayidx594, align 4, !tbaa !7
  %727 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx595 = getelementptr inbounds i32, i32* %727, i32 14
  %728 = load i32, i32* %arrayidx595, align 4, !tbaa !7
  %add596 = add nsw i32 %726, %728
  %729 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx597 = getelementptr inbounds i32, i32* %729, i32 1
  store i32 %add596, i32* %arrayidx597, align 4, !tbaa !7
  %730 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx598 = getelementptr inbounds i32, i32* %730, i32 2
  %731 = load i32, i32* %arrayidx598, align 4, !tbaa !7
  %732 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx599 = getelementptr inbounds i32, i32* %732, i32 13
  %733 = load i32, i32* %arrayidx599, align 4, !tbaa !7
  %add600 = add nsw i32 %731, %733
  %734 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx601 = getelementptr inbounds i32, i32* %734, i32 2
  store i32 %add600, i32* %arrayidx601, align 4, !tbaa !7
  %735 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx602 = getelementptr inbounds i32, i32* %735, i32 3
  %736 = load i32, i32* %arrayidx602, align 4, !tbaa !7
  %737 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx603 = getelementptr inbounds i32, i32* %737, i32 12
  %738 = load i32, i32* %arrayidx603, align 4, !tbaa !7
  %add604 = add nsw i32 %736, %738
  %739 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx605 = getelementptr inbounds i32, i32* %739, i32 3
  store i32 %add604, i32* %arrayidx605, align 4, !tbaa !7
  %740 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx606 = getelementptr inbounds i32, i32* %740, i32 4
  %741 = load i32, i32* %arrayidx606, align 4, !tbaa !7
  %742 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx607 = getelementptr inbounds i32, i32* %742, i32 11
  %743 = load i32, i32* %arrayidx607, align 4, !tbaa !7
  %add608 = add nsw i32 %741, %743
  %744 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx609 = getelementptr inbounds i32, i32* %744, i32 4
  store i32 %add608, i32* %arrayidx609, align 4, !tbaa !7
  %745 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx610 = getelementptr inbounds i32, i32* %745, i32 5
  %746 = load i32, i32* %arrayidx610, align 4, !tbaa !7
  %747 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx611 = getelementptr inbounds i32, i32* %747, i32 10
  %748 = load i32, i32* %arrayidx611, align 4, !tbaa !7
  %add612 = add nsw i32 %746, %748
  %749 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx613 = getelementptr inbounds i32, i32* %749, i32 5
  store i32 %add612, i32* %arrayidx613, align 4, !tbaa !7
  %750 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx614 = getelementptr inbounds i32, i32* %750, i32 6
  %751 = load i32, i32* %arrayidx614, align 4, !tbaa !7
  %752 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx615 = getelementptr inbounds i32, i32* %752, i32 9
  %753 = load i32, i32* %arrayidx615, align 4, !tbaa !7
  %add616 = add nsw i32 %751, %753
  %754 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx617 = getelementptr inbounds i32, i32* %754, i32 6
  store i32 %add616, i32* %arrayidx617, align 4, !tbaa !7
  %755 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx618 = getelementptr inbounds i32, i32* %755, i32 7
  %756 = load i32, i32* %arrayidx618, align 4, !tbaa !7
  %757 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx619 = getelementptr inbounds i32, i32* %757, i32 8
  %758 = load i32, i32* %arrayidx619, align 4, !tbaa !7
  %add620 = add nsw i32 %756, %758
  %759 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx621 = getelementptr inbounds i32, i32* %759, i32 7
  store i32 %add620, i32* %arrayidx621, align 4, !tbaa !7
  %760 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx622 = getelementptr inbounds i32, i32* %760, i32 8
  %761 = load i32, i32* %arrayidx622, align 4, !tbaa !7
  %sub623 = sub nsw i32 0, %761
  %762 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx624 = getelementptr inbounds i32, i32* %762, i32 7
  %763 = load i32, i32* %arrayidx624, align 4, !tbaa !7
  %add625 = add nsw i32 %sub623, %763
  %764 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx626 = getelementptr inbounds i32, i32* %764, i32 8
  store i32 %add625, i32* %arrayidx626, align 4, !tbaa !7
  %765 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx627 = getelementptr inbounds i32, i32* %765, i32 9
  %766 = load i32, i32* %arrayidx627, align 4, !tbaa !7
  %sub628 = sub nsw i32 0, %766
  %767 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx629 = getelementptr inbounds i32, i32* %767, i32 6
  %768 = load i32, i32* %arrayidx629, align 4, !tbaa !7
  %add630 = add nsw i32 %sub628, %768
  %769 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx631 = getelementptr inbounds i32, i32* %769, i32 9
  store i32 %add630, i32* %arrayidx631, align 4, !tbaa !7
  %770 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx632 = getelementptr inbounds i32, i32* %770, i32 10
  %771 = load i32, i32* %arrayidx632, align 4, !tbaa !7
  %sub633 = sub nsw i32 0, %771
  %772 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx634 = getelementptr inbounds i32, i32* %772, i32 5
  %773 = load i32, i32* %arrayidx634, align 4, !tbaa !7
  %add635 = add nsw i32 %sub633, %773
  %774 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx636 = getelementptr inbounds i32, i32* %774, i32 10
  store i32 %add635, i32* %arrayidx636, align 4, !tbaa !7
  %775 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx637 = getelementptr inbounds i32, i32* %775, i32 11
  %776 = load i32, i32* %arrayidx637, align 4, !tbaa !7
  %sub638 = sub nsw i32 0, %776
  %777 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx639 = getelementptr inbounds i32, i32* %777, i32 4
  %778 = load i32, i32* %arrayidx639, align 4, !tbaa !7
  %add640 = add nsw i32 %sub638, %778
  %779 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx641 = getelementptr inbounds i32, i32* %779, i32 11
  store i32 %add640, i32* %arrayidx641, align 4, !tbaa !7
  %780 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx642 = getelementptr inbounds i32, i32* %780, i32 12
  %781 = load i32, i32* %arrayidx642, align 4, !tbaa !7
  %sub643 = sub nsw i32 0, %781
  %782 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx644 = getelementptr inbounds i32, i32* %782, i32 3
  %783 = load i32, i32* %arrayidx644, align 4, !tbaa !7
  %add645 = add nsw i32 %sub643, %783
  %784 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx646 = getelementptr inbounds i32, i32* %784, i32 12
  store i32 %add645, i32* %arrayidx646, align 4, !tbaa !7
  %785 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx647 = getelementptr inbounds i32, i32* %785, i32 13
  %786 = load i32, i32* %arrayidx647, align 4, !tbaa !7
  %sub648 = sub nsw i32 0, %786
  %787 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx649 = getelementptr inbounds i32, i32* %787, i32 2
  %788 = load i32, i32* %arrayidx649, align 4, !tbaa !7
  %add650 = add nsw i32 %sub648, %788
  %789 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx651 = getelementptr inbounds i32, i32* %789, i32 13
  store i32 %add650, i32* %arrayidx651, align 4, !tbaa !7
  %790 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx652 = getelementptr inbounds i32, i32* %790, i32 14
  %791 = load i32, i32* %arrayidx652, align 4, !tbaa !7
  %sub653 = sub nsw i32 0, %791
  %792 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx654 = getelementptr inbounds i32, i32* %792, i32 1
  %793 = load i32, i32* %arrayidx654, align 4, !tbaa !7
  %add655 = add nsw i32 %sub653, %793
  %794 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx656 = getelementptr inbounds i32, i32* %794, i32 14
  store i32 %add655, i32* %arrayidx656, align 4, !tbaa !7
  %795 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx657 = getelementptr inbounds i32, i32* %795, i32 15
  %796 = load i32, i32* %arrayidx657, align 4, !tbaa !7
  %sub658 = sub nsw i32 0, %796
  %797 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx659 = getelementptr inbounds i32, i32* %797, i32 0
  %798 = load i32, i32* %arrayidx659, align 4, !tbaa !7
  %add660 = add nsw i32 %sub658, %798
  %799 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx661 = getelementptr inbounds i32, i32* %799, i32 15
  store i32 %add660, i32* %arrayidx661, align 4, !tbaa !7
  %800 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx662 = getelementptr inbounds i32, i32* %800, i32 16
  %801 = load i32, i32* %arrayidx662, align 4, !tbaa !7
  %802 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx663 = getelementptr inbounds i32, i32* %802, i32 16
  store i32 %801, i32* %arrayidx663, align 4, !tbaa !7
  %803 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx664 = getelementptr inbounds i32, i32* %803, i32 17
  %804 = load i32, i32* %arrayidx664, align 4, !tbaa !7
  %805 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx665 = getelementptr inbounds i32, i32* %805, i32 17
  store i32 %804, i32* %arrayidx665, align 4, !tbaa !7
  %806 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx666 = getelementptr inbounds i32, i32* %806, i32 18
  %807 = load i32, i32* %arrayidx666, align 4, !tbaa !7
  %808 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx667 = getelementptr inbounds i32, i32* %808, i32 18
  store i32 %807, i32* %arrayidx667, align 4, !tbaa !7
  %809 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx668 = getelementptr inbounds i32, i32* %809, i32 19
  %810 = load i32, i32* %arrayidx668, align 4, !tbaa !7
  %811 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx669 = getelementptr inbounds i32, i32* %811, i32 19
  store i32 %810, i32* %arrayidx669, align 4, !tbaa !7
  %812 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx670 = getelementptr inbounds i32, i32* %812, i32 32
  %813 = load i32, i32* %arrayidx670, align 4, !tbaa !7
  %sub671 = sub nsw i32 0, %813
  %814 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx672 = getelementptr inbounds i32, i32* %814, i32 20
  %815 = load i32, i32* %arrayidx672, align 4, !tbaa !7
  %816 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx673 = getelementptr inbounds i32, i32* %816, i32 32
  %817 = load i32, i32* %arrayidx673, align 4, !tbaa !7
  %818 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx674 = getelementptr inbounds i32, i32* %818, i32 27
  %819 = load i32, i32* %arrayidx674, align 4, !tbaa !7
  %820 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv675 = sext i8 %820 to i32
  %call676 = call i32 @half_btf(i32 %sub671, i32 %815, i32 %817, i32 %819, i32 %conv675)
  %821 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx677 = getelementptr inbounds i32, i32* %821, i32 20
  store i32 %call676, i32* %arrayidx677, align 4, !tbaa !7
  %822 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx678 = getelementptr inbounds i32, i32* %822, i32 32
  %823 = load i32, i32* %arrayidx678, align 4, !tbaa !7
  %sub679 = sub nsw i32 0, %823
  %824 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx680 = getelementptr inbounds i32, i32* %824, i32 21
  %825 = load i32, i32* %arrayidx680, align 4, !tbaa !7
  %826 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx681 = getelementptr inbounds i32, i32* %826, i32 32
  %827 = load i32, i32* %arrayidx681, align 4, !tbaa !7
  %828 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx682 = getelementptr inbounds i32, i32* %828, i32 26
  %829 = load i32, i32* %arrayidx682, align 4, !tbaa !7
  %830 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv683 = sext i8 %830 to i32
  %call684 = call i32 @half_btf(i32 %sub679, i32 %825, i32 %827, i32 %829, i32 %conv683)
  %831 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx685 = getelementptr inbounds i32, i32* %831, i32 21
  store i32 %call684, i32* %arrayidx685, align 4, !tbaa !7
  %832 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx686 = getelementptr inbounds i32, i32* %832, i32 32
  %833 = load i32, i32* %arrayidx686, align 4, !tbaa !7
  %sub687 = sub nsw i32 0, %833
  %834 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx688 = getelementptr inbounds i32, i32* %834, i32 22
  %835 = load i32, i32* %arrayidx688, align 4, !tbaa !7
  %836 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx689 = getelementptr inbounds i32, i32* %836, i32 32
  %837 = load i32, i32* %arrayidx689, align 4, !tbaa !7
  %838 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx690 = getelementptr inbounds i32, i32* %838, i32 25
  %839 = load i32, i32* %arrayidx690, align 4, !tbaa !7
  %840 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv691 = sext i8 %840 to i32
  %call692 = call i32 @half_btf(i32 %sub687, i32 %835, i32 %837, i32 %839, i32 %conv691)
  %841 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx693 = getelementptr inbounds i32, i32* %841, i32 22
  store i32 %call692, i32* %arrayidx693, align 4, !tbaa !7
  %842 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx694 = getelementptr inbounds i32, i32* %842, i32 32
  %843 = load i32, i32* %arrayidx694, align 4, !tbaa !7
  %sub695 = sub nsw i32 0, %843
  %844 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx696 = getelementptr inbounds i32, i32* %844, i32 23
  %845 = load i32, i32* %arrayidx696, align 4, !tbaa !7
  %846 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx697 = getelementptr inbounds i32, i32* %846, i32 32
  %847 = load i32, i32* %arrayidx697, align 4, !tbaa !7
  %848 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx698 = getelementptr inbounds i32, i32* %848, i32 24
  %849 = load i32, i32* %arrayidx698, align 4, !tbaa !7
  %850 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv699 = sext i8 %850 to i32
  %call700 = call i32 @half_btf(i32 %sub695, i32 %845, i32 %847, i32 %849, i32 %conv699)
  %851 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx701 = getelementptr inbounds i32, i32* %851, i32 23
  store i32 %call700, i32* %arrayidx701, align 4, !tbaa !7
  %852 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx702 = getelementptr inbounds i32, i32* %852, i32 32
  %853 = load i32, i32* %arrayidx702, align 4, !tbaa !7
  %854 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx703 = getelementptr inbounds i32, i32* %854, i32 24
  %855 = load i32, i32* %arrayidx703, align 4, !tbaa !7
  %856 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx704 = getelementptr inbounds i32, i32* %856, i32 32
  %857 = load i32, i32* %arrayidx704, align 4, !tbaa !7
  %858 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx705 = getelementptr inbounds i32, i32* %858, i32 23
  %859 = load i32, i32* %arrayidx705, align 4, !tbaa !7
  %860 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv706 = sext i8 %860 to i32
  %call707 = call i32 @half_btf(i32 %853, i32 %855, i32 %857, i32 %859, i32 %conv706)
  %861 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx708 = getelementptr inbounds i32, i32* %861, i32 24
  store i32 %call707, i32* %arrayidx708, align 4, !tbaa !7
  %862 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx709 = getelementptr inbounds i32, i32* %862, i32 32
  %863 = load i32, i32* %arrayidx709, align 4, !tbaa !7
  %864 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx710 = getelementptr inbounds i32, i32* %864, i32 25
  %865 = load i32, i32* %arrayidx710, align 4, !tbaa !7
  %866 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx711 = getelementptr inbounds i32, i32* %866, i32 32
  %867 = load i32, i32* %arrayidx711, align 4, !tbaa !7
  %868 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx712 = getelementptr inbounds i32, i32* %868, i32 22
  %869 = load i32, i32* %arrayidx712, align 4, !tbaa !7
  %870 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv713 = sext i8 %870 to i32
  %call714 = call i32 @half_btf(i32 %863, i32 %865, i32 %867, i32 %869, i32 %conv713)
  %871 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx715 = getelementptr inbounds i32, i32* %871, i32 25
  store i32 %call714, i32* %arrayidx715, align 4, !tbaa !7
  %872 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx716 = getelementptr inbounds i32, i32* %872, i32 32
  %873 = load i32, i32* %arrayidx716, align 4, !tbaa !7
  %874 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx717 = getelementptr inbounds i32, i32* %874, i32 26
  %875 = load i32, i32* %arrayidx717, align 4, !tbaa !7
  %876 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx718 = getelementptr inbounds i32, i32* %876, i32 32
  %877 = load i32, i32* %arrayidx718, align 4, !tbaa !7
  %878 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx719 = getelementptr inbounds i32, i32* %878, i32 21
  %879 = load i32, i32* %arrayidx719, align 4, !tbaa !7
  %880 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv720 = sext i8 %880 to i32
  %call721 = call i32 @half_btf(i32 %873, i32 %875, i32 %877, i32 %879, i32 %conv720)
  %881 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx722 = getelementptr inbounds i32, i32* %881, i32 26
  store i32 %call721, i32* %arrayidx722, align 4, !tbaa !7
  %882 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx723 = getelementptr inbounds i32, i32* %882, i32 32
  %883 = load i32, i32* %arrayidx723, align 4, !tbaa !7
  %884 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx724 = getelementptr inbounds i32, i32* %884, i32 27
  %885 = load i32, i32* %arrayidx724, align 4, !tbaa !7
  %886 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx725 = getelementptr inbounds i32, i32* %886, i32 32
  %887 = load i32, i32* %arrayidx725, align 4, !tbaa !7
  %888 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx726 = getelementptr inbounds i32, i32* %888, i32 20
  %889 = load i32, i32* %arrayidx726, align 4, !tbaa !7
  %890 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv727 = sext i8 %890 to i32
  %call728 = call i32 @half_btf(i32 %883, i32 %885, i32 %887, i32 %889, i32 %conv727)
  %891 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx729 = getelementptr inbounds i32, i32* %891, i32 27
  store i32 %call728, i32* %arrayidx729, align 4, !tbaa !7
  %892 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx730 = getelementptr inbounds i32, i32* %892, i32 28
  %893 = load i32, i32* %arrayidx730, align 4, !tbaa !7
  %894 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx731 = getelementptr inbounds i32, i32* %894, i32 28
  store i32 %893, i32* %arrayidx731, align 4, !tbaa !7
  %895 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx732 = getelementptr inbounds i32, i32* %895, i32 29
  %896 = load i32, i32* %arrayidx732, align 4, !tbaa !7
  %897 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx733 = getelementptr inbounds i32, i32* %897, i32 29
  store i32 %896, i32* %arrayidx733, align 4, !tbaa !7
  %898 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx734 = getelementptr inbounds i32, i32* %898, i32 30
  %899 = load i32, i32* %arrayidx734, align 4, !tbaa !7
  %900 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx735 = getelementptr inbounds i32, i32* %900, i32 30
  store i32 %899, i32* %arrayidx735, align 4, !tbaa !7
  %901 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx736 = getelementptr inbounds i32, i32* %901, i32 31
  %902 = load i32, i32* %arrayidx736, align 4, !tbaa !7
  %903 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx737 = getelementptr inbounds i32, i32* %903, i32 31
  store i32 %902, i32* %arrayidx737, align 4, !tbaa !7
  %904 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx738 = getelementptr inbounds i32, i32* %904, i32 32
  %905 = load i32, i32* %arrayidx738, align 4, !tbaa !7
  %906 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx739 = getelementptr inbounds i32, i32* %906, i32 47
  %907 = load i32, i32* %arrayidx739, align 4, !tbaa !7
  %add740 = add nsw i32 %905, %907
  %908 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx741 = getelementptr inbounds i32, i32* %908, i32 32
  store i32 %add740, i32* %arrayidx741, align 4, !tbaa !7
  %909 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx742 = getelementptr inbounds i32, i32* %909, i32 33
  %910 = load i32, i32* %arrayidx742, align 4, !tbaa !7
  %911 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx743 = getelementptr inbounds i32, i32* %911, i32 46
  %912 = load i32, i32* %arrayidx743, align 4, !tbaa !7
  %add744 = add nsw i32 %910, %912
  %913 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx745 = getelementptr inbounds i32, i32* %913, i32 33
  store i32 %add744, i32* %arrayidx745, align 4, !tbaa !7
  %914 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx746 = getelementptr inbounds i32, i32* %914, i32 34
  %915 = load i32, i32* %arrayidx746, align 4, !tbaa !7
  %916 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx747 = getelementptr inbounds i32, i32* %916, i32 45
  %917 = load i32, i32* %arrayidx747, align 4, !tbaa !7
  %add748 = add nsw i32 %915, %917
  %918 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx749 = getelementptr inbounds i32, i32* %918, i32 34
  store i32 %add748, i32* %arrayidx749, align 4, !tbaa !7
  %919 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx750 = getelementptr inbounds i32, i32* %919, i32 35
  %920 = load i32, i32* %arrayidx750, align 4, !tbaa !7
  %921 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx751 = getelementptr inbounds i32, i32* %921, i32 44
  %922 = load i32, i32* %arrayidx751, align 4, !tbaa !7
  %add752 = add nsw i32 %920, %922
  %923 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx753 = getelementptr inbounds i32, i32* %923, i32 35
  store i32 %add752, i32* %arrayidx753, align 4, !tbaa !7
  %924 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx754 = getelementptr inbounds i32, i32* %924, i32 36
  %925 = load i32, i32* %arrayidx754, align 4, !tbaa !7
  %926 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx755 = getelementptr inbounds i32, i32* %926, i32 43
  %927 = load i32, i32* %arrayidx755, align 4, !tbaa !7
  %add756 = add nsw i32 %925, %927
  %928 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx757 = getelementptr inbounds i32, i32* %928, i32 36
  store i32 %add756, i32* %arrayidx757, align 4, !tbaa !7
  %929 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx758 = getelementptr inbounds i32, i32* %929, i32 37
  %930 = load i32, i32* %arrayidx758, align 4, !tbaa !7
  %931 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx759 = getelementptr inbounds i32, i32* %931, i32 42
  %932 = load i32, i32* %arrayidx759, align 4, !tbaa !7
  %add760 = add nsw i32 %930, %932
  %933 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx761 = getelementptr inbounds i32, i32* %933, i32 37
  store i32 %add760, i32* %arrayidx761, align 4, !tbaa !7
  %934 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx762 = getelementptr inbounds i32, i32* %934, i32 38
  %935 = load i32, i32* %arrayidx762, align 4, !tbaa !7
  %936 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx763 = getelementptr inbounds i32, i32* %936, i32 41
  %937 = load i32, i32* %arrayidx763, align 4, !tbaa !7
  %add764 = add nsw i32 %935, %937
  %938 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx765 = getelementptr inbounds i32, i32* %938, i32 38
  store i32 %add764, i32* %arrayidx765, align 4, !tbaa !7
  %939 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx766 = getelementptr inbounds i32, i32* %939, i32 39
  %940 = load i32, i32* %arrayidx766, align 4, !tbaa !7
  %941 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx767 = getelementptr inbounds i32, i32* %941, i32 40
  %942 = load i32, i32* %arrayidx767, align 4, !tbaa !7
  %add768 = add nsw i32 %940, %942
  %943 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx769 = getelementptr inbounds i32, i32* %943, i32 39
  store i32 %add768, i32* %arrayidx769, align 4, !tbaa !7
  %944 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx770 = getelementptr inbounds i32, i32* %944, i32 40
  %945 = load i32, i32* %arrayidx770, align 4, !tbaa !7
  %sub771 = sub nsw i32 0, %945
  %946 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx772 = getelementptr inbounds i32, i32* %946, i32 39
  %947 = load i32, i32* %arrayidx772, align 4, !tbaa !7
  %add773 = add nsw i32 %sub771, %947
  %948 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx774 = getelementptr inbounds i32, i32* %948, i32 40
  store i32 %add773, i32* %arrayidx774, align 4, !tbaa !7
  %949 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx775 = getelementptr inbounds i32, i32* %949, i32 41
  %950 = load i32, i32* %arrayidx775, align 4, !tbaa !7
  %sub776 = sub nsw i32 0, %950
  %951 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx777 = getelementptr inbounds i32, i32* %951, i32 38
  %952 = load i32, i32* %arrayidx777, align 4, !tbaa !7
  %add778 = add nsw i32 %sub776, %952
  %953 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx779 = getelementptr inbounds i32, i32* %953, i32 41
  store i32 %add778, i32* %arrayidx779, align 4, !tbaa !7
  %954 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx780 = getelementptr inbounds i32, i32* %954, i32 42
  %955 = load i32, i32* %arrayidx780, align 4, !tbaa !7
  %sub781 = sub nsw i32 0, %955
  %956 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx782 = getelementptr inbounds i32, i32* %956, i32 37
  %957 = load i32, i32* %arrayidx782, align 4, !tbaa !7
  %add783 = add nsw i32 %sub781, %957
  %958 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx784 = getelementptr inbounds i32, i32* %958, i32 42
  store i32 %add783, i32* %arrayidx784, align 4, !tbaa !7
  %959 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx785 = getelementptr inbounds i32, i32* %959, i32 43
  %960 = load i32, i32* %arrayidx785, align 4, !tbaa !7
  %sub786 = sub nsw i32 0, %960
  %961 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx787 = getelementptr inbounds i32, i32* %961, i32 36
  %962 = load i32, i32* %arrayidx787, align 4, !tbaa !7
  %add788 = add nsw i32 %sub786, %962
  %963 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx789 = getelementptr inbounds i32, i32* %963, i32 43
  store i32 %add788, i32* %arrayidx789, align 4, !tbaa !7
  %964 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx790 = getelementptr inbounds i32, i32* %964, i32 44
  %965 = load i32, i32* %arrayidx790, align 4, !tbaa !7
  %sub791 = sub nsw i32 0, %965
  %966 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx792 = getelementptr inbounds i32, i32* %966, i32 35
  %967 = load i32, i32* %arrayidx792, align 4, !tbaa !7
  %add793 = add nsw i32 %sub791, %967
  %968 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx794 = getelementptr inbounds i32, i32* %968, i32 44
  store i32 %add793, i32* %arrayidx794, align 4, !tbaa !7
  %969 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx795 = getelementptr inbounds i32, i32* %969, i32 45
  %970 = load i32, i32* %arrayidx795, align 4, !tbaa !7
  %sub796 = sub nsw i32 0, %970
  %971 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx797 = getelementptr inbounds i32, i32* %971, i32 34
  %972 = load i32, i32* %arrayidx797, align 4, !tbaa !7
  %add798 = add nsw i32 %sub796, %972
  %973 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx799 = getelementptr inbounds i32, i32* %973, i32 45
  store i32 %add798, i32* %arrayidx799, align 4, !tbaa !7
  %974 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx800 = getelementptr inbounds i32, i32* %974, i32 46
  %975 = load i32, i32* %arrayidx800, align 4, !tbaa !7
  %sub801 = sub nsw i32 0, %975
  %976 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx802 = getelementptr inbounds i32, i32* %976, i32 33
  %977 = load i32, i32* %arrayidx802, align 4, !tbaa !7
  %add803 = add nsw i32 %sub801, %977
  %978 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx804 = getelementptr inbounds i32, i32* %978, i32 46
  store i32 %add803, i32* %arrayidx804, align 4, !tbaa !7
  %979 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx805 = getelementptr inbounds i32, i32* %979, i32 47
  %980 = load i32, i32* %arrayidx805, align 4, !tbaa !7
  %sub806 = sub nsw i32 0, %980
  %981 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx807 = getelementptr inbounds i32, i32* %981, i32 32
  %982 = load i32, i32* %arrayidx807, align 4, !tbaa !7
  %add808 = add nsw i32 %sub806, %982
  %983 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx809 = getelementptr inbounds i32, i32* %983, i32 47
  store i32 %add808, i32* %arrayidx809, align 4, !tbaa !7
  %984 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx810 = getelementptr inbounds i32, i32* %984, i32 48
  %985 = load i32, i32* %arrayidx810, align 4, !tbaa !7
  %sub811 = sub nsw i32 0, %985
  %986 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx812 = getelementptr inbounds i32, i32* %986, i32 63
  %987 = load i32, i32* %arrayidx812, align 4, !tbaa !7
  %add813 = add nsw i32 %sub811, %987
  %988 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx814 = getelementptr inbounds i32, i32* %988, i32 48
  store i32 %add813, i32* %arrayidx814, align 4, !tbaa !7
  %989 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx815 = getelementptr inbounds i32, i32* %989, i32 49
  %990 = load i32, i32* %arrayidx815, align 4, !tbaa !7
  %sub816 = sub nsw i32 0, %990
  %991 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx817 = getelementptr inbounds i32, i32* %991, i32 62
  %992 = load i32, i32* %arrayidx817, align 4, !tbaa !7
  %add818 = add nsw i32 %sub816, %992
  %993 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx819 = getelementptr inbounds i32, i32* %993, i32 49
  store i32 %add818, i32* %arrayidx819, align 4, !tbaa !7
  %994 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx820 = getelementptr inbounds i32, i32* %994, i32 50
  %995 = load i32, i32* %arrayidx820, align 4, !tbaa !7
  %sub821 = sub nsw i32 0, %995
  %996 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx822 = getelementptr inbounds i32, i32* %996, i32 61
  %997 = load i32, i32* %arrayidx822, align 4, !tbaa !7
  %add823 = add nsw i32 %sub821, %997
  %998 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx824 = getelementptr inbounds i32, i32* %998, i32 50
  store i32 %add823, i32* %arrayidx824, align 4, !tbaa !7
  %999 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx825 = getelementptr inbounds i32, i32* %999, i32 51
  %1000 = load i32, i32* %arrayidx825, align 4, !tbaa !7
  %sub826 = sub nsw i32 0, %1000
  %1001 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx827 = getelementptr inbounds i32, i32* %1001, i32 60
  %1002 = load i32, i32* %arrayidx827, align 4, !tbaa !7
  %add828 = add nsw i32 %sub826, %1002
  %1003 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx829 = getelementptr inbounds i32, i32* %1003, i32 51
  store i32 %add828, i32* %arrayidx829, align 4, !tbaa !7
  %1004 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx830 = getelementptr inbounds i32, i32* %1004, i32 52
  %1005 = load i32, i32* %arrayidx830, align 4, !tbaa !7
  %sub831 = sub nsw i32 0, %1005
  %1006 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx832 = getelementptr inbounds i32, i32* %1006, i32 59
  %1007 = load i32, i32* %arrayidx832, align 4, !tbaa !7
  %add833 = add nsw i32 %sub831, %1007
  %1008 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx834 = getelementptr inbounds i32, i32* %1008, i32 52
  store i32 %add833, i32* %arrayidx834, align 4, !tbaa !7
  %1009 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx835 = getelementptr inbounds i32, i32* %1009, i32 53
  %1010 = load i32, i32* %arrayidx835, align 4, !tbaa !7
  %sub836 = sub nsw i32 0, %1010
  %1011 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx837 = getelementptr inbounds i32, i32* %1011, i32 58
  %1012 = load i32, i32* %arrayidx837, align 4, !tbaa !7
  %add838 = add nsw i32 %sub836, %1012
  %1013 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx839 = getelementptr inbounds i32, i32* %1013, i32 53
  store i32 %add838, i32* %arrayidx839, align 4, !tbaa !7
  %1014 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx840 = getelementptr inbounds i32, i32* %1014, i32 54
  %1015 = load i32, i32* %arrayidx840, align 4, !tbaa !7
  %sub841 = sub nsw i32 0, %1015
  %1016 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx842 = getelementptr inbounds i32, i32* %1016, i32 57
  %1017 = load i32, i32* %arrayidx842, align 4, !tbaa !7
  %add843 = add nsw i32 %sub841, %1017
  %1018 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx844 = getelementptr inbounds i32, i32* %1018, i32 54
  store i32 %add843, i32* %arrayidx844, align 4, !tbaa !7
  %1019 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx845 = getelementptr inbounds i32, i32* %1019, i32 55
  %1020 = load i32, i32* %arrayidx845, align 4, !tbaa !7
  %sub846 = sub nsw i32 0, %1020
  %1021 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx847 = getelementptr inbounds i32, i32* %1021, i32 56
  %1022 = load i32, i32* %arrayidx847, align 4, !tbaa !7
  %add848 = add nsw i32 %sub846, %1022
  %1023 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx849 = getelementptr inbounds i32, i32* %1023, i32 55
  store i32 %add848, i32* %arrayidx849, align 4, !tbaa !7
  %1024 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx850 = getelementptr inbounds i32, i32* %1024, i32 56
  %1025 = load i32, i32* %arrayidx850, align 4, !tbaa !7
  %1026 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx851 = getelementptr inbounds i32, i32* %1026, i32 55
  %1027 = load i32, i32* %arrayidx851, align 4, !tbaa !7
  %add852 = add nsw i32 %1025, %1027
  %1028 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx853 = getelementptr inbounds i32, i32* %1028, i32 56
  store i32 %add852, i32* %arrayidx853, align 4, !tbaa !7
  %1029 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx854 = getelementptr inbounds i32, i32* %1029, i32 57
  %1030 = load i32, i32* %arrayidx854, align 4, !tbaa !7
  %1031 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx855 = getelementptr inbounds i32, i32* %1031, i32 54
  %1032 = load i32, i32* %arrayidx855, align 4, !tbaa !7
  %add856 = add nsw i32 %1030, %1032
  %1033 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx857 = getelementptr inbounds i32, i32* %1033, i32 57
  store i32 %add856, i32* %arrayidx857, align 4, !tbaa !7
  %1034 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx858 = getelementptr inbounds i32, i32* %1034, i32 58
  %1035 = load i32, i32* %arrayidx858, align 4, !tbaa !7
  %1036 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx859 = getelementptr inbounds i32, i32* %1036, i32 53
  %1037 = load i32, i32* %arrayidx859, align 4, !tbaa !7
  %add860 = add nsw i32 %1035, %1037
  %1038 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx861 = getelementptr inbounds i32, i32* %1038, i32 58
  store i32 %add860, i32* %arrayidx861, align 4, !tbaa !7
  %1039 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx862 = getelementptr inbounds i32, i32* %1039, i32 59
  %1040 = load i32, i32* %arrayidx862, align 4, !tbaa !7
  %1041 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx863 = getelementptr inbounds i32, i32* %1041, i32 52
  %1042 = load i32, i32* %arrayidx863, align 4, !tbaa !7
  %add864 = add nsw i32 %1040, %1042
  %1043 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx865 = getelementptr inbounds i32, i32* %1043, i32 59
  store i32 %add864, i32* %arrayidx865, align 4, !tbaa !7
  %1044 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx866 = getelementptr inbounds i32, i32* %1044, i32 60
  %1045 = load i32, i32* %arrayidx866, align 4, !tbaa !7
  %1046 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx867 = getelementptr inbounds i32, i32* %1046, i32 51
  %1047 = load i32, i32* %arrayidx867, align 4, !tbaa !7
  %add868 = add nsw i32 %1045, %1047
  %1048 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx869 = getelementptr inbounds i32, i32* %1048, i32 60
  store i32 %add868, i32* %arrayidx869, align 4, !tbaa !7
  %1049 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx870 = getelementptr inbounds i32, i32* %1049, i32 61
  %1050 = load i32, i32* %arrayidx870, align 4, !tbaa !7
  %1051 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx871 = getelementptr inbounds i32, i32* %1051, i32 50
  %1052 = load i32, i32* %arrayidx871, align 4, !tbaa !7
  %add872 = add nsw i32 %1050, %1052
  %1053 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx873 = getelementptr inbounds i32, i32* %1053, i32 61
  store i32 %add872, i32* %arrayidx873, align 4, !tbaa !7
  %1054 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx874 = getelementptr inbounds i32, i32* %1054, i32 62
  %1055 = load i32, i32* %arrayidx874, align 4, !tbaa !7
  %1056 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx875 = getelementptr inbounds i32, i32* %1056, i32 49
  %1057 = load i32, i32* %arrayidx875, align 4, !tbaa !7
  %add876 = add nsw i32 %1055, %1057
  %1058 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx877 = getelementptr inbounds i32, i32* %1058, i32 62
  store i32 %add876, i32* %arrayidx877, align 4, !tbaa !7
  %1059 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx878 = getelementptr inbounds i32, i32* %1059, i32 63
  %1060 = load i32, i32* %arrayidx878, align 4, !tbaa !7
  %1061 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx879 = getelementptr inbounds i32, i32* %1061, i32 48
  %1062 = load i32, i32* %arrayidx879, align 4, !tbaa !7
  %add880 = add nsw i32 %1060, %1062
  %1063 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx881 = getelementptr inbounds i32, i32* %1063, i32 63
  store i32 %add880, i32* %arrayidx881, align 4, !tbaa !7
  %1064 = load i32, i32* %stage, align 4, !tbaa !7
  %1065 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %1066 = load i32*, i32** %bf1, align 4, !tbaa !2
  %1067 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %1068 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx882 = getelementptr inbounds i8, i8* %1067, i32 %1068
  %1069 = load i8, i8* %arrayidx882, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %1064, i32* %1065, i32* %1066, i32 64, i8 signext %1069)
  %1070 = load i32, i32* %stage, align 4, !tbaa !7
  %inc883 = add nsw i32 %1070, 1
  store i32 %inc883, i32* %stage, align 4, !tbaa !7
  %1071 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv884 = sext i8 %1071 to i32
  %call885 = call i32* @cospi_arr(i32 %conv884)
  store i32* %call885, i32** %cospi, align 4, !tbaa !2
  %1072 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %1072, i32** %bf0, align 4, !tbaa !2
  %arraydecay886 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay886, i32** %bf1, align 4, !tbaa !2
  %1073 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx887 = getelementptr inbounds i32, i32* %1073, i32 0
  %1074 = load i32, i32* %arrayidx887, align 4, !tbaa !7
  %1075 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx888 = getelementptr inbounds i32, i32* %1075, i32 7
  %1076 = load i32, i32* %arrayidx888, align 4, !tbaa !7
  %add889 = add nsw i32 %1074, %1076
  %1077 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx890 = getelementptr inbounds i32, i32* %1077, i32 0
  store i32 %add889, i32* %arrayidx890, align 4, !tbaa !7
  %1078 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx891 = getelementptr inbounds i32, i32* %1078, i32 1
  %1079 = load i32, i32* %arrayidx891, align 4, !tbaa !7
  %1080 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx892 = getelementptr inbounds i32, i32* %1080, i32 6
  %1081 = load i32, i32* %arrayidx892, align 4, !tbaa !7
  %add893 = add nsw i32 %1079, %1081
  %1082 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx894 = getelementptr inbounds i32, i32* %1082, i32 1
  store i32 %add893, i32* %arrayidx894, align 4, !tbaa !7
  %1083 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx895 = getelementptr inbounds i32, i32* %1083, i32 2
  %1084 = load i32, i32* %arrayidx895, align 4, !tbaa !7
  %1085 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx896 = getelementptr inbounds i32, i32* %1085, i32 5
  %1086 = load i32, i32* %arrayidx896, align 4, !tbaa !7
  %add897 = add nsw i32 %1084, %1086
  %1087 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx898 = getelementptr inbounds i32, i32* %1087, i32 2
  store i32 %add897, i32* %arrayidx898, align 4, !tbaa !7
  %1088 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx899 = getelementptr inbounds i32, i32* %1088, i32 3
  %1089 = load i32, i32* %arrayidx899, align 4, !tbaa !7
  %1090 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx900 = getelementptr inbounds i32, i32* %1090, i32 4
  %1091 = load i32, i32* %arrayidx900, align 4, !tbaa !7
  %add901 = add nsw i32 %1089, %1091
  %1092 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx902 = getelementptr inbounds i32, i32* %1092, i32 3
  store i32 %add901, i32* %arrayidx902, align 4, !tbaa !7
  %1093 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx903 = getelementptr inbounds i32, i32* %1093, i32 4
  %1094 = load i32, i32* %arrayidx903, align 4, !tbaa !7
  %sub904 = sub nsw i32 0, %1094
  %1095 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx905 = getelementptr inbounds i32, i32* %1095, i32 3
  %1096 = load i32, i32* %arrayidx905, align 4, !tbaa !7
  %add906 = add nsw i32 %sub904, %1096
  %1097 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx907 = getelementptr inbounds i32, i32* %1097, i32 4
  store i32 %add906, i32* %arrayidx907, align 4, !tbaa !7
  %1098 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx908 = getelementptr inbounds i32, i32* %1098, i32 5
  %1099 = load i32, i32* %arrayidx908, align 4, !tbaa !7
  %sub909 = sub nsw i32 0, %1099
  %1100 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx910 = getelementptr inbounds i32, i32* %1100, i32 2
  %1101 = load i32, i32* %arrayidx910, align 4, !tbaa !7
  %add911 = add nsw i32 %sub909, %1101
  %1102 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx912 = getelementptr inbounds i32, i32* %1102, i32 5
  store i32 %add911, i32* %arrayidx912, align 4, !tbaa !7
  %1103 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx913 = getelementptr inbounds i32, i32* %1103, i32 6
  %1104 = load i32, i32* %arrayidx913, align 4, !tbaa !7
  %sub914 = sub nsw i32 0, %1104
  %1105 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx915 = getelementptr inbounds i32, i32* %1105, i32 1
  %1106 = load i32, i32* %arrayidx915, align 4, !tbaa !7
  %add916 = add nsw i32 %sub914, %1106
  %1107 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx917 = getelementptr inbounds i32, i32* %1107, i32 6
  store i32 %add916, i32* %arrayidx917, align 4, !tbaa !7
  %1108 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx918 = getelementptr inbounds i32, i32* %1108, i32 7
  %1109 = load i32, i32* %arrayidx918, align 4, !tbaa !7
  %sub919 = sub nsw i32 0, %1109
  %1110 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx920 = getelementptr inbounds i32, i32* %1110, i32 0
  %1111 = load i32, i32* %arrayidx920, align 4, !tbaa !7
  %add921 = add nsw i32 %sub919, %1111
  %1112 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx922 = getelementptr inbounds i32, i32* %1112, i32 7
  store i32 %add921, i32* %arrayidx922, align 4, !tbaa !7
  %1113 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx923 = getelementptr inbounds i32, i32* %1113, i32 8
  %1114 = load i32, i32* %arrayidx923, align 4, !tbaa !7
  %1115 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx924 = getelementptr inbounds i32, i32* %1115, i32 8
  store i32 %1114, i32* %arrayidx924, align 4, !tbaa !7
  %1116 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx925 = getelementptr inbounds i32, i32* %1116, i32 9
  %1117 = load i32, i32* %arrayidx925, align 4, !tbaa !7
  %1118 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx926 = getelementptr inbounds i32, i32* %1118, i32 9
  store i32 %1117, i32* %arrayidx926, align 4, !tbaa !7
  %1119 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx927 = getelementptr inbounds i32, i32* %1119, i32 32
  %1120 = load i32, i32* %arrayidx927, align 4, !tbaa !7
  %sub928 = sub nsw i32 0, %1120
  %1121 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx929 = getelementptr inbounds i32, i32* %1121, i32 10
  %1122 = load i32, i32* %arrayidx929, align 4, !tbaa !7
  %1123 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx930 = getelementptr inbounds i32, i32* %1123, i32 32
  %1124 = load i32, i32* %arrayidx930, align 4, !tbaa !7
  %1125 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx931 = getelementptr inbounds i32, i32* %1125, i32 13
  %1126 = load i32, i32* %arrayidx931, align 4, !tbaa !7
  %1127 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv932 = sext i8 %1127 to i32
  %call933 = call i32 @half_btf(i32 %sub928, i32 %1122, i32 %1124, i32 %1126, i32 %conv932)
  %1128 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx934 = getelementptr inbounds i32, i32* %1128, i32 10
  store i32 %call933, i32* %arrayidx934, align 4, !tbaa !7
  %1129 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx935 = getelementptr inbounds i32, i32* %1129, i32 32
  %1130 = load i32, i32* %arrayidx935, align 4, !tbaa !7
  %sub936 = sub nsw i32 0, %1130
  %1131 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx937 = getelementptr inbounds i32, i32* %1131, i32 11
  %1132 = load i32, i32* %arrayidx937, align 4, !tbaa !7
  %1133 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx938 = getelementptr inbounds i32, i32* %1133, i32 32
  %1134 = load i32, i32* %arrayidx938, align 4, !tbaa !7
  %1135 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx939 = getelementptr inbounds i32, i32* %1135, i32 12
  %1136 = load i32, i32* %arrayidx939, align 4, !tbaa !7
  %1137 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv940 = sext i8 %1137 to i32
  %call941 = call i32 @half_btf(i32 %sub936, i32 %1132, i32 %1134, i32 %1136, i32 %conv940)
  %1138 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx942 = getelementptr inbounds i32, i32* %1138, i32 11
  store i32 %call941, i32* %arrayidx942, align 4, !tbaa !7
  %1139 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx943 = getelementptr inbounds i32, i32* %1139, i32 32
  %1140 = load i32, i32* %arrayidx943, align 4, !tbaa !7
  %1141 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx944 = getelementptr inbounds i32, i32* %1141, i32 12
  %1142 = load i32, i32* %arrayidx944, align 4, !tbaa !7
  %1143 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx945 = getelementptr inbounds i32, i32* %1143, i32 32
  %1144 = load i32, i32* %arrayidx945, align 4, !tbaa !7
  %1145 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx946 = getelementptr inbounds i32, i32* %1145, i32 11
  %1146 = load i32, i32* %arrayidx946, align 4, !tbaa !7
  %1147 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv947 = sext i8 %1147 to i32
  %call948 = call i32 @half_btf(i32 %1140, i32 %1142, i32 %1144, i32 %1146, i32 %conv947)
  %1148 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx949 = getelementptr inbounds i32, i32* %1148, i32 12
  store i32 %call948, i32* %arrayidx949, align 4, !tbaa !7
  %1149 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx950 = getelementptr inbounds i32, i32* %1149, i32 32
  %1150 = load i32, i32* %arrayidx950, align 4, !tbaa !7
  %1151 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx951 = getelementptr inbounds i32, i32* %1151, i32 13
  %1152 = load i32, i32* %arrayidx951, align 4, !tbaa !7
  %1153 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx952 = getelementptr inbounds i32, i32* %1153, i32 32
  %1154 = load i32, i32* %arrayidx952, align 4, !tbaa !7
  %1155 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx953 = getelementptr inbounds i32, i32* %1155, i32 10
  %1156 = load i32, i32* %arrayidx953, align 4, !tbaa !7
  %1157 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv954 = sext i8 %1157 to i32
  %call955 = call i32 @half_btf(i32 %1150, i32 %1152, i32 %1154, i32 %1156, i32 %conv954)
  %1158 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx956 = getelementptr inbounds i32, i32* %1158, i32 13
  store i32 %call955, i32* %arrayidx956, align 4, !tbaa !7
  %1159 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx957 = getelementptr inbounds i32, i32* %1159, i32 14
  %1160 = load i32, i32* %arrayidx957, align 4, !tbaa !7
  %1161 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx958 = getelementptr inbounds i32, i32* %1161, i32 14
  store i32 %1160, i32* %arrayidx958, align 4, !tbaa !7
  %1162 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx959 = getelementptr inbounds i32, i32* %1162, i32 15
  %1163 = load i32, i32* %arrayidx959, align 4, !tbaa !7
  %1164 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx960 = getelementptr inbounds i32, i32* %1164, i32 15
  store i32 %1163, i32* %arrayidx960, align 4, !tbaa !7
  %1165 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx961 = getelementptr inbounds i32, i32* %1165, i32 16
  %1166 = load i32, i32* %arrayidx961, align 4, !tbaa !7
  %1167 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx962 = getelementptr inbounds i32, i32* %1167, i32 23
  %1168 = load i32, i32* %arrayidx962, align 4, !tbaa !7
  %add963 = add nsw i32 %1166, %1168
  %1169 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx964 = getelementptr inbounds i32, i32* %1169, i32 16
  store i32 %add963, i32* %arrayidx964, align 4, !tbaa !7
  %1170 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx965 = getelementptr inbounds i32, i32* %1170, i32 17
  %1171 = load i32, i32* %arrayidx965, align 4, !tbaa !7
  %1172 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx966 = getelementptr inbounds i32, i32* %1172, i32 22
  %1173 = load i32, i32* %arrayidx966, align 4, !tbaa !7
  %add967 = add nsw i32 %1171, %1173
  %1174 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx968 = getelementptr inbounds i32, i32* %1174, i32 17
  store i32 %add967, i32* %arrayidx968, align 4, !tbaa !7
  %1175 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx969 = getelementptr inbounds i32, i32* %1175, i32 18
  %1176 = load i32, i32* %arrayidx969, align 4, !tbaa !7
  %1177 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx970 = getelementptr inbounds i32, i32* %1177, i32 21
  %1178 = load i32, i32* %arrayidx970, align 4, !tbaa !7
  %add971 = add nsw i32 %1176, %1178
  %1179 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx972 = getelementptr inbounds i32, i32* %1179, i32 18
  store i32 %add971, i32* %arrayidx972, align 4, !tbaa !7
  %1180 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx973 = getelementptr inbounds i32, i32* %1180, i32 19
  %1181 = load i32, i32* %arrayidx973, align 4, !tbaa !7
  %1182 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx974 = getelementptr inbounds i32, i32* %1182, i32 20
  %1183 = load i32, i32* %arrayidx974, align 4, !tbaa !7
  %add975 = add nsw i32 %1181, %1183
  %1184 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx976 = getelementptr inbounds i32, i32* %1184, i32 19
  store i32 %add975, i32* %arrayidx976, align 4, !tbaa !7
  %1185 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx977 = getelementptr inbounds i32, i32* %1185, i32 20
  %1186 = load i32, i32* %arrayidx977, align 4, !tbaa !7
  %sub978 = sub nsw i32 0, %1186
  %1187 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx979 = getelementptr inbounds i32, i32* %1187, i32 19
  %1188 = load i32, i32* %arrayidx979, align 4, !tbaa !7
  %add980 = add nsw i32 %sub978, %1188
  %1189 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx981 = getelementptr inbounds i32, i32* %1189, i32 20
  store i32 %add980, i32* %arrayidx981, align 4, !tbaa !7
  %1190 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx982 = getelementptr inbounds i32, i32* %1190, i32 21
  %1191 = load i32, i32* %arrayidx982, align 4, !tbaa !7
  %sub983 = sub nsw i32 0, %1191
  %1192 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx984 = getelementptr inbounds i32, i32* %1192, i32 18
  %1193 = load i32, i32* %arrayidx984, align 4, !tbaa !7
  %add985 = add nsw i32 %sub983, %1193
  %1194 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx986 = getelementptr inbounds i32, i32* %1194, i32 21
  store i32 %add985, i32* %arrayidx986, align 4, !tbaa !7
  %1195 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx987 = getelementptr inbounds i32, i32* %1195, i32 22
  %1196 = load i32, i32* %arrayidx987, align 4, !tbaa !7
  %sub988 = sub nsw i32 0, %1196
  %1197 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx989 = getelementptr inbounds i32, i32* %1197, i32 17
  %1198 = load i32, i32* %arrayidx989, align 4, !tbaa !7
  %add990 = add nsw i32 %sub988, %1198
  %1199 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx991 = getelementptr inbounds i32, i32* %1199, i32 22
  store i32 %add990, i32* %arrayidx991, align 4, !tbaa !7
  %1200 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx992 = getelementptr inbounds i32, i32* %1200, i32 23
  %1201 = load i32, i32* %arrayidx992, align 4, !tbaa !7
  %sub993 = sub nsw i32 0, %1201
  %1202 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx994 = getelementptr inbounds i32, i32* %1202, i32 16
  %1203 = load i32, i32* %arrayidx994, align 4, !tbaa !7
  %add995 = add nsw i32 %sub993, %1203
  %1204 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx996 = getelementptr inbounds i32, i32* %1204, i32 23
  store i32 %add995, i32* %arrayidx996, align 4, !tbaa !7
  %1205 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx997 = getelementptr inbounds i32, i32* %1205, i32 24
  %1206 = load i32, i32* %arrayidx997, align 4, !tbaa !7
  %sub998 = sub nsw i32 0, %1206
  %1207 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx999 = getelementptr inbounds i32, i32* %1207, i32 31
  %1208 = load i32, i32* %arrayidx999, align 4, !tbaa !7
  %add1000 = add nsw i32 %sub998, %1208
  %1209 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1001 = getelementptr inbounds i32, i32* %1209, i32 24
  store i32 %add1000, i32* %arrayidx1001, align 4, !tbaa !7
  %1210 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1002 = getelementptr inbounds i32, i32* %1210, i32 25
  %1211 = load i32, i32* %arrayidx1002, align 4, !tbaa !7
  %sub1003 = sub nsw i32 0, %1211
  %1212 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1004 = getelementptr inbounds i32, i32* %1212, i32 30
  %1213 = load i32, i32* %arrayidx1004, align 4, !tbaa !7
  %add1005 = add nsw i32 %sub1003, %1213
  %1214 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1006 = getelementptr inbounds i32, i32* %1214, i32 25
  store i32 %add1005, i32* %arrayidx1006, align 4, !tbaa !7
  %1215 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1007 = getelementptr inbounds i32, i32* %1215, i32 26
  %1216 = load i32, i32* %arrayidx1007, align 4, !tbaa !7
  %sub1008 = sub nsw i32 0, %1216
  %1217 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1009 = getelementptr inbounds i32, i32* %1217, i32 29
  %1218 = load i32, i32* %arrayidx1009, align 4, !tbaa !7
  %add1010 = add nsw i32 %sub1008, %1218
  %1219 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1011 = getelementptr inbounds i32, i32* %1219, i32 26
  store i32 %add1010, i32* %arrayidx1011, align 4, !tbaa !7
  %1220 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1012 = getelementptr inbounds i32, i32* %1220, i32 27
  %1221 = load i32, i32* %arrayidx1012, align 4, !tbaa !7
  %sub1013 = sub nsw i32 0, %1221
  %1222 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1014 = getelementptr inbounds i32, i32* %1222, i32 28
  %1223 = load i32, i32* %arrayidx1014, align 4, !tbaa !7
  %add1015 = add nsw i32 %sub1013, %1223
  %1224 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1016 = getelementptr inbounds i32, i32* %1224, i32 27
  store i32 %add1015, i32* %arrayidx1016, align 4, !tbaa !7
  %1225 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1017 = getelementptr inbounds i32, i32* %1225, i32 28
  %1226 = load i32, i32* %arrayidx1017, align 4, !tbaa !7
  %1227 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1018 = getelementptr inbounds i32, i32* %1227, i32 27
  %1228 = load i32, i32* %arrayidx1018, align 4, !tbaa !7
  %add1019 = add nsw i32 %1226, %1228
  %1229 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1020 = getelementptr inbounds i32, i32* %1229, i32 28
  store i32 %add1019, i32* %arrayidx1020, align 4, !tbaa !7
  %1230 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1021 = getelementptr inbounds i32, i32* %1230, i32 29
  %1231 = load i32, i32* %arrayidx1021, align 4, !tbaa !7
  %1232 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1022 = getelementptr inbounds i32, i32* %1232, i32 26
  %1233 = load i32, i32* %arrayidx1022, align 4, !tbaa !7
  %add1023 = add nsw i32 %1231, %1233
  %1234 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1024 = getelementptr inbounds i32, i32* %1234, i32 29
  store i32 %add1023, i32* %arrayidx1024, align 4, !tbaa !7
  %1235 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1025 = getelementptr inbounds i32, i32* %1235, i32 30
  %1236 = load i32, i32* %arrayidx1025, align 4, !tbaa !7
  %1237 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1026 = getelementptr inbounds i32, i32* %1237, i32 25
  %1238 = load i32, i32* %arrayidx1026, align 4, !tbaa !7
  %add1027 = add nsw i32 %1236, %1238
  %1239 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1028 = getelementptr inbounds i32, i32* %1239, i32 30
  store i32 %add1027, i32* %arrayidx1028, align 4, !tbaa !7
  %1240 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1029 = getelementptr inbounds i32, i32* %1240, i32 31
  %1241 = load i32, i32* %arrayidx1029, align 4, !tbaa !7
  %1242 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1030 = getelementptr inbounds i32, i32* %1242, i32 24
  %1243 = load i32, i32* %arrayidx1030, align 4, !tbaa !7
  %add1031 = add nsw i32 %1241, %1243
  %1244 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1032 = getelementptr inbounds i32, i32* %1244, i32 31
  store i32 %add1031, i32* %arrayidx1032, align 4, !tbaa !7
  %1245 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1033 = getelementptr inbounds i32, i32* %1245, i32 32
  %1246 = load i32, i32* %arrayidx1033, align 4, !tbaa !7
  %1247 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1034 = getelementptr inbounds i32, i32* %1247, i32 32
  store i32 %1246, i32* %arrayidx1034, align 4, !tbaa !7
  %1248 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1035 = getelementptr inbounds i32, i32* %1248, i32 33
  %1249 = load i32, i32* %arrayidx1035, align 4, !tbaa !7
  %1250 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1036 = getelementptr inbounds i32, i32* %1250, i32 33
  store i32 %1249, i32* %arrayidx1036, align 4, !tbaa !7
  %1251 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1037 = getelementptr inbounds i32, i32* %1251, i32 34
  %1252 = load i32, i32* %arrayidx1037, align 4, !tbaa !7
  %1253 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1038 = getelementptr inbounds i32, i32* %1253, i32 34
  store i32 %1252, i32* %arrayidx1038, align 4, !tbaa !7
  %1254 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1039 = getelementptr inbounds i32, i32* %1254, i32 35
  %1255 = load i32, i32* %arrayidx1039, align 4, !tbaa !7
  %1256 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1040 = getelementptr inbounds i32, i32* %1256, i32 35
  store i32 %1255, i32* %arrayidx1040, align 4, !tbaa !7
  %1257 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1041 = getelementptr inbounds i32, i32* %1257, i32 16
  %1258 = load i32, i32* %arrayidx1041, align 4, !tbaa !7
  %sub1042 = sub nsw i32 0, %1258
  %1259 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1043 = getelementptr inbounds i32, i32* %1259, i32 36
  %1260 = load i32, i32* %arrayidx1043, align 4, !tbaa !7
  %1261 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1044 = getelementptr inbounds i32, i32* %1261, i32 48
  %1262 = load i32, i32* %arrayidx1044, align 4, !tbaa !7
  %1263 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1045 = getelementptr inbounds i32, i32* %1263, i32 59
  %1264 = load i32, i32* %arrayidx1045, align 4, !tbaa !7
  %1265 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1046 = sext i8 %1265 to i32
  %call1047 = call i32 @half_btf(i32 %sub1042, i32 %1260, i32 %1262, i32 %1264, i32 %conv1046)
  %1266 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1048 = getelementptr inbounds i32, i32* %1266, i32 36
  store i32 %call1047, i32* %arrayidx1048, align 4, !tbaa !7
  %1267 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1049 = getelementptr inbounds i32, i32* %1267, i32 16
  %1268 = load i32, i32* %arrayidx1049, align 4, !tbaa !7
  %sub1050 = sub nsw i32 0, %1268
  %1269 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1051 = getelementptr inbounds i32, i32* %1269, i32 37
  %1270 = load i32, i32* %arrayidx1051, align 4, !tbaa !7
  %1271 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1052 = getelementptr inbounds i32, i32* %1271, i32 48
  %1272 = load i32, i32* %arrayidx1052, align 4, !tbaa !7
  %1273 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1053 = getelementptr inbounds i32, i32* %1273, i32 58
  %1274 = load i32, i32* %arrayidx1053, align 4, !tbaa !7
  %1275 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1054 = sext i8 %1275 to i32
  %call1055 = call i32 @half_btf(i32 %sub1050, i32 %1270, i32 %1272, i32 %1274, i32 %conv1054)
  %1276 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1056 = getelementptr inbounds i32, i32* %1276, i32 37
  store i32 %call1055, i32* %arrayidx1056, align 4, !tbaa !7
  %1277 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1057 = getelementptr inbounds i32, i32* %1277, i32 16
  %1278 = load i32, i32* %arrayidx1057, align 4, !tbaa !7
  %sub1058 = sub nsw i32 0, %1278
  %1279 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1059 = getelementptr inbounds i32, i32* %1279, i32 38
  %1280 = load i32, i32* %arrayidx1059, align 4, !tbaa !7
  %1281 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1060 = getelementptr inbounds i32, i32* %1281, i32 48
  %1282 = load i32, i32* %arrayidx1060, align 4, !tbaa !7
  %1283 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1061 = getelementptr inbounds i32, i32* %1283, i32 57
  %1284 = load i32, i32* %arrayidx1061, align 4, !tbaa !7
  %1285 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1062 = sext i8 %1285 to i32
  %call1063 = call i32 @half_btf(i32 %sub1058, i32 %1280, i32 %1282, i32 %1284, i32 %conv1062)
  %1286 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1064 = getelementptr inbounds i32, i32* %1286, i32 38
  store i32 %call1063, i32* %arrayidx1064, align 4, !tbaa !7
  %1287 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1065 = getelementptr inbounds i32, i32* %1287, i32 16
  %1288 = load i32, i32* %arrayidx1065, align 4, !tbaa !7
  %sub1066 = sub nsw i32 0, %1288
  %1289 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1067 = getelementptr inbounds i32, i32* %1289, i32 39
  %1290 = load i32, i32* %arrayidx1067, align 4, !tbaa !7
  %1291 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1068 = getelementptr inbounds i32, i32* %1291, i32 48
  %1292 = load i32, i32* %arrayidx1068, align 4, !tbaa !7
  %1293 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1069 = getelementptr inbounds i32, i32* %1293, i32 56
  %1294 = load i32, i32* %arrayidx1069, align 4, !tbaa !7
  %1295 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1070 = sext i8 %1295 to i32
  %call1071 = call i32 @half_btf(i32 %sub1066, i32 %1290, i32 %1292, i32 %1294, i32 %conv1070)
  %1296 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1072 = getelementptr inbounds i32, i32* %1296, i32 39
  store i32 %call1071, i32* %arrayidx1072, align 4, !tbaa !7
  %1297 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1073 = getelementptr inbounds i32, i32* %1297, i32 48
  %1298 = load i32, i32* %arrayidx1073, align 4, !tbaa !7
  %sub1074 = sub nsw i32 0, %1298
  %1299 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1075 = getelementptr inbounds i32, i32* %1299, i32 40
  %1300 = load i32, i32* %arrayidx1075, align 4, !tbaa !7
  %1301 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1076 = getelementptr inbounds i32, i32* %1301, i32 16
  %1302 = load i32, i32* %arrayidx1076, align 4, !tbaa !7
  %sub1077 = sub nsw i32 0, %1302
  %1303 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1078 = getelementptr inbounds i32, i32* %1303, i32 55
  %1304 = load i32, i32* %arrayidx1078, align 4, !tbaa !7
  %1305 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1079 = sext i8 %1305 to i32
  %call1080 = call i32 @half_btf(i32 %sub1074, i32 %1300, i32 %sub1077, i32 %1304, i32 %conv1079)
  %1306 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1081 = getelementptr inbounds i32, i32* %1306, i32 40
  store i32 %call1080, i32* %arrayidx1081, align 4, !tbaa !7
  %1307 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1082 = getelementptr inbounds i32, i32* %1307, i32 48
  %1308 = load i32, i32* %arrayidx1082, align 4, !tbaa !7
  %sub1083 = sub nsw i32 0, %1308
  %1309 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1084 = getelementptr inbounds i32, i32* %1309, i32 41
  %1310 = load i32, i32* %arrayidx1084, align 4, !tbaa !7
  %1311 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1085 = getelementptr inbounds i32, i32* %1311, i32 16
  %1312 = load i32, i32* %arrayidx1085, align 4, !tbaa !7
  %sub1086 = sub nsw i32 0, %1312
  %1313 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1087 = getelementptr inbounds i32, i32* %1313, i32 54
  %1314 = load i32, i32* %arrayidx1087, align 4, !tbaa !7
  %1315 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1088 = sext i8 %1315 to i32
  %call1089 = call i32 @half_btf(i32 %sub1083, i32 %1310, i32 %sub1086, i32 %1314, i32 %conv1088)
  %1316 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1090 = getelementptr inbounds i32, i32* %1316, i32 41
  store i32 %call1089, i32* %arrayidx1090, align 4, !tbaa !7
  %1317 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1091 = getelementptr inbounds i32, i32* %1317, i32 48
  %1318 = load i32, i32* %arrayidx1091, align 4, !tbaa !7
  %sub1092 = sub nsw i32 0, %1318
  %1319 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1093 = getelementptr inbounds i32, i32* %1319, i32 42
  %1320 = load i32, i32* %arrayidx1093, align 4, !tbaa !7
  %1321 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1094 = getelementptr inbounds i32, i32* %1321, i32 16
  %1322 = load i32, i32* %arrayidx1094, align 4, !tbaa !7
  %sub1095 = sub nsw i32 0, %1322
  %1323 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1096 = getelementptr inbounds i32, i32* %1323, i32 53
  %1324 = load i32, i32* %arrayidx1096, align 4, !tbaa !7
  %1325 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1097 = sext i8 %1325 to i32
  %call1098 = call i32 @half_btf(i32 %sub1092, i32 %1320, i32 %sub1095, i32 %1324, i32 %conv1097)
  %1326 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1099 = getelementptr inbounds i32, i32* %1326, i32 42
  store i32 %call1098, i32* %arrayidx1099, align 4, !tbaa !7
  %1327 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1100 = getelementptr inbounds i32, i32* %1327, i32 48
  %1328 = load i32, i32* %arrayidx1100, align 4, !tbaa !7
  %sub1101 = sub nsw i32 0, %1328
  %1329 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1102 = getelementptr inbounds i32, i32* %1329, i32 43
  %1330 = load i32, i32* %arrayidx1102, align 4, !tbaa !7
  %1331 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1103 = getelementptr inbounds i32, i32* %1331, i32 16
  %1332 = load i32, i32* %arrayidx1103, align 4, !tbaa !7
  %sub1104 = sub nsw i32 0, %1332
  %1333 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1105 = getelementptr inbounds i32, i32* %1333, i32 52
  %1334 = load i32, i32* %arrayidx1105, align 4, !tbaa !7
  %1335 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1106 = sext i8 %1335 to i32
  %call1107 = call i32 @half_btf(i32 %sub1101, i32 %1330, i32 %sub1104, i32 %1334, i32 %conv1106)
  %1336 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1108 = getelementptr inbounds i32, i32* %1336, i32 43
  store i32 %call1107, i32* %arrayidx1108, align 4, !tbaa !7
  %1337 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1109 = getelementptr inbounds i32, i32* %1337, i32 44
  %1338 = load i32, i32* %arrayidx1109, align 4, !tbaa !7
  %1339 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1110 = getelementptr inbounds i32, i32* %1339, i32 44
  store i32 %1338, i32* %arrayidx1110, align 4, !tbaa !7
  %1340 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1111 = getelementptr inbounds i32, i32* %1340, i32 45
  %1341 = load i32, i32* %arrayidx1111, align 4, !tbaa !7
  %1342 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1112 = getelementptr inbounds i32, i32* %1342, i32 45
  store i32 %1341, i32* %arrayidx1112, align 4, !tbaa !7
  %1343 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1113 = getelementptr inbounds i32, i32* %1343, i32 46
  %1344 = load i32, i32* %arrayidx1113, align 4, !tbaa !7
  %1345 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1114 = getelementptr inbounds i32, i32* %1345, i32 46
  store i32 %1344, i32* %arrayidx1114, align 4, !tbaa !7
  %1346 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1115 = getelementptr inbounds i32, i32* %1346, i32 47
  %1347 = load i32, i32* %arrayidx1115, align 4, !tbaa !7
  %1348 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1116 = getelementptr inbounds i32, i32* %1348, i32 47
  store i32 %1347, i32* %arrayidx1116, align 4, !tbaa !7
  %1349 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1117 = getelementptr inbounds i32, i32* %1349, i32 48
  %1350 = load i32, i32* %arrayidx1117, align 4, !tbaa !7
  %1351 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1118 = getelementptr inbounds i32, i32* %1351, i32 48
  store i32 %1350, i32* %arrayidx1118, align 4, !tbaa !7
  %1352 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1119 = getelementptr inbounds i32, i32* %1352, i32 49
  %1353 = load i32, i32* %arrayidx1119, align 4, !tbaa !7
  %1354 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1120 = getelementptr inbounds i32, i32* %1354, i32 49
  store i32 %1353, i32* %arrayidx1120, align 4, !tbaa !7
  %1355 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1121 = getelementptr inbounds i32, i32* %1355, i32 50
  %1356 = load i32, i32* %arrayidx1121, align 4, !tbaa !7
  %1357 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1122 = getelementptr inbounds i32, i32* %1357, i32 50
  store i32 %1356, i32* %arrayidx1122, align 4, !tbaa !7
  %1358 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1123 = getelementptr inbounds i32, i32* %1358, i32 51
  %1359 = load i32, i32* %arrayidx1123, align 4, !tbaa !7
  %1360 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1124 = getelementptr inbounds i32, i32* %1360, i32 51
  store i32 %1359, i32* %arrayidx1124, align 4, !tbaa !7
  %1361 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1125 = getelementptr inbounds i32, i32* %1361, i32 48
  %1362 = load i32, i32* %arrayidx1125, align 4, !tbaa !7
  %1363 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1126 = getelementptr inbounds i32, i32* %1363, i32 52
  %1364 = load i32, i32* %arrayidx1126, align 4, !tbaa !7
  %1365 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1127 = getelementptr inbounds i32, i32* %1365, i32 16
  %1366 = load i32, i32* %arrayidx1127, align 4, !tbaa !7
  %sub1128 = sub nsw i32 0, %1366
  %1367 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1129 = getelementptr inbounds i32, i32* %1367, i32 43
  %1368 = load i32, i32* %arrayidx1129, align 4, !tbaa !7
  %1369 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1130 = sext i8 %1369 to i32
  %call1131 = call i32 @half_btf(i32 %1362, i32 %1364, i32 %sub1128, i32 %1368, i32 %conv1130)
  %1370 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1132 = getelementptr inbounds i32, i32* %1370, i32 52
  store i32 %call1131, i32* %arrayidx1132, align 4, !tbaa !7
  %1371 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1133 = getelementptr inbounds i32, i32* %1371, i32 48
  %1372 = load i32, i32* %arrayidx1133, align 4, !tbaa !7
  %1373 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1134 = getelementptr inbounds i32, i32* %1373, i32 53
  %1374 = load i32, i32* %arrayidx1134, align 4, !tbaa !7
  %1375 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1135 = getelementptr inbounds i32, i32* %1375, i32 16
  %1376 = load i32, i32* %arrayidx1135, align 4, !tbaa !7
  %sub1136 = sub nsw i32 0, %1376
  %1377 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1137 = getelementptr inbounds i32, i32* %1377, i32 42
  %1378 = load i32, i32* %arrayidx1137, align 4, !tbaa !7
  %1379 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1138 = sext i8 %1379 to i32
  %call1139 = call i32 @half_btf(i32 %1372, i32 %1374, i32 %sub1136, i32 %1378, i32 %conv1138)
  %1380 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1140 = getelementptr inbounds i32, i32* %1380, i32 53
  store i32 %call1139, i32* %arrayidx1140, align 4, !tbaa !7
  %1381 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1141 = getelementptr inbounds i32, i32* %1381, i32 48
  %1382 = load i32, i32* %arrayidx1141, align 4, !tbaa !7
  %1383 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1142 = getelementptr inbounds i32, i32* %1383, i32 54
  %1384 = load i32, i32* %arrayidx1142, align 4, !tbaa !7
  %1385 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1143 = getelementptr inbounds i32, i32* %1385, i32 16
  %1386 = load i32, i32* %arrayidx1143, align 4, !tbaa !7
  %sub1144 = sub nsw i32 0, %1386
  %1387 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1145 = getelementptr inbounds i32, i32* %1387, i32 41
  %1388 = load i32, i32* %arrayidx1145, align 4, !tbaa !7
  %1389 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1146 = sext i8 %1389 to i32
  %call1147 = call i32 @half_btf(i32 %1382, i32 %1384, i32 %sub1144, i32 %1388, i32 %conv1146)
  %1390 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1148 = getelementptr inbounds i32, i32* %1390, i32 54
  store i32 %call1147, i32* %arrayidx1148, align 4, !tbaa !7
  %1391 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1149 = getelementptr inbounds i32, i32* %1391, i32 48
  %1392 = load i32, i32* %arrayidx1149, align 4, !tbaa !7
  %1393 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1150 = getelementptr inbounds i32, i32* %1393, i32 55
  %1394 = load i32, i32* %arrayidx1150, align 4, !tbaa !7
  %1395 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1151 = getelementptr inbounds i32, i32* %1395, i32 16
  %1396 = load i32, i32* %arrayidx1151, align 4, !tbaa !7
  %sub1152 = sub nsw i32 0, %1396
  %1397 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1153 = getelementptr inbounds i32, i32* %1397, i32 40
  %1398 = load i32, i32* %arrayidx1153, align 4, !tbaa !7
  %1399 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1154 = sext i8 %1399 to i32
  %call1155 = call i32 @half_btf(i32 %1392, i32 %1394, i32 %sub1152, i32 %1398, i32 %conv1154)
  %1400 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1156 = getelementptr inbounds i32, i32* %1400, i32 55
  store i32 %call1155, i32* %arrayidx1156, align 4, !tbaa !7
  %1401 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1157 = getelementptr inbounds i32, i32* %1401, i32 16
  %1402 = load i32, i32* %arrayidx1157, align 4, !tbaa !7
  %1403 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1158 = getelementptr inbounds i32, i32* %1403, i32 56
  %1404 = load i32, i32* %arrayidx1158, align 4, !tbaa !7
  %1405 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1159 = getelementptr inbounds i32, i32* %1405, i32 48
  %1406 = load i32, i32* %arrayidx1159, align 4, !tbaa !7
  %1407 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1160 = getelementptr inbounds i32, i32* %1407, i32 39
  %1408 = load i32, i32* %arrayidx1160, align 4, !tbaa !7
  %1409 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1161 = sext i8 %1409 to i32
  %call1162 = call i32 @half_btf(i32 %1402, i32 %1404, i32 %1406, i32 %1408, i32 %conv1161)
  %1410 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1163 = getelementptr inbounds i32, i32* %1410, i32 56
  store i32 %call1162, i32* %arrayidx1163, align 4, !tbaa !7
  %1411 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1164 = getelementptr inbounds i32, i32* %1411, i32 16
  %1412 = load i32, i32* %arrayidx1164, align 4, !tbaa !7
  %1413 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1165 = getelementptr inbounds i32, i32* %1413, i32 57
  %1414 = load i32, i32* %arrayidx1165, align 4, !tbaa !7
  %1415 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1166 = getelementptr inbounds i32, i32* %1415, i32 48
  %1416 = load i32, i32* %arrayidx1166, align 4, !tbaa !7
  %1417 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1167 = getelementptr inbounds i32, i32* %1417, i32 38
  %1418 = load i32, i32* %arrayidx1167, align 4, !tbaa !7
  %1419 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1168 = sext i8 %1419 to i32
  %call1169 = call i32 @half_btf(i32 %1412, i32 %1414, i32 %1416, i32 %1418, i32 %conv1168)
  %1420 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1170 = getelementptr inbounds i32, i32* %1420, i32 57
  store i32 %call1169, i32* %arrayidx1170, align 4, !tbaa !7
  %1421 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1171 = getelementptr inbounds i32, i32* %1421, i32 16
  %1422 = load i32, i32* %arrayidx1171, align 4, !tbaa !7
  %1423 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1172 = getelementptr inbounds i32, i32* %1423, i32 58
  %1424 = load i32, i32* %arrayidx1172, align 4, !tbaa !7
  %1425 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1173 = getelementptr inbounds i32, i32* %1425, i32 48
  %1426 = load i32, i32* %arrayidx1173, align 4, !tbaa !7
  %1427 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1174 = getelementptr inbounds i32, i32* %1427, i32 37
  %1428 = load i32, i32* %arrayidx1174, align 4, !tbaa !7
  %1429 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1175 = sext i8 %1429 to i32
  %call1176 = call i32 @half_btf(i32 %1422, i32 %1424, i32 %1426, i32 %1428, i32 %conv1175)
  %1430 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1177 = getelementptr inbounds i32, i32* %1430, i32 58
  store i32 %call1176, i32* %arrayidx1177, align 4, !tbaa !7
  %1431 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1178 = getelementptr inbounds i32, i32* %1431, i32 16
  %1432 = load i32, i32* %arrayidx1178, align 4, !tbaa !7
  %1433 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1179 = getelementptr inbounds i32, i32* %1433, i32 59
  %1434 = load i32, i32* %arrayidx1179, align 4, !tbaa !7
  %1435 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1180 = getelementptr inbounds i32, i32* %1435, i32 48
  %1436 = load i32, i32* %arrayidx1180, align 4, !tbaa !7
  %1437 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1181 = getelementptr inbounds i32, i32* %1437, i32 36
  %1438 = load i32, i32* %arrayidx1181, align 4, !tbaa !7
  %1439 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1182 = sext i8 %1439 to i32
  %call1183 = call i32 @half_btf(i32 %1432, i32 %1434, i32 %1436, i32 %1438, i32 %conv1182)
  %1440 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1184 = getelementptr inbounds i32, i32* %1440, i32 59
  store i32 %call1183, i32* %arrayidx1184, align 4, !tbaa !7
  %1441 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1185 = getelementptr inbounds i32, i32* %1441, i32 60
  %1442 = load i32, i32* %arrayidx1185, align 4, !tbaa !7
  %1443 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1186 = getelementptr inbounds i32, i32* %1443, i32 60
  store i32 %1442, i32* %arrayidx1186, align 4, !tbaa !7
  %1444 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1187 = getelementptr inbounds i32, i32* %1444, i32 61
  %1445 = load i32, i32* %arrayidx1187, align 4, !tbaa !7
  %1446 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1188 = getelementptr inbounds i32, i32* %1446, i32 61
  store i32 %1445, i32* %arrayidx1188, align 4, !tbaa !7
  %1447 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1189 = getelementptr inbounds i32, i32* %1447, i32 62
  %1448 = load i32, i32* %arrayidx1189, align 4, !tbaa !7
  %1449 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1190 = getelementptr inbounds i32, i32* %1449, i32 62
  store i32 %1448, i32* %arrayidx1190, align 4, !tbaa !7
  %1450 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1191 = getelementptr inbounds i32, i32* %1450, i32 63
  %1451 = load i32, i32* %arrayidx1191, align 4, !tbaa !7
  %1452 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1192 = getelementptr inbounds i32, i32* %1452, i32 63
  store i32 %1451, i32* %arrayidx1192, align 4, !tbaa !7
  %1453 = load i32, i32* %stage, align 4, !tbaa !7
  %1454 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %1455 = load i32*, i32** %bf1, align 4, !tbaa !2
  %1456 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %1457 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx1193 = getelementptr inbounds i8, i8* %1456, i32 %1457
  %1458 = load i8, i8* %arrayidx1193, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %1453, i32* %1454, i32* %1455, i32 64, i8 signext %1458)
  %1459 = load i32, i32* %stage, align 4, !tbaa !7
  %inc1194 = add nsw i32 %1459, 1
  store i32 %inc1194, i32* %stage, align 4, !tbaa !7
  %1460 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1195 = sext i8 %1460 to i32
  %call1196 = call i32* @cospi_arr(i32 %conv1195)
  store i32* %call1196, i32** %cospi, align 4, !tbaa !2
  %arraydecay1197 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay1197, i32** %bf0, align 4, !tbaa !2
  %1461 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %1461, i32** %bf1, align 4, !tbaa !2
  %1462 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1198 = getelementptr inbounds i32, i32* %1462, i32 0
  %1463 = load i32, i32* %arrayidx1198, align 4, !tbaa !7
  %1464 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1199 = getelementptr inbounds i32, i32* %1464, i32 3
  %1465 = load i32, i32* %arrayidx1199, align 4, !tbaa !7
  %add1200 = add nsw i32 %1463, %1465
  %1466 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1201 = getelementptr inbounds i32, i32* %1466, i32 0
  store i32 %add1200, i32* %arrayidx1201, align 4, !tbaa !7
  %1467 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1202 = getelementptr inbounds i32, i32* %1467, i32 1
  %1468 = load i32, i32* %arrayidx1202, align 4, !tbaa !7
  %1469 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1203 = getelementptr inbounds i32, i32* %1469, i32 2
  %1470 = load i32, i32* %arrayidx1203, align 4, !tbaa !7
  %add1204 = add nsw i32 %1468, %1470
  %1471 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1205 = getelementptr inbounds i32, i32* %1471, i32 1
  store i32 %add1204, i32* %arrayidx1205, align 4, !tbaa !7
  %1472 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1206 = getelementptr inbounds i32, i32* %1472, i32 2
  %1473 = load i32, i32* %arrayidx1206, align 4, !tbaa !7
  %sub1207 = sub nsw i32 0, %1473
  %1474 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1208 = getelementptr inbounds i32, i32* %1474, i32 1
  %1475 = load i32, i32* %arrayidx1208, align 4, !tbaa !7
  %add1209 = add nsw i32 %sub1207, %1475
  %1476 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1210 = getelementptr inbounds i32, i32* %1476, i32 2
  store i32 %add1209, i32* %arrayidx1210, align 4, !tbaa !7
  %1477 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1211 = getelementptr inbounds i32, i32* %1477, i32 3
  %1478 = load i32, i32* %arrayidx1211, align 4, !tbaa !7
  %sub1212 = sub nsw i32 0, %1478
  %1479 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1213 = getelementptr inbounds i32, i32* %1479, i32 0
  %1480 = load i32, i32* %arrayidx1213, align 4, !tbaa !7
  %add1214 = add nsw i32 %sub1212, %1480
  %1481 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1215 = getelementptr inbounds i32, i32* %1481, i32 3
  store i32 %add1214, i32* %arrayidx1215, align 4, !tbaa !7
  %1482 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1216 = getelementptr inbounds i32, i32* %1482, i32 4
  %1483 = load i32, i32* %arrayidx1216, align 4, !tbaa !7
  %1484 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1217 = getelementptr inbounds i32, i32* %1484, i32 4
  store i32 %1483, i32* %arrayidx1217, align 4, !tbaa !7
  %1485 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1218 = getelementptr inbounds i32, i32* %1485, i32 32
  %1486 = load i32, i32* %arrayidx1218, align 4, !tbaa !7
  %sub1219 = sub nsw i32 0, %1486
  %1487 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1220 = getelementptr inbounds i32, i32* %1487, i32 5
  %1488 = load i32, i32* %arrayidx1220, align 4, !tbaa !7
  %1489 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1221 = getelementptr inbounds i32, i32* %1489, i32 32
  %1490 = load i32, i32* %arrayidx1221, align 4, !tbaa !7
  %1491 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1222 = getelementptr inbounds i32, i32* %1491, i32 6
  %1492 = load i32, i32* %arrayidx1222, align 4, !tbaa !7
  %1493 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1223 = sext i8 %1493 to i32
  %call1224 = call i32 @half_btf(i32 %sub1219, i32 %1488, i32 %1490, i32 %1492, i32 %conv1223)
  %1494 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1225 = getelementptr inbounds i32, i32* %1494, i32 5
  store i32 %call1224, i32* %arrayidx1225, align 4, !tbaa !7
  %1495 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1226 = getelementptr inbounds i32, i32* %1495, i32 32
  %1496 = load i32, i32* %arrayidx1226, align 4, !tbaa !7
  %1497 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1227 = getelementptr inbounds i32, i32* %1497, i32 6
  %1498 = load i32, i32* %arrayidx1227, align 4, !tbaa !7
  %1499 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1228 = getelementptr inbounds i32, i32* %1499, i32 32
  %1500 = load i32, i32* %arrayidx1228, align 4, !tbaa !7
  %1501 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1229 = getelementptr inbounds i32, i32* %1501, i32 5
  %1502 = load i32, i32* %arrayidx1229, align 4, !tbaa !7
  %1503 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1230 = sext i8 %1503 to i32
  %call1231 = call i32 @half_btf(i32 %1496, i32 %1498, i32 %1500, i32 %1502, i32 %conv1230)
  %1504 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1232 = getelementptr inbounds i32, i32* %1504, i32 6
  store i32 %call1231, i32* %arrayidx1232, align 4, !tbaa !7
  %1505 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1233 = getelementptr inbounds i32, i32* %1505, i32 7
  %1506 = load i32, i32* %arrayidx1233, align 4, !tbaa !7
  %1507 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1234 = getelementptr inbounds i32, i32* %1507, i32 7
  store i32 %1506, i32* %arrayidx1234, align 4, !tbaa !7
  %1508 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1235 = getelementptr inbounds i32, i32* %1508, i32 8
  %1509 = load i32, i32* %arrayidx1235, align 4, !tbaa !7
  %1510 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1236 = getelementptr inbounds i32, i32* %1510, i32 11
  %1511 = load i32, i32* %arrayidx1236, align 4, !tbaa !7
  %add1237 = add nsw i32 %1509, %1511
  %1512 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1238 = getelementptr inbounds i32, i32* %1512, i32 8
  store i32 %add1237, i32* %arrayidx1238, align 4, !tbaa !7
  %1513 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1239 = getelementptr inbounds i32, i32* %1513, i32 9
  %1514 = load i32, i32* %arrayidx1239, align 4, !tbaa !7
  %1515 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1240 = getelementptr inbounds i32, i32* %1515, i32 10
  %1516 = load i32, i32* %arrayidx1240, align 4, !tbaa !7
  %add1241 = add nsw i32 %1514, %1516
  %1517 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1242 = getelementptr inbounds i32, i32* %1517, i32 9
  store i32 %add1241, i32* %arrayidx1242, align 4, !tbaa !7
  %1518 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1243 = getelementptr inbounds i32, i32* %1518, i32 10
  %1519 = load i32, i32* %arrayidx1243, align 4, !tbaa !7
  %sub1244 = sub nsw i32 0, %1519
  %1520 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1245 = getelementptr inbounds i32, i32* %1520, i32 9
  %1521 = load i32, i32* %arrayidx1245, align 4, !tbaa !7
  %add1246 = add nsw i32 %sub1244, %1521
  %1522 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1247 = getelementptr inbounds i32, i32* %1522, i32 10
  store i32 %add1246, i32* %arrayidx1247, align 4, !tbaa !7
  %1523 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1248 = getelementptr inbounds i32, i32* %1523, i32 11
  %1524 = load i32, i32* %arrayidx1248, align 4, !tbaa !7
  %sub1249 = sub nsw i32 0, %1524
  %1525 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1250 = getelementptr inbounds i32, i32* %1525, i32 8
  %1526 = load i32, i32* %arrayidx1250, align 4, !tbaa !7
  %add1251 = add nsw i32 %sub1249, %1526
  %1527 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1252 = getelementptr inbounds i32, i32* %1527, i32 11
  store i32 %add1251, i32* %arrayidx1252, align 4, !tbaa !7
  %1528 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1253 = getelementptr inbounds i32, i32* %1528, i32 12
  %1529 = load i32, i32* %arrayidx1253, align 4, !tbaa !7
  %sub1254 = sub nsw i32 0, %1529
  %1530 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1255 = getelementptr inbounds i32, i32* %1530, i32 15
  %1531 = load i32, i32* %arrayidx1255, align 4, !tbaa !7
  %add1256 = add nsw i32 %sub1254, %1531
  %1532 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1257 = getelementptr inbounds i32, i32* %1532, i32 12
  store i32 %add1256, i32* %arrayidx1257, align 4, !tbaa !7
  %1533 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1258 = getelementptr inbounds i32, i32* %1533, i32 13
  %1534 = load i32, i32* %arrayidx1258, align 4, !tbaa !7
  %sub1259 = sub nsw i32 0, %1534
  %1535 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1260 = getelementptr inbounds i32, i32* %1535, i32 14
  %1536 = load i32, i32* %arrayidx1260, align 4, !tbaa !7
  %add1261 = add nsw i32 %sub1259, %1536
  %1537 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1262 = getelementptr inbounds i32, i32* %1537, i32 13
  store i32 %add1261, i32* %arrayidx1262, align 4, !tbaa !7
  %1538 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1263 = getelementptr inbounds i32, i32* %1538, i32 14
  %1539 = load i32, i32* %arrayidx1263, align 4, !tbaa !7
  %1540 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1264 = getelementptr inbounds i32, i32* %1540, i32 13
  %1541 = load i32, i32* %arrayidx1264, align 4, !tbaa !7
  %add1265 = add nsw i32 %1539, %1541
  %1542 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1266 = getelementptr inbounds i32, i32* %1542, i32 14
  store i32 %add1265, i32* %arrayidx1266, align 4, !tbaa !7
  %1543 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1267 = getelementptr inbounds i32, i32* %1543, i32 15
  %1544 = load i32, i32* %arrayidx1267, align 4, !tbaa !7
  %1545 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1268 = getelementptr inbounds i32, i32* %1545, i32 12
  %1546 = load i32, i32* %arrayidx1268, align 4, !tbaa !7
  %add1269 = add nsw i32 %1544, %1546
  %1547 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1270 = getelementptr inbounds i32, i32* %1547, i32 15
  store i32 %add1269, i32* %arrayidx1270, align 4, !tbaa !7
  %1548 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1271 = getelementptr inbounds i32, i32* %1548, i32 16
  %1549 = load i32, i32* %arrayidx1271, align 4, !tbaa !7
  %1550 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1272 = getelementptr inbounds i32, i32* %1550, i32 16
  store i32 %1549, i32* %arrayidx1272, align 4, !tbaa !7
  %1551 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1273 = getelementptr inbounds i32, i32* %1551, i32 17
  %1552 = load i32, i32* %arrayidx1273, align 4, !tbaa !7
  %1553 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1274 = getelementptr inbounds i32, i32* %1553, i32 17
  store i32 %1552, i32* %arrayidx1274, align 4, !tbaa !7
  %1554 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1275 = getelementptr inbounds i32, i32* %1554, i32 16
  %1555 = load i32, i32* %arrayidx1275, align 4, !tbaa !7
  %sub1276 = sub nsw i32 0, %1555
  %1556 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1277 = getelementptr inbounds i32, i32* %1556, i32 18
  %1557 = load i32, i32* %arrayidx1277, align 4, !tbaa !7
  %1558 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1278 = getelementptr inbounds i32, i32* %1558, i32 48
  %1559 = load i32, i32* %arrayidx1278, align 4, !tbaa !7
  %1560 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1279 = getelementptr inbounds i32, i32* %1560, i32 29
  %1561 = load i32, i32* %arrayidx1279, align 4, !tbaa !7
  %1562 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1280 = sext i8 %1562 to i32
  %call1281 = call i32 @half_btf(i32 %sub1276, i32 %1557, i32 %1559, i32 %1561, i32 %conv1280)
  %1563 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1282 = getelementptr inbounds i32, i32* %1563, i32 18
  store i32 %call1281, i32* %arrayidx1282, align 4, !tbaa !7
  %1564 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1283 = getelementptr inbounds i32, i32* %1564, i32 16
  %1565 = load i32, i32* %arrayidx1283, align 4, !tbaa !7
  %sub1284 = sub nsw i32 0, %1565
  %1566 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1285 = getelementptr inbounds i32, i32* %1566, i32 19
  %1567 = load i32, i32* %arrayidx1285, align 4, !tbaa !7
  %1568 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1286 = getelementptr inbounds i32, i32* %1568, i32 48
  %1569 = load i32, i32* %arrayidx1286, align 4, !tbaa !7
  %1570 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1287 = getelementptr inbounds i32, i32* %1570, i32 28
  %1571 = load i32, i32* %arrayidx1287, align 4, !tbaa !7
  %1572 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1288 = sext i8 %1572 to i32
  %call1289 = call i32 @half_btf(i32 %sub1284, i32 %1567, i32 %1569, i32 %1571, i32 %conv1288)
  %1573 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1290 = getelementptr inbounds i32, i32* %1573, i32 19
  store i32 %call1289, i32* %arrayidx1290, align 4, !tbaa !7
  %1574 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1291 = getelementptr inbounds i32, i32* %1574, i32 48
  %1575 = load i32, i32* %arrayidx1291, align 4, !tbaa !7
  %sub1292 = sub nsw i32 0, %1575
  %1576 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1293 = getelementptr inbounds i32, i32* %1576, i32 20
  %1577 = load i32, i32* %arrayidx1293, align 4, !tbaa !7
  %1578 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1294 = getelementptr inbounds i32, i32* %1578, i32 16
  %1579 = load i32, i32* %arrayidx1294, align 4, !tbaa !7
  %sub1295 = sub nsw i32 0, %1579
  %1580 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1296 = getelementptr inbounds i32, i32* %1580, i32 27
  %1581 = load i32, i32* %arrayidx1296, align 4, !tbaa !7
  %1582 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1297 = sext i8 %1582 to i32
  %call1298 = call i32 @half_btf(i32 %sub1292, i32 %1577, i32 %sub1295, i32 %1581, i32 %conv1297)
  %1583 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1299 = getelementptr inbounds i32, i32* %1583, i32 20
  store i32 %call1298, i32* %arrayidx1299, align 4, !tbaa !7
  %1584 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1300 = getelementptr inbounds i32, i32* %1584, i32 48
  %1585 = load i32, i32* %arrayidx1300, align 4, !tbaa !7
  %sub1301 = sub nsw i32 0, %1585
  %1586 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1302 = getelementptr inbounds i32, i32* %1586, i32 21
  %1587 = load i32, i32* %arrayidx1302, align 4, !tbaa !7
  %1588 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1303 = getelementptr inbounds i32, i32* %1588, i32 16
  %1589 = load i32, i32* %arrayidx1303, align 4, !tbaa !7
  %sub1304 = sub nsw i32 0, %1589
  %1590 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1305 = getelementptr inbounds i32, i32* %1590, i32 26
  %1591 = load i32, i32* %arrayidx1305, align 4, !tbaa !7
  %1592 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1306 = sext i8 %1592 to i32
  %call1307 = call i32 @half_btf(i32 %sub1301, i32 %1587, i32 %sub1304, i32 %1591, i32 %conv1306)
  %1593 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1308 = getelementptr inbounds i32, i32* %1593, i32 21
  store i32 %call1307, i32* %arrayidx1308, align 4, !tbaa !7
  %1594 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1309 = getelementptr inbounds i32, i32* %1594, i32 22
  %1595 = load i32, i32* %arrayidx1309, align 4, !tbaa !7
  %1596 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1310 = getelementptr inbounds i32, i32* %1596, i32 22
  store i32 %1595, i32* %arrayidx1310, align 4, !tbaa !7
  %1597 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1311 = getelementptr inbounds i32, i32* %1597, i32 23
  %1598 = load i32, i32* %arrayidx1311, align 4, !tbaa !7
  %1599 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1312 = getelementptr inbounds i32, i32* %1599, i32 23
  store i32 %1598, i32* %arrayidx1312, align 4, !tbaa !7
  %1600 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1313 = getelementptr inbounds i32, i32* %1600, i32 24
  %1601 = load i32, i32* %arrayidx1313, align 4, !tbaa !7
  %1602 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1314 = getelementptr inbounds i32, i32* %1602, i32 24
  store i32 %1601, i32* %arrayidx1314, align 4, !tbaa !7
  %1603 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1315 = getelementptr inbounds i32, i32* %1603, i32 25
  %1604 = load i32, i32* %arrayidx1315, align 4, !tbaa !7
  %1605 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1316 = getelementptr inbounds i32, i32* %1605, i32 25
  store i32 %1604, i32* %arrayidx1316, align 4, !tbaa !7
  %1606 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1317 = getelementptr inbounds i32, i32* %1606, i32 48
  %1607 = load i32, i32* %arrayidx1317, align 4, !tbaa !7
  %1608 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1318 = getelementptr inbounds i32, i32* %1608, i32 26
  %1609 = load i32, i32* %arrayidx1318, align 4, !tbaa !7
  %1610 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1319 = getelementptr inbounds i32, i32* %1610, i32 16
  %1611 = load i32, i32* %arrayidx1319, align 4, !tbaa !7
  %sub1320 = sub nsw i32 0, %1611
  %1612 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1321 = getelementptr inbounds i32, i32* %1612, i32 21
  %1613 = load i32, i32* %arrayidx1321, align 4, !tbaa !7
  %1614 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1322 = sext i8 %1614 to i32
  %call1323 = call i32 @half_btf(i32 %1607, i32 %1609, i32 %sub1320, i32 %1613, i32 %conv1322)
  %1615 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1324 = getelementptr inbounds i32, i32* %1615, i32 26
  store i32 %call1323, i32* %arrayidx1324, align 4, !tbaa !7
  %1616 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1325 = getelementptr inbounds i32, i32* %1616, i32 48
  %1617 = load i32, i32* %arrayidx1325, align 4, !tbaa !7
  %1618 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1326 = getelementptr inbounds i32, i32* %1618, i32 27
  %1619 = load i32, i32* %arrayidx1326, align 4, !tbaa !7
  %1620 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1327 = getelementptr inbounds i32, i32* %1620, i32 16
  %1621 = load i32, i32* %arrayidx1327, align 4, !tbaa !7
  %sub1328 = sub nsw i32 0, %1621
  %1622 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1329 = getelementptr inbounds i32, i32* %1622, i32 20
  %1623 = load i32, i32* %arrayidx1329, align 4, !tbaa !7
  %1624 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1330 = sext i8 %1624 to i32
  %call1331 = call i32 @half_btf(i32 %1617, i32 %1619, i32 %sub1328, i32 %1623, i32 %conv1330)
  %1625 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1332 = getelementptr inbounds i32, i32* %1625, i32 27
  store i32 %call1331, i32* %arrayidx1332, align 4, !tbaa !7
  %1626 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1333 = getelementptr inbounds i32, i32* %1626, i32 16
  %1627 = load i32, i32* %arrayidx1333, align 4, !tbaa !7
  %1628 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1334 = getelementptr inbounds i32, i32* %1628, i32 28
  %1629 = load i32, i32* %arrayidx1334, align 4, !tbaa !7
  %1630 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1335 = getelementptr inbounds i32, i32* %1630, i32 48
  %1631 = load i32, i32* %arrayidx1335, align 4, !tbaa !7
  %1632 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1336 = getelementptr inbounds i32, i32* %1632, i32 19
  %1633 = load i32, i32* %arrayidx1336, align 4, !tbaa !7
  %1634 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1337 = sext i8 %1634 to i32
  %call1338 = call i32 @half_btf(i32 %1627, i32 %1629, i32 %1631, i32 %1633, i32 %conv1337)
  %1635 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1339 = getelementptr inbounds i32, i32* %1635, i32 28
  store i32 %call1338, i32* %arrayidx1339, align 4, !tbaa !7
  %1636 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1340 = getelementptr inbounds i32, i32* %1636, i32 16
  %1637 = load i32, i32* %arrayidx1340, align 4, !tbaa !7
  %1638 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1341 = getelementptr inbounds i32, i32* %1638, i32 29
  %1639 = load i32, i32* %arrayidx1341, align 4, !tbaa !7
  %1640 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1342 = getelementptr inbounds i32, i32* %1640, i32 48
  %1641 = load i32, i32* %arrayidx1342, align 4, !tbaa !7
  %1642 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1343 = getelementptr inbounds i32, i32* %1642, i32 18
  %1643 = load i32, i32* %arrayidx1343, align 4, !tbaa !7
  %1644 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1344 = sext i8 %1644 to i32
  %call1345 = call i32 @half_btf(i32 %1637, i32 %1639, i32 %1641, i32 %1643, i32 %conv1344)
  %1645 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1346 = getelementptr inbounds i32, i32* %1645, i32 29
  store i32 %call1345, i32* %arrayidx1346, align 4, !tbaa !7
  %1646 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1347 = getelementptr inbounds i32, i32* %1646, i32 30
  %1647 = load i32, i32* %arrayidx1347, align 4, !tbaa !7
  %1648 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1348 = getelementptr inbounds i32, i32* %1648, i32 30
  store i32 %1647, i32* %arrayidx1348, align 4, !tbaa !7
  %1649 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1349 = getelementptr inbounds i32, i32* %1649, i32 31
  %1650 = load i32, i32* %arrayidx1349, align 4, !tbaa !7
  %1651 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1350 = getelementptr inbounds i32, i32* %1651, i32 31
  store i32 %1650, i32* %arrayidx1350, align 4, !tbaa !7
  %1652 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1351 = getelementptr inbounds i32, i32* %1652, i32 32
  %1653 = load i32, i32* %arrayidx1351, align 4, !tbaa !7
  %1654 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1352 = getelementptr inbounds i32, i32* %1654, i32 39
  %1655 = load i32, i32* %arrayidx1352, align 4, !tbaa !7
  %add1353 = add nsw i32 %1653, %1655
  %1656 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1354 = getelementptr inbounds i32, i32* %1656, i32 32
  store i32 %add1353, i32* %arrayidx1354, align 4, !tbaa !7
  %1657 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1355 = getelementptr inbounds i32, i32* %1657, i32 33
  %1658 = load i32, i32* %arrayidx1355, align 4, !tbaa !7
  %1659 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1356 = getelementptr inbounds i32, i32* %1659, i32 38
  %1660 = load i32, i32* %arrayidx1356, align 4, !tbaa !7
  %add1357 = add nsw i32 %1658, %1660
  %1661 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1358 = getelementptr inbounds i32, i32* %1661, i32 33
  store i32 %add1357, i32* %arrayidx1358, align 4, !tbaa !7
  %1662 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1359 = getelementptr inbounds i32, i32* %1662, i32 34
  %1663 = load i32, i32* %arrayidx1359, align 4, !tbaa !7
  %1664 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1360 = getelementptr inbounds i32, i32* %1664, i32 37
  %1665 = load i32, i32* %arrayidx1360, align 4, !tbaa !7
  %add1361 = add nsw i32 %1663, %1665
  %1666 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1362 = getelementptr inbounds i32, i32* %1666, i32 34
  store i32 %add1361, i32* %arrayidx1362, align 4, !tbaa !7
  %1667 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1363 = getelementptr inbounds i32, i32* %1667, i32 35
  %1668 = load i32, i32* %arrayidx1363, align 4, !tbaa !7
  %1669 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1364 = getelementptr inbounds i32, i32* %1669, i32 36
  %1670 = load i32, i32* %arrayidx1364, align 4, !tbaa !7
  %add1365 = add nsw i32 %1668, %1670
  %1671 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1366 = getelementptr inbounds i32, i32* %1671, i32 35
  store i32 %add1365, i32* %arrayidx1366, align 4, !tbaa !7
  %1672 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1367 = getelementptr inbounds i32, i32* %1672, i32 36
  %1673 = load i32, i32* %arrayidx1367, align 4, !tbaa !7
  %sub1368 = sub nsw i32 0, %1673
  %1674 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1369 = getelementptr inbounds i32, i32* %1674, i32 35
  %1675 = load i32, i32* %arrayidx1369, align 4, !tbaa !7
  %add1370 = add nsw i32 %sub1368, %1675
  %1676 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1371 = getelementptr inbounds i32, i32* %1676, i32 36
  store i32 %add1370, i32* %arrayidx1371, align 4, !tbaa !7
  %1677 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1372 = getelementptr inbounds i32, i32* %1677, i32 37
  %1678 = load i32, i32* %arrayidx1372, align 4, !tbaa !7
  %sub1373 = sub nsw i32 0, %1678
  %1679 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1374 = getelementptr inbounds i32, i32* %1679, i32 34
  %1680 = load i32, i32* %arrayidx1374, align 4, !tbaa !7
  %add1375 = add nsw i32 %sub1373, %1680
  %1681 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1376 = getelementptr inbounds i32, i32* %1681, i32 37
  store i32 %add1375, i32* %arrayidx1376, align 4, !tbaa !7
  %1682 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1377 = getelementptr inbounds i32, i32* %1682, i32 38
  %1683 = load i32, i32* %arrayidx1377, align 4, !tbaa !7
  %sub1378 = sub nsw i32 0, %1683
  %1684 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1379 = getelementptr inbounds i32, i32* %1684, i32 33
  %1685 = load i32, i32* %arrayidx1379, align 4, !tbaa !7
  %add1380 = add nsw i32 %sub1378, %1685
  %1686 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1381 = getelementptr inbounds i32, i32* %1686, i32 38
  store i32 %add1380, i32* %arrayidx1381, align 4, !tbaa !7
  %1687 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1382 = getelementptr inbounds i32, i32* %1687, i32 39
  %1688 = load i32, i32* %arrayidx1382, align 4, !tbaa !7
  %sub1383 = sub nsw i32 0, %1688
  %1689 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1384 = getelementptr inbounds i32, i32* %1689, i32 32
  %1690 = load i32, i32* %arrayidx1384, align 4, !tbaa !7
  %add1385 = add nsw i32 %sub1383, %1690
  %1691 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1386 = getelementptr inbounds i32, i32* %1691, i32 39
  store i32 %add1385, i32* %arrayidx1386, align 4, !tbaa !7
  %1692 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1387 = getelementptr inbounds i32, i32* %1692, i32 40
  %1693 = load i32, i32* %arrayidx1387, align 4, !tbaa !7
  %sub1388 = sub nsw i32 0, %1693
  %1694 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1389 = getelementptr inbounds i32, i32* %1694, i32 47
  %1695 = load i32, i32* %arrayidx1389, align 4, !tbaa !7
  %add1390 = add nsw i32 %sub1388, %1695
  %1696 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1391 = getelementptr inbounds i32, i32* %1696, i32 40
  store i32 %add1390, i32* %arrayidx1391, align 4, !tbaa !7
  %1697 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1392 = getelementptr inbounds i32, i32* %1697, i32 41
  %1698 = load i32, i32* %arrayidx1392, align 4, !tbaa !7
  %sub1393 = sub nsw i32 0, %1698
  %1699 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1394 = getelementptr inbounds i32, i32* %1699, i32 46
  %1700 = load i32, i32* %arrayidx1394, align 4, !tbaa !7
  %add1395 = add nsw i32 %sub1393, %1700
  %1701 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1396 = getelementptr inbounds i32, i32* %1701, i32 41
  store i32 %add1395, i32* %arrayidx1396, align 4, !tbaa !7
  %1702 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1397 = getelementptr inbounds i32, i32* %1702, i32 42
  %1703 = load i32, i32* %arrayidx1397, align 4, !tbaa !7
  %sub1398 = sub nsw i32 0, %1703
  %1704 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1399 = getelementptr inbounds i32, i32* %1704, i32 45
  %1705 = load i32, i32* %arrayidx1399, align 4, !tbaa !7
  %add1400 = add nsw i32 %sub1398, %1705
  %1706 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1401 = getelementptr inbounds i32, i32* %1706, i32 42
  store i32 %add1400, i32* %arrayidx1401, align 4, !tbaa !7
  %1707 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1402 = getelementptr inbounds i32, i32* %1707, i32 43
  %1708 = load i32, i32* %arrayidx1402, align 4, !tbaa !7
  %sub1403 = sub nsw i32 0, %1708
  %1709 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1404 = getelementptr inbounds i32, i32* %1709, i32 44
  %1710 = load i32, i32* %arrayidx1404, align 4, !tbaa !7
  %add1405 = add nsw i32 %sub1403, %1710
  %1711 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1406 = getelementptr inbounds i32, i32* %1711, i32 43
  store i32 %add1405, i32* %arrayidx1406, align 4, !tbaa !7
  %1712 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1407 = getelementptr inbounds i32, i32* %1712, i32 44
  %1713 = load i32, i32* %arrayidx1407, align 4, !tbaa !7
  %1714 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1408 = getelementptr inbounds i32, i32* %1714, i32 43
  %1715 = load i32, i32* %arrayidx1408, align 4, !tbaa !7
  %add1409 = add nsw i32 %1713, %1715
  %1716 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1410 = getelementptr inbounds i32, i32* %1716, i32 44
  store i32 %add1409, i32* %arrayidx1410, align 4, !tbaa !7
  %1717 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1411 = getelementptr inbounds i32, i32* %1717, i32 45
  %1718 = load i32, i32* %arrayidx1411, align 4, !tbaa !7
  %1719 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1412 = getelementptr inbounds i32, i32* %1719, i32 42
  %1720 = load i32, i32* %arrayidx1412, align 4, !tbaa !7
  %add1413 = add nsw i32 %1718, %1720
  %1721 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1414 = getelementptr inbounds i32, i32* %1721, i32 45
  store i32 %add1413, i32* %arrayidx1414, align 4, !tbaa !7
  %1722 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1415 = getelementptr inbounds i32, i32* %1722, i32 46
  %1723 = load i32, i32* %arrayidx1415, align 4, !tbaa !7
  %1724 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1416 = getelementptr inbounds i32, i32* %1724, i32 41
  %1725 = load i32, i32* %arrayidx1416, align 4, !tbaa !7
  %add1417 = add nsw i32 %1723, %1725
  %1726 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1418 = getelementptr inbounds i32, i32* %1726, i32 46
  store i32 %add1417, i32* %arrayidx1418, align 4, !tbaa !7
  %1727 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1419 = getelementptr inbounds i32, i32* %1727, i32 47
  %1728 = load i32, i32* %arrayidx1419, align 4, !tbaa !7
  %1729 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1420 = getelementptr inbounds i32, i32* %1729, i32 40
  %1730 = load i32, i32* %arrayidx1420, align 4, !tbaa !7
  %add1421 = add nsw i32 %1728, %1730
  %1731 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1422 = getelementptr inbounds i32, i32* %1731, i32 47
  store i32 %add1421, i32* %arrayidx1422, align 4, !tbaa !7
  %1732 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1423 = getelementptr inbounds i32, i32* %1732, i32 48
  %1733 = load i32, i32* %arrayidx1423, align 4, !tbaa !7
  %1734 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1424 = getelementptr inbounds i32, i32* %1734, i32 55
  %1735 = load i32, i32* %arrayidx1424, align 4, !tbaa !7
  %add1425 = add nsw i32 %1733, %1735
  %1736 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1426 = getelementptr inbounds i32, i32* %1736, i32 48
  store i32 %add1425, i32* %arrayidx1426, align 4, !tbaa !7
  %1737 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1427 = getelementptr inbounds i32, i32* %1737, i32 49
  %1738 = load i32, i32* %arrayidx1427, align 4, !tbaa !7
  %1739 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1428 = getelementptr inbounds i32, i32* %1739, i32 54
  %1740 = load i32, i32* %arrayidx1428, align 4, !tbaa !7
  %add1429 = add nsw i32 %1738, %1740
  %1741 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1430 = getelementptr inbounds i32, i32* %1741, i32 49
  store i32 %add1429, i32* %arrayidx1430, align 4, !tbaa !7
  %1742 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1431 = getelementptr inbounds i32, i32* %1742, i32 50
  %1743 = load i32, i32* %arrayidx1431, align 4, !tbaa !7
  %1744 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1432 = getelementptr inbounds i32, i32* %1744, i32 53
  %1745 = load i32, i32* %arrayidx1432, align 4, !tbaa !7
  %add1433 = add nsw i32 %1743, %1745
  %1746 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1434 = getelementptr inbounds i32, i32* %1746, i32 50
  store i32 %add1433, i32* %arrayidx1434, align 4, !tbaa !7
  %1747 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1435 = getelementptr inbounds i32, i32* %1747, i32 51
  %1748 = load i32, i32* %arrayidx1435, align 4, !tbaa !7
  %1749 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1436 = getelementptr inbounds i32, i32* %1749, i32 52
  %1750 = load i32, i32* %arrayidx1436, align 4, !tbaa !7
  %add1437 = add nsw i32 %1748, %1750
  %1751 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1438 = getelementptr inbounds i32, i32* %1751, i32 51
  store i32 %add1437, i32* %arrayidx1438, align 4, !tbaa !7
  %1752 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1439 = getelementptr inbounds i32, i32* %1752, i32 52
  %1753 = load i32, i32* %arrayidx1439, align 4, !tbaa !7
  %sub1440 = sub nsw i32 0, %1753
  %1754 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1441 = getelementptr inbounds i32, i32* %1754, i32 51
  %1755 = load i32, i32* %arrayidx1441, align 4, !tbaa !7
  %add1442 = add nsw i32 %sub1440, %1755
  %1756 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1443 = getelementptr inbounds i32, i32* %1756, i32 52
  store i32 %add1442, i32* %arrayidx1443, align 4, !tbaa !7
  %1757 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1444 = getelementptr inbounds i32, i32* %1757, i32 53
  %1758 = load i32, i32* %arrayidx1444, align 4, !tbaa !7
  %sub1445 = sub nsw i32 0, %1758
  %1759 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1446 = getelementptr inbounds i32, i32* %1759, i32 50
  %1760 = load i32, i32* %arrayidx1446, align 4, !tbaa !7
  %add1447 = add nsw i32 %sub1445, %1760
  %1761 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1448 = getelementptr inbounds i32, i32* %1761, i32 53
  store i32 %add1447, i32* %arrayidx1448, align 4, !tbaa !7
  %1762 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1449 = getelementptr inbounds i32, i32* %1762, i32 54
  %1763 = load i32, i32* %arrayidx1449, align 4, !tbaa !7
  %sub1450 = sub nsw i32 0, %1763
  %1764 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1451 = getelementptr inbounds i32, i32* %1764, i32 49
  %1765 = load i32, i32* %arrayidx1451, align 4, !tbaa !7
  %add1452 = add nsw i32 %sub1450, %1765
  %1766 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1453 = getelementptr inbounds i32, i32* %1766, i32 54
  store i32 %add1452, i32* %arrayidx1453, align 4, !tbaa !7
  %1767 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1454 = getelementptr inbounds i32, i32* %1767, i32 55
  %1768 = load i32, i32* %arrayidx1454, align 4, !tbaa !7
  %sub1455 = sub nsw i32 0, %1768
  %1769 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1456 = getelementptr inbounds i32, i32* %1769, i32 48
  %1770 = load i32, i32* %arrayidx1456, align 4, !tbaa !7
  %add1457 = add nsw i32 %sub1455, %1770
  %1771 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1458 = getelementptr inbounds i32, i32* %1771, i32 55
  store i32 %add1457, i32* %arrayidx1458, align 4, !tbaa !7
  %1772 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1459 = getelementptr inbounds i32, i32* %1772, i32 56
  %1773 = load i32, i32* %arrayidx1459, align 4, !tbaa !7
  %sub1460 = sub nsw i32 0, %1773
  %1774 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1461 = getelementptr inbounds i32, i32* %1774, i32 63
  %1775 = load i32, i32* %arrayidx1461, align 4, !tbaa !7
  %add1462 = add nsw i32 %sub1460, %1775
  %1776 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1463 = getelementptr inbounds i32, i32* %1776, i32 56
  store i32 %add1462, i32* %arrayidx1463, align 4, !tbaa !7
  %1777 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1464 = getelementptr inbounds i32, i32* %1777, i32 57
  %1778 = load i32, i32* %arrayidx1464, align 4, !tbaa !7
  %sub1465 = sub nsw i32 0, %1778
  %1779 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1466 = getelementptr inbounds i32, i32* %1779, i32 62
  %1780 = load i32, i32* %arrayidx1466, align 4, !tbaa !7
  %add1467 = add nsw i32 %sub1465, %1780
  %1781 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1468 = getelementptr inbounds i32, i32* %1781, i32 57
  store i32 %add1467, i32* %arrayidx1468, align 4, !tbaa !7
  %1782 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1469 = getelementptr inbounds i32, i32* %1782, i32 58
  %1783 = load i32, i32* %arrayidx1469, align 4, !tbaa !7
  %sub1470 = sub nsw i32 0, %1783
  %1784 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1471 = getelementptr inbounds i32, i32* %1784, i32 61
  %1785 = load i32, i32* %arrayidx1471, align 4, !tbaa !7
  %add1472 = add nsw i32 %sub1470, %1785
  %1786 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1473 = getelementptr inbounds i32, i32* %1786, i32 58
  store i32 %add1472, i32* %arrayidx1473, align 4, !tbaa !7
  %1787 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1474 = getelementptr inbounds i32, i32* %1787, i32 59
  %1788 = load i32, i32* %arrayidx1474, align 4, !tbaa !7
  %sub1475 = sub nsw i32 0, %1788
  %1789 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1476 = getelementptr inbounds i32, i32* %1789, i32 60
  %1790 = load i32, i32* %arrayidx1476, align 4, !tbaa !7
  %add1477 = add nsw i32 %sub1475, %1790
  %1791 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1478 = getelementptr inbounds i32, i32* %1791, i32 59
  store i32 %add1477, i32* %arrayidx1478, align 4, !tbaa !7
  %1792 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1479 = getelementptr inbounds i32, i32* %1792, i32 60
  %1793 = load i32, i32* %arrayidx1479, align 4, !tbaa !7
  %1794 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1480 = getelementptr inbounds i32, i32* %1794, i32 59
  %1795 = load i32, i32* %arrayidx1480, align 4, !tbaa !7
  %add1481 = add nsw i32 %1793, %1795
  %1796 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1482 = getelementptr inbounds i32, i32* %1796, i32 60
  store i32 %add1481, i32* %arrayidx1482, align 4, !tbaa !7
  %1797 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1483 = getelementptr inbounds i32, i32* %1797, i32 61
  %1798 = load i32, i32* %arrayidx1483, align 4, !tbaa !7
  %1799 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1484 = getelementptr inbounds i32, i32* %1799, i32 58
  %1800 = load i32, i32* %arrayidx1484, align 4, !tbaa !7
  %add1485 = add nsw i32 %1798, %1800
  %1801 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1486 = getelementptr inbounds i32, i32* %1801, i32 61
  store i32 %add1485, i32* %arrayidx1486, align 4, !tbaa !7
  %1802 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1487 = getelementptr inbounds i32, i32* %1802, i32 62
  %1803 = load i32, i32* %arrayidx1487, align 4, !tbaa !7
  %1804 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1488 = getelementptr inbounds i32, i32* %1804, i32 57
  %1805 = load i32, i32* %arrayidx1488, align 4, !tbaa !7
  %add1489 = add nsw i32 %1803, %1805
  %1806 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1490 = getelementptr inbounds i32, i32* %1806, i32 62
  store i32 %add1489, i32* %arrayidx1490, align 4, !tbaa !7
  %1807 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1491 = getelementptr inbounds i32, i32* %1807, i32 63
  %1808 = load i32, i32* %arrayidx1491, align 4, !tbaa !7
  %1809 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1492 = getelementptr inbounds i32, i32* %1809, i32 56
  %1810 = load i32, i32* %arrayidx1492, align 4, !tbaa !7
  %add1493 = add nsw i32 %1808, %1810
  %1811 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1494 = getelementptr inbounds i32, i32* %1811, i32 63
  store i32 %add1493, i32* %arrayidx1494, align 4, !tbaa !7
  %1812 = load i32, i32* %stage, align 4, !tbaa !7
  %1813 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %1814 = load i32*, i32** %bf1, align 4, !tbaa !2
  %1815 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %1816 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx1495 = getelementptr inbounds i8, i8* %1815, i32 %1816
  %1817 = load i8, i8* %arrayidx1495, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %1812, i32* %1813, i32* %1814, i32 64, i8 signext %1817)
  %1818 = load i32, i32* %stage, align 4, !tbaa !7
  %inc1496 = add nsw i32 %1818, 1
  store i32 %inc1496, i32* %stage, align 4, !tbaa !7
  %1819 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1497 = sext i8 %1819 to i32
  %call1498 = call i32* @cospi_arr(i32 %conv1497)
  store i32* %call1498, i32** %cospi, align 4, !tbaa !2
  %1820 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %1820, i32** %bf0, align 4, !tbaa !2
  %arraydecay1499 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay1499, i32** %bf1, align 4, !tbaa !2
  %1821 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1500 = getelementptr inbounds i32, i32* %1821, i32 32
  %1822 = load i32, i32* %arrayidx1500, align 4, !tbaa !7
  %1823 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1501 = getelementptr inbounds i32, i32* %1823, i32 0
  %1824 = load i32, i32* %arrayidx1501, align 4, !tbaa !7
  %1825 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1502 = getelementptr inbounds i32, i32* %1825, i32 32
  %1826 = load i32, i32* %arrayidx1502, align 4, !tbaa !7
  %1827 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1503 = getelementptr inbounds i32, i32* %1827, i32 1
  %1828 = load i32, i32* %arrayidx1503, align 4, !tbaa !7
  %1829 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1504 = sext i8 %1829 to i32
  %call1505 = call i32 @half_btf(i32 %1822, i32 %1824, i32 %1826, i32 %1828, i32 %conv1504)
  %1830 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1506 = getelementptr inbounds i32, i32* %1830, i32 0
  store i32 %call1505, i32* %arrayidx1506, align 4, !tbaa !7
  %1831 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1507 = getelementptr inbounds i32, i32* %1831, i32 32
  %1832 = load i32, i32* %arrayidx1507, align 4, !tbaa !7
  %sub1508 = sub nsw i32 0, %1832
  %1833 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1509 = getelementptr inbounds i32, i32* %1833, i32 1
  %1834 = load i32, i32* %arrayidx1509, align 4, !tbaa !7
  %1835 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1510 = getelementptr inbounds i32, i32* %1835, i32 32
  %1836 = load i32, i32* %arrayidx1510, align 4, !tbaa !7
  %1837 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1511 = getelementptr inbounds i32, i32* %1837, i32 0
  %1838 = load i32, i32* %arrayidx1511, align 4, !tbaa !7
  %1839 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1512 = sext i8 %1839 to i32
  %call1513 = call i32 @half_btf(i32 %sub1508, i32 %1834, i32 %1836, i32 %1838, i32 %conv1512)
  %1840 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1514 = getelementptr inbounds i32, i32* %1840, i32 1
  store i32 %call1513, i32* %arrayidx1514, align 4, !tbaa !7
  %1841 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1515 = getelementptr inbounds i32, i32* %1841, i32 48
  %1842 = load i32, i32* %arrayidx1515, align 4, !tbaa !7
  %1843 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1516 = getelementptr inbounds i32, i32* %1843, i32 2
  %1844 = load i32, i32* %arrayidx1516, align 4, !tbaa !7
  %1845 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1517 = getelementptr inbounds i32, i32* %1845, i32 16
  %1846 = load i32, i32* %arrayidx1517, align 4, !tbaa !7
  %1847 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1518 = getelementptr inbounds i32, i32* %1847, i32 3
  %1848 = load i32, i32* %arrayidx1518, align 4, !tbaa !7
  %1849 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1519 = sext i8 %1849 to i32
  %call1520 = call i32 @half_btf(i32 %1842, i32 %1844, i32 %1846, i32 %1848, i32 %conv1519)
  %1850 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1521 = getelementptr inbounds i32, i32* %1850, i32 2
  store i32 %call1520, i32* %arrayidx1521, align 4, !tbaa !7
  %1851 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1522 = getelementptr inbounds i32, i32* %1851, i32 48
  %1852 = load i32, i32* %arrayidx1522, align 4, !tbaa !7
  %1853 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1523 = getelementptr inbounds i32, i32* %1853, i32 3
  %1854 = load i32, i32* %arrayidx1523, align 4, !tbaa !7
  %1855 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1524 = getelementptr inbounds i32, i32* %1855, i32 16
  %1856 = load i32, i32* %arrayidx1524, align 4, !tbaa !7
  %sub1525 = sub nsw i32 0, %1856
  %1857 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1526 = getelementptr inbounds i32, i32* %1857, i32 2
  %1858 = load i32, i32* %arrayidx1526, align 4, !tbaa !7
  %1859 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1527 = sext i8 %1859 to i32
  %call1528 = call i32 @half_btf(i32 %1852, i32 %1854, i32 %sub1525, i32 %1858, i32 %conv1527)
  %1860 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1529 = getelementptr inbounds i32, i32* %1860, i32 3
  store i32 %call1528, i32* %arrayidx1529, align 4, !tbaa !7
  %1861 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1530 = getelementptr inbounds i32, i32* %1861, i32 4
  %1862 = load i32, i32* %arrayidx1530, align 4, !tbaa !7
  %1863 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1531 = getelementptr inbounds i32, i32* %1863, i32 5
  %1864 = load i32, i32* %arrayidx1531, align 4, !tbaa !7
  %add1532 = add nsw i32 %1862, %1864
  %1865 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1533 = getelementptr inbounds i32, i32* %1865, i32 4
  store i32 %add1532, i32* %arrayidx1533, align 4, !tbaa !7
  %1866 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1534 = getelementptr inbounds i32, i32* %1866, i32 5
  %1867 = load i32, i32* %arrayidx1534, align 4, !tbaa !7
  %sub1535 = sub nsw i32 0, %1867
  %1868 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1536 = getelementptr inbounds i32, i32* %1868, i32 4
  %1869 = load i32, i32* %arrayidx1536, align 4, !tbaa !7
  %add1537 = add nsw i32 %sub1535, %1869
  %1870 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1538 = getelementptr inbounds i32, i32* %1870, i32 5
  store i32 %add1537, i32* %arrayidx1538, align 4, !tbaa !7
  %1871 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1539 = getelementptr inbounds i32, i32* %1871, i32 6
  %1872 = load i32, i32* %arrayidx1539, align 4, !tbaa !7
  %sub1540 = sub nsw i32 0, %1872
  %1873 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1541 = getelementptr inbounds i32, i32* %1873, i32 7
  %1874 = load i32, i32* %arrayidx1541, align 4, !tbaa !7
  %add1542 = add nsw i32 %sub1540, %1874
  %1875 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1543 = getelementptr inbounds i32, i32* %1875, i32 6
  store i32 %add1542, i32* %arrayidx1543, align 4, !tbaa !7
  %1876 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1544 = getelementptr inbounds i32, i32* %1876, i32 7
  %1877 = load i32, i32* %arrayidx1544, align 4, !tbaa !7
  %1878 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1545 = getelementptr inbounds i32, i32* %1878, i32 6
  %1879 = load i32, i32* %arrayidx1545, align 4, !tbaa !7
  %add1546 = add nsw i32 %1877, %1879
  %1880 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1547 = getelementptr inbounds i32, i32* %1880, i32 7
  store i32 %add1546, i32* %arrayidx1547, align 4, !tbaa !7
  %1881 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1548 = getelementptr inbounds i32, i32* %1881, i32 8
  %1882 = load i32, i32* %arrayidx1548, align 4, !tbaa !7
  %1883 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1549 = getelementptr inbounds i32, i32* %1883, i32 8
  store i32 %1882, i32* %arrayidx1549, align 4, !tbaa !7
  %1884 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1550 = getelementptr inbounds i32, i32* %1884, i32 16
  %1885 = load i32, i32* %arrayidx1550, align 4, !tbaa !7
  %sub1551 = sub nsw i32 0, %1885
  %1886 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1552 = getelementptr inbounds i32, i32* %1886, i32 9
  %1887 = load i32, i32* %arrayidx1552, align 4, !tbaa !7
  %1888 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1553 = getelementptr inbounds i32, i32* %1888, i32 48
  %1889 = load i32, i32* %arrayidx1553, align 4, !tbaa !7
  %1890 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1554 = getelementptr inbounds i32, i32* %1890, i32 14
  %1891 = load i32, i32* %arrayidx1554, align 4, !tbaa !7
  %1892 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1555 = sext i8 %1892 to i32
  %call1556 = call i32 @half_btf(i32 %sub1551, i32 %1887, i32 %1889, i32 %1891, i32 %conv1555)
  %1893 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1557 = getelementptr inbounds i32, i32* %1893, i32 9
  store i32 %call1556, i32* %arrayidx1557, align 4, !tbaa !7
  %1894 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1558 = getelementptr inbounds i32, i32* %1894, i32 48
  %1895 = load i32, i32* %arrayidx1558, align 4, !tbaa !7
  %sub1559 = sub nsw i32 0, %1895
  %1896 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1560 = getelementptr inbounds i32, i32* %1896, i32 10
  %1897 = load i32, i32* %arrayidx1560, align 4, !tbaa !7
  %1898 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1561 = getelementptr inbounds i32, i32* %1898, i32 16
  %1899 = load i32, i32* %arrayidx1561, align 4, !tbaa !7
  %sub1562 = sub nsw i32 0, %1899
  %1900 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1563 = getelementptr inbounds i32, i32* %1900, i32 13
  %1901 = load i32, i32* %arrayidx1563, align 4, !tbaa !7
  %1902 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1564 = sext i8 %1902 to i32
  %call1565 = call i32 @half_btf(i32 %sub1559, i32 %1897, i32 %sub1562, i32 %1901, i32 %conv1564)
  %1903 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1566 = getelementptr inbounds i32, i32* %1903, i32 10
  store i32 %call1565, i32* %arrayidx1566, align 4, !tbaa !7
  %1904 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1567 = getelementptr inbounds i32, i32* %1904, i32 11
  %1905 = load i32, i32* %arrayidx1567, align 4, !tbaa !7
  %1906 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1568 = getelementptr inbounds i32, i32* %1906, i32 11
  store i32 %1905, i32* %arrayidx1568, align 4, !tbaa !7
  %1907 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1569 = getelementptr inbounds i32, i32* %1907, i32 12
  %1908 = load i32, i32* %arrayidx1569, align 4, !tbaa !7
  %1909 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1570 = getelementptr inbounds i32, i32* %1909, i32 12
  store i32 %1908, i32* %arrayidx1570, align 4, !tbaa !7
  %1910 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1571 = getelementptr inbounds i32, i32* %1910, i32 48
  %1911 = load i32, i32* %arrayidx1571, align 4, !tbaa !7
  %1912 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1572 = getelementptr inbounds i32, i32* %1912, i32 13
  %1913 = load i32, i32* %arrayidx1572, align 4, !tbaa !7
  %1914 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1573 = getelementptr inbounds i32, i32* %1914, i32 16
  %1915 = load i32, i32* %arrayidx1573, align 4, !tbaa !7
  %sub1574 = sub nsw i32 0, %1915
  %1916 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1575 = getelementptr inbounds i32, i32* %1916, i32 10
  %1917 = load i32, i32* %arrayidx1575, align 4, !tbaa !7
  %1918 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1576 = sext i8 %1918 to i32
  %call1577 = call i32 @half_btf(i32 %1911, i32 %1913, i32 %sub1574, i32 %1917, i32 %conv1576)
  %1919 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1578 = getelementptr inbounds i32, i32* %1919, i32 13
  store i32 %call1577, i32* %arrayidx1578, align 4, !tbaa !7
  %1920 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1579 = getelementptr inbounds i32, i32* %1920, i32 16
  %1921 = load i32, i32* %arrayidx1579, align 4, !tbaa !7
  %1922 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1580 = getelementptr inbounds i32, i32* %1922, i32 14
  %1923 = load i32, i32* %arrayidx1580, align 4, !tbaa !7
  %1924 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1581 = getelementptr inbounds i32, i32* %1924, i32 48
  %1925 = load i32, i32* %arrayidx1581, align 4, !tbaa !7
  %1926 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1582 = getelementptr inbounds i32, i32* %1926, i32 9
  %1927 = load i32, i32* %arrayidx1582, align 4, !tbaa !7
  %1928 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1583 = sext i8 %1928 to i32
  %call1584 = call i32 @half_btf(i32 %1921, i32 %1923, i32 %1925, i32 %1927, i32 %conv1583)
  %1929 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1585 = getelementptr inbounds i32, i32* %1929, i32 14
  store i32 %call1584, i32* %arrayidx1585, align 4, !tbaa !7
  %1930 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1586 = getelementptr inbounds i32, i32* %1930, i32 15
  %1931 = load i32, i32* %arrayidx1586, align 4, !tbaa !7
  %1932 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1587 = getelementptr inbounds i32, i32* %1932, i32 15
  store i32 %1931, i32* %arrayidx1587, align 4, !tbaa !7
  %1933 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1588 = getelementptr inbounds i32, i32* %1933, i32 16
  %1934 = load i32, i32* %arrayidx1588, align 4, !tbaa !7
  %1935 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1589 = getelementptr inbounds i32, i32* %1935, i32 19
  %1936 = load i32, i32* %arrayidx1589, align 4, !tbaa !7
  %add1590 = add nsw i32 %1934, %1936
  %1937 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1591 = getelementptr inbounds i32, i32* %1937, i32 16
  store i32 %add1590, i32* %arrayidx1591, align 4, !tbaa !7
  %1938 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1592 = getelementptr inbounds i32, i32* %1938, i32 17
  %1939 = load i32, i32* %arrayidx1592, align 4, !tbaa !7
  %1940 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1593 = getelementptr inbounds i32, i32* %1940, i32 18
  %1941 = load i32, i32* %arrayidx1593, align 4, !tbaa !7
  %add1594 = add nsw i32 %1939, %1941
  %1942 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1595 = getelementptr inbounds i32, i32* %1942, i32 17
  store i32 %add1594, i32* %arrayidx1595, align 4, !tbaa !7
  %1943 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1596 = getelementptr inbounds i32, i32* %1943, i32 18
  %1944 = load i32, i32* %arrayidx1596, align 4, !tbaa !7
  %sub1597 = sub nsw i32 0, %1944
  %1945 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1598 = getelementptr inbounds i32, i32* %1945, i32 17
  %1946 = load i32, i32* %arrayidx1598, align 4, !tbaa !7
  %add1599 = add nsw i32 %sub1597, %1946
  %1947 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1600 = getelementptr inbounds i32, i32* %1947, i32 18
  store i32 %add1599, i32* %arrayidx1600, align 4, !tbaa !7
  %1948 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1601 = getelementptr inbounds i32, i32* %1948, i32 19
  %1949 = load i32, i32* %arrayidx1601, align 4, !tbaa !7
  %sub1602 = sub nsw i32 0, %1949
  %1950 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1603 = getelementptr inbounds i32, i32* %1950, i32 16
  %1951 = load i32, i32* %arrayidx1603, align 4, !tbaa !7
  %add1604 = add nsw i32 %sub1602, %1951
  %1952 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1605 = getelementptr inbounds i32, i32* %1952, i32 19
  store i32 %add1604, i32* %arrayidx1605, align 4, !tbaa !7
  %1953 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1606 = getelementptr inbounds i32, i32* %1953, i32 20
  %1954 = load i32, i32* %arrayidx1606, align 4, !tbaa !7
  %sub1607 = sub nsw i32 0, %1954
  %1955 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1608 = getelementptr inbounds i32, i32* %1955, i32 23
  %1956 = load i32, i32* %arrayidx1608, align 4, !tbaa !7
  %add1609 = add nsw i32 %sub1607, %1956
  %1957 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1610 = getelementptr inbounds i32, i32* %1957, i32 20
  store i32 %add1609, i32* %arrayidx1610, align 4, !tbaa !7
  %1958 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1611 = getelementptr inbounds i32, i32* %1958, i32 21
  %1959 = load i32, i32* %arrayidx1611, align 4, !tbaa !7
  %sub1612 = sub nsw i32 0, %1959
  %1960 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1613 = getelementptr inbounds i32, i32* %1960, i32 22
  %1961 = load i32, i32* %arrayidx1613, align 4, !tbaa !7
  %add1614 = add nsw i32 %sub1612, %1961
  %1962 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1615 = getelementptr inbounds i32, i32* %1962, i32 21
  store i32 %add1614, i32* %arrayidx1615, align 4, !tbaa !7
  %1963 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1616 = getelementptr inbounds i32, i32* %1963, i32 22
  %1964 = load i32, i32* %arrayidx1616, align 4, !tbaa !7
  %1965 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1617 = getelementptr inbounds i32, i32* %1965, i32 21
  %1966 = load i32, i32* %arrayidx1617, align 4, !tbaa !7
  %add1618 = add nsw i32 %1964, %1966
  %1967 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1619 = getelementptr inbounds i32, i32* %1967, i32 22
  store i32 %add1618, i32* %arrayidx1619, align 4, !tbaa !7
  %1968 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1620 = getelementptr inbounds i32, i32* %1968, i32 23
  %1969 = load i32, i32* %arrayidx1620, align 4, !tbaa !7
  %1970 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1621 = getelementptr inbounds i32, i32* %1970, i32 20
  %1971 = load i32, i32* %arrayidx1621, align 4, !tbaa !7
  %add1622 = add nsw i32 %1969, %1971
  %1972 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1623 = getelementptr inbounds i32, i32* %1972, i32 23
  store i32 %add1622, i32* %arrayidx1623, align 4, !tbaa !7
  %1973 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1624 = getelementptr inbounds i32, i32* %1973, i32 24
  %1974 = load i32, i32* %arrayidx1624, align 4, !tbaa !7
  %1975 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1625 = getelementptr inbounds i32, i32* %1975, i32 27
  %1976 = load i32, i32* %arrayidx1625, align 4, !tbaa !7
  %add1626 = add nsw i32 %1974, %1976
  %1977 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1627 = getelementptr inbounds i32, i32* %1977, i32 24
  store i32 %add1626, i32* %arrayidx1627, align 4, !tbaa !7
  %1978 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1628 = getelementptr inbounds i32, i32* %1978, i32 25
  %1979 = load i32, i32* %arrayidx1628, align 4, !tbaa !7
  %1980 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1629 = getelementptr inbounds i32, i32* %1980, i32 26
  %1981 = load i32, i32* %arrayidx1629, align 4, !tbaa !7
  %add1630 = add nsw i32 %1979, %1981
  %1982 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1631 = getelementptr inbounds i32, i32* %1982, i32 25
  store i32 %add1630, i32* %arrayidx1631, align 4, !tbaa !7
  %1983 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1632 = getelementptr inbounds i32, i32* %1983, i32 26
  %1984 = load i32, i32* %arrayidx1632, align 4, !tbaa !7
  %sub1633 = sub nsw i32 0, %1984
  %1985 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1634 = getelementptr inbounds i32, i32* %1985, i32 25
  %1986 = load i32, i32* %arrayidx1634, align 4, !tbaa !7
  %add1635 = add nsw i32 %sub1633, %1986
  %1987 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1636 = getelementptr inbounds i32, i32* %1987, i32 26
  store i32 %add1635, i32* %arrayidx1636, align 4, !tbaa !7
  %1988 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1637 = getelementptr inbounds i32, i32* %1988, i32 27
  %1989 = load i32, i32* %arrayidx1637, align 4, !tbaa !7
  %sub1638 = sub nsw i32 0, %1989
  %1990 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1639 = getelementptr inbounds i32, i32* %1990, i32 24
  %1991 = load i32, i32* %arrayidx1639, align 4, !tbaa !7
  %add1640 = add nsw i32 %sub1638, %1991
  %1992 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1641 = getelementptr inbounds i32, i32* %1992, i32 27
  store i32 %add1640, i32* %arrayidx1641, align 4, !tbaa !7
  %1993 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1642 = getelementptr inbounds i32, i32* %1993, i32 28
  %1994 = load i32, i32* %arrayidx1642, align 4, !tbaa !7
  %sub1643 = sub nsw i32 0, %1994
  %1995 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1644 = getelementptr inbounds i32, i32* %1995, i32 31
  %1996 = load i32, i32* %arrayidx1644, align 4, !tbaa !7
  %add1645 = add nsw i32 %sub1643, %1996
  %1997 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1646 = getelementptr inbounds i32, i32* %1997, i32 28
  store i32 %add1645, i32* %arrayidx1646, align 4, !tbaa !7
  %1998 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1647 = getelementptr inbounds i32, i32* %1998, i32 29
  %1999 = load i32, i32* %arrayidx1647, align 4, !tbaa !7
  %sub1648 = sub nsw i32 0, %1999
  %2000 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1649 = getelementptr inbounds i32, i32* %2000, i32 30
  %2001 = load i32, i32* %arrayidx1649, align 4, !tbaa !7
  %add1650 = add nsw i32 %sub1648, %2001
  %2002 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1651 = getelementptr inbounds i32, i32* %2002, i32 29
  store i32 %add1650, i32* %arrayidx1651, align 4, !tbaa !7
  %2003 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1652 = getelementptr inbounds i32, i32* %2003, i32 30
  %2004 = load i32, i32* %arrayidx1652, align 4, !tbaa !7
  %2005 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1653 = getelementptr inbounds i32, i32* %2005, i32 29
  %2006 = load i32, i32* %arrayidx1653, align 4, !tbaa !7
  %add1654 = add nsw i32 %2004, %2006
  %2007 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1655 = getelementptr inbounds i32, i32* %2007, i32 30
  store i32 %add1654, i32* %arrayidx1655, align 4, !tbaa !7
  %2008 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1656 = getelementptr inbounds i32, i32* %2008, i32 31
  %2009 = load i32, i32* %arrayidx1656, align 4, !tbaa !7
  %2010 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1657 = getelementptr inbounds i32, i32* %2010, i32 28
  %2011 = load i32, i32* %arrayidx1657, align 4, !tbaa !7
  %add1658 = add nsw i32 %2009, %2011
  %2012 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1659 = getelementptr inbounds i32, i32* %2012, i32 31
  store i32 %add1658, i32* %arrayidx1659, align 4, !tbaa !7
  %2013 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1660 = getelementptr inbounds i32, i32* %2013, i32 32
  %2014 = load i32, i32* %arrayidx1660, align 4, !tbaa !7
  %2015 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1661 = getelementptr inbounds i32, i32* %2015, i32 32
  store i32 %2014, i32* %arrayidx1661, align 4, !tbaa !7
  %2016 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1662 = getelementptr inbounds i32, i32* %2016, i32 33
  %2017 = load i32, i32* %arrayidx1662, align 4, !tbaa !7
  %2018 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1663 = getelementptr inbounds i32, i32* %2018, i32 33
  store i32 %2017, i32* %arrayidx1663, align 4, !tbaa !7
  %2019 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1664 = getelementptr inbounds i32, i32* %2019, i32 8
  %2020 = load i32, i32* %arrayidx1664, align 4, !tbaa !7
  %sub1665 = sub nsw i32 0, %2020
  %2021 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1666 = getelementptr inbounds i32, i32* %2021, i32 34
  %2022 = load i32, i32* %arrayidx1666, align 4, !tbaa !7
  %2023 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1667 = getelementptr inbounds i32, i32* %2023, i32 56
  %2024 = load i32, i32* %arrayidx1667, align 4, !tbaa !7
  %2025 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1668 = getelementptr inbounds i32, i32* %2025, i32 61
  %2026 = load i32, i32* %arrayidx1668, align 4, !tbaa !7
  %2027 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1669 = sext i8 %2027 to i32
  %call1670 = call i32 @half_btf(i32 %sub1665, i32 %2022, i32 %2024, i32 %2026, i32 %conv1669)
  %2028 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1671 = getelementptr inbounds i32, i32* %2028, i32 34
  store i32 %call1670, i32* %arrayidx1671, align 4, !tbaa !7
  %2029 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1672 = getelementptr inbounds i32, i32* %2029, i32 8
  %2030 = load i32, i32* %arrayidx1672, align 4, !tbaa !7
  %sub1673 = sub nsw i32 0, %2030
  %2031 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1674 = getelementptr inbounds i32, i32* %2031, i32 35
  %2032 = load i32, i32* %arrayidx1674, align 4, !tbaa !7
  %2033 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1675 = getelementptr inbounds i32, i32* %2033, i32 56
  %2034 = load i32, i32* %arrayidx1675, align 4, !tbaa !7
  %2035 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1676 = getelementptr inbounds i32, i32* %2035, i32 60
  %2036 = load i32, i32* %arrayidx1676, align 4, !tbaa !7
  %2037 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1677 = sext i8 %2037 to i32
  %call1678 = call i32 @half_btf(i32 %sub1673, i32 %2032, i32 %2034, i32 %2036, i32 %conv1677)
  %2038 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1679 = getelementptr inbounds i32, i32* %2038, i32 35
  store i32 %call1678, i32* %arrayidx1679, align 4, !tbaa !7
  %2039 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1680 = getelementptr inbounds i32, i32* %2039, i32 56
  %2040 = load i32, i32* %arrayidx1680, align 4, !tbaa !7
  %sub1681 = sub nsw i32 0, %2040
  %2041 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1682 = getelementptr inbounds i32, i32* %2041, i32 36
  %2042 = load i32, i32* %arrayidx1682, align 4, !tbaa !7
  %2043 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1683 = getelementptr inbounds i32, i32* %2043, i32 8
  %2044 = load i32, i32* %arrayidx1683, align 4, !tbaa !7
  %sub1684 = sub nsw i32 0, %2044
  %2045 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1685 = getelementptr inbounds i32, i32* %2045, i32 59
  %2046 = load i32, i32* %arrayidx1685, align 4, !tbaa !7
  %2047 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1686 = sext i8 %2047 to i32
  %call1687 = call i32 @half_btf(i32 %sub1681, i32 %2042, i32 %sub1684, i32 %2046, i32 %conv1686)
  %2048 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1688 = getelementptr inbounds i32, i32* %2048, i32 36
  store i32 %call1687, i32* %arrayidx1688, align 4, !tbaa !7
  %2049 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1689 = getelementptr inbounds i32, i32* %2049, i32 56
  %2050 = load i32, i32* %arrayidx1689, align 4, !tbaa !7
  %sub1690 = sub nsw i32 0, %2050
  %2051 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1691 = getelementptr inbounds i32, i32* %2051, i32 37
  %2052 = load i32, i32* %arrayidx1691, align 4, !tbaa !7
  %2053 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1692 = getelementptr inbounds i32, i32* %2053, i32 8
  %2054 = load i32, i32* %arrayidx1692, align 4, !tbaa !7
  %sub1693 = sub nsw i32 0, %2054
  %2055 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1694 = getelementptr inbounds i32, i32* %2055, i32 58
  %2056 = load i32, i32* %arrayidx1694, align 4, !tbaa !7
  %2057 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1695 = sext i8 %2057 to i32
  %call1696 = call i32 @half_btf(i32 %sub1690, i32 %2052, i32 %sub1693, i32 %2056, i32 %conv1695)
  %2058 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1697 = getelementptr inbounds i32, i32* %2058, i32 37
  store i32 %call1696, i32* %arrayidx1697, align 4, !tbaa !7
  %2059 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1698 = getelementptr inbounds i32, i32* %2059, i32 38
  %2060 = load i32, i32* %arrayidx1698, align 4, !tbaa !7
  %2061 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1699 = getelementptr inbounds i32, i32* %2061, i32 38
  store i32 %2060, i32* %arrayidx1699, align 4, !tbaa !7
  %2062 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1700 = getelementptr inbounds i32, i32* %2062, i32 39
  %2063 = load i32, i32* %arrayidx1700, align 4, !tbaa !7
  %2064 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1701 = getelementptr inbounds i32, i32* %2064, i32 39
  store i32 %2063, i32* %arrayidx1701, align 4, !tbaa !7
  %2065 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1702 = getelementptr inbounds i32, i32* %2065, i32 40
  %2066 = load i32, i32* %arrayidx1702, align 4, !tbaa !7
  %2067 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1703 = getelementptr inbounds i32, i32* %2067, i32 40
  store i32 %2066, i32* %arrayidx1703, align 4, !tbaa !7
  %2068 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1704 = getelementptr inbounds i32, i32* %2068, i32 41
  %2069 = load i32, i32* %arrayidx1704, align 4, !tbaa !7
  %2070 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1705 = getelementptr inbounds i32, i32* %2070, i32 41
  store i32 %2069, i32* %arrayidx1705, align 4, !tbaa !7
  %2071 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1706 = getelementptr inbounds i32, i32* %2071, i32 40
  %2072 = load i32, i32* %arrayidx1706, align 4, !tbaa !7
  %sub1707 = sub nsw i32 0, %2072
  %2073 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1708 = getelementptr inbounds i32, i32* %2073, i32 42
  %2074 = load i32, i32* %arrayidx1708, align 4, !tbaa !7
  %2075 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1709 = getelementptr inbounds i32, i32* %2075, i32 24
  %2076 = load i32, i32* %arrayidx1709, align 4, !tbaa !7
  %2077 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1710 = getelementptr inbounds i32, i32* %2077, i32 53
  %2078 = load i32, i32* %arrayidx1710, align 4, !tbaa !7
  %2079 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1711 = sext i8 %2079 to i32
  %call1712 = call i32 @half_btf(i32 %sub1707, i32 %2074, i32 %2076, i32 %2078, i32 %conv1711)
  %2080 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1713 = getelementptr inbounds i32, i32* %2080, i32 42
  store i32 %call1712, i32* %arrayidx1713, align 4, !tbaa !7
  %2081 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1714 = getelementptr inbounds i32, i32* %2081, i32 40
  %2082 = load i32, i32* %arrayidx1714, align 4, !tbaa !7
  %sub1715 = sub nsw i32 0, %2082
  %2083 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1716 = getelementptr inbounds i32, i32* %2083, i32 43
  %2084 = load i32, i32* %arrayidx1716, align 4, !tbaa !7
  %2085 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1717 = getelementptr inbounds i32, i32* %2085, i32 24
  %2086 = load i32, i32* %arrayidx1717, align 4, !tbaa !7
  %2087 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1718 = getelementptr inbounds i32, i32* %2087, i32 52
  %2088 = load i32, i32* %arrayidx1718, align 4, !tbaa !7
  %2089 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1719 = sext i8 %2089 to i32
  %call1720 = call i32 @half_btf(i32 %sub1715, i32 %2084, i32 %2086, i32 %2088, i32 %conv1719)
  %2090 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1721 = getelementptr inbounds i32, i32* %2090, i32 43
  store i32 %call1720, i32* %arrayidx1721, align 4, !tbaa !7
  %2091 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1722 = getelementptr inbounds i32, i32* %2091, i32 24
  %2092 = load i32, i32* %arrayidx1722, align 4, !tbaa !7
  %sub1723 = sub nsw i32 0, %2092
  %2093 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1724 = getelementptr inbounds i32, i32* %2093, i32 44
  %2094 = load i32, i32* %arrayidx1724, align 4, !tbaa !7
  %2095 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1725 = getelementptr inbounds i32, i32* %2095, i32 40
  %2096 = load i32, i32* %arrayidx1725, align 4, !tbaa !7
  %sub1726 = sub nsw i32 0, %2096
  %2097 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1727 = getelementptr inbounds i32, i32* %2097, i32 51
  %2098 = load i32, i32* %arrayidx1727, align 4, !tbaa !7
  %2099 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1728 = sext i8 %2099 to i32
  %call1729 = call i32 @half_btf(i32 %sub1723, i32 %2094, i32 %sub1726, i32 %2098, i32 %conv1728)
  %2100 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1730 = getelementptr inbounds i32, i32* %2100, i32 44
  store i32 %call1729, i32* %arrayidx1730, align 4, !tbaa !7
  %2101 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1731 = getelementptr inbounds i32, i32* %2101, i32 24
  %2102 = load i32, i32* %arrayidx1731, align 4, !tbaa !7
  %sub1732 = sub nsw i32 0, %2102
  %2103 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1733 = getelementptr inbounds i32, i32* %2103, i32 45
  %2104 = load i32, i32* %arrayidx1733, align 4, !tbaa !7
  %2105 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1734 = getelementptr inbounds i32, i32* %2105, i32 40
  %2106 = load i32, i32* %arrayidx1734, align 4, !tbaa !7
  %sub1735 = sub nsw i32 0, %2106
  %2107 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1736 = getelementptr inbounds i32, i32* %2107, i32 50
  %2108 = load i32, i32* %arrayidx1736, align 4, !tbaa !7
  %2109 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1737 = sext i8 %2109 to i32
  %call1738 = call i32 @half_btf(i32 %sub1732, i32 %2104, i32 %sub1735, i32 %2108, i32 %conv1737)
  %2110 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1739 = getelementptr inbounds i32, i32* %2110, i32 45
  store i32 %call1738, i32* %arrayidx1739, align 4, !tbaa !7
  %2111 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1740 = getelementptr inbounds i32, i32* %2111, i32 46
  %2112 = load i32, i32* %arrayidx1740, align 4, !tbaa !7
  %2113 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1741 = getelementptr inbounds i32, i32* %2113, i32 46
  store i32 %2112, i32* %arrayidx1741, align 4, !tbaa !7
  %2114 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1742 = getelementptr inbounds i32, i32* %2114, i32 47
  %2115 = load i32, i32* %arrayidx1742, align 4, !tbaa !7
  %2116 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1743 = getelementptr inbounds i32, i32* %2116, i32 47
  store i32 %2115, i32* %arrayidx1743, align 4, !tbaa !7
  %2117 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1744 = getelementptr inbounds i32, i32* %2117, i32 48
  %2118 = load i32, i32* %arrayidx1744, align 4, !tbaa !7
  %2119 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1745 = getelementptr inbounds i32, i32* %2119, i32 48
  store i32 %2118, i32* %arrayidx1745, align 4, !tbaa !7
  %2120 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1746 = getelementptr inbounds i32, i32* %2120, i32 49
  %2121 = load i32, i32* %arrayidx1746, align 4, !tbaa !7
  %2122 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1747 = getelementptr inbounds i32, i32* %2122, i32 49
  store i32 %2121, i32* %arrayidx1747, align 4, !tbaa !7
  %2123 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1748 = getelementptr inbounds i32, i32* %2123, i32 24
  %2124 = load i32, i32* %arrayidx1748, align 4, !tbaa !7
  %2125 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1749 = getelementptr inbounds i32, i32* %2125, i32 50
  %2126 = load i32, i32* %arrayidx1749, align 4, !tbaa !7
  %2127 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1750 = getelementptr inbounds i32, i32* %2127, i32 40
  %2128 = load i32, i32* %arrayidx1750, align 4, !tbaa !7
  %sub1751 = sub nsw i32 0, %2128
  %2129 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1752 = getelementptr inbounds i32, i32* %2129, i32 45
  %2130 = load i32, i32* %arrayidx1752, align 4, !tbaa !7
  %2131 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1753 = sext i8 %2131 to i32
  %call1754 = call i32 @half_btf(i32 %2124, i32 %2126, i32 %sub1751, i32 %2130, i32 %conv1753)
  %2132 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1755 = getelementptr inbounds i32, i32* %2132, i32 50
  store i32 %call1754, i32* %arrayidx1755, align 4, !tbaa !7
  %2133 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1756 = getelementptr inbounds i32, i32* %2133, i32 24
  %2134 = load i32, i32* %arrayidx1756, align 4, !tbaa !7
  %2135 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1757 = getelementptr inbounds i32, i32* %2135, i32 51
  %2136 = load i32, i32* %arrayidx1757, align 4, !tbaa !7
  %2137 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1758 = getelementptr inbounds i32, i32* %2137, i32 40
  %2138 = load i32, i32* %arrayidx1758, align 4, !tbaa !7
  %sub1759 = sub nsw i32 0, %2138
  %2139 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1760 = getelementptr inbounds i32, i32* %2139, i32 44
  %2140 = load i32, i32* %arrayidx1760, align 4, !tbaa !7
  %2141 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1761 = sext i8 %2141 to i32
  %call1762 = call i32 @half_btf(i32 %2134, i32 %2136, i32 %sub1759, i32 %2140, i32 %conv1761)
  %2142 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1763 = getelementptr inbounds i32, i32* %2142, i32 51
  store i32 %call1762, i32* %arrayidx1763, align 4, !tbaa !7
  %2143 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1764 = getelementptr inbounds i32, i32* %2143, i32 40
  %2144 = load i32, i32* %arrayidx1764, align 4, !tbaa !7
  %2145 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1765 = getelementptr inbounds i32, i32* %2145, i32 52
  %2146 = load i32, i32* %arrayidx1765, align 4, !tbaa !7
  %2147 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1766 = getelementptr inbounds i32, i32* %2147, i32 24
  %2148 = load i32, i32* %arrayidx1766, align 4, !tbaa !7
  %2149 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1767 = getelementptr inbounds i32, i32* %2149, i32 43
  %2150 = load i32, i32* %arrayidx1767, align 4, !tbaa !7
  %2151 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1768 = sext i8 %2151 to i32
  %call1769 = call i32 @half_btf(i32 %2144, i32 %2146, i32 %2148, i32 %2150, i32 %conv1768)
  %2152 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1770 = getelementptr inbounds i32, i32* %2152, i32 52
  store i32 %call1769, i32* %arrayidx1770, align 4, !tbaa !7
  %2153 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1771 = getelementptr inbounds i32, i32* %2153, i32 40
  %2154 = load i32, i32* %arrayidx1771, align 4, !tbaa !7
  %2155 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1772 = getelementptr inbounds i32, i32* %2155, i32 53
  %2156 = load i32, i32* %arrayidx1772, align 4, !tbaa !7
  %2157 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1773 = getelementptr inbounds i32, i32* %2157, i32 24
  %2158 = load i32, i32* %arrayidx1773, align 4, !tbaa !7
  %2159 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1774 = getelementptr inbounds i32, i32* %2159, i32 42
  %2160 = load i32, i32* %arrayidx1774, align 4, !tbaa !7
  %2161 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1775 = sext i8 %2161 to i32
  %call1776 = call i32 @half_btf(i32 %2154, i32 %2156, i32 %2158, i32 %2160, i32 %conv1775)
  %2162 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1777 = getelementptr inbounds i32, i32* %2162, i32 53
  store i32 %call1776, i32* %arrayidx1777, align 4, !tbaa !7
  %2163 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1778 = getelementptr inbounds i32, i32* %2163, i32 54
  %2164 = load i32, i32* %arrayidx1778, align 4, !tbaa !7
  %2165 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1779 = getelementptr inbounds i32, i32* %2165, i32 54
  store i32 %2164, i32* %arrayidx1779, align 4, !tbaa !7
  %2166 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1780 = getelementptr inbounds i32, i32* %2166, i32 55
  %2167 = load i32, i32* %arrayidx1780, align 4, !tbaa !7
  %2168 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1781 = getelementptr inbounds i32, i32* %2168, i32 55
  store i32 %2167, i32* %arrayidx1781, align 4, !tbaa !7
  %2169 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1782 = getelementptr inbounds i32, i32* %2169, i32 56
  %2170 = load i32, i32* %arrayidx1782, align 4, !tbaa !7
  %2171 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1783 = getelementptr inbounds i32, i32* %2171, i32 56
  store i32 %2170, i32* %arrayidx1783, align 4, !tbaa !7
  %2172 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1784 = getelementptr inbounds i32, i32* %2172, i32 57
  %2173 = load i32, i32* %arrayidx1784, align 4, !tbaa !7
  %2174 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1785 = getelementptr inbounds i32, i32* %2174, i32 57
  store i32 %2173, i32* %arrayidx1785, align 4, !tbaa !7
  %2175 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1786 = getelementptr inbounds i32, i32* %2175, i32 56
  %2176 = load i32, i32* %arrayidx1786, align 4, !tbaa !7
  %2177 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1787 = getelementptr inbounds i32, i32* %2177, i32 58
  %2178 = load i32, i32* %arrayidx1787, align 4, !tbaa !7
  %2179 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1788 = getelementptr inbounds i32, i32* %2179, i32 8
  %2180 = load i32, i32* %arrayidx1788, align 4, !tbaa !7
  %sub1789 = sub nsw i32 0, %2180
  %2181 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1790 = getelementptr inbounds i32, i32* %2181, i32 37
  %2182 = load i32, i32* %arrayidx1790, align 4, !tbaa !7
  %2183 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1791 = sext i8 %2183 to i32
  %call1792 = call i32 @half_btf(i32 %2176, i32 %2178, i32 %sub1789, i32 %2182, i32 %conv1791)
  %2184 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1793 = getelementptr inbounds i32, i32* %2184, i32 58
  store i32 %call1792, i32* %arrayidx1793, align 4, !tbaa !7
  %2185 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1794 = getelementptr inbounds i32, i32* %2185, i32 56
  %2186 = load i32, i32* %arrayidx1794, align 4, !tbaa !7
  %2187 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1795 = getelementptr inbounds i32, i32* %2187, i32 59
  %2188 = load i32, i32* %arrayidx1795, align 4, !tbaa !7
  %2189 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1796 = getelementptr inbounds i32, i32* %2189, i32 8
  %2190 = load i32, i32* %arrayidx1796, align 4, !tbaa !7
  %sub1797 = sub nsw i32 0, %2190
  %2191 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1798 = getelementptr inbounds i32, i32* %2191, i32 36
  %2192 = load i32, i32* %arrayidx1798, align 4, !tbaa !7
  %2193 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1799 = sext i8 %2193 to i32
  %call1800 = call i32 @half_btf(i32 %2186, i32 %2188, i32 %sub1797, i32 %2192, i32 %conv1799)
  %2194 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1801 = getelementptr inbounds i32, i32* %2194, i32 59
  store i32 %call1800, i32* %arrayidx1801, align 4, !tbaa !7
  %2195 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1802 = getelementptr inbounds i32, i32* %2195, i32 8
  %2196 = load i32, i32* %arrayidx1802, align 4, !tbaa !7
  %2197 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1803 = getelementptr inbounds i32, i32* %2197, i32 60
  %2198 = load i32, i32* %arrayidx1803, align 4, !tbaa !7
  %2199 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1804 = getelementptr inbounds i32, i32* %2199, i32 56
  %2200 = load i32, i32* %arrayidx1804, align 4, !tbaa !7
  %2201 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1805 = getelementptr inbounds i32, i32* %2201, i32 35
  %2202 = load i32, i32* %arrayidx1805, align 4, !tbaa !7
  %2203 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1806 = sext i8 %2203 to i32
  %call1807 = call i32 @half_btf(i32 %2196, i32 %2198, i32 %2200, i32 %2202, i32 %conv1806)
  %2204 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1808 = getelementptr inbounds i32, i32* %2204, i32 60
  store i32 %call1807, i32* %arrayidx1808, align 4, !tbaa !7
  %2205 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1809 = getelementptr inbounds i32, i32* %2205, i32 8
  %2206 = load i32, i32* %arrayidx1809, align 4, !tbaa !7
  %2207 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1810 = getelementptr inbounds i32, i32* %2207, i32 61
  %2208 = load i32, i32* %arrayidx1810, align 4, !tbaa !7
  %2209 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1811 = getelementptr inbounds i32, i32* %2209, i32 56
  %2210 = load i32, i32* %arrayidx1811, align 4, !tbaa !7
  %2211 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1812 = getelementptr inbounds i32, i32* %2211, i32 34
  %2212 = load i32, i32* %arrayidx1812, align 4, !tbaa !7
  %2213 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1813 = sext i8 %2213 to i32
  %call1814 = call i32 @half_btf(i32 %2206, i32 %2208, i32 %2210, i32 %2212, i32 %conv1813)
  %2214 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1815 = getelementptr inbounds i32, i32* %2214, i32 61
  store i32 %call1814, i32* %arrayidx1815, align 4, !tbaa !7
  %2215 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1816 = getelementptr inbounds i32, i32* %2215, i32 62
  %2216 = load i32, i32* %arrayidx1816, align 4, !tbaa !7
  %2217 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1817 = getelementptr inbounds i32, i32* %2217, i32 62
  store i32 %2216, i32* %arrayidx1817, align 4, !tbaa !7
  %2218 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1818 = getelementptr inbounds i32, i32* %2218, i32 63
  %2219 = load i32, i32* %arrayidx1818, align 4, !tbaa !7
  %2220 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1819 = getelementptr inbounds i32, i32* %2220, i32 63
  store i32 %2219, i32* %arrayidx1819, align 4, !tbaa !7
  %2221 = load i32, i32* %stage, align 4, !tbaa !7
  %2222 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2223 = load i32*, i32** %bf1, align 4, !tbaa !2
  %2224 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %2225 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx1820 = getelementptr inbounds i8, i8* %2224, i32 %2225
  %2226 = load i8, i8* %arrayidx1820, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %2221, i32* %2222, i32* %2223, i32 64, i8 signext %2226)
  %2227 = load i32, i32* %stage, align 4, !tbaa !7
  %inc1821 = add nsw i32 %2227, 1
  store i32 %inc1821, i32* %stage, align 4, !tbaa !7
  %2228 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1822 = sext i8 %2228 to i32
  %call1823 = call i32* @cospi_arr(i32 %conv1822)
  store i32* %call1823, i32** %cospi, align 4, !tbaa !2
  %arraydecay1824 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay1824, i32** %bf0, align 4, !tbaa !2
  %2229 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %2229, i32** %bf1, align 4, !tbaa !2
  %2230 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1825 = getelementptr inbounds i32, i32* %2230, i32 0
  %2231 = load i32, i32* %arrayidx1825, align 4, !tbaa !7
  %2232 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1826 = getelementptr inbounds i32, i32* %2232, i32 0
  store i32 %2231, i32* %arrayidx1826, align 4, !tbaa !7
  %2233 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1827 = getelementptr inbounds i32, i32* %2233, i32 1
  %2234 = load i32, i32* %arrayidx1827, align 4, !tbaa !7
  %2235 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1828 = getelementptr inbounds i32, i32* %2235, i32 1
  store i32 %2234, i32* %arrayidx1828, align 4, !tbaa !7
  %2236 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1829 = getelementptr inbounds i32, i32* %2236, i32 2
  %2237 = load i32, i32* %arrayidx1829, align 4, !tbaa !7
  %2238 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1830 = getelementptr inbounds i32, i32* %2238, i32 2
  store i32 %2237, i32* %arrayidx1830, align 4, !tbaa !7
  %2239 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1831 = getelementptr inbounds i32, i32* %2239, i32 3
  %2240 = load i32, i32* %arrayidx1831, align 4, !tbaa !7
  %2241 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1832 = getelementptr inbounds i32, i32* %2241, i32 3
  store i32 %2240, i32* %arrayidx1832, align 4, !tbaa !7
  %2242 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1833 = getelementptr inbounds i32, i32* %2242, i32 56
  %2243 = load i32, i32* %arrayidx1833, align 4, !tbaa !7
  %2244 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1834 = getelementptr inbounds i32, i32* %2244, i32 4
  %2245 = load i32, i32* %arrayidx1834, align 4, !tbaa !7
  %2246 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1835 = getelementptr inbounds i32, i32* %2246, i32 8
  %2247 = load i32, i32* %arrayidx1835, align 4, !tbaa !7
  %2248 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1836 = getelementptr inbounds i32, i32* %2248, i32 7
  %2249 = load i32, i32* %arrayidx1836, align 4, !tbaa !7
  %2250 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1837 = sext i8 %2250 to i32
  %call1838 = call i32 @half_btf(i32 %2243, i32 %2245, i32 %2247, i32 %2249, i32 %conv1837)
  %2251 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1839 = getelementptr inbounds i32, i32* %2251, i32 4
  store i32 %call1838, i32* %arrayidx1839, align 4, !tbaa !7
  %2252 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1840 = getelementptr inbounds i32, i32* %2252, i32 24
  %2253 = load i32, i32* %arrayidx1840, align 4, !tbaa !7
  %2254 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1841 = getelementptr inbounds i32, i32* %2254, i32 5
  %2255 = load i32, i32* %arrayidx1841, align 4, !tbaa !7
  %2256 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1842 = getelementptr inbounds i32, i32* %2256, i32 40
  %2257 = load i32, i32* %arrayidx1842, align 4, !tbaa !7
  %2258 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1843 = getelementptr inbounds i32, i32* %2258, i32 6
  %2259 = load i32, i32* %arrayidx1843, align 4, !tbaa !7
  %2260 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1844 = sext i8 %2260 to i32
  %call1845 = call i32 @half_btf(i32 %2253, i32 %2255, i32 %2257, i32 %2259, i32 %conv1844)
  %2261 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1846 = getelementptr inbounds i32, i32* %2261, i32 5
  store i32 %call1845, i32* %arrayidx1846, align 4, !tbaa !7
  %2262 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1847 = getelementptr inbounds i32, i32* %2262, i32 24
  %2263 = load i32, i32* %arrayidx1847, align 4, !tbaa !7
  %2264 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1848 = getelementptr inbounds i32, i32* %2264, i32 6
  %2265 = load i32, i32* %arrayidx1848, align 4, !tbaa !7
  %2266 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1849 = getelementptr inbounds i32, i32* %2266, i32 40
  %2267 = load i32, i32* %arrayidx1849, align 4, !tbaa !7
  %sub1850 = sub nsw i32 0, %2267
  %2268 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1851 = getelementptr inbounds i32, i32* %2268, i32 5
  %2269 = load i32, i32* %arrayidx1851, align 4, !tbaa !7
  %2270 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1852 = sext i8 %2270 to i32
  %call1853 = call i32 @half_btf(i32 %2263, i32 %2265, i32 %sub1850, i32 %2269, i32 %conv1852)
  %2271 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1854 = getelementptr inbounds i32, i32* %2271, i32 6
  store i32 %call1853, i32* %arrayidx1854, align 4, !tbaa !7
  %2272 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1855 = getelementptr inbounds i32, i32* %2272, i32 56
  %2273 = load i32, i32* %arrayidx1855, align 4, !tbaa !7
  %2274 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1856 = getelementptr inbounds i32, i32* %2274, i32 7
  %2275 = load i32, i32* %arrayidx1856, align 4, !tbaa !7
  %2276 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1857 = getelementptr inbounds i32, i32* %2276, i32 8
  %2277 = load i32, i32* %arrayidx1857, align 4, !tbaa !7
  %sub1858 = sub nsw i32 0, %2277
  %2278 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1859 = getelementptr inbounds i32, i32* %2278, i32 4
  %2279 = load i32, i32* %arrayidx1859, align 4, !tbaa !7
  %2280 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1860 = sext i8 %2280 to i32
  %call1861 = call i32 @half_btf(i32 %2273, i32 %2275, i32 %sub1858, i32 %2279, i32 %conv1860)
  %2281 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1862 = getelementptr inbounds i32, i32* %2281, i32 7
  store i32 %call1861, i32* %arrayidx1862, align 4, !tbaa !7
  %2282 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1863 = getelementptr inbounds i32, i32* %2282, i32 8
  %2283 = load i32, i32* %arrayidx1863, align 4, !tbaa !7
  %2284 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1864 = getelementptr inbounds i32, i32* %2284, i32 9
  %2285 = load i32, i32* %arrayidx1864, align 4, !tbaa !7
  %add1865 = add nsw i32 %2283, %2285
  %2286 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1866 = getelementptr inbounds i32, i32* %2286, i32 8
  store i32 %add1865, i32* %arrayidx1866, align 4, !tbaa !7
  %2287 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1867 = getelementptr inbounds i32, i32* %2287, i32 9
  %2288 = load i32, i32* %arrayidx1867, align 4, !tbaa !7
  %sub1868 = sub nsw i32 0, %2288
  %2289 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1869 = getelementptr inbounds i32, i32* %2289, i32 8
  %2290 = load i32, i32* %arrayidx1869, align 4, !tbaa !7
  %add1870 = add nsw i32 %sub1868, %2290
  %2291 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1871 = getelementptr inbounds i32, i32* %2291, i32 9
  store i32 %add1870, i32* %arrayidx1871, align 4, !tbaa !7
  %2292 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1872 = getelementptr inbounds i32, i32* %2292, i32 10
  %2293 = load i32, i32* %arrayidx1872, align 4, !tbaa !7
  %sub1873 = sub nsw i32 0, %2293
  %2294 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1874 = getelementptr inbounds i32, i32* %2294, i32 11
  %2295 = load i32, i32* %arrayidx1874, align 4, !tbaa !7
  %add1875 = add nsw i32 %sub1873, %2295
  %2296 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1876 = getelementptr inbounds i32, i32* %2296, i32 10
  store i32 %add1875, i32* %arrayidx1876, align 4, !tbaa !7
  %2297 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1877 = getelementptr inbounds i32, i32* %2297, i32 11
  %2298 = load i32, i32* %arrayidx1877, align 4, !tbaa !7
  %2299 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1878 = getelementptr inbounds i32, i32* %2299, i32 10
  %2300 = load i32, i32* %arrayidx1878, align 4, !tbaa !7
  %add1879 = add nsw i32 %2298, %2300
  %2301 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1880 = getelementptr inbounds i32, i32* %2301, i32 11
  store i32 %add1879, i32* %arrayidx1880, align 4, !tbaa !7
  %2302 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1881 = getelementptr inbounds i32, i32* %2302, i32 12
  %2303 = load i32, i32* %arrayidx1881, align 4, !tbaa !7
  %2304 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1882 = getelementptr inbounds i32, i32* %2304, i32 13
  %2305 = load i32, i32* %arrayidx1882, align 4, !tbaa !7
  %add1883 = add nsw i32 %2303, %2305
  %2306 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1884 = getelementptr inbounds i32, i32* %2306, i32 12
  store i32 %add1883, i32* %arrayidx1884, align 4, !tbaa !7
  %2307 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1885 = getelementptr inbounds i32, i32* %2307, i32 13
  %2308 = load i32, i32* %arrayidx1885, align 4, !tbaa !7
  %sub1886 = sub nsw i32 0, %2308
  %2309 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1887 = getelementptr inbounds i32, i32* %2309, i32 12
  %2310 = load i32, i32* %arrayidx1887, align 4, !tbaa !7
  %add1888 = add nsw i32 %sub1886, %2310
  %2311 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1889 = getelementptr inbounds i32, i32* %2311, i32 13
  store i32 %add1888, i32* %arrayidx1889, align 4, !tbaa !7
  %2312 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1890 = getelementptr inbounds i32, i32* %2312, i32 14
  %2313 = load i32, i32* %arrayidx1890, align 4, !tbaa !7
  %sub1891 = sub nsw i32 0, %2313
  %2314 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1892 = getelementptr inbounds i32, i32* %2314, i32 15
  %2315 = load i32, i32* %arrayidx1892, align 4, !tbaa !7
  %add1893 = add nsw i32 %sub1891, %2315
  %2316 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1894 = getelementptr inbounds i32, i32* %2316, i32 14
  store i32 %add1893, i32* %arrayidx1894, align 4, !tbaa !7
  %2317 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1895 = getelementptr inbounds i32, i32* %2317, i32 15
  %2318 = load i32, i32* %arrayidx1895, align 4, !tbaa !7
  %2319 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1896 = getelementptr inbounds i32, i32* %2319, i32 14
  %2320 = load i32, i32* %arrayidx1896, align 4, !tbaa !7
  %add1897 = add nsw i32 %2318, %2320
  %2321 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1898 = getelementptr inbounds i32, i32* %2321, i32 15
  store i32 %add1897, i32* %arrayidx1898, align 4, !tbaa !7
  %2322 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1899 = getelementptr inbounds i32, i32* %2322, i32 16
  %2323 = load i32, i32* %arrayidx1899, align 4, !tbaa !7
  %2324 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1900 = getelementptr inbounds i32, i32* %2324, i32 16
  store i32 %2323, i32* %arrayidx1900, align 4, !tbaa !7
  %2325 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1901 = getelementptr inbounds i32, i32* %2325, i32 8
  %2326 = load i32, i32* %arrayidx1901, align 4, !tbaa !7
  %sub1902 = sub nsw i32 0, %2326
  %2327 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1903 = getelementptr inbounds i32, i32* %2327, i32 17
  %2328 = load i32, i32* %arrayidx1903, align 4, !tbaa !7
  %2329 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1904 = getelementptr inbounds i32, i32* %2329, i32 56
  %2330 = load i32, i32* %arrayidx1904, align 4, !tbaa !7
  %2331 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1905 = getelementptr inbounds i32, i32* %2331, i32 30
  %2332 = load i32, i32* %arrayidx1905, align 4, !tbaa !7
  %2333 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1906 = sext i8 %2333 to i32
  %call1907 = call i32 @half_btf(i32 %sub1902, i32 %2328, i32 %2330, i32 %2332, i32 %conv1906)
  %2334 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1908 = getelementptr inbounds i32, i32* %2334, i32 17
  store i32 %call1907, i32* %arrayidx1908, align 4, !tbaa !7
  %2335 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1909 = getelementptr inbounds i32, i32* %2335, i32 56
  %2336 = load i32, i32* %arrayidx1909, align 4, !tbaa !7
  %sub1910 = sub nsw i32 0, %2336
  %2337 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1911 = getelementptr inbounds i32, i32* %2337, i32 18
  %2338 = load i32, i32* %arrayidx1911, align 4, !tbaa !7
  %2339 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1912 = getelementptr inbounds i32, i32* %2339, i32 8
  %2340 = load i32, i32* %arrayidx1912, align 4, !tbaa !7
  %sub1913 = sub nsw i32 0, %2340
  %2341 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1914 = getelementptr inbounds i32, i32* %2341, i32 29
  %2342 = load i32, i32* %arrayidx1914, align 4, !tbaa !7
  %2343 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1915 = sext i8 %2343 to i32
  %call1916 = call i32 @half_btf(i32 %sub1910, i32 %2338, i32 %sub1913, i32 %2342, i32 %conv1915)
  %2344 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1917 = getelementptr inbounds i32, i32* %2344, i32 18
  store i32 %call1916, i32* %arrayidx1917, align 4, !tbaa !7
  %2345 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1918 = getelementptr inbounds i32, i32* %2345, i32 19
  %2346 = load i32, i32* %arrayidx1918, align 4, !tbaa !7
  %2347 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1919 = getelementptr inbounds i32, i32* %2347, i32 19
  store i32 %2346, i32* %arrayidx1919, align 4, !tbaa !7
  %2348 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1920 = getelementptr inbounds i32, i32* %2348, i32 20
  %2349 = load i32, i32* %arrayidx1920, align 4, !tbaa !7
  %2350 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1921 = getelementptr inbounds i32, i32* %2350, i32 20
  store i32 %2349, i32* %arrayidx1921, align 4, !tbaa !7
  %2351 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1922 = getelementptr inbounds i32, i32* %2351, i32 40
  %2352 = load i32, i32* %arrayidx1922, align 4, !tbaa !7
  %sub1923 = sub nsw i32 0, %2352
  %2353 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1924 = getelementptr inbounds i32, i32* %2353, i32 21
  %2354 = load i32, i32* %arrayidx1924, align 4, !tbaa !7
  %2355 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1925 = getelementptr inbounds i32, i32* %2355, i32 24
  %2356 = load i32, i32* %arrayidx1925, align 4, !tbaa !7
  %2357 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1926 = getelementptr inbounds i32, i32* %2357, i32 26
  %2358 = load i32, i32* %arrayidx1926, align 4, !tbaa !7
  %2359 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1927 = sext i8 %2359 to i32
  %call1928 = call i32 @half_btf(i32 %sub1923, i32 %2354, i32 %2356, i32 %2358, i32 %conv1927)
  %2360 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1929 = getelementptr inbounds i32, i32* %2360, i32 21
  store i32 %call1928, i32* %arrayidx1929, align 4, !tbaa !7
  %2361 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1930 = getelementptr inbounds i32, i32* %2361, i32 24
  %2362 = load i32, i32* %arrayidx1930, align 4, !tbaa !7
  %sub1931 = sub nsw i32 0, %2362
  %2363 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1932 = getelementptr inbounds i32, i32* %2363, i32 22
  %2364 = load i32, i32* %arrayidx1932, align 4, !tbaa !7
  %2365 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1933 = getelementptr inbounds i32, i32* %2365, i32 40
  %2366 = load i32, i32* %arrayidx1933, align 4, !tbaa !7
  %sub1934 = sub nsw i32 0, %2366
  %2367 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1935 = getelementptr inbounds i32, i32* %2367, i32 25
  %2368 = load i32, i32* %arrayidx1935, align 4, !tbaa !7
  %2369 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1936 = sext i8 %2369 to i32
  %call1937 = call i32 @half_btf(i32 %sub1931, i32 %2364, i32 %sub1934, i32 %2368, i32 %conv1936)
  %2370 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1938 = getelementptr inbounds i32, i32* %2370, i32 22
  store i32 %call1937, i32* %arrayidx1938, align 4, !tbaa !7
  %2371 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1939 = getelementptr inbounds i32, i32* %2371, i32 23
  %2372 = load i32, i32* %arrayidx1939, align 4, !tbaa !7
  %2373 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1940 = getelementptr inbounds i32, i32* %2373, i32 23
  store i32 %2372, i32* %arrayidx1940, align 4, !tbaa !7
  %2374 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1941 = getelementptr inbounds i32, i32* %2374, i32 24
  %2375 = load i32, i32* %arrayidx1941, align 4, !tbaa !7
  %2376 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1942 = getelementptr inbounds i32, i32* %2376, i32 24
  store i32 %2375, i32* %arrayidx1942, align 4, !tbaa !7
  %2377 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1943 = getelementptr inbounds i32, i32* %2377, i32 24
  %2378 = load i32, i32* %arrayidx1943, align 4, !tbaa !7
  %2379 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1944 = getelementptr inbounds i32, i32* %2379, i32 25
  %2380 = load i32, i32* %arrayidx1944, align 4, !tbaa !7
  %2381 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1945 = getelementptr inbounds i32, i32* %2381, i32 40
  %2382 = load i32, i32* %arrayidx1945, align 4, !tbaa !7
  %sub1946 = sub nsw i32 0, %2382
  %2383 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1947 = getelementptr inbounds i32, i32* %2383, i32 22
  %2384 = load i32, i32* %arrayidx1947, align 4, !tbaa !7
  %2385 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1948 = sext i8 %2385 to i32
  %call1949 = call i32 @half_btf(i32 %2378, i32 %2380, i32 %sub1946, i32 %2384, i32 %conv1948)
  %2386 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1950 = getelementptr inbounds i32, i32* %2386, i32 25
  store i32 %call1949, i32* %arrayidx1950, align 4, !tbaa !7
  %2387 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1951 = getelementptr inbounds i32, i32* %2387, i32 40
  %2388 = load i32, i32* %arrayidx1951, align 4, !tbaa !7
  %2389 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1952 = getelementptr inbounds i32, i32* %2389, i32 26
  %2390 = load i32, i32* %arrayidx1952, align 4, !tbaa !7
  %2391 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1953 = getelementptr inbounds i32, i32* %2391, i32 24
  %2392 = load i32, i32* %arrayidx1953, align 4, !tbaa !7
  %2393 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1954 = getelementptr inbounds i32, i32* %2393, i32 21
  %2394 = load i32, i32* %arrayidx1954, align 4, !tbaa !7
  %2395 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1955 = sext i8 %2395 to i32
  %call1956 = call i32 @half_btf(i32 %2388, i32 %2390, i32 %2392, i32 %2394, i32 %conv1955)
  %2396 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1957 = getelementptr inbounds i32, i32* %2396, i32 26
  store i32 %call1956, i32* %arrayidx1957, align 4, !tbaa !7
  %2397 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1958 = getelementptr inbounds i32, i32* %2397, i32 27
  %2398 = load i32, i32* %arrayidx1958, align 4, !tbaa !7
  %2399 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1959 = getelementptr inbounds i32, i32* %2399, i32 27
  store i32 %2398, i32* %arrayidx1959, align 4, !tbaa !7
  %2400 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1960 = getelementptr inbounds i32, i32* %2400, i32 28
  %2401 = load i32, i32* %arrayidx1960, align 4, !tbaa !7
  %2402 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1961 = getelementptr inbounds i32, i32* %2402, i32 28
  store i32 %2401, i32* %arrayidx1961, align 4, !tbaa !7
  %2403 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1962 = getelementptr inbounds i32, i32* %2403, i32 56
  %2404 = load i32, i32* %arrayidx1962, align 4, !tbaa !7
  %2405 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1963 = getelementptr inbounds i32, i32* %2405, i32 29
  %2406 = load i32, i32* %arrayidx1963, align 4, !tbaa !7
  %2407 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1964 = getelementptr inbounds i32, i32* %2407, i32 8
  %2408 = load i32, i32* %arrayidx1964, align 4, !tbaa !7
  %sub1965 = sub nsw i32 0, %2408
  %2409 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1966 = getelementptr inbounds i32, i32* %2409, i32 18
  %2410 = load i32, i32* %arrayidx1966, align 4, !tbaa !7
  %2411 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1967 = sext i8 %2411 to i32
  %call1968 = call i32 @half_btf(i32 %2404, i32 %2406, i32 %sub1965, i32 %2410, i32 %conv1967)
  %2412 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1969 = getelementptr inbounds i32, i32* %2412, i32 29
  store i32 %call1968, i32* %arrayidx1969, align 4, !tbaa !7
  %2413 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1970 = getelementptr inbounds i32, i32* %2413, i32 8
  %2414 = load i32, i32* %arrayidx1970, align 4, !tbaa !7
  %2415 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1971 = getelementptr inbounds i32, i32* %2415, i32 30
  %2416 = load i32, i32* %arrayidx1971, align 4, !tbaa !7
  %2417 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx1972 = getelementptr inbounds i32, i32* %2417, i32 56
  %2418 = load i32, i32* %arrayidx1972, align 4, !tbaa !7
  %2419 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1973 = getelementptr inbounds i32, i32* %2419, i32 17
  %2420 = load i32, i32* %arrayidx1973, align 4, !tbaa !7
  %2421 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv1974 = sext i8 %2421 to i32
  %call1975 = call i32 @half_btf(i32 %2414, i32 %2416, i32 %2418, i32 %2420, i32 %conv1974)
  %2422 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1976 = getelementptr inbounds i32, i32* %2422, i32 30
  store i32 %call1975, i32* %arrayidx1976, align 4, !tbaa !7
  %2423 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1977 = getelementptr inbounds i32, i32* %2423, i32 31
  %2424 = load i32, i32* %arrayidx1977, align 4, !tbaa !7
  %2425 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1978 = getelementptr inbounds i32, i32* %2425, i32 31
  store i32 %2424, i32* %arrayidx1978, align 4, !tbaa !7
  %2426 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1979 = getelementptr inbounds i32, i32* %2426, i32 32
  %2427 = load i32, i32* %arrayidx1979, align 4, !tbaa !7
  %2428 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1980 = getelementptr inbounds i32, i32* %2428, i32 35
  %2429 = load i32, i32* %arrayidx1980, align 4, !tbaa !7
  %add1981 = add nsw i32 %2427, %2429
  %2430 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1982 = getelementptr inbounds i32, i32* %2430, i32 32
  store i32 %add1981, i32* %arrayidx1982, align 4, !tbaa !7
  %2431 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1983 = getelementptr inbounds i32, i32* %2431, i32 33
  %2432 = load i32, i32* %arrayidx1983, align 4, !tbaa !7
  %2433 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1984 = getelementptr inbounds i32, i32* %2433, i32 34
  %2434 = load i32, i32* %arrayidx1984, align 4, !tbaa !7
  %add1985 = add nsw i32 %2432, %2434
  %2435 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1986 = getelementptr inbounds i32, i32* %2435, i32 33
  store i32 %add1985, i32* %arrayidx1986, align 4, !tbaa !7
  %2436 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1987 = getelementptr inbounds i32, i32* %2436, i32 34
  %2437 = load i32, i32* %arrayidx1987, align 4, !tbaa !7
  %sub1988 = sub nsw i32 0, %2437
  %2438 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1989 = getelementptr inbounds i32, i32* %2438, i32 33
  %2439 = load i32, i32* %arrayidx1989, align 4, !tbaa !7
  %add1990 = add nsw i32 %sub1988, %2439
  %2440 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1991 = getelementptr inbounds i32, i32* %2440, i32 34
  store i32 %add1990, i32* %arrayidx1991, align 4, !tbaa !7
  %2441 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1992 = getelementptr inbounds i32, i32* %2441, i32 35
  %2442 = load i32, i32* %arrayidx1992, align 4, !tbaa !7
  %sub1993 = sub nsw i32 0, %2442
  %2443 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1994 = getelementptr inbounds i32, i32* %2443, i32 32
  %2444 = load i32, i32* %arrayidx1994, align 4, !tbaa !7
  %add1995 = add nsw i32 %sub1993, %2444
  %2445 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx1996 = getelementptr inbounds i32, i32* %2445, i32 35
  store i32 %add1995, i32* %arrayidx1996, align 4, !tbaa !7
  %2446 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1997 = getelementptr inbounds i32, i32* %2446, i32 36
  %2447 = load i32, i32* %arrayidx1997, align 4, !tbaa !7
  %sub1998 = sub nsw i32 0, %2447
  %2448 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx1999 = getelementptr inbounds i32, i32* %2448, i32 39
  %2449 = load i32, i32* %arrayidx1999, align 4, !tbaa !7
  %add2000 = add nsw i32 %sub1998, %2449
  %2450 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2001 = getelementptr inbounds i32, i32* %2450, i32 36
  store i32 %add2000, i32* %arrayidx2001, align 4, !tbaa !7
  %2451 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2002 = getelementptr inbounds i32, i32* %2451, i32 37
  %2452 = load i32, i32* %arrayidx2002, align 4, !tbaa !7
  %sub2003 = sub nsw i32 0, %2452
  %2453 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2004 = getelementptr inbounds i32, i32* %2453, i32 38
  %2454 = load i32, i32* %arrayidx2004, align 4, !tbaa !7
  %add2005 = add nsw i32 %sub2003, %2454
  %2455 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2006 = getelementptr inbounds i32, i32* %2455, i32 37
  store i32 %add2005, i32* %arrayidx2006, align 4, !tbaa !7
  %2456 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2007 = getelementptr inbounds i32, i32* %2456, i32 38
  %2457 = load i32, i32* %arrayidx2007, align 4, !tbaa !7
  %2458 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2008 = getelementptr inbounds i32, i32* %2458, i32 37
  %2459 = load i32, i32* %arrayidx2008, align 4, !tbaa !7
  %add2009 = add nsw i32 %2457, %2459
  %2460 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2010 = getelementptr inbounds i32, i32* %2460, i32 38
  store i32 %add2009, i32* %arrayidx2010, align 4, !tbaa !7
  %2461 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2011 = getelementptr inbounds i32, i32* %2461, i32 39
  %2462 = load i32, i32* %arrayidx2011, align 4, !tbaa !7
  %2463 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2012 = getelementptr inbounds i32, i32* %2463, i32 36
  %2464 = load i32, i32* %arrayidx2012, align 4, !tbaa !7
  %add2013 = add nsw i32 %2462, %2464
  %2465 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2014 = getelementptr inbounds i32, i32* %2465, i32 39
  store i32 %add2013, i32* %arrayidx2014, align 4, !tbaa !7
  %2466 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2015 = getelementptr inbounds i32, i32* %2466, i32 40
  %2467 = load i32, i32* %arrayidx2015, align 4, !tbaa !7
  %2468 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2016 = getelementptr inbounds i32, i32* %2468, i32 43
  %2469 = load i32, i32* %arrayidx2016, align 4, !tbaa !7
  %add2017 = add nsw i32 %2467, %2469
  %2470 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2018 = getelementptr inbounds i32, i32* %2470, i32 40
  store i32 %add2017, i32* %arrayidx2018, align 4, !tbaa !7
  %2471 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2019 = getelementptr inbounds i32, i32* %2471, i32 41
  %2472 = load i32, i32* %arrayidx2019, align 4, !tbaa !7
  %2473 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2020 = getelementptr inbounds i32, i32* %2473, i32 42
  %2474 = load i32, i32* %arrayidx2020, align 4, !tbaa !7
  %add2021 = add nsw i32 %2472, %2474
  %2475 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2022 = getelementptr inbounds i32, i32* %2475, i32 41
  store i32 %add2021, i32* %arrayidx2022, align 4, !tbaa !7
  %2476 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2023 = getelementptr inbounds i32, i32* %2476, i32 42
  %2477 = load i32, i32* %arrayidx2023, align 4, !tbaa !7
  %sub2024 = sub nsw i32 0, %2477
  %2478 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2025 = getelementptr inbounds i32, i32* %2478, i32 41
  %2479 = load i32, i32* %arrayidx2025, align 4, !tbaa !7
  %add2026 = add nsw i32 %sub2024, %2479
  %2480 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2027 = getelementptr inbounds i32, i32* %2480, i32 42
  store i32 %add2026, i32* %arrayidx2027, align 4, !tbaa !7
  %2481 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2028 = getelementptr inbounds i32, i32* %2481, i32 43
  %2482 = load i32, i32* %arrayidx2028, align 4, !tbaa !7
  %sub2029 = sub nsw i32 0, %2482
  %2483 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2030 = getelementptr inbounds i32, i32* %2483, i32 40
  %2484 = load i32, i32* %arrayidx2030, align 4, !tbaa !7
  %add2031 = add nsw i32 %sub2029, %2484
  %2485 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2032 = getelementptr inbounds i32, i32* %2485, i32 43
  store i32 %add2031, i32* %arrayidx2032, align 4, !tbaa !7
  %2486 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2033 = getelementptr inbounds i32, i32* %2486, i32 44
  %2487 = load i32, i32* %arrayidx2033, align 4, !tbaa !7
  %sub2034 = sub nsw i32 0, %2487
  %2488 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2035 = getelementptr inbounds i32, i32* %2488, i32 47
  %2489 = load i32, i32* %arrayidx2035, align 4, !tbaa !7
  %add2036 = add nsw i32 %sub2034, %2489
  %2490 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2037 = getelementptr inbounds i32, i32* %2490, i32 44
  store i32 %add2036, i32* %arrayidx2037, align 4, !tbaa !7
  %2491 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2038 = getelementptr inbounds i32, i32* %2491, i32 45
  %2492 = load i32, i32* %arrayidx2038, align 4, !tbaa !7
  %sub2039 = sub nsw i32 0, %2492
  %2493 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2040 = getelementptr inbounds i32, i32* %2493, i32 46
  %2494 = load i32, i32* %arrayidx2040, align 4, !tbaa !7
  %add2041 = add nsw i32 %sub2039, %2494
  %2495 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2042 = getelementptr inbounds i32, i32* %2495, i32 45
  store i32 %add2041, i32* %arrayidx2042, align 4, !tbaa !7
  %2496 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2043 = getelementptr inbounds i32, i32* %2496, i32 46
  %2497 = load i32, i32* %arrayidx2043, align 4, !tbaa !7
  %2498 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2044 = getelementptr inbounds i32, i32* %2498, i32 45
  %2499 = load i32, i32* %arrayidx2044, align 4, !tbaa !7
  %add2045 = add nsw i32 %2497, %2499
  %2500 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2046 = getelementptr inbounds i32, i32* %2500, i32 46
  store i32 %add2045, i32* %arrayidx2046, align 4, !tbaa !7
  %2501 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2047 = getelementptr inbounds i32, i32* %2501, i32 47
  %2502 = load i32, i32* %arrayidx2047, align 4, !tbaa !7
  %2503 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2048 = getelementptr inbounds i32, i32* %2503, i32 44
  %2504 = load i32, i32* %arrayidx2048, align 4, !tbaa !7
  %add2049 = add nsw i32 %2502, %2504
  %2505 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2050 = getelementptr inbounds i32, i32* %2505, i32 47
  store i32 %add2049, i32* %arrayidx2050, align 4, !tbaa !7
  %2506 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2051 = getelementptr inbounds i32, i32* %2506, i32 48
  %2507 = load i32, i32* %arrayidx2051, align 4, !tbaa !7
  %2508 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2052 = getelementptr inbounds i32, i32* %2508, i32 51
  %2509 = load i32, i32* %arrayidx2052, align 4, !tbaa !7
  %add2053 = add nsw i32 %2507, %2509
  %2510 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2054 = getelementptr inbounds i32, i32* %2510, i32 48
  store i32 %add2053, i32* %arrayidx2054, align 4, !tbaa !7
  %2511 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2055 = getelementptr inbounds i32, i32* %2511, i32 49
  %2512 = load i32, i32* %arrayidx2055, align 4, !tbaa !7
  %2513 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2056 = getelementptr inbounds i32, i32* %2513, i32 50
  %2514 = load i32, i32* %arrayidx2056, align 4, !tbaa !7
  %add2057 = add nsw i32 %2512, %2514
  %2515 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2058 = getelementptr inbounds i32, i32* %2515, i32 49
  store i32 %add2057, i32* %arrayidx2058, align 4, !tbaa !7
  %2516 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2059 = getelementptr inbounds i32, i32* %2516, i32 50
  %2517 = load i32, i32* %arrayidx2059, align 4, !tbaa !7
  %sub2060 = sub nsw i32 0, %2517
  %2518 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2061 = getelementptr inbounds i32, i32* %2518, i32 49
  %2519 = load i32, i32* %arrayidx2061, align 4, !tbaa !7
  %add2062 = add nsw i32 %sub2060, %2519
  %2520 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2063 = getelementptr inbounds i32, i32* %2520, i32 50
  store i32 %add2062, i32* %arrayidx2063, align 4, !tbaa !7
  %2521 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2064 = getelementptr inbounds i32, i32* %2521, i32 51
  %2522 = load i32, i32* %arrayidx2064, align 4, !tbaa !7
  %sub2065 = sub nsw i32 0, %2522
  %2523 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2066 = getelementptr inbounds i32, i32* %2523, i32 48
  %2524 = load i32, i32* %arrayidx2066, align 4, !tbaa !7
  %add2067 = add nsw i32 %sub2065, %2524
  %2525 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2068 = getelementptr inbounds i32, i32* %2525, i32 51
  store i32 %add2067, i32* %arrayidx2068, align 4, !tbaa !7
  %2526 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2069 = getelementptr inbounds i32, i32* %2526, i32 52
  %2527 = load i32, i32* %arrayidx2069, align 4, !tbaa !7
  %sub2070 = sub nsw i32 0, %2527
  %2528 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2071 = getelementptr inbounds i32, i32* %2528, i32 55
  %2529 = load i32, i32* %arrayidx2071, align 4, !tbaa !7
  %add2072 = add nsw i32 %sub2070, %2529
  %2530 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2073 = getelementptr inbounds i32, i32* %2530, i32 52
  store i32 %add2072, i32* %arrayidx2073, align 4, !tbaa !7
  %2531 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2074 = getelementptr inbounds i32, i32* %2531, i32 53
  %2532 = load i32, i32* %arrayidx2074, align 4, !tbaa !7
  %sub2075 = sub nsw i32 0, %2532
  %2533 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2076 = getelementptr inbounds i32, i32* %2533, i32 54
  %2534 = load i32, i32* %arrayidx2076, align 4, !tbaa !7
  %add2077 = add nsw i32 %sub2075, %2534
  %2535 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2078 = getelementptr inbounds i32, i32* %2535, i32 53
  store i32 %add2077, i32* %arrayidx2078, align 4, !tbaa !7
  %2536 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2079 = getelementptr inbounds i32, i32* %2536, i32 54
  %2537 = load i32, i32* %arrayidx2079, align 4, !tbaa !7
  %2538 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2080 = getelementptr inbounds i32, i32* %2538, i32 53
  %2539 = load i32, i32* %arrayidx2080, align 4, !tbaa !7
  %add2081 = add nsw i32 %2537, %2539
  %2540 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2082 = getelementptr inbounds i32, i32* %2540, i32 54
  store i32 %add2081, i32* %arrayidx2082, align 4, !tbaa !7
  %2541 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2083 = getelementptr inbounds i32, i32* %2541, i32 55
  %2542 = load i32, i32* %arrayidx2083, align 4, !tbaa !7
  %2543 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2084 = getelementptr inbounds i32, i32* %2543, i32 52
  %2544 = load i32, i32* %arrayidx2084, align 4, !tbaa !7
  %add2085 = add nsw i32 %2542, %2544
  %2545 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2086 = getelementptr inbounds i32, i32* %2545, i32 55
  store i32 %add2085, i32* %arrayidx2086, align 4, !tbaa !7
  %2546 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2087 = getelementptr inbounds i32, i32* %2546, i32 56
  %2547 = load i32, i32* %arrayidx2087, align 4, !tbaa !7
  %2548 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2088 = getelementptr inbounds i32, i32* %2548, i32 59
  %2549 = load i32, i32* %arrayidx2088, align 4, !tbaa !7
  %add2089 = add nsw i32 %2547, %2549
  %2550 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2090 = getelementptr inbounds i32, i32* %2550, i32 56
  store i32 %add2089, i32* %arrayidx2090, align 4, !tbaa !7
  %2551 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2091 = getelementptr inbounds i32, i32* %2551, i32 57
  %2552 = load i32, i32* %arrayidx2091, align 4, !tbaa !7
  %2553 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2092 = getelementptr inbounds i32, i32* %2553, i32 58
  %2554 = load i32, i32* %arrayidx2092, align 4, !tbaa !7
  %add2093 = add nsw i32 %2552, %2554
  %2555 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2094 = getelementptr inbounds i32, i32* %2555, i32 57
  store i32 %add2093, i32* %arrayidx2094, align 4, !tbaa !7
  %2556 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2095 = getelementptr inbounds i32, i32* %2556, i32 58
  %2557 = load i32, i32* %arrayidx2095, align 4, !tbaa !7
  %sub2096 = sub nsw i32 0, %2557
  %2558 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2097 = getelementptr inbounds i32, i32* %2558, i32 57
  %2559 = load i32, i32* %arrayidx2097, align 4, !tbaa !7
  %add2098 = add nsw i32 %sub2096, %2559
  %2560 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2099 = getelementptr inbounds i32, i32* %2560, i32 58
  store i32 %add2098, i32* %arrayidx2099, align 4, !tbaa !7
  %2561 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2100 = getelementptr inbounds i32, i32* %2561, i32 59
  %2562 = load i32, i32* %arrayidx2100, align 4, !tbaa !7
  %sub2101 = sub nsw i32 0, %2562
  %2563 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2102 = getelementptr inbounds i32, i32* %2563, i32 56
  %2564 = load i32, i32* %arrayidx2102, align 4, !tbaa !7
  %add2103 = add nsw i32 %sub2101, %2564
  %2565 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2104 = getelementptr inbounds i32, i32* %2565, i32 59
  store i32 %add2103, i32* %arrayidx2104, align 4, !tbaa !7
  %2566 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2105 = getelementptr inbounds i32, i32* %2566, i32 60
  %2567 = load i32, i32* %arrayidx2105, align 4, !tbaa !7
  %sub2106 = sub nsw i32 0, %2567
  %2568 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2107 = getelementptr inbounds i32, i32* %2568, i32 63
  %2569 = load i32, i32* %arrayidx2107, align 4, !tbaa !7
  %add2108 = add nsw i32 %sub2106, %2569
  %2570 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2109 = getelementptr inbounds i32, i32* %2570, i32 60
  store i32 %add2108, i32* %arrayidx2109, align 4, !tbaa !7
  %2571 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2110 = getelementptr inbounds i32, i32* %2571, i32 61
  %2572 = load i32, i32* %arrayidx2110, align 4, !tbaa !7
  %sub2111 = sub nsw i32 0, %2572
  %2573 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2112 = getelementptr inbounds i32, i32* %2573, i32 62
  %2574 = load i32, i32* %arrayidx2112, align 4, !tbaa !7
  %add2113 = add nsw i32 %sub2111, %2574
  %2575 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2114 = getelementptr inbounds i32, i32* %2575, i32 61
  store i32 %add2113, i32* %arrayidx2114, align 4, !tbaa !7
  %2576 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2115 = getelementptr inbounds i32, i32* %2576, i32 62
  %2577 = load i32, i32* %arrayidx2115, align 4, !tbaa !7
  %2578 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2116 = getelementptr inbounds i32, i32* %2578, i32 61
  %2579 = load i32, i32* %arrayidx2116, align 4, !tbaa !7
  %add2117 = add nsw i32 %2577, %2579
  %2580 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2118 = getelementptr inbounds i32, i32* %2580, i32 62
  store i32 %add2117, i32* %arrayidx2118, align 4, !tbaa !7
  %2581 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2119 = getelementptr inbounds i32, i32* %2581, i32 63
  %2582 = load i32, i32* %arrayidx2119, align 4, !tbaa !7
  %2583 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2120 = getelementptr inbounds i32, i32* %2583, i32 60
  %2584 = load i32, i32* %arrayidx2120, align 4, !tbaa !7
  %add2121 = add nsw i32 %2582, %2584
  %2585 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2122 = getelementptr inbounds i32, i32* %2585, i32 63
  store i32 %add2121, i32* %arrayidx2122, align 4, !tbaa !7
  %2586 = load i32, i32* %stage, align 4, !tbaa !7
  %2587 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2588 = load i32*, i32** %bf1, align 4, !tbaa !2
  %2589 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %2590 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx2123 = getelementptr inbounds i8, i8* %2589, i32 %2590
  %2591 = load i8, i8* %arrayidx2123, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %2586, i32* %2587, i32* %2588, i32 64, i8 signext %2591)
  %2592 = load i32, i32* %stage, align 4, !tbaa !7
  %inc2124 = add nsw i32 %2592, 1
  store i32 %inc2124, i32* %stage, align 4, !tbaa !7
  %2593 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2125 = sext i8 %2593 to i32
  %call2126 = call i32* @cospi_arr(i32 %conv2125)
  store i32* %call2126, i32** %cospi, align 4, !tbaa !2
  %2594 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %2594, i32** %bf0, align 4, !tbaa !2
  %arraydecay2127 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay2127, i32** %bf1, align 4, !tbaa !2
  %2595 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2128 = getelementptr inbounds i32, i32* %2595, i32 0
  %2596 = load i32, i32* %arrayidx2128, align 4, !tbaa !7
  %2597 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2129 = getelementptr inbounds i32, i32* %2597, i32 0
  store i32 %2596, i32* %arrayidx2129, align 4, !tbaa !7
  %2598 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2130 = getelementptr inbounds i32, i32* %2598, i32 1
  %2599 = load i32, i32* %arrayidx2130, align 4, !tbaa !7
  %2600 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2131 = getelementptr inbounds i32, i32* %2600, i32 1
  store i32 %2599, i32* %arrayidx2131, align 4, !tbaa !7
  %2601 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2132 = getelementptr inbounds i32, i32* %2601, i32 2
  %2602 = load i32, i32* %arrayidx2132, align 4, !tbaa !7
  %2603 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2133 = getelementptr inbounds i32, i32* %2603, i32 2
  store i32 %2602, i32* %arrayidx2133, align 4, !tbaa !7
  %2604 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2134 = getelementptr inbounds i32, i32* %2604, i32 3
  %2605 = load i32, i32* %arrayidx2134, align 4, !tbaa !7
  %2606 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2135 = getelementptr inbounds i32, i32* %2606, i32 3
  store i32 %2605, i32* %arrayidx2135, align 4, !tbaa !7
  %2607 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2136 = getelementptr inbounds i32, i32* %2607, i32 4
  %2608 = load i32, i32* %arrayidx2136, align 4, !tbaa !7
  %2609 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2137 = getelementptr inbounds i32, i32* %2609, i32 4
  store i32 %2608, i32* %arrayidx2137, align 4, !tbaa !7
  %2610 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2138 = getelementptr inbounds i32, i32* %2610, i32 5
  %2611 = load i32, i32* %arrayidx2138, align 4, !tbaa !7
  %2612 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2139 = getelementptr inbounds i32, i32* %2612, i32 5
  store i32 %2611, i32* %arrayidx2139, align 4, !tbaa !7
  %2613 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2140 = getelementptr inbounds i32, i32* %2613, i32 6
  %2614 = load i32, i32* %arrayidx2140, align 4, !tbaa !7
  %2615 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2141 = getelementptr inbounds i32, i32* %2615, i32 6
  store i32 %2614, i32* %arrayidx2141, align 4, !tbaa !7
  %2616 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2142 = getelementptr inbounds i32, i32* %2616, i32 7
  %2617 = load i32, i32* %arrayidx2142, align 4, !tbaa !7
  %2618 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2143 = getelementptr inbounds i32, i32* %2618, i32 7
  store i32 %2617, i32* %arrayidx2143, align 4, !tbaa !7
  %2619 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2144 = getelementptr inbounds i32, i32* %2619, i32 60
  %2620 = load i32, i32* %arrayidx2144, align 4, !tbaa !7
  %2621 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2145 = getelementptr inbounds i32, i32* %2621, i32 8
  %2622 = load i32, i32* %arrayidx2145, align 4, !tbaa !7
  %2623 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2146 = getelementptr inbounds i32, i32* %2623, i32 4
  %2624 = load i32, i32* %arrayidx2146, align 4, !tbaa !7
  %2625 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2147 = getelementptr inbounds i32, i32* %2625, i32 15
  %2626 = load i32, i32* %arrayidx2147, align 4, !tbaa !7
  %2627 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2148 = sext i8 %2627 to i32
  %call2149 = call i32 @half_btf(i32 %2620, i32 %2622, i32 %2624, i32 %2626, i32 %conv2148)
  %2628 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2150 = getelementptr inbounds i32, i32* %2628, i32 8
  store i32 %call2149, i32* %arrayidx2150, align 4, !tbaa !7
  %2629 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2151 = getelementptr inbounds i32, i32* %2629, i32 28
  %2630 = load i32, i32* %arrayidx2151, align 4, !tbaa !7
  %2631 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2152 = getelementptr inbounds i32, i32* %2631, i32 9
  %2632 = load i32, i32* %arrayidx2152, align 4, !tbaa !7
  %2633 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2153 = getelementptr inbounds i32, i32* %2633, i32 36
  %2634 = load i32, i32* %arrayidx2153, align 4, !tbaa !7
  %2635 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2154 = getelementptr inbounds i32, i32* %2635, i32 14
  %2636 = load i32, i32* %arrayidx2154, align 4, !tbaa !7
  %2637 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2155 = sext i8 %2637 to i32
  %call2156 = call i32 @half_btf(i32 %2630, i32 %2632, i32 %2634, i32 %2636, i32 %conv2155)
  %2638 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2157 = getelementptr inbounds i32, i32* %2638, i32 9
  store i32 %call2156, i32* %arrayidx2157, align 4, !tbaa !7
  %2639 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2158 = getelementptr inbounds i32, i32* %2639, i32 44
  %2640 = load i32, i32* %arrayidx2158, align 4, !tbaa !7
  %2641 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2159 = getelementptr inbounds i32, i32* %2641, i32 10
  %2642 = load i32, i32* %arrayidx2159, align 4, !tbaa !7
  %2643 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2160 = getelementptr inbounds i32, i32* %2643, i32 20
  %2644 = load i32, i32* %arrayidx2160, align 4, !tbaa !7
  %2645 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2161 = getelementptr inbounds i32, i32* %2645, i32 13
  %2646 = load i32, i32* %arrayidx2161, align 4, !tbaa !7
  %2647 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2162 = sext i8 %2647 to i32
  %call2163 = call i32 @half_btf(i32 %2640, i32 %2642, i32 %2644, i32 %2646, i32 %conv2162)
  %2648 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2164 = getelementptr inbounds i32, i32* %2648, i32 10
  store i32 %call2163, i32* %arrayidx2164, align 4, !tbaa !7
  %2649 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2165 = getelementptr inbounds i32, i32* %2649, i32 12
  %2650 = load i32, i32* %arrayidx2165, align 4, !tbaa !7
  %2651 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2166 = getelementptr inbounds i32, i32* %2651, i32 11
  %2652 = load i32, i32* %arrayidx2166, align 4, !tbaa !7
  %2653 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2167 = getelementptr inbounds i32, i32* %2653, i32 52
  %2654 = load i32, i32* %arrayidx2167, align 4, !tbaa !7
  %2655 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2168 = getelementptr inbounds i32, i32* %2655, i32 12
  %2656 = load i32, i32* %arrayidx2168, align 4, !tbaa !7
  %2657 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2169 = sext i8 %2657 to i32
  %call2170 = call i32 @half_btf(i32 %2650, i32 %2652, i32 %2654, i32 %2656, i32 %conv2169)
  %2658 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2171 = getelementptr inbounds i32, i32* %2658, i32 11
  store i32 %call2170, i32* %arrayidx2171, align 4, !tbaa !7
  %2659 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2172 = getelementptr inbounds i32, i32* %2659, i32 12
  %2660 = load i32, i32* %arrayidx2172, align 4, !tbaa !7
  %2661 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2173 = getelementptr inbounds i32, i32* %2661, i32 12
  %2662 = load i32, i32* %arrayidx2173, align 4, !tbaa !7
  %2663 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2174 = getelementptr inbounds i32, i32* %2663, i32 52
  %2664 = load i32, i32* %arrayidx2174, align 4, !tbaa !7
  %sub2175 = sub nsw i32 0, %2664
  %2665 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2176 = getelementptr inbounds i32, i32* %2665, i32 11
  %2666 = load i32, i32* %arrayidx2176, align 4, !tbaa !7
  %2667 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2177 = sext i8 %2667 to i32
  %call2178 = call i32 @half_btf(i32 %2660, i32 %2662, i32 %sub2175, i32 %2666, i32 %conv2177)
  %2668 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2179 = getelementptr inbounds i32, i32* %2668, i32 12
  store i32 %call2178, i32* %arrayidx2179, align 4, !tbaa !7
  %2669 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2180 = getelementptr inbounds i32, i32* %2669, i32 44
  %2670 = load i32, i32* %arrayidx2180, align 4, !tbaa !7
  %2671 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2181 = getelementptr inbounds i32, i32* %2671, i32 13
  %2672 = load i32, i32* %arrayidx2181, align 4, !tbaa !7
  %2673 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2182 = getelementptr inbounds i32, i32* %2673, i32 20
  %2674 = load i32, i32* %arrayidx2182, align 4, !tbaa !7
  %sub2183 = sub nsw i32 0, %2674
  %2675 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2184 = getelementptr inbounds i32, i32* %2675, i32 10
  %2676 = load i32, i32* %arrayidx2184, align 4, !tbaa !7
  %2677 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2185 = sext i8 %2677 to i32
  %call2186 = call i32 @half_btf(i32 %2670, i32 %2672, i32 %sub2183, i32 %2676, i32 %conv2185)
  %2678 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2187 = getelementptr inbounds i32, i32* %2678, i32 13
  store i32 %call2186, i32* %arrayidx2187, align 4, !tbaa !7
  %2679 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2188 = getelementptr inbounds i32, i32* %2679, i32 28
  %2680 = load i32, i32* %arrayidx2188, align 4, !tbaa !7
  %2681 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2189 = getelementptr inbounds i32, i32* %2681, i32 14
  %2682 = load i32, i32* %arrayidx2189, align 4, !tbaa !7
  %2683 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2190 = getelementptr inbounds i32, i32* %2683, i32 36
  %2684 = load i32, i32* %arrayidx2190, align 4, !tbaa !7
  %sub2191 = sub nsw i32 0, %2684
  %2685 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2192 = getelementptr inbounds i32, i32* %2685, i32 9
  %2686 = load i32, i32* %arrayidx2192, align 4, !tbaa !7
  %2687 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2193 = sext i8 %2687 to i32
  %call2194 = call i32 @half_btf(i32 %2680, i32 %2682, i32 %sub2191, i32 %2686, i32 %conv2193)
  %2688 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2195 = getelementptr inbounds i32, i32* %2688, i32 14
  store i32 %call2194, i32* %arrayidx2195, align 4, !tbaa !7
  %2689 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2196 = getelementptr inbounds i32, i32* %2689, i32 60
  %2690 = load i32, i32* %arrayidx2196, align 4, !tbaa !7
  %2691 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2197 = getelementptr inbounds i32, i32* %2691, i32 15
  %2692 = load i32, i32* %arrayidx2197, align 4, !tbaa !7
  %2693 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2198 = getelementptr inbounds i32, i32* %2693, i32 4
  %2694 = load i32, i32* %arrayidx2198, align 4, !tbaa !7
  %sub2199 = sub nsw i32 0, %2694
  %2695 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2200 = getelementptr inbounds i32, i32* %2695, i32 8
  %2696 = load i32, i32* %arrayidx2200, align 4, !tbaa !7
  %2697 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2201 = sext i8 %2697 to i32
  %call2202 = call i32 @half_btf(i32 %2690, i32 %2692, i32 %sub2199, i32 %2696, i32 %conv2201)
  %2698 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2203 = getelementptr inbounds i32, i32* %2698, i32 15
  store i32 %call2202, i32* %arrayidx2203, align 4, !tbaa !7
  %2699 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2204 = getelementptr inbounds i32, i32* %2699, i32 16
  %2700 = load i32, i32* %arrayidx2204, align 4, !tbaa !7
  %2701 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2205 = getelementptr inbounds i32, i32* %2701, i32 17
  %2702 = load i32, i32* %arrayidx2205, align 4, !tbaa !7
  %add2206 = add nsw i32 %2700, %2702
  %2703 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2207 = getelementptr inbounds i32, i32* %2703, i32 16
  store i32 %add2206, i32* %arrayidx2207, align 4, !tbaa !7
  %2704 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2208 = getelementptr inbounds i32, i32* %2704, i32 17
  %2705 = load i32, i32* %arrayidx2208, align 4, !tbaa !7
  %sub2209 = sub nsw i32 0, %2705
  %2706 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2210 = getelementptr inbounds i32, i32* %2706, i32 16
  %2707 = load i32, i32* %arrayidx2210, align 4, !tbaa !7
  %add2211 = add nsw i32 %sub2209, %2707
  %2708 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2212 = getelementptr inbounds i32, i32* %2708, i32 17
  store i32 %add2211, i32* %arrayidx2212, align 4, !tbaa !7
  %2709 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2213 = getelementptr inbounds i32, i32* %2709, i32 18
  %2710 = load i32, i32* %arrayidx2213, align 4, !tbaa !7
  %sub2214 = sub nsw i32 0, %2710
  %2711 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2215 = getelementptr inbounds i32, i32* %2711, i32 19
  %2712 = load i32, i32* %arrayidx2215, align 4, !tbaa !7
  %add2216 = add nsw i32 %sub2214, %2712
  %2713 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2217 = getelementptr inbounds i32, i32* %2713, i32 18
  store i32 %add2216, i32* %arrayidx2217, align 4, !tbaa !7
  %2714 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2218 = getelementptr inbounds i32, i32* %2714, i32 19
  %2715 = load i32, i32* %arrayidx2218, align 4, !tbaa !7
  %2716 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2219 = getelementptr inbounds i32, i32* %2716, i32 18
  %2717 = load i32, i32* %arrayidx2219, align 4, !tbaa !7
  %add2220 = add nsw i32 %2715, %2717
  %2718 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2221 = getelementptr inbounds i32, i32* %2718, i32 19
  store i32 %add2220, i32* %arrayidx2221, align 4, !tbaa !7
  %2719 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2222 = getelementptr inbounds i32, i32* %2719, i32 20
  %2720 = load i32, i32* %arrayidx2222, align 4, !tbaa !7
  %2721 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2223 = getelementptr inbounds i32, i32* %2721, i32 21
  %2722 = load i32, i32* %arrayidx2223, align 4, !tbaa !7
  %add2224 = add nsw i32 %2720, %2722
  %2723 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2225 = getelementptr inbounds i32, i32* %2723, i32 20
  store i32 %add2224, i32* %arrayidx2225, align 4, !tbaa !7
  %2724 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2226 = getelementptr inbounds i32, i32* %2724, i32 21
  %2725 = load i32, i32* %arrayidx2226, align 4, !tbaa !7
  %sub2227 = sub nsw i32 0, %2725
  %2726 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2228 = getelementptr inbounds i32, i32* %2726, i32 20
  %2727 = load i32, i32* %arrayidx2228, align 4, !tbaa !7
  %add2229 = add nsw i32 %sub2227, %2727
  %2728 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2230 = getelementptr inbounds i32, i32* %2728, i32 21
  store i32 %add2229, i32* %arrayidx2230, align 4, !tbaa !7
  %2729 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2231 = getelementptr inbounds i32, i32* %2729, i32 22
  %2730 = load i32, i32* %arrayidx2231, align 4, !tbaa !7
  %sub2232 = sub nsw i32 0, %2730
  %2731 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2233 = getelementptr inbounds i32, i32* %2731, i32 23
  %2732 = load i32, i32* %arrayidx2233, align 4, !tbaa !7
  %add2234 = add nsw i32 %sub2232, %2732
  %2733 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2235 = getelementptr inbounds i32, i32* %2733, i32 22
  store i32 %add2234, i32* %arrayidx2235, align 4, !tbaa !7
  %2734 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2236 = getelementptr inbounds i32, i32* %2734, i32 23
  %2735 = load i32, i32* %arrayidx2236, align 4, !tbaa !7
  %2736 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2237 = getelementptr inbounds i32, i32* %2736, i32 22
  %2737 = load i32, i32* %arrayidx2237, align 4, !tbaa !7
  %add2238 = add nsw i32 %2735, %2737
  %2738 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2239 = getelementptr inbounds i32, i32* %2738, i32 23
  store i32 %add2238, i32* %arrayidx2239, align 4, !tbaa !7
  %2739 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2240 = getelementptr inbounds i32, i32* %2739, i32 24
  %2740 = load i32, i32* %arrayidx2240, align 4, !tbaa !7
  %2741 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2241 = getelementptr inbounds i32, i32* %2741, i32 25
  %2742 = load i32, i32* %arrayidx2241, align 4, !tbaa !7
  %add2242 = add nsw i32 %2740, %2742
  %2743 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2243 = getelementptr inbounds i32, i32* %2743, i32 24
  store i32 %add2242, i32* %arrayidx2243, align 4, !tbaa !7
  %2744 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2244 = getelementptr inbounds i32, i32* %2744, i32 25
  %2745 = load i32, i32* %arrayidx2244, align 4, !tbaa !7
  %sub2245 = sub nsw i32 0, %2745
  %2746 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2246 = getelementptr inbounds i32, i32* %2746, i32 24
  %2747 = load i32, i32* %arrayidx2246, align 4, !tbaa !7
  %add2247 = add nsw i32 %sub2245, %2747
  %2748 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2248 = getelementptr inbounds i32, i32* %2748, i32 25
  store i32 %add2247, i32* %arrayidx2248, align 4, !tbaa !7
  %2749 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2249 = getelementptr inbounds i32, i32* %2749, i32 26
  %2750 = load i32, i32* %arrayidx2249, align 4, !tbaa !7
  %sub2250 = sub nsw i32 0, %2750
  %2751 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2251 = getelementptr inbounds i32, i32* %2751, i32 27
  %2752 = load i32, i32* %arrayidx2251, align 4, !tbaa !7
  %add2252 = add nsw i32 %sub2250, %2752
  %2753 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2253 = getelementptr inbounds i32, i32* %2753, i32 26
  store i32 %add2252, i32* %arrayidx2253, align 4, !tbaa !7
  %2754 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2254 = getelementptr inbounds i32, i32* %2754, i32 27
  %2755 = load i32, i32* %arrayidx2254, align 4, !tbaa !7
  %2756 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2255 = getelementptr inbounds i32, i32* %2756, i32 26
  %2757 = load i32, i32* %arrayidx2255, align 4, !tbaa !7
  %add2256 = add nsw i32 %2755, %2757
  %2758 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2257 = getelementptr inbounds i32, i32* %2758, i32 27
  store i32 %add2256, i32* %arrayidx2257, align 4, !tbaa !7
  %2759 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2258 = getelementptr inbounds i32, i32* %2759, i32 28
  %2760 = load i32, i32* %arrayidx2258, align 4, !tbaa !7
  %2761 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2259 = getelementptr inbounds i32, i32* %2761, i32 29
  %2762 = load i32, i32* %arrayidx2259, align 4, !tbaa !7
  %add2260 = add nsw i32 %2760, %2762
  %2763 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2261 = getelementptr inbounds i32, i32* %2763, i32 28
  store i32 %add2260, i32* %arrayidx2261, align 4, !tbaa !7
  %2764 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2262 = getelementptr inbounds i32, i32* %2764, i32 29
  %2765 = load i32, i32* %arrayidx2262, align 4, !tbaa !7
  %sub2263 = sub nsw i32 0, %2765
  %2766 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2264 = getelementptr inbounds i32, i32* %2766, i32 28
  %2767 = load i32, i32* %arrayidx2264, align 4, !tbaa !7
  %add2265 = add nsw i32 %sub2263, %2767
  %2768 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2266 = getelementptr inbounds i32, i32* %2768, i32 29
  store i32 %add2265, i32* %arrayidx2266, align 4, !tbaa !7
  %2769 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2267 = getelementptr inbounds i32, i32* %2769, i32 30
  %2770 = load i32, i32* %arrayidx2267, align 4, !tbaa !7
  %sub2268 = sub nsw i32 0, %2770
  %2771 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2269 = getelementptr inbounds i32, i32* %2771, i32 31
  %2772 = load i32, i32* %arrayidx2269, align 4, !tbaa !7
  %add2270 = add nsw i32 %sub2268, %2772
  %2773 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2271 = getelementptr inbounds i32, i32* %2773, i32 30
  store i32 %add2270, i32* %arrayidx2271, align 4, !tbaa !7
  %2774 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2272 = getelementptr inbounds i32, i32* %2774, i32 31
  %2775 = load i32, i32* %arrayidx2272, align 4, !tbaa !7
  %2776 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2273 = getelementptr inbounds i32, i32* %2776, i32 30
  %2777 = load i32, i32* %arrayidx2273, align 4, !tbaa !7
  %add2274 = add nsw i32 %2775, %2777
  %2778 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2275 = getelementptr inbounds i32, i32* %2778, i32 31
  store i32 %add2274, i32* %arrayidx2275, align 4, !tbaa !7
  %2779 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2276 = getelementptr inbounds i32, i32* %2779, i32 32
  %2780 = load i32, i32* %arrayidx2276, align 4, !tbaa !7
  %2781 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2277 = getelementptr inbounds i32, i32* %2781, i32 32
  store i32 %2780, i32* %arrayidx2277, align 4, !tbaa !7
  %2782 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2278 = getelementptr inbounds i32, i32* %2782, i32 4
  %2783 = load i32, i32* %arrayidx2278, align 4, !tbaa !7
  %sub2279 = sub nsw i32 0, %2783
  %2784 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2280 = getelementptr inbounds i32, i32* %2784, i32 33
  %2785 = load i32, i32* %arrayidx2280, align 4, !tbaa !7
  %2786 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2281 = getelementptr inbounds i32, i32* %2786, i32 60
  %2787 = load i32, i32* %arrayidx2281, align 4, !tbaa !7
  %2788 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2282 = getelementptr inbounds i32, i32* %2788, i32 62
  %2789 = load i32, i32* %arrayidx2282, align 4, !tbaa !7
  %2790 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2283 = sext i8 %2790 to i32
  %call2284 = call i32 @half_btf(i32 %sub2279, i32 %2785, i32 %2787, i32 %2789, i32 %conv2283)
  %2791 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2285 = getelementptr inbounds i32, i32* %2791, i32 33
  store i32 %call2284, i32* %arrayidx2285, align 4, !tbaa !7
  %2792 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2286 = getelementptr inbounds i32, i32* %2792, i32 60
  %2793 = load i32, i32* %arrayidx2286, align 4, !tbaa !7
  %sub2287 = sub nsw i32 0, %2793
  %2794 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2288 = getelementptr inbounds i32, i32* %2794, i32 34
  %2795 = load i32, i32* %arrayidx2288, align 4, !tbaa !7
  %2796 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2289 = getelementptr inbounds i32, i32* %2796, i32 4
  %2797 = load i32, i32* %arrayidx2289, align 4, !tbaa !7
  %sub2290 = sub nsw i32 0, %2797
  %2798 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2291 = getelementptr inbounds i32, i32* %2798, i32 61
  %2799 = load i32, i32* %arrayidx2291, align 4, !tbaa !7
  %2800 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2292 = sext i8 %2800 to i32
  %call2293 = call i32 @half_btf(i32 %sub2287, i32 %2795, i32 %sub2290, i32 %2799, i32 %conv2292)
  %2801 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2294 = getelementptr inbounds i32, i32* %2801, i32 34
  store i32 %call2293, i32* %arrayidx2294, align 4, !tbaa !7
  %2802 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2295 = getelementptr inbounds i32, i32* %2802, i32 35
  %2803 = load i32, i32* %arrayidx2295, align 4, !tbaa !7
  %2804 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2296 = getelementptr inbounds i32, i32* %2804, i32 35
  store i32 %2803, i32* %arrayidx2296, align 4, !tbaa !7
  %2805 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2297 = getelementptr inbounds i32, i32* %2805, i32 36
  %2806 = load i32, i32* %arrayidx2297, align 4, !tbaa !7
  %2807 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2298 = getelementptr inbounds i32, i32* %2807, i32 36
  store i32 %2806, i32* %arrayidx2298, align 4, !tbaa !7
  %2808 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2299 = getelementptr inbounds i32, i32* %2808, i32 36
  %2809 = load i32, i32* %arrayidx2299, align 4, !tbaa !7
  %sub2300 = sub nsw i32 0, %2809
  %2810 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2301 = getelementptr inbounds i32, i32* %2810, i32 37
  %2811 = load i32, i32* %arrayidx2301, align 4, !tbaa !7
  %2812 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2302 = getelementptr inbounds i32, i32* %2812, i32 28
  %2813 = load i32, i32* %arrayidx2302, align 4, !tbaa !7
  %2814 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2303 = getelementptr inbounds i32, i32* %2814, i32 58
  %2815 = load i32, i32* %arrayidx2303, align 4, !tbaa !7
  %2816 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2304 = sext i8 %2816 to i32
  %call2305 = call i32 @half_btf(i32 %sub2300, i32 %2811, i32 %2813, i32 %2815, i32 %conv2304)
  %2817 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2306 = getelementptr inbounds i32, i32* %2817, i32 37
  store i32 %call2305, i32* %arrayidx2306, align 4, !tbaa !7
  %2818 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2307 = getelementptr inbounds i32, i32* %2818, i32 28
  %2819 = load i32, i32* %arrayidx2307, align 4, !tbaa !7
  %sub2308 = sub nsw i32 0, %2819
  %2820 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2309 = getelementptr inbounds i32, i32* %2820, i32 38
  %2821 = load i32, i32* %arrayidx2309, align 4, !tbaa !7
  %2822 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2310 = getelementptr inbounds i32, i32* %2822, i32 36
  %2823 = load i32, i32* %arrayidx2310, align 4, !tbaa !7
  %sub2311 = sub nsw i32 0, %2823
  %2824 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2312 = getelementptr inbounds i32, i32* %2824, i32 57
  %2825 = load i32, i32* %arrayidx2312, align 4, !tbaa !7
  %2826 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2313 = sext i8 %2826 to i32
  %call2314 = call i32 @half_btf(i32 %sub2308, i32 %2821, i32 %sub2311, i32 %2825, i32 %conv2313)
  %2827 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2315 = getelementptr inbounds i32, i32* %2827, i32 38
  store i32 %call2314, i32* %arrayidx2315, align 4, !tbaa !7
  %2828 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2316 = getelementptr inbounds i32, i32* %2828, i32 39
  %2829 = load i32, i32* %arrayidx2316, align 4, !tbaa !7
  %2830 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2317 = getelementptr inbounds i32, i32* %2830, i32 39
  store i32 %2829, i32* %arrayidx2317, align 4, !tbaa !7
  %2831 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2318 = getelementptr inbounds i32, i32* %2831, i32 40
  %2832 = load i32, i32* %arrayidx2318, align 4, !tbaa !7
  %2833 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2319 = getelementptr inbounds i32, i32* %2833, i32 40
  store i32 %2832, i32* %arrayidx2319, align 4, !tbaa !7
  %2834 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2320 = getelementptr inbounds i32, i32* %2834, i32 20
  %2835 = load i32, i32* %arrayidx2320, align 4, !tbaa !7
  %sub2321 = sub nsw i32 0, %2835
  %2836 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2322 = getelementptr inbounds i32, i32* %2836, i32 41
  %2837 = load i32, i32* %arrayidx2322, align 4, !tbaa !7
  %2838 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2323 = getelementptr inbounds i32, i32* %2838, i32 44
  %2839 = load i32, i32* %arrayidx2323, align 4, !tbaa !7
  %2840 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2324 = getelementptr inbounds i32, i32* %2840, i32 54
  %2841 = load i32, i32* %arrayidx2324, align 4, !tbaa !7
  %2842 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2325 = sext i8 %2842 to i32
  %call2326 = call i32 @half_btf(i32 %sub2321, i32 %2837, i32 %2839, i32 %2841, i32 %conv2325)
  %2843 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2327 = getelementptr inbounds i32, i32* %2843, i32 41
  store i32 %call2326, i32* %arrayidx2327, align 4, !tbaa !7
  %2844 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2328 = getelementptr inbounds i32, i32* %2844, i32 44
  %2845 = load i32, i32* %arrayidx2328, align 4, !tbaa !7
  %sub2329 = sub nsw i32 0, %2845
  %2846 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2330 = getelementptr inbounds i32, i32* %2846, i32 42
  %2847 = load i32, i32* %arrayidx2330, align 4, !tbaa !7
  %2848 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2331 = getelementptr inbounds i32, i32* %2848, i32 20
  %2849 = load i32, i32* %arrayidx2331, align 4, !tbaa !7
  %sub2332 = sub nsw i32 0, %2849
  %2850 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2333 = getelementptr inbounds i32, i32* %2850, i32 53
  %2851 = load i32, i32* %arrayidx2333, align 4, !tbaa !7
  %2852 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2334 = sext i8 %2852 to i32
  %call2335 = call i32 @half_btf(i32 %sub2329, i32 %2847, i32 %sub2332, i32 %2851, i32 %conv2334)
  %2853 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2336 = getelementptr inbounds i32, i32* %2853, i32 42
  store i32 %call2335, i32* %arrayidx2336, align 4, !tbaa !7
  %2854 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2337 = getelementptr inbounds i32, i32* %2854, i32 43
  %2855 = load i32, i32* %arrayidx2337, align 4, !tbaa !7
  %2856 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2338 = getelementptr inbounds i32, i32* %2856, i32 43
  store i32 %2855, i32* %arrayidx2338, align 4, !tbaa !7
  %2857 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2339 = getelementptr inbounds i32, i32* %2857, i32 44
  %2858 = load i32, i32* %arrayidx2339, align 4, !tbaa !7
  %2859 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2340 = getelementptr inbounds i32, i32* %2859, i32 44
  store i32 %2858, i32* %arrayidx2340, align 4, !tbaa !7
  %2860 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2341 = getelementptr inbounds i32, i32* %2860, i32 52
  %2861 = load i32, i32* %arrayidx2341, align 4, !tbaa !7
  %sub2342 = sub nsw i32 0, %2861
  %2862 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2343 = getelementptr inbounds i32, i32* %2862, i32 45
  %2863 = load i32, i32* %arrayidx2343, align 4, !tbaa !7
  %2864 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2344 = getelementptr inbounds i32, i32* %2864, i32 12
  %2865 = load i32, i32* %arrayidx2344, align 4, !tbaa !7
  %2866 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2345 = getelementptr inbounds i32, i32* %2866, i32 50
  %2867 = load i32, i32* %arrayidx2345, align 4, !tbaa !7
  %2868 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2346 = sext i8 %2868 to i32
  %call2347 = call i32 @half_btf(i32 %sub2342, i32 %2863, i32 %2865, i32 %2867, i32 %conv2346)
  %2869 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2348 = getelementptr inbounds i32, i32* %2869, i32 45
  store i32 %call2347, i32* %arrayidx2348, align 4, !tbaa !7
  %2870 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2349 = getelementptr inbounds i32, i32* %2870, i32 12
  %2871 = load i32, i32* %arrayidx2349, align 4, !tbaa !7
  %sub2350 = sub nsw i32 0, %2871
  %2872 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2351 = getelementptr inbounds i32, i32* %2872, i32 46
  %2873 = load i32, i32* %arrayidx2351, align 4, !tbaa !7
  %2874 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2352 = getelementptr inbounds i32, i32* %2874, i32 52
  %2875 = load i32, i32* %arrayidx2352, align 4, !tbaa !7
  %sub2353 = sub nsw i32 0, %2875
  %2876 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2354 = getelementptr inbounds i32, i32* %2876, i32 49
  %2877 = load i32, i32* %arrayidx2354, align 4, !tbaa !7
  %2878 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2355 = sext i8 %2878 to i32
  %call2356 = call i32 @half_btf(i32 %sub2350, i32 %2873, i32 %sub2353, i32 %2877, i32 %conv2355)
  %2879 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2357 = getelementptr inbounds i32, i32* %2879, i32 46
  store i32 %call2356, i32* %arrayidx2357, align 4, !tbaa !7
  %2880 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2358 = getelementptr inbounds i32, i32* %2880, i32 47
  %2881 = load i32, i32* %arrayidx2358, align 4, !tbaa !7
  %2882 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2359 = getelementptr inbounds i32, i32* %2882, i32 47
  store i32 %2881, i32* %arrayidx2359, align 4, !tbaa !7
  %2883 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2360 = getelementptr inbounds i32, i32* %2883, i32 48
  %2884 = load i32, i32* %arrayidx2360, align 4, !tbaa !7
  %2885 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2361 = getelementptr inbounds i32, i32* %2885, i32 48
  store i32 %2884, i32* %arrayidx2361, align 4, !tbaa !7
  %2886 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2362 = getelementptr inbounds i32, i32* %2886, i32 12
  %2887 = load i32, i32* %arrayidx2362, align 4, !tbaa !7
  %2888 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2363 = getelementptr inbounds i32, i32* %2888, i32 49
  %2889 = load i32, i32* %arrayidx2363, align 4, !tbaa !7
  %2890 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2364 = getelementptr inbounds i32, i32* %2890, i32 52
  %2891 = load i32, i32* %arrayidx2364, align 4, !tbaa !7
  %sub2365 = sub nsw i32 0, %2891
  %2892 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2366 = getelementptr inbounds i32, i32* %2892, i32 46
  %2893 = load i32, i32* %arrayidx2366, align 4, !tbaa !7
  %2894 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2367 = sext i8 %2894 to i32
  %call2368 = call i32 @half_btf(i32 %2887, i32 %2889, i32 %sub2365, i32 %2893, i32 %conv2367)
  %2895 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2369 = getelementptr inbounds i32, i32* %2895, i32 49
  store i32 %call2368, i32* %arrayidx2369, align 4, !tbaa !7
  %2896 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2370 = getelementptr inbounds i32, i32* %2896, i32 52
  %2897 = load i32, i32* %arrayidx2370, align 4, !tbaa !7
  %2898 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2371 = getelementptr inbounds i32, i32* %2898, i32 50
  %2899 = load i32, i32* %arrayidx2371, align 4, !tbaa !7
  %2900 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2372 = getelementptr inbounds i32, i32* %2900, i32 12
  %2901 = load i32, i32* %arrayidx2372, align 4, !tbaa !7
  %2902 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2373 = getelementptr inbounds i32, i32* %2902, i32 45
  %2903 = load i32, i32* %arrayidx2373, align 4, !tbaa !7
  %2904 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2374 = sext i8 %2904 to i32
  %call2375 = call i32 @half_btf(i32 %2897, i32 %2899, i32 %2901, i32 %2903, i32 %conv2374)
  %2905 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2376 = getelementptr inbounds i32, i32* %2905, i32 50
  store i32 %call2375, i32* %arrayidx2376, align 4, !tbaa !7
  %2906 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2377 = getelementptr inbounds i32, i32* %2906, i32 51
  %2907 = load i32, i32* %arrayidx2377, align 4, !tbaa !7
  %2908 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2378 = getelementptr inbounds i32, i32* %2908, i32 51
  store i32 %2907, i32* %arrayidx2378, align 4, !tbaa !7
  %2909 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2379 = getelementptr inbounds i32, i32* %2909, i32 52
  %2910 = load i32, i32* %arrayidx2379, align 4, !tbaa !7
  %2911 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2380 = getelementptr inbounds i32, i32* %2911, i32 52
  store i32 %2910, i32* %arrayidx2380, align 4, !tbaa !7
  %2912 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2381 = getelementptr inbounds i32, i32* %2912, i32 44
  %2913 = load i32, i32* %arrayidx2381, align 4, !tbaa !7
  %2914 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2382 = getelementptr inbounds i32, i32* %2914, i32 53
  %2915 = load i32, i32* %arrayidx2382, align 4, !tbaa !7
  %2916 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2383 = getelementptr inbounds i32, i32* %2916, i32 20
  %2917 = load i32, i32* %arrayidx2383, align 4, !tbaa !7
  %sub2384 = sub nsw i32 0, %2917
  %2918 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2385 = getelementptr inbounds i32, i32* %2918, i32 42
  %2919 = load i32, i32* %arrayidx2385, align 4, !tbaa !7
  %2920 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2386 = sext i8 %2920 to i32
  %call2387 = call i32 @half_btf(i32 %2913, i32 %2915, i32 %sub2384, i32 %2919, i32 %conv2386)
  %2921 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2388 = getelementptr inbounds i32, i32* %2921, i32 53
  store i32 %call2387, i32* %arrayidx2388, align 4, !tbaa !7
  %2922 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2389 = getelementptr inbounds i32, i32* %2922, i32 20
  %2923 = load i32, i32* %arrayidx2389, align 4, !tbaa !7
  %2924 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2390 = getelementptr inbounds i32, i32* %2924, i32 54
  %2925 = load i32, i32* %arrayidx2390, align 4, !tbaa !7
  %2926 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2391 = getelementptr inbounds i32, i32* %2926, i32 44
  %2927 = load i32, i32* %arrayidx2391, align 4, !tbaa !7
  %2928 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2392 = getelementptr inbounds i32, i32* %2928, i32 41
  %2929 = load i32, i32* %arrayidx2392, align 4, !tbaa !7
  %2930 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2393 = sext i8 %2930 to i32
  %call2394 = call i32 @half_btf(i32 %2923, i32 %2925, i32 %2927, i32 %2929, i32 %conv2393)
  %2931 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2395 = getelementptr inbounds i32, i32* %2931, i32 54
  store i32 %call2394, i32* %arrayidx2395, align 4, !tbaa !7
  %2932 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2396 = getelementptr inbounds i32, i32* %2932, i32 55
  %2933 = load i32, i32* %arrayidx2396, align 4, !tbaa !7
  %2934 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2397 = getelementptr inbounds i32, i32* %2934, i32 55
  store i32 %2933, i32* %arrayidx2397, align 4, !tbaa !7
  %2935 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2398 = getelementptr inbounds i32, i32* %2935, i32 56
  %2936 = load i32, i32* %arrayidx2398, align 4, !tbaa !7
  %2937 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2399 = getelementptr inbounds i32, i32* %2937, i32 56
  store i32 %2936, i32* %arrayidx2399, align 4, !tbaa !7
  %2938 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2400 = getelementptr inbounds i32, i32* %2938, i32 28
  %2939 = load i32, i32* %arrayidx2400, align 4, !tbaa !7
  %2940 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2401 = getelementptr inbounds i32, i32* %2940, i32 57
  %2941 = load i32, i32* %arrayidx2401, align 4, !tbaa !7
  %2942 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2402 = getelementptr inbounds i32, i32* %2942, i32 36
  %2943 = load i32, i32* %arrayidx2402, align 4, !tbaa !7
  %sub2403 = sub nsw i32 0, %2943
  %2944 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2404 = getelementptr inbounds i32, i32* %2944, i32 38
  %2945 = load i32, i32* %arrayidx2404, align 4, !tbaa !7
  %2946 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2405 = sext i8 %2946 to i32
  %call2406 = call i32 @half_btf(i32 %2939, i32 %2941, i32 %sub2403, i32 %2945, i32 %conv2405)
  %2947 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2407 = getelementptr inbounds i32, i32* %2947, i32 57
  store i32 %call2406, i32* %arrayidx2407, align 4, !tbaa !7
  %2948 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2408 = getelementptr inbounds i32, i32* %2948, i32 36
  %2949 = load i32, i32* %arrayidx2408, align 4, !tbaa !7
  %2950 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2409 = getelementptr inbounds i32, i32* %2950, i32 58
  %2951 = load i32, i32* %arrayidx2409, align 4, !tbaa !7
  %2952 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2410 = getelementptr inbounds i32, i32* %2952, i32 28
  %2953 = load i32, i32* %arrayidx2410, align 4, !tbaa !7
  %2954 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2411 = getelementptr inbounds i32, i32* %2954, i32 37
  %2955 = load i32, i32* %arrayidx2411, align 4, !tbaa !7
  %2956 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2412 = sext i8 %2956 to i32
  %call2413 = call i32 @half_btf(i32 %2949, i32 %2951, i32 %2953, i32 %2955, i32 %conv2412)
  %2957 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2414 = getelementptr inbounds i32, i32* %2957, i32 58
  store i32 %call2413, i32* %arrayidx2414, align 4, !tbaa !7
  %2958 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2415 = getelementptr inbounds i32, i32* %2958, i32 59
  %2959 = load i32, i32* %arrayidx2415, align 4, !tbaa !7
  %2960 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2416 = getelementptr inbounds i32, i32* %2960, i32 59
  store i32 %2959, i32* %arrayidx2416, align 4, !tbaa !7
  %2961 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2417 = getelementptr inbounds i32, i32* %2961, i32 60
  %2962 = load i32, i32* %arrayidx2417, align 4, !tbaa !7
  %2963 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2418 = getelementptr inbounds i32, i32* %2963, i32 60
  store i32 %2962, i32* %arrayidx2418, align 4, !tbaa !7
  %2964 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2419 = getelementptr inbounds i32, i32* %2964, i32 60
  %2965 = load i32, i32* %arrayidx2419, align 4, !tbaa !7
  %2966 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2420 = getelementptr inbounds i32, i32* %2966, i32 61
  %2967 = load i32, i32* %arrayidx2420, align 4, !tbaa !7
  %2968 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2421 = getelementptr inbounds i32, i32* %2968, i32 4
  %2969 = load i32, i32* %arrayidx2421, align 4, !tbaa !7
  %sub2422 = sub nsw i32 0, %2969
  %2970 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2423 = getelementptr inbounds i32, i32* %2970, i32 34
  %2971 = load i32, i32* %arrayidx2423, align 4, !tbaa !7
  %2972 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2424 = sext i8 %2972 to i32
  %call2425 = call i32 @half_btf(i32 %2965, i32 %2967, i32 %sub2422, i32 %2971, i32 %conv2424)
  %2973 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2426 = getelementptr inbounds i32, i32* %2973, i32 61
  store i32 %call2425, i32* %arrayidx2426, align 4, !tbaa !7
  %2974 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2427 = getelementptr inbounds i32, i32* %2974, i32 4
  %2975 = load i32, i32* %arrayidx2427, align 4, !tbaa !7
  %2976 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2428 = getelementptr inbounds i32, i32* %2976, i32 62
  %2977 = load i32, i32* %arrayidx2428, align 4, !tbaa !7
  %2978 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2429 = getelementptr inbounds i32, i32* %2978, i32 60
  %2979 = load i32, i32* %arrayidx2429, align 4, !tbaa !7
  %2980 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2430 = getelementptr inbounds i32, i32* %2980, i32 33
  %2981 = load i32, i32* %arrayidx2430, align 4, !tbaa !7
  %2982 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2431 = sext i8 %2982 to i32
  %call2432 = call i32 @half_btf(i32 %2975, i32 %2977, i32 %2979, i32 %2981, i32 %conv2431)
  %2983 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2433 = getelementptr inbounds i32, i32* %2983, i32 62
  store i32 %call2432, i32* %arrayidx2433, align 4, !tbaa !7
  %2984 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2434 = getelementptr inbounds i32, i32* %2984, i32 63
  %2985 = load i32, i32* %arrayidx2434, align 4, !tbaa !7
  %2986 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2435 = getelementptr inbounds i32, i32* %2986, i32 63
  store i32 %2985, i32* %arrayidx2435, align 4, !tbaa !7
  %2987 = load i32, i32* %stage, align 4, !tbaa !7
  %2988 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %2989 = load i32*, i32** %bf1, align 4, !tbaa !2
  %2990 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %2991 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx2436 = getelementptr inbounds i8, i8* %2990, i32 %2991
  %2992 = load i8, i8* %arrayidx2436, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %2987, i32* %2988, i32* %2989, i32 64, i8 signext %2992)
  %2993 = load i32, i32* %stage, align 4, !tbaa !7
  %inc2437 = add nsw i32 %2993, 1
  store i32 %inc2437, i32* %stage, align 4, !tbaa !7
  %2994 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2438 = sext i8 %2994 to i32
  %call2439 = call i32* @cospi_arr(i32 %conv2438)
  store i32* %call2439, i32** %cospi, align 4, !tbaa !2
  %arraydecay2440 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay2440, i32** %bf0, align 4, !tbaa !2
  %2995 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %2995, i32** %bf1, align 4, !tbaa !2
  %2996 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2441 = getelementptr inbounds i32, i32* %2996, i32 0
  %2997 = load i32, i32* %arrayidx2441, align 4, !tbaa !7
  %2998 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2442 = getelementptr inbounds i32, i32* %2998, i32 0
  store i32 %2997, i32* %arrayidx2442, align 4, !tbaa !7
  %2999 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2443 = getelementptr inbounds i32, i32* %2999, i32 1
  %3000 = load i32, i32* %arrayidx2443, align 4, !tbaa !7
  %3001 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2444 = getelementptr inbounds i32, i32* %3001, i32 1
  store i32 %3000, i32* %arrayidx2444, align 4, !tbaa !7
  %3002 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2445 = getelementptr inbounds i32, i32* %3002, i32 2
  %3003 = load i32, i32* %arrayidx2445, align 4, !tbaa !7
  %3004 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2446 = getelementptr inbounds i32, i32* %3004, i32 2
  store i32 %3003, i32* %arrayidx2446, align 4, !tbaa !7
  %3005 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2447 = getelementptr inbounds i32, i32* %3005, i32 3
  %3006 = load i32, i32* %arrayidx2447, align 4, !tbaa !7
  %3007 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2448 = getelementptr inbounds i32, i32* %3007, i32 3
  store i32 %3006, i32* %arrayidx2448, align 4, !tbaa !7
  %3008 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2449 = getelementptr inbounds i32, i32* %3008, i32 4
  %3009 = load i32, i32* %arrayidx2449, align 4, !tbaa !7
  %3010 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2450 = getelementptr inbounds i32, i32* %3010, i32 4
  store i32 %3009, i32* %arrayidx2450, align 4, !tbaa !7
  %3011 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2451 = getelementptr inbounds i32, i32* %3011, i32 5
  %3012 = load i32, i32* %arrayidx2451, align 4, !tbaa !7
  %3013 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2452 = getelementptr inbounds i32, i32* %3013, i32 5
  store i32 %3012, i32* %arrayidx2452, align 4, !tbaa !7
  %3014 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2453 = getelementptr inbounds i32, i32* %3014, i32 6
  %3015 = load i32, i32* %arrayidx2453, align 4, !tbaa !7
  %3016 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2454 = getelementptr inbounds i32, i32* %3016, i32 6
  store i32 %3015, i32* %arrayidx2454, align 4, !tbaa !7
  %3017 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2455 = getelementptr inbounds i32, i32* %3017, i32 7
  %3018 = load i32, i32* %arrayidx2455, align 4, !tbaa !7
  %3019 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2456 = getelementptr inbounds i32, i32* %3019, i32 7
  store i32 %3018, i32* %arrayidx2456, align 4, !tbaa !7
  %3020 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2457 = getelementptr inbounds i32, i32* %3020, i32 8
  %3021 = load i32, i32* %arrayidx2457, align 4, !tbaa !7
  %3022 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2458 = getelementptr inbounds i32, i32* %3022, i32 8
  store i32 %3021, i32* %arrayidx2458, align 4, !tbaa !7
  %3023 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2459 = getelementptr inbounds i32, i32* %3023, i32 9
  %3024 = load i32, i32* %arrayidx2459, align 4, !tbaa !7
  %3025 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2460 = getelementptr inbounds i32, i32* %3025, i32 9
  store i32 %3024, i32* %arrayidx2460, align 4, !tbaa !7
  %3026 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2461 = getelementptr inbounds i32, i32* %3026, i32 10
  %3027 = load i32, i32* %arrayidx2461, align 4, !tbaa !7
  %3028 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2462 = getelementptr inbounds i32, i32* %3028, i32 10
  store i32 %3027, i32* %arrayidx2462, align 4, !tbaa !7
  %3029 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2463 = getelementptr inbounds i32, i32* %3029, i32 11
  %3030 = load i32, i32* %arrayidx2463, align 4, !tbaa !7
  %3031 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2464 = getelementptr inbounds i32, i32* %3031, i32 11
  store i32 %3030, i32* %arrayidx2464, align 4, !tbaa !7
  %3032 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2465 = getelementptr inbounds i32, i32* %3032, i32 12
  %3033 = load i32, i32* %arrayidx2465, align 4, !tbaa !7
  %3034 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2466 = getelementptr inbounds i32, i32* %3034, i32 12
  store i32 %3033, i32* %arrayidx2466, align 4, !tbaa !7
  %3035 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2467 = getelementptr inbounds i32, i32* %3035, i32 13
  %3036 = load i32, i32* %arrayidx2467, align 4, !tbaa !7
  %3037 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2468 = getelementptr inbounds i32, i32* %3037, i32 13
  store i32 %3036, i32* %arrayidx2468, align 4, !tbaa !7
  %3038 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2469 = getelementptr inbounds i32, i32* %3038, i32 14
  %3039 = load i32, i32* %arrayidx2469, align 4, !tbaa !7
  %3040 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2470 = getelementptr inbounds i32, i32* %3040, i32 14
  store i32 %3039, i32* %arrayidx2470, align 4, !tbaa !7
  %3041 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2471 = getelementptr inbounds i32, i32* %3041, i32 15
  %3042 = load i32, i32* %arrayidx2471, align 4, !tbaa !7
  %3043 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2472 = getelementptr inbounds i32, i32* %3043, i32 15
  store i32 %3042, i32* %arrayidx2472, align 4, !tbaa !7
  %3044 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2473 = getelementptr inbounds i32, i32* %3044, i32 62
  %3045 = load i32, i32* %arrayidx2473, align 4, !tbaa !7
  %3046 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2474 = getelementptr inbounds i32, i32* %3046, i32 16
  %3047 = load i32, i32* %arrayidx2474, align 4, !tbaa !7
  %3048 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2475 = getelementptr inbounds i32, i32* %3048, i32 2
  %3049 = load i32, i32* %arrayidx2475, align 4, !tbaa !7
  %3050 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2476 = getelementptr inbounds i32, i32* %3050, i32 31
  %3051 = load i32, i32* %arrayidx2476, align 4, !tbaa !7
  %3052 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2477 = sext i8 %3052 to i32
  %call2478 = call i32 @half_btf(i32 %3045, i32 %3047, i32 %3049, i32 %3051, i32 %conv2477)
  %3053 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2479 = getelementptr inbounds i32, i32* %3053, i32 16
  store i32 %call2478, i32* %arrayidx2479, align 4, !tbaa !7
  %3054 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2480 = getelementptr inbounds i32, i32* %3054, i32 30
  %3055 = load i32, i32* %arrayidx2480, align 4, !tbaa !7
  %3056 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2481 = getelementptr inbounds i32, i32* %3056, i32 17
  %3057 = load i32, i32* %arrayidx2481, align 4, !tbaa !7
  %3058 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2482 = getelementptr inbounds i32, i32* %3058, i32 34
  %3059 = load i32, i32* %arrayidx2482, align 4, !tbaa !7
  %3060 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2483 = getelementptr inbounds i32, i32* %3060, i32 30
  %3061 = load i32, i32* %arrayidx2483, align 4, !tbaa !7
  %3062 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2484 = sext i8 %3062 to i32
  %call2485 = call i32 @half_btf(i32 %3055, i32 %3057, i32 %3059, i32 %3061, i32 %conv2484)
  %3063 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2486 = getelementptr inbounds i32, i32* %3063, i32 17
  store i32 %call2485, i32* %arrayidx2486, align 4, !tbaa !7
  %3064 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2487 = getelementptr inbounds i32, i32* %3064, i32 46
  %3065 = load i32, i32* %arrayidx2487, align 4, !tbaa !7
  %3066 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2488 = getelementptr inbounds i32, i32* %3066, i32 18
  %3067 = load i32, i32* %arrayidx2488, align 4, !tbaa !7
  %3068 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2489 = getelementptr inbounds i32, i32* %3068, i32 18
  %3069 = load i32, i32* %arrayidx2489, align 4, !tbaa !7
  %3070 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2490 = getelementptr inbounds i32, i32* %3070, i32 29
  %3071 = load i32, i32* %arrayidx2490, align 4, !tbaa !7
  %3072 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2491 = sext i8 %3072 to i32
  %call2492 = call i32 @half_btf(i32 %3065, i32 %3067, i32 %3069, i32 %3071, i32 %conv2491)
  %3073 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2493 = getelementptr inbounds i32, i32* %3073, i32 18
  store i32 %call2492, i32* %arrayidx2493, align 4, !tbaa !7
  %3074 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2494 = getelementptr inbounds i32, i32* %3074, i32 14
  %3075 = load i32, i32* %arrayidx2494, align 4, !tbaa !7
  %3076 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2495 = getelementptr inbounds i32, i32* %3076, i32 19
  %3077 = load i32, i32* %arrayidx2495, align 4, !tbaa !7
  %3078 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2496 = getelementptr inbounds i32, i32* %3078, i32 50
  %3079 = load i32, i32* %arrayidx2496, align 4, !tbaa !7
  %3080 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2497 = getelementptr inbounds i32, i32* %3080, i32 28
  %3081 = load i32, i32* %arrayidx2497, align 4, !tbaa !7
  %3082 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2498 = sext i8 %3082 to i32
  %call2499 = call i32 @half_btf(i32 %3075, i32 %3077, i32 %3079, i32 %3081, i32 %conv2498)
  %3083 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2500 = getelementptr inbounds i32, i32* %3083, i32 19
  store i32 %call2499, i32* %arrayidx2500, align 4, !tbaa !7
  %3084 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2501 = getelementptr inbounds i32, i32* %3084, i32 54
  %3085 = load i32, i32* %arrayidx2501, align 4, !tbaa !7
  %3086 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2502 = getelementptr inbounds i32, i32* %3086, i32 20
  %3087 = load i32, i32* %arrayidx2502, align 4, !tbaa !7
  %3088 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2503 = getelementptr inbounds i32, i32* %3088, i32 10
  %3089 = load i32, i32* %arrayidx2503, align 4, !tbaa !7
  %3090 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2504 = getelementptr inbounds i32, i32* %3090, i32 27
  %3091 = load i32, i32* %arrayidx2504, align 4, !tbaa !7
  %3092 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2505 = sext i8 %3092 to i32
  %call2506 = call i32 @half_btf(i32 %3085, i32 %3087, i32 %3089, i32 %3091, i32 %conv2505)
  %3093 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2507 = getelementptr inbounds i32, i32* %3093, i32 20
  store i32 %call2506, i32* %arrayidx2507, align 4, !tbaa !7
  %3094 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2508 = getelementptr inbounds i32, i32* %3094, i32 22
  %3095 = load i32, i32* %arrayidx2508, align 4, !tbaa !7
  %3096 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2509 = getelementptr inbounds i32, i32* %3096, i32 21
  %3097 = load i32, i32* %arrayidx2509, align 4, !tbaa !7
  %3098 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2510 = getelementptr inbounds i32, i32* %3098, i32 42
  %3099 = load i32, i32* %arrayidx2510, align 4, !tbaa !7
  %3100 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2511 = getelementptr inbounds i32, i32* %3100, i32 26
  %3101 = load i32, i32* %arrayidx2511, align 4, !tbaa !7
  %3102 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2512 = sext i8 %3102 to i32
  %call2513 = call i32 @half_btf(i32 %3095, i32 %3097, i32 %3099, i32 %3101, i32 %conv2512)
  %3103 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2514 = getelementptr inbounds i32, i32* %3103, i32 21
  store i32 %call2513, i32* %arrayidx2514, align 4, !tbaa !7
  %3104 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2515 = getelementptr inbounds i32, i32* %3104, i32 38
  %3105 = load i32, i32* %arrayidx2515, align 4, !tbaa !7
  %3106 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2516 = getelementptr inbounds i32, i32* %3106, i32 22
  %3107 = load i32, i32* %arrayidx2516, align 4, !tbaa !7
  %3108 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2517 = getelementptr inbounds i32, i32* %3108, i32 26
  %3109 = load i32, i32* %arrayidx2517, align 4, !tbaa !7
  %3110 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2518 = getelementptr inbounds i32, i32* %3110, i32 25
  %3111 = load i32, i32* %arrayidx2518, align 4, !tbaa !7
  %3112 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2519 = sext i8 %3112 to i32
  %call2520 = call i32 @half_btf(i32 %3105, i32 %3107, i32 %3109, i32 %3111, i32 %conv2519)
  %3113 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2521 = getelementptr inbounds i32, i32* %3113, i32 22
  store i32 %call2520, i32* %arrayidx2521, align 4, !tbaa !7
  %3114 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2522 = getelementptr inbounds i32, i32* %3114, i32 6
  %3115 = load i32, i32* %arrayidx2522, align 4, !tbaa !7
  %3116 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2523 = getelementptr inbounds i32, i32* %3116, i32 23
  %3117 = load i32, i32* %arrayidx2523, align 4, !tbaa !7
  %3118 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2524 = getelementptr inbounds i32, i32* %3118, i32 58
  %3119 = load i32, i32* %arrayidx2524, align 4, !tbaa !7
  %3120 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2525 = getelementptr inbounds i32, i32* %3120, i32 24
  %3121 = load i32, i32* %arrayidx2525, align 4, !tbaa !7
  %3122 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2526 = sext i8 %3122 to i32
  %call2527 = call i32 @half_btf(i32 %3115, i32 %3117, i32 %3119, i32 %3121, i32 %conv2526)
  %3123 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2528 = getelementptr inbounds i32, i32* %3123, i32 23
  store i32 %call2527, i32* %arrayidx2528, align 4, !tbaa !7
  %3124 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2529 = getelementptr inbounds i32, i32* %3124, i32 6
  %3125 = load i32, i32* %arrayidx2529, align 4, !tbaa !7
  %3126 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2530 = getelementptr inbounds i32, i32* %3126, i32 24
  %3127 = load i32, i32* %arrayidx2530, align 4, !tbaa !7
  %3128 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2531 = getelementptr inbounds i32, i32* %3128, i32 58
  %3129 = load i32, i32* %arrayidx2531, align 4, !tbaa !7
  %sub2532 = sub nsw i32 0, %3129
  %3130 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2533 = getelementptr inbounds i32, i32* %3130, i32 23
  %3131 = load i32, i32* %arrayidx2533, align 4, !tbaa !7
  %3132 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2534 = sext i8 %3132 to i32
  %call2535 = call i32 @half_btf(i32 %3125, i32 %3127, i32 %sub2532, i32 %3131, i32 %conv2534)
  %3133 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2536 = getelementptr inbounds i32, i32* %3133, i32 24
  store i32 %call2535, i32* %arrayidx2536, align 4, !tbaa !7
  %3134 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2537 = getelementptr inbounds i32, i32* %3134, i32 38
  %3135 = load i32, i32* %arrayidx2537, align 4, !tbaa !7
  %3136 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2538 = getelementptr inbounds i32, i32* %3136, i32 25
  %3137 = load i32, i32* %arrayidx2538, align 4, !tbaa !7
  %3138 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2539 = getelementptr inbounds i32, i32* %3138, i32 26
  %3139 = load i32, i32* %arrayidx2539, align 4, !tbaa !7
  %sub2540 = sub nsw i32 0, %3139
  %3140 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2541 = getelementptr inbounds i32, i32* %3140, i32 22
  %3141 = load i32, i32* %arrayidx2541, align 4, !tbaa !7
  %3142 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2542 = sext i8 %3142 to i32
  %call2543 = call i32 @half_btf(i32 %3135, i32 %3137, i32 %sub2540, i32 %3141, i32 %conv2542)
  %3143 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2544 = getelementptr inbounds i32, i32* %3143, i32 25
  store i32 %call2543, i32* %arrayidx2544, align 4, !tbaa !7
  %3144 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2545 = getelementptr inbounds i32, i32* %3144, i32 22
  %3145 = load i32, i32* %arrayidx2545, align 4, !tbaa !7
  %3146 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2546 = getelementptr inbounds i32, i32* %3146, i32 26
  %3147 = load i32, i32* %arrayidx2546, align 4, !tbaa !7
  %3148 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2547 = getelementptr inbounds i32, i32* %3148, i32 42
  %3149 = load i32, i32* %arrayidx2547, align 4, !tbaa !7
  %sub2548 = sub nsw i32 0, %3149
  %3150 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2549 = getelementptr inbounds i32, i32* %3150, i32 21
  %3151 = load i32, i32* %arrayidx2549, align 4, !tbaa !7
  %3152 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2550 = sext i8 %3152 to i32
  %call2551 = call i32 @half_btf(i32 %3145, i32 %3147, i32 %sub2548, i32 %3151, i32 %conv2550)
  %3153 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2552 = getelementptr inbounds i32, i32* %3153, i32 26
  store i32 %call2551, i32* %arrayidx2552, align 4, !tbaa !7
  %3154 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2553 = getelementptr inbounds i32, i32* %3154, i32 54
  %3155 = load i32, i32* %arrayidx2553, align 4, !tbaa !7
  %3156 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2554 = getelementptr inbounds i32, i32* %3156, i32 27
  %3157 = load i32, i32* %arrayidx2554, align 4, !tbaa !7
  %3158 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2555 = getelementptr inbounds i32, i32* %3158, i32 10
  %3159 = load i32, i32* %arrayidx2555, align 4, !tbaa !7
  %sub2556 = sub nsw i32 0, %3159
  %3160 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2557 = getelementptr inbounds i32, i32* %3160, i32 20
  %3161 = load i32, i32* %arrayidx2557, align 4, !tbaa !7
  %3162 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2558 = sext i8 %3162 to i32
  %call2559 = call i32 @half_btf(i32 %3155, i32 %3157, i32 %sub2556, i32 %3161, i32 %conv2558)
  %3163 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2560 = getelementptr inbounds i32, i32* %3163, i32 27
  store i32 %call2559, i32* %arrayidx2560, align 4, !tbaa !7
  %3164 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2561 = getelementptr inbounds i32, i32* %3164, i32 14
  %3165 = load i32, i32* %arrayidx2561, align 4, !tbaa !7
  %3166 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2562 = getelementptr inbounds i32, i32* %3166, i32 28
  %3167 = load i32, i32* %arrayidx2562, align 4, !tbaa !7
  %3168 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2563 = getelementptr inbounds i32, i32* %3168, i32 50
  %3169 = load i32, i32* %arrayidx2563, align 4, !tbaa !7
  %sub2564 = sub nsw i32 0, %3169
  %3170 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2565 = getelementptr inbounds i32, i32* %3170, i32 19
  %3171 = load i32, i32* %arrayidx2565, align 4, !tbaa !7
  %3172 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2566 = sext i8 %3172 to i32
  %call2567 = call i32 @half_btf(i32 %3165, i32 %3167, i32 %sub2564, i32 %3171, i32 %conv2566)
  %3173 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2568 = getelementptr inbounds i32, i32* %3173, i32 28
  store i32 %call2567, i32* %arrayidx2568, align 4, !tbaa !7
  %3174 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2569 = getelementptr inbounds i32, i32* %3174, i32 46
  %3175 = load i32, i32* %arrayidx2569, align 4, !tbaa !7
  %3176 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2570 = getelementptr inbounds i32, i32* %3176, i32 29
  %3177 = load i32, i32* %arrayidx2570, align 4, !tbaa !7
  %3178 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2571 = getelementptr inbounds i32, i32* %3178, i32 18
  %3179 = load i32, i32* %arrayidx2571, align 4, !tbaa !7
  %sub2572 = sub nsw i32 0, %3179
  %3180 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2573 = getelementptr inbounds i32, i32* %3180, i32 18
  %3181 = load i32, i32* %arrayidx2573, align 4, !tbaa !7
  %3182 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2574 = sext i8 %3182 to i32
  %call2575 = call i32 @half_btf(i32 %3175, i32 %3177, i32 %sub2572, i32 %3181, i32 %conv2574)
  %3183 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2576 = getelementptr inbounds i32, i32* %3183, i32 29
  store i32 %call2575, i32* %arrayidx2576, align 4, !tbaa !7
  %3184 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2577 = getelementptr inbounds i32, i32* %3184, i32 30
  %3185 = load i32, i32* %arrayidx2577, align 4, !tbaa !7
  %3186 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2578 = getelementptr inbounds i32, i32* %3186, i32 30
  %3187 = load i32, i32* %arrayidx2578, align 4, !tbaa !7
  %3188 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2579 = getelementptr inbounds i32, i32* %3188, i32 34
  %3189 = load i32, i32* %arrayidx2579, align 4, !tbaa !7
  %sub2580 = sub nsw i32 0, %3189
  %3190 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2581 = getelementptr inbounds i32, i32* %3190, i32 17
  %3191 = load i32, i32* %arrayidx2581, align 4, !tbaa !7
  %3192 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2582 = sext i8 %3192 to i32
  %call2583 = call i32 @half_btf(i32 %3185, i32 %3187, i32 %sub2580, i32 %3191, i32 %conv2582)
  %3193 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2584 = getelementptr inbounds i32, i32* %3193, i32 30
  store i32 %call2583, i32* %arrayidx2584, align 4, !tbaa !7
  %3194 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2585 = getelementptr inbounds i32, i32* %3194, i32 62
  %3195 = load i32, i32* %arrayidx2585, align 4, !tbaa !7
  %3196 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2586 = getelementptr inbounds i32, i32* %3196, i32 31
  %3197 = load i32, i32* %arrayidx2586, align 4, !tbaa !7
  %3198 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2587 = getelementptr inbounds i32, i32* %3198, i32 2
  %3199 = load i32, i32* %arrayidx2587, align 4, !tbaa !7
  %sub2588 = sub nsw i32 0, %3199
  %3200 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2589 = getelementptr inbounds i32, i32* %3200, i32 16
  %3201 = load i32, i32* %arrayidx2589, align 4, !tbaa !7
  %3202 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2590 = sext i8 %3202 to i32
  %call2591 = call i32 @half_btf(i32 %3195, i32 %3197, i32 %sub2588, i32 %3201, i32 %conv2590)
  %3203 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2592 = getelementptr inbounds i32, i32* %3203, i32 31
  store i32 %call2591, i32* %arrayidx2592, align 4, !tbaa !7
  %3204 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2593 = getelementptr inbounds i32, i32* %3204, i32 32
  %3205 = load i32, i32* %arrayidx2593, align 4, !tbaa !7
  %3206 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2594 = getelementptr inbounds i32, i32* %3206, i32 33
  %3207 = load i32, i32* %arrayidx2594, align 4, !tbaa !7
  %add2595 = add nsw i32 %3205, %3207
  %3208 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2596 = getelementptr inbounds i32, i32* %3208, i32 32
  store i32 %add2595, i32* %arrayidx2596, align 4, !tbaa !7
  %3209 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2597 = getelementptr inbounds i32, i32* %3209, i32 33
  %3210 = load i32, i32* %arrayidx2597, align 4, !tbaa !7
  %sub2598 = sub nsw i32 0, %3210
  %3211 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2599 = getelementptr inbounds i32, i32* %3211, i32 32
  %3212 = load i32, i32* %arrayidx2599, align 4, !tbaa !7
  %add2600 = add nsw i32 %sub2598, %3212
  %3213 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2601 = getelementptr inbounds i32, i32* %3213, i32 33
  store i32 %add2600, i32* %arrayidx2601, align 4, !tbaa !7
  %3214 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2602 = getelementptr inbounds i32, i32* %3214, i32 34
  %3215 = load i32, i32* %arrayidx2602, align 4, !tbaa !7
  %sub2603 = sub nsw i32 0, %3215
  %3216 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2604 = getelementptr inbounds i32, i32* %3216, i32 35
  %3217 = load i32, i32* %arrayidx2604, align 4, !tbaa !7
  %add2605 = add nsw i32 %sub2603, %3217
  %3218 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2606 = getelementptr inbounds i32, i32* %3218, i32 34
  store i32 %add2605, i32* %arrayidx2606, align 4, !tbaa !7
  %3219 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2607 = getelementptr inbounds i32, i32* %3219, i32 35
  %3220 = load i32, i32* %arrayidx2607, align 4, !tbaa !7
  %3221 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2608 = getelementptr inbounds i32, i32* %3221, i32 34
  %3222 = load i32, i32* %arrayidx2608, align 4, !tbaa !7
  %add2609 = add nsw i32 %3220, %3222
  %3223 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2610 = getelementptr inbounds i32, i32* %3223, i32 35
  store i32 %add2609, i32* %arrayidx2610, align 4, !tbaa !7
  %3224 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2611 = getelementptr inbounds i32, i32* %3224, i32 36
  %3225 = load i32, i32* %arrayidx2611, align 4, !tbaa !7
  %3226 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2612 = getelementptr inbounds i32, i32* %3226, i32 37
  %3227 = load i32, i32* %arrayidx2612, align 4, !tbaa !7
  %add2613 = add nsw i32 %3225, %3227
  %3228 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2614 = getelementptr inbounds i32, i32* %3228, i32 36
  store i32 %add2613, i32* %arrayidx2614, align 4, !tbaa !7
  %3229 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2615 = getelementptr inbounds i32, i32* %3229, i32 37
  %3230 = load i32, i32* %arrayidx2615, align 4, !tbaa !7
  %sub2616 = sub nsw i32 0, %3230
  %3231 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2617 = getelementptr inbounds i32, i32* %3231, i32 36
  %3232 = load i32, i32* %arrayidx2617, align 4, !tbaa !7
  %add2618 = add nsw i32 %sub2616, %3232
  %3233 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2619 = getelementptr inbounds i32, i32* %3233, i32 37
  store i32 %add2618, i32* %arrayidx2619, align 4, !tbaa !7
  %3234 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2620 = getelementptr inbounds i32, i32* %3234, i32 38
  %3235 = load i32, i32* %arrayidx2620, align 4, !tbaa !7
  %sub2621 = sub nsw i32 0, %3235
  %3236 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2622 = getelementptr inbounds i32, i32* %3236, i32 39
  %3237 = load i32, i32* %arrayidx2622, align 4, !tbaa !7
  %add2623 = add nsw i32 %sub2621, %3237
  %3238 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2624 = getelementptr inbounds i32, i32* %3238, i32 38
  store i32 %add2623, i32* %arrayidx2624, align 4, !tbaa !7
  %3239 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2625 = getelementptr inbounds i32, i32* %3239, i32 39
  %3240 = load i32, i32* %arrayidx2625, align 4, !tbaa !7
  %3241 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2626 = getelementptr inbounds i32, i32* %3241, i32 38
  %3242 = load i32, i32* %arrayidx2626, align 4, !tbaa !7
  %add2627 = add nsw i32 %3240, %3242
  %3243 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2628 = getelementptr inbounds i32, i32* %3243, i32 39
  store i32 %add2627, i32* %arrayidx2628, align 4, !tbaa !7
  %3244 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2629 = getelementptr inbounds i32, i32* %3244, i32 40
  %3245 = load i32, i32* %arrayidx2629, align 4, !tbaa !7
  %3246 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2630 = getelementptr inbounds i32, i32* %3246, i32 41
  %3247 = load i32, i32* %arrayidx2630, align 4, !tbaa !7
  %add2631 = add nsw i32 %3245, %3247
  %3248 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2632 = getelementptr inbounds i32, i32* %3248, i32 40
  store i32 %add2631, i32* %arrayidx2632, align 4, !tbaa !7
  %3249 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2633 = getelementptr inbounds i32, i32* %3249, i32 41
  %3250 = load i32, i32* %arrayidx2633, align 4, !tbaa !7
  %sub2634 = sub nsw i32 0, %3250
  %3251 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2635 = getelementptr inbounds i32, i32* %3251, i32 40
  %3252 = load i32, i32* %arrayidx2635, align 4, !tbaa !7
  %add2636 = add nsw i32 %sub2634, %3252
  %3253 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2637 = getelementptr inbounds i32, i32* %3253, i32 41
  store i32 %add2636, i32* %arrayidx2637, align 4, !tbaa !7
  %3254 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2638 = getelementptr inbounds i32, i32* %3254, i32 42
  %3255 = load i32, i32* %arrayidx2638, align 4, !tbaa !7
  %sub2639 = sub nsw i32 0, %3255
  %3256 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2640 = getelementptr inbounds i32, i32* %3256, i32 43
  %3257 = load i32, i32* %arrayidx2640, align 4, !tbaa !7
  %add2641 = add nsw i32 %sub2639, %3257
  %3258 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2642 = getelementptr inbounds i32, i32* %3258, i32 42
  store i32 %add2641, i32* %arrayidx2642, align 4, !tbaa !7
  %3259 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2643 = getelementptr inbounds i32, i32* %3259, i32 43
  %3260 = load i32, i32* %arrayidx2643, align 4, !tbaa !7
  %3261 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2644 = getelementptr inbounds i32, i32* %3261, i32 42
  %3262 = load i32, i32* %arrayidx2644, align 4, !tbaa !7
  %add2645 = add nsw i32 %3260, %3262
  %3263 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2646 = getelementptr inbounds i32, i32* %3263, i32 43
  store i32 %add2645, i32* %arrayidx2646, align 4, !tbaa !7
  %3264 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2647 = getelementptr inbounds i32, i32* %3264, i32 44
  %3265 = load i32, i32* %arrayidx2647, align 4, !tbaa !7
  %3266 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2648 = getelementptr inbounds i32, i32* %3266, i32 45
  %3267 = load i32, i32* %arrayidx2648, align 4, !tbaa !7
  %add2649 = add nsw i32 %3265, %3267
  %3268 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2650 = getelementptr inbounds i32, i32* %3268, i32 44
  store i32 %add2649, i32* %arrayidx2650, align 4, !tbaa !7
  %3269 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2651 = getelementptr inbounds i32, i32* %3269, i32 45
  %3270 = load i32, i32* %arrayidx2651, align 4, !tbaa !7
  %sub2652 = sub nsw i32 0, %3270
  %3271 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2653 = getelementptr inbounds i32, i32* %3271, i32 44
  %3272 = load i32, i32* %arrayidx2653, align 4, !tbaa !7
  %add2654 = add nsw i32 %sub2652, %3272
  %3273 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2655 = getelementptr inbounds i32, i32* %3273, i32 45
  store i32 %add2654, i32* %arrayidx2655, align 4, !tbaa !7
  %3274 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2656 = getelementptr inbounds i32, i32* %3274, i32 46
  %3275 = load i32, i32* %arrayidx2656, align 4, !tbaa !7
  %sub2657 = sub nsw i32 0, %3275
  %3276 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2658 = getelementptr inbounds i32, i32* %3276, i32 47
  %3277 = load i32, i32* %arrayidx2658, align 4, !tbaa !7
  %add2659 = add nsw i32 %sub2657, %3277
  %3278 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2660 = getelementptr inbounds i32, i32* %3278, i32 46
  store i32 %add2659, i32* %arrayidx2660, align 4, !tbaa !7
  %3279 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2661 = getelementptr inbounds i32, i32* %3279, i32 47
  %3280 = load i32, i32* %arrayidx2661, align 4, !tbaa !7
  %3281 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2662 = getelementptr inbounds i32, i32* %3281, i32 46
  %3282 = load i32, i32* %arrayidx2662, align 4, !tbaa !7
  %add2663 = add nsw i32 %3280, %3282
  %3283 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2664 = getelementptr inbounds i32, i32* %3283, i32 47
  store i32 %add2663, i32* %arrayidx2664, align 4, !tbaa !7
  %3284 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2665 = getelementptr inbounds i32, i32* %3284, i32 48
  %3285 = load i32, i32* %arrayidx2665, align 4, !tbaa !7
  %3286 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2666 = getelementptr inbounds i32, i32* %3286, i32 49
  %3287 = load i32, i32* %arrayidx2666, align 4, !tbaa !7
  %add2667 = add nsw i32 %3285, %3287
  %3288 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2668 = getelementptr inbounds i32, i32* %3288, i32 48
  store i32 %add2667, i32* %arrayidx2668, align 4, !tbaa !7
  %3289 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2669 = getelementptr inbounds i32, i32* %3289, i32 49
  %3290 = load i32, i32* %arrayidx2669, align 4, !tbaa !7
  %sub2670 = sub nsw i32 0, %3290
  %3291 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2671 = getelementptr inbounds i32, i32* %3291, i32 48
  %3292 = load i32, i32* %arrayidx2671, align 4, !tbaa !7
  %add2672 = add nsw i32 %sub2670, %3292
  %3293 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2673 = getelementptr inbounds i32, i32* %3293, i32 49
  store i32 %add2672, i32* %arrayidx2673, align 4, !tbaa !7
  %3294 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2674 = getelementptr inbounds i32, i32* %3294, i32 50
  %3295 = load i32, i32* %arrayidx2674, align 4, !tbaa !7
  %sub2675 = sub nsw i32 0, %3295
  %3296 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2676 = getelementptr inbounds i32, i32* %3296, i32 51
  %3297 = load i32, i32* %arrayidx2676, align 4, !tbaa !7
  %add2677 = add nsw i32 %sub2675, %3297
  %3298 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2678 = getelementptr inbounds i32, i32* %3298, i32 50
  store i32 %add2677, i32* %arrayidx2678, align 4, !tbaa !7
  %3299 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2679 = getelementptr inbounds i32, i32* %3299, i32 51
  %3300 = load i32, i32* %arrayidx2679, align 4, !tbaa !7
  %3301 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2680 = getelementptr inbounds i32, i32* %3301, i32 50
  %3302 = load i32, i32* %arrayidx2680, align 4, !tbaa !7
  %add2681 = add nsw i32 %3300, %3302
  %3303 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2682 = getelementptr inbounds i32, i32* %3303, i32 51
  store i32 %add2681, i32* %arrayidx2682, align 4, !tbaa !7
  %3304 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2683 = getelementptr inbounds i32, i32* %3304, i32 52
  %3305 = load i32, i32* %arrayidx2683, align 4, !tbaa !7
  %3306 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2684 = getelementptr inbounds i32, i32* %3306, i32 53
  %3307 = load i32, i32* %arrayidx2684, align 4, !tbaa !7
  %add2685 = add nsw i32 %3305, %3307
  %3308 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2686 = getelementptr inbounds i32, i32* %3308, i32 52
  store i32 %add2685, i32* %arrayidx2686, align 4, !tbaa !7
  %3309 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2687 = getelementptr inbounds i32, i32* %3309, i32 53
  %3310 = load i32, i32* %arrayidx2687, align 4, !tbaa !7
  %sub2688 = sub nsw i32 0, %3310
  %3311 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2689 = getelementptr inbounds i32, i32* %3311, i32 52
  %3312 = load i32, i32* %arrayidx2689, align 4, !tbaa !7
  %add2690 = add nsw i32 %sub2688, %3312
  %3313 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2691 = getelementptr inbounds i32, i32* %3313, i32 53
  store i32 %add2690, i32* %arrayidx2691, align 4, !tbaa !7
  %3314 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2692 = getelementptr inbounds i32, i32* %3314, i32 54
  %3315 = load i32, i32* %arrayidx2692, align 4, !tbaa !7
  %sub2693 = sub nsw i32 0, %3315
  %3316 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2694 = getelementptr inbounds i32, i32* %3316, i32 55
  %3317 = load i32, i32* %arrayidx2694, align 4, !tbaa !7
  %add2695 = add nsw i32 %sub2693, %3317
  %3318 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2696 = getelementptr inbounds i32, i32* %3318, i32 54
  store i32 %add2695, i32* %arrayidx2696, align 4, !tbaa !7
  %3319 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2697 = getelementptr inbounds i32, i32* %3319, i32 55
  %3320 = load i32, i32* %arrayidx2697, align 4, !tbaa !7
  %3321 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2698 = getelementptr inbounds i32, i32* %3321, i32 54
  %3322 = load i32, i32* %arrayidx2698, align 4, !tbaa !7
  %add2699 = add nsw i32 %3320, %3322
  %3323 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2700 = getelementptr inbounds i32, i32* %3323, i32 55
  store i32 %add2699, i32* %arrayidx2700, align 4, !tbaa !7
  %3324 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2701 = getelementptr inbounds i32, i32* %3324, i32 56
  %3325 = load i32, i32* %arrayidx2701, align 4, !tbaa !7
  %3326 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2702 = getelementptr inbounds i32, i32* %3326, i32 57
  %3327 = load i32, i32* %arrayidx2702, align 4, !tbaa !7
  %add2703 = add nsw i32 %3325, %3327
  %3328 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2704 = getelementptr inbounds i32, i32* %3328, i32 56
  store i32 %add2703, i32* %arrayidx2704, align 4, !tbaa !7
  %3329 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2705 = getelementptr inbounds i32, i32* %3329, i32 57
  %3330 = load i32, i32* %arrayidx2705, align 4, !tbaa !7
  %sub2706 = sub nsw i32 0, %3330
  %3331 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2707 = getelementptr inbounds i32, i32* %3331, i32 56
  %3332 = load i32, i32* %arrayidx2707, align 4, !tbaa !7
  %add2708 = add nsw i32 %sub2706, %3332
  %3333 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2709 = getelementptr inbounds i32, i32* %3333, i32 57
  store i32 %add2708, i32* %arrayidx2709, align 4, !tbaa !7
  %3334 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2710 = getelementptr inbounds i32, i32* %3334, i32 58
  %3335 = load i32, i32* %arrayidx2710, align 4, !tbaa !7
  %sub2711 = sub nsw i32 0, %3335
  %3336 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2712 = getelementptr inbounds i32, i32* %3336, i32 59
  %3337 = load i32, i32* %arrayidx2712, align 4, !tbaa !7
  %add2713 = add nsw i32 %sub2711, %3337
  %3338 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2714 = getelementptr inbounds i32, i32* %3338, i32 58
  store i32 %add2713, i32* %arrayidx2714, align 4, !tbaa !7
  %3339 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2715 = getelementptr inbounds i32, i32* %3339, i32 59
  %3340 = load i32, i32* %arrayidx2715, align 4, !tbaa !7
  %3341 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2716 = getelementptr inbounds i32, i32* %3341, i32 58
  %3342 = load i32, i32* %arrayidx2716, align 4, !tbaa !7
  %add2717 = add nsw i32 %3340, %3342
  %3343 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2718 = getelementptr inbounds i32, i32* %3343, i32 59
  store i32 %add2717, i32* %arrayidx2718, align 4, !tbaa !7
  %3344 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2719 = getelementptr inbounds i32, i32* %3344, i32 60
  %3345 = load i32, i32* %arrayidx2719, align 4, !tbaa !7
  %3346 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2720 = getelementptr inbounds i32, i32* %3346, i32 61
  %3347 = load i32, i32* %arrayidx2720, align 4, !tbaa !7
  %add2721 = add nsw i32 %3345, %3347
  %3348 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2722 = getelementptr inbounds i32, i32* %3348, i32 60
  store i32 %add2721, i32* %arrayidx2722, align 4, !tbaa !7
  %3349 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2723 = getelementptr inbounds i32, i32* %3349, i32 61
  %3350 = load i32, i32* %arrayidx2723, align 4, !tbaa !7
  %sub2724 = sub nsw i32 0, %3350
  %3351 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2725 = getelementptr inbounds i32, i32* %3351, i32 60
  %3352 = load i32, i32* %arrayidx2725, align 4, !tbaa !7
  %add2726 = add nsw i32 %sub2724, %3352
  %3353 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2727 = getelementptr inbounds i32, i32* %3353, i32 61
  store i32 %add2726, i32* %arrayidx2727, align 4, !tbaa !7
  %3354 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2728 = getelementptr inbounds i32, i32* %3354, i32 62
  %3355 = load i32, i32* %arrayidx2728, align 4, !tbaa !7
  %sub2729 = sub nsw i32 0, %3355
  %3356 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2730 = getelementptr inbounds i32, i32* %3356, i32 63
  %3357 = load i32, i32* %arrayidx2730, align 4, !tbaa !7
  %add2731 = add nsw i32 %sub2729, %3357
  %3358 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2732 = getelementptr inbounds i32, i32* %3358, i32 62
  store i32 %add2731, i32* %arrayidx2732, align 4, !tbaa !7
  %3359 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2733 = getelementptr inbounds i32, i32* %3359, i32 63
  %3360 = load i32, i32* %arrayidx2733, align 4, !tbaa !7
  %3361 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2734 = getelementptr inbounds i32, i32* %3361, i32 62
  %3362 = load i32, i32* %arrayidx2734, align 4, !tbaa !7
  %add2735 = add nsw i32 %3360, %3362
  %3363 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2736 = getelementptr inbounds i32, i32* %3363, i32 63
  store i32 %add2735, i32* %arrayidx2736, align 4, !tbaa !7
  %3364 = load i32, i32* %stage, align 4, !tbaa !7
  %3365 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %3366 = load i32*, i32** %bf1, align 4, !tbaa !2
  %3367 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %3368 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx2737 = getelementptr inbounds i8, i8* %3367, i32 %3368
  %3369 = load i8, i8* %arrayidx2737, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %3364, i32* %3365, i32* %3366, i32 64, i8 signext %3369)
  %3370 = load i32, i32* %stage, align 4, !tbaa !7
  %inc2738 = add nsw i32 %3370, 1
  store i32 %inc2738, i32* %stage, align 4, !tbaa !7
  %3371 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2739 = sext i8 %3371 to i32
  %call2740 = call i32* @cospi_arr(i32 %conv2739)
  store i32* %call2740, i32** %cospi, align 4, !tbaa !2
  %3372 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %3372, i32** %bf0, align 4, !tbaa !2
  %arraydecay2741 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay2741, i32** %bf1, align 4, !tbaa !2
  %3373 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2742 = getelementptr inbounds i32, i32* %3373, i32 0
  %3374 = load i32, i32* %arrayidx2742, align 4, !tbaa !7
  %3375 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2743 = getelementptr inbounds i32, i32* %3375, i32 0
  store i32 %3374, i32* %arrayidx2743, align 4, !tbaa !7
  %3376 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2744 = getelementptr inbounds i32, i32* %3376, i32 1
  %3377 = load i32, i32* %arrayidx2744, align 4, !tbaa !7
  %3378 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2745 = getelementptr inbounds i32, i32* %3378, i32 1
  store i32 %3377, i32* %arrayidx2745, align 4, !tbaa !7
  %3379 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2746 = getelementptr inbounds i32, i32* %3379, i32 2
  %3380 = load i32, i32* %arrayidx2746, align 4, !tbaa !7
  %3381 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2747 = getelementptr inbounds i32, i32* %3381, i32 2
  store i32 %3380, i32* %arrayidx2747, align 4, !tbaa !7
  %3382 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2748 = getelementptr inbounds i32, i32* %3382, i32 3
  %3383 = load i32, i32* %arrayidx2748, align 4, !tbaa !7
  %3384 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2749 = getelementptr inbounds i32, i32* %3384, i32 3
  store i32 %3383, i32* %arrayidx2749, align 4, !tbaa !7
  %3385 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2750 = getelementptr inbounds i32, i32* %3385, i32 4
  %3386 = load i32, i32* %arrayidx2750, align 4, !tbaa !7
  %3387 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2751 = getelementptr inbounds i32, i32* %3387, i32 4
  store i32 %3386, i32* %arrayidx2751, align 4, !tbaa !7
  %3388 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2752 = getelementptr inbounds i32, i32* %3388, i32 5
  %3389 = load i32, i32* %arrayidx2752, align 4, !tbaa !7
  %3390 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2753 = getelementptr inbounds i32, i32* %3390, i32 5
  store i32 %3389, i32* %arrayidx2753, align 4, !tbaa !7
  %3391 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2754 = getelementptr inbounds i32, i32* %3391, i32 6
  %3392 = load i32, i32* %arrayidx2754, align 4, !tbaa !7
  %3393 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2755 = getelementptr inbounds i32, i32* %3393, i32 6
  store i32 %3392, i32* %arrayidx2755, align 4, !tbaa !7
  %3394 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2756 = getelementptr inbounds i32, i32* %3394, i32 7
  %3395 = load i32, i32* %arrayidx2756, align 4, !tbaa !7
  %3396 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2757 = getelementptr inbounds i32, i32* %3396, i32 7
  store i32 %3395, i32* %arrayidx2757, align 4, !tbaa !7
  %3397 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2758 = getelementptr inbounds i32, i32* %3397, i32 8
  %3398 = load i32, i32* %arrayidx2758, align 4, !tbaa !7
  %3399 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2759 = getelementptr inbounds i32, i32* %3399, i32 8
  store i32 %3398, i32* %arrayidx2759, align 4, !tbaa !7
  %3400 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2760 = getelementptr inbounds i32, i32* %3400, i32 9
  %3401 = load i32, i32* %arrayidx2760, align 4, !tbaa !7
  %3402 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2761 = getelementptr inbounds i32, i32* %3402, i32 9
  store i32 %3401, i32* %arrayidx2761, align 4, !tbaa !7
  %3403 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2762 = getelementptr inbounds i32, i32* %3403, i32 10
  %3404 = load i32, i32* %arrayidx2762, align 4, !tbaa !7
  %3405 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2763 = getelementptr inbounds i32, i32* %3405, i32 10
  store i32 %3404, i32* %arrayidx2763, align 4, !tbaa !7
  %3406 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2764 = getelementptr inbounds i32, i32* %3406, i32 11
  %3407 = load i32, i32* %arrayidx2764, align 4, !tbaa !7
  %3408 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2765 = getelementptr inbounds i32, i32* %3408, i32 11
  store i32 %3407, i32* %arrayidx2765, align 4, !tbaa !7
  %3409 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2766 = getelementptr inbounds i32, i32* %3409, i32 12
  %3410 = load i32, i32* %arrayidx2766, align 4, !tbaa !7
  %3411 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2767 = getelementptr inbounds i32, i32* %3411, i32 12
  store i32 %3410, i32* %arrayidx2767, align 4, !tbaa !7
  %3412 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2768 = getelementptr inbounds i32, i32* %3412, i32 13
  %3413 = load i32, i32* %arrayidx2768, align 4, !tbaa !7
  %3414 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2769 = getelementptr inbounds i32, i32* %3414, i32 13
  store i32 %3413, i32* %arrayidx2769, align 4, !tbaa !7
  %3415 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2770 = getelementptr inbounds i32, i32* %3415, i32 14
  %3416 = load i32, i32* %arrayidx2770, align 4, !tbaa !7
  %3417 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2771 = getelementptr inbounds i32, i32* %3417, i32 14
  store i32 %3416, i32* %arrayidx2771, align 4, !tbaa !7
  %3418 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2772 = getelementptr inbounds i32, i32* %3418, i32 15
  %3419 = load i32, i32* %arrayidx2772, align 4, !tbaa !7
  %3420 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2773 = getelementptr inbounds i32, i32* %3420, i32 15
  store i32 %3419, i32* %arrayidx2773, align 4, !tbaa !7
  %3421 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2774 = getelementptr inbounds i32, i32* %3421, i32 16
  %3422 = load i32, i32* %arrayidx2774, align 4, !tbaa !7
  %3423 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2775 = getelementptr inbounds i32, i32* %3423, i32 16
  store i32 %3422, i32* %arrayidx2775, align 4, !tbaa !7
  %3424 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2776 = getelementptr inbounds i32, i32* %3424, i32 17
  %3425 = load i32, i32* %arrayidx2776, align 4, !tbaa !7
  %3426 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2777 = getelementptr inbounds i32, i32* %3426, i32 17
  store i32 %3425, i32* %arrayidx2777, align 4, !tbaa !7
  %3427 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2778 = getelementptr inbounds i32, i32* %3427, i32 18
  %3428 = load i32, i32* %arrayidx2778, align 4, !tbaa !7
  %3429 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2779 = getelementptr inbounds i32, i32* %3429, i32 18
  store i32 %3428, i32* %arrayidx2779, align 4, !tbaa !7
  %3430 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2780 = getelementptr inbounds i32, i32* %3430, i32 19
  %3431 = load i32, i32* %arrayidx2780, align 4, !tbaa !7
  %3432 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2781 = getelementptr inbounds i32, i32* %3432, i32 19
  store i32 %3431, i32* %arrayidx2781, align 4, !tbaa !7
  %3433 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2782 = getelementptr inbounds i32, i32* %3433, i32 20
  %3434 = load i32, i32* %arrayidx2782, align 4, !tbaa !7
  %3435 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2783 = getelementptr inbounds i32, i32* %3435, i32 20
  store i32 %3434, i32* %arrayidx2783, align 4, !tbaa !7
  %3436 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2784 = getelementptr inbounds i32, i32* %3436, i32 21
  %3437 = load i32, i32* %arrayidx2784, align 4, !tbaa !7
  %3438 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2785 = getelementptr inbounds i32, i32* %3438, i32 21
  store i32 %3437, i32* %arrayidx2785, align 4, !tbaa !7
  %3439 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2786 = getelementptr inbounds i32, i32* %3439, i32 22
  %3440 = load i32, i32* %arrayidx2786, align 4, !tbaa !7
  %3441 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2787 = getelementptr inbounds i32, i32* %3441, i32 22
  store i32 %3440, i32* %arrayidx2787, align 4, !tbaa !7
  %3442 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2788 = getelementptr inbounds i32, i32* %3442, i32 23
  %3443 = load i32, i32* %arrayidx2788, align 4, !tbaa !7
  %3444 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2789 = getelementptr inbounds i32, i32* %3444, i32 23
  store i32 %3443, i32* %arrayidx2789, align 4, !tbaa !7
  %3445 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2790 = getelementptr inbounds i32, i32* %3445, i32 24
  %3446 = load i32, i32* %arrayidx2790, align 4, !tbaa !7
  %3447 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2791 = getelementptr inbounds i32, i32* %3447, i32 24
  store i32 %3446, i32* %arrayidx2791, align 4, !tbaa !7
  %3448 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2792 = getelementptr inbounds i32, i32* %3448, i32 25
  %3449 = load i32, i32* %arrayidx2792, align 4, !tbaa !7
  %3450 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2793 = getelementptr inbounds i32, i32* %3450, i32 25
  store i32 %3449, i32* %arrayidx2793, align 4, !tbaa !7
  %3451 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2794 = getelementptr inbounds i32, i32* %3451, i32 26
  %3452 = load i32, i32* %arrayidx2794, align 4, !tbaa !7
  %3453 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2795 = getelementptr inbounds i32, i32* %3453, i32 26
  store i32 %3452, i32* %arrayidx2795, align 4, !tbaa !7
  %3454 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2796 = getelementptr inbounds i32, i32* %3454, i32 27
  %3455 = load i32, i32* %arrayidx2796, align 4, !tbaa !7
  %3456 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2797 = getelementptr inbounds i32, i32* %3456, i32 27
  store i32 %3455, i32* %arrayidx2797, align 4, !tbaa !7
  %3457 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2798 = getelementptr inbounds i32, i32* %3457, i32 28
  %3458 = load i32, i32* %arrayidx2798, align 4, !tbaa !7
  %3459 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2799 = getelementptr inbounds i32, i32* %3459, i32 28
  store i32 %3458, i32* %arrayidx2799, align 4, !tbaa !7
  %3460 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2800 = getelementptr inbounds i32, i32* %3460, i32 29
  %3461 = load i32, i32* %arrayidx2800, align 4, !tbaa !7
  %3462 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2801 = getelementptr inbounds i32, i32* %3462, i32 29
  store i32 %3461, i32* %arrayidx2801, align 4, !tbaa !7
  %3463 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2802 = getelementptr inbounds i32, i32* %3463, i32 30
  %3464 = load i32, i32* %arrayidx2802, align 4, !tbaa !7
  %3465 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2803 = getelementptr inbounds i32, i32* %3465, i32 30
  store i32 %3464, i32* %arrayidx2803, align 4, !tbaa !7
  %3466 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2804 = getelementptr inbounds i32, i32* %3466, i32 31
  %3467 = load i32, i32* %arrayidx2804, align 4, !tbaa !7
  %3468 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2805 = getelementptr inbounds i32, i32* %3468, i32 31
  store i32 %3467, i32* %arrayidx2805, align 4, !tbaa !7
  %3469 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2806 = getelementptr inbounds i32, i32* %3469, i32 63
  %3470 = load i32, i32* %arrayidx2806, align 4, !tbaa !7
  %3471 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2807 = getelementptr inbounds i32, i32* %3471, i32 32
  %3472 = load i32, i32* %arrayidx2807, align 4, !tbaa !7
  %3473 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2808 = getelementptr inbounds i32, i32* %3473, i32 1
  %3474 = load i32, i32* %arrayidx2808, align 4, !tbaa !7
  %3475 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2809 = getelementptr inbounds i32, i32* %3475, i32 63
  %3476 = load i32, i32* %arrayidx2809, align 4, !tbaa !7
  %3477 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2810 = sext i8 %3477 to i32
  %call2811 = call i32 @half_btf(i32 %3470, i32 %3472, i32 %3474, i32 %3476, i32 %conv2810)
  %3478 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2812 = getelementptr inbounds i32, i32* %3478, i32 32
  store i32 %call2811, i32* %arrayidx2812, align 4, !tbaa !7
  %3479 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2813 = getelementptr inbounds i32, i32* %3479, i32 31
  %3480 = load i32, i32* %arrayidx2813, align 4, !tbaa !7
  %3481 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2814 = getelementptr inbounds i32, i32* %3481, i32 33
  %3482 = load i32, i32* %arrayidx2814, align 4, !tbaa !7
  %3483 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2815 = getelementptr inbounds i32, i32* %3483, i32 33
  %3484 = load i32, i32* %arrayidx2815, align 4, !tbaa !7
  %3485 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2816 = getelementptr inbounds i32, i32* %3485, i32 62
  %3486 = load i32, i32* %arrayidx2816, align 4, !tbaa !7
  %3487 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2817 = sext i8 %3487 to i32
  %call2818 = call i32 @half_btf(i32 %3480, i32 %3482, i32 %3484, i32 %3486, i32 %conv2817)
  %3488 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2819 = getelementptr inbounds i32, i32* %3488, i32 33
  store i32 %call2818, i32* %arrayidx2819, align 4, !tbaa !7
  %3489 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2820 = getelementptr inbounds i32, i32* %3489, i32 47
  %3490 = load i32, i32* %arrayidx2820, align 4, !tbaa !7
  %3491 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2821 = getelementptr inbounds i32, i32* %3491, i32 34
  %3492 = load i32, i32* %arrayidx2821, align 4, !tbaa !7
  %3493 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2822 = getelementptr inbounds i32, i32* %3493, i32 17
  %3494 = load i32, i32* %arrayidx2822, align 4, !tbaa !7
  %3495 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2823 = getelementptr inbounds i32, i32* %3495, i32 61
  %3496 = load i32, i32* %arrayidx2823, align 4, !tbaa !7
  %3497 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2824 = sext i8 %3497 to i32
  %call2825 = call i32 @half_btf(i32 %3490, i32 %3492, i32 %3494, i32 %3496, i32 %conv2824)
  %3498 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2826 = getelementptr inbounds i32, i32* %3498, i32 34
  store i32 %call2825, i32* %arrayidx2826, align 4, !tbaa !7
  %3499 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2827 = getelementptr inbounds i32, i32* %3499, i32 15
  %3500 = load i32, i32* %arrayidx2827, align 4, !tbaa !7
  %3501 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2828 = getelementptr inbounds i32, i32* %3501, i32 35
  %3502 = load i32, i32* %arrayidx2828, align 4, !tbaa !7
  %3503 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2829 = getelementptr inbounds i32, i32* %3503, i32 49
  %3504 = load i32, i32* %arrayidx2829, align 4, !tbaa !7
  %3505 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2830 = getelementptr inbounds i32, i32* %3505, i32 60
  %3506 = load i32, i32* %arrayidx2830, align 4, !tbaa !7
  %3507 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2831 = sext i8 %3507 to i32
  %call2832 = call i32 @half_btf(i32 %3500, i32 %3502, i32 %3504, i32 %3506, i32 %conv2831)
  %3508 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2833 = getelementptr inbounds i32, i32* %3508, i32 35
  store i32 %call2832, i32* %arrayidx2833, align 4, !tbaa !7
  %3509 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2834 = getelementptr inbounds i32, i32* %3509, i32 55
  %3510 = load i32, i32* %arrayidx2834, align 4, !tbaa !7
  %3511 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2835 = getelementptr inbounds i32, i32* %3511, i32 36
  %3512 = load i32, i32* %arrayidx2835, align 4, !tbaa !7
  %3513 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2836 = getelementptr inbounds i32, i32* %3513, i32 9
  %3514 = load i32, i32* %arrayidx2836, align 4, !tbaa !7
  %3515 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2837 = getelementptr inbounds i32, i32* %3515, i32 59
  %3516 = load i32, i32* %arrayidx2837, align 4, !tbaa !7
  %3517 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2838 = sext i8 %3517 to i32
  %call2839 = call i32 @half_btf(i32 %3510, i32 %3512, i32 %3514, i32 %3516, i32 %conv2838)
  %3518 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2840 = getelementptr inbounds i32, i32* %3518, i32 36
  store i32 %call2839, i32* %arrayidx2840, align 4, !tbaa !7
  %3519 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2841 = getelementptr inbounds i32, i32* %3519, i32 23
  %3520 = load i32, i32* %arrayidx2841, align 4, !tbaa !7
  %3521 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2842 = getelementptr inbounds i32, i32* %3521, i32 37
  %3522 = load i32, i32* %arrayidx2842, align 4, !tbaa !7
  %3523 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2843 = getelementptr inbounds i32, i32* %3523, i32 41
  %3524 = load i32, i32* %arrayidx2843, align 4, !tbaa !7
  %3525 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2844 = getelementptr inbounds i32, i32* %3525, i32 58
  %3526 = load i32, i32* %arrayidx2844, align 4, !tbaa !7
  %3527 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2845 = sext i8 %3527 to i32
  %call2846 = call i32 @half_btf(i32 %3520, i32 %3522, i32 %3524, i32 %3526, i32 %conv2845)
  %3528 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2847 = getelementptr inbounds i32, i32* %3528, i32 37
  store i32 %call2846, i32* %arrayidx2847, align 4, !tbaa !7
  %3529 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2848 = getelementptr inbounds i32, i32* %3529, i32 39
  %3530 = load i32, i32* %arrayidx2848, align 4, !tbaa !7
  %3531 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2849 = getelementptr inbounds i32, i32* %3531, i32 38
  %3532 = load i32, i32* %arrayidx2849, align 4, !tbaa !7
  %3533 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2850 = getelementptr inbounds i32, i32* %3533, i32 25
  %3534 = load i32, i32* %arrayidx2850, align 4, !tbaa !7
  %3535 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2851 = getelementptr inbounds i32, i32* %3535, i32 57
  %3536 = load i32, i32* %arrayidx2851, align 4, !tbaa !7
  %3537 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2852 = sext i8 %3537 to i32
  %call2853 = call i32 @half_btf(i32 %3530, i32 %3532, i32 %3534, i32 %3536, i32 %conv2852)
  %3538 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2854 = getelementptr inbounds i32, i32* %3538, i32 38
  store i32 %call2853, i32* %arrayidx2854, align 4, !tbaa !7
  %3539 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2855 = getelementptr inbounds i32, i32* %3539, i32 7
  %3540 = load i32, i32* %arrayidx2855, align 4, !tbaa !7
  %3541 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2856 = getelementptr inbounds i32, i32* %3541, i32 39
  %3542 = load i32, i32* %arrayidx2856, align 4, !tbaa !7
  %3543 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2857 = getelementptr inbounds i32, i32* %3543, i32 57
  %3544 = load i32, i32* %arrayidx2857, align 4, !tbaa !7
  %3545 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2858 = getelementptr inbounds i32, i32* %3545, i32 56
  %3546 = load i32, i32* %arrayidx2858, align 4, !tbaa !7
  %3547 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2859 = sext i8 %3547 to i32
  %call2860 = call i32 @half_btf(i32 %3540, i32 %3542, i32 %3544, i32 %3546, i32 %conv2859)
  %3548 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2861 = getelementptr inbounds i32, i32* %3548, i32 39
  store i32 %call2860, i32* %arrayidx2861, align 4, !tbaa !7
  %3549 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2862 = getelementptr inbounds i32, i32* %3549, i32 59
  %3550 = load i32, i32* %arrayidx2862, align 4, !tbaa !7
  %3551 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2863 = getelementptr inbounds i32, i32* %3551, i32 40
  %3552 = load i32, i32* %arrayidx2863, align 4, !tbaa !7
  %3553 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2864 = getelementptr inbounds i32, i32* %3553, i32 5
  %3554 = load i32, i32* %arrayidx2864, align 4, !tbaa !7
  %3555 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2865 = getelementptr inbounds i32, i32* %3555, i32 55
  %3556 = load i32, i32* %arrayidx2865, align 4, !tbaa !7
  %3557 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2866 = sext i8 %3557 to i32
  %call2867 = call i32 @half_btf(i32 %3550, i32 %3552, i32 %3554, i32 %3556, i32 %conv2866)
  %3558 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2868 = getelementptr inbounds i32, i32* %3558, i32 40
  store i32 %call2867, i32* %arrayidx2868, align 4, !tbaa !7
  %3559 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2869 = getelementptr inbounds i32, i32* %3559, i32 27
  %3560 = load i32, i32* %arrayidx2869, align 4, !tbaa !7
  %3561 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2870 = getelementptr inbounds i32, i32* %3561, i32 41
  %3562 = load i32, i32* %arrayidx2870, align 4, !tbaa !7
  %3563 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2871 = getelementptr inbounds i32, i32* %3563, i32 37
  %3564 = load i32, i32* %arrayidx2871, align 4, !tbaa !7
  %3565 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2872 = getelementptr inbounds i32, i32* %3565, i32 54
  %3566 = load i32, i32* %arrayidx2872, align 4, !tbaa !7
  %3567 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2873 = sext i8 %3567 to i32
  %call2874 = call i32 @half_btf(i32 %3560, i32 %3562, i32 %3564, i32 %3566, i32 %conv2873)
  %3568 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2875 = getelementptr inbounds i32, i32* %3568, i32 41
  store i32 %call2874, i32* %arrayidx2875, align 4, !tbaa !7
  %3569 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2876 = getelementptr inbounds i32, i32* %3569, i32 43
  %3570 = load i32, i32* %arrayidx2876, align 4, !tbaa !7
  %3571 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2877 = getelementptr inbounds i32, i32* %3571, i32 42
  %3572 = load i32, i32* %arrayidx2877, align 4, !tbaa !7
  %3573 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2878 = getelementptr inbounds i32, i32* %3573, i32 21
  %3574 = load i32, i32* %arrayidx2878, align 4, !tbaa !7
  %3575 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2879 = getelementptr inbounds i32, i32* %3575, i32 53
  %3576 = load i32, i32* %arrayidx2879, align 4, !tbaa !7
  %3577 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2880 = sext i8 %3577 to i32
  %call2881 = call i32 @half_btf(i32 %3570, i32 %3572, i32 %3574, i32 %3576, i32 %conv2880)
  %3578 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2882 = getelementptr inbounds i32, i32* %3578, i32 42
  store i32 %call2881, i32* %arrayidx2882, align 4, !tbaa !7
  %3579 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2883 = getelementptr inbounds i32, i32* %3579, i32 11
  %3580 = load i32, i32* %arrayidx2883, align 4, !tbaa !7
  %3581 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2884 = getelementptr inbounds i32, i32* %3581, i32 43
  %3582 = load i32, i32* %arrayidx2884, align 4, !tbaa !7
  %3583 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2885 = getelementptr inbounds i32, i32* %3583, i32 53
  %3584 = load i32, i32* %arrayidx2885, align 4, !tbaa !7
  %3585 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2886 = getelementptr inbounds i32, i32* %3585, i32 52
  %3586 = load i32, i32* %arrayidx2886, align 4, !tbaa !7
  %3587 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2887 = sext i8 %3587 to i32
  %call2888 = call i32 @half_btf(i32 %3580, i32 %3582, i32 %3584, i32 %3586, i32 %conv2887)
  %3588 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2889 = getelementptr inbounds i32, i32* %3588, i32 43
  store i32 %call2888, i32* %arrayidx2889, align 4, !tbaa !7
  %3589 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2890 = getelementptr inbounds i32, i32* %3589, i32 51
  %3590 = load i32, i32* %arrayidx2890, align 4, !tbaa !7
  %3591 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2891 = getelementptr inbounds i32, i32* %3591, i32 44
  %3592 = load i32, i32* %arrayidx2891, align 4, !tbaa !7
  %3593 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2892 = getelementptr inbounds i32, i32* %3593, i32 13
  %3594 = load i32, i32* %arrayidx2892, align 4, !tbaa !7
  %3595 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2893 = getelementptr inbounds i32, i32* %3595, i32 51
  %3596 = load i32, i32* %arrayidx2893, align 4, !tbaa !7
  %3597 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2894 = sext i8 %3597 to i32
  %call2895 = call i32 @half_btf(i32 %3590, i32 %3592, i32 %3594, i32 %3596, i32 %conv2894)
  %3598 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2896 = getelementptr inbounds i32, i32* %3598, i32 44
  store i32 %call2895, i32* %arrayidx2896, align 4, !tbaa !7
  %3599 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2897 = getelementptr inbounds i32, i32* %3599, i32 19
  %3600 = load i32, i32* %arrayidx2897, align 4, !tbaa !7
  %3601 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2898 = getelementptr inbounds i32, i32* %3601, i32 45
  %3602 = load i32, i32* %arrayidx2898, align 4, !tbaa !7
  %3603 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2899 = getelementptr inbounds i32, i32* %3603, i32 45
  %3604 = load i32, i32* %arrayidx2899, align 4, !tbaa !7
  %3605 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2900 = getelementptr inbounds i32, i32* %3605, i32 50
  %3606 = load i32, i32* %arrayidx2900, align 4, !tbaa !7
  %3607 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2901 = sext i8 %3607 to i32
  %call2902 = call i32 @half_btf(i32 %3600, i32 %3602, i32 %3604, i32 %3606, i32 %conv2901)
  %3608 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2903 = getelementptr inbounds i32, i32* %3608, i32 45
  store i32 %call2902, i32* %arrayidx2903, align 4, !tbaa !7
  %3609 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2904 = getelementptr inbounds i32, i32* %3609, i32 35
  %3610 = load i32, i32* %arrayidx2904, align 4, !tbaa !7
  %3611 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2905 = getelementptr inbounds i32, i32* %3611, i32 46
  %3612 = load i32, i32* %arrayidx2905, align 4, !tbaa !7
  %3613 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2906 = getelementptr inbounds i32, i32* %3613, i32 29
  %3614 = load i32, i32* %arrayidx2906, align 4, !tbaa !7
  %3615 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2907 = getelementptr inbounds i32, i32* %3615, i32 49
  %3616 = load i32, i32* %arrayidx2907, align 4, !tbaa !7
  %3617 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2908 = sext i8 %3617 to i32
  %call2909 = call i32 @half_btf(i32 %3610, i32 %3612, i32 %3614, i32 %3616, i32 %conv2908)
  %3618 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2910 = getelementptr inbounds i32, i32* %3618, i32 46
  store i32 %call2909, i32* %arrayidx2910, align 4, !tbaa !7
  %3619 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2911 = getelementptr inbounds i32, i32* %3619, i32 3
  %3620 = load i32, i32* %arrayidx2911, align 4, !tbaa !7
  %3621 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2912 = getelementptr inbounds i32, i32* %3621, i32 47
  %3622 = load i32, i32* %arrayidx2912, align 4, !tbaa !7
  %3623 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2913 = getelementptr inbounds i32, i32* %3623, i32 61
  %3624 = load i32, i32* %arrayidx2913, align 4, !tbaa !7
  %3625 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2914 = getelementptr inbounds i32, i32* %3625, i32 48
  %3626 = load i32, i32* %arrayidx2914, align 4, !tbaa !7
  %3627 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2915 = sext i8 %3627 to i32
  %call2916 = call i32 @half_btf(i32 %3620, i32 %3622, i32 %3624, i32 %3626, i32 %conv2915)
  %3628 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2917 = getelementptr inbounds i32, i32* %3628, i32 47
  store i32 %call2916, i32* %arrayidx2917, align 4, !tbaa !7
  %3629 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2918 = getelementptr inbounds i32, i32* %3629, i32 3
  %3630 = load i32, i32* %arrayidx2918, align 4, !tbaa !7
  %3631 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2919 = getelementptr inbounds i32, i32* %3631, i32 48
  %3632 = load i32, i32* %arrayidx2919, align 4, !tbaa !7
  %3633 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2920 = getelementptr inbounds i32, i32* %3633, i32 61
  %3634 = load i32, i32* %arrayidx2920, align 4, !tbaa !7
  %sub2921 = sub nsw i32 0, %3634
  %3635 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2922 = getelementptr inbounds i32, i32* %3635, i32 47
  %3636 = load i32, i32* %arrayidx2922, align 4, !tbaa !7
  %3637 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2923 = sext i8 %3637 to i32
  %call2924 = call i32 @half_btf(i32 %3630, i32 %3632, i32 %sub2921, i32 %3636, i32 %conv2923)
  %3638 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2925 = getelementptr inbounds i32, i32* %3638, i32 48
  store i32 %call2924, i32* %arrayidx2925, align 4, !tbaa !7
  %3639 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2926 = getelementptr inbounds i32, i32* %3639, i32 35
  %3640 = load i32, i32* %arrayidx2926, align 4, !tbaa !7
  %3641 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2927 = getelementptr inbounds i32, i32* %3641, i32 49
  %3642 = load i32, i32* %arrayidx2927, align 4, !tbaa !7
  %3643 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2928 = getelementptr inbounds i32, i32* %3643, i32 29
  %3644 = load i32, i32* %arrayidx2928, align 4, !tbaa !7
  %sub2929 = sub nsw i32 0, %3644
  %3645 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2930 = getelementptr inbounds i32, i32* %3645, i32 46
  %3646 = load i32, i32* %arrayidx2930, align 4, !tbaa !7
  %3647 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2931 = sext i8 %3647 to i32
  %call2932 = call i32 @half_btf(i32 %3640, i32 %3642, i32 %sub2929, i32 %3646, i32 %conv2931)
  %3648 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2933 = getelementptr inbounds i32, i32* %3648, i32 49
  store i32 %call2932, i32* %arrayidx2933, align 4, !tbaa !7
  %3649 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2934 = getelementptr inbounds i32, i32* %3649, i32 19
  %3650 = load i32, i32* %arrayidx2934, align 4, !tbaa !7
  %3651 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2935 = getelementptr inbounds i32, i32* %3651, i32 50
  %3652 = load i32, i32* %arrayidx2935, align 4, !tbaa !7
  %3653 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2936 = getelementptr inbounds i32, i32* %3653, i32 45
  %3654 = load i32, i32* %arrayidx2936, align 4, !tbaa !7
  %sub2937 = sub nsw i32 0, %3654
  %3655 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2938 = getelementptr inbounds i32, i32* %3655, i32 45
  %3656 = load i32, i32* %arrayidx2938, align 4, !tbaa !7
  %3657 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2939 = sext i8 %3657 to i32
  %call2940 = call i32 @half_btf(i32 %3650, i32 %3652, i32 %sub2937, i32 %3656, i32 %conv2939)
  %3658 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2941 = getelementptr inbounds i32, i32* %3658, i32 50
  store i32 %call2940, i32* %arrayidx2941, align 4, !tbaa !7
  %3659 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2942 = getelementptr inbounds i32, i32* %3659, i32 51
  %3660 = load i32, i32* %arrayidx2942, align 4, !tbaa !7
  %3661 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2943 = getelementptr inbounds i32, i32* %3661, i32 51
  %3662 = load i32, i32* %arrayidx2943, align 4, !tbaa !7
  %3663 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2944 = getelementptr inbounds i32, i32* %3663, i32 13
  %3664 = load i32, i32* %arrayidx2944, align 4, !tbaa !7
  %sub2945 = sub nsw i32 0, %3664
  %3665 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2946 = getelementptr inbounds i32, i32* %3665, i32 44
  %3666 = load i32, i32* %arrayidx2946, align 4, !tbaa !7
  %3667 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2947 = sext i8 %3667 to i32
  %call2948 = call i32 @half_btf(i32 %3660, i32 %3662, i32 %sub2945, i32 %3666, i32 %conv2947)
  %3668 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2949 = getelementptr inbounds i32, i32* %3668, i32 51
  store i32 %call2948, i32* %arrayidx2949, align 4, !tbaa !7
  %3669 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2950 = getelementptr inbounds i32, i32* %3669, i32 11
  %3670 = load i32, i32* %arrayidx2950, align 4, !tbaa !7
  %3671 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2951 = getelementptr inbounds i32, i32* %3671, i32 52
  %3672 = load i32, i32* %arrayidx2951, align 4, !tbaa !7
  %3673 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2952 = getelementptr inbounds i32, i32* %3673, i32 53
  %3674 = load i32, i32* %arrayidx2952, align 4, !tbaa !7
  %sub2953 = sub nsw i32 0, %3674
  %3675 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2954 = getelementptr inbounds i32, i32* %3675, i32 43
  %3676 = load i32, i32* %arrayidx2954, align 4, !tbaa !7
  %3677 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2955 = sext i8 %3677 to i32
  %call2956 = call i32 @half_btf(i32 %3670, i32 %3672, i32 %sub2953, i32 %3676, i32 %conv2955)
  %3678 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2957 = getelementptr inbounds i32, i32* %3678, i32 52
  store i32 %call2956, i32* %arrayidx2957, align 4, !tbaa !7
  %3679 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2958 = getelementptr inbounds i32, i32* %3679, i32 43
  %3680 = load i32, i32* %arrayidx2958, align 4, !tbaa !7
  %3681 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2959 = getelementptr inbounds i32, i32* %3681, i32 53
  %3682 = load i32, i32* %arrayidx2959, align 4, !tbaa !7
  %3683 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2960 = getelementptr inbounds i32, i32* %3683, i32 21
  %3684 = load i32, i32* %arrayidx2960, align 4, !tbaa !7
  %sub2961 = sub nsw i32 0, %3684
  %3685 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2962 = getelementptr inbounds i32, i32* %3685, i32 42
  %3686 = load i32, i32* %arrayidx2962, align 4, !tbaa !7
  %3687 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2963 = sext i8 %3687 to i32
  %call2964 = call i32 @half_btf(i32 %3680, i32 %3682, i32 %sub2961, i32 %3686, i32 %conv2963)
  %3688 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2965 = getelementptr inbounds i32, i32* %3688, i32 53
  store i32 %call2964, i32* %arrayidx2965, align 4, !tbaa !7
  %3689 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2966 = getelementptr inbounds i32, i32* %3689, i32 27
  %3690 = load i32, i32* %arrayidx2966, align 4, !tbaa !7
  %3691 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2967 = getelementptr inbounds i32, i32* %3691, i32 54
  %3692 = load i32, i32* %arrayidx2967, align 4, !tbaa !7
  %3693 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2968 = getelementptr inbounds i32, i32* %3693, i32 37
  %3694 = load i32, i32* %arrayidx2968, align 4, !tbaa !7
  %sub2969 = sub nsw i32 0, %3694
  %3695 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2970 = getelementptr inbounds i32, i32* %3695, i32 41
  %3696 = load i32, i32* %arrayidx2970, align 4, !tbaa !7
  %3697 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2971 = sext i8 %3697 to i32
  %call2972 = call i32 @half_btf(i32 %3690, i32 %3692, i32 %sub2969, i32 %3696, i32 %conv2971)
  %3698 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2973 = getelementptr inbounds i32, i32* %3698, i32 54
  store i32 %call2972, i32* %arrayidx2973, align 4, !tbaa !7
  %3699 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2974 = getelementptr inbounds i32, i32* %3699, i32 59
  %3700 = load i32, i32* %arrayidx2974, align 4, !tbaa !7
  %3701 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2975 = getelementptr inbounds i32, i32* %3701, i32 55
  %3702 = load i32, i32* %arrayidx2975, align 4, !tbaa !7
  %3703 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2976 = getelementptr inbounds i32, i32* %3703, i32 5
  %3704 = load i32, i32* %arrayidx2976, align 4, !tbaa !7
  %sub2977 = sub nsw i32 0, %3704
  %3705 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2978 = getelementptr inbounds i32, i32* %3705, i32 40
  %3706 = load i32, i32* %arrayidx2978, align 4, !tbaa !7
  %3707 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2979 = sext i8 %3707 to i32
  %call2980 = call i32 @half_btf(i32 %3700, i32 %3702, i32 %sub2977, i32 %3706, i32 %conv2979)
  %3708 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2981 = getelementptr inbounds i32, i32* %3708, i32 55
  store i32 %call2980, i32* %arrayidx2981, align 4, !tbaa !7
  %3709 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2982 = getelementptr inbounds i32, i32* %3709, i32 7
  %3710 = load i32, i32* %arrayidx2982, align 4, !tbaa !7
  %3711 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2983 = getelementptr inbounds i32, i32* %3711, i32 56
  %3712 = load i32, i32* %arrayidx2983, align 4, !tbaa !7
  %3713 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2984 = getelementptr inbounds i32, i32* %3713, i32 57
  %3714 = load i32, i32* %arrayidx2984, align 4, !tbaa !7
  %sub2985 = sub nsw i32 0, %3714
  %3715 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2986 = getelementptr inbounds i32, i32* %3715, i32 39
  %3716 = load i32, i32* %arrayidx2986, align 4, !tbaa !7
  %3717 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2987 = sext i8 %3717 to i32
  %call2988 = call i32 @half_btf(i32 %3710, i32 %3712, i32 %sub2985, i32 %3716, i32 %conv2987)
  %3718 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2989 = getelementptr inbounds i32, i32* %3718, i32 56
  store i32 %call2988, i32* %arrayidx2989, align 4, !tbaa !7
  %3719 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2990 = getelementptr inbounds i32, i32* %3719, i32 39
  %3720 = load i32, i32* %arrayidx2990, align 4, !tbaa !7
  %3721 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2991 = getelementptr inbounds i32, i32* %3721, i32 57
  %3722 = load i32, i32* %arrayidx2991, align 4, !tbaa !7
  %3723 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2992 = getelementptr inbounds i32, i32* %3723, i32 25
  %3724 = load i32, i32* %arrayidx2992, align 4, !tbaa !7
  %sub2993 = sub nsw i32 0, %3724
  %3725 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2994 = getelementptr inbounds i32, i32* %3725, i32 38
  %3726 = load i32, i32* %arrayidx2994, align 4, !tbaa !7
  %3727 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv2995 = sext i8 %3727 to i32
  %call2996 = call i32 @half_btf(i32 %3720, i32 %3722, i32 %sub2993, i32 %3726, i32 %conv2995)
  %3728 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx2997 = getelementptr inbounds i32, i32* %3728, i32 57
  store i32 %call2996, i32* %arrayidx2997, align 4, !tbaa !7
  %3729 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx2998 = getelementptr inbounds i32, i32* %3729, i32 23
  %3730 = load i32, i32* %arrayidx2998, align 4, !tbaa !7
  %3731 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx2999 = getelementptr inbounds i32, i32* %3731, i32 58
  %3732 = load i32, i32* %arrayidx2999, align 4, !tbaa !7
  %3733 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3000 = getelementptr inbounds i32, i32* %3733, i32 41
  %3734 = load i32, i32* %arrayidx3000, align 4, !tbaa !7
  %sub3001 = sub nsw i32 0, %3734
  %3735 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3002 = getelementptr inbounds i32, i32* %3735, i32 37
  %3736 = load i32, i32* %arrayidx3002, align 4, !tbaa !7
  %3737 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv3003 = sext i8 %3737 to i32
  %call3004 = call i32 @half_btf(i32 %3730, i32 %3732, i32 %sub3001, i32 %3736, i32 %conv3003)
  %3738 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3005 = getelementptr inbounds i32, i32* %3738, i32 58
  store i32 %call3004, i32* %arrayidx3005, align 4, !tbaa !7
  %3739 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3006 = getelementptr inbounds i32, i32* %3739, i32 55
  %3740 = load i32, i32* %arrayidx3006, align 4, !tbaa !7
  %3741 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3007 = getelementptr inbounds i32, i32* %3741, i32 59
  %3742 = load i32, i32* %arrayidx3007, align 4, !tbaa !7
  %3743 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3008 = getelementptr inbounds i32, i32* %3743, i32 9
  %3744 = load i32, i32* %arrayidx3008, align 4, !tbaa !7
  %sub3009 = sub nsw i32 0, %3744
  %3745 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3010 = getelementptr inbounds i32, i32* %3745, i32 36
  %3746 = load i32, i32* %arrayidx3010, align 4, !tbaa !7
  %3747 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv3011 = sext i8 %3747 to i32
  %call3012 = call i32 @half_btf(i32 %3740, i32 %3742, i32 %sub3009, i32 %3746, i32 %conv3011)
  %3748 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3013 = getelementptr inbounds i32, i32* %3748, i32 59
  store i32 %call3012, i32* %arrayidx3013, align 4, !tbaa !7
  %3749 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3014 = getelementptr inbounds i32, i32* %3749, i32 15
  %3750 = load i32, i32* %arrayidx3014, align 4, !tbaa !7
  %3751 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3015 = getelementptr inbounds i32, i32* %3751, i32 60
  %3752 = load i32, i32* %arrayidx3015, align 4, !tbaa !7
  %3753 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3016 = getelementptr inbounds i32, i32* %3753, i32 49
  %3754 = load i32, i32* %arrayidx3016, align 4, !tbaa !7
  %sub3017 = sub nsw i32 0, %3754
  %3755 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3018 = getelementptr inbounds i32, i32* %3755, i32 35
  %3756 = load i32, i32* %arrayidx3018, align 4, !tbaa !7
  %3757 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv3019 = sext i8 %3757 to i32
  %call3020 = call i32 @half_btf(i32 %3750, i32 %3752, i32 %sub3017, i32 %3756, i32 %conv3019)
  %3758 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3021 = getelementptr inbounds i32, i32* %3758, i32 60
  store i32 %call3020, i32* %arrayidx3021, align 4, !tbaa !7
  %3759 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3022 = getelementptr inbounds i32, i32* %3759, i32 47
  %3760 = load i32, i32* %arrayidx3022, align 4, !tbaa !7
  %3761 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3023 = getelementptr inbounds i32, i32* %3761, i32 61
  %3762 = load i32, i32* %arrayidx3023, align 4, !tbaa !7
  %3763 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3024 = getelementptr inbounds i32, i32* %3763, i32 17
  %3764 = load i32, i32* %arrayidx3024, align 4, !tbaa !7
  %sub3025 = sub nsw i32 0, %3764
  %3765 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3026 = getelementptr inbounds i32, i32* %3765, i32 34
  %3766 = load i32, i32* %arrayidx3026, align 4, !tbaa !7
  %3767 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv3027 = sext i8 %3767 to i32
  %call3028 = call i32 @half_btf(i32 %3760, i32 %3762, i32 %sub3025, i32 %3766, i32 %conv3027)
  %3768 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3029 = getelementptr inbounds i32, i32* %3768, i32 61
  store i32 %call3028, i32* %arrayidx3029, align 4, !tbaa !7
  %3769 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3030 = getelementptr inbounds i32, i32* %3769, i32 31
  %3770 = load i32, i32* %arrayidx3030, align 4, !tbaa !7
  %3771 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3031 = getelementptr inbounds i32, i32* %3771, i32 62
  %3772 = load i32, i32* %arrayidx3031, align 4, !tbaa !7
  %3773 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3032 = getelementptr inbounds i32, i32* %3773, i32 33
  %3774 = load i32, i32* %arrayidx3032, align 4, !tbaa !7
  %sub3033 = sub nsw i32 0, %3774
  %3775 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3034 = getelementptr inbounds i32, i32* %3775, i32 33
  %3776 = load i32, i32* %arrayidx3034, align 4, !tbaa !7
  %3777 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv3035 = sext i8 %3777 to i32
  %call3036 = call i32 @half_btf(i32 %3770, i32 %3772, i32 %sub3033, i32 %3776, i32 %conv3035)
  %3778 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3037 = getelementptr inbounds i32, i32* %3778, i32 62
  store i32 %call3036, i32* %arrayidx3037, align 4, !tbaa !7
  %3779 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3038 = getelementptr inbounds i32, i32* %3779, i32 63
  %3780 = load i32, i32* %arrayidx3038, align 4, !tbaa !7
  %3781 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3039 = getelementptr inbounds i32, i32* %3781, i32 63
  %3782 = load i32, i32* %arrayidx3039, align 4, !tbaa !7
  %3783 = load i32*, i32** %cospi, align 4, !tbaa !2
  %arrayidx3040 = getelementptr inbounds i32, i32* %3783, i32 1
  %3784 = load i32, i32* %arrayidx3040, align 4, !tbaa !7
  %sub3041 = sub nsw i32 0, %3784
  %3785 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3042 = getelementptr inbounds i32, i32* %3785, i32 32
  %3786 = load i32, i32* %arrayidx3042, align 4, !tbaa !7
  %3787 = load i8, i8* %cos_bit.addr, align 1, !tbaa !6
  %conv3043 = sext i8 %3787 to i32
  %call3044 = call i32 @half_btf(i32 %3780, i32 %3782, i32 %sub3041, i32 %3786, i32 %conv3043)
  %3788 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3045 = getelementptr inbounds i32, i32* %3788, i32 63
  store i32 %call3044, i32* %arrayidx3045, align 4, !tbaa !7
  %3789 = load i32, i32* %stage, align 4, !tbaa !7
  %3790 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %3791 = load i32*, i32** %bf1, align 4, !tbaa !2
  %3792 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %3793 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx3046 = getelementptr inbounds i8, i8* %3792, i32 %3793
  %3794 = load i8, i8* %arrayidx3046, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %3789, i32* %3790, i32* %3791, i32 64, i8 signext %3794)
  %3795 = load i32, i32* %stage, align 4, !tbaa !7
  %inc3047 = add nsw i32 %3795, 1
  store i32 %inc3047, i32* %stage, align 4, !tbaa !7
  %arraydecay3048 = getelementptr inbounds [64 x i32], [64 x i32]* %step, i32 0, i32 0
  store i32* %arraydecay3048, i32** %bf0, align 4, !tbaa !2
  %3796 = load i32*, i32** %output.addr, align 4, !tbaa !2
  store i32* %3796, i32** %bf1, align 4, !tbaa !2
  %3797 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3049 = getelementptr inbounds i32, i32* %3797, i32 0
  %3798 = load i32, i32* %arrayidx3049, align 4, !tbaa !7
  %3799 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3050 = getelementptr inbounds i32, i32* %3799, i32 0
  store i32 %3798, i32* %arrayidx3050, align 4, !tbaa !7
  %3800 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3051 = getelementptr inbounds i32, i32* %3800, i32 32
  %3801 = load i32, i32* %arrayidx3051, align 4, !tbaa !7
  %3802 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3052 = getelementptr inbounds i32, i32* %3802, i32 1
  store i32 %3801, i32* %arrayidx3052, align 4, !tbaa !7
  %3803 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3053 = getelementptr inbounds i32, i32* %3803, i32 16
  %3804 = load i32, i32* %arrayidx3053, align 4, !tbaa !7
  %3805 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3054 = getelementptr inbounds i32, i32* %3805, i32 2
  store i32 %3804, i32* %arrayidx3054, align 4, !tbaa !7
  %3806 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3055 = getelementptr inbounds i32, i32* %3806, i32 48
  %3807 = load i32, i32* %arrayidx3055, align 4, !tbaa !7
  %3808 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3056 = getelementptr inbounds i32, i32* %3808, i32 3
  store i32 %3807, i32* %arrayidx3056, align 4, !tbaa !7
  %3809 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3057 = getelementptr inbounds i32, i32* %3809, i32 8
  %3810 = load i32, i32* %arrayidx3057, align 4, !tbaa !7
  %3811 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3058 = getelementptr inbounds i32, i32* %3811, i32 4
  store i32 %3810, i32* %arrayidx3058, align 4, !tbaa !7
  %3812 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3059 = getelementptr inbounds i32, i32* %3812, i32 40
  %3813 = load i32, i32* %arrayidx3059, align 4, !tbaa !7
  %3814 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3060 = getelementptr inbounds i32, i32* %3814, i32 5
  store i32 %3813, i32* %arrayidx3060, align 4, !tbaa !7
  %3815 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3061 = getelementptr inbounds i32, i32* %3815, i32 24
  %3816 = load i32, i32* %arrayidx3061, align 4, !tbaa !7
  %3817 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3062 = getelementptr inbounds i32, i32* %3817, i32 6
  store i32 %3816, i32* %arrayidx3062, align 4, !tbaa !7
  %3818 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3063 = getelementptr inbounds i32, i32* %3818, i32 56
  %3819 = load i32, i32* %arrayidx3063, align 4, !tbaa !7
  %3820 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3064 = getelementptr inbounds i32, i32* %3820, i32 7
  store i32 %3819, i32* %arrayidx3064, align 4, !tbaa !7
  %3821 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3065 = getelementptr inbounds i32, i32* %3821, i32 4
  %3822 = load i32, i32* %arrayidx3065, align 4, !tbaa !7
  %3823 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3066 = getelementptr inbounds i32, i32* %3823, i32 8
  store i32 %3822, i32* %arrayidx3066, align 4, !tbaa !7
  %3824 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3067 = getelementptr inbounds i32, i32* %3824, i32 36
  %3825 = load i32, i32* %arrayidx3067, align 4, !tbaa !7
  %3826 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3068 = getelementptr inbounds i32, i32* %3826, i32 9
  store i32 %3825, i32* %arrayidx3068, align 4, !tbaa !7
  %3827 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3069 = getelementptr inbounds i32, i32* %3827, i32 20
  %3828 = load i32, i32* %arrayidx3069, align 4, !tbaa !7
  %3829 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3070 = getelementptr inbounds i32, i32* %3829, i32 10
  store i32 %3828, i32* %arrayidx3070, align 4, !tbaa !7
  %3830 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3071 = getelementptr inbounds i32, i32* %3830, i32 52
  %3831 = load i32, i32* %arrayidx3071, align 4, !tbaa !7
  %3832 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3072 = getelementptr inbounds i32, i32* %3832, i32 11
  store i32 %3831, i32* %arrayidx3072, align 4, !tbaa !7
  %3833 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3073 = getelementptr inbounds i32, i32* %3833, i32 12
  %3834 = load i32, i32* %arrayidx3073, align 4, !tbaa !7
  %3835 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3074 = getelementptr inbounds i32, i32* %3835, i32 12
  store i32 %3834, i32* %arrayidx3074, align 4, !tbaa !7
  %3836 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3075 = getelementptr inbounds i32, i32* %3836, i32 44
  %3837 = load i32, i32* %arrayidx3075, align 4, !tbaa !7
  %3838 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3076 = getelementptr inbounds i32, i32* %3838, i32 13
  store i32 %3837, i32* %arrayidx3076, align 4, !tbaa !7
  %3839 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3077 = getelementptr inbounds i32, i32* %3839, i32 28
  %3840 = load i32, i32* %arrayidx3077, align 4, !tbaa !7
  %3841 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3078 = getelementptr inbounds i32, i32* %3841, i32 14
  store i32 %3840, i32* %arrayidx3078, align 4, !tbaa !7
  %3842 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3079 = getelementptr inbounds i32, i32* %3842, i32 60
  %3843 = load i32, i32* %arrayidx3079, align 4, !tbaa !7
  %3844 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3080 = getelementptr inbounds i32, i32* %3844, i32 15
  store i32 %3843, i32* %arrayidx3080, align 4, !tbaa !7
  %3845 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3081 = getelementptr inbounds i32, i32* %3845, i32 2
  %3846 = load i32, i32* %arrayidx3081, align 4, !tbaa !7
  %3847 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3082 = getelementptr inbounds i32, i32* %3847, i32 16
  store i32 %3846, i32* %arrayidx3082, align 4, !tbaa !7
  %3848 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3083 = getelementptr inbounds i32, i32* %3848, i32 34
  %3849 = load i32, i32* %arrayidx3083, align 4, !tbaa !7
  %3850 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3084 = getelementptr inbounds i32, i32* %3850, i32 17
  store i32 %3849, i32* %arrayidx3084, align 4, !tbaa !7
  %3851 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3085 = getelementptr inbounds i32, i32* %3851, i32 18
  %3852 = load i32, i32* %arrayidx3085, align 4, !tbaa !7
  %3853 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3086 = getelementptr inbounds i32, i32* %3853, i32 18
  store i32 %3852, i32* %arrayidx3086, align 4, !tbaa !7
  %3854 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3087 = getelementptr inbounds i32, i32* %3854, i32 50
  %3855 = load i32, i32* %arrayidx3087, align 4, !tbaa !7
  %3856 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3088 = getelementptr inbounds i32, i32* %3856, i32 19
  store i32 %3855, i32* %arrayidx3088, align 4, !tbaa !7
  %3857 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3089 = getelementptr inbounds i32, i32* %3857, i32 10
  %3858 = load i32, i32* %arrayidx3089, align 4, !tbaa !7
  %3859 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3090 = getelementptr inbounds i32, i32* %3859, i32 20
  store i32 %3858, i32* %arrayidx3090, align 4, !tbaa !7
  %3860 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3091 = getelementptr inbounds i32, i32* %3860, i32 42
  %3861 = load i32, i32* %arrayidx3091, align 4, !tbaa !7
  %3862 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3092 = getelementptr inbounds i32, i32* %3862, i32 21
  store i32 %3861, i32* %arrayidx3092, align 4, !tbaa !7
  %3863 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3093 = getelementptr inbounds i32, i32* %3863, i32 26
  %3864 = load i32, i32* %arrayidx3093, align 4, !tbaa !7
  %3865 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3094 = getelementptr inbounds i32, i32* %3865, i32 22
  store i32 %3864, i32* %arrayidx3094, align 4, !tbaa !7
  %3866 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3095 = getelementptr inbounds i32, i32* %3866, i32 58
  %3867 = load i32, i32* %arrayidx3095, align 4, !tbaa !7
  %3868 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3096 = getelementptr inbounds i32, i32* %3868, i32 23
  store i32 %3867, i32* %arrayidx3096, align 4, !tbaa !7
  %3869 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3097 = getelementptr inbounds i32, i32* %3869, i32 6
  %3870 = load i32, i32* %arrayidx3097, align 4, !tbaa !7
  %3871 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3098 = getelementptr inbounds i32, i32* %3871, i32 24
  store i32 %3870, i32* %arrayidx3098, align 4, !tbaa !7
  %3872 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3099 = getelementptr inbounds i32, i32* %3872, i32 38
  %3873 = load i32, i32* %arrayidx3099, align 4, !tbaa !7
  %3874 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3100 = getelementptr inbounds i32, i32* %3874, i32 25
  store i32 %3873, i32* %arrayidx3100, align 4, !tbaa !7
  %3875 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3101 = getelementptr inbounds i32, i32* %3875, i32 22
  %3876 = load i32, i32* %arrayidx3101, align 4, !tbaa !7
  %3877 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3102 = getelementptr inbounds i32, i32* %3877, i32 26
  store i32 %3876, i32* %arrayidx3102, align 4, !tbaa !7
  %3878 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3103 = getelementptr inbounds i32, i32* %3878, i32 54
  %3879 = load i32, i32* %arrayidx3103, align 4, !tbaa !7
  %3880 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3104 = getelementptr inbounds i32, i32* %3880, i32 27
  store i32 %3879, i32* %arrayidx3104, align 4, !tbaa !7
  %3881 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3105 = getelementptr inbounds i32, i32* %3881, i32 14
  %3882 = load i32, i32* %arrayidx3105, align 4, !tbaa !7
  %3883 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3106 = getelementptr inbounds i32, i32* %3883, i32 28
  store i32 %3882, i32* %arrayidx3106, align 4, !tbaa !7
  %3884 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3107 = getelementptr inbounds i32, i32* %3884, i32 46
  %3885 = load i32, i32* %arrayidx3107, align 4, !tbaa !7
  %3886 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3108 = getelementptr inbounds i32, i32* %3886, i32 29
  store i32 %3885, i32* %arrayidx3108, align 4, !tbaa !7
  %3887 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3109 = getelementptr inbounds i32, i32* %3887, i32 30
  %3888 = load i32, i32* %arrayidx3109, align 4, !tbaa !7
  %3889 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3110 = getelementptr inbounds i32, i32* %3889, i32 30
  store i32 %3888, i32* %arrayidx3110, align 4, !tbaa !7
  %3890 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3111 = getelementptr inbounds i32, i32* %3890, i32 62
  %3891 = load i32, i32* %arrayidx3111, align 4, !tbaa !7
  %3892 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3112 = getelementptr inbounds i32, i32* %3892, i32 31
  store i32 %3891, i32* %arrayidx3112, align 4, !tbaa !7
  %3893 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3113 = getelementptr inbounds i32, i32* %3893, i32 1
  %3894 = load i32, i32* %arrayidx3113, align 4, !tbaa !7
  %3895 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3114 = getelementptr inbounds i32, i32* %3895, i32 32
  store i32 %3894, i32* %arrayidx3114, align 4, !tbaa !7
  %3896 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3115 = getelementptr inbounds i32, i32* %3896, i32 33
  %3897 = load i32, i32* %arrayidx3115, align 4, !tbaa !7
  %3898 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3116 = getelementptr inbounds i32, i32* %3898, i32 33
  store i32 %3897, i32* %arrayidx3116, align 4, !tbaa !7
  %3899 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3117 = getelementptr inbounds i32, i32* %3899, i32 17
  %3900 = load i32, i32* %arrayidx3117, align 4, !tbaa !7
  %3901 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3118 = getelementptr inbounds i32, i32* %3901, i32 34
  store i32 %3900, i32* %arrayidx3118, align 4, !tbaa !7
  %3902 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3119 = getelementptr inbounds i32, i32* %3902, i32 49
  %3903 = load i32, i32* %arrayidx3119, align 4, !tbaa !7
  %3904 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3120 = getelementptr inbounds i32, i32* %3904, i32 35
  store i32 %3903, i32* %arrayidx3120, align 4, !tbaa !7
  %3905 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3121 = getelementptr inbounds i32, i32* %3905, i32 9
  %3906 = load i32, i32* %arrayidx3121, align 4, !tbaa !7
  %3907 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3122 = getelementptr inbounds i32, i32* %3907, i32 36
  store i32 %3906, i32* %arrayidx3122, align 4, !tbaa !7
  %3908 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3123 = getelementptr inbounds i32, i32* %3908, i32 41
  %3909 = load i32, i32* %arrayidx3123, align 4, !tbaa !7
  %3910 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3124 = getelementptr inbounds i32, i32* %3910, i32 37
  store i32 %3909, i32* %arrayidx3124, align 4, !tbaa !7
  %3911 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3125 = getelementptr inbounds i32, i32* %3911, i32 25
  %3912 = load i32, i32* %arrayidx3125, align 4, !tbaa !7
  %3913 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3126 = getelementptr inbounds i32, i32* %3913, i32 38
  store i32 %3912, i32* %arrayidx3126, align 4, !tbaa !7
  %3914 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3127 = getelementptr inbounds i32, i32* %3914, i32 57
  %3915 = load i32, i32* %arrayidx3127, align 4, !tbaa !7
  %3916 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3128 = getelementptr inbounds i32, i32* %3916, i32 39
  store i32 %3915, i32* %arrayidx3128, align 4, !tbaa !7
  %3917 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3129 = getelementptr inbounds i32, i32* %3917, i32 5
  %3918 = load i32, i32* %arrayidx3129, align 4, !tbaa !7
  %3919 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3130 = getelementptr inbounds i32, i32* %3919, i32 40
  store i32 %3918, i32* %arrayidx3130, align 4, !tbaa !7
  %3920 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3131 = getelementptr inbounds i32, i32* %3920, i32 37
  %3921 = load i32, i32* %arrayidx3131, align 4, !tbaa !7
  %3922 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3132 = getelementptr inbounds i32, i32* %3922, i32 41
  store i32 %3921, i32* %arrayidx3132, align 4, !tbaa !7
  %3923 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3133 = getelementptr inbounds i32, i32* %3923, i32 21
  %3924 = load i32, i32* %arrayidx3133, align 4, !tbaa !7
  %3925 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3134 = getelementptr inbounds i32, i32* %3925, i32 42
  store i32 %3924, i32* %arrayidx3134, align 4, !tbaa !7
  %3926 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3135 = getelementptr inbounds i32, i32* %3926, i32 53
  %3927 = load i32, i32* %arrayidx3135, align 4, !tbaa !7
  %3928 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3136 = getelementptr inbounds i32, i32* %3928, i32 43
  store i32 %3927, i32* %arrayidx3136, align 4, !tbaa !7
  %3929 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3137 = getelementptr inbounds i32, i32* %3929, i32 13
  %3930 = load i32, i32* %arrayidx3137, align 4, !tbaa !7
  %3931 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3138 = getelementptr inbounds i32, i32* %3931, i32 44
  store i32 %3930, i32* %arrayidx3138, align 4, !tbaa !7
  %3932 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3139 = getelementptr inbounds i32, i32* %3932, i32 45
  %3933 = load i32, i32* %arrayidx3139, align 4, !tbaa !7
  %3934 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3140 = getelementptr inbounds i32, i32* %3934, i32 45
  store i32 %3933, i32* %arrayidx3140, align 4, !tbaa !7
  %3935 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3141 = getelementptr inbounds i32, i32* %3935, i32 29
  %3936 = load i32, i32* %arrayidx3141, align 4, !tbaa !7
  %3937 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3142 = getelementptr inbounds i32, i32* %3937, i32 46
  store i32 %3936, i32* %arrayidx3142, align 4, !tbaa !7
  %3938 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3143 = getelementptr inbounds i32, i32* %3938, i32 61
  %3939 = load i32, i32* %arrayidx3143, align 4, !tbaa !7
  %3940 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3144 = getelementptr inbounds i32, i32* %3940, i32 47
  store i32 %3939, i32* %arrayidx3144, align 4, !tbaa !7
  %3941 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3145 = getelementptr inbounds i32, i32* %3941, i32 3
  %3942 = load i32, i32* %arrayidx3145, align 4, !tbaa !7
  %3943 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3146 = getelementptr inbounds i32, i32* %3943, i32 48
  store i32 %3942, i32* %arrayidx3146, align 4, !tbaa !7
  %3944 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3147 = getelementptr inbounds i32, i32* %3944, i32 35
  %3945 = load i32, i32* %arrayidx3147, align 4, !tbaa !7
  %3946 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3148 = getelementptr inbounds i32, i32* %3946, i32 49
  store i32 %3945, i32* %arrayidx3148, align 4, !tbaa !7
  %3947 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3149 = getelementptr inbounds i32, i32* %3947, i32 19
  %3948 = load i32, i32* %arrayidx3149, align 4, !tbaa !7
  %3949 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3150 = getelementptr inbounds i32, i32* %3949, i32 50
  store i32 %3948, i32* %arrayidx3150, align 4, !tbaa !7
  %3950 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3151 = getelementptr inbounds i32, i32* %3950, i32 51
  %3951 = load i32, i32* %arrayidx3151, align 4, !tbaa !7
  %3952 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3152 = getelementptr inbounds i32, i32* %3952, i32 51
  store i32 %3951, i32* %arrayidx3152, align 4, !tbaa !7
  %3953 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3153 = getelementptr inbounds i32, i32* %3953, i32 11
  %3954 = load i32, i32* %arrayidx3153, align 4, !tbaa !7
  %3955 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3154 = getelementptr inbounds i32, i32* %3955, i32 52
  store i32 %3954, i32* %arrayidx3154, align 4, !tbaa !7
  %3956 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3155 = getelementptr inbounds i32, i32* %3956, i32 43
  %3957 = load i32, i32* %arrayidx3155, align 4, !tbaa !7
  %3958 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3156 = getelementptr inbounds i32, i32* %3958, i32 53
  store i32 %3957, i32* %arrayidx3156, align 4, !tbaa !7
  %3959 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3157 = getelementptr inbounds i32, i32* %3959, i32 27
  %3960 = load i32, i32* %arrayidx3157, align 4, !tbaa !7
  %3961 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3158 = getelementptr inbounds i32, i32* %3961, i32 54
  store i32 %3960, i32* %arrayidx3158, align 4, !tbaa !7
  %3962 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3159 = getelementptr inbounds i32, i32* %3962, i32 59
  %3963 = load i32, i32* %arrayidx3159, align 4, !tbaa !7
  %3964 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3160 = getelementptr inbounds i32, i32* %3964, i32 55
  store i32 %3963, i32* %arrayidx3160, align 4, !tbaa !7
  %3965 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3161 = getelementptr inbounds i32, i32* %3965, i32 7
  %3966 = load i32, i32* %arrayidx3161, align 4, !tbaa !7
  %3967 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3162 = getelementptr inbounds i32, i32* %3967, i32 56
  store i32 %3966, i32* %arrayidx3162, align 4, !tbaa !7
  %3968 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3163 = getelementptr inbounds i32, i32* %3968, i32 39
  %3969 = load i32, i32* %arrayidx3163, align 4, !tbaa !7
  %3970 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3164 = getelementptr inbounds i32, i32* %3970, i32 57
  store i32 %3969, i32* %arrayidx3164, align 4, !tbaa !7
  %3971 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3165 = getelementptr inbounds i32, i32* %3971, i32 23
  %3972 = load i32, i32* %arrayidx3165, align 4, !tbaa !7
  %3973 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3166 = getelementptr inbounds i32, i32* %3973, i32 58
  store i32 %3972, i32* %arrayidx3166, align 4, !tbaa !7
  %3974 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3167 = getelementptr inbounds i32, i32* %3974, i32 55
  %3975 = load i32, i32* %arrayidx3167, align 4, !tbaa !7
  %3976 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3168 = getelementptr inbounds i32, i32* %3976, i32 59
  store i32 %3975, i32* %arrayidx3168, align 4, !tbaa !7
  %3977 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3169 = getelementptr inbounds i32, i32* %3977, i32 15
  %3978 = load i32, i32* %arrayidx3169, align 4, !tbaa !7
  %3979 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3170 = getelementptr inbounds i32, i32* %3979, i32 60
  store i32 %3978, i32* %arrayidx3170, align 4, !tbaa !7
  %3980 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3171 = getelementptr inbounds i32, i32* %3980, i32 47
  %3981 = load i32, i32* %arrayidx3171, align 4, !tbaa !7
  %3982 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3172 = getelementptr inbounds i32, i32* %3982, i32 61
  store i32 %3981, i32* %arrayidx3172, align 4, !tbaa !7
  %3983 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3173 = getelementptr inbounds i32, i32* %3983, i32 31
  %3984 = load i32, i32* %arrayidx3173, align 4, !tbaa !7
  %3985 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3174 = getelementptr inbounds i32, i32* %3985, i32 62
  store i32 %3984, i32* %arrayidx3174, align 4, !tbaa !7
  %3986 = load i32*, i32** %bf0, align 4, !tbaa !2
  %arrayidx3175 = getelementptr inbounds i32, i32* %3986, i32 63
  %3987 = load i32, i32* %arrayidx3175, align 4, !tbaa !7
  %3988 = load i32*, i32** %bf1, align 4, !tbaa !2
  %arrayidx3176 = getelementptr inbounds i32, i32* %3988, i32 63
  store i32 %3987, i32* %arrayidx3176, align 4, !tbaa !7
  %3989 = load i32, i32* %stage, align 4, !tbaa !7
  %3990 = load i32*, i32** %input.addr, align 4, !tbaa !2
  %3991 = load i32*, i32** %bf1, align 4, !tbaa !2
  %3992 = load i8*, i8** %stage_range.addr, align 4, !tbaa !2
  %3993 = load i32, i32* %stage, align 4, !tbaa !7
  %arrayidx3177 = getelementptr inbounds i8, i8* %3992, i32 %3993
  %3994 = load i8, i8* %arrayidx3177, align 1, !tbaa !6
  call void @av1_range_check_buf(i32 %3989, i32* %3990, i32* %3991, i32 64, i8 signext %3994)
  %3995 = bitcast [64 x i32]* %step to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %3995) #4
  %3996 = bitcast i32** %bf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3996) #4
  %3997 = bitcast i32** %bf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3997) #4
  %3998 = bitcast i32* %stage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3998) #4
  %3999 = bitcast i32** %cospi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3999) #4
  %4000 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4000) #4
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!4, !4, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"int", !4, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"long long", !4, i64 0}
