; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/read.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/read.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifCodecDecodeInput = type { %struct.avifDecodeSampleArray, i32 }
%struct.avifDecodeSampleArray = type { %struct.avifDecodeSample*, i32, i32, i32 }
%struct.avifDecodeSample = type { %struct.avifROData, i32 }
%struct.avifROData = type { i8*, i32 }
%struct.avifROStream = type { %struct.avifROData*, i32 }
%struct.avifBoxHeader = type { i32, [4 x i8] }
%struct.avifFileType = type { [4 x i8], i32, i8*, i32 }
%struct.avifDecoder = type { i32, i32, %struct.avifImage*, i32, i32, %struct.avifImageTiming, i64, double, i64, i32, %struct.avifIOStats, %struct.avifDecoderData* }
%struct.avifImage = type { i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i8*, i32, i32, %struct.avifRWData, i32, i32, i32, i32, %struct.avifPixelAspectRatioBox, %struct.avifCleanApertureBox, %struct.avifImageRotation, %struct.avifImageMirror, %struct.avifRWData, %struct.avifRWData }
%struct.avifPixelAspectRatioBox = type { i32, i32 }
%struct.avifCleanApertureBox = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifImageRotation = type { i8 }
%struct.avifImageMirror = type { i8 }
%struct.avifRWData = type { i8*, i32 }
%struct.avifImageTiming = type { i64, double, i64, double, i64 }
%struct.avifIOStats = type { i32, i32 }
%struct.avifDecoderData = type { %struct.avifFileType, %struct.avifMeta*, %struct.avifTrackArray, %struct.avifROData, %struct.avifTileArray, i32, i32, %struct.avifImageGrid, %struct.avifImageGrid, i32, %struct.avifSampleTable*, i32 }
%struct.avifMeta = type { %struct.avifDecoderItemArray, %struct.avifPropertyArray, %struct.avifDecoderItemDataArray, i32, i32 }
%struct.avifDecoderItemArray = type { %struct.avifDecoderItem*, i32, i32, i32 }
%struct.avifDecoderItem = type { i32, %struct.avifMeta*, [4 x i8], i32, i32, i32, %struct.avifContentType, %struct.avifPropertyArray, i32, i32, i32, i32, i32 }
%struct.avifContentType = type { [64 x i8] }
%struct.avifPropertyArray = type { %struct.avifProperty*, i32, i32, i32 }
%struct.avifProperty = type { [4 x i8], %union.anon }
%union.anon = type { %struct.avifColourInformationBox, [32 x i8] }
%struct.avifColourInformationBox = type { i32, i8*, i32, i32, i32, i32, i32, i32 }
%struct.avifDecoderItemDataArray = type { %struct.avifDecoderItemData*, i32, i32, i32 }
%struct.avifDecoderItemData = type { i32, %struct.avifROData }
%struct.avifTrackArray = type { %struct.avifTrack*, i32, i32, i32 }
%struct.avifTrack = type { i32, i32, i32, i64, i32, i32, %struct.avifSampleTable*, %struct.avifMeta* }
%struct.avifTileArray = type { %struct.avifTile*, i32, i32, i32 }
%struct.avifTile = type { %struct.avifCodecDecodeInput*, %struct.avifCodec*, %struct.avifImage* }
%struct.avifCodec = type { %struct.avifCodecDecodeInput*, %struct.avifCodecConfigurationBox, %struct.avifCodecInternal*, i32 (%struct.avifCodec*, i32)*, i32 (%struct.avifCodec*, %struct.avifImage*)*, i32 (%struct.avifCodec*, %struct.avifEncoder*, %struct.avifImage*, i32, i32, %struct.avifCodecEncodeOutput*)*, i32 (%struct.avifCodec*, %struct.avifCodecEncodeOutput*)*, void (%struct.avifCodec*)* }
%struct.avifCodecConfigurationBox = type { i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.avifCodecInternal = type opaque
%struct.avifEncoder = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i64, %struct.avifIOStats, %struct.avifEncoderData* }
%struct.avifEncoderData = type opaque
%struct.avifCodecEncodeOutput = type { %struct.avifEncodeSampleArray }
%struct.avifEncodeSampleArray = type { %struct.avifEncodeSample*, i32, i32, i32 }
%struct.avifEncodeSample = type { %struct.avifRWData, i32 }
%struct.avifImageGrid = type { i8, i8, i32, i32 }
%struct.avifSampleTable = type { %struct.avifSampleTableChunkArray, %struct.avifSampleDescriptionArray, %struct.avifSampleTableSampleToChunkArray, %struct.avifSampleTableSampleSizeArray, %struct.avifSampleTableTimeToSampleArray, %struct.avifSyncSampleArray, i32 }
%struct.avifSampleTableChunkArray = type { %struct.avifSampleTableChunk*, i32, i32, i32 }
%struct.avifSampleTableChunk = type { i64 }
%struct.avifSampleDescriptionArray = type { %struct.avifSampleDescription*, i32, i32, i32 }
%struct.avifSampleDescription = type { [4 x i8], %struct.avifPropertyArray }
%struct.avifSampleTableSampleToChunkArray = type { %struct.avifSampleTableSampleToChunk*, i32, i32, i32 }
%struct.avifSampleTableSampleToChunk = type { i32, i32, i32 }
%struct.avifSampleTableSampleSizeArray = type { %struct.avifSampleTableSampleSize*, i32, i32, i32 }
%struct.avifSampleTableSampleSize = type { i32 }
%struct.avifSampleTableTimeToSampleArray = type { %struct.avifSampleTableTimeToSample*, i32, i32, i32 }
%struct.avifSampleTableTimeToSample = type { i32, i32 }
%struct.avifSyncSampleArray = type { %struct.avifSyncSample*, i32, i32, i32 }
%struct.avifSyncSample = type { i32 }
%struct.avifSequenceHeader = type { i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifAuxiliaryType = type { [64 x i8] }
%struct.avifImageSpatialExtents = type { i32, i32 }
%struct.avifPixelFormatInfo = type { i32, i32, i32 }
%struct.avifPixelInformationProperty = type { [4 x i8], i8 }

@.str = private unnamed_addr constant [5 x i8] c"ftyp\00", align 1
@.str.1 = private unnamed_addr constant [5 x i8] c"av01\00", align 1
@.str.2 = private unnamed_addr constant [5 x i8] c"grid\00", align 1
@.str.3 = private unnamed_addr constant [5 x i8] c"auxC\00", align 1
@.str.4 = private unnamed_addr constant [5 x i8] c"ispe\00", align 1
@.str.5 = private unnamed_addr constant [5 x i8] c"colr\00", align 1
@.str.6 = private unnamed_addr constant [5 x i8] c"pasp\00", align 1
@.str.7 = private unnamed_addr constant [5 x i8] c"clap\00", align 1
@.str.8 = private unnamed_addr constant [5 x i8] c"irot\00", align 1
@.str.9 = private unnamed_addr constant [5 x i8] c"imir\00", align 1
@.str.10 = private unnamed_addr constant [5 x i8] c"av1C\00", align 1
@.str.11 = private unnamed_addr constant [5 x i8] c"avif\00", align 1
@.str.12 = private unnamed_addr constant [5 x i8] c"avis\00", align 1
@.str.13 = private unnamed_addr constant [5 x i8] c"meta\00", align 1
@.str.14 = private unnamed_addr constant [5 x i8] c"moov\00", align 1
@.str.15 = private unnamed_addr constant [5 x i8] c"iloc\00", align 1
@.str.16 = private unnamed_addr constant [5 x i8] c"pitm\00", align 1
@.str.17 = private unnamed_addr constant [5 x i8] c"idat\00", align 1
@.str.18 = private unnamed_addr constant [5 x i8] c"iprp\00", align 1
@.str.19 = private unnamed_addr constant [5 x i8] c"iinf\00", align 1
@.str.20 = private unnamed_addr constant [5 x i8] c"iref\00", align 1
@.str.21 = private unnamed_addr constant [5 x i8] c"ipco\00", align 1
@.str.22 = private unnamed_addr constant [5 x i8] c"ipma\00", align 1
@.str.23 = private unnamed_addr constant [5 x i8] c"pixi\00", align 1
@.str.24 = private unnamed_addr constant [5 x i8] c"rICC\00", align 1
@.str.25 = private unnamed_addr constant [5 x i8] c"prof\00", align 1
@.str.26 = private unnamed_addr constant [5 x i8] c"nclx\00", align 1
@avifParseItemPropertyAssociation.supportedTypes = internal global [9 x i8*] [i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.6, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.23, i32 0, i32 0)], align 16
@.str.27 = private unnamed_addr constant [5 x i8] c"infe\00", align 1
@.str.28 = private unnamed_addr constant [5 x i8] c"mime\00", align 1
@.str.29 = private unnamed_addr constant [5 x i8] c"thmb\00", align 1
@.str.30 = private unnamed_addr constant [5 x i8] c"auxl\00", align 1
@.str.31 = private unnamed_addr constant [5 x i8] c"cdsc\00", align 1
@.str.32 = private unnamed_addr constant [5 x i8] c"dimg\00", align 1
@.str.33 = private unnamed_addr constant [5 x i8] c"trak\00", align 1
@.str.34 = private unnamed_addr constant [5 x i8] c"tkhd\00", align 1
@.str.35 = private unnamed_addr constant [5 x i8] c"mdia\00", align 1
@.str.36 = private unnamed_addr constant [5 x i8] c"tref\00", align 1
@.str.37 = private unnamed_addr constant [5 x i8] c"mdhd\00", align 1
@.str.38 = private unnamed_addr constant [5 x i8] c"minf\00", align 1
@.str.39 = private unnamed_addr constant [5 x i8] c"stbl\00", align 1
@.str.40 = private unnamed_addr constant [5 x i8] c"stco\00", align 1
@.str.41 = private unnamed_addr constant [5 x i8] c"co64\00", align 1
@.str.42 = private unnamed_addr constant [5 x i8] c"stsc\00", align 1
@.str.43 = private unnamed_addr constant [5 x i8] c"stsz\00", align 1
@.str.44 = private unnamed_addr constant [5 x i8] c"stss\00", align 1
@.str.45 = private unnamed_addr constant [5 x i8] c"stts\00", align 1
@.str.46 = private unnamed_addr constant [5 x i8] c"stsd\00", align 1
@.str.47 = private unnamed_addr constant [5 x i8] c"Exif\00", align 1
@xmpContentType = internal constant [20 x i8] c"application/rdf+xml\00", align 16
@.str.48 = private unnamed_addr constant [44 x i8] c"urn:mpeg:mpegB:cicp:systems:auxiliary:alpha\00", align 1
@.str.49 = private unnamed_addr constant [27 x i8] c"urn:mpeg:hevc:2015:auxid:1\00", align 1

; Function Attrs: nounwind
define hidden %struct.avifCodecDecodeInput* @avifCodecDecodeInputCreate() #0 {
entry:
  %decodeInput = alloca %struct.avifCodecDecodeInput*, align 4
  %0 = bitcast %struct.avifCodecDecodeInput** %decodeInput to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 20)
  %1 = bitcast i8* %call to %struct.avifCodecDecodeInput*
  store %struct.avifCodecDecodeInput* %1, %struct.avifCodecDecodeInput** %decodeInput, align 4, !tbaa !2
  %2 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput, align 4, !tbaa !2
  %3 = bitcast %struct.avifCodecDecodeInput* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 20, i1 false)
  %4 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput, align 4, !tbaa !2
  %samples = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %4, i32 0, i32 0
  %5 = bitcast %struct.avifDecodeSampleArray* %samples to i8*
  call void @avifArrayCreate(i8* %5, i32 12, i32 1)
  %6 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput, align 4, !tbaa !2
  %7 = bitcast %struct.avifCodecDecodeInput** %decodeInput to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret %struct.avifCodecDecodeInput* %6
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare i8* @avifAlloc(i32) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare void @avifArrayCreate(i8*, i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @avifCodecDecodeInputDestroy(%struct.avifCodecDecodeInput* %decodeInput) #0 {
entry:
  %decodeInput.addr = alloca %struct.avifCodecDecodeInput*, align 4
  store %struct.avifCodecDecodeInput* %decodeInput, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %0 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %samples = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %0, i32 0, i32 0
  %1 = bitcast %struct.avifDecodeSampleArray* %samples to i8*
  call void @avifArrayDestroy(i8* %1)
  %2 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %3 = bitcast %struct.avifCodecDecodeInput* %2 to i8*
  call void @avifFree(i8* %3)
  ret void
}

declare void @avifArrayDestroy(i8*) #2

declare void @avifFree(i8*) #2

; Function Attrs: nounwind
define hidden i32 @avifPeekCompatibleFileType(%struct.avifROData* %input) #0 {
entry:
  %retval = alloca i32, align 4
  %input.addr = alloca %struct.avifROData*, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ftyp = alloca %struct.avifFileType, align 4
  %parsed = alloca i32, align 4
  store %struct.avifROData* %input, %struct.avifROData** %input.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load %struct.avifROData*, %struct.avifROData** %input.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %2, i32 0, i32 0
  %3 = load i8*, i8** %data, align 4, !tbaa !6
  %data1 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %3, i8** %data1, align 4, !tbaa !6
  %4 = load %struct.avifROData*, %struct.avifROData** %input.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %4, i32 0, i32 1
  %5 = load i32, i32* %size, align 4, !tbaa !9
  %size2 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %5, i32* %size2, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  %6 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call3 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i32 4)
  %cmp = icmp ne i32 %call3, 0
  br i1 %cmp, label %if.then4, label %if.end5

if.then4:                                         ; preds = %do.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

if.end5:                                          ; preds = %do.end
  %7 = bitcast %struct.avifFileType* %ftyp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #4
  %8 = bitcast %struct.avifFileType* %ftyp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %8, i8 0, i32 16, i1 false)
  %9 = bitcast i32* %parsed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %call6 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size7 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %10 = load i32, i32* %size7, align 4, !tbaa !10
  %call8 = call i32 @avifParseFileTypeBox(%struct.avifFileType* %ftyp, i8* %call6, i32 %10)
  store i32 %call8, i32* %parsed, align 4, !tbaa !12
  %11 = load i32, i32* %parsed, align 4, !tbaa !12
  %tobool9 = icmp ne i32 %11, 0
  br i1 %tobool9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %if.end5
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %if.end5
  %call12 = call i32 @avifFileTypeIsCompatible(%struct.avifFileType* %ftyp)
  store i32 %call12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end11, %if.then10
  %12 = bitcast i32* %parsed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  %13 = bitcast %struct.avifFileType* %ftyp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #4
  br label %cleanup14

cleanup14:                                        ; preds = %cleanup, %if.then4, %if.then
  %14 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %14) #4
  %15 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #4
  %16 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %16) #4
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

declare void @avifROStreamStart(%struct.avifROStream*, %struct.avifROData*) #2

declare i32 @avifROStreamReadBoxHeader(%struct.avifROStream*, %struct.avifBoxHeader*) #2

declare i32 @memcmp(i8*, i8*, i32) #2

; Function Attrs: nounwind
define internal i32 @avifParseFileTypeBox(%struct.avifFileType* %ftyp, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %ftyp.addr = alloca %struct.avifFileType*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %compatibleBrandsBytes = alloca i32, align 4
  store %struct.avifFileType* %ftyp, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %4 = load %struct.avifFileType*, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %majorBrand = getelementptr inbounds %struct.avifFileType, %struct.avifFileType* %4, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %majorBrand, i32 0, i32 0
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %arraydecay, i32 4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %5 = load %struct.avifFileType*, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %minorVersion = getelementptr inbounds %struct.avifFileType, %struct.avifFileType* %5, i32 0, i32 1
  %call2 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %minorVersion)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %6 = bitcast i32* %compatibleBrandsBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %call8 = call i32 @avifROStreamRemainingBytes(%struct.avifROStream* %s)
  store i32 %call8, i32* %compatibleBrandsBytes, align 4, !tbaa !14
  %7 = load i32, i32* %compatibleBrandsBytes, align 4, !tbaa !14
  %rem = urem i32 %7, 4
  %cmp = icmp ne i32 %rem, 0
  br i1 %cmp, label %if.then9, label %if.end10

if.then9:                                         ; preds = %do.end7
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %do.end7
  %call11 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %8 = load %struct.avifFileType*, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %compatibleBrands = getelementptr inbounds %struct.avifFileType, %struct.avifFileType* %8, i32 0, i32 2
  store i8* %call11, i8** %compatibleBrands, align 4, !tbaa !15
  br label %do.body12

do.body12:                                        ; preds = %if.end10
  %9 = load i32, i32* %compatibleBrandsBytes, align 4, !tbaa !14
  %call13 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %9)
  %tobool14 = icmp ne i32 %call13, 0
  br i1 %tobool14, label %if.end16, label %if.then15

if.then15:                                        ; preds = %do.body12
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %do.body12
  br label %do.cond17

do.cond17:                                        ; preds = %if.end16
  br label %do.end18

do.end18:                                         ; preds = %do.cond17
  %10 = load i32, i32* %compatibleBrandsBytes, align 4, !tbaa !14
  %div = sdiv i32 %10, 4
  %11 = load %struct.avifFileType*, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %compatibleBrandsCount = getelementptr inbounds %struct.avifFileType, %struct.avifFileType* %11, i32 0, i32 3
  store i32 %div, i32* %compatibleBrandsCount, align 4, !tbaa !17
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end18, %if.then15, %if.then9
  %12 = bitcast i32* %compatibleBrandsBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  br label %cleanup19

cleanup19:                                        ; preds = %cleanup, %if.then4, %if.then
  %13 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #4
  %14 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %14) #4
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

declare i8* @avifROStreamCurrent(%struct.avifROStream*) #2

; Function Attrs: nounwind
define internal i32 @avifFileTypeIsCompatible(%struct.avifFileType* %ftyp) #0 {
entry:
  %ftyp.addr = alloca %struct.avifFileType*, align 4
  %avifCompatible = alloca i32, align 4
  %compatibleBrandIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %compatibleBrand = alloca i8*, align 4
  store %struct.avifFileType* %ftyp, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %0 = bitcast i32* %avifCompatible to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifFileType*, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %majorBrand = getelementptr inbounds %struct.avifFileType, %struct.avifFileType* %1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %majorBrand, i32 0, i32 0
  %call = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.11, i32 0, i32 0), i32 4)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %2 = load %struct.avifFileType*, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %majorBrand1 = getelementptr inbounds %struct.avifFileType, %struct.avifFileType* %2, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [4 x i8], [4 x i8]* %majorBrand1, i32 0, i32 0
  %call3 = call i32 @memcmp(i8* %arraydecay2, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.12, i32 0, i32 0), i32 4)
  %cmp4 = icmp eq i32 %call3, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %3 = phi i1 [ true, %entry ], [ %cmp4, %lor.rhs ]
  %lor.ext = zext i1 %3 to i32
  store i32 %lor.ext, i32* %avifCompatible, align 4, !tbaa !12
  %4 = load i32, i32* %avifCompatible, align 4, !tbaa !12
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.end12, label %if.then

if.then:                                          ; preds = %lor.end
  %5 = bitcast i32* %compatibleBrandIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %compatibleBrandIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %6 = load i32, i32* %compatibleBrandIndex, align 4, !tbaa !12
  %7 = load %struct.avifFileType*, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %compatibleBrandsCount = getelementptr inbounds %struct.avifFileType, %struct.avifFileType* %7, i32 0, i32 3
  %8 = load i32, i32* %compatibleBrandsCount, align 4, !tbaa !17
  %cmp5 = icmp slt i32 %6, %8
  br i1 %cmp5, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup11

for.body:                                         ; preds = %for.cond
  %9 = bitcast i8** %compatibleBrand to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.avifFileType*, %struct.avifFileType** %ftyp.addr, align 4, !tbaa !2
  %compatibleBrands = getelementptr inbounds %struct.avifFileType, %struct.avifFileType* %10, i32 0, i32 2
  %11 = load i8*, i8** %compatibleBrands, align 4, !tbaa !15
  %12 = load i32, i32* %compatibleBrandIndex, align 4, !tbaa !12
  %mul = mul nsw i32 4, %12
  %arrayidx = getelementptr inbounds i8, i8* %11, i32 %mul
  store i8* %arrayidx, i8** %compatibleBrand, align 4, !tbaa !2
  %13 = load i8*, i8** %compatibleBrand, align 4, !tbaa !2
  %call6 = call i32 @memcmp(i8* %13, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.11, i32 0, i32 0), i32 4)
  %tobool7 = icmp ne i32 %call6, 0
  br i1 %tobool7, label %lor.lhs.false, label %if.then10

lor.lhs.false:                                    ; preds = %for.body
  %14 = load i8*, i8** %compatibleBrand, align 4, !tbaa !2
  %call8 = call i32 @memcmp(i8* %14, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.12, i32 0, i32 0), i32 4)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %if.end, label %if.then10

if.then10:                                        ; preds = %lor.lhs.false, %for.body
  store i32 1, i32* %avifCompatible, align 4, !tbaa !12
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then10
  %15 = bitcast i8** %compatibleBrand to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup11 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %16 = load i32, i32* %compatibleBrandIndex, align 4, !tbaa !12
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %compatibleBrandIndex, align 4, !tbaa !12
  br label %for.cond

cleanup11:                                        ; preds = %cleanup, %for.cond.cleanup
  %17 = bitcast i32* %compatibleBrandIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %for.end

for.end:                                          ; preds = %cleanup11
  br label %if.end12

if.end12:                                         ; preds = %for.end, %lor.end
  %18 = load i32, i32* %avifCompatible, align 4, !tbaa !12
  store i32 1, i32* %cleanup.dest.slot, align 4
  %19 = bitcast i32* %avifCompatible to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  ret i32 %18
}

; Function Attrs: nounwind
define hidden %struct.avifDecoder* @avifDecoderCreate() #0 {
entry:
  %decoder = alloca %struct.avifDecoder*, align 4
  %0 = bitcast %struct.avifDecoder** %decoder to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 104)
  %1 = bitcast i8* %call to %struct.avifDecoder*
  store %struct.avifDecoder* %1, %struct.avifDecoder** %decoder, align 4, !tbaa !2
  %2 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder, align 4, !tbaa !2
  %3 = bitcast %struct.avifDecoder* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %3, i8 0, i32 104, i1 false)
  %4 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder, align 4, !tbaa !2
  %5 = bitcast %struct.avifDecoder** %decoder to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #4
  ret %struct.avifDecoder* %4
}

; Function Attrs: nounwind
define hidden void @avifDecoderDestroy(%struct.avifDecoder* %decoder) #0 {
entry:
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %0 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  call void @avifDecoderCleanup(%struct.avifDecoder* %0)
  %1 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %2 = bitcast %struct.avifDecoder* %1 to i8*
  call void @avifFree(i8* %2)
  ret void
}

; Function Attrs: nounwind
define internal void @avifDecoderCleanup(%struct.avifDecoder* %decoder) #0 {
entry:
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %0 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %0, i32 0, i32 11
  %1 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !18
  %tobool = icmp ne %struct.avifDecoderData* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %2, i32 0, i32 11
  %3 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data1, align 4, !tbaa !18
  call void @avifDecoderDataDestroy(%struct.avifDecoderData* %3)
  %4 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data2 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %4, i32 0, i32 11
  store %struct.avifDecoderData* null, %struct.avifDecoderData** %data2, align 4, !tbaa !18
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %5, i32 0, i32 2
  %6 = load %struct.avifImage*, %struct.avifImage** %image, align 8, !tbaa !24
  %tobool3 = icmp ne %struct.avifImage* %6, null
  br i1 %tobool3, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.end
  %7 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image5 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %7, i32 0, i32 2
  %8 = load %struct.avifImage*, %struct.avifImage** %image5, align 8, !tbaa !24
  call void @avifImageDestroy(%struct.avifImage* %8)
  %9 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image6 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %9, i32 0, i32 2
  store %struct.avifImage* null, %struct.avifImage** %image6, align 8, !tbaa !24
  br label %if.end7

if.end7:                                          ; preds = %if.then4, %if.end
  ret void
}

; Function Attrs: nounwind
define hidden i32 @avifDecoderSetSource(%struct.avifDecoder* %decoder, i32 %source) #0 {
entry:
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %source.addr = alloca i32, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  store i32 %source, i32* %source.addr, align 4, !tbaa !25
  %0 = load i32, i32* %source.addr, align 4, !tbaa !25
  %1 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %requestedSource = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %1, i32 0, i32 1
  store i32 %0, i32* %requestedSource, align 4, !tbaa !26
  %2 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %call = call i32 @avifDecoderReset(%struct.avifDecoder* %2)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @avifDecoderReset(%struct.avifDecoder* %decoder) #0 {
entry:
  %retval = alloca i32, align 4
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %data = alloca %struct.avifDecoderData*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %colorProperties = alloca %struct.avifPropertyArray*, align 4
  %colorTrack = alloca %struct.avifTrack*, align 4
  %alphaTrack = alloca %struct.avifTrack*, align 4
  %colorTrackIndex = alloca i32, align 4
  %track = alloca %struct.avifTrack*, align 4
  %alphaTrackIndex = alloca i32, align 4
  %track75 = alloca %struct.avifTrack*, align 4
  %colorTile = alloca %struct.avifTile*, align 4
  %alphaTile = alloca %struct.avifTile*, align 4
  %colorOBU = alloca %struct.avifROData, align 4
  %alphaOBU = alloca %struct.avifROData, align 4
  %colorOBUItem = alloca %struct.avifDecoderItem*, align 4
  %alphaOBUItem = alloca %struct.avifDecoderItem*, align 4
  %itemIndex = alloca i32, align 4
  %item = alloca %struct.avifDecoderItem*, align 4
  %isGrid = alloca i32, align 4
  %itemPtr = alloca i8*, align 4
  %itemIndex246 = alloca i32, align 4
  %item255 = alloca %struct.avifDecoderItem*, align 4
  %isGrid268 = alloca i32, align 4
  %auxCProp = alloca %struct.avifProperty*, align 4
  %itemPtr302 = alloca i8*, align 4
  %colorTile364 = alloca %struct.avifTile*, align 4
  %colorSample = alloca %struct.avifDecodeSample*, align 4
  %alphaTile398 = alloca %struct.avifTile*, align 4
  %alphaSample = alloca %struct.avifDecodeSample*, align 4
  %ispeProp = alloca %struct.avifProperty*, align 4
  %tileIndex = alloca i32, align 4
  %tile = alloca %struct.avifTile*, align 4
  %sampleIndex = alloca i32, align 4
  %sample = alloca %struct.avifDecodeSample*, align 4
  %colrProp = alloca %struct.avifProperty*, align 4
  %paspProp = alloca %struct.avifProperty*, align 4
  %clapProp = alloca %struct.avifProperty*, align 4
  %irotProp = alloca %struct.avifProperty*, align 4
  %imirProp = alloca %struct.avifProperty*, align 4
  %firstTile = alloca %struct.avifTile*, align 4
  %sample598 = alloca %struct.avifDecodeSample*, align 4
  %sequenceHeader = alloca %struct.avifSequenceHeader, align 4
  %av1CProp = alloca %struct.avifProperty*, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifDecoderData** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %1, i32 0, i32 11
  %2 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data1, align 4, !tbaa !18
  store %struct.avifDecoderData* %2, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %3 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tobool = icmp ne %struct.avifDecoderData* %3, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup676

if.end:                                           ; preds = %entry
  %4 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %colorGrid = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %4, i32 0, i32 7
  %5 = bitcast %struct.avifImageGrid* %colorGrid to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %5, i8 0, i32 12, i1 false)
  %6 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %alphaGrid = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %6, i32 0, i32 8
  %7 = bitcast %struct.avifImageGrid* %alphaGrid to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %7, i8 0, i32 12, i1 false)
  %8 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  call void @avifDecoderDataClearTiles(%struct.avifDecoderData* %8)
  %9 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %9, i32 0, i32 2
  %10 = load %struct.avifImage*, %struct.avifImage** %image, align 8, !tbaa !24
  %tobool2 = icmp ne %struct.avifImage* %10, null
  br i1 %tobool2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %11 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image4 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %11, i32 0, i32 2
  %12 = load %struct.avifImage*, %struct.avifImage** %image4, align 8, !tbaa !24
  call void @avifImageDestroy(%struct.avifImage* %12)
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  %call = call %struct.avifImage* @avifImageCreateEmpty()
  %13 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image6 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %13, i32 0, i32 2
  store %struct.avifImage* %call, %struct.avifImage** %image6, align 8, !tbaa !24
  %14 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %cicpSet = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %14, i32 0, i32 11
  store i32 0, i32* %cicpSet, align 4, !tbaa !27
  %15 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %ioStats = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %15, i32 0, i32 10
  %16 = bitcast %struct.avifIOStats* %ioStats to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %16, i8 0, i32 8, i1 false)
  %17 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %sourceSampleTable = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %17, i32 0, i32 10
  store %struct.avifSampleTable* null, %struct.avifSampleTable** %sourceSampleTable, align 4, !tbaa !32
  %18 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %requestedSource = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %18, i32 0, i32 1
  %19 = load i32, i32* %requestedSource, align 4, !tbaa !26
  %cmp = icmp eq i32 %19, 0
  br i1 %cmp, label %if.then7, label %if.else12

if.then7:                                         ; preds = %if.end5
  %20 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tracks = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %20, i32 0, i32 2
  %count = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks, i32 0, i32 2
  %21 = load i32, i32* %count, align 4, !tbaa !33
  %cmp8 = icmp ugt i32 %21, 0
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.then7
  %22 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %source = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %22, i32 0, i32 9
  store i32 2, i32* %source, align 4, !tbaa !34
  br label %if.end11

if.else:                                          ; preds = %if.then7
  %23 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %source10 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %23, i32 0, i32 9
  store i32 1, i32* %source10, align 4, !tbaa !34
  br label %if.end11

if.end11:                                         ; preds = %if.else, %if.then9
  br label %if.end15

if.else12:                                        ; preds = %if.end5
  %24 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %requestedSource13 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %24, i32 0, i32 1
  %25 = load i32, i32* %requestedSource13, align 4, !tbaa !26
  %26 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %source14 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %26, i32 0, i32 9
  store i32 %25, i32* %source14, align 4, !tbaa !34
  br label %if.end15

if.end15:                                         ; preds = %if.else12, %if.end11
  %27 = bitcast %struct.avifPropertyArray** %colorProperties to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  store %struct.avifPropertyArray* null, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %28 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %source16 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %28, i32 0, i32 9
  %29 = load i32, i32* %source16, align 4, !tbaa !34
  %cmp17 = icmp eq i32 %29, 2
  br i1 %cmp17, label %if.then18, label %if.else171

if.then18:                                        ; preds = %if.end15
  %30 = bitcast %struct.avifTrack** %colorTrack to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store %struct.avifTrack* null, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %31 = bitcast %struct.avifTrack** %alphaTrack to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  store %struct.avifTrack* null, %struct.avifTrack** %alphaTrack, align 4, !tbaa !2
  %32 = bitcast i32* %colorTrackIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  store i32 0, i32* %colorTrackIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then18
  %33 = load i32, i32* %colorTrackIndex, align 4, !tbaa !12
  %34 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data19 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %34, i32 0, i32 11
  %35 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data19, align 4, !tbaa !18
  %tracks20 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %35, i32 0, i32 2
  %count21 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks20, i32 0, i32 2
  %36 = load i32, i32* %count21, align 4, !tbaa !33
  %cmp22 = icmp ult i32 %33, %36
  br i1 %cmp22, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %37 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  %38 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data23 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %38, i32 0, i32 11
  %39 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data23, align 4, !tbaa !18
  %tracks24 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %39, i32 0, i32 2
  %track25 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks24, i32 0, i32 0
  %40 = load %struct.avifTrack*, %struct.avifTrack** %track25, align 4, !tbaa !35
  %41 = load i32, i32* %colorTrackIndex, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %40, i32 %41
  store %struct.avifTrack* %arrayidx, %struct.avifTrack** %track, align 4, !tbaa !2
  %42 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %sampleTable = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %42, i32 0, i32 6
  %43 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 8, !tbaa !36
  %tobool26 = icmp ne %struct.avifSampleTable* %43, null
  br i1 %tobool26, label %if.end28, label %if.then27

if.then27:                                        ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end28:                                         ; preds = %for.body
  %44 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %id = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %44, i32 0, i32 0
  %45 = load i32, i32* %id, align 8, !tbaa !38
  %tobool29 = icmp ne i32 %45, 0
  br i1 %tobool29, label %if.end31, label %if.then30

if.then30:                                        ; preds = %if.end28
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %if.end28
  %46 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %sampleTable32 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %46, i32 0, i32 6
  %47 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable32, align 8, !tbaa !36
  %chunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %47, i32 0, i32 0
  %count33 = getelementptr inbounds %struct.avifSampleTableChunkArray, %struct.avifSampleTableChunkArray* %chunks, i32 0, i32 2
  %48 = load i32, i32* %count33, align 4, !tbaa !39
  %tobool34 = icmp ne i32 %48, 0
  br i1 %tobool34, label %if.end36, label %if.then35

if.then35:                                        ; preds = %if.end31
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end36:                                         ; preds = %if.end31
  %49 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %sampleTable37 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %49, i32 0, i32 6
  %50 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable37, align 8, !tbaa !36
  %call38 = call i32 @avifSampleTableHasFormat(%struct.avifSampleTable* %50, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0))
  %tobool39 = icmp ne i32 %call38, 0
  br i1 %tobool39, label %if.end41, label %if.then40

if.then40:                                        ; preds = %if.end36
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end41:                                         ; preds = %if.end36
  %51 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %auxForID = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %51, i32 0, i32 1
  %52 = load i32, i32* %auxForID, align 4, !tbaa !47
  %cmp42 = icmp ne i32 %52, 0
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %if.end41
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end44:                                         ; preds = %if.end41
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end44, %if.then43, %if.then40, %if.then35, %if.then30, %if.then27
  %53 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 4, label %for.inc
    i32 2, label %for.end
  ]

for.inc:                                          ; preds = %cleanup
  %54 = load i32, i32* %colorTrackIndex, align 4, !tbaa !12
  %inc = add i32 %54, 1
  store i32 %inc, i32* %colorTrackIndex, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %cleanup, %for.cond
  %55 = load i32, i32* %colorTrackIndex, align 4, !tbaa !12
  %56 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data45 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %56, i32 0, i32 11
  %57 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data45, align 4, !tbaa !18
  %tracks46 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %57, i32 0, i32 2
  %count47 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks46, i32 0, i32 2
  %58 = load i32, i32* %count47, align 4, !tbaa !33
  %cmp48 = icmp eq i32 %55, %58
  br i1 %cmp48, label %if.then49, label %if.end50

if.then49:                                        ; preds = %for.end
  store i32 3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup166

if.end50:                                         ; preds = %for.end
  %59 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data51 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %59, i32 0, i32 11
  %60 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data51, align 4, !tbaa !18
  %tracks52 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %60, i32 0, i32 2
  %track53 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks52, i32 0, i32 0
  %61 = load %struct.avifTrack*, %struct.avifTrack** %track53, align 4, !tbaa !35
  %62 = load i32, i32* %colorTrackIndex, align 4, !tbaa !12
  %arrayidx54 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %61, i32 %62
  store %struct.avifTrack* %arrayidx54, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %63 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %sampleTable55 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %63, i32 0, i32 6
  %64 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable55, align 8, !tbaa !36
  %call56 = call %struct.avifPropertyArray* @avifSampleTableGetProperties(%struct.avifSampleTable* %64)
  store %struct.avifPropertyArray* %call56, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %65 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %tobool57 = icmp ne %struct.avifPropertyArray* %65, null
  br i1 %tobool57, label %if.end59, label %if.then58

if.then58:                                        ; preds = %if.end50
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup166

if.end59:                                         ; preds = %if.end50
  %66 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %meta = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %66, i32 0, i32 7
  %67 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !48
  %tobool60 = icmp ne %struct.avifMeta* %67, null
  br i1 %tobool60, label %if.then61, label %if.end68

if.then61:                                        ; preds = %if.end59
  %68 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %69 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %meta62 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %69, i32 0, i32 7
  %70 = load %struct.avifMeta*, %struct.avifMeta** %meta62, align 4, !tbaa !48
  %71 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image63 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %71, i32 0, i32 2
  %72 = load %struct.avifImage*, %struct.avifImage** %image63, align 8, !tbaa !24
  %call64 = call i32 @avifDecoderDataFindMetadata(%struct.avifDecoderData* %68, %struct.avifMeta* %70, %struct.avifImage* %72, i32 0)
  %tobool65 = icmp ne i32 %call64, 0
  br i1 %tobool65, label %if.end67, label %if.then66

if.then66:                                        ; preds = %if.then61
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup166

if.end67:                                         ; preds = %if.then61
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %if.end59
  %73 = bitcast i32* %alphaTrackIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #4
  store i32 0, i32* %alphaTrackIndex, align 4, !tbaa !12
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc106, %if.end68
  %74 = load i32, i32* %alphaTrackIndex, align 4, !tbaa !12
  %75 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data70 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %75, i32 0, i32 11
  %76 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data70, align 4, !tbaa !18
  %tracks71 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %76, i32 0, i32 2
  %count72 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks71, i32 0, i32 2
  %77 = load i32, i32* %count72, align 4, !tbaa !33
  %cmp73 = icmp ult i32 %74, %77
  br i1 %cmp73, label %for.body74, label %for.end108

for.body74:                                       ; preds = %for.cond69
  %78 = bitcast %struct.avifTrack** %track75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #4
  %79 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data76 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %79, i32 0, i32 11
  %80 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data76, align 4, !tbaa !18
  %tracks77 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %80, i32 0, i32 2
  %track78 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks77, i32 0, i32 0
  %81 = load %struct.avifTrack*, %struct.avifTrack** %track78, align 4, !tbaa !35
  %82 = load i32, i32* %alphaTrackIndex, align 4, !tbaa !12
  %arrayidx79 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %81, i32 %82
  store %struct.avifTrack* %arrayidx79, %struct.avifTrack** %track75, align 4, !tbaa !2
  %83 = load %struct.avifTrack*, %struct.avifTrack** %track75, align 4, !tbaa !2
  %sampleTable80 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %83, i32 0, i32 6
  %84 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable80, align 8, !tbaa !36
  %tobool81 = icmp ne %struct.avifSampleTable* %84, null
  br i1 %tobool81, label %if.end83, label %if.then82

if.then82:                                        ; preds = %for.body74
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup104

if.end83:                                         ; preds = %for.body74
  %85 = load %struct.avifTrack*, %struct.avifTrack** %track75, align 4, !tbaa !2
  %id84 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %85, i32 0, i32 0
  %86 = load i32, i32* %id84, align 8, !tbaa !38
  %tobool85 = icmp ne i32 %86, 0
  br i1 %tobool85, label %if.end87, label %if.then86

if.then86:                                        ; preds = %if.end83
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup104

if.end87:                                         ; preds = %if.end83
  %87 = load %struct.avifTrack*, %struct.avifTrack** %track75, align 4, !tbaa !2
  %sampleTable88 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %87, i32 0, i32 6
  %88 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable88, align 8, !tbaa !36
  %chunks89 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %88, i32 0, i32 0
  %count90 = getelementptr inbounds %struct.avifSampleTableChunkArray, %struct.avifSampleTableChunkArray* %chunks89, i32 0, i32 2
  %89 = load i32, i32* %count90, align 4, !tbaa !39
  %tobool91 = icmp ne i32 %89, 0
  br i1 %tobool91, label %if.end93, label %if.then92

if.then92:                                        ; preds = %if.end87
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup104

if.end93:                                         ; preds = %if.end87
  %90 = load %struct.avifTrack*, %struct.avifTrack** %track75, align 4, !tbaa !2
  %sampleTable94 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %90, i32 0, i32 6
  %91 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable94, align 8, !tbaa !36
  %call95 = call i32 @avifSampleTableHasFormat(%struct.avifSampleTable* %91, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0))
  %tobool96 = icmp ne i32 %call95, 0
  br i1 %tobool96, label %if.end98, label %if.then97

if.then97:                                        ; preds = %if.end93
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup104

if.end98:                                         ; preds = %if.end93
  %92 = load %struct.avifTrack*, %struct.avifTrack** %track75, align 4, !tbaa !2
  %auxForID99 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %92, i32 0, i32 1
  %93 = load i32, i32* %auxForID99, align 4, !tbaa !47
  %94 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %id100 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %94, i32 0, i32 0
  %95 = load i32, i32* %id100, align 8, !tbaa !38
  %cmp101 = icmp eq i32 %93, %95
  br i1 %cmp101, label %if.then102, label %if.end103

if.then102:                                       ; preds = %if.end98
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup104

if.end103:                                        ; preds = %if.end98
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup104

cleanup104:                                       ; preds = %if.end103, %if.then102, %if.then97, %if.then92, %if.then86, %if.then82
  %96 = bitcast %struct.avifTrack** %track75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %cleanup.dest105 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest105, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc106
    i32 5, label %for.end108
  ]

cleanup.cont:                                     ; preds = %cleanup104
  br label %for.inc106

for.inc106:                                       ; preds = %cleanup.cont, %cleanup104
  %97 = load i32, i32* %alphaTrackIndex, align 4, !tbaa !12
  %inc107 = add i32 %97, 1
  store i32 %inc107, i32* %alphaTrackIndex, align 4, !tbaa !12
  br label %for.cond69

for.end108:                                       ; preds = %cleanup104, %for.cond69
  %98 = load i32, i32* %alphaTrackIndex, align 4, !tbaa !12
  %99 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data109 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %99, i32 0, i32 11
  %100 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data109, align 4, !tbaa !18
  %tracks110 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %100, i32 0, i32 2
  %count111 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks110, i32 0, i32 2
  %101 = load i32, i32* %count111, align 4, !tbaa !33
  %cmp112 = icmp ne i32 %98, %101
  br i1 %cmp112, label %if.then113, label %if.end118

if.then113:                                       ; preds = %for.end108
  %102 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data114 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %102, i32 0, i32 11
  %103 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data114, align 4, !tbaa !18
  %tracks115 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %103, i32 0, i32 2
  %track116 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks115, i32 0, i32 0
  %104 = load %struct.avifTrack*, %struct.avifTrack** %track116, align 4, !tbaa !35
  %105 = load i32, i32* %alphaTrackIndex, align 4, !tbaa !12
  %arrayidx117 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %104, i32 %105
  store %struct.avifTrack* %arrayidx117, %struct.avifTrack** %alphaTrack, align 4, !tbaa !2
  br label %if.end118

if.end118:                                        ; preds = %if.then113, %for.end108
  %106 = bitcast %struct.avifTile** %colorTile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #4
  %107 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data119 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %107, i32 0, i32 11
  %108 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data119, align 4, !tbaa !18
  %call120 = call %struct.avifTile* @avifDecoderDataCreateTile(%struct.avifDecoderData* %108)
  store %struct.avifTile* %call120, %struct.avifTile** %colorTile, align 4, !tbaa !2
  %109 = load %struct.avifTile*, %struct.avifTile** %colorTile, align 4, !tbaa !2
  %input = getelementptr inbounds %struct.avifTile, %struct.avifTile* %109, i32 0, i32 0
  %110 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input, align 4, !tbaa !49
  %111 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %sampleTable121 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %111, i32 0, i32 6
  %112 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable121, align 8, !tbaa !36
  %113 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data122 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %113, i32 0, i32 11
  %114 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data122, align 4, !tbaa !18
  %rawInput = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %114, i32 0, i32 3
  %call123 = call i32 @avifCodecDecodeInputGetSamples(%struct.avifCodecDecodeInput* %110, %struct.avifSampleTable* %112, %struct.avifROData* %rawInput)
  %tobool124 = icmp ne i32 %call123, 0
  br i1 %tobool124, label %if.end126, label %if.then125

if.then125:                                       ; preds = %if.end118
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup164

if.end126:                                        ; preds = %if.end118
  %115 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data127 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %115, i32 0, i32 11
  %116 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data127, align 4, !tbaa !18
  %colorTileCount = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %116, i32 0, i32 5
  store i32 1, i32* %colorTileCount, align 4, !tbaa !51
  %117 = bitcast %struct.avifTile** %alphaTile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #4
  store %struct.avifTile* null, %struct.avifTile** %alphaTile, align 4, !tbaa !2
  %118 = load %struct.avifTrack*, %struct.avifTrack** %alphaTrack, align 4, !tbaa !2
  %tobool128 = icmp ne %struct.avifTrack* %118, null
  br i1 %tobool128, label %if.then129, label %if.end142

if.then129:                                       ; preds = %if.end126
  %119 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data130 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %119, i32 0, i32 11
  %120 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data130, align 4, !tbaa !18
  %call131 = call %struct.avifTile* @avifDecoderDataCreateTile(%struct.avifDecoderData* %120)
  store %struct.avifTile* %call131, %struct.avifTile** %alphaTile, align 4, !tbaa !2
  %121 = load %struct.avifTile*, %struct.avifTile** %alphaTile, align 4, !tbaa !2
  %input132 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %121, i32 0, i32 0
  %122 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input132, align 4, !tbaa !49
  %123 = load %struct.avifTrack*, %struct.avifTrack** %alphaTrack, align 4, !tbaa !2
  %sampleTable133 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %123, i32 0, i32 6
  %124 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable133, align 8, !tbaa !36
  %125 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data134 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %125, i32 0, i32 11
  %126 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data134, align 4, !tbaa !18
  %rawInput135 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %126, i32 0, i32 3
  %call136 = call i32 @avifCodecDecodeInputGetSamples(%struct.avifCodecDecodeInput* %122, %struct.avifSampleTable* %124, %struct.avifROData* %rawInput135)
  %tobool137 = icmp ne i32 %call136, 0
  br i1 %tobool137, label %if.end139, label %if.then138

if.then138:                                       ; preds = %if.then129
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup163

if.end139:                                        ; preds = %if.then129
  %127 = load %struct.avifTile*, %struct.avifTile** %alphaTile, align 4, !tbaa !2
  %input140 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %127, i32 0, i32 0
  %128 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input140, align 4, !tbaa !49
  %alpha = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %128, i32 0, i32 1
  store i32 1, i32* %alpha, align 4, !tbaa !52
  %129 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data141 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %129, i32 0, i32 11
  %130 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data141, align 4, !tbaa !18
  %alphaTileCount = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %130, i32 0, i32 6
  store i32 1, i32* %alphaTileCount, align 4, !tbaa !55
  br label %if.end142

if.end142:                                        ; preds = %if.end139, %if.end126
  %131 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %sampleTable143 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %131, i32 0, i32 6
  %132 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable143, align 8, !tbaa !36
  %133 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %sourceSampleTable144 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %133, i32 0, i32 10
  store %struct.avifSampleTable* %132, %struct.avifSampleTable** %sourceSampleTable144, align 4, !tbaa !32
  %134 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %134, i32 0, i32 3
  store i32 -1, i32* %imageIndex, align 4, !tbaa !56
  %135 = load %struct.avifTile*, %struct.avifTile** %colorTile, align 4, !tbaa !2
  %input145 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %135, i32 0, i32 0
  %136 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input145, align 4, !tbaa !49
  %samples = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %136, i32 0, i32 0
  %count146 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples, i32 0, i32 2
  %137 = load i32, i32* %count146, align 4, !tbaa !57
  %138 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageCount = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %138, i32 0, i32 4
  store i32 %137, i32* %imageCount, align 8, !tbaa !58
  %139 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %mediaTimescale = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %139, i32 0, i32 2
  %140 = load i32, i32* %mediaTimescale, align 8, !tbaa !59
  %conv = zext i32 %140 to i64
  %141 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %timescale = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %141, i32 0, i32 6
  store i64 %conv, i64* %timescale, align 8, !tbaa !60
  %142 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %mediaDuration = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %142, i32 0, i32 3
  %143 = load i64, i64* %mediaDuration, align 8, !tbaa !61
  %144 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %durationInTimescales = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %144, i32 0, i32 8
  store i64 %143, i64* %durationInTimescales, align 8, !tbaa !62
  %145 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %mediaTimescale147 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %145, i32 0, i32 2
  %146 = load i32, i32* %mediaTimescale147, align 8, !tbaa !59
  %tobool148 = icmp ne i32 %146, 0
  br i1 %tobool148, label %if.then149, label %if.else154

if.then149:                                       ; preds = %if.end142
  %147 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %durationInTimescales150 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %147, i32 0, i32 8
  %148 = load i64, i64* %durationInTimescales150, align 8, !tbaa !62
  %conv151 = uitofp i64 %148 to double
  %149 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %mediaTimescale152 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %149, i32 0, i32 2
  %150 = load i32, i32* %mediaTimescale152, align 8, !tbaa !59
  %conv153 = uitofp i32 %150 to double
  %div = fdiv double %conv151, %conv153
  %151 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %duration = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %151, i32 0, i32 7
  store double %div, double* %duration, align 8, !tbaa !63
  br label %if.end156

if.else154:                                       ; preds = %if.end142
  %152 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %duration155 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %152, i32 0, i32 7
  store double 0.000000e+00, double* %duration155, align 8, !tbaa !63
  br label %if.end156

if.end156:                                        ; preds = %if.else154, %if.then149
  %153 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageTiming = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %153, i32 0, i32 5
  %154 = bitcast %struct.avifImageTiming* %imageTiming to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %154, i8 0, i32 40, i1 false)
  %155 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %155, i32 0, i32 4
  %156 = load i32, i32* %width, align 8, !tbaa !64
  %157 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image157 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %157, i32 0, i32 2
  %158 = load %struct.avifImage*, %struct.avifImage** %image157, align 8, !tbaa !24
  %width158 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %158, i32 0, i32 0
  store i32 %156, i32* %width158, align 4, !tbaa !65
  %159 = load %struct.avifTrack*, %struct.avifTrack** %colorTrack, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %159, i32 0, i32 5
  %160 = load i32, i32* %height, align 4, !tbaa !72
  %161 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image159 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %161, i32 0, i32 2
  %162 = load %struct.avifImage*, %struct.avifImage** %image159, align 8, !tbaa !24
  %height160 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %162, i32 0, i32 1
  store i32 %160, i32* %height160, align 4, !tbaa !73
  %163 = load %struct.avifTrack*, %struct.avifTrack** %alphaTrack, align 4, !tbaa !2
  %cmp161 = icmp ne %struct.avifTrack* %163, null
  %conv162 = zext i1 %cmp161 to i32
  %164 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %alphaPresent = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %164, i32 0, i32 9
  store i32 %conv162, i32* %alphaPresent, align 8, !tbaa !74
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup163

cleanup163:                                       ; preds = %if.end156, %if.then138
  %165 = bitcast %struct.avifTile** %alphaTile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #4
  br label %cleanup164

cleanup164:                                       ; preds = %cleanup163, %if.then125
  %166 = bitcast %struct.avifTile** %colorTile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #4
  %167 = bitcast i32* %alphaTrackIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  br label %cleanup166

cleanup166:                                       ; preds = %cleanup164, %if.then66, %if.then58, %if.then49
  %168 = bitcast i32* %colorTrackIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  %169 = bitcast %struct.avifTrack** %alphaTrack to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  %170 = bitcast %struct.avifTrack** %colorTrack to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #4
  %cleanup.dest169 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest169, label %cleanup675 [
    i32 0, label %cleanup.cont170
  ]

cleanup.cont170:                                  ; preds = %cleanup166
  br label %if.end460

if.else171:                                       ; preds = %if.end15
  %171 = bitcast %struct.avifROData* %colorOBU to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %171) #4
  %172 = bitcast %struct.avifROData* %colorOBU to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %172, i8 0, i32 8, i1 false)
  %173 = bitcast %struct.avifROData* %alphaOBU to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %173) #4
  %174 = bitcast %struct.avifROData* %alphaOBU to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %174, i8 0, i32 8, i1 false)
  %175 = bitcast %struct.avifDecoderItem** %colorOBUItem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %175) #4
  store %struct.avifDecoderItem* null, %struct.avifDecoderItem** %colorOBUItem, align 4, !tbaa !2
  %176 = bitcast %struct.avifDecoderItem** %alphaOBUItem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %176) #4
  store %struct.avifDecoderItem* null, %struct.avifDecoderItem** %alphaOBUItem, align 4, !tbaa !2
  %177 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %177) #4
  store i32 0, i32* %itemIndex, align 4, !tbaa !12
  br label %for.cond172

for.cond172:                                      ; preds = %for.inc238, %if.else171
  %178 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %179 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %meta173 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %179, i32 0, i32 1
  %180 = load %struct.avifMeta*, %struct.avifMeta** %meta173, align 4, !tbaa !75
  %items = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %180, i32 0, i32 0
  %count174 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items, i32 0, i32 2
  %181 = load i32, i32* %count174, align 4, !tbaa !76
  %cmp175 = icmp ult i32 %178, %181
  br i1 %cmp175, label %for.body177, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond172
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup240

for.body177:                                      ; preds = %for.cond172
  %182 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %182) #4
  %183 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %meta178 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %183, i32 0, i32 1
  %184 = load %struct.avifMeta*, %struct.avifMeta** %meta178, align 4, !tbaa !75
  %items179 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %184, i32 0, i32 0
  %item180 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items179, i32 0, i32 0
  %185 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item180, align 4, !tbaa !81
  %186 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %arrayidx181 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %185, i32 %186
  store %struct.avifDecoderItem* %arrayidx181, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %187 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %187, i32 0, i32 4
  %188 = load i32, i32* %size, align 4, !tbaa !82
  %tobool182 = icmp ne i32 %188, 0
  br i1 %tobool182, label %if.end184, label %if.then183

if.then183:                                       ; preds = %for.body177
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup236

if.end184:                                        ; preds = %for.body177
  %189 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %hasUnsupportedEssentialProperty = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %189, i32 0, i32 12
  %190 = load i32, i32* %hasUnsupportedEssentialProperty, align 4, !tbaa !85
  %tobool185 = icmp ne i32 %190, 0
  br i1 %tobool185, label %if.then186, label %if.end187

if.then186:                                       ; preds = %if.end184
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup236

if.end187:                                        ; preds = %if.end184
  %191 = bitcast i32* %isGrid to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %191) #4
  %192 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %192, i32 0, i32 2
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call188 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0), i32 4)
  %cmp189 = icmp eq i32 %call188, 0
  %conv190 = zext i1 %cmp189 to i32
  store i32 %conv190, i32* %isGrid, align 4, !tbaa !12
  %193 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %type191 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %193, i32 0, i32 2
  %arraydecay192 = getelementptr inbounds [4 x i8], [4 x i8]* %type191, i32 0, i32 0
  %call193 = call i32 @memcmp(i8* %arraydecay192, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0), i32 4)
  %tobool194 = icmp ne i32 %call193, 0
  br i1 %tobool194, label %land.lhs.true, label %if.end197

land.lhs.true:                                    ; preds = %if.end187
  %194 = load i32, i32* %isGrid, align 4, !tbaa !12
  %tobool195 = icmp ne i32 %194, 0
  br i1 %tobool195, label %if.end197, label %if.then196

if.then196:                                       ; preds = %land.lhs.true
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

if.end197:                                        ; preds = %land.lhs.true, %if.end187
  %195 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %thumbnailForID = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %195, i32 0, i32 8
  %196 = load i32, i32* %thumbnailForID, align 4, !tbaa !86
  %cmp198 = icmp ne i32 %196, 0
  br i1 %cmp198, label %if.then200, label %if.end201

if.then200:                                       ; preds = %if.end197
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

if.end201:                                        ; preds = %if.end197
  %197 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %meta202 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %197, i32 0, i32 1
  %198 = load %struct.avifMeta*, %struct.avifMeta** %meta202, align 4, !tbaa !75
  %primaryItemID = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %198, i32 0, i32 4
  %199 = load i32, i32* %primaryItemID, align 4, !tbaa !87
  %cmp203 = icmp ugt i32 %199, 0
  br i1 %cmp203, label %land.lhs.true205, label %if.end212

land.lhs.true205:                                 ; preds = %if.end201
  %200 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %id206 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %200, i32 0, i32 0
  %201 = load i32, i32* %id206, align 4, !tbaa !88
  %202 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %meta207 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %202, i32 0, i32 1
  %203 = load %struct.avifMeta*, %struct.avifMeta** %meta207, align 4, !tbaa !75
  %primaryItemID208 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %203, i32 0, i32 4
  %204 = load i32, i32* %primaryItemID208, align 4, !tbaa !87
  %cmp209 = icmp ne i32 %201, %204
  br i1 %cmp209, label %if.then211, label %if.end212

if.then211:                                       ; preds = %land.lhs.true205
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

if.end212:                                        ; preds = %land.lhs.true205, %if.end201
  %205 = load i32, i32* %isGrid, align 4, !tbaa !12
  %tobool213 = icmp ne i32 %205, 0
  br i1 %tobool213, label %if.then214, label %if.else229

if.then214:                                       ; preds = %if.end212
  %206 = bitcast i8** %itemPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %206) #4
  %207 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %208 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %call215 = call i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %207, %struct.avifDecoderItem* %208)
  store i8* %call215, i8** %itemPtr, align 4, !tbaa !2
  %209 = load i8*, i8** %itemPtr, align 4, !tbaa !2
  %cmp216 = icmp eq i8* %209, null
  br i1 %cmp216, label %if.then218, label %if.end219

if.then218:                                       ; preds = %if.then214
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup226

if.end219:                                        ; preds = %if.then214
  %210 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %colorGrid220 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %210, i32 0, i32 7
  %211 = load i8*, i8** %itemPtr, align 4, !tbaa !2
  %212 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %size221 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %212, i32 0, i32 4
  %213 = load i32, i32* %size221, align 4, !tbaa !82
  %call222 = call i32 @avifParseImageGridBox(%struct.avifImageGrid* %colorGrid220, i8* %211, i32 %213)
  %tobool223 = icmp ne i32 %call222, 0
  br i1 %tobool223, label %if.end225, label %if.then224

if.then224:                                       ; preds = %if.end219
  store i32 18, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup226

if.end225:                                        ; preds = %if.end219
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup226

cleanup226:                                       ; preds = %if.end225, %if.then224, %if.then218
  %214 = bitcast i8** %itemPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  %cleanup.dest227 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest227, label %cleanup235 [
    i32 0, label %cleanup.cont228
  ]

cleanup.cont228:                                  ; preds = %cleanup226
  br label %if.end234

if.else229:                                       ; preds = %if.end212
  %215 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %216 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %call230 = call i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %215, %struct.avifDecoderItem* %216)
  %data231 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %colorOBU, i32 0, i32 0
  store i8* %call230, i8** %data231, align 4, !tbaa !6
  %217 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %size232 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %217, i32 0, i32 4
  %218 = load i32, i32* %size232, align 4, !tbaa !82
  %size233 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %colorOBU, i32 0, i32 1
  store i32 %218, i32* %size233, align 4, !tbaa !9
  br label %if.end234

if.end234:                                        ; preds = %if.else229, %cleanup.cont228
  %219 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  store %struct.avifDecoderItem* %219, %struct.avifDecoderItem** %colorOBUItem, align 4, !tbaa !2
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup235

cleanup235:                                       ; preds = %if.end234, %cleanup226, %if.then211, %if.then200, %if.then196
  %220 = bitcast i32* %isGrid to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  br label %cleanup236

cleanup236:                                       ; preds = %cleanup235, %if.then186, %if.then183
  %221 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #4
  %cleanup.dest237 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest237, label %cleanup240 [
    i32 10, label %for.inc238
  ]

for.inc238:                                       ; preds = %cleanup236
  %222 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %inc239 = add i32 %222, 1
  store i32 %inc239, i32* %itemIndex, align 4, !tbaa !12
  br label %for.cond172

cleanup240:                                       ; preds = %cleanup236, %for.cond.cleanup
  %223 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #4
  %cleanup.dest241 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest241, label %cleanup454 [
    i32 8, label %for.end242
  ]

for.end242:                                       ; preds = %cleanup240
  %224 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %colorOBUItem, align 4, !tbaa !2
  %tobool243 = icmp ne %struct.avifDecoderItem* %224, null
  br i1 %tobool243, label %if.end245, label %if.then244

if.then244:                                       ; preds = %for.end242
  store i32 10, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup454

if.end245:                                        ; preds = %for.end242
  %225 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %colorOBUItem, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %225, i32 0, i32 7
  store %struct.avifPropertyArray* %properties, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %226 = bitcast i32* %itemIndex246 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %226) #4
  store i32 0, i32* %itemIndex246, align 4, !tbaa !12
  br label %for.cond247

for.cond247:                                      ; preds = %for.inc329, %if.end245
  %227 = load i32, i32* %itemIndex246, align 4, !tbaa !12
  %228 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %meta248 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %228, i32 0, i32 1
  %229 = load %struct.avifMeta*, %struct.avifMeta** %meta248, align 4, !tbaa !75
  %items249 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %229, i32 0, i32 0
  %count250 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items249, i32 0, i32 2
  %230 = load i32, i32* %count250, align 4, !tbaa !76
  %cmp251 = icmp ult i32 %227, %230
  br i1 %cmp251, label %for.body254, label %for.cond.cleanup253

for.cond.cleanup253:                              ; preds = %for.cond247
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup331

for.body254:                                      ; preds = %for.cond247
  %231 = bitcast %struct.avifDecoderItem** %item255 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %231) #4
  %232 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %meta256 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %232, i32 0, i32 1
  %233 = load %struct.avifMeta*, %struct.avifMeta** %meta256, align 4, !tbaa !75
  %items257 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %233, i32 0, i32 0
  %item258 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items257, i32 0, i32 0
  %234 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item258, align 4, !tbaa !81
  %235 = load i32, i32* %itemIndex246, align 4, !tbaa !12
  %arrayidx259 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %234, i32 %235
  store %struct.avifDecoderItem* %arrayidx259, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %236 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %size260 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %236, i32 0, i32 4
  %237 = load i32, i32* %size260, align 4, !tbaa !82
  %tobool261 = icmp ne i32 %237, 0
  br i1 %tobool261, label %if.end263, label %if.then262

if.then262:                                       ; preds = %for.body254
  store i32 13, i32* %cleanup.dest.slot, align 4
  br label %cleanup326

if.end263:                                        ; preds = %for.body254
  %238 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %hasUnsupportedEssentialProperty264 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %238, i32 0, i32 12
  %239 = load i32, i32* %hasUnsupportedEssentialProperty264, align 4, !tbaa !85
  %tobool265 = icmp ne i32 %239, 0
  br i1 %tobool265, label %if.then266, label %if.end267

if.then266:                                       ; preds = %if.end263
  store i32 13, i32* %cleanup.dest.slot, align 4
  br label %cleanup326

if.end267:                                        ; preds = %if.end263
  %240 = bitcast i32* %isGrid268 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %240) #4
  %241 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %type269 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %241, i32 0, i32 2
  %arraydecay270 = getelementptr inbounds [4 x i8], [4 x i8]* %type269, i32 0, i32 0
  %call271 = call i32 @memcmp(i8* %arraydecay270, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i32 0, i32 0), i32 4)
  %cmp272 = icmp eq i32 %call271, 0
  %conv273 = zext i1 %cmp272 to i32
  store i32 %conv273, i32* %isGrid268, align 4, !tbaa !12
  %242 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %type274 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %242, i32 0, i32 2
  %arraydecay275 = getelementptr inbounds [4 x i8], [4 x i8]* %type274, i32 0, i32 0
  %call276 = call i32 @memcmp(i8* %arraydecay275, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0), i32 4)
  %tobool277 = icmp ne i32 %call276, 0
  br i1 %tobool277, label %land.lhs.true278, label %if.end281

land.lhs.true278:                                 ; preds = %if.end267
  %243 = load i32, i32* %isGrid268, align 4, !tbaa !12
  %tobool279 = icmp ne i32 %243, 0
  br i1 %tobool279, label %if.end281, label %if.then280

if.then280:                                       ; preds = %land.lhs.true278
  store i32 13, i32* %cleanup.dest.slot, align 4
  br label %cleanup325

if.end281:                                        ; preds = %land.lhs.true278, %if.end267
  %244 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %thumbnailForID282 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %244, i32 0, i32 8
  %245 = load i32, i32* %thumbnailForID282, align 4, !tbaa !86
  %cmp283 = icmp ne i32 %245, 0
  br i1 %cmp283, label %if.then285, label %if.end286

if.then285:                                       ; preds = %if.end281
  store i32 13, i32* %cleanup.dest.slot, align 4
  br label %cleanup325

if.end286:                                        ; preds = %if.end281
  %246 = bitcast %struct.avifProperty** %auxCProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %246) #4
  %247 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %properties287 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %247, i32 0, i32 7
  %call288 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %properties287, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0))
  store %struct.avifProperty* %call288, %struct.avifProperty** %auxCProp, align 4, !tbaa !2
  %248 = load %struct.avifProperty*, %struct.avifProperty** %auxCProp, align 4, !tbaa !2
  %tobool289 = icmp ne %struct.avifProperty* %248, null
  br i1 %tobool289, label %land.lhs.true290, label %if.end323

land.lhs.true290:                                 ; preds = %if.end286
  %249 = load %struct.avifProperty*, %struct.avifProperty** %auxCProp, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %249, i32 0, i32 1
  %auxC = bitcast %union.anon* %u to %struct.avifAuxiliaryType*
  %auxType = getelementptr inbounds %struct.avifAuxiliaryType, %struct.avifAuxiliaryType* %auxC, i32 0, i32 0
  %arraydecay291 = getelementptr inbounds [64 x i8], [64 x i8]* %auxType, i32 0, i32 0
  %call292 = call i32 @isAlphaURN(i8* %arraydecay291)
  %tobool293 = icmp ne i32 %call292, 0
  br i1 %tobool293, label %land.lhs.true294, label %if.end323

land.lhs.true294:                                 ; preds = %land.lhs.true290
  %250 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %auxForID295 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %250, i32 0, i32 9
  %251 = load i32, i32* %auxForID295, align 4, !tbaa !89
  %252 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %colorOBUItem, align 4, !tbaa !2
  %id296 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %252, i32 0, i32 0
  %253 = load i32, i32* %id296, align 4, !tbaa !88
  %cmp297 = icmp eq i32 %251, %253
  br i1 %cmp297, label %if.then299, label %if.end323

if.then299:                                       ; preds = %land.lhs.true294
  %254 = load i32, i32* %isGrid268, align 4, !tbaa !12
  %tobool300 = icmp ne i32 %254, 0
  br i1 %tobool300, label %if.then301, label %if.else317

if.then301:                                       ; preds = %if.then299
  %255 = bitcast i8** %itemPtr302 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %255) #4
  %256 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %257 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %call303 = call i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %256, %struct.avifDecoderItem* %257)
  store i8* %call303, i8** %itemPtr302, align 4, !tbaa !2
  %258 = load i8*, i8** %itemPtr302, align 4, !tbaa !2
  %cmp304 = icmp eq i8* %258, null
  br i1 %cmp304, label %if.then306, label %if.end307

if.then306:                                       ; preds = %if.then301
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup314

if.end307:                                        ; preds = %if.then301
  %259 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %alphaGrid308 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %259, i32 0, i32 8
  %260 = load i8*, i8** %itemPtr302, align 4, !tbaa !2
  %261 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %size309 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %261, i32 0, i32 4
  %262 = load i32, i32* %size309, align 4, !tbaa !82
  %call310 = call i32 @avifParseImageGridBox(%struct.avifImageGrid* %alphaGrid308, i8* %260, i32 %262)
  %tobool311 = icmp ne i32 %call310, 0
  br i1 %tobool311, label %if.end313, label %if.then312

if.then312:                                       ; preds = %if.end307
  store i32 18, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup314

if.end313:                                        ; preds = %if.end307
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup314

cleanup314:                                       ; preds = %if.end313, %if.then312, %if.then306
  %263 = bitcast i8** %itemPtr302 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #4
  %cleanup.dest315 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest315, label %cleanup324 [
    i32 0, label %cleanup.cont316
  ]

cleanup.cont316:                                  ; preds = %cleanup314
  br label %if.end322

if.else317:                                       ; preds = %if.then299
  %264 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %265 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %call318 = call i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %264, %struct.avifDecoderItem* %265)
  %data319 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %alphaOBU, i32 0, i32 0
  store i8* %call318, i8** %data319, align 4, !tbaa !6
  %266 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  %size320 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %266, i32 0, i32 4
  %267 = load i32, i32* %size320, align 4, !tbaa !82
  %size321 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %alphaOBU, i32 0, i32 1
  store i32 %267, i32* %size321, align 4, !tbaa !9
  br label %if.end322

if.end322:                                        ; preds = %if.else317, %cleanup.cont316
  %268 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item255, align 4, !tbaa !2
  store %struct.avifDecoderItem* %268, %struct.avifDecoderItem** %alphaOBUItem, align 4, !tbaa !2
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup324

if.end323:                                        ; preds = %land.lhs.true294, %land.lhs.true290, %if.end286
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup324

cleanup324:                                       ; preds = %if.end323, %if.end322, %cleanup314
  %269 = bitcast %struct.avifProperty** %auxCProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #4
  br label %cleanup325

cleanup325:                                       ; preds = %cleanup324, %if.then285, %if.then280
  %270 = bitcast i32* %isGrid268 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %270) #4
  br label %cleanup326

cleanup326:                                       ; preds = %cleanup325, %if.then266, %if.then262
  %271 = bitcast %struct.avifDecoderItem** %item255 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %271) #4
  %cleanup.dest327 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest327, label %cleanup331 [
    i32 0, label %cleanup.cont328
    i32 13, label %for.inc329
  ]

cleanup.cont328:                                  ; preds = %cleanup326
  br label %for.inc329

for.inc329:                                       ; preds = %cleanup.cont328, %cleanup326
  %272 = load i32, i32* %itemIndex246, align 4, !tbaa !12
  %inc330 = add i32 %272, 1
  store i32 %inc330, i32* %itemIndex246, align 4, !tbaa !12
  br label %for.cond247

cleanup331:                                       ; preds = %cleanup326, %for.cond.cleanup253
  %273 = bitcast i32* %itemIndex246 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #4
  %cleanup.dest332 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest332, label %cleanup454 [
    i32 11, label %for.end333
  ]

for.end333:                                       ; preds = %cleanup331
  %274 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %275 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %meta334 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %275, i32 0, i32 1
  %276 = load %struct.avifMeta*, %struct.avifMeta** %meta334, align 4, !tbaa !75
  %277 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image335 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %277, i32 0, i32 2
  %278 = load %struct.avifImage*, %struct.avifImage** %image335, align 8, !tbaa !24
  %279 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %colorOBUItem, align 4, !tbaa !2
  %id336 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %279, i32 0, i32 0
  %280 = load i32, i32* %id336, align 4, !tbaa !88
  %call337 = call i32 @avifDecoderDataFindMetadata(%struct.avifDecoderData* %274, %struct.avifMeta* %276, %struct.avifImage* %278, i32 %280)
  %tobool338 = icmp ne i32 %call337, 0
  br i1 %tobool338, label %if.end340, label %if.then339

if.then339:                                       ; preds = %for.end333
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup454

if.end340:                                        ; preds = %for.end333
  %281 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %colorGrid341 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %281, i32 0, i32 7
  %rows = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %colorGrid341, i32 0, i32 0
  %282 = load i8, i8* %rows, align 4, !tbaa !90
  %conv342 = zext i8 %282 to i32
  %cmp343 = icmp sgt i32 %conv342, 0
  br i1 %cmp343, label %land.lhs.true345, label %if.else358

land.lhs.true345:                                 ; preds = %if.end340
  %283 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %colorGrid346 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %283, i32 0, i32 7
  %columns = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %colorGrid346, i32 0, i32 1
  %284 = load i8, i8* %columns, align 1, !tbaa !91
  %conv347 = zext i8 %284 to i32
  %cmp348 = icmp sgt i32 %conv347, 0
  br i1 %cmp348, label %if.then350, label %if.else358

if.then350:                                       ; preds = %land.lhs.true345
  %285 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %286 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %colorGrid351 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %286, i32 0, i32 7
  %287 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %colorOBUItem, align 4, !tbaa !2
  %call352 = call i32 @avifDecoderDataGenerateImageGridTiles(%struct.avifDecoderData* %285, %struct.avifImageGrid* %colorGrid351, %struct.avifDecoderItem* %287, i32 0)
  %tobool353 = icmp ne i32 %call352, 0
  br i1 %tobool353, label %if.end355, label %if.then354

if.then354:                                       ; preds = %if.then350
  store i32 18, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup454

if.end355:                                        ; preds = %if.then350
  %288 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %288, i32 0, i32 4
  %count356 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles, i32 0, i32 2
  %289 = load i32, i32* %count356, align 4, !tbaa !92
  %290 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %colorTileCount357 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %290, i32 0, i32 5
  store i32 %289, i32* %colorTileCount357, align 4, !tbaa !51
  br label %if.end373

if.else358:                                       ; preds = %land.lhs.true345, %if.end340
  %size359 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %colorOBU, i32 0, i32 1
  %291 = load i32, i32* %size359, align 4, !tbaa !9
  %cmp360 = icmp eq i32 %291, 0
  br i1 %cmp360, label %if.then362, label %if.end363

if.then362:                                       ; preds = %if.else358
  store i32 10, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup454

if.end363:                                        ; preds = %if.else358
  %292 = bitcast %struct.avifTile** %colorTile364 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %292) #4
  %293 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data365 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %293, i32 0, i32 11
  %294 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data365, align 4, !tbaa !18
  %call366 = call %struct.avifTile* @avifDecoderDataCreateTile(%struct.avifDecoderData* %294)
  store %struct.avifTile* %call366, %struct.avifTile** %colorTile364, align 4, !tbaa !2
  %295 = bitcast %struct.avifDecodeSample** %colorSample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %295) #4
  %296 = load %struct.avifTile*, %struct.avifTile** %colorTile364, align 4, !tbaa !2
  %input367 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %296, i32 0, i32 0
  %297 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input367, align 4, !tbaa !49
  %samples368 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %297, i32 0, i32 0
  %298 = bitcast %struct.avifDecodeSampleArray* %samples368 to i8*
  %call369 = call i8* @avifArrayPushPtr(i8* %298)
  %299 = bitcast i8* %call369 to %struct.avifDecodeSample*
  store %struct.avifDecodeSample* %299, %struct.avifDecodeSample** %colorSample, align 4, !tbaa !2
  %300 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %colorSample, align 4, !tbaa !2
  %data370 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %300, i32 0, i32 0
  %301 = bitcast %struct.avifROData* %data370 to i8*
  %302 = bitcast %struct.avifROData* %colorOBU to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %301, i8* align 4 %302, i32 8, i1 false)
  %303 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %colorSample, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %303, i32 0, i32 1
  store i32 1, i32* %sync, align 4, !tbaa !93
  %304 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data371 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %304, i32 0, i32 11
  %305 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data371, align 4, !tbaa !18
  %colorTileCount372 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %305, i32 0, i32 5
  store i32 1, i32* %colorTileCount372, align 4, !tbaa !51
  %306 = bitcast %struct.avifDecodeSample** %colorSample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %306) #4
  %307 = bitcast %struct.avifTile** %colorTile364 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %307) #4
  br label %if.end373

if.end373:                                        ; preds = %if.end363, %if.end355
  %308 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %alphaGrid374 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %308, i32 0, i32 8
  %rows375 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %alphaGrid374, i32 0, i32 0
  %309 = load i8, i8* %rows375, align 4, !tbaa !95
  %conv376 = zext i8 %309 to i32
  %cmp377 = icmp sgt i32 %conv376, 0
  br i1 %cmp377, label %land.lhs.true379, label %if.else397

land.lhs.true379:                                 ; preds = %if.end373
  %310 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %alphaGrid380 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %310, i32 0, i32 8
  %columns381 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %alphaGrid380, i32 0, i32 1
  %311 = load i8, i8* %columns381, align 1, !tbaa !96
  %conv382 = zext i8 %311 to i32
  %cmp383 = icmp sgt i32 %conv382, 0
  br i1 %cmp383, label %land.lhs.true385, label %if.else397

land.lhs.true385:                                 ; preds = %land.lhs.true379
  %312 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %alphaOBUItem, align 4, !tbaa !2
  %tobool386 = icmp ne %struct.avifDecoderItem* %312, null
  br i1 %tobool386, label %if.then387, label %if.else397

if.then387:                                       ; preds = %land.lhs.true385
  %313 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %314 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %alphaGrid388 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %314, i32 0, i32 8
  %315 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %alphaOBUItem, align 4, !tbaa !2
  %call389 = call i32 @avifDecoderDataGenerateImageGridTiles(%struct.avifDecoderData* %313, %struct.avifImageGrid* %alphaGrid388, %struct.avifDecoderItem* %315, i32 0)
  %tobool390 = icmp ne i32 %call389, 0
  br i1 %tobool390, label %if.end392, label %if.then391

if.then391:                                       ; preds = %if.then387
  store i32 18, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup454

if.end392:                                        ; preds = %if.then387
  %316 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tiles393 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %316, i32 0, i32 4
  %count394 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles393, i32 0, i32 2
  %317 = load i32, i32* %count394, align 4, !tbaa !92
  %318 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %colorTileCount395 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %318, i32 0, i32 5
  %319 = load i32, i32* %colorTileCount395, align 4, !tbaa !51
  %sub = sub i32 %317, %319
  %320 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %alphaTileCount396 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %320, i32 0, i32 6
  store i32 %sub, i32* %alphaTileCount396, align 4, !tbaa !55
  br label %if.end415

if.else397:                                       ; preds = %land.lhs.true385, %land.lhs.true379, %if.end373
  %321 = bitcast %struct.avifTile** %alphaTile398 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %321) #4
  store %struct.avifTile* null, %struct.avifTile** %alphaTile398, align 4, !tbaa !2
  %size399 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %alphaOBU, i32 0, i32 1
  %322 = load i32, i32* %size399, align 4, !tbaa !9
  %cmp400 = icmp ugt i32 %322, 0
  br i1 %cmp400, label %if.then402, label %if.end414

if.then402:                                       ; preds = %if.else397
  %323 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data403 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %323, i32 0, i32 11
  %324 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data403, align 4, !tbaa !18
  %call404 = call %struct.avifTile* @avifDecoderDataCreateTile(%struct.avifDecoderData* %324)
  store %struct.avifTile* %call404, %struct.avifTile** %alphaTile398, align 4, !tbaa !2
  %325 = bitcast %struct.avifDecodeSample** %alphaSample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %325) #4
  %326 = load %struct.avifTile*, %struct.avifTile** %alphaTile398, align 4, !tbaa !2
  %input405 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %326, i32 0, i32 0
  %327 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input405, align 4, !tbaa !49
  %samples406 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %327, i32 0, i32 0
  %328 = bitcast %struct.avifDecodeSampleArray* %samples406 to i8*
  %call407 = call i8* @avifArrayPushPtr(i8* %328)
  %329 = bitcast i8* %call407 to %struct.avifDecodeSample*
  store %struct.avifDecodeSample* %329, %struct.avifDecodeSample** %alphaSample, align 4, !tbaa !2
  %330 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %alphaSample, align 4, !tbaa !2
  %data408 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %330, i32 0, i32 0
  %331 = bitcast %struct.avifROData* %data408 to i8*
  %332 = bitcast %struct.avifROData* %alphaOBU to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %331, i8* align 4 %332, i32 8, i1 false)
  %333 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %alphaSample, align 4, !tbaa !2
  %sync409 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %333, i32 0, i32 1
  store i32 1, i32* %sync409, align 4, !tbaa !93
  %334 = load %struct.avifTile*, %struct.avifTile** %alphaTile398, align 4, !tbaa !2
  %input410 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %334, i32 0, i32 0
  %335 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input410, align 4, !tbaa !49
  %alpha411 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %335, i32 0, i32 1
  store i32 1, i32* %alpha411, align 4, !tbaa !52
  %336 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data412 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %336, i32 0, i32 11
  %337 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data412, align 4, !tbaa !18
  %alphaTileCount413 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %337, i32 0, i32 6
  store i32 1, i32* %alphaTileCount413, align 4, !tbaa !55
  %338 = bitcast %struct.avifDecodeSample** %alphaSample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #4
  br label %if.end414

if.end414:                                        ; preds = %if.then402, %if.else397
  %339 = bitcast %struct.avifTile** %alphaTile398 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %339) #4
  br label %if.end415

if.end415:                                        ; preds = %if.end414, %if.end392
  %340 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex416 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %340, i32 0, i32 3
  store i32 -1, i32* %imageIndex416, align 4, !tbaa !56
  %341 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageCount417 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %341, i32 0, i32 4
  store i32 1, i32* %imageCount417, align 8, !tbaa !58
  %342 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageTiming418 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %342, i32 0, i32 5
  %timescale419 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %imageTiming418, i32 0, i32 0
  store i64 1, i64* %timescale419, align 8, !tbaa !97
  %343 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageTiming420 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %343, i32 0, i32 5
  %pts = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %imageTiming420, i32 0, i32 1
  store double 0.000000e+00, double* %pts, align 8, !tbaa !98
  %344 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageTiming421 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %344, i32 0, i32 5
  %ptsInTimescales = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %imageTiming421, i32 0, i32 2
  store i64 0, i64* %ptsInTimescales, align 8, !tbaa !99
  %345 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageTiming422 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %345, i32 0, i32 5
  %duration423 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %imageTiming422, i32 0, i32 3
  store double 1.000000e+00, double* %duration423, align 8, !tbaa !100
  %346 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageTiming424 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %346, i32 0, i32 5
  %durationInTimescales425 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %imageTiming424, i32 0, i32 4
  store i64 1, i64* %durationInTimescales425, align 8, !tbaa !101
  %347 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %timescale426 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %347, i32 0, i32 6
  store i64 1, i64* %timescale426, align 8, !tbaa !60
  %348 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %duration427 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %348, i32 0, i32 7
  store double 1.000000e+00, double* %duration427, align 8, !tbaa !63
  %349 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %durationInTimescales428 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %349, i32 0, i32 8
  store i64 1, i64* %durationInTimescales428, align 8, !tbaa !62
  %size429 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %colorOBU, i32 0, i32 1
  %350 = load i32, i32* %size429, align 4, !tbaa !9
  %351 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %ioStats430 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %351, i32 0, i32 10
  %colorOBUSize = getelementptr inbounds %struct.avifIOStats, %struct.avifIOStats* %ioStats430, i32 0, i32 0
  store i32 %350, i32* %colorOBUSize, align 4, !tbaa !102
  %size431 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %alphaOBU, i32 0, i32 1
  %352 = load i32, i32* %size431, align 4, !tbaa !9
  %353 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %ioStats432 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %353, i32 0, i32 10
  %alphaOBUSize = getelementptr inbounds %struct.avifIOStats, %struct.avifIOStats* %ioStats432, i32 0, i32 1
  store i32 %352, i32* %alphaOBUSize, align 4, !tbaa !103
  %354 = bitcast %struct.avifProperty** %ispeProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %354) #4
  %355 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %call433 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %355, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0))
  store %struct.avifProperty* %call433, %struct.avifProperty** %ispeProp, align 4, !tbaa !2
  %356 = load %struct.avifProperty*, %struct.avifProperty** %ispeProp, align 4, !tbaa !2
  %tobool434 = icmp ne %struct.avifProperty* %356, null
  br i1 %tobool434, label %if.then435, label %if.else445

if.then435:                                       ; preds = %if.end415
  %357 = load %struct.avifProperty*, %struct.avifProperty** %ispeProp, align 4, !tbaa !2
  %u436 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %357, i32 0, i32 1
  %ispe = bitcast %union.anon* %u436 to %struct.avifImageSpatialExtents*
  %width437 = getelementptr inbounds %struct.avifImageSpatialExtents, %struct.avifImageSpatialExtents* %ispe, i32 0, i32 0
  %358 = load i32, i32* %width437, align 4, !tbaa !25
  %359 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image438 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %359, i32 0, i32 2
  %360 = load %struct.avifImage*, %struct.avifImage** %image438, align 8, !tbaa !24
  %width439 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %360, i32 0, i32 0
  store i32 %358, i32* %width439, align 4, !tbaa !65
  %361 = load %struct.avifProperty*, %struct.avifProperty** %ispeProp, align 4, !tbaa !2
  %u440 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %361, i32 0, i32 1
  %ispe441 = bitcast %union.anon* %u440 to %struct.avifImageSpatialExtents*
  %height442 = getelementptr inbounds %struct.avifImageSpatialExtents, %struct.avifImageSpatialExtents* %ispe441, i32 0, i32 1
  %362 = load i32, i32* %height442, align 4, !tbaa !25
  %363 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image443 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %363, i32 0, i32 2
  %364 = load %struct.avifImage*, %struct.avifImage** %image443, align 8, !tbaa !24
  %height444 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %364, i32 0, i32 1
  store i32 %362, i32* %height444, align 4, !tbaa !73
  br label %if.end450

if.else445:                                       ; preds = %if.end415
  %365 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image446 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %365, i32 0, i32 2
  %366 = load %struct.avifImage*, %struct.avifImage** %image446, align 8, !tbaa !24
  %width447 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %366, i32 0, i32 0
  store i32 0, i32* %width447, align 4, !tbaa !65
  %367 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image448 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %367, i32 0, i32 2
  %368 = load %struct.avifImage*, %struct.avifImage** %image448, align 8, !tbaa !24
  %height449 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %368, i32 0, i32 1
  store i32 0, i32* %height449, align 4, !tbaa !73
  br label %if.end450

if.end450:                                        ; preds = %if.else445, %if.then435
  %369 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %alphaOBUItem, align 4, !tbaa !2
  %cmp451 = icmp ne %struct.avifDecoderItem* %369, null
  %conv452 = zext i1 %cmp451 to i32
  %370 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %alphaPresent453 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %370, i32 0, i32 9
  store i32 %conv452, i32* %alphaPresent453, align 8, !tbaa !74
  %371 = bitcast %struct.avifProperty** %ispeProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %371) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup454

cleanup454:                                       ; preds = %if.end450, %if.then391, %if.then362, %if.then354, %if.then339, %cleanup331, %if.then244, %cleanup240
  %372 = bitcast %struct.avifDecoderItem** %alphaOBUItem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %372) #4
  %373 = bitcast %struct.avifDecoderItem** %colorOBUItem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %373) #4
  %374 = bitcast %struct.avifROData* %alphaOBU to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %374) #4
  %375 = bitcast %struct.avifROData* %colorOBU to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %375) #4
  %cleanup.dest458 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest458, label %cleanup675 [
    i32 0, label %cleanup.cont459
  ]

cleanup.cont459:                                  ; preds = %cleanup454
  br label %if.end460

if.end460:                                        ; preds = %cleanup.cont459, %cleanup.cont170
  %376 = bitcast i32* %tileIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %376) #4
  store i32 0, i32* %tileIndex, align 4, !tbaa !12
  br label %for.cond461

for.cond461:                                      ; preds = %for.inc502, %if.end460
  %377 = load i32, i32* %tileIndex, align 4, !tbaa !12
  %378 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tiles462 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %378, i32 0, i32 4
  %count463 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles462, i32 0, i32 2
  %379 = load i32, i32* %count463, align 4, !tbaa !92
  %cmp464 = icmp ult i32 %377, %379
  br i1 %cmp464, label %for.body467, label %for.cond.cleanup466

for.cond.cleanup466:                              ; preds = %for.cond461
  store i32 14, i32* %cleanup.dest.slot, align 4
  br label %cleanup504

for.body467:                                      ; preds = %for.cond461
  %380 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %380) #4
  %381 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tiles468 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %381, i32 0, i32 4
  %tile469 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles468, i32 0, i32 0
  %382 = load %struct.avifTile*, %struct.avifTile** %tile469, align 4, !tbaa !104
  %383 = load i32, i32* %tileIndex, align 4, !tbaa !12
  %arrayidx470 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %382, i32 %383
  store %struct.avifTile* %arrayidx470, %struct.avifTile** %tile, align 4, !tbaa !2
  %384 = bitcast i32* %sampleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %384) #4
  store i32 0, i32* %sampleIndex, align 4, !tbaa !12
  br label %for.cond471

for.cond471:                                      ; preds = %for.inc494, %for.body467
  %385 = load i32, i32* %sampleIndex, align 4, !tbaa !12
  %386 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input472 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %386, i32 0, i32 0
  %387 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input472, align 4, !tbaa !49
  %samples473 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %387, i32 0, i32 0
  %count474 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples473, i32 0, i32 2
  %388 = load i32, i32* %count474, align 4, !tbaa !57
  %cmp475 = icmp ult i32 %385, %388
  br i1 %cmp475, label %for.body478, label %for.cond.cleanup477

for.cond.cleanup477:                              ; preds = %for.cond471
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup496

for.body478:                                      ; preds = %for.cond471
  %389 = bitcast %struct.avifDecodeSample** %sample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %389) #4
  %390 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input479 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %390, i32 0, i32 0
  %391 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input479, align 4, !tbaa !49
  %samples480 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %391, i32 0, i32 0
  %sample481 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples480, i32 0, i32 0
  %392 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample481, align 4, !tbaa !105
  %393 = load i32, i32* %sampleIndex, align 4, !tbaa !12
  %arrayidx482 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %392, i32 %393
  store %struct.avifDecodeSample* %arrayidx482, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %394 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %data483 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %394, i32 0, i32 0
  %data484 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data483, i32 0, i32 0
  %395 = load i8*, i8** %data484, align 4, !tbaa !106
  %tobool485 = icmp ne i8* %395, null
  br i1 %tobool485, label %lor.lhs.false, label %if.then489

lor.lhs.false:                                    ; preds = %for.body478
  %396 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %data486 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %396, i32 0, i32 0
  %size487 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data486, i32 0, i32 1
  %397 = load i32, i32* %size487, align 4, !tbaa !107
  %tobool488 = icmp ne i32 %397, 0
  br i1 %tobool488, label %if.end490, label %if.then489

if.then489:                                       ; preds = %lor.lhs.false, %for.body478
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup491

if.end490:                                        ; preds = %lor.lhs.false
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup491

cleanup491:                                       ; preds = %if.end490, %if.then489
  %398 = bitcast %struct.avifDecodeSample** %sample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %398) #4
  %cleanup.dest492 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest492, label %cleanup496 [
    i32 0, label %cleanup.cont493
  ]

cleanup.cont493:                                  ; preds = %cleanup491
  br label %for.inc494

for.inc494:                                       ; preds = %cleanup.cont493
  %399 = load i32, i32* %sampleIndex, align 4, !tbaa !12
  %inc495 = add i32 %399, 1
  store i32 %inc495, i32* %sampleIndex, align 4, !tbaa !12
  br label %for.cond471

cleanup496:                                       ; preds = %cleanup491, %for.cond.cleanup477
  %400 = bitcast i32* %sampleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %400) #4
  %cleanup.dest497 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest497, label %cleanup499 [
    i32 17, label %for.end498
  ]

for.end498:                                       ; preds = %cleanup496
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup499

cleanup499:                                       ; preds = %for.end498, %cleanup496
  %401 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %401) #4
  %cleanup.dest500 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest500, label %cleanup504 [
    i32 0, label %cleanup.cont501
  ]

cleanup.cont501:                                  ; preds = %cleanup499
  br label %for.inc502

for.inc502:                                       ; preds = %cleanup.cont501
  %402 = load i32, i32* %tileIndex, align 4, !tbaa !12
  %inc503 = add i32 %402, 1
  store i32 %inc503, i32* %tileIndex, align 4, !tbaa !12
  br label %for.cond461

cleanup504:                                       ; preds = %cleanup499, %for.cond.cleanup466
  %403 = bitcast i32* %tileIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %403) #4
  %cleanup.dest505 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest505, label %cleanup675 [
    i32 14, label %for.end506
  ]

for.end506:                                       ; preds = %cleanup504
  %404 = bitcast %struct.avifProperty** %colrProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %404) #4
  %405 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %call507 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %405, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0))
  store %struct.avifProperty* %call507, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %406 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %tobool508 = icmp ne %struct.avifProperty* %406, null
  br i1 %tobool508, label %if.then509, label %if.end541

if.then509:                                       ; preds = %for.end506
  %407 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %u510 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %407, i32 0, i32 1
  %colr = bitcast %union.anon* %u510 to %struct.avifColourInformationBox*
  %hasICC = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %colr, i32 0, i32 0
  %408 = load i32, i32* %hasICC, align 4, !tbaa !25
  %tobool511 = icmp ne i32 %408, 0
  br i1 %tobool511, label %if.then512, label %if.else518

if.then512:                                       ; preds = %if.then509
  %409 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image513 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %409, i32 0, i32 2
  %410 = load %struct.avifImage*, %struct.avifImage** %image513, align 8, !tbaa !24
  %411 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %u514 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %411, i32 0, i32 1
  %colr515 = bitcast %union.anon* %u514 to %struct.avifColourInformationBox*
  %icc = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %colr515, i32 0, i32 1
  %412 = load i8*, i8** %icc, align 4, !tbaa !25
  %413 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %u516 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %413, i32 0, i32 1
  %colr517 = bitcast %union.anon* %u516 to %struct.avifColourInformationBox*
  %iccSize = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %colr517, i32 0, i32 2
  %414 = load i32, i32* %iccSize, align 4, !tbaa !25
  call void @avifImageSetProfileICC(%struct.avifImage* %410, i8* %412, i32 %414)
  br label %if.end540

if.else518:                                       ; preds = %if.then509
  %415 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %u519 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %415, i32 0, i32 1
  %colr520 = bitcast %union.anon* %u519 to %struct.avifColourInformationBox*
  %hasNCLX = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %colr520, i32 0, i32 3
  %416 = load i32, i32* %hasNCLX, align 4, !tbaa !25
  %tobool521 = icmp ne i32 %416, 0
  br i1 %tobool521, label %if.then522, label %if.end539

if.then522:                                       ; preds = %if.else518
  %417 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %cicpSet523 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %417, i32 0, i32 11
  store i32 1, i32* %cicpSet523, align 4, !tbaa !27
  %418 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %u524 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %418, i32 0, i32 1
  %colr525 = bitcast %union.anon* %u524 to %struct.avifColourInformationBox*
  %colorPrimaries = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %colr525, i32 0, i32 4
  %419 = load i32, i32* %colorPrimaries, align 4, !tbaa !25
  %420 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image526 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %420, i32 0, i32 2
  %421 = load %struct.avifImage*, %struct.avifImage** %image526, align 8, !tbaa !24
  %colorPrimaries527 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %421, i32 0, i32 14
  store i32 %419, i32* %colorPrimaries527, align 4, !tbaa !108
  %422 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %u528 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %422, i32 0, i32 1
  %colr529 = bitcast %union.anon* %u528 to %struct.avifColourInformationBox*
  %transferCharacteristics = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %colr529, i32 0, i32 5
  %423 = load i32, i32* %transferCharacteristics, align 4, !tbaa !25
  %424 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image530 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %424, i32 0, i32 2
  %425 = load %struct.avifImage*, %struct.avifImage** %image530, align 8, !tbaa !24
  %transferCharacteristics531 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %425, i32 0, i32 15
  store i32 %423, i32* %transferCharacteristics531, align 4, !tbaa !109
  %426 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %u532 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %426, i32 0, i32 1
  %colr533 = bitcast %union.anon* %u532 to %struct.avifColourInformationBox*
  %matrixCoefficients = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %colr533, i32 0, i32 6
  %427 = load i32, i32* %matrixCoefficients, align 4, !tbaa !25
  %428 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image534 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %428, i32 0, i32 2
  %429 = load %struct.avifImage*, %struct.avifImage** %image534, align 8, !tbaa !24
  %matrixCoefficients535 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %429, i32 0, i32 16
  store i32 %427, i32* %matrixCoefficients535, align 4, !tbaa !110
  %430 = load %struct.avifProperty*, %struct.avifProperty** %colrProp, align 4, !tbaa !2
  %u536 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %430, i32 0, i32 1
  %colr537 = bitcast %union.anon* %u536 to %struct.avifColourInformationBox*
  %range = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %colr537, i32 0, i32 7
  %431 = load i32, i32* %range, align 4, !tbaa !25
  %432 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image538 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %432, i32 0, i32 2
  %433 = load %struct.avifImage*, %struct.avifImage** %image538, align 8, !tbaa !24
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %433, i32 0, i32 4
  store i32 %431, i32* %yuvRange, align 4, !tbaa !111
  br label %if.end539

if.end539:                                        ; preds = %if.then522, %if.else518
  br label %if.end540

if.end540:                                        ; preds = %if.end539, %if.then512
  br label %if.end541

if.end541:                                        ; preds = %if.end540, %for.end506
  %434 = bitcast %struct.avifProperty** %paspProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %434) #4
  %435 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %call542 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %435, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.6, i32 0, i32 0))
  store %struct.avifProperty* %call542, %struct.avifProperty** %paspProp, align 4, !tbaa !2
  %436 = load %struct.avifProperty*, %struct.avifProperty** %paspProp, align 4, !tbaa !2
  %tobool543 = icmp ne %struct.avifProperty* %436, null
  br i1 %tobool543, label %if.then544, label %if.end549

if.then544:                                       ; preds = %if.end541
  %437 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image545 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %437, i32 0, i32 2
  %438 = load %struct.avifImage*, %struct.avifImage** %image545, align 8, !tbaa !24
  %transformFlags = getelementptr inbounds %struct.avifImage, %struct.avifImage* %438, i32 0, i32 17
  %439 = load i32, i32* %transformFlags, align 4, !tbaa !112
  %or = or i32 %439, 1
  store i32 %or, i32* %transformFlags, align 4, !tbaa !112
  %440 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image546 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %440, i32 0, i32 2
  %441 = load %struct.avifImage*, %struct.avifImage** %image546, align 8, !tbaa !24
  %pasp = getelementptr inbounds %struct.avifImage, %struct.avifImage* %441, i32 0, i32 18
  %442 = bitcast %struct.avifPixelAspectRatioBox* %pasp to i8*
  %443 = load %struct.avifProperty*, %struct.avifProperty** %paspProp, align 4, !tbaa !2
  %u547 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %443, i32 0, i32 1
  %pasp548 = bitcast %union.anon* %u547 to %struct.avifPixelAspectRatioBox*
  %444 = bitcast %struct.avifPixelAspectRatioBox* %pasp548 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %442, i8* align 4 %444, i32 8, i1 false)
  br label %if.end549

if.end549:                                        ; preds = %if.then544, %if.end541
  %445 = bitcast %struct.avifProperty** %clapProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %445) #4
  %446 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %call550 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %446, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.7, i32 0, i32 0))
  store %struct.avifProperty* %call550, %struct.avifProperty** %clapProp, align 4, !tbaa !2
  %447 = load %struct.avifProperty*, %struct.avifProperty** %clapProp, align 4, !tbaa !2
  %tobool551 = icmp ne %struct.avifProperty* %447, null
  br i1 %tobool551, label %if.then552, label %if.end559

if.then552:                                       ; preds = %if.end549
  %448 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image553 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %448, i32 0, i32 2
  %449 = load %struct.avifImage*, %struct.avifImage** %image553, align 8, !tbaa !24
  %transformFlags554 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %449, i32 0, i32 17
  %450 = load i32, i32* %transformFlags554, align 4, !tbaa !112
  %or555 = or i32 %450, 2
  store i32 %or555, i32* %transformFlags554, align 4, !tbaa !112
  %451 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image556 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %451, i32 0, i32 2
  %452 = load %struct.avifImage*, %struct.avifImage** %image556, align 8, !tbaa !24
  %clap = getelementptr inbounds %struct.avifImage, %struct.avifImage* %452, i32 0, i32 19
  %453 = bitcast %struct.avifCleanApertureBox* %clap to i8*
  %454 = load %struct.avifProperty*, %struct.avifProperty** %clapProp, align 4, !tbaa !2
  %u557 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %454, i32 0, i32 1
  %clap558 = bitcast %union.anon* %u557 to %struct.avifCleanApertureBox*
  %455 = bitcast %struct.avifCleanApertureBox* %clap558 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %453, i8* align 4 %455, i32 32, i1 false)
  br label %if.end559

if.end559:                                        ; preds = %if.then552, %if.end549
  %456 = bitcast %struct.avifProperty** %irotProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %456) #4
  %457 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %call560 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %457, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i32 0, i32 0))
  store %struct.avifProperty* %call560, %struct.avifProperty** %irotProp, align 4, !tbaa !2
  %458 = load %struct.avifProperty*, %struct.avifProperty** %irotProp, align 4, !tbaa !2
  %tobool561 = icmp ne %struct.avifProperty* %458, null
  br i1 %tobool561, label %if.then562, label %if.end569

if.then562:                                       ; preds = %if.end559
  %459 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image563 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %459, i32 0, i32 2
  %460 = load %struct.avifImage*, %struct.avifImage** %image563, align 8, !tbaa !24
  %transformFlags564 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %460, i32 0, i32 17
  %461 = load i32, i32* %transformFlags564, align 4, !tbaa !112
  %or565 = or i32 %461, 4
  store i32 %or565, i32* %transformFlags564, align 4, !tbaa !112
  %462 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image566 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %462, i32 0, i32 2
  %463 = load %struct.avifImage*, %struct.avifImage** %image566, align 8, !tbaa !24
  %irot = getelementptr inbounds %struct.avifImage, %struct.avifImage* %463, i32 0, i32 20
  %464 = bitcast %struct.avifImageRotation* %irot to i8*
  %465 = load %struct.avifProperty*, %struct.avifProperty** %irotProp, align 4, !tbaa !2
  %u567 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %465, i32 0, i32 1
  %irot568 = bitcast %union.anon* %u567 to %struct.avifImageRotation*
  %466 = bitcast %struct.avifImageRotation* %irot568 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %464, i8* align 4 %466, i32 1, i1 false)
  br label %if.end569

if.end569:                                        ; preds = %if.then562, %if.end559
  %467 = bitcast %struct.avifProperty** %imirProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %467) #4
  %468 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %call570 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %468, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0))
  store %struct.avifProperty* %call570, %struct.avifProperty** %imirProp, align 4, !tbaa !2
  %469 = load %struct.avifProperty*, %struct.avifProperty** %imirProp, align 4, !tbaa !2
  %tobool571 = icmp ne %struct.avifProperty* %469, null
  br i1 %tobool571, label %if.then572, label %if.end579

if.then572:                                       ; preds = %if.end569
  %470 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image573 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %470, i32 0, i32 2
  %471 = load %struct.avifImage*, %struct.avifImage** %image573, align 8, !tbaa !24
  %transformFlags574 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %471, i32 0, i32 17
  %472 = load i32, i32* %transformFlags574, align 4, !tbaa !112
  %or575 = or i32 %472, 8
  store i32 %or575, i32* %transformFlags574, align 4, !tbaa !112
  %473 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image576 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %473, i32 0, i32 2
  %474 = load %struct.avifImage*, %struct.avifImage** %image576, align 8, !tbaa !24
  %imir = getelementptr inbounds %struct.avifImage, %struct.avifImage* %474, i32 0, i32 21
  %475 = bitcast %struct.avifImageMirror* %imir to i8*
  %476 = load %struct.avifProperty*, %struct.avifProperty** %imirProp, align 4, !tbaa !2
  %u577 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %476, i32 0, i32 1
  %imir578 = bitcast %union.anon* %u577 to %struct.avifImageMirror*
  %477 = bitcast %struct.avifImageMirror* %imir578 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %475, i8* align 4 %477, i32 1, i1 false)
  br label %if.end579

if.end579:                                        ; preds = %if.then572, %if.end569
  %478 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data580 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %478, i32 0, i32 11
  %479 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data580, align 4, !tbaa !18
  %cicpSet581 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %479, i32 0, i32 11
  %480 = load i32, i32* %cicpSet581, align 4, !tbaa !27
  %tobool582 = icmp ne i32 %480, 0
  br i1 %tobool582, label %if.end623, label %land.lhs.true583

land.lhs.true583:                                 ; preds = %if.end579
  %481 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tiles584 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %481, i32 0, i32 4
  %count585 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles584, i32 0, i32 2
  %482 = load i32, i32* %count585, align 4, !tbaa !92
  %cmp586 = icmp ugt i32 %482, 0
  br i1 %cmp586, label %if.then588, label %if.end623

if.then588:                                       ; preds = %land.lhs.true583
  %483 = bitcast %struct.avifTile** %firstTile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %483) #4
  %484 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tiles589 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %484, i32 0, i32 4
  %tile590 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles589, i32 0, i32 0
  %485 = load %struct.avifTile*, %struct.avifTile** %tile590, align 4, !tbaa !104
  %arrayidx591 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %485, i32 0
  store %struct.avifTile* %arrayidx591, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %486 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %input592 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %486, i32 0, i32 0
  %487 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input592, align 4, !tbaa !49
  %samples593 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %487, i32 0, i32 0
  %count594 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples593, i32 0, i32 2
  %488 = load i32, i32* %count594, align 4, !tbaa !57
  %cmp595 = icmp ugt i32 %488, 0
  br i1 %cmp595, label %if.then597, label %if.end622

if.then597:                                       ; preds = %if.then588
  %489 = bitcast %struct.avifDecodeSample** %sample598 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %489) #4
  %490 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %input599 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %490, i32 0, i32 0
  %491 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input599, align 4, !tbaa !49
  %samples600 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %491, i32 0, i32 0
  %sample601 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples600, i32 0, i32 0
  %492 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample601, align 4, !tbaa !105
  %arrayidx602 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %492, i32 0
  store %struct.avifDecodeSample* %arrayidx602, %struct.avifDecodeSample** %sample598, align 4, !tbaa !2
  %493 = bitcast %struct.avifSequenceHeader* %sequenceHeader to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %493) #4
  %494 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample598, align 4, !tbaa !2
  %data603 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %494, i32 0, i32 0
  %call604 = call i32 @avifSequenceHeaderParse(%struct.avifSequenceHeader* %sequenceHeader, %struct.avifROData* %data603)
  %tobool605 = icmp ne i32 %call604, 0
  br i1 %tobool605, label %if.then606, label %if.end621

if.then606:                                       ; preds = %if.then597
  %495 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data607 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %495, i32 0, i32 11
  %496 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data607, align 4, !tbaa !18
  %cicpSet608 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %496, i32 0, i32 11
  store i32 1, i32* %cicpSet608, align 4, !tbaa !27
  %colorPrimaries609 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %sequenceHeader, i32 0, i32 5
  %497 = load i32, i32* %colorPrimaries609, align 4, !tbaa !113
  %498 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image610 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %498, i32 0, i32 2
  %499 = load %struct.avifImage*, %struct.avifImage** %image610, align 8, !tbaa !24
  %colorPrimaries611 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %499, i32 0, i32 14
  store i32 %497, i32* %colorPrimaries611, align 4, !tbaa !108
  %transferCharacteristics612 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %sequenceHeader, i32 0, i32 6
  %500 = load i32, i32* %transferCharacteristics612, align 4, !tbaa !115
  %501 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image613 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %501, i32 0, i32 2
  %502 = load %struct.avifImage*, %struct.avifImage** %image613, align 8, !tbaa !24
  %transferCharacteristics614 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %502, i32 0, i32 15
  store i32 %500, i32* %transferCharacteristics614, align 4, !tbaa !109
  %matrixCoefficients615 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %sequenceHeader, i32 0, i32 7
  %503 = load i32, i32* %matrixCoefficients615, align 4, !tbaa !116
  %504 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image616 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %504, i32 0, i32 2
  %505 = load %struct.avifImage*, %struct.avifImage** %image616, align 8, !tbaa !24
  %matrixCoefficients617 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %505, i32 0, i32 16
  store i32 %503, i32* %matrixCoefficients617, align 4, !tbaa !110
  %range618 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %sequenceHeader, i32 0, i32 8
  %506 = load i32, i32* %range618, align 4, !tbaa !117
  %507 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image619 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %507, i32 0, i32 2
  %508 = load %struct.avifImage*, %struct.avifImage** %image619, align 8, !tbaa !24
  %yuvRange620 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %508, i32 0, i32 4
  store i32 %506, i32* %yuvRange620, align 4, !tbaa !111
  br label %if.end621

if.end621:                                        ; preds = %if.then606, %if.then597
  %509 = bitcast %struct.avifSequenceHeader* %sequenceHeader to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %509) #4
  %510 = bitcast %struct.avifDecodeSample** %sample598 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %510) #4
  br label %if.end622

if.end622:                                        ; preds = %if.end621, %if.then588
  %511 = bitcast %struct.avifTile** %firstTile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %511) #4
  br label %if.end623

if.end623:                                        ; preds = %if.end622, %land.lhs.true583, %if.end579
  %512 = bitcast %struct.avifProperty** %av1CProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %512) #4
  %513 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %colorProperties, align 4, !tbaa !2
  %call624 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %513, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i32 0, i32 0))
  store %struct.avifProperty* %call624, %struct.avifProperty** %av1CProp, align 4, !tbaa !2
  %514 = load %struct.avifProperty*, %struct.avifProperty** %av1CProp, align 4, !tbaa !2
  %tobool625 = icmp ne %struct.avifProperty* %514, null
  br i1 %tobool625, label %if.then626, label %if.else666

if.then626:                                       ; preds = %if.end623
  %515 = load %struct.avifProperty*, %struct.avifProperty** %av1CProp, align 4, !tbaa !2
  %u627 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %515, i32 0, i32 1
  %av1C = bitcast %union.anon* %u627 to %struct.avifCodecConfigurationBox*
  %call628 = call i32 @avifCodecConfigurationBoxGetDepth(%struct.avifCodecConfigurationBox* %av1C)
  %516 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image629 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %516, i32 0, i32 2
  %517 = load %struct.avifImage*, %struct.avifImage** %image629, align 8, !tbaa !24
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %517, i32 0, i32 2
  store i32 %call628, i32* %depth, align 4, !tbaa !118
  %518 = load %struct.avifProperty*, %struct.avifProperty** %av1CProp, align 4, !tbaa !2
  %u630 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %518, i32 0, i32 1
  %av1C631 = bitcast %union.anon* %u630 to %struct.avifCodecConfigurationBox*
  %monochrome = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %av1C631, i32 0, i32 5
  %519 = load i8, i8* %monochrome, align 1, !tbaa !25
  %tobool632 = icmp ne i8 %519, 0
  br i1 %tobool632, label %if.then633, label %if.else635

if.then633:                                       ; preds = %if.then626
  %520 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image634 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %520, i32 0, i32 2
  %521 = load %struct.avifImage*, %struct.avifImage** %image634, align 8, !tbaa !24
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %521, i32 0, i32 3
  store i32 4, i32* %yuvFormat, align 4, !tbaa !119
  br label %if.end661

if.else635:                                       ; preds = %if.then626
  %522 = load %struct.avifProperty*, %struct.avifProperty** %av1CProp, align 4, !tbaa !2
  %u636 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %522, i32 0, i32 1
  %av1C637 = bitcast %union.anon* %u636 to %struct.avifCodecConfigurationBox*
  %chromaSubsamplingX = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %av1C637, i32 0, i32 6
  %523 = load i8, i8* %chromaSubsamplingX, align 2, !tbaa !25
  %conv638 = zext i8 %523 to i32
  %tobool639 = icmp ne i32 %conv638, 0
  br i1 %tobool639, label %land.lhs.true640, label %if.else648

land.lhs.true640:                                 ; preds = %if.else635
  %524 = load %struct.avifProperty*, %struct.avifProperty** %av1CProp, align 4, !tbaa !2
  %u641 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %524, i32 0, i32 1
  %av1C642 = bitcast %union.anon* %u641 to %struct.avifCodecConfigurationBox*
  %chromaSubsamplingY = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %av1C642, i32 0, i32 7
  %525 = load i8, i8* %chromaSubsamplingY, align 1, !tbaa !25
  %conv643 = zext i8 %525 to i32
  %tobool644 = icmp ne i32 %conv643, 0
  br i1 %tobool644, label %if.then645, label %if.else648

if.then645:                                       ; preds = %land.lhs.true640
  %526 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image646 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %526, i32 0, i32 2
  %527 = load %struct.avifImage*, %struct.avifImage** %image646, align 8, !tbaa !24
  %yuvFormat647 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %527, i32 0, i32 3
  store i32 3, i32* %yuvFormat647, align 4, !tbaa !119
  br label %if.end660

if.else648:                                       ; preds = %land.lhs.true640, %if.else635
  %528 = load %struct.avifProperty*, %struct.avifProperty** %av1CProp, align 4, !tbaa !2
  %u649 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %528, i32 0, i32 1
  %av1C650 = bitcast %union.anon* %u649 to %struct.avifCodecConfigurationBox*
  %chromaSubsamplingX651 = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %av1C650, i32 0, i32 6
  %529 = load i8, i8* %chromaSubsamplingX651, align 2, !tbaa !25
  %tobool652 = icmp ne i8 %529, 0
  br i1 %tobool652, label %if.then653, label %if.else656

if.then653:                                       ; preds = %if.else648
  %530 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image654 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %530, i32 0, i32 2
  %531 = load %struct.avifImage*, %struct.avifImage** %image654, align 8, !tbaa !24
  %yuvFormat655 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %531, i32 0, i32 3
  store i32 2, i32* %yuvFormat655, align 4, !tbaa !119
  br label %if.end659

if.else656:                                       ; preds = %if.else648
  %532 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image657 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %532, i32 0, i32 2
  %533 = load %struct.avifImage*, %struct.avifImage** %image657, align 8, !tbaa !24
  %yuvFormat658 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %533, i32 0, i32 3
  store i32 1, i32* %yuvFormat658, align 4, !tbaa !119
  br label %if.end659

if.end659:                                        ; preds = %if.else656, %if.then653
  br label %if.end660

if.end660:                                        ; preds = %if.end659, %if.then645
  br label %if.end661

if.end661:                                        ; preds = %if.end660, %if.then633
  %534 = load %struct.avifProperty*, %struct.avifProperty** %av1CProp, align 4, !tbaa !2
  %u662 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %534, i32 0, i32 1
  %av1C663 = bitcast %union.anon* %u662 to %struct.avifCodecConfigurationBox*
  %chromaSamplePosition = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %av1C663, i32 0, i32 8
  %535 = load i8, i8* %chromaSamplePosition, align 4, !tbaa !25
  %conv664 = zext i8 %535 to i32
  %536 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image665 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %536, i32 0, i32 2
  %537 = load %struct.avifImage*, %struct.avifImage** %image665, align 8, !tbaa !24
  %yuvChromaSamplePosition = getelementptr inbounds %struct.avifImage, %struct.avifImage* %537, i32 0, i32 5
  store i32 %conv664, i32* %yuvChromaSamplePosition, align 4, !tbaa !120
  br label %if.end667

if.else666:                                       ; preds = %if.end623
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup669

if.end667:                                        ; preds = %if.end661
  %538 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %call668 = call i32 @avifDecoderFlush(%struct.avifDecoder* %538)
  store i32 %call668, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup669

cleanup669:                                       ; preds = %if.end667, %if.else666
  %539 = bitcast %struct.avifProperty** %av1CProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %539) #4
  %540 = bitcast %struct.avifProperty** %imirProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %540) #4
  %541 = bitcast %struct.avifProperty** %irotProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %541) #4
  %542 = bitcast %struct.avifProperty** %clapProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %542) #4
  %543 = bitcast %struct.avifProperty** %paspProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %543) #4
  %544 = bitcast %struct.avifProperty** %colrProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %544) #4
  br label %cleanup675

cleanup675:                                       ; preds = %cleanup669, %cleanup504, %cleanup454, %cleanup166
  %545 = bitcast %struct.avifPropertyArray** %colorProperties to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %545) #4
  br label %cleanup676

cleanup676:                                       ; preds = %cleanup675, %if.then
  %546 = bitcast %struct.avifDecoderData** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %546) #4
  %547 = load i32, i32* %retval, align 4
  ret i32 %547

unreachable:                                      ; preds = %cleanup104, %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @avifDecoderParse(%struct.avifDecoder* %decoder, %struct.avifROData* %rawInput) #0 {
entry:
  %retval = alloca i32, align 4
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %rawInput.addr = alloca %struct.avifROData*, align 4
  %avifCompatible = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %itemIndex = alloca i32, align 4
  %item = alloca %struct.avifDecoderItem*, align 4
  %p = alloca i8*, align 4
  %trackIndex = alloca i32, align 4
  %track = alloca %struct.avifTrack*, align 4
  %chunkIndex = alloca i32, align 4
  %chunk = alloca %struct.avifSampleTableChunk*, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  store %struct.avifROData* %rawInput, %struct.avifROData** %rawInput.addr, align 4, !tbaa !2
  %0 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  call void @avifDecoderCleanup(%struct.avifDecoder* %0)
  %call = call %struct.avifDecoderData* @avifDecoderDataCreate()
  %1 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %1, i32 0, i32 11
  store %struct.avifDecoderData* %call, %struct.avifDecoderData** %data, align 4, !tbaa !18
  %2 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %2, i32 0, i32 11
  %3 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data1, align 4, !tbaa !18
  %rawInput2 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %3, i32 0, i32 3
  %4 = bitcast %struct.avifROData* %rawInput2 to i8*
  %5 = load %struct.avifROData*, %struct.avifROData** %rawInput.addr, align 4, !tbaa !2
  %6 = bitcast %struct.avifROData* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %6, i32 8, i1 false)
  %7 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data3 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %7, i32 0, i32 11
  %8 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data3, align 4, !tbaa !18
  %9 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data4 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %9, i32 0, i32 11
  %10 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data4, align 4, !tbaa !18
  %rawInput5 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %10, i32 0, i32 3
  %data6 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %rawInput5, i32 0, i32 0
  %11 = load i8*, i8** %data6, align 4, !tbaa !121
  %12 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data7 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %12, i32 0, i32 11
  %13 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data7, align 4, !tbaa !18
  %rawInput8 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %13, i32 0, i32 3
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %rawInput8, i32 0, i32 1
  %14 = load i32, i32* %size, align 4, !tbaa !122
  %call9 = call i32 @avifParse(%struct.avifDecoderData* %8, i8* %11, i32 %14)
  %tobool = icmp ne i32 %call9, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 9, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %15 = bitcast i32* %avifCompatible to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data10 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %16, i32 0, i32 11
  %17 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data10, align 4, !tbaa !18
  %ftyp = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %17, i32 0, i32 0
  %call11 = call i32 @avifFileTypeIsCompatible(%struct.avifFileType* %ftyp)
  store i32 %call11, i32* %avifCompatible, align 4, !tbaa !12
  %18 = load i32, i32* %avifCompatible, align 4, !tbaa !12
  %tobool12 = icmp ne i32 %18, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.end
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup78

if.end14:                                         ; preds = %if.end
  %19 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  store i32 0, i32* %itemIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end14
  %20 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %21 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data15 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %21, i32 0, i32 11
  %22 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data15, align 4, !tbaa !18
  %meta = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %22, i32 0, i32 1
  %23 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !75
  %items = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %23, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items, i32 0, i32 2
  %24 = load i32, i32* %count, align 4, !tbaa !76
  %cmp = icmp ult i32 %20, %24
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

for.body:                                         ; preds = %for.cond
  %25 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  %26 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data16 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %26, i32 0, i32 11
  %27 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data16, align 4, !tbaa !18
  %meta17 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %27, i32 0, i32 1
  %28 = load %struct.avifMeta*, %struct.avifMeta** %meta17, align 4, !tbaa !75
  %items18 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %28, i32 0, i32 0
  %item19 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items18, i32 0, i32 0
  %29 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item19, align 4, !tbaa !81
  %30 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %29, i32 %30
  store %struct.avifDecoderItem* %arrayidx, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %31 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %hasUnsupportedEssentialProperty = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %31, i32 0, i32 12
  %32 = load i32, i32* %hasUnsupportedEssentialProperty, align 4, !tbaa !85
  %tobool20 = icmp ne i32 %32, 0
  br i1 %tobool20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

if.end22:                                         ; preds = %for.body
  %33 = bitcast i8** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data23 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %34, i32 0, i32 11
  %35 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data23, align 4, !tbaa !18
  %36 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %call24 = call i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %35, %struct.avifDecoderItem* %36)
  store i8* %call24, i8** %p, align 4, !tbaa !2
  %37 = load i8*, i8** %p, align 4, !tbaa !2
  %cmp25 = icmp eq i8* %37, null
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.end22
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %if.end22
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end27, %if.then26
  %38 = bitcast i8** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  br label %cleanup28

cleanup28:                                        ; preds = %cleanup, %if.then21
  %39 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup29 [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup28
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup28
  %40 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %inc = add i32 %40, 1
  store i32 %inc, i32* %itemIndex, align 4, !tbaa !12
  br label %for.cond

cleanup29:                                        ; preds = %cleanup28, %for.cond.cleanup
  %41 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %cleanup.dest30 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest30, label %cleanup78 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup29
  %42 = bitcast i32* %trackIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  store i32 0, i32* %trackIndex, align 4, !tbaa !12
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc72, %for.end
  %43 = load i32, i32* %trackIndex, align 4, !tbaa !12
  %44 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data32 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %44, i32 0, i32 11
  %45 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data32, align 4, !tbaa !18
  %tracks = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %45, i32 0, i32 2
  %count33 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks, i32 0, i32 2
  %46 = load i32, i32* %count33, align 4, !tbaa !33
  %cmp34 = icmp ult i32 %43, %46
  br i1 %cmp34, label %for.body36, label %for.cond.cleanup35

for.cond.cleanup35:                               ; preds = %for.cond31
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup74

for.body36:                                       ; preds = %for.cond31
  %47 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #4
  %48 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data37 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %48, i32 0, i32 11
  %49 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data37, align 4, !tbaa !18
  %tracks38 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %49, i32 0, i32 2
  %track39 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks38, i32 0, i32 0
  %50 = load %struct.avifTrack*, %struct.avifTrack** %track39, align 4, !tbaa !35
  %51 = load i32, i32* %trackIndex, align 4, !tbaa !12
  %arrayidx40 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %50, i32 %51
  store %struct.avifTrack* %arrayidx40, %struct.avifTrack** %track, align 4, !tbaa !2
  %52 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %sampleTable = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %52, i32 0, i32 6
  %53 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 8, !tbaa !36
  %tobool41 = icmp ne %struct.avifSampleTable* %53, null
  br i1 %tobool41, label %if.end43, label %if.then42

if.then42:                                        ; preds = %for.body36
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup69

if.end43:                                         ; preds = %for.body36
  %54 = bitcast i32* %chunkIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #4
  store i32 0, i32* %chunkIndex, align 4, !tbaa !12
  br label %for.cond44

for.cond44:                                       ; preds = %for.inc64, %if.end43
  %55 = load i32, i32* %chunkIndex, align 4, !tbaa !12
  %56 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %sampleTable45 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %56, i32 0, i32 6
  %57 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable45, align 8, !tbaa !36
  %chunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %57, i32 0, i32 0
  %count46 = getelementptr inbounds %struct.avifSampleTableChunkArray, %struct.avifSampleTableChunkArray* %chunks, i32 0, i32 2
  %58 = load i32, i32* %count46, align 4, !tbaa !39
  %cmp47 = icmp ult i32 %55, %58
  br i1 %cmp47, label %for.body49, label %for.cond.cleanup48

for.cond.cleanup48:                               ; preds = %for.cond44
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup66

for.body49:                                       ; preds = %for.cond44
  %59 = bitcast %struct.avifSampleTableChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #4
  %60 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %sampleTable50 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %60, i32 0, i32 6
  %61 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable50, align 8, !tbaa !36
  %chunks51 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %61, i32 0, i32 0
  %chunk52 = getelementptr inbounds %struct.avifSampleTableChunkArray, %struct.avifSampleTableChunkArray* %chunks51, i32 0, i32 0
  %62 = load %struct.avifSampleTableChunk*, %struct.avifSampleTableChunk** %chunk52, align 4, !tbaa !123
  %63 = load i32, i32* %chunkIndex, align 4, !tbaa !12
  %arrayidx53 = getelementptr inbounds %struct.avifSampleTableChunk, %struct.avifSampleTableChunk* %62, i32 %63
  store %struct.avifSampleTableChunk* %arrayidx53, %struct.avifSampleTableChunk** %chunk, align 4, !tbaa !2
  %64 = load %struct.avifSampleTableChunk*, %struct.avifSampleTableChunk** %chunk, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifSampleTableChunk, %struct.avifSampleTableChunk* %64, i32 0, i32 0
  %65 = load i64, i64* %offset, align 8, !tbaa !124
  %66 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data54 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %66, i32 0, i32 11
  %67 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data54, align 4, !tbaa !18
  %rawInput55 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %67, i32 0, i32 3
  %size56 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %rawInput55, i32 0, i32 1
  %68 = load i32, i32* %size56, align 4, !tbaa !122
  %conv = zext i32 %68 to i64
  %cmp57 = icmp ugt i64 %65, %conv
  br i1 %cmp57, label %if.then59, label %if.end60

if.then59:                                        ; preds = %for.body49
  store i32 9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end60:                                         ; preds = %for.body49
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

cleanup61:                                        ; preds = %if.end60, %if.then59
  %69 = bitcast %struct.avifSampleTableChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %cleanup.dest62 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest62, label %cleanup66 [
    i32 0, label %cleanup.cont63
  ]

cleanup.cont63:                                   ; preds = %cleanup61
  br label %for.inc64

for.inc64:                                        ; preds = %cleanup.cont63
  %70 = load i32, i32* %chunkIndex, align 4, !tbaa !12
  %inc65 = add i32 %70, 1
  store i32 %inc65, i32* %chunkIndex, align 4, !tbaa !12
  br label %for.cond44

cleanup66:                                        ; preds = %cleanup61, %for.cond.cleanup48
  %71 = bitcast i32* %chunkIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %cleanup.dest67 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest67, label %cleanup69 [
    i32 8, label %for.end68
  ]

for.end68:                                        ; preds = %cleanup66
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup69

cleanup69:                                        ; preds = %for.end68, %cleanup66, %if.then42
  %72 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  %cleanup.dest70 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest70, label %cleanup74 [
    i32 0, label %cleanup.cont71
    i32 7, label %for.inc72
  ]

cleanup.cont71:                                   ; preds = %cleanup69
  br label %for.inc72

for.inc72:                                        ; preds = %cleanup.cont71, %cleanup69
  %73 = load i32, i32* %trackIndex, align 4, !tbaa !12
  %inc73 = add i32 %73, 1
  store i32 %inc73, i32* %trackIndex, align 4, !tbaa !12
  br label %for.cond31

cleanup74:                                        ; preds = %cleanup69, %for.cond.cleanup35
  %74 = bitcast i32* %trackIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #4
  %cleanup.dest75 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest75, label %cleanup78 [
    i32 5, label %for.end76
  ]

for.end76:                                        ; preds = %cleanup74
  %75 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %call77 = call i32 @avifDecoderReset(%struct.avifDecoder* %75)
  store i32 %call77, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup78

cleanup78:                                        ; preds = %for.end76, %cleanup74, %cleanup29, %if.then13
  %76 = bitcast i32* %avifCompatible to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  br label %return

return:                                           ; preds = %cleanup78, %if.then
  %77 = load i32, i32* %retval, align 4
  ret i32 %77
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal i32 @avifParse(%struct.avifDecoderData* %data, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data1, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %call = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  br label %do.body

do.body:                                          ; preds = %while.body
  %call2 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call4 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), i32 4)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.else, label %if.then6

if.then6:                                         ; preds = %do.end
  br label %do.body7

do.body7:                                         ; preds = %if.then6
  %5 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %ftyp = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %5, i32 0, i32 0
  %call8 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size9 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %6 = load i32, i32* %size9, align 4, !tbaa !10
  %call10 = call i32 @avifParseFileTypeBox(%struct.avifFileType* %ftyp, i8* %call8, i32 %6)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %do.body7
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %do.body7
  br label %do.cond14

do.cond14:                                        ; preds = %if.end13
  br label %do.end15

do.end15:                                         ; preds = %do.cond14
  br label %if.end47

if.else:                                          ; preds = %do.end
  %type16 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay17 = getelementptr inbounds [4 x i8], [4 x i8]* %type16, i32 0, i32 0
  %call18 = call i32 @memcmp(i8* %arraydecay17, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.13, i32 0, i32 0), i32 4)
  %tobool19 = icmp ne i32 %call18, 0
  br i1 %tobool19, label %if.else30, label %if.then20

if.then20:                                        ; preds = %if.else
  br label %do.body21

do.body21:                                        ; preds = %if.then20
  %7 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %meta = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %7, i32 0, i32 1
  %8 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !75
  %call22 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size23 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %9 = load i32, i32* %size23, align 4, !tbaa !10
  %call24 = call i32 @avifParseMetaBox(%struct.avifMeta* %8, i8* %call22, i32 %9)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %do.body21
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %do.body21
  br label %do.cond28

do.cond28:                                        ; preds = %if.end27
  br label %do.end29

do.end29:                                         ; preds = %do.cond28
  br label %if.end46

if.else30:                                        ; preds = %if.else
  %type31 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay32 = getelementptr inbounds [4 x i8], [4 x i8]* %type31, i32 0, i32 0
  %call33 = call i32 @memcmp(i8* %arraydecay32, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.14, i32 0, i32 0), i32 4)
  %tobool34 = icmp ne i32 %call33, 0
  br i1 %tobool34, label %if.end45, label %if.then35

if.then35:                                        ; preds = %if.else30
  br label %do.body36

do.body36:                                        ; preds = %if.then35
  %10 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %call37 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size38 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %11 = load i32, i32* %size38, align 4, !tbaa !10
  %call39 = call i32 @avifParseMoovBox(%struct.avifDecoderData* %10, i8* %call37, i32 %11)
  %tobool40 = icmp ne i32 %call39, 0
  br i1 %tobool40, label %if.end42, label %if.then41

if.then41:                                        ; preds = %do.body36
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end42:                                         ; preds = %do.body36
  br label %do.cond43

do.cond43:                                        ; preds = %if.end42
  br label %do.end44

do.end44:                                         ; preds = %do.cond43
  br label %if.end45

if.end45:                                         ; preds = %do.end44, %if.else30
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %do.end29
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %do.end15
  br label %do.body48

do.body48:                                        ; preds = %if.end47
  %size49 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %12 = load i32, i32* %size49, align 4, !tbaa !10
  %call50 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %12)
  %tobool51 = icmp ne i32 %call50, 0
  br i1 %tobool51, label %if.end53, label %if.then52

if.then52:                                        ; preds = %do.body48
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %do.body48
  br label %do.cond54

do.cond54:                                        ; preds = %if.end53
  br label %do.end55

do.end55:                                         ; preds = %do.cond54
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end55, %if.then52, %if.then41, %if.then26, %if.then12, %if.then
  %13 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup56 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

cleanup56:                                        ; preds = %while.end, %cleanup
  %14 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %14) #4
  %15 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #4
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: nounwind
define internal i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %data, %struct.avifDecoderItem* %item) #0 {
entry:
  %retval = alloca i8*, align 4
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %item.addr = alloca %struct.avifDecoderItem*, align 4
  %offsetBuffer = alloca %struct.avifROData*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %offsetSize = alloca i64, align 8
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  store %struct.avifDecoderItem* %item, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifROData** %offsetBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store %struct.avifROData* null, %struct.avifROData** %offsetBuffer, align 4, !tbaa !2
  %1 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %idatID = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %1, i32 0, i32 5
  %2 = load i32, i32* %idatID, align 4, !tbaa !126
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %rawInput = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %3, i32 0, i32 3
  store %struct.avifROData* %rawInput, %struct.avifROData** %offsetBuffer, align 4, !tbaa !2
  br label %if.end15

if.else:                                          ; preds = %entry
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %5 = load i32, i32* %i, align 4, !tbaa !12
  %6 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %meta = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %6, i32 0, i32 1
  %7 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !127
  %idats = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %7, i32 0, i32 2
  %count = getelementptr inbounds %struct.avifDecoderItemDataArray, %struct.avifDecoderItemDataArray* %idats, i32 0, i32 2
  %8 = load i32, i32* %count, align 4, !tbaa !128
  %cmp1 = icmp ult i32 %5, %8
  br i1 %cmp1, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %9 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %meta2 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %9, i32 0, i32 1
  %10 = load %struct.avifMeta*, %struct.avifMeta** %meta2, align 4, !tbaa !127
  %idats3 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %10, i32 0, i32 2
  %idat = getelementptr inbounds %struct.avifDecoderItemDataArray, %struct.avifDecoderItemDataArray* %idats3, i32 0, i32 0
  %11 = load %struct.avifDecoderItemData*, %struct.avifDecoderItemData** %idat, align 4, !tbaa !129
  %12 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %11, i32 %12
  %id = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %arrayidx, i32 0, i32 0
  %13 = load i32, i32* %id, align 4, !tbaa !130
  %14 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %idatID4 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %14, i32 0, i32 5
  %15 = load i32, i32* %idatID4, align 4, !tbaa !126
  %cmp5 = icmp eq i32 %13, %15
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %for.body
  %16 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %meta7 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %16, i32 0, i32 1
  %17 = load %struct.avifMeta*, %struct.avifMeta** %meta7, align 4, !tbaa !127
  %idats8 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %17, i32 0, i32 2
  %idat9 = getelementptr inbounds %struct.avifDecoderItemDataArray, %struct.avifDecoderItemDataArray* %idats8, i32 0, i32 0
  %18 = load %struct.avifDecoderItemData*, %struct.avifDecoderItemData** %idat9, align 4, !tbaa !129
  %19 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx10 = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %18, i32 %19
  %data11 = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %arrayidx10, i32 0, i32 1
  store %struct.avifROData* %data11, %struct.avifROData** %offsetBuffer, align 4, !tbaa !2
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %20 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup:                                          ; preds = %if.then6, %for.cond.cleanup
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  br label %for.end

for.end:                                          ; preds = %cleanup
  %22 = load %struct.avifROData*, %struct.avifROData** %offsetBuffer, align 4, !tbaa !2
  %cmp12 = icmp eq %struct.avifROData* %22, null
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %for.end
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup31

if.end14:                                         ; preds = %for.end
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.then
  %23 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %23, i32 0, i32 3
  %24 = load i32, i32* %offset, align 4, !tbaa !132
  %25 = load %struct.avifROData*, %struct.avifROData** %offsetBuffer, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %25, i32 0, i32 1
  %26 = load i32, i32* %size, align 4, !tbaa !9
  %cmp16 = icmp ugt i32 %24, %26
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup31

if.end18:                                         ; preds = %if.end15
  %27 = bitcast i64* %offsetSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %27) #4
  %28 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %offset19 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %28, i32 0, i32 3
  %29 = load i32, i32* %offset19, align 4, !tbaa !132
  %conv = zext i32 %29 to i64
  %30 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %size20 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %30, i32 0, i32 4
  %31 = load i32, i32* %size20, align 4, !tbaa !82
  %conv21 = zext i32 %31 to i64
  %add = add i64 %conv, %conv21
  store i64 %add, i64* %offsetSize, align 8, !tbaa !133
  %32 = load i64, i64* %offsetSize, align 8, !tbaa !133
  %33 = load %struct.avifROData*, %struct.avifROData** %offsetBuffer, align 4, !tbaa !2
  %size22 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %33, i32 0, i32 1
  %34 = load i32, i32* %size22, align 4, !tbaa !9
  %conv23 = zext i32 %34 to i64
  %cmp24 = icmp ugt i64 %32, %conv23
  br i1 %cmp24, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.end18
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

if.end27:                                         ; preds = %if.end18
  %35 = load %struct.avifROData*, %struct.avifROData** %offsetBuffer, align 4, !tbaa !2
  %data28 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %35, i32 0, i32 0
  %36 = load i8*, i8** %data28, align 4, !tbaa !6
  %37 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item.addr, align 4, !tbaa !2
  %offset29 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %37, i32 0, i32 3
  %38 = load i32, i32* %offset29, align 4, !tbaa !132
  %add.ptr = getelementptr inbounds i8, i8* %36, i32 %38
  store i8* %add.ptr, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

cleanup30:                                        ; preds = %if.end27, %if.then26
  %39 = bitcast i64* %offsetSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %39) #4
  br label %cleanup31

cleanup31:                                        ; preds = %cleanup30, %if.then17, %if.then13
  %40 = bitcast %struct.avifROData** %offsetBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = load i8*, i8** %retval, align 4
  ret i8* %41
}

; Function Attrs: nounwind
define internal void @avifDecoderDataClearTiles(%struct.avifDecoderData* %data) #0 {
entry:
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %i = alloca i32, align 4
  %tile = alloca %struct.avifTile*, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !12
  %2 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %2, i32 0, i32 4
  %count = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !92
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles1 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %6, i32 0, i32 4
  %tile2 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles1, i32 0, i32 0
  %7 = load %struct.avifTile*, %struct.avifTile** %tile2, align 4, !tbaa !104
  %8 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifTile, %struct.avifTile* %7, i32 %8
  store %struct.avifTile* %arrayidx, %struct.avifTile** %tile, align 4, !tbaa !2
  %9 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input = getelementptr inbounds %struct.avifTile, %struct.avifTile* %9, i32 0, i32 0
  %10 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input, align 4, !tbaa !49
  %tobool = icmp ne %struct.avifCodecDecodeInput* %10, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %11 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input3 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %11, i32 0, i32 0
  %12 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input3, align 4, !tbaa !49
  call void @avifCodecDecodeInputDestroy(%struct.avifCodecDecodeInput* %12)
  %13 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input4 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %13, i32 0, i32 0
  store %struct.avifCodecDecodeInput* null, %struct.avifCodecDecodeInput** %input4, align 4, !tbaa !49
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %14 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec = getelementptr inbounds %struct.avifTile, %struct.avifTile* %14, i32 0, i32 1
  %15 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !134
  %tobool5 = icmp ne %struct.avifCodec* %15, null
  br i1 %tobool5, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end
  %16 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec7 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %16, i32 0, i32 1
  %17 = load %struct.avifCodec*, %struct.avifCodec** %codec7, align 4, !tbaa !134
  call void @avifCodecDestroy(%struct.avifCodec* %17)
  %18 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec8 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %18, i32 0, i32 1
  store %struct.avifCodec* null, %struct.avifCodec** %codec8, align 4, !tbaa !134
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end
  %19 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %image = getelementptr inbounds %struct.avifTile, %struct.avifTile* %19, i32 0, i32 2
  %20 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !135
  %tobool10 = icmp ne %struct.avifImage* %20, null
  br i1 %tobool10, label %if.then11, label %if.end14

if.then11:                                        ; preds = %if.end9
  %21 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %image12 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %21, i32 0, i32 2
  %22 = load %struct.avifImage*, %struct.avifImage** %image12, align 4, !tbaa !135
  call void @avifImageDestroy(%struct.avifImage* %22)
  %23 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %image13 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %23, i32 0, i32 2
  store %struct.avifImage* null, %struct.avifImage** %image13, align 4, !tbaa !135
  br label %if.end14

if.end14:                                         ; preds = %if.then11, %if.end9
  %24 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %25 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %25, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %26 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles15 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %26, i32 0, i32 4
  %count16 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles15, i32 0, i32 2
  store i32 0, i32* %count16, align 4, !tbaa !92
  %27 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %colorTileCount = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %27, i32 0, i32 5
  store i32 0, i32* %colorTileCount, align 4, !tbaa !51
  %28 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %alphaTileCount = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %28, i32 0, i32 6
  store i32 0, i32* %alphaTileCount, align 4, !tbaa !55
  ret void
}

declare void @avifImageDestroy(%struct.avifImage*) #2

declare %struct.avifImage* @avifImageCreateEmpty() #2

; Function Attrs: nounwind
define internal i32 @avifSampleTableHasFormat(%struct.avifSampleTable* %sampleTable, i8* %format) #0 {
entry:
  %retval = alloca i32, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %format.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store i8* %format, i8** %format.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !12
  %2 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleDescriptions = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %2, i32 0, i32 1
  %count = getelementptr inbounds %struct.avifSampleDescriptionArray, %struct.avifSampleDescriptionArray* %sampleDescriptions, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !136
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %4 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleDescriptions1 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %4, i32 0, i32 1
  %description = getelementptr inbounds %struct.avifSampleDescriptionArray, %struct.avifSampleDescriptionArray* %sampleDescriptions1, i32 0, i32 0
  %5 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description, align 4, !tbaa !137
  %6 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %5, i32 %6
  %format2 = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %arrayidx, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %format2, i32 0, i32 0
  %7 = load i8*, i8** %format.addr, align 4, !tbaa !2
  %call = call i32 @memcmp(i8* %arraydecay, i8* %7, i32 4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup
  %10 = load i32, i32* %retval, align 4
  ret i32 %10

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal %struct.avifPropertyArray* @avifSampleTableGetProperties(%struct.avifSampleTable* %sampleTable) #0 {
entry:
  %retval = alloca %struct.avifPropertyArray*, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %description = alloca %struct.avifSampleDescription*, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !12
  %2 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleDescriptions = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %2, i32 0, i32 1
  %count = getelementptr inbounds %struct.avifSampleDescriptionArray, %struct.avifSampleDescriptionArray* %sampleDescriptions, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !136
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup3

for.body:                                         ; preds = %for.cond
  %4 = bitcast %struct.avifSampleDescription** %description to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleDescriptions1 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %5, i32 0, i32 1
  %description2 = getelementptr inbounds %struct.avifSampleDescriptionArray, %struct.avifSampleDescriptionArray* %sampleDescriptions1, i32 0, i32 0
  %6 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description2, align 4, !tbaa !137
  %7 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %6, i32 %7
  store %struct.avifSampleDescription* %arrayidx, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %8 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %format = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %8, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %format, i32 0, i32 0
  %call = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0), i32 4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  %9 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %9, i32 0, i32 1
  store %struct.avifPropertyArray* %properties, %struct.avifPropertyArray** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %10 = bitcast %struct.avifSampleDescription** %description to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup3 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %11 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup3:                                         ; preds = %cleanup, %for.cond.cleanup
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  %cleanup.dest4 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest4, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup3
  store %struct.avifPropertyArray* null, %struct.avifPropertyArray** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup3
  %13 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %retval, align 4
  ret %struct.avifPropertyArray* %13

unreachable:                                      ; preds = %cleanup3
  unreachable
}

; Function Attrs: nounwind
define internal i32 @avifDecoderDataFindMetadata(%struct.avifDecoderData* %data, %struct.avifMeta* %meta, %struct.avifImage* %image, i32 %colorId) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %colorId.addr = alloca i32, align 4
  %exifData = alloca %struct.avifROData, align 4
  %xmpData = alloca %struct.avifROData, align 4
  %itemIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %item = alloca %struct.avifDecoderItem*, align 4
  %boxPtr = alloca i8*, align 4
  %exifBoxStream = alloca %struct.avifROStream, align 4
  %exifBoxStream_roData = alloca %struct.avifROData, align 4
  %exifTiffHeaderOffset = alloca i32, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store i32 %colorId, i32* %colorId.addr, align 4, !tbaa !12
  %0 = bitcast %struct.avifROData* %exifData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %exifData to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 8, i1 false)
  %2 = bitcast %struct.avifROData* %xmpData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #4
  %3 = bitcast %struct.avifROData* %xmpData to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 8, i1 false)
  %4 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 0, i32* %itemIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %6 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %6, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items, i32 0, i32 2
  %7 = load i32, i32* %count, align 4, !tbaa !76
  %cmp = icmp ult i32 %5, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

for.body:                                         ; preds = %for.cond
  %8 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items1 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %9, i32 0, i32 0
  %item2 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items1, i32 0, i32 0
  %10 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item2, align 4, !tbaa !81
  %11 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %10, i32 %11
  store %struct.avifDecoderItem* %arrayidx, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %12 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %12, i32 0, i32 4
  %13 = load i32, i32* %size, align 4, !tbaa !82
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

if.end:                                           ; preds = %for.body
  %14 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %hasUnsupportedEssentialProperty = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %14, i32 0, i32 12
  %15 = load i32, i32* %hasUnsupportedEssentialProperty, align 4, !tbaa !85
  %tobool3 = icmp ne i32 %15, 0
  br i1 %tobool3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

if.end5:                                          ; preds = %if.end
  %16 = load i32, i32* %colorId.addr, align 4, !tbaa !12
  %cmp6 = icmp ugt i32 %16, 0
  br i1 %cmp6, label %land.lhs.true, label %if.end9

land.lhs.true:                                    ; preds = %if.end5
  %17 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %descForID = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %17, i32 0, i32 10
  %18 = load i32, i32* %descForID, align 4, !tbaa !138
  %19 = load i32, i32* %colorId.addr, align 4, !tbaa !12
  %cmp7 = icmp ne i32 %18, %19
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %land.lhs.true
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

if.end9:                                          ; preds = %land.lhs.true, %if.end5
  %20 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %20, i32 0, i32 2
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.47, i32 0, i32 0), i32 4)
  %tobool10 = icmp ne i32 %call, 0
  br i1 %tobool10, label %if.else, label %if.then11

if.then11:                                        ; preds = %if.end9
  %21 = bitcast i8** %boxPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %23 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %call12 = call i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %22, %struct.avifDecoderItem* %23)
  store i8* %call12, i8** %boxPtr, align 4, !tbaa !2
  %24 = bitcast %struct.avifROStream* %exifBoxStream to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %24) #4
  %25 = bitcast %struct.avifROData* %exifBoxStream_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %25) #4
  %26 = load i8*, i8** %boxPtr, align 4, !tbaa !2
  %data13 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %exifBoxStream_roData, i32 0, i32 0
  store i8* %26, i8** %data13, align 4, !tbaa !6
  %27 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %size14 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %27, i32 0, i32 4
  %28 = load i32, i32* %size14, align 4, !tbaa !82
  %size15 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %exifBoxStream_roData, i32 0, i32 1
  store i32 %28, i32* %size15, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %exifBoxStream, %struct.avifROData* %exifBoxStream_roData)
  %29 = bitcast i32* %exifTiffHeaderOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  br label %do.body

do.body:                                          ; preds = %if.then11
  %call16 = call i32 @avifROStreamReadU32(%struct.avifROStream* %exifBoxStream, i32* %exifTiffHeaderOffset)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end19
  br label %do.end

do.end:                                           ; preds = %do.cond
  %call20 = call i8* @avifROStreamCurrent(%struct.avifROStream* %exifBoxStream)
  %data21 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %exifData, i32 0, i32 0
  store i8* %call20, i8** %data21, align 4, !tbaa !6
  %call22 = call i32 @avifROStreamRemainingBytes(%struct.avifROStream* %exifBoxStream)
  %size23 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %exifData, i32 0, i32 1
  store i32 %call22, i32* %size23, align 4, !tbaa !9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end, %if.then18
  %30 = bitcast i32* %exifTiffHeaderOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  %31 = bitcast %struct.avifROData* %exifBoxStream_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %31) #4
  %32 = bitcast %struct.avifROStream* %exifBoxStream to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %32) #4
  %33 = bitcast i8** %boxPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup43 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end42

if.else:                                          ; preds = %if.end9
  %34 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %type27 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %34, i32 0, i32 2
  %arraydecay28 = getelementptr inbounds [4 x i8], [4 x i8]* %type27, i32 0, i32 0
  %call29 = call i32 @memcmp(i8* %arraydecay28, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.28, i32 0, i32 0), i32 4)
  %tobool30 = icmp ne i32 %call29, 0
  br i1 %tobool30, label %if.end41, label %land.lhs.true31

land.lhs.true31:                                  ; preds = %if.else
  %35 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %contentType = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %35, i32 0, i32 6
  %contentType32 = getelementptr inbounds %struct.avifContentType, %struct.avifContentType* %contentType, i32 0, i32 0
  %arraydecay33 = getelementptr inbounds [64 x i8], [64 x i8]* %contentType32, i32 0, i32 0
  %call34 = call i32 @memcmp(i8* %arraydecay33, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @xmpContentType, i32 0, i32 0), i32 20)
  %tobool35 = icmp ne i32 %call34, 0
  br i1 %tobool35, label %if.end41, label %if.then36

if.then36:                                        ; preds = %land.lhs.true31
  %36 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %37 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %call37 = call i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %36, %struct.avifDecoderItem* %37)
  %data38 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %xmpData, i32 0, i32 0
  store i8* %call37, i8** %data38, align 4, !tbaa !6
  %38 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %size39 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %38, i32 0, i32 4
  %39 = load i32, i32* %size39, align 4, !tbaa !82
  %size40 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %xmpData, i32 0, i32 1
  store i32 %39, i32* %size40, align 4, !tbaa !9
  br label %if.end41

if.end41:                                         ; preds = %if.then36, %land.lhs.true31, %if.else
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

cleanup43:                                        ; preds = %if.end42, %cleanup, %if.then8, %if.then4, %if.then
  %40 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %cleanup.dest44 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest44, label %cleanup46 [
    i32 0, label %cleanup.cont45
    i32 4, label %for.inc
  ]

cleanup.cont45:                                   ; preds = %cleanup43
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont45, %cleanup43
  %41 = load i32, i32* %itemIndex, align 4, !tbaa !12
  %inc = add i32 %41, 1
  store i32 %inc, i32* %itemIndex, align 4, !tbaa !12
  br label %for.cond

cleanup46:                                        ; preds = %cleanup43, %for.cond.cleanup
  %42 = bitcast i32* %itemIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %cleanup.dest47 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest47, label %cleanup66 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup46
  %data48 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %exifData, i32 0, i32 0
  %43 = load i8*, i8** %data48, align 4, !tbaa !6
  %tobool49 = icmp ne i8* %43, null
  br i1 %tobool49, label %land.lhs.true50, label %if.end56

land.lhs.true50:                                  ; preds = %for.end
  %size51 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %exifData, i32 0, i32 1
  %44 = load i32, i32* %size51, align 4, !tbaa !9
  %tobool52 = icmp ne i32 %44, 0
  br i1 %tobool52, label %if.then53, label %if.end56

if.then53:                                        ; preds = %land.lhs.true50
  %45 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %data54 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %exifData, i32 0, i32 0
  %46 = load i8*, i8** %data54, align 4, !tbaa !6
  %size55 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %exifData, i32 0, i32 1
  %47 = load i32, i32* %size55, align 4, !tbaa !9
  call void @avifImageSetMetadataExif(%struct.avifImage* %45, i8* %46, i32 %47)
  br label %if.end56

if.end56:                                         ; preds = %if.then53, %land.lhs.true50, %for.end
  %data57 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %xmpData, i32 0, i32 0
  %48 = load i8*, i8** %data57, align 4, !tbaa !6
  %tobool58 = icmp ne i8* %48, null
  br i1 %tobool58, label %land.lhs.true59, label %if.end65

land.lhs.true59:                                  ; preds = %if.end56
  %size60 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %xmpData, i32 0, i32 1
  %49 = load i32, i32* %size60, align 4, !tbaa !9
  %tobool61 = icmp ne i32 %49, 0
  br i1 %tobool61, label %if.then62, label %if.end65

if.then62:                                        ; preds = %land.lhs.true59
  %50 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %data63 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %xmpData, i32 0, i32 0
  %51 = load i8*, i8** %data63, align 4, !tbaa !6
  %size64 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %xmpData, i32 0, i32 1
  %52 = load i32, i32* %size64, align 4, !tbaa !9
  call void @avifImageSetMetadataXMP(%struct.avifImage* %50, i8* %51, i32 %52)
  br label %if.end65

if.end65:                                         ; preds = %if.then62, %land.lhs.true59, %if.end56
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup66

cleanup66:                                        ; preds = %if.end65, %cleanup46
  %53 = bitcast %struct.avifROData* %xmpData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %53) #4
  %54 = bitcast %struct.avifROData* %exifData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %54) #4
  %55 = load i32, i32* %retval, align 4
  ret i32 %55
}

; Function Attrs: nounwind
define internal %struct.avifTile* @avifDecoderDataCreateTile(%struct.avifDecoderData* %data) #0 {
entry:
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %tile = alloca %struct.avifTile*, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %1, i32 0, i32 4
  %2 = bitcast %struct.avifTileArray* %tiles to i8*
  %call = call i8* @avifArrayPushPtr(i8* %2)
  %3 = bitcast i8* %call to %struct.avifTile*
  store %struct.avifTile* %3, %struct.avifTile** %tile, align 4, !tbaa !2
  %call1 = call %struct.avifImage* @avifImageCreateEmpty()
  %4 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %image = getelementptr inbounds %struct.avifTile, %struct.avifTile* %4, i32 0, i32 2
  store %struct.avifImage* %call1, %struct.avifImage** %image, align 4, !tbaa !135
  %call2 = call %struct.avifCodecDecodeInput* @avifCodecDecodeInputCreate()
  %5 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input = getelementptr inbounds %struct.avifTile, %struct.avifTile* %5, i32 0, i32 0
  store %struct.avifCodecDecodeInput* %call2, %struct.avifCodecDecodeInput** %input, align 4, !tbaa !49
  %6 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %7 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #4
  ret %struct.avifTile* %6
}

; Function Attrs: nounwind
define internal i32 @avifCodecDecodeInputGetSamples(%struct.avifCodecDecodeInput* %decodeInput, %struct.avifSampleTable* %sampleTable, %struct.avifROData* %rawInput) #0 {
entry:
  %retval = alloca i32, align 4
  %decodeInput.addr = alloca %struct.avifCodecDecodeInput*, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %rawInput.addr = alloca %struct.avifROData*, align 4
  %sampleSizeIndex = alloca i32, align 4
  %chunkIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %chunk = alloca %struct.avifSampleTableChunk*, align 4
  %sampleCount = alloca i32, align 4
  %sampleToChunkIndex = alloca i32, align 4
  %sampleToChunk = alloca %struct.avifSampleTableSampleToChunk*, align 4
  %sampleOffset = alloca i64, align 8
  %sampleIndex = alloca i32, align 4
  %sampleSize = alloca i32, align 4
  %sampleSizePtr = alloca %struct.avifSampleTableSampleSize*, align 4
  %sample = alloca %struct.avifDecodeSample*, align 4
  %syncSampleIndex = alloca i32, align 4
  %frameIndex = alloca i32, align 4
  store %struct.avifCodecDecodeInput* %decodeInput, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store %struct.avifROData* %rawInput, %struct.avifROData** %rawInput.addr, align 4, !tbaa !2
  %0 = bitcast i32* %sampleSizeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %sampleSizeIndex, align 4, !tbaa !12
  %1 = bitcast i32* %chunkIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %chunkIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %entry
  %2 = load i32, i32* %chunkIndex, align 4, !tbaa !12
  %3 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %chunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %3, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifSampleTableChunkArray, %struct.avifSampleTableChunkArray* %chunks, i32 0, i32 2
  %4 = load i32, i32* %count, align 4, !tbaa !39
  %cmp = icmp ult i32 %2, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup59

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct.avifSampleTableChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %chunks1 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %6, i32 0, i32 0
  %chunk2 = getelementptr inbounds %struct.avifSampleTableChunkArray, %struct.avifSampleTableChunkArray* %chunks1, i32 0, i32 0
  %7 = load %struct.avifSampleTableChunk*, %struct.avifSampleTableChunk** %chunk2, align 4, !tbaa !123
  %8 = load i32, i32* %chunkIndex, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifSampleTableChunk, %struct.avifSampleTableChunk* %7, i32 %8
  store %struct.avifSampleTableChunk* %arrayidx, %struct.avifSampleTableChunk** %chunk, align 4, !tbaa !2
  %9 = bitcast i32* %sampleCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  store i32 0, i32* %sampleCount, align 4, !tbaa !12
  %10 = bitcast i32* %sampleToChunkIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleToChunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %11, i32 0, i32 2
  %count3 = getelementptr inbounds %struct.avifSampleTableSampleToChunkArray, %struct.avifSampleTableSampleToChunkArray* %sampleToChunks, i32 0, i32 2
  %12 = load i32, i32* %count3, align 4, !tbaa !139
  %sub = sub i32 %12, 1
  store i32 %sub, i32* %sampleToChunkIndex, align 4, !tbaa !12
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %sampleToChunkIndex, align 4, !tbaa !12
  %cmp5 = icmp sge i32 %13, 0
  br i1 %cmp5, label %for.body7, label %for.cond.cleanup6

for.cond.cleanup6:                                ; preds = %for.cond4
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup12

for.body7:                                        ; preds = %for.cond4
  %14 = bitcast %struct.avifSampleTableSampleToChunk** %sampleToChunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleToChunks8 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %15, i32 0, i32 2
  %sampleToChunk9 = getelementptr inbounds %struct.avifSampleTableSampleToChunkArray, %struct.avifSampleTableSampleToChunkArray* %sampleToChunks8, i32 0, i32 0
  %16 = load %struct.avifSampleTableSampleToChunk*, %struct.avifSampleTableSampleToChunk** %sampleToChunk9, align 4, !tbaa !140
  %17 = load i32, i32* %sampleToChunkIndex, align 4, !tbaa !12
  %arrayidx10 = getelementptr inbounds %struct.avifSampleTableSampleToChunk, %struct.avifSampleTableSampleToChunk* %16, i32 %17
  store %struct.avifSampleTableSampleToChunk* %arrayidx10, %struct.avifSampleTableSampleToChunk** %sampleToChunk, align 4, !tbaa !2
  %18 = load %struct.avifSampleTableSampleToChunk*, %struct.avifSampleTableSampleToChunk** %sampleToChunk, align 4, !tbaa !2
  %firstChunk = getelementptr inbounds %struct.avifSampleTableSampleToChunk, %struct.avifSampleTableSampleToChunk* %18, i32 0, i32 0
  %19 = load i32, i32* %firstChunk, align 4, !tbaa !141
  %20 = load i32, i32* %chunkIndex, align 4, !tbaa !12
  %add = add i32 %20, 1
  %cmp11 = icmp ule i32 %19, %add
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %for.body7
  %21 = load %struct.avifSampleTableSampleToChunk*, %struct.avifSampleTableSampleToChunk** %sampleToChunk, align 4, !tbaa !2
  %samplesPerChunk = getelementptr inbounds %struct.avifSampleTableSampleToChunk, %struct.avifSampleTableSampleToChunk* %21, i32 0, i32 1
  %22 = load i32, i32* %samplesPerChunk, align 4, !tbaa !143
  store i32 %22, i32* %sampleCount, align 4, !tbaa !12
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %23 = bitcast %struct.avifSampleTableSampleToChunk** %sampleToChunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup12 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %24 = load i32, i32* %sampleToChunkIndex, align 4, !tbaa !12
  %dec = add nsw i32 %24, -1
  store i32 %dec, i32* %sampleToChunkIndex, align 4, !tbaa !12
  br label %for.cond4

cleanup12:                                        ; preds = %cleanup, %for.cond.cleanup6
  %25 = bitcast i32* %sampleToChunkIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  br label %for.end

for.end:                                          ; preds = %cleanup12
  %26 = load i32, i32* %sampleCount, align 4, !tbaa !12
  %cmp13 = icmp eq i32 %26, 0
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %for.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end15:                                         ; preds = %for.end
  %27 = bitcast i64* %sampleOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %27) #4
  %28 = load %struct.avifSampleTableChunk*, %struct.avifSampleTableChunk** %chunk, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifSampleTableChunk, %struct.avifSampleTableChunk* %28, i32 0, i32 0
  %29 = load i64, i64* %offset, align 8, !tbaa !124
  store i64 %29, i64* %sampleOffset, align 8, !tbaa !133
  %30 = bitcast i32* %sampleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store i32 0, i32* %sampleIndex, align 4, !tbaa !12
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc47, %if.end15
  %31 = load i32, i32* %sampleIndex, align 4, !tbaa !12
  %32 = load i32, i32* %sampleCount, align 4, !tbaa !12
  %cmp17 = icmp ult i32 %31, %32
  br i1 %cmp17, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond16
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup49

for.body19:                                       ; preds = %for.cond16
  %33 = bitcast i32* %sampleSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %allSamplesSize = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %34, i32 0, i32 6
  %35 = load i32, i32* %allSamplesSize, align 4, !tbaa !144
  store i32 %35, i32* %sampleSize, align 4, !tbaa !12
  %36 = load i32, i32* %sampleSize, align 4, !tbaa !12
  %cmp20 = icmp eq i32 %36, 0
  br i1 %cmp20, label %if.then21, label %if.end29

if.then21:                                        ; preds = %for.body19
  %37 = load i32, i32* %sampleSizeIndex, align 4, !tbaa !12
  %38 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleSizes = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %38, i32 0, i32 3
  %count22 = getelementptr inbounds %struct.avifSampleTableSampleSizeArray, %struct.avifSampleTableSampleSizeArray* %sampleSizes, i32 0, i32 2
  %39 = load i32, i32* %count22, align 4, !tbaa !145
  %cmp23 = icmp uge i32 %37, %39
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.then21
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup44

if.end25:                                         ; preds = %if.then21
  %40 = bitcast %struct.avifSampleTableSampleSize** %sampleSizePtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #4
  %41 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleSizes26 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %41, i32 0, i32 3
  %sampleSize27 = getelementptr inbounds %struct.avifSampleTableSampleSizeArray, %struct.avifSampleTableSampleSizeArray* %sampleSizes26, i32 0, i32 0
  %42 = load %struct.avifSampleTableSampleSize*, %struct.avifSampleTableSampleSize** %sampleSize27, align 4, !tbaa !146
  %43 = load i32, i32* %sampleSizeIndex, align 4, !tbaa !12
  %arrayidx28 = getelementptr inbounds %struct.avifSampleTableSampleSize, %struct.avifSampleTableSampleSize* %42, i32 %43
  store %struct.avifSampleTableSampleSize* %arrayidx28, %struct.avifSampleTableSampleSize** %sampleSizePtr, align 4, !tbaa !2
  %44 = load %struct.avifSampleTableSampleSize*, %struct.avifSampleTableSampleSize** %sampleSizePtr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.avifSampleTableSampleSize, %struct.avifSampleTableSampleSize* %44, i32 0, i32 0
  %45 = load i32, i32* %size, align 4, !tbaa !147
  store i32 %45, i32* %sampleSize, align 4, !tbaa !12
  %46 = bitcast %struct.avifSampleTableSampleSize** %sampleSizePtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  br label %if.end29

if.end29:                                         ; preds = %if.end25, %for.body19
  %47 = bitcast %struct.avifDecodeSample** %sample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #4
  %48 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %samples = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %48, i32 0, i32 0
  %49 = bitcast %struct.avifDecodeSampleArray* %samples to i8*
  %call = call i8* @avifArrayPushPtr(i8* %49)
  %50 = bitcast i8* %call to %struct.avifDecodeSample*
  store %struct.avifDecodeSample* %50, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %51 = load %struct.avifROData*, %struct.avifROData** %rawInput.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %51, i32 0, i32 0
  %52 = load i8*, i8** %data, align 4, !tbaa !6
  %53 = load i64, i64* %sampleOffset, align 8, !tbaa !133
  %idx.ext = trunc i64 %53 to i32
  %add.ptr = getelementptr inbounds i8, i8* %52, i32 %idx.ext
  %54 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %data30 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %54, i32 0, i32 0
  %data31 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data30, i32 0, i32 0
  store i8* %add.ptr, i8** %data31, align 4, !tbaa !106
  %55 = load i32, i32* %sampleSize, align 4, !tbaa !12
  %56 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %data32 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %56, i32 0, i32 0
  %size33 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data32, i32 0, i32 1
  store i32 %55, i32* %size33, align 4, !tbaa !107
  %57 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %57, i32 0, i32 1
  store i32 0, i32* %sync, align 4, !tbaa !93
  %58 = load i64, i64* %sampleOffset, align 8, !tbaa !133
  %59 = load i32, i32* %sampleSize, align 4, !tbaa !12
  %conv = zext i32 %59 to i64
  %add34 = add i64 %58, %conv
  %60 = load %struct.avifROData*, %struct.avifROData** %rawInput.addr, align 4, !tbaa !2
  %size35 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %60, i32 0, i32 1
  %61 = load i32, i32* %size35, align 4, !tbaa !9
  %conv36 = zext i32 %61 to i64
  %cmp37 = icmp ugt i64 %add34, %conv36
  br i1 %cmp37, label %if.then39, label %if.end40

if.then39:                                        ; preds = %if.end29
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

if.end40:                                         ; preds = %if.end29
  %62 = load i32, i32* %sampleSize, align 4, !tbaa !12
  %conv41 = zext i32 %62 to i64
  %63 = load i64, i64* %sampleOffset, align 8, !tbaa !133
  %add42 = add i64 %63, %conv41
  store i64 %add42, i64* %sampleOffset, align 8, !tbaa !133
  %64 = load i32, i32* %sampleSizeIndex, align 4, !tbaa !12
  %inc = add i32 %64, 1
  store i32 %inc, i32* %sampleSizeIndex, align 4, !tbaa !12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

cleanup43:                                        ; preds = %if.end40, %if.then39
  %65 = bitcast %struct.avifDecodeSample** %sample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  br label %cleanup44

cleanup44:                                        ; preds = %cleanup43, %if.then24
  %66 = bitcast i32* %sampleSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #4
  %cleanup.dest45 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest45, label %cleanup49 [
    i32 0, label %cleanup.cont46
  ]

cleanup.cont46:                                   ; preds = %cleanup44
  br label %for.inc47

for.inc47:                                        ; preds = %cleanup.cont46
  %67 = load i32, i32* %sampleIndex, align 4, !tbaa !12
  %inc48 = add i32 %67, 1
  store i32 %inc48, i32* %sampleIndex, align 4, !tbaa !12
  br label %for.cond16

cleanup49:                                        ; preds = %cleanup44, %for.cond.cleanup18
  %68 = bitcast i32* %sampleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #4
  %cleanup.dest50 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest50, label %cleanup52 [
    i32 8, label %for.end51
  ]

for.end51:                                        ; preds = %cleanup49
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

cleanup52:                                        ; preds = %for.end51, %cleanup49
  %69 = bitcast i64* %sampleOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %69) #4
  br label %cleanup53

cleanup53:                                        ; preds = %cleanup52, %if.then14
  %70 = bitcast i32* %sampleCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast %struct.avifSampleTableChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  %cleanup.dest55 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest55, label %cleanup59 [
    i32 0, label %cleanup.cont56
  ]

cleanup.cont56:                                   ; preds = %cleanup53
  br label %for.inc57

for.inc57:                                        ; preds = %cleanup.cont56
  %72 = load i32, i32* %chunkIndex, align 4, !tbaa !12
  %inc58 = add i32 %72, 1
  store i32 %inc58, i32* %chunkIndex, align 4, !tbaa !12
  br label %for.cond

cleanup59:                                        ; preds = %cleanup53, %for.cond.cleanup
  %73 = bitcast i32* %chunkIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #4
  %cleanup.dest60 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest60, label %cleanup95 [
    i32 2, label %for.end61
  ]

for.end61:                                        ; preds = %cleanup59
  %74 = bitcast i32* %syncSampleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #4
  store i32 0, i32* %syncSampleIndex, align 4, !tbaa !12
  br label %for.cond62

for.cond62:                                       ; preds = %for.inc81, %for.end61
  %75 = load i32, i32* %syncSampleIndex, align 4, !tbaa !12
  %76 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %syncSamples = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %76, i32 0, i32 5
  %count63 = getelementptr inbounds %struct.avifSyncSampleArray, %struct.avifSyncSampleArray* %syncSamples, i32 0, i32 2
  %77 = load i32, i32* %count63, align 4, !tbaa !149
  %cmp64 = icmp ult i32 %75, %77
  br i1 %cmp64, label %for.body67, label %for.cond.cleanup66

for.cond.cleanup66:                               ; preds = %for.cond62
  store i32 11, i32* %cleanup.dest.slot, align 4
  %78 = bitcast i32* %syncSampleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  br label %for.end84

for.body67:                                       ; preds = %for.cond62
  %79 = bitcast i32* %frameIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #4
  %80 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %syncSamples68 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %80, i32 0, i32 5
  %syncSample = getelementptr inbounds %struct.avifSyncSampleArray, %struct.avifSyncSampleArray* %syncSamples68, i32 0, i32 0
  %81 = load %struct.avifSyncSample*, %struct.avifSyncSample** %syncSample, align 4, !tbaa !150
  %82 = load i32, i32* %syncSampleIndex, align 4, !tbaa !12
  %arrayidx69 = getelementptr inbounds %struct.avifSyncSample, %struct.avifSyncSample* %81, i32 %82
  %sampleNumber = getelementptr inbounds %struct.avifSyncSample, %struct.avifSyncSample* %arrayidx69, i32 0, i32 0
  %83 = load i32, i32* %sampleNumber, align 4, !tbaa !151
  %sub70 = sub i32 %83, 1
  store i32 %sub70, i32* %frameIndex, align 4, !tbaa !12
  %84 = load i32, i32* %frameIndex, align 4, !tbaa !12
  %85 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %samples71 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %85, i32 0, i32 0
  %count72 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples71, i32 0, i32 2
  %86 = load i32, i32* %count72, align 4, !tbaa !57
  %cmp73 = icmp ult i32 %84, %86
  br i1 %cmp73, label %if.then75, label %if.end80

if.then75:                                        ; preds = %for.body67
  %87 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %samples76 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %87, i32 0, i32 0
  %sample77 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples76, i32 0, i32 0
  %88 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample77, align 4, !tbaa !105
  %89 = load i32, i32* %frameIndex, align 4, !tbaa !12
  %arrayidx78 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %88, i32 %89
  %sync79 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %arrayidx78, i32 0, i32 1
  store i32 1, i32* %sync79, align 4, !tbaa !93
  br label %if.end80

if.end80:                                         ; preds = %if.then75, %for.body67
  %90 = bitcast i32* %frameIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  br label %for.inc81

for.inc81:                                        ; preds = %if.end80
  %91 = load i32, i32* %syncSampleIndex, align 4, !tbaa !12
  %inc82 = add i32 %91, 1
  store i32 %inc82, i32* %syncSampleIndex, align 4, !tbaa !12
  br label %for.cond62

for.end84:                                        ; preds = %for.cond.cleanup66
  %92 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %samples85 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %92, i32 0, i32 0
  %count86 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples85, i32 0, i32 2
  %93 = load i32, i32* %count86, align 4, !tbaa !57
  %cmp87 = icmp ugt i32 %93, 0
  br i1 %cmp87, label %if.then89, label %if.end94

if.then89:                                        ; preds = %for.end84
  %94 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %samples90 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %94, i32 0, i32 0
  %sample91 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples90, i32 0, i32 0
  %95 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample91, align 4, !tbaa !105
  %arrayidx92 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %95, i32 0
  %sync93 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %arrayidx92, i32 0, i32 1
  store i32 1, i32* %sync93, align 4, !tbaa !93
  br label %if.end94

if.end94:                                         ; preds = %if.then89, %for.end84
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup95

cleanup95:                                        ; preds = %if.end94, %cleanup59
  %96 = bitcast i32* %sampleSizeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = load i32, i32* %retval, align 4
  ret i32 %97
}

; Function Attrs: nounwind
define internal i32 @avifParseImageGridBox(%struct.avifImageGrid* %grid, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %grid.addr = alloca %struct.avifImageGrid*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %version = alloca i8, align 1
  %flags = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %fieldLength = alloca i32, align 4
  %outputWidth16 = alloca i16, align 2
  %outputHeight16 = alloca i16, align 2
  store %struct.avifImageGrid* %grid, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %flags) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %version, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup71

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = load i8, i8* %version, align 1, !tbaa !25
  %conv = zext i8 %4 to i32
  %cmp = icmp ne i32 %conv, 0
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %do.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup71

if.end3:                                          ; preds = %do.end
  br label %do.body4

do.body4:                                         ; preds = %if.end3
  %call5 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %flags, i32 1)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %do.body4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup71

if.end8:                                          ; preds = %do.body4
  br label %do.cond9

do.cond9:                                         ; preds = %if.end8
  br label %do.end10

do.end10:                                         ; preds = %do.cond9
  br label %do.body11

do.body11:                                        ; preds = %do.end10
  %5 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %5, i32 0, i32 0
  %call12 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %rows, i32 1)
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %do.body11
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup71

if.end15:                                         ; preds = %do.body11
  br label %do.cond16

do.cond16:                                        ; preds = %if.end15
  br label %do.end17

do.end17:                                         ; preds = %do.cond16
  br label %do.body18

do.body18:                                        ; preds = %do.end17
  %6 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %columns = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %6, i32 0, i32 1
  %call19 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %columns, i32 1)
  %tobool20 = icmp ne i32 %call19, 0
  br i1 %tobool20, label %if.end22, label %if.then21

if.then21:                                        ; preds = %do.body18
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup71

if.end22:                                         ; preds = %do.body18
  br label %do.cond23

do.cond23:                                        ; preds = %if.end22
  br label %do.end24

do.end24:                                         ; preds = %do.cond23
  %7 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %rows25 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %7, i32 0, i32 0
  %8 = load i8, i8* %rows25, align 4, !tbaa !153
  %inc = add i8 %8, 1
  store i8 %inc, i8* %rows25, align 4, !tbaa !153
  %9 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %columns26 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %9, i32 0, i32 1
  %10 = load i8, i8* %columns26, align 1, !tbaa !154
  %inc27 = add i8 %10, 1
  store i8 %inc27, i8* %columns26, align 1, !tbaa !154
  %11 = bitcast i32* %fieldLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i8, i8* %flags, align 1, !tbaa !25
  %conv28 = zext i8 %12 to i32
  %and = and i32 %conv28, 1
  %add = add nsw i32 %and, 1
  %mul = mul nsw i32 %add, 16
  store i32 %mul, i32* %fieldLength, align 4, !tbaa !12
  %13 = load i32, i32* %fieldLength, align 4, !tbaa !12
  %cmp29 = icmp eq i32 %13, 16
  br i1 %cmp29, label %if.then31, label %if.else

if.then31:                                        ; preds = %do.end24
  %14 = bitcast i16* %outputWidth16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #4
  %15 = bitcast i16* %outputHeight16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %15) #4
  br label %do.body32

do.body32:                                        ; preds = %if.then31
  %call33 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %outputWidth16)
  %tobool34 = icmp ne i32 %call33, 0
  br i1 %tobool34, label %if.end36, label %if.then35

if.then35:                                        ; preds = %do.body32
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end36:                                         ; preds = %do.body32
  br label %do.cond37

do.cond37:                                        ; preds = %if.end36
  br label %do.end38

do.end38:                                         ; preds = %do.cond37
  br label %do.body39

do.body39:                                        ; preds = %do.end38
  %call40 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %outputHeight16)
  %tobool41 = icmp ne i32 %call40, 0
  br i1 %tobool41, label %if.end43, label %if.then42

if.then42:                                        ; preds = %do.body39
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end43:                                         ; preds = %do.body39
  br label %do.cond44

do.cond44:                                        ; preds = %if.end43
  br label %do.end45

do.end45:                                         ; preds = %do.cond44
  %16 = load i16, i16* %outputWidth16, align 2, !tbaa !155
  %conv46 = zext i16 %16 to i32
  %17 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputWidth = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %17, i32 0, i32 2
  store i32 %conv46, i32* %outputWidth, align 4, !tbaa !157
  %18 = load i16, i16* %outputHeight16, align 2, !tbaa !155
  %conv47 = zext i16 %18 to i32
  %19 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputHeight = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %19, i32 0, i32 3
  store i32 %conv47, i32* %outputHeight, align 4, !tbaa !158
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end45, %if.then42, %if.then35
  %20 = bitcast i16* %outputHeight16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %20) #4
  %21 = bitcast i16* %outputWidth16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %21) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup70 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end69

if.else:                                          ; preds = %do.end24
  %22 = load i32, i32* %fieldLength, align 4, !tbaa !12
  %cmp49 = icmp ne i32 %22, 32
  br i1 %cmp49, label %if.then51, label %if.end52

if.then51:                                        ; preds = %if.else
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

if.end52:                                         ; preds = %if.else
  br label %do.body53

do.body53:                                        ; preds = %if.end52
  %23 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputWidth54 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %23, i32 0, i32 2
  %call55 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %outputWidth54)
  %tobool56 = icmp ne i32 %call55, 0
  br i1 %tobool56, label %if.end58, label %if.then57

if.then57:                                        ; preds = %do.body53
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

if.end58:                                         ; preds = %do.body53
  br label %do.cond59

do.cond59:                                        ; preds = %if.end58
  br label %do.end60

do.end60:                                         ; preds = %do.cond59
  br label %do.body61

do.body61:                                        ; preds = %do.end60
  %24 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputHeight62 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %24, i32 0, i32 3
  %call63 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %outputHeight62)
  %tobool64 = icmp ne i32 %call63, 0
  br i1 %tobool64, label %if.end66, label %if.then65

if.then65:                                        ; preds = %do.body61
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

if.end66:                                         ; preds = %do.body61
  br label %do.cond67

do.cond67:                                        ; preds = %if.end66
  br label %do.end68

do.end68:                                         ; preds = %do.cond67
  br label %if.end69

if.end69:                                         ; preds = %do.end68, %cleanup.cont
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

cleanup70:                                        ; preds = %if.end69, %if.then65, %if.then57, %if.then51, %cleanup
  %25 = bitcast i32* %fieldLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  br label %cleanup71

cleanup71:                                        ; preds = %cleanup70, %if.then21, %if.then14, %if.then7, %if.then2, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %flags) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %26 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %26) #4
  %27 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %27) #4
  %28 = load i32, i32* %retval, align 4
  ret i32 %28
}

; Function Attrs: nounwind
define internal %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %properties, i8* %type) #0 {
entry:
  %retval = alloca %struct.avifProperty*, align 4
  %properties.addr = alloca %struct.avifPropertyArray*, align 4
  %type.addr = alloca i8*, align 4
  %propertyIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %prop = alloca %struct.avifProperty*, align 4
  store %struct.avifPropertyArray* %properties, %struct.avifPropertyArray** %properties.addr, align 4, !tbaa !2
  store i8* %type, i8** %type.addr, align 4, !tbaa !2
  %0 = bitcast i32* %propertyIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %propertyIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %propertyIndex, align 4, !tbaa !12
  %2 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %properties.addr, align 4, !tbaa !2
  %count = getelementptr inbounds %struct.avifPropertyArray, %struct.avifPropertyArray* %2, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !159
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup3

for.body:                                         ; preds = %for.cond
  %4 = bitcast %struct.avifProperty** %prop to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %properties.addr, align 4, !tbaa !2
  %prop1 = getelementptr inbounds %struct.avifPropertyArray, %struct.avifPropertyArray* %5, i32 0, i32 0
  %6 = load %struct.avifProperty*, %struct.avifProperty** %prop1, align 4, !tbaa !160
  %7 = load i32, i32* %propertyIndex, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %6, i32 %7
  store %struct.avifProperty* %arrayidx, %struct.avifProperty** %prop, align 4, !tbaa !2
  %8 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %type2 = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %8, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type2, i32 0, i32 0
  %9 = load i8*, i8** %type.addr, align 4, !tbaa !2
  %call = call i32 @memcmp(i8* %arraydecay, i8* %9, i32 4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  %10 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  store %struct.avifProperty* %10, %struct.avifProperty** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %11 = bitcast %struct.avifProperty** %prop to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup3 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %12 = load i32, i32* %propertyIndex, align 4, !tbaa !12
  %inc = add i32 %12, 1
  store i32 %inc, i32* %propertyIndex, align 4, !tbaa !12
  br label %for.cond

cleanup3:                                         ; preds = %cleanup, %for.cond.cleanup
  %13 = bitcast i32* %propertyIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  %cleanup.dest4 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest4, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup3
  store %struct.avifProperty* null, %struct.avifProperty** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup3
  %14 = load %struct.avifProperty*, %struct.avifProperty** %retval, align 4
  ret %struct.avifProperty* %14

unreachable:                                      ; preds = %cleanup3
  unreachable
}

; Function Attrs: nounwind
define internal i32 @isAlphaURN(i8* %urn) #0 {
entry:
  %urn.addr = alloca i8*, align 4
  store i8* %urn, i8** %urn.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %urn.addr, align 4, !tbaa !2
  %call = call i32 @strcmp(i8* %0, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.48, i32 0, i32 0))
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %lor.rhs, label %lor.end

lor.rhs:                                          ; preds = %entry
  %1 = load i8*, i8** %urn.addr, align 4, !tbaa !2
  %call1 = call i32 @strcmp(i8* %1, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.49, i32 0, i32 0))
  %tobool2 = icmp ne i32 %call1, 0
  %lnot = xor i1 %tobool2, true
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %lnot, %lor.rhs ]
  %lor.ext = zext i1 %2 to i32
  ret i32 %lor.ext
}

; Function Attrs: nounwind
define internal i32 @avifDecoderDataGenerateImageGridTiles(%struct.avifDecoderData* %data, %struct.avifImageGrid* %grid, %struct.avifDecoderItem* %gridItem, i32 %alpha) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %grid.addr = alloca %struct.avifImageGrid*, align 4
  %gridItem.addr = alloca %struct.avifDecoderItem*, align 4
  %alpha.addr = alloca i32, align 4
  %tilesRequested = alloca i32, align 4
  %tilesAvailable = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %item = alloca %struct.avifDecoderItem*, align 4
  %firstTile = alloca i32, align 4
  %i19 = alloca i32, align 4
  %item28 = alloca %struct.avifDecoderItem*, align 4
  %tile = alloca %struct.avifTile*, align 4
  %sample = alloca %struct.avifDecodeSample*, align 4
  %srcProp = alloca %struct.avifProperty*, align 4
  %dstProp = alloca %struct.avifProperty*, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  store %struct.avifImageGrid* %grid, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  store %struct.avifDecoderItem* %gridItem, %struct.avifDecoderItem** %gridItem.addr, align 4, !tbaa !2
  store i32 %alpha, i32* %alpha.addr, align 4, !tbaa !12
  %0 = bitcast i32* %tilesRequested to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %1, i32 0, i32 0
  %2 = load i8, i8* %rows, align 4, !tbaa !153
  %conv = zext i8 %2 to i32
  %3 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %columns = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %3, i32 0, i32 1
  %4 = load i8, i8* %columns, align 1, !tbaa !154
  %conv1 = zext i8 %4 to i32
  %mul = mul i32 %conv, %conv1
  store i32 %mul, i32* %tilesRequested, align 4, !tbaa !12
  %5 = bitcast i32* %tilesAvailable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %tilesAvailable, align 4, !tbaa !12
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !12
  %8 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %gridItem.addr, align 4, !tbaa !2
  %meta = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %8, i32 0, i32 1
  %9 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !127
  %items = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %9, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items, i32 0, i32 2
  %10 = load i32, i32* %count, align 4, !tbaa !76
  %cmp = icmp ult i32 %7, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %gridItem.addr, align 4, !tbaa !2
  %meta3 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %13, i32 0, i32 1
  %14 = load %struct.avifMeta*, %struct.avifMeta** %meta3, align 4, !tbaa !127
  %items4 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %14, i32 0, i32 0
  %item5 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items4, i32 0, i32 0
  %15 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item5, align 4, !tbaa !81
  %16 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %15, i32 %16
  store %struct.avifDecoderItem* %arrayidx, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %17 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %dimgForID = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %17, i32 0, i32 11
  %18 = load i32, i32* %dimgForID, align 4, !tbaa !161
  %19 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %gridItem.addr, align 4, !tbaa !2
  %id = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %19, i32 0, i32 0
  %20 = load i32, i32* %id, align 4, !tbaa !88
  %cmp6 = icmp eq i32 %18, %20
  br i1 %cmp6, label %if.then, label %if.end12

if.then:                                          ; preds = %for.body
  %21 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %21, i32 0, i32 2
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0), i32 4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.then
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %22 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %hasUnsupportedEssentialProperty = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %22, i32 0, i32 12
  %23 = load i32, i32* %hasUnsupportedEssentialProperty, align 4, !tbaa !85
  %tobool9 = icmp ne i32 %23, 0
  br i1 %tobool9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %if.end
  %24 = load i32, i32* %tilesAvailable, align 4, !tbaa !12
  %inc = add i32 %24, 1
  store i32 %inc, i32* %tilesAvailable, align 4, !tbaa !12
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end12, %if.then10, %if.then8
  %25 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %26 = load i32, i32* %i, align 4, !tbaa !12
  %inc13 = add i32 %26, 1
  store i32 %inc13, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %27 = load i32, i32* %tilesRequested, align 4, !tbaa !12
  %28 = load i32, i32* %tilesAvailable, align 4, !tbaa !12
  %cmp15 = icmp ne i32 %27, %28
  br i1 %cmp15, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup83

if.end18:                                         ; preds = %for.end
  %29 = bitcast i32* %firstTile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  store i32 1, i32* %firstTile, align 4, !tbaa !12
  %30 = bitcast i32* %i19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  store i32 0, i32* %i19, align 4, !tbaa !12
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc77, %if.end18
  %31 = load i32, i32* %i19, align 4, !tbaa !12
  %32 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %gridItem.addr, align 4, !tbaa !2
  %meta21 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %32, i32 0, i32 1
  %33 = load %struct.avifMeta*, %struct.avifMeta** %meta21, align 4, !tbaa !127
  %items22 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %33, i32 0, i32 0
  %count23 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items22, i32 0, i32 2
  %34 = load i32, i32* %count23, align 4, !tbaa !76
  %cmp24 = icmp ult i32 %31, %34
  br i1 %cmp24, label %for.body27, label %for.cond.cleanup26

for.cond.cleanup26:                               ; preds = %for.cond20
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup79

for.body27:                                       ; preds = %for.cond20
  %35 = bitcast %struct.avifDecoderItem** %item28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %gridItem.addr, align 4, !tbaa !2
  %meta29 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %36, i32 0, i32 1
  %37 = load %struct.avifMeta*, %struct.avifMeta** %meta29, align 4, !tbaa !127
  %items30 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %37, i32 0, i32 0
  %item31 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items30, i32 0, i32 0
  %38 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item31, align 4, !tbaa !81
  %39 = load i32, i32* %i19, align 4, !tbaa !12
  %arrayidx32 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %38, i32 %39
  store %struct.avifDecoderItem* %arrayidx32, %struct.avifDecoderItem** %item28, align 4, !tbaa !2
  %40 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item28, align 4, !tbaa !2
  %dimgForID33 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %40, i32 0, i32 11
  %41 = load i32, i32* %dimgForID33, align 4, !tbaa !161
  %42 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %gridItem.addr, align 4, !tbaa !2
  %id34 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %42, i32 0, i32 0
  %43 = load i32, i32* %id34, align 4, !tbaa !88
  %cmp35 = icmp eq i32 %41, %43
  br i1 %cmp35, label %if.then37, label %if.end73

if.then37:                                        ; preds = %for.body27
  %44 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item28, align 4, !tbaa !2
  %type38 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %44, i32 0, i32 2
  %arraydecay39 = getelementptr inbounds [4 x i8], [4 x i8]* %type38, i32 0, i32 0
  %call40 = call i32 @memcmp(i8* %arraydecay39, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0), i32 4)
  %tobool41 = icmp ne i32 %call40, 0
  br i1 %tobool41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.then37
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup74

if.end43:                                         ; preds = %if.then37
  %45 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item28, align 4, !tbaa !2
  %hasUnsupportedEssentialProperty44 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %45, i32 0, i32 12
  %46 = load i32, i32* %hasUnsupportedEssentialProperty44, align 4, !tbaa !85
  %tobool45 = icmp ne i32 %46, 0
  br i1 %tobool45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.end43
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup74

if.end47:                                         ; preds = %if.end43
  %47 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #4
  %48 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %call48 = call %struct.avifTile* @avifDecoderDataCreateTile(%struct.avifDecoderData* %48)
  store %struct.avifTile* %call48, %struct.avifTile** %tile, align 4, !tbaa !2
  %49 = bitcast %struct.avifDecodeSample** %sample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #4
  %50 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input = getelementptr inbounds %struct.avifTile, %struct.avifTile* %50, i32 0, i32 0
  %51 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input, align 4, !tbaa !49
  %samples = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %51, i32 0, i32 0
  %52 = bitcast %struct.avifDecodeSampleArray* %samples to i8*
  %call49 = call i8* @avifArrayPushPtr(i8* %52)
  %53 = bitcast i8* %call49 to %struct.avifDecodeSample*
  store %struct.avifDecodeSample* %53, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %54 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %55 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item28, align 4, !tbaa !2
  %call50 = call i8* @avifDecoderDataCalcItemPtr(%struct.avifDecoderData* %54, %struct.avifDecoderItem* %55)
  %56 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %data51 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %56, i32 0, i32 0
  %data52 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data51, i32 0, i32 0
  store i8* %call50, i8** %data52, align 4, !tbaa !106
  %57 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item28, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %57, i32 0, i32 4
  %58 = load i32, i32* %size, align 4, !tbaa !82
  %59 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %data53 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %59, i32 0, i32 0
  %size54 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data53, i32 0, i32 1
  store i32 %58, i32* %size54, align 4, !tbaa !107
  %60 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %60, i32 0, i32 1
  store i32 1, i32* %sync, align 4, !tbaa !93
  %61 = load i32, i32* %alpha.addr, align 4, !tbaa !12
  %62 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input55 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %62, i32 0, i32 0
  %63 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input55, align 4, !tbaa !49
  %alpha56 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %63, i32 0, i32 1
  store i32 %61, i32* %alpha56, align 4, !tbaa !52
  %64 = load i32, i32* %firstTile, align 4, !tbaa !12
  %tobool57 = icmp ne i32 %64, 0
  br i1 %tobool57, label %if.then58, label %if.end68

if.then58:                                        ; preds = %if.end47
  store i32 0, i32* %firstTile, align 4, !tbaa !12
  %65 = bitcast %struct.avifProperty** %srcProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #4
  %66 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item28, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %66, i32 0, i32 7
  %call59 = call %struct.avifProperty* @avifPropertyArrayFind(%struct.avifPropertyArray* %properties, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i32 0, i32 0))
  store %struct.avifProperty* %call59, %struct.avifProperty** %srcProp, align 4, !tbaa !2
  %67 = load %struct.avifProperty*, %struct.avifProperty** %srcProp, align 4, !tbaa !2
  %tobool60 = icmp ne %struct.avifProperty* %67, null
  br i1 %tobool60, label %if.end62, label %if.then61

if.then61:                                        ; preds = %if.then58
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup65

if.end62:                                         ; preds = %if.then58
  %68 = bitcast %struct.avifProperty** %dstProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #4
  %69 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %gridItem.addr, align 4, !tbaa !2
  %properties63 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %69, i32 0, i32 7
  %70 = bitcast %struct.avifPropertyArray* %properties63 to i8*
  %call64 = call i8* @avifArrayPushPtr(i8* %70)
  %71 = bitcast i8* %call64 to %struct.avifProperty*
  store %struct.avifProperty* %71, %struct.avifProperty** %dstProp, align 4, !tbaa !2
  %72 = load %struct.avifProperty*, %struct.avifProperty** %dstProp, align 4, !tbaa !2
  %73 = bitcast %struct.avifProperty* %72 to i8*
  %74 = load %struct.avifProperty*, %struct.avifProperty** %srcProp, align 4, !tbaa !2
  %75 = bitcast %struct.avifProperty* %74 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %75, i32 68, i1 false)
  %76 = bitcast %struct.avifProperty** %dstProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup65

cleanup65:                                        ; preds = %if.end62, %if.then61
  %77 = bitcast %struct.avifProperty** %srcProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #4
  %cleanup.dest66 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest66, label %cleanup69 [
    i32 0, label %cleanup.cont67
  ]

cleanup.cont67:                                   ; preds = %cleanup65
  br label %if.end68

if.end68:                                         ; preds = %cleanup.cont67, %if.end47
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup69

cleanup69:                                        ; preds = %if.end68, %cleanup65
  %78 = bitcast %struct.avifDecodeSample** %sample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  %79 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %cleanup.dest71 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest71, label %cleanup74 [
    i32 0, label %cleanup.cont72
  ]

cleanup.cont72:                                   ; preds = %cleanup69
  br label %if.end73

if.end73:                                         ; preds = %cleanup.cont72, %for.body27
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup74

cleanup74:                                        ; preds = %if.end73, %cleanup69, %if.then46, %if.then42
  %80 = bitcast %struct.avifDecoderItem** %item28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %cleanup.dest75 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest75, label %cleanup79 [
    i32 0, label %cleanup.cont76
    i32 7, label %for.inc77
  ]

cleanup.cont76:                                   ; preds = %cleanup74
  br label %for.inc77

for.inc77:                                        ; preds = %cleanup.cont76, %cleanup74
  %81 = load i32, i32* %i19, align 4, !tbaa !12
  %inc78 = add i32 %81, 1
  store i32 %inc78, i32* %i19, align 4, !tbaa !12
  br label %for.cond20

cleanup79:                                        ; preds = %cleanup74, %for.cond.cleanup26
  %82 = bitcast i32* %i19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %cleanup.dest80 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest80, label %cleanup82 [
    i32 5, label %for.end81
  ]

for.end81:                                        ; preds = %cleanup79
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup82

cleanup82:                                        ; preds = %for.end81, %cleanup79
  %83 = bitcast i32* %firstTile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  br label %cleanup83

cleanup83:                                        ; preds = %cleanup82, %if.then17
  %84 = bitcast i32* %tilesAvailable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %tilesRequested to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = load i32, i32* %retval, align 4
  ret i32 %86

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i8* @avifArrayPushPtr(i8*) #2

declare void @avifImageSetProfileICC(%struct.avifImage*, i8*, i32) #2

declare i32 @avifSequenceHeaderParse(%struct.avifSequenceHeader*, %struct.avifROData*) #2

; Function Attrs: nounwind
define internal i32 @avifCodecConfigurationBoxGetDepth(%struct.avifCodecConfigurationBox* %av1C) #0 {
entry:
  %retval = alloca i32, align 4
  %av1C.addr = alloca %struct.avifCodecConfigurationBox*, align 4
  store %struct.avifCodecConfigurationBox* %av1C, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %0 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %twelveBit = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %0, i32 0, i32 4
  %1 = load i8, i8* %twelveBit, align 1, !tbaa !162
  %tobool = icmp ne i8 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 12, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %2 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %highBitdepth = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %2, i32 0, i32 3
  %3 = load i8, i8* %highBitdepth, align 1, !tbaa !164
  %tobool1 = icmp ne i8 %3, 0
  br i1 %tobool1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.else
  store i32 10, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.else
  br label %if.end3

if.end3:                                          ; preds = %if.end
  store i32 8, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: nounwind
define internal i32 @avifDecoderFlush(%struct.avifDecoder* %decoder) #0 {
entry:
  %retval = alloca i32, align 4
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tile = alloca %struct.avifTile*, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %0 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %0, i32 0, i32 11
  %1 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !18
  call void @avifDecoderDataResetCodec(%struct.avifDecoderData* %1)
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !12
  %4 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %4, i32 0, i32 11
  %5 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data1, align 4, !tbaa !18
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %5, i32 0, i32 4
  %count = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles, i32 0, i32 2
  %6 = load i32, i32* %count, align 4, !tbaa !92
  %cmp = icmp ult i32 %3, %6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup12

for.body:                                         ; preds = %for.cond
  %7 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data2 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %8, i32 0, i32 11
  %9 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data2, align 4, !tbaa !18
  %tiles3 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %9, i32 0, i32 4
  %tile4 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles3, i32 0, i32 0
  %10 = load %struct.avifTile*, %struct.avifTile** %tile4, align 4, !tbaa !104
  %11 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifTile, %struct.avifTile* %10, i32 %11
  store %struct.avifTile* %arrayidx, %struct.avifTile** %tile, align 4, !tbaa !2
  %12 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %codecChoice = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %12, i32 0, i32 0
  %13 = load i32, i32* %codecChoice, align 8, !tbaa !165
  %14 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input = getelementptr inbounds %struct.avifTile, %struct.avifTile* %14, i32 0, i32 0
  %15 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input, align 4, !tbaa !49
  %call = call %struct.avifCodec* @avifCodecCreateInternal(i32 %13, %struct.avifCodecDecodeInput* %15)
  %16 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec = getelementptr inbounds %struct.avifTile, %struct.avifTile* %16, i32 0, i32 1
  store %struct.avifCodec* %call, %struct.avifCodec** %codec, align 4, !tbaa !134
  %17 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec5 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %17, i32 0, i32 1
  %18 = load %struct.avifCodec*, %struct.avifCodec** %codec5, align 4, !tbaa !134
  %tobool = icmp ne %struct.avifCodec* %18, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i32 15, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %19 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec6 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %19, i32 0, i32 1
  %20 = load %struct.avifCodec*, %struct.avifCodec** %codec6, align 4, !tbaa !134
  %open = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %20, i32 0, i32 3
  %21 = load i32 (%struct.avifCodec*, i32)*, i32 (%struct.avifCodec*, i32)** %open, align 4, !tbaa !166
  %22 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec7 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %22, i32 0, i32 1
  %23 = load %struct.avifCodec*, %struct.avifCodec** %codec7, align 4, !tbaa !134
  %24 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %24, i32 0, i32 3
  %25 = load i32, i32* %imageIndex, align 4, !tbaa !56
  %add = add nsw i32 %25, 1
  %call8 = call i32 %21(%struct.avifCodec* %23, i32 %add)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %if.end
  store i32 11, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end11, %if.then10, %if.then
  %26 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup12 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %27 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup12:                                        ; preds = %cleanup, %for.cond.cleanup
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  %cleanup.dest13 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest13, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup12
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup12
  %29 = load i32, i32* %retval, align 4
  ret i32 %29

unreachable:                                      ; preds = %cleanup12
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @avifDecoderNextImage(%struct.avifDecoder* %decoder) #0 {
entry:
  %retval = alloca i32, align 4
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %tileIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tile = alloca %struct.avifTile*, align 4
  %srcColor = alloca %struct.avifImage*, align 4
  %srcAlpha = alloca %struct.avifImage*, align 4
  %timingResult = alloca i32, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %0 = bitcast i32* %tileIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %tileIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %tileIndex, align 4, !tbaa !12
  %2 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %2, i32 0, i32 11
  %3 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !18
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %3, i32 0, i32 4
  %count = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles, i32 0, i32 2
  %4 = load i32, i32* %count, align 4, !tbaa !92
  %cmp = icmp ult i32 %1, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup11

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %6, i32 0, i32 11
  %7 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data1, align 4, !tbaa !18
  %tiles2 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %7, i32 0, i32 4
  %tile3 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles2, i32 0, i32 0
  %8 = load %struct.avifTile*, %struct.avifTile** %tile3, align 4, !tbaa !104
  %9 = load i32, i32* %tileIndex, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifTile, %struct.avifTile* %8, i32 %9
  store %struct.avifTile* %arrayidx, %struct.avifTile** %tile, align 4, !tbaa !2
  %10 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec = getelementptr inbounds %struct.avifTile, %struct.avifTile* %10, i32 0, i32 1
  %11 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !134
  %getNextImage = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %11, i32 0, i32 4
  %12 = load i32 (%struct.avifCodec*, %struct.avifImage*)*, i32 (%struct.avifCodec*, %struct.avifImage*)** %getNextImage, align 4, !tbaa !168
  %13 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec4 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %13, i32 0, i32 1
  %14 = load %struct.avifCodec*, %struct.avifCodec** %codec4, align 4, !tbaa !134
  %15 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %image = getelementptr inbounds %struct.avifTile, %struct.avifTile* %15, i32 0, i32 2
  %16 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !135
  %call = call i32 %12(%struct.avifCodec* %14, %struct.avifImage* %16)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end10, label %if.then

if.then:                                          ; preds = %for.body
  %17 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %input = getelementptr inbounds %struct.avifTile, %struct.avifTile* %17, i32 0, i32 0
  %18 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input, align 4, !tbaa !49
  %alpha = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %18, i32 0, i32 1
  %19 = load i32, i32* %alpha, align 4, !tbaa !52
  %tobool5 = icmp ne i32 %19, 0
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  store i32 12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then
  %20 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %image7 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %20, i32 0, i32 2
  %21 = load %struct.avifImage*, %struct.avifImage** %image7, align 4, !tbaa !135
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %21, i32 0, i32 0
  %22 = load i32, i32* %width, align 4, !tbaa !65
  %tobool8 = icmp ne i32 %22, 0
  br i1 %tobool8, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.else
  store i32 16, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.else
  store i32 11, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end10, %if.end, %if.then9, %if.then6
  %23 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup11 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %24 = load i32, i32* %tileIndex, align 4, !tbaa !12
  %inc = add i32 %24, 1
  store i32 %inc, i32* %tileIndex, align 4, !tbaa !12
  br label %for.cond

cleanup11:                                        ; preds = %cleanup, %for.cond.cleanup
  %25 = bitcast i32* %tileIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %cleanup.dest12 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest12, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup11
  %26 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data13 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %26, i32 0, i32 11
  %27 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data13, align 4, !tbaa !18
  %tiles14 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %27, i32 0, i32 4
  %count15 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles14, i32 0, i32 2
  %28 = load i32, i32* %count15, align 4, !tbaa !92
  %29 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data16 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %29, i32 0, i32 11
  %30 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data16, align 4, !tbaa !18
  %colorTileCount = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %30, i32 0, i32 5
  %31 = load i32, i32* %colorTileCount, align 4, !tbaa !51
  %32 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data17 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %32, i32 0, i32 11
  %33 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data17, align 4, !tbaa !18
  %alphaTileCount = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %33, i32 0, i32 6
  %34 = load i32, i32* %alphaTileCount, align 4, !tbaa !55
  %add = add i32 %31, %34
  %cmp18 = icmp ne i32 %28, %add
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %for.end
  store i32 1, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %for.end
  %35 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data21 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %35, i32 0, i32 11
  %36 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data21, align 4, !tbaa !18
  %colorGrid = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %36, i32 0, i32 7
  %rows = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %colorGrid, i32 0, i32 0
  %37 = load i8, i8* %rows, align 4, !tbaa !90
  %conv = zext i8 %37 to i32
  %cmp22 = icmp sgt i32 %conv, 0
  br i1 %cmp22, label %if.then29, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end20
  %38 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data24 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %38, i32 0, i32 11
  %39 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data24, align 4, !tbaa !18
  %colorGrid25 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %39, i32 0, i32 7
  %columns = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %colorGrid25, i32 0, i32 1
  %40 = load i8, i8* %columns, align 1, !tbaa !91
  %conv26 = zext i8 %40 to i32
  %cmp27 = icmp sgt i32 %conv26, 0
  br i1 %cmp27, label %if.then29, label %if.else40

if.then29:                                        ; preds = %lor.lhs.false, %if.end20
  %41 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data30 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %41, i32 0, i32 11
  %42 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data30, align 4, !tbaa !18
  %43 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data31 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %43, i32 0, i32 11
  %44 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data31, align 4, !tbaa !18
  %colorGrid32 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %44, i32 0, i32 7
  %45 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image33 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %45, i32 0, i32 2
  %46 = load %struct.avifImage*, %struct.avifImage** %image33, align 8, !tbaa !24
  %47 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data34 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %47, i32 0, i32 11
  %48 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data34, align 4, !tbaa !18
  %colorTileCount35 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %48, i32 0, i32 5
  %49 = load i32, i32* %colorTileCount35, align 4, !tbaa !51
  %call36 = call i32 @avifDecoderDataFillImageGrid(%struct.avifDecoderData* %42, %struct.avifImageGrid* %colorGrid32, %struct.avifImage* %46, i32 0, i32 %49, i32 0)
  %tobool37 = icmp ne i32 %call36, 0
  br i1 %tobool37, label %if.end39, label %if.then38

if.then38:                                        ; preds = %if.then29
  store i32 18, i32* %retval, align 4
  br label %return

if.end39:                                         ; preds = %if.then29
  br label %if.end80

if.else40:                                        ; preds = %lor.lhs.false
  %50 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data41 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %50, i32 0, i32 11
  %51 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data41, align 4, !tbaa !18
  %colorTileCount42 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %51, i32 0, i32 5
  %52 = load i32, i32* %colorTileCount42, align 4, !tbaa !51
  %cmp43 = icmp ne i32 %52, 1
  br i1 %cmp43, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.else40
  store i32 11, i32* %retval, align 4
  br label %return

if.end46:                                         ; preds = %if.else40
  %53 = bitcast %struct.avifImage** %srcColor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #4
  %54 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data47 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %54, i32 0, i32 11
  %55 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data47, align 4, !tbaa !18
  %tiles48 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %55, i32 0, i32 4
  %tile49 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles48, i32 0, i32 0
  %56 = load %struct.avifTile*, %struct.avifTile** %tile49, align 4, !tbaa !104
  %arrayidx50 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %56, i32 0
  %image51 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %arrayidx50, i32 0, i32 2
  %57 = load %struct.avifImage*, %struct.avifImage** %image51, align 4, !tbaa !135
  store %struct.avifImage* %57, %struct.avifImage** %srcColor, align 4, !tbaa !2
  %58 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image52 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %58, i32 0, i32 2
  %59 = load %struct.avifImage*, %struct.avifImage** %image52, align 8, !tbaa !24
  %width53 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %59, i32 0, i32 0
  %60 = load i32, i32* %width53, align 4, !tbaa !65
  %61 = load %struct.avifImage*, %struct.avifImage** %srcColor, align 4, !tbaa !2
  %width54 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %61, i32 0, i32 0
  %62 = load i32, i32* %width54, align 4, !tbaa !65
  %cmp55 = icmp ne i32 %60, %62
  br i1 %cmp55, label %if.then67, label %lor.lhs.false57

lor.lhs.false57:                                  ; preds = %if.end46
  %63 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image58 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %63, i32 0, i32 2
  %64 = load %struct.avifImage*, %struct.avifImage** %image58, align 8, !tbaa !24
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %64, i32 0, i32 1
  %65 = load i32, i32* %height, align 4, !tbaa !73
  %66 = load %struct.avifImage*, %struct.avifImage** %srcColor, align 4, !tbaa !2
  %height59 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %66, i32 0, i32 1
  %67 = load i32, i32* %height59, align 4, !tbaa !73
  %cmp60 = icmp ne i32 %65, %67
  br i1 %cmp60, label %if.then67, label %lor.lhs.false62

lor.lhs.false62:                                  ; preds = %lor.lhs.false57
  %68 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image63 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %68, i32 0, i32 2
  %69 = load %struct.avifImage*, %struct.avifImage** %image63, align 8, !tbaa !24
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %69, i32 0, i32 2
  %70 = load i32, i32* %depth, align 4, !tbaa !118
  %71 = load %struct.avifImage*, %struct.avifImage** %srcColor, align 4, !tbaa !2
  %depth64 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %71, i32 0, i32 2
  %72 = load i32, i32* %depth64, align 4, !tbaa !118
  %cmp65 = icmp ne i32 %70, %72
  br i1 %cmp65, label %if.then67, label %if.end78

if.then67:                                        ; preds = %lor.lhs.false62, %lor.lhs.false57, %if.end46
  %73 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image68 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %73, i32 0, i32 2
  %74 = load %struct.avifImage*, %struct.avifImage** %image68, align 8, !tbaa !24
  call void @avifImageFreePlanes(%struct.avifImage* %74, i32 255)
  %75 = load %struct.avifImage*, %struct.avifImage** %srcColor, align 4, !tbaa !2
  %width69 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %75, i32 0, i32 0
  %76 = load i32, i32* %width69, align 4, !tbaa !65
  %77 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image70 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %77, i32 0, i32 2
  %78 = load %struct.avifImage*, %struct.avifImage** %image70, align 8, !tbaa !24
  %width71 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %78, i32 0, i32 0
  store i32 %76, i32* %width71, align 4, !tbaa !65
  %79 = load %struct.avifImage*, %struct.avifImage** %srcColor, align 4, !tbaa !2
  %height72 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %79, i32 0, i32 1
  %80 = load i32, i32* %height72, align 4, !tbaa !73
  %81 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image73 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %81, i32 0, i32 2
  %82 = load %struct.avifImage*, %struct.avifImage** %image73, align 8, !tbaa !24
  %height74 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %82, i32 0, i32 1
  store i32 %80, i32* %height74, align 4, !tbaa !73
  %83 = load %struct.avifImage*, %struct.avifImage** %srcColor, align 4, !tbaa !2
  %depth75 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %83, i32 0, i32 2
  %84 = load i32, i32* %depth75, align 4, !tbaa !118
  %85 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image76 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %85, i32 0, i32 2
  %86 = load %struct.avifImage*, %struct.avifImage** %image76, align 8, !tbaa !24
  %depth77 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %86, i32 0, i32 2
  store i32 %84, i32* %depth77, align 4, !tbaa !118
  br label %if.end78

if.end78:                                         ; preds = %if.then67, %lor.lhs.false62
  %87 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image79 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %87, i32 0, i32 2
  %88 = load %struct.avifImage*, %struct.avifImage** %image79, align 8, !tbaa !24
  %89 = load %struct.avifImage*, %struct.avifImage** %srcColor, align 4, !tbaa !2
  call void @avifImageStealPlanes(%struct.avifImage* %88, %struct.avifImage* %89, i32 1)
  %90 = bitcast %struct.avifImage** %srcColor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  br label %if.end80

if.end80:                                         ; preds = %if.end78, %if.end39
  %91 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data81 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %91, i32 0, i32 11
  %92 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data81, align 4, !tbaa !18
  %alphaGrid = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %92, i32 0, i32 8
  %rows82 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %alphaGrid, i32 0, i32 0
  %93 = load i8, i8* %rows82, align 4, !tbaa !95
  %conv83 = zext i8 %93 to i32
  %cmp84 = icmp sgt i32 %conv83, 0
  br i1 %cmp84, label %if.then93, label %lor.lhs.false86

lor.lhs.false86:                                  ; preds = %if.end80
  %94 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data87 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %94, i32 0, i32 11
  %95 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data87, align 4, !tbaa !18
  %alphaGrid88 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %95, i32 0, i32 8
  %columns89 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %alphaGrid88, i32 0, i32 1
  %96 = load i8, i8* %columns89, align 1, !tbaa !96
  %conv90 = zext i8 %96 to i32
  %cmp91 = icmp sgt i32 %conv90, 0
  br i1 %cmp91, label %if.then93, label %if.else106

if.then93:                                        ; preds = %lor.lhs.false86, %if.end80
  %97 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data94 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %97, i32 0, i32 11
  %98 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data94, align 4, !tbaa !18
  %99 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data95 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %99, i32 0, i32 11
  %100 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data95, align 4, !tbaa !18
  %alphaGrid96 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %100, i32 0, i32 8
  %101 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image97 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %101, i32 0, i32 2
  %102 = load %struct.avifImage*, %struct.avifImage** %image97, align 8, !tbaa !24
  %103 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data98 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %103, i32 0, i32 11
  %104 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data98, align 4, !tbaa !18
  %colorTileCount99 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %104, i32 0, i32 5
  %105 = load i32, i32* %colorTileCount99, align 4, !tbaa !51
  %106 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data100 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %106, i32 0, i32 11
  %107 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data100, align 4, !tbaa !18
  %alphaTileCount101 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %107, i32 0, i32 6
  %108 = load i32, i32* %alphaTileCount101, align 4, !tbaa !55
  %call102 = call i32 @avifDecoderDataFillImageGrid(%struct.avifDecoderData* %98, %struct.avifImageGrid* %alphaGrid96, %struct.avifImage* %102, i32 %105, i32 %108, i32 1)
  %tobool103 = icmp ne i32 %call102, 0
  br i1 %tobool103, label %if.end105, label %if.then104

if.then104:                                       ; preds = %if.then93
  store i32 18, i32* %retval, align 4
  br label %return

if.end105:                                        ; preds = %if.then93
  br label %if.end151

if.else106:                                       ; preds = %lor.lhs.false86
  %109 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data107 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %109, i32 0, i32 11
  %110 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data107, align 4, !tbaa !18
  %alphaTileCount108 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %110, i32 0, i32 6
  %111 = load i32, i32* %alphaTileCount108, align 4, !tbaa !55
  %cmp109 = icmp eq i32 %111, 0
  br i1 %cmp109, label %if.then111, label %if.else113

if.then111:                                       ; preds = %if.else106
  %112 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image112 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %112, i32 0, i32 2
  %113 = load %struct.avifImage*, %struct.avifImage** %image112, align 8, !tbaa !24
  call void @avifImageFreePlanes(%struct.avifImage* %113, i32 2)
  br label %if.end150

if.else113:                                       ; preds = %if.else106
  %114 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data114 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %114, i32 0, i32 11
  %115 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data114, align 4, !tbaa !18
  %alphaTileCount115 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %115, i32 0, i32 6
  %116 = load i32, i32* %alphaTileCount115, align 4, !tbaa !55
  %cmp116 = icmp ne i32 %116, 1
  br i1 %cmp116, label %if.then118, label %if.end119

if.then118:                                       ; preds = %if.else113
  store i32 12, i32* %retval, align 4
  br label %return

if.end119:                                        ; preds = %if.else113
  %117 = bitcast %struct.avifImage** %srcAlpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #4
  %118 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data120 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %118, i32 0, i32 11
  %119 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data120, align 4, !tbaa !18
  %tiles121 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %119, i32 0, i32 4
  %tile122 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles121, i32 0, i32 0
  %120 = load %struct.avifTile*, %struct.avifTile** %tile122, align 4, !tbaa !104
  %121 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data123 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %121, i32 0, i32 11
  %122 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data123, align 4, !tbaa !18
  %colorTileCount124 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %122, i32 0, i32 5
  %123 = load i32, i32* %colorTileCount124, align 4, !tbaa !51
  %arrayidx125 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %120, i32 %123
  %image126 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %arrayidx125, i32 0, i32 2
  %124 = load %struct.avifImage*, %struct.avifImage** %image126, align 4, !tbaa !135
  store %struct.avifImage* %124, %struct.avifImage** %srcAlpha, align 4, !tbaa !2
  %125 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image127 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %125, i32 0, i32 2
  %126 = load %struct.avifImage*, %struct.avifImage** %image127, align 8, !tbaa !24
  %width128 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %126, i32 0, i32 0
  %127 = load i32, i32* %width128, align 4, !tbaa !65
  %128 = load %struct.avifImage*, %struct.avifImage** %srcAlpha, align 4, !tbaa !2
  %width129 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %128, i32 0, i32 0
  %129 = load i32, i32* %width129, align 4, !tbaa !65
  %cmp130 = icmp ne i32 %127, %129
  br i1 %cmp130, label %if.then144, label %lor.lhs.false132

lor.lhs.false132:                                 ; preds = %if.end119
  %130 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image133 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %130, i32 0, i32 2
  %131 = load %struct.avifImage*, %struct.avifImage** %image133, align 8, !tbaa !24
  %height134 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %131, i32 0, i32 1
  %132 = load i32, i32* %height134, align 4, !tbaa !73
  %133 = load %struct.avifImage*, %struct.avifImage** %srcAlpha, align 4, !tbaa !2
  %height135 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %133, i32 0, i32 1
  %134 = load i32, i32* %height135, align 4, !tbaa !73
  %cmp136 = icmp ne i32 %132, %134
  br i1 %cmp136, label %if.then144, label %lor.lhs.false138

lor.lhs.false138:                                 ; preds = %lor.lhs.false132
  %135 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image139 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %135, i32 0, i32 2
  %136 = load %struct.avifImage*, %struct.avifImage** %image139, align 8, !tbaa !24
  %depth140 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %136, i32 0, i32 2
  %137 = load i32, i32* %depth140, align 4, !tbaa !118
  %138 = load %struct.avifImage*, %struct.avifImage** %srcAlpha, align 4, !tbaa !2
  %depth141 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %138, i32 0, i32 2
  %139 = load i32, i32* %depth141, align 4, !tbaa !118
  %cmp142 = icmp ne i32 %137, %139
  br i1 %cmp142, label %if.then144, label %if.end145

if.then144:                                       ; preds = %lor.lhs.false138, %lor.lhs.false132, %if.end119
  store i32 12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup147

if.end145:                                        ; preds = %lor.lhs.false138
  %140 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image146 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %140, i32 0, i32 2
  %141 = load %struct.avifImage*, %struct.avifImage** %image146, align 8, !tbaa !24
  %142 = load %struct.avifImage*, %struct.avifImage** %srcAlpha, align 4, !tbaa !2
  call void @avifImageStealPlanes(%struct.avifImage* %141, %struct.avifImage* %142, i32 2)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup147

cleanup147:                                       ; preds = %if.end145, %if.then144
  %143 = bitcast %struct.avifImage** %srcAlpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %cleanup.dest148 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest148, label %unreachable [
    i32 0, label %cleanup.cont149
    i32 1, label %return
  ]

cleanup.cont149:                                  ; preds = %cleanup147
  br label %if.end150

if.end150:                                        ; preds = %cleanup.cont149, %if.then111
  br label %if.end151

if.end151:                                        ; preds = %if.end150, %if.end105
  %144 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %144, i32 0, i32 3
  %145 = load i32, i32* %imageIndex, align 4, !tbaa !56
  %inc152 = add nsw i32 %145, 1
  store i32 %inc152, i32* %imageIndex, align 4, !tbaa !56
  %146 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data153 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %146, i32 0, i32 11
  %147 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data153, align 4, !tbaa !18
  %sourceSampleTable = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %147, i32 0, i32 10
  %148 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sourceSampleTable, align 4, !tbaa !32
  %tobool154 = icmp ne %struct.avifSampleTable* %148, null
  br i1 %tobool154, label %if.then155, label %if.end165

if.then155:                                       ; preds = %if.end151
  %149 = bitcast i32* %timingResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #4
  %150 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %151 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex156 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %151, i32 0, i32 3
  %152 = load i32, i32* %imageIndex156, align 4, !tbaa !56
  %153 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageTiming = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %153, i32 0, i32 5
  %call157 = call i32 @avifDecoderNthImageTiming(%struct.avifDecoder* %150, i32 %152, %struct.avifImageTiming* %imageTiming)
  store i32 %call157, i32* %timingResult, align 4, !tbaa !25
  %154 = load i32, i32* %timingResult, align 4, !tbaa !25
  %cmp158 = icmp ne i32 %154, 0
  br i1 %cmp158, label %if.then160, label %if.end161

if.then160:                                       ; preds = %if.then155
  %155 = load i32, i32* %timingResult, align 4, !tbaa !25
  store i32 %155, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup162

if.end161:                                        ; preds = %if.then155
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup162

cleanup162:                                       ; preds = %if.end161, %if.then160
  %156 = bitcast i32* %timingResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #4
  %cleanup.dest163 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest163, label %unreachable [
    i32 0, label %cleanup.cont164
    i32 1, label %return
  ]

cleanup.cont164:                                  ; preds = %cleanup162
  br label %if.end165

if.end165:                                        ; preds = %cleanup.cont164, %if.end151
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end165, %cleanup162, %cleanup147, %if.then118, %if.then104, %if.then45, %if.then38, %if.then19, %cleanup11
  %157 = load i32, i32* %retval, align 4
  ret i32 %157

unreachable:                                      ; preds = %cleanup162, %cleanup147, %cleanup11
  unreachable
}

; Function Attrs: nounwind
define internal i32 @avifDecoderDataFillImageGrid(%struct.avifDecoderData* %data, %struct.avifImageGrid* %grid, %struct.avifImage* %dstImage, i32 %firstTileIndex, i32 %tileCount, i32 %alpha) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %grid.addr = alloca %struct.avifImageGrid*, align 4
  %dstImage.addr = alloca %struct.avifImage*, align 4
  %firstTileIndex.addr = alloca i32, align 4
  %tileCount.addr = alloca i32, align 4
  %alpha.addr = alloca i32, align 4
  %firstTile = alloca %struct.avifTile*, align 4
  %firstTileUVPresent = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tile7 = alloca %struct.avifTile*, align 4
  %uvPresent = alloca i32, align 4
  %formatInfo = alloca %struct.avifPixelFormatInfo, align 4
  %tileIndex = alloca i32, align 4
  %pixelBytes = alloca i32, align 4
  %rowIndex = alloca i32, align 4
  %colIndex = alloca i32, align 4
  %tile134 = alloca %struct.avifTile*, align 4
  %widthToCopy = alloca i32, align 4
  %maxX = alloca i32, align 4
  %heightToCopy = alloca i32, align 4
  %maxY = alloca i32, align 4
  %yaColOffset = alloca i32, align 4
  %yaRowOffset = alloca i32, align 4
  %yaRowBytes = alloca i32, align 4
  %j = alloca i32, align 4
  %src = alloca i8*, align 4
  %dst = alloca i8*, align 4
  %j193 = alloca i32, align 4
  %src199 = alloca i8*, align 4
  %dst207 = alloca i8*, align 4
  %uvColOffset = alloca i32, align 4
  %uvRowOffset = alloca i32, align 4
  %uvRowBytes = alloca i32, align 4
  %j229 = alloca i32, align 4
  %srcU = alloca i8*, align 4
  %dstU = alloca i8*, align 4
  %srcV = alloca i8*, align 4
  %dstV = alloca i8*, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  store %struct.avifImageGrid* %grid, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  store %struct.avifImage* %dstImage, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  store i32 %firstTileIndex, i32* %firstTileIndex.addr, align 4, !tbaa !12
  store i32 %tileCount, i32* %tileCount.addr, align 4, !tbaa !12
  store i32 %alpha, i32* %alpha.addr, align 4, !tbaa !12
  %0 = load i32, i32* %tileCount.addr, align 4, !tbaa !12
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %struct.avifTile** %firstTile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %2, i32 0, i32 4
  %tile = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles, i32 0, i32 0
  %3 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !104
  %4 = load i32, i32* %firstTileIndex.addr, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifTile, %struct.avifTile* %3, i32 %4
  store %struct.avifTile* %arrayidx, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %5 = bitcast i32* %firstTileUVPresent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image = getelementptr inbounds %struct.avifTile, %struct.avifTile* %6, i32 0, i32 2
  %7 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !135
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %7, i32 0, i32 6
  %arrayidx1 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 1
  %8 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  %tobool = icmp ne i8* %8, null
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end
  %9 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image2 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %9, i32 0, i32 2
  %10 = load %struct.avifImage*, %struct.avifImage** %image2, align 4, !tbaa !135
  %yuvPlanes3 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %10, i32 0, i32 6
  %arrayidx4 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes3, i32 0, i32 2
  %11 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  %tobool5 = icmp ne i8* %11, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end
  %12 = phi i1 [ false, %if.end ], [ %tobool5, %land.rhs ]
  %land.ext = zext i1 %12 to i32
  store i32 %land.ext, i32* %firstTileUVPresent, align 4, !tbaa !12
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  store i32 1, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %land.end
  %14 = load i32, i32* %i, align 4, !tbaa !12
  %15 = load i32, i32* %tileCount.addr, align 4, !tbaa !12
  %cmp6 = icmp ult i32 %14, %15
  br i1 %cmp6, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup65

for.body:                                         ; preds = %for.cond
  %16 = bitcast %struct.avifTile** %tile7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles8 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %17, i32 0, i32 4
  %tile9 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles8, i32 0, i32 0
  %18 = load %struct.avifTile*, %struct.avifTile** %tile9, align 4, !tbaa !104
  %19 = load i32, i32* %firstTileIndex.addr, align 4, !tbaa !12
  %20 = load i32, i32* %i, align 4, !tbaa !12
  %add = add i32 %19, %20
  %arrayidx10 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %18, i32 %add
  store %struct.avifTile* %arrayidx10, %struct.avifTile** %tile7, align 4, !tbaa !2
  %21 = bitcast i32* %uvPresent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image11 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %22, i32 0, i32 2
  %23 = load %struct.avifImage*, %struct.avifImage** %image11, align 4, !tbaa !135
  %yuvPlanes12 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 6
  %arrayidx13 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes12, i32 0, i32 1
  %24 = load i8*, i8** %arrayidx13, align 4, !tbaa !2
  %tobool14 = icmp ne i8* %24, null
  br i1 %tobool14, label %land.rhs15, label %land.end20

land.rhs15:                                       ; preds = %for.body
  %25 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image16 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %25, i32 0, i32 2
  %26 = load %struct.avifImage*, %struct.avifImage** %image16, align 4, !tbaa !135
  %yuvPlanes17 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 6
  %arrayidx18 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes17, i32 0, i32 2
  %27 = load i8*, i8** %arrayidx18, align 4, !tbaa !2
  %tobool19 = icmp ne i8* %27, null
  br label %land.end20

land.end20:                                       ; preds = %land.rhs15, %for.body
  %28 = phi i1 [ false, %for.body ], [ %tobool19, %land.rhs15 ]
  %land.ext21 = zext i1 %28 to i32
  store i32 %land.ext21, i32* %uvPresent, align 4, !tbaa !12
  %29 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image22 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %29, i32 0, i32 2
  %30 = load %struct.avifImage*, %struct.avifImage** %image22, align 4, !tbaa !135
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %30, i32 0, i32 0
  %31 = load i32, i32* %width, align 4, !tbaa !65
  %32 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image23 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %32, i32 0, i32 2
  %33 = load %struct.avifImage*, %struct.avifImage** %image23, align 4, !tbaa !135
  %width24 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %33, i32 0, i32 0
  %34 = load i32, i32* %width24, align 4, !tbaa !65
  %cmp25 = icmp ne i32 %31, %34
  br i1 %cmp25, label %if.then62, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.end20
  %35 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image26 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %35, i32 0, i32 2
  %36 = load %struct.avifImage*, %struct.avifImage** %image26, align 4, !tbaa !135
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %36, i32 0, i32 1
  %37 = load i32, i32* %height, align 4, !tbaa !73
  %38 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image27 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %38, i32 0, i32 2
  %39 = load %struct.avifImage*, %struct.avifImage** %image27, align 4, !tbaa !135
  %height28 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %39, i32 0, i32 1
  %40 = load i32, i32* %height28, align 4, !tbaa !73
  %cmp29 = icmp ne i32 %37, %40
  br i1 %cmp29, label %if.then62, label %lor.lhs.false30

lor.lhs.false30:                                  ; preds = %lor.lhs.false
  %41 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image31 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %41, i32 0, i32 2
  %42 = load %struct.avifImage*, %struct.avifImage** %image31, align 4, !tbaa !135
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %42, i32 0, i32 2
  %43 = load i32, i32* %depth, align 4, !tbaa !118
  %44 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image32 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %44, i32 0, i32 2
  %45 = load %struct.avifImage*, %struct.avifImage** %image32, align 4, !tbaa !135
  %depth33 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %45, i32 0, i32 2
  %46 = load i32, i32* %depth33, align 4, !tbaa !118
  %cmp34 = icmp ne i32 %43, %46
  br i1 %cmp34, label %if.then62, label %lor.lhs.false35

lor.lhs.false35:                                  ; preds = %lor.lhs.false30
  %47 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image36 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %47, i32 0, i32 2
  %48 = load %struct.avifImage*, %struct.avifImage** %image36, align 4, !tbaa !135
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %48, i32 0, i32 3
  %49 = load i32, i32* %yuvFormat, align 4, !tbaa !119
  %50 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image37 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %50, i32 0, i32 2
  %51 = load %struct.avifImage*, %struct.avifImage** %image37, align 4, !tbaa !135
  %yuvFormat38 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %51, i32 0, i32 3
  %52 = load i32, i32* %yuvFormat38, align 4, !tbaa !119
  %cmp39 = icmp ne i32 %49, %52
  br i1 %cmp39, label %if.then62, label %lor.lhs.false40

lor.lhs.false40:                                  ; preds = %lor.lhs.false35
  %53 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image41 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %53, i32 0, i32 2
  %54 = load %struct.avifImage*, %struct.avifImage** %image41, align 4, !tbaa !135
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %54, i32 0, i32 4
  %55 = load i32, i32* %yuvRange, align 4, !tbaa !111
  %56 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image42 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %56, i32 0, i32 2
  %57 = load %struct.avifImage*, %struct.avifImage** %image42, align 4, !tbaa !135
  %yuvRange43 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %57, i32 0, i32 4
  %58 = load i32, i32* %yuvRange43, align 4, !tbaa !111
  %cmp44 = icmp ne i32 %55, %58
  br i1 %cmp44, label %if.then62, label %lor.lhs.false45

lor.lhs.false45:                                  ; preds = %lor.lhs.false40
  %59 = load i32, i32* %uvPresent, align 4, !tbaa !12
  %60 = load i32, i32* %firstTileUVPresent, align 4, !tbaa !12
  %cmp46 = icmp ne i32 %59, %60
  br i1 %cmp46, label %if.then62, label %lor.lhs.false47

lor.lhs.false47:                                  ; preds = %lor.lhs.false45
  %61 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image48 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %61, i32 0, i32 2
  %62 = load %struct.avifImage*, %struct.avifImage** %image48, align 4, !tbaa !135
  %colorPrimaries = getelementptr inbounds %struct.avifImage, %struct.avifImage* %62, i32 0, i32 14
  %63 = load i32, i32* %colorPrimaries, align 4, !tbaa !108
  %64 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image49 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %64, i32 0, i32 2
  %65 = load %struct.avifImage*, %struct.avifImage** %image49, align 4, !tbaa !135
  %colorPrimaries50 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %65, i32 0, i32 14
  %66 = load i32, i32* %colorPrimaries50, align 4, !tbaa !108
  %cmp51 = icmp ne i32 %63, %66
  br i1 %cmp51, label %if.then62, label %lor.lhs.false52

lor.lhs.false52:                                  ; preds = %lor.lhs.false47
  %67 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image53 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %67, i32 0, i32 2
  %68 = load %struct.avifImage*, %struct.avifImage** %image53, align 4, !tbaa !135
  %transferCharacteristics = getelementptr inbounds %struct.avifImage, %struct.avifImage* %68, i32 0, i32 15
  %69 = load i32, i32* %transferCharacteristics, align 4, !tbaa !109
  %70 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image54 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %70, i32 0, i32 2
  %71 = load %struct.avifImage*, %struct.avifImage** %image54, align 4, !tbaa !135
  %transferCharacteristics55 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %71, i32 0, i32 15
  %72 = load i32, i32* %transferCharacteristics55, align 4, !tbaa !109
  %cmp56 = icmp ne i32 %69, %72
  br i1 %cmp56, label %if.then62, label %lor.lhs.false57

lor.lhs.false57:                                  ; preds = %lor.lhs.false52
  %73 = load %struct.avifTile*, %struct.avifTile** %tile7, align 4, !tbaa !2
  %image58 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %73, i32 0, i32 2
  %74 = load %struct.avifImage*, %struct.avifImage** %image58, align 4, !tbaa !135
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %74, i32 0, i32 16
  %75 = load i32, i32* %matrixCoefficients, align 4, !tbaa !110
  %76 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image59 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %76, i32 0, i32 2
  %77 = load %struct.avifImage*, %struct.avifImage** %image59, align 4, !tbaa !135
  %matrixCoefficients60 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %77, i32 0, i32 16
  %78 = load i32, i32* %matrixCoefficients60, align 4, !tbaa !110
  %cmp61 = icmp ne i32 %75, %78
  br i1 %cmp61, label %if.then62, label %if.end63

if.then62:                                        ; preds = %lor.lhs.false57, %lor.lhs.false52, %lor.lhs.false47, %lor.lhs.false45, %lor.lhs.false40, %lor.lhs.false35, %lor.lhs.false30, %lor.lhs.false, %land.end20
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end63:                                         ; preds = %lor.lhs.false57
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end63, %if.then62
  %79 = bitcast i32* %uvPresent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast %struct.avifTile** %tile7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup65 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %81 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %81, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup65:                                        ; preds = %cleanup, %for.cond.cleanup
  %82 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %cleanup.dest66 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest66, label %cleanup296 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup65
  %83 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %width67 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %83, i32 0, i32 0
  %84 = load i32, i32* %width67, align 4, !tbaa !65
  %85 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputWidth = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %85, i32 0, i32 2
  %86 = load i32, i32* %outputWidth, align 4, !tbaa !157
  %cmp68 = icmp ne i32 %84, %86
  br i1 %cmp68, label %if.then82, label %lor.lhs.false69

lor.lhs.false69:                                  ; preds = %for.end
  %87 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %height70 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %87, i32 0, i32 1
  %88 = load i32, i32* %height70, align 4, !tbaa !73
  %89 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputHeight = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %89, i32 0, i32 3
  %90 = load i32, i32* %outputHeight, align 4, !tbaa !158
  %cmp71 = icmp ne i32 %88, %90
  br i1 %cmp71, label %if.then82, label %lor.lhs.false72

lor.lhs.false72:                                  ; preds = %lor.lhs.false69
  %91 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %depth73 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %91, i32 0, i32 2
  %92 = load i32, i32* %depth73, align 4, !tbaa !118
  %93 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image74 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %93, i32 0, i32 2
  %94 = load %struct.avifImage*, %struct.avifImage** %image74, align 4, !tbaa !135
  %depth75 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %94, i32 0, i32 2
  %95 = load i32, i32* %depth75, align 4, !tbaa !118
  %cmp76 = icmp ne i32 %92, %95
  br i1 %cmp76, label %if.then82, label %lor.lhs.false77

lor.lhs.false77:                                  ; preds = %lor.lhs.false72
  %96 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvFormat78 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %96, i32 0, i32 3
  %97 = load i32, i32* %yuvFormat78, align 4, !tbaa !119
  %98 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image79 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %98, i32 0, i32 2
  %99 = load %struct.avifImage*, %struct.avifImage** %image79, align 4, !tbaa !135
  %yuvFormat80 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %99, i32 0, i32 3
  %100 = load i32, i32* %yuvFormat80, align 4, !tbaa !119
  %cmp81 = icmp ne i32 %97, %100
  br i1 %cmp81, label %if.then82, label %if.end112

if.then82:                                        ; preds = %lor.lhs.false77, %lor.lhs.false72, %lor.lhs.false69, %for.end
  %101 = load i32, i32* %alpha.addr, align 4, !tbaa !12
  %tobool83 = icmp ne i32 %101, 0
  br i1 %tobool83, label %if.then84, label %if.end85

if.then84:                                        ; preds = %if.then82
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup296

if.end85:                                         ; preds = %if.then82
  %102 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  call void @avifImageFreePlanes(%struct.avifImage* %102, i32 255)
  %103 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputWidth86 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %103, i32 0, i32 2
  %104 = load i32, i32* %outputWidth86, align 4, !tbaa !157
  %105 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %width87 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %105, i32 0, i32 0
  store i32 %104, i32* %width87, align 4, !tbaa !65
  %106 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputHeight88 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %106, i32 0, i32 3
  %107 = load i32, i32* %outputHeight88, align 4, !tbaa !158
  %108 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %height89 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %108, i32 0, i32 1
  store i32 %107, i32* %height89, align 4, !tbaa !73
  %109 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image90 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %109, i32 0, i32 2
  %110 = load %struct.avifImage*, %struct.avifImage** %image90, align 4, !tbaa !135
  %depth91 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %110, i32 0, i32 2
  %111 = load i32, i32* %depth91, align 4, !tbaa !118
  %112 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %depth92 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %112, i32 0, i32 2
  store i32 %111, i32* %depth92, align 4, !tbaa !118
  %113 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image93 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %113, i32 0, i32 2
  %114 = load %struct.avifImage*, %struct.avifImage** %image93, align 4, !tbaa !135
  %yuvFormat94 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %114, i32 0, i32 3
  %115 = load i32, i32* %yuvFormat94, align 4, !tbaa !119
  %116 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvFormat95 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %116, i32 0, i32 3
  store i32 %115, i32* %yuvFormat95, align 4, !tbaa !119
  %117 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image96 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %117, i32 0, i32 2
  %118 = load %struct.avifImage*, %struct.avifImage** %image96, align 4, !tbaa !135
  %yuvRange97 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %118, i32 0, i32 4
  %119 = load i32, i32* %yuvRange97, align 4, !tbaa !111
  %120 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvRange98 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %120, i32 0, i32 4
  store i32 %119, i32* %yuvRange98, align 4, !tbaa !111
  %121 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %cicpSet = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %121, i32 0, i32 11
  %122 = load i32, i32* %cicpSet, align 4, !tbaa !27
  %tobool99 = icmp ne i32 %122, 0
  br i1 %tobool99, label %if.end111, label %if.then100

if.then100:                                       ; preds = %if.end85
  %123 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %cicpSet101 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %123, i32 0, i32 11
  store i32 1, i32* %cicpSet101, align 4, !tbaa !27
  %124 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image102 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %124, i32 0, i32 2
  %125 = load %struct.avifImage*, %struct.avifImage** %image102, align 4, !tbaa !135
  %colorPrimaries103 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %125, i32 0, i32 14
  %126 = load i32, i32* %colorPrimaries103, align 4, !tbaa !108
  %127 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %colorPrimaries104 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %127, i32 0, i32 14
  store i32 %126, i32* %colorPrimaries104, align 4, !tbaa !108
  %128 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image105 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %128, i32 0, i32 2
  %129 = load %struct.avifImage*, %struct.avifImage** %image105, align 4, !tbaa !135
  %transferCharacteristics106 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %129, i32 0, i32 15
  %130 = load i32, i32* %transferCharacteristics106, align 4, !tbaa !109
  %131 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %transferCharacteristics107 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %131, i32 0, i32 15
  store i32 %130, i32* %transferCharacteristics107, align 4, !tbaa !109
  %132 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image108 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %132, i32 0, i32 2
  %133 = load %struct.avifImage*, %struct.avifImage** %image108, align 4, !tbaa !135
  %matrixCoefficients109 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %133, i32 0, i32 16
  %134 = load i32, i32* %matrixCoefficients109, align 4, !tbaa !110
  %135 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %matrixCoefficients110 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %135, i32 0, i32 16
  store i32 %134, i32* %matrixCoefficients110, align 4, !tbaa !110
  br label %if.end111

if.end111:                                        ; preds = %if.then100, %if.end85
  br label %if.end112

if.end112:                                        ; preds = %if.end111, %lor.lhs.false77
  %136 = load i32, i32* %alpha.addr, align 4, !tbaa !12
  %tobool113 = icmp ne i32 %136, 0
  br i1 %tobool113, label %if.then114, label %if.end117

if.then114:                                       ; preds = %if.end112
  %137 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image115 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %137, i32 0, i32 2
  %138 = load %struct.avifImage*, %struct.avifImage** %image115, align 4, !tbaa !135
  %alphaRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %138, i32 0, i32 9
  %139 = load i32, i32* %alphaRange, align 4, !tbaa !169
  %140 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %alphaRange116 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %140, i32 0, i32 9
  store i32 %139, i32* %alphaRange116, align 4, !tbaa !169
  br label %if.end117

if.end117:                                        ; preds = %if.then114, %if.end112
  %141 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %142 = load i32, i32* %alpha.addr, align 4, !tbaa !12
  %tobool118 = icmp ne i32 %142, 0
  %143 = zext i1 %tobool118 to i64
  %cond = select i1 %tobool118, i32 2, i32 1
  call void @avifImageAllocatePlanes(%struct.avifImage* %141, i32 %cond)
  %144 = bitcast %struct.avifPixelFormatInfo* %formatInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %144) #4
  %145 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image119 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %145, i32 0, i32 2
  %146 = load %struct.avifImage*, %struct.avifImage** %image119, align 4, !tbaa !135
  %yuvFormat120 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %146, i32 0, i32 3
  %147 = load i32, i32* %yuvFormat120, align 4, !tbaa !119
  call void @avifGetPixelFormatInfo(i32 %147, %struct.avifPixelFormatInfo* %formatInfo)
  %148 = bitcast i32* %tileIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #4
  %149 = load i32, i32* %firstTileIndex.addr, align 4, !tbaa !12
  store i32 %149, i32* %tileIndex, align 4, !tbaa !12
  %150 = bitcast i32* %pixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #4
  %151 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %call = call i32 @avifImageUsesU16(%struct.avifImage* %151)
  %tobool121 = icmp ne i32 %call, 0
  %152 = zext i1 %tobool121 to i64
  %cond122 = select i1 %tobool121, i32 2, i32 1
  store i32 %cond122, i32* %pixelBytes, align 4, !tbaa !14
  %153 = bitcast i32* %rowIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %153) #4
  store i32 0, i32* %rowIndex, align 4, !tbaa !12
  br label %for.cond123

for.cond123:                                      ; preds = %for.inc289, %if.end117
  %154 = load i32, i32* %rowIndex, align 4, !tbaa !12
  %155 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %155, i32 0, i32 0
  %156 = load i8, i8* %rows, align 4, !tbaa !153
  %conv = zext i8 %156 to i32
  %cmp124 = icmp ult i32 %154, %conv
  br i1 %cmp124, label %for.body127, label %for.cond.cleanup126

for.cond.cleanup126:                              ; preds = %for.cond123
  store i32 5, i32* %cleanup.dest.slot, align 4
  %157 = bitcast i32* %rowIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  br label %for.end292

for.body127:                                      ; preds = %for.cond123
  %158 = bitcast i32* %colIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %158) #4
  store i32 0, i32* %colIndex, align 4, !tbaa !12
  br label %for.cond128

for.cond128:                                      ; preds = %for.inc284, %for.body127
  %159 = load i32, i32* %colIndex, align 4, !tbaa !12
  %160 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %columns = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %160, i32 0, i32 1
  %161 = load i8, i8* %columns, align 1, !tbaa !154
  %conv129 = zext i8 %161 to i32
  %cmp130 = icmp ult i32 %159, %conv129
  br i1 %cmp130, label %for.body133, label %for.cond.cleanup132

for.cond.cleanup132:                              ; preds = %for.cond128
  store i32 8, i32* %cleanup.dest.slot, align 4
  %162 = bitcast i32* %colIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #4
  br label %for.end288

for.body133:                                      ; preds = %for.cond128
  %163 = bitcast %struct.avifTile** %tile134 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %163) #4
  %164 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles135 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %164, i32 0, i32 4
  %tile136 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles135, i32 0, i32 0
  %165 = load %struct.avifTile*, %struct.avifTile** %tile136, align 4, !tbaa !104
  %166 = load i32, i32* %tileIndex, align 4, !tbaa !12
  %arrayidx137 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %165, i32 %166
  store %struct.avifTile* %arrayidx137, %struct.avifTile** %tile134, align 4, !tbaa !2
  %167 = bitcast i32* %widthToCopy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %167) #4
  %168 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image138 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %168, i32 0, i32 2
  %169 = load %struct.avifImage*, %struct.avifImage** %image138, align 4, !tbaa !135
  %width139 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %169, i32 0, i32 0
  %170 = load i32, i32* %width139, align 4, !tbaa !65
  store i32 %170, i32* %widthToCopy, align 4, !tbaa !12
  %171 = bitcast i32* %maxX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %171) #4
  %172 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image140 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %172, i32 0, i32 2
  %173 = load %struct.avifImage*, %struct.avifImage** %image140, align 4, !tbaa !135
  %width141 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %173, i32 0, i32 0
  %174 = load i32, i32* %width141, align 4, !tbaa !65
  %175 = load i32, i32* %colIndex, align 4, !tbaa !12
  %add142 = add i32 %175, 1
  %mul = mul i32 %174, %add142
  store i32 %mul, i32* %maxX, align 4, !tbaa !12
  %176 = load i32, i32* %maxX, align 4, !tbaa !12
  %177 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputWidth143 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %177, i32 0, i32 2
  %178 = load i32, i32* %outputWidth143, align 4, !tbaa !157
  %cmp144 = icmp ugt i32 %176, %178
  br i1 %cmp144, label %if.then146, label %if.end149

if.then146:                                       ; preds = %for.body133
  %179 = load i32, i32* %maxX, align 4, !tbaa !12
  %180 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputWidth147 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %180, i32 0, i32 2
  %181 = load i32, i32* %outputWidth147, align 4, !tbaa !157
  %sub = sub i32 %179, %181
  %182 = load i32, i32* %widthToCopy, align 4, !tbaa !12
  %sub148 = sub i32 %182, %sub
  store i32 %sub148, i32* %widthToCopy, align 4, !tbaa !12
  br label %if.end149

if.end149:                                        ; preds = %if.then146, %for.body133
  %183 = bitcast i32* %heightToCopy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %183) #4
  %184 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image150 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %184, i32 0, i32 2
  %185 = load %struct.avifImage*, %struct.avifImage** %image150, align 4, !tbaa !135
  %height151 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %185, i32 0, i32 1
  %186 = load i32, i32* %height151, align 4, !tbaa !73
  store i32 %186, i32* %heightToCopy, align 4, !tbaa !12
  %187 = bitcast i32* %maxY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #4
  %188 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image152 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %188, i32 0, i32 2
  %189 = load %struct.avifImage*, %struct.avifImage** %image152, align 4, !tbaa !135
  %height153 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %189, i32 0, i32 1
  %190 = load i32, i32* %height153, align 4, !tbaa !73
  %191 = load i32, i32* %rowIndex, align 4, !tbaa !12
  %add154 = add i32 %191, 1
  %mul155 = mul i32 %190, %add154
  store i32 %mul155, i32* %maxY, align 4, !tbaa !12
  %192 = load i32, i32* %maxY, align 4, !tbaa !12
  %193 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputHeight156 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %193, i32 0, i32 3
  %194 = load i32, i32* %outputHeight156, align 4, !tbaa !158
  %cmp157 = icmp ugt i32 %192, %194
  br i1 %cmp157, label %if.then159, label %if.end163

if.then159:                                       ; preds = %if.end149
  %195 = load i32, i32* %maxY, align 4, !tbaa !12
  %196 = load %struct.avifImageGrid*, %struct.avifImageGrid** %grid.addr, align 4, !tbaa !2
  %outputHeight160 = getelementptr inbounds %struct.avifImageGrid, %struct.avifImageGrid* %196, i32 0, i32 3
  %197 = load i32, i32* %outputHeight160, align 4, !tbaa !158
  %sub161 = sub i32 %195, %197
  %198 = load i32, i32* %heightToCopy, align 4, !tbaa !12
  %sub162 = sub i32 %198, %sub161
  store i32 %sub162, i32* %heightToCopy, align 4, !tbaa !12
  br label %if.end163

if.end163:                                        ; preds = %if.then159, %if.end149
  %199 = bitcast i32* %yaColOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %199) #4
  %200 = load i32, i32* %colIndex, align 4, !tbaa !12
  %201 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image164 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %201, i32 0, i32 2
  %202 = load %struct.avifImage*, %struct.avifImage** %image164, align 4, !tbaa !135
  %width165 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %202, i32 0, i32 0
  %203 = load i32, i32* %width165, align 4, !tbaa !65
  %mul166 = mul i32 %200, %203
  store i32 %mul166, i32* %yaColOffset, align 4, !tbaa !14
  %204 = bitcast i32* %yaRowOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %204) #4
  %205 = load i32, i32* %rowIndex, align 4, !tbaa !12
  %206 = load %struct.avifTile*, %struct.avifTile** %firstTile, align 4, !tbaa !2
  %image167 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %206, i32 0, i32 2
  %207 = load %struct.avifImage*, %struct.avifImage** %image167, align 4, !tbaa !135
  %height168 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %207, i32 0, i32 1
  %208 = load i32, i32* %height168, align 4, !tbaa !73
  %mul169 = mul i32 %205, %208
  store i32 %mul169, i32* %yaRowOffset, align 4, !tbaa !14
  %209 = bitcast i32* %yaRowBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %209) #4
  %210 = load i32, i32* %widthToCopy, align 4, !tbaa !12
  %211 = load i32, i32* %pixelBytes, align 4, !tbaa !14
  %mul170 = mul i32 %210, %211
  store i32 %mul170, i32* %yaRowBytes, align 4, !tbaa !14
  %212 = load i32, i32* %alpha.addr, align 4, !tbaa !12
  %tobool171 = icmp ne i32 %212, 0
  br i1 %tobool171, label %if.then172, label %if.else

if.then172:                                       ; preds = %if.end163
  %213 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %213) #4
  store i32 0, i32* %j, align 4, !tbaa !12
  br label %for.cond173

for.cond173:                                      ; preds = %for.inc189, %if.then172
  %214 = load i32, i32* %j, align 4, !tbaa !12
  %215 = load i32, i32* %heightToCopy, align 4, !tbaa !12
  %cmp174 = icmp ult i32 %214, %215
  br i1 %cmp174, label %for.body177, label %for.cond.cleanup176

for.cond.cleanup176:                              ; preds = %for.cond173
  store i32 11, i32* %cleanup.dest.slot, align 4
  %216 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #4
  br label %for.end192

for.body177:                                      ; preds = %for.cond173
  %217 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %217) #4
  %218 = load %struct.avifTile*, %struct.avifTile** %tile134, align 4, !tbaa !2
  %image178 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %218, i32 0, i32 2
  %219 = load %struct.avifImage*, %struct.avifImage** %image178, align 4, !tbaa !135
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %219, i32 0, i32 10
  %220 = load i8*, i8** %alphaPlane, align 4, !tbaa !170
  %221 = load i32, i32* %j, align 4, !tbaa !12
  %222 = load %struct.avifTile*, %struct.avifTile** %tile134, align 4, !tbaa !2
  %image179 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %222, i32 0, i32 2
  %223 = load %struct.avifImage*, %struct.avifImage** %image179, align 4, !tbaa !135
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %223, i32 0, i32 11
  %224 = load i32, i32* %alphaRowBytes, align 4, !tbaa !171
  %mul180 = mul i32 %221, %224
  %arrayidx181 = getelementptr inbounds i8, i8* %220, i32 %mul180
  store i8* %arrayidx181, i8** %src, align 4, !tbaa !2
  %225 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %225) #4
  %226 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %alphaPlane182 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %226, i32 0, i32 10
  %227 = load i8*, i8** %alphaPlane182, align 4, !tbaa !170
  %228 = load i32, i32* %yaColOffset, align 4, !tbaa !14
  %229 = load i32, i32* %pixelBytes, align 4, !tbaa !14
  %mul183 = mul i32 %228, %229
  %230 = load i32, i32* %yaRowOffset, align 4, !tbaa !14
  %231 = load i32, i32* %j, align 4, !tbaa !12
  %add184 = add i32 %230, %231
  %232 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %alphaRowBytes185 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %232, i32 0, i32 11
  %233 = load i32, i32* %alphaRowBytes185, align 4, !tbaa !171
  %mul186 = mul i32 %add184, %233
  %add187 = add i32 %mul183, %mul186
  %arrayidx188 = getelementptr inbounds i8, i8* %227, i32 %add187
  store i8* %arrayidx188, i8** %dst, align 4, !tbaa !2
  %234 = load i8*, i8** %dst, align 4, !tbaa !2
  %235 = load i8*, i8** %src, align 4, !tbaa !2
  %236 = load i32, i32* %yaRowBytes, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %234, i8* align 1 %235, i32 %236, i1 false)
  %237 = bitcast i8** %dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #4
  %238 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #4
  br label %for.inc189

for.inc189:                                       ; preds = %for.body177
  %239 = load i32, i32* %j, align 4, !tbaa !12
  %inc190 = add i32 %239, 1
  store i32 %inc190, i32* %j, align 4, !tbaa !12
  br label %for.cond173

for.end192:                                       ; preds = %for.cond.cleanup176
  br label %if.end273

if.else:                                          ; preds = %if.end163
  %240 = bitcast i32* %j193 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %240) #4
  store i32 0, i32* %j193, align 4, !tbaa !12
  br label %for.cond194

for.cond194:                                      ; preds = %for.inc217, %if.else
  %241 = load i32, i32* %j193, align 4, !tbaa !12
  %242 = load i32, i32* %heightToCopy, align 4, !tbaa !12
  %cmp195 = icmp ult i32 %241, %242
  br i1 %cmp195, label %for.body198, label %for.cond.cleanup197

for.cond.cleanup197:                              ; preds = %for.cond194
  store i32 14, i32* %cleanup.dest.slot, align 4
  %243 = bitcast i32* %j193 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #4
  br label %for.end220

for.body198:                                      ; preds = %for.cond194
  %244 = bitcast i8** %src199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %244) #4
  %245 = load %struct.avifTile*, %struct.avifTile** %tile134, align 4, !tbaa !2
  %image200 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %245, i32 0, i32 2
  %246 = load %struct.avifImage*, %struct.avifImage** %image200, align 4, !tbaa !135
  %yuvPlanes201 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %246, i32 0, i32 6
  %arrayidx202 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes201, i32 0, i32 0
  %247 = load i8*, i8** %arrayidx202, align 4, !tbaa !2
  %248 = load i32, i32* %j193, align 4, !tbaa !12
  %249 = load %struct.avifTile*, %struct.avifTile** %tile134, align 4, !tbaa !2
  %image203 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %249, i32 0, i32 2
  %250 = load %struct.avifImage*, %struct.avifImage** %image203, align 4, !tbaa !135
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %250, i32 0, i32 7
  %arrayidx204 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %251 = load i32, i32* %arrayidx204, align 4, !tbaa !12
  %mul205 = mul i32 %248, %251
  %arrayidx206 = getelementptr inbounds i8, i8* %247, i32 %mul205
  store i8* %arrayidx206, i8** %src199, align 4, !tbaa !2
  %252 = bitcast i8** %dst207 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %252) #4
  %253 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvPlanes208 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %253, i32 0, i32 6
  %arrayidx209 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes208, i32 0, i32 0
  %254 = load i8*, i8** %arrayidx209, align 4, !tbaa !2
  %255 = load i32, i32* %yaColOffset, align 4, !tbaa !14
  %256 = load i32, i32* %pixelBytes, align 4, !tbaa !14
  %mul210 = mul i32 %255, %256
  %257 = load i32, i32* %yaRowOffset, align 4, !tbaa !14
  %258 = load i32, i32* %j193, align 4, !tbaa !12
  %add211 = add i32 %257, %258
  %259 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvRowBytes212 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %259, i32 0, i32 7
  %arrayidx213 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes212, i32 0, i32 0
  %260 = load i32, i32* %arrayidx213, align 4, !tbaa !12
  %mul214 = mul i32 %add211, %260
  %add215 = add i32 %mul210, %mul214
  %arrayidx216 = getelementptr inbounds i8, i8* %254, i32 %add215
  store i8* %arrayidx216, i8** %dst207, align 4, !tbaa !2
  %261 = load i8*, i8** %dst207, align 4, !tbaa !2
  %262 = load i8*, i8** %src199, align 4, !tbaa !2
  %263 = load i32, i32* %yaRowBytes, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %261, i8* align 1 %262, i32 %263, i1 false)
  %264 = bitcast i8** %dst207 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #4
  %265 = bitcast i8** %src199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #4
  br label %for.inc217

for.inc217:                                       ; preds = %for.body198
  %266 = load i32, i32* %j193, align 4, !tbaa !12
  %inc218 = add i32 %266, 1
  store i32 %inc218, i32* %j193, align 4, !tbaa !12
  br label %for.cond194

for.end220:                                       ; preds = %for.cond.cleanup197
  %267 = load i32, i32* %firstTileUVPresent, align 4, !tbaa !12
  %tobool221 = icmp ne i32 %267, 0
  br i1 %tobool221, label %if.end223, label %if.then222

if.then222:                                       ; preds = %for.end220
  store i32 10, i32* %cleanup.dest.slot, align 4
  br label %cleanup274

if.end223:                                        ; preds = %for.end220
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %268 = load i32, i32* %chromaShiftY, align 4, !tbaa !172
  %269 = load i32, i32* %heightToCopy, align 4, !tbaa !12
  %shr = lshr i32 %269, %268
  store i32 %shr, i32* %heightToCopy, align 4, !tbaa !12
  %270 = bitcast i32* %uvColOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %270) #4
  %271 = load i32, i32* %yaColOffset, align 4, !tbaa !14
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 1
  %272 = load i32, i32* %chromaShiftX, align 4, !tbaa !174
  %shr224 = lshr i32 %271, %272
  store i32 %shr224, i32* %uvColOffset, align 4, !tbaa !14
  %273 = bitcast i32* %uvRowOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %273) #4
  %274 = load i32, i32* %yaRowOffset, align 4, !tbaa !14
  %chromaShiftY225 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %275 = load i32, i32* %chromaShiftY225, align 4, !tbaa !172
  %shr226 = lshr i32 %274, %275
  store i32 %shr226, i32* %uvRowOffset, align 4, !tbaa !14
  %276 = bitcast i32* %uvRowBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %276) #4
  %277 = load i32, i32* %yaRowBytes, align 4, !tbaa !14
  %chromaShiftX227 = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 1
  %278 = load i32, i32* %chromaShiftX227, align 4, !tbaa !174
  %shr228 = lshr i32 %277, %278
  store i32 %shr228, i32* %uvRowBytes, align 4, !tbaa !14
  %279 = bitcast i32* %j229 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %279) #4
  store i32 0, i32* %j229, align 4, !tbaa !12
  br label %for.cond230

for.cond230:                                      ; preds = %for.inc269, %if.end223
  %280 = load i32, i32* %j229, align 4, !tbaa !12
  %281 = load i32, i32* %heightToCopy, align 4, !tbaa !12
  %cmp231 = icmp ult i32 %280, %281
  br i1 %cmp231, label %for.body234, label %for.cond.cleanup233

for.cond.cleanup233:                              ; preds = %for.cond230
  store i32 17, i32* %cleanup.dest.slot, align 4
  %282 = bitcast i32* %j229 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #4
  br label %for.end272

for.body234:                                      ; preds = %for.cond230
  %283 = bitcast i8** %srcU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %283) #4
  %284 = load %struct.avifTile*, %struct.avifTile** %tile134, align 4, !tbaa !2
  %image235 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %284, i32 0, i32 2
  %285 = load %struct.avifImage*, %struct.avifImage** %image235, align 4, !tbaa !135
  %yuvPlanes236 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %285, i32 0, i32 6
  %arrayidx237 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes236, i32 0, i32 1
  %286 = load i8*, i8** %arrayidx237, align 4, !tbaa !2
  %287 = load i32, i32* %j229, align 4, !tbaa !12
  %288 = load %struct.avifTile*, %struct.avifTile** %tile134, align 4, !tbaa !2
  %image238 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %288, i32 0, i32 2
  %289 = load %struct.avifImage*, %struct.avifImage** %image238, align 4, !tbaa !135
  %yuvRowBytes239 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %289, i32 0, i32 7
  %arrayidx240 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes239, i32 0, i32 1
  %290 = load i32, i32* %arrayidx240, align 4, !tbaa !12
  %mul241 = mul i32 %287, %290
  %arrayidx242 = getelementptr inbounds i8, i8* %286, i32 %mul241
  store i8* %arrayidx242, i8** %srcU, align 4, !tbaa !2
  %291 = bitcast i8** %dstU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %291) #4
  %292 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvPlanes243 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %292, i32 0, i32 6
  %arrayidx244 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes243, i32 0, i32 1
  %293 = load i8*, i8** %arrayidx244, align 4, !tbaa !2
  %294 = load i32, i32* %uvColOffset, align 4, !tbaa !14
  %295 = load i32, i32* %pixelBytes, align 4, !tbaa !14
  %mul245 = mul i32 %294, %295
  %296 = load i32, i32* %uvRowOffset, align 4, !tbaa !14
  %297 = load i32, i32* %j229, align 4, !tbaa !12
  %add246 = add i32 %296, %297
  %298 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvRowBytes247 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %298, i32 0, i32 7
  %arrayidx248 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes247, i32 0, i32 1
  %299 = load i32, i32* %arrayidx248, align 4, !tbaa !12
  %mul249 = mul i32 %add246, %299
  %add250 = add i32 %mul245, %mul249
  %arrayidx251 = getelementptr inbounds i8, i8* %293, i32 %add250
  store i8* %arrayidx251, i8** %dstU, align 4, !tbaa !2
  %300 = load i8*, i8** %dstU, align 4, !tbaa !2
  %301 = load i8*, i8** %srcU, align 4, !tbaa !2
  %302 = load i32, i32* %uvRowBytes, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %300, i8* align 1 %301, i32 %302, i1 false)
  %303 = bitcast i8** %srcV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %303) #4
  %304 = load %struct.avifTile*, %struct.avifTile** %tile134, align 4, !tbaa !2
  %image252 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %304, i32 0, i32 2
  %305 = load %struct.avifImage*, %struct.avifImage** %image252, align 4, !tbaa !135
  %yuvPlanes253 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %305, i32 0, i32 6
  %arrayidx254 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes253, i32 0, i32 2
  %306 = load i8*, i8** %arrayidx254, align 4, !tbaa !2
  %307 = load i32, i32* %j229, align 4, !tbaa !12
  %308 = load %struct.avifTile*, %struct.avifTile** %tile134, align 4, !tbaa !2
  %image255 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %308, i32 0, i32 2
  %309 = load %struct.avifImage*, %struct.avifImage** %image255, align 4, !tbaa !135
  %yuvRowBytes256 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %309, i32 0, i32 7
  %arrayidx257 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes256, i32 0, i32 2
  %310 = load i32, i32* %arrayidx257, align 4, !tbaa !12
  %mul258 = mul i32 %307, %310
  %arrayidx259 = getelementptr inbounds i8, i8* %306, i32 %mul258
  store i8* %arrayidx259, i8** %srcV, align 4, !tbaa !2
  %311 = bitcast i8** %dstV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %311) #4
  %312 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvPlanes260 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %312, i32 0, i32 6
  %arrayidx261 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes260, i32 0, i32 2
  %313 = load i8*, i8** %arrayidx261, align 4, !tbaa !2
  %314 = load i32, i32* %uvColOffset, align 4, !tbaa !14
  %315 = load i32, i32* %pixelBytes, align 4, !tbaa !14
  %mul262 = mul i32 %314, %315
  %316 = load i32, i32* %uvRowOffset, align 4, !tbaa !14
  %317 = load i32, i32* %j229, align 4, !tbaa !12
  %add263 = add i32 %316, %317
  %318 = load %struct.avifImage*, %struct.avifImage** %dstImage.addr, align 4, !tbaa !2
  %yuvRowBytes264 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %318, i32 0, i32 7
  %arrayidx265 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes264, i32 0, i32 2
  %319 = load i32, i32* %arrayidx265, align 4, !tbaa !12
  %mul266 = mul i32 %add263, %319
  %add267 = add i32 %mul262, %mul266
  %arrayidx268 = getelementptr inbounds i8, i8* %313, i32 %add267
  store i8* %arrayidx268, i8** %dstV, align 4, !tbaa !2
  %320 = load i8*, i8** %dstV, align 4, !tbaa !2
  %321 = load i8*, i8** %srcV, align 4, !tbaa !2
  %322 = load i32, i32* %uvRowBytes, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %320, i8* align 1 %321, i32 %322, i1 false)
  %323 = bitcast i8** %dstV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %323) #4
  %324 = bitcast i8** %srcV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %324) #4
  %325 = bitcast i8** %dstU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #4
  %326 = bitcast i8** %srcU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #4
  br label %for.inc269

for.inc269:                                       ; preds = %for.body234
  %327 = load i32, i32* %j229, align 4, !tbaa !12
  %inc270 = add i32 %327, 1
  store i32 %inc270, i32* %j229, align 4, !tbaa !12
  br label %for.cond230

for.end272:                                       ; preds = %for.cond.cleanup233
  %328 = bitcast i32* %uvRowBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %328) #4
  %329 = bitcast i32* %uvRowOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %329) #4
  %330 = bitcast i32* %uvColOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %330) #4
  br label %if.end273

if.end273:                                        ; preds = %for.end272, %for.end192
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup274

cleanup274:                                       ; preds = %if.end273, %if.then222
  %331 = bitcast i32* %yaRowBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #4
  %332 = bitcast i32* %yaRowOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #4
  %333 = bitcast i32* %yaColOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #4
  %334 = bitcast i32* %maxY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #4
  %335 = bitcast i32* %heightToCopy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #4
  %336 = bitcast i32* %maxX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #4
  %337 = bitcast i32* %widthToCopy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #4
  %338 = bitcast %struct.avifTile** %tile134 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #4
  %cleanup.dest282 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest282, label %unreachable [
    i32 0, label %cleanup.cont283
    i32 10, label %for.inc284
  ]

cleanup.cont283:                                  ; preds = %cleanup274
  br label %for.inc284

for.inc284:                                       ; preds = %cleanup.cont283, %cleanup274
  %339 = load i32, i32* %colIndex, align 4, !tbaa !12
  %inc285 = add i32 %339, 1
  store i32 %inc285, i32* %colIndex, align 4, !tbaa !12
  %340 = load i32, i32* %tileIndex, align 4, !tbaa !12
  %inc286 = add i32 %340, 1
  store i32 %inc286, i32* %tileIndex, align 4, !tbaa !12
  br label %for.cond128

for.end288:                                       ; preds = %for.cond.cleanup132
  br label %for.inc289

for.inc289:                                       ; preds = %for.end288
  %341 = load i32, i32* %rowIndex, align 4, !tbaa !12
  %inc290 = add i32 %341, 1
  store i32 %inc290, i32* %rowIndex, align 4, !tbaa !12
  br label %for.cond123

for.end292:                                       ; preds = %for.cond.cleanup126
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %342 = bitcast i32* %pixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #4
  %343 = bitcast i32* %tileIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %343) #4
  %344 = bitcast %struct.avifPixelFormatInfo* %formatInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %344) #4
  br label %cleanup296

cleanup296:                                       ; preds = %for.end292, %if.then84, %cleanup65
  %345 = bitcast i32* %firstTileUVPresent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %345) #4
  %346 = bitcast %struct.avifTile** %firstTile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %346) #4
  br label %return

return:                                           ; preds = %cleanup296, %if.then
  %347 = load i32, i32* %retval, align 4
  ret i32 %347

unreachable:                                      ; preds = %cleanup274
  unreachable
}

declare void @avifImageFreePlanes(%struct.avifImage*, i32) #2

declare void @avifImageStealPlanes(%struct.avifImage*, %struct.avifImage*, i32) #2

; Function Attrs: nounwind
define hidden i32 @avifDecoderNthImageTiming(%struct.avifDecoder* %decoder, i32 %frameIndex, %struct.avifImageTiming* %outTiming) #0 {
entry:
  %retval = alloca i32, align 4
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %frameIndex.addr = alloca i32, align 4
  %outTiming.addr = alloca %struct.avifImageTiming*, align 4
  %imageIndex = alloca i32, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  store i32 %frameIndex, i32* %frameIndex.addr, align 4, !tbaa !12
  store %struct.avifImageTiming* %outTiming, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %0 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %0, i32 0, i32 11
  %1 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !18
  %tobool = icmp ne %struct.avifDecoderData* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %3 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageCount = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %3, i32 0, i32 4
  %4 = load i32, i32* %imageCount, align 8, !tbaa !58
  %cmp = icmp sge i32 %2, %4
  br i1 %cmp, label %if.then1, label %if.end2

if.then1:                                         ; preds = %if.end
  store i32 16, i32* %retval, align 4
  br label %return

if.end2:                                          ; preds = %if.end
  %5 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data3 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %5, i32 0, i32 11
  %6 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data3, align 4, !tbaa !18
  %sourceSampleTable = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %6, i32 0, i32 10
  %7 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sourceSampleTable, align 4, !tbaa !32
  %tobool4 = icmp ne %struct.avifSampleTable* %7, null
  br i1 %tobool4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end2
  %8 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %9 = bitcast %struct.avifImageTiming* %8 to i8*
  %10 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageTiming = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %10, i32 0, i32 5
  %11 = bitcast %struct.avifImageTiming* %imageTiming to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %9, i8* align 8 %11, i32 40, i1 false)
  store i32 0, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end2
  %12 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %timescale = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %12, i32 0, i32 6
  %13 = load i64, i64* %timescale, align 8, !tbaa !60
  %14 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %timescale7 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %14, i32 0, i32 0
  store i64 %13, i64* %timescale7, align 8, !tbaa !175
  %15 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %ptsInTimescales = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %15, i32 0, i32 2
  store i64 0, i64* %ptsInTimescales, align 8, !tbaa !176
  %16 = bitcast i32* %imageIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  store i32 0, i32* %imageIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end6
  %17 = load i32, i32* %imageIndex, align 4, !tbaa !12
  %18 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %cmp8 = icmp slt i32 %17, %18
  br i1 %cmp8, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %19 = bitcast i32* %imageIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data9 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %20, i32 0, i32 11
  %21 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data9, align 4, !tbaa !18
  %sourceSampleTable10 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %21, i32 0, i32 10
  %22 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sourceSampleTable10, align 4, !tbaa !32
  %23 = load i32, i32* %imageIndex, align 4, !tbaa !12
  %call = call i32 @avifSampleTableGetImageDelta(%struct.avifSampleTable* %22, i32 %23)
  %conv = zext i32 %call to i64
  %24 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %ptsInTimescales11 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %24, i32 0, i32 2
  %25 = load i64, i64* %ptsInTimescales11, align 8, !tbaa !176
  %add = add i64 %25, %conv
  store i64 %add, i64* %ptsInTimescales11, align 8, !tbaa !176
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %imageIndex, align 4, !tbaa !12
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %imageIndex, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %27 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data12 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %27, i32 0, i32 11
  %28 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data12, align 4, !tbaa !18
  %sourceSampleTable13 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %28, i32 0, i32 10
  %29 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sourceSampleTable13, align 4, !tbaa !32
  %30 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %call14 = call i32 @avifSampleTableGetImageDelta(%struct.avifSampleTable* %29, i32 %30)
  %conv15 = zext i32 %call14 to i64
  %31 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %durationInTimescales = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %31, i32 0, i32 4
  store i64 %conv15, i64* %durationInTimescales, align 8, !tbaa !177
  %32 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %timescale16 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %32, i32 0, i32 0
  %33 = load i64, i64* %timescale16, align 8, !tbaa !175
  %cmp17 = icmp ugt i64 %33, 0
  br i1 %cmp17, label %if.then19, label %if.else

if.then19:                                        ; preds = %for.end
  %34 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %ptsInTimescales20 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %34, i32 0, i32 2
  %35 = load i64, i64* %ptsInTimescales20, align 8, !tbaa !176
  %conv21 = uitofp i64 %35 to double
  %36 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %timescale22 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %36, i32 0, i32 0
  %37 = load i64, i64* %timescale22, align 8, !tbaa !175
  %conv23 = uitofp i64 %37 to double
  %div = fdiv double %conv21, %conv23
  %38 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %pts = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %38, i32 0, i32 1
  store double %div, double* %pts, align 8, !tbaa !178
  %39 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %durationInTimescales24 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %39, i32 0, i32 4
  %40 = load i64, i64* %durationInTimescales24, align 8, !tbaa !177
  %conv25 = uitofp i64 %40 to double
  %41 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %timescale26 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %41, i32 0, i32 0
  %42 = load i64, i64* %timescale26, align 8, !tbaa !175
  %conv27 = uitofp i64 %42 to double
  %div28 = fdiv double %conv25, %conv27
  %43 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %duration = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %43, i32 0, i32 3
  store double %div28, double* %duration, align 8, !tbaa !179
  br label %if.end31

if.else:                                          ; preds = %for.end
  %44 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %pts29 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %44, i32 0, i32 1
  store double 0.000000e+00, double* %pts29, align 8, !tbaa !178
  %45 = load %struct.avifImageTiming*, %struct.avifImageTiming** %outTiming.addr, align 4, !tbaa !2
  %duration30 = getelementptr inbounds %struct.avifImageTiming, %struct.avifImageTiming* %45, i32 0, i32 3
  store double 0.000000e+00, double* %duration30, align 8, !tbaa !179
  br label %if.end31

if.end31:                                         ; preds = %if.else, %if.then19
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end31, %if.then5, %if.then1, %if.then
  %46 = load i32, i32* %retval, align 4
  ret i32 %46
}

; Function Attrs: nounwind
define internal i32 @avifSampleTableGetImageDelta(%struct.avifSampleTable* %sampleTable, i32 %imageIndex) #0 {
entry:
  %retval = alloca i32, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %imageIndex.addr = alloca i32, align 4
  %maxSampleIndex = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %timeToSample = alloca %struct.avifSampleTableTimeToSample*, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store i32 %imageIndex, i32* %imageIndex.addr, align 4, !tbaa !12
  %0 = bitcast i32* %maxSampleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %maxSampleIndex, align 4, !tbaa !12
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !12
  %3 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %timeToSamples = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %3, i32 0, i32 4
  %count = getelementptr inbounds %struct.avifSampleTableTimeToSampleArray, %struct.avifSampleTableTimeToSampleArray* %timeToSamples, i32 0, i32 2
  %4 = load i32, i32* %count, align 4, !tbaa !180
  %cmp = icmp ult i32 %2, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup7

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct.avifSampleTableTimeToSample** %timeToSample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %timeToSamples1 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %6, i32 0, i32 4
  %timeToSample2 = getelementptr inbounds %struct.avifSampleTableTimeToSampleArray, %struct.avifSampleTableTimeToSampleArray* %timeToSamples1, i32 0, i32 0
  %7 = load %struct.avifSampleTableTimeToSample*, %struct.avifSampleTableTimeToSample** %timeToSample2, align 4, !tbaa !181
  %8 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifSampleTableTimeToSample, %struct.avifSampleTableTimeToSample* %7, i32 %8
  store %struct.avifSampleTableTimeToSample* %arrayidx, %struct.avifSampleTableTimeToSample** %timeToSample, align 4, !tbaa !2
  %9 = load %struct.avifSampleTableTimeToSample*, %struct.avifSampleTableTimeToSample** %timeToSample, align 4, !tbaa !2
  %sampleCount = getelementptr inbounds %struct.avifSampleTableTimeToSample, %struct.avifSampleTableTimeToSample* %9, i32 0, i32 0
  %10 = load i32, i32* %sampleCount, align 4, !tbaa !182
  %11 = load i32, i32* %maxSampleIndex, align 4, !tbaa !12
  %add = add i32 %11, %10
  store i32 %add, i32* %maxSampleIndex, align 4, !tbaa !12
  %12 = load i32, i32* %imageIndex.addr, align 4, !tbaa !12
  %13 = load i32, i32* %maxSampleIndex, align 4, !tbaa !12
  %cmp3 = icmp slt i32 %12, %13
  br i1 %cmp3, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !12
  %15 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %timeToSamples4 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %15, i32 0, i32 4
  %count5 = getelementptr inbounds %struct.avifSampleTableTimeToSampleArray, %struct.avifSampleTableTimeToSampleArray* %timeToSamples4, i32 0, i32 2
  %16 = load i32, i32* %count5, align 4, !tbaa !180
  %sub = sub i32 %16, 1
  %cmp6 = icmp eq i32 %14, %sub
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body
  %17 = load %struct.avifSampleTableTimeToSample*, %struct.avifSampleTableTimeToSample** %timeToSample, align 4, !tbaa !2
  %sampleDelta = getelementptr inbounds %struct.avifSampleTableTimeToSample, %struct.avifSampleTableTimeToSample* %17, i32 0, i32 1
  %18 = load i32, i32* %sampleDelta, align 4, !tbaa !184
  store i32 %18, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %19 = bitcast %struct.avifSampleTableTimeToSample** %timeToSample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup7 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %20 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup7:                                         ; preds = %cleanup, %for.cond.cleanup
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  %cleanup.dest8 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest8, label %cleanup9 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup7
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup9

cleanup9:                                         ; preds = %for.end, %cleanup7
  %22 = bitcast i32* %maxSampleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: nounwind
define hidden i32 @avifDecoderNthImage(%struct.avifDecoder* %decoder, i32 %frameIndex) #0 {
entry:
  %retval = alloca i32, align 4
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %frameIndex.addr = alloca i32, align 4
  %requestedIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %result = alloca i32, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  store i32 %frameIndex, i32* %frameIndex.addr, align 4, !tbaa !12
  %0 = bitcast i32* %requestedIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  store i32 %1, i32* %requestedIndex, align 4, !tbaa !12
  %2 = load i32, i32* %requestedIndex, align 4, !tbaa !12
  %3 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %3, i32 0, i32 3
  %4 = load i32, i32* %imageIndex, align 4, !tbaa !56
  %cmp = icmp eq i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

if.end:                                           ; preds = %entry
  %5 = load i32, i32* %requestedIndex, align 4, !tbaa !12
  %6 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex1 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %6, i32 0, i32 3
  %7 = load i32, i32* %imageIndex1, align 4, !tbaa !56
  %add = add nsw i32 %7, 1
  %cmp2 = icmp eq i32 %5, %add
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %8 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %call = call i32 @avifDecoderNextImage(%struct.avifDecoder* %8)
  store i32 %call, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

if.end4:                                          ; preds = %if.end
  %9 = load i32, i32* %requestedIndex, align 4, !tbaa !12
  %10 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageCount = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %10, i32 0, i32 4
  %11 = load i32, i32* %imageCount, align 8, !tbaa !58
  %cmp5 = icmp sge i32 %9, %11
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end4
  store i32 16, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

if.end7:                                          ; preds = %if.end4
  %12 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %13 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %call8 = call i32 @avifDecoderNearestKeyframe(%struct.avifDecoder* %12, i32 %13)
  %sub = sub nsw i32 %call8, 1
  %14 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex9 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %14, i32 0, i32 3
  store i32 %sub, i32* %imageIndex9, align 4, !tbaa !56
  %15 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %call10 = call i32 @avifDecoderFlush(%struct.avifDecoder* %15)
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %if.end7
  %16 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %call11 = call i32 @avifDecoderNextImage(%struct.avifDecoder* %17)
  store i32 %call11, i32* %result, align 4, !tbaa !25
  %18 = load i32, i32* %result, align 4, !tbaa !25
  %cmp12 = icmp ne i32 %18, 0
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %for.cond
  %19 = load i32, i32* %result, align 4, !tbaa !25
  store i32 %19, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %for.cond
  %20 = load i32, i32* %requestedIndex, align 4, !tbaa !12
  %21 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %imageIndex15 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %21, i32 0, i32 3
  %22 = load i32, i32* %imageIndex15, align 4, !tbaa !56
  %cmp16 = icmp eq i32 %20, %22
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end14
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %if.end14
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end18, %if.then17, %if.then13
  %23 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup19 [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond

for.end:                                          ; preds = %cleanup
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

cleanup19:                                        ; preds = %for.end, %cleanup, %if.then6, %if.then3, %if.then
  %24 = bitcast i32* %requestedIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

; Function Attrs: nounwind
define hidden i32 @avifDecoderNearestKeyframe(%struct.avifDecoder* %decoder, i32 %frameIndex) #0 {
entry:
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %frameIndex.addr = alloca i32, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  store i32 %frameIndex, i32* %frameIndex.addr, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %2 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %call = call i32 @avifDecoderIsKeyframe(%struct.avifDecoder* %1, i32 %2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %3 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %dec = add i32 %3, -1
  store i32 %dec, i32* %frameIndex.addr, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %4 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  ret i32 %4
}

; Function Attrs: nounwind
define hidden i32 @avifDecoderIsKeyframe(%struct.avifDecoder* %decoder, i32 %frameIndex) #0 {
entry:
  %retval = alloca i32, align 4
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %frameIndex.addr = alloca i32, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  store i32 %frameIndex, i32* %frameIndex.addr, align 4, !tbaa !12
  %0 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %0, i32 0, i32 11
  %1 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !18
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %1, i32 0, i32 4
  %count = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles, i32 0, i32 2
  %2 = load i32, i32* %count, align 4, !tbaa !92
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %land.lhs.true, label %if.end18

land.lhs.true:                                    ; preds = %entry
  %3 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %3, i32 0, i32 11
  %4 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data1, align 4, !tbaa !18
  %tiles2 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %4, i32 0, i32 4
  %tile = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles2, i32 0, i32 0
  %5 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !104
  %arrayidx = getelementptr inbounds %struct.avifTile, %struct.avifTile* %5, i32 0
  %input = getelementptr inbounds %struct.avifTile, %struct.avifTile* %arrayidx, i32 0, i32 0
  %6 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input, align 4, !tbaa !49
  %tobool = icmp ne %struct.avifCodecDecodeInput* %6, null
  br i1 %tobool, label %if.then, label %if.end18

if.then:                                          ; preds = %land.lhs.true
  %7 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %8 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data3 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %8, i32 0, i32 11
  %9 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data3, align 4, !tbaa !18
  %tiles4 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %9, i32 0, i32 4
  %tile5 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles4, i32 0, i32 0
  %10 = load %struct.avifTile*, %struct.avifTile** %tile5, align 4, !tbaa !104
  %arrayidx6 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %10, i32 0
  %input7 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %arrayidx6, i32 0, i32 0
  %11 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input7, align 4, !tbaa !49
  %samples = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %11, i32 0, i32 0
  %count8 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples, i32 0, i32 2
  %12 = load i32, i32* %count8, align 4, !tbaa !57
  %cmp9 = icmp ult i32 %7, %12
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then
  %13 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %data11 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %13, i32 0, i32 11
  %14 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data11, align 4, !tbaa !18
  %tiles12 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %14, i32 0, i32 4
  %tile13 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles12, i32 0, i32 0
  %15 = load %struct.avifTile*, %struct.avifTile** %tile13, align 4, !tbaa !104
  %arrayidx14 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %15, i32 0
  %input15 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %arrayidx14, i32 0, i32 0
  %16 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %input15, align 4, !tbaa !49
  %samples16 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %16, i32 0, i32 0
  %sample = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples16, i32 0, i32 0
  %17 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !105
  %18 = load i32, i32* %frameIndex.addr, align 4, !tbaa !12
  %arrayidx17 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %17, i32 %18
  %sync = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %arrayidx17, i32 0, i32 1
  %19 = load i32, i32* %sync, align 4, !tbaa !93
  store i32 %19, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end18

if.end18:                                         ; preds = %if.end, %land.lhs.true, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end18, %if.then10
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: nounwind
define hidden i32 @avifDecoderRead(%struct.avifDecoder* %decoder, %struct.avifImage* %image, %struct.avifROData* %input) #0 {
entry:
  %retval = alloca i32, align 4
  %decoder.addr = alloca %struct.avifDecoder*, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %input.addr = alloca %struct.avifROData*, align 4
  %result = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifDecoder* %decoder, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifROData* %input, %struct.avifROData** %input.addr, align 4, !tbaa !2
  %0 = bitcast i32* %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %2 = load %struct.avifROData*, %struct.avifROData** %input.addr, align 4, !tbaa !2
  %call = call i32 @avifDecoderParse(%struct.avifDecoder* %1, %struct.avifROData* %2)
  store i32 %call, i32* %result, align 4, !tbaa !25
  %3 = load i32, i32* %result, align 4, !tbaa !25
  %cmp = icmp ne i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %result, align 4, !tbaa !25
  store i32 %4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %5 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %call1 = call i32 @avifDecoderNextImage(%struct.avifDecoder* %5)
  store i32 %call1, i32* %result, align 4, !tbaa !25
  %6 = load i32, i32* %result, align 4, !tbaa !25
  %cmp2 = icmp ne i32 %6, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %7 = load i32, i32* %result, align 4, !tbaa !25
  store i32 %7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %if.end
  %8 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %9 = load %struct.avifDecoder*, %struct.avifDecoder** %decoder.addr, align 4, !tbaa !2
  %image5 = getelementptr inbounds %struct.avifDecoder, %struct.avifDecoder* %9, i32 0, i32 2
  %10 = load %struct.avifImage*, %struct.avifImage** %image5, align 8, !tbaa !24
  call void @avifImageCopy(%struct.avifImage* %8, %struct.avifImage* %10, i32 255)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end4, %if.then3, %if.then
  %11 = bitcast i32* %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

declare void @avifImageCopy(%struct.avifImage*, %struct.avifImage*, i32) #2

declare i32 @avifROStreamRead(%struct.avifROStream*, i8*, i32) #2

declare i32 @avifROStreamReadU32(%struct.avifROStream*, i32*) #2

declare i32 @avifROStreamRemainingBytes(%struct.avifROStream*) #2

declare i32 @avifROStreamSkip(%struct.avifROStream*, i32) #2

; Function Attrs: nounwind
define internal void @avifDecoderDataDestroy(%struct.avifDecoderData* %data) #0 {
entry:
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %i = alloca i32, align 4
  %track = alloca %struct.avifTrack*, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %0 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %meta = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %0, i32 0, i32 1
  %1 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !75
  call void @avifMetaDestroy(%struct.avifMeta* %1)
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !12
  %4 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tracks = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %4, i32 0, i32 2
  %count = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks, i32 0, i32 2
  %5 = load i32, i32* %count, align 4, !tbaa !33
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tracks1 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %8, i32 0, i32 2
  %track2 = getelementptr inbounds %struct.avifTrackArray, %struct.avifTrackArray* %tracks1, i32 0, i32 0
  %9 = load %struct.avifTrack*, %struct.avifTrack** %track2, align 4, !tbaa !35
  %10 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %9, i32 %10
  store %struct.avifTrack* %arrayidx, %struct.avifTrack** %track, align 4, !tbaa !2
  %11 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %sampleTable = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %11, i32 0, i32 6
  %12 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 8, !tbaa !36
  %tobool = icmp ne %struct.avifSampleTable* %12, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %13 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %sampleTable3 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %13, i32 0, i32 6
  %14 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable3, align 8, !tbaa !36
  call void @avifSampleTableDestroy(%struct.avifSampleTable* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %15 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %meta4 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %15, i32 0, i32 7
  %16 = load %struct.avifMeta*, %struct.avifMeta** %meta4, align 4, !tbaa !48
  %tobool5 = icmp ne %struct.avifMeta* %16, null
  br i1 %tobool5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  %17 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %meta7 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %17, i32 0, i32 7
  %18 = load %struct.avifMeta*, %struct.avifMeta** %meta7, align 4, !tbaa !48
  call void @avifMetaDestroy(%struct.avifMeta* %18)
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.end
  %19 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  br label %for.inc

for.inc:                                          ; preds = %if.end8
  %20 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %21 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tracks9 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %21, i32 0, i32 2
  %22 = bitcast %struct.avifTrackArray* %tracks9 to i8*
  call void @avifArrayDestroy(i8* %22)
  %23 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  call void @avifDecoderDataClearTiles(%struct.avifDecoderData* %23)
  %24 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %24, i32 0, i32 4
  %25 = bitcast %struct.avifTileArray* %tiles to i8*
  call void @avifArrayDestroy(i8* %25)
  %26 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %27 = bitcast %struct.avifDecoderData* %26 to i8*
  call void @avifFree(i8* %27)
  ret void
}

; Function Attrs: nounwind
define internal void @avifMetaDestroy(%struct.avifMeta* %meta) #0 {
entry:
  %meta.addr = alloca %struct.avifMeta*, align 4
  %i = alloca i32, align 4
  %item = alloca %struct.avifDecoderItem*, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !12
  %2 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %2, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !76
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items1 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %6, i32 0, i32 0
  %item2 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items1, i32 0, i32 0
  %7 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item2, align 4, !tbaa !81
  %8 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %7, i32 %8
  store %struct.avifDecoderItem* %arrayidx, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %9 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %9, i32 0, i32 7
  %10 = bitcast %struct.avifPropertyArray* %properties to i8*
  call void @avifArrayDestroy(i8* %10)
  %11 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %13 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items3 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %13, i32 0, i32 0
  %14 = bitcast %struct.avifDecoderItemArray* %items3 to i8*
  call void @avifArrayDestroy(i8* %14)
  %15 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %properties4 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %15, i32 0, i32 1
  %16 = bitcast %struct.avifPropertyArray* %properties4 to i8*
  call void @avifArrayDestroy(i8* %16)
  %17 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idats = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %17, i32 0, i32 2
  %18 = bitcast %struct.avifDecoderItemDataArray* %idats to i8*
  call void @avifArrayDestroy(i8* %18)
  %19 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %20 = bitcast %struct.avifMeta* %19 to i8*
  call void @avifFree(i8* %20)
  ret void
}

; Function Attrs: nounwind
define internal void @avifSampleTableDestroy(%struct.avifSampleTable* %sampleTable) #0 {
entry:
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %i = alloca i32, align 4
  %description = alloca %struct.avifSampleDescription*, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %0 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %chunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %0, i32 0, i32 0
  %1 = bitcast %struct.avifSampleTableChunkArray* %chunks to i8*
  call void @avifArrayDestroy(i8* %1)
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !12
  %4 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleDescriptions = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %4, i32 0, i32 1
  %count = getelementptr inbounds %struct.avifSampleDescriptionArray, %struct.avifSampleDescriptionArray* %sampleDescriptions, i32 0, i32 2
  %5 = load i32, i32* %count, align 4, !tbaa !136
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %struct.avifSampleDescription** %description to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleDescriptions1 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %8, i32 0, i32 1
  %description2 = getelementptr inbounds %struct.avifSampleDescriptionArray, %struct.avifSampleDescriptionArray* %sampleDescriptions1, i32 0, i32 0
  %9 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description2, align 4, !tbaa !137
  %10 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %9, i32 %10
  store %struct.avifSampleDescription* %arrayidx, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %11 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %11, i32 0, i32 1
  %12 = bitcast %struct.avifPropertyArray* %properties to i8*
  call void @avifArrayDestroy(i8* %12)
  %13 = bitcast %struct.avifSampleDescription** %description to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %15 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleDescriptions3 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %15, i32 0, i32 1
  %16 = bitcast %struct.avifSampleDescriptionArray* %sampleDescriptions3 to i8*
  call void @avifArrayDestroy(i8* %16)
  %17 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleToChunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %17, i32 0, i32 2
  %18 = bitcast %struct.avifSampleTableSampleToChunkArray* %sampleToChunks to i8*
  call void @avifArrayDestroy(i8* %18)
  %19 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleSizes = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %19, i32 0, i32 3
  %20 = bitcast %struct.avifSampleTableSampleSizeArray* %sampleSizes to i8*
  call void @avifArrayDestroy(i8* %20)
  %21 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %timeToSamples = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %21, i32 0, i32 4
  %22 = bitcast %struct.avifSampleTableTimeToSampleArray* %timeToSamples to i8*
  call void @avifArrayDestroy(i8* %22)
  %23 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %syncSamples = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %23, i32 0, i32 5
  %24 = bitcast %struct.avifSyncSampleArray* %syncSamples to i8*
  call void @avifArrayDestroy(i8* %24)
  %25 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %26 = bitcast %struct.avifSampleTable* %25 to i8*
  call void @avifFree(i8* %26)
  ret void
}

; Function Attrs: nounwind
define internal %struct.avifDecoderData* @avifDecoderDataCreate() #0 {
entry:
  %data = alloca %struct.avifDecoderData*, align 4
  %0 = bitcast %struct.avifDecoderData** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 104)
  %1 = bitcast i8* %call to %struct.avifDecoderData*
  store %struct.avifDecoderData* %1, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %2 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %3 = bitcast %struct.avifDecoderData* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 104, i1 false)
  %call1 = call %struct.avifMeta* @avifMetaCreate()
  %4 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %meta = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %4, i32 0, i32 1
  store %struct.avifMeta* %call1, %struct.avifMeta** %meta, align 4, !tbaa !75
  %5 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tracks = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %5, i32 0, i32 2
  %6 = bitcast %struct.avifTrackArray* %tracks to i8*
  call void @avifArrayCreate(i8* %6, i32 40, i32 2)
  %7 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %7, i32 0, i32 4
  %8 = bitcast %struct.avifTileArray* %tiles to i8*
  call void @avifArrayCreate(i8* %8, i32 12, i32 8)
  %9 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data, align 4, !tbaa !2
  %10 = bitcast %struct.avifDecoderData** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  ret %struct.avifDecoderData* %9
}

; Function Attrs: nounwind
define internal %struct.avifMeta* @avifMetaCreate() #0 {
entry:
  %meta = alloca %struct.avifMeta*, align 4
  %0 = bitcast %struct.avifMeta** %meta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 56)
  %1 = bitcast i8* %call to %struct.avifMeta*
  store %struct.avifMeta* %1, %struct.avifMeta** %meta, align 4, !tbaa !2
  %2 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !2
  %3 = bitcast %struct.avifMeta* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 56, i1 false)
  %4 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !2
  %items = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %4, i32 0, i32 0
  %5 = bitcast %struct.avifDecoderItemArray* %items to i8*
  call void @avifArrayCreate(i8* %5, i32 124, i32 8)
  %6 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %6, i32 0, i32 1
  %7 = bitcast %struct.avifPropertyArray* %properties to i8*
  call void @avifArrayCreate(i8* %7, i32 68, i32 16)
  %8 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !2
  %idats = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %8, i32 0, i32 2
  %9 = bitcast %struct.avifDecoderItemDataArray* %idats to i8*
  call void @avifArrayCreate(i8* %9, i32 12, i32 1)
  %10 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !2
  %11 = bitcast %struct.avifMeta** %meta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret %struct.avifMeta* %10
}

declare i32 @avifROStreamHasBytesLeft(%struct.avifROStream*, i32) #2

; Function Attrs: nounwind
define internal i32 @avifParseMetaBox(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup110

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idatID = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %4, i32 0, i32 3
  %5 = load i32, i32* %idatID, align 4, !tbaa !185
  %inc = add i32 %5, 1
  store i32 %inc, i32* %idatID, align 4, !tbaa !185
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %do.end
  %call1 = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #4
  br label %do.body3

do.body3:                                         ; preds = %while.body
  %call4 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %do.body3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %do.body3
  br label %do.cond8

do.cond8:                                         ; preds = %if.end7
  br label %do.end9

do.end9:                                          ; preds = %do.cond8
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call10 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.15, i32 0, i32 0), i32 4)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.else, label %if.then12

if.then12:                                        ; preds = %do.end9
  br label %do.body13

do.body13:                                        ; preds = %if.then12
  %7 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %call14 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size15 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %8 = load i32, i32* %size15, align 4, !tbaa !10
  %call16 = call i32 @avifParseItemLocationBox(%struct.avifMeta* %7, i8* %call14, i32 %8)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %do.body13
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %do.body13
  br label %do.cond20

do.cond20:                                        ; preds = %if.end19
  br label %do.end21

do.end21:                                         ; preds = %do.cond20
  br label %if.end101

if.else:                                          ; preds = %do.end9
  %type22 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay23 = getelementptr inbounds [4 x i8], [4 x i8]* %type22, i32 0, i32 0
  %call24 = call i32 @memcmp(i8* %arraydecay23, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.16, i32 0, i32 0), i32 4)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %if.else36, label %if.then26

if.then26:                                        ; preds = %if.else
  br label %do.body27

do.body27:                                        ; preds = %if.then26
  %9 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %call28 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size29 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %10 = load i32, i32* %size29, align 4, !tbaa !10
  %call30 = call i32 @avifParsePrimaryItemBox(%struct.avifMeta* %9, i8* %call28, i32 %10)
  %tobool31 = icmp ne i32 %call30, 0
  br i1 %tobool31, label %if.end33, label %if.then32

if.then32:                                        ; preds = %do.body27
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end33:                                         ; preds = %do.body27
  br label %do.cond34

do.cond34:                                        ; preds = %if.end33
  br label %do.end35

do.end35:                                         ; preds = %do.cond34
  br label %if.end100

if.else36:                                        ; preds = %if.else
  %type37 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay38 = getelementptr inbounds [4 x i8], [4 x i8]* %type37, i32 0, i32 0
  %call39 = call i32 @memcmp(i8* %arraydecay38, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.17, i32 0, i32 0), i32 4)
  %tobool40 = icmp ne i32 %call39, 0
  br i1 %tobool40, label %if.else51, label %if.then41

if.then41:                                        ; preds = %if.else36
  br label %do.body42

do.body42:                                        ; preds = %if.then41
  %11 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %call43 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size44 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %12 = load i32, i32* %size44, align 4, !tbaa !10
  %call45 = call i32 @avifParseItemDataBox(%struct.avifMeta* %11, i8* %call43, i32 %12)
  %tobool46 = icmp ne i32 %call45, 0
  br i1 %tobool46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %do.body42
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end48:                                         ; preds = %do.body42
  br label %do.cond49

do.cond49:                                        ; preds = %if.end48
  br label %do.end50

do.end50:                                         ; preds = %do.cond49
  br label %if.end99

if.else51:                                        ; preds = %if.else36
  %type52 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay53 = getelementptr inbounds [4 x i8], [4 x i8]* %type52, i32 0, i32 0
  %call54 = call i32 @memcmp(i8* %arraydecay53, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.18, i32 0, i32 0), i32 4)
  %tobool55 = icmp ne i32 %call54, 0
  br i1 %tobool55, label %if.else66, label %if.then56

if.then56:                                        ; preds = %if.else51
  br label %do.body57

do.body57:                                        ; preds = %if.then56
  %13 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %call58 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size59 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %14 = load i32, i32* %size59, align 4, !tbaa !10
  %call60 = call i32 @avifParseItemPropertiesBox(%struct.avifMeta* %13, i8* %call58, i32 %14)
  %tobool61 = icmp ne i32 %call60, 0
  br i1 %tobool61, label %if.end63, label %if.then62

if.then62:                                        ; preds = %do.body57
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end63:                                         ; preds = %do.body57
  br label %do.cond64

do.cond64:                                        ; preds = %if.end63
  br label %do.end65

do.end65:                                         ; preds = %do.cond64
  br label %if.end98

if.else66:                                        ; preds = %if.else51
  %type67 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay68 = getelementptr inbounds [4 x i8], [4 x i8]* %type67, i32 0, i32 0
  %call69 = call i32 @memcmp(i8* %arraydecay68, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.19, i32 0, i32 0), i32 4)
  %tobool70 = icmp ne i32 %call69, 0
  br i1 %tobool70, label %if.else81, label %if.then71

if.then71:                                        ; preds = %if.else66
  br label %do.body72

do.body72:                                        ; preds = %if.then71
  %15 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %call73 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size74 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %16 = load i32, i32* %size74, align 4, !tbaa !10
  %call75 = call i32 @avifParseItemInfoBox(%struct.avifMeta* %15, i8* %call73, i32 %16)
  %tobool76 = icmp ne i32 %call75, 0
  br i1 %tobool76, label %if.end78, label %if.then77

if.then77:                                        ; preds = %do.body72
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end78:                                         ; preds = %do.body72
  br label %do.cond79

do.cond79:                                        ; preds = %if.end78
  br label %do.end80

do.end80:                                         ; preds = %do.cond79
  br label %if.end97

if.else81:                                        ; preds = %if.else66
  %type82 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay83 = getelementptr inbounds [4 x i8], [4 x i8]* %type82, i32 0, i32 0
  %call84 = call i32 @memcmp(i8* %arraydecay83, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.20, i32 0, i32 0), i32 4)
  %tobool85 = icmp ne i32 %call84, 0
  br i1 %tobool85, label %if.end96, label %if.then86

if.then86:                                        ; preds = %if.else81
  br label %do.body87

do.body87:                                        ; preds = %if.then86
  %17 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %call88 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size89 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %18 = load i32, i32* %size89, align 4, !tbaa !10
  %call90 = call i32 @avifParseItemReferenceBox(%struct.avifMeta* %17, i8* %call88, i32 %18)
  %tobool91 = icmp ne i32 %call90, 0
  br i1 %tobool91, label %if.end93, label %if.then92

if.then92:                                        ; preds = %do.body87
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end93:                                         ; preds = %do.body87
  br label %do.cond94

do.cond94:                                        ; preds = %if.end93
  br label %do.end95

do.end95:                                         ; preds = %do.cond94
  br label %if.end96

if.end96:                                         ; preds = %do.end95, %if.else81
  br label %if.end97

if.end97:                                         ; preds = %if.end96, %do.end80
  br label %if.end98

if.end98:                                         ; preds = %if.end97, %do.end65
  br label %if.end99

if.end99:                                         ; preds = %if.end98, %do.end50
  br label %if.end100

if.end100:                                        ; preds = %if.end99, %do.end35
  br label %if.end101

if.end101:                                        ; preds = %if.end100, %do.end21
  br label %do.body102

do.body102:                                       ; preds = %if.end101
  %size103 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %19 = load i32, i32* %size103, align 4, !tbaa !10
  %call104 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %19)
  %tobool105 = icmp ne i32 %call104, 0
  br i1 %tobool105, label %if.end107, label %if.then106

if.then106:                                       ; preds = %do.body102
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end107:                                        ; preds = %do.body102
  br label %do.cond108

do.cond108:                                       ; preds = %if.end107
  br label %do.end109

do.end109:                                        ; preds = %do.cond108
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end109, %if.then106, %if.then92, %if.then77, %if.then62, %if.then47, %if.then32, %if.then18, %if.then6
  %20 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %20) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup110 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup110

cleanup110:                                       ; preds = %while.end, %cleanup, %if.then
  %21 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %21) #4
  %22 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #4
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: nounwind
define internal i32 @avifParseMoovBox(%struct.avifDecoderData* %data, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data1, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %call = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  br label %do.body

do.body:                                          ; preds = %while.body
  %call2 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call4 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.33, i32 0, i32 0), i32 4)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end16, label %if.then6

if.then6:                                         ; preds = %do.end
  br label %do.body7

do.body7:                                         ; preds = %if.then6
  %5 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %call8 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size9 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %6 = load i32, i32* %size9, align 4, !tbaa !10
  %call10 = call i32 @avifParseTrackBox(%struct.avifDecoderData* %5, i8* %call8, i32 %6)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %do.body7
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %do.body7
  br label %do.cond14

do.cond14:                                        ; preds = %if.end13
  br label %do.end15

do.end15:                                         ; preds = %do.cond14
  br label %if.end16

if.end16:                                         ; preds = %do.end15, %do.end
  br label %do.body17

do.body17:                                        ; preds = %if.end16
  %size18 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %7 = load i32, i32* %size18, align 4, !tbaa !10
  %call19 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %7)
  %tobool20 = icmp ne i32 %call19, 0
  br i1 %tobool20, label %if.end22, label %if.then21

if.then21:                                        ; preds = %do.body17
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %do.body17
  br label %do.cond23

do.cond23:                                        ; preds = %if.end22
  br label %do.end24

do.end24:                                         ; preds = %do.cond23
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end24, %if.then21, %if.then12, %if.then
  %8 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %8) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup25 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

cleanup25:                                        ; preds = %while.end, %cleanup
  %9 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %9) #4
  %10 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #4
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

declare i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream*, i8 zeroext) #2

; Function Attrs: nounwind
define internal i32 @avifParseItemLocationBox(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %version = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %offsetSizeAndLengthSize = alloca i8, align 1
  %offsetSize = alloca i8, align 1
  %lengthSize = alloca i8, align 1
  %baseOffsetSizeAndIndexSize = alloca i8, align 1
  %baseOffsetSize = alloca i8, align 1
  %indexSize = alloca i8, align 1
  %tmp16 = alloca i16, align 2
  %itemCount = alloca i32, align 4
  %i = alloca i32, align 4
  %itemID = alloca i32, align 4
  %idatID = alloca i32, align 4
  %ignored = alloca i8, align 1
  %constructionMethod = alloca i8, align 1
  %dataReferenceIndex = alloca i16, align 2
  %baseOffset = alloca i64, align 8
  %extentCount = alloca i16, align 2
  %extentOffset = alloca i64, align 8
  %extentLength = alloca i64, align 8
  %item = alloca %struct.avifDecoderItem*, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %s, i8* %version, i32* null)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup202

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = load i8, i8* %version, align 1, !tbaa !25
  %conv = zext i8 %4 to i32
  %cmp = icmp sgt i32 %conv, 2
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %do.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup202

if.end3:                                          ; preds = %do.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %offsetSizeAndLengthSize) #4
  br label %do.body4

do.body4:                                         ; preds = %if.end3
  %call5 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %offsetSizeAndLengthSize, i32 1)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %do.body4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup201

if.end8:                                          ; preds = %do.body4
  br label %do.cond9

do.cond9:                                         ; preds = %if.end8
  br label %do.end10

do.end10:                                         ; preds = %do.cond9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %offsetSize) #4
  %5 = load i8, i8* %offsetSizeAndLengthSize, align 1, !tbaa !25
  %conv11 = zext i8 %5 to i32
  %shr = ashr i32 %conv11, 4
  %and = and i32 %shr, 15
  %conv12 = trunc i32 %and to i8
  store i8 %conv12, i8* %offsetSize, align 1, !tbaa !25
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %lengthSize) #4
  %6 = load i8, i8* %offsetSizeAndLengthSize, align 1, !tbaa !25
  %conv13 = zext i8 %6 to i32
  %shr14 = ashr i32 %conv13, 0
  %and15 = and i32 %shr14, 15
  %conv16 = trunc i32 %and15 to i8
  store i8 %conv16, i8* %lengthSize, align 1, !tbaa !25
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %baseOffsetSizeAndIndexSize) #4
  br label %do.body17

do.body17:                                        ; preds = %do.end10
  %call18 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %baseOffsetSizeAndIndexSize, i32 1)
  %tobool19 = icmp ne i32 %call18, 0
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %do.body17
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup198

if.end21:                                         ; preds = %do.body17
  br label %do.cond22

do.cond22:                                        ; preds = %if.end21
  br label %do.end23

do.end23:                                         ; preds = %do.cond22
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %baseOffsetSize) #4
  %7 = load i8, i8* %baseOffsetSizeAndIndexSize, align 1, !tbaa !25
  %conv24 = zext i8 %7 to i32
  %shr25 = ashr i32 %conv24, 4
  %and26 = and i32 %shr25, 15
  %conv27 = trunc i32 %and26 to i8
  store i8 %conv27, i8* %baseOffsetSize, align 1, !tbaa !25
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %indexSize) #4
  store i8 0, i8* %indexSize, align 1, !tbaa !25
  %8 = load i8, i8* %version, align 1, !tbaa !25
  %conv28 = zext i8 %8 to i32
  %cmp29 = icmp eq i32 %conv28, 1
  br i1 %cmp29, label %if.then34, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %do.end23
  %9 = load i8, i8* %version, align 1, !tbaa !25
  %conv31 = zext i8 %9 to i32
  %cmp32 = icmp eq i32 %conv31, 2
  br i1 %cmp32, label %if.then34, label %if.end43

if.then34:                                        ; preds = %lor.lhs.false, %do.end23
  %10 = load i8, i8* %baseOffsetSizeAndIndexSize, align 1, !tbaa !25
  %conv35 = zext i8 %10 to i32
  %and36 = and i32 %conv35, 15
  %conv37 = trunc i32 %and36 to i8
  store i8 %conv37, i8* %indexSize, align 1, !tbaa !25
  %11 = load i8, i8* %indexSize, align 1, !tbaa !25
  %conv38 = zext i8 %11 to i32
  %cmp39 = icmp ne i32 %conv38, 0
  br i1 %cmp39, label %if.then41, label %if.end42

if.then41:                                        ; preds = %if.then34
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup196

if.end42:                                         ; preds = %if.then34
  br label %if.end43

if.end43:                                         ; preds = %if.end42, %lor.lhs.false
  %12 = bitcast i16* %tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  %13 = bitcast i32* %itemCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i8, i8* %version, align 1, !tbaa !25
  %conv44 = zext i8 %14 to i32
  %cmp45 = icmp slt i32 %conv44, 2
  br i1 %cmp45, label %if.then47, label %if.else

if.then47:                                        ; preds = %if.end43
  br label %do.body48

do.body48:                                        ; preds = %if.then47
  %call49 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp16)
  %tobool50 = icmp ne i32 %call49, 0
  br i1 %tobool50, label %if.end52, label %if.then51

if.then51:                                        ; preds = %do.body48
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup194

if.end52:                                         ; preds = %do.body48
  br label %do.cond53

do.cond53:                                        ; preds = %if.end52
  br label %do.end54

do.end54:                                         ; preds = %do.cond53
  %15 = load i16, i16* %tmp16, align 2, !tbaa !155
  %conv55 = zext i16 %15 to i32
  store i32 %conv55, i32* %itemCount, align 4, !tbaa !12
  br label %if.end63

if.else:                                          ; preds = %if.end43
  br label %do.body56

do.body56:                                        ; preds = %if.else
  %call57 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %itemCount)
  %tobool58 = icmp ne i32 %call57, 0
  br i1 %tobool58, label %if.end60, label %if.then59

if.then59:                                        ; preds = %do.body56
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup194

if.end60:                                         ; preds = %do.body56
  br label %do.cond61

do.cond61:                                        ; preds = %if.end60
  br label %do.end62

do.end62:                                         ; preds = %do.cond61
  br label %if.end63

if.end63:                                         ; preds = %do.end62, %do.end54
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end63
  %17 = load i32, i32* %i, align 4, !tbaa !12
  %18 = load i32, i32* %itemCount, align 4, !tbaa !12
  %cmp64 = icmp ult i32 %17, %18
  br i1 %cmp64, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 12, i32* %cleanup.dest.slot, align 4
  br label %cleanup192

for.body:                                         ; preds = %for.cond
  %19 = bitcast i32* %itemID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = bitcast i32* %idatID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  store i32 0, i32* %idatID, align 4, !tbaa !12
  %21 = load i8, i8* %version, align 1, !tbaa !25
  %conv66 = zext i8 %21 to i32
  %cmp67 = icmp slt i32 %conv66, 2
  br i1 %cmp67, label %if.then69, label %if.else78

if.then69:                                        ; preds = %for.body
  br label %do.body70

do.body70:                                        ; preds = %if.then69
  %call71 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp16)
  %tobool72 = icmp ne i32 %call71, 0
  br i1 %tobool72, label %if.end74, label %if.then73

if.then73:                                        ; preds = %do.body70
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup188

if.end74:                                         ; preds = %do.body70
  br label %do.cond75

do.cond75:                                        ; preds = %if.end74
  br label %do.end76

do.end76:                                         ; preds = %do.cond75
  %22 = load i16, i16* %tmp16, align 2, !tbaa !155
  %conv77 = zext i16 %22 to i32
  store i32 %conv77, i32* %itemID, align 4, !tbaa !12
  br label %if.end86

if.else78:                                        ; preds = %for.body
  br label %do.body79

do.body79:                                        ; preds = %if.else78
  %call80 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %itemID)
  %tobool81 = icmp ne i32 %call80, 0
  br i1 %tobool81, label %if.end83, label %if.then82

if.then82:                                        ; preds = %do.body79
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup188

if.end83:                                         ; preds = %do.body79
  br label %do.cond84

do.cond84:                                        ; preds = %if.end83
  br label %do.end85

do.end85:                                         ; preds = %do.cond84
  br label %if.end86

if.end86:                                         ; preds = %do.end85, %do.end76
  %23 = load i8, i8* %version, align 1, !tbaa !25
  %conv87 = zext i8 %23 to i32
  %cmp88 = icmp eq i32 %conv87, 1
  br i1 %cmp88, label %if.then94, label %lor.lhs.false90

lor.lhs.false90:                                  ; preds = %if.end86
  %24 = load i8, i8* %version, align 1, !tbaa !25
  %conv91 = zext i8 %24 to i32
  %cmp92 = icmp eq i32 %conv91, 2
  br i1 %cmp92, label %if.then94, label %if.end127

if.then94:                                        ; preds = %lor.lhs.false90, %if.end86
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ignored) #4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %constructionMethod) #4
  br label %do.body95

do.body95:                                        ; preds = %if.then94
  %call96 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %ignored, i32 1)
  %tobool97 = icmp ne i32 %call96, 0
  br i1 %tobool97, label %if.end99, label %if.then98

if.then98:                                        ; preds = %do.body95
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end99:                                         ; preds = %do.body95
  br label %do.cond100

do.cond100:                                       ; preds = %if.end99
  br label %do.end101

do.end101:                                        ; preds = %do.cond100
  br label %do.body102

do.body102:                                       ; preds = %do.end101
  %call103 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %constructionMethod, i32 1)
  %tobool104 = icmp ne i32 %call103, 0
  br i1 %tobool104, label %if.end106, label %if.then105

if.then105:                                       ; preds = %do.body102
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end106:                                        ; preds = %do.body102
  br label %do.cond107

do.cond107:                                       ; preds = %if.end106
  br label %do.end108

do.end108:                                        ; preds = %do.cond107
  %25 = load i8, i8* %constructionMethod, align 1, !tbaa !25
  %conv109 = zext i8 %25 to i32
  %and110 = and i32 %conv109, 15
  %conv111 = trunc i32 %and110 to i8
  store i8 %conv111, i8* %constructionMethod, align 1, !tbaa !25
  %26 = load i8, i8* %constructionMethod, align 1, !tbaa !25
  %conv112 = zext i8 %26 to i32
  %cmp113 = icmp ne i32 %conv112, 0
  br i1 %cmp113, label %land.lhs.true, label %if.end119

land.lhs.true:                                    ; preds = %do.end108
  %27 = load i8, i8* %constructionMethod, align 1, !tbaa !25
  %conv115 = zext i8 %27 to i32
  %cmp116 = icmp ne i32 %conv115, 1
  br i1 %cmp116, label %if.then118, label %if.end119

if.then118:                                       ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end119:                                        ; preds = %land.lhs.true, %do.end108
  %28 = load i8, i8* %constructionMethod, align 1, !tbaa !25
  %conv120 = zext i8 %28 to i32
  %cmp121 = icmp eq i32 %conv120, 1
  br i1 %cmp121, label %if.then123, label %if.end125

if.then123:                                       ; preds = %if.end119
  %29 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idatID124 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %29, i32 0, i32 3
  %30 = load i32, i32* %idatID124, align 4, !tbaa !185
  store i32 %30, i32* %idatID, align 4, !tbaa !12
  br label %if.end125

if.end125:                                        ; preds = %if.then123, %if.end119
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end125, %if.then118, %if.then105, %if.then98
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %constructionMethod) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ignored) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup188 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end127

if.end127:                                        ; preds = %cleanup.cont, %lor.lhs.false90
  %31 = bitcast i16* %dataReferenceIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %31) #4
  br label %do.body128

do.body128:                                       ; preds = %if.end127
  %call129 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %dataReferenceIndex)
  %tobool130 = icmp ne i32 %call129, 0
  br i1 %tobool130, label %if.end132, label %if.then131

if.then131:                                       ; preds = %do.body128
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup187

if.end132:                                        ; preds = %do.body128
  br label %do.cond133

do.cond133:                                       ; preds = %if.end132
  br label %do.end134

do.end134:                                        ; preds = %do.cond133
  %32 = bitcast i64* %baseOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %32) #4
  br label %do.body135

do.body135:                                       ; preds = %do.end134
  %33 = load i8, i8* %baseOffsetSize, align 1, !tbaa !25
  %conv136 = zext i8 %33 to i64
  %call137 = call i32 @avifROStreamReadUX8(%struct.avifROStream* %s, i64* %baseOffset, i64 %conv136)
  %tobool138 = icmp ne i32 %call137, 0
  br i1 %tobool138, label %if.end140, label %if.then139

if.then139:                                       ; preds = %do.body135
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup186

if.end140:                                        ; preds = %do.body135
  br label %do.cond141

do.cond141:                                       ; preds = %if.end140
  br label %do.end142

do.end142:                                        ; preds = %do.cond141
  %34 = bitcast i16* %extentCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %34) #4
  br label %do.body143

do.body143:                                       ; preds = %do.end142
  %call144 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %extentCount)
  %tobool145 = icmp ne i32 %call144, 0
  br i1 %tobool145, label %if.end147, label %if.then146

if.then146:                                       ; preds = %do.body143
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup185

if.end147:                                        ; preds = %do.body143
  br label %do.cond148

do.cond148:                                       ; preds = %if.end147
  br label %do.end149

do.end149:                                        ; preds = %do.cond148
  %35 = load i16, i16* %extentCount, align 2, !tbaa !155
  %conv150 = zext i16 %35 to i32
  %cmp151 = icmp eq i32 %conv150, 1
  br i1 %cmp151, label %if.then153, label %if.else183

if.then153:                                       ; preds = %do.end149
  %36 = bitcast i64* %extentOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %36) #4
  br label %do.body154

do.body154:                                       ; preds = %if.then153
  %37 = load i8, i8* %offsetSize, align 1, !tbaa !25
  %conv155 = zext i8 %37 to i64
  %call156 = call i32 @avifROStreamReadUX8(%struct.avifROStream* %s, i64* %extentOffset, i64 %conv155)
  %tobool157 = icmp ne i32 %call156, 0
  br i1 %tobool157, label %if.end159, label %if.then158

if.then158:                                       ; preds = %do.body154
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup180

if.end159:                                        ; preds = %do.body154
  br label %do.cond160

do.cond160:                                       ; preds = %if.end159
  br label %do.end161

do.end161:                                        ; preds = %do.cond160
  %38 = bitcast i64* %extentLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %38) #4
  br label %do.body162

do.body162:                                       ; preds = %do.end161
  %39 = load i8, i8* %lengthSize, align 1, !tbaa !25
  %conv163 = zext i8 %39 to i64
  %call164 = call i32 @avifROStreamReadUX8(%struct.avifROStream* %s, i64* %extentLength, i64 %conv163)
  %tobool165 = icmp ne i32 %call164, 0
  br i1 %tobool165, label %if.end167, label %if.then166

if.then166:                                       ; preds = %do.body162
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup179

if.end167:                                        ; preds = %do.body162
  br label %do.cond168

do.cond168:                                       ; preds = %if.end167
  br label %do.end169

do.end169:                                        ; preds = %do.cond168
  %40 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #4
  %41 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %42 = load i32, i32* %itemID, align 4, !tbaa !12
  %call170 = call %struct.avifDecoderItem* @avifMetaFindItem(%struct.avifMeta* %41, i32 %42)
  store %struct.avifDecoderItem* %call170, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %43 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %tobool171 = icmp ne %struct.avifDecoderItem* %43, null
  br i1 %tobool171, label %if.end173, label %if.then172

if.then172:                                       ; preds = %do.end169
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup178

if.end173:                                        ; preds = %do.end169
  %44 = load i32, i32* %itemID, align 4, !tbaa !12
  %45 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %id = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %45, i32 0, i32 0
  store i32 %44, i32* %id, align 4, !tbaa !88
  %46 = load i64, i64* %baseOffset, align 8, !tbaa !133
  %47 = load i64, i64* %extentOffset, align 8, !tbaa !133
  %add = add i64 %46, %47
  %conv174 = trunc i64 %add to i32
  %48 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %offset = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %48, i32 0, i32 3
  store i32 %conv174, i32* %offset, align 4, !tbaa !132
  %49 = load i64, i64* %extentLength, align 8, !tbaa !133
  %conv175 = trunc i64 %49 to i32
  %50 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %size176 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %50, i32 0, i32 4
  store i32 %conv175, i32* %size176, align 4, !tbaa !82
  %51 = load i32, i32* %idatID, align 4, !tbaa !12
  %52 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %idatID177 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %52, i32 0, i32 5
  store i32 %51, i32* %idatID177, align 4, !tbaa !126
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup178

cleanup178:                                       ; preds = %if.end173, %if.then172
  %53 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  br label %cleanup179

cleanup179:                                       ; preds = %cleanup178, %if.then166
  %54 = bitcast i64* %extentLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %54) #4
  br label %cleanup180

cleanup180:                                       ; preds = %cleanup179, %if.then158
  %55 = bitcast i64* %extentOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %55) #4
  %cleanup.dest181 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest181, label %cleanup185 [
    i32 0, label %cleanup.cont182
  ]

cleanup.cont182:                                  ; preds = %cleanup180
  br label %if.end184

if.else183:                                       ; preds = %do.end149
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup185

if.end184:                                        ; preds = %cleanup.cont182
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup185

cleanup185:                                       ; preds = %if.end184, %if.else183, %cleanup180, %if.then146
  %56 = bitcast i16* %extentCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %56) #4
  br label %cleanup186

cleanup186:                                       ; preds = %cleanup185, %if.then139
  %57 = bitcast i64* %baseOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %57) #4
  br label %cleanup187

cleanup187:                                       ; preds = %cleanup186, %if.then131
  %58 = bitcast i16* %dataReferenceIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %58) #4
  br label %cleanup188

cleanup188:                                       ; preds = %cleanup187, %cleanup, %if.then82, %if.then73
  %59 = bitcast i32* %idatID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = bitcast i32* %itemID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %cleanup.dest190 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest190, label %cleanup192 [
    i32 0, label %cleanup.cont191
  ]

cleanup.cont191:                                  ; preds = %cleanup188
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont191
  %61 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %61, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup192:                                       ; preds = %cleanup188, %for.cond.cleanup
  %62 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  %cleanup.dest193 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest193, label %cleanup194 [
    i32 12, label %for.end
  ]

for.end:                                          ; preds = %cleanup192
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup194

cleanup194:                                       ; preds = %for.end, %cleanup192, %if.then59, %if.then51
  %63 = bitcast i32* %itemCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  %64 = bitcast i16* %tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %64) #4
  br label %cleanup196

cleanup196:                                       ; preds = %cleanup194, %if.then41
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %indexSize) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %baseOffsetSize) #4
  br label %cleanup198

cleanup198:                                       ; preds = %cleanup196, %if.then20
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %baseOffsetSizeAndIndexSize) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %lengthSize) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %offsetSize) #4
  br label %cleanup201

cleanup201:                                       ; preds = %cleanup198, %if.then7
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %offsetSizeAndLengthSize) #4
  br label %cleanup202

cleanup202:                                       ; preds = %cleanup201, %if.then2, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %65 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %65) #4
  %66 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %66) #4
  %67 = load i32, i32* %retval, align 4
  ret i32 %67
}

; Function Attrs: nounwind
define internal i32 @avifParsePrimaryItemBox(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %version = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %tmp16 = alloca i16, align 2
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %primaryItemID = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %0, i32 0, i32 4
  %1 = load i32, i32* %primaryItemID, align 4, !tbaa !87
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #4
  %3 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #4
  %4 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %4, i8** %data, align 4, !tbaa !6
  %5 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %5, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  br label %do.body

do.body:                                          ; preds = %if.end
  %call = call i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %s, i8* %version, i32* null)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end2, label %if.then1

if.then1:                                         ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup24

if.end2:                                          ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end2
  br label %do.end

do.end:                                           ; preds = %do.cond
  %6 = load i8, i8* %version, align 1, !tbaa !25
  %conv = zext i8 %6 to i32
  %cmp3 = icmp eq i32 %conv, 0
  br i1 %cmp3, label %if.then5, label %if.else

if.then5:                                         ; preds = %do.end
  %7 = bitcast i16* %tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #4
  br label %do.body6

do.body6:                                         ; preds = %if.then5
  %call7 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp16)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %do.body6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %do.body6
  br label %do.cond11

do.cond11:                                        ; preds = %if.end10
  br label %do.end12

do.end12:                                         ; preds = %do.cond11
  %8 = load i16, i16* %tmp16, align 2, !tbaa !155
  %conv13 = zext i16 %8 to i32
  %9 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %primaryItemID14 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %9, i32 0, i32 4
  store i32 %conv13, i32* %primaryItemID14, align 4, !tbaa !87
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end12, %if.then9
  %10 = bitcast i16* %tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %10) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup24 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end23

if.else:                                          ; preds = %do.end
  br label %do.body15

do.body15:                                        ; preds = %if.else
  %11 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %primaryItemID16 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %11, i32 0, i32 4
  %call17 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %primaryItemID16)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %do.body15
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup24

if.end20:                                         ; preds = %do.body15
  br label %do.cond21

do.cond21:                                        ; preds = %if.end20
  br label %do.end22

do.end22:                                         ; preds = %do.cond21
  br label %if.end23

if.end23:                                         ; preds = %do.end22, %cleanup.cont
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup24

cleanup24:                                        ; preds = %if.end23, %if.then19, %cleanup, %if.then1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %12 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #4
  %13 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #4
  br label %return

return:                                           ; preds = %cleanup24, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: nounwind
define internal i32 @avifParseItemDataBox(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %index = alloca i32, align 4
  %idat4 = alloca %struct.avifDecoderItemData*, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !12
  %2 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idats = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %2, i32 0, i32 2
  %count = getelementptr inbounds %struct.avifDecoderItemDataArray, %struct.avifDecoderItemDataArray* %idats, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !128
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %4 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idats1 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %4, i32 0, i32 2
  %idat = getelementptr inbounds %struct.avifDecoderItemDataArray, %struct.avifDecoderItemDataArray* %idats1, i32 0, i32 0
  %5 = load %struct.avifDecoderItemData*, %struct.avifDecoderItemData** %idat, align 4, !tbaa !129
  %6 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %5, i32 %6
  %id = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %arrayidx, i32 0, i32 0
  %7 = load i32, i32* %id, align 4, !tbaa !130
  %8 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idatID = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %8, i32 0, i32 3
  %9 = load i32, i32* %idatID, align 4, !tbaa !185
  %cmp2 = icmp eq i32 %7, %9
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idats3 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %13, i32 0, i32 2
  %14 = bitcast %struct.avifDecoderItemDataArray* %idats3 to i8*
  %call = call i32 @avifArrayPushIndex(i8* %14)
  store i32 %call, i32* %index, align 4, !tbaa !12
  %15 = bitcast %struct.avifDecoderItemData** %idat4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idats5 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %16, i32 0, i32 2
  %idat6 = getelementptr inbounds %struct.avifDecoderItemDataArray, %struct.avifDecoderItemDataArray* %idats5, i32 0, i32 0
  %17 = load %struct.avifDecoderItemData*, %struct.avifDecoderItemData** %idat6, align 4, !tbaa !129
  %18 = load i32, i32* %index, align 4, !tbaa !12
  %arrayidx7 = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %17, i32 %18
  store %struct.avifDecoderItemData* %arrayidx7, %struct.avifDecoderItemData** %idat4, align 4, !tbaa !2
  %19 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %idatID8 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %19, i32 0, i32 3
  %20 = load i32, i32* %idatID8, align 4, !tbaa !185
  %21 = load %struct.avifDecoderItemData*, %struct.avifDecoderItemData** %idat4, align 4, !tbaa !2
  %id9 = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %21, i32 0, i32 0
  store i32 %20, i32* %id9, align 4, !tbaa !130
  %22 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %23 = load %struct.avifDecoderItemData*, %struct.avifDecoderItemData** %idat4, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %23, i32 0, i32 1
  %data10 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data, i32 0, i32 0
  store i8* %22, i8** %data10, align 4, !tbaa !186
  %24 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %25 = load %struct.avifDecoderItemData*, %struct.avifDecoderItemData** %idat4, align 4, !tbaa !2
  %data11 = getelementptr inbounds %struct.avifDecoderItemData, %struct.avifDecoderItemData* %25, i32 0, i32 1
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data11, i32 0, i32 1
  store i32 %24, i32* %size, align 4, !tbaa !187
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %26 = bitcast %struct.avifDecoderItemData** %idat4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  br label %return

return:                                           ; preds = %for.end, %cleanup
  %28 = load i32, i32* %retval, align 4
  ret i32 %28

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i32 @avifParseItemPropertiesBox(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %ipcoHeader = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ipmaHeader = alloca %struct.avifBoxHeader, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  %4 = bitcast %struct.avifBoxHeader* %ipcoHeader to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %ipcoHeader)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %ipcoHeader, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call1 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.21, i32 0, i32 0), i32 4)
  %cmp = icmp ne i32 %call1, 0
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %do.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end3:                                          ; preds = %do.end
  br label %do.body4

do.body4:                                         ; preds = %if.end3
  %5 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %5, i32 0, i32 1
  %call5 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size6 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %ipcoHeader, i32 0, i32 0
  %6 = load i32, i32* %size6, align 4, !tbaa !10
  %call7 = call i32 @avifParseItemPropertyContainerBox(%struct.avifPropertyArray* %properties, i8* %call5, i32 %6)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %do.body4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end10:                                         ; preds = %do.body4
  br label %do.cond11

do.cond11:                                        ; preds = %if.end10
  br label %do.end12

do.end12:                                         ; preds = %do.cond11
  br label %do.body13

do.body13:                                        ; preds = %do.end12
  %size14 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %ipcoHeader, i32 0, i32 0
  %7 = load i32, i32* %size14, align 4, !tbaa !10
  %call15 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %7)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %do.body13
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end18:                                         ; preds = %do.body13
  br label %do.cond19

do.cond19:                                        ; preds = %if.end18
  br label %do.end20

do.end20:                                         ; preds = %do.cond19
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %do.end20
  %call21 = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool22 = icmp ne i32 %call21, 0
  br i1 %tobool22, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = bitcast %struct.avifBoxHeader* %ipmaHeader to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #4
  br label %do.body23

do.body23:                                        ; preds = %while.body
  %call24 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %ipmaHeader)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %do.body23
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %do.body23
  br label %do.cond28

do.cond28:                                        ; preds = %if.end27
  br label %do.end29

do.end29:                                         ; preds = %do.cond28
  %type30 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %ipmaHeader, i32 0, i32 1
  %arraydecay31 = getelementptr inbounds [4 x i8], [4 x i8]* %type30, i32 0, i32 0
  %call32 = call i32 @memcmp(i8* %arraydecay31, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.22, i32 0, i32 0), i32 4)
  %tobool33 = icmp ne i32 %call32, 0
  br i1 %tobool33, label %if.else, label %if.then34

if.then34:                                        ; preds = %do.end29
  br label %do.body35

do.body35:                                        ; preds = %if.then34
  %9 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %call36 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size37 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %ipmaHeader, i32 0, i32 0
  %10 = load i32, i32* %size37, align 4, !tbaa !10
  %call38 = call i32 @avifParseItemPropertyAssociation(%struct.avifMeta* %9, i8* %call36, i32 %10)
  %tobool39 = icmp ne i32 %call38, 0
  br i1 %tobool39, label %if.end41, label %if.then40

if.then40:                                        ; preds = %do.body35
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end41:                                         ; preds = %do.body35
  br label %do.cond42

do.cond42:                                        ; preds = %if.end41
  br label %do.end43

do.end43:                                         ; preds = %do.cond42
  br label %if.end44

if.else:                                          ; preds = %do.end29
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end44:                                         ; preds = %do.end43
  br label %do.body45

do.body45:                                        ; preds = %if.end44
  %size46 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %ipmaHeader, i32 0, i32 0
  %11 = load i32, i32* %size46, align 4, !tbaa !10
  %call47 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %11)
  %tobool48 = icmp ne i32 %call47, 0
  br i1 %tobool48, label %if.end50, label %if.then49

if.then49:                                        ; preds = %do.body45
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end50:                                         ; preds = %do.body45
  br label %do.cond51

do.cond51:                                        ; preds = %if.end50
  br label %do.end52

do.end52:                                         ; preds = %do.cond51
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end52, %if.then49, %if.else, %if.then40, %if.then26
  %12 = bitcast %struct.avifBoxHeader* %ipmaHeader to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup53 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

cleanup53:                                        ; preds = %while.end, %cleanup, %if.then17, %if.then9, %if.then2, %if.then
  %13 = bitcast %struct.avifBoxHeader* %ipcoHeader to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #4
  %14 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %14) #4
  %15 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #4
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: nounwind
define internal i32 @avifParseItemInfoBox(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %version = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %entryCount = alloca i32, align 4
  %tmp = alloca i16, align 2
  %entryIndex = alloca i32, align 4
  %infeHeader = alloca %struct.avifBoxHeader, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %s, i8* %version, i32* null)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup62

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i8, i8* %version, align 1, !tbaa !25
  %conv = zext i8 %5 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %do.end
  %6 = bitcast i16* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %6) #4
  br label %do.body3

do.body3:                                         ; preds = %if.then2
  %call4 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %do.body3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %do.body3
  br label %do.cond8

do.cond8:                                         ; preds = %if.end7
  br label %do.end9

do.end9:                                          ; preds = %do.cond8
  %7 = load i16, i16* %tmp, align 2, !tbaa !155
  %conv10 = zext i16 %7 to i32
  store i32 %conv10, i32* %entryCount, align 4, !tbaa !12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end9, %if.then6
  %8 = bitcast i16* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %8) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup61 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end24

if.else:                                          ; preds = %do.end
  %9 = load i8, i8* %version, align 1, !tbaa !25
  %conv11 = zext i8 %9 to i32
  %cmp12 = icmp eq i32 %conv11, 1
  br i1 %cmp12, label %if.then14, label %if.else22

if.then14:                                        ; preds = %if.else
  br label %do.body15

do.body15:                                        ; preds = %if.then14
  %call16 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %entryCount)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %do.body15
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end19:                                         ; preds = %do.body15
  br label %do.cond20

do.cond20:                                        ; preds = %if.end19
  br label %do.end21

do.end21:                                         ; preds = %do.cond20
  br label %if.end23

if.else22:                                        ; preds = %if.else
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end23:                                         ; preds = %do.end21
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %cleanup.cont
  %10 = bitcast i32* %entryIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  store i32 0, i32* %entryIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end24
  %11 = load i32, i32* %entryIndex, align 4, !tbaa !12
  %12 = load i32, i32* %entryCount, align 4, !tbaa !12
  %cmp25 = icmp ult i32 %11, %12
  br i1 %cmp25, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup59

for.body:                                         ; preds = %for.cond
  %13 = bitcast %struct.avifBoxHeader* %infeHeader to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #4
  br label %do.body27

do.body27:                                        ; preds = %for.body
  %call28 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %infeHeader)
  %tobool29 = icmp ne i32 %call28, 0
  br i1 %tobool29, label %if.end31, label %if.then30

if.then30:                                        ; preds = %do.body27
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

if.end31:                                         ; preds = %do.body27
  br label %do.cond32

do.cond32:                                        ; preds = %if.end31
  br label %do.end33

do.end33:                                         ; preds = %do.cond32
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %infeHeader, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call34 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.27, i32 0, i32 0), i32 4)
  %tobool35 = icmp ne i32 %call34, 0
  br i1 %tobool35, label %if.else46, label %if.then36

if.then36:                                        ; preds = %do.end33
  br label %do.body37

do.body37:                                        ; preds = %if.then36
  %14 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %call38 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size39 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %infeHeader, i32 0, i32 0
  %15 = load i32, i32* %size39, align 4, !tbaa !10
  %call40 = call i32 @avifParseItemInfoEntry(%struct.avifMeta* %14, i8* %call38, i32 %15)
  %tobool41 = icmp ne i32 %call40, 0
  br i1 %tobool41, label %if.end43, label %if.then42

if.then42:                                        ; preds = %do.body37
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

if.end43:                                         ; preds = %do.body37
  br label %do.cond44

do.cond44:                                        ; preds = %if.end43
  br label %do.end45

do.end45:                                         ; preds = %do.cond44
  br label %if.end47

if.else46:                                        ; preds = %do.end33
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

if.end47:                                         ; preds = %do.end45
  br label %do.body48

do.body48:                                        ; preds = %if.end47
  %size49 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %infeHeader, i32 0, i32 0
  %16 = load i32, i32* %size49, align 4, !tbaa !10
  %call50 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %16)
  %tobool51 = icmp ne i32 %call50, 0
  br i1 %tobool51, label %if.end53, label %if.then52

if.then52:                                        ; preds = %do.body48
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

if.end53:                                         ; preds = %do.body48
  br label %do.cond54

do.cond54:                                        ; preds = %if.end53
  br label %do.end55

do.end55:                                         ; preds = %do.cond54
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

cleanup56:                                        ; preds = %do.end55, %if.then52, %if.else46, %if.then42, %if.then30
  %17 = bitcast %struct.avifBoxHeader* %infeHeader to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %17) #4
  %cleanup.dest57 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest57, label %cleanup59 [
    i32 0, label %cleanup.cont58
  ]

cleanup.cont58:                                   ; preds = %cleanup56
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont58
  %18 = load i32, i32* %entryIndex, align 4, !tbaa !12
  %inc = add i32 %18, 1
  store i32 %inc, i32* %entryIndex, align 4, !tbaa !12
  br label %for.cond

cleanup59:                                        ; preds = %cleanup56, %for.cond.cleanup
  %19 = bitcast i32* %entryIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %cleanup.dest60 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest60, label %cleanup61 [
    i32 8, label %for.end
  ]

for.end:                                          ; preds = %cleanup59
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

cleanup61:                                        ; preds = %for.end, %cleanup59, %if.else22, %if.then18, %cleanup
  %20 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  br label %cleanup62

cleanup62:                                        ; preds = %cleanup61, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %21 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %21) #4
  %22 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #4
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: nounwind
define internal i32 @avifParseItemReferenceBox(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %version = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %irefHeader = alloca %struct.avifBoxHeader, align 4
  %fromID = alloca i32, align 4
  %tmp = alloca i16, align 2
  %referenceCount = alloca i16, align 2
  %refIndex = alloca i16, align 2
  %toID = alloca i32, align 4
  %tmp49 = alloca i16, align 2
  %item = alloca %struct.avifDecoderItem*, align 4
  %dimg = alloca %struct.avifDecoderItem*, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %s, i8* %version, i32* null)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont125, %do.end
  %call1 = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %struct.avifBoxHeader* %irefHeader to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  br label %do.body3

do.body3:                                         ; preds = %while.body
  %call4 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %irefHeader)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %do.body3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup123

if.end7:                                          ; preds = %do.body3
  br label %do.cond8

do.cond8:                                         ; preds = %if.end7
  br label %do.end9

do.end9:                                          ; preds = %do.cond8
  %5 = bitcast i32* %fromID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %fromID, align 4, !tbaa !12
  %6 = load i8, i8* %version, align 1, !tbaa !25
  %conv = zext i8 %6 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then11, label %if.else

if.then11:                                        ; preds = %do.end9
  %7 = bitcast i16* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #4
  br label %do.body12

do.body12:                                        ; preds = %if.then11
  %call13 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp)
  %tobool14 = icmp ne i32 %call13, 0
  br i1 %tobool14, label %if.end16, label %if.then15

if.then15:                                        ; preds = %do.body12
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %do.body12
  br label %do.cond17

do.cond17:                                        ; preds = %if.end16
  br label %do.end18

do.end18:                                         ; preds = %do.cond17
  %8 = load i16, i16* %tmp, align 2, !tbaa !155
  %conv19 = zext i16 %8 to i32
  store i32 %conv19, i32* %fromID, align 4, !tbaa !12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end18, %if.then15
  %9 = bitcast i16* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %9) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup122 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end33

if.else:                                          ; preds = %do.end9
  %10 = load i8, i8* %version, align 1, !tbaa !25
  %conv20 = zext i8 %10 to i32
  %cmp21 = icmp eq i32 %conv20, 1
  br i1 %cmp21, label %if.then23, label %if.else31

if.then23:                                        ; preds = %if.else
  br label %do.body24

do.body24:                                        ; preds = %if.then23
  %call25 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %fromID)
  %tobool26 = icmp ne i32 %call25, 0
  br i1 %tobool26, label %if.end28, label %if.then27

if.then27:                                        ; preds = %do.body24
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup122

if.end28:                                         ; preds = %do.body24
  br label %do.cond29

do.cond29:                                        ; preds = %if.end28
  br label %do.end30

do.end30:                                         ; preds = %do.cond29
  br label %if.end32

if.else31:                                        ; preds = %if.else
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup122

if.end32:                                         ; preds = %do.end30
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %cleanup.cont
  %11 = bitcast i16* %referenceCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %11) #4
  store i16 0, i16* %referenceCount, align 2, !tbaa !155
  br label %do.body34

do.body34:                                        ; preds = %if.end33
  %call35 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %referenceCount)
  %tobool36 = icmp ne i32 %call35, 0
  br i1 %tobool36, label %if.end38, label %if.then37

if.then37:                                        ; preds = %do.body34
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup121

if.end38:                                         ; preds = %do.body34
  br label %do.cond39

do.cond39:                                        ; preds = %if.end38
  br label %do.end40

do.end40:                                         ; preds = %do.cond39
  %12 = bitcast i16* %refIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  store i16 0, i16* %refIndex, align 2, !tbaa !155
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end40
  %13 = load i16, i16* %refIndex, align 2, !tbaa !155
  %conv41 = zext i16 %13 to i32
  %14 = load i16, i16* %referenceCount, align 2, !tbaa !155
  %conv42 = zext i16 %14 to i32
  %cmp43 = icmp slt i32 %conv41, %conv42
  br i1 %cmp43, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 14, i32* %cleanup.dest.slot, align 4
  br label %cleanup119

for.body:                                         ; preds = %for.cond
  %15 = bitcast i32* %toID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  store i32 0, i32* %toID, align 4, !tbaa !12
  %16 = load i8, i8* %version, align 1, !tbaa !25
  %conv45 = zext i8 %16 to i32
  %cmp46 = icmp eq i32 %conv45, 0
  br i1 %cmp46, label %if.then48, label %if.else61

if.then48:                                        ; preds = %for.body
  %17 = bitcast i16* %tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %17) #4
  br label %do.body50

do.body50:                                        ; preds = %if.then48
  %call51 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp49)
  %tobool52 = icmp ne i32 %call51, 0
  br i1 %tobool52, label %if.end54, label %if.then53

if.then53:                                        ; preds = %do.body50
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup58

if.end54:                                         ; preds = %do.body50
  br label %do.cond55

do.cond55:                                        ; preds = %if.end54
  br label %do.end56

do.end56:                                         ; preds = %do.cond55
  %18 = load i16, i16* %tmp49, align 2, !tbaa !155
  %conv57 = zext i16 %18 to i32
  store i32 %conv57, i32* %toID, align 4, !tbaa !12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup58

cleanup58:                                        ; preds = %do.end56, %if.then53
  %19 = bitcast i16* %tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %19) #4
  %cleanup.dest59 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest59, label %cleanup116 [
    i32 0, label %cleanup.cont60
  ]

cleanup.cont60:                                   ; preds = %cleanup58
  br label %if.end75

if.else61:                                        ; preds = %for.body
  %20 = load i8, i8* %version, align 1, !tbaa !25
  %conv62 = zext i8 %20 to i32
  %cmp63 = icmp eq i32 %conv62, 1
  br i1 %cmp63, label %if.then65, label %if.else73

if.then65:                                        ; preds = %if.else61
  br label %do.body66

do.body66:                                        ; preds = %if.then65
  %call67 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %toID)
  %tobool68 = icmp ne i32 %call67, 0
  br i1 %tobool68, label %if.end70, label %if.then69

if.then69:                                        ; preds = %do.body66
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup116

if.end70:                                         ; preds = %do.body66
  br label %do.cond71

do.cond71:                                        ; preds = %if.end70
  br label %do.end72

do.end72:                                         ; preds = %do.cond71
  br label %if.end74

if.else73:                                        ; preds = %if.else61
  store i32 14, i32* %cleanup.dest.slot, align 4
  br label %cleanup116

if.end74:                                         ; preds = %do.end72
  br label %if.end75

if.end75:                                         ; preds = %if.end74, %cleanup.cont60
  %21 = load i32, i32* %fromID, align 4, !tbaa !12
  %tobool76 = icmp ne i32 %21, 0
  br i1 %tobool76, label %land.lhs.true, label %if.end115

land.lhs.true:                                    ; preds = %if.end75
  %22 = load i32, i32* %toID, align 4, !tbaa !12
  %tobool77 = icmp ne i32 %22, 0
  br i1 %tobool77, label %if.then78, label %if.end115

if.then78:                                        ; preds = %land.lhs.true
  %23 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %25 = load i32, i32* %fromID, align 4, !tbaa !12
  %call79 = call %struct.avifDecoderItem* @avifMetaFindItem(%struct.avifMeta* %24, i32 %25)
  store %struct.avifDecoderItem* %call79, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %26 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %tobool80 = icmp ne %struct.avifDecoderItem* %26, null
  br i1 %tobool80, label %if.end82, label %if.then81

if.then81:                                        ; preds = %if.then78
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup112

if.end82:                                         ; preds = %if.then78
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %irefHeader, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call83 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.29, i32 0, i32 0), i32 4)
  %tobool84 = icmp ne i32 %call83, 0
  br i1 %tobool84, label %if.end86, label %if.then85

if.then85:                                        ; preds = %if.end82
  %27 = load i32, i32* %toID, align 4, !tbaa !12
  %28 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %thumbnailForID = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %28, i32 0, i32 8
  store i32 %27, i32* %thumbnailForID, align 4, !tbaa !86
  br label %if.end86

if.end86:                                         ; preds = %if.then85, %if.end82
  %type87 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %irefHeader, i32 0, i32 1
  %arraydecay88 = getelementptr inbounds [4 x i8], [4 x i8]* %type87, i32 0, i32 0
  %call89 = call i32 @memcmp(i8* %arraydecay88, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.30, i32 0, i32 0), i32 4)
  %tobool90 = icmp ne i32 %call89, 0
  br i1 %tobool90, label %if.end92, label %if.then91

if.then91:                                        ; preds = %if.end86
  %29 = load i32, i32* %toID, align 4, !tbaa !12
  %30 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %auxForID = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %30, i32 0, i32 9
  store i32 %29, i32* %auxForID, align 4, !tbaa !89
  br label %if.end92

if.end92:                                         ; preds = %if.then91, %if.end86
  %type93 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %irefHeader, i32 0, i32 1
  %arraydecay94 = getelementptr inbounds [4 x i8], [4 x i8]* %type93, i32 0, i32 0
  %call95 = call i32 @memcmp(i8* %arraydecay94, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.31, i32 0, i32 0), i32 4)
  %tobool96 = icmp ne i32 %call95, 0
  br i1 %tobool96, label %if.end98, label %if.then97

if.then97:                                        ; preds = %if.end92
  %31 = load i32, i32* %toID, align 4, !tbaa !12
  %32 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %descForID = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %32, i32 0, i32 10
  store i32 %31, i32* %descForID, align 4, !tbaa !138
  br label %if.end98

if.end98:                                         ; preds = %if.then97, %if.end92
  %type99 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %irefHeader, i32 0, i32 1
  %arraydecay100 = getelementptr inbounds [4 x i8], [4 x i8]* %type99, i32 0, i32 0
  %call101 = call i32 @memcmp(i8* %arraydecay100, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.32, i32 0, i32 0), i32 4)
  %tobool102 = icmp ne i32 %call101, 0
  br i1 %tobool102, label %if.end111, label %if.then103

if.then103:                                       ; preds = %if.end98
  %33 = bitcast %struct.avifDecoderItem** %dimg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %35 = load i32, i32* %toID, align 4, !tbaa !12
  %call104 = call %struct.avifDecoderItem* @avifMetaFindItem(%struct.avifMeta* %34, i32 %35)
  store %struct.avifDecoderItem* %call104, %struct.avifDecoderItem** %dimg, align 4, !tbaa !2
  %36 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %dimg, align 4, !tbaa !2
  %tobool105 = icmp ne %struct.avifDecoderItem* %36, null
  br i1 %tobool105, label %if.end107, label %if.then106

if.then106:                                       ; preds = %if.then103
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup108

if.end107:                                        ; preds = %if.then103
  %37 = load i32, i32* %fromID, align 4, !tbaa !12
  %38 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %dimg, align 4, !tbaa !2
  %dimgForID = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %38, i32 0, i32 11
  store i32 %37, i32* %dimgForID, align 4, !tbaa !161
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup108

cleanup108:                                       ; preds = %if.end107, %if.then106
  %39 = bitcast %struct.avifDecoderItem** %dimg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %cleanup.dest109 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest109, label %cleanup112 [
    i32 0, label %cleanup.cont110
  ]

cleanup.cont110:                                  ; preds = %cleanup108
  br label %if.end111

if.end111:                                        ; preds = %cleanup.cont110, %if.end98
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup112

cleanup112:                                       ; preds = %if.end111, %cleanup108, %if.then81
  %40 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %cleanup.dest113 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest113, label %cleanup116 [
    i32 0, label %cleanup.cont114
  ]

cleanup.cont114:                                  ; preds = %cleanup112
  br label %if.end115

if.end115:                                        ; preds = %cleanup.cont114, %land.lhs.true, %if.end75
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup116

cleanup116:                                       ; preds = %if.end115, %cleanup112, %if.else73, %if.then69, %cleanup58
  %41 = bitcast i32* %toID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %cleanup.dest117 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest117, label %cleanup119 [
    i32 0, label %cleanup.cont118
  ]

cleanup.cont118:                                  ; preds = %cleanup116
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont118
  %42 = load i16, i16* %refIndex, align 2, !tbaa !155
  %inc = add i16 %42, 1
  store i16 %inc, i16* %refIndex, align 2, !tbaa !155
  br label %for.cond

cleanup119:                                       ; preds = %cleanup116, %for.cond.cleanup
  %43 = bitcast i16* %refIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %43) #4
  %cleanup.dest120 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest120, label %cleanup121 [
    i32 14, label %for.end
  ]

for.end:                                          ; preds = %cleanup119
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup121

cleanup121:                                       ; preds = %for.end, %cleanup119, %if.then37
  %44 = bitcast i16* %referenceCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %44) #4
  br label %cleanup122

cleanup122:                                       ; preds = %cleanup121, %if.else31, %if.then27, %cleanup
  %45 = bitcast i32* %fromID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  br label %cleanup123

cleanup123:                                       ; preds = %cleanup122, %if.then6
  %46 = bitcast %struct.avifBoxHeader* %irefHeader to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %46) #4
  %cleanup.dest124 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest124, label %cleanup126 [
    i32 0, label %cleanup.cont125
    i32 5, label %while.end
  ]

cleanup.cont125:                                  ; preds = %cleanup123
  br label %while.cond

while.end:                                        ; preds = %cleanup123, %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

cleanup126:                                       ; preds = %while.end, %cleanup123, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %47 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %47) #4
  %48 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %48) #4
  %49 = load i32, i32* %retval, align 4
  ret i32 %49
}

declare i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream*, i8*, i32*) #2

declare i32 @avifROStreamReadU16(%struct.avifROStream*, i16*) #2

declare i32 @avifROStreamReadUX8(%struct.avifROStream*, i64*, i64) #2

; Function Attrs: nounwind
define internal %struct.avifDecoderItem* @avifMetaFindItem(%struct.avifMeta* %meta, i32 %itemID) #0 {
entry:
  %retval = alloca %struct.avifDecoderItem*, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %itemID.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %item9 = alloca %struct.avifDecoderItem*, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i32 %itemID, i32* %itemID.addr, align 4, !tbaa !12
  %0 = load i32, i32* %itemID.addr, align 4, !tbaa !12
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.avifDecoderItem* null, %struct.avifDecoderItem** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %2 = load i32, i32* %i, align 4, !tbaa !12
  %3 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %3, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items, i32 0, i32 2
  %4 = load i32, i32* %count, align 4, !tbaa !76
  %cmp1 = icmp ult i32 %2, %4
  br i1 %cmp1, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %5 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items2 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %5, i32 0, i32 0
  %item = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items2, i32 0, i32 0
  %6 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !81
  %7 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %6, i32 %7
  %id = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %arrayidx, i32 0, i32 0
  %8 = load i32, i32* %id, align 4, !tbaa !88
  %9 = load i32, i32* %itemID.addr, align 4, !tbaa !12
  %cmp3 = icmp eq i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end8

if.then4:                                         ; preds = %for.body
  %10 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items5 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %10, i32 0, i32 0
  %item6 = getelementptr inbounds %struct.avifDecoderItemArray, %struct.avifDecoderItemArray* %items5, i32 0, i32 0
  %11 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item6, align 4, !tbaa !81
  %12 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx7 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %11, i32 %12
  store %struct.avifDecoderItem* %arrayidx7, %struct.avifDecoderItem** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end8
  %13 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup:                                          ; preds = %if.then4, %for.cond.cleanup
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  %15 = bitcast %struct.avifDecoderItem** %item9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %items10 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %16, i32 0, i32 0
  %17 = bitcast %struct.avifDecoderItemArray* %items10 to i8*
  %call = call i8* @avifArrayPushPtr(i8* %17)
  %18 = bitcast i8* %call to %struct.avifDecoderItem*
  store %struct.avifDecoderItem* %18, %struct.avifDecoderItem** %item9, align 4, !tbaa !2
  %19 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item9, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %19, i32 0, i32 7
  %20 = bitcast %struct.avifPropertyArray* %properties to i8*
  call void @avifArrayCreate(i8* %20, i32 68, i32 16)
  %21 = load i32, i32* %itemID.addr, align 4, !tbaa !12
  %22 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item9, align 4, !tbaa !2
  %id11 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %22, i32 0, i32 0
  store i32 %21, i32* %id11, align 4, !tbaa !88
  %23 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %24 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item9, align 4, !tbaa !2
  %meta12 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %24, i32 0, i32 1
  store %struct.avifMeta* %23, %struct.avifMeta** %meta12, align 4, !tbaa !127
  %25 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item9, align 4, !tbaa !2
  store %struct.avifDecoderItem* %25, %struct.avifDecoderItem** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %26 = bitcast %struct.avifDecoderItem** %item9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  br label %return

return:                                           ; preds = %for.end, %cleanup, %if.then
  %27 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %retval, align 4
  ret %struct.avifDecoderItem* %27

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i32 @avifArrayPushIndex(i8*) #2

; Function Attrs: nounwind
define internal i32 @avifParseItemPropertyContainerBox(%struct.avifPropertyArray* %properties, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %properties.addr = alloca %struct.avifPropertyArray*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %propertyIndex = alloca i32, align 4
  %prop = alloca %struct.avifProperty*, align 4
  store %struct.avifPropertyArray* %properties, %struct.avifPropertyArray** %properties.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %call = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  br label %do.body

do.body:                                          ; preds = %while.body
  %call1 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup158

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %5 = bitcast i32* %propertyIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %properties.addr, align 4, !tbaa !2
  %7 = bitcast %struct.avifPropertyArray* %6 to i8*
  %call3 = call i32 @avifArrayPushIndex(i8* %7)
  store i32 %call3, i32* %propertyIndex, align 4, !tbaa !12
  %8 = bitcast %struct.avifProperty** %prop to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.avifPropertyArray*, %struct.avifPropertyArray** %properties.addr, align 4, !tbaa !2
  %prop4 = getelementptr inbounds %struct.avifPropertyArray, %struct.avifPropertyArray* %9, i32 0, i32 0
  %10 = load %struct.avifProperty*, %struct.avifProperty** %prop4, align 4, !tbaa !160
  %11 = load i32, i32* %propertyIndex, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %10, i32 %11
  store %struct.avifProperty* %arrayidx, %struct.avifProperty** %prop, align 4, !tbaa !2
  %12 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %12, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %type5 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay6 = getelementptr inbounds [4 x i8], [4 x i8]* %type5, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %arraydecay, i8* align 4 %arraydecay6, i32 4, i1 false)
  %type7 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay8 = getelementptr inbounds [4 x i8], [4 x i8]* %type7, i32 0, i32 0
  %call9 = call i32 @memcmp(i8* %arraydecay8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0), i32 4)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.else, label %if.then11

if.then11:                                        ; preds = %do.end
  br label %do.body12

do.body12:                                        ; preds = %if.then11
  %13 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call13 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size14 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %14 = load i32, i32* %size14, align 4, !tbaa !10
  %call15 = call i32 @avifParseImageSpatialExtentsProperty(%struct.avifProperty* %13, i8* %call13, i32 %14)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %do.body12
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %do.body12
  br label %do.cond19

do.cond19:                                        ; preds = %if.end18
  br label %do.end20

do.end20:                                         ; preds = %do.cond19
  br label %if.end148

if.else:                                          ; preds = %do.end
  %type21 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay22 = getelementptr inbounds [4 x i8], [4 x i8]* %type21, i32 0, i32 0
  %call23 = call i32 @memcmp(i8* %arraydecay22, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0), i32 4)
  %tobool24 = icmp ne i32 %call23, 0
  br i1 %tobool24, label %if.else35, label %if.then25

if.then25:                                        ; preds = %if.else
  br label %do.body26

do.body26:                                        ; preds = %if.then25
  %15 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call27 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size28 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %16 = load i32, i32* %size28, align 4, !tbaa !10
  %call29 = call i32 @avifParseAuxiliaryTypeProperty(%struct.avifProperty* %15, i8* %call27, i32 %16)
  %tobool30 = icmp ne i32 %call29, 0
  br i1 %tobool30, label %if.end32, label %if.then31

if.then31:                                        ; preds = %do.body26
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end32:                                         ; preds = %do.body26
  br label %do.cond33

do.cond33:                                        ; preds = %if.end32
  br label %do.end34

do.end34:                                         ; preds = %do.cond33
  br label %if.end147

if.else35:                                        ; preds = %if.else
  %type36 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay37 = getelementptr inbounds [4 x i8], [4 x i8]* %type36, i32 0, i32 0
  %call38 = call i32 @memcmp(i8* %arraydecay37, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0), i32 4)
  %tobool39 = icmp ne i32 %call38, 0
  br i1 %tobool39, label %if.else50, label %if.then40

if.then40:                                        ; preds = %if.else35
  br label %do.body41

do.body41:                                        ; preds = %if.then40
  %17 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call42 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size43 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %18 = load i32, i32* %size43, align 4, !tbaa !10
  %call44 = call i32 @avifParseColourInformationBox(%struct.avifProperty* %17, i8* %call42, i32 %18)
  %tobool45 = icmp ne i32 %call44, 0
  br i1 %tobool45, label %if.end47, label %if.then46

if.then46:                                        ; preds = %do.body41
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end47:                                         ; preds = %do.body41
  br label %do.cond48

do.cond48:                                        ; preds = %if.end47
  br label %do.end49

do.end49:                                         ; preds = %do.cond48
  br label %if.end146

if.else50:                                        ; preds = %if.else35
  %type51 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay52 = getelementptr inbounds [4 x i8], [4 x i8]* %type51, i32 0, i32 0
  %call53 = call i32 @memcmp(i8* %arraydecay52, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i32 0, i32 0), i32 4)
  %tobool54 = icmp ne i32 %call53, 0
  br i1 %tobool54, label %if.else65, label %if.then55

if.then55:                                        ; preds = %if.else50
  br label %do.body56

do.body56:                                        ; preds = %if.then55
  %19 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call57 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size58 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %20 = load i32, i32* %size58, align 4, !tbaa !10
  %call59 = call i32 @avifParseAV1CodecConfigurationBoxProperty(%struct.avifProperty* %19, i8* %call57, i32 %20)
  %tobool60 = icmp ne i32 %call59, 0
  br i1 %tobool60, label %if.end62, label %if.then61

if.then61:                                        ; preds = %do.body56
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end62:                                         ; preds = %do.body56
  br label %do.cond63

do.cond63:                                        ; preds = %if.end62
  br label %do.end64

do.end64:                                         ; preds = %do.cond63
  br label %if.end145

if.else65:                                        ; preds = %if.else50
  %type66 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay67 = getelementptr inbounds [4 x i8], [4 x i8]* %type66, i32 0, i32 0
  %call68 = call i32 @memcmp(i8* %arraydecay67, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.6, i32 0, i32 0), i32 4)
  %tobool69 = icmp ne i32 %call68, 0
  br i1 %tobool69, label %if.else80, label %if.then70

if.then70:                                        ; preds = %if.else65
  br label %do.body71

do.body71:                                        ; preds = %if.then70
  %21 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call72 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size73 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %22 = load i32, i32* %size73, align 4, !tbaa !10
  %call74 = call i32 @avifParsePixelAspectRatioBoxProperty(%struct.avifProperty* %21, i8* %call72, i32 %22)
  %tobool75 = icmp ne i32 %call74, 0
  br i1 %tobool75, label %if.end77, label %if.then76

if.then76:                                        ; preds = %do.body71
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end77:                                         ; preds = %do.body71
  br label %do.cond78

do.cond78:                                        ; preds = %if.end77
  br label %do.end79

do.end79:                                         ; preds = %do.cond78
  br label %if.end144

if.else80:                                        ; preds = %if.else65
  %type81 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay82 = getelementptr inbounds [4 x i8], [4 x i8]* %type81, i32 0, i32 0
  %call83 = call i32 @memcmp(i8* %arraydecay82, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.7, i32 0, i32 0), i32 4)
  %tobool84 = icmp ne i32 %call83, 0
  br i1 %tobool84, label %if.else95, label %if.then85

if.then85:                                        ; preds = %if.else80
  br label %do.body86

do.body86:                                        ; preds = %if.then85
  %23 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call87 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size88 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %24 = load i32, i32* %size88, align 4, !tbaa !10
  %call89 = call i32 @avifParseCleanApertureBoxProperty(%struct.avifProperty* %23, i8* %call87, i32 %24)
  %tobool90 = icmp ne i32 %call89, 0
  br i1 %tobool90, label %if.end92, label %if.then91

if.then91:                                        ; preds = %do.body86
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end92:                                         ; preds = %do.body86
  br label %do.cond93

do.cond93:                                        ; preds = %if.end92
  br label %do.end94

do.end94:                                         ; preds = %do.cond93
  br label %if.end143

if.else95:                                        ; preds = %if.else80
  %type96 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay97 = getelementptr inbounds [4 x i8], [4 x i8]* %type96, i32 0, i32 0
  %call98 = call i32 @memcmp(i8* %arraydecay97, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i32 0, i32 0), i32 4)
  %tobool99 = icmp ne i32 %call98, 0
  br i1 %tobool99, label %if.else110, label %if.then100

if.then100:                                       ; preds = %if.else95
  br label %do.body101

do.body101:                                       ; preds = %if.then100
  %25 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call102 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size103 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %26 = load i32, i32* %size103, align 4, !tbaa !10
  %call104 = call i32 @avifParseImageRotationProperty(%struct.avifProperty* %25, i8* %call102, i32 %26)
  %tobool105 = icmp ne i32 %call104, 0
  br i1 %tobool105, label %if.end107, label %if.then106

if.then106:                                       ; preds = %do.body101
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end107:                                        ; preds = %do.body101
  br label %do.cond108

do.cond108:                                       ; preds = %if.end107
  br label %do.end109

do.end109:                                        ; preds = %do.cond108
  br label %if.end142

if.else110:                                       ; preds = %if.else95
  %type111 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay112 = getelementptr inbounds [4 x i8], [4 x i8]* %type111, i32 0, i32 0
  %call113 = call i32 @memcmp(i8* %arraydecay112, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0), i32 4)
  %tobool114 = icmp ne i32 %call113, 0
  br i1 %tobool114, label %if.else125, label %if.then115

if.then115:                                       ; preds = %if.else110
  br label %do.body116

do.body116:                                       ; preds = %if.then115
  %27 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call117 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size118 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %28 = load i32, i32* %size118, align 4, !tbaa !10
  %call119 = call i32 @avifParseImageMirrorProperty(%struct.avifProperty* %27, i8* %call117, i32 %28)
  %tobool120 = icmp ne i32 %call119, 0
  br i1 %tobool120, label %if.end122, label %if.then121

if.then121:                                       ; preds = %do.body116
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end122:                                        ; preds = %do.body116
  br label %do.cond123

do.cond123:                                       ; preds = %if.end122
  br label %do.end124

do.end124:                                        ; preds = %do.cond123
  br label %if.end141

if.else125:                                       ; preds = %if.else110
  %type126 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay127 = getelementptr inbounds [4 x i8], [4 x i8]* %type126, i32 0, i32 0
  %call128 = call i32 @memcmp(i8* %arraydecay127, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.23, i32 0, i32 0), i32 4)
  %tobool129 = icmp ne i32 %call128, 0
  br i1 %tobool129, label %if.end140, label %if.then130

if.then130:                                       ; preds = %if.else125
  br label %do.body131

do.body131:                                       ; preds = %if.then130
  %29 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !2
  %call132 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size133 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %30 = load i32, i32* %size133, align 4, !tbaa !10
  %call134 = call i32 @avifParsePixelInformationProperty(%struct.avifProperty* %29, i8* %call132, i32 %30)
  %tobool135 = icmp ne i32 %call134, 0
  br i1 %tobool135, label %if.end137, label %if.then136

if.then136:                                       ; preds = %do.body131
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end137:                                        ; preds = %do.body131
  br label %do.cond138

do.cond138:                                       ; preds = %if.end137
  br label %do.end139

do.end139:                                        ; preds = %do.cond138
  br label %if.end140

if.end140:                                        ; preds = %do.end139, %if.else125
  br label %if.end141

if.end141:                                        ; preds = %if.end140, %do.end124
  br label %if.end142

if.end142:                                        ; preds = %if.end141, %do.end109
  br label %if.end143

if.end143:                                        ; preds = %if.end142, %do.end94
  br label %if.end144

if.end144:                                        ; preds = %if.end143, %do.end79
  br label %if.end145

if.end145:                                        ; preds = %if.end144, %do.end64
  br label %if.end146

if.end146:                                        ; preds = %if.end145, %do.end49
  br label %if.end147

if.end147:                                        ; preds = %if.end146, %do.end34
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %do.end20
  br label %do.body149

do.body149:                                       ; preds = %if.end148
  %size150 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %31 = load i32, i32* %size150, align 4, !tbaa !10
  %call151 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %31)
  %tobool152 = icmp ne i32 %call151, 0
  br i1 %tobool152, label %if.end154, label %if.then153

if.then153:                                       ; preds = %do.body149
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end154:                                        ; preds = %do.body149
  br label %do.cond155

do.cond155:                                       ; preds = %if.end154
  br label %do.end156

do.end156:                                        ; preds = %do.cond155
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end156, %if.then153, %if.then136, %if.then121, %if.then106, %if.then91, %if.then76, %if.then61, %if.then46, %if.then31, %if.then17
  %32 = bitcast %struct.avifProperty** %prop to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #4
  %33 = bitcast i32* %propertyIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #4
  br label %cleanup158

cleanup158:                                       ; preds = %cleanup, %if.then
  %34 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %34) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup159 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup158
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup159

cleanup159:                                       ; preds = %while.end, %cleanup158
  %35 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %35) #4
  %36 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %36) #4
  %37 = load i32, i32* %retval, align 4
  ret i32 %37
}

; Function Attrs: nounwind
define internal i32 @avifParseItemPropertyAssociation(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %version = alloca i8, align 1
  %flags = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %propertyIndexIsU16 = alloca i32, align 4
  %entryCount = alloca i32, align 4
  %entryIndex = alloca i32, align 4
  %itemID = alloca i32, align 4
  %tmp = alloca i16, align 2
  %associationCount = alloca i8, align 1
  %associationIndex = alloca i8, align 1
  %essential = alloca i32, align 4
  %propertyIndex = alloca i16, align 2
  %tmp61 = alloca i8, align 1
  %item = alloca %struct.avifDecoderItem*, align 4
  %srcProp = alloca %struct.avifProperty*, align 4
  %supportedTypesCount = alloca i32, align 4
  %supportedType = alloca i32, align 4
  %i = alloca i32, align 4
  %dstProp = alloca %struct.avifProperty*, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  %4 = bitcast i32* %flags to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %s, i8* %version, i32* %flags)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup136

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %5 = bitcast i32* %propertyIndexIsU16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %flags, align 4, !tbaa !12
  %and = and i32 %6, 1
  %cmp = icmp ne i32 %and, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %propertyIndexIsU16, align 4, !tbaa !12
  %7 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %entryCount)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup134

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %8 = bitcast i32* %entryIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i32 0, i32* %entryIndex, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc129, %do.end7
  %9 = load i32, i32* %entryIndex, align 4, !tbaa !12
  %10 = load i32, i32* %entryCount, align 4, !tbaa !12
  %cmp8 = icmp ult i32 %9, %10
  br i1 %cmp8, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup131

for.body:                                         ; preds = %for.cond
  %11 = bitcast i32* %itemID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load i8, i8* %version, align 1, !tbaa !25
  %conv10 = zext i8 %12 to i32
  %cmp11 = icmp slt i32 %conv10, 1
  br i1 %cmp11, label %if.then13, label %if.else

if.then13:                                        ; preds = %for.body
  %13 = bitcast i16* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #4
  br label %do.body14

do.body14:                                        ; preds = %if.then13
  %call15 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %do.body14
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %do.body14
  br label %do.cond19

do.cond19:                                        ; preds = %if.end18
  br label %do.end20

do.end20:                                         ; preds = %do.cond19
  %14 = load i16, i16* %tmp, align 2, !tbaa !155
  %conv21 = zext i16 %14 to i32
  store i32 %conv21, i32* %itemID, align 4, !tbaa !12
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end20, %if.then17
  %15 = bitcast i16* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %15) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup126 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end29

if.else:                                          ; preds = %for.body
  br label %do.body22

do.body22:                                        ; preds = %if.else
  %call23 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %itemID)
  %tobool24 = icmp ne i32 %call23, 0
  br i1 %tobool24, label %if.end26, label %if.then25

if.then25:                                        ; preds = %do.body22
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end26:                                         ; preds = %do.body22
  br label %do.cond27

do.cond27:                                        ; preds = %if.end26
  br label %do.end28

do.end28:                                         ; preds = %do.cond27
  br label %if.end29

if.end29:                                         ; preds = %do.end28, %cleanup.cont
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %associationCount) #4
  br label %do.body30

do.body30:                                        ; preds = %if.end29
  %call31 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %associationCount, i32 1)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %if.end34, label %if.then33

if.then33:                                        ; preds = %do.body30
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup125

if.end34:                                         ; preds = %do.body30
  br label %do.cond35

do.cond35:                                        ; preds = %if.end34
  br label %do.end36

do.end36:                                         ; preds = %do.cond35
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %associationIndex) #4
  store i8 0, i8* %associationIndex, align 1, !tbaa !25
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc120, %do.end36
  %16 = load i8, i8* %associationIndex, align 1, !tbaa !25
  %conv38 = zext i8 %16 to i32
  %17 = load i8, i8* %associationCount, align 1, !tbaa !25
  %conv39 = zext i8 %17 to i32
  %cmp40 = icmp slt i32 %conv38, %conv39
  br i1 %cmp40, label %for.body43, label %for.cond.cleanup42

for.cond.cleanup42:                               ; preds = %for.cond37
  store i32 15, i32* %cleanup.dest.slot, align 4
  br label %cleanup122

for.body43:                                       ; preds = %for.cond37
  %18 = bitcast i32* %essential to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  store i32 0, i32* %essential, align 4, !tbaa !12
  %19 = bitcast i16* %propertyIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %19) #4
  store i16 0, i16* %propertyIndex, align 2, !tbaa !155
  %20 = load i32, i32* %propertyIndexIsU16, align 4, !tbaa !12
  %tobool44 = icmp ne i32 %20, 0
  br i1 %tobool44, label %if.then45, label %if.else60

if.then45:                                        ; preds = %for.body43
  br label %do.body46

do.body46:                                        ; preds = %if.then45
  %call47 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %propertyIndex)
  %tobool48 = icmp ne i32 %call47, 0
  br i1 %tobool48, label %if.end50, label %if.then49

if.then49:                                        ; preds = %do.body46
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup116

if.end50:                                         ; preds = %do.body46
  br label %do.cond51

do.cond51:                                        ; preds = %if.end50
  br label %do.end52

do.end52:                                         ; preds = %do.cond51
  %21 = load i16, i16* %propertyIndex, align 2, !tbaa !155
  %conv53 = zext i16 %21 to i32
  %and54 = and i32 %conv53, 32768
  %cmp55 = icmp ne i32 %and54, 0
  %conv56 = zext i1 %cmp55 to i32
  store i32 %conv56, i32* %essential, align 4, !tbaa !12
  %22 = load i16, i16* %propertyIndex, align 2, !tbaa !155
  %conv57 = zext i16 %22 to i32
  %and58 = and i32 %conv57, 32767
  %conv59 = trunc i32 %and58 to i16
  store i16 %conv59, i16* %propertyIndex, align 2, !tbaa !155
  br label %if.end79

if.else60:                                        ; preds = %for.body43
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tmp61) #4
  br label %do.body62

do.body62:                                        ; preds = %if.else60
  %call63 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %tmp61, i32 1)
  %tobool64 = icmp ne i32 %call63, 0
  br i1 %tobool64, label %if.end66, label %if.then65

if.then65:                                        ; preds = %do.body62
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup76

if.end66:                                         ; preds = %do.body62
  br label %do.cond67

do.cond67:                                        ; preds = %if.end66
  br label %do.end68

do.end68:                                         ; preds = %do.cond67
  %23 = load i8, i8* %tmp61, align 1, !tbaa !25
  %conv69 = zext i8 %23 to i32
  %and70 = and i32 %conv69, 128
  %cmp71 = icmp ne i32 %and70, 0
  %conv72 = zext i1 %cmp71 to i32
  store i32 %conv72, i32* %essential, align 4, !tbaa !12
  %24 = load i8, i8* %tmp61, align 1, !tbaa !25
  %conv73 = zext i8 %24 to i32
  %and74 = and i32 %conv73, 127
  %conv75 = trunc i32 %and74 to i16
  store i16 %conv75, i16* %propertyIndex, align 2, !tbaa !155
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup76

cleanup76:                                        ; preds = %do.end68, %if.then65
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tmp61) #4
  %cleanup.dest77 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest77, label %cleanup116 [
    i32 0, label %cleanup.cont78
  ]

cleanup.cont78:                                   ; preds = %cleanup76
  br label %if.end79

if.end79:                                         ; preds = %cleanup.cont78, %do.end52
  %25 = load i16, i16* %propertyIndex, align 2, !tbaa !155
  %conv80 = zext i16 %25 to i32
  %cmp81 = icmp eq i32 %conv80, 0
  br i1 %cmp81, label %if.then83, label %if.end84

if.then83:                                        ; preds = %if.end79
  store i32 17, i32* %cleanup.dest.slot, align 4
  br label %cleanup116

if.end84:                                         ; preds = %if.end79
  %26 = load i16, i16* %propertyIndex, align 2, !tbaa !155
  %dec = add i16 %26, -1
  store i16 %dec, i16* %propertyIndex, align 2, !tbaa !155
  %27 = load i16, i16* %propertyIndex, align 2, !tbaa !155
  %conv85 = zext i16 %27 to i32
  %28 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %28, i32 0, i32 1
  %count = getelementptr inbounds %struct.avifPropertyArray, %struct.avifPropertyArray* %properties, i32 0, i32 2
  %29 = load i32, i32* %count, align 4, !tbaa !188
  %cmp86 = icmp uge i32 %conv85, %29
  br i1 %cmp86, label %if.then88, label %if.end89

if.then88:                                        ; preds = %if.end84
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup116

if.end89:                                         ; preds = %if.end84
  %30 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %32 = load i32, i32* %itemID, align 4, !tbaa !12
  %call90 = call %struct.avifDecoderItem* @avifMetaFindItem(%struct.avifMeta* %31, i32 %32)
  store %struct.avifDecoderItem* %call90, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %33 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %tobool91 = icmp ne %struct.avifDecoderItem* %33, null
  br i1 %tobool91, label %if.end93, label %if.then92

if.then92:                                        ; preds = %if.end89
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup115

if.end93:                                         ; preds = %if.end89
  %34 = bitcast %struct.avifProperty** %srcProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %properties94 = getelementptr inbounds %struct.avifMeta, %struct.avifMeta* %35, i32 0, i32 1
  %prop = getelementptr inbounds %struct.avifPropertyArray, %struct.avifPropertyArray* %properties94, i32 0, i32 0
  %36 = load %struct.avifProperty*, %struct.avifProperty** %prop, align 4, !tbaa !189
  %37 = load i16, i16* %propertyIndex, align 2, !tbaa !155
  %idxprom = zext i16 %37 to i32
  %arrayidx = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %36, i32 %idxprom
  store %struct.avifProperty* %arrayidx, %struct.avifProperty** %srcProp, align 4, !tbaa !2
  %38 = bitcast i32* %supportedTypesCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #4
  store i32 9, i32* %supportedTypesCount, align 4, !tbaa !14
  %39 = bitcast i32* %supportedType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #4
  store i32 0, i32* %supportedType, align 4, !tbaa !12
  %40 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #4
  store i32 0, i32* %i, align 4, !tbaa !14
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc, %if.end93
  %41 = load i32, i32* %i, align 4, !tbaa !14
  %42 = load i32, i32* %supportedTypesCount, align 4, !tbaa !14
  %cmp96 = icmp ult i32 %41, %42
  br i1 %cmp96, label %for.body99, label %for.cond.cleanup98

for.cond.cleanup98:                               ; preds = %for.cond95
  store i32 22, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

for.body99:                                       ; preds = %for.cond95
  %43 = load %struct.avifProperty*, %struct.avifProperty** %srcProp, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %43, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %44 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx100 = getelementptr inbounds [9 x i8*], [9 x i8*]* @avifParseItemPropertyAssociation.supportedTypes, i32 0, i32 %44
  %45 = load i8*, i8** %arrayidx100, align 4, !tbaa !2
  %call101 = call i32 @memcmp(i8* %arraydecay, i8* %45, i32 4)
  %tobool102 = icmp ne i32 %call101, 0
  br i1 %tobool102, label %if.end104, label %if.then103

if.then103:                                       ; preds = %for.body99
  store i32 1, i32* %supportedType, align 4, !tbaa !12
  store i32 22, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end104:                                        ; preds = %for.body99
  br label %for.inc

for.inc:                                          ; preds = %if.end104
  %46 = load i32, i32* %i, align 4, !tbaa !14
  %inc = add i32 %46, 1
  store i32 %inc, i32* %i, align 4, !tbaa !14
  br label %for.cond95

cleanup105:                                       ; preds = %if.then103, %for.cond.cleanup98
  %47 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  br label %for.end

for.end:                                          ; preds = %cleanup105
  %48 = load i32, i32* %supportedType, align 4, !tbaa !12
  %tobool106 = icmp ne i32 %48, 0
  br i1 %tobool106, label %if.then107, label %if.else110

if.then107:                                       ; preds = %for.end
  %49 = bitcast %struct.avifProperty** %dstProp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #4
  %50 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %properties108 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %50, i32 0, i32 7
  %51 = bitcast %struct.avifPropertyArray* %properties108 to i8*
  %call109 = call i8* @avifArrayPushPtr(i8* %51)
  %52 = bitcast i8* %call109 to %struct.avifProperty*
  store %struct.avifProperty* %52, %struct.avifProperty** %dstProp, align 4, !tbaa !2
  %53 = load %struct.avifProperty*, %struct.avifProperty** %dstProp, align 4, !tbaa !2
  %54 = bitcast %struct.avifProperty* %53 to i8*
  %55 = load %struct.avifProperty*, %struct.avifProperty** %srcProp, align 4, !tbaa !2
  %56 = bitcast %struct.avifProperty* %55 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %54, i8* align 4 %56, i32 68, i1 false)
  %57 = bitcast %struct.avifProperty** %dstProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  br label %if.end114

if.else110:                                       ; preds = %for.end
  %58 = load i32, i32* %essential, align 4, !tbaa !12
  %tobool111 = icmp ne i32 %58, 0
  br i1 %tobool111, label %if.then112, label %if.end113

if.then112:                                       ; preds = %if.else110
  %59 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %hasUnsupportedEssentialProperty = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %59, i32 0, i32 12
  store i32 1, i32* %hasUnsupportedEssentialProperty, align 4, !tbaa !85
  br label %if.end113

if.end113:                                        ; preds = %if.then112, %if.else110
  br label %if.end114

if.end114:                                        ; preds = %if.end113, %if.then107
  %60 = bitcast i32* %supportedType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %61 = bitcast i32* %supportedTypesCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %62 = bitcast %struct.avifProperty** %srcProp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup115

cleanup115:                                       ; preds = %if.end114, %if.then92
  %63 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  br label %cleanup116

cleanup116:                                       ; preds = %cleanup115, %if.then88, %if.then83, %cleanup76, %if.then49
  %64 = bitcast i16* %propertyIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %64) #4
  %65 = bitcast i32* %essential to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  %cleanup.dest118 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest118, label %cleanup122 [
    i32 0, label %cleanup.cont119
    i32 17, label %for.inc120
  ]

cleanup.cont119:                                  ; preds = %cleanup116
  br label %for.inc120

for.inc120:                                       ; preds = %cleanup.cont119, %cleanup116
  %66 = load i8, i8* %associationIndex, align 1, !tbaa !25
  %inc121 = add i8 %66, 1
  store i8 %inc121, i8* %associationIndex, align 1, !tbaa !25
  br label %for.cond37

cleanup122:                                       ; preds = %cleanup116, %for.cond.cleanup42
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %associationIndex) #4
  %cleanup.dest123 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest123, label %cleanup125 [
    i32 15, label %for.end124
  ]

for.end124:                                       ; preds = %cleanup122
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup125

cleanup125:                                       ; preds = %for.end124, %cleanup122, %if.then33
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %associationCount) #4
  br label %cleanup126

cleanup126:                                       ; preds = %cleanup125, %if.then25, %cleanup
  %67 = bitcast i32* %itemID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  %cleanup.dest127 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest127, label %cleanup131 [
    i32 0, label %cleanup.cont128
  ]

cleanup.cont128:                                  ; preds = %cleanup126
  br label %for.inc129

for.inc129:                                       ; preds = %cleanup.cont128
  %68 = load i32, i32* %entryIndex, align 4, !tbaa !12
  %inc130 = add i32 %68, 1
  store i32 %inc130, i32* %entryIndex, align 4, !tbaa !12
  br label %for.cond

cleanup131:                                       ; preds = %cleanup126, %for.cond.cleanup
  %69 = bitcast i32* %entryIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #4
  %cleanup.dest132 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest132, label %cleanup134 [
    i32 6, label %for.end133
  ]

for.end133:                                       ; preds = %cleanup131
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup134

cleanup134:                                       ; preds = %for.end133, %cleanup131, %if.then4
  %70 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #4
  %71 = bitcast i32* %propertyIndexIsU16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  br label %cleanup136

cleanup136:                                       ; preds = %cleanup134, %if.then
  %72 = bitcast i32* %flags to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %73 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %73) #4
  %74 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %74) #4
  %75 = load i32, i32* %retval, align 4
  ret i32 %75
}

; Function Attrs: nounwind
define internal i32 @avifParseImageSpatialExtentsProperty(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ispe = alloca %struct.avifImageSpatialExtents*, align 4
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup16

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast %struct.avifImageSpatialExtents** %ispe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %5, i32 0, i32 1
  %ispe1 = bitcast %union.anon* %u to %struct.avifImageSpatialExtents*
  store %struct.avifImageSpatialExtents* %ispe1, %struct.avifImageSpatialExtents** %ispe, align 4, !tbaa !2
  br label %do.body2

do.body2:                                         ; preds = %do.end
  %6 = load %struct.avifImageSpatialExtents*, %struct.avifImageSpatialExtents** %ispe, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImageSpatialExtents, %struct.avifImageSpatialExtents* %6, i32 0, i32 0
  %call3 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %width)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %do.body2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %do.body2
  br label %do.cond7

do.cond7:                                         ; preds = %if.end6
  br label %do.end8

do.end8:                                          ; preds = %do.cond7
  br label %do.body9

do.body9:                                         ; preds = %do.end8
  %7 = load %struct.avifImageSpatialExtents*, %struct.avifImageSpatialExtents** %ispe, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImageSpatialExtents, %struct.avifImageSpatialExtents* %7, i32 0, i32 1
  %call10 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %height)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %do.body9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %do.body9
  br label %do.cond14

do.cond14:                                        ; preds = %if.end13
  br label %do.end15

do.end15:                                         ; preds = %do.cond14
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end15, %if.then12, %if.then5
  %8 = bitcast %struct.avifImageSpatialExtents** %ispe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  br label %cleanup16

cleanup16:                                        ; preds = %cleanup, %if.then
  %9 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %9) #4
  %10 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #4
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: nounwind
define internal i32 @avifParseAuxiliaryTypeProperty(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %4 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %4, i32 0, i32 1
  %auxC = bitcast %union.anon* %u to %struct.avifAuxiliaryType*
  %auxType = getelementptr inbounds %struct.avifAuxiliaryType, %struct.avifAuxiliaryType* %auxC, i32 0, i32 0
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %auxType, i32 0, i32 0
  %call2 = call i32 @avifROStreamReadString(%struct.avifROStream* %s, i8* %arraydecay, i32 64)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end7, %if.then4, %if.then
  %5 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %5) #4
  %6 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %6) #4
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: nounwind
define internal i32 @avifParseColourInformationBox(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %colr = alloca %struct.avifColourInformationBox*, align 4
  %colorType = alloca [4 x i8], align 1
  %cleanup.dest.slot = alloca i32, align 4
  %tmp16 = alloca i16, align 2
  %tmp8 = alloca i8, align 1
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  %4 = bitcast %struct.avifColourInformationBox** %colr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %5, i32 0, i32 1
  %colr1 = bitcast %union.anon* %u to %struct.avifColourInformationBox*
  store %struct.avifColourInformationBox* %colr1, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %6 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %hasICC = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %6, i32 0, i32 0
  store i32 0, i32* %hasICC, align 4, !tbaa !190
  %7 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %hasNCLX = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %7, i32 0, i32 3
  store i32 0, i32* %hasNCLX, align 4, !tbaa !192
  %8 = bitcast [4 x i8]* %colorType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %colorType, i32 0, i32 0
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %arraydecay, i32 4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %arraydecay2 = getelementptr inbounds [4 x i8], [4 x i8]* %colorType, i32 0, i32 0
  %call3 = call i32 @memcmp(i8* %arraydecay2, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.24, i32 0, i32 0), i32 4)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %lor.lhs.false, label %if.then8

lor.lhs.false:                                    ; preds = %do.end
  %arraydecay5 = getelementptr inbounds [4 x i8], [4 x i8]* %colorType, i32 0, i32 0
  %call6 = call i32 @memcmp(i8* %arraydecay5, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.25, i32 0, i32 0), i32 4)
  %tobool7 = icmp ne i32 %call6, 0
  br i1 %tobool7, label %if.else, label %if.then8

if.then8:                                         ; preds = %lor.lhs.false, %do.end
  %9 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %hasICC9 = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %9, i32 0, i32 0
  store i32 1, i32* %hasICC9, align 4, !tbaa !190
  %call10 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %10 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %icc = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %10, i32 0, i32 1
  store i8* %call10, i8** %icc, align 4, !tbaa !193
  %call11 = call i32 @avifROStreamRemainingBytes(%struct.avifROStream* %s)
  %11 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %iccSize = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %11, i32 0, i32 2
  store i32 %call11, i32* %iccSize, align 4, !tbaa !194
  br label %if.end51

if.else:                                          ; preds = %lor.lhs.false
  %arraydecay12 = getelementptr inbounds [4 x i8], [4 x i8]* %colorType, i32 0, i32 0
  %call13 = call i32 @memcmp(i8* %arraydecay12, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.26, i32 0, i32 0), i32 4)
  %tobool14 = icmp ne i32 %call13, 0
  br i1 %tobool14, label %if.end50, label %if.then15

if.then15:                                        ; preds = %if.else
  %12 = bitcast i16* %tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %12) #4
  br label %do.body16

do.body16:                                        ; preds = %if.then15
  %call17 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp16)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %do.body16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup49

if.end20:                                         ; preds = %do.body16
  br label %do.cond21

do.cond21:                                        ; preds = %if.end20
  br label %do.end22

do.end22:                                         ; preds = %do.cond21
  %13 = load i16, i16* %tmp16, align 2, !tbaa !155
  %conv = zext i16 %13 to i32
  %14 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %colorPrimaries = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %14, i32 0, i32 4
  store i32 %conv, i32* %colorPrimaries, align 4, !tbaa !195
  br label %do.body23

do.body23:                                        ; preds = %do.end22
  %call24 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp16)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %do.body23
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup49

if.end27:                                         ; preds = %do.body23
  br label %do.cond28

do.cond28:                                        ; preds = %if.end27
  br label %do.end29

do.end29:                                         ; preds = %do.cond28
  %15 = load i16, i16* %tmp16, align 2, !tbaa !155
  %conv30 = zext i16 %15 to i32
  %16 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %transferCharacteristics = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %16, i32 0, i32 5
  store i32 %conv30, i32* %transferCharacteristics, align 4, !tbaa !196
  br label %do.body31

do.body31:                                        ; preds = %do.end29
  %call32 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %tmp16)
  %tobool33 = icmp ne i32 %call32, 0
  br i1 %tobool33, label %if.end35, label %if.then34

if.then34:                                        ; preds = %do.body31
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup49

if.end35:                                         ; preds = %do.body31
  br label %do.cond36

do.cond36:                                        ; preds = %if.end35
  br label %do.end37

do.end37:                                         ; preds = %do.cond36
  %17 = load i16, i16* %tmp16, align 2, !tbaa !155
  %conv38 = zext i16 %17 to i32
  %18 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %matrixCoefficients = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %18, i32 0, i32 6
  store i32 %conv38, i32* %matrixCoefficients, align 4, !tbaa !197
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %tmp8) #4
  br label %do.body39

do.body39:                                        ; preds = %do.end37
  %call40 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %tmp8, i32 1)
  %tobool41 = icmp ne i32 %call40, 0
  br i1 %tobool41, label %if.end43, label %if.then42

if.then42:                                        ; preds = %do.body39
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end43:                                         ; preds = %do.body39
  br label %do.cond44

do.cond44:                                        ; preds = %if.end43
  br label %do.end45

do.end45:                                         ; preds = %do.cond44
  %19 = load i8, i8* %tmp8, align 1, !tbaa !25
  %conv46 = zext i8 %19 to i32
  %and = and i32 %conv46, 128
  %tobool47 = icmp ne i32 %and, 0
  %20 = zext i1 %tobool47 to i64
  %cond = select i1 %tobool47, i32 1, i32 0
  %21 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %range = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %21, i32 0, i32 7
  store i32 %cond, i32* %range, align 4, !tbaa !198
  %22 = load %struct.avifColourInformationBox*, %struct.avifColourInformationBox** %colr, align 4, !tbaa !2
  %hasNCLX48 = getelementptr inbounds %struct.avifColourInformationBox, %struct.avifColourInformationBox* %22, i32 0, i32 3
  store i32 1, i32* %hasNCLX48, align 4, !tbaa !192
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end45, %if.then42
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %tmp8) #4
  br label %cleanup49

cleanup49:                                        ; preds = %cleanup, %if.then34, %if.then26, %if.then19
  %23 = bitcast i16* %tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %23) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup52 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup49
  br label %if.end50

if.end50:                                         ; preds = %cleanup.cont, %if.else
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.then8
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

cleanup52:                                        ; preds = %if.end51, %cleanup49, %if.then
  %24 = bitcast [4 x i8]* %colorType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast %struct.avifColourInformationBox** %colr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %26) #4
  %27 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %27) #4
  %28 = load i32, i32* %retval, align 4
  ret i32 %28
}

; Function Attrs: nounwind
define internal i32 @avifParseAV1CodecConfigurationBoxProperty(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %1 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %2 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %2, i32 0, i32 1
  %av1C = bitcast %union.anon* %u to %struct.avifCodecConfigurationBox*
  %call = call i32 @avifParseAV1CodecConfigurationBox(i8* %0, i32 %1, %struct.avifCodecConfigurationBox* %av1C)
  ret i32 %call
}

; Function Attrs: nounwind
define internal i32 @avifParsePixelAspectRatioBoxProperty(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %pasp = alloca %struct.avifPixelAspectRatioBox*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  %4 = bitcast %struct.avifPixelAspectRatioBox** %pasp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %5, i32 0, i32 1
  %pasp1 = bitcast %union.anon* %u to %struct.avifPixelAspectRatioBox*
  store %struct.avifPixelAspectRatioBox* %pasp1, %struct.avifPixelAspectRatioBox** %pasp, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %entry
  %6 = load %struct.avifPixelAspectRatioBox*, %struct.avifPixelAspectRatioBox** %pasp, align 4, !tbaa !2
  %hSpacing = getelementptr inbounds %struct.avifPixelAspectRatioBox, %struct.avifPixelAspectRatioBox* %6, i32 0, i32 0
  %call = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %hSpacing)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body2

do.body2:                                         ; preds = %do.end
  %7 = load %struct.avifPixelAspectRatioBox*, %struct.avifPixelAspectRatioBox** %pasp, align 4, !tbaa !2
  %vSpacing = getelementptr inbounds %struct.avifPixelAspectRatioBox, %struct.avifPixelAspectRatioBox* %7, i32 0, i32 1
  %call3 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %vSpacing)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %do.body2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %do.body2
  br label %do.cond7

do.cond7:                                         ; preds = %if.end6
  br label %do.end8

do.end8:                                          ; preds = %do.cond7
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end8, %if.then5, %if.then
  %8 = bitcast %struct.avifPixelAspectRatioBox** %pasp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  %9 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %9) #4
  %10 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #4
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: nounwind
define internal i32 @avifParseCleanApertureBoxProperty(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %clap = alloca %struct.avifCleanApertureBox*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  %4 = bitcast %struct.avifCleanApertureBox** %clap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %5, i32 0, i32 1
  %clap1 = bitcast %union.anon* %u to %struct.avifCleanApertureBox*
  store %struct.avifCleanApertureBox* %clap1, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %entry
  %6 = load %struct.avifCleanApertureBox*, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  %widthN = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %6, i32 0, i32 0
  %call = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %widthN)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %do.body2

do.body2:                                         ; preds = %do.end
  %7 = load %struct.avifCleanApertureBox*, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  %widthD = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %7, i32 0, i32 1
  %call3 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %widthD)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %do.body2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %do.body2
  br label %do.cond7

do.cond7:                                         ; preds = %if.end6
  br label %do.end8

do.end8:                                          ; preds = %do.cond7
  br label %do.body9

do.body9:                                         ; preds = %do.end8
  %8 = load %struct.avifCleanApertureBox*, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  %heightN = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %8, i32 0, i32 2
  %call10 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %heightN)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %do.body9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %do.body9
  br label %do.cond14

do.cond14:                                        ; preds = %if.end13
  br label %do.end15

do.end15:                                         ; preds = %do.cond14
  br label %do.body16

do.body16:                                        ; preds = %do.end15
  %9 = load %struct.avifCleanApertureBox*, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  %heightD = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %9, i32 0, i32 3
  %call17 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %heightD)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %do.body16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %do.body16
  br label %do.cond21

do.cond21:                                        ; preds = %if.end20
  br label %do.end22

do.end22:                                         ; preds = %do.cond21
  br label %do.body23

do.body23:                                        ; preds = %do.end22
  %10 = load %struct.avifCleanApertureBox*, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  %horizOffN = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %10, i32 0, i32 4
  %call24 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %horizOffN)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %do.body23
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %do.body23
  br label %do.cond28

do.cond28:                                        ; preds = %if.end27
  br label %do.end29

do.end29:                                         ; preds = %do.cond28
  br label %do.body30

do.body30:                                        ; preds = %do.end29
  %11 = load %struct.avifCleanApertureBox*, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  %horizOffD = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %11, i32 0, i32 5
  %call31 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %horizOffD)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %if.end34, label %if.then33

if.then33:                                        ; preds = %do.body30
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end34:                                         ; preds = %do.body30
  br label %do.cond35

do.cond35:                                        ; preds = %if.end34
  br label %do.end36

do.end36:                                         ; preds = %do.cond35
  br label %do.body37

do.body37:                                        ; preds = %do.end36
  %12 = load %struct.avifCleanApertureBox*, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  %vertOffN = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %12, i32 0, i32 6
  %call38 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %vertOffN)
  %tobool39 = icmp ne i32 %call38, 0
  br i1 %tobool39, label %if.end41, label %if.then40

if.then40:                                        ; preds = %do.body37
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end41:                                         ; preds = %do.body37
  br label %do.cond42

do.cond42:                                        ; preds = %if.end41
  br label %do.end43

do.end43:                                         ; preds = %do.cond42
  br label %do.body44

do.body44:                                        ; preds = %do.end43
  %13 = load %struct.avifCleanApertureBox*, %struct.avifCleanApertureBox** %clap, align 4, !tbaa !2
  %vertOffD = getelementptr inbounds %struct.avifCleanApertureBox, %struct.avifCleanApertureBox* %13, i32 0, i32 7
  %call45 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %vertOffD)
  %tobool46 = icmp ne i32 %call45, 0
  br i1 %tobool46, label %if.end48, label %if.then47

if.then47:                                        ; preds = %do.body44
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end48:                                         ; preds = %do.body44
  br label %do.cond49

do.cond49:                                        ; preds = %if.end48
  br label %do.end50

do.end50:                                         ; preds = %do.cond49
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end50, %if.then47, %if.then40, %if.then33, %if.then26, %if.then19, %if.then12, %if.then5, %if.then
  %14 = bitcast %struct.avifCleanApertureBox** %clap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  %15 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #4
  %16 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %16) #4
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

; Function Attrs: nounwind
define internal i32 @avifParseImageRotationProperty(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %irot = alloca %struct.avifImageRotation*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  %4 = bitcast %struct.avifImageRotation** %irot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %5, i32 0, i32 1
  %irot1 = bitcast %union.anon* %u to %struct.avifImageRotation*
  store %struct.avifImageRotation* %irot1, %struct.avifImageRotation** %irot, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %entry
  %6 = load %struct.avifImageRotation*, %struct.avifImageRotation** %irot, align 4, !tbaa !2
  %angle = getelementptr inbounds %struct.avifImageRotation, %struct.avifImageRotation* %6, i32 0, i32 0
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %angle, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %7 = load %struct.avifImageRotation*, %struct.avifImageRotation** %irot, align 4, !tbaa !2
  %angle2 = getelementptr inbounds %struct.avifImageRotation, %struct.avifImageRotation* %7, i32 0, i32 0
  %8 = load i8, i8* %angle2, align 1, !tbaa !199
  %conv = zext i8 %8 to i32
  %and = and i32 %conv, 252
  %cmp = icmp ne i32 %and, 0
  br i1 %cmp, label %if.then4, label %if.end5

if.then4:                                         ; preds = %do.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %do.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end5, %if.then4, %if.then
  %9 = bitcast %struct.avifImageRotation** %irot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %10 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #4
  %11 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define internal i32 @avifParseImageMirrorProperty(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %imir = alloca %struct.avifImageMirror*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  %4 = bitcast %struct.avifImageMirror** %imir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %5, i32 0, i32 1
  %imir1 = bitcast %union.anon* %u to %struct.avifImageMirror*
  store %struct.avifImageMirror* %imir1, %struct.avifImageMirror** %imir, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %entry
  %6 = load %struct.avifImageMirror*, %struct.avifImageMirror** %imir, align 4, !tbaa !2
  %axis = getelementptr inbounds %struct.avifImageMirror, %struct.avifImageMirror* %6, i32 0, i32 0
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %axis, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %7 = load %struct.avifImageMirror*, %struct.avifImageMirror** %imir, align 4, !tbaa !2
  %axis2 = getelementptr inbounds %struct.avifImageMirror, %struct.avifImageMirror* %7, i32 0, i32 0
  %8 = load i8, i8* %axis2, align 1, !tbaa !200
  %conv = zext i8 %8 to i32
  %and = and i32 %conv, 254
  %cmp = icmp ne i32 %and, 0
  br i1 %cmp, label %if.then4, label %if.end5

if.then4:                                         ; preds = %do.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %do.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end5, %if.then4, %if.then
  %9 = bitcast %struct.avifImageMirror** %imir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %10 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #4
  %11 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define internal i32 @avifParsePixelInformationProperty(%struct.avifProperty* %prop, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %prop.addr = alloca %struct.avifProperty*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %pixi = alloca %struct.avifPixelInformationProperty*, align 4
  %i = alloca i8, align 1
  store %struct.avifProperty* %prop, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup26

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast %struct.avifPixelInformationProperty** %pixi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifProperty*, %struct.avifProperty** %prop.addr, align 4, !tbaa !2
  %u = getelementptr inbounds %struct.avifProperty, %struct.avifProperty* %5, i32 0, i32 1
  %pixi1 = bitcast %union.anon* %u to %struct.avifPixelInformationProperty*
  store %struct.avifPixelInformationProperty* %pixi1, %struct.avifPixelInformationProperty** %pixi, align 4, !tbaa !2
  br label %do.body2

do.body2:                                         ; preds = %do.end
  %6 = load %struct.avifPixelInformationProperty*, %struct.avifPixelInformationProperty** %pixi, align 4, !tbaa !2
  %planeCount = getelementptr inbounds %struct.avifPixelInformationProperty, %struct.avifPixelInformationProperty* %6, i32 0, i32 1
  %call3 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %planeCount, i32 1)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %do.body2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

if.end6:                                          ; preds = %do.body2
  br label %do.cond7

do.cond7:                                         ; preds = %if.end6
  br label %do.end8

do.end8:                                          ; preds = %do.cond7
  %7 = load %struct.avifPixelInformationProperty*, %struct.avifPixelInformationProperty** %pixi, align 4, !tbaa !2
  %planeCount9 = getelementptr inbounds %struct.avifPixelInformationProperty, %struct.avifPixelInformationProperty* %7, i32 0, i32 1
  %8 = load i8, i8* %planeCount9, align 1, !tbaa !201
  %conv = zext i8 %8 to i32
  %cmp = icmp sgt i32 %conv, 4
  br i1 %cmp, label %if.then11, label %if.end12

if.then11:                                        ; preds = %do.end8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

if.end12:                                         ; preds = %do.end8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %i) #4
  store i8 0, i8* %i, align 1, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end12
  %9 = load i8, i8* %i, align 1, !tbaa !25
  %conv13 = zext i8 %9 to i32
  %10 = load %struct.avifPixelInformationProperty*, %struct.avifPixelInformationProperty** %pixi, align 4, !tbaa !2
  %planeCount14 = getelementptr inbounds %struct.avifPixelInformationProperty, %struct.avifPixelInformationProperty* %10, i32 0, i32 1
  %11 = load i8, i8* %planeCount14, align 1, !tbaa !201
  %conv15 = zext i8 %11 to i32
  %cmp16 = icmp slt i32 %conv13, %conv15
  br i1 %cmp16, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  br label %do.body18

do.body18:                                        ; preds = %for.body
  %12 = load %struct.avifPixelInformationProperty*, %struct.avifPixelInformationProperty** %pixi, align 4, !tbaa !2
  %planeDepths = getelementptr inbounds %struct.avifPixelInformationProperty, %struct.avifPixelInformationProperty* %12, i32 0, i32 0
  %13 = load i8, i8* %i, align 1, !tbaa !25
  %idxprom = zext i8 %13 to i32
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %planeDepths, i32 0, i32 %idxprom
  %call19 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %arrayidx, i32 1)
  %tobool20 = icmp ne i32 %call19, 0
  br i1 %tobool20, label %if.end22, label %if.then21

if.then21:                                        ; preds = %do.body18
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %do.body18
  br label %do.cond23

do.cond23:                                        ; preds = %if.end22
  br label %do.end24

do.end24:                                         ; preds = %do.cond23
  br label %for.inc

for.inc:                                          ; preds = %do.end24
  %14 = load i8, i8* %i, align 1, !tbaa !25
  %inc = add i8 %14, 1
  store i8 %inc, i8* %i, align 1, !tbaa !25
  br label %for.cond

cleanup:                                          ; preds = %if.then21, %for.cond.cleanup
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %i) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup25 [
    i32 6, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

cleanup25:                                        ; preds = %for.end, %cleanup, %if.then11, %if.then5
  %15 = bitcast %struct.avifPixelInformationProperty** %pixi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  br label %cleanup26

cleanup26:                                        ; preds = %cleanup25, %if.then
  %16 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %16) #4
  %17 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %17) #4
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

declare i32 @avifROStreamReadString(%struct.avifROStream*, i8*, i32) #2

; Function Attrs: nounwind
define internal i32 @avifParseAV1CodecConfigurationBox(i8* %raw, i32 %rawLen, %struct.avifCodecConfigurationBox* %av1C) #0 {
entry:
  %retval = alloca i32, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %av1C.addr = alloca %struct.avifCodecConfigurationBox*, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %markerAndVersion = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %seqProfileAndIndex = alloca i8, align 1
  %rawFlags = alloca i8, align 1
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  store %struct.avifCodecConfigurationBox* %av1C, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %markerAndVersion) #4
  store i8 0, i8* %markerAndVersion, align 1, !tbaa !25
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %markerAndVersion, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %seqProfileAndIndex) #4
  store i8 0, i8* %seqProfileAndIndex, align 1, !tbaa !25
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %seqProfileAndIndex, i32 1)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %rawFlags) #4
  store i8 0, i8* %rawFlags, align 1, !tbaa !25
  br label %do.body8

do.body8:                                         ; preds = %do.end7
  %call9 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %rawFlags, i32 1)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %do.body8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %do.body8
  br label %do.cond13

do.cond13:                                        ; preds = %if.end12
  br label %do.end14

do.end14:                                         ; preds = %do.cond13
  %4 = load i8, i8* %markerAndVersion, align 1, !tbaa !25
  %conv = zext i8 %4 to i32
  %cmp = icmp ne i32 %conv, 129
  br i1 %cmp, label %if.then16, label %if.end17

if.then16:                                        ; preds = %do.end14
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %do.end14
  %5 = load i8, i8* %seqProfileAndIndex, align 1, !tbaa !25
  %conv18 = zext i8 %5 to i32
  %shr = ashr i32 %conv18, 5
  %and = and i32 %shr, 7
  %conv19 = trunc i32 %and to i8
  %6 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %seqProfile = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %6, i32 0, i32 0
  store i8 %conv19, i8* %seqProfile, align 1, !tbaa !203
  %7 = load i8, i8* %seqProfileAndIndex, align 1, !tbaa !25
  %conv20 = zext i8 %7 to i32
  %shr21 = ashr i32 %conv20, 0
  %and22 = and i32 %shr21, 31
  %conv23 = trunc i32 %and22 to i8
  %8 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %seqLevelIdx0 = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %8, i32 0, i32 1
  store i8 %conv23, i8* %seqLevelIdx0, align 1, !tbaa !204
  %9 = load i8, i8* %rawFlags, align 1, !tbaa !25
  %conv24 = zext i8 %9 to i32
  %shr25 = ashr i32 %conv24, 7
  %and26 = and i32 %shr25, 1
  %conv27 = trunc i32 %and26 to i8
  %10 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %seqTier0 = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %10, i32 0, i32 2
  store i8 %conv27, i8* %seqTier0, align 1, !tbaa !205
  %11 = load i8, i8* %rawFlags, align 1, !tbaa !25
  %conv28 = zext i8 %11 to i32
  %shr29 = ashr i32 %conv28, 6
  %and30 = and i32 %shr29, 1
  %conv31 = trunc i32 %and30 to i8
  %12 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %highBitdepth = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %12, i32 0, i32 3
  store i8 %conv31, i8* %highBitdepth, align 1, !tbaa !164
  %13 = load i8, i8* %rawFlags, align 1, !tbaa !25
  %conv32 = zext i8 %13 to i32
  %shr33 = ashr i32 %conv32, 5
  %and34 = and i32 %shr33, 1
  %conv35 = trunc i32 %and34 to i8
  %14 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %twelveBit = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %14, i32 0, i32 4
  store i8 %conv35, i8* %twelveBit, align 1, !tbaa !162
  %15 = load i8, i8* %rawFlags, align 1, !tbaa !25
  %conv36 = zext i8 %15 to i32
  %shr37 = ashr i32 %conv36, 4
  %and38 = and i32 %shr37, 1
  %conv39 = trunc i32 %and38 to i8
  %16 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %monochrome = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %16, i32 0, i32 5
  store i8 %conv39, i8* %monochrome, align 1, !tbaa !206
  %17 = load i8, i8* %rawFlags, align 1, !tbaa !25
  %conv40 = zext i8 %17 to i32
  %shr41 = ashr i32 %conv40, 3
  %and42 = and i32 %shr41, 1
  %conv43 = trunc i32 %and42 to i8
  %18 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %chromaSubsamplingX = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %18, i32 0, i32 6
  store i8 %conv43, i8* %chromaSubsamplingX, align 1, !tbaa !207
  %19 = load i8, i8* %rawFlags, align 1, !tbaa !25
  %conv44 = zext i8 %19 to i32
  %shr45 = ashr i32 %conv44, 2
  %and46 = and i32 %shr45, 1
  %conv47 = trunc i32 %and46 to i8
  %20 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %chromaSubsamplingY = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %20, i32 0, i32 7
  store i8 %conv47, i8* %chromaSubsamplingY, align 1, !tbaa !208
  %21 = load i8, i8* %rawFlags, align 1, !tbaa !25
  %conv48 = zext i8 %21 to i32
  %shr49 = ashr i32 %conv48, 0
  %and50 = and i32 %shr49, 3
  %conv51 = trunc i32 %and50 to i8
  %22 = load %struct.avifCodecConfigurationBox*, %struct.avifCodecConfigurationBox** %av1C.addr, align 4, !tbaa !2
  %chromaSamplePosition = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %22, i32 0, i32 8
  store i8 %conv51, i8* %chromaSamplePosition, align 1, !tbaa !209
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end17, %if.then16, %if.then11
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %rawFlags) #4
  br label %cleanup52

cleanup52:                                        ; preds = %cleanup, %if.then4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %seqProfileAndIndex) #4
  br label %cleanup53

cleanup53:                                        ; preds = %cleanup52, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %markerAndVersion) #4
  %23 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #4
  %24 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %24) #4
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

; Function Attrs: nounwind
define internal i32 @avifParseItemInfoEntry(%struct.avifMeta* %meta, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %meta.addr = alloca %struct.avifMeta*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %itemID = alloca i16, align 2
  %itemProtectionIndex = alloca i16, align 2
  %itemType = alloca [4 x i8], align 1
  %contentType = alloca %struct.avifContentType, align 1
  %item = alloca %struct.avifDecoderItem*, align 4
  store %struct.avifMeta* %meta, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 2)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup54

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i16* %itemID to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #4
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %itemID)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %5 = bitcast i16* %itemProtectionIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %5) #4
  br label %do.body8

do.body8:                                         ; preds = %do.end7
  %call9 = call i32 @avifROStreamReadU16(%struct.avifROStream* %s, i16* %itemProtectionIndex)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %do.body8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup52

if.end12:                                         ; preds = %do.body8
  br label %do.cond13

do.cond13:                                        ; preds = %if.end12
  br label %do.end14

do.end14:                                         ; preds = %do.cond13
  %6 = bitcast [4 x i8]* %itemType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  br label %do.body15

do.body15:                                        ; preds = %do.end14
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %itemType, i32 0, i32 0
  %call16 = call i32 @avifROStreamRead(%struct.avifROStream* %s, i8* %arraydecay, i32 4)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %do.body15
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup51

if.end19:                                         ; preds = %do.body15
  br label %do.cond20

do.cond20:                                        ; preds = %if.end19
  br label %do.end21

do.end21:                                         ; preds = %do.cond20
  %7 = bitcast %struct.avifContentType* %contentType to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %7) #4
  %arraydecay22 = getelementptr inbounds [4 x i8], [4 x i8]* %itemType, i32 0, i32 0
  %call23 = call i32 @memcmp(i8* %arraydecay22, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.28, i32 0, i32 0), i32 4)
  %tobool24 = icmp ne i32 %call23, 0
  br i1 %tobool24, label %if.else, label %if.then25

if.then25:                                        ; preds = %do.end21
  br label %do.body26

do.body26:                                        ; preds = %if.then25
  %call27 = call i32 @avifROStreamReadString(%struct.avifROStream* %s, i8* null, i32 0)
  %tobool28 = icmp ne i32 %call27, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %do.body26
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup50

if.end30:                                         ; preds = %do.body26
  br label %do.cond31

do.cond31:                                        ; preds = %if.end30
  br label %do.end32

do.end32:                                         ; preds = %do.cond31
  br label %do.body33

do.body33:                                        ; preds = %do.end32
  %contentType34 = getelementptr inbounds %struct.avifContentType, %struct.avifContentType* %contentType, i32 0, i32 0
  %arraydecay35 = getelementptr inbounds [64 x i8], [64 x i8]* %contentType34, i32 0, i32 0
  %call36 = call i32 @avifROStreamReadString(%struct.avifROStream* %s, i8* %arraydecay35, i32 64)
  %tobool37 = icmp ne i32 %call36, 0
  br i1 %tobool37, label %if.end39, label %if.then38

if.then38:                                        ; preds = %do.body33
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup50

if.end39:                                         ; preds = %do.body33
  br label %do.cond40

do.cond40:                                        ; preds = %if.end39
  br label %do.end41

do.end41:                                         ; preds = %do.cond40
  br label %if.end42

if.else:                                          ; preds = %do.end21
  %8 = bitcast %struct.avifContentType* %contentType to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %8, i8 0, i32 64, i1 false)
  br label %if.end42

if.end42:                                         ; preds = %if.else, %do.end41
  %9 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.avifMeta*, %struct.avifMeta** %meta.addr, align 4, !tbaa !2
  %11 = load i16, i16* %itemID, align 2, !tbaa !155
  %conv = zext i16 %11 to i32
  %call43 = call %struct.avifDecoderItem* @avifMetaFindItem(%struct.avifMeta* %10, i32 %conv)
  store %struct.avifDecoderItem* %call43, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %12 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %tobool44 = icmp ne %struct.avifDecoderItem* %12, null
  br i1 %tobool44, label %if.end46, label %if.then45

if.then45:                                        ; preds = %if.end42
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %if.end42
  %13 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %type = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %13, i32 0, i32 2
  %arraydecay47 = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %arraydecay48 = getelementptr inbounds [4 x i8], [4 x i8]* %itemType, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %arraydecay47, i8* align 1 %arraydecay48, i32 4, i1 false)
  %14 = load %struct.avifDecoderItem*, %struct.avifDecoderItem** %item, align 4, !tbaa !2
  %contentType49 = getelementptr inbounds %struct.avifDecoderItem, %struct.avifDecoderItem* %14, i32 0, i32 6
  %15 = bitcast %struct.avifContentType* %contentType49 to i8*
  %16 = bitcast %struct.avifContentType* %contentType to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 1 %16, i32 64, i1 false)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end46, %if.then45
  %17 = bitcast %struct.avifDecoderItem** %item to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %cleanup50

cleanup50:                                        ; preds = %cleanup, %if.then38, %if.then29
  %18 = bitcast %struct.avifContentType* %contentType to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %18) #4
  br label %cleanup51

cleanup51:                                        ; preds = %cleanup50, %if.then18
  %19 = bitcast [4 x i8]* %itemType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  br label %cleanup52

cleanup52:                                        ; preds = %cleanup51, %if.then11
  %20 = bitcast i16* %itemProtectionIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %20) #4
  br label %cleanup53

cleanup53:                                        ; preds = %cleanup52, %if.then4
  %21 = bitcast i16* %itemID to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %21) #4
  br label %cleanup54

cleanup54:                                        ; preds = %cleanup53, %if.then
  %22 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #4
  %23 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #4
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: nounwind
define internal i32 @avifParseTrackBox(%struct.avifDecoderData* %data, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %track = alloca %struct.avifTrack*, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data1, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  %4 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %call = call %struct.avifTrack* @avifDecoderDataCreateTrack(%struct.avifDecoderData* %5)
  store %struct.avifTrack* %call, %struct.avifTrack** %track, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %call2 = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #4
  br label %do.body

do.body:                                          ; preds = %while.body
  %call3 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call5 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.34, i32 0, i32 0), i32 4)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.else, label %if.then7

if.then7:                                         ; preds = %do.end
  br label %do.body8

do.body8:                                         ; preds = %if.then7
  %7 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %call9 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size10 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %8 = load i32, i32* %size10, align 4, !tbaa !10
  %call11 = call i32 @avifParseTrackHeaderBox(%struct.avifTrack* %7, i8* %call9, i32 %8)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %do.body8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %do.body8
  br label %do.cond15

do.cond15:                                        ; preds = %if.end14
  br label %do.end16

do.end16:                                         ; preds = %do.cond15
  br label %if.end64

if.else:                                          ; preds = %do.end
  %type17 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay18 = getelementptr inbounds [4 x i8], [4 x i8]* %type17, i32 0, i32 0
  %call19 = call i32 @memcmp(i8* %arraydecay18, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.13, i32 0, i32 0), i32 4)
  %tobool20 = icmp ne i32 %call19, 0
  br i1 %tobool20, label %if.else31, label %if.then21

if.then21:                                        ; preds = %if.else
  br label %do.body22

do.body22:                                        ; preds = %if.then21
  %9 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %meta = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %9, i32 0, i32 7
  %10 = load %struct.avifMeta*, %struct.avifMeta** %meta, align 4, !tbaa !48
  %call23 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size24 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %11 = load i32, i32* %size24, align 4, !tbaa !10
  %call25 = call i32 @avifParseMetaBox(%struct.avifMeta* %10, i8* %call23, i32 %11)
  %tobool26 = icmp ne i32 %call25, 0
  br i1 %tobool26, label %if.end28, label %if.then27

if.then27:                                        ; preds = %do.body22
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end28:                                         ; preds = %do.body22
  br label %do.cond29

do.cond29:                                        ; preds = %if.end28
  br label %do.end30

do.end30:                                         ; preds = %do.cond29
  br label %if.end63

if.else31:                                        ; preds = %if.else
  %type32 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay33 = getelementptr inbounds [4 x i8], [4 x i8]* %type32, i32 0, i32 0
  %call34 = call i32 @memcmp(i8* %arraydecay33, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.35, i32 0, i32 0), i32 4)
  %tobool35 = icmp ne i32 %call34, 0
  br i1 %tobool35, label %if.else46, label %if.then36

if.then36:                                        ; preds = %if.else31
  br label %do.body37

do.body37:                                        ; preds = %if.then36
  %12 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %call38 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size39 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %13 = load i32, i32* %size39, align 4, !tbaa !10
  %call40 = call i32 @avifParseMediaBox(%struct.avifTrack* %12, i8* %call38, i32 %13)
  %tobool41 = icmp ne i32 %call40, 0
  br i1 %tobool41, label %if.end43, label %if.then42

if.then42:                                        ; preds = %do.body37
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end43:                                         ; preds = %do.body37
  br label %do.cond44

do.cond44:                                        ; preds = %if.end43
  br label %do.end45

do.end45:                                         ; preds = %do.cond44
  br label %if.end62

if.else46:                                        ; preds = %if.else31
  %type47 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay48 = getelementptr inbounds [4 x i8], [4 x i8]* %type47, i32 0, i32 0
  %call49 = call i32 @memcmp(i8* %arraydecay48, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.36, i32 0, i32 0), i32 4)
  %tobool50 = icmp ne i32 %call49, 0
  br i1 %tobool50, label %if.end61, label %if.then51

if.then51:                                        ; preds = %if.else46
  br label %do.body52

do.body52:                                        ; preds = %if.then51
  %14 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %call53 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size54 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %15 = load i32, i32* %size54, align 4, !tbaa !10
  %call55 = call i32 @avifTrackReferenceBox(%struct.avifTrack* %14, i8* %call53, i32 %15)
  %tobool56 = icmp ne i32 %call55, 0
  br i1 %tobool56, label %if.end58, label %if.then57

if.then57:                                        ; preds = %do.body52
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end58:                                         ; preds = %do.body52
  br label %do.cond59

do.cond59:                                        ; preds = %if.end58
  br label %do.end60

do.end60:                                         ; preds = %do.cond59
  br label %if.end61

if.end61:                                         ; preds = %do.end60, %if.else46
  br label %if.end62

if.end62:                                         ; preds = %if.end61, %do.end45
  br label %if.end63

if.end63:                                         ; preds = %if.end62, %do.end30
  br label %if.end64

if.end64:                                         ; preds = %if.end63, %do.end16
  br label %do.body65

do.body65:                                        ; preds = %if.end64
  %size66 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %16 = load i32, i32* %size66, align 4, !tbaa !10
  %call67 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %16)
  %tobool68 = icmp ne i32 %call67, 0
  br i1 %tobool68, label %if.end70, label %if.then69

if.then69:                                        ; preds = %do.body65
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end70:                                         ; preds = %do.body65
  br label %do.cond71

do.cond71:                                        ; preds = %if.end70
  br label %do.end72

do.end72:                                         ; preds = %do.cond71
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end72, %if.then69, %if.then57, %if.then42, %if.then27, %if.then13, %if.then
  %17 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %17) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup73 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup73

cleanup73:                                        ; preds = %while.end, %cleanup
  %18 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %19 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %19) #4
  %20 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %20) #4
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define internal %struct.avifTrack* @avifDecoderDataCreateTrack(%struct.avifDecoderData* %data) #0 {
entry:
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %track = alloca %struct.avifTrack*, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tracks = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %1, i32 0, i32 2
  %2 = bitcast %struct.avifTrackArray* %tracks to i8*
  %call = call i8* @avifArrayPushPtr(i8* %2)
  %3 = bitcast i8* %call to %struct.avifTrack*
  store %struct.avifTrack* %3, %struct.avifTrack** %track, align 4, !tbaa !2
  %call1 = call %struct.avifMeta* @avifMetaCreate()
  %4 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %meta = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %4, i32 0, i32 7
  store %struct.avifMeta* %call1, %struct.avifMeta** %meta, align 4, !tbaa !48
  %5 = load %struct.avifTrack*, %struct.avifTrack** %track, align 4, !tbaa !2
  %6 = bitcast %struct.avifTrack** %track to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  ret %struct.avifTrack* %5
}

; Function Attrs: nounwind
define internal i32 @avifParseTrackHeaderBox(%struct.avifTrack* %track, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %track.addr = alloca %struct.avifTrack*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %version = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %ignored32 = alloca i32, align 4
  %trackID = alloca i32, align 4
  %ignored64 = alloca i64, align 8
  %width = alloca i32, align 4
  %height = alloca i32, align 4
  store %struct.avifTrack* %track, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %s, i8* %version, i32* null)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup108

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %ignored32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %trackID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i64* %ignored64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #4
  %7 = load i8, i8* %version, align 1, !tbaa !25
  %conv = zext i8 %7 to i32
  %cmp = icmp eq i32 %conv, 1
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %do.end
  br label %do.body3

do.body3:                                         ; preds = %if.then2
  %call4 = call i32 @avifROStreamReadU64(%struct.avifROStream* %s, i64* %ignored64)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %do.body3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end7:                                          ; preds = %do.body3
  br label %do.cond8

do.cond8:                                         ; preds = %if.end7
  br label %do.end9

do.end9:                                          ; preds = %do.cond8
  br label %do.body10

do.body10:                                        ; preds = %do.end9
  %call11 = call i32 @avifROStreamReadU64(%struct.avifROStream* %s, i64* %ignored64)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %do.body10
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end14:                                         ; preds = %do.body10
  br label %do.cond15

do.cond15:                                        ; preds = %if.end14
  br label %do.end16

do.end16:                                         ; preds = %do.cond15
  br label %do.body17

do.body17:                                        ; preds = %do.end16
  %call18 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %trackID)
  %tobool19 = icmp ne i32 %call18, 0
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %do.body17
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end21:                                         ; preds = %do.body17
  br label %do.cond22

do.cond22:                                        ; preds = %if.end21
  br label %do.end23

do.end23:                                         ; preds = %do.cond22
  br label %do.body24

do.body24:                                        ; preds = %do.end23
  %call25 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %ignored32)
  %tobool26 = icmp ne i32 %call25, 0
  br i1 %tobool26, label %if.end28, label %if.then27

if.then27:                                        ; preds = %do.body24
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end28:                                         ; preds = %do.body24
  br label %do.cond29

do.cond29:                                        ; preds = %if.end28
  br label %do.end30

do.end30:                                         ; preds = %do.cond29
  br label %do.body31

do.body31:                                        ; preds = %do.end30
  %call32 = call i32 @avifROStreamReadU64(%struct.avifROStream* %s, i64* %ignored64)
  %tobool33 = icmp ne i32 %call32, 0
  br i1 %tobool33, label %if.end35, label %if.then34

if.then34:                                        ; preds = %do.body31
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end35:                                         ; preds = %do.body31
  br label %do.cond36

do.cond36:                                        ; preds = %if.end35
  br label %do.end37

do.end37:                                         ; preds = %do.cond36
  br label %if.end79

if.else:                                          ; preds = %do.end
  %8 = load i8, i8* %version, align 1, !tbaa !25
  %conv38 = zext i8 %8 to i32
  %cmp39 = icmp eq i32 %conv38, 0
  br i1 %cmp39, label %if.then41, label %if.else77

if.then41:                                        ; preds = %if.else
  br label %do.body42

do.body42:                                        ; preds = %if.then41
  %call43 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %ignored32)
  %tobool44 = icmp ne i32 %call43, 0
  br i1 %tobool44, label %if.end46, label %if.then45

if.then45:                                        ; preds = %do.body42
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end46:                                         ; preds = %do.body42
  br label %do.cond47

do.cond47:                                        ; preds = %if.end46
  br label %do.end48

do.end48:                                         ; preds = %do.cond47
  br label %do.body49

do.body49:                                        ; preds = %do.end48
  %call50 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %ignored32)
  %tobool51 = icmp ne i32 %call50, 0
  br i1 %tobool51, label %if.end53, label %if.then52

if.then52:                                        ; preds = %do.body49
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end53:                                         ; preds = %do.body49
  br label %do.cond54

do.cond54:                                        ; preds = %if.end53
  br label %do.end55

do.end55:                                         ; preds = %do.cond54
  br label %do.body56

do.body56:                                        ; preds = %do.end55
  %call57 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %trackID)
  %tobool58 = icmp ne i32 %call57, 0
  br i1 %tobool58, label %if.end60, label %if.then59

if.then59:                                        ; preds = %do.body56
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end60:                                         ; preds = %do.body56
  br label %do.cond61

do.cond61:                                        ; preds = %if.end60
  br label %do.end62

do.end62:                                         ; preds = %do.cond61
  br label %do.body63

do.body63:                                        ; preds = %do.end62
  %call64 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %ignored32)
  %tobool65 = icmp ne i32 %call64, 0
  br i1 %tobool65, label %if.end67, label %if.then66

if.then66:                                        ; preds = %do.body63
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end67:                                         ; preds = %do.body63
  br label %do.cond68

do.cond68:                                        ; preds = %if.end67
  br label %do.end69

do.end69:                                         ; preds = %do.cond68
  br label %do.body70

do.body70:                                        ; preds = %do.end69
  %call71 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %ignored32)
  %tobool72 = icmp ne i32 %call71, 0
  br i1 %tobool72, label %if.end74, label %if.then73

if.then73:                                        ; preds = %do.body70
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end74:                                         ; preds = %do.body70
  br label %do.cond75

do.cond75:                                        ; preds = %if.end74
  br label %do.end76

do.end76:                                         ; preds = %do.cond75
  br label %if.end78

if.else77:                                        ; preds = %if.else
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end78:                                         ; preds = %do.end76
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %do.end37
  br label %do.body80

do.body80:                                        ; preds = %if.end79
  %call81 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 52)
  %tobool82 = icmp ne i32 %call81, 0
  br i1 %tobool82, label %if.end84, label %if.then83

if.then83:                                        ; preds = %do.body80
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup105

if.end84:                                         ; preds = %do.body80
  br label %do.cond85

do.cond85:                                        ; preds = %if.end84
  br label %do.end86

do.end86:                                         ; preds = %do.cond85
  %9 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  br label %do.body87

do.body87:                                        ; preds = %do.end86
  %call88 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %width)
  %tobool89 = icmp ne i32 %call88, 0
  br i1 %tobool89, label %if.end91, label %if.then90

if.then90:                                        ; preds = %do.body87
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end91:                                         ; preds = %do.body87
  br label %do.cond92

do.cond92:                                        ; preds = %if.end91
  br label %do.end93

do.end93:                                         ; preds = %do.cond92
  br label %do.body94

do.body94:                                        ; preds = %do.end93
  %call95 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %height)
  %tobool96 = icmp ne i32 %call95, 0
  br i1 %tobool96, label %if.end98, label %if.then97

if.then97:                                        ; preds = %do.body94
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end98:                                         ; preds = %do.body94
  br label %do.cond99

do.cond99:                                        ; preds = %if.end98
  br label %do.end100

do.end100:                                        ; preds = %do.cond99
  %11 = load i32, i32* %width, align 4, !tbaa !12
  %shr = lshr i32 %11, 16
  %12 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %width101 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %12, i32 0, i32 4
  store i32 %shr, i32* %width101, align 8, !tbaa !64
  %13 = load i32, i32* %height, align 4, !tbaa !12
  %shr102 = lshr i32 %13, 16
  %14 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %height103 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %14, i32 0, i32 5
  store i32 %shr102, i32* %height103, align 4, !tbaa !72
  %15 = load i32, i32* %trackID, align 4, !tbaa !12
  %16 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %id = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %16, i32 0, i32 0
  store i32 %15, i32* %id, align 8, !tbaa !38
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end100, %if.then97, %if.then90
  %17 = bitcast i32* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  %18 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  br label %cleanup105

cleanup105:                                       ; preds = %cleanup, %if.then83, %if.else77, %if.then73, %if.then66, %if.then59, %if.then52, %if.then45, %if.then34, %if.then27, %if.then20, %if.then13, %if.then6
  %19 = bitcast i64* %ignored64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %19) #4
  %20 = bitcast i32* %trackID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = bitcast i32* %ignored32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  br label %cleanup108

cleanup108:                                       ; preds = %cleanup105, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %22 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #4
  %23 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #4
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: nounwind
define internal i32 @avifParseMediaBox(%struct.avifTrack* %track, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %track.addr = alloca %struct.avifTrack*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifTrack* %track, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %call = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  br label %do.body

do.body:                                          ; preds = %while.body
  %call1 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call3 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.37, i32 0, i32 0), i32 4)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.else, label %if.then5

if.then5:                                         ; preds = %do.end
  br label %do.body6

do.body6:                                         ; preds = %if.then5
  %5 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %call7 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size8 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %6 = load i32, i32* %size8, align 4, !tbaa !10
  %call9 = call i32 @avifParseMediaHeaderBox(%struct.avifTrack* %5, i8* %call7, i32 %6)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %do.body6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %do.body6
  br label %do.cond13

do.cond13:                                        ; preds = %if.end12
  br label %do.end14

do.end14:                                         ; preds = %do.cond13
  br label %if.end30

if.else:                                          ; preds = %do.end
  %type15 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay16 = getelementptr inbounds [4 x i8], [4 x i8]* %type15, i32 0, i32 0
  %call17 = call i32 @memcmp(i8* %arraydecay16, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.38, i32 0, i32 0), i32 4)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.end29, label %if.then19

if.then19:                                        ; preds = %if.else
  br label %do.body20

do.body20:                                        ; preds = %if.then19
  %7 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %call21 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size22 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %8 = load i32, i32* %size22, align 4, !tbaa !10
  %call23 = call i32 @avifParseMediaInformationBox(%struct.avifTrack* %7, i8* %call21, i32 %8)
  %tobool24 = icmp ne i32 %call23, 0
  br i1 %tobool24, label %if.end26, label %if.then25

if.then25:                                        ; preds = %do.body20
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end26:                                         ; preds = %do.body20
  br label %do.cond27

do.cond27:                                        ; preds = %if.end26
  br label %do.end28

do.end28:                                         ; preds = %do.cond27
  br label %if.end29

if.end29:                                         ; preds = %do.end28, %if.else
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %do.end14
  br label %do.body31

do.body31:                                        ; preds = %if.end30
  %size32 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %9 = load i32, i32* %size32, align 4, !tbaa !10
  %call33 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %9)
  %tobool34 = icmp ne i32 %call33, 0
  br i1 %tobool34, label %if.end36, label %if.then35

if.then35:                                        ; preds = %do.body31
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end36:                                         ; preds = %do.body31
  br label %do.cond37

do.cond37:                                        ; preds = %if.end36
  br label %do.end38

do.end38:                                         ; preds = %do.cond37
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end38, %if.then35, %if.then25, %if.then11, %if.then
  %10 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup39 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup39

cleanup39:                                        ; preds = %while.end, %cleanup
  %11 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  %12 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #4
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

; Function Attrs: nounwind
define internal i32 @avifTrackReferenceBox(%struct.avifTrack* %track, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %track.addr = alloca %struct.avifTrack*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %toID = alloca i32, align 4
  store %struct.avifTrack* %track, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont32, %entry
  %call = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  br label %do.body

do.body:                                          ; preds = %while.body
  %call1 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call3 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.30, i32 0, i32 0), i32 4)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.else, label %if.then5

if.then5:                                         ; preds = %do.end
  %5 = bitcast i32* %toID to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  br label %do.body6

do.body6:                                         ; preds = %if.then5
  %call7 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %toID)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %do.body6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %do.body6
  br label %do.cond11

do.cond11:                                        ; preds = %if.end10
  br label %do.end12

do.end12:                                         ; preds = %do.cond11
  br label %do.body13

do.body13:                                        ; preds = %do.end12
  %size14 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %6 = load i32, i32* %size14, align 4, !tbaa !10
  %sub = sub i32 %6, 4
  %call15 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %sub)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %do.body13
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %do.body13
  br label %do.cond19

do.cond19:                                        ; preds = %if.end18
  br label %do.end20

do.end20:                                         ; preds = %do.cond19
  %7 = load i32, i32* %toID, align 4, !tbaa !12
  %8 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %auxForID = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %8, i32 0, i32 1
  store i32 %7, i32* %auxForID, align 4, !tbaa !47
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end20, %if.then17, %if.then9
  %9 = bitcast i32* %toID to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup30 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end29

if.else:                                          ; preds = %do.end
  br label %do.body21

do.body21:                                        ; preds = %if.else
  %size22 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %10 = load i32, i32* %size22, align 4, !tbaa !10
  %call23 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %10)
  %tobool24 = icmp ne i32 %call23, 0
  br i1 %tobool24, label %if.end26, label %if.then25

if.then25:                                        ; preds = %do.body21
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

if.end26:                                         ; preds = %do.body21
  br label %do.cond27

do.cond27:                                        ; preds = %if.end26
  br label %do.end28

do.end28:                                         ; preds = %do.cond27
  br label %if.end29

if.end29:                                         ; preds = %do.end28, %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

cleanup30:                                        ; preds = %if.end29, %if.then25, %cleanup, %if.then
  %11 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %11) #4
  %cleanup.dest31 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest31, label %cleanup33 [
    i32 0, label %cleanup.cont32
  ]

cleanup.cont32:                                   ; preds = %cleanup30
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

cleanup33:                                        ; preds = %while.end, %cleanup30
  %12 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #4
  %13 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %13) #4
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

declare i32 @avifROStreamReadU64(%struct.avifROStream*, i64*) #2

; Function Attrs: nounwind
define internal i32 @avifParseMediaHeaderBox(%struct.avifTrack* %track, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %track.addr = alloca %struct.avifTrack*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %version = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %ignored32 = alloca i32, align 4
  %mediaTimescale = alloca i32, align 4
  %mediaDuration32 = alloca i32, align 4
  %ignored64 = alloca i64, align 8
  %mediaDuration64 = alloca i64, align 8
  store %struct.avifTrack* %track, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %version) #4
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadVersionAndFlags(%struct.avifROStream* %s, i8* %version, i32* null)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup74

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %ignored32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %mediaTimescale to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %mediaDuration32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i64* %ignored64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #4
  %8 = bitcast i64* %mediaDuration64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #4
  %9 = load i8, i8* %version, align 1, !tbaa !25
  %conv = zext i8 %9 to i32
  %cmp = icmp eq i32 %conv, 1
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %do.end
  br label %do.body3

do.body3:                                         ; preds = %if.then2
  %call4 = call i32 @avifROStreamReadU64(%struct.avifROStream* %s, i64* %ignored64)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %do.body3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %do.body3
  br label %do.cond8

do.cond8:                                         ; preds = %if.end7
  br label %do.end9

do.end9:                                          ; preds = %do.cond8
  br label %do.body10

do.body10:                                        ; preds = %do.end9
  %call11 = call i32 @avifROStreamReadU64(%struct.avifROStream* %s, i64* %ignored64)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %do.body10
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %do.body10
  br label %do.cond15

do.cond15:                                        ; preds = %if.end14
  br label %do.end16

do.end16:                                         ; preds = %do.cond15
  br label %do.body17

do.body17:                                        ; preds = %do.end16
  %call18 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %mediaTimescale)
  %tobool19 = icmp ne i32 %call18, 0
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %do.body17
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end21:                                         ; preds = %do.body17
  br label %do.cond22

do.cond22:                                        ; preds = %if.end21
  br label %do.end23

do.end23:                                         ; preds = %do.cond22
  br label %do.body24

do.body24:                                        ; preds = %do.end23
  %call25 = call i32 @avifROStreamReadU64(%struct.avifROStream* %s, i64* %mediaDuration64)
  %tobool26 = icmp ne i32 %call25, 0
  br i1 %tobool26, label %if.end28, label %if.then27

if.then27:                                        ; preds = %do.body24
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end28:                                         ; preds = %do.body24
  br label %do.cond29

do.cond29:                                        ; preds = %if.end28
  br label %do.end30

do.end30:                                         ; preds = %do.cond29
  %10 = load i64, i64* %mediaDuration64, align 8, !tbaa !133
  %11 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %mediaDuration = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %11, i32 0, i32 3
  store i64 %10, i64* %mediaDuration, align 8, !tbaa !61
  br label %if.end68

if.else:                                          ; preds = %do.end
  %12 = load i8, i8* %version, align 1, !tbaa !25
  %conv31 = zext i8 %12 to i32
  %cmp32 = icmp eq i32 %conv31, 0
  br i1 %cmp32, label %if.then34, label %if.else66

if.then34:                                        ; preds = %if.else
  br label %do.body35

do.body35:                                        ; preds = %if.then34
  %call36 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %ignored32)
  %tobool37 = icmp ne i32 %call36, 0
  br i1 %tobool37, label %if.end39, label %if.then38

if.then38:                                        ; preds = %do.body35
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end39:                                         ; preds = %do.body35
  br label %do.cond40

do.cond40:                                        ; preds = %if.end39
  br label %do.end41

do.end41:                                         ; preds = %do.cond40
  br label %do.body42

do.body42:                                        ; preds = %do.end41
  %call43 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %ignored32)
  %tobool44 = icmp ne i32 %call43, 0
  br i1 %tobool44, label %if.end46, label %if.then45

if.then45:                                        ; preds = %do.body42
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %do.body42
  br label %do.cond47

do.cond47:                                        ; preds = %if.end46
  br label %do.end48

do.end48:                                         ; preds = %do.cond47
  br label %do.body49

do.body49:                                        ; preds = %do.end48
  %call50 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %mediaTimescale)
  %tobool51 = icmp ne i32 %call50, 0
  br i1 %tobool51, label %if.end53, label %if.then52

if.then52:                                        ; preds = %do.body49
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %do.body49
  br label %do.cond54

do.cond54:                                        ; preds = %if.end53
  br label %do.end55

do.end55:                                         ; preds = %do.cond54
  br label %do.body56

do.body56:                                        ; preds = %do.end55
  %call57 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %mediaDuration32)
  %tobool58 = icmp ne i32 %call57, 0
  br i1 %tobool58, label %if.end60, label %if.then59

if.then59:                                        ; preds = %do.body56
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end60:                                         ; preds = %do.body56
  br label %do.cond61

do.cond61:                                        ; preds = %if.end60
  br label %do.end62

do.end62:                                         ; preds = %do.cond61
  %13 = load i32, i32* %mediaDuration32, align 4, !tbaa !12
  %conv63 = zext i32 %13 to i64
  %14 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %mediaDuration65 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %14, i32 0, i32 3
  store i64 %conv63, i64* %mediaDuration65, align 8, !tbaa !61
  br label %if.end67

if.else66:                                        ; preds = %if.else
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end67:                                         ; preds = %do.end62
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %do.end30
  %15 = load i32, i32* %mediaTimescale, align 4, !tbaa !12
  %16 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %mediaTimescale69 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %16, i32 0, i32 2
  store i32 %15, i32* %mediaTimescale69, align 8, !tbaa !59
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end68, %if.else66, %if.then59, %if.then52, %if.then45, %if.then38, %if.then27, %if.then20, %if.then13, %if.then6
  %17 = bitcast i64* %mediaDuration64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %17) #4
  %18 = bitcast i64* %ignored64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %18) #4
  %19 = bitcast i32* %mediaDuration32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %20 = bitcast i32* %mediaTimescale to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = bitcast i32* %ignored32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  br label %cleanup74

cleanup74:                                        ; preds = %cleanup, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %version) #4
  %22 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #4
  %23 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #4
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: nounwind
define internal i32 @avifParseMediaInformationBox(%struct.avifTrack* %track, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %track.addr = alloca %struct.avifTrack*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifTrack* %track, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %call = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  br label %do.body

do.body:                                          ; preds = %while.body
  %call1 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call3 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.39, i32 0, i32 0), i32 4)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.end15, label %if.then5

if.then5:                                         ; preds = %do.end
  br label %do.body6

do.body6:                                         ; preds = %if.then5
  %5 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %call7 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size8 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %6 = load i32, i32* %size8, align 4, !tbaa !10
  %call9 = call i32 @avifParseSampleTableBox(%struct.avifTrack* %5, i8* %call7, i32 %6)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %do.body6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %do.body6
  br label %do.cond13

do.cond13:                                        ; preds = %if.end12
  br label %do.end14

do.end14:                                         ; preds = %do.cond13
  br label %if.end15

if.end15:                                         ; preds = %do.end14, %do.end
  br label %do.body16

do.body16:                                        ; preds = %if.end15
  %size17 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %7 = load i32, i32* %size17, align 4, !tbaa !10
  %call18 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %7)
  %tobool19 = icmp ne i32 %call18, 0
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %do.body16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end21:                                         ; preds = %do.body16
  br label %do.cond22

do.cond22:                                        ; preds = %if.end21
  br label %do.end23

do.end23:                                         ; preds = %do.cond22
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end23, %if.then20, %if.then11, %if.then
  %8 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %8) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup24 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup24

cleanup24:                                        ; preds = %while.end, %cleanup
  %9 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %9) #4
  %10 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %10) #4
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: nounwind
define internal i32 @avifParseSampleTableBox(%struct.avifTrack* %track, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %track.addr = alloca %struct.avifTrack*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %header = alloca %struct.avifBoxHeader, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifTrack* %track, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %0, i32 0, i32 6
  %1 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 8, !tbaa !36
  %tobool = icmp ne %struct.avifSampleTable* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call = call %struct.avifSampleTable* @avifSampleTableCreate()
  %2 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable1 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %2, i32 0, i32 6
  store %struct.avifSampleTable* %call, %struct.avifSampleTable** %sampleTable1, align 8, !tbaa !36
  %3 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #4
  %4 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  %5 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %5, i8** %data, align 4, !tbaa !6
  %6 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %6, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.end
  %call2 = call i32 @avifROStreamHasBytesLeft(%struct.avifROStream* %s, i32 1)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #4
  br label %do.body

do.body:                                          ; preds = %while.body
  %call4 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %header)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end7
  br label %do.end

do.end:                                           ; preds = %do.cond
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  %call8 = call i32 @memcmp(i8* %arraydecay, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.40, i32 0, i32 0), i32 4)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %if.else, label %if.then10

if.then10:                                        ; preds = %do.end
  br label %do.body11

do.body11:                                        ; preds = %if.then10
  %8 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable12 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %8, i32 0, i32 6
  %9 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable12, align 8, !tbaa !36
  %call13 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size14 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %10 = load i32, i32* %size14, align 4, !tbaa !10
  %call15 = call i32 @avifParseChunkOffsetBox(%struct.avifSampleTable* %9, i32 0, i8* %call13, i32 %10)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %do.body11
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %do.body11
  br label %do.cond19

do.cond19:                                        ; preds = %if.end18
  br label %do.end20

do.end20:                                         ; preds = %do.cond19
  br label %if.end122

if.else:                                          ; preds = %do.end
  %type21 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay22 = getelementptr inbounds [4 x i8], [4 x i8]* %type21, i32 0, i32 0
  %call23 = call i32 @memcmp(i8* %arraydecay22, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.41, i32 0, i32 0), i32 4)
  %tobool24 = icmp ne i32 %call23, 0
  br i1 %tobool24, label %if.else36, label %if.then25

if.then25:                                        ; preds = %if.else
  br label %do.body26

do.body26:                                        ; preds = %if.then25
  %11 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable27 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %11, i32 0, i32 6
  %12 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable27, align 8, !tbaa !36
  %call28 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size29 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %13 = load i32, i32* %size29, align 4, !tbaa !10
  %call30 = call i32 @avifParseChunkOffsetBox(%struct.avifSampleTable* %12, i32 1, i8* %call28, i32 %13)
  %tobool31 = icmp ne i32 %call30, 0
  br i1 %tobool31, label %if.end33, label %if.then32

if.then32:                                        ; preds = %do.body26
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end33:                                         ; preds = %do.body26
  br label %do.cond34

do.cond34:                                        ; preds = %if.end33
  br label %do.end35

do.end35:                                         ; preds = %do.cond34
  br label %if.end121

if.else36:                                        ; preds = %if.else
  %type37 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay38 = getelementptr inbounds [4 x i8], [4 x i8]* %type37, i32 0, i32 0
  %call39 = call i32 @memcmp(i8* %arraydecay38, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.42, i32 0, i32 0), i32 4)
  %tobool40 = icmp ne i32 %call39, 0
  br i1 %tobool40, label %if.else52, label %if.then41

if.then41:                                        ; preds = %if.else36
  br label %do.body42

do.body42:                                        ; preds = %if.then41
  %14 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable43 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %14, i32 0, i32 6
  %15 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable43, align 8, !tbaa !36
  %call44 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size45 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %16 = load i32, i32* %size45, align 4, !tbaa !10
  %call46 = call i32 @avifParseSampleToChunkBox(%struct.avifSampleTable* %15, i8* %call44, i32 %16)
  %tobool47 = icmp ne i32 %call46, 0
  br i1 %tobool47, label %if.end49, label %if.then48

if.then48:                                        ; preds = %do.body42
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end49:                                         ; preds = %do.body42
  br label %do.cond50

do.cond50:                                        ; preds = %if.end49
  br label %do.end51

do.end51:                                         ; preds = %do.cond50
  br label %if.end120

if.else52:                                        ; preds = %if.else36
  %type53 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay54 = getelementptr inbounds [4 x i8], [4 x i8]* %type53, i32 0, i32 0
  %call55 = call i32 @memcmp(i8* %arraydecay54, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.43, i32 0, i32 0), i32 4)
  %tobool56 = icmp ne i32 %call55, 0
  br i1 %tobool56, label %if.else68, label %if.then57

if.then57:                                        ; preds = %if.else52
  br label %do.body58

do.body58:                                        ; preds = %if.then57
  %17 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable59 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %17, i32 0, i32 6
  %18 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable59, align 8, !tbaa !36
  %call60 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size61 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %19 = load i32, i32* %size61, align 4, !tbaa !10
  %call62 = call i32 @avifParseSampleSizeBox(%struct.avifSampleTable* %18, i8* %call60, i32 %19)
  %tobool63 = icmp ne i32 %call62, 0
  br i1 %tobool63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %do.body58
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end65:                                         ; preds = %do.body58
  br label %do.cond66

do.cond66:                                        ; preds = %if.end65
  br label %do.end67

do.end67:                                         ; preds = %do.cond66
  br label %if.end119

if.else68:                                        ; preds = %if.else52
  %type69 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay70 = getelementptr inbounds [4 x i8], [4 x i8]* %type69, i32 0, i32 0
  %call71 = call i32 @memcmp(i8* %arraydecay70, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.44, i32 0, i32 0), i32 4)
  %tobool72 = icmp ne i32 %call71, 0
  br i1 %tobool72, label %if.else84, label %if.then73

if.then73:                                        ; preds = %if.else68
  br label %do.body74

do.body74:                                        ; preds = %if.then73
  %20 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable75 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %20, i32 0, i32 6
  %21 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable75, align 8, !tbaa !36
  %call76 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size77 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %22 = load i32, i32* %size77, align 4, !tbaa !10
  %call78 = call i32 @avifParseSyncSampleBox(%struct.avifSampleTable* %21, i8* %call76, i32 %22)
  %tobool79 = icmp ne i32 %call78, 0
  br i1 %tobool79, label %if.end81, label %if.then80

if.then80:                                        ; preds = %do.body74
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end81:                                         ; preds = %do.body74
  br label %do.cond82

do.cond82:                                        ; preds = %if.end81
  br label %do.end83

do.end83:                                         ; preds = %do.cond82
  br label %if.end118

if.else84:                                        ; preds = %if.else68
  %type85 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay86 = getelementptr inbounds [4 x i8], [4 x i8]* %type85, i32 0, i32 0
  %call87 = call i32 @memcmp(i8* %arraydecay86, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.45, i32 0, i32 0), i32 4)
  %tobool88 = icmp ne i32 %call87, 0
  br i1 %tobool88, label %if.else100, label %if.then89

if.then89:                                        ; preds = %if.else84
  br label %do.body90

do.body90:                                        ; preds = %if.then89
  %23 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable91 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %23, i32 0, i32 6
  %24 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable91, align 8, !tbaa !36
  %call92 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size93 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %25 = load i32, i32* %size93, align 4, !tbaa !10
  %call94 = call i32 @avifParseTimeToSampleBox(%struct.avifSampleTable* %24, i8* %call92, i32 %25)
  %tobool95 = icmp ne i32 %call94, 0
  br i1 %tobool95, label %if.end97, label %if.then96

if.then96:                                        ; preds = %do.body90
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end97:                                         ; preds = %do.body90
  br label %do.cond98

do.cond98:                                        ; preds = %if.end97
  br label %do.end99

do.end99:                                         ; preds = %do.cond98
  br label %if.end117

if.else100:                                       ; preds = %if.else84
  %type101 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 1
  %arraydecay102 = getelementptr inbounds [4 x i8], [4 x i8]* %type101, i32 0, i32 0
  %call103 = call i32 @memcmp(i8* %arraydecay102, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.46, i32 0, i32 0), i32 4)
  %tobool104 = icmp ne i32 %call103, 0
  br i1 %tobool104, label %if.end116, label %if.then105

if.then105:                                       ; preds = %if.else100
  br label %do.body106

do.body106:                                       ; preds = %if.then105
  %26 = load %struct.avifTrack*, %struct.avifTrack** %track.addr, align 4, !tbaa !2
  %sampleTable107 = getelementptr inbounds %struct.avifTrack, %struct.avifTrack* %26, i32 0, i32 6
  %27 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable107, align 8, !tbaa !36
  %call108 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %size109 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %28 = load i32, i32* %size109, align 4, !tbaa !10
  %call110 = call i32 @avifParseSampleDescriptionBox(%struct.avifSampleTable* %27, i8* %call108, i32 %28)
  %tobool111 = icmp ne i32 %call110, 0
  br i1 %tobool111, label %if.end113, label %if.then112

if.then112:                                       ; preds = %do.body106
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end113:                                        ; preds = %do.body106
  br label %do.cond114

do.cond114:                                       ; preds = %if.end113
  br label %do.end115

do.end115:                                        ; preds = %do.cond114
  br label %if.end116

if.end116:                                        ; preds = %do.end115, %if.else100
  br label %if.end117

if.end117:                                        ; preds = %if.end116, %do.end99
  br label %if.end118

if.end118:                                        ; preds = %if.end117, %do.end83
  br label %if.end119

if.end119:                                        ; preds = %if.end118, %do.end67
  br label %if.end120

if.end120:                                        ; preds = %if.end119, %do.end51
  br label %if.end121

if.end121:                                        ; preds = %if.end120, %do.end35
  br label %if.end122

if.end122:                                        ; preds = %if.end121, %do.end20
  br label %do.body123

do.body123:                                       ; preds = %if.end122
  %size124 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %header, i32 0, i32 0
  %29 = load i32, i32* %size124, align 4, !tbaa !10
  %call125 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %29)
  %tobool126 = icmp ne i32 %call125, 0
  br i1 %tobool126, label %if.end128, label %if.then127

if.then127:                                       ; preds = %do.body123
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end128:                                        ; preds = %do.body123
  br label %do.cond129

do.cond129:                                       ; preds = %if.end128
  br label %do.end130

do.end130:                                        ; preds = %do.cond129
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end130, %if.then127, %if.then112, %if.then96, %if.then80, %if.then64, %if.then48, %if.then32, %if.then17, %if.then6
  %30 = bitcast %struct.avifBoxHeader* %header to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %30) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup131 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup131

cleanup131:                                       ; preds = %while.end, %cleanup
  %31 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %31) #4
  %32 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %32) #4
  br label %return

return:                                           ; preds = %cleanup131, %if.then
  %33 = load i32, i32* %retval, align 4
  ret i32 %33
}

; Function Attrs: nounwind
define internal i32 @avifParseChunkOffsetBox(%struct.avifSampleTable* %sampleTable, i32 %largeOffsets, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %largeOffsets.addr = alloca i32, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %entryCount = alloca i32, align 4
  %i = alloca i32, align 4
  %offset = alloca i64, align 8
  %offset32 = alloca i32, align 4
  %chunk = alloca %struct.avifSampleTableChunk*, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store i32 %largeOffsets, i32* %largeOffsets.addr, align 4, !tbaa !12
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %entryCount)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup32

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end7
  %6 = load i32, i32* %i, align 4, !tbaa !12
  %7 = load i32, i32* %entryCount, align 4, !tbaa !12
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

for.body:                                         ; preds = %for.cond
  %8 = bitcast i64* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #4
  %9 = load i32, i32* %largeOffsets.addr, align 4, !tbaa !12
  %tobool8 = icmp ne i32 %9, 0
  br i1 %tobool8, label %if.then9, label %if.else

if.then9:                                         ; preds = %for.body
  br label %do.body10

do.body10:                                        ; preds = %if.then9
  %call11 = call i32 @avifROStreamReadU64(%struct.avifROStream* %s, i64* %offset)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %do.body10
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup27

if.end14:                                         ; preds = %do.body10
  br label %do.cond15

do.cond15:                                        ; preds = %if.end14
  br label %do.end16

do.end16:                                         ; preds = %do.cond15
  br label %if.end24

if.else:                                          ; preds = %for.body
  %10 = bitcast i32* %offset32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  br label %do.body17

do.body17:                                        ; preds = %if.else
  %call18 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %offset32)
  %tobool19 = icmp ne i32 %call18, 0
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %do.body17
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end21:                                         ; preds = %do.body17
  br label %do.cond22

do.cond22:                                        ; preds = %if.end21
  br label %do.end23

do.end23:                                         ; preds = %do.cond22
  %11 = load i32, i32* %offset32, align 4, !tbaa !12
  %conv = zext i32 %11 to i64
  store i64 %conv, i64* %offset, align 8, !tbaa !133
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end23, %if.then20
  %12 = bitcast i32* %offset32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup27 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end24

if.end24:                                         ; preds = %cleanup.cont, %do.end16
  %13 = bitcast %struct.avifSampleTableChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %chunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %14, i32 0, i32 0
  %15 = bitcast %struct.avifSampleTableChunkArray* %chunks to i8*
  %call25 = call i8* @avifArrayPushPtr(i8* %15)
  %16 = bitcast i8* %call25 to %struct.avifSampleTableChunk*
  store %struct.avifSampleTableChunk* %16, %struct.avifSampleTableChunk** %chunk, align 4, !tbaa !2
  %17 = load i64, i64* %offset, align 8, !tbaa !133
  %18 = load %struct.avifSampleTableChunk*, %struct.avifSampleTableChunk** %chunk, align 4, !tbaa !2
  %offset26 = getelementptr inbounds %struct.avifSampleTableChunk, %struct.avifSampleTableChunk* %18, i32 0, i32 0
  store i64 %17, i64* %offset26, align 8, !tbaa !124
  %19 = bitcast %struct.avifSampleTableChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup27

cleanup27:                                        ; preds = %if.end24, %cleanup, %if.then13
  %20 = bitcast i64* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %20) #4
  %cleanup.dest28 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest28, label %cleanup30 [
    i32 0, label %cleanup.cont29
  ]

cleanup.cont29:                                   ; preds = %cleanup27
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont29
  %21 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup30:                                        ; preds = %cleanup27, %for.cond.cleanup
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  %cleanup.dest31 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest31, label %cleanup32 [
    i32 6, label %for.end
  ]

for.end:                                          ; preds = %cleanup30
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup32

cleanup32:                                        ; preds = %for.end, %cleanup30, %if.then4
  %23 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  br label %cleanup33

cleanup33:                                        ; preds = %cleanup32, %if.then
  %24 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %24) #4
  %25 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %25) #4
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define internal i32 @avifParseSampleToChunkBox(%struct.avifSampleTable* %sampleTable, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %entryCount = alloca i32, align 4
  %i = alloca i32, align 4
  %sampleToChunk = alloca %struct.avifSampleTableSampleToChunk*, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup33

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %entryCount)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup32

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end7
  %6 = load i32, i32* %i, align 4, !tbaa !12
  %7 = load i32, i32* %entryCount, align 4, !tbaa !12
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

for.body:                                         ; preds = %for.cond
  %8 = bitcast %struct.avifSampleTableSampleToChunk** %sampleToChunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleToChunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %9, i32 0, i32 2
  %10 = bitcast %struct.avifSampleTableSampleToChunkArray* %sampleToChunks to i8*
  %call8 = call i8* @avifArrayPushPtr(i8* %10)
  %11 = bitcast i8* %call8 to %struct.avifSampleTableSampleToChunk*
  store %struct.avifSampleTableSampleToChunk* %11, %struct.avifSampleTableSampleToChunk** %sampleToChunk, align 4, !tbaa !2
  br label %do.body9

do.body9:                                         ; preds = %for.body
  %12 = load %struct.avifSampleTableSampleToChunk*, %struct.avifSampleTableSampleToChunk** %sampleToChunk, align 4, !tbaa !2
  %firstChunk = getelementptr inbounds %struct.avifSampleTableSampleToChunk, %struct.avifSampleTableSampleToChunk* %12, i32 0, i32 0
  %call10 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %firstChunk)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %do.body9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %do.body9
  br label %do.cond14

do.cond14:                                        ; preds = %if.end13
  br label %do.end15

do.end15:                                         ; preds = %do.cond14
  br label %do.body16

do.body16:                                        ; preds = %do.end15
  %13 = load %struct.avifSampleTableSampleToChunk*, %struct.avifSampleTableSampleToChunk** %sampleToChunk, align 4, !tbaa !2
  %samplesPerChunk = getelementptr inbounds %struct.avifSampleTableSampleToChunk, %struct.avifSampleTableSampleToChunk* %13, i32 0, i32 1
  %call17 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %samplesPerChunk)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %do.body16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %do.body16
  br label %do.cond21

do.cond21:                                        ; preds = %if.end20
  br label %do.end22

do.end22:                                         ; preds = %do.cond21
  br label %do.body23

do.body23:                                        ; preds = %do.end22
  %14 = load %struct.avifSampleTableSampleToChunk*, %struct.avifSampleTableSampleToChunk** %sampleToChunk, align 4, !tbaa !2
  %sampleDescriptionIndex = getelementptr inbounds %struct.avifSampleTableSampleToChunk, %struct.avifSampleTableSampleToChunk* %14, i32 0, i32 2
  %call24 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %sampleDescriptionIndex)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %do.body23
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %do.body23
  br label %do.cond28

do.cond28:                                        ; preds = %if.end27
  br label %do.end29

do.end29:                                         ; preds = %do.cond28
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end29, %if.then26, %if.then19, %if.then12
  %15 = bitcast %struct.avifSampleTableSampleToChunk** %sampleToChunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup30 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %16 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %16, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup30:                                        ; preds = %cleanup, %for.cond.cleanup
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  %cleanup.dest31 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest31, label %cleanup32 [
    i32 6, label %for.end
  ]

for.end:                                          ; preds = %cleanup30
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup32

cleanup32:                                        ; preds = %for.end, %cleanup30, %if.then4
  %18 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  br label %cleanup33

cleanup33:                                        ; preds = %cleanup32, %if.then
  %19 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %19) #4
  %20 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %20) #4
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define internal i32 @avifParseSampleSizeBox(%struct.avifSampleTable* %sampleTable, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %allSamplesSize = alloca i32, align 4
  %sampleCount = alloca i32, align 4
  %i = alloca i32, align 4
  %sampleSize = alloca %struct.avifSampleTableSampleSize*, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup32

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %allSamplesSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %sampleCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %allSamplesSize)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  br label %do.body8

do.body8:                                         ; preds = %do.end7
  %call9 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %sampleCount)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %do.body8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

if.end12:                                         ; preds = %do.body8
  br label %do.cond13

do.cond13:                                        ; preds = %if.end12
  br label %do.end14

do.end14:                                         ; preds = %do.cond13
  %6 = load i32, i32* %allSamplesSize, align 4, !tbaa !12
  %cmp = icmp ugt i32 %6, 0
  br i1 %cmp, label %if.then15, label %if.else

if.then15:                                        ; preds = %do.end14
  %7 = load i32, i32* %allSamplesSize, align 4, !tbaa !12
  %8 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %allSamplesSize16 = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %8, i32 0, i32 6
  store i32 %7, i32* %allSamplesSize16, align 4, !tbaa !144
  br label %if.end29

if.else:                                          ; preds = %do.end14
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %10 = load i32, i32* %i, align 4, !tbaa !12
  %11 = load i32, i32* %sampleCount, align 4, !tbaa !12
  %cmp17 = icmp ult i32 %10, %11
  br i1 %cmp17, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 8, i32* %cleanup.dest.slot, align 4
  br label %cleanup27

for.body:                                         ; preds = %for.cond
  %12 = bitcast %struct.avifSampleTableSampleSize** %sampleSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleSizes = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %13, i32 0, i32 3
  %14 = bitcast %struct.avifSampleTableSampleSizeArray* %sampleSizes to i8*
  %call18 = call i8* @avifArrayPushPtr(i8* %14)
  %15 = bitcast i8* %call18 to %struct.avifSampleTableSampleSize*
  store %struct.avifSampleTableSampleSize* %15, %struct.avifSampleTableSampleSize** %sampleSize, align 4, !tbaa !2
  br label %do.body19

do.body19:                                        ; preds = %for.body
  %16 = load %struct.avifSampleTableSampleSize*, %struct.avifSampleTableSampleSize** %sampleSize, align 4, !tbaa !2
  %size20 = getelementptr inbounds %struct.avifSampleTableSampleSize, %struct.avifSampleTableSampleSize* %16, i32 0, i32 0
  %call21 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %size20)
  %tobool22 = icmp ne i32 %call21, 0
  br i1 %tobool22, label %if.end24, label %if.then23

if.then23:                                        ; preds = %do.body19
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end24:                                         ; preds = %do.body19
  br label %do.cond25

do.cond25:                                        ; preds = %if.end24
  br label %do.end26

do.end26:                                         ; preds = %do.cond25
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end26, %if.then23
  %17 = bitcast %struct.avifSampleTableSampleSize** %sampleSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup27 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %18 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup27:                                        ; preds = %cleanup, %for.cond.cleanup
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %cleanup.dest28 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest28, label %cleanup30 [
    i32 8, label %for.end
  ]

for.end:                                          ; preds = %cleanup27
  br label %if.end29

if.end29:                                         ; preds = %for.end, %if.then15
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup30

cleanup30:                                        ; preds = %if.end29, %cleanup27, %if.then11, %if.then4
  %20 = bitcast i32* %sampleCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = bitcast i32* %allSamplesSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  br label %cleanup32

cleanup32:                                        ; preds = %cleanup30, %if.then
  %22 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #4
  %23 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #4
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: nounwind
define internal i32 @avifParseSyncSampleBox(%struct.avifSampleTable* %sampleTable, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %entryCount = alloca i32, align 4
  %i = alloca i32, align 4
  %sampleNumber = alloca i32, align 4
  %syncSample = alloca %struct.avifSyncSample*, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup20

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %entryCount)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end7
  %6 = load i32, i32* %i, align 4, !tbaa !12
  %7 = load i32, i32* %entryCount, align 4, !tbaa !12
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

for.body:                                         ; preds = %for.cond
  %8 = bitcast i32* %sampleNumber to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store i32 0, i32* %sampleNumber, align 4, !tbaa !12
  br label %do.body8

do.body8:                                         ; preds = %for.body
  %call9 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %sampleNumber)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %do.body8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %do.body8
  br label %do.cond13

do.cond13:                                        ; preds = %if.end12
  br label %do.end14

do.end14:                                         ; preds = %do.cond13
  %9 = bitcast %struct.avifSyncSample** %syncSample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %syncSamples = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %10, i32 0, i32 5
  %11 = bitcast %struct.avifSyncSampleArray* %syncSamples to i8*
  %call15 = call i8* @avifArrayPushPtr(i8* %11)
  %12 = bitcast i8* %call15 to %struct.avifSyncSample*
  store %struct.avifSyncSample* %12, %struct.avifSyncSample** %syncSample, align 4, !tbaa !2
  %13 = load i32, i32* %sampleNumber, align 4, !tbaa !12
  %14 = load %struct.avifSyncSample*, %struct.avifSyncSample** %syncSample, align 4, !tbaa !2
  %sampleNumber16 = getelementptr inbounds %struct.avifSyncSample, %struct.avifSyncSample* %14, i32 0, i32 0
  store i32 %13, i32* %sampleNumber16, align 4, !tbaa !151
  %15 = bitcast %struct.avifSyncSample** %syncSample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end14, %if.then11
  %16 = bitcast i32* %sampleNumber to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup17 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %17 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup17:                                        ; preds = %cleanup, %for.cond.cleanup
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %cleanup.dest18 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest18, label %cleanup19 [
    i32 6, label %for.end
  ]

for.end:                                          ; preds = %cleanup17
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup19

cleanup19:                                        ; preds = %for.end, %cleanup17, %if.then4
  %19 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  br label %cleanup20

cleanup20:                                        ; preds = %cleanup19, %if.then
  %20 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %20) #4
  %21 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %21) #4
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

; Function Attrs: nounwind
define internal i32 @avifParseTimeToSampleBox(%struct.avifSampleTable* %sampleTable, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %entryCount = alloca i32, align 4
  %i = alloca i32, align 4
  %timeToSample = alloca %struct.avifSampleTableTimeToSample*, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup26

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %entryCount)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end7
  %6 = load i32, i32* %i, align 4, !tbaa !12
  %7 = load i32, i32* %entryCount, align 4, !tbaa !12
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

for.body:                                         ; preds = %for.cond
  %8 = bitcast %struct.avifSampleTableTimeToSample** %timeToSample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %timeToSamples = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %9, i32 0, i32 4
  %10 = bitcast %struct.avifSampleTableTimeToSampleArray* %timeToSamples to i8*
  %call8 = call i8* @avifArrayPushPtr(i8* %10)
  %11 = bitcast i8* %call8 to %struct.avifSampleTableTimeToSample*
  store %struct.avifSampleTableTimeToSample* %11, %struct.avifSampleTableTimeToSample** %timeToSample, align 4, !tbaa !2
  br label %do.body9

do.body9:                                         ; preds = %for.body
  %12 = load %struct.avifSampleTableTimeToSample*, %struct.avifSampleTableTimeToSample** %timeToSample, align 4, !tbaa !2
  %sampleCount = getelementptr inbounds %struct.avifSampleTableTimeToSample, %struct.avifSampleTableTimeToSample* %12, i32 0, i32 0
  %call10 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %sampleCount)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %do.body9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %do.body9
  br label %do.cond14

do.cond14:                                        ; preds = %if.end13
  br label %do.end15

do.end15:                                         ; preds = %do.cond14
  br label %do.body16

do.body16:                                        ; preds = %do.end15
  %13 = load %struct.avifSampleTableTimeToSample*, %struct.avifSampleTableTimeToSample** %timeToSample, align 4, !tbaa !2
  %sampleDelta = getelementptr inbounds %struct.avifSampleTableTimeToSample, %struct.avifSampleTableTimeToSample* %13, i32 0, i32 1
  %call17 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %sampleDelta)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %do.body16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %do.body16
  br label %do.cond21

do.cond21:                                        ; preds = %if.end20
  br label %do.end22

do.end22:                                         ; preds = %do.cond21
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end22, %if.then19, %if.then12
  %14 = bitcast %struct.avifSampleTableTimeToSample** %timeToSample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup23 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %15 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup23:                                        ; preds = %cleanup, %for.cond.cleanup
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  %cleanup.dest24 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest24, label %cleanup25 [
    i32 6, label %for.end
  ]

for.end:                                          ; preds = %cleanup23
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

cleanup25:                                        ; preds = %for.end, %cleanup23, %if.then4
  %17 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %cleanup26

cleanup26:                                        ; preds = %cleanup25, %if.then
  %18 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %18) #4
  %19 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %19) #4
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: nounwind
define internal i32 @avifParseSampleDescriptionBox(%struct.avifSampleTable* %sampleTable, i8* %raw, i32 %rawLen) #0 {
entry:
  %retval = alloca i32, align 4
  %sampleTable.addr = alloca %struct.avifSampleTable*, align 4
  %raw.addr = alloca i8*, align 4
  %rawLen.addr = alloca i32, align 4
  %s = alloca %struct.avifROStream, align 4
  %s_roData = alloca %struct.avifROData, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %entryCount = alloca i32, align 4
  %i = alloca i32, align 4
  %sampleEntryHeader = alloca %struct.avifBoxHeader, align 4
  %description = alloca %struct.avifSampleDescription*, align 4
  %remainingBytes = alloca i32, align 4
  store %struct.avifSampleTable* %sampleTable, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  store i8* %raw, i8** %raw.addr, align 4, !tbaa !2
  store i32 %rawLen, i32* %rawLen.addr, align 4, !tbaa !14
  %0 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #4
  %1 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #4
  %2 = load i8*, i8** %raw.addr, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 0
  store i8* %2, i8** %data, align 4, !tbaa !6
  %3 = load i32, i32* %rawLen.addr, align 4, !tbaa !14
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %s_roData, i32 0, i32 1
  store i32 %3, i32* %size, align 4, !tbaa !9
  call void @avifROStreamStart(%struct.avifROStream* %s, %struct.avifROData* %s_roData)
  br label %do.body

do.body:                                          ; preds = %entry
  %call = call i32 @avifROStreamReadAndEnforceVersion(%struct.avifROStream* %s, i8 zeroext 0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup47

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  br label %do.end

do.end:                                           ; preds = %do.cond
  %4 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  br label %do.body1

do.body1:                                         ; preds = %do.end
  %call2 = call i32 @avifROStreamReadU32(%struct.avifROStream* %s, i32* %entryCount)
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.body1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

if.end5:                                          ; preds = %do.body1
  br label %do.cond6

do.cond6:                                         ; preds = %if.end5
  br label %do.end7

do.end7:                                          ; preds = %do.cond6
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end7
  %6 = load i32, i32* %i, align 4, !tbaa !12
  %7 = load i32, i32* %entryCount, align 4, !tbaa !12
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup44

for.body:                                         ; preds = %for.cond
  %8 = bitcast %struct.avifBoxHeader* %sampleEntryHeader to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #4
  br label %do.body8

do.body8:                                         ; preds = %for.body
  %call9 = call i32 @avifROStreamReadBoxHeader(%struct.avifROStream* %s, %struct.avifBoxHeader* %sampleEntryHeader)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %do.body8
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup43

if.end12:                                         ; preds = %do.body8
  br label %do.cond13

do.cond13:                                        ; preds = %if.end12
  br label %do.end14

do.end14:                                         ; preds = %do.cond13
  %9 = bitcast %struct.avifSampleDescription** %description to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable.addr, align 4, !tbaa !2
  %sampleDescriptions = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %10, i32 0, i32 1
  %11 = bitcast %struct.avifSampleDescriptionArray* %sampleDescriptions to i8*
  %call15 = call i8* @avifArrayPushPtr(i8* %11)
  %12 = bitcast i8* %call15 to %struct.avifSampleDescription*
  store %struct.avifSampleDescription* %12, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %13 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %properties = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %13, i32 0, i32 1
  %14 = bitcast %struct.avifPropertyArray* %properties to i8*
  call void @avifArrayCreate(i8* %14, i32 68, i32 16)
  %15 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %format = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %15, i32 0, i32 0
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %format, i32 0, i32 0
  %type = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %sampleEntryHeader, i32 0, i32 1
  %arraydecay16 = getelementptr inbounds [4 x i8], [4 x i8]* %type, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %arraydecay, i8* align 4 %arraydecay16, i32 4, i1 false)
  %16 = bitcast i32* %remainingBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %call17 = call i32 @avifROStreamRemainingBytes(%struct.avifROStream* %s)
  store i32 %call17, i32* %remainingBytes, align 4, !tbaa !14
  %17 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %format18 = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %17, i32 0, i32 0
  %arraydecay19 = getelementptr inbounds [4 x i8], [4 x i8]* %format18, i32 0, i32 0
  %call20 = call i32 @memcmp(i8* %arraydecay19, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0), i32 4)
  %tobool21 = icmp ne i32 %call20, 0
  br i1 %tobool21, label %if.end33, label %land.lhs.true

land.lhs.true:                                    ; preds = %do.end14
  %18 = load i32, i32* %remainingBytes, align 4, !tbaa !14
  %cmp22 = icmp ugt i32 %18, 78
  br i1 %cmp22, label %if.then23, label %if.end33

if.then23:                                        ; preds = %land.lhs.true
  br label %do.body24

do.body24:                                        ; preds = %if.then23
  %19 = load %struct.avifSampleDescription*, %struct.avifSampleDescription** %description, align 4, !tbaa !2
  %properties25 = getelementptr inbounds %struct.avifSampleDescription, %struct.avifSampleDescription* %19, i32 0, i32 1
  %call26 = call i8* @avifROStreamCurrent(%struct.avifROStream* %s)
  %add.ptr = getelementptr inbounds i8, i8* %call26, i32 78
  %20 = load i32, i32* %remainingBytes, align 4, !tbaa !14
  %sub = sub i32 %20, 78
  %call27 = call i32 @avifParseItemPropertyContainerBox(%struct.avifPropertyArray* %properties25, i8* %add.ptr, i32 %sub)
  %tobool28 = icmp ne i32 %call27, 0
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %do.body24
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end30:                                         ; preds = %do.body24
  br label %do.cond31

do.cond31:                                        ; preds = %if.end30
  br label %do.end32

do.end32:                                         ; preds = %do.cond31
  br label %if.end33

if.end33:                                         ; preds = %do.end32, %land.lhs.true, %do.end14
  br label %do.body34

do.body34:                                        ; preds = %if.end33
  %size35 = getelementptr inbounds %struct.avifBoxHeader, %struct.avifBoxHeader* %sampleEntryHeader, i32 0, i32 0
  %21 = load i32, i32* %size35, align 4, !tbaa !10
  %call36 = call i32 @avifROStreamSkip(%struct.avifROStream* %s, i32 %21)
  %tobool37 = icmp ne i32 %call36, 0
  br i1 %tobool37, label %if.end39, label %if.then38

if.then38:                                        ; preds = %do.body34
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end39:                                         ; preds = %do.body34
  br label %do.cond40

do.cond40:                                        ; preds = %if.end39
  br label %do.end41

do.end41:                                         ; preds = %do.cond40
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end41, %if.then38, %if.then29
  %22 = bitcast i32* %remainingBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  %23 = bitcast %struct.avifSampleDescription** %description to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  br label %cleanup43

cleanup43:                                        ; preds = %cleanup, %if.then11
  %24 = bitcast %struct.avifBoxHeader* %sampleEntryHeader to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %24) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup44 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup43
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %25 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %25, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

cleanup44:                                        ; preds = %cleanup43, %for.cond.cleanup
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %cleanup.dest45 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest45, label %cleanup46 [
    i32 6, label %for.end
  ]

for.end:                                          ; preds = %cleanup44
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

cleanup46:                                        ; preds = %for.end, %cleanup44, %if.then4
  %27 = bitcast i32* %entryCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  br label %cleanup47

cleanup47:                                        ; preds = %cleanup46, %if.then
  %28 = bitcast %struct.avifROData* %s_roData to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %28) #4
  %29 = bitcast %struct.avifROStream* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %29) #4
  %30 = load i32, i32* %retval, align 4
  ret i32 %30
}

; Function Attrs: nounwind
define internal %struct.avifSampleTable* @avifSampleTableCreate() #0 {
entry:
  %sampleTable = alloca %struct.avifSampleTable*, align 4
  %0 = bitcast %struct.avifSampleTable** %sampleTable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 100)
  %1 = bitcast i8* %call to %struct.avifSampleTable*
  store %struct.avifSampleTable* %1, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %2 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %3 = bitcast %struct.avifSampleTable* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 100, i1 false)
  %4 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %chunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %4, i32 0, i32 0
  %5 = bitcast %struct.avifSampleTableChunkArray* %chunks to i8*
  call void @avifArrayCreate(i8* %5, i32 8, i32 16)
  %6 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %sampleDescriptions = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %6, i32 0, i32 1
  %7 = bitcast %struct.avifSampleDescriptionArray* %sampleDescriptions to i8*
  call void @avifArrayCreate(i8* %7, i32 20, i32 2)
  %8 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %sampleToChunks = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %8, i32 0, i32 2
  %9 = bitcast %struct.avifSampleTableSampleToChunkArray* %sampleToChunks to i8*
  call void @avifArrayCreate(i8* %9, i32 12, i32 16)
  %10 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %sampleSizes = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %10, i32 0, i32 3
  %11 = bitcast %struct.avifSampleTableSampleSizeArray* %sampleSizes to i8*
  call void @avifArrayCreate(i8* %11, i32 4, i32 16)
  %12 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %timeToSamples = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %12, i32 0, i32 4
  %13 = bitcast %struct.avifSampleTableTimeToSampleArray* %timeToSamples to i8*
  call void @avifArrayCreate(i8* %13, i32 8, i32 16)
  %14 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %syncSamples = getelementptr inbounds %struct.avifSampleTable, %struct.avifSampleTable* %14, i32 0, i32 5
  %15 = bitcast %struct.avifSyncSampleArray* %syncSamples to i8*
  call void @avifArrayCreate(i8* %15, i32 4, i32 16)
  %16 = load %struct.avifSampleTable*, %struct.avifSampleTable** %sampleTable, align 4, !tbaa !2
  %17 = bitcast %struct.avifSampleTable** %sampleTable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  ret %struct.avifSampleTable* %16
}

declare void @avifCodecDestroy(%struct.avifCodec*) #2

declare void @avifImageSetMetadataExif(%struct.avifImage*, i8*, i32) #2

declare void @avifImageSetMetadataXMP(%struct.avifImage*, i8*, i32) #2

declare i32 @strcmp(i8*, i8*) #2

; Function Attrs: nounwind
define internal void @avifDecoderDataResetCodec(%struct.avifDecoderData* %data) #0 {
entry:
  %data.addr = alloca %struct.avifDecoderData*, align 4
  %i = alloca i32, align 4
  %tile = alloca %struct.avifTile*, align 4
  store %struct.avifDecoderData* %data, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !12
  %2 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %2, i32 0, i32 4
  %count = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles, i32 0, i32 2
  %3 = load i32, i32* %count, align 4, !tbaa !92
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.avifDecoderData*, %struct.avifDecoderData** %data.addr, align 4, !tbaa !2
  %tiles1 = getelementptr inbounds %struct.avifDecoderData, %struct.avifDecoderData* %6, i32 0, i32 4
  %tile2 = getelementptr inbounds %struct.avifTileArray, %struct.avifTileArray* %tiles1, i32 0, i32 0
  %7 = load %struct.avifTile*, %struct.avifTile** %tile2, align 4, !tbaa !104
  %8 = load i32, i32* %i, align 4, !tbaa !12
  %arrayidx = getelementptr inbounds %struct.avifTile, %struct.avifTile* %7, i32 %8
  store %struct.avifTile* %arrayidx, %struct.avifTile** %tile, align 4, !tbaa !2
  %9 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %image = getelementptr inbounds %struct.avifTile, %struct.avifTile* %9, i32 0, i32 2
  %10 = load %struct.avifImage*, %struct.avifImage** %image, align 4, !tbaa !135
  %tobool = icmp ne %struct.avifImage* %10, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %11 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %image3 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %11, i32 0, i32 2
  %12 = load %struct.avifImage*, %struct.avifImage** %image3, align 4, !tbaa !135
  call void @avifImageFreePlanes(%struct.avifImage* %12, i32 255)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %13 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec = getelementptr inbounds %struct.avifTile, %struct.avifTile* %13, i32 0, i32 1
  %14 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !134
  %tobool4 = icmp ne %struct.avifCodec* %14, null
  br i1 %tobool4, label %if.then5, label %if.end8

if.then5:                                         ; preds = %if.end
  %15 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec6 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %15, i32 0, i32 1
  %16 = load %struct.avifCodec*, %struct.avifCodec** %codec6, align 4, !tbaa !134
  call void @avifCodecDestroy(%struct.avifCodec* %16)
  %17 = load %struct.avifTile*, %struct.avifTile** %tile, align 4, !tbaa !2
  %codec7 = getelementptr inbounds %struct.avifTile, %struct.avifTile* %17, i32 0, i32 1
  store %struct.avifCodec* null, %struct.avifCodec** %codec7, align 4, !tbaa !134
  br label %if.end8

if.end8:                                          ; preds = %if.then5, %if.end
  %18 = bitcast %struct.avifTile** %tile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  br label %for.inc

for.inc:                                          ; preds = %if.end8
  %19 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal %struct.avifCodec* @avifCodecCreateInternal(i32 %choice, %struct.avifCodecDecodeInput* %decodeInput) #0 {
entry:
  %choice.addr = alloca i32, align 4
  %decodeInput.addr = alloca %struct.avifCodecDecodeInput*, align 4
  %codec = alloca %struct.avifCodec*, align 4
  store i32 %choice, i32* %choice.addr, align 4, !tbaa !25
  store %struct.avifCodecDecodeInput* %decodeInput, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifCodec** %codec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %choice.addr, align 4, !tbaa !25
  %call = call %struct.avifCodec* @avifCodecCreate(i32 %1, i32 1)
  store %struct.avifCodec* %call, %struct.avifCodec** %codec, align 4, !tbaa !2
  %2 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %tobool = icmp ne %struct.avifCodec* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput.addr, align 4, !tbaa !2
  %4 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %decodeInput1 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %4, i32 0, i32 0
  store %struct.avifCodecDecodeInput* %3, %struct.avifCodecDecodeInput** %decodeInput1, align 4, !tbaa !210
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %6 = bitcast %struct.avifCodec** %codec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #4
  ret %struct.avifCodec* %5
}

declare %struct.avifCodec* @avifCodecCreate(i32, i32) #2

declare void @avifImageAllocatePlanes(%struct.avifImage*, i32) #2

declare void @avifGetPixelFormatInfo(i32, %struct.avifPixelFormatInfo*) #2

declare i32 @avifImageUsesU16(%struct.avifImage*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 0}
!7 = !{!"avifROData", !3, i64 0, !8, i64 4}
!8 = !{!"long", !4, i64 0}
!9 = !{!7, !8, i64 4}
!10 = !{!11, !8, i64 0}
!11 = !{!"avifBoxHeader", !8, i64 0, !4, i64 4}
!12 = !{!13, !13, i64 0}
!13 = !{!"int", !4, i64 0}
!14 = !{!8, !8, i64 0}
!15 = !{!16, !3, i64 8}
!16 = !{!"avifFileType", !4, i64 0, !13, i64 4, !3, i64 8, !13, i64 12}
!17 = !{!16, !13, i64 12}
!18 = !{!19, !3, i64 100}
!19 = !{!"avifDecoder", !4, i64 0, !4, i64 4, !3, i64 8, !13, i64 12, !13, i64 16, !20, i64 24, !21, i64 64, !22, i64 72, !21, i64 80, !13, i64 88, !23, i64 92, !3, i64 100}
!20 = !{!"avifImageTiming", !21, i64 0, !22, i64 8, !21, i64 16, !22, i64 24, !21, i64 32}
!21 = !{!"long long", !4, i64 0}
!22 = !{!"double", !4, i64 0}
!23 = !{!"avifIOStats", !8, i64 0, !8, i64 4}
!24 = !{!19, !3, i64 8}
!25 = !{!4, !4, i64 0}
!26 = !{!19, !4, i64 4}
!27 = !{!28, !13, i64 100}
!28 = !{!"avifDecoderData", !16, i64 0, !3, i64 16, !29, i64 20, !7, i64 36, !30, i64 44, !13, i64 60, !13, i64 64, !31, i64 68, !31, i64 80, !4, i64 92, !3, i64 96, !13, i64 100}
!29 = !{!"avifTrackArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!30 = !{!"avifTileArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!31 = !{!"avifImageGrid", !4, i64 0, !4, i64 1, !13, i64 4, !13, i64 8}
!32 = !{!28, !3, i64 96}
!33 = !{!28, !13, i64 28}
!34 = !{!28, !4, i64 92}
!35 = !{!28, !3, i64 20}
!36 = !{!37, !3, i64 32}
!37 = !{!"avifTrack", !13, i64 0, !13, i64 4, !13, i64 8, !21, i64 16, !13, i64 24, !13, i64 28, !3, i64 32, !3, i64 36}
!38 = !{!37, !13, i64 0}
!39 = !{!40, !13, i64 8}
!40 = !{!"avifSampleTable", !41, i64 0, !42, i64 16, !43, i64 32, !44, i64 48, !45, i64 64, !46, i64 80, !13, i64 96}
!41 = !{!"avifSampleTableChunkArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!42 = !{!"avifSampleDescriptionArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!43 = !{!"avifSampleTableSampleToChunkArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!44 = !{!"avifSampleTableSampleSizeArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!45 = !{!"avifSampleTableTimeToSampleArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!46 = !{!"avifSyncSampleArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!47 = !{!37, !13, i64 4}
!48 = !{!37, !3, i64 36}
!49 = !{!50, !3, i64 0}
!50 = !{!"avifTile", !3, i64 0, !3, i64 4, !3, i64 8}
!51 = !{!28, !13, i64 60}
!52 = !{!53, !13, i64 16}
!53 = !{!"avifCodecDecodeInput", !54, i64 0, !13, i64 16}
!54 = !{!"avifDecodeSampleArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!55 = !{!28, !13, i64 64}
!56 = !{!19, !13, i64 12}
!57 = !{!53, !13, i64 8}
!58 = !{!19, !13, i64 16}
!59 = !{!37, !13, i64 8}
!60 = !{!19, !21, i64 64}
!61 = !{!37, !21, i64 16}
!62 = !{!19, !21, i64 80}
!63 = !{!19, !22, i64 72}
!64 = !{!37, !13, i64 24}
!65 = !{!66, !13, i64 0}
!66 = !{!"avifImage", !13, i64 0, !13, i64 4, !13, i64 8, !4, i64 12, !4, i64 16, !4, i64 20, !4, i64 24, !4, i64 36, !13, i64 48, !4, i64 52, !3, i64 56, !13, i64 60, !13, i64 64, !67, i64 68, !4, i64 76, !4, i64 80, !4, i64 84, !13, i64 88, !68, i64 92, !69, i64 100, !70, i64 132, !71, i64 133, !67, i64 136, !67, i64 144}
!67 = !{!"avifRWData", !3, i64 0, !8, i64 4}
!68 = !{!"avifPixelAspectRatioBox", !13, i64 0, !13, i64 4}
!69 = !{!"avifCleanApertureBox", !13, i64 0, !13, i64 4, !13, i64 8, !13, i64 12, !13, i64 16, !13, i64 20, !13, i64 24, !13, i64 28}
!70 = !{!"avifImageRotation", !4, i64 0}
!71 = !{!"avifImageMirror", !4, i64 0}
!72 = !{!37, !13, i64 28}
!73 = !{!66, !13, i64 4}
!74 = !{!19, !13, i64 88}
!75 = !{!28, !3, i64 16}
!76 = !{!77, !13, i64 8}
!77 = !{!"avifMeta", !78, i64 0, !79, i64 16, !80, i64 32, !13, i64 48, !13, i64 52}
!78 = !{!"avifDecoderItemArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!79 = !{!"avifPropertyArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!80 = !{!"avifDecoderItemDataArray", !3, i64 0, !13, i64 4, !13, i64 8, !13, i64 12}
!81 = !{!77, !3, i64 0}
!82 = !{!83, !13, i64 16}
!83 = !{!"avifDecoderItem", !13, i64 0, !3, i64 4, !4, i64 8, !13, i64 12, !13, i64 16, !13, i64 20, !84, i64 24, !79, i64 88, !13, i64 104, !13, i64 108, !13, i64 112, !13, i64 116, !13, i64 120}
!84 = !{!"avifContentType", !4, i64 0}
!85 = !{!83, !13, i64 120}
!86 = !{!83, !13, i64 104}
!87 = !{!77, !13, i64 52}
!88 = !{!83, !13, i64 0}
!89 = !{!83, !13, i64 108}
!90 = !{!28, !4, i64 68}
!91 = !{!28, !4, i64 69}
!92 = !{!28, !13, i64 52}
!93 = !{!94, !13, i64 8}
!94 = !{!"avifDecodeSample", !7, i64 0, !13, i64 8}
!95 = !{!28, !4, i64 80}
!96 = !{!28, !4, i64 81}
!97 = !{!19, !21, i64 24}
!98 = !{!19, !22, i64 32}
!99 = !{!19, !21, i64 40}
!100 = !{!19, !22, i64 48}
!101 = !{!19, !21, i64 56}
!102 = !{!19, !8, i64 92}
!103 = !{!19, !8, i64 96}
!104 = !{!28, !3, i64 44}
!105 = !{!53, !3, i64 0}
!106 = !{!94, !3, i64 0}
!107 = !{!94, !8, i64 4}
!108 = !{!66, !4, i64 76}
!109 = !{!66, !4, i64 80}
!110 = !{!66, !4, i64 84}
!111 = !{!66, !4, i64 16}
!112 = !{!66, !13, i64 88}
!113 = !{!114, !4, i64 20}
!114 = !{!"avifSequenceHeader", !13, i64 0, !13, i64 4, !13, i64 8, !4, i64 12, !4, i64 16, !4, i64 20, !4, i64 24, !4, i64 28, !4, i64 32}
!115 = !{!114, !4, i64 24}
!116 = !{!114, !4, i64 28}
!117 = !{!114, !4, i64 32}
!118 = !{!66, !13, i64 8}
!119 = !{!66, !4, i64 12}
!120 = !{!66, !4, i64 20}
!121 = !{!28, !3, i64 36}
!122 = !{!28, !8, i64 40}
!123 = !{!40, !3, i64 0}
!124 = !{!125, !21, i64 0}
!125 = !{!"avifSampleTableChunk", !21, i64 0}
!126 = !{!83, !13, i64 20}
!127 = !{!83, !3, i64 4}
!128 = !{!77, !13, i64 40}
!129 = !{!77, !3, i64 32}
!130 = !{!131, !13, i64 0}
!131 = !{!"avifDecoderItemData", !13, i64 0, !7, i64 4}
!132 = !{!83, !13, i64 12}
!133 = !{!21, !21, i64 0}
!134 = !{!50, !3, i64 4}
!135 = !{!50, !3, i64 8}
!136 = !{!40, !13, i64 24}
!137 = !{!40, !3, i64 16}
!138 = !{!83, !13, i64 112}
!139 = !{!40, !13, i64 40}
!140 = !{!40, !3, i64 32}
!141 = !{!142, !13, i64 0}
!142 = !{!"avifSampleTableSampleToChunk", !13, i64 0, !13, i64 4, !13, i64 8}
!143 = !{!142, !13, i64 4}
!144 = !{!40, !13, i64 96}
!145 = !{!40, !13, i64 56}
!146 = !{!40, !3, i64 48}
!147 = !{!148, !13, i64 0}
!148 = !{!"avifSampleTableSampleSize", !13, i64 0}
!149 = !{!40, !13, i64 88}
!150 = !{!40, !3, i64 80}
!151 = !{!152, !13, i64 0}
!152 = !{!"avifSyncSample", !13, i64 0}
!153 = !{!31, !4, i64 0}
!154 = !{!31, !4, i64 1}
!155 = !{!156, !156, i64 0}
!156 = !{!"short", !4, i64 0}
!157 = !{!31, !13, i64 4}
!158 = !{!31, !13, i64 8}
!159 = !{!79, !13, i64 8}
!160 = !{!79, !3, i64 0}
!161 = !{!83, !13, i64 116}
!162 = !{!163, !4, i64 4}
!163 = !{!"avifCodecConfigurationBox", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7, !4, i64 8}
!164 = !{!163, !4, i64 3}
!165 = !{!19, !4, i64 0}
!166 = !{!167, !3, i64 20}
!167 = !{!"avifCodec", !3, i64 0, !163, i64 4, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36}
!168 = !{!167, !3, i64 24}
!169 = !{!66, !4, i64 52}
!170 = !{!66, !3, i64 56}
!171 = !{!66, !13, i64 60}
!172 = !{!173, !13, i64 8}
!173 = !{!"avifPixelFormatInfo", !13, i64 0, !13, i64 4, !13, i64 8}
!174 = !{!173, !13, i64 4}
!175 = !{!20, !21, i64 0}
!176 = !{!20, !21, i64 16}
!177 = !{!20, !21, i64 32}
!178 = !{!20, !22, i64 8}
!179 = !{!20, !22, i64 24}
!180 = !{!40, !13, i64 72}
!181 = !{!40, !3, i64 64}
!182 = !{!183, !13, i64 0}
!183 = !{!"avifSampleTableTimeToSample", !13, i64 0, !13, i64 4}
!184 = !{!183, !13, i64 4}
!185 = !{!77, !13, i64 48}
!186 = !{!131, !3, i64 4}
!187 = !{!131, !8, i64 8}
!188 = !{!77, !13, i64 24}
!189 = !{!77, !3, i64 16}
!190 = !{!191, !13, i64 0}
!191 = !{!"avifColourInformationBox", !13, i64 0, !3, i64 4, !8, i64 8, !13, i64 12, !4, i64 16, !4, i64 20, !4, i64 24, !4, i64 28}
!192 = !{!191, !13, i64 12}
!193 = !{!191, !3, i64 4}
!194 = !{!191, !8, i64 8}
!195 = !{!191, !4, i64 16}
!196 = !{!191, !4, i64 20}
!197 = !{!191, !4, i64 24}
!198 = !{!191, !4, i64 28}
!199 = !{!70, !4, i64 0}
!200 = !{!71, !4, i64 0}
!201 = !{!202, !4, i64 4}
!202 = !{!"avifPixelInformationProperty", !4, i64 0, !4, i64 4}
!203 = !{!163, !4, i64 0}
!204 = !{!163, !4, i64 1}
!205 = !{!163, !4, i64 2}
!206 = !{!163, !4, i64 5}
!207 = !{!163, !4, i64 6}
!208 = !{!163, !4, i64 7}
!209 = !{!163, !4, i64 8}
!210 = !{!167, !3, i64 0}
