; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/reformat.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/reformat.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifImage = type { i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i8*, i32, i32, %struct.avifRWData, i32, i32, i32, i32, %struct.avifPixelAspectRatioBox, %struct.avifCleanApertureBox, %struct.avifImageRotation, %struct.avifImageMirror, %struct.avifRWData, %struct.avifRWData }
%struct.avifPixelAspectRatioBox = type { i32, i32 }
%struct.avifCleanApertureBox = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifImageRotation = type { i8 }
%struct.avifImageMirror = type { i8 }
%struct.avifRWData = type { i8*, i32 }
%struct.avifRGBImage = type { i32, i32, i32, i32, i32, i32, i8*, i32 }
%struct.avifReformatState = type { float, float, float, i32, i32, i32, i32, i32, i32, i32, i32, %struct.avifPixelFormatInfo, [4096 x float], [4096 x float], i32 }
%struct.avifPixelFormatInfo = type { i32, i32, i32 }
%struct.YUVBlock = type { float, float, float }
%struct.avifAlphaParams = type { i32, i32, i32, i32, i8*, i32, i32, i32, i32, i32, i8*, i32, i32, i32 }

; Function Attrs: nounwind
define hidden i32 @avifPrepareReformatState(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %retval = alloca i32, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %cpCount = alloca i32, align 4
  %yuvMaxChannel = alloca float, align 4
  %cp = alloca i32, align 4
  %unormY = alloca i32, align 4
  %unormUV = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 2
  %1 = load i32, i32* %depth, align 4, !tbaa !6
  %cmp = icmp ne i32 %1, 8
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth1 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %2, i32 0, i32 2
  %3 = load i32, i32* %depth1, align 4, !tbaa !6
  %cmp2 = icmp ne i32 %3, 10
  br i1 %cmp2, label %land.lhs.true3, label %if.end

land.lhs.true3:                                   ; preds = %land.lhs.true
  %4 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth4 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %4, i32 0, i32 2
  %5 = load i32, i32* %depth4, align 4, !tbaa !6
  %cmp5 = icmp ne i32 %5, 12
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true3
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true3, %land.lhs.true, %entry
  %6 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth6 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %6, i32 0, i32 2
  %7 = load i32, i32* %depth6, align 4, !tbaa !15
  %cmp7 = icmp ne i32 %7, 8
  br i1 %cmp7, label %land.lhs.true8, label %if.end18

land.lhs.true8:                                   ; preds = %if.end
  %8 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth9 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %8, i32 0, i32 2
  %9 = load i32, i32* %depth9, align 4, !tbaa !15
  %cmp10 = icmp ne i32 %9, 10
  br i1 %cmp10, label %land.lhs.true11, label %if.end18

land.lhs.true11:                                  ; preds = %land.lhs.true8
  %10 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth12 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %10, i32 0, i32 2
  %11 = load i32, i32* %depth12, align 4, !tbaa !15
  %cmp13 = icmp ne i32 %11, 12
  br i1 %cmp13, label %land.lhs.true14, label %if.end18

land.lhs.true14:                                  ; preds = %land.lhs.true11
  %12 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth15 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %12, i32 0, i32 2
  %13 = load i32, i32* %depth15, align 4, !tbaa !15
  %cmp16 = icmp ne i32 %13, 16
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %land.lhs.true14
  store i32 0, i32* %retval, align 4
  br label %return

if.end18:                                         ; preds = %land.lhs.true14, %land.lhs.true11, %land.lhs.true8, %if.end
  %14 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %14, i32 0, i32 3
  %15 = load i32, i32* %yuvFormat, align 4, !tbaa !17
  %cmp19 = icmp eq i32 %15, 0
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end18
  store i32 0, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.end18
  %16 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat22 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %16, i32 0, i32 3
  %17 = load i32, i32* %yuvFormat22, align 4, !tbaa !17
  %18 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %18, i32 0, i32 11
  call void @avifGetPixelFormatInfo(i32 %17, %struct.avifPixelFormatInfo* %formatInfo)
  %19 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %20 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %20, i32 0, i32 0
  %21 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %21, i32 0, i32 1
  %22 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %22, i32 0, i32 2
  call void @avifCalcYUVCoefficients(%struct.avifImage* %19, float* %kr, float* %kg, float* %kb)
  %23 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %23, i32 0, i32 14
  store i32 0, i32* %mode, align 4, !tbaa !18
  %24 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %24, i32 0, i32 16
  %25 = load i32, i32* %matrixCoefficients, align 4, !tbaa !22
  %cmp23 = icmp eq i32 %25, 0
  br i1 %cmp23, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.end21
  %26 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %mode25 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %26, i32 0, i32 14
  store i32 1, i32* %mode25, align 4, !tbaa !18
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %if.end21
  %27 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %mode27 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %27, i32 0, i32 14
  %28 = load i32, i32* %mode27, align 4, !tbaa !18
  %cmp28 = icmp ne i32 %28, 0
  br i1 %cmp28, label %if.then29, label %if.end33

if.then29:                                        ; preds = %if.end26
  %29 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr30 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %29, i32 0, i32 0
  store float 0.000000e+00, float* %kr30, align 4, !tbaa !23
  %30 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg31 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %30, i32 0, i32 1
  store float 0.000000e+00, float* %kg31, align 4, !tbaa !24
  %31 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb32 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %31, i32 0, i32 2
  store float 0.000000e+00, float* %kb32, align 4, !tbaa !25
  br label %if.end33

if.end33:                                         ; preds = %if.then29, %if.end26
  %32 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth34 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 2
  %33 = load i32, i32* %depth34, align 4, !tbaa !6
  %cmp35 = icmp ugt i32 %33, 8
  %34 = zext i1 %cmp35 to i64
  %cond = select i1 %cmp35, i32 2, i32 1
  %35 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %yuvChannelBytes = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %35, i32 0, i32 3
  store i32 %cond, i32* %yuvChannelBytes, align 4, !tbaa !26
  %36 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth36 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %36, i32 0, i32 2
  %37 = load i32, i32* %depth36, align 4, !tbaa !15
  %cmp37 = icmp ugt i32 %37, 8
  %38 = zext i1 %cmp37 to i64
  %cond38 = select i1 %cmp37, i32 2, i32 1
  %39 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %39, i32 0, i32 4
  store i32 %cond38, i32* %rgbChannelBytes, align 4, !tbaa !27
  %40 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %format = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %40, i32 0, i32 3
  %41 = load i32, i32* %format, align 4, !tbaa !28
  %call = call i32 @avifRGBFormatChannelCount(i32 %41)
  %42 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelCount = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %42, i32 0, i32 5
  store i32 %call, i32* %rgbChannelCount, align 4, !tbaa !29
  %43 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes39 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %43, i32 0, i32 4
  %44 = load i32, i32* %rgbChannelBytes39, align 4, !tbaa !27
  %45 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelCount40 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %45, i32 0, i32 5
  %46 = load i32, i32* %rgbChannelCount40, align 4, !tbaa !29
  %mul = mul i32 %44, %46
  %47 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %47, i32 0, i32 6
  store i32 %mul, i32* %rgbPixelBytes, align 4, !tbaa !30
  %48 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %format41 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %48, i32 0, i32 3
  %49 = load i32, i32* %format41, align 4, !tbaa !28
  switch i32 %49, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb48
    i32 2, label %sw.bb61
    i32 3, label %sw.bb74
    i32 4, label %sw.bb85
    i32 5, label %sw.bb98
  ]

sw.bb:                                            ; preds = %if.end33
  %50 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes42 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %50, i32 0, i32 4
  %51 = load i32, i32* %rgbChannelBytes42, align 4, !tbaa !27
  %mul43 = mul i32 %51, 0
  %52 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %52, i32 0, i32 7
  store i32 %mul43, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %53 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes44 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %53, i32 0, i32 4
  %54 = load i32, i32* %rgbChannelBytes44, align 4, !tbaa !27
  %mul45 = mul i32 %54, 1
  %55 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %55, i32 0, i32 8
  store i32 %mul45, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %56 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes46 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %56, i32 0, i32 4
  %57 = load i32, i32* %rgbChannelBytes46, align 4, !tbaa !27
  %mul47 = mul i32 %57, 2
  %58 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %58, i32 0, i32 9
  store i32 %mul47, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %59 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesA = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %59, i32 0, i32 10
  store i32 0, i32* %rgbOffsetBytesA, align 4, !tbaa !34
  br label %sw.epilog

sw.bb48:                                          ; preds = %if.end33
  %60 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes49 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %60, i32 0, i32 4
  %61 = load i32, i32* %rgbChannelBytes49, align 4, !tbaa !27
  %mul50 = mul i32 %61, 0
  %62 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR51 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %62, i32 0, i32 7
  store i32 %mul50, i32* %rgbOffsetBytesR51, align 4, !tbaa !31
  %63 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes52 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %63, i32 0, i32 4
  %64 = load i32, i32* %rgbChannelBytes52, align 4, !tbaa !27
  %mul53 = mul i32 %64, 1
  %65 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG54 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %65, i32 0, i32 8
  store i32 %mul53, i32* %rgbOffsetBytesG54, align 4, !tbaa !32
  %66 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes55 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %66, i32 0, i32 4
  %67 = load i32, i32* %rgbChannelBytes55, align 4, !tbaa !27
  %mul56 = mul i32 %67, 2
  %68 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB57 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %68, i32 0, i32 9
  store i32 %mul56, i32* %rgbOffsetBytesB57, align 4, !tbaa !33
  %69 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes58 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %69, i32 0, i32 4
  %70 = load i32, i32* %rgbChannelBytes58, align 4, !tbaa !27
  %mul59 = mul i32 %70, 3
  %71 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesA60 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %71, i32 0, i32 10
  store i32 %mul59, i32* %rgbOffsetBytesA60, align 4, !tbaa !34
  br label %sw.epilog

sw.bb61:                                          ; preds = %if.end33
  %72 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes62 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %72, i32 0, i32 4
  %73 = load i32, i32* %rgbChannelBytes62, align 4, !tbaa !27
  %mul63 = mul i32 %73, 0
  %74 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesA64 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %74, i32 0, i32 10
  store i32 %mul63, i32* %rgbOffsetBytesA64, align 4, !tbaa !34
  %75 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes65 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %75, i32 0, i32 4
  %76 = load i32, i32* %rgbChannelBytes65, align 4, !tbaa !27
  %mul66 = mul i32 %76, 1
  %77 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR67 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %77, i32 0, i32 7
  store i32 %mul66, i32* %rgbOffsetBytesR67, align 4, !tbaa !31
  %78 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes68 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %78, i32 0, i32 4
  %79 = load i32, i32* %rgbChannelBytes68, align 4, !tbaa !27
  %mul69 = mul i32 %79, 2
  %80 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG70 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %80, i32 0, i32 8
  store i32 %mul69, i32* %rgbOffsetBytesG70, align 4, !tbaa !32
  %81 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes71 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %81, i32 0, i32 4
  %82 = load i32, i32* %rgbChannelBytes71, align 4, !tbaa !27
  %mul72 = mul i32 %82, 3
  %83 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB73 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %83, i32 0, i32 9
  store i32 %mul72, i32* %rgbOffsetBytesB73, align 4, !tbaa !33
  br label %sw.epilog

sw.bb74:                                          ; preds = %if.end33
  %84 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes75 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %84, i32 0, i32 4
  %85 = load i32, i32* %rgbChannelBytes75, align 4, !tbaa !27
  %mul76 = mul i32 %85, 0
  %86 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB77 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %86, i32 0, i32 9
  store i32 %mul76, i32* %rgbOffsetBytesB77, align 4, !tbaa !33
  %87 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes78 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %87, i32 0, i32 4
  %88 = load i32, i32* %rgbChannelBytes78, align 4, !tbaa !27
  %mul79 = mul i32 %88, 1
  %89 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG80 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %89, i32 0, i32 8
  store i32 %mul79, i32* %rgbOffsetBytesG80, align 4, !tbaa !32
  %90 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes81 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %90, i32 0, i32 4
  %91 = load i32, i32* %rgbChannelBytes81, align 4, !tbaa !27
  %mul82 = mul i32 %91, 2
  %92 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR83 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %92, i32 0, i32 7
  store i32 %mul82, i32* %rgbOffsetBytesR83, align 4, !tbaa !31
  %93 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesA84 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %93, i32 0, i32 10
  store i32 0, i32* %rgbOffsetBytesA84, align 4, !tbaa !34
  br label %sw.epilog

sw.bb85:                                          ; preds = %if.end33
  %94 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes86 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %94, i32 0, i32 4
  %95 = load i32, i32* %rgbChannelBytes86, align 4, !tbaa !27
  %mul87 = mul i32 %95, 0
  %96 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB88 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %96, i32 0, i32 9
  store i32 %mul87, i32* %rgbOffsetBytesB88, align 4, !tbaa !33
  %97 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes89 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %97, i32 0, i32 4
  %98 = load i32, i32* %rgbChannelBytes89, align 4, !tbaa !27
  %mul90 = mul i32 %98, 1
  %99 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG91 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %99, i32 0, i32 8
  store i32 %mul90, i32* %rgbOffsetBytesG91, align 4, !tbaa !32
  %100 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes92 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %100, i32 0, i32 4
  %101 = load i32, i32* %rgbChannelBytes92, align 4, !tbaa !27
  %mul93 = mul i32 %101, 2
  %102 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR94 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %102, i32 0, i32 7
  store i32 %mul93, i32* %rgbOffsetBytesR94, align 4, !tbaa !31
  %103 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes95 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %103, i32 0, i32 4
  %104 = load i32, i32* %rgbChannelBytes95, align 4, !tbaa !27
  %mul96 = mul i32 %104, 3
  %105 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesA97 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %105, i32 0, i32 10
  store i32 %mul96, i32* %rgbOffsetBytesA97, align 4, !tbaa !34
  br label %sw.epilog

sw.bb98:                                          ; preds = %if.end33
  %106 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes99 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %106, i32 0, i32 4
  %107 = load i32, i32* %rgbChannelBytes99, align 4, !tbaa !27
  %mul100 = mul i32 %107, 0
  %108 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesA101 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %108, i32 0, i32 10
  store i32 %mul100, i32* %rgbOffsetBytesA101, align 4, !tbaa !34
  %109 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes102 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %109, i32 0, i32 4
  %110 = load i32, i32* %rgbChannelBytes102, align 4, !tbaa !27
  %mul103 = mul i32 %110, 1
  %111 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB104 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %111, i32 0, i32 9
  store i32 %mul103, i32* %rgbOffsetBytesB104, align 4, !tbaa !33
  %112 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes105 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %112, i32 0, i32 4
  %113 = load i32, i32* %rgbChannelBytes105, align 4, !tbaa !27
  %mul106 = mul i32 %113, 2
  %114 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG107 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %114, i32 0, i32 8
  store i32 %mul106, i32* %rgbOffsetBytesG107, align 4, !tbaa !32
  %115 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbChannelBytes108 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %115, i32 0, i32 4
  %116 = load i32, i32* %rgbChannelBytes108, align 4, !tbaa !27
  %mul109 = mul i32 %116, 3
  %117 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR110 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %117, i32 0, i32 7
  store i32 %mul109, i32* %rgbOffsetBytesR110, align 4, !tbaa !31
  br label %sw.epilog

sw.default:                                       ; preds = %if.end33
  store i32 0, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %sw.bb98, %sw.bb85, %sw.bb74, %sw.bb61, %sw.bb48, %sw.bb
  %118 = bitcast i32* %cpCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #3
  %119 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth111 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %119, i32 0, i32 2
  %120 = load i32, i32* %depth111, align 4, !tbaa !6
  %shl = shl i32 1, %120
  store i32 %shl, i32* %cpCount, align 4, !tbaa !35
  %121 = bitcast float* %yuvMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #3
  %122 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth112 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %122, i32 0, i32 2
  %123 = load i32, i32* %depth112, align 4, !tbaa !6
  %shl113 = shl i32 1, %123
  %sub = sub nsw i32 %shl113, 1
  %conv = sitofp i32 %sub to float
  store float %conv, float* %yuvMaxChannel, align 4, !tbaa !36
  %124 = bitcast i32* %cp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #3
  store i32 0, i32* %cp, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.epilog
  %125 = load i32, i32* %cp, align 4, !tbaa !35
  %126 = load i32, i32* %cpCount, align 4, !tbaa !35
  %cmp114 = icmp ult i32 %125, %126
  br i1 %cmp114, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %127 = bitcast i32* %cp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %128 = bitcast i32* %unormY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #3
  %129 = load i32, i32* %cp, align 4, !tbaa !35
  store i32 %129, i32* %unormY, align 4, !tbaa !35
  %130 = bitcast i32* %unormUV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #3
  %131 = load i32, i32* %cp, align 4, !tbaa !35
  store i32 %131, i32* %unormUV, align 4, !tbaa !35
  %132 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %132, i32 0, i32 4
  %133 = load i32, i32* %yuvRange, align 4, !tbaa !37
  %cmp116 = icmp eq i32 %133, 0
  br i1 %cmp116, label %if.then118, label %if.end123

if.then118:                                       ; preds = %for.body
  %134 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth119 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %134, i32 0, i32 2
  %135 = load i32, i32* %depth119, align 4, !tbaa !6
  %136 = load i32, i32* %unormY, align 4, !tbaa !35
  %call120 = call i32 @avifLimitedToFullY(i32 %135, i32 %136)
  store i32 %call120, i32* %unormY, align 4, !tbaa !35
  %137 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth121 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %137, i32 0, i32 2
  %138 = load i32, i32* %depth121, align 4, !tbaa !6
  %139 = load i32, i32* %unormUV, align 4, !tbaa !35
  %call122 = call i32 @avifLimitedToFullUV(i32 %138, i32 %139)
  store i32 %call122, i32* %unormUV, align 4, !tbaa !35
  br label %if.end123

if.end123:                                        ; preds = %if.then118, %for.body
  %140 = load i32, i32* %unormY, align 4, !tbaa !35
  %conv124 = sitofp i32 %140 to float
  %141 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %div = fdiv float %conv124, %141
  %142 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %142, i32 0, i32 12
  %143 = load i32, i32* %cp, align 4, !tbaa !35
  %arrayidx = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY, i32 0, i32 %143
  store float %div, float* %arrayidx, align 4, !tbaa !36
  %144 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %mode125 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %144, i32 0, i32 14
  %145 = load i32, i32* %mode125, align 4, !tbaa !18
  %cmp126 = icmp eq i32 %145, 1
  br i1 %cmp126, label %if.then128, label %if.else

if.then128:                                       ; preds = %if.end123
  %146 = load i32, i32* %unormY, align 4, !tbaa !35
  %conv129 = sitofp i32 %146 to float
  %147 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %div130 = fdiv float %conv129, %147
  %148 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableUV = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %148, i32 0, i32 13
  %149 = load i32, i32* %cp, align 4, !tbaa !35
  %arrayidx131 = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableUV, i32 0, i32 %149
  store float %div130, float* %arrayidx131, align 4, !tbaa !36
  br label %if.end137

if.else:                                          ; preds = %if.end123
  %150 = load i32, i32* %unormUV, align 4, !tbaa !35
  %conv132 = sitofp i32 %150 to float
  %151 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %div133 = fdiv float %conv132, %151
  %sub134 = fsub float %div133, 5.000000e-01
  %152 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableUV135 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %152, i32 0, i32 13
  %153 = load i32, i32* %cp, align 4, !tbaa !35
  %arrayidx136 = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableUV135, i32 0, i32 %153
  store float %sub134, float* %arrayidx136, align 4, !tbaa !36
  br label %if.end137

if.end137:                                        ; preds = %if.else, %if.then128
  %154 = bitcast i32* %unormUV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #3
  %155 = bitcast i32* %unormY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #3
  br label %for.inc

for.inc:                                          ; preds = %if.end137
  %156 = load i32, i32* %cp, align 4, !tbaa !35
  %inc = add i32 %156, 1
  store i32 %inc, i32* %cp, align 4, !tbaa !35
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  store i32 1, i32* %retval, align 4
  %157 = bitcast float* %yuvMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #3
  %158 = bitcast i32* %cpCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #3
  br label %return

return:                                           ; preds = %for.end, %sw.default, %if.then20, %if.then17, %if.then
  %159 = load i32, i32* %retval, align 4
  ret i32 %159
}

declare void @avifGetPixelFormatInfo(i32, %struct.avifPixelFormatInfo*) #1

declare void @avifCalcYUVCoefficients(%struct.avifImage*, float*, float*, float*) #1

declare i32 @avifRGBFormatChannelCount(i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden i32 @avifLimitedToFullY(i32 %depth, i32 %v) #0 {
entry:
  %depth.addr = alloca i32, align 4
  %v.addr = alloca i32, align 4
  store i32 %depth, i32* %depth.addr, align 4, !tbaa !35
  store i32 %v, i32* %v.addr, align 4, !tbaa !35
  %0 = load i32, i32* %depth.addr, align 4, !tbaa !35
  switch i32 %0, label %sw.epilog [
    i32 8, label %sw.bb
    i32 10, label %sw.bb6
    i32 12, label %sw.bb21
    i32 16, label %sw.bb36
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %v.addr, align 4, !tbaa !35
  %sub = sub nsw i32 %1, 16
  %mul = mul nsw i32 %sub, 255
  %add = add nsw i32 %mul, 109
  %div = sdiv i32 %add, 219
  store i32 %div, i32* %v.addr, align 4, !tbaa !35
  %2 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp = icmp slt i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.bb
  br label %cond.end4

cond.false:                                       ; preds = %sw.bb
  %3 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp1 = icmp slt i32 255, %3
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %4 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 255, %cond.true2 ], [ %4, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 0, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond5, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb6:                                           ; preds = %entry
  %5 = load i32, i32* %v.addr, align 4, !tbaa !35
  %sub7 = sub nsw i32 %5, 64
  %mul8 = mul nsw i32 %sub7, 1023
  %add9 = add nsw i32 %mul8, 438
  %div10 = sdiv i32 %add9, 876
  store i32 %div10, i32* %v.addr, align 4, !tbaa !35
  %6 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp11 = icmp slt i32 %6, 0
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %sw.bb6
  br label %cond.end19

cond.false13:                                     ; preds = %sw.bb6
  %7 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp14 = icmp slt i32 1023, %7
  br i1 %cmp14, label %cond.true15, label %cond.false16

cond.true15:                                      ; preds = %cond.false13
  br label %cond.end17

cond.false16:                                     ; preds = %cond.false13
  %8 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true15
  %cond18 = phi i32 [ 1023, %cond.true15 ], [ %8, %cond.false16 ]
  br label %cond.end19

cond.end19:                                       ; preds = %cond.end17, %cond.true12
  %cond20 = phi i32 [ 0, %cond.true12 ], [ %cond18, %cond.end17 ]
  store i32 %cond20, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb21:                                          ; preds = %entry
  %9 = load i32, i32* %v.addr, align 4, !tbaa !35
  %sub22 = sub nsw i32 %9, 256
  %mul23 = mul nsw i32 %sub22, 4095
  %add24 = add nsw i32 %mul23, 1752
  %div25 = sdiv i32 %add24, 3504
  store i32 %div25, i32* %v.addr, align 4, !tbaa !35
  %10 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp26 = icmp slt i32 %10, 0
  br i1 %cmp26, label %cond.true27, label %cond.false28

cond.true27:                                      ; preds = %sw.bb21
  br label %cond.end34

cond.false28:                                     ; preds = %sw.bb21
  %11 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp29 = icmp slt i32 4095, %11
  br i1 %cmp29, label %cond.true30, label %cond.false31

cond.true30:                                      ; preds = %cond.false28
  br label %cond.end32

cond.false31:                                     ; preds = %cond.false28
  %12 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end32

cond.end32:                                       ; preds = %cond.false31, %cond.true30
  %cond33 = phi i32 [ 4095, %cond.true30 ], [ %12, %cond.false31 ]
  br label %cond.end34

cond.end34:                                       ; preds = %cond.end32, %cond.true27
  %cond35 = phi i32 [ 0, %cond.true27 ], [ %cond33, %cond.end32 ]
  store i32 %cond35, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb36:                                          ; preds = %entry
  %13 = load i32, i32* %v.addr, align 4, !tbaa !35
  %sub37 = sub nsw i32 %13, 1024
  %mul38 = mul nsw i32 %sub37, 65535
  %add39 = add nsw i32 %mul38, 29568
  %div40 = sdiv i32 %add39, 59136
  store i32 %div40, i32* %v.addr, align 4, !tbaa !35
  %14 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp41 = icmp slt i32 %14, 0
  br i1 %cmp41, label %cond.true42, label %cond.false43

cond.true42:                                      ; preds = %sw.bb36
  br label %cond.end49

cond.false43:                                     ; preds = %sw.bb36
  %15 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp44 = icmp slt i32 65535, %15
  br i1 %cmp44, label %cond.true45, label %cond.false46

cond.true45:                                      ; preds = %cond.false43
  br label %cond.end47

cond.false46:                                     ; preds = %cond.false43
  %16 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end47

cond.end47:                                       ; preds = %cond.false46, %cond.true45
  %cond48 = phi i32 [ 65535, %cond.true45 ], [ %16, %cond.false46 ]
  br label %cond.end49

cond.end49:                                       ; preds = %cond.end47, %cond.true42
  %cond50 = phi i32 [ 0, %cond.true42 ], [ %cond48, %cond.end47 ]
  store i32 %cond50, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %cond.end49, %cond.end34, %cond.end19, %cond.end4
  %17 = load i32, i32* %v.addr, align 4, !tbaa !35
  ret i32 %17
}

; Function Attrs: nounwind
define hidden i32 @avifLimitedToFullUV(i32 %depth, i32 %v) #0 {
entry:
  %depth.addr = alloca i32, align 4
  %v.addr = alloca i32, align 4
  store i32 %depth, i32* %depth.addr, align 4, !tbaa !35
  store i32 %v, i32* %v.addr, align 4, !tbaa !35
  %0 = load i32, i32* %depth.addr, align 4, !tbaa !35
  switch i32 %0, label %sw.epilog [
    i32 8, label %sw.bb
    i32 10, label %sw.bb6
    i32 12, label %sw.bb21
    i32 16, label %sw.bb36
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %v.addr, align 4, !tbaa !35
  %sub = sub nsw i32 %1, 16
  %mul = mul nsw i32 %sub, 255
  %add = add nsw i32 %mul, 112
  %div = sdiv i32 %add, 224
  store i32 %div, i32* %v.addr, align 4, !tbaa !35
  %2 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp = icmp slt i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.bb
  br label %cond.end4

cond.false:                                       ; preds = %sw.bb
  %3 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp1 = icmp slt i32 255, %3
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %4 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ 255, %cond.true2 ], [ %4, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ 0, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond5, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb6:                                           ; preds = %entry
  %5 = load i32, i32* %v.addr, align 4, !tbaa !35
  %sub7 = sub nsw i32 %5, 64
  %mul8 = mul nsw i32 %sub7, 1023
  %add9 = add nsw i32 %mul8, 448
  %div10 = sdiv i32 %add9, 896
  store i32 %div10, i32* %v.addr, align 4, !tbaa !35
  %6 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp11 = icmp slt i32 %6, 0
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %sw.bb6
  br label %cond.end19

cond.false13:                                     ; preds = %sw.bb6
  %7 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp14 = icmp slt i32 1023, %7
  br i1 %cmp14, label %cond.true15, label %cond.false16

cond.true15:                                      ; preds = %cond.false13
  br label %cond.end17

cond.false16:                                     ; preds = %cond.false13
  %8 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true15
  %cond18 = phi i32 [ 1023, %cond.true15 ], [ %8, %cond.false16 ]
  br label %cond.end19

cond.end19:                                       ; preds = %cond.end17, %cond.true12
  %cond20 = phi i32 [ 0, %cond.true12 ], [ %cond18, %cond.end17 ]
  store i32 %cond20, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb21:                                          ; preds = %entry
  %9 = load i32, i32* %v.addr, align 4, !tbaa !35
  %sub22 = sub nsw i32 %9, 256
  %mul23 = mul nsw i32 %sub22, 4095
  %add24 = add nsw i32 %mul23, 1792
  %div25 = sdiv i32 %add24, 3584
  store i32 %div25, i32* %v.addr, align 4, !tbaa !35
  %10 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp26 = icmp slt i32 %10, 0
  br i1 %cmp26, label %cond.true27, label %cond.false28

cond.true27:                                      ; preds = %sw.bb21
  br label %cond.end34

cond.false28:                                     ; preds = %sw.bb21
  %11 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp29 = icmp slt i32 4095, %11
  br i1 %cmp29, label %cond.true30, label %cond.false31

cond.true30:                                      ; preds = %cond.false28
  br label %cond.end32

cond.false31:                                     ; preds = %cond.false28
  %12 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end32

cond.end32:                                       ; preds = %cond.false31, %cond.true30
  %cond33 = phi i32 [ 4095, %cond.true30 ], [ %12, %cond.false31 ]
  br label %cond.end34

cond.end34:                                       ; preds = %cond.end32, %cond.true27
  %cond35 = phi i32 [ 0, %cond.true27 ], [ %cond33, %cond.end32 ]
  store i32 %cond35, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb36:                                          ; preds = %entry
  %13 = load i32, i32* %v.addr, align 4, !tbaa !35
  %sub37 = sub nsw i32 %13, 1024
  %mul38 = mul nsw i32 %sub37, 65535
  %add39 = add nsw i32 %mul38, 30208
  %div40 = sdiv i32 %add39, 60416
  store i32 %div40, i32* %v.addr, align 4, !tbaa !35
  %14 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp41 = icmp slt i32 %14, 0
  br i1 %cmp41, label %cond.true42, label %cond.false43

cond.true42:                                      ; preds = %sw.bb36
  br label %cond.end49

cond.false43:                                     ; preds = %sw.bb36
  %15 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp44 = icmp slt i32 65535, %15
  br i1 %cmp44, label %cond.true45, label %cond.false46

cond.true45:                                      ; preds = %cond.false43
  br label %cond.end47

cond.false46:                                     ; preds = %cond.false43
  %16 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end47

cond.end47:                                       ; preds = %cond.false46, %cond.true45
  %cond48 = phi i32 [ 65535, %cond.true45 ], [ %16, %cond.false46 ]
  br label %cond.end49

cond.end49:                                       ; preds = %cond.end47, %cond.true42
  %cond50 = phi i32 [ 0, %cond.true42 ], [ %cond48, %cond.end47 ]
  store i32 %cond50, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %cond.end49, %cond.end34, %cond.end19, %cond.end4
  %17 = load i32, i32* %v.addr, align 4, !tbaa !35
  ret i32 %17
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden i32 @avifImageRGBToYUV(%struct.avifImage* %image, %struct.avifRGBImage* %rgb) #0 {
entry:
  %retval = alloca i32, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state = alloca %struct.avifReformatState, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %yuvBlock = alloca [2 x [2 x %struct.YUVBlock]], align 16
  %rgbPixel = alloca [3 x float], align 4
  %yuvMaxChannel = alloca float, align 4
  %rgbMaxChannel = alloca float, align 4
  %yuvPlanes = alloca i8**, align 4
  %yuvRowBytes = alloca i32*, align 4
  %outerJ = alloca i32, align 4
  %outerI = alloca i32, align 4
  %blockW = alloca i32, align 4
  %blockH = alloca i32, align 4
  %bJ = alloca i32, align 4
  %bI = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %Y = alloca float, align 4
  %pY = alloca i16*, align 4
  %pU = alloca i16*, align 4
  %pV = alloca i16*, align 4
  %sumU = alloca float, align 4
  %sumV = alloca float, align 4
  %bJ264 = alloca i32, align 4
  %bI270 = alloca i32, align 4
  %totalSamples = alloca float, align 4
  %avgU = alloca float, align 4
  %avgV = alloca float, align 4
  %chromaShiftX = alloca i32, align 4
  %chromaShiftY = alloca i32, align 4
  %uvI = alloca i32, align 4
  %uvJ = alloca i32, align 4
  %pU299 = alloca i16*, align 4
  %pV311 = alloca i16*, align 4
  %bJ350 = alloca i32, align 4
  %sumU356 = alloca float, align 4
  %sumV357 = alloca float, align 4
  %bI358 = alloca i32, align 4
  %totalSamples375 = alloca float, align 4
  %avgU377 = alloca float, align 4
  %avgV379 = alloca float, align 4
  %chromaShiftX381 = alloca i32, align 4
  %uvI382 = alloca i32, align 4
  %uvJ384 = alloca i32, align 4
  %pU390 = alloca i16*, align 4
  %pV402 = alloca i16*, align 4
  %params = alloca %struct.avifAlphaParams, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %0 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %0, i32 0, i32 6
  %1 = load i8*, i8** %pixels, align 4, !tbaa !38
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %struct.avifReformatState* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 32828, i8* %2) #3
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %4 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call = call i32 @avifPrepareReformatState(%struct.avifImage* %3, %struct.avifRGBImage* %4, %struct.avifReformatState* %state)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %5 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  call void @avifImageAllocatePlanes(%struct.avifImage* %5, i32 1)
  %6 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %format = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %6, i32 0, i32 3
  %7 = load i32, i32* %format, align 4, !tbaa !28
  %call4 = call i32 @avifRGBFormatHasAlpha(i32 %7)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %land.lhs.true, label %if.end8

land.lhs.true:                                    ; preds = %if.end3
  %8 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %ignoreAlpha = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %8, i32 0, i32 5
  %9 = load i32, i32* %ignoreAlpha, align 4, !tbaa !39
  %tobool6 = icmp ne i32 %9, 0
  br i1 %tobool6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %land.lhs.true
  %10 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  call void @avifImageAllocatePlanes(%struct.avifImage* %10, i32 2)
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %land.lhs.true, %if.end3
  %11 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %kr9 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 0
  %12 = load float, float* %kr9, align 4, !tbaa !23
  store float %12, float* %kr, align 4, !tbaa !36
  %13 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %kg10 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 1
  %14 = load float, float* %kg10, align 4, !tbaa !24
  store float %14, float* %kg, align 4, !tbaa !36
  %15 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %kb11 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 2
  %16 = load float, float* %kb11, align 4, !tbaa !25
  store float %16, float* %kb, align 4, !tbaa !36
  %17 = bitcast [2 x [2 x %struct.YUVBlock]]* %yuvBlock to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %17) #3
  %18 = bitcast [3 x float]* %rgbPixel to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %18) #3
  %19 = bitcast float* %yuvMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %20, i32 0, i32 2
  %21 = load i32, i32* %depth, align 4, !tbaa !6
  %shl = shl i32 1, %21
  %sub = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub to float
  store float %conv, float* %yuvMaxChannel, align 4, !tbaa !36
  %22 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth12 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %23, i32 0, i32 2
  %24 = load i32, i32* %depth12, align 4, !tbaa !15
  %shl13 = shl i32 1, %24
  %sub14 = sub nsw i32 %shl13, 1
  %conv15 = sitofp i32 %sub14 to float
  store float %conv15, float* %rgbMaxChannel, align 4, !tbaa !36
  %25 = bitcast i8*** %yuvPlanes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes16 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 6
  %arraydecay = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes16, i32 0, i32 0
  store i8** %arraydecay, i8*** %yuvPlanes, align 4, !tbaa !2
  %27 = bitcast i32** %yuvRowBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes17 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %28, i32 0, i32 7
  %arraydecay18 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes17, i32 0, i32 0
  store i32* %arraydecay18, i32** %yuvRowBytes, align 4, !tbaa !2
  %29 = bitcast i32* %outerJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #3
  store i32 0, i32* %outerJ, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc444, %if.end8
  %30 = load i32, i32* %outerJ, align 4, !tbaa !35
  %31 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %31, i32 0, i32 1
  %32 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %30, %32
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %outerJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  br label %for.end446

for.body:                                         ; preds = %for.cond
  %34 = bitcast i32* %outerI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #3
  store i32 0, i32* %outerI, align 4, !tbaa !35
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc441, %for.body
  %35 = load i32, i32* %outerI, align 4, !tbaa !35
  %36 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %36, i32 0, i32 0
  %37 = load i32, i32* %width, align 4, !tbaa !41
  %cmp21 = icmp ult i32 %35, %37
  br i1 %cmp21, label %for.body24, label %for.cond.cleanup23

for.cond.cleanup23:                               ; preds = %for.cond20
  store i32 5, i32* %cleanup.dest.slot, align 4
  %38 = bitcast i32* %outerI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  br label %for.end443

for.body24:                                       ; preds = %for.cond20
  %39 = bitcast i32* %blockW to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #3
  store i32 2, i32* %blockW, align 4, !tbaa !35
  %40 = bitcast i32* %blockH to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  store i32 2, i32* %blockH, align 4, !tbaa !35
  %41 = load i32, i32* %outerI, align 4, !tbaa !35
  %add = add i32 %41, 1
  %42 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width25 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %42, i32 0, i32 0
  %43 = load i32, i32* %width25, align 4, !tbaa !41
  %cmp26 = icmp uge i32 %add, %43
  br i1 %cmp26, label %if.then28, label %if.end29

if.then28:                                        ; preds = %for.body24
  store i32 1, i32* %blockW, align 4, !tbaa !35
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %for.body24
  %44 = load i32, i32* %outerJ, align 4, !tbaa !35
  %add30 = add i32 %44, 1
  %45 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height31 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %45, i32 0, i32 1
  %46 = load i32, i32* %height31, align 4, !tbaa !40
  %cmp32 = icmp uge i32 %add30, %46
  br i1 %cmp32, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end29
  store i32 1, i32* %blockH, align 4, !tbaa !35
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end29
  %47 = bitcast i32* %bJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #3
  store i32 0, i32* %bJ, align 4, !tbaa !35
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc257, %if.end35
  %48 = load i32, i32* %bJ, align 4, !tbaa !35
  %49 = load i32, i32* %blockH, align 4, !tbaa !35
  %cmp37 = icmp slt i32 %48, %49
  br i1 %cmp37, label %for.body40, label %for.cond.cleanup39

for.cond.cleanup39:                               ; preds = %for.cond36
  store i32 8, i32* %cleanup.dest.slot, align 4
  %50 = bitcast i32* %bJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  br label %for.end259

for.body40:                                       ; preds = %for.cond36
  %51 = bitcast i32* %bI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #3
  store i32 0, i32* %bI, align 4, !tbaa !35
  br label %for.cond41

for.cond41:                                       ; preds = %for.inc, %for.body40
  %52 = load i32, i32* %bI, align 4, !tbaa !35
  %53 = load i32, i32* %blockW, align 4, !tbaa !35
  %cmp42 = icmp slt i32 %52, %53
  br i1 %cmp42, label %for.body45, label %for.cond.cleanup44

for.cond.cleanup44:                               ; preds = %for.cond41
  store i32 11, i32* %cleanup.dest.slot, align 4
  %54 = bitcast i32* %bI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  br label %for.end

for.body45:                                       ; preds = %for.cond41
  %55 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #3
  %56 = load i32, i32* %outerI, align 4, !tbaa !35
  %57 = load i32, i32* %bI, align 4, !tbaa !35
  %add46 = add i32 %56, %57
  store i32 %add46, i32* %i, align 4, !tbaa !35
  %58 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #3
  %59 = load i32, i32* %outerJ, align 4, !tbaa !35
  %60 = load i32, i32* %bJ, align 4, !tbaa !35
  %add47 = add i32 %59, %60
  store i32 %add47, i32* %j, align 4, !tbaa !35
  %rgbChannelBytes = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 4
  %61 = load i32, i32* %rgbChannelBytes, align 4, !tbaa !27
  %cmp48 = icmp ugt i32 %61, 1
  br i1 %cmp48, label %if.then50, label %if.else

if.then50:                                        ; preds = %for.body45
  %62 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels51 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %62, i32 0, i32 6
  %63 = load i8*, i8** %pixels51, align 4, !tbaa !38
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 7
  %64 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %65 = load i32, i32* %i, align 4, !tbaa !35
  %rgbPixelBytes = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 6
  %66 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !30
  %mul = mul i32 %65, %66
  %add52 = add i32 %64, %mul
  %67 = load i32, i32* %j, align 4, !tbaa !35
  %68 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %68, i32 0, i32 7
  %69 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul53 = mul i32 %67, %69
  %add54 = add i32 %add52, %mul53
  %arrayidx = getelementptr inbounds i8, i8* %63, i32 %add54
  %70 = bitcast i8* %arrayidx to i16*
  %71 = load i16, i16* %70, align 2, !tbaa !43
  %conv55 = zext i16 %71 to i32
  %conv56 = sitofp i32 %conv55 to float
  %72 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %div = fdiv float %conv56, %72
  %arrayidx57 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 0
  store float %div, float* %arrayidx57, align 4, !tbaa !36
  %73 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels58 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %73, i32 0, i32 6
  %74 = load i8*, i8** %pixels58, align 4, !tbaa !38
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 8
  %75 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %76 = load i32, i32* %i, align 4, !tbaa !35
  %rgbPixelBytes59 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 6
  %77 = load i32, i32* %rgbPixelBytes59, align 4, !tbaa !30
  %mul60 = mul i32 %76, %77
  %add61 = add i32 %75, %mul60
  %78 = load i32, i32* %j, align 4, !tbaa !35
  %79 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes62 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %79, i32 0, i32 7
  %80 = load i32, i32* %rowBytes62, align 4, !tbaa !42
  %mul63 = mul i32 %78, %80
  %add64 = add i32 %add61, %mul63
  %arrayidx65 = getelementptr inbounds i8, i8* %74, i32 %add64
  %81 = bitcast i8* %arrayidx65 to i16*
  %82 = load i16, i16* %81, align 2, !tbaa !43
  %conv66 = zext i16 %82 to i32
  %conv67 = sitofp i32 %conv66 to float
  %83 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %div68 = fdiv float %conv67, %83
  %arrayidx69 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 1
  store float %div68, float* %arrayidx69, align 4, !tbaa !36
  %84 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels70 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %84, i32 0, i32 6
  %85 = load i8*, i8** %pixels70, align 4, !tbaa !38
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 9
  %86 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %87 = load i32, i32* %i, align 4, !tbaa !35
  %rgbPixelBytes71 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 6
  %88 = load i32, i32* %rgbPixelBytes71, align 4, !tbaa !30
  %mul72 = mul i32 %87, %88
  %add73 = add i32 %86, %mul72
  %89 = load i32, i32* %j, align 4, !tbaa !35
  %90 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes74 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %90, i32 0, i32 7
  %91 = load i32, i32* %rowBytes74, align 4, !tbaa !42
  %mul75 = mul i32 %89, %91
  %add76 = add i32 %add73, %mul75
  %arrayidx77 = getelementptr inbounds i8, i8* %85, i32 %add76
  %92 = bitcast i8* %arrayidx77 to i16*
  %93 = load i16, i16* %92, align 2, !tbaa !43
  %conv78 = zext i16 %93 to i32
  %conv79 = sitofp i32 %conv78 to float
  %94 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %div80 = fdiv float %conv79, %94
  %arrayidx81 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 2
  store float %div80, float* %arrayidx81, align 4, !tbaa !36
  br label %if.end121

if.else:                                          ; preds = %for.body45
  %95 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels82 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %95, i32 0, i32 6
  %96 = load i8*, i8** %pixels82, align 4, !tbaa !38
  %rgbOffsetBytesR83 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 7
  %97 = load i32, i32* %rgbOffsetBytesR83, align 4, !tbaa !31
  %98 = load i32, i32* %i, align 4, !tbaa !35
  %rgbPixelBytes84 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 6
  %99 = load i32, i32* %rgbPixelBytes84, align 4, !tbaa !30
  %mul85 = mul i32 %98, %99
  %add86 = add i32 %97, %mul85
  %100 = load i32, i32* %j, align 4, !tbaa !35
  %101 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes87 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %101, i32 0, i32 7
  %102 = load i32, i32* %rowBytes87, align 4, !tbaa !42
  %mul88 = mul i32 %100, %102
  %add89 = add i32 %add86, %mul88
  %arrayidx90 = getelementptr inbounds i8, i8* %96, i32 %add89
  %103 = load i8, i8* %arrayidx90, align 1, !tbaa !45
  %conv91 = zext i8 %103 to i32
  %conv92 = sitofp i32 %conv91 to float
  %104 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %div93 = fdiv float %conv92, %104
  %arrayidx94 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 0
  store float %div93, float* %arrayidx94, align 4, !tbaa !36
  %105 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels95 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %105, i32 0, i32 6
  %106 = load i8*, i8** %pixels95, align 4, !tbaa !38
  %rgbOffsetBytesG96 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 8
  %107 = load i32, i32* %rgbOffsetBytesG96, align 4, !tbaa !32
  %108 = load i32, i32* %i, align 4, !tbaa !35
  %rgbPixelBytes97 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 6
  %109 = load i32, i32* %rgbPixelBytes97, align 4, !tbaa !30
  %mul98 = mul i32 %108, %109
  %add99 = add i32 %107, %mul98
  %110 = load i32, i32* %j, align 4, !tbaa !35
  %111 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes100 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %111, i32 0, i32 7
  %112 = load i32, i32* %rowBytes100, align 4, !tbaa !42
  %mul101 = mul i32 %110, %112
  %add102 = add i32 %add99, %mul101
  %arrayidx103 = getelementptr inbounds i8, i8* %106, i32 %add102
  %113 = load i8, i8* %arrayidx103, align 1, !tbaa !45
  %conv104 = zext i8 %113 to i32
  %conv105 = sitofp i32 %conv104 to float
  %114 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %div106 = fdiv float %conv105, %114
  %arrayidx107 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 1
  store float %div106, float* %arrayidx107, align 4, !tbaa !36
  %115 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels108 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %115, i32 0, i32 6
  %116 = load i8*, i8** %pixels108, align 4, !tbaa !38
  %rgbOffsetBytesB109 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 9
  %117 = load i32, i32* %rgbOffsetBytesB109, align 4, !tbaa !33
  %118 = load i32, i32* %i, align 4, !tbaa !35
  %rgbPixelBytes110 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 6
  %119 = load i32, i32* %rgbPixelBytes110, align 4, !tbaa !30
  %mul111 = mul i32 %118, %119
  %add112 = add i32 %117, %mul111
  %120 = load i32, i32* %j, align 4, !tbaa !35
  %121 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes113 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %121, i32 0, i32 7
  %122 = load i32, i32* %rowBytes113, align 4, !tbaa !42
  %mul114 = mul i32 %120, %122
  %add115 = add i32 %add112, %mul114
  %arrayidx116 = getelementptr inbounds i8, i8* %116, i32 %add115
  %123 = load i8, i8* %arrayidx116, align 1, !tbaa !45
  %conv117 = zext i8 %123 to i32
  %conv118 = sitofp i32 %conv117 to float
  %124 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %div119 = fdiv float %conv118, %124
  %arrayidx120 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 2
  store float %div119, float* %arrayidx120, align 4, !tbaa !36
  br label %if.end121

if.end121:                                        ; preds = %if.else, %if.then50
  %mode = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %125 = load i32, i32* %mode, align 4, !tbaa !18
  %cmp122 = icmp eq i32 %125, 1
  br i1 %cmp122, label %if.then124, label %if.else134

if.then124:                                       ; preds = %if.end121
  %arrayidx125 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 1
  %126 = load float, float* %arrayidx125, align 4, !tbaa !36
  %127 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx126 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %127
  %128 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx127 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx126, i32 0, i32 %128
  %y = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx127, i32 0, i32 0
  store float %126, float* %y, align 4, !tbaa !46
  %arrayidx128 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 2
  %129 = load float, float* %arrayidx128, align 4, !tbaa !36
  %130 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx129 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %130
  %131 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx130 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx129, i32 0, i32 %131
  %u = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx130, i32 0, i32 1
  store float %129, float* %u, align 4, !tbaa !48
  %arrayidx131 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 0
  %132 = load float, float* %arrayidx131, align 4, !tbaa !36
  %133 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx132 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %133
  %134 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx133 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx132, i32 0, i32 %134
  %v = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx133, i32 0, i32 2
  store float %132, float* %v, align 4, !tbaa !49
  br label %if.end162

if.else134:                                       ; preds = %if.end121
  %135 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #3
  %136 = load float, float* %kr, align 4, !tbaa !36
  %arrayidx135 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 0
  %137 = load float, float* %arrayidx135, align 4, !tbaa !36
  %mul136 = fmul float %136, %137
  %138 = load float, float* %kg, align 4, !tbaa !36
  %arrayidx137 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 1
  %139 = load float, float* %arrayidx137, align 4, !tbaa !36
  %mul138 = fmul float %138, %139
  %add139 = fadd float %mul136, %mul138
  %140 = load float, float* %kb, align 4, !tbaa !36
  %arrayidx140 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 2
  %141 = load float, float* %arrayidx140, align 4, !tbaa !36
  %mul141 = fmul float %140, %141
  %add142 = fadd float %add139, %mul141
  store float %add142, float* %Y, align 4, !tbaa !36
  %142 = load float, float* %Y, align 4, !tbaa !36
  %143 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx143 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %143
  %144 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx144 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx143, i32 0, i32 %144
  %y145 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx144, i32 0, i32 0
  store float %142, float* %y145, align 4, !tbaa !46
  %arrayidx146 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 2
  %145 = load float, float* %arrayidx146, align 4, !tbaa !36
  %146 = load float, float* %Y, align 4, !tbaa !36
  %sub147 = fsub float %145, %146
  %147 = load float, float* %kb, align 4, !tbaa !36
  %sub148 = fsub float 1.000000e+00, %147
  %mul149 = fmul float 2.000000e+00, %sub148
  %div150 = fdiv float %sub147, %mul149
  %148 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx151 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %148
  %149 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx152 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx151, i32 0, i32 %149
  %u153 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx152, i32 0, i32 1
  store float %div150, float* %u153, align 4, !tbaa !48
  %arrayidx154 = getelementptr inbounds [3 x float], [3 x float]* %rgbPixel, i32 0, i32 0
  %150 = load float, float* %arrayidx154, align 4, !tbaa !36
  %151 = load float, float* %Y, align 4, !tbaa !36
  %sub155 = fsub float %150, %151
  %152 = load float, float* %kr, align 4, !tbaa !36
  %sub156 = fsub float 1.000000e+00, %152
  %mul157 = fmul float 2.000000e+00, %sub156
  %div158 = fdiv float %sub155, %mul157
  %153 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx159 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %153
  %154 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx160 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx159, i32 0, i32 %154
  %v161 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx160, i32 0, i32 2
  store float %div158, float* %v161, align 4, !tbaa !49
  %155 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #3
  br label %if.end162

if.end162:                                        ; preds = %if.else134, %if.then124
  %yuvChannelBytes = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 3
  %156 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !26
  %cmp163 = icmp ugt i32 %156, 1
  br i1 %cmp163, label %if.then165, label %if.else211

if.then165:                                       ; preds = %if.end162
  %157 = bitcast i16** %pY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #3
  %158 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i8*, i8** %158, i32 0
  %159 = load i8*, i8** %arrayidx166, align 4, !tbaa !2
  %160 = load i32, i32* %i, align 4, !tbaa !35
  %mul167 = mul nsw i32 %160, 2
  %161 = load i32, i32* %j, align 4, !tbaa !35
  %162 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx168 = getelementptr inbounds i32, i32* %162, i32 0
  %163 = load i32, i32* %arrayidx168, align 4, !tbaa !35
  %mul169 = mul i32 %161, %163
  %add170 = add i32 %mul167, %mul169
  %arrayidx171 = getelementptr inbounds i8, i8* %159, i32 %add170
  %164 = bitcast i8* %arrayidx171 to i16*
  store i16* %164, i16** %pY, align 4, !tbaa !2
  %165 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %165, i32 0, i32 4
  %166 = load i32, i32* %yuvRange, align 4, !tbaa !37
  %167 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth172 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %167, i32 0, i32 2
  %168 = load i32, i32* %depth172, align 4, !tbaa !6
  %169 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %170 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx173 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %170
  %171 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx174 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx173, i32 0, i32 %171
  %y175 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx174, i32 0, i32 0
  %172 = load float, float* %y175, align 4, !tbaa !46
  %mode176 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %173 = load i32, i32* %mode176, align 4, !tbaa !18
  %call177 = call i32 @yuvToUNorm(i32 0, i32 %166, i32 %168, float %169, float %172, i32 %173)
  %conv178 = trunc i32 %call177 to i16
  %174 = load i16*, i16** %pY, align 4, !tbaa !2
  store i16 %conv178, i16* %174, align 2, !tbaa !43
  %175 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %175, i32 0, i32 3
  %176 = load i32, i32* %yuvFormat, align 4, !tbaa !17
  %cmp179 = icmp eq i32 %176, 1
  br i1 %cmp179, label %if.then181, label %if.end210

if.then181:                                       ; preds = %if.then165
  %177 = bitcast i16** %pU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %177) #3
  %178 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx182 = getelementptr inbounds i8*, i8** %178, i32 1
  %179 = load i8*, i8** %arrayidx182, align 4, !tbaa !2
  %180 = load i32, i32* %i, align 4, !tbaa !35
  %mul183 = mul nsw i32 %180, 2
  %181 = load i32, i32* %j, align 4, !tbaa !35
  %182 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx184 = getelementptr inbounds i32, i32* %182, i32 1
  %183 = load i32, i32* %arrayidx184, align 4, !tbaa !35
  %mul185 = mul i32 %181, %183
  %add186 = add i32 %mul183, %mul185
  %arrayidx187 = getelementptr inbounds i8, i8* %179, i32 %add186
  %184 = bitcast i8* %arrayidx187 to i16*
  store i16* %184, i16** %pU, align 4, !tbaa !2
  %185 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange188 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %185, i32 0, i32 4
  %186 = load i32, i32* %yuvRange188, align 4, !tbaa !37
  %187 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth189 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %187, i32 0, i32 2
  %188 = load i32, i32* %depth189, align 4, !tbaa !6
  %189 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %190 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx190 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %190
  %191 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx191 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx190, i32 0, i32 %191
  %u192 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx191, i32 0, i32 1
  %192 = load float, float* %u192, align 4, !tbaa !48
  %mode193 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %193 = load i32, i32* %mode193, align 4, !tbaa !18
  %call194 = call i32 @yuvToUNorm(i32 1, i32 %186, i32 %188, float %189, float %192, i32 %193)
  %conv195 = trunc i32 %call194 to i16
  %194 = load i16*, i16** %pU, align 4, !tbaa !2
  store i16 %conv195, i16* %194, align 2, !tbaa !43
  %195 = bitcast i16** %pV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %195) #3
  %196 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx196 = getelementptr inbounds i8*, i8** %196, i32 2
  %197 = load i8*, i8** %arrayidx196, align 4, !tbaa !2
  %198 = load i32, i32* %i, align 4, !tbaa !35
  %mul197 = mul nsw i32 %198, 2
  %199 = load i32, i32* %j, align 4, !tbaa !35
  %200 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx198 = getelementptr inbounds i32, i32* %200, i32 2
  %201 = load i32, i32* %arrayidx198, align 4, !tbaa !35
  %mul199 = mul i32 %199, %201
  %add200 = add i32 %mul197, %mul199
  %arrayidx201 = getelementptr inbounds i8, i8* %197, i32 %add200
  %202 = bitcast i8* %arrayidx201 to i16*
  store i16* %202, i16** %pV, align 4, !tbaa !2
  %203 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange202 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %203, i32 0, i32 4
  %204 = load i32, i32* %yuvRange202, align 4, !tbaa !37
  %205 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth203 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %205, i32 0, i32 2
  %206 = load i32, i32* %depth203, align 4, !tbaa !6
  %207 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %208 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx204 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %208
  %209 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx205 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx204, i32 0, i32 %209
  %v206 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx205, i32 0, i32 2
  %210 = load float, float* %v206, align 4, !tbaa !49
  %mode207 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %211 = load i32, i32* %mode207, align 4, !tbaa !18
  %call208 = call i32 @yuvToUNorm(i32 2, i32 %204, i32 %206, float %207, float %210, i32 %211)
  %conv209 = trunc i32 %call208 to i16
  %212 = load i16*, i16** %pV, align 4, !tbaa !2
  store i16 %conv209, i16* %212, align 2, !tbaa !43
  %213 = bitcast i16** %pV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #3
  %214 = bitcast i16** %pU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #3
  br label %if.end210

if.end210:                                        ; preds = %if.then181, %if.then165
  %215 = bitcast i16** %pY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #3
  br label %if.end256

if.else211:                                       ; preds = %if.end162
  %216 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange212 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %216, i32 0, i32 4
  %217 = load i32, i32* %yuvRange212, align 4, !tbaa !37
  %218 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth213 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %218, i32 0, i32 2
  %219 = load i32, i32* %depth213, align 4, !tbaa !6
  %220 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %221 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx214 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %221
  %222 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx215 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx214, i32 0, i32 %222
  %y216 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx215, i32 0, i32 0
  %223 = load float, float* %y216, align 4, !tbaa !46
  %mode217 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %224 = load i32, i32* %mode217, align 4, !tbaa !18
  %call218 = call i32 @yuvToUNorm(i32 0, i32 %217, i32 %219, float %220, float %223, i32 %224)
  %conv219 = trunc i32 %call218 to i8
  %225 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx220 = getelementptr inbounds i8*, i8** %225, i32 0
  %226 = load i8*, i8** %arrayidx220, align 4, !tbaa !2
  %227 = load i32, i32* %i, align 4, !tbaa !35
  %228 = load i32, i32* %j, align 4, !tbaa !35
  %229 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx221 = getelementptr inbounds i32, i32* %229, i32 0
  %230 = load i32, i32* %arrayidx221, align 4, !tbaa !35
  %mul222 = mul i32 %228, %230
  %add223 = add i32 %227, %mul222
  %arrayidx224 = getelementptr inbounds i8, i8* %226, i32 %add223
  store i8 %conv219, i8* %arrayidx224, align 1, !tbaa !45
  %231 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat225 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %231, i32 0, i32 3
  %232 = load i32, i32* %yuvFormat225, align 4, !tbaa !17
  %cmp226 = icmp eq i32 %232, 1
  br i1 %cmp226, label %if.then228, label %if.end255

if.then228:                                       ; preds = %if.else211
  %233 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange229 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %233, i32 0, i32 4
  %234 = load i32, i32* %yuvRange229, align 4, !tbaa !37
  %235 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth230 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %235, i32 0, i32 2
  %236 = load i32, i32* %depth230, align 4, !tbaa !6
  %237 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %238 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx231 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %238
  %239 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx232 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx231, i32 0, i32 %239
  %u233 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx232, i32 0, i32 1
  %240 = load float, float* %u233, align 4, !tbaa !48
  %mode234 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %241 = load i32, i32* %mode234, align 4, !tbaa !18
  %call235 = call i32 @yuvToUNorm(i32 1, i32 %234, i32 %236, float %237, float %240, i32 %241)
  %conv236 = trunc i32 %call235 to i8
  %242 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx237 = getelementptr inbounds i8*, i8** %242, i32 1
  %243 = load i8*, i8** %arrayidx237, align 4, !tbaa !2
  %244 = load i32, i32* %i, align 4, !tbaa !35
  %245 = load i32, i32* %j, align 4, !tbaa !35
  %246 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx238 = getelementptr inbounds i32, i32* %246, i32 1
  %247 = load i32, i32* %arrayidx238, align 4, !tbaa !35
  %mul239 = mul i32 %245, %247
  %add240 = add i32 %244, %mul239
  %arrayidx241 = getelementptr inbounds i8, i8* %243, i32 %add240
  store i8 %conv236, i8* %arrayidx241, align 1, !tbaa !45
  %248 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange242 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %248, i32 0, i32 4
  %249 = load i32, i32* %yuvRange242, align 4, !tbaa !37
  %250 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth243 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %250, i32 0, i32 2
  %251 = load i32, i32* %depth243, align 4, !tbaa !6
  %252 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %253 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx244 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %253
  %254 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx245 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx244, i32 0, i32 %254
  %v246 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx245, i32 0, i32 2
  %255 = load float, float* %v246, align 4, !tbaa !49
  %mode247 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %256 = load i32, i32* %mode247, align 4, !tbaa !18
  %call248 = call i32 @yuvToUNorm(i32 2, i32 %249, i32 %251, float %252, float %255, i32 %256)
  %conv249 = trunc i32 %call248 to i8
  %257 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx250 = getelementptr inbounds i8*, i8** %257, i32 2
  %258 = load i8*, i8** %arrayidx250, align 4, !tbaa !2
  %259 = load i32, i32* %i, align 4, !tbaa !35
  %260 = load i32, i32* %j, align 4, !tbaa !35
  %261 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx251 = getelementptr inbounds i32, i32* %261, i32 2
  %262 = load i32, i32* %arrayidx251, align 4, !tbaa !35
  %mul252 = mul i32 %260, %262
  %add253 = add i32 %259, %mul252
  %arrayidx254 = getelementptr inbounds i8, i8* %258, i32 %add253
  store i8 %conv249, i8* %arrayidx254, align 1, !tbaa !45
  br label %if.end255

if.end255:                                        ; preds = %if.then228, %if.else211
  br label %if.end256

if.end256:                                        ; preds = %if.end255, %if.end210
  %263 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #3
  %264 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #3
  br label %for.inc

for.inc:                                          ; preds = %if.end256
  %265 = load i32, i32* %bI, align 4, !tbaa !35
  %inc = add nsw i32 %265, 1
  store i32 %inc, i32* %bI, align 4, !tbaa !35
  br label %for.cond41

for.end:                                          ; preds = %for.cond.cleanup44
  br label %for.inc257

for.inc257:                                       ; preds = %for.end
  %266 = load i32, i32* %bJ, align 4, !tbaa !35
  %inc258 = add nsw i32 %266, 1
  store i32 %inc258, i32* %bJ, align 4, !tbaa !35
  br label %for.cond36

for.end259:                                       ; preds = %for.cond.cleanup39
  %267 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat260 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %267, i32 0, i32 3
  %268 = load i32, i32* %yuvFormat260, align 4, !tbaa !17
  %cmp261 = icmp eq i32 %268, 3
  br i1 %cmp261, label %if.then263, label %if.else345

if.then263:                                       ; preds = %for.end259
  %269 = bitcast float* %sumU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %269) #3
  store float 0.000000e+00, float* %sumU, align 4, !tbaa !36
  %270 = bitcast float* %sumV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %270) #3
  store float 0.000000e+00, float* %sumV, align 4, !tbaa !36
  %271 = bitcast i32* %bJ264 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %271) #3
  store i32 0, i32* %bJ264, align 4, !tbaa !35
  br label %for.cond265

for.cond265:                                      ; preds = %for.inc287, %if.then263
  %272 = load i32, i32* %bJ264, align 4, !tbaa !35
  %273 = load i32, i32* %blockH, align 4, !tbaa !35
  %cmp266 = icmp slt i32 %272, %273
  br i1 %cmp266, label %for.body269, label %for.cond.cleanup268

for.cond.cleanup268:                              ; preds = %for.cond265
  store i32 14, i32* %cleanup.dest.slot, align 4
  %274 = bitcast i32* %bJ264 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #3
  br label %for.end289

for.body269:                                      ; preds = %for.cond265
  %275 = bitcast i32* %bI270 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %275) #3
  store i32 0, i32* %bI270, align 4, !tbaa !35
  br label %for.cond271

for.cond271:                                      ; preds = %for.inc284, %for.body269
  %276 = load i32, i32* %bI270, align 4, !tbaa !35
  %277 = load i32, i32* %blockW, align 4, !tbaa !35
  %cmp272 = icmp slt i32 %276, %277
  br i1 %cmp272, label %for.body275, label %for.cond.cleanup274

for.cond.cleanup274:                              ; preds = %for.cond271
  store i32 17, i32* %cleanup.dest.slot, align 4
  %278 = bitcast i32* %bI270 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #3
  br label %for.end286

for.body275:                                      ; preds = %for.cond271
  %279 = load i32, i32* %bI270, align 4, !tbaa !35
  %arrayidx276 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %279
  %280 = load i32, i32* %bJ264, align 4, !tbaa !35
  %arrayidx277 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx276, i32 0, i32 %280
  %u278 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx277, i32 0, i32 1
  %281 = load float, float* %u278, align 4, !tbaa !48
  %282 = load float, float* %sumU, align 4, !tbaa !36
  %add279 = fadd float %282, %281
  store float %add279, float* %sumU, align 4, !tbaa !36
  %283 = load i32, i32* %bI270, align 4, !tbaa !35
  %arrayidx280 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %283
  %284 = load i32, i32* %bJ264, align 4, !tbaa !35
  %arrayidx281 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx280, i32 0, i32 %284
  %v282 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx281, i32 0, i32 2
  %285 = load float, float* %v282, align 4, !tbaa !49
  %286 = load float, float* %sumV, align 4, !tbaa !36
  %add283 = fadd float %286, %285
  store float %add283, float* %sumV, align 4, !tbaa !36
  br label %for.inc284

for.inc284:                                       ; preds = %for.body275
  %287 = load i32, i32* %bI270, align 4, !tbaa !35
  %inc285 = add nsw i32 %287, 1
  store i32 %inc285, i32* %bI270, align 4, !tbaa !35
  br label %for.cond271

for.end286:                                       ; preds = %for.cond.cleanup274
  br label %for.inc287

for.inc287:                                       ; preds = %for.end286
  %288 = load i32, i32* %bJ264, align 4, !tbaa !35
  %inc288 = add nsw i32 %288, 1
  store i32 %inc288, i32* %bJ264, align 4, !tbaa !35
  br label %for.cond265

for.end289:                                       ; preds = %for.cond.cleanup268
  %289 = bitcast float* %totalSamples to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %289) #3
  %290 = load i32, i32* %blockW, align 4, !tbaa !35
  %291 = load i32, i32* %blockH, align 4, !tbaa !35
  %mul290 = mul nsw i32 %290, %291
  %conv291 = sitofp i32 %mul290 to float
  store float %conv291, float* %totalSamples, align 4, !tbaa !36
  %292 = bitcast float* %avgU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %292) #3
  %293 = load float, float* %sumU, align 4, !tbaa !36
  %294 = load float, float* %totalSamples, align 4, !tbaa !36
  %div292 = fdiv float %293, %294
  store float %div292, float* %avgU, align 4, !tbaa !36
  %295 = bitcast float* %avgV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %295) #3
  %296 = load float, float* %sumV, align 4, !tbaa !36
  %297 = load float, float* %totalSamples, align 4, !tbaa !36
  %div293 = fdiv float %296, %297
  store float %div293, float* %avgV, align 4, !tbaa !36
  %298 = bitcast i32* %chromaShiftX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %298) #3
  store i32 1, i32* %chromaShiftX, align 4, !tbaa !35
  %299 = bitcast i32* %chromaShiftY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %299) #3
  store i32 1, i32* %chromaShiftY, align 4, !tbaa !35
  %300 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %300) #3
  %301 = load i32, i32* %outerI, align 4, !tbaa !35
  %shr = lshr i32 %301, 1
  store i32 %shr, i32* %uvI, align 4, !tbaa !35
  %302 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %302) #3
  %303 = load i32, i32* %outerJ, align 4, !tbaa !35
  %shr294 = lshr i32 %303, 1
  store i32 %shr294, i32* %uvJ, align 4, !tbaa !35
  %yuvChannelBytes295 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 3
  %304 = load i32, i32* %yuvChannelBytes295, align 4, !tbaa !26
  %cmp296 = icmp ugt i32 %304, 1
  br i1 %cmp296, label %if.then298, label %if.else323

if.then298:                                       ; preds = %for.end289
  %305 = bitcast i16** %pU299 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %305) #3
  %306 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx300 = getelementptr inbounds i8*, i8** %306, i32 1
  %307 = load i8*, i8** %arrayidx300, align 4, !tbaa !2
  %308 = load i32, i32* %uvI, align 4, !tbaa !35
  %mul301 = mul nsw i32 %308, 2
  %309 = load i32, i32* %uvJ, align 4, !tbaa !35
  %310 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx302 = getelementptr inbounds i32, i32* %310, i32 1
  %311 = load i32, i32* %arrayidx302, align 4, !tbaa !35
  %mul303 = mul i32 %309, %311
  %add304 = add i32 %mul301, %mul303
  %arrayidx305 = getelementptr inbounds i8, i8* %307, i32 %add304
  %312 = bitcast i8* %arrayidx305 to i16*
  store i16* %312, i16** %pU299, align 4, !tbaa !2
  %313 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange306 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %313, i32 0, i32 4
  %314 = load i32, i32* %yuvRange306, align 4, !tbaa !37
  %315 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth307 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %315, i32 0, i32 2
  %316 = load i32, i32* %depth307, align 4, !tbaa !6
  %317 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %318 = load float, float* %avgU, align 4, !tbaa !36
  %mode308 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %319 = load i32, i32* %mode308, align 4, !tbaa !18
  %call309 = call i32 @yuvToUNorm(i32 1, i32 %314, i32 %316, float %317, float %318, i32 %319)
  %conv310 = trunc i32 %call309 to i16
  %320 = load i16*, i16** %pU299, align 4, !tbaa !2
  store i16 %conv310, i16* %320, align 2, !tbaa !43
  %321 = bitcast i16** %pV311 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %321) #3
  %322 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx312 = getelementptr inbounds i8*, i8** %322, i32 2
  %323 = load i8*, i8** %arrayidx312, align 4, !tbaa !2
  %324 = load i32, i32* %uvI, align 4, !tbaa !35
  %mul313 = mul nsw i32 %324, 2
  %325 = load i32, i32* %uvJ, align 4, !tbaa !35
  %326 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx314 = getelementptr inbounds i32, i32* %326, i32 2
  %327 = load i32, i32* %arrayidx314, align 4, !tbaa !35
  %mul315 = mul i32 %325, %327
  %add316 = add i32 %mul313, %mul315
  %arrayidx317 = getelementptr inbounds i8, i8* %323, i32 %add316
  %328 = bitcast i8* %arrayidx317 to i16*
  store i16* %328, i16** %pV311, align 4, !tbaa !2
  %329 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange318 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %329, i32 0, i32 4
  %330 = load i32, i32* %yuvRange318, align 4, !tbaa !37
  %331 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth319 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %331, i32 0, i32 2
  %332 = load i32, i32* %depth319, align 4, !tbaa !6
  %333 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %334 = load float, float* %avgV, align 4, !tbaa !36
  %mode320 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %335 = load i32, i32* %mode320, align 4, !tbaa !18
  %call321 = call i32 @yuvToUNorm(i32 2, i32 %330, i32 %332, float %333, float %334, i32 %335)
  %conv322 = trunc i32 %call321 to i16
  %336 = load i16*, i16** %pV311, align 4, !tbaa !2
  store i16 %conv322, i16* %336, align 2, !tbaa !43
  %337 = bitcast i16** %pV311 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #3
  %338 = bitcast i16** %pU299 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #3
  br label %if.end344

if.else323:                                       ; preds = %for.end289
  %339 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange324 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %339, i32 0, i32 4
  %340 = load i32, i32* %yuvRange324, align 4, !tbaa !37
  %341 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth325 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %341, i32 0, i32 2
  %342 = load i32, i32* %depth325, align 4, !tbaa !6
  %343 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %344 = load float, float* %avgU, align 4, !tbaa !36
  %mode326 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %345 = load i32, i32* %mode326, align 4, !tbaa !18
  %call327 = call i32 @yuvToUNorm(i32 1, i32 %340, i32 %342, float %343, float %344, i32 %345)
  %conv328 = trunc i32 %call327 to i8
  %346 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx329 = getelementptr inbounds i8*, i8** %346, i32 1
  %347 = load i8*, i8** %arrayidx329, align 4, !tbaa !2
  %348 = load i32, i32* %uvI, align 4, !tbaa !35
  %349 = load i32, i32* %uvJ, align 4, !tbaa !35
  %350 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx330 = getelementptr inbounds i32, i32* %350, i32 1
  %351 = load i32, i32* %arrayidx330, align 4, !tbaa !35
  %mul331 = mul i32 %349, %351
  %add332 = add i32 %348, %mul331
  %arrayidx333 = getelementptr inbounds i8, i8* %347, i32 %add332
  store i8 %conv328, i8* %arrayidx333, align 1, !tbaa !45
  %352 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange334 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %352, i32 0, i32 4
  %353 = load i32, i32* %yuvRange334, align 4, !tbaa !37
  %354 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth335 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %354, i32 0, i32 2
  %355 = load i32, i32* %depth335, align 4, !tbaa !6
  %356 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %357 = load float, float* %avgV, align 4, !tbaa !36
  %mode336 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %358 = load i32, i32* %mode336, align 4, !tbaa !18
  %call337 = call i32 @yuvToUNorm(i32 2, i32 %353, i32 %355, float %356, float %357, i32 %358)
  %conv338 = trunc i32 %call337 to i8
  %359 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx339 = getelementptr inbounds i8*, i8** %359, i32 2
  %360 = load i8*, i8** %arrayidx339, align 4, !tbaa !2
  %361 = load i32, i32* %uvI, align 4, !tbaa !35
  %362 = load i32, i32* %uvJ, align 4, !tbaa !35
  %363 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx340 = getelementptr inbounds i32, i32* %363, i32 2
  %364 = load i32, i32* %arrayidx340, align 4, !tbaa !35
  %mul341 = mul i32 %362, %364
  %add342 = add i32 %361, %mul341
  %arrayidx343 = getelementptr inbounds i8, i8* %360, i32 %add342
  store i8 %conv338, i8* %arrayidx343, align 1, !tbaa !45
  br label %if.end344

if.end344:                                        ; preds = %if.else323, %if.then298
  %365 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %365) #3
  %366 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %366) #3
  %367 = bitcast i32* %chromaShiftY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %367) #3
  %368 = bitcast i32* %chromaShiftX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %368) #3
  %369 = bitcast float* %avgV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %369) #3
  %370 = bitcast float* %avgU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %370) #3
  %371 = bitcast float* %totalSamples to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %371) #3
  %372 = bitcast float* %sumV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %372) #3
  %373 = bitcast float* %sumU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %373) #3
  br label %if.end440

if.else345:                                       ; preds = %for.end259
  %374 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat346 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %374, i32 0, i32 3
  %375 = load i32, i32* %yuvFormat346, align 4, !tbaa !17
  %cmp347 = icmp eq i32 %375, 2
  br i1 %cmp347, label %if.then349, label %if.end439

if.then349:                                       ; preds = %if.else345
  %376 = bitcast i32* %bJ350 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %376) #3
  store i32 0, i32* %bJ350, align 4, !tbaa !35
  br label %for.cond351

for.cond351:                                      ; preds = %for.inc436, %if.then349
  %377 = load i32, i32* %bJ350, align 4, !tbaa !35
  %378 = load i32, i32* %blockH, align 4, !tbaa !35
  %cmp352 = icmp slt i32 %377, %378
  br i1 %cmp352, label %for.body355, label %for.cond.cleanup354

for.cond.cleanup354:                              ; preds = %for.cond351
  store i32 20, i32* %cleanup.dest.slot, align 4
  %379 = bitcast i32* %bJ350 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %379) #3
  br label %for.end438

for.body355:                                      ; preds = %for.cond351
  %380 = bitcast float* %sumU356 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %380) #3
  store float 0.000000e+00, float* %sumU356, align 4, !tbaa !36
  %381 = bitcast float* %sumV357 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %381) #3
  store float 0.000000e+00, float* %sumV357, align 4, !tbaa !36
  %382 = bitcast i32* %bI358 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %382) #3
  store i32 0, i32* %bI358, align 4, !tbaa !35
  br label %for.cond359

for.cond359:                                      ; preds = %for.inc372, %for.body355
  %383 = load i32, i32* %bI358, align 4, !tbaa !35
  %384 = load i32, i32* %blockW, align 4, !tbaa !35
  %cmp360 = icmp slt i32 %383, %384
  br i1 %cmp360, label %for.body363, label %for.cond.cleanup362

for.cond.cleanup362:                              ; preds = %for.cond359
  store i32 23, i32* %cleanup.dest.slot, align 4
  %385 = bitcast i32* %bI358 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %385) #3
  br label %for.end374

for.body363:                                      ; preds = %for.cond359
  %386 = load i32, i32* %bI358, align 4, !tbaa !35
  %arrayidx364 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %386
  %387 = load i32, i32* %bJ350, align 4, !tbaa !35
  %arrayidx365 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx364, i32 0, i32 %387
  %u366 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx365, i32 0, i32 1
  %388 = load float, float* %u366, align 4, !tbaa !48
  %389 = load float, float* %sumU356, align 4, !tbaa !36
  %add367 = fadd float %389, %388
  store float %add367, float* %sumU356, align 4, !tbaa !36
  %390 = load i32, i32* %bI358, align 4, !tbaa !35
  %arrayidx368 = getelementptr inbounds [2 x [2 x %struct.YUVBlock]], [2 x [2 x %struct.YUVBlock]]* %yuvBlock, i32 0, i32 %390
  %391 = load i32, i32* %bJ350, align 4, !tbaa !35
  %arrayidx369 = getelementptr inbounds [2 x %struct.YUVBlock], [2 x %struct.YUVBlock]* %arrayidx368, i32 0, i32 %391
  %v370 = getelementptr inbounds %struct.YUVBlock, %struct.YUVBlock* %arrayidx369, i32 0, i32 2
  %392 = load float, float* %v370, align 4, !tbaa !49
  %393 = load float, float* %sumV357, align 4, !tbaa !36
  %add371 = fadd float %393, %392
  store float %add371, float* %sumV357, align 4, !tbaa !36
  br label %for.inc372

for.inc372:                                       ; preds = %for.body363
  %394 = load i32, i32* %bI358, align 4, !tbaa !35
  %inc373 = add nsw i32 %394, 1
  store i32 %inc373, i32* %bI358, align 4, !tbaa !35
  br label %for.cond359

for.end374:                                       ; preds = %for.cond.cleanup362
  %395 = bitcast float* %totalSamples375 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %395) #3
  %396 = load i32, i32* %blockW, align 4, !tbaa !35
  %conv376 = sitofp i32 %396 to float
  store float %conv376, float* %totalSamples375, align 4, !tbaa !36
  %397 = bitcast float* %avgU377 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %397) #3
  %398 = load float, float* %sumU356, align 4, !tbaa !36
  %399 = load float, float* %totalSamples375, align 4, !tbaa !36
  %div378 = fdiv float %398, %399
  store float %div378, float* %avgU377, align 4, !tbaa !36
  %400 = bitcast float* %avgV379 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %400) #3
  %401 = load float, float* %sumV357, align 4, !tbaa !36
  %402 = load float, float* %totalSamples375, align 4, !tbaa !36
  %div380 = fdiv float %401, %402
  store float %div380, float* %avgV379, align 4, !tbaa !36
  %403 = bitcast i32* %chromaShiftX381 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %403) #3
  store i32 1, i32* %chromaShiftX381, align 4, !tbaa !35
  %404 = bitcast i32* %uvI382 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %404) #3
  %405 = load i32, i32* %outerI, align 4, !tbaa !35
  %shr383 = lshr i32 %405, 1
  store i32 %shr383, i32* %uvI382, align 4, !tbaa !35
  %406 = bitcast i32* %uvJ384 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %406) #3
  %407 = load i32, i32* %outerJ, align 4, !tbaa !35
  %408 = load i32, i32* %bJ350, align 4, !tbaa !35
  %add385 = add i32 %407, %408
  store i32 %add385, i32* %uvJ384, align 4, !tbaa !35
  %yuvChannelBytes386 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 3
  %409 = load i32, i32* %yuvChannelBytes386, align 4, !tbaa !26
  %cmp387 = icmp ugt i32 %409, 1
  br i1 %cmp387, label %if.then389, label %if.else414

if.then389:                                       ; preds = %for.end374
  %410 = bitcast i16** %pU390 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %410) #3
  %411 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx391 = getelementptr inbounds i8*, i8** %411, i32 1
  %412 = load i8*, i8** %arrayidx391, align 4, !tbaa !2
  %413 = load i32, i32* %uvI382, align 4, !tbaa !35
  %mul392 = mul nsw i32 %413, 2
  %414 = load i32, i32* %uvJ384, align 4, !tbaa !35
  %415 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx393 = getelementptr inbounds i32, i32* %415, i32 1
  %416 = load i32, i32* %arrayidx393, align 4, !tbaa !35
  %mul394 = mul i32 %414, %416
  %add395 = add i32 %mul392, %mul394
  %arrayidx396 = getelementptr inbounds i8, i8* %412, i32 %add395
  %417 = bitcast i8* %arrayidx396 to i16*
  store i16* %417, i16** %pU390, align 4, !tbaa !2
  %418 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange397 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %418, i32 0, i32 4
  %419 = load i32, i32* %yuvRange397, align 4, !tbaa !37
  %420 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth398 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %420, i32 0, i32 2
  %421 = load i32, i32* %depth398, align 4, !tbaa !6
  %422 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %423 = load float, float* %avgU377, align 4, !tbaa !36
  %mode399 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %424 = load i32, i32* %mode399, align 4, !tbaa !18
  %call400 = call i32 @yuvToUNorm(i32 1, i32 %419, i32 %421, float %422, float %423, i32 %424)
  %conv401 = trunc i32 %call400 to i16
  %425 = load i16*, i16** %pU390, align 4, !tbaa !2
  store i16 %conv401, i16* %425, align 2, !tbaa !43
  %426 = bitcast i16** %pV402 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %426) #3
  %427 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx403 = getelementptr inbounds i8*, i8** %427, i32 2
  %428 = load i8*, i8** %arrayidx403, align 4, !tbaa !2
  %429 = load i32, i32* %uvI382, align 4, !tbaa !35
  %mul404 = mul nsw i32 %429, 2
  %430 = load i32, i32* %uvJ384, align 4, !tbaa !35
  %431 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx405 = getelementptr inbounds i32, i32* %431, i32 2
  %432 = load i32, i32* %arrayidx405, align 4, !tbaa !35
  %mul406 = mul i32 %430, %432
  %add407 = add i32 %mul404, %mul406
  %arrayidx408 = getelementptr inbounds i8, i8* %428, i32 %add407
  %433 = bitcast i8* %arrayidx408 to i16*
  store i16* %433, i16** %pV402, align 4, !tbaa !2
  %434 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange409 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %434, i32 0, i32 4
  %435 = load i32, i32* %yuvRange409, align 4, !tbaa !37
  %436 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth410 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %436, i32 0, i32 2
  %437 = load i32, i32* %depth410, align 4, !tbaa !6
  %438 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %439 = load float, float* %avgV379, align 4, !tbaa !36
  %mode411 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %440 = load i32, i32* %mode411, align 4, !tbaa !18
  %call412 = call i32 @yuvToUNorm(i32 2, i32 %435, i32 %437, float %438, float %439, i32 %440)
  %conv413 = trunc i32 %call412 to i16
  %441 = load i16*, i16** %pV402, align 4, !tbaa !2
  store i16 %conv413, i16* %441, align 2, !tbaa !43
  %442 = bitcast i16** %pV402 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %442) #3
  %443 = bitcast i16** %pU390 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %443) #3
  br label %if.end435

if.else414:                                       ; preds = %for.end374
  %444 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange415 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %444, i32 0, i32 4
  %445 = load i32, i32* %yuvRange415, align 4, !tbaa !37
  %446 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth416 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %446, i32 0, i32 2
  %447 = load i32, i32* %depth416, align 4, !tbaa !6
  %448 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %449 = load float, float* %avgU377, align 4, !tbaa !36
  %mode417 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %450 = load i32, i32* %mode417, align 4, !tbaa !18
  %call418 = call i32 @yuvToUNorm(i32 1, i32 %445, i32 %447, float %448, float %449, i32 %450)
  %conv419 = trunc i32 %call418 to i8
  %451 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx420 = getelementptr inbounds i8*, i8** %451, i32 1
  %452 = load i8*, i8** %arrayidx420, align 4, !tbaa !2
  %453 = load i32, i32* %uvI382, align 4, !tbaa !35
  %454 = load i32, i32* %uvJ384, align 4, !tbaa !35
  %455 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx421 = getelementptr inbounds i32, i32* %455, i32 1
  %456 = load i32, i32* %arrayidx421, align 4, !tbaa !35
  %mul422 = mul i32 %454, %456
  %add423 = add i32 %453, %mul422
  %arrayidx424 = getelementptr inbounds i8, i8* %452, i32 %add423
  store i8 %conv419, i8* %arrayidx424, align 1, !tbaa !45
  %457 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange425 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %457, i32 0, i32 4
  %458 = load i32, i32* %yuvRange425, align 4, !tbaa !37
  %459 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth426 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %459, i32 0, i32 2
  %460 = load i32, i32* %depth426, align 4, !tbaa !6
  %461 = load float, float* %yuvMaxChannel, align 4, !tbaa !36
  %462 = load float, float* %avgV379, align 4, !tbaa !36
  %mode427 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %463 = load i32, i32* %mode427, align 4, !tbaa !18
  %call428 = call i32 @yuvToUNorm(i32 2, i32 %458, i32 %460, float %461, float %462, i32 %463)
  %conv429 = trunc i32 %call428 to i8
  %464 = load i8**, i8*** %yuvPlanes, align 4, !tbaa !2
  %arrayidx430 = getelementptr inbounds i8*, i8** %464, i32 2
  %465 = load i8*, i8** %arrayidx430, align 4, !tbaa !2
  %466 = load i32, i32* %uvI382, align 4, !tbaa !35
  %467 = load i32, i32* %uvJ384, align 4, !tbaa !35
  %468 = load i32*, i32** %yuvRowBytes, align 4, !tbaa !2
  %arrayidx431 = getelementptr inbounds i32, i32* %468, i32 2
  %469 = load i32, i32* %arrayidx431, align 4, !tbaa !35
  %mul432 = mul i32 %467, %469
  %add433 = add i32 %466, %mul432
  %arrayidx434 = getelementptr inbounds i8, i8* %465, i32 %add433
  store i8 %conv429, i8* %arrayidx434, align 1, !tbaa !45
  br label %if.end435

if.end435:                                        ; preds = %if.else414, %if.then389
  %470 = bitcast i32* %uvJ384 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %470) #3
  %471 = bitcast i32* %uvI382 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %471) #3
  %472 = bitcast i32* %chromaShiftX381 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %472) #3
  %473 = bitcast float* %avgV379 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %473) #3
  %474 = bitcast float* %avgU377 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %474) #3
  %475 = bitcast float* %totalSamples375 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %475) #3
  %476 = bitcast float* %sumV357 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %476) #3
  %477 = bitcast float* %sumU356 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %477) #3
  br label %for.inc436

for.inc436:                                       ; preds = %if.end435
  %478 = load i32, i32* %bJ350, align 4, !tbaa !35
  %inc437 = add nsw i32 %478, 1
  store i32 %inc437, i32* %bJ350, align 4, !tbaa !35
  br label %for.cond351

for.end438:                                       ; preds = %for.cond.cleanup354
  br label %if.end439

if.end439:                                        ; preds = %for.end438, %if.else345
  br label %if.end440

if.end440:                                        ; preds = %if.end439, %if.end344
  %479 = bitcast i32* %blockH to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %479) #3
  %480 = bitcast i32* %blockW to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %480) #3
  br label %for.inc441

for.inc441:                                       ; preds = %if.end440
  %481 = load i32, i32* %outerI, align 4, !tbaa !35
  %add442 = add i32 %481, 2
  store i32 %add442, i32* %outerI, align 4, !tbaa !35
  br label %for.cond20

for.end443:                                       ; preds = %for.cond.cleanup23
  br label %for.inc444

for.inc444:                                       ; preds = %for.end443
  %482 = load i32, i32* %outerJ, align 4, !tbaa !35
  %add445 = add i32 %482, 2
  store i32 %add445, i32* %outerJ, align 4, !tbaa !35
  br label %for.cond

for.end446:                                       ; preds = %for.cond.cleanup
  %483 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %483, i32 0, i32 10
  %484 = load i8*, i8** %alphaPlane, align 4, !tbaa !50
  %tobool447 = icmp ne i8* %484, null
  br i1 %tobool447, label %land.lhs.true448, label %if.end474

land.lhs.true448:                                 ; preds = %for.end446
  %485 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %485, i32 0, i32 11
  %486 = load i32, i32* %alphaRowBytes, align 4, !tbaa !51
  %tobool449 = icmp ne i32 %486, 0
  br i1 %tobool449, label %if.then450, label %if.end474

if.then450:                                       ; preds = %land.lhs.true448
  %487 = bitcast %struct.avifAlphaParams* %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %487) #3
  %488 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width451 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %488, i32 0, i32 0
  %489 = load i32, i32* %width451, align 4, !tbaa !41
  %width452 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 0
  store i32 %489, i32* %width452, align 4, !tbaa !52
  %490 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height453 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %490, i32 0, i32 1
  %491 = load i32, i32* %height453, align 4, !tbaa !40
  %height454 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 1
  store i32 %491, i32* %height454, align 4, !tbaa !54
  %492 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth455 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %492, i32 0, i32 2
  %493 = load i32, i32* %depth455, align 4, !tbaa !6
  %dstDepth = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 8
  store i32 %493, i32* %dstDepth, align 4, !tbaa !55
  %494 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %494, i32 0, i32 9
  %495 = load i32, i32* %alphaRange, align 4, !tbaa !56
  %dstRange = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 9
  store i32 %495, i32* %dstRange, align 4, !tbaa !57
  %496 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane456 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %496, i32 0, i32 10
  %497 = load i8*, i8** %alphaPlane456, align 4, !tbaa !50
  %dstPlane = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 10
  store i8* %497, i8** %dstPlane, align 4, !tbaa !58
  %498 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes457 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %498, i32 0, i32 11
  %499 = load i32, i32* %alphaRowBytes457, align 4, !tbaa !51
  %dstRowBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 11
  store i32 %499, i32* %dstRowBytes, align 4, !tbaa !59
  %dstOffsetBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 12
  store i32 0, i32* %dstOffsetBytes, align 4, !tbaa !60
  %yuvChannelBytes458 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 3
  %500 = load i32, i32* %yuvChannelBytes458, align 4, !tbaa !26
  %dstPixelBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 13
  store i32 %500, i32* %dstPixelBytes, align 4, !tbaa !61
  %501 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %format459 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %501, i32 0, i32 3
  %502 = load i32, i32* %format459, align 4, !tbaa !28
  %call460 = call i32 @avifRGBFormatHasAlpha(i32 %502)
  %tobool461 = icmp ne i32 %call460, 0
  br i1 %tobool461, label %land.lhs.true462, label %if.else471

land.lhs.true462:                                 ; preds = %if.then450
  %503 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %ignoreAlpha463 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %503, i32 0, i32 5
  %504 = load i32, i32* %ignoreAlpha463, align 4, !tbaa !39
  %tobool464 = icmp ne i32 %504, 0
  br i1 %tobool464, label %if.else471, label %if.then465

if.then465:                                       ; preds = %land.lhs.true462
  %505 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth466 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %505, i32 0, i32 2
  %506 = load i32, i32* %depth466, align 4, !tbaa !15
  %srcDepth = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 2
  store i32 %506, i32* %srcDepth, align 4, !tbaa !62
  %srcRange = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 3
  store i32 1, i32* %srcRange, align 4, !tbaa !63
  %507 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels467 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %507, i32 0, i32 6
  %508 = load i8*, i8** %pixels467, align 4, !tbaa !38
  %srcPlane = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 4
  store i8* %508, i8** %srcPlane, align 4, !tbaa !64
  %509 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes468 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %509, i32 0, i32 7
  %510 = load i32, i32* %rowBytes468, align 4, !tbaa !42
  %srcRowBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 5
  store i32 %510, i32* %srcRowBytes, align 4, !tbaa !65
  %rgbOffsetBytesA = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 10
  %511 = load i32, i32* %rgbOffsetBytesA, align 4, !tbaa !34
  %srcOffsetBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 6
  store i32 %511, i32* %srcOffsetBytes, align 4, !tbaa !66
  %rgbPixelBytes469 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 6
  %512 = load i32, i32* %rgbPixelBytes469, align 4, !tbaa !30
  %srcPixelBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 7
  store i32 %512, i32* %srcPixelBytes, align 4, !tbaa !67
  %call470 = call i32 @avifReformatAlpha(%struct.avifAlphaParams* %params)
  br label %if.end473

if.else471:                                       ; preds = %land.lhs.true462, %if.then450
  %call472 = call i32 @avifFillAlpha(%struct.avifAlphaParams* %params)
  br label %if.end473

if.end473:                                        ; preds = %if.else471, %if.then465
  %513 = bitcast %struct.avifAlphaParams* %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %513) #3
  br label %if.end474

if.end474:                                        ; preds = %if.end473, %land.lhs.true448, %for.end446
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %514 = bitcast i32** %yuvRowBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %514) #3
  %515 = bitcast i8*** %yuvPlanes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %515) #3
  %516 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %516) #3
  %517 = bitcast float* %yuvMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %517) #3
  %518 = bitcast [3 x float]* %rgbPixel to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %518) #3
  %519 = bitcast [2 x [2 x %struct.YUVBlock]]* %yuvBlock to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %519) #3
  %520 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %520) #3
  %521 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %521) #3
  %522 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %522) #3
  br label %cleanup

cleanup:                                          ; preds = %if.end474, %if.then2
  %523 = bitcast %struct.avifReformatState* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 32828, i8* %523) #3
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %524 = load i32, i32* %retval, align 4
  ret i32 %524
}

declare void @avifImageAllocatePlanes(%struct.avifImage*, i32) #1

declare i32 @avifRGBFormatHasAlpha(i32) #1

; Function Attrs: nounwind
define internal i32 @yuvToUNorm(i32 %chan, i32 %range, i32 %depth, float %maxChannel, float %v, i32 %mode) #0 {
entry:
  %chan.addr = alloca i32, align 4
  %range.addr = alloca i32, align 4
  %depth.addr = alloca i32, align 4
  %maxChannel.addr = alloca float, align 4
  %v.addr = alloca float, align 4
  %mode.addr = alloca i32, align 4
  %unorm = alloca i32, align 4
  store i32 %chan, i32* %chan.addr, align 4, !tbaa !35
  store i32 %range, i32* %range.addr, align 4, !tbaa !45
  store i32 %depth, i32* %depth.addr, align 4, !tbaa !35
  store float %maxChannel, float* %maxChannel.addr, align 4, !tbaa !36
  store float %v, float* %v.addr, align 4, !tbaa !36
  store i32 %mode, i32* %mode.addr, align 4, !tbaa !45
  %0 = load i32, i32* %chan.addr, align 4, !tbaa !35
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %mode.addr, align 4, !tbaa !45
  %cmp1 = icmp ne i32 %1, 1
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load float, float* %v.addr, align 4, !tbaa !36
  %add = fadd float %2, 5.000000e-01
  store float %add, float* %v.addr, align 4, !tbaa !36
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %3 = load float, float* %v.addr, align 4, !tbaa !36
  %cmp2 = fcmp olt float %3, 0.000000e+00
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end6

cond.false:                                       ; preds = %if.end
  %4 = load float, float* %v.addr, align 4, !tbaa !36
  %cmp3 = fcmp olt float 1.000000e+00, %4
  br i1 %cmp3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.false
  br label %cond.end

cond.false5:                                      ; preds = %cond.false
  %5 = load float, float* %v.addr, align 4, !tbaa !36
  br label %cond.end

cond.end:                                         ; preds = %cond.false5, %cond.true4
  %cond = phi float [ 1.000000e+00, %cond.true4 ], [ %5, %cond.false5 ]
  br label %cond.end6

cond.end6:                                        ; preds = %cond.end, %cond.true
  %cond7 = phi float [ 0.000000e+00, %cond.true ], [ %cond, %cond.end ]
  store float %cond7, float* %v.addr, align 4, !tbaa !36
  %6 = bitcast i32* %unorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load float, float* %v.addr, align 4, !tbaa !36
  %8 = load float, float* %maxChannel.addr, align 4, !tbaa !36
  %mul = fmul float %7, %8
  %call = call float @avifRoundf(float %mul)
  %conv = fptosi float %call to i32
  store i32 %conv, i32* %unorm, align 4, !tbaa !35
  %9 = load i32, i32* %range.addr, align 4, !tbaa !45
  %cmp8 = icmp eq i32 %9, 0
  br i1 %cmp8, label %if.then10, label %if.end19

if.then10:                                        ; preds = %cond.end6
  %10 = load i32, i32* %chan.addr, align 4, !tbaa !35
  %cmp11 = icmp eq i32 %10, 0
  br i1 %cmp11, label %if.then15, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then10
  %11 = load i32, i32* %mode.addr, align 4, !tbaa !45
  %cmp13 = icmp eq i32 %11, 1
  br i1 %cmp13, label %if.then15, label %if.else

if.then15:                                        ; preds = %lor.lhs.false, %if.then10
  %12 = load i32, i32* %depth.addr, align 4, !tbaa !35
  %13 = load i32, i32* %unorm, align 4, !tbaa !35
  %call16 = call i32 @avifFullToLimitedY(i32 %12, i32 %13)
  store i32 %call16, i32* %unorm, align 4, !tbaa !35
  br label %if.end18

if.else:                                          ; preds = %lor.lhs.false
  %14 = load i32, i32* %depth.addr, align 4, !tbaa !35
  %15 = load i32, i32* %unorm, align 4, !tbaa !35
  %call17 = call i32 @avifFullToLimitedUV(i32 %14, i32 %15)
  store i32 %call17, i32* %unorm, align 4, !tbaa !35
  br label %if.end18

if.end18:                                         ; preds = %if.else, %if.then15
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %cond.end6
  %16 = load i32, i32* %unorm, align 4, !tbaa !35
  %17 = bitcast i32* %unorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  ret i32 %16
}

declare i32 @avifReformatAlpha(%struct.avifAlphaParams*) #1

declare i32 @avifFillAlpha(%struct.avifAlphaParams*) #1

; Function Attrs: nounwind
define hidden i32 @avifImageYUVToRGB(%struct.avifImage* %image, %struct.avifRGBImage* %rgb) #0 {
entry:
  %retval = alloca i32, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state = alloca %struct.avifReformatState, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %params = alloca %struct.avifAlphaParams, align 4
  %hasColor = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %0 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %0, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %1 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %struct.avifReformatState* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 32828, i8* %2) #3
  %3 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %4 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call = call i32 @avifPrepareReformatState(%struct.avifImage* %3, %struct.avifRGBImage* %4, %struct.avifReformatState* %state)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup83

if.end3:                                          ; preds = %if.end
  %5 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %format = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %5, i32 0, i32 3
  %6 = load i32, i32* %format, align 4, !tbaa !28
  %call4 = call i32 @avifRGBFormatHasAlpha(i32 %6)
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %land.lhs.true, label %if.end20

land.lhs.true:                                    ; preds = %if.end3
  %7 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %ignoreAlpha = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %7, i32 0, i32 5
  %8 = load i32, i32* %ignoreAlpha, align 4, !tbaa !39
  %tobool6 = icmp ne i32 %8, 0
  br i1 %tobool6, label %if.end20, label %if.then7

if.then7:                                         ; preds = %land.lhs.true
  %9 = bitcast %struct.avifAlphaParams* %params to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %9) #3
  %10 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %10, i32 0, i32 0
  %11 = load i32, i32* %width, align 4, !tbaa !68
  %width8 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 0
  store i32 %11, i32* %width8, align 4, !tbaa !52
  %12 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %12, i32 0, i32 1
  %13 = load i32, i32* %height, align 4, !tbaa !69
  %height9 = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 1
  store i32 %13, i32* %height9, align 4, !tbaa !54
  %14 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %14, i32 0, i32 2
  %15 = load i32, i32* %depth, align 4, !tbaa !15
  %dstDepth = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 8
  store i32 %15, i32* %dstDepth, align 4, !tbaa !55
  %dstRange = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 9
  store i32 1, i32* %dstRange, align 4, !tbaa !57
  %16 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %16, i32 0, i32 6
  %17 = load i8*, i8** %pixels, align 4, !tbaa !38
  %dstPlane = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 10
  store i8* %17, i8** %dstPlane, align 4, !tbaa !58
  %18 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %18, i32 0, i32 7
  %19 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %dstRowBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 11
  store i32 %19, i32* %dstRowBytes, align 4, !tbaa !59
  %rgbOffsetBytesA = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 10
  %20 = load i32, i32* %rgbOffsetBytesA, align 4, !tbaa !34
  %dstOffsetBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 12
  store i32 %20, i32* %dstOffsetBytes, align 4, !tbaa !60
  %rgbPixelBytes = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 6
  %21 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !30
  %dstPixelBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 13
  store i32 %21, i32* %dstPixelBytes, align 4, !tbaa !61
  %22 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %22, i32 0, i32 10
  %23 = load i8*, i8** %alphaPlane, align 4, !tbaa !50
  %tobool10 = icmp ne i8* %23, null
  br i1 %tobool10, label %land.lhs.true11, label %if.else

land.lhs.true11:                                  ; preds = %if.then7
  %24 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %24, i32 0, i32 11
  %25 = load i32, i32* %alphaRowBytes, align 4, !tbaa !51
  %tobool12 = icmp ne i32 %25, 0
  br i1 %tobool12, label %if.then13, label %if.else

if.then13:                                        ; preds = %land.lhs.true11
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth14 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 2
  %27 = load i32, i32* %depth14, align 4, !tbaa !6
  %srcDepth = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 2
  store i32 %27, i32* %srcDepth, align 4, !tbaa !62
  %28 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %28, i32 0, i32 9
  %29 = load i32, i32* %alphaRange, align 4, !tbaa !56
  %srcRange = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 3
  store i32 %29, i32* %srcRange, align 4, !tbaa !63
  %30 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane15 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %30, i32 0, i32 10
  %31 = load i8*, i8** %alphaPlane15, align 4, !tbaa !50
  %srcPlane = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 4
  store i8* %31, i8** %srcPlane, align 4, !tbaa !64
  %32 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes16 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 11
  %33 = load i32, i32* %alphaRowBytes16, align 4, !tbaa !51
  %srcRowBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 5
  store i32 %33, i32* %srcRowBytes, align 4, !tbaa !65
  %srcOffsetBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 6
  store i32 0, i32* %srcOffsetBytes, align 4, !tbaa !66
  %yuvChannelBytes = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 3
  %34 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !26
  %srcPixelBytes = getelementptr inbounds %struct.avifAlphaParams, %struct.avifAlphaParams* %params, i32 0, i32 7
  store i32 %34, i32* %srcPixelBytes, align 4, !tbaa !67
  %call17 = call i32 @avifReformatAlpha(%struct.avifAlphaParams* %params)
  br label %if.end19

if.else:                                          ; preds = %land.lhs.true11, %if.then7
  %call18 = call i32 @avifFillAlpha(%struct.avifAlphaParams* %params)
  br label %if.end19

if.end19:                                         ; preds = %if.else, %if.then13
  %35 = bitcast %struct.avifAlphaParams* %params to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %35) #3
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %land.lhs.true, %if.end3
  %36 = bitcast i32* %hasColor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %37, i32 0, i32 7
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 1
  %38 = load i32, i32* %arrayidx21, align 4, !tbaa !35
  %tobool22 = icmp ne i32 %38, 0
  br i1 %tobool22, label %land.lhs.true23, label %land.end

land.lhs.true23:                                  ; preds = %if.end20
  %39 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes24 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %39, i32 0, i32 7
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes24, i32 0, i32 2
  %40 = load i32, i32* %arrayidx25, align 4, !tbaa !35
  %tobool26 = icmp ne i32 %40, 0
  br i1 %tobool26, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true23
  %41 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %41, i32 0, i32 3
  %42 = load i32, i32* %yuvFormat, align 4, !tbaa !17
  %cmp = icmp ne i32 %42, 4
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true23, %if.end20
  %43 = phi i1 [ false, %land.lhs.true23 ], [ false, %if.end20 ], [ %cmp, %land.rhs ]
  %land.ext = zext i1 %43 to i32
  store i32 %land.ext, i32* %hasColor, align 4, !tbaa !35
  %44 = load i32, i32* %hasColor, align 4, !tbaa !35
  %tobool27 = icmp ne i32 %44, 0
  br i1 %tobool27, label %lor.lhs.false, label %if.then32

lor.lhs.false:                                    ; preds = %land.end
  %45 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat28 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %45, i32 0, i32 3
  %46 = load i32, i32* %yuvFormat28, align 4, !tbaa !17
  %cmp29 = icmp eq i32 %46, 1
  br i1 %cmp29, label %if.then32, label %lor.lhs.false30

lor.lhs.false30:                                  ; preds = %lor.lhs.false
  %47 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %chromaUpsampling = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %47, i32 0, i32 4
  %48 = load i32, i32* %chromaUpsampling, align 4, !tbaa !70
  %cmp31 = icmp eq i32 %48, 1
  br i1 %cmp31, label %if.then32, label %if.end81

if.then32:                                        ; preds = %lor.lhs.false30, %lor.lhs.false, %land.end
  %mode = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %state, i32 0, i32 14
  %49 = load i32, i32* %mode, align 4, !tbaa !18
  %cmp33 = icmp eq i32 %49, 1
  br i1 %cmp33, label %if.then34, label %if.else47

if.then34:                                        ; preds = %if.then32
  %50 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth35 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %50, i32 0, i32 2
  %51 = load i32, i32* %depth35, align 4, !tbaa !6
  %cmp36 = icmp eq i32 %51, 8
  br i1 %cmp36, label %land.lhs.true37, label %if.end46

land.lhs.true37:                                  ; preds = %if.then34
  %52 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth38 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %52, i32 0, i32 2
  %53 = load i32, i32* %depth38, align 4, !tbaa !15
  %cmp39 = icmp eq i32 %53, 8
  br i1 %cmp39, label %land.lhs.true40, label %if.end46

land.lhs.true40:                                  ; preds = %land.lhs.true37
  %54 = load i32, i32* %hasColor, align 4, !tbaa !35
  %tobool41 = icmp ne i32 %54, 0
  br i1 %tobool41, label %land.lhs.true42, label %if.end46

land.lhs.true42:                                  ; preds = %land.lhs.true40
  %55 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %55, i32 0, i32 4
  %56 = load i32, i32* %yuvRange, align 4, !tbaa !37
  %cmp43 = icmp eq i32 %56, 1
  br i1 %cmp43, label %if.then44, label %if.end46

if.then44:                                        ; preds = %land.lhs.true42
  %57 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %58 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call45 = call i32 @avifImageIdentity8ToRGB8ColorFullRange(%struct.avifImage* %57, %struct.avifRGBImage* %58, %struct.avifReformatState* %state)
  store i32 %call45, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %land.lhs.true42, %land.lhs.true40, %land.lhs.true37, %if.then34
  br label %if.end80

if.else47:                                        ; preds = %if.then32
  %59 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth48 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %59, i32 0, i32 2
  %60 = load i32, i32* %depth48, align 4, !tbaa !6
  %cmp49 = icmp ugt i32 %60, 8
  br i1 %cmp49, label %if.then50, label %if.else65

if.then50:                                        ; preds = %if.else47
  %61 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth51 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %61, i32 0, i32 2
  %62 = load i32, i32* %depth51, align 4, !tbaa !15
  %cmp52 = icmp ugt i32 %62, 8
  br i1 %cmp52, label %if.then53, label %if.else59

if.then53:                                        ; preds = %if.then50
  %63 = load i32, i32* %hasColor, align 4, !tbaa !35
  %tobool54 = icmp ne i32 %63, 0
  br i1 %tobool54, label %if.then55, label %if.end57

if.then55:                                        ; preds = %if.then53
  %64 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %65 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call56 = call i32 @avifImageYUV16ToRGB16Color(%struct.avifImage* %64, %struct.avifRGBImage* %65, %struct.avifReformatState* %state)
  store i32 %call56, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end57:                                         ; preds = %if.then53
  %66 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %67 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call58 = call i32 @avifImageYUV16ToRGB16Mono(%struct.avifImage* %66, %struct.avifRGBImage* %67, %struct.avifReformatState* %state)
  store i32 %call58, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else59:                                        ; preds = %if.then50
  %68 = load i32, i32* %hasColor, align 4, !tbaa !35
  %tobool60 = icmp ne i32 %68, 0
  br i1 %tobool60, label %if.then61, label %if.end63

if.then61:                                        ; preds = %if.else59
  %69 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %70 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call62 = call i32 @avifImageYUV16ToRGB8Color(%struct.avifImage* %69, %struct.avifRGBImage* %70, %struct.avifReformatState* %state)
  store i32 %call62, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end63:                                         ; preds = %if.else59
  %71 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %72 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call64 = call i32 @avifImageYUV16ToRGB8Mono(%struct.avifImage* %71, %struct.avifRGBImage* %72, %struct.avifReformatState* %state)
  store i32 %call64, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else65:                                        ; preds = %if.else47
  %73 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth66 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %73, i32 0, i32 2
  %74 = load i32, i32* %depth66, align 4, !tbaa !15
  %cmp67 = icmp ugt i32 %74, 8
  br i1 %cmp67, label %if.then68, label %if.else74

if.then68:                                        ; preds = %if.else65
  %75 = load i32, i32* %hasColor, align 4, !tbaa !35
  %tobool69 = icmp ne i32 %75, 0
  br i1 %tobool69, label %if.then70, label %if.end72

if.then70:                                        ; preds = %if.then68
  %76 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %77 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call71 = call i32 @avifImageYUV8ToRGB16Color(%struct.avifImage* %76, %struct.avifRGBImage* %77, %struct.avifReformatState* %state)
  store i32 %call71, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end72:                                         ; preds = %if.then68
  %78 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %79 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call73 = call i32 @avifImageYUV8ToRGB16Mono(%struct.avifImage* %78, %struct.avifRGBImage* %79, %struct.avifReformatState* %state)
  store i32 %call73, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else74:                                        ; preds = %if.else65
  %80 = load i32, i32* %hasColor, align 4, !tbaa !35
  %tobool75 = icmp ne i32 %80, 0
  br i1 %tobool75, label %if.then76, label %if.end78

if.then76:                                        ; preds = %if.else74
  %81 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %82 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call77 = call i32 @avifImageYUV8ToRGB8Color(%struct.avifImage* %81, %struct.avifRGBImage* %82, %struct.avifReformatState* %state)
  store i32 %call77, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end78:                                         ; preds = %if.else74
  %83 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %84 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call79 = call i32 @avifImageYUV8ToRGB8Mono(%struct.avifImage* %83, %struct.avifRGBImage* %84, %struct.avifReformatState* %state)
  store i32 %call79, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end80:                                         ; preds = %if.end46
  br label %if.end81

if.end81:                                         ; preds = %if.end80, %lor.lhs.false30
  %85 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %86 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %call82 = call i32 @avifImageYUVAnyToRGBAnySlow(%struct.avifImage* %85, %struct.avifRGBImage* %86, %struct.avifReformatState* %state)
  store i32 %call82, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end81, %if.end78, %if.then76, %if.end72, %if.then70, %if.end63, %if.then61, %if.end57, %if.then55, %if.then44
  %87 = bitcast i32* %hasColor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #3
  br label %cleanup83

cleanup83:                                        ; preds = %cleanup, %if.then2
  %88 = bitcast %struct.avifReformatState* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 32828, i8* %88) #3
  br label %return

return:                                           ; preds = %cleanup83, %if.then
  %89 = load i32, i32* %retval, align 4
  ret i32 %89
}

; Function Attrs: nounwind
define internal i32 @avifImageIdentity8ToRGB8ColorFullRange(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %rgbPixelBytes = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ptrY = alloca i8*, align 4
  %ptrU = alloca i8*, align 4
  %ptrV = alloca i8*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 6
  %2 = load i32, i32* %rgbPixelBytes1, align 4, !tbaa !30
  store i32 %2, i32* %rgbPixelBytes, align 4, !tbaa !35
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc37, %entry
  %4 = load i32, i32* %j, align 4, !tbaa !35
  %5 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %5, i32 0, i32 1
  %6 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %4, %6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #3
  br label %for.end39

for.body:                                         ; preds = %for.cond
  %8 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %9, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %10 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %j, align 4, !tbaa !35
  %12 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 7
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %13 = load i32, i32* %arrayidx2, align 4, !tbaa !35
  %mul = mul i32 %11, %13
  %arrayidx3 = getelementptr inbounds i8, i8* %10, i32 %mul
  store i8* %arrayidx3, i8** %ptrY, align 4, !tbaa !2
  %14 = bitcast i8** %ptrU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes4 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %15, i32 0, i32 6
  %arrayidx5 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes4, i32 0, i32 1
  %16 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  %17 = load i32, i32* %j, align 4, !tbaa !35
  %18 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes6 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %18, i32 0, i32 7
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes6, i32 0, i32 1
  %19 = load i32, i32* %arrayidx7, align 4, !tbaa !35
  %mul8 = mul i32 %17, %19
  %arrayidx9 = getelementptr inbounds i8, i8* %16, i32 %mul8
  store i8* %arrayidx9, i8** %ptrU, align 4, !tbaa !2
  %20 = bitcast i8** %ptrV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes10 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %21, i32 0, i32 6
  %arrayidx11 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes10, i32 0, i32 2
  %22 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  %23 = load i32, i32* %j, align 4, !tbaa !35
  %24 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes12 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %24, i32 0, i32 7
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes12, i32 0, i32 2
  %25 = load i32, i32* %arrayidx13, align 4, !tbaa !35
  %mul14 = mul i32 %23, %25
  %arrayidx15 = getelementptr inbounds i8, i8* %22, i32 %mul14
  store i8* %arrayidx15, i8** %ptrV, align 4, !tbaa !2
  %26 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %27, i32 0, i32 6
  %28 = load i8*, i8** %pixels, align 4, !tbaa !38
  %29 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %29, i32 0, i32 7
  %30 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %31 = load i32, i32* %j, align 4, !tbaa !35
  %32 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %32, i32 0, i32 7
  %33 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul16 = mul i32 %31, %33
  %add = add i32 %30, %mul16
  %arrayidx17 = getelementptr inbounds i8, i8* %28, i32 %add
  store i8* %arrayidx17, i8** %ptrR, align 4, !tbaa !2
  %34 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #3
  %35 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels18 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %35, i32 0, i32 6
  %36 = load i8*, i8** %pixels18, align 4, !tbaa !38
  %37 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %37, i32 0, i32 8
  %38 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %39 = load i32, i32* %j, align 4, !tbaa !35
  %40 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes19 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %40, i32 0, i32 7
  %41 = load i32, i32* %rowBytes19, align 4, !tbaa !42
  %mul20 = mul i32 %39, %41
  %add21 = add i32 %38, %mul20
  %arrayidx22 = getelementptr inbounds i8, i8* %36, i32 %add21
  store i8* %arrayidx22, i8** %ptrG, align 4, !tbaa !2
  %42 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #3
  %43 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels23 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %43, i32 0, i32 6
  %44 = load i8*, i8** %pixels23, align 4, !tbaa !38
  %45 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %45, i32 0, i32 9
  %46 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %47 = load i32, i32* %j, align 4, !tbaa !35
  %48 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes24 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %48, i32 0, i32 7
  %49 = load i32, i32* %rowBytes24, align 4, !tbaa !42
  %mul25 = mul i32 %47, %49
  %add26 = add i32 %46, %mul25
  %arrayidx27 = getelementptr inbounds i8, i8* %44, i32 %add26
  store i8* %arrayidx27, i8** %ptrB, align 4, !tbaa !2
  %50 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc, %for.body
  %51 = load i32, i32* %i, align 4, !tbaa !35
  %52 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %52, i32 0, i32 0
  %53 = load i32, i32* %width, align 4, !tbaa !41
  %cmp29 = icmp ult i32 %51, %53
  br i1 %cmp29, label %for.body31, label %for.cond.cleanup30

for.cond.cleanup30:                               ; preds = %for.cond28
  store i32 5, i32* %cleanup.dest.slot, align 4
  %54 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  br label %for.end

for.body31:                                       ; preds = %for.cond28
  %55 = load i8*, i8** %ptrV, align 4, !tbaa !2
  %56 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx32 = getelementptr inbounds i8, i8* %55, i32 %56
  %57 = load i8, i8* %arrayidx32, align 1, !tbaa !45
  %58 = load i8*, i8** %ptrR, align 4, !tbaa !2
  store i8 %57, i8* %58, align 1, !tbaa !45
  %59 = load i8*, i8** %ptrY, align 4, !tbaa !2
  %60 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx33 = getelementptr inbounds i8, i8* %59, i32 %60
  %61 = load i8, i8* %arrayidx33, align 1, !tbaa !45
  %62 = load i8*, i8** %ptrG, align 4, !tbaa !2
  store i8 %61, i8* %62, align 1, !tbaa !45
  %63 = load i8*, i8** %ptrU, align 4, !tbaa !2
  %64 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx34 = getelementptr inbounds i8, i8* %63, i32 %64
  %65 = load i8, i8* %arrayidx34, align 1, !tbaa !45
  %66 = load i8*, i8** %ptrB, align 4, !tbaa !2
  store i8 %65, i8* %66, align 1, !tbaa !45
  %67 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %68 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %68, i32 %67
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %69 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %70 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr35 = getelementptr inbounds i8, i8* %70, i32 %69
  store i8* %add.ptr35, i8** %ptrG, align 4, !tbaa !2
  %71 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %72 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr36 = getelementptr inbounds i8, i8* %72, i32 %71
  store i8* %add.ptr36, i8** %ptrB, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body31
  %73 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %73, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond28

for.end:                                          ; preds = %for.cond.cleanup30
  %74 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %75 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %77 = bitcast i8** %ptrV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast i8** %ptrU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  %79 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  br label %for.inc37

for.inc37:                                        ; preds = %for.end
  %80 = load i32, i32* %j, align 4, !tbaa !35
  %inc38 = add i32 %80, 1
  store i32 %inc38, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end39:                                        ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %81 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUV16ToRGB16Color(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %rgbPixelBytes = alloca i32, align 4
  %unormFloatTableY = alloca float*, align 4
  %unormFloatTableUV = alloca float*, align 4
  %yuvMaxChannel = alloca i16, align 2
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %uvJ = alloca i32, align 4
  %ptrY = alloca i16*, align 4
  %ptrU = alloca i16*, align 4
  %ptrV = alloca i16*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %uvI = alloca i32, align 4
  %unormY = alloca i16, align 2
  %unormU = alloca i16, align 2
  %unormV = alloca i16, align 2
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %R = alloca float, align 4
  %B = alloca float, align 4
  %G = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 6
  %11 = load i32, i32* %rgbPixelBytes4, align 4, !tbaa !30
  store i32 %11, i32* %rgbPixelBytes, align 4, !tbaa !35
  %12 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %13, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY5, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %14 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableUV6 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %15, i32 0, i32 13
  %arraydecay7 = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableUV6, i32 0, i32 0
  store float* %arraydecay7, float** %unormFloatTableUV, align 4, !tbaa !2
  %16 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #3
  %17 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %17, i32 0, i32 2
  %18 = load i32, i32* %depth, align 4, !tbaa !6
  %shl = shl i32 1, %18
  %sub = sub nsw i32 %shl, 1
  %conv = trunc i32 %sub to i16
  store i16 %conv, i16* %yuvMaxChannel, align 2, !tbaa !43
  %19 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth8 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %20, i32 0, i32 2
  %21 = load i32, i32* %depth8, align 4, !tbaa !15
  %shl9 = shl i32 1, %21
  %sub10 = sub nsw i32 %shl9, 1
  %conv11 = sitofp i32 %sub10 to float
  store float %conv11, float* %rgbMaxChannel, align 4, !tbaa !36
  %22 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc150, %entry
  %23 = load i32, i32* %j, align 4, !tbaa !35
  %24 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %24, i32 0, i32 1
  %25 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %23, %25
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %26 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  br label %for.end152

for.body:                                         ; preds = %for.cond
  %27 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = load i32, i32* %j, align 4, !tbaa !35
  %29 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %29, i32 0, i32 11
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %30 = load i32, i32* %chromaShiftY, align 4, !tbaa !71
  %shr = lshr i32 %28, %30
  store i32 %shr, i32* %uvJ, align 4, !tbaa !35
  %31 = bitcast i16** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #3
  %32 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %33 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %j, align 4, !tbaa !35
  %35 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %35, i32 0, i32 7
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %36 = load i32, i32* %arrayidx13, align 4, !tbaa !35
  %mul = mul i32 %34, %36
  %arrayidx14 = getelementptr inbounds i8, i8* %33, i32 %mul
  %37 = bitcast i8* %arrayidx14 to i16*
  store i16* %37, i16** %ptrY, align 4, !tbaa !2
  %38 = bitcast i16** %ptrU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #3
  %39 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes15 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %39, i32 0, i32 6
  %arrayidx16 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes15, i32 0, i32 1
  %40 = load i8*, i8** %arrayidx16, align 4, !tbaa !2
  %41 = load i32, i32* %uvJ, align 4, !tbaa !35
  %42 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes17 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %42, i32 0, i32 7
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes17, i32 0, i32 1
  %43 = load i32, i32* %arrayidx18, align 4, !tbaa !35
  %mul19 = mul i32 %41, %43
  %arrayidx20 = getelementptr inbounds i8, i8* %40, i32 %mul19
  %44 = bitcast i8* %arrayidx20 to i16*
  store i16* %44, i16** %ptrU, align 4, !tbaa !2
  %45 = bitcast i16** %ptrV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #3
  %46 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes21 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %46, i32 0, i32 6
  %arrayidx22 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes21, i32 0, i32 2
  %47 = load i8*, i8** %arrayidx22, align 4, !tbaa !2
  %48 = load i32, i32* %uvJ, align 4, !tbaa !35
  %49 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes23 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %49, i32 0, i32 7
  %arrayidx24 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes23, i32 0, i32 2
  %50 = load i32, i32* %arrayidx24, align 4, !tbaa !35
  %mul25 = mul i32 %48, %50
  %arrayidx26 = getelementptr inbounds i8, i8* %47, i32 %mul25
  %51 = bitcast i8* %arrayidx26 to i16*
  store i16* %51, i16** %ptrV, align 4, !tbaa !2
  %52 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %53, i32 0, i32 6
  %54 = load i8*, i8** %pixels, align 4, !tbaa !38
  %55 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %55, i32 0, i32 7
  %56 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %57 = load i32, i32* %j, align 4, !tbaa !35
  %58 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %58, i32 0, i32 7
  %59 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul27 = mul i32 %57, %59
  %add = add i32 %56, %mul27
  %arrayidx28 = getelementptr inbounds i8, i8* %54, i32 %add
  store i8* %arrayidx28, i8** %ptrR, align 4, !tbaa !2
  %60 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #3
  %61 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels29 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %61, i32 0, i32 6
  %62 = load i8*, i8** %pixels29, align 4, !tbaa !38
  %63 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %63, i32 0, i32 8
  %64 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %65 = load i32, i32* %j, align 4, !tbaa !35
  %66 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes30 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %66, i32 0, i32 7
  %67 = load i32, i32* %rowBytes30, align 4, !tbaa !42
  %mul31 = mul i32 %65, %67
  %add32 = add i32 %64, %mul31
  %arrayidx33 = getelementptr inbounds i8, i8* %62, i32 %add32
  store i8* %arrayidx33, i8** %ptrG, align 4, !tbaa !2
  %68 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels34 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %69, i32 0, i32 6
  %70 = load i8*, i8** %pixels34, align 4, !tbaa !38
  %71 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %71, i32 0, i32 9
  %72 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %73 = load i32, i32* %j, align 4, !tbaa !35
  %74 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes35 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %74, i32 0, i32 7
  %75 = load i32, i32* %rowBytes35, align 4, !tbaa !42
  %mul36 = mul i32 %73, %75
  %add37 = add i32 %72, %mul36
  %arrayidx38 = getelementptr inbounds i8, i8* %70, i32 %add37
  store i8* %arrayidx38, i8** %ptrB, align 4, !tbaa !2
  %76 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc, %for.body
  %77 = load i32, i32* %i, align 4, !tbaa !35
  %78 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %78, i32 0, i32 0
  %79 = load i32, i32* %width, align 4, !tbaa !41
  %cmp40 = icmp ult i32 %77, %79
  br i1 %cmp40, label %for.body43, label %for.cond.cleanup42

for.cond.cleanup42:                               ; preds = %for.cond39
  store i32 5, i32* %cleanup.dest.slot, align 4
  %80 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #3
  br label %for.end

for.body43:                                       ; preds = %for.cond39
  %81 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #3
  %82 = load i32, i32* %i, align 4, !tbaa !35
  %83 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo44 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %83, i32 0, i32 11
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo44, i32 0, i32 1
  %84 = load i32, i32* %chromaShiftX, align 4, !tbaa !72
  %shr45 = lshr i32 %82, %84
  store i32 %shr45, i32* %uvI, align 4, !tbaa !35
  %85 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %85) #3
  %86 = load i16*, i16** %ptrY, align 4, !tbaa !2
  %87 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx46 = getelementptr inbounds i16, i16* %86, i32 %87
  %88 = load i16, i16* %arrayidx46, align 2, !tbaa !43
  %conv47 = zext i16 %88 to i32
  %89 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv48 = zext i16 %89 to i32
  %cmp49 = icmp slt i32 %conv47, %conv48
  br i1 %cmp49, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body43
  %90 = load i16*, i16** %ptrY, align 4, !tbaa !2
  %91 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx51 = getelementptr inbounds i16, i16* %90, i32 %91
  %92 = load i16, i16* %arrayidx51, align 2, !tbaa !43
  %conv52 = zext i16 %92 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body43
  %93 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv53 = zext i16 %93 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv52, %cond.true ], [ %conv53, %cond.false ]
  %conv54 = trunc i32 %cond to i16
  store i16 %conv54, i16* %unormY, align 2, !tbaa !43
  %94 = bitcast i16* %unormU to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %94) #3
  %95 = load i16*, i16** %ptrU, align 4, !tbaa !2
  %96 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx55 = getelementptr inbounds i16, i16* %95, i32 %96
  %97 = load i16, i16* %arrayidx55, align 2, !tbaa !43
  %conv56 = zext i16 %97 to i32
  %98 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv57 = zext i16 %98 to i32
  %cmp58 = icmp slt i32 %conv56, %conv57
  br i1 %cmp58, label %cond.true60, label %cond.false63

cond.true60:                                      ; preds = %cond.end
  %99 = load i16*, i16** %ptrU, align 4, !tbaa !2
  %100 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx61 = getelementptr inbounds i16, i16* %99, i32 %100
  %101 = load i16, i16* %arrayidx61, align 2, !tbaa !43
  %conv62 = zext i16 %101 to i32
  br label %cond.end65

cond.false63:                                     ; preds = %cond.end
  %102 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv64 = zext i16 %102 to i32
  br label %cond.end65

cond.end65:                                       ; preds = %cond.false63, %cond.true60
  %cond66 = phi i32 [ %conv62, %cond.true60 ], [ %conv64, %cond.false63 ]
  %conv67 = trunc i32 %cond66 to i16
  store i16 %conv67, i16* %unormU, align 2, !tbaa !43
  %103 = bitcast i16* %unormV to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %103) #3
  %104 = load i16*, i16** %ptrV, align 4, !tbaa !2
  %105 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx68 = getelementptr inbounds i16, i16* %104, i32 %105
  %106 = load i16, i16* %arrayidx68, align 2, !tbaa !43
  %conv69 = zext i16 %106 to i32
  %107 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv70 = zext i16 %107 to i32
  %cmp71 = icmp slt i32 %conv69, %conv70
  br i1 %cmp71, label %cond.true73, label %cond.false76

cond.true73:                                      ; preds = %cond.end65
  %108 = load i16*, i16** %ptrV, align 4, !tbaa !2
  %109 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx74 = getelementptr inbounds i16, i16* %108, i32 %109
  %110 = load i16, i16* %arrayidx74, align 2, !tbaa !43
  %conv75 = zext i16 %110 to i32
  br label %cond.end78

cond.false76:                                     ; preds = %cond.end65
  %111 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv77 = zext i16 %111 to i32
  br label %cond.end78

cond.end78:                                       ; preds = %cond.false76, %cond.true73
  %cond79 = phi i32 [ %conv75, %cond.true73 ], [ %conv77, %cond.false76 ]
  %conv80 = trunc i32 %cond79 to i16
  store i16 %conv80, i16* %unormV, align 2, !tbaa !43
  %112 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #3
  %113 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %114 = load i16, i16* %unormY, align 2, !tbaa !43
  %idxprom = zext i16 %114 to i32
  %arrayidx81 = getelementptr inbounds float, float* %113, i32 %idxprom
  %115 = load float, float* %arrayidx81, align 4, !tbaa !36
  store float %115, float* %Y, align 4, !tbaa !36
  %116 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #3
  %117 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %118 = load i16, i16* %unormU, align 2, !tbaa !43
  %idxprom82 = zext i16 %118 to i32
  %arrayidx83 = getelementptr inbounds float, float* %117, i32 %idxprom82
  %119 = load float, float* %arrayidx83, align 4, !tbaa !36
  store float %119, float* %Cb, align 4, !tbaa !36
  %120 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #3
  %121 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %122 = load i16, i16* %unormV, align 2, !tbaa !43
  %idxprom84 = zext i16 %122 to i32
  %arrayidx85 = getelementptr inbounds float, float* %121, i32 %idxprom84
  %123 = load float, float* %arrayidx85, align 4, !tbaa !36
  store float %123, float* %Cr, align 4, !tbaa !36
  %124 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #3
  %125 = load float, float* %Y, align 4, !tbaa !36
  %126 = load float, float* %kr, align 4, !tbaa !36
  %sub86 = fsub float 1.000000e+00, %126
  %mul87 = fmul float 2.000000e+00, %sub86
  %127 = load float, float* %Cr, align 4, !tbaa !36
  %mul88 = fmul float %mul87, %127
  %add89 = fadd float %125, %mul88
  store float %add89, float* %R, align 4, !tbaa !36
  %128 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #3
  %129 = load float, float* %Y, align 4, !tbaa !36
  %130 = load float, float* %kb, align 4, !tbaa !36
  %sub90 = fsub float 1.000000e+00, %130
  %mul91 = fmul float 2.000000e+00, %sub90
  %131 = load float, float* %Cb, align 4, !tbaa !36
  %mul92 = fmul float %mul91, %131
  %add93 = fadd float %129, %mul92
  store float %add93, float* %B, align 4, !tbaa !36
  %132 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #3
  %133 = load float, float* %Y, align 4, !tbaa !36
  %134 = load float, float* %kr, align 4, !tbaa !36
  %135 = load float, float* %kr, align 4, !tbaa !36
  %sub94 = fsub float 1.000000e+00, %135
  %mul95 = fmul float %134, %sub94
  %136 = load float, float* %Cr, align 4, !tbaa !36
  %mul96 = fmul float %mul95, %136
  %137 = load float, float* %kb, align 4, !tbaa !36
  %138 = load float, float* %kb, align 4, !tbaa !36
  %sub97 = fsub float 1.000000e+00, %138
  %mul98 = fmul float %137, %sub97
  %139 = load float, float* %Cb, align 4, !tbaa !36
  %mul99 = fmul float %mul98, %139
  %add100 = fadd float %mul96, %mul99
  %mul101 = fmul float 2.000000e+00, %add100
  %140 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul101, %140
  %sub102 = fsub float %133, %div
  store float %sub102, float* %G, align 4, !tbaa !36
  %141 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %141) #3
  %142 = load float, float* %R, align 4, !tbaa !36
  %cmp103 = fcmp olt float %142, 0.000000e+00
  br i1 %cmp103, label %cond.true105, label %cond.false106

cond.true105:                                     ; preds = %cond.end78
  br label %cond.end113

cond.false106:                                    ; preds = %cond.end78
  %143 = load float, float* %R, align 4, !tbaa !36
  %cmp107 = fcmp olt float 1.000000e+00, %143
  br i1 %cmp107, label %cond.true109, label %cond.false110

cond.true109:                                     ; preds = %cond.false106
  br label %cond.end111

cond.false110:                                    ; preds = %cond.false106
  %144 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end111

cond.end111:                                      ; preds = %cond.false110, %cond.true109
  %cond112 = phi float [ 1.000000e+00, %cond.true109 ], [ %144, %cond.false110 ]
  br label %cond.end113

cond.end113:                                      ; preds = %cond.end111, %cond.true105
  %cond114 = phi float [ 0.000000e+00, %cond.true105 ], [ %cond112, %cond.end111 ]
  store float %cond114, float* %Rc, align 4, !tbaa !36
  %145 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #3
  %146 = load float, float* %G, align 4, !tbaa !36
  %cmp115 = fcmp olt float %146, 0.000000e+00
  br i1 %cmp115, label %cond.true117, label %cond.false118

cond.true117:                                     ; preds = %cond.end113
  br label %cond.end125

cond.false118:                                    ; preds = %cond.end113
  %147 = load float, float* %G, align 4, !tbaa !36
  %cmp119 = fcmp olt float 1.000000e+00, %147
  br i1 %cmp119, label %cond.true121, label %cond.false122

cond.true121:                                     ; preds = %cond.false118
  br label %cond.end123

cond.false122:                                    ; preds = %cond.false118
  %148 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end123

cond.end123:                                      ; preds = %cond.false122, %cond.true121
  %cond124 = phi float [ 1.000000e+00, %cond.true121 ], [ %148, %cond.false122 ]
  br label %cond.end125

cond.end125:                                      ; preds = %cond.end123, %cond.true117
  %cond126 = phi float [ 0.000000e+00, %cond.true117 ], [ %cond124, %cond.end123 ]
  store float %cond126, float* %Gc, align 4, !tbaa !36
  %149 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #3
  %150 = load float, float* %B, align 4, !tbaa !36
  %cmp127 = fcmp olt float %150, 0.000000e+00
  br i1 %cmp127, label %cond.true129, label %cond.false130

cond.true129:                                     ; preds = %cond.end125
  br label %cond.end137

cond.false130:                                    ; preds = %cond.end125
  %151 = load float, float* %B, align 4, !tbaa !36
  %cmp131 = fcmp olt float 1.000000e+00, %151
  br i1 %cmp131, label %cond.true133, label %cond.false134

cond.true133:                                     ; preds = %cond.false130
  br label %cond.end135

cond.false134:                                    ; preds = %cond.false130
  %152 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end135

cond.end135:                                      ; preds = %cond.false134, %cond.true133
  %cond136 = phi float [ 1.000000e+00, %cond.true133 ], [ %152, %cond.false134 ]
  br label %cond.end137

cond.end137:                                      ; preds = %cond.end135, %cond.true129
  %cond138 = phi float [ 0.000000e+00, %cond.true129 ], [ %cond136, %cond.end135 ]
  store float %cond138, float* %Bc, align 4, !tbaa !36
  %153 = load float, float* %Rc, align 4, !tbaa !36
  %154 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul139 = fmul float %153, %154
  %add140 = fadd float 5.000000e-01, %mul139
  %conv141 = fptoui float %add140 to i16
  %155 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %156 = bitcast i8* %155 to i16*
  store i16 %conv141, i16* %156, align 2, !tbaa !43
  %157 = load float, float* %Gc, align 4, !tbaa !36
  %158 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul142 = fmul float %157, %158
  %add143 = fadd float 5.000000e-01, %mul142
  %conv144 = fptoui float %add143 to i16
  %159 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %160 = bitcast i8* %159 to i16*
  store i16 %conv144, i16* %160, align 2, !tbaa !43
  %161 = load float, float* %Bc, align 4, !tbaa !36
  %162 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul145 = fmul float %161, %162
  %add146 = fadd float 5.000000e-01, %mul145
  %conv147 = fptoui float %add146 to i16
  %163 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %164 = bitcast i8* %163 to i16*
  store i16 %conv147, i16* %164, align 2, !tbaa !43
  %165 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %166 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %166, i32 %165
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %167 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %168 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr148 = getelementptr inbounds i8, i8* %168, i32 %167
  store i8* %add.ptr148, i8** %ptrG, align 4, !tbaa !2
  %169 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %170 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr149 = getelementptr inbounds i8, i8* %170, i32 %169
  store i8* %add.ptr149, i8** %ptrB, align 4, !tbaa !2
  %171 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #3
  %172 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #3
  %173 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #3
  %174 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #3
  %175 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #3
  %176 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #3
  %177 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #3
  %178 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #3
  %179 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #3
  %180 = bitcast i16* %unormV to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %180) #3
  %181 = bitcast i16* %unormU to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %181) #3
  %182 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %182) #3
  %183 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end137
  %184 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %184, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond39

for.end:                                          ; preds = %for.cond.cleanup42
  %185 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #3
  %186 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #3
  %187 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #3
  %188 = bitcast i16** %ptrV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #3
  %189 = bitcast i16** %ptrU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #3
  %190 = bitcast i16** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #3
  %191 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #3
  br label %for.inc150

for.inc150:                                       ; preds = %for.end
  %192 = load i32, i32* %j, align 4, !tbaa !35
  %inc151 = add i32 %192, 1
  store i32 %inc151, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end152:                                       ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %193 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #3
  %194 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %194) #3
  %195 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #3
  %196 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #3
  %197 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #3
  %198 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #3
  %199 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #3
  %200 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUV16ToRGB16Mono(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %rgbPixelBytes = alloca i32, align 4
  %unormFloatTableY = alloca float*, align 4
  %yuvMaxChannel = alloca i16, align 2
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ptrY = alloca i16*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %unormY = alloca i16, align 2
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %R = alloca float, align 4
  %B = alloca float, align 4
  %G = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 6
  %11 = load i32, i32* %rgbPixelBytes4, align 4, !tbaa !30
  store i32 %11, i32* %rgbPixelBytes, align 4, !tbaa !35
  %12 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %13, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY5, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %14 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #3
  %15 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %15, i32 0, i32 2
  %16 = load i32, i32* %depth, align 4, !tbaa !6
  %shl = shl i32 1, %16
  %sub = sub nsw i32 %shl, 1
  %conv = trunc i32 %sub to i16
  store i16 %conv, i16* %yuvMaxChannel, align 2, !tbaa !43
  %17 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth6 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %18, i32 0, i32 2
  %19 = load i32, i32* %depth6, align 4, !tbaa !15
  %shl7 = shl i32 1, %19
  %sub8 = sub nsw i32 %shl7, 1
  %conv9 = sitofp i32 %sub8 to float
  store float %conv9, float* %rgbMaxChannel, align 4, !tbaa !36
  %20 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc104, %entry
  %21 = load i32, i32* %j, align 4, !tbaa !35
  %22 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %22, i32 0, i32 1
  %23 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %21, %23
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  br label %for.end106

for.body:                                         ; preds = %for.cond
  %25 = bitcast i16** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %27 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %28 = load i32, i32* %j, align 4, !tbaa !35
  %29 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %29, i32 0, i32 7
  %arrayidx11 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %30 = load i32, i32* %arrayidx11, align 4, !tbaa !35
  %mul = mul i32 %28, %30
  %arrayidx12 = getelementptr inbounds i8, i8* %27, i32 %mul
  %31 = bitcast i8* %arrayidx12 to i16*
  store i16* %31, i16** %ptrY, align 4, !tbaa !2
  %32 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %33, i32 0, i32 6
  %34 = load i8*, i8** %pixels, align 4, !tbaa !38
  %35 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %35, i32 0, i32 7
  %36 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %37 = load i32, i32* %j, align 4, !tbaa !35
  %38 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %38, i32 0, i32 7
  %39 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul13 = mul i32 %37, %39
  %add = add i32 %36, %mul13
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 %add
  store i8* %arrayidx14, i8** %ptrR, align 4, !tbaa !2
  %40 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels15 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %41, i32 0, i32 6
  %42 = load i8*, i8** %pixels15, align 4, !tbaa !38
  %43 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %43, i32 0, i32 8
  %44 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %45 = load i32, i32* %j, align 4, !tbaa !35
  %46 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes16 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %46, i32 0, i32 7
  %47 = load i32, i32* %rowBytes16, align 4, !tbaa !42
  %mul17 = mul i32 %45, %47
  %add18 = add i32 %44, %mul17
  %arrayidx19 = getelementptr inbounds i8, i8* %42, i32 %add18
  store i8* %arrayidx19, i8** %ptrG, align 4, !tbaa !2
  %48 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #3
  %49 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels20 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %49, i32 0, i32 6
  %50 = load i8*, i8** %pixels20, align 4, !tbaa !38
  %51 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %51, i32 0, i32 9
  %52 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %53 = load i32, i32* %j, align 4, !tbaa !35
  %54 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes21 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %54, i32 0, i32 7
  %55 = load i32, i32* %rowBytes21, align 4, !tbaa !42
  %mul22 = mul i32 %53, %55
  %add23 = add i32 %52, %mul22
  %arrayidx24 = getelementptr inbounds i8, i8* %50, i32 %add23
  store i8* %arrayidx24, i8** %ptrB, align 4, !tbaa !2
  %56 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc, %for.body
  %57 = load i32, i32* %i, align 4, !tbaa !35
  %58 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %58, i32 0, i32 0
  %59 = load i32, i32* %width, align 4, !tbaa !41
  %cmp26 = icmp ult i32 %57, %59
  br i1 %cmp26, label %for.body29, label %for.cond.cleanup28

for.cond.cleanup28:                               ; preds = %for.cond25
  store i32 5, i32* %cleanup.dest.slot, align 4
  %60 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  br label %for.end

for.body29:                                       ; preds = %for.cond25
  %61 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %61) #3
  %62 = load i16*, i16** %ptrY, align 4, !tbaa !2
  %63 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx30 = getelementptr inbounds i16, i16* %62, i32 %63
  %64 = load i16, i16* %arrayidx30, align 2, !tbaa !43
  %conv31 = zext i16 %64 to i32
  %65 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv32 = zext i16 %65 to i32
  %cmp33 = icmp slt i32 %conv31, %conv32
  br i1 %cmp33, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body29
  %66 = load i16*, i16** %ptrY, align 4, !tbaa !2
  %67 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx35 = getelementptr inbounds i16, i16* %66, i32 %67
  %68 = load i16, i16* %arrayidx35, align 2, !tbaa !43
  %conv36 = zext i16 %68 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body29
  %69 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv37 = zext i16 %69 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv36, %cond.true ], [ %conv37, %cond.false ]
  %conv38 = trunc i32 %cond to i16
  store i16 %conv38, i16* %unormY, align 2, !tbaa !43
  %70 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #3
  %71 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %72 = load i16, i16* %unormY, align 2, !tbaa !43
  %idxprom = zext i16 %72 to i32
  %arrayidx39 = getelementptr inbounds float, float* %71, i32 %idxprom
  %73 = load float, float* %arrayidx39, align 4, !tbaa !36
  store float %73, float* %Y, align 4, !tbaa !36
  %74 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #3
  store float 0.000000e+00, float* %Cb, align 4, !tbaa !36
  %75 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #3
  store float 0.000000e+00, float* %Cr, align 4, !tbaa !36
  %76 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #3
  %77 = load float, float* %Y, align 4, !tbaa !36
  %78 = load float, float* %kr, align 4, !tbaa !36
  %sub40 = fsub float 1.000000e+00, %78
  %mul41 = fmul float 2.000000e+00, %sub40
  %mul42 = fmul float %mul41, 0.000000e+00
  %add43 = fadd float %77, %mul42
  store float %add43, float* %R, align 4, !tbaa !36
  %79 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #3
  %80 = load float, float* %Y, align 4, !tbaa !36
  %81 = load float, float* %kb, align 4, !tbaa !36
  %sub44 = fsub float 1.000000e+00, %81
  %mul45 = fmul float 2.000000e+00, %sub44
  %mul46 = fmul float %mul45, 0.000000e+00
  %add47 = fadd float %80, %mul46
  store float %add47, float* %B, align 4, !tbaa !36
  %82 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #3
  %83 = load float, float* %Y, align 4, !tbaa !36
  %84 = load float, float* %kr, align 4, !tbaa !36
  %85 = load float, float* %kr, align 4, !tbaa !36
  %sub48 = fsub float 1.000000e+00, %85
  %mul49 = fmul float %84, %sub48
  %mul50 = fmul float %mul49, 0.000000e+00
  %86 = load float, float* %kb, align 4, !tbaa !36
  %87 = load float, float* %kb, align 4, !tbaa !36
  %sub51 = fsub float 1.000000e+00, %87
  %mul52 = fmul float %86, %sub51
  %mul53 = fmul float %mul52, 0.000000e+00
  %add54 = fadd float %mul50, %mul53
  %mul55 = fmul float 2.000000e+00, %add54
  %88 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul55, %88
  %sub56 = fsub float %83, %div
  store float %sub56, float* %G, align 4, !tbaa !36
  %89 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #3
  %90 = load float, float* %R, align 4, !tbaa !36
  %cmp57 = fcmp olt float %90, 0.000000e+00
  br i1 %cmp57, label %cond.true59, label %cond.false60

cond.true59:                                      ; preds = %cond.end
  br label %cond.end67

cond.false60:                                     ; preds = %cond.end
  %91 = load float, float* %R, align 4, !tbaa !36
  %cmp61 = fcmp olt float 1.000000e+00, %91
  br i1 %cmp61, label %cond.true63, label %cond.false64

cond.true63:                                      ; preds = %cond.false60
  br label %cond.end65

cond.false64:                                     ; preds = %cond.false60
  %92 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end65

cond.end65:                                       ; preds = %cond.false64, %cond.true63
  %cond66 = phi float [ 1.000000e+00, %cond.true63 ], [ %92, %cond.false64 ]
  br label %cond.end67

cond.end67:                                       ; preds = %cond.end65, %cond.true59
  %cond68 = phi float [ 0.000000e+00, %cond.true59 ], [ %cond66, %cond.end65 ]
  store float %cond68, float* %Rc, align 4, !tbaa !36
  %93 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #3
  %94 = load float, float* %G, align 4, !tbaa !36
  %cmp69 = fcmp olt float %94, 0.000000e+00
  br i1 %cmp69, label %cond.true71, label %cond.false72

cond.true71:                                      ; preds = %cond.end67
  br label %cond.end79

cond.false72:                                     ; preds = %cond.end67
  %95 = load float, float* %G, align 4, !tbaa !36
  %cmp73 = fcmp olt float 1.000000e+00, %95
  br i1 %cmp73, label %cond.true75, label %cond.false76

cond.true75:                                      ; preds = %cond.false72
  br label %cond.end77

cond.false76:                                     ; preds = %cond.false72
  %96 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end77

cond.end77:                                       ; preds = %cond.false76, %cond.true75
  %cond78 = phi float [ 1.000000e+00, %cond.true75 ], [ %96, %cond.false76 ]
  br label %cond.end79

cond.end79:                                       ; preds = %cond.end77, %cond.true71
  %cond80 = phi float [ 0.000000e+00, %cond.true71 ], [ %cond78, %cond.end77 ]
  store float %cond80, float* %Gc, align 4, !tbaa !36
  %97 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #3
  %98 = load float, float* %B, align 4, !tbaa !36
  %cmp81 = fcmp olt float %98, 0.000000e+00
  br i1 %cmp81, label %cond.true83, label %cond.false84

cond.true83:                                      ; preds = %cond.end79
  br label %cond.end91

cond.false84:                                     ; preds = %cond.end79
  %99 = load float, float* %B, align 4, !tbaa !36
  %cmp85 = fcmp olt float 1.000000e+00, %99
  br i1 %cmp85, label %cond.true87, label %cond.false88

cond.true87:                                      ; preds = %cond.false84
  br label %cond.end89

cond.false88:                                     ; preds = %cond.false84
  %100 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end89

cond.end89:                                       ; preds = %cond.false88, %cond.true87
  %cond90 = phi float [ 1.000000e+00, %cond.true87 ], [ %100, %cond.false88 ]
  br label %cond.end91

cond.end91:                                       ; preds = %cond.end89, %cond.true83
  %cond92 = phi float [ 0.000000e+00, %cond.true83 ], [ %cond90, %cond.end89 ]
  store float %cond92, float* %Bc, align 4, !tbaa !36
  %101 = load float, float* %Rc, align 4, !tbaa !36
  %102 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul93 = fmul float %101, %102
  %add94 = fadd float 5.000000e-01, %mul93
  %conv95 = fptoui float %add94 to i16
  %103 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %104 = bitcast i8* %103 to i16*
  store i16 %conv95, i16* %104, align 2, !tbaa !43
  %105 = load float, float* %Gc, align 4, !tbaa !36
  %106 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul96 = fmul float %105, %106
  %add97 = fadd float 5.000000e-01, %mul96
  %conv98 = fptoui float %add97 to i16
  %107 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %108 = bitcast i8* %107 to i16*
  store i16 %conv98, i16* %108, align 2, !tbaa !43
  %109 = load float, float* %Bc, align 4, !tbaa !36
  %110 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul99 = fmul float %109, %110
  %add100 = fadd float 5.000000e-01, %mul99
  %conv101 = fptoui float %add100 to i16
  %111 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %112 = bitcast i8* %111 to i16*
  store i16 %conv101, i16* %112, align 2, !tbaa !43
  %113 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %114 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %114, i32 %113
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %115 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %116 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr102 = getelementptr inbounds i8, i8* %116, i32 %115
  store i8* %add.ptr102, i8** %ptrG, align 4, !tbaa !2
  %117 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %118 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr103 = getelementptr inbounds i8, i8* %118, i32 %117
  store i8* %add.ptr103, i8** %ptrB, align 4, !tbaa !2
  %119 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #3
  %120 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  %122 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  %126 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %128) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end91
  %129 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %129, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond25

for.end:                                          ; preds = %for.cond.cleanup28
  %130 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #3
  %131 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #3
  %132 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #3
  %133 = bitcast i16** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #3
  br label %for.inc104

for.inc104:                                       ; preds = %for.end
  %134 = load i32, i32* %j, align 4, !tbaa !35
  %inc105 = add i32 %134, 1
  store i32 %inc105, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end106:                                       ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %135 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #3
  %136 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %136) #3
  %137 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #3
  %138 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #3
  %139 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #3
  %140 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #3
  %141 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUV16ToRGB8Color(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %rgbPixelBytes = alloca i32, align 4
  %unormFloatTableY = alloca float*, align 4
  %unormFloatTableUV = alloca float*, align 4
  %yuvMaxChannel = alloca i16, align 2
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %uvJ = alloca i32, align 4
  %ptrY = alloca i16*, align 4
  %ptrU = alloca i16*, align 4
  %ptrV = alloca i16*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %uvI = alloca i32, align 4
  %unormY = alloca i16, align 2
  %unormU = alloca i16, align 2
  %unormV = alloca i16, align 2
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %R = alloca float, align 4
  %B = alloca float, align 4
  %G = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 6
  %11 = load i32, i32* %rgbPixelBytes4, align 4, !tbaa !30
  store i32 %11, i32* %rgbPixelBytes, align 4, !tbaa !35
  %12 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %13, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY5, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %14 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableUV6 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %15, i32 0, i32 13
  %arraydecay7 = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableUV6, i32 0, i32 0
  store float* %arraydecay7, float** %unormFloatTableUV, align 4, !tbaa !2
  %16 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #3
  %17 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %17, i32 0, i32 2
  %18 = load i32, i32* %depth, align 4, !tbaa !6
  %shl = shl i32 1, %18
  %sub = sub nsw i32 %shl, 1
  %conv = trunc i32 %sub to i16
  store i16 %conv, i16* %yuvMaxChannel, align 2, !tbaa !43
  %19 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth8 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %20, i32 0, i32 2
  %21 = load i32, i32* %depth8, align 4, !tbaa !15
  %shl9 = shl i32 1, %21
  %sub10 = sub nsw i32 %shl9, 1
  %conv11 = sitofp i32 %sub10 to float
  store float %conv11, float* %rgbMaxChannel, align 4, !tbaa !36
  %22 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc150, %entry
  %23 = load i32, i32* %j, align 4, !tbaa !35
  %24 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %24, i32 0, i32 1
  %25 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %23, %25
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %26 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  br label %for.end152

for.body:                                         ; preds = %for.cond
  %27 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = load i32, i32* %j, align 4, !tbaa !35
  %29 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %29, i32 0, i32 11
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %30 = load i32, i32* %chromaShiftY, align 4, !tbaa !71
  %shr = lshr i32 %28, %30
  store i32 %shr, i32* %uvJ, align 4, !tbaa !35
  %31 = bitcast i16** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #3
  %32 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %33 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %j, align 4, !tbaa !35
  %35 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %35, i32 0, i32 7
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %36 = load i32, i32* %arrayidx13, align 4, !tbaa !35
  %mul = mul i32 %34, %36
  %arrayidx14 = getelementptr inbounds i8, i8* %33, i32 %mul
  %37 = bitcast i8* %arrayidx14 to i16*
  store i16* %37, i16** %ptrY, align 4, !tbaa !2
  %38 = bitcast i16** %ptrU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #3
  %39 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes15 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %39, i32 0, i32 6
  %arrayidx16 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes15, i32 0, i32 1
  %40 = load i8*, i8** %arrayidx16, align 4, !tbaa !2
  %41 = load i32, i32* %uvJ, align 4, !tbaa !35
  %42 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes17 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %42, i32 0, i32 7
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes17, i32 0, i32 1
  %43 = load i32, i32* %arrayidx18, align 4, !tbaa !35
  %mul19 = mul i32 %41, %43
  %arrayidx20 = getelementptr inbounds i8, i8* %40, i32 %mul19
  %44 = bitcast i8* %arrayidx20 to i16*
  store i16* %44, i16** %ptrU, align 4, !tbaa !2
  %45 = bitcast i16** %ptrV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #3
  %46 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes21 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %46, i32 0, i32 6
  %arrayidx22 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes21, i32 0, i32 2
  %47 = load i8*, i8** %arrayidx22, align 4, !tbaa !2
  %48 = load i32, i32* %uvJ, align 4, !tbaa !35
  %49 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes23 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %49, i32 0, i32 7
  %arrayidx24 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes23, i32 0, i32 2
  %50 = load i32, i32* %arrayidx24, align 4, !tbaa !35
  %mul25 = mul i32 %48, %50
  %arrayidx26 = getelementptr inbounds i8, i8* %47, i32 %mul25
  %51 = bitcast i8* %arrayidx26 to i16*
  store i16* %51, i16** %ptrV, align 4, !tbaa !2
  %52 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %53, i32 0, i32 6
  %54 = load i8*, i8** %pixels, align 4, !tbaa !38
  %55 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %55, i32 0, i32 7
  %56 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %57 = load i32, i32* %j, align 4, !tbaa !35
  %58 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %58, i32 0, i32 7
  %59 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul27 = mul i32 %57, %59
  %add = add i32 %56, %mul27
  %arrayidx28 = getelementptr inbounds i8, i8* %54, i32 %add
  store i8* %arrayidx28, i8** %ptrR, align 4, !tbaa !2
  %60 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #3
  %61 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels29 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %61, i32 0, i32 6
  %62 = load i8*, i8** %pixels29, align 4, !tbaa !38
  %63 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %63, i32 0, i32 8
  %64 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %65 = load i32, i32* %j, align 4, !tbaa !35
  %66 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes30 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %66, i32 0, i32 7
  %67 = load i32, i32* %rowBytes30, align 4, !tbaa !42
  %mul31 = mul i32 %65, %67
  %add32 = add i32 %64, %mul31
  %arrayidx33 = getelementptr inbounds i8, i8* %62, i32 %add32
  store i8* %arrayidx33, i8** %ptrG, align 4, !tbaa !2
  %68 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels34 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %69, i32 0, i32 6
  %70 = load i8*, i8** %pixels34, align 4, !tbaa !38
  %71 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %71, i32 0, i32 9
  %72 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %73 = load i32, i32* %j, align 4, !tbaa !35
  %74 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes35 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %74, i32 0, i32 7
  %75 = load i32, i32* %rowBytes35, align 4, !tbaa !42
  %mul36 = mul i32 %73, %75
  %add37 = add i32 %72, %mul36
  %arrayidx38 = getelementptr inbounds i8, i8* %70, i32 %add37
  store i8* %arrayidx38, i8** %ptrB, align 4, !tbaa !2
  %76 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc, %for.body
  %77 = load i32, i32* %i, align 4, !tbaa !35
  %78 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %78, i32 0, i32 0
  %79 = load i32, i32* %width, align 4, !tbaa !41
  %cmp40 = icmp ult i32 %77, %79
  br i1 %cmp40, label %for.body43, label %for.cond.cleanup42

for.cond.cleanup42:                               ; preds = %for.cond39
  store i32 5, i32* %cleanup.dest.slot, align 4
  %80 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #3
  br label %for.end

for.body43:                                       ; preds = %for.cond39
  %81 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #3
  %82 = load i32, i32* %i, align 4, !tbaa !35
  %83 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo44 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %83, i32 0, i32 11
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo44, i32 0, i32 1
  %84 = load i32, i32* %chromaShiftX, align 4, !tbaa !72
  %shr45 = lshr i32 %82, %84
  store i32 %shr45, i32* %uvI, align 4, !tbaa !35
  %85 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %85) #3
  %86 = load i16*, i16** %ptrY, align 4, !tbaa !2
  %87 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx46 = getelementptr inbounds i16, i16* %86, i32 %87
  %88 = load i16, i16* %arrayidx46, align 2, !tbaa !43
  %conv47 = zext i16 %88 to i32
  %89 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv48 = zext i16 %89 to i32
  %cmp49 = icmp slt i32 %conv47, %conv48
  br i1 %cmp49, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body43
  %90 = load i16*, i16** %ptrY, align 4, !tbaa !2
  %91 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx51 = getelementptr inbounds i16, i16* %90, i32 %91
  %92 = load i16, i16* %arrayidx51, align 2, !tbaa !43
  %conv52 = zext i16 %92 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body43
  %93 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv53 = zext i16 %93 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv52, %cond.true ], [ %conv53, %cond.false ]
  %conv54 = trunc i32 %cond to i16
  store i16 %conv54, i16* %unormY, align 2, !tbaa !43
  %94 = bitcast i16* %unormU to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %94) #3
  %95 = load i16*, i16** %ptrU, align 4, !tbaa !2
  %96 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx55 = getelementptr inbounds i16, i16* %95, i32 %96
  %97 = load i16, i16* %arrayidx55, align 2, !tbaa !43
  %conv56 = zext i16 %97 to i32
  %98 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv57 = zext i16 %98 to i32
  %cmp58 = icmp slt i32 %conv56, %conv57
  br i1 %cmp58, label %cond.true60, label %cond.false63

cond.true60:                                      ; preds = %cond.end
  %99 = load i16*, i16** %ptrU, align 4, !tbaa !2
  %100 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx61 = getelementptr inbounds i16, i16* %99, i32 %100
  %101 = load i16, i16* %arrayidx61, align 2, !tbaa !43
  %conv62 = zext i16 %101 to i32
  br label %cond.end65

cond.false63:                                     ; preds = %cond.end
  %102 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv64 = zext i16 %102 to i32
  br label %cond.end65

cond.end65:                                       ; preds = %cond.false63, %cond.true60
  %cond66 = phi i32 [ %conv62, %cond.true60 ], [ %conv64, %cond.false63 ]
  %conv67 = trunc i32 %cond66 to i16
  store i16 %conv67, i16* %unormU, align 2, !tbaa !43
  %103 = bitcast i16* %unormV to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %103) #3
  %104 = load i16*, i16** %ptrV, align 4, !tbaa !2
  %105 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx68 = getelementptr inbounds i16, i16* %104, i32 %105
  %106 = load i16, i16* %arrayidx68, align 2, !tbaa !43
  %conv69 = zext i16 %106 to i32
  %107 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv70 = zext i16 %107 to i32
  %cmp71 = icmp slt i32 %conv69, %conv70
  br i1 %cmp71, label %cond.true73, label %cond.false76

cond.true73:                                      ; preds = %cond.end65
  %108 = load i16*, i16** %ptrV, align 4, !tbaa !2
  %109 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx74 = getelementptr inbounds i16, i16* %108, i32 %109
  %110 = load i16, i16* %arrayidx74, align 2, !tbaa !43
  %conv75 = zext i16 %110 to i32
  br label %cond.end78

cond.false76:                                     ; preds = %cond.end65
  %111 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv77 = zext i16 %111 to i32
  br label %cond.end78

cond.end78:                                       ; preds = %cond.false76, %cond.true73
  %cond79 = phi i32 [ %conv75, %cond.true73 ], [ %conv77, %cond.false76 ]
  %conv80 = trunc i32 %cond79 to i16
  store i16 %conv80, i16* %unormV, align 2, !tbaa !43
  %112 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #3
  %113 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %114 = load i16, i16* %unormY, align 2, !tbaa !43
  %idxprom = zext i16 %114 to i32
  %arrayidx81 = getelementptr inbounds float, float* %113, i32 %idxprom
  %115 = load float, float* %arrayidx81, align 4, !tbaa !36
  store float %115, float* %Y, align 4, !tbaa !36
  %116 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #3
  %117 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %118 = load i16, i16* %unormU, align 2, !tbaa !43
  %idxprom82 = zext i16 %118 to i32
  %arrayidx83 = getelementptr inbounds float, float* %117, i32 %idxprom82
  %119 = load float, float* %arrayidx83, align 4, !tbaa !36
  store float %119, float* %Cb, align 4, !tbaa !36
  %120 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #3
  %121 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %122 = load i16, i16* %unormV, align 2, !tbaa !43
  %idxprom84 = zext i16 %122 to i32
  %arrayidx85 = getelementptr inbounds float, float* %121, i32 %idxprom84
  %123 = load float, float* %arrayidx85, align 4, !tbaa !36
  store float %123, float* %Cr, align 4, !tbaa !36
  %124 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #3
  %125 = load float, float* %Y, align 4, !tbaa !36
  %126 = load float, float* %kr, align 4, !tbaa !36
  %sub86 = fsub float 1.000000e+00, %126
  %mul87 = fmul float 2.000000e+00, %sub86
  %127 = load float, float* %Cr, align 4, !tbaa !36
  %mul88 = fmul float %mul87, %127
  %add89 = fadd float %125, %mul88
  store float %add89, float* %R, align 4, !tbaa !36
  %128 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #3
  %129 = load float, float* %Y, align 4, !tbaa !36
  %130 = load float, float* %kb, align 4, !tbaa !36
  %sub90 = fsub float 1.000000e+00, %130
  %mul91 = fmul float 2.000000e+00, %sub90
  %131 = load float, float* %Cb, align 4, !tbaa !36
  %mul92 = fmul float %mul91, %131
  %add93 = fadd float %129, %mul92
  store float %add93, float* %B, align 4, !tbaa !36
  %132 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %132) #3
  %133 = load float, float* %Y, align 4, !tbaa !36
  %134 = load float, float* %kr, align 4, !tbaa !36
  %135 = load float, float* %kr, align 4, !tbaa !36
  %sub94 = fsub float 1.000000e+00, %135
  %mul95 = fmul float %134, %sub94
  %136 = load float, float* %Cr, align 4, !tbaa !36
  %mul96 = fmul float %mul95, %136
  %137 = load float, float* %kb, align 4, !tbaa !36
  %138 = load float, float* %kb, align 4, !tbaa !36
  %sub97 = fsub float 1.000000e+00, %138
  %mul98 = fmul float %137, %sub97
  %139 = load float, float* %Cb, align 4, !tbaa !36
  %mul99 = fmul float %mul98, %139
  %add100 = fadd float %mul96, %mul99
  %mul101 = fmul float 2.000000e+00, %add100
  %140 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul101, %140
  %sub102 = fsub float %133, %div
  store float %sub102, float* %G, align 4, !tbaa !36
  %141 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %141) #3
  %142 = load float, float* %R, align 4, !tbaa !36
  %cmp103 = fcmp olt float %142, 0.000000e+00
  br i1 %cmp103, label %cond.true105, label %cond.false106

cond.true105:                                     ; preds = %cond.end78
  br label %cond.end113

cond.false106:                                    ; preds = %cond.end78
  %143 = load float, float* %R, align 4, !tbaa !36
  %cmp107 = fcmp olt float 1.000000e+00, %143
  br i1 %cmp107, label %cond.true109, label %cond.false110

cond.true109:                                     ; preds = %cond.false106
  br label %cond.end111

cond.false110:                                    ; preds = %cond.false106
  %144 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end111

cond.end111:                                      ; preds = %cond.false110, %cond.true109
  %cond112 = phi float [ 1.000000e+00, %cond.true109 ], [ %144, %cond.false110 ]
  br label %cond.end113

cond.end113:                                      ; preds = %cond.end111, %cond.true105
  %cond114 = phi float [ 0.000000e+00, %cond.true105 ], [ %cond112, %cond.end111 ]
  store float %cond114, float* %Rc, align 4, !tbaa !36
  %145 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #3
  %146 = load float, float* %G, align 4, !tbaa !36
  %cmp115 = fcmp olt float %146, 0.000000e+00
  br i1 %cmp115, label %cond.true117, label %cond.false118

cond.true117:                                     ; preds = %cond.end113
  br label %cond.end125

cond.false118:                                    ; preds = %cond.end113
  %147 = load float, float* %G, align 4, !tbaa !36
  %cmp119 = fcmp olt float 1.000000e+00, %147
  br i1 %cmp119, label %cond.true121, label %cond.false122

cond.true121:                                     ; preds = %cond.false118
  br label %cond.end123

cond.false122:                                    ; preds = %cond.false118
  %148 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end123

cond.end123:                                      ; preds = %cond.false122, %cond.true121
  %cond124 = phi float [ 1.000000e+00, %cond.true121 ], [ %148, %cond.false122 ]
  br label %cond.end125

cond.end125:                                      ; preds = %cond.end123, %cond.true117
  %cond126 = phi float [ 0.000000e+00, %cond.true117 ], [ %cond124, %cond.end123 ]
  store float %cond126, float* %Gc, align 4, !tbaa !36
  %149 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #3
  %150 = load float, float* %B, align 4, !tbaa !36
  %cmp127 = fcmp olt float %150, 0.000000e+00
  br i1 %cmp127, label %cond.true129, label %cond.false130

cond.true129:                                     ; preds = %cond.end125
  br label %cond.end137

cond.false130:                                    ; preds = %cond.end125
  %151 = load float, float* %B, align 4, !tbaa !36
  %cmp131 = fcmp olt float 1.000000e+00, %151
  br i1 %cmp131, label %cond.true133, label %cond.false134

cond.true133:                                     ; preds = %cond.false130
  br label %cond.end135

cond.false134:                                    ; preds = %cond.false130
  %152 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end135

cond.end135:                                      ; preds = %cond.false134, %cond.true133
  %cond136 = phi float [ 1.000000e+00, %cond.true133 ], [ %152, %cond.false134 ]
  br label %cond.end137

cond.end137:                                      ; preds = %cond.end135, %cond.true129
  %cond138 = phi float [ 0.000000e+00, %cond.true129 ], [ %cond136, %cond.end135 ]
  store float %cond138, float* %Bc, align 4, !tbaa !36
  %153 = load float, float* %Rc, align 4, !tbaa !36
  %154 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul139 = fmul float %153, %154
  %add140 = fadd float 5.000000e-01, %mul139
  %conv141 = fptoui float %add140 to i8
  %155 = load i8*, i8** %ptrR, align 4, !tbaa !2
  store i8 %conv141, i8* %155, align 1, !tbaa !45
  %156 = load float, float* %Gc, align 4, !tbaa !36
  %157 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul142 = fmul float %156, %157
  %add143 = fadd float 5.000000e-01, %mul142
  %conv144 = fptoui float %add143 to i8
  %158 = load i8*, i8** %ptrG, align 4, !tbaa !2
  store i8 %conv144, i8* %158, align 1, !tbaa !45
  %159 = load float, float* %Bc, align 4, !tbaa !36
  %160 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul145 = fmul float %159, %160
  %add146 = fadd float 5.000000e-01, %mul145
  %conv147 = fptoui float %add146 to i8
  %161 = load i8*, i8** %ptrB, align 4, !tbaa !2
  store i8 %conv147, i8* %161, align 1, !tbaa !45
  %162 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %163 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %163, i32 %162
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %164 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %165 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr148 = getelementptr inbounds i8, i8* %165, i32 %164
  store i8* %add.ptr148, i8** %ptrG, align 4, !tbaa !2
  %166 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %167 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr149 = getelementptr inbounds i8, i8* %167, i32 %166
  store i8* %add.ptr149, i8** %ptrB, align 4, !tbaa !2
  %168 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #3
  %169 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #3
  %170 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #3
  %171 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #3
  %172 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #3
  %173 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #3
  %174 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #3
  %175 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #3
  %176 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #3
  %177 = bitcast i16* %unormV to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %177) #3
  %178 = bitcast i16* %unormU to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %178) #3
  %179 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %179) #3
  %180 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end137
  %181 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %181, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond39

for.end:                                          ; preds = %for.cond.cleanup42
  %182 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #3
  %183 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #3
  %184 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #3
  %185 = bitcast i16** %ptrV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #3
  %186 = bitcast i16** %ptrU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #3
  %187 = bitcast i16** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #3
  %188 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #3
  br label %for.inc150

for.inc150:                                       ; preds = %for.end
  %189 = load i32, i32* %j, align 4, !tbaa !35
  %inc151 = add i32 %189, 1
  store i32 %inc151, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end152:                                       ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %190 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #3
  %191 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %191) #3
  %192 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #3
  %193 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #3
  %194 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #3
  %195 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #3
  %196 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #3
  %197 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUV16ToRGB8Mono(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %rgbPixelBytes = alloca i32, align 4
  %unormFloatTableY = alloca float*, align 4
  %yuvMaxChannel = alloca i16, align 2
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ptrY = alloca i16*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %unormY = alloca i16, align 2
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %R = alloca float, align 4
  %B = alloca float, align 4
  %G = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 6
  %11 = load i32, i32* %rgbPixelBytes4, align 4, !tbaa !30
  store i32 %11, i32* %rgbPixelBytes, align 4, !tbaa !35
  %12 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %13, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY5, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %14 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %14) #3
  %15 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %15, i32 0, i32 2
  %16 = load i32, i32* %depth, align 4, !tbaa !6
  %shl = shl i32 1, %16
  %sub = sub nsw i32 %shl, 1
  %conv = trunc i32 %sub to i16
  store i16 %conv, i16* %yuvMaxChannel, align 2, !tbaa !43
  %17 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth6 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %18, i32 0, i32 2
  %19 = load i32, i32* %depth6, align 4, !tbaa !15
  %shl7 = shl i32 1, %19
  %sub8 = sub nsw i32 %shl7, 1
  %conv9 = sitofp i32 %sub8 to float
  store float %conv9, float* %rgbMaxChannel, align 4, !tbaa !36
  %20 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc104, %entry
  %21 = load i32, i32* %j, align 4, !tbaa !35
  %22 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %22, i32 0, i32 1
  %23 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %21, %23
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  br label %for.end106

for.body:                                         ; preds = %for.cond
  %25 = bitcast i16** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %27 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %28 = load i32, i32* %j, align 4, !tbaa !35
  %29 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %29, i32 0, i32 7
  %arrayidx11 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %30 = load i32, i32* %arrayidx11, align 4, !tbaa !35
  %mul = mul i32 %28, %30
  %arrayidx12 = getelementptr inbounds i8, i8* %27, i32 %mul
  %31 = bitcast i8* %arrayidx12 to i16*
  store i16* %31, i16** %ptrY, align 4, !tbaa !2
  %32 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %33, i32 0, i32 6
  %34 = load i8*, i8** %pixels, align 4, !tbaa !38
  %35 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %35, i32 0, i32 7
  %36 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %37 = load i32, i32* %j, align 4, !tbaa !35
  %38 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %38, i32 0, i32 7
  %39 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul13 = mul i32 %37, %39
  %add = add i32 %36, %mul13
  %arrayidx14 = getelementptr inbounds i8, i8* %34, i32 %add
  store i8* %arrayidx14, i8** %ptrR, align 4, !tbaa !2
  %40 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels15 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %41, i32 0, i32 6
  %42 = load i8*, i8** %pixels15, align 4, !tbaa !38
  %43 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %43, i32 0, i32 8
  %44 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %45 = load i32, i32* %j, align 4, !tbaa !35
  %46 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes16 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %46, i32 0, i32 7
  %47 = load i32, i32* %rowBytes16, align 4, !tbaa !42
  %mul17 = mul i32 %45, %47
  %add18 = add i32 %44, %mul17
  %arrayidx19 = getelementptr inbounds i8, i8* %42, i32 %add18
  store i8* %arrayidx19, i8** %ptrG, align 4, !tbaa !2
  %48 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #3
  %49 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels20 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %49, i32 0, i32 6
  %50 = load i8*, i8** %pixels20, align 4, !tbaa !38
  %51 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %51, i32 0, i32 9
  %52 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %53 = load i32, i32* %j, align 4, !tbaa !35
  %54 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes21 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %54, i32 0, i32 7
  %55 = load i32, i32* %rowBytes21, align 4, !tbaa !42
  %mul22 = mul i32 %53, %55
  %add23 = add i32 %52, %mul22
  %arrayidx24 = getelementptr inbounds i8, i8* %50, i32 %add23
  store i8* %arrayidx24, i8** %ptrB, align 4, !tbaa !2
  %56 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc, %for.body
  %57 = load i32, i32* %i, align 4, !tbaa !35
  %58 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %58, i32 0, i32 0
  %59 = load i32, i32* %width, align 4, !tbaa !41
  %cmp26 = icmp ult i32 %57, %59
  br i1 %cmp26, label %for.body29, label %for.cond.cleanup28

for.cond.cleanup28:                               ; preds = %for.cond25
  store i32 5, i32* %cleanup.dest.slot, align 4
  %60 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  br label %for.end

for.body29:                                       ; preds = %for.cond25
  %61 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %61) #3
  %62 = load i16*, i16** %ptrY, align 4, !tbaa !2
  %63 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx30 = getelementptr inbounds i16, i16* %62, i32 %63
  %64 = load i16, i16* %arrayidx30, align 2, !tbaa !43
  %conv31 = zext i16 %64 to i32
  %65 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv32 = zext i16 %65 to i32
  %cmp33 = icmp slt i32 %conv31, %conv32
  br i1 %cmp33, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body29
  %66 = load i16*, i16** %ptrY, align 4, !tbaa !2
  %67 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx35 = getelementptr inbounds i16, i16* %66, i32 %67
  %68 = load i16, i16* %arrayidx35, align 2, !tbaa !43
  %conv36 = zext i16 %68 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body29
  %69 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv37 = zext i16 %69 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv36, %cond.true ], [ %conv37, %cond.false ]
  %conv38 = trunc i32 %cond to i16
  store i16 %conv38, i16* %unormY, align 2, !tbaa !43
  %70 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #3
  %71 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %72 = load i16, i16* %unormY, align 2, !tbaa !43
  %idxprom = zext i16 %72 to i32
  %arrayidx39 = getelementptr inbounds float, float* %71, i32 %idxprom
  %73 = load float, float* %arrayidx39, align 4, !tbaa !36
  store float %73, float* %Y, align 4, !tbaa !36
  %74 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #3
  store float 0.000000e+00, float* %Cb, align 4, !tbaa !36
  %75 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #3
  store float 0.000000e+00, float* %Cr, align 4, !tbaa !36
  %76 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #3
  %77 = load float, float* %Y, align 4, !tbaa !36
  %78 = load float, float* %kr, align 4, !tbaa !36
  %sub40 = fsub float 1.000000e+00, %78
  %mul41 = fmul float 2.000000e+00, %sub40
  %mul42 = fmul float %mul41, 0.000000e+00
  %add43 = fadd float %77, %mul42
  store float %add43, float* %R, align 4, !tbaa !36
  %79 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #3
  %80 = load float, float* %Y, align 4, !tbaa !36
  %81 = load float, float* %kb, align 4, !tbaa !36
  %sub44 = fsub float 1.000000e+00, %81
  %mul45 = fmul float 2.000000e+00, %sub44
  %mul46 = fmul float %mul45, 0.000000e+00
  %add47 = fadd float %80, %mul46
  store float %add47, float* %B, align 4, !tbaa !36
  %82 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #3
  %83 = load float, float* %Y, align 4, !tbaa !36
  %84 = load float, float* %kr, align 4, !tbaa !36
  %85 = load float, float* %kr, align 4, !tbaa !36
  %sub48 = fsub float 1.000000e+00, %85
  %mul49 = fmul float %84, %sub48
  %mul50 = fmul float %mul49, 0.000000e+00
  %86 = load float, float* %kb, align 4, !tbaa !36
  %87 = load float, float* %kb, align 4, !tbaa !36
  %sub51 = fsub float 1.000000e+00, %87
  %mul52 = fmul float %86, %sub51
  %mul53 = fmul float %mul52, 0.000000e+00
  %add54 = fadd float %mul50, %mul53
  %mul55 = fmul float 2.000000e+00, %add54
  %88 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul55, %88
  %sub56 = fsub float %83, %div
  store float %sub56, float* %G, align 4, !tbaa !36
  %89 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #3
  %90 = load float, float* %R, align 4, !tbaa !36
  %cmp57 = fcmp olt float %90, 0.000000e+00
  br i1 %cmp57, label %cond.true59, label %cond.false60

cond.true59:                                      ; preds = %cond.end
  br label %cond.end67

cond.false60:                                     ; preds = %cond.end
  %91 = load float, float* %R, align 4, !tbaa !36
  %cmp61 = fcmp olt float 1.000000e+00, %91
  br i1 %cmp61, label %cond.true63, label %cond.false64

cond.true63:                                      ; preds = %cond.false60
  br label %cond.end65

cond.false64:                                     ; preds = %cond.false60
  %92 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end65

cond.end65:                                       ; preds = %cond.false64, %cond.true63
  %cond66 = phi float [ 1.000000e+00, %cond.true63 ], [ %92, %cond.false64 ]
  br label %cond.end67

cond.end67:                                       ; preds = %cond.end65, %cond.true59
  %cond68 = phi float [ 0.000000e+00, %cond.true59 ], [ %cond66, %cond.end65 ]
  store float %cond68, float* %Rc, align 4, !tbaa !36
  %93 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #3
  %94 = load float, float* %G, align 4, !tbaa !36
  %cmp69 = fcmp olt float %94, 0.000000e+00
  br i1 %cmp69, label %cond.true71, label %cond.false72

cond.true71:                                      ; preds = %cond.end67
  br label %cond.end79

cond.false72:                                     ; preds = %cond.end67
  %95 = load float, float* %G, align 4, !tbaa !36
  %cmp73 = fcmp olt float 1.000000e+00, %95
  br i1 %cmp73, label %cond.true75, label %cond.false76

cond.true75:                                      ; preds = %cond.false72
  br label %cond.end77

cond.false76:                                     ; preds = %cond.false72
  %96 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end77

cond.end77:                                       ; preds = %cond.false76, %cond.true75
  %cond78 = phi float [ 1.000000e+00, %cond.true75 ], [ %96, %cond.false76 ]
  br label %cond.end79

cond.end79:                                       ; preds = %cond.end77, %cond.true71
  %cond80 = phi float [ 0.000000e+00, %cond.true71 ], [ %cond78, %cond.end77 ]
  store float %cond80, float* %Gc, align 4, !tbaa !36
  %97 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #3
  %98 = load float, float* %B, align 4, !tbaa !36
  %cmp81 = fcmp olt float %98, 0.000000e+00
  br i1 %cmp81, label %cond.true83, label %cond.false84

cond.true83:                                      ; preds = %cond.end79
  br label %cond.end91

cond.false84:                                     ; preds = %cond.end79
  %99 = load float, float* %B, align 4, !tbaa !36
  %cmp85 = fcmp olt float 1.000000e+00, %99
  br i1 %cmp85, label %cond.true87, label %cond.false88

cond.true87:                                      ; preds = %cond.false84
  br label %cond.end89

cond.false88:                                     ; preds = %cond.false84
  %100 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end89

cond.end89:                                       ; preds = %cond.false88, %cond.true87
  %cond90 = phi float [ 1.000000e+00, %cond.true87 ], [ %100, %cond.false88 ]
  br label %cond.end91

cond.end91:                                       ; preds = %cond.end89, %cond.true83
  %cond92 = phi float [ 0.000000e+00, %cond.true83 ], [ %cond90, %cond.end89 ]
  store float %cond92, float* %Bc, align 4, !tbaa !36
  %101 = load float, float* %Rc, align 4, !tbaa !36
  %102 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul93 = fmul float %101, %102
  %add94 = fadd float 5.000000e-01, %mul93
  %conv95 = fptoui float %add94 to i8
  %103 = load i8*, i8** %ptrR, align 4, !tbaa !2
  store i8 %conv95, i8* %103, align 1, !tbaa !45
  %104 = load float, float* %Gc, align 4, !tbaa !36
  %105 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul96 = fmul float %104, %105
  %add97 = fadd float 5.000000e-01, %mul96
  %conv98 = fptoui float %add97 to i8
  %106 = load i8*, i8** %ptrG, align 4, !tbaa !2
  store i8 %conv98, i8* %106, align 1, !tbaa !45
  %107 = load float, float* %Bc, align 4, !tbaa !36
  %108 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul99 = fmul float %107, %108
  %add100 = fadd float 5.000000e-01, %mul99
  %conv101 = fptoui float %add100 to i8
  %109 = load i8*, i8** %ptrB, align 4, !tbaa !2
  store i8 %conv101, i8* %109, align 1, !tbaa !45
  %110 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %111 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %111, i32 %110
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %112 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %113 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr102 = getelementptr inbounds i8, i8* %113, i32 %112
  store i8* %add.ptr102, i8** %ptrG, align 4, !tbaa !2
  %114 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %115 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr103 = getelementptr inbounds i8, i8* %115, i32 %114
  store i8* %add.ptr103, i8** %ptrB, align 4, !tbaa !2
  %116 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  %119 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #3
  %120 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  %122 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %125) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end91
  %126 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %126, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond25

for.end:                                          ; preds = %for.cond.cleanup28
  %127 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #3
  %129 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #3
  %130 = bitcast i16** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #3
  br label %for.inc104

for.inc104:                                       ; preds = %for.end
  %131 = load i32, i32* %j, align 4, !tbaa !35
  %inc105 = add i32 %131, 1
  store i32 %inc105, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end106:                                       ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %132 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #3
  %133 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %133) #3
  %134 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #3
  %135 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #3
  %136 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #3
  %137 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #3
  %138 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUV8ToRGB16Color(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %rgbPixelBytes = alloca i32, align 4
  %unormFloatTableY = alloca float*, align 4
  %unormFloatTableUV = alloca float*, align 4
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %uvJ = alloca i32, align 4
  %ptrY = alloca i8*, align 4
  %ptrU = alloca i8*, align 4
  %ptrV = alloca i8*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %uvI = alloca i32, align 4
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %R = alloca float, align 4
  %B = alloca float, align 4
  %G = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 6
  %11 = load i32, i32* %rgbPixelBytes4, align 4, !tbaa !30
  store i32 %11, i32* %rgbPixelBytes, align 4, !tbaa !35
  %12 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %13, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY5, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %14 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableUV6 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %15, i32 0, i32 13
  %arraydecay7 = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableUV6, i32 0, i32 0
  store float* %arraydecay7, float** %unormFloatTableUV, align 4, !tbaa !2
  %16 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %17, i32 0, i32 2
  %18 = load i32, i32* %depth, align 4, !tbaa !15
  %shl = shl i32 1, %18
  %sub = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub to float
  store float %conv, float* %rgbMaxChannel, align 4, !tbaa !36
  %19 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc110, %entry
  %20 = load i32, i32* %j, align 4, !tbaa !35
  %21 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %21, i32 0, i32 1
  %22 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %20, %22
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  br label %for.end112

for.body:                                         ; preds = %for.cond
  %24 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load i32, i32* %j, align 4, !tbaa !35
  %26 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %26, i32 0, i32 11
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %27 = load i32, i32* %chromaShiftY, align 4, !tbaa !71
  %shr = lshr i32 %25, %27
  store i32 %shr, i32* %uvJ, align 4, !tbaa !35
  %28 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %29, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %30 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %31 = load i32, i32* %j, align 4, !tbaa !35
  %32 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 7
  %arrayidx9 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %33 = load i32, i32* %arrayidx9, align 4, !tbaa !35
  %mul = mul i32 %31, %33
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 %mul
  store i8* %arrayidx10, i8** %ptrY, align 4, !tbaa !2
  %34 = bitcast i8** %ptrU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #3
  %35 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes11 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %35, i32 0, i32 6
  %arrayidx12 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes11, i32 0, i32 1
  %36 = load i8*, i8** %arrayidx12, align 4, !tbaa !2
  %37 = load i32, i32* %uvJ, align 4, !tbaa !35
  %38 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes13 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %38, i32 0, i32 7
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes13, i32 0, i32 1
  %39 = load i32, i32* %arrayidx14, align 4, !tbaa !35
  %mul15 = mul i32 %37, %39
  %arrayidx16 = getelementptr inbounds i8, i8* %36, i32 %mul15
  store i8* %arrayidx16, i8** %ptrU, align 4, !tbaa !2
  %40 = bitcast i8** %ptrV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes17 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %41, i32 0, i32 6
  %arrayidx18 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes17, i32 0, i32 2
  %42 = load i8*, i8** %arrayidx18, align 4, !tbaa !2
  %43 = load i32, i32* %uvJ, align 4, !tbaa !35
  %44 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes19 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %44, i32 0, i32 7
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes19, i32 0, i32 2
  %45 = load i32, i32* %arrayidx20, align 4, !tbaa !35
  %mul21 = mul i32 %43, %45
  %arrayidx22 = getelementptr inbounds i8, i8* %42, i32 %mul21
  store i8* %arrayidx22, i8** %ptrV, align 4, !tbaa !2
  %46 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #3
  %47 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %47, i32 0, i32 6
  %48 = load i8*, i8** %pixels, align 4, !tbaa !38
  %49 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %49, i32 0, i32 7
  %50 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %51 = load i32, i32* %j, align 4, !tbaa !35
  %52 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %52, i32 0, i32 7
  %53 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul23 = mul i32 %51, %53
  %add = add i32 %50, %mul23
  %arrayidx24 = getelementptr inbounds i8, i8* %48, i32 %add
  store i8* %arrayidx24, i8** %ptrR, align 4, !tbaa !2
  %54 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #3
  %55 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels25 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %55, i32 0, i32 6
  %56 = load i8*, i8** %pixels25, align 4, !tbaa !38
  %57 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %57, i32 0, i32 8
  %58 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %59 = load i32, i32* %j, align 4, !tbaa !35
  %60 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes26 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %60, i32 0, i32 7
  %61 = load i32, i32* %rowBytes26, align 4, !tbaa !42
  %mul27 = mul i32 %59, %61
  %add28 = add i32 %58, %mul27
  %arrayidx29 = getelementptr inbounds i8, i8* %56, i32 %add28
  store i8* %arrayidx29, i8** %ptrG, align 4, !tbaa !2
  %62 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #3
  %63 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels30 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %63, i32 0, i32 6
  %64 = load i8*, i8** %pixels30, align 4, !tbaa !38
  %65 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %65, i32 0, i32 9
  %66 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %67 = load i32, i32* %j, align 4, !tbaa !35
  %68 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes31 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %68, i32 0, i32 7
  %69 = load i32, i32* %rowBytes31, align 4, !tbaa !42
  %mul32 = mul i32 %67, %69
  %add33 = add i32 %66, %mul32
  %arrayidx34 = getelementptr inbounds i8, i8* %64, i32 %add33
  store i8* %arrayidx34, i8** %ptrB, align 4, !tbaa !2
  %70 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc, %for.body
  %71 = load i32, i32* %i, align 4, !tbaa !35
  %72 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %72, i32 0, i32 0
  %73 = load i32, i32* %width, align 4, !tbaa !41
  %cmp36 = icmp ult i32 %71, %73
  br i1 %cmp36, label %for.body39, label %for.cond.cleanup38

for.cond.cleanup38:                               ; preds = %for.cond35
  store i32 5, i32* %cleanup.dest.slot, align 4
  %74 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  br label %for.end

for.body39:                                       ; preds = %for.cond35
  %75 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #3
  %76 = load i32, i32* %i, align 4, !tbaa !35
  %77 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo40 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %77, i32 0, i32 11
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo40, i32 0, i32 1
  %78 = load i32, i32* %chromaShiftX, align 4, !tbaa !72
  %shr41 = lshr i32 %76, %78
  store i32 %shr41, i32* %uvI, align 4, !tbaa !35
  %79 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #3
  %80 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %81 = load i8*, i8** %ptrY, align 4, !tbaa !2
  %82 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx42 = getelementptr inbounds i8, i8* %81, i32 %82
  %83 = load i8, i8* %arrayidx42, align 1, !tbaa !45
  %idxprom = zext i8 %83 to i32
  %arrayidx43 = getelementptr inbounds float, float* %80, i32 %idxprom
  %84 = load float, float* %arrayidx43, align 4, !tbaa !36
  store float %84, float* %Y, align 4, !tbaa !36
  %85 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #3
  %86 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %87 = load i8*, i8** %ptrU, align 4, !tbaa !2
  %88 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx44 = getelementptr inbounds i8, i8* %87, i32 %88
  %89 = load i8, i8* %arrayidx44, align 1, !tbaa !45
  %idxprom45 = zext i8 %89 to i32
  %arrayidx46 = getelementptr inbounds float, float* %86, i32 %idxprom45
  %90 = load float, float* %arrayidx46, align 4, !tbaa !36
  store float %90, float* %Cb, align 4, !tbaa !36
  %91 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #3
  %92 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %93 = load i8*, i8** %ptrV, align 4, !tbaa !2
  %94 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx47 = getelementptr inbounds i8, i8* %93, i32 %94
  %95 = load i8, i8* %arrayidx47, align 1, !tbaa !45
  %idxprom48 = zext i8 %95 to i32
  %arrayidx49 = getelementptr inbounds float, float* %92, i32 %idxprom48
  %96 = load float, float* %arrayidx49, align 4, !tbaa !36
  store float %96, float* %Cr, align 4, !tbaa !36
  %97 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #3
  %98 = load float, float* %Y, align 4, !tbaa !36
  %99 = load float, float* %kr, align 4, !tbaa !36
  %sub50 = fsub float 1.000000e+00, %99
  %mul51 = fmul float 2.000000e+00, %sub50
  %100 = load float, float* %Cr, align 4, !tbaa !36
  %mul52 = fmul float %mul51, %100
  %add53 = fadd float %98, %mul52
  store float %add53, float* %R, align 4, !tbaa !36
  %101 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #3
  %102 = load float, float* %Y, align 4, !tbaa !36
  %103 = load float, float* %kb, align 4, !tbaa !36
  %sub54 = fsub float 1.000000e+00, %103
  %mul55 = fmul float 2.000000e+00, %sub54
  %104 = load float, float* %Cb, align 4, !tbaa !36
  %mul56 = fmul float %mul55, %104
  %add57 = fadd float %102, %mul56
  store float %add57, float* %B, align 4, !tbaa !36
  %105 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #3
  %106 = load float, float* %Y, align 4, !tbaa !36
  %107 = load float, float* %kr, align 4, !tbaa !36
  %108 = load float, float* %kr, align 4, !tbaa !36
  %sub58 = fsub float 1.000000e+00, %108
  %mul59 = fmul float %107, %sub58
  %109 = load float, float* %Cr, align 4, !tbaa !36
  %mul60 = fmul float %mul59, %109
  %110 = load float, float* %kb, align 4, !tbaa !36
  %111 = load float, float* %kb, align 4, !tbaa !36
  %sub61 = fsub float 1.000000e+00, %111
  %mul62 = fmul float %110, %sub61
  %112 = load float, float* %Cb, align 4, !tbaa !36
  %mul63 = fmul float %mul62, %112
  %add64 = fadd float %mul60, %mul63
  %mul65 = fmul float 2.000000e+00, %add64
  %113 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul65, %113
  %sub66 = fsub float %106, %div
  store float %sub66, float* %G, align 4, !tbaa !36
  %114 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #3
  %115 = load float, float* %R, align 4, !tbaa !36
  %cmp67 = fcmp olt float %115, 0.000000e+00
  br i1 %cmp67, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body39
  br label %cond.end73

cond.false:                                       ; preds = %for.body39
  %116 = load float, float* %R, align 4, !tbaa !36
  %cmp69 = fcmp olt float 1.000000e+00, %116
  br i1 %cmp69, label %cond.true71, label %cond.false72

cond.true71:                                      ; preds = %cond.false
  br label %cond.end

cond.false72:                                     ; preds = %cond.false
  %117 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end

cond.end:                                         ; preds = %cond.false72, %cond.true71
  %cond = phi float [ 1.000000e+00, %cond.true71 ], [ %117, %cond.false72 ]
  br label %cond.end73

cond.end73:                                       ; preds = %cond.end, %cond.true
  %cond74 = phi float [ 0.000000e+00, %cond.true ], [ %cond, %cond.end ]
  store float %cond74, float* %Rc, align 4, !tbaa !36
  %118 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #3
  %119 = load float, float* %G, align 4, !tbaa !36
  %cmp75 = fcmp olt float %119, 0.000000e+00
  br i1 %cmp75, label %cond.true77, label %cond.false78

cond.true77:                                      ; preds = %cond.end73
  br label %cond.end85

cond.false78:                                     ; preds = %cond.end73
  %120 = load float, float* %G, align 4, !tbaa !36
  %cmp79 = fcmp olt float 1.000000e+00, %120
  br i1 %cmp79, label %cond.true81, label %cond.false82

cond.true81:                                      ; preds = %cond.false78
  br label %cond.end83

cond.false82:                                     ; preds = %cond.false78
  %121 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end83

cond.end83:                                       ; preds = %cond.false82, %cond.true81
  %cond84 = phi float [ 1.000000e+00, %cond.true81 ], [ %121, %cond.false82 ]
  br label %cond.end85

cond.end85:                                       ; preds = %cond.end83, %cond.true77
  %cond86 = phi float [ 0.000000e+00, %cond.true77 ], [ %cond84, %cond.end83 ]
  store float %cond86, float* %Gc, align 4, !tbaa !36
  %122 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #3
  %123 = load float, float* %B, align 4, !tbaa !36
  %cmp87 = fcmp olt float %123, 0.000000e+00
  br i1 %cmp87, label %cond.true89, label %cond.false90

cond.true89:                                      ; preds = %cond.end85
  br label %cond.end97

cond.false90:                                     ; preds = %cond.end85
  %124 = load float, float* %B, align 4, !tbaa !36
  %cmp91 = fcmp olt float 1.000000e+00, %124
  br i1 %cmp91, label %cond.true93, label %cond.false94

cond.true93:                                      ; preds = %cond.false90
  br label %cond.end95

cond.false94:                                     ; preds = %cond.false90
  %125 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end95

cond.end95:                                       ; preds = %cond.false94, %cond.true93
  %cond96 = phi float [ 1.000000e+00, %cond.true93 ], [ %125, %cond.false94 ]
  br label %cond.end97

cond.end97:                                       ; preds = %cond.end95, %cond.true89
  %cond98 = phi float [ 0.000000e+00, %cond.true89 ], [ %cond96, %cond.end95 ]
  store float %cond98, float* %Bc, align 4, !tbaa !36
  %126 = load float, float* %Rc, align 4, !tbaa !36
  %127 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul99 = fmul float %126, %127
  %add100 = fadd float 5.000000e-01, %mul99
  %conv101 = fptoui float %add100 to i16
  %128 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %129 = bitcast i8* %128 to i16*
  store i16 %conv101, i16* %129, align 2, !tbaa !43
  %130 = load float, float* %Gc, align 4, !tbaa !36
  %131 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul102 = fmul float %130, %131
  %add103 = fadd float 5.000000e-01, %mul102
  %conv104 = fptoui float %add103 to i16
  %132 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %133 = bitcast i8* %132 to i16*
  store i16 %conv104, i16* %133, align 2, !tbaa !43
  %134 = load float, float* %Bc, align 4, !tbaa !36
  %135 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul105 = fmul float %134, %135
  %add106 = fadd float 5.000000e-01, %mul105
  %conv107 = fptoui float %add106 to i16
  %136 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %137 = bitcast i8* %136 to i16*
  store i16 %conv107, i16* %137, align 2, !tbaa !43
  %138 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %139 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %139, i32 %138
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %140 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %141 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr108 = getelementptr inbounds i8, i8* %141, i32 %140
  store i8* %add.ptr108, i8** %ptrG, align 4, !tbaa !2
  %142 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %143 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr109 = getelementptr inbounds i8, i8* %143, i32 %142
  store i8* %add.ptr109, i8** %ptrB, align 4, !tbaa !2
  %144 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #3
  %145 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #3
  %146 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #3
  %147 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #3
  %148 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #3
  %149 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #3
  %150 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #3
  %151 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #3
  %152 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #3
  %153 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end97
  %154 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %154, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond35

for.end:                                          ; preds = %for.cond.cleanup38
  %155 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #3
  %156 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #3
  %157 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #3
  %158 = bitcast i8** %ptrV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #3
  %159 = bitcast i8** %ptrU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #3
  %160 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #3
  %161 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #3
  br label %for.inc110

for.inc110:                                       ; preds = %for.end
  %162 = load i32, i32* %j, align 4, !tbaa !35
  %inc111 = add i32 %162, 1
  store i32 %inc111, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end112:                                       ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %163 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #3
  %164 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #3
  %165 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #3
  %166 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #3
  %167 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #3
  %168 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #3
  %169 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUV8ToRGB16Mono(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %rgbPixelBytes = alloca i32, align 4
  %unormFloatTableY = alloca float*, align 4
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ptrY = alloca i8*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %R = alloca float, align 4
  %B = alloca float, align 4
  %G = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 6
  %11 = load i32, i32* %rgbPixelBytes4, align 4, !tbaa !30
  store i32 %11, i32* %rgbPixelBytes, align 4, !tbaa !35
  %12 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %13, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY5, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %14 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %15, i32 0, i32 2
  %16 = load i32, i32* %depth, align 4, !tbaa !15
  %shl = shl i32 1, %16
  %sub = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub to float
  store float %conv, float* %rgbMaxChannel, align 4, !tbaa !36
  %17 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc88, %entry
  %18 = load i32, i32* %j, align 4, !tbaa !35
  %19 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %19, i32 0, i32 1
  %20 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %18, %20
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  br label %for.end90

for.body:                                         ; preds = %for.cond
  %22 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %24 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %25 = load i32, i32* %j, align 4, !tbaa !35
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 7
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %27 = load i32, i32* %arrayidx7, align 4, !tbaa !35
  %mul = mul i32 %25, %27
  %arrayidx8 = getelementptr inbounds i8, i8* %24, i32 %mul
  store i8* %arrayidx8, i8** %ptrY, align 4, !tbaa !2
  %28 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %29, i32 0, i32 6
  %30 = load i8*, i8** %pixels, align 4, !tbaa !38
  %31 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %31, i32 0, i32 7
  %32 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %33 = load i32, i32* %j, align 4, !tbaa !35
  %34 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %34, i32 0, i32 7
  %35 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul9 = mul i32 %33, %35
  %add = add i32 %32, %mul9
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 %add
  store i8* %arrayidx10, i8** %ptrR, align 4, !tbaa !2
  %36 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels11 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %37, i32 0, i32 6
  %38 = load i8*, i8** %pixels11, align 4, !tbaa !38
  %39 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %39, i32 0, i32 8
  %40 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %41 = load i32, i32* %j, align 4, !tbaa !35
  %42 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes12 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %42, i32 0, i32 7
  %43 = load i32, i32* %rowBytes12, align 4, !tbaa !42
  %mul13 = mul i32 %41, %43
  %add14 = add i32 %40, %mul13
  %arrayidx15 = getelementptr inbounds i8, i8* %38, i32 %add14
  store i8* %arrayidx15, i8** %ptrG, align 4, !tbaa !2
  %44 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels16 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %45, i32 0, i32 6
  %46 = load i8*, i8** %pixels16, align 4, !tbaa !38
  %47 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %47, i32 0, i32 9
  %48 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %49 = load i32, i32* %j, align 4, !tbaa !35
  %50 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes17 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %50, i32 0, i32 7
  %51 = load i32, i32* %rowBytes17, align 4, !tbaa !42
  %mul18 = mul i32 %49, %51
  %add19 = add i32 %48, %mul18
  %arrayidx20 = getelementptr inbounds i8, i8* %46, i32 %add19
  store i8* %arrayidx20, i8** %ptrB, align 4, !tbaa !2
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %for.body
  %53 = load i32, i32* %i, align 4, !tbaa !35
  %54 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %54, i32 0, i32 0
  %55 = load i32, i32* %width, align 4, !tbaa !41
  %cmp22 = icmp ult i32 %53, %55
  br i1 %cmp22, label %for.body25, label %for.cond.cleanup24

for.cond.cleanup24:                               ; preds = %for.cond21
  store i32 5, i32* %cleanup.dest.slot, align 4
  %56 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  br label %for.end

for.body25:                                       ; preds = %for.cond21
  %57 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #3
  %58 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %59 = load i8*, i8** %ptrY, align 4, !tbaa !2
  %60 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx26 = getelementptr inbounds i8, i8* %59, i32 %60
  %61 = load i8, i8* %arrayidx26, align 1, !tbaa !45
  %idxprom = zext i8 %61 to i32
  %arrayidx27 = getelementptr inbounds float, float* %58, i32 %idxprom
  %62 = load float, float* %arrayidx27, align 4, !tbaa !36
  store float %62, float* %Y, align 4, !tbaa !36
  %63 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #3
  store float 0.000000e+00, float* %Cb, align 4, !tbaa !36
  %64 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  store float 0.000000e+00, float* %Cr, align 4, !tbaa !36
  %65 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #3
  %66 = load float, float* %Y, align 4, !tbaa !36
  %67 = load float, float* %kr, align 4, !tbaa !36
  %sub28 = fsub float 1.000000e+00, %67
  %mul29 = fmul float 2.000000e+00, %sub28
  %mul30 = fmul float %mul29, 0.000000e+00
  %add31 = fadd float %66, %mul30
  store float %add31, float* %R, align 4, !tbaa !36
  %68 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load float, float* %Y, align 4, !tbaa !36
  %70 = load float, float* %kb, align 4, !tbaa !36
  %sub32 = fsub float 1.000000e+00, %70
  %mul33 = fmul float 2.000000e+00, %sub32
  %mul34 = fmul float %mul33, 0.000000e+00
  %add35 = fadd float %69, %mul34
  store float %add35, float* %B, align 4, !tbaa !36
  %71 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #3
  %72 = load float, float* %Y, align 4, !tbaa !36
  %73 = load float, float* %kr, align 4, !tbaa !36
  %74 = load float, float* %kr, align 4, !tbaa !36
  %sub36 = fsub float 1.000000e+00, %74
  %mul37 = fmul float %73, %sub36
  %mul38 = fmul float %mul37, 0.000000e+00
  %75 = load float, float* %kb, align 4, !tbaa !36
  %76 = load float, float* %kb, align 4, !tbaa !36
  %sub39 = fsub float 1.000000e+00, %76
  %mul40 = fmul float %75, %sub39
  %mul41 = fmul float %mul40, 0.000000e+00
  %add42 = fadd float %mul38, %mul41
  %mul43 = fmul float 2.000000e+00, %add42
  %77 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul43, %77
  %sub44 = fsub float %72, %div
  store float %sub44, float* %G, align 4, !tbaa !36
  %78 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #3
  %79 = load float, float* %R, align 4, !tbaa !36
  %cmp45 = fcmp olt float %79, 0.000000e+00
  br i1 %cmp45, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body25
  br label %cond.end51

cond.false:                                       ; preds = %for.body25
  %80 = load float, float* %R, align 4, !tbaa !36
  %cmp47 = fcmp olt float 1.000000e+00, %80
  br i1 %cmp47, label %cond.true49, label %cond.false50

cond.true49:                                      ; preds = %cond.false
  br label %cond.end

cond.false50:                                     ; preds = %cond.false
  %81 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end

cond.end:                                         ; preds = %cond.false50, %cond.true49
  %cond = phi float [ 1.000000e+00, %cond.true49 ], [ %81, %cond.false50 ]
  br label %cond.end51

cond.end51:                                       ; preds = %cond.end, %cond.true
  %cond52 = phi float [ 0.000000e+00, %cond.true ], [ %cond, %cond.end ]
  store float %cond52, float* %Rc, align 4, !tbaa !36
  %82 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #3
  %83 = load float, float* %G, align 4, !tbaa !36
  %cmp53 = fcmp olt float %83, 0.000000e+00
  br i1 %cmp53, label %cond.true55, label %cond.false56

cond.true55:                                      ; preds = %cond.end51
  br label %cond.end63

cond.false56:                                     ; preds = %cond.end51
  %84 = load float, float* %G, align 4, !tbaa !36
  %cmp57 = fcmp olt float 1.000000e+00, %84
  br i1 %cmp57, label %cond.true59, label %cond.false60

cond.true59:                                      ; preds = %cond.false56
  br label %cond.end61

cond.false60:                                     ; preds = %cond.false56
  %85 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end61

cond.end61:                                       ; preds = %cond.false60, %cond.true59
  %cond62 = phi float [ 1.000000e+00, %cond.true59 ], [ %85, %cond.false60 ]
  br label %cond.end63

cond.end63:                                       ; preds = %cond.end61, %cond.true55
  %cond64 = phi float [ 0.000000e+00, %cond.true55 ], [ %cond62, %cond.end61 ]
  store float %cond64, float* %Gc, align 4, !tbaa !36
  %86 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #3
  %87 = load float, float* %B, align 4, !tbaa !36
  %cmp65 = fcmp olt float %87, 0.000000e+00
  br i1 %cmp65, label %cond.true67, label %cond.false68

cond.true67:                                      ; preds = %cond.end63
  br label %cond.end75

cond.false68:                                     ; preds = %cond.end63
  %88 = load float, float* %B, align 4, !tbaa !36
  %cmp69 = fcmp olt float 1.000000e+00, %88
  br i1 %cmp69, label %cond.true71, label %cond.false72

cond.true71:                                      ; preds = %cond.false68
  br label %cond.end73

cond.false72:                                     ; preds = %cond.false68
  %89 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end73

cond.end73:                                       ; preds = %cond.false72, %cond.true71
  %cond74 = phi float [ 1.000000e+00, %cond.true71 ], [ %89, %cond.false72 ]
  br label %cond.end75

cond.end75:                                       ; preds = %cond.end73, %cond.true67
  %cond76 = phi float [ 0.000000e+00, %cond.true67 ], [ %cond74, %cond.end73 ]
  store float %cond76, float* %Bc, align 4, !tbaa !36
  %90 = load float, float* %Rc, align 4, !tbaa !36
  %91 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul77 = fmul float %90, %91
  %add78 = fadd float 5.000000e-01, %mul77
  %conv79 = fptoui float %add78 to i8
  %92 = load i8*, i8** %ptrR, align 4, !tbaa !2
  store i8 %conv79, i8* %92, align 1, !tbaa !45
  %93 = load float, float* %Gc, align 4, !tbaa !36
  %94 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul80 = fmul float %93, %94
  %add81 = fadd float 5.000000e-01, %mul80
  %conv82 = fptoui float %add81 to i8
  %95 = load i8*, i8** %ptrG, align 4, !tbaa !2
  store i8 %conv82, i8* %95, align 1, !tbaa !45
  %96 = load float, float* %Bc, align 4, !tbaa !36
  %97 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul83 = fmul float %96, %97
  %add84 = fadd float 5.000000e-01, %mul83
  %conv85 = fptoui float %add84 to i8
  %98 = load i8*, i8** %ptrB, align 4, !tbaa !2
  store i8 %conv85, i8* %98, align 1, !tbaa !45
  %99 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %100 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %100, i32 %99
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %101 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %102 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr86 = getelementptr inbounds i8, i8* %102, i32 %101
  store i8* %add.ptr86, i8** %ptrG, align 4, !tbaa !2
  %103 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %104 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr87 = getelementptr inbounds i8, i8* %104, i32 %103
  store i8* %add.ptr87, i8** %ptrB, align 4, !tbaa !2
  %105 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  %106 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  %107 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %112 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  %113 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end75
  %114 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %114, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond21

for.end:                                          ; preds = %for.cond.cleanup24
  %115 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #3
  %116 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  br label %for.inc88

for.inc88:                                        ; preds = %for.end
  %119 = load i32, i32* %j, align 4, !tbaa !35
  %inc89 = add i32 %119, 1
  store i32 %inc89, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end90:                                        ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %120 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  %122 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUV8ToRGB8Color(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %rgbPixelBytes = alloca i32, align 4
  %unormFloatTableY = alloca float*, align 4
  %unormFloatTableUV = alloca float*, align 4
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %uvJ = alloca i32, align 4
  %ptrY = alloca i8*, align 4
  %ptrU = alloca i8*, align 4
  %ptrV = alloca i8*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %uvI = alloca i32, align 4
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %R = alloca float, align 4
  %B = alloca float, align 4
  %G = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 6
  %11 = load i32, i32* %rgbPixelBytes4, align 4, !tbaa !30
  store i32 %11, i32* %rgbPixelBytes, align 4, !tbaa !35
  %12 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %13, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY5, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %14 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableUV6 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %15, i32 0, i32 13
  %arraydecay7 = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableUV6, i32 0, i32 0
  store float* %arraydecay7, float** %unormFloatTableUV, align 4, !tbaa !2
  %16 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %17, i32 0, i32 2
  %18 = load i32, i32* %depth, align 4, !tbaa !15
  %shl = shl i32 1, %18
  %sub = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub to float
  store float %conv, float* %rgbMaxChannel, align 4, !tbaa !36
  %19 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc110, %entry
  %20 = load i32, i32* %j, align 4, !tbaa !35
  %21 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %21, i32 0, i32 1
  %22 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %20, %22
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  br label %for.end112

for.body:                                         ; preds = %for.cond
  %24 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load i32, i32* %j, align 4, !tbaa !35
  %26 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %26, i32 0, i32 11
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %27 = load i32, i32* %chromaShiftY, align 4, !tbaa !71
  %shr = lshr i32 %25, %27
  store i32 %shr, i32* %uvJ, align 4, !tbaa !35
  %28 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %29, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %30 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %31 = load i32, i32* %j, align 4, !tbaa !35
  %32 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 7
  %arrayidx9 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %33 = load i32, i32* %arrayidx9, align 4, !tbaa !35
  %mul = mul i32 %31, %33
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 %mul
  store i8* %arrayidx10, i8** %ptrY, align 4, !tbaa !2
  %34 = bitcast i8** %ptrU to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #3
  %35 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes11 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %35, i32 0, i32 6
  %arrayidx12 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes11, i32 0, i32 1
  %36 = load i8*, i8** %arrayidx12, align 4, !tbaa !2
  %37 = load i32, i32* %uvJ, align 4, !tbaa !35
  %38 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes13 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %38, i32 0, i32 7
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes13, i32 0, i32 1
  %39 = load i32, i32* %arrayidx14, align 4, !tbaa !35
  %mul15 = mul i32 %37, %39
  %arrayidx16 = getelementptr inbounds i8, i8* %36, i32 %mul15
  store i8* %arrayidx16, i8** %ptrU, align 4, !tbaa !2
  %40 = bitcast i8** %ptrV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes17 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %41, i32 0, i32 6
  %arrayidx18 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes17, i32 0, i32 2
  %42 = load i8*, i8** %arrayidx18, align 4, !tbaa !2
  %43 = load i32, i32* %uvJ, align 4, !tbaa !35
  %44 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes19 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %44, i32 0, i32 7
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes19, i32 0, i32 2
  %45 = load i32, i32* %arrayidx20, align 4, !tbaa !35
  %mul21 = mul i32 %43, %45
  %arrayidx22 = getelementptr inbounds i8, i8* %42, i32 %mul21
  store i8* %arrayidx22, i8** %ptrV, align 4, !tbaa !2
  %46 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #3
  %47 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %47, i32 0, i32 6
  %48 = load i8*, i8** %pixels, align 4, !tbaa !38
  %49 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %49, i32 0, i32 7
  %50 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %51 = load i32, i32* %j, align 4, !tbaa !35
  %52 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %52, i32 0, i32 7
  %53 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul23 = mul i32 %51, %53
  %add = add i32 %50, %mul23
  %arrayidx24 = getelementptr inbounds i8, i8* %48, i32 %add
  store i8* %arrayidx24, i8** %ptrR, align 4, !tbaa !2
  %54 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #3
  %55 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels25 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %55, i32 0, i32 6
  %56 = load i8*, i8** %pixels25, align 4, !tbaa !38
  %57 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %57, i32 0, i32 8
  %58 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %59 = load i32, i32* %j, align 4, !tbaa !35
  %60 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes26 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %60, i32 0, i32 7
  %61 = load i32, i32* %rowBytes26, align 4, !tbaa !42
  %mul27 = mul i32 %59, %61
  %add28 = add i32 %58, %mul27
  %arrayidx29 = getelementptr inbounds i8, i8* %56, i32 %add28
  store i8* %arrayidx29, i8** %ptrG, align 4, !tbaa !2
  %62 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #3
  %63 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels30 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %63, i32 0, i32 6
  %64 = load i8*, i8** %pixels30, align 4, !tbaa !38
  %65 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %65, i32 0, i32 9
  %66 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %67 = load i32, i32* %j, align 4, !tbaa !35
  %68 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes31 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %68, i32 0, i32 7
  %69 = load i32, i32* %rowBytes31, align 4, !tbaa !42
  %mul32 = mul i32 %67, %69
  %add33 = add i32 %66, %mul32
  %arrayidx34 = getelementptr inbounds i8, i8* %64, i32 %add33
  store i8* %arrayidx34, i8** %ptrB, align 4, !tbaa !2
  %70 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc, %for.body
  %71 = load i32, i32* %i, align 4, !tbaa !35
  %72 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %72, i32 0, i32 0
  %73 = load i32, i32* %width, align 4, !tbaa !41
  %cmp36 = icmp ult i32 %71, %73
  br i1 %cmp36, label %for.body39, label %for.cond.cleanup38

for.cond.cleanup38:                               ; preds = %for.cond35
  store i32 5, i32* %cleanup.dest.slot, align 4
  %74 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  br label %for.end

for.body39:                                       ; preds = %for.cond35
  %75 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #3
  %76 = load i32, i32* %i, align 4, !tbaa !35
  %77 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo40 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %77, i32 0, i32 11
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo40, i32 0, i32 1
  %78 = load i32, i32* %chromaShiftX, align 4, !tbaa !72
  %shr41 = lshr i32 %76, %78
  store i32 %shr41, i32* %uvI, align 4, !tbaa !35
  %79 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #3
  %80 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %81 = load i8*, i8** %ptrY, align 4, !tbaa !2
  %82 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx42 = getelementptr inbounds i8, i8* %81, i32 %82
  %83 = load i8, i8* %arrayidx42, align 1, !tbaa !45
  %idxprom = zext i8 %83 to i32
  %arrayidx43 = getelementptr inbounds float, float* %80, i32 %idxprom
  %84 = load float, float* %arrayidx43, align 4, !tbaa !36
  store float %84, float* %Y, align 4, !tbaa !36
  %85 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #3
  %86 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %87 = load i8*, i8** %ptrU, align 4, !tbaa !2
  %88 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx44 = getelementptr inbounds i8, i8* %87, i32 %88
  %89 = load i8, i8* %arrayidx44, align 1, !tbaa !45
  %idxprom45 = zext i8 %89 to i32
  %arrayidx46 = getelementptr inbounds float, float* %86, i32 %idxprom45
  %90 = load float, float* %arrayidx46, align 4, !tbaa !36
  store float %90, float* %Cb, align 4, !tbaa !36
  %91 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #3
  %92 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %93 = load i8*, i8** %ptrV, align 4, !tbaa !2
  %94 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx47 = getelementptr inbounds i8, i8* %93, i32 %94
  %95 = load i8, i8* %arrayidx47, align 1, !tbaa !45
  %idxprom48 = zext i8 %95 to i32
  %arrayidx49 = getelementptr inbounds float, float* %92, i32 %idxprom48
  %96 = load float, float* %arrayidx49, align 4, !tbaa !36
  store float %96, float* %Cr, align 4, !tbaa !36
  %97 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #3
  %98 = load float, float* %Y, align 4, !tbaa !36
  %99 = load float, float* %kr, align 4, !tbaa !36
  %sub50 = fsub float 1.000000e+00, %99
  %mul51 = fmul float 2.000000e+00, %sub50
  %100 = load float, float* %Cr, align 4, !tbaa !36
  %mul52 = fmul float %mul51, %100
  %add53 = fadd float %98, %mul52
  store float %add53, float* %R, align 4, !tbaa !36
  %101 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #3
  %102 = load float, float* %Y, align 4, !tbaa !36
  %103 = load float, float* %kb, align 4, !tbaa !36
  %sub54 = fsub float 1.000000e+00, %103
  %mul55 = fmul float 2.000000e+00, %sub54
  %104 = load float, float* %Cb, align 4, !tbaa !36
  %mul56 = fmul float %mul55, %104
  %add57 = fadd float %102, %mul56
  store float %add57, float* %B, align 4, !tbaa !36
  %105 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #3
  %106 = load float, float* %Y, align 4, !tbaa !36
  %107 = load float, float* %kr, align 4, !tbaa !36
  %108 = load float, float* %kr, align 4, !tbaa !36
  %sub58 = fsub float 1.000000e+00, %108
  %mul59 = fmul float %107, %sub58
  %109 = load float, float* %Cr, align 4, !tbaa !36
  %mul60 = fmul float %mul59, %109
  %110 = load float, float* %kb, align 4, !tbaa !36
  %111 = load float, float* %kb, align 4, !tbaa !36
  %sub61 = fsub float 1.000000e+00, %111
  %mul62 = fmul float %110, %sub61
  %112 = load float, float* %Cb, align 4, !tbaa !36
  %mul63 = fmul float %mul62, %112
  %add64 = fadd float %mul60, %mul63
  %mul65 = fmul float 2.000000e+00, %add64
  %113 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul65, %113
  %sub66 = fsub float %106, %div
  store float %sub66, float* %G, align 4, !tbaa !36
  %114 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #3
  %115 = load float, float* %R, align 4, !tbaa !36
  %cmp67 = fcmp olt float %115, 0.000000e+00
  br i1 %cmp67, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body39
  br label %cond.end73

cond.false:                                       ; preds = %for.body39
  %116 = load float, float* %R, align 4, !tbaa !36
  %cmp69 = fcmp olt float 1.000000e+00, %116
  br i1 %cmp69, label %cond.true71, label %cond.false72

cond.true71:                                      ; preds = %cond.false
  br label %cond.end

cond.false72:                                     ; preds = %cond.false
  %117 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end

cond.end:                                         ; preds = %cond.false72, %cond.true71
  %cond = phi float [ 1.000000e+00, %cond.true71 ], [ %117, %cond.false72 ]
  br label %cond.end73

cond.end73:                                       ; preds = %cond.end, %cond.true
  %cond74 = phi float [ 0.000000e+00, %cond.true ], [ %cond, %cond.end ]
  store float %cond74, float* %Rc, align 4, !tbaa !36
  %118 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #3
  %119 = load float, float* %G, align 4, !tbaa !36
  %cmp75 = fcmp olt float %119, 0.000000e+00
  br i1 %cmp75, label %cond.true77, label %cond.false78

cond.true77:                                      ; preds = %cond.end73
  br label %cond.end85

cond.false78:                                     ; preds = %cond.end73
  %120 = load float, float* %G, align 4, !tbaa !36
  %cmp79 = fcmp olt float 1.000000e+00, %120
  br i1 %cmp79, label %cond.true81, label %cond.false82

cond.true81:                                      ; preds = %cond.false78
  br label %cond.end83

cond.false82:                                     ; preds = %cond.false78
  %121 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end83

cond.end83:                                       ; preds = %cond.false82, %cond.true81
  %cond84 = phi float [ 1.000000e+00, %cond.true81 ], [ %121, %cond.false82 ]
  br label %cond.end85

cond.end85:                                       ; preds = %cond.end83, %cond.true77
  %cond86 = phi float [ 0.000000e+00, %cond.true77 ], [ %cond84, %cond.end83 ]
  store float %cond86, float* %Gc, align 4, !tbaa !36
  %122 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #3
  %123 = load float, float* %B, align 4, !tbaa !36
  %cmp87 = fcmp olt float %123, 0.000000e+00
  br i1 %cmp87, label %cond.true89, label %cond.false90

cond.true89:                                      ; preds = %cond.end85
  br label %cond.end97

cond.false90:                                     ; preds = %cond.end85
  %124 = load float, float* %B, align 4, !tbaa !36
  %cmp91 = fcmp olt float 1.000000e+00, %124
  br i1 %cmp91, label %cond.true93, label %cond.false94

cond.true93:                                      ; preds = %cond.false90
  br label %cond.end95

cond.false94:                                     ; preds = %cond.false90
  %125 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end95

cond.end95:                                       ; preds = %cond.false94, %cond.true93
  %cond96 = phi float [ 1.000000e+00, %cond.true93 ], [ %125, %cond.false94 ]
  br label %cond.end97

cond.end97:                                       ; preds = %cond.end95, %cond.true89
  %cond98 = phi float [ 0.000000e+00, %cond.true89 ], [ %cond96, %cond.end95 ]
  store float %cond98, float* %Bc, align 4, !tbaa !36
  %126 = load float, float* %Rc, align 4, !tbaa !36
  %127 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul99 = fmul float %126, %127
  %add100 = fadd float 5.000000e-01, %mul99
  %conv101 = fptoui float %add100 to i8
  %128 = load i8*, i8** %ptrR, align 4, !tbaa !2
  store i8 %conv101, i8* %128, align 1, !tbaa !45
  %129 = load float, float* %Gc, align 4, !tbaa !36
  %130 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul102 = fmul float %129, %130
  %add103 = fadd float 5.000000e-01, %mul102
  %conv104 = fptoui float %add103 to i8
  %131 = load i8*, i8** %ptrG, align 4, !tbaa !2
  store i8 %conv104, i8* %131, align 1, !tbaa !45
  %132 = load float, float* %Bc, align 4, !tbaa !36
  %133 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul105 = fmul float %132, %133
  %add106 = fadd float 5.000000e-01, %mul105
  %conv107 = fptoui float %add106 to i8
  %134 = load i8*, i8** %ptrB, align 4, !tbaa !2
  store i8 %conv107, i8* %134, align 1, !tbaa !45
  %135 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %136 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %136, i32 %135
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %137 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %138 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr108 = getelementptr inbounds i8, i8* %138, i32 %137
  store i8* %add.ptr108, i8** %ptrG, align 4, !tbaa !2
  %139 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %140 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr109 = getelementptr inbounds i8, i8* %140, i32 %139
  store i8* %add.ptr109, i8** %ptrB, align 4, !tbaa !2
  %141 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #3
  %142 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #3
  %143 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #3
  %144 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #3
  %145 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #3
  %146 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #3
  %147 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #3
  %148 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #3
  %149 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #3
  %150 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end97
  %151 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %151, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond35

for.end:                                          ; preds = %for.cond.cleanup38
  %152 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #3
  %153 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #3
  %154 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #3
  %155 = bitcast i8** %ptrV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #3
  %156 = bitcast i8** %ptrU to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #3
  %157 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #3
  %158 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #3
  br label %for.inc110

for.inc110:                                       ; preds = %for.end
  %159 = load i32, i32* %j, align 4, !tbaa !35
  %inc111 = add i32 %159, 1
  store i32 %inc111, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end112:                                       ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %160 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #3
  %161 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #3
  %162 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #3
  %163 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #3
  %164 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #3
  %165 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #3
  %166 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUV8ToRGB8Mono(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %rgbPixelBytes = alloca i32, align 4
  %unormFloatTableY = alloca float*, align 4
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ptrY = alloca i8*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %R = alloca float, align 4
  %B = alloca float, align 4
  %G = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 6
  %11 = load i32, i32* %rgbPixelBytes4, align 4, !tbaa !30
  store i32 %11, i32* %rgbPixelBytes, align 4, !tbaa !35
  %12 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %13, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY5, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %14 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %15, i32 0, i32 2
  %16 = load i32, i32* %depth, align 4, !tbaa !15
  %shl = shl i32 1, %16
  %sub = sub nsw i32 %shl, 1
  %conv = sitofp i32 %sub to float
  store float %conv, float* %rgbMaxChannel, align 4, !tbaa !36
  %17 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc88, %entry
  %18 = load i32, i32* %j, align 4, !tbaa !35
  %19 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %19, i32 0, i32 1
  %20 = load i32, i32* %height, align 4, !tbaa !40
  %cmp = icmp ult i32 %18, %20
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  br label %for.end90

for.body:                                         ; preds = %for.cond
  %22 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %24 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %25 = load i32, i32* %j, align 4, !tbaa !35
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 7
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %27 = load i32, i32* %arrayidx7, align 4, !tbaa !35
  %mul = mul i32 %25, %27
  %arrayidx8 = getelementptr inbounds i8, i8* %24, i32 %mul
  store i8* %arrayidx8, i8** %ptrY, align 4, !tbaa !2
  %28 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %29, i32 0, i32 6
  %30 = load i8*, i8** %pixels, align 4, !tbaa !38
  %31 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %31, i32 0, i32 7
  %32 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %33 = load i32, i32* %j, align 4, !tbaa !35
  %34 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %34, i32 0, i32 7
  %35 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul9 = mul i32 %33, %35
  %add = add i32 %32, %mul9
  %arrayidx10 = getelementptr inbounds i8, i8* %30, i32 %add
  store i8* %arrayidx10, i8** %ptrR, align 4, !tbaa !2
  %36 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels11 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %37, i32 0, i32 6
  %38 = load i8*, i8** %pixels11, align 4, !tbaa !38
  %39 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %39, i32 0, i32 8
  %40 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %41 = load i32, i32* %j, align 4, !tbaa !35
  %42 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes12 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %42, i32 0, i32 7
  %43 = load i32, i32* %rowBytes12, align 4, !tbaa !42
  %mul13 = mul i32 %41, %43
  %add14 = add i32 %40, %mul13
  %arrayidx15 = getelementptr inbounds i8, i8* %38, i32 %add14
  store i8* %arrayidx15, i8** %ptrG, align 4, !tbaa !2
  %44 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels16 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %45, i32 0, i32 6
  %46 = load i8*, i8** %pixels16, align 4, !tbaa !38
  %47 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %47, i32 0, i32 9
  %48 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %49 = load i32, i32* %j, align 4, !tbaa !35
  %50 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes17 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %50, i32 0, i32 7
  %51 = load i32, i32* %rowBytes17, align 4, !tbaa !42
  %mul18 = mul i32 %49, %51
  %add19 = add i32 %48, %mul18
  %arrayidx20 = getelementptr inbounds i8, i8* %46, i32 %add19
  store i8* %arrayidx20, i8** %ptrB, align 4, !tbaa !2
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %for.body
  %53 = load i32, i32* %i, align 4, !tbaa !35
  %54 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %54, i32 0, i32 0
  %55 = load i32, i32* %width, align 4, !tbaa !41
  %cmp22 = icmp ult i32 %53, %55
  br i1 %cmp22, label %for.body25, label %for.cond.cleanup24

for.cond.cleanup24:                               ; preds = %for.cond21
  store i32 5, i32* %cleanup.dest.slot, align 4
  %56 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  br label %for.end

for.body25:                                       ; preds = %for.cond21
  %57 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #3
  %58 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %59 = load i8*, i8** %ptrY, align 4, !tbaa !2
  %60 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx26 = getelementptr inbounds i8, i8* %59, i32 %60
  %61 = load i8, i8* %arrayidx26, align 1, !tbaa !45
  %idxprom = zext i8 %61 to i32
  %arrayidx27 = getelementptr inbounds float, float* %58, i32 %idxprom
  %62 = load float, float* %arrayidx27, align 4, !tbaa !36
  store float %62, float* %Y, align 4, !tbaa !36
  %63 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #3
  store float 0.000000e+00, float* %Cb, align 4, !tbaa !36
  %64 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  store float 0.000000e+00, float* %Cr, align 4, !tbaa !36
  %65 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #3
  %66 = load float, float* %Y, align 4, !tbaa !36
  %67 = load float, float* %kr, align 4, !tbaa !36
  %sub28 = fsub float 1.000000e+00, %67
  %mul29 = fmul float 2.000000e+00, %sub28
  %mul30 = fmul float %mul29, 0.000000e+00
  %add31 = fadd float %66, %mul30
  store float %add31, float* %R, align 4, !tbaa !36
  %68 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #3
  %69 = load float, float* %Y, align 4, !tbaa !36
  %70 = load float, float* %kb, align 4, !tbaa !36
  %sub32 = fsub float 1.000000e+00, %70
  %mul33 = fmul float 2.000000e+00, %sub32
  %mul34 = fmul float %mul33, 0.000000e+00
  %add35 = fadd float %69, %mul34
  store float %add35, float* %B, align 4, !tbaa !36
  %71 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #3
  %72 = load float, float* %Y, align 4, !tbaa !36
  %73 = load float, float* %kr, align 4, !tbaa !36
  %74 = load float, float* %kr, align 4, !tbaa !36
  %sub36 = fsub float 1.000000e+00, %74
  %mul37 = fmul float %73, %sub36
  %mul38 = fmul float %mul37, 0.000000e+00
  %75 = load float, float* %kb, align 4, !tbaa !36
  %76 = load float, float* %kb, align 4, !tbaa !36
  %sub39 = fsub float 1.000000e+00, %76
  %mul40 = fmul float %75, %sub39
  %mul41 = fmul float %mul40, 0.000000e+00
  %add42 = fadd float %mul38, %mul41
  %mul43 = fmul float 2.000000e+00, %add42
  %77 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul43, %77
  %sub44 = fsub float %72, %div
  store float %sub44, float* %G, align 4, !tbaa !36
  %78 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #3
  %79 = load float, float* %R, align 4, !tbaa !36
  %cmp45 = fcmp olt float %79, 0.000000e+00
  br i1 %cmp45, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body25
  br label %cond.end51

cond.false:                                       ; preds = %for.body25
  %80 = load float, float* %R, align 4, !tbaa !36
  %cmp47 = fcmp olt float 1.000000e+00, %80
  br i1 %cmp47, label %cond.true49, label %cond.false50

cond.true49:                                      ; preds = %cond.false
  br label %cond.end

cond.false50:                                     ; preds = %cond.false
  %81 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end

cond.end:                                         ; preds = %cond.false50, %cond.true49
  %cond = phi float [ 1.000000e+00, %cond.true49 ], [ %81, %cond.false50 ]
  br label %cond.end51

cond.end51:                                       ; preds = %cond.end, %cond.true
  %cond52 = phi float [ 0.000000e+00, %cond.true ], [ %cond, %cond.end ]
  store float %cond52, float* %Rc, align 4, !tbaa !36
  %82 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #3
  %83 = load float, float* %G, align 4, !tbaa !36
  %cmp53 = fcmp olt float %83, 0.000000e+00
  br i1 %cmp53, label %cond.true55, label %cond.false56

cond.true55:                                      ; preds = %cond.end51
  br label %cond.end63

cond.false56:                                     ; preds = %cond.end51
  %84 = load float, float* %G, align 4, !tbaa !36
  %cmp57 = fcmp olt float 1.000000e+00, %84
  br i1 %cmp57, label %cond.true59, label %cond.false60

cond.true59:                                      ; preds = %cond.false56
  br label %cond.end61

cond.false60:                                     ; preds = %cond.false56
  %85 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end61

cond.end61:                                       ; preds = %cond.false60, %cond.true59
  %cond62 = phi float [ 1.000000e+00, %cond.true59 ], [ %85, %cond.false60 ]
  br label %cond.end63

cond.end63:                                       ; preds = %cond.end61, %cond.true55
  %cond64 = phi float [ 0.000000e+00, %cond.true55 ], [ %cond62, %cond.end61 ]
  store float %cond64, float* %Gc, align 4, !tbaa !36
  %86 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #3
  %87 = load float, float* %B, align 4, !tbaa !36
  %cmp65 = fcmp olt float %87, 0.000000e+00
  br i1 %cmp65, label %cond.true67, label %cond.false68

cond.true67:                                      ; preds = %cond.end63
  br label %cond.end75

cond.false68:                                     ; preds = %cond.end63
  %88 = load float, float* %B, align 4, !tbaa !36
  %cmp69 = fcmp olt float 1.000000e+00, %88
  br i1 %cmp69, label %cond.true71, label %cond.false72

cond.true71:                                      ; preds = %cond.false68
  br label %cond.end73

cond.false72:                                     ; preds = %cond.false68
  %89 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end73

cond.end73:                                       ; preds = %cond.false72, %cond.true71
  %cond74 = phi float [ 1.000000e+00, %cond.true71 ], [ %89, %cond.false72 ]
  br label %cond.end75

cond.end75:                                       ; preds = %cond.end73, %cond.true67
  %cond76 = phi float [ 0.000000e+00, %cond.true67 ], [ %cond74, %cond.end73 ]
  store float %cond76, float* %Bc, align 4, !tbaa !36
  %90 = load float, float* %Rc, align 4, !tbaa !36
  %91 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul77 = fmul float %90, %91
  %add78 = fadd float 5.000000e-01, %mul77
  %conv79 = fptoui float %add78 to i8
  %92 = load i8*, i8** %ptrR, align 4, !tbaa !2
  store i8 %conv79, i8* %92, align 1, !tbaa !45
  %93 = load float, float* %Gc, align 4, !tbaa !36
  %94 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul80 = fmul float %93, %94
  %add81 = fadd float 5.000000e-01, %mul80
  %conv82 = fptoui float %add81 to i8
  %95 = load i8*, i8** %ptrG, align 4, !tbaa !2
  store i8 %conv82, i8* %95, align 1, !tbaa !45
  %96 = load float, float* %Bc, align 4, !tbaa !36
  %97 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul83 = fmul float %96, %97
  %add84 = fadd float 5.000000e-01, %mul83
  %conv85 = fptoui float %add84 to i8
  %98 = load i8*, i8** %ptrB, align 4, !tbaa !2
  store i8 %conv85, i8* %98, align 1, !tbaa !45
  %99 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %100 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %100, i32 %99
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %101 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %102 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr86 = getelementptr inbounds i8, i8* %102, i32 %101
  store i8* %add.ptr86, i8** %ptrG, align 4, !tbaa !2
  %103 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %104 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr87 = getelementptr inbounds i8, i8* %104, i32 %103
  store i8* %add.ptr87, i8** %ptrB, align 4, !tbaa !2
  %105 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  %106 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  %107 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %112 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  %113 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #3
  br label %for.inc

for.inc:                                          ; preds = %cond.end75
  %114 = load i32, i32* %i, align 4, !tbaa !35
  %inc = add i32 %114, 1
  store i32 %inc, i32* %i, align 4, !tbaa !35
  br label %for.cond21

for.end:                                          ; preds = %for.cond.cleanup24
  %115 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #3
  %116 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast i8** %ptrY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  br label %for.inc88

for.inc88:                                        ; preds = %for.end
  %119 = load i32, i32* %j, align 4, !tbaa !35
  %inc89 = add i32 %119, 1
  store i32 %inc89, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end90:                                        ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %120 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  %122 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @avifImageYUVAnyToRGBAnySlow(%struct.avifImage* %image, %struct.avifRGBImage* %rgb, %struct.avifReformatState* %state) #0 {
entry:
  %image.addr = alloca %struct.avifImage*, align 4
  %rgb.addr = alloca %struct.avifRGBImage*, align 4
  %state.addr = alloca %struct.avifReformatState*, align 4
  %kr = alloca float, align 4
  %kg = alloca float, align 4
  %kb = alloca float, align 4
  %unormFloatTableY = alloca float*, align 4
  %unormFloatTableUV = alloca float*, align 4
  %yuvChannelBytes = alloca i32, align 4
  %rgbPixelBytes = alloca i32, align 4
  %yPlane = alloca i8*, align 4
  %uPlane = alloca i8*, align 4
  %vPlane = alloca i8*, align 4
  %yRowBytes = alloca i32, align 4
  %uRowBytes = alloca i32, align 4
  %vRowBytes = alloca i32, align 4
  %hasColor = alloca i32, align 4
  %yuvMaxChannel = alloca i16, align 2
  %rgbMaxChannel = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %uvJ = alloca i32, align 4
  %ptrY8 = alloca i8*, align 4
  %ptrU8 = alloca i8*, align 4
  %ptrV8 = alloca i8*, align 4
  %ptrY16 = alloca i16*, align 4
  %ptrU16 = alloca i16*, align 4
  %ptrV16 = alloca i16*, align 4
  %ptrR = alloca i8*, align 4
  %ptrG = alloca i8*, align 4
  %ptrB = alloca i8*, align 4
  %i = alloca i32, align 4
  %uvI = alloca i32, align 4
  %Y = alloca float, align 4
  %Cb = alloca float, align 4
  %Cr = alloca float, align 4
  %unormY = alloca i16, align 2
  %unormU = alloca i16, align 2
  %unormV = alloca i16, align 2
  %unormU111 = alloca [2 x [2 x i16]], align 2
  %unormV112 = alloca [2 x [2 x i16]], align 2
  %uAdjCol = alloca i32, align 4
  %vAdjCol = alloca i32, align 4
  %uAdjRow = alloca i32, align 4
  %vAdjRow = alloca i32, align 4
  %bJ = alloca i32, align 4
  %bI = alloca i32, align 4
  %R = alloca float, align 4
  %G = alloca float, align 4
  %B = alloca float, align 4
  %Rc = alloca float, align 4
  %Gc = alloca float, align 4
  %Bc = alloca float, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store %struct.avifRGBImage* %rgb, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  store %struct.avifReformatState* %state, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %0 = bitcast float* %kr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kr1 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %1, i32 0, i32 0
  %2 = load float, float* %kr1, align 4, !tbaa !23
  store float %2, float* %kr, align 4, !tbaa !36
  %3 = bitcast float* %kg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kg2 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %4, i32 0, i32 1
  %5 = load float, float* %kg2, align 4, !tbaa !24
  store float %5, float* %kg, align 4, !tbaa !36
  %6 = bitcast float* %kb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %kb3 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %7, i32 0, i32 2
  %8 = load float, float* %kb3, align 4, !tbaa !25
  store float %8, float* %kb, align 4, !tbaa !36
  %9 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableY4 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %10, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableY4, i32 0, i32 0
  store float* %arraydecay, float** %unormFloatTableY, align 4, !tbaa !2
  %11 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %unormFloatTableUV5 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %12, i32 0, i32 13
  %arraydecay6 = getelementptr inbounds [4096 x float], [4096 x float]* %unormFloatTableUV5, i32 0, i32 0
  store float* %arraydecay6, float** %unormFloatTableUV, align 4, !tbaa !2
  %13 = bitcast i32* %yuvChannelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %yuvChannelBytes7 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %14, i32 0, i32 3
  %15 = load i32, i32* %yuvChannelBytes7, align 4, !tbaa !26
  store i32 %15, i32* %yuvChannelBytes, align 4, !tbaa !35
  %16 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbPixelBytes8 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %17, i32 0, i32 6
  %18 = load i32, i32* %rgbPixelBytes8, align 4, !tbaa !30
  store i32 %18, i32* %rgbPixelBytes, align 4, !tbaa !35
  %19 = bitcast i8** %yPlane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %20, i32 0, i32 6
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 0
  %21 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %21, i8** %yPlane, align 4, !tbaa !2
  %22 = bitcast i8** %uPlane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes9 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 6
  %arrayidx10 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes9, i32 0, i32 1
  %24 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %24, i8** %uPlane, align 4, !tbaa !2
  %25 = bitcast i8** %vPlane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes11 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %26, i32 0, i32 6
  %arrayidx12 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes11, i32 0, i32 2
  %27 = load i8*, i8** %arrayidx12, align 4, !tbaa !2
  store i8* %27, i8** %vPlane, align 4, !tbaa !2
  %28 = bitcast i32* %yRowBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %29, i32 0, i32 7
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 0
  %30 = load i32, i32* %arrayidx13, align 4, !tbaa !35
  store i32 %30, i32* %yRowBytes, align 4, !tbaa !35
  %31 = bitcast i32* %uRowBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #3
  %32 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes14 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %32, i32 0, i32 7
  %arrayidx15 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes14, i32 0, i32 1
  %33 = load i32, i32* %arrayidx15, align 4, !tbaa !35
  store i32 %33, i32* %uRowBytes, align 4, !tbaa !35
  %34 = bitcast i32* %vRowBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #3
  %35 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes16 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %35, i32 0, i32 7
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes16, i32 0, i32 2
  %36 = load i32, i32* %arrayidx17, align 4, !tbaa !35
  store i32 %36, i32* %vRowBytes, align 4, !tbaa !35
  %37 = bitcast i32* %hasColor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #3
  %38 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %tobool = icmp ne i8* %38, null
  br i1 %tobool, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %39 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %tobool18 = icmp ne i8* %39, null
  br i1 %tobool18, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %40 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %40, i32 0, i32 3
  %41 = load i32, i32* %yuvFormat, align 4, !tbaa !17
  %cmp = icmp ne i32 %41, 4
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %entry
  %42 = phi i1 [ false, %land.lhs.true ], [ false, %entry ], [ %cmp, %land.rhs ]
  %land.ext = zext i1 %42 to i32
  store i32 %land.ext, i32* %hasColor, align 4, !tbaa !35
  %43 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %43) #3
  %44 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %44, i32 0, i32 2
  %45 = load i32, i32* %depth, align 4, !tbaa !6
  %shl = shl i32 1, %45
  %sub = sub nsw i32 %shl, 1
  %conv = trunc i32 %sub to i16
  store i16 %conv, i16* %yuvMaxChannel, align 2, !tbaa !43
  %46 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #3
  %47 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth19 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %47, i32 0, i32 2
  %48 = load i32, i32* %depth19, align 4, !tbaa !15
  %shl20 = shl i32 1, %48
  %sub21 = sub nsw i32 %shl20, 1
  %conv22 = sitofp i32 %sub21 to float
  store float %conv22, float* %rgbMaxChannel, align 4, !tbaa !36
  %49 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #3
  store i32 0, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.cond:                                         ; preds = %for.inc484, %land.end
  %50 = load i32, i32* %j, align 4, !tbaa !35
  %51 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %51, i32 0, i32 1
  %52 = load i32, i32* %height, align 4, !tbaa !40
  %cmp23 = icmp ult i32 %50, %52
  br i1 %cmp23, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %53 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  br label %for.end486

for.body:                                         ; preds = %for.cond
  %54 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #3
  %55 = load i32, i32* %j, align 4, !tbaa !35
  %56 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %56, i32 0, i32 11
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo, i32 0, i32 2
  %57 = load i32, i32* %chromaShiftY, align 4, !tbaa !71
  %shr = lshr i32 %55, %57
  store i32 %shr, i32* %uvJ, align 4, !tbaa !35
  %58 = bitcast i8** %ptrY8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #3
  %59 = load i8*, i8** %yPlane, align 4, !tbaa !2
  %60 = load i32, i32* %j, align 4, !tbaa !35
  %61 = load i32, i32* %yRowBytes, align 4, !tbaa !35
  %mul = mul i32 %60, %61
  %arrayidx25 = getelementptr inbounds i8, i8* %59, i32 %mul
  store i8* %arrayidx25, i8** %ptrY8, align 4, !tbaa !2
  %62 = bitcast i8** %ptrU8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #3
  %63 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %64 = load i32, i32* %uvJ, align 4, !tbaa !35
  %65 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul26 = mul i32 %64, %65
  %arrayidx27 = getelementptr inbounds i8, i8* %63, i32 %mul26
  store i8* %arrayidx27, i8** %ptrU8, align 4, !tbaa !2
  %66 = bitcast i8** %ptrV8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #3
  %67 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %68 = load i32, i32* %uvJ, align 4, !tbaa !35
  %69 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul28 = mul i32 %68, %69
  %arrayidx29 = getelementptr inbounds i8, i8* %67, i32 %mul28
  store i8* %arrayidx29, i8** %ptrV8, align 4, !tbaa !2
  %70 = bitcast i16** %ptrY16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #3
  %71 = load i8*, i8** %ptrY8, align 4, !tbaa !2
  %72 = bitcast i8* %71 to i16*
  store i16* %72, i16** %ptrY16, align 4, !tbaa !2
  %73 = bitcast i16** %ptrU16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #3
  %74 = load i8*, i8** %ptrU8, align 4, !tbaa !2
  %75 = bitcast i8* %74 to i16*
  store i16* %75, i16** %ptrU16, align 4, !tbaa !2
  %76 = bitcast i16** %ptrV16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #3
  %77 = load i8*, i8** %ptrV8, align 4, !tbaa !2
  %78 = bitcast i8* %77 to i16*
  store i16* %78, i16** %ptrV16, align 4, !tbaa !2
  %79 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #3
  %80 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %80, i32 0, i32 6
  %81 = load i8*, i8** %pixels, align 4, !tbaa !38
  %82 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesR = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %82, i32 0, i32 7
  %83 = load i32, i32* %rgbOffsetBytesR, align 4, !tbaa !31
  %84 = load i32, i32* %j, align 4, !tbaa !35
  %85 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %85, i32 0, i32 7
  %86 = load i32, i32* %rowBytes, align 4, !tbaa !42
  %mul30 = mul i32 %84, %86
  %add = add i32 %83, %mul30
  %arrayidx31 = getelementptr inbounds i8, i8* %81, i32 %add
  store i8* %arrayidx31, i8** %ptrR, align 4, !tbaa !2
  %87 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #3
  %88 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels32 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %88, i32 0, i32 6
  %89 = load i8*, i8** %pixels32, align 4, !tbaa !38
  %90 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesG = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %90, i32 0, i32 8
  %91 = load i32, i32* %rgbOffsetBytesG, align 4, !tbaa !32
  %92 = load i32, i32* %j, align 4, !tbaa !35
  %93 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes33 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %93, i32 0, i32 7
  %94 = load i32, i32* %rowBytes33, align 4, !tbaa !42
  %mul34 = mul i32 %92, %94
  %add35 = add i32 %91, %mul34
  %arrayidx36 = getelementptr inbounds i8, i8* %89, i32 %add35
  store i8* %arrayidx36, i8** %ptrG, align 4, !tbaa !2
  %95 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #3
  %96 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %pixels37 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %96, i32 0, i32 6
  %97 = load i8*, i8** %pixels37, align 4, !tbaa !38
  %98 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %rgbOffsetBytesB = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %98, i32 0, i32 9
  %99 = load i32, i32* %rgbOffsetBytesB, align 4, !tbaa !33
  %100 = load i32, i32* %j, align 4, !tbaa !35
  %101 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %rowBytes38 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %101, i32 0, i32 7
  %102 = load i32, i32* %rowBytes38, align 4, !tbaa !42
  %mul39 = mul i32 %100, %102
  %add40 = add i32 %99, %mul39
  %arrayidx41 = getelementptr inbounds i8, i8* %97, i32 %add40
  store i8* %arrayidx41, i8** %ptrB, align 4, !tbaa !2
  %103 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #3
  store i32 0, i32* %i, align 4, !tbaa !35
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc481, %for.body
  %104 = load i32, i32* %i, align 4, !tbaa !35
  %105 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %105, i32 0, i32 0
  %106 = load i32, i32* %width, align 4, !tbaa !41
  %cmp43 = icmp ult i32 %104, %106
  br i1 %cmp43, label %for.body46, label %for.cond.cleanup45

for.cond.cleanup45:                               ; preds = %for.cond42
  store i32 5, i32* %cleanup.dest.slot, align 4
  %107 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  br label %for.end483

for.body46:                                       ; preds = %for.cond42
  %108 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #3
  %109 = load i32, i32* %i, align 4, !tbaa !35
  %110 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %formatInfo47 = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %110, i32 0, i32 11
  %chromaShiftX = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo47, i32 0, i32 1
  %111 = load i32, i32* %chromaShiftX, align 4, !tbaa !72
  %shr48 = lshr i32 %109, %111
  store i32 %shr48, i32* %uvI, align 4, !tbaa !35
  %112 = bitcast float* %Y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #3
  %113 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #3
  store float 5.000000e-01, float* %Cb, align 4, !tbaa !36
  %114 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #3
  store float 5.000000e-01, float* %Cr, align 4, !tbaa !36
  %115 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %115) #3
  %116 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth49 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %116, i32 0, i32 2
  %117 = load i32, i32* %depth49, align 4, !tbaa !6
  %cmp50 = icmp eq i32 %117, 8
  br i1 %cmp50, label %if.then, label %if.else

if.then:                                          ; preds = %for.body46
  %118 = load i8*, i8** %ptrY8, align 4, !tbaa !2
  %119 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx52 = getelementptr inbounds i8, i8* %118, i32 %119
  %120 = load i8, i8* %arrayidx52, align 1, !tbaa !45
  %conv53 = zext i8 %120 to i16
  store i16 %conv53, i16* %unormY, align 2, !tbaa !43
  br label %if.end

if.else:                                          ; preds = %for.body46
  %121 = load i16*, i16** %ptrY16, align 4, !tbaa !2
  %122 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx54 = getelementptr inbounds i16, i16* %121, i32 %122
  %123 = load i16, i16* %arrayidx54, align 2, !tbaa !43
  %conv55 = zext i16 %123 to i32
  %124 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv56 = zext i16 %124 to i32
  %cmp57 = icmp slt i32 %conv55, %conv56
  br i1 %cmp57, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %125 = load i16*, i16** %ptrY16, align 4, !tbaa !2
  %126 = load i32, i32* %i, align 4, !tbaa !35
  %arrayidx59 = getelementptr inbounds i16, i16* %125, i32 %126
  %127 = load i16, i16* %arrayidx59, align 2, !tbaa !43
  %conv60 = zext i16 %127 to i32
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %128 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv61 = zext i16 %128 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv60, %cond.true ], [ %conv61, %cond.false ]
  %conv62 = trunc i32 %cond to i16
  store i16 %conv62, i16* %unormY, align 2, !tbaa !43
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %129 = load float*, float** %unormFloatTableY, align 4, !tbaa !2
  %130 = load i16, i16* %unormY, align 2, !tbaa !43
  %idxprom = zext i16 %130 to i32
  %arrayidx63 = getelementptr inbounds float, float* %129, i32 %idxprom
  %131 = load float, float* %arrayidx63, align 4, !tbaa !36
  store float %131, float* %Y, align 4, !tbaa !36
  %132 = load i32, i32* %hasColor, align 4, !tbaa !35
  %tobool64 = icmp ne i32 %132, 0
  br i1 %tobool64, label %if.then65, label %if.end392

if.then65:                                        ; preds = %if.end
  %133 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat66 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %133, i32 0, i32 3
  %134 = load i32, i32* %yuvFormat66, align 4, !tbaa !17
  %cmp67 = icmp eq i32 %134, 1
  br i1 %cmp67, label %if.then69, label %if.else110

if.then69:                                        ; preds = %if.then65
  %135 = bitcast i16* %unormU to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %135) #3
  %136 = bitcast i16* %unormV to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %136) #3
  %137 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth70 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %137, i32 0, i32 2
  %138 = load i32, i32* %depth70, align 4, !tbaa !6
  %cmp71 = icmp eq i32 %138, 8
  br i1 %cmp71, label %if.then73, label %if.else78

if.then73:                                        ; preds = %if.then69
  %139 = load i8*, i8** %ptrU8, align 4, !tbaa !2
  %140 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx74 = getelementptr inbounds i8, i8* %139, i32 %140
  %141 = load i8, i8* %arrayidx74, align 1, !tbaa !45
  %conv75 = zext i8 %141 to i16
  store i16 %conv75, i16* %unormU, align 2, !tbaa !43
  %142 = load i8*, i8** %ptrV8, align 4, !tbaa !2
  %143 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx76 = getelementptr inbounds i8, i8* %142, i32 %143
  %144 = load i8, i8* %arrayidx76, align 1, !tbaa !45
  %conv77 = zext i8 %144 to i16
  store i16 %conv77, i16* %unormV, align 2, !tbaa !43
  br label %if.end105

if.else78:                                        ; preds = %if.then69
  %145 = load i16*, i16** %ptrU16, align 4, !tbaa !2
  %146 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx79 = getelementptr inbounds i16, i16* %145, i32 %146
  %147 = load i16, i16* %arrayidx79, align 2, !tbaa !43
  %conv80 = zext i16 %147 to i32
  %148 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv81 = zext i16 %148 to i32
  %cmp82 = icmp slt i32 %conv80, %conv81
  br i1 %cmp82, label %cond.true84, label %cond.false87

cond.true84:                                      ; preds = %if.else78
  %149 = load i16*, i16** %ptrU16, align 4, !tbaa !2
  %150 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx85 = getelementptr inbounds i16, i16* %149, i32 %150
  %151 = load i16, i16* %arrayidx85, align 2, !tbaa !43
  %conv86 = zext i16 %151 to i32
  br label %cond.end89

cond.false87:                                     ; preds = %if.else78
  %152 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv88 = zext i16 %152 to i32
  br label %cond.end89

cond.end89:                                       ; preds = %cond.false87, %cond.true84
  %cond90 = phi i32 [ %conv86, %cond.true84 ], [ %conv88, %cond.false87 ]
  %conv91 = trunc i32 %cond90 to i16
  store i16 %conv91, i16* %unormU, align 2, !tbaa !43
  %153 = load i16*, i16** %ptrV16, align 4, !tbaa !2
  %154 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx92 = getelementptr inbounds i16, i16* %153, i32 %154
  %155 = load i16, i16* %arrayidx92, align 2, !tbaa !43
  %conv93 = zext i16 %155 to i32
  %156 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv94 = zext i16 %156 to i32
  %cmp95 = icmp slt i32 %conv93, %conv94
  br i1 %cmp95, label %cond.true97, label %cond.false100

cond.true97:                                      ; preds = %cond.end89
  %157 = load i16*, i16** %ptrV16, align 4, !tbaa !2
  %158 = load i32, i32* %uvI, align 4, !tbaa !35
  %arrayidx98 = getelementptr inbounds i16, i16* %157, i32 %158
  %159 = load i16, i16* %arrayidx98, align 2, !tbaa !43
  %conv99 = zext i16 %159 to i32
  br label %cond.end102

cond.false100:                                    ; preds = %cond.end89
  %160 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv101 = zext i16 %160 to i32
  br label %cond.end102

cond.end102:                                      ; preds = %cond.false100, %cond.true97
  %cond103 = phi i32 [ %conv99, %cond.true97 ], [ %conv101, %cond.false100 ]
  %conv104 = trunc i32 %cond103 to i16
  store i16 %conv104, i16* %unormV, align 2, !tbaa !43
  br label %if.end105

if.end105:                                        ; preds = %cond.end102, %if.then73
  %161 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %162 = load i16, i16* %unormU, align 2, !tbaa !43
  %idxprom106 = zext i16 %162 to i32
  %arrayidx107 = getelementptr inbounds float, float* %161, i32 %idxprom106
  %163 = load float, float* %arrayidx107, align 4, !tbaa !36
  store float %163, float* %Cb, align 4, !tbaa !36
  %164 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %165 = load i16, i16* %unormV, align 2, !tbaa !43
  %idxprom108 = zext i16 %165 to i32
  %arrayidx109 = getelementptr inbounds float, float* %164, i32 %idxprom108
  %166 = load float, float* %arrayidx109, align 4, !tbaa !36
  store float %166, float* %Cr, align 4, !tbaa !36
  %167 = bitcast i16* %unormV to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %167) #3
  %168 = bitcast i16* %unormU to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %168) #3
  br label %if.end391

if.else110:                                       ; preds = %if.then65
  %169 = bitcast [2 x [2 x i16]]* %unormU111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %169) #3
  %170 = bitcast [2 x [2 x i16]]* %unormV112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %170) #3
  %171 = bitcast i32* %uAdjCol to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %171) #3
  %172 = bitcast i32* %vAdjCol to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %172) #3
  %173 = bitcast i32* %uAdjRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %173) #3
  %174 = bitcast i32* %vAdjRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %174) #3
  %175 = load i32, i32* %i, align 4, !tbaa !35
  %cmp113 = icmp eq i32 %175, 0
  br i1 %cmp113, label %if.then122, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else110
  %176 = load i32, i32* %i, align 4, !tbaa !35
  %177 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width115 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %177, i32 0, i32 0
  %178 = load i32, i32* %width115, align 4, !tbaa !41
  %sub116 = sub i32 %178, 1
  %cmp117 = icmp eq i32 %176, %sub116
  br i1 %cmp117, label %land.lhs.true119, label %if.else123

land.lhs.true119:                                 ; preds = %lor.lhs.false
  %179 = load i32, i32* %i, align 4, !tbaa !35
  %rem = urem i32 %179, 2
  %cmp120 = icmp ne i32 %rem, 0
  br i1 %cmp120, label %if.then122, label %if.else123

if.then122:                                       ; preds = %land.lhs.true119, %if.else110
  store i32 0, i32* %uAdjCol, align 4, !tbaa !35
  store i32 0, i32* %vAdjCol, align 4, !tbaa !35
  br label %if.end132

if.else123:                                       ; preds = %land.lhs.true119, %lor.lhs.false
  %180 = load i32, i32* %i, align 4, !tbaa !35
  %rem124 = urem i32 %180, 2
  %cmp125 = icmp ne i32 %rem124, 0
  br i1 %cmp125, label %if.then127, label %if.else128

if.then127:                                       ; preds = %if.else123
  %181 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  store i32 %181, i32* %uAdjCol, align 4, !tbaa !35
  %182 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  store i32 %182, i32* %vAdjCol, align 4, !tbaa !35
  br label %if.end131

if.else128:                                       ; preds = %if.else123
  %183 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul129 = mul i32 -1, %183
  store i32 %mul129, i32* %uAdjCol, align 4, !tbaa !35
  %184 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul130 = mul i32 -1, %184
  store i32 %mul130, i32* %vAdjCol, align 4, !tbaa !35
  br label %if.end131

if.end131:                                        ; preds = %if.else128, %if.then127
  br label %if.end132

if.end132:                                        ; preds = %if.end131, %if.then122
  %185 = load i32, i32* %j, align 4, !tbaa !35
  %cmp133 = icmp eq i32 %185, 0
  br i1 %cmp133, label %if.then148, label %lor.lhs.false135

lor.lhs.false135:                                 ; preds = %if.end132
  %186 = load i32, i32* %j, align 4, !tbaa !35
  %187 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height136 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %187, i32 0, i32 1
  %188 = load i32, i32* %height136, align 4, !tbaa !40
  %sub137 = sub i32 %188, 1
  %cmp138 = icmp eq i32 %186, %sub137
  br i1 %cmp138, label %land.lhs.true140, label %lor.lhs.false144

land.lhs.true140:                                 ; preds = %lor.lhs.false135
  %189 = load i32, i32* %j, align 4, !tbaa !35
  %rem141 = urem i32 %189, 2
  %cmp142 = icmp ne i32 %rem141, 0
  br i1 %cmp142, label %if.then148, label %lor.lhs.false144

lor.lhs.false144:                                 ; preds = %land.lhs.true140, %lor.lhs.false135
  %190 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat145 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %190, i32 0, i32 3
  %191 = load i32, i32* %yuvFormat145, align 4, !tbaa !17
  %cmp146 = icmp eq i32 %191, 2
  br i1 %cmp146, label %if.then148, label %if.else149

if.then148:                                       ; preds = %lor.lhs.false144, %land.lhs.true140, %if.end132
  store i32 0, i32* %uAdjRow, align 4, !tbaa !35
  store i32 0, i32* %vAdjRow, align 4, !tbaa !35
  br label %if.end158

if.else149:                                       ; preds = %lor.lhs.false144
  %192 = load i32, i32* %j, align 4, !tbaa !35
  %rem150 = urem i32 %192, 2
  %cmp151 = icmp ne i32 %rem150, 0
  br i1 %cmp151, label %if.then153, label %if.else154

if.then153:                                       ; preds = %if.else149
  %193 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  store i32 %193, i32* %uAdjRow, align 4, !tbaa !35
  %194 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  store i32 %194, i32* %vAdjRow, align 4, !tbaa !35
  br label %if.end157

if.else154:                                       ; preds = %if.else149
  %195 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul155 = mul nsw i32 -1, %195
  store i32 %mul155, i32* %uAdjRow, align 4, !tbaa !35
  %196 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul156 = mul nsw i32 -1, %196
  store i32 %mul156, i32* %vAdjRow, align 4, !tbaa !35
  br label %if.end157

if.end157:                                        ; preds = %if.else154, %if.then153
  br label %if.end158

if.end158:                                        ; preds = %if.end157, %if.then148
  %197 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth159 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %197, i32 0, i32 2
  %198 = load i32, i32* %depth159, align 4, !tbaa !6
  %cmp160 = icmp eq i32 %198, 8
  br i1 %cmp160, label %if.then162, label %if.else227

if.then162:                                       ; preds = %if.end158
  %199 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %200 = load i32, i32* %uvJ, align 4, !tbaa !35
  %201 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul163 = mul i32 %200, %201
  %202 = load i32, i32* %uvI, align 4, !tbaa !35
  %203 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul164 = mul i32 %202, %203
  %add165 = add i32 %mul163, %mul164
  %arrayidx166 = getelementptr inbounds i8, i8* %199, i32 %add165
  %204 = load i8, i8* %arrayidx166, align 1, !tbaa !45
  %conv167 = zext i8 %204 to i16
  %arrayidx168 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 0
  %arrayidx169 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx168, i32 0, i32 0
  store i16 %conv167, i16* %arrayidx169, align 2, !tbaa !43
  %205 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %206 = load i32, i32* %uvJ, align 4, !tbaa !35
  %207 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul170 = mul i32 %206, %207
  %208 = load i32, i32* %uvI, align 4, !tbaa !35
  %209 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul171 = mul i32 %208, %209
  %add172 = add i32 %mul170, %mul171
  %arrayidx173 = getelementptr inbounds i8, i8* %205, i32 %add172
  %210 = load i8, i8* %arrayidx173, align 1, !tbaa !45
  %conv174 = zext i8 %210 to i16
  %arrayidx175 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 0
  %arrayidx176 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx175, i32 0, i32 0
  store i16 %conv174, i16* %arrayidx176, align 2, !tbaa !43
  %211 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %212 = load i32, i32* %uvJ, align 4, !tbaa !35
  %213 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul177 = mul i32 %212, %213
  %214 = load i32, i32* %uvI, align 4, !tbaa !35
  %215 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul178 = mul i32 %214, %215
  %add179 = add i32 %mul177, %mul178
  %216 = load i32, i32* %uAdjCol, align 4, !tbaa !35
  %add180 = add i32 %add179, %216
  %arrayidx181 = getelementptr inbounds i8, i8* %211, i32 %add180
  %217 = load i8, i8* %arrayidx181, align 1, !tbaa !45
  %conv182 = zext i8 %217 to i16
  %arrayidx183 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 1
  %arrayidx184 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx183, i32 0, i32 0
  store i16 %conv182, i16* %arrayidx184, align 2, !tbaa !43
  %218 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %219 = load i32, i32* %uvJ, align 4, !tbaa !35
  %220 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul185 = mul i32 %219, %220
  %221 = load i32, i32* %uvI, align 4, !tbaa !35
  %222 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul186 = mul i32 %221, %222
  %add187 = add i32 %mul185, %mul186
  %223 = load i32, i32* %vAdjCol, align 4, !tbaa !35
  %add188 = add i32 %add187, %223
  %arrayidx189 = getelementptr inbounds i8, i8* %218, i32 %add188
  %224 = load i8, i8* %arrayidx189, align 1, !tbaa !45
  %conv190 = zext i8 %224 to i16
  %arrayidx191 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 1
  %arrayidx192 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx191, i32 0, i32 0
  store i16 %conv190, i16* %arrayidx192, align 2, !tbaa !43
  %225 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %226 = load i32, i32* %uvJ, align 4, !tbaa !35
  %227 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul193 = mul i32 %226, %227
  %228 = load i32, i32* %uvI, align 4, !tbaa !35
  %229 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul194 = mul i32 %228, %229
  %add195 = add i32 %mul193, %mul194
  %230 = load i32, i32* %uAdjRow, align 4, !tbaa !35
  %add196 = add i32 %add195, %230
  %arrayidx197 = getelementptr inbounds i8, i8* %225, i32 %add196
  %231 = load i8, i8* %arrayidx197, align 1, !tbaa !45
  %conv198 = zext i8 %231 to i16
  %arrayidx199 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 0
  %arrayidx200 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx199, i32 0, i32 1
  store i16 %conv198, i16* %arrayidx200, align 2, !tbaa !43
  %232 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %233 = load i32, i32* %uvJ, align 4, !tbaa !35
  %234 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul201 = mul i32 %233, %234
  %235 = load i32, i32* %uvI, align 4, !tbaa !35
  %236 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul202 = mul i32 %235, %236
  %add203 = add i32 %mul201, %mul202
  %237 = load i32, i32* %vAdjRow, align 4, !tbaa !35
  %add204 = add i32 %add203, %237
  %arrayidx205 = getelementptr inbounds i8, i8* %232, i32 %add204
  %238 = load i8, i8* %arrayidx205, align 1, !tbaa !45
  %conv206 = zext i8 %238 to i16
  %arrayidx207 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 0
  %arrayidx208 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx207, i32 0, i32 1
  store i16 %conv206, i16* %arrayidx208, align 2, !tbaa !43
  %239 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %240 = load i32, i32* %uvJ, align 4, !tbaa !35
  %241 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul209 = mul i32 %240, %241
  %242 = load i32, i32* %uvI, align 4, !tbaa !35
  %243 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul210 = mul i32 %242, %243
  %add211 = add i32 %mul209, %mul210
  %244 = load i32, i32* %uAdjCol, align 4, !tbaa !35
  %add212 = add i32 %add211, %244
  %245 = load i32, i32* %uAdjRow, align 4, !tbaa !35
  %add213 = add i32 %add212, %245
  %arrayidx214 = getelementptr inbounds i8, i8* %239, i32 %add213
  %246 = load i8, i8* %arrayidx214, align 1, !tbaa !45
  %conv215 = zext i8 %246 to i16
  %arrayidx216 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 1
  %arrayidx217 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx216, i32 0, i32 1
  store i16 %conv215, i16* %arrayidx217, align 2, !tbaa !43
  %247 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %248 = load i32, i32* %uvJ, align 4, !tbaa !35
  %249 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul218 = mul i32 %248, %249
  %250 = load i32, i32* %uvI, align 4, !tbaa !35
  %251 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul219 = mul i32 %250, %251
  %add220 = add i32 %mul218, %mul219
  %252 = load i32, i32* %vAdjCol, align 4, !tbaa !35
  %add221 = add i32 %add220, %252
  %253 = load i32, i32* %vAdjRow, align 4, !tbaa !35
  %add222 = add i32 %add221, %253
  %arrayidx223 = getelementptr inbounds i8, i8* %247, i32 %add222
  %254 = load i8, i8* %arrayidx223, align 1, !tbaa !45
  %conv224 = zext i8 %254 to i16
  %arrayidx225 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 1
  %arrayidx226 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx225, i32 0, i32 1
  store i16 %conv224, i16* %arrayidx226, align 2, !tbaa !43
  br label %if.end331

if.else227:                                       ; preds = %if.end158
  %255 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %256 = load i32, i32* %uvJ, align 4, !tbaa !35
  %257 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul228 = mul i32 %256, %257
  %258 = load i32, i32* %uvI, align 4, !tbaa !35
  %259 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul229 = mul i32 %258, %259
  %add230 = add i32 %mul228, %mul229
  %arrayidx231 = getelementptr inbounds i8, i8* %255, i32 %add230
  %260 = bitcast i8* %arrayidx231 to i16*
  %261 = load i16, i16* %260, align 2, !tbaa !43
  %arrayidx232 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 0
  %arrayidx233 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx232, i32 0, i32 0
  store i16 %261, i16* %arrayidx233, align 2, !tbaa !43
  %262 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %263 = load i32, i32* %uvJ, align 4, !tbaa !35
  %264 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul234 = mul i32 %263, %264
  %265 = load i32, i32* %uvI, align 4, !tbaa !35
  %266 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul235 = mul i32 %265, %266
  %add236 = add i32 %mul234, %mul235
  %arrayidx237 = getelementptr inbounds i8, i8* %262, i32 %add236
  %267 = bitcast i8* %arrayidx237 to i16*
  %268 = load i16, i16* %267, align 2, !tbaa !43
  %arrayidx238 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 0
  %arrayidx239 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx238, i32 0, i32 0
  store i16 %268, i16* %arrayidx239, align 2, !tbaa !43
  %269 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %270 = load i32, i32* %uvJ, align 4, !tbaa !35
  %271 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul240 = mul i32 %270, %271
  %272 = load i32, i32* %uvI, align 4, !tbaa !35
  %273 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul241 = mul i32 %272, %273
  %add242 = add i32 %mul240, %mul241
  %274 = load i32, i32* %uAdjCol, align 4, !tbaa !35
  %add243 = add i32 %add242, %274
  %arrayidx244 = getelementptr inbounds i8, i8* %269, i32 %add243
  %275 = bitcast i8* %arrayidx244 to i16*
  %276 = load i16, i16* %275, align 2, !tbaa !43
  %arrayidx245 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 1
  %arrayidx246 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx245, i32 0, i32 0
  store i16 %276, i16* %arrayidx246, align 2, !tbaa !43
  %277 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %278 = load i32, i32* %uvJ, align 4, !tbaa !35
  %279 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul247 = mul i32 %278, %279
  %280 = load i32, i32* %uvI, align 4, !tbaa !35
  %281 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul248 = mul i32 %280, %281
  %add249 = add i32 %mul247, %mul248
  %282 = load i32, i32* %vAdjCol, align 4, !tbaa !35
  %add250 = add i32 %add249, %282
  %arrayidx251 = getelementptr inbounds i8, i8* %277, i32 %add250
  %283 = bitcast i8* %arrayidx251 to i16*
  %284 = load i16, i16* %283, align 2, !tbaa !43
  %arrayidx252 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 1
  %arrayidx253 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx252, i32 0, i32 0
  store i16 %284, i16* %arrayidx253, align 2, !tbaa !43
  %285 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %286 = load i32, i32* %uvJ, align 4, !tbaa !35
  %287 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul254 = mul i32 %286, %287
  %288 = load i32, i32* %uvI, align 4, !tbaa !35
  %289 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul255 = mul i32 %288, %289
  %add256 = add i32 %mul254, %mul255
  %290 = load i32, i32* %uAdjRow, align 4, !tbaa !35
  %add257 = add i32 %add256, %290
  %arrayidx258 = getelementptr inbounds i8, i8* %285, i32 %add257
  %291 = bitcast i8* %arrayidx258 to i16*
  %292 = load i16, i16* %291, align 2, !tbaa !43
  %arrayidx259 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 0
  %arrayidx260 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx259, i32 0, i32 1
  store i16 %292, i16* %arrayidx260, align 2, !tbaa !43
  %293 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %294 = load i32, i32* %uvJ, align 4, !tbaa !35
  %295 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul261 = mul i32 %294, %295
  %296 = load i32, i32* %uvI, align 4, !tbaa !35
  %297 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul262 = mul i32 %296, %297
  %add263 = add i32 %mul261, %mul262
  %298 = load i32, i32* %vAdjRow, align 4, !tbaa !35
  %add264 = add i32 %add263, %298
  %arrayidx265 = getelementptr inbounds i8, i8* %293, i32 %add264
  %299 = bitcast i8* %arrayidx265 to i16*
  %300 = load i16, i16* %299, align 2, !tbaa !43
  %arrayidx266 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 0
  %arrayidx267 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx266, i32 0, i32 1
  store i16 %300, i16* %arrayidx267, align 2, !tbaa !43
  %301 = load i8*, i8** %uPlane, align 4, !tbaa !2
  %302 = load i32, i32* %uvJ, align 4, !tbaa !35
  %303 = load i32, i32* %uRowBytes, align 4, !tbaa !35
  %mul268 = mul i32 %302, %303
  %304 = load i32, i32* %uvI, align 4, !tbaa !35
  %305 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul269 = mul i32 %304, %305
  %add270 = add i32 %mul268, %mul269
  %306 = load i32, i32* %uAdjCol, align 4, !tbaa !35
  %add271 = add i32 %add270, %306
  %307 = load i32, i32* %uAdjRow, align 4, !tbaa !35
  %add272 = add i32 %add271, %307
  %arrayidx273 = getelementptr inbounds i8, i8* %301, i32 %add272
  %308 = bitcast i8* %arrayidx273 to i16*
  %309 = load i16, i16* %308, align 2, !tbaa !43
  %arrayidx274 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 1
  %arrayidx275 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx274, i32 0, i32 1
  store i16 %309, i16* %arrayidx275, align 2, !tbaa !43
  %310 = load i8*, i8** %vPlane, align 4, !tbaa !2
  %311 = load i32, i32* %uvJ, align 4, !tbaa !35
  %312 = load i32, i32* %vRowBytes, align 4, !tbaa !35
  %mul276 = mul i32 %311, %312
  %313 = load i32, i32* %uvI, align 4, !tbaa !35
  %314 = load i32, i32* %yuvChannelBytes, align 4, !tbaa !35
  %mul277 = mul i32 %313, %314
  %add278 = add i32 %mul276, %mul277
  %315 = load i32, i32* %vAdjCol, align 4, !tbaa !35
  %add279 = add i32 %add278, %315
  %316 = load i32, i32* %vAdjRow, align 4, !tbaa !35
  %add280 = add i32 %add279, %316
  %arrayidx281 = getelementptr inbounds i8, i8* %310, i32 %add280
  %317 = bitcast i8* %arrayidx281 to i16*
  %318 = load i16, i16* %317, align 2, !tbaa !43
  %arrayidx282 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 1
  %arrayidx283 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx282, i32 0, i32 1
  store i16 %318, i16* %arrayidx283, align 2, !tbaa !43
  %319 = bitcast i32* %bJ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %319) #3
  store i32 0, i32* %bJ, align 4, !tbaa !35
  br label %for.cond284

for.cond284:                                      ; preds = %for.inc328, %if.else227
  %320 = load i32, i32* %bJ, align 4, !tbaa !35
  %cmp285 = icmp slt i32 %320, 2
  br i1 %cmp285, label %for.body288, label %for.cond.cleanup287

for.cond.cleanup287:                              ; preds = %for.cond284
  store i32 8, i32* %cleanup.dest.slot, align 4
  %321 = bitcast i32* %bJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %321) #3
  br label %for.end330

for.body288:                                      ; preds = %for.cond284
  %322 = bitcast i32* %bI to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %322) #3
  store i32 0, i32* %bI, align 4, !tbaa !35
  br label %for.cond289

for.cond289:                                      ; preds = %for.inc, %for.body288
  %323 = load i32, i32* %bI, align 4, !tbaa !35
  %cmp290 = icmp slt i32 %323, 2
  br i1 %cmp290, label %for.body293, label %for.cond.cleanup292

for.cond.cleanup292:                              ; preds = %for.cond289
  store i32 11, i32* %cleanup.dest.slot, align 4
  %324 = bitcast i32* %bI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %324) #3
  br label %for.end

for.body293:                                      ; preds = %for.cond289
  %325 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx294 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 %325
  %326 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx295 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx294, i32 0, i32 %326
  %327 = load i16, i16* %arrayidx295, align 2, !tbaa !43
  %conv296 = zext i16 %327 to i32
  %328 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv297 = zext i16 %328 to i32
  %cmp298 = icmp slt i32 %conv296, %conv297
  br i1 %cmp298, label %cond.true300, label %cond.false304

cond.true300:                                     ; preds = %for.body293
  %329 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx301 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 %329
  %330 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx302 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx301, i32 0, i32 %330
  %331 = load i16, i16* %arrayidx302, align 2, !tbaa !43
  %conv303 = zext i16 %331 to i32
  br label %cond.end306

cond.false304:                                    ; preds = %for.body293
  %332 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv305 = zext i16 %332 to i32
  br label %cond.end306

cond.end306:                                      ; preds = %cond.false304, %cond.true300
  %cond307 = phi i32 [ %conv303, %cond.true300 ], [ %conv305, %cond.false304 ]
  %conv308 = trunc i32 %cond307 to i16
  %333 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx309 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 %333
  %334 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx310 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx309, i32 0, i32 %334
  store i16 %conv308, i16* %arrayidx310, align 2, !tbaa !43
  %335 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx311 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 %335
  %336 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx312 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx311, i32 0, i32 %336
  %337 = load i16, i16* %arrayidx312, align 2, !tbaa !43
  %conv313 = zext i16 %337 to i32
  %338 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv314 = zext i16 %338 to i32
  %cmp315 = icmp slt i32 %conv313, %conv314
  br i1 %cmp315, label %cond.true317, label %cond.false321

cond.true317:                                     ; preds = %cond.end306
  %339 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx318 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 %339
  %340 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx319 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx318, i32 0, i32 %340
  %341 = load i16, i16* %arrayidx319, align 2, !tbaa !43
  %conv320 = zext i16 %341 to i32
  br label %cond.end323

cond.false321:                                    ; preds = %cond.end306
  %342 = load i16, i16* %yuvMaxChannel, align 2, !tbaa !43
  %conv322 = zext i16 %342 to i32
  br label %cond.end323

cond.end323:                                      ; preds = %cond.false321, %cond.true317
  %cond324 = phi i32 [ %conv320, %cond.true317 ], [ %conv322, %cond.false321 ]
  %conv325 = trunc i32 %cond324 to i16
  %343 = load i32, i32* %bI, align 4, !tbaa !35
  %arrayidx326 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 %343
  %344 = load i32, i32* %bJ, align 4, !tbaa !35
  %arrayidx327 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx326, i32 0, i32 %344
  store i16 %conv325, i16* %arrayidx327, align 2, !tbaa !43
  br label %for.inc

for.inc:                                          ; preds = %cond.end323
  %345 = load i32, i32* %bI, align 4, !tbaa !35
  %inc = add nsw i32 %345, 1
  store i32 %inc, i32* %bI, align 4, !tbaa !35
  br label %for.cond289

for.end:                                          ; preds = %for.cond.cleanup292
  br label %for.inc328

for.inc328:                                       ; preds = %for.end
  %346 = load i32, i32* %bJ, align 4, !tbaa !35
  %inc329 = add nsw i32 %346, 1
  store i32 %inc329, i32* %bJ, align 4, !tbaa !35
  br label %for.cond284

for.end330:                                       ; preds = %for.cond.cleanup287
  br label %if.end331

if.end331:                                        ; preds = %for.end330, %if.then162
  %347 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %chromaUpsampling = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %347, i32 0, i32 4
  %348 = load i32, i32* %chromaUpsampling, align 4, !tbaa !70
  %cmp332 = icmp eq i32 %348, 0
  br i1 %cmp332, label %if.then334, label %if.else381

if.then334:                                       ; preds = %if.end331
  %349 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx335 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 0
  %arrayidx336 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx335, i32 0, i32 0
  %350 = load i16, i16* %arrayidx336, align 2, !tbaa !43
  %idxprom337 = zext i16 %350 to i32
  %arrayidx338 = getelementptr inbounds float, float* %349, i32 %idxprom337
  %351 = load float, float* %arrayidx338, align 4, !tbaa !36
  %mul339 = fmul float %351, 5.625000e-01
  %352 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx340 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 1
  %arrayidx341 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx340, i32 0, i32 0
  %353 = load i16, i16* %arrayidx341, align 2, !tbaa !43
  %idxprom342 = zext i16 %353 to i32
  %arrayidx343 = getelementptr inbounds float, float* %352, i32 %idxprom342
  %354 = load float, float* %arrayidx343, align 4, !tbaa !36
  %mul344 = fmul float %354, 1.875000e-01
  %add345 = fadd float %mul339, %mul344
  %355 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx346 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 0
  %arrayidx347 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx346, i32 0, i32 1
  %356 = load i16, i16* %arrayidx347, align 2, !tbaa !43
  %idxprom348 = zext i16 %356 to i32
  %arrayidx349 = getelementptr inbounds float, float* %355, i32 %idxprom348
  %357 = load float, float* %arrayidx349, align 4, !tbaa !36
  %mul350 = fmul float %357, 1.875000e-01
  %add351 = fadd float %add345, %mul350
  %358 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx352 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 1
  %arrayidx353 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx352, i32 0, i32 1
  %359 = load i16, i16* %arrayidx353, align 2, !tbaa !43
  %idxprom354 = zext i16 %359 to i32
  %arrayidx355 = getelementptr inbounds float, float* %358, i32 %idxprom354
  %360 = load float, float* %arrayidx355, align 4, !tbaa !36
  %mul356 = fmul float %360, 6.250000e-02
  %add357 = fadd float %add351, %mul356
  store float %add357, float* %Cb, align 4, !tbaa !36
  %361 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx358 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 0
  %arrayidx359 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx358, i32 0, i32 0
  %362 = load i16, i16* %arrayidx359, align 2, !tbaa !43
  %idxprom360 = zext i16 %362 to i32
  %arrayidx361 = getelementptr inbounds float, float* %361, i32 %idxprom360
  %363 = load float, float* %arrayidx361, align 4, !tbaa !36
  %mul362 = fmul float %363, 5.625000e-01
  %364 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx363 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 1
  %arrayidx364 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx363, i32 0, i32 0
  %365 = load i16, i16* %arrayidx364, align 2, !tbaa !43
  %idxprom365 = zext i16 %365 to i32
  %arrayidx366 = getelementptr inbounds float, float* %364, i32 %idxprom365
  %366 = load float, float* %arrayidx366, align 4, !tbaa !36
  %mul367 = fmul float %366, 1.875000e-01
  %add368 = fadd float %mul362, %mul367
  %367 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx369 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 0
  %arrayidx370 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx369, i32 0, i32 1
  %368 = load i16, i16* %arrayidx370, align 2, !tbaa !43
  %idxprom371 = zext i16 %368 to i32
  %arrayidx372 = getelementptr inbounds float, float* %367, i32 %idxprom371
  %369 = load float, float* %arrayidx372, align 4, !tbaa !36
  %mul373 = fmul float %369, 1.875000e-01
  %add374 = fadd float %add368, %mul373
  %370 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx375 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 1
  %arrayidx376 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx375, i32 0, i32 1
  %371 = load i16, i16* %arrayidx376, align 2, !tbaa !43
  %idxprom377 = zext i16 %371 to i32
  %arrayidx378 = getelementptr inbounds float, float* %370, i32 %idxprom377
  %372 = load float, float* %arrayidx378, align 4, !tbaa !36
  %mul379 = fmul float %372, 6.250000e-02
  %add380 = fadd float %add374, %mul379
  store float %add380, float* %Cr, align 4, !tbaa !36
  br label %if.end390

if.else381:                                       ; preds = %if.end331
  %373 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx382 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormU111, i32 0, i32 0
  %arrayidx383 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx382, i32 0, i32 0
  %374 = load i16, i16* %arrayidx383, align 2, !tbaa !43
  %idxprom384 = zext i16 %374 to i32
  %arrayidx385 = getelementptr inbounds float, float* %373, i32 %idxprom384
  %375 = load float, float* %arrayidx385, align 4, !tbaa !36
  store float %375, float* %Cb, align 4, !tbaa !36
  %376 = load float*, float** %unormFloatTableUV, align 4, !tbaa !2
  %arrayidx386 = getelementptr inbounds [2 x [2 x i16]], [2 x [2 x i16]]* %unormV112, i32 0, i32 0
  %arrayidx387 = getelementptr inbounds [2 x i16], [2 x i16]* %arrayidx386, i32 0, i32 0
  %377 = load i16, i16* %arrayidx387, align 2, !tbaa !43
  %idxprom388 = zext i16 %377 to i32
  %arrayidx389 = getelementptr inbounds float, float* %376, i32 %idxprom388
  %378 = load float, float* %arrayidx389, align 4, !tbaa !36
  store float %378, float* %Cr, align 4, !tbaa !36
  br label %if.end390

if.end390:                                        ; preds = %if.else381, %if.then334
  %379 = bitcast i32* %vAdjRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %379) #3
  %380 = bitcast i32* %uAdjRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %380) #3
  %381 = bitcast i32* %vAdjCol to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %381) #3
  %382 = bitcast i32* %uAdjCol to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %382) #3
  %383 = bitcast [2 x [2 x i16]]* %unormV112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %383) #3
  %384 = bitcast [2 x [2 x i16]]* %unormU111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %384) #3
  br label %if.end391

if.end391:                                        ; preds = %if.end390, %if.end105
  br label %if.end392

if.end392:                                        ; preds = %if.end391, %if.end
  %385 = bitcast float* %R to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %385) #3
  %386 = bitcast float* %G to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %386) #3
  %387 = bitcast float* %B to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %387) #3
  %388 = load i32, i32* %hasColor, align 4, !tbaa !35
  %tobool393 = icmp ne i32 %388, 0
  br i1 %tobool393, label %if.then394, label %if.else417

if.then394:                                       ; preds = %if.end392
  %389 = load %struct.avifReformatState*, %struct.avifReformatState** %state.addr, align 4, !tbaa !2
  %mode = getelementptr inbounds %struct.avifReformatState, %struct.avifReformatState* %389, i32 0, i32 14
  %390 = load i32, i32* %mode, align 4, !tbaa !18
  %cmp395 = icmp eq i32 %390, 1
  br i1 %cmp395, label %if.then397, label %if.else398

if.then397:                                       ; preds = %if.then394
  %391 = load float, float* %Y, align 4, !tbaa !36
  store float %391, float* %G, align 4, !tbaa !36
  %392 = load float, float* %Cb, align 4, !tbaa !36
  store float %392, float* %B, align 4, !tbaa !36
  %393 = load float, float* %Cr, align 4, !tbaa !36
  store float %393, float* %R, align 4, !tbaa !36
  br label %if.end416

if.else398:                                       ; preds = %if.then394
  %394 = load float, float* %Y, align 4, !tbaa !36
  %395 = load float, float* %kr, align 4, !tbaa !36
  %sub399 = fsub float 1.000000e+00, %395
  %mul400 = fmul float 2.000000e+00, %sub399
  %396 = load float, float* %Cr, align 4, !tbaa !36
  %mul401 = fmul float %mul400, %396
  %add402 = fadd float %394, %mul401
  store float %add402, float* %R, align 4, !tbaa !36
  %397 = load float, float* %Y, align 4, !tbaa !36
  %398 = load float, float* %kb, align 4, !tbaa !36
  %sub403 = fsub float 1.000000e+00, %398
  %mul404 = fmul float 2.000000e+00, %sub403
  %399 = load float, float* %Cb, align 4, !tbaa !36
  %mul405 = fmul float %mul404, %399
  %add406 = fadd float %397, %mul405
  store float %add406, float* %B, align 4, !tbaa !36
  %400 = load float, float* %Y, align 4, !tbaa !36
  %401 = load float, float* %kr, align 4, !tbaa !36
  %402 = load float, float* %kr, align 4, !tbaa !36
  %sub407 = fsub float 1.000000e+00, %402
  %mul408 = fmul float %401, %sub407
  %403 = load float, float* %Cr, align 4, !tbaa !36
  %mul409 = fmul float %mul408, %403
  %404 = load float, float* %kb, align 4, !tbaa !36
  %405 = load float, float* %kb, align 4, !tbaa !36
  %sub410 = fsub float 1.000000e+00, %405
  %mul411 = fmul float %404, %sub410
  %406 = load float, float* %Cb, align 4, !tbaa !36
  %mul412 = fmul float %mul411, %406
  %add413 = fadd float %mul409, %mul412
  %mul414 = fmul float 2.000000e+00, %add413
  %407 = load float, float* %kg, align 4, !tbaa !36
  %div = fdiv float %mul414, %407
  %sub415 = fsub float %400, %div
  store float %sub415, float* %G, align 4, !tbaa !36
  br label %if.end416

if.end416:                                        ; preds = %if.else398, %if.then397
  br label %if.end418

if.else417:                                       ; preds = %if.end392
  %408 = load float, float* %Y, align 4, !tbaa !36
  store float %408, float* %R, align 4, !tbaa !36
  %409 = load float, float* %Y, align 4, !tbaa !36
  store float %409, float* %G, align 4, !tbaa !36
  %410 = load float, float* %Y, align 4, !tbaa !36
  store float %410, float* %B, align 4, !tbaa !36
  br label %if.end418

if.end418:                                        ; preds = %if.else417, %if.end416
  %411 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %411) #3
  %412 = load float, float* %R, align 4, !tbaa !36
  %cmp419 = fcmp olt float %412, 0.000000e+00
  br i1 %cmp419, label %cond.true421, label %cond.false422

cond.true421:                                     ; preds = %if.end418
  br label %cond.end429

cond.false422:                                    ; preds = %if.end418
  %413 = load float, float* %R, align 4, !tbaa !36
  %cmp423 = fcmp olt float 1.000000e+00, %413
  br i1 %cmp423, label %cond.true425, label %cond.false426

cond.true425:                                     ; preds = %cond.false422
  br label %cond.end427

cond.false426:                                    ; preds = %cond.false422
  %414 = load float, float* %R, align 4, !tbaa !36
  br label %cond.end427

cond.end427:                                      ; preds = %cond.false426, %cond.true425
  %cond428 = phi float [ 1.000000e+00, %cond.true425 ], [ %414, %cond.false426 ]
  br label %cond.end429

cond.end429:                                      ; preds = %cond.end427, %cond.true421
  %cond430 = phi float [ 0.000000e+00, %cond.true421 ], [ %cond428, %cond.end427 ]
  store float %cond430, float* %Rc, align 4, !tbaa !36
  %415 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %415) #3
  %416 = load float, float* %G, align 4, !tbaa !36
  %cmp431 = fcmp olt float %416, 0.000000e+00
  br i1 %cmp431, label %cond.true433, label %cond.false434

cond.true433:                                     ; preds = %cond.end429
  br label %cond.end441

cond.false434:                                    ; preds = %cond.end429
  %417 = load float, float* %G, align 4, !tbaa !36
  %cmp435 = fcmp olt float 1.000000e+00, %417
  br i1 %cmp435, label %cond.true437, label %cond.false438

cond.true437:                                     ; preds = %cond.false434
  br label %cond.end439

cond.false438:                                    ; preds = %cond.false434
  %418 = load float, float* %G, align 4, !tbaa !36
  br label %cond.end439

cond.end439:                                      ; preds = %cond.false438, %cond.true437
  %cond440 = phi float [ 1.000000e+00, %cond.true437 ], [ %418, %cond.false438 ]
  br label %cond.end441

cond.end441:                                      ; preds = %cond.end439, %cond.true433
  %cond442 = phi float [ 0.000000e+00, %cond.true433 ], [ %cond440, %cond.end439 ]
  store float %cond442, float* %Gc, align 4, !tbaa !36
  %419 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %419) #3
  %420 = load float, float* %B, align 4, !tbaa !36
  %cmp443 = fcmp olt float %420, 0.000000e+00
  br i1 %cmp443, label %cond.true445, label %cond.false446

cond.true445:                                     ; preds = %cond.end441
  br label %cond.end453

cond.false446:                                    ; preds = %cond.end441
  %421 = load float, float* %B, align 4, !tbaa !36
  %cmp447 = fcmp olt float 1.000000e+00, %421
  br i1 %cmp447, label %cond.true449, label %cond.false450

cond.true449:                                     ; preds = %cond.false446
  br label %cond.end451

cond.false450:                                    ; preds = %cond.false446
  %422 = load float, float* %B, align 4, !tbaa !36
  br label %cond.end451

cond.end451:                                      ; preds = %cond.false450, %cond.true449
  %cond452 = phi float [ 1.000000e+00, %cond.true449 ], [ %422, %cond.false450 ]
  br label %cond.end453

cond.end453:                                      ; preds = %cond.end451, %cond.true445
  %cond454 = phi float [ 0.000000e+00, %cond.true445 ], [ %cond452, %cond.end451 ]
  store float %cond454, float* %Bc, align 4, !tbaa !36
  %423 = load %struct.avifRGBImage*, %struct.avifRGBImage** %rgb.addr, align 4, !tbaa !2
  %depth455 = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %423, i32 0, i32 2
  %424 = load i32, i32* %depth455, align 4, !tbaa !15
  %cmp456 = icmp eq i32 %424, 8
  br i1 %cmp456, label %if.then458, label %if.else468

if.then458:                                       ; preds = %cond.end453
  %425 = load float, float* %Rc, align 4, !tbaa !36
  %426 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul459 = fmul float %425, %426
  %add460 = fadd float 5.000000e-01, %mul459
  %conv461 = fptoui float %add460 to i8
  %427 = load i8*, i8** %ptrR, align 4, !tbaa !2
  store i8 %conv461, i8* %427, align 1, !tbaa !45
  %428 = load float, float* %Gc, align 4, !tbaa !36
  %429 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul462 = fmul float %428, %429
  %add463 = fadd float 5.000000e-01, %mul462
  %conv464 = fptoui float %add463 to i8
  %430 = load i8*, i8** %ptrG, align 4, !tbaa !2
  store i8 %conv464, i8* %430, align 1, !tbaa !45
  %431 = load float, float* %Bc, align 4, !tbaa !36
  %432 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul465 = fmul float %431, %432
  %add466 = fadd float 5.000000e-01, %mul465
  %conv467 = fptoui float %add466 to i8
  %433 = load i8*, i8** %ptrB, align 4, !tbaa !2
  store i8 %conv467, i8* %433, align 1, !tbaa !45
  br label %if.end478

if.else468:                                       ; preds = %cond.end453
  %434 = load float, float* %Rc, align 4, !tbaa !36
  %435 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul469 = fmul float %434, %435
  %add470 = fadd float 5.000000e-01, %mul469
  %conv471 = fptoui float %add470 to i16
  %436 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %437 = bitcast i8* %436 to i16*
  store i16 %conv471, i16* %437, align 2, !tbaa !43
  %438 = load float, float* %Gc, align 4, !tbaa !36
  %439 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul472 = fmul float %438, %439
  %add473 = fadd float 5.000000e-01, %mul472
  %conv474 = fptoui float %add473 to i16
  %440 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %441 = bitcast i8* %440 to i16*
  store i16 %conv474, i16* %441, align 2, !tbaa !43
  %442 = load float, float* %Bc, align 4, !tbaa !36
  %443 = load float, float* %rgbMaxChannel, align 4, !tbaa !36
  %mul475 = fmul float %442, %443
  %add476 = fadd float 5.000000e-01, %mul475
  %conv477 = fptoui float %add476 to i16
  %444 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %445 = bitcast i8* %444 to i16*
  store i16 %conv477, i16* %445, align 2, !tbaa !43
  br label %if.end478

if.end478:                                        ; preds = %if.else468, %if.then458
  %446 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %447 = load i8*, i8** %ptrR, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %447, i32 %446
  store i8* %add.ptr, i8** %ptrR, align 4, !tbaa !2
  %448 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %449 = load i8*, i8** %ptrG, align 4, !tbaa !2
  %add.ptr479 = getelementptr inbounds i8, i8* %449, i32 %448
  store i8* %add.ptr479, i8** %ptrG, align 4, !tbaa !2
  %450 = load i32, i32* %rgbPixelBytes, align 4, !tbaa !35
  %451 = load i8*, i8** %ptrB, align 4, !tbaa !2
  %add.ptr480 = getelementptr inbounds i8, i8* %451, i32 %450
  store i8* %add.ptr480, i8** %ptrB, align 4, !tbaa !2
  %452 = bitcast float* %Bc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %452) #3
  %453 = bitcast float* %Gc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %453) #3
  %454 = bitcast float* %Rc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %454) #3
  %455 = bitcast float* %B to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %455) #3
  %456 = bitcast float* %G to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %456) #3
  %457 = bitcast float* %R to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %457) #3
  %458 = bitcast i16* %unormY to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %458) #3
  %459 = bitcast float* %Cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %459) #3
  %460 = bitcast float* %Cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %460) #3
  %461 = bitcast float* %Y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %461) #3
  %462 = bitcast i32* %uvI to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %462) #3
  br label %for.inc481

for.inc481:                                       ; preds = %if.end478
  %463 = load i32, i32* %i, align 4, !tbaa !35
  %inc482 = add i32 %463, 1
  store i32 %inc482, i32* %i, align 4, !tbaa !35
  br label %for.cond42

for.end483:                                       ; preds = %for.cond.cleanup45
  %464 = bitcast i8** %ptrB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %464) #3
  %465 = bitcast i8** %ptrG to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %465) #3
  %466 = bitcast i8** %ptrR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %466) #3
  %467 = bitcast i16** %ptrV16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %467) #3
  %468 = bitcast i16** %ptrU16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %468) #3
  %469 = bitcast i16** %ptrY16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %469) #3
  %470 = bitcast i8** %ptrV8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %470) #3
  %471 = bitcast i8** %ptrU8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %471) #3
  %472 = bitcast i8** %ptrY8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %472) #3
  %473 = bitcast i32* %uvJ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %473) #3
  br label %for.inc484

for.inc484:                                       ; preds = %for.end483
  %474 = load i32, i32* %j, align 4, !tbaa !35
  %inc485 = add i32 %474, 1
  store i32 %inc485, i32* %j, align 4, !tbaa !35
  br label %for.cond

for.end486:                                       ; preds = %for.cond.cleanup
  store i32 1, i32* %cleanup.dest.slot, align 4
  %475 = bitcast float* %rgbMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %475) #3
  %476 = bitcast i16* %yuvMaxChannel to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %476) #3
  %477 = bitcast i32* %hasColor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %477) #3
  %478 = bitcast i32* %vRowBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %478) #3
  %479 = bitcast i32* %uRowBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %479) #3
  %480 = bitcast i32* %yRowBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %480) #3
  %481 = bitcast i8** %vPlane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %481) #3
  %482 = bitcast i8** %uPlane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %482) #3
  %483 = bitcast i8** %yPlane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %483) #3
  %484 = bitcast i32* %rgbPixelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %484) #3
  %485 = bitcast i32* %yuvChannelBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %485) #3
  %486 = bitcast float** %unormFloatTableUV to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %486) #3
  %487 = bitcast float** %unormFloatTableY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %487) #3
  %488 = bitcast float* %kb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %488) #3
  %489 = bitcast float* %kg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %489) #3
  %490 = bitcast float* %kr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %490) #3
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @avifFullToLimitedY(i32 %depth, i32 %v) #0 {
entry:
  %depth.addr = alloca i32, align 4
  %v.addr = alloca i32, align 4
  store i32 %depth, i32* %depth.addr, align 4, !tbaa !35
  store i32 %v, i32* %v.addr, align 4, !tbaa !35
  %0 = load i32, i32* %depth.addr, align 4, !tbaa !35
  switch i32 %0, label %sw.epilog [
    i32 8, label %sw.bb
    i32 10, label %sw.bb7
    i32 12, label %sw.bb22
    i32 16, label %sw.bb37
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %v.addr, align 4, !tbaa !35
  %mul = mul nsw i32 %1, 219
  %add = add nsw i32 %mul, 127
  %div = sdiv i32 %add, 255
  %add1 = add nsw i32 %div, 16
  store i32 %add1, i32* %v.addr, align 4, !tbaa !35
  %2 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp = icmp slt i32 %2, 16
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.bb
  br label %cond.end5

cond.false:                                       ; preds = %sw.bb
  %3 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp2 = icmp slt i32 235, %3
  br i1 %cmp2, label %cond.true3, label %cond.false4

cond.true3:                                       ; preds = %cond.false
  br label %cond.end

cond.false4:                                      ; preds = %cond.false
  %4 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end

cond.end:                                         ; preds = %cond.false4, %cond.true3
  %cond = phi i32 [ 235, %cond.true3 ], [ %4, %cond.false4 ]
  br label %cond.end5

cond.end5:                                        ; preds = %cond.end, %cond.true
  %cond6 = phi i32 [ 16, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond6, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry
  %5 = load i32, i32* %v.addr, align 4, !tbaa !35
  %mul8 = mul nsw i32 %5, 876
  %add9 = add nsw i32 %mul8, 511
  %div10 = sdiv i32 %add9, 1023
  %add11 = add nsw i32 %div10, 64
  store i32 %add11, i32* %v.addr, align 4, !tbaa !35
  %6 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp12 = icmp slt i32 %6, 64
  br i1 %cmp12, label %cond.true13, label %cond.false14

cond.true13:                                      ; preds = %sw.bb7
  br label %cond.end20

cond.false14:                                     ; preds = %sw.bb7
  %7 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp15 = icmp slt i32 940, %7
  br i1 %cmp15, label %cond.true16, label %cond.false17

cond.true16:                                      ; preds = %cond.false14
  br label %cond.end18

cond.false17:                                     ; preds = %cond.false14
  %8 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false17, %cond.true16
  %cond19 = phi i32 [ 940, %cond.true16 ], [ %8, %cond.false17 ]
  br label %cond.end20

cond.end20:                                       ; preds = %cond.end18, %cond.true13
  %cond21 = phi i32 [ 64, %cond.true13 ], [ %cond19, %cond.end18 ]
  store i32 %cond21, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb22:                                          ; preds = %entry
  %9 = load i32, i32* %v.addr, align 4, !tbaa !35
  %mul23 = mul nsw i32 %9, 3504
  %add24 = add nsw i32 %mul23, 2047
  %div25 = sdiv i32 %add24, 4095
  %add26 = add nsw i32 %div25, 256
  store i32 %add26, i32* %v.addr, align 4, !tbaa !35
  %10 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp27 = icmp slt i32 %10, 256
  br i1 %cmp27, label %cond.true28, label %cond.false29

cond.true28:                                      ; preds = %sw.bb22
  br label %cond.end35

cond.false29:                                     ; preds = %sw.bb22
  %11 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp30 = icmp slt i32 3760, %11
  br i1 %cmp30, label %cond.true31, label %cond.false32

cond.true31:                                      ; preds = %cond.false29
  br label %cond.end33

cond.false32:                                     ; preds = %cond.false29
  %12 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end33

cond.end33:                                       ; preds = %cond.false32, %cond.true31
  %cond34 = phi i32 [ 3760, %cond.true31 ], [ %12, %cond.false32 ]
  br label %cond.end35

cond.end35:                                       ; preds = %cond.end33, %cond.true28
  %cond36 = phi i32 [ 256, %cond.true28 ], [ %cond34, %cond.end33 ]
  store i32 %cond36, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb37:                                          ; preds = %entry
  %13 = load i32, i32* %v.addr, align 4, !tbaa !35
  %mul38 = mul nsw i32 %13, 59136
  %add39 = add nsw i32 %mul38, 32767
  %div40 = sdiv i32 %add39, 65535
  %add41 = add nsw i32 %div40, 1024
  store i32 %add41, i32* %v.addr, align 4, !tbaa !35
  %14 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp42 = icmp slt i32 %14, 1024
  br i1 %cmp42, label %cond.true43, label %cond.false44

cond.true43:                                      ; preds = %sw.bb37
  br label %cond.end50

cond.false44:                                     ; preds = %sw.bb37
  %15 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp45 = icmp slt i32 60160, %15
  br i1 %cmp45, label %cond.true46, label %cond.false47

cond.true46:                                      ; preds = %cond.false44
  br label %cond.end48

cond.false47:                                     ; preds = %cond.false44
  %16 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end48

cond.end48:                                       ; preds = %cond.false47, %cond.true46
  %cond49 = phi i32 [ 60160, %cond.true46 ], [ %16, %cond.false47 ]
  br label %cond.end50

cond.end50:                                       ; preds = %cond.end48, %cond.true43
  %cond51 = phi i32 [ 1024, %cond.true43 ], [ %cond49, %cond.end48 ]
  store i32 %cond51, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %cond.end50, %cond.end35, %cond.end20, %cond.end5
  %17 = load i32, i32* %v.addr, align 4, !tbaa !35
  ret i32 %17
}

; Function Attrs: nounwind
define hidden i32 @avifFullToLimitedUV(i32 %depth, i32 %v) #0 {
entry:
  %depth.addr = alloca i32, align 4
  %v.addr = alloca i32, align 4
  store i32 %depth, i32* %depth.addr, align 4, !tbaa !35
  store i32 %v, i32* %v.addr, align 4, !tbaa !35
  %0 = load i32, i32* %depth.addr, align 4, !tbaa !35
  switch i32 %0, label %sw.epilog [
    i32 8, label %sw.bb
    i32 10, label %sw.bb7
    i32 12, label %sw.bb22
    i32 16, label %sw.bb37
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %v.addr, align 4, !tbaa !35
  %mul = mul nsw i32 %1, 224
  %add = add nsw i32 %mul, 127
  %div = sdiv i32 %add, 255
  %add1 = add nsw i32 %div, 16
  store i32 %add1, i32* %v.addr, align 4, !tbaa !35
  %2 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp = icmp slt i32 %2, 16
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.bb
  br label %cond.end5

cond.false:                                       ; preds = %sw.bb
  %3 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp2 = icmp slt i32 240, %3
  br i1 %cmp2, label %cond.true3, label %cond.false4

cond.true3:                                       ; preds = %cond.false
  br label %cond.end

cond.false4:                                      ; preds = %cond.false
  %4 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end

cond.end:                                         ; preds = %cond.false4, %cond.true3
  %cond = phi i32 [ 240, %cond.true3 ], [ %4, %cond.false4 ]
  br label %cond.end5

cond.end5:                                        ; preds = %cond.end, %cond.true
  %cond6 = phi i32 [ 16, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond6, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry
  %5 = load i32, i32* %v.addr, align 4, !tbaa !35
  %mul8 = mul nsw i32 %5, 896
  %add9 = add nsw i32 %mul8, 511
  %div10 = sdiv i32 %add9, 1023
  %add11 = add nsw i32 %div10, 64
  store i32 %add11, i32* %v.addr, align 4, !tbaa !35
  %6 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp12 = icmp slt i32 %6, 64
  br i1 %cmp12, label %cond.true13, label %cond.false14

cond.true13:                                      ; preds = %sw.bb7
  br label %cond.end20

cond.false14:                                     ; preds = %sw.bb7
  %7 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp15 = icmp slt i32 960, %7
  br i1 %cmp15, label %cond.true16, label %cond.false17

cond.true16:                                      ; preds = %cond.false14
  br label %cond.end18

cond.false17:                                     ; preds = %cond.false14
  %8 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false17, %cond.true16
  %cond19 = phi i32 [ 960, %cond.true16 ], [ %8, %cond.false17 ]
  br label %cond.end20

cond.end20:                                       ; preds = %cond.end18, %cond.true13
  %cond21 = phi i32 [ 64, %cond.true13 ], [ %cond19, %cond.end18 ]
  store i32 %cond21, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb22:                                          ; preds = %entry
  %9 = load i32, i32* %v.addr, align 4, !tbaa !35
  %mul23 = mul nsw i32 %9, 3584
  %add24 = add nsw i32 %mul23, 2047
  %div25 = sdiv i32 %add24, 4095
  %add26 = add nsw i32 %div25, 256
  store i32 %add26, i32* %v.addr, align 4, !tbaa !35
  %10 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp27 = icmp slt i32 %10, 256
  br i1 %cmp27, label %cond.true28, label %cond.false29

cond.true28:                                      ; preds = %sw.bb22
  br label %cond.end35

cond.false29:                                     ; preds = %sw.bb22
  %11 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp30 = icmp slt i32 3840, %11
  br i1 %cmp30, label %cond.true31, label %cond.false32

cond.true31:                                      ; preds = %cond.false29
  br label %cond.end33

cond.false32:                                     ; preds = %cond.false29
  %12 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end33

cond.end33:                                       ; preds = %cond.false32, %cond.true31
  %cond34 = phi i32 [ 3840, %cond.true31 ], [ %12, %cond.false32 ]
  br label %cond.end35

cond.end35:                                       ; preds = %cond.end33, %cond.true28
  %cond36 = phi i32 [ 256, %cond.true28 ], [ %cond34, %cond.end33 ]
  store i32 %cond36, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.bb37:                                          ; preds = %entry
  %13 = load i32, i32* %v.addr, align 4, !tbaa !35
  %mul38 = mul nsw i32 %13, 60416
  %add39 = add nsw i32 %mul38, 32767
  %div40 = sdiv i32 %add39, 65535
  %add41 = add nsw i32 %div40, 1024
  store i32 %add41, i32* %v.addr, align 4, !tbaa !35
  %14 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp42 = icmp slt i32 %14, 1024
  br i1 %cmp42, label %cond.true43, label %cond.false44

cond.true43:                                      ; preds = %sw.bb37
  br label %cond.end50

cond.false44:                                     ; preds = %sw.bb37
  %15 = load i32, i32* %v.addr, align 4, !tbaa !35
  %cmp45 = icmp slt i32 61440, %15
  br i1 %cmp45, label %cond.true46, label %cond.false47

cond.true46:                                      ; preds = %cond.false44
  br label %cond.end48

cond.false47:                                     ; preds = %cond.false44
  %16 = load i32, i32* %v.addr, align 4, !tbaa !35
  br label %cond.end48

cond.end48:                                       ; preds = %cond.false47, %cond.true46
  %cond49 = phi i32 [ 61440, %cond.true46 ], [ %16, %cond.false47 ]
  br label %cond.end50

cond.end50:                                       ; preds = %cond.end48, %cond.true43
  %cond51 = phi i32 [ 1024, %cond.true43 ], [ %cond49, %cond.end48 ]
  store i32 %cond51, i32* %v.addr, align 4, !tbaa !35
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %cond.end50, %cond.end35, %cond.end20, %cond.end5
  %17 = load i32, i32* %v.addr, align 4, !tbaa !35
  ret i32 %17
}

declare float @avifRoundf(float) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 8}
!7 = !{!"avifImage", !8, i64 0, !8, i64 4, !8, i64 8, !4, i64 12, !4, i64 16, !4, i64 20, !4, i64 24, !4, i64 36, !8, i64 48, !4, i64 52, !3, i64 56, !8, i64 60, !8, i64 64, !9, i64 68, !4, i64 76, !4, i64 80, !4, i64 84, !8, i64 88, !11, i64 92, !12, i64 100, !13, i64 132, !14, i64 133, !9, i64 136, !9, i64 144}
!8 = !{!"int", !4, i64 0}
!9 = !{!"avifRWData", !3, i64 0, !10, i64 4}
!10 = !{!"long", !4, i64 0}
!11 = !{!"avifPixelAspectRatioBox", !8, i64 0, !8, i64 4}
!12 = !{!"avifCleanApertureBox", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28}
!13 = !{!"avifImageRotation", !4, i64 0}
!14 = !{!"avifImageMirror", !4, i64 0}
!15 = !{!16, !8, i64 8}
!16 = !{!"avifRGBImage", !8, i64 0, !8, i64 4, !8, i64 8, !4, i64 12, !4, i64 16, !8, i64 20, !3, i64 24, !8, i64 28}
!17 = !{!7, !4, i64 12}
!18 = !{!19, !4, i64 32824}
!19 = !{!"avifReformatState", !20, i64 0, !20, i64 4, !20, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !21, i64 44, !4, i64 56, !4, i64 16440, !4, i64 32824}
!20 = !{!"float", !4, i64 0}
!21 = !{!"avifPixelFormatInfo", !8, i64 0, !8, i64 4, !8, i64 8}
!22 = !{!7, !4, i64 84}
!23 = !{!19, !20, i64 0}
!24 = !{!19, !20, i64 4}
!25 = !{!19, !20, i64 8}
!26 = !{!19, !8, i64 12}
!27 = !{!19, !8, i64 16}
!28 = !{!16, !4, i64 12}
!29 = !{!19, !8, i64 20}
!30 = !{!19, !8, i64 24}
!31 = !{!19, !8, i64 28}
!32 = !{!19, !8, i64 32}
!33 = !{!19, !8, i64 36}
!34 = !{!19, !8, i64 40}
!35 = !{!8, !8, i64 0}
!36 = !{!20, !20, i64 0}
!37 = !{!7, !4, i64 16}
!38 = !{!16, !3, i64 24}
!39 = !{!16, !8, i64 20}
!40 = !{!7, !8, i64 4}
!41 = !{!7, !8, i64 0}
!42 = !{!16, !8, i64 28}
!43 = !{!44, !44, i64 0}
!44 = !{!"short", !4, i64 0}
!45 = !{!4, !4, i64 0}
!46 = !{!47, !20, i64 0}
!47 = !{!"YUVBlock", !20, i64 0, !20, i64 4, !20, i64 8}
!48 = !{!47, !20, i64 4}
!49 = !{!47, !20, i64 8}
!50 = !{!7, !3, i64 56}
!51 = !{!7, !8, i64 60}
!52 = !{!53, !8, i64 0}
!53 = !{!"avifAlphaParams", !8, i64 0, !8, i64 4, !8, i64 8, !4, i64 12, !3, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !4, i64 36, !3, i64 40, !8, i64 44, !8, i64 48, !8, i64 52}
!54 = !{!53, !8, i64 4}
!55 = !{!53, !8, i64 32}
!56 = !{!7, !4, i64 52}
!57 = !{!53, !4, i64 36}
!58 = !{!53, !3, i64 40}
!59 = !{!53, !8, i64 44}
!60 = !{!53, !8, i64 48}
!61 = !{!53, !8, i64 52}
!62 = !{!53, !8, i64 8}
!63 = !{!53, !4, i64 12}
!64 = !{!53, !3, i64 16}
!65 = !{!53, !8, i64 20}
!66 = !{!53, !8, i64 24}
!67 = !{!53, !8, i64 28}
!68 = !{!16, !8, i64 0}
!69 = !{!16, !8, i64 4}
!70 = !{!16, !4, i64 16}
!71 = !{!19, !8, i64 52}
!72 = !{!19, !8, i64 48}
