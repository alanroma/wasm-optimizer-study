; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/third_party/fastfeat/fast.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/third_party/fastfeat/fast.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.xy = type { i32, i32 }

; Function Attrs: nounwind
define hidden %struct.xy* @aom_fast9_detect_nonmax(i8* %im, i32 %xsize, i32 %ysize, i32 %stride, i32 %b, i32* %ret_num_corners) #0 {
entry:
  %im.addr = alloca i8*, align 4
  %xsize.addr = alloca i32, align 4
  %ysize.addr = alloca i32, align 4
  %stride.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  %ret_num_corners.addr = alloca i32*, align 4
  %corners = alloca %struct.xy*, align 4
  %num_corners = alloca i32, align 4
  %scores = alloca i32*, align 4
  %nonmax = alloca %struct.xy*, align 4
  store i8* %im, i8** %im.addr, align 4, !tbaa !2
  store i32 %xsize, i32* %xsize.addr, align 4, !tbaa !6
  store i32 %ysize, i32* %ysize.addr, align 4, !tbaa !6
  store i32 %stride, i32* %stride.addr, align 4, !tbaa !6
  store i32 %b, i32* %b.addr, align 4, !tbaa !6
  store i32* %ret_num_corners, i32** %ret_num_corners.addr, align 4, !tbaa !2
  %0 = bitcast %struct.xy** %corners to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %num_corners to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32** %scores to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast %struct.xy** %nonmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load i8*, i8** %im.addr, align 4, !tbaa !2
  %5 = load i32, i32* %xsize.addr, align 4, !tbaa !6
  %6 = load i32, i32* %ysize.addr, align 4, !tbaa !6
  %7 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %8 = load i32, i32* %b.addr, align 4, !tbaa !6
  %call = call %struct.xy* @aom_fast9_detect(i8* %4, i32 %5, i32 %6, i32 %7, i32 %8, i32* %num_corners)
  store %struct.xy* %call, %struct.xy** %corners, align 4, !tbaa !2
  %9 = load i8*, i8** %im.addr, align 4, !tbaa !2
  %10 = load i32, i32* %stride.addr, align 4, !tbaa !6
  %11 = load %struct.xy*, %struct.xy** %corners, align 4, !tbaa !2
  %12 = load i32, i32* %num_corners, align 4, !tbaa !6
  %13 = load i32, i32* %b.addr, align 4, !tbaa !6
  %call1 = call i32* @aom_fast9_score(i8* %9, i32 %10, %struct.xy* %11, i32 %12, i32 %13)
  store i32* %call1, i32** %scores, align 4, !tbaa !2
  %14 = load %struct.xy*, %struct.xy** %corners, align 4, !tbaa !2
  %15 = load i32*, i32** %scores, align 4, !tbaa !2
  %16 = load i32, i32* %num_corners, align 4, !tbaa !6
  %17 = load i32*, i32** %ret_num_corners.addr, align 4, !tbaa !2
  %call2 = call %struct.xy* @aom_nonmax_suppression(%struct.xy* %14, i32* %15, i32 %16, i32* %17)
  store %struct.xy* %call2, %struct.xy** %nonmax, align 4, !tbaa !2
  %18 = load %struct.xy*, %struct.xy** %corners, align 4, !tbaa !2
  %19 = bitcast %struct.xy* %18 to i8*
  call void @free(i8* %19)
  %20 = load i32*, i32** %scores, align 4, !tbaa !2
  %21 = bitcast i32* %20 to i8*
  call void @free(i8* %21)
  %22 = load %struct.xy*, %struct.xy** %nonmax, align 4, !tbaa !2
  %23 = bitcast %struct.xy** %nonmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  %24 = bitcast i32** %scores to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  %25 = bitcast i32* %num_corners to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  %26 = bitcast %struct.xy** %corners to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  ret %struct.xy* %22
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare %struct.xy* @aom_fast9_detect(i8*, i32, i32, i32, i32, i32*) #2

declare i32* @aom_fast9_score(i8*, i32, %struct.xy*, i32, i32) #2

declare %struct.xy* @aom_nonmax_suppression(%struct.xy*, i32*, i32, i32*) #2

declare void @free(i8*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
