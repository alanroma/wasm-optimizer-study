; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/entenc.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/entenc.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.od_ec_enc = type { i8*, i32, i16*, i32, i32, i32, i16, i16, i32 }

; Function Attrs: nounwind
define hidden void @od_ec_enc_init(%struct.od_ec_enc* nonnull %enc, i32 %size) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  %size.addr = alloca i32, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %0 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  call void @od_ec_enc_reset(%struct.od_ec_enc* %0)
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul i32 1, %1
  %call = call i8* @malloc(i32 %mul)
  %2 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %2, i32 0, i32 0
  store i8* %call, i8** %buf, align 4, !tbaa !8
  %3 = load i32, i32* %size.addr, align 4, !tbaa !6
  %4 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %storage = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %4, i32 0, i32 1
  store i32 %3, i32* %storage, align 4, !tbaa !11
  %5 = load i32, i32* %size.addr, align 4, !tbaa !6
  %cmp = icmp ugt i32 %5, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %6 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %buf1 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %6, i32 0, i32 0
  %7 = load i8*, i8** %buf1, align 4, !tbaa !8
  %cmp2 = icmp eq i8* %7, null
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %8 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %storage3 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %8, i32 0, i32 1
  store i32 0, i32* %storage3, align 4, !tbaa !11
  %9 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %9, i32 0, i32 8
  store i32 -1, i32* %error, align 4, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %10 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul4 = mul i32 2, %10
  %call5 = call i8* @malloc(i32 %mul4)
  %11 = bitcast i8* %call5 to i16*
  %12 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %12, i32 0, i32 2
  store i16* %11, i16** %precarry_buf, align 4, !tbaa !13
  %13 = load i32, i32* %size.addr, align 4, !tbaa !6
  %14 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_storage = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %14, i32 0, i32 3
  store i32 %13, i32* %precarry_storage, align 4, !tbaa !14
  %15 = load i32, i32* %size.addr, align 4, !tbaa !6
  %cmp6 = icmp ugt i32 %15, 0
  br i1 %cmp6, label %land.lhs.true7, label %if.end13

land.lhs.true7:                                   ; preds = %if.end
  %16 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf8 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %16, i32 0, i32 2
  %17 = load i16*, i16** %precarry_buf8, align 4, !tbaa !13
  %cmp9 = icmp eq i16* %17, null
  br i1 %cmp9, label %if.then10, label %if.end13

if.then10:                                        ; preds = %land.lhs.true7
  %18 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_storage11 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %18, i32 0, i32 3
  store i32 0, i32* %precarry_storage11, align 4, !tbaa !14
  %19 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %error12 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %19, i32 0, i32 8
  store i32 -1, i32* %error12, align 4, !tbaa !12
  br label %if.end13

if.end13:                                         ; preds = %if.then10, %land.lhs.true7, %if.end
  ret void
}

; Function Attrs: nounwind
define hidden void @od_ec_enc_reset(%struct.od_ec_enc* nonnull %enc) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %0 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %offs = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %0, i32 0, i32 4
  store i32 0, i32* %offs, align 4, !tbaa !15
  %1 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %low = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %1, i32 0, i32 5
  store i32 0, i32* %low, align 4, !tbaa !16
  %2 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %2, i32 0, i32 6
  store i16 -32768, i16* %rng, align 4, !tbaa !17
  %3 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %3, i32 0, i32 7
  store i16 -9, i16* %cnt, align 2, !tbaa !18
  %4 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %4, i32 0, i32 8
  store i32 0, i32* %error, align 4, !tbaa !12
  ret void
}

declare i8* @malloc(i32) #1

; Function Attrs: nounwind
define hidden void @od_ec_enc_clear(%struct.od_ec_enc* nonnull %enc) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %0 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %0, i32 0, i32 2
  %1 = load i16*, i16** %precarry_buf, align 4, !tbaa !13
  %2 = bitcast i16* %1 to i8*
  call void @free(i8* %2)
  %3 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %buf = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %3, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4, !tbaa !8
  call void @free(i8* %4)
  ret void
}

declare void @free(i8*) #1

; Function Attrs: nounwind
define hidden void @od_ec_encode_bool_q15(%struct.od_ec_enc* nonnull %enc, i32 %val, i32 %f) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  %val.addr = alloca i32, align 4
  %f.addr = alloca i32, align 4
  %l = alloca i32, align 4
  %r = alloca i32, align 4
  %v = alloca i32, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store i32 %f, i32* %f.addr, align 4, !tbaa !6
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %low = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %3, i32 0, i32 5
  %4 = load i32, i32* %low, align 4, !tbaa !16
  store i32 %4, i32* %l, align 4, !tbaa !6
  %5 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %5, i32 0, i32 6
  %6 = load i16, i16* %rng, align 4, !tbaa !17
  %conv = zext i16 %6 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !6
  %7 = load i32, i32* %r, align 4, !tbaa !6
  %shr = lshr i32 %7, 8
  %8 = load i32, i32* %f.addr, align 4, !tbaa !6
  %shr1 = lshr i32 %8, 6
  %mul = mul i32 %shr, %shr1
  %shr2 = lshr i32 %mul, 1
  store i32 %shr2, i32* %v, align 4, !tbaa !6
  %9 = load i32, i32* %v, align 4, !tbaa !6
  %add = add i32 %9, 4
  store i32 %add, i32* %v, align 4, !tbaa !6
  %10 = load i32, i32* %val.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = load i32, i32* %r, align 4, !tbaa !6
  %12 = load i32, i32* %v, align 4, !tbaa !6
  %sub = sub i32 %11, %12
  %13 = load i32, i32* %l, align 4, !tbaa !6
  %add3 = add i32 %13, %sub
  store i32 %add3, i32* %l, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load i32, i32* %val.addr, align 4, !tbaa !6
  %tobool4 = icmp ne i32 %14, 0
  br i1 %tobool4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %15 = load i32, i32* %v, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %16 = load i32, i32* %r, align 4, !tbaa !6
  %17 = load i32, i32* %v, align 4, !tbaa !6
  %sub5 = sub i32 %16, %17
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %15, %cond.true ], [ %sub5, %cond.false ]
  store i32 %cond, i32* %r, align 4, !tbaa !6
  %18 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %19 = load i32, i32* %l, align 4, !tbaa !6
  %20 = load i32, i32* %r, align 4, !tbaa !6
  call void @od_ec_enc_normalize(%struct.od_ec_enc* %18, i32 %19, i32 %20)
  %21 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #5
  %22 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  %23 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define internal void @od_ec_enc_normalize(%struct.od_ec_enc* %enc, i32 %low, i32 %rng) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  %low.addr = alloca i32, align 4
  %rng.addr = alloca i32, align 4
  %d = alloca i32, align 4
  %c = alloca i32, align 4
  %s = alloca i32, align 4
  %buf = alloca i16*, align 4
  %storage = alloca i32, align 4
  %offs = alloca i32, align 4
  %m = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  store i32 %low, i32* %low.addr, align 4, !tbaa !6
  store i32 %rng, i32* %rng.addr, align 4, !tbaa !6
  %0 = bitcast i32* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %3, i32 0, i32 7
  %4 = load i16, i16* %cnt, align 2, !tbaa !18
  %conv = sext i16 %4 to i32
  store i32 %conv, i32* %c, align 4, !tbaa !6
  %5 = load i32, i32* %rng.addr, align 4, !tbaa !6
  %call = call i32 @get_msb(i32 %5)
  %add = add nsw i32 1, %call
  %sub = sub nsw i32 16, %add
  store i32 %sub, i32* %d, align 4, !tbaa !6
  %6 = load i32, i32* %c, align 4, !tbaa !6
  %7 = load i32, i32* %d, align 4, !tbaa !6
  %add1 = add nsw i32 %6, %7
  store i32 %add1, i32* %s, align 4, !tbaa !6
  %8 = load i32, i32* %s, align 4, !tbaa !6
  %cmp = icmp sge i32 %8, 0
  br i1 %cmp, label %if.then, label %if.end38

if.then:                                          ; preds = %entry
  %9 = bitcast i16** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #5
  %10 = bitcast i32* %storage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #5
  %11 = bitcast i32* %offs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #5
  %12 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #5
  %13 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %13, i32 0, i32 2
  %14 = load i16*, i16** %precarry_buf, align 4, !tbaa !13
  store i16* %14, i16** %buf, align 4, !tbaa !2
  %15 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_storage = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %15, i32 0, i32 3
  %16 = load i32, i32* %precarry_storage, align 4, !tbaa !14
  store i32 %16, i32* %storage, align 4, !tbaa !6
  %17 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %offs3 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %17, i32 0, i32 4
  %18 = load i32, i32* %offs3, align 4, !tbaa !15
  store i32 %18, i32* %offs, align 4, !tbaa !6
  %19 = load i32, i32* %offs, align 4, !tbaa !6
  %add4 = add i32 %19, 2
  %20 = load i32, i32* %storage, align 4, !tbaa !6
  %cmp5 = icmp ugt i32 %add4, %20
  br i1 %cmp5, label %if.then7, label %if.end17

if.then7:                                         ; preds = %if.then
  %21 = load i32, i32* %storage, align 4, !tbaa !6
  %mul = mul i32 2, %21
  %add8 = add i32 %mul, 2
  store i32 %add8, i32* %storage, align 4, !tbaa !6
  %22 = load i16*, i16** %buf, align 4, !tbaa !2
  %23 = bitcast i16* %22 to i8*
  %24 = load i32, i32* %storage, align 4, !tbaa !6
  %mul9 = mul i32 2, %24
  %call10 = call i8* @realloc(i8* %23, i32 %mul9)
  %25 = bitcast i8* %call10 to i16*
  store i16* %25, i16** %buf, align 4, !tbaa !2
  %26 = load i16*, i16** %buf, align 4, !tbaa !2
  %cmp11 = icmp eq i16* %26, null
  br i1 %cmp11, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.then7
  %27 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %27, i32 0, i32 8
  store i32 -1, i32* %error, align 4, !tbaa !12
  %28 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %offs14 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %28, i32 0, i32 4
  store i32 0, i32* %offs14, align 4, !tbaa !15
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then7
  %29 = load i16*, i16** %buf, align 4, !tbaa !2
  %30 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf15 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %30, i32 0, i32 2
  store i16* %29, i16** %precarry_buf15, align 4, !tbaa !13
  %31 = load i32, i32* %storage, align 4, !tbaa !6
  %32 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_storage16 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %32, i32 0, i32 3
  store i32 %31, i32* %precarry_storage16, align 4, !tbaa !14
  br label %if.end17

if.end17:                                         ; preds = %if.end, %if.then
  %33 = load i32, i32* %c, align 4, !tbaa !6
  %add18 = add nsw i32 %33, 16
  store i32 %add18, i32* %c, align 4, !tbaa !6
  %34 = load i32, i32* %c, align 4, !tbaa !6
  %shl = shl i32 1, %34
  %sub19 = sub nsw i32 %shl, 1
  store i32 %sub19, i32* %m, align 4, !tbaa !6
  %35 = load i32, i32* %s, align 4, !tbaa !6
  %cmp20 = icmp sge i32 %35, 8
  br i1 %cmp20, label %if.then22, label %if.end26

if.then22:                                        ; preds = %if.end17
  %36 = load i32, i32* %low.addr, align 4, !tbaa !6
  %37 = load i32, i32* %c, align 4, !tbaa !6
  %shr = lshr i32 %36, %37
  %conv23 = trunc i32 %shr to i16
  %38 = load i16*, i16** %buf, align 4, !tbaa !2
  %39 = load i32, i32* %offs, align 4, !tbaa !6
  %inc = add i32 %39, 1
  store i32 %inc, i32* %offs, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %38, i32 %39
  store i16 %conv23, i16* %arrayidx, align 2, !tbaa !19
  %40 = load i32, i32* %m, align 4, !tbaa !6
  %41 = load i32, i32* %low.addr, align 4, !tbaa !6
  %and = and i32 %41, %40
  store i32 %and, i32* %low.addr, align 4, !tbaa !6
  %42 = load i32, i32* %c, align 4, !tbaa !6
  %sub24 = sub nsw i32 %42, 8
  store i32 %sub24, i32* %c, align 4, !tbaa !6
  %43 = load i32, i32* %m, align 4, !tbaa !6
  %shr25 = lshr i32 %43, 8
  store i32 %shr25, i32* %m, align 4, !tbaa !6
  br label %if.end26

if.end26:                                         ; preds = %if.then22, %if.end17
  %44 = load i32, i32* %low.addr, align 4, !tbaa !6
  %45 = load i32, i32* %c, align 4, !tbaa !6
  %shr27 = lshr i32 %44, %45
  %conv28 = trunc i32 %shr27 to i16
  %46 = load i16*, i16** %buf, align 4, !tbaa !2
  %47 = load i32, i32* %offs, align 4, !tbaa !6
  %inc29 = add i32 %47, 1
  store i32 %inc29, i32* %offs, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds i16, i16* %46, i32 %47
  store i16 %conv28, i16* %arrayidx30, align 2, !tbaa !19
  %48 = load i32, i32* %c, align 4, !tbaa !6
  %49 = load i32, i32* %d, align 4, !tbaa !6
  %add31 = add nsw i32 %48, %49
  %sub32 = sub nsw i32 %add31, 24
  store i32 %sub32, i32* %s, align 4, !tbaa !6
  %50 = load i32, i32* %m, align 4, !tbaa !6
  %51 = load i32, i32* %low.addr, align 4, !tbaa !6
  %and33 = and i32 %51, %50
  store i32 %and33, i32* %low.addr, align 4, !tbaa !6
  %52 = load i32, i32* %offs, align 4, !tbaa !6
  %53 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %offs34 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %53, i32 0, i32 4
  store i32 %52, i32* %offs34, align 4, !tbaa !15
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end26, %if.then13
  %54 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #5
  %55 = bitcast i32* %offs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  %56 = bitcast i32* %storage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #5
  %57 = bitcast i16** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup46 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end38

if.end38:                                         ; preds = %cleanup.cont, %entry
  %58 = load i32, i32* %low.addr, align 4, !tbaa !6
  %59 = load i32, i32* %d, align 4, !tbaa !6
  %shl39 = shl i32 %58, %59
  %60 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %low40 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %60, i32 0, i32 5
  store i32 %shl39, i32* %low40, align 4, !tbaa !16
  %61 = load i32, i32* %rng.addr, align 4, !tbaa !6
  %62 = load i32, i32* %d, align 4, !tbaa !6
  %shl41 = shl i32 %61, %62
  %conv42 = trunc i32 %shl41 to i16
  %63 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %rng43 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %63, i32 0, i32 6
  store i16 %conv42, i16* %rng43, align 4, !tbaa !17
  %64 = load i32, i32* %s, align 4, !tbaa !6
  %conv44 = trunc i32 %64 to i16
  %65 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %cnt45 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %65, i32 0, i32 7
  store i16 %conv44, i16* %cnt45, align 2, !tbaa !18
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup46

cleanup46:                                        ; preds = %if.end38, %cleanup
  %66 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #5
  %67 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #5
  %68 = bitcast i32* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #5
  %cleanup.dest49 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest49, label %unreachable [
    i32 0, label %cleanup.cont50
    i32 1, label %cleanup.cont50
  ]

cleanup.cont50:                                   ; preds = %cleanup46, %cleanup46
  ret void

unreachable:                                      ; preds = %cleanup46
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden void @od_ec_encode_cdf_q15(%struct.od_ec_enc* nonnull %enc, i32 %s, i16* nonnull %icdf, i32 %nsyms) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  %s.addr = alloca i32, align 4
  %icdf.addr = alloca i16*, align 4
  %nsyms.addr = alloca i32, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  store i32 %s, i32* %s.addr, align 4, !tbaa !6
  store i16* %icdf, i16** %icdf.addr, align 4, !tbaa !2
  store i32 %nsyms, i32* %nsyms.addr, align 4, !tbaa !6
  %0 = load i32, i32* %nsyms.addr, align 4, !tbaa !6
  %1 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %2 = load i32, i32* %s.addr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load i16*, i16** %icdf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %s.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %4, 1
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 %sub
  %5 = load i16, i16* %arrayidx, align 2, !tbaa !19
  %conv = zext i16 %5 to i32
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ 32768, %cond.false ]
  %6 = load i16*, i16** %icdf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %s.addr, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx1, align 2, !tbaa !19
  %conv2 = zext i16 %8 to i32
  %9 = load i32, i32* %s.addr, align 4, !tbaa !6
  %10 = load i32, i32* %nsyms.addr, align 4, !tbaa !6
  call void @od_ec_encode_q15(%struct.od_ec_enc* %1, i32 %cond, i32 %conv2, i32 %9, i32 %10)
  ret void
}

; Function Attrs: nounwind
define internal void @od_ec_encode_q15(%struct.od_ec_enc* %enc, i32 %fl, i32 %fh, i32 %s, i32 %nsyms) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  %fl.addr = alloca i32, align 4
  %fh.addr = alloca i32, align 4
  %s.addr = alloca i32, align 4
  %nsyms.addr = alloca i32, align 4
  %l = alloca i32, align 4
  %r = alloca i32, align 4
  %u = alloca i32, align 4
  %v = alloca i32, align 4
  %N = alloca i32, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  store i32 %fl, i32* %fl.addr, align 4, !tbaa !6
  store i32 %fh, i32* %fh.addr, align 4, !tbaa !6
  store i32 %s, i32* %s.addr, align 4, !tbaa !6
  store i32 %nsyms, i32* %nsyms.addr, align 4, !tbaa !6
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %u to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %low = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %4, i32 0, i32 5
  %5 = load i32, i32* %low, align 4, !tbaa !16
  store i32 %5, i32* %l, align 4, !tbaa !6
  %6 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %6, i32 0, i32 6
  %7 = load i16, i16* %rng, align 4, !tbaa !17
  %conv = zext i16 %7 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !6
  %8 = bitcast i32* %N to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load i32, i32* %nsyms.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %9, 1
  store i32 %sub, i32* %N, align 4, !tbaa !6
  %10 = load i32, i32* %fl.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %10, 32768
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %11 = load i32, i32* %r, align 4, !tbaa !6
  %shr = lshr i32 %11, 8
  %12 = load i32, i32* %fl.addr, align 4, !tbaa !6
  %shr2 = lshr i32 %12, 6
  %mul = mul i32 %shr, %shr2
  %shr3 = lshr i32 %mul, 1
  %13 = load i32, i32* %N, align 4, !tbaa !6
  %14 = load i32, i32* %s.addr, align 4, !tbaa !6
  %sub4 = sub nsw i32 %14, 1
  %sub5 = sub nsw i32 %13, %sub4
  %mul6 = mul nsw i32 4, %sub5
  %add = add i32 %shr3, %mul6
  store i32 %add, i32* %u, align 4, !tbaa !6
  %15 = load i32, i32* %r, align 4, !tbaa !6
  %shr7 = lshr i32 %15, 8
  %16 = load i32, i32* %fh.addr, align 4, !tbaa !6
  %shr8 = lshr i32 %16, 6
  %mul9 = mul i32 %shr7, %shr8
  %shr10 = lshr i32 %mul9, 1
  %17 = load i32, i32* %N, align 4, !tbaa !6
  %18 = load i32, i32* %s.addr, align 4, !tbaa !6
  %add11 = add nsw i32 %18, 0
  %sub12 = sub nsw i32 %17, %add11
  %mul13 = mul nsw i32 4, %sub12
  %add14 = add i32 %shr10, %mul13
  store i32 %add14, i32* %v, align 4, !tbaa !6
  %19 = load i32, i32* %r, align 4, !tbaa !6
  %20 = load i32, i32* %u, align 4, !tbaa !6
  %sub15 = sub i32 %19, %20
  %21 = load i32, i32* %l, align 4, !tbaa !6
  %add16 = add i32 %21, %sub15
  store i32 %add16, i32* %l, align 4, !tbaa !6
  %22 = load i32, i32* %u, align 4, !tbaa !6
  %23 = load i32, i32* %v, align 4, !tbaa !6
  %sub17 = sub i32 %22, %23
  store i32 %sub17, i32* %r, align 4, !tbaa !6
  br label %if.end

if.else:                                          ; preds = %entry
  %24 = load i32, i32* %r, align 4, !tbaa !6
  %shr18 = lshr i32 %24, 8
  %25 = load i32, i32* %fh.addr, align 4, !tbaa !6
  %shr19 = lshr i32 %25, 6
  %mul20 = mul i32 %shr18, %shr19
  %shr21 = lshr i32 %mul20, 1
  %26 = load i32, i32* %N, align 4, !tbaa !6
  %27 = load i32, i32* %s.addr, align 4, !tbaa !6
  %add22 = add nsw i32 %27, 0
  %sub23 = sub nsw i32 %26, %add22
  %mul24 = mul nsw i32 4, %sub23
  %add25 = add i32 %shr21, %mul24
  %28 = load i32, i32* %r, align 4, !tbaa !6
  %sub26 = sub i32 %28, %add25
  store i32 %sub26, i32* %r, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %29 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %30 = load i32, i32* %l, align 4, !tbaa !6
  %31 = load i32, i32* %r, align 4, !tbaa !6
  call void @od_ec_enc_normalize(%struct.od_ec_enc* %29, i32 %30, i32 %31)
  %32 = bitcast i32* %N to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #5
  %33 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #5
  %34 = bitcast i32* %u to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #5
  %35 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #5
  %36 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #5
  ret void
}

; Function Attrs: nounwind
define hidden void @od_ec_enc_patch_initial_bits(%struct.od_ec_enc* nonnull %enc, i32 %val, i32 %nbits) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  %val.addr = alloca i32, align 4
  %nbits.addr = alloca i32, align 4
  %shift = alloca i32, align 4
  %mask = alloca i32, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  store i32 %val, i32* %val.addr, align 4, !tbaa !6
  store i32 %nbits, i32* %nbits.addr, align 4, !tbaa !6
  %0 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load i32, i32* %nbits.addr, align 4, !tbaa !6
  %sub = sub nsw i32 8, %2
  store i32 %sub, i32* %shift, align 4, !tbaa !6
  %3 = load i32, i32* %nbits.addr, align 4, !tbaa !6
  %shl = shl i32 1, %3
  %sub1 = sub i32 %shl, 1
  %4 = load i32, i32* %shift, align 4, !tbaa !6
  %shl2 = shl i32 %sub1, %4
  store i32 %shl2, i32* %mask, align 4, !tbaa !6
  %5 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %offs = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %5, i32 0, i32 4
  %6 = load i32, i32* %offs, align 4, !tbaa !15
  %cmp = icmp ugt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %7, i32 0, i32 2
  %8 = load i16*, i16** %precarry_buf, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds i16, i16* %8, i32 0
  %9 = load i16, i16* %arrayidx, align 2, !tbaa !19
  %conv = zext i16 %9 to i32
  %10 = load i32, i32* %mask, align 4, !tbaa !6
  %neg = xor i32 %10, -1
  %and = and i32 %conv, %neg
  %11 = load i32, i32* %val.addr, align 4, !tbaa !6
  %12 = load i32, i32* %shift, align 4, !tbaa !6
  %shl3 = shl i32 %11, %12
  %or = or i32 %and, %shl3
  %conv4 = trunc i32 %or to i16
  %13 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf5 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %13, i32 0, i32 2
  %14 = load i16*, i16** %precarry_buf5, align 4, !tbaa !13
  %arrayidx6 = getelementptr inbounds i16, i16* %14, i32 0
  store i16 %conv4, i16* %arrayidx6, align 2, !tbaa !19
  br label %if.end29

if.else:                                          ; preds = %entry
  %15 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %15, i32 0, i32 7
  %16 = load i16, i16* %cnt, align 2, !tbaa !18
  %conv7 = sext i16 %16 to i32
  %add = add nsw i32 9, %conv7
  %17 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %17, i32 0, i32 6
  %18 = load i16, i16* %rng, align 4, !tbaa !17
  %conv8 = zext i16 %18 to i32
  %cmp9 = icmp eq i32 %conv8, 32768
  %conv10 = zext i1 %cmp9 to i32
  %add11 = add nsw i32 %add, %conv10
  %19 = load i32, i32* %nbits.addr, align 4, !tbaa !6
  %cmp12 = icmp sgt i32 %add11, %19
  br i1 %cmp12, label %if.then14, label %if.else28

if.then14:                                        ; preds = %if.else
  %20 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %low = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %20, i32 0, i32 5
  %21 = load i32, i32* %low, align 4, !tbaa !16
  %22 = load i32, i32* %mask, align 4, !tbaa !6
  %23 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %cnt15 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %23, i32 0, i32 7
  %24 = load i16, i16* %cnt15, align 2, !tbaa !18
  %conv16 = sext i16 %24 to i32
  %add17 = add nsw i32 16, %conv16
  %shl18 = shl i32 %22, %add17
  %neg19 = xor i32 %shl18, -1
  %and20 = and i32 %21, %neg19
  %25 = load i32, i32* %val.addr, align 4, !tbaa !6
  %26 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %cnt21 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %26, i32 0, i32 7
  %27 = load i16, i16* %cnt21, align 2, !tbaa !18
  %conv22 = sext i16 %27 to i32
  %add23 = add nsw i32 16, %conv22
  %28 = load i32, i32* %shift, align 4, !tbaa !6
  %add24 = add nsw i32 %add23, %28
  %shl25 = shl i32 %25, %add24
  %or26 = or i32 %and20, %shl25
  %29 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %low27 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %29, i32 0, i32 5
  store i32 %or26, i32* %low27, align 4, !tbaa !16
  br label %if.end

if.else28:                                        ; preds = %if.else
  %30 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %30, i32 0, i32 8
  store i32 -1, i32* %error, align 4, !tbaa !12
  br label %if.end

if.end:                                           ; preds = %if.else28, %if.then14
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then
  %31 = bitcast i32* %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #5
  %32 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #5
  ret void
}

; Function Attrs: nounwind
define hidden i8* @od_ec_enc_done(%struct.od_ec_enc* nonnull %enc, i32* nonnull %nbytes) #0 {
entry:
  %retval = alloca i8*, align 4
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  %nbytes.addr = alloca i32*, align 4
  %out = alloca i8*, align 4
  %storage = alloca i32, align 4
  %buf = alloca i16*, align 4
  %offs = alloca i32, align 4
  %m = alloca i32, align 4
  %e = alloca i32, align 4
  %l = alloca i32, align 4
  %c = alloca i32, align 4
  %s = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %n = alloca i32, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  store i32* %nbytes, i32** %nbytes.addr, align 4, !tbaa !2
  %0 = bitcast i8** %out to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %storage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i16** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %offs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #5
  %9 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %9, i32 0, i32 8
  %10 = load i32, i32* %error, align 4, !tbaa !12
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup66

if.end:                                           ; preds = %entry
  %11 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %low = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %11, i32 0, i32 5
  %12 = load i32, i32* %low, align 4, !tbaa !16
  store i32 %12, i32* %l, align 4, !tbaa !6
  %13 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %13, i32 0, i32 7
  %14 = load i16, i16* %cnt, align 2, !tbaa !18
  %conv = sext i16 %14 to i32
  store i32 %conv, i32* %c, align 4, !tbaa !6
  store i32 10, i32* %s, align 4, !tbaa !6
  store i32 16383, i32* %m, align 4, !tbaa !6
  %15 = load i32, i32* %l, align 4, !tbaa !6
  %16 = load i32, i32* %m, align 4, !tbaa !6
  %add = add i32 %15, %16
  %17 = load i32, i32* %m, align 4, !tbaa !6
  %neg = xor i32 %17, -1
  %and = and i32 %add, %neg
  %18 = load i32, i32* %m, align 4, !tbaa !6
  %add1 = add i32 %18, 1
  %or = or i32 %and, %add1
  store i32 %or, i32* %e, align 4, !tbaa !6
  %19 = load i32, i32* %c, align 4, !tbaa !6
  %20 = load i32, i32* %s, align 4, !tbaa !6
  %add2 = add nsw i32 %20, %19
  store i32 %add2, i32* %s, align 4, !tbaa !6
  %21 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %offs3 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %21, i32 0, i32 4
  %22 = load i32, i32* %offs3, align 4, !tbaa !15
  store i32 %22, i32* %offs, align 4, !tbaa !6
  %23 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %23, i32 0, i32 2
  %24 = load i16*, i16** %precarry_buf, align 4, !tbaa !13
  store i16* %24, i16** %buf, align 4, !tbaa !2
  %25 = load i32, i32* %s, align 4, !tbaa !6
  %cmp = icmp sgt i32 %25, 0
  br i1 %cmp, label %if.then5, label %if.end33

if.then5:                                         ; preds = %if.end
  %26 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #5
  %27 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_storage = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %27, i32 0, i32 3
  %28 = load i32, i32* %precarry_storage, align 4, !tbaa !14
  store i32 %28, i32* %storage, align 4, !tbaa !6
  %29 = load i32, i32* %offs, align 4, !tbaa !6
  %30 = load i32, i32* %s, align 4, !tbaa !6
  %add6 = add nsw i32 %30, 7
  %shr = ashr i32 %add6, 3
  %add7 = add i32 %29, %shr
  %31 = load i32, i32* %storage, align 4, !tbaa !6
  %cmp8 = icmp ugt i32 %add7, %31
  br i1 %cmp8, label %if.then10, label %if.end22

if.then10:                                        ; preds = %if.then5
  %32 = load i32, i32* %storage, align 4, !tbaa !6
  %mul = mul i32 %32, 2
  %33 = load i32, i32* %s, align 4, !tbaa !6
  %add11 = add nsw i32 %33, 7
  %shr12 = ashr i32 %add11, 3
  %add13 = add i32 %mul, %shr12
  store i32 %add13, i32* %storage, align 4, !tbaa !6
  %34 = load i16*, i16** %buf, align 4, !tbaa !2
  %35 = bitcast i16* %34 to i8*
  %36 = load i32, i32* %storage, align 4, !tbaa !6
  %mul14 = mul i32 2, %36
  %call = call i8* @realloc(i8* %35, i32 %mul14)
  %37 = bitcast i8* %call to i16*
  store i16* %37, i16** %buf, align 4, !tbaa !2
  %38 = load i16*, i16** %buf, align 4, !tbaa !2
  %cmp15 = icmp eq i16* %38, null
  br i1 %cmp15, label %if.then17, label %if.end19

if.then17:                                        ; preds = %if.then10
  %39 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %error18 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %39, i32 0, i32 8
  store i32 -1, i32* %error18, align 4, !tbaa !12
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.then10
  %40 = load i16*, i16** %buf, align 4, !tbaa !2
  %41 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_buf20 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %41, i32 0, i32 2
  store i16* %40, i16** %precarry_buf20, align 4, !tbaa !13
  %42 = load i32, i32* %storage, align 4, !tbaa !6
  %43 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %precarry_storage21 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %43, i32 0, i32 3
  store i32 %42, i32* %precarry_storage21, align 4, !tbaa !14
  br label %if.end22

if.end22:                                         ; preds = %if.end19, %if.then5
  %44 = load i32, i32* %c, align 4, !tbaa !6
  %add23 = add nsw i32 %44, 16
  %shl = shl i32 1, %add23
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %n, align 4, !tbaa !6
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.end22
  %45 = load i32, i32* %e, align 4, !tbaa !6
  %46 = load i32, i32* %c, align 4, !tbaa !6
  %add24 = add nsw i32 %46, 16
  %shr25 = lshr i32 %45, %add24
  %conv26 = trunc i32 %shr25 to i16
  %47 = load i16*, i16** %buf, align 4, !tbaa !2
  %48 = load i32, i32* %offs, align 4, !tbaa !6
  %inc = add i32 %48, 1
  store i32 %inc, i32* %offs, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %47, i32 %48
  store i16 %conv26, i16* %arrayidx, align 2, !tbaa !19
  %49 = load i32, i32* %n, align 4, !tbaa !6
  %50 = load i32, i32* %e, align 4, !tbaa !6
  %and27 = and i32 %50, %49
  store i32 %and27, i32* %e, align 4, !tbaa !6
  %51 = load i32, i32* %s, align 4, !tbaa !6
  %sub28 = sub nsw i32 %51, 8
  store i32 %sub28, i32* %s, align 4, !tbaa !6
  %52 = load i32, i32* %c, align 4, !tbaa !6
  %sub29 = sub nsw i32 %52, 8
  store i32 %sub29, i32* %c, align 4, !tbaa !6
  %53 = load i32, i32* %n, align 4, !tbaa !6
  %shr30 = lshr i32 %53, 8
  store i32 %shr30, i32* %n, align 4, !tbaa !6
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %54 = load i32, i32* %s, align 4, !tbaa !6
  %cmp31 = icmp sgt i32 %54, 0
  br i1 %cmp31, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %do.end, %if.then17
  %55 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup66 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end33

if.end33:                                         ; preds = %cleanup.cont, %if.end
  %56 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %buf34 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %56, i32 0, i32 0
  %57 = load i8*, i8** %buf34, align 4, !tbaa !8
  store i8* %57, i8** %out, align 4, !tbaa !2
  %58 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %storage35 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %58, i32 0, i32 1
  %59 = load i32, i32* %storage35, align 4, !tbaa !11
  store i32 %59, i32* %storage, align 4, !tbaa !6
  %60 = load i32, i32* %s, align 4, !tbaa !6
  %add36 = add nsw i32 %60, 7
  %shr37 = ashr i32 %add36, 3
  %cmp38 = icmp sgt i32 %shr37, 0
  br i1 %cmp38, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end33
  %61 = load i32, i32* %s, align 4, !tbaa !6
  %add40 = add nsw i32 %61, 7
  %shr41 = ashr i32 %add40, 3
  br label %cond.end

cond.false:                                       ; preds = %if.end33
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shr41, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %c, align 4, !tbaa !6
  %62 = load i32, i32* %offs, align 4, !tbaa !6
  %63 = load i32, i32* %c, align 4, !tbaa !6
  %add42 = add i32 %62, %63
  %64 = load i32, i32* %storage, align 4, !tbaa !6
  %cmp43 = icmp ugt i32 %add42, %64
  br i1 %cmp43, label %if.then45, label %if.end56

if.then45:                                        ; preds = %cond.end
  %65 = load i32, i32* %offs, align 4, !tbaa !6
  %66 = load i32, i32* %c, align 4, !tbaa !6
  %add46 = add i32 %65, %66
  store i32 %add46, i32* %storage, align 4, !tbaa !6
  %67 = load i8*, i8** %out, align 4, !tbaa !2
  %68 = load i32, i32* %storage, align 4, !tbaa !6
  %mul47 = mul i32 1, %68
  %call48 = call i8* @realloc(i8* %67, i32 %mul47)
  store i8* %call48, i8** %out, align 4, !tbaa !2
  %69 = load i8*, i8** %out, align 4, !tbaa !2
  %cmp49 = icmp eq i8* %69, null
  br i1 %cmp49, label %if.then51, label %if.end53

if.then51:                                        ; preds = %if.then45
  %70 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %error52 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %70, i32 0, i32 8
  store i32 -1, i32* %error52, align 4, !tbaa !12
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup66

if.end53:                                         ; preds = %if.then45
  %71 = load i8*, i8** %out, align 4, !tbaa !2
  %72 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %buf54 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %72, i32 0, i32 0
  store i8* %71, i8** %buf54, align 4, !tbaa !8
  %73 = load i32, i32* %storage, align 4, !tbaa !6
  %74 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %storage55 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %74, i32 0, i32 1
  store i32 %73, i32* %storage55, align 4, !tbaa !11
  br label %if.end56

if.end56:                                         ; preds = %if.end53, %cond.end
  %75 = load i32, i32* %offs, align 4, !tbaa !6
  %76 = load i32*, i32** %nbytes.addr, align 4, !tbaa !2
  store i32 %75, i32* %76, align 4, !tbaa !6
  %77 = load i8*, i8** %out, align 4, !tbaa !2
  %78 = load i32, i32* %storage, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %77, i32 %78
  %79 = load i32, i32* %offs, align 4, !tbaa !6
  %idx.neg = sub i32 0, %79
  %add.ptr57 = getelementptr inbounds i8, i8* %add.ptr, i32 %idx.neg
  store i8* %add.ptr57, i8** %out, align 4, !tbaa !2
  store i32 0, i32* %c, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end56
  %80 = load i32, i32* %offs, align 4, !tbaa !6
  %cmp58 = icmp ugt i32 %80, 0
  br i1 %cmp58, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %81 = load i32, i32* %offs, align 4, !tbaa !6
  %dec = add i32 %81, -1
  store i32 %dec, i32* %offs, align 4, !tbaa !6
  %82 = load i16*, i16** %buf, align 4, !tbaa !2
  %83 = load i32, i32* %offs, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds i16, i16* %82, i32 %83
  %84 = load i16, i16* %arrayidx60, align 2, !tbaa !19
  %conv61 = zext i16 %84 to i32
  %85 = load i32, i32* %c, align 4, !tbaa !6
  %add62 = add nsw i32 %conv61, %85
  store i32 %add62, i32* %c, align 4, !tbaa !6
  %86 = load i32, i32* %c, align 4, !tbaa !6
  %conv63 = trunc i32 %86 to i8
  %87 = load i8*, i8** %out, align 4, !tbaa !2
  %88 = load i32, i32* %offs, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds i8, i8* %87, i32 %88
  store i8 %conv63, i8* %arrayidx64, align 1, !tbaa !20
  %89 = load i32, i32* %c, align 4, !tbaa !6
  %shr65 = ashr i32 %89, 8
  store i32 %shr65, i32* %c, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %90 = load i8*, i8** %out, align 4, !tbaa !2
  store i8* %90, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup66

cleanup66:                                        ; preds = %while.end, %if.then51, %cleanup, %if.then
  %91 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #5
  %92 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #5
  %93 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #5
  %94 = bitcast i32* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #5
  %95 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #5
  %96 = bitcast i32* %offs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #5
  %97 = bitcast i16** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #5
  %98 = bitcast i32* %storage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #5
  %99 = bitcast i8** %out to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #5
  %100 = load i8*, i8** %retval, align 4
  ret i8* %100
}

declare i8* @realloc(i8*, i32) #1

; Function Attrs: nounwind
define hidden i32 @od_ec_enc_tell(%struct.od_ec_enc* nonnull %enc) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %0 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %cnt = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %0, i32 0, i32 7
  %1 = load i16, i16* %cnt, align 2, !tbaa !18
  %conv = sext i16 %1 to i32
  %add = add nsw i32 %conv, 10
  %2 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %offs = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %2, i32 0, i32 4
  %3 = load i32, i32* %offs, align 4, !tbaa !15
  %mul = mul i32 %3, 8
  %add1 = add i32 %add, %mul
  ret i32 %add1
}

; Function Attrs: nounwind
define hidden i32 @od_ec_enc_tell_frac(%struct.od_ec_enc* nonnull %enc) #0 {
entry:
  %enc.addr = alloca %struct.od_ec_enc*, align 4
  store %struct.od_ec_enc* %enc, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %0 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %call = call i32 @od_ec_enc_tell(%struct.od_ec_enc* %0)
  %1 = load %struct.od_ec_enc*, %struct.od_ec_enc** %enc.addr, align 4, !tbaa !2
  %rng = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %1, i32 0, i32 6
  %2 = load i16, i16* %rng, align 4, !tbaa !17
  %conv = zext i16 %2 to i32
  %call1 = call i32 @od_ec_tell_frac(i32 %call, i32 %conv)
  ret i32 %call1
}

declare i32 @od_ec_tell_frac(i32, i32) #1

; Function Attrs: nounwind
define hidden void @od_ec_enc_checkpoint(%struct.od_ec_enc* %dst, %struct.od_ec_enc* %src) #0 {
entry:
  %dst.addr = alloca %struct.od_ec_enc*, align 4
  %src.addr = alloca %struct.od_ec_enc*, align 4
  store %struct.od_ec_enc* %dst, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  store %struct.od_ec_enc* %src, %struct.od_ec_enc** %src.addr, align 4, !tbaa !2
  %0 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %1 = bitcast %struct.od_ec_enc* %0 to i8*
  %2 = load %struct.od_ec_enc*, %struct.od_ec_enc** %src.addr, align 4, !tbaa !2
  %3 = bitcast %struct.od_ec_enc* %2 to i8*
  %4 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %5 = load %struct.od_ec_enc*, %struct.od_ec_enc** %src.addr, align 4, !tbaa !2
  %sub.ptr.lhs.cast = ptrtoint %struct.od_ec_enc* %4 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.od_ec_enc* %5 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 32
  %mul = mul nsw i32 0, %sub.ptr.div
  %add = add i32 32, %mul
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %3, i32 %add, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define hidden void @od_ec_enc_rollback(%struct.od_ec_enc* %dst, %struct.od_ec_enc* %src) #0 {
entry:
  %dst.addr = alloca %struct.od_ec_enc*, align 4
  %src.addr = alloca %struct.od_ec_enc*, align 4
  %buf = alloca i8*, align 4
  %storage = alloca i32, align 4
  %precarry_buf = alloca i16*, align 4
  %precarry_storage = alloca i32, align 4
  store %struct.od_ec_enc* %dst, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  store %struct.od_ec_enc* %src, %struct.od_ec_enc** %src.addr, align 4, !tbaa !2
  %0 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %storage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i16** %precarry_buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %precarry_storage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %buf1 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %4, i32 0, i32 0
  %5 = load i8*, i8** %buf1, align 4, !tbaa !8
  store i8* %5, i8** %buf, align 4, !tbaa !2
  %6 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %storage2 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %6, i32 0, i32 1
  %7 = load i32, i32* %storage2, align 4, !tbaa !11
  store i32 %7, i32* %storage, align 4, !tbaa !6
  %8 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %precarry_buf3 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %8, i32 0, i32 2
  %9 = load i16*, i16** %precarry_buf3, align 4, !tbaa !13
  store i16* %9, i16** %precarry_buf, align 4, !tbaa !2
  %10 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %precarry_storage4 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %10, i32 0, i32 3
  %11 = load i32, i32* %precarry_storage4, align 4, !tbaa !14
  store i32 %11, i32* %precarry_storage, align 4, !tbaa !6
  %12 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %13 = bitcast %struct.od_ec_enc* %12 to i8*
  %14 = load %struct.od_ec_enc*, %struct.od_ec_enc** %src.addr, align 4, !tbaa !2
  %15 = bitcast %struct.od_ec_enc* %14 to i8*
  %16 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %17 = load %struct.od_ec_enc*, %struct.od_ec_enc** %src.addr, align 4, !tbaa !2
  %sub.ptr.lhs.cast = ptrtoint %struct.od_ec_enc* %16 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.od_ec_enc* %17 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 32
  %mul = mul nsw i32 0, %sub.ptr.div
  %add = add i32 32, %mul
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %15, i32 %add, i1 false)
  %18 = load i8*, i8** %buf, align 4, !tbaa !2
  %19 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %buf5 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %19, i32 0, i32 0
  store i8* %18, i8** %buf5, align 4, !tbaa !8
  %20 = load i32, i32* %storage, align 4, !tbaa !6
  %21 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %storage6 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %21, i32 0, i32 1
  store i32 %20, i32* %storage6, align 4, !tbaa !11
  %22 = load i16*, i16** %precarry_buf, align 4, !tbaa !2
  %23 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %precarry_buf7 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %23, i32 0, i32 2
  store i16* %22, i16** %precarry_buf7, align 4, !tbaa !13
  %24 = load i32, i32* %precarry_storage, align 4, !tbaa !6
  %25 = load %struct.od_ec_enc*, %struct.od_ec_enc** %dst.addr, align 4, !tbaa !2
  %precarry_storage8 = getelementptr inbounds %struct.od_ec_enc, %struct.od_ec_enc* %25, i32 0, i32 3
  store i32 %24, i32* %precarry_storage8, align 4, !tbaa !14
  %26 = bitcast i32* %precarry_storage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #5
  %27 = bitcast i16** %precarry_buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #5
  %28 = bitcast i32* %storage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #5
  %29 = bitcast i8** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #5
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #3 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #4

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 0}
!9 = !{!"od_ec_enc", !3, i64 0, !7, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !10, i64 24, !10, i64 26, !7, i64 28}
!10 = !{!"short", !4, i64 0}
!11 = !{!9, !7, i64 4}
!12 = !{!9, !7, i64 28}
!13 = !{!9, !3, i64 8}
!14 = !{!9, !7, i64 12}
!15 = !{!9, !7, i64 16}
!16 = !{!9, !7, i64 20}
!17 = !{!9, !10, i64 24}
!18 = !{!9, !10, i64 26}
!19 = !{!10, !10, i64 0}
!20 = !{!4, !4, i64 0}
