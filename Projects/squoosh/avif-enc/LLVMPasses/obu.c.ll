; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/obu.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/obu.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifSequenceHeader = type { i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifROData = type { i8*, i32 }
%struct.avifBits = type { i32, i32, i64, i32, i8*, i8*, i8* }

; Function Attrs: nounwind
define hidden i32 @avifSequenceHeaderParse(%struct.avifSequenceHeader* %header, %struct.avifROData* %sample) #0 {
entry:
  %retval = alloca i32, align 4
  %header.addr = alloca %struct.avifSequenceHeader*, align 4
  %sample.addr = alloca %struct.avifROData*, align 4
  %obus = alloca %struct.avifROData, align 4
  %bits = alloca %struct.avifBits, align 8
  %obu_type = alloca i32, align 4
  %obu_extension_flag = alloca i32, align 4
  %obu_has_size_field = alloca i32, align 4
  %obu_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %init_bit_pos = alloca i32, align 4
  %init_byte_pos = alloca i32, align 4
  store %struct.avifSequenceHeader* %header, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  store %struct.avifROData* %sample, %struct.avifROData** %sample.addr, align 4, !tbaa !2
  %0 = bitcast %struct.avifROData* %obus to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #3
  %1 = load %struct.avifROData*, %struct.avifROData** %sample.addr, align 4, !tbaa !2
  %2 = bitcast %struct.avifROData* %obus to i8*
  %3 = bitcast %struct.avifROData* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 8, i1 false), !tbaa.struct !6
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %obus, i32 0, i32 1
  %4 = load i32, i32* %size, align 4, !tbaa !9
  %cmp = icmp ugt i32 %4, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = bitcast %struct.avifBits* %bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %5) #3
  %data = getelementptr inbounds %struct.avifROData, %struct.avifROData* %obus, i32 0, i32 0
  %6 = load i8*, i8** %data, align 4, !tbaa !11
  %size1 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %obus, i32 0, i32 1
  %7 = load i32, i32* %size1, align 4, !tbaa !9
  call void @avifBitsInit(%struct.avifBits* %bits, i8* %6, i32 %7)
  %call = call i32 @avifBitsRead(%struct.avifBits* %bits, i32 1)
  %8 = bitcast i32* %obu_type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %call2 = call i32 @avifBitsRead(%struct.avifBits* %bits, i32 4)
  store i32 %call2, i32* %obu_type, align 4, !tbaa !12
  %9 = bitcast i32* %obu_extension_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %call3 = call i32 @avifBitsRead(%struct.avifBits* %bits, i32 1)
  store i32 %call3, i32* %obu_extension_flag, align 4, !tbaa !12
  %10 = bitcast i32* %obu_has_size_field to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %call4 = call i32 @avifBitsRead(%struct.avifBits* %bits, i32 1)
  store i32 %call4, i32* %obu_has_size_field, align 4, !tbaa !12
  %call5 = call i32 @avifBitsRead(%struct.avifBits* %bits, i32 1)
  %11 = load i32, i32* %obu_extension_flag, align 4, !tbaa !12
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %call6 = call i32 @avifBitsRead(%struct.avifBits* %bits, i32 8)
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  %12 = bitcast i32* %obu_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  store i32 0, i32* %obu_size, align 4, !tbaa !12
  %13 = load i32, i32* %obu_has_size_field, align 4, !tbaa !12
  %tobool7 = icmp ne i32 %13, 0
  br i1 %tobool7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end
  %call9 = call i32 @avifBitsReadUleb128(%struct.avifBits* %bits)
  store i32 %call9, i32* %obu_size, align 4, !tbaa !12
  br label %if.end12

if.else:                                          ; preds = %if.end
  %size10 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %obus, i32 0, i32 1
  %14 = load i32, i32* %size10, align 4, !tbaa !9
  %sub = sub nsw i32 %14, 1
  %15 = load i32, i32* %obu_extension_flag, align 4, !tbaa !12
  %sub11 = sub i32 %sub, %15
  store i32 %sub11, i32* %obu_size, align 4, !tbaa !12
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then8
  %error = getelementptr inbounds %struct.avifBits, %struct.avifBits* %bits, i32 0, i32 0
  %16 = load i32, i32* %error, align 8, !tbaa !14
  %tobool13 = icmp ne i32 %16, 0
  br i1 %tobool13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end12
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup31

if.end15:                                         ; preds = %if.end12
  %17 = bitcast i32* %init_bit_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %call16 = call i32 @avifBitsReadPos(%struct.avifBits* %bits)
  store i32 %call16, i32* %init_bit_pos, align 4, !tbaa !12
  %18 = bitcast i32* %init_byte_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load i32, i32* %init_bit_pos, align 4, !tbaa !12
  %shr = lshr i32 %19, 3
  store i32 %shr, i32* %init_byte_pos, align 4, !tbaa !12
  %20 = load i32, i32* %obu_size, align 4, !tbaa !12
  %size17 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %obus, i32 0, i32 1
  %21 = load i32, i32* %size17, align 4, !tbaa !9
  %22 = load i32, i32* %init_byte_pos, align 4, !tbaa !12
  %sub18 = sub i32 %21, %22
  %cmp19 = icmp ugt i32 %20, %sub18
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end15
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end21:                                         ; preds = %if.end15
  %23 = load i32, i32* %obu_type, align 4, !tbaa !12
  %cmp22 = icmp eq i32 %23, 1
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.end21
  %24 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %call24 = call i32 @parseSequenceHeader(%struct.avifBits* %bits, %struct.avifSequenceHeader* %24)
  store i32 %call24, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end25:                                         ; preds = %if.end21
  %25 = load i32, i32* %obu_size, align 4, !tbaa !12
  %26 = load i32, i32* %init_byte_pos, align 4, !tbaa !12
  %add = add i32 %25, %26
  %data26 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %obus, i32 0, i32 0
  %27 = load i8*, i8** %data26, align 4, !tbaa !11
  %add.ptr = getelementptr inbounds i8, i8* %27, i32 %add
  store i8* %add.ptr, i8** %data26, align 4, !tbaa !11
  %28 = load i32, i32* %obu_size, align 4, !tbaa !12
  %29 = load i32, i32* %init_byte_pos, align 4, !tbaa !12
  %add27 = add i32 %28, %29
  %size28 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %obus, i32 0, i32 1
  %30 = load i32, i32* %size28, align 4, !tbaa !9
  %sub29 = sub i32 %30, %add27
  store i32 %sub29, i32* %size28, align 4, !tbaa !9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end25, %if.then23, %if.then20
  %31 = bitcast i32* %init_byte_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  %32 = bitcast i32* %init_bit_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  br label %cleanup31

cleanup31:                                        ; preds = %cleanup, %if.then14
  %33 = bitcast i32* %obu_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  %34 = bitcast i32* %obu_has_size_field to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #3
  %35 = bitcast i32* %obu_extension_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  %36 = bitcast i32* %obu_type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  %37 = bitcast %struct.avifBits* %bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %37) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup36 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup31
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup36

cleanup36:                                        ; preds = %while.end, %cleanup31
  %38 = bitcast %struct.avifROData* %obus to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %38) #3
  %39 = load i32, i32* %retval, align 4
  ret i32 %39
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal void @avifBitsInit(%struct.avifBits* %bits, i8* %data, i32 %size) #0 {
entry:
  %bits.addr = alloca %struct.avifBits*, align 4
  %data.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store %struct.avifBits* %bits, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !7
  %0 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %1 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %start = getelementptr inbounds %struct.avifBits, %struct.avifBits* %1, i32 0, i32 5
  store i8* %0, i8** %start, align 8, !tbaa !17
  %2 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %ptr = getelementptr inbounds %struct.avifBits, %struct.avifBits* %2, i32 0, i32 4
  store i8* %0, i8** %ptr, align 4, !tbaa !18
  %3 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %start1 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %3, i32 0, i32 5
  %4 = load i8*, i8** %start1, align 8, !tbaa !17
  %5 = load i32, i32* %size.addr, align 4, !tbaa !7
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  %6 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %end = getelementptr inbounds %struct.avifBits, %struct.avifBits* %6, i32 0, i32 6
  store i8* %arrayidx, i8** %end, align 4, !tbaa !19
  %7 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %bitsLeft = getelementptr inbounds %struct.avifBits, %struct.avifBits* %7, i32 0, i32 3
  store i32 0, i32* %bitsLeft, align 8, !tbaa !20
  %8 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %state = getelementptr inbounds %struct.avifBits, %struct.avifBits* %8, i32 0, i32 2
  store i64 0, i64* %state, align 8, !tbaa !21
  %9 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.avifBits, %struct.avifBits* %9, i32 0, i32 0
  store i32 0, i32* %error, align 8, !tbaa !14
  %10 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %eof = getelementptr inbounds %struct.avifBits, %struct.avifBits* %10, i32 0, i32 1
  store i32 0, i32* %eof, align 4, !tbaa !22
  ret void
}

; Function Attrs: nounwind
define internal i32 @avifBitsRead(%struct.avifBits* %bits, i32 %n) #0 {
entry:
  %bits.addr = alloca %struct.avifBits*, align 4
  %n.addr = alloca i32, align 4
  %state = alloca i64, align 8
  store %struct.avifBits* %bits, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !12
  %0 = load i32, i32* %n.addr, align 4, !tbaa !12
  %1 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %bitsLeft = getelementptr inbounds %struct.avifBits, %struct.avifBits* %1, i32 0, i32 3
  %2 = load i32, i32* %bitsLeft, align 8, !tbaa !20
  %cmp = icmp ugt i32 %0, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %4 = load i32, i32* %n.addr, align 4, !tbaa !12
  call void @avifBitsRefill(%struct.avifBits* %3, i32 %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = bitcast i64* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #3
  %6 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %state1 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %6, i32 0, i32 2
  %7 = load i64, i64* %state1, align 8, !tbaa !21
  store i64 %7, i64* %state, align 8, !tbaa !23
  %8 = load i32, i32* %n.addr, align 4, !tbaa !12
  %9 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %bitsLeft2 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %9, i32 0, i32 3
  %10 = load i32, i32* %bitsLeft2, align 8, !tbaa !20
  %sub = sub i32 %10, %8
  store i32 %sub, i32* %bitsLeft2, align 8, !tbaa !20
  %11 = load i32, i32* %n.addr, align 4, !tbaa !12
  %12 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %state3 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %12, i32 0, i32 2
  %13 = load i64, i64* %state3, align 8, !tbaa !21
  %sh_prom = zext i32 %11 to i64
  %shl = shl i64 %13, %sh_prom
  store i64 %shl, i64* %state3, align 8, !tbaa !21
  %14 = load i64, i64* %state, align 8, !tbaa !23
  %15 = load i32, i32* %n.addr, align 4, !tbaa !12
  %sub4 = sub i32 64, %15
  %sh_prom5 = zext i32 %sub4 to i64
  %shr = lshr i64 %14, %sh_prom5
  %conv = trunc i64 %shr to i32
  %16 = bitcast i64* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %16) #3
  ret i32 %conv
}

; Function Attrs: nounwind
define internal i32 @avifBitsReadUleb128(%struct.avifBits* %bits) #0 {
entry:
  %retval = alloca i32, align 4
  %bits.addr = alloca %struct.avifBits*, align 4
  %val = alloca i64, align 8
  %more = alloca i32, align 4
  %i = alloca i32, align 4
  %v = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifBits* %bits, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %0 = bitcast i64* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #3
  store i64 0, i64* %val, align 8, !tbaa !23
  %1 = bitcast i32* %more to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %do.body

do.body:                                          ; preds = %land.end, %entry
  %3 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call = call i32 @avifBitsRead(%struct.avifBits* %4, i32 8)
  store i32 %call, i32* %v, align 4, !tbaa !12
  %5 = load i32, i32* %v, align 4, !tbaa !12
  %and = and i32 %5, 128
  store i32 %and, i32* %more, align 4, !tbaa !12
  %6 = load i32, i32* %v, align 4, !tbaa !12
  %and1 = and i32 %6, 127
  %conv = zext i32 %and1 to i64
  %7 = load i32, i32* %i, align 4, !tbaa !12
  %sh_prom = zext i32 %7 to i64
  %shl = shl i64 %conv, %sh_prom
  %8 = load i64, i64* %val, align 8, !tbaa !23
  %or = or i64 %8, %shl
  store i64 %or, i64* %val, align 8, !tbaa !23
  %9 = load i32, i32* %i, align 4, !tbaa !12
  %add = add i32 %9, 7
  store i32 %add, i32* %i, align 4, !tbaa !12
  %10 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %11 = load i32, i32* %more, align 4, !tbaa !12
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %do.cond
  %12 = load i32, i32* %i, align 4, !tbaa !12
  %cmp = icmp ult i32 %12, 56
  br label %land.end

land.end:                                         ; preds = %land.rhs, %do.cond
  %13 = phi i1 [ false, %do.cond ], [ %cmp, %land.rhs ]
  br i1 %13, label %do.body, label %do.end

do.end:                                           ; preds = %land.end
  %14 = load i64, i64* %val, align 8, !tbaa !23
  %cmp3 = icmp ugt i64 %14, 4294967295
  br i1 %cmp3, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %do.end
  %15 = load i32, i32* %more, align 4, !tbaa !12
  %tobool5 = icmp ne i32 %15, 0
  br i1 %tobool5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %do.end
  %16 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.avifBits, %struct.avifBits* %16, i32 0, i32 0
  store i32 1, i32* %error, align 8, !tbaa !14
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %17 = load i64, i64* %val, align 8, !tbaa !23
  %conv6 = trunc i64 %17 to i32
  store i32 %conv6, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %more to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i64* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %20) #3
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: inlinehint nounwind
define internal i32 @avifBitsReadPos(%struct.avifBits* %bits) #2 {
entry:
  %bits.addr = alloca %struct.avifBits*, align 4
  store %struct.avifBits* %bits, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %0 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %ptr = getelementptr inbounds %struct.avifBits, %struct.avifBits* %0, i32 0, i32 4
  %1 = load i8*, i8** %ptr, align 4, !tbaa !18
  %2 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %start = getelementptr inbounds %struct.avifBits, %struct.avifBits* %2, i32 0, i32 5
  %3 = load i8*, i8** %start, align 8, !tbaa !17
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %mul = mul i32 %sub.ptr.sub, 8
  %4 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %bitsLeft = getelementptr inbounds %struct.avifBits, %struct.avifBits* %4, i32 0, i32 3
  %5 = load i32, i32* %bitsLeft, align 8, !tbaa !20
  %sub = sub i32 %mul, %5
  ret i32 %sub
}

; Function Attrs: nounwind
define internal i32 @parseSequenceHeader(%struct.avifBits* %bits, %struct.avifSequenceHeader* %header) #0 {
entry:
  %retval = alloca i32, align 4
  %bits.addr = alloca %struct.avifBits*, align 4
  %header.addr = alloca %struct.avifSequenceHeader*, align 4
  %seq_profile = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %still_picture = alloca i32, align 4
  %reduced_still_picture_header = alloca i32, align 4
  %timing_info_present_flag = alloca i32, align 4
  %decoder_model_info_present_flag = alloca i32, align 4
  %buffer_delay_length = alloca i32, align 4
  %equal_picture_interval = alloca i32, align 4
  %num_ticks_per_picture_minus_1 = alloca i32, align 4
  %initial_display_delay_present_flag = alloca i32, align 4
  %operating_points_cnt = alloca i32, align 4
  %i = alloca i32, align 4
  %seq_level_idx = alloca i32, align 4
  %decoder_model_present_for_this_op = alloca i32, align 4
  %initial_display_delay_present_for_this_op = alloca i32, align 4
  %frame_width_bits = alloca i32, align 4
  %frame_height_bits = alloca i32, align 4
  %frame_id_numbers_present_flag = alloca i32, align 4
  %enable_order_hint = alloca i32, align 4
  %seq_force_screen_content_tools = alloca i32, align 4
  %seq_choose_screen_content_tools = alloca i32, align 4
  %seq_choose_integer_mv = alloca i32, align 4
  %high_bitdepth = alloca i32, align 4
  %twelve_bit = alloca i32, align 4
  %mono_chrome = alloca i32, align 4
  %color_description_present_flag = alloca i32, align 4
  %subsampling_x = alloca i32, align 4
  %subsampling_y = alloca i32, align 4
  store %struct.avifBits* %bits, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  store %struct.avifSequenceHeader* %header, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %0 = bitcast i32* %seq_profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call = call i32 @avifBitsRead(%struct.avifBits* %1, i32 3)
  store i32 %call, i32* %seq_profile, align 4, !tbaa !12
  %2 = load i32, i32* %seq_profile, align 4, !tbaa !12
  %cmp = icmp ugt i32 %2, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup208

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %still_picture to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call1 = call i32 @avifBitsRead(%struct.avifBits* %4, i32 1)
  store i32 %call1, i32* %still_picture, align 4, !tbaa !12
  %5 = bitcast i32* %reduced_still_picture_header to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call2 = call i32 @avifBitsRead(%struct.avifBits* %6, i32 1)
  store i32 %call2, i32* %reduced_still_picture_header, align 4, !tbaa !12
  %7 = load i32, i32* %reduced_still_picture_header, align 4, !tbaa !12
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %land.lhs.true, label %if.end5

land.lhs.true:                                    ; preds = %if.end
  %8 = load i32, i32* %still_picture, align 4, !tbaa !12
  %tobool3 = icmp ne i32 %8, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup206

if.end5:                                          ; preds = %land.lhs.true, %if.end
  %9 = load i32, i32* %reduced_still_picture_header, align 4, !tbaa !12
  %tobool6 = icmp ne i32 %9, 0
  br i1 %tobool6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end5
  %10 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call8 = call i32 @avifBitsRead(%struct.avifBits* %10, i32 5)
  br label %if.end67

if.else:                                          ; preds = %if.end5
  %11 = bitcast i32* %timing_info_present_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call9 = call i32 @avifBitsRead(%struct.avifBits* %12, i32 1)
  store i32 %call9, i32* %timing_info_present_flag, align 4, !tbaa !12
  %13 = bitcast i32* %decoder_model_info_present_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  store i32 0, i32* %decoder_model_info_present_flag, align 4, !tbaa !12
  %14 = bitcast i32* %buffer_delay_length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  store i32 0, i32* %buffer_delay_length, align 4, !tbaa !12
  %15 = load i32, i32* %timing_info_present_flag, align 4, !tbaa !12
  %tobool10 = icmp ne i32 %15, 0
  br i1 %tobool10, label %if.then11, label %if.end32

if.then11:                                        ; preds = %if.else
  %16 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call12 = call i32 @avifBitsRead(%struct.avifBits* %16, i32 32)
  %17 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call13 = call i32 @avifBitsRead(%struct.avifBits* %17, i32 32)
  %18 = bitcast i32* %equal_picture_interval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call14 = call i32 @avifBitsRead(%struct.avifBits* %19, i32 1)
  store i32 %call14, i32* %equal_picture_interval, align 4, !tbaa !12
  %20 = load i32, i32* %equal_picture_interval, align 4, !tbaa !12
  %tobool15 = icmp ne i32 %20, 0
  br i1 %tobool15, label %if.then16, label %if.end21

if.then16:                                        ; preds = %if.then11
  %21 = bitcast i32* %num_ticks_per_picture_minus_1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call17 = call i32 @avifBitsReadVLC(%struct.avifBits* %22)
  store i32 %call17, i32* %num_ticks_per_picture_minus_1, align 4, !tbaa !12
  %23 = load i32, i32* %num_ticks_per_picture_minus_1, align 4, !tbaa !12
  %cmp18 = icmp eq i32 %23, -1
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.then16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %if.then16
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.then19
  %24 = bitcast i32* %num_ticks_per_picture_minus_1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup29 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end21

if.end21:                                         ; preds = %cleanup.cont, %if.then11
  %25 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call22 = call i32 @avifBitsRead(%struct.avifBits* %25, i32 1)
  store i32 %call22, i32* %decoder_model_info_present_flag, align 4, !tbaa !12
  %26 = load i32, i32* %decoder_model_info_present_flag, align 4, !tbaa !12
  %tobool23 = icmp ne i32 %26, 0
  br i1 %tobool23, label %if.then24, label %if.end28

if.then24:                                        ; preds = %if.end21
  %27 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call25 = call i32 @avifBitsRead(%struct.avifBits* %27, i32 5)
  %add = add i32 %call25, 1
  store i32 %add, i32* %buffer_delay_length, align 4, !tbaa !12
  %28 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call26 = call i32 @avifBitsRead(%struct.avifBits* %28, i32 32)
  %29 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call27 = call i32 @avifBitsRead(%struct.avifBits* %29, i32 10)
  br label %if.end28

if.end28:                                         ; preds = %if.then24, %if.end21
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

cleanup29:                                        ; preds = %if.end28, %cleanup
  %30 = bitcast i32* %equal_picture_interval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %cleanup.dest30 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest30, label %cleanup62 [
    i32 0, label %cleanup.cont31
  ]

cleanup.cont31:                                   ; preds = %cleanup29
  br label %if.end32

if.end32:                                         ; preds = %cleanup.cont31, %if.else
  %31 = bitcast i32* %initial_display_delay_present_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #3
  %32 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call33 = call i32 @avifBitsRead(%struct.avifBits* %32, i32 1)
  store i32 %call33, i32* %initial_display_delay_present_flag, align 4, !tbaa !12
  %33 = bitcast i32* %operating_points_cnt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #3
  %34 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call34 = call i32 @avifBitsRead(%struct.avifBits* %34, i32 5)
  %add35 = add i32 %call34, 1
  store i32 %add35, i32* %operating_points_cnt, align 4, !tbaa !12
  %35 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #3
  store i32 0, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end32
  %36 = load i32, i32* %i, align 4, !tbaa !12
  %37 = load i32, i32* %operating_points_cnt, align 4, !tbaa !12
  %cmp36 = icmp ult i32 %36, %37
  br i1 %cmp36, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %38 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %39 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call37 = call i32 @avifBitsRead(%struct.avifBits* %39, i32 12)
  %40 = bitcast i32* %seq_level_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call38 = call i32 @avifBitsRead(%struct.avifBits* %41, i32 5)
  store i32 %call38, i32* %seq_level_idx, align 4, !tbaa !12
  %42 = load i32, i32* %seq_level_idx, align 4, !tbaa !12
  %cmp39 = icmp ugt i32 %42, 7
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %for.body
  %43 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call41 = call i32 @avifBitsRead(%struct.avifBits* %43, i32 1)
  br label %if.end42

if.end42:                                         ; preds = %if.then40, %for.body
  %44 = load i32, i32* %decoder_model_info_present_flag, align 4, !tbaa !12
  %tobool43 = icmp ne i32 %44, 0
  br i1 %tobool43, label %if.then44, label %if.end52

if.then44:                                        ; preds = %if.end42
  %45 = bitcast i32* %decoder_model_present_for_this_op to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #3
  %46 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call45 = call i32 @avifBitsRead(%struct.avifBits* %46, i32 1)
  store i32 %call45, i32* %decoder_model_present_for_this_op, align 4, !tbaa !12
  %47 = load i32, i32* %decoder_model_present_for_this_op, align 4, !tbaa !12
  %tobool46 = icmp ne i32 %47, 0
  br i1 %tobool46, label %if.then47, label %if.end51

if.then47:                                        ; preds = %if.then44
  %48 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %49 = load i32, i32* %buffer_delay_length, align 4, !tbaa !12
  %call48 = call i32 @avifBitsRead(%struct.avifBits* %48, i32 %49)
  %50 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %51 = load i32, i32* %buffer_delay_length, align 4, !tbaa !12
  %call49 = call i32 @avifBitsRead(%struct.avifBits* %50, i32 %51)
  %52 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call50 = call i32 @avifBitsRead(%struct.avifBits* %52, i32 1)
  br label %if.end51

if.end51:                                         ; preds = %if.then47, %if.then44
  %53 = bitcast i32* %decoder_model_present_for_this_op to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end42
  %54 = load i32, i32* %initial_display_delay_present_flag, align 4, !tbaa !12
  %tobool53 = icmp ne i32 %54, 0
  br i1 %tobool53, label %if.then54, label %if.end60

if.then54:                                        ; preds = %if.end52
  %55 = bitcast i32* %initial_display_delay_present_for_this_op to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #3
  %56 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call55 = call i32 @avifBitsRead(%struct.avifBits* %56, i32 1)
  store i32 %call55, i32* %initial_display_delay_present_for_this_op, align 4, !tbaa !12
  %57 = load i32, i32* %initial_display_delay_present_for_this_op, align 4, !tbaa !12
  %tobool56 = icmp ne i32 %57, 0
  br i1 %tobool56, label %if.then57, label %if.end59

if.then57:                                        ; preds = %if.then54
  %58 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call58 = call i32 @avifBitsRead(%struct.avifBits* %58, i32 4)
  br label %if.end59

if.end59:                                         ; preds = %if.then57, %if.then54
  %59 = bitcast i32* %initial_display_delay_present_for_this_op to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  br label %if.end60

if.end60:                                         ; preds = %if.end59, %if.end52
  %60 = bitcast i32* %seq_level_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  br label %for.inc

for.inc:                                          ; preds = %if.end60
  %61 = load i32, i32* %i, align 4, !tbaa !12
  %inc = add i32 %61, 1
  store i32 %inc, i32* %i, align 4, !tbaa !12
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %62 = bitcast i32* %operating_points_cnt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #3
  %63 = bitcast i32* %initial_display_delay_present_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #3
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup62

cleanup62:                                        ; preds = %for.end, %cleanup29
  %64 = bitcast i32* %buffer_delay_length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #3
  %65 = bitcast i32* %decoder_model_info_present_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #3
  %66 = bitcast i32* %timing_info_present_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #3
  %cleanup.dest65 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest65, label %cleanup206 [
    i32 0, label %cleanup.cont66
  ]

cleanup.cont66:                                   ; preds = %cleanup62
  br label %if.end67

if.end67:                                         ; preds = %cleanup.cont66, %if.then7
  %67 = bitcast i32* %frame_width_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #3
  %68 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call68 = call i32 @avifBitsRead(%struct.avifBits* %68, i32 4)
  %add69 = add i32 %call68, 1
  store i32 %add69, i32* %frame_width_bits, align 4, !tbaa !12
  %69 = bitcast i32* %frame_height_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #3
  %70 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call70 = call i32 @avifBitsRead(%struct.avifBits* %70, i32 4)
  %add71 = add i32 %call70, 1
  store i32 %add71, i32* %frame_height_bits, align 4, !tbaa !12
  %71 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %72 = load i32, i32* %frame_width_bits, align 4, !tbaa !12
  %call72 = call i32 @avifBitsRead(%struct.avifBits* %71, i32 %72)
  %add73 = add i32 %call72, 1
  %73 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %maxWidth = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %73, i32 0, i32 0
  store i32 %add73, i32* %maxWidth, align 4, !tbaa !24
  %74 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %75 = load i32, i32* %frame_height_bits, align 4, !tbaa !12
  %call74 = call i32 @avifBitsRead(%struct.avifBits* %74, i32 %75)
  %add75 = add i32 %call74, 1
  %76 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %maxHeight = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %76, i32 0, i32 1
  store i32 %add75, i32* %maxHeight, align 4, !tbaa !26
  %77 = bitcast i32* %frame_id_numbers_present_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #3
  store i32 0, i32* %frame_id_numbers_present_flag, align 4, !tbaa !12
  %78 = load i32, i32* %reduced_still_picture_header, align 4, !tbaa !12
  %tobool76 = icmp ne i32 %78, 0
  br i1 %tobool76, label %if.end79, label %if.then77

if.then77:                                        ; preds = %if.end67
  %79 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call78 = call i32 @avifBitsRead(%struct.avifBits* %79, i32 1)
  store i32 %call78, i32* %frame_id_numbers_present_flag, align 4, !tbaa !12
  br label %if.end79

if.end79:                                         ; preds = %if.then77, %if.end67
  %80 = load i32, i32* %frame_id_numbers_present_flag, align 4, !tbaa !12
  %tobool80 = icmp ne i32 %80, 0
  br i1 %tobool80, label %if.then81, label %if.end83

if.then81:                                        ; preds = %if.end79
  %81 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call82 = call i32 @avifBitsRead(%struct.avifBits* %81, i32 7)
  br label %if.end83

if.end83:                                         ; preds = %if.then81, %if.end79
  %82 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call84 = call i32 @avifBitsRead(%struct.avifBits* %82, i32 3)
  %83 = load i32, i32* %reduced_still_picture_header, align 4, !tbaa !12
  %tobool85 = icmp ne i32 %83, 0
  br i1 %tobool85, label %if.end111, label %if.then86

if.then86:                                        ; preds = %if.end83
  %84 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call87 = call i32 @avifBitsRead(%struct.avifBits* %84, i32 4)
  %85 = bitcast i32* %enable_order_hint to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #3
  %86 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call88 = call i32 @avifBitsRead(%struct.avifBits* %86, i32 1)
  store i32 %call88, i32* %enable_order_hint, align 4, !tbaa !12
  %87 = load i32, i32* %enable_order_hint, align 4, !tbaa !12
  %tobool89 = icmp ne i32 %87, 0
  br i1 %tobool89, label %if.then90, label %if.end92

if.then90:                                        ; preds = %if.then86
  %88 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call91 = call i32 @avifBitsRead(%struct.avifBits* %88, i32 2)
  br label %if.end92

if.end92:                                         ; preds = %if.then90, %if.then86
  %89 = bitcast i32* %seq_force_screen_content_tools to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #3
  store i32 0, i32* %seq_force_screen_content_tools, align 4, !tbaa !12
  %90 = bitcast i32* %seq_choose_screen_content_tools to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #3
  %91 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call93 = call i32 @avifBitsRead(%struct.avifBits* %91, i32 1)
  store i32 %call93, i32* %seq_choose_screen_content_tools, align 4, !tbaa !12
  %92 = load i32, i32* %seq_choose_screen_content_tools, align 4, !tbaa !12
  %tobool94 = icmp ne i32 %92, 0
  br i1 %tobool94, label %if.then95, label %if.else96

if.then95:                                        ; preds = %if.end92
  store i32 2, i32* %seq_force_screen_content_tools, align 4, !tbaa !12
  br label %if.end98

if.else96:                                        ; preds = %if.end92
  %93 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call97 = call i32 @avifBitsRead(%struct.avifBits* %93, i32 1)
  store i32 %call97, i32* %seq_force_screen_content_tools, align 4, !tbaa !12
  br label %if.end98

if.end98:                                         ; preds = %if.else96, %if.then95
  %94 = load i32, i32* %seq_force_screen_content_tools, align 4, !tbaa !12
  %cmp99 = icmp ugt i32 %94, 0
  br i1 %cmp99, label %if.then100, label %if.end106

if.then100:                                       ; preds = %if.end98
  %95 = bitcast i32* %seq_choose_integer_mv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #3
  %96 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call101 = call i32 @avifBitsRead(%struct.avifBits* %96, i32 1)
  store i32 %call101, i32* %seq_choose_integer_mv, align 4, !tbaa !12
  %97 = load i32, i32* %seq_choose_integer_mv, align 4, !tbaa !12
  %tobool102 = icmp ne i32 %97, 0
  br i1 %tobool102, label %if.end105, label %if.then103

if.then103:                                       ; preds = %if.then100
  %98 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call104 = call i32 @avifBitsRead(%struct.avifBits* %98, i32 1)
  br label %if.end105

if.end105:                                        ; preds = %if.then103, %if.then100
  %99 = bitcast i32* %seq_choose_integer_mv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #3
  br label %if.end106

if.end106:                                        ; preds = %if.end105, %if.end98
  %100 = load i32, i32* %enable_order_hint, align 4, !tbaa !12
  %tobool107 = icmp ne i32 %100, 0
  br i1 %tobool107, label %if.then108, label %if.end110

if.then108:                                       ; preds = %if.end106
  %101 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call109 = call i32 @avifBitsRead(%struct.avifBits* %101, i32 3)
  br label %if.end110

if.end110:                                        ; preds = %if.then108, %if.end106
  %102 = bitcast i32* %seq_choose_screen_content_tools to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #3
  %103 = bitcast i32* %seq_force_screen_content_tools to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #3
  %104 = bitcast i32* %enable_order_hint to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #3
  br label %if.end111

if.end111:                                        ; preds = %if.end110, %if.end83
  %105 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call112 = call i32 @avifBitsRead(%struct.avifBits* %105, i32 3)
  %106 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %bitDepth = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %106, i32 0, i32 2
  store i32 8, i32* %bitDepth, align 4, !tbaa !27
  %107 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %chromaSamplePosition = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %107, i32 0, i32 4
  store i32 0, i32* %chromaSamplePosition, align 4, !tbaa !28
  %108 = bitcast i32* %high_bitdepth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #3
  %109 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call113 = call i32 @avifBitsRead(%struct.avifBits* %109, i32 1)
  store i32 %call113, i32* %high_bitdepth, align 4, !tbaa !12
  %110 = load i32, i32* %seq_profile, align 4, !tbaa !12
  %cmp114 = icmp eq i32 %110, 2
  br i1 %cmp114, label %land.lhs.true115, label %if.else121

land.lhs.true115:                                 ; preds = %if.end111
  %111 = load i32, i32* %high_bitdepth, align 4, !tbaa !12
  %tobool116 = icmp ne i32 %111, 0
  br i1 %tobool116, label %if.then117, label %if.else121

if.then117:                                       ; preds = %land.lhs.true115
  %112 = bitcast i32* %twelve_bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #3
  %113 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call118 = call i32 @avifBitsRead(%struct.avifBits* %113, i32 1)
  store i32 %call118, i32* %twelve_bit, align 4, !tbaa !12
  %114 = load i32, i32* %twelve_bit, align 4, !tbaa !12
  %tobool119 = icmp ne i32 %114, 0
  %115 = zext i1 %tobool119 to i64
  %cond = select i1 %tobool119, i32 12, i32 10
  %116 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %bitDepth120 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %116, i32 0, i32 2
  store i32 %cond, i32* %bitDepth120, align 4, !tbaa !27
  %117 = bitcast i32* %twelve_bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  br label %if.end125

if.else121:                                       ; preds = %land.lhs.true115, %if.end111
  %118 = load i32, i32* %high_bitdepth, align 4, !tbaa !12
  %tobool122 = icmp ne i32 %118, 0
  %119 = zext i1 %tobool122 to i64
  %cond123 = select i1 %tobool122, i32 10, i32 8
  %120 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %bitDepth124 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %120, i32 0, i32 2
  store i32 %cond123, i32* %bitDepth124, align 4, !tbaa !27
  br label %if.end125

if.end125:                                        ; preds = %if.else121, %if.then117
  %121 = bitcast i32* %mono_chrome to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #3
  store i32 0, i32* %mono_chrome, align 4, !tbaa !12
  %122 = load i32, i32* %seq_profile, align 4, !tbaa !12
  %cmp126 = icmp ne i32 %122, 1
  br i1 %cmp126, label %if.then127, label %if.end129

if.then127:                                       ; preds = %if.end125
  %123 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call128 = call i32 @avifBitsRead(%struct.avifBits* %123, i32 1)
  store i32 %call128, i32* %mono_chrome, align 4, !tbaa !12
  br label %if.end129

if.end129:                                        ; preds = %if.then127, %if.end125
  %124 = bitcast i32* %color_description_present_flag to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #3
  %125 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call130 = call i32 @avifBitsRead(%struct.avifBits* %125, i32 1)
  store i32 %call130, i32* %color_description_present_flag, align 4, !tbaa !12
  %126 = load i32, i32* %color_description_present_flag, align 4, !tbaa !12
  %tobool131 = icmp ne i32 %126, 0
  br i1 %tobool131, label %if.then132, label %if.else136

if.then132:                                       ; preds = %if.end129
  %127 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call133 = call i32 @avifBitsRead(%struct.avifBits* %127, i32 8)
  %128 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %colorPrimaries = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %128, i32 0, i32 5
  store i32 %call133, i32* %colorPrimaries, align 4, !tbaa !29
  %129 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call134 = call i32 @avifBitsRead(%struct.avifBits* %129, i32 8)
  %130 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %transferCharacteristics = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %130, i32 0, i32 6
  store i32 %call134, i32* %transferCharacteristics, align 4, !tbaa !30
  %131 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call135 = call i32 @avifBitsRead(%struct.avifBits* %131, i32 8)
  %132 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %matrixCoefficients = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %132, i32 0, i32 7
  store i32 %call135, i32* %matrixCoefficients, align 4, !tbaa !31
  br label %if.end140

if.else136:                                       ; preds = %if.end129
  %133 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %colorPrimaries137 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %133, i32 0, i32 5
  store i32 2, i32* %colorPrimaries137, align 4, !tbaa !29
  %134 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %transferCharacteristics138 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %134, i32 0, i32 6
  store i32 2, i32* %transferCharacteristics138, align 4, !tbaa !30
  %135 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %matrixCoefficients139 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %135, i32 0, i32 7
  store i32 2, i32* %matrixCoefficients139, align 4, !tbaa !31
  br label %if.end140

if.end140:                                        ; preds = %if.else136, %if.then132
  %136 = load i32, i32* %mono_chrome, align 4, !tbaa !12
  %tobool141 = icmp ne i32 %136, 0
  br i1 %tobool141, label %if.then142, label %if.else146

if.then142:                                       ; preds = %if.end140
  %137 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call143 = call i32 @avifBitsRead(%struct.avifBits* %137, i32 1)
  %tobool144 = icmp ne i32 %call143, 0
  %138 = zext i1 %tobool144 to i64
  %cond145 = select i1 %tobool144, i32 1, i32 0
  %139 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %range = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %139, i32 0, i32 8
  store i32 %cond145, i32* %range, align 4, !tbaa !32
  %140 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %140, i32 0, i32 3
  store i32 4, i32* %yuvFormat, align 4, !tbaa !33
  br label %if.end193

if.else146:                                       ; preds = %if.end140
  %141 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %colorPrimaries147 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %141, i32 0, i32 5
  %142 = load i32, i32* %colorPrimaries147, align 4, !tbaa !29
  %cmp148 = icmp eq i32 %142, 1
  br i1 %cmp148, label %land.lhs.true149, label %if.else158

land.lhs.true149:                                 ; preds = %if.else146
  %143 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %transferCharacteristics150 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %143, i32 0, i32 6
  %144 = load i32, i32* %transferCharacteristics150, align 4, !tbaa !30
  %cmp151 = icmp eq i32 %144, 13
  br i1 %cmp151, label %land.lhs.true152, label %if.else158

land.lhs.true152:                                 ; preds = %land.lhs.true149
  %145 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %matrixCoefficients153 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %145, i32 0, i32 7
  %146 = load i32, i32* %matrixCoefficients153, align 4, !tbaa !31
  %cmp154 = icmp eq i32 %146, 0
  br i1 %cmp154, label %if.then155, label %if.else158

if.then155:                                       ; preds = %land.lhs.true152
  %147 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %range156 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %147, i32 0, i32 8
  store i32 1, i32* %range156, align 4, !tbaa !32
  %148 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %yuvFormat157 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %148, i32 0, i32 3
  store i32 1, i32* %yuvFormat157, align 4, !tbaa !33
  br label %if.end192

if.else158:                                       ; preds = %land.lhs.true152, %land.lhs.true149, %if.else146
  %149 = bitcast i32* %subsampling_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #3
  store i32 0, i32* %subsampling_x, align 4, !tbaa !12
  %150 = bitcast i32* %subsampling_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #3
  store i32 0, i32* %subsampling_y, align 4, !tbaa !12
  %151 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call159 = call i32 @avifBitsRead(%struct.avifBits* %151, i32 1)
  %tobool160 = icmp ne i32 %call159, 0
  %152 = zext i1 %tobool160 to i64
  %cond161 = select i1 %tobool160, i32 1, i32 0
  %153 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %range162 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %153, i32 0, i32 8
  store i32 %cond161, i32* %range162, align 4, !tbaa !32
  %154 = load i32, i32* %seq_profile, align 4, !tbaa !12
  switch i32 %154, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb164
    i32 2, label %sw.bb166
  ]

sw.bb:                                            ; preds = %if.else158
  store i32 1, i32* %subsampling_x, align 4, !tbaa !12
  store i32 1, i32* %subsampling_y, align 4, !tbaa !12
  %155 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %yuvFormat163 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %155, i32 0, i32 3
  store i32 3, i32* %yuvFormat163, align 4, !tbaa !33
  br label %sw.epilog

sw.bb164:                                         ; preds = %if.else158
  store i32 0, i32* %subsampling_x, align 4, !tbaa !12
  store i32 0, i32* %subsampling_y, align 4, !tbaa !12
  %156 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %yuvFormat165 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %156, i32 0, i32 3
  store i32 1, i32* %yuvFormat165, align 4, !tbaa !33
  br label %sw.epilog

sw.bb166:                                         ; preds = %if.else158
  %157 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %bitDepth167 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %157, i32 0, i32 2
  %158 = load i32, i32* %bitDepth167, align 4, !tbaa !27
  %cmp168 = icmp eq i32 %158, 12
  br i1 %cmp168, label %if.then169, label %if.else175

if.then169:                                       ; preds = %sw.bb166
  %159 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call170 = call i32 @avifBitsRead(%struct.avifBits* %159, i32 1)
  store i32 %call170, i32* %subsampling_x, align 4, !tbaa !12
  %160 = load i32, i32* %subsampling_x, align 4, !tbaa !12
  %tobool171 = icmp ne i32 %160, 0
  br i1 %tobool171, label %if.then172, label %if.end174

if.then172:                                       ; preds = %if.then169
  %161 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call173 = call i32 @avifBitsRead(%struct.avifBits* %161, i32 1)
  store i32 %call173, i32* %subsampling_y, align 4, !tbaa !12
  br label %if.end174

if.end174:                                        ; preds = %if.then172, %if.then169
  br label %if.end176

if.else175:                                       ; preds = %sw.bb166
  store i32 1, i32* %subsampling_x, align 4, !tbaa !12
  store i32 0, i32* %subsampling_y, align 4, !tbaa !12
  br label %if.end176

if.end176:                                        ; preds = %if.else175, %if.end174
  %162 = load i32, i32* %subsampling_x, align 4, !tbaa !12
  %tobool177 = icmp ne i32 %162, 0
  br i1 %tobool177, label %if.then178, label %if.else182

if.then178:                                       ; preds = %if.end176
  %163 = load i32, i32* %subsampling_y, align 4, !tbaa !12
  %tobool179 = icmp ne i32 %163, 0
  %164 = zext i1 %tobool179 to i64
  %cond180 = select i1 %tobool179, i32 3, i32 2
  %165 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %yuvFormat181 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %165, i32 0, i32 3
  store i32 %cond180, i32* %yuvFormat181, align 4, !tbaa !33
  br label %if.end184

if.else182:                                       ; preds = %if.end176
  %166 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %yuvFormat183 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %166, i32 0, i32 3
  store i32 1, i32* %yuvFormat183, align 4, !tbaa !33
  br label %if.end184

if.end184:                                        ; preds = %if.else182, %if.then178
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.else158, %if.end184, %sw.bb164, %sw.bb
  %167 = load i32, i32* %subsampling_x, align 4, !tbaa !12
  %tobool185 = icmp ne i32 %167, 0
  br i1 %tobool185, label %land.lhs.true186, label %if.end191

land.lhs.true186:                                 ; preds = %sw.epilog
  %168 = load i32, i32* %subsampling_y, align 4, !tbaa !12
  %tobool187 = icmp ne i32 %168, 0
  br i1 %tobool187, label %if.then188, label %if.end191

if.then188:                                       ; preds = %land.lhs.true186
  %169 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call189 = call i32 @avifBitsRead(%struct.avifBits* %169, i32 2)
  %170 = load %struct.avifSequenceHeader*, %struct.avifSequenceHeader** %header.addr, align 4, !tbaa !2
  %chromaSamplePosition190 = getelementptr inbounds %struct.avifSequenceHeader, %struct.avifSequenceHeader* %170, i32 0, i32 4
  store i32 %call189, i32* %chromaSamplePosition190, align 4, !tbaa !28
  br label %if.end191

if.end191:                                        ; preds = %if.then188, %land.lhs.true186, %sw.epilog
  %171 = bitcast i32* %subsampling_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #3
  %172 = bitcast i32* %subsampling_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #3
  br label %if.end192

if.end192:                                        ; preds = %if.end191, %if.then155
  br label %if.end193

if.end193:                                        ; preds = %if.end192, %if.then142
  %173 = load i32, i32* %mono_chrome, align 4, !tbaa !12
  %tobool194 = icmp ne i32 %173, 0
  br i1 %tobool194, label %if.end197, label %if.then195

if.then195:                                       ; preds = %if.end193
  %174 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call196 = call i32 @avifBitsRead(%struct.avifBits* %174, i32 1)
  br label %if.end197

if.end197:                                        ; preds = %if.then195, %if.end193
  %175 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call198 = call i32 @avifBitsRead(%struct.avifBits* %175, i32 1)
  %176 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.avifBits, %struct.avifBits* %176, i32 0, i32 0
  %177 = load i32, i32* %error, align 8, !tbaa !14
  %tobool199 = icmp ne i32 %177, 0
  %lnot = xor i1 %tobool199, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %178 = bitcast i32* %color_description_present_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #3
  %179 = bitcast i32* %mono_chrome to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #3
  %180 = bitcast i32* %high_bitdepth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #3
  %181 = bitcast i32* %frame_id_numbers_present_flag to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #3
  %182 = bitcast i32* %frame_height_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #3
  %183 = bitcast i32* %frame_width_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #3
  br label %cleanup206

cleanup206:                                       ; preds = %if.end197, %cleanup62, %if.then4
  %184 = bitcast i32* %reduced_still_picture_header to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #3
  %185 = bitcast i32* %still_picture to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #3
  br label %cleanup208

cleanup208:                                       ; preds = %cleanup206, %if.then
  %186 = bitcast i32* %seq_profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #3
  %187 = load i32, i32* %retval, align 4
  ret i32 %187
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @avifBitsRefill(%struct.avifBits* %bits, i32 %n) #0 {
entry:
  %bits.addr = alloca %struct.avifBits*, align 4
  %n.addr = alloca i32, align 4
  %state = alloca i64, align 8
  store %struct.avifBits* %bits, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !12
  %0 = bitcast i64* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #3
  store i64 0, i64* %state, align 8, !tbaa !23
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %1 = load i64, i64* %state, align 8, !tbaa !23
  %shl = shl i64 %1, 8
  store i64 %shl, i64* %state, align 8, !tbaa !23
  %2 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %bitsLeft = getelementptr inbounds %struct.avifBits, %struct.avifBits* %2, i32 0, i32 3
  %3 = load i32, i32* %bitsLeft, align 8, !tbaa !20
  %add = add i32 %3, 8
  store i32 %add, i32* %bitsLeft, align 8, !tbaa !20
  %4 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %eof = getelementptr inbounds %struct.avifBits, %struct.avifBits* %4, i32 0, i32 1
  %5 = load i32, i32* %eof, align 4, !tbaa !22
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  %6 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %ptr = getelementptr inbounds %struct.avifBits, %struct.avifBits* %6, i32 0, i32 4
  %7 = load i8*, i8** %ptr, align 4, !tbaa !18
  %incdec.ptr = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr, i8** %ptr, align 4, !tbaa !18
  %8 = load i8, i8* %7, align 1, !tbaa !34
  %conv = zext i8 %8 to i64
  %9 = load i64, i64* %state, align 8, !tbaa !23
  %or = or i64 %9, %conv
  store i64 %or, i64* %state, align 8, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %if.then, %do.body
  %10 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %ptr1 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %10, i32 0, i32 4
  %11 = load i8*, i8** %ptr1, align 4, !tbaa !18
  %12 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %end = getelementptr inbounds %struct.avifBits, %struct.avifBits* %12, i32 0, i32 6
  %13 = load i8*, i8** %end, align 4, !tbaa !19
  %cmp = icmp uge i8* %11, %13
  br i1 %cmp, label %if.then3, label %if.end6

if.then3:                                         ; preds = %if.end
  %14 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %eof4 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %14, i32 0, i32 1
  %15 = load i32, i32* %eof4, align 4, !tbaa !22
  %16 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %error = getelementptr inbounds %struct.avifBits, %struct.avifBits* %16, i32 0, i32 0
  store i32 %15, i32* %error, align 8, !tbaa !14
  %17 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %eof5 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %17, i32 0, i32 1
  store i32 1, i32* %eof5, align 4, !tbaa !22
  br label %if.end6

if.end6:                                          ; preds = %if.then3, %if.end
  br label %do.cond

do.cond:                                          ; preds = %if.end6
  %18 = load i32, i32* %n.addr, align 4, !tbaa !12
  %19 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %bitsLeft7 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %19, i32 0, i32 3
  %20 = load i32, i32* %bitsLeft7, align 8, !tbaa !20
  %cmp8 = icmp ugt i32 %18, %20
  br i1 %cmp8, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %21 = load i64, i64* %state, align 8, !tbaa !23
  %22 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %bitsLeft10 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %22, i32 0, i32 3
  %23 = load i32, i32* %bitsLeft10, align 8, !tbaa !20
  %sub = sub i32 64, %23
  %sh_prom = zext i32 %sub to i64
  %shl11 = shl i64 %21, %sh_prom
  %24 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %state12 = getelementptr inbounds %struct.avifBits, %struct.avifBits* %24, i32 0, i32 2
  %25 = load i64, i64* %state12, align 8, !tbaa !21
  %or13 = or i64 %25, %shl11
  store i64 %or13, i64* %state12, align 8, !tbaa !21
  %26 = bitcast i64* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %26) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @avifBitsReadVLC(%struct.avifBits* %bits) #0 {
entry:
  %retval = alloca i32, align 4
  %bits.addr = alloca %struct.avifBits*, align 4
  %numBits = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifBits* %bits, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %0 = bitcast i32* %numBits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %numBits, align 4, !tbaa !12
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %1 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %call = call i32 @avifBitsRead(%struct.avifBits* %1, i32 1)
  %tobool = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %numBits, align 4, !tbaa !12
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %numBits, align 4, !tbaa !12
  %cmp = icmp eq i32 %inc, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %3 = load i32, i32* %numBits, align 4, !tbaa !12
  %tobool1 = icmp ne i32 %3, 0
  br i1 %tobool1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.end
  %4 = load i32, i32* %numBits, align 4, !tbaa !12
  %shl = shl i32 1, %4
  %sub = sub i32 %shl, 1
  %5 = load %struct.avifBits*, %struct.avifBits** %bits.addr, align 4, !tbaa !2
  %6 = load i32, i32* %numBits, align 4, !tbaa !12
  %call2 = call i32 @avifBitsRead(%struct.avifBits* %5, i32 %6)
  %add = add i32 %sub, %call2
  br label %cond.end

cond.false:                                       ; preds = %while.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end, %if.then
  %7 = bitcast i32* %numBits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #3
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{i64 0, i64 4, !2, i64 4, i64 4, !7}
!7 = !{!8, !8, i64 0}
!8 = !{!"long", !4, i64 0}
!9 = !{!10, !8, i64 4}
!10 = !{!"avifROData", !3, i64 0, !8, i64 4}
!11 = !{!10, !3, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"int", !4, i64 0}
!14 = !{!15, !13, i64 0}
!15 = !{!"avifBits", !13, i64 0, !13, i64 4, !16, i64 8, !13, i64 16, !3, i64 20, !3, i64 24, !3, i64 28}
!16 = !{!"long long", !4, i64 0}
!17 = !{!15, !3, i64 24}
!18 = !{!15, !3, i64 20}
!19 = !{!15, !3, i64 28}
!20 = !{!15, !13, i64 16}
!21 = !{!15, !16, i64 8}
!22 = !{!15, !13, i64 4}
!23 = !{!16, !16, i64 0}
!24 = !{!25, !13, i64 0}
!25 = !{!"avifSequenceHeader", !13, i64 0, !13, i64 4, !13, i64 8, !4, i64 12, !4, i64 16, !4, i64 20, !4, i64 24, !4, i64 28, !4, i64 32}
!26 = !{!25, !13, i64 4}
!27 = !{!25, !13, i64 8}
!28 = !{!25, !4, i64 16}
!29 = !{!25, !4, i64 20}
!30 = !{!25, !4, i64 24}
!31 = !{!25, !4, i64 28}
!32 = !{!25, !4, i64 32}
!33 = !{!25, !4, i64 12}
!34 = !{!4, !4, i64 0}
