; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/hash.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/hash.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._crc_calculator = type { i32, i32, i32, [256 x i32], i32 }
%struct._CRC32C = type { [8 x [256 x i32]] }

; Function Attrs: nounwind
define hidden void @av1_crc_calculator_init(%struct._crc_calculator* %p_crc_calculator, i32 %bits, i32 %truncPoly) #0 {
entry:
  %p_crc_calculator.addr = alloca %struct._crc_calculator*, align 4
  %bits.addr = alloca i32, align 4
  %truncPoly.addr = alloca i32, align 4
  store %struct._crc_calculator* %p_crc_calculator, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !6
  store i32 %truncPoly, i32* %truncPoly.addr, align 4, !tbaa !6
  %0 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %remainder = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %0, i32 0, i32 0
  store i32 0, i32* %remainder, align 4, !tbaa !8
  %1 = load i32, i32* %bits.addr, align 4, !tbaa !6
  %2 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %bits1 = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %2, i32 0, i32 2
  store i32 %1, i32* %bits1, align 4, !tbaa !10
  %3 = load i32, i32* %truncPoly.addr, align 4, !tbaa !6
  %4 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %trunc_poly = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %4, i32 0, i32 1
  store i32 %3, i32* %trunc_poly, align 4, !tbaa !11
  %5 = load i32, i32* %bits.addr, align 4, !tbaa !6
  %shl = shl i32 1, %5
  %sub = sub nsw i32 %shl, 1
  %6 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %final_result_mask = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %6, i32 0, i32 4
  store i32 %sub, i32* %final_result_mask, align 4, !tbaa !12
  %7 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  call void @crc_calculator_init_table(%struct._crc_calculator* %7)
  ret void
}

; Function Attrs: nounwind
define internal void @crc_calculator_init_table(%struct._crc_calculator* %p_crc_calculator) #0 {
entry:
  %p_crc_calculator.addr = alloca %struct._crc_calculator*, align 4
  %high_bit = alloca i32, align 4
  %byte_high_bit = alloca i32, align 4
  %value = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %remainder = alloca i32, align 4
  %mask = alloca i8, align 1
  store %struct._crc_calculator* %p_crc_calculator, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %0 = bitcast i32* %high_bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %bits = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %1, i32 0, i32 2
  %2 = load i32, i32* %bits, align 4, !tbaa !10
  %sub = sub i32 %2, 1
  %shl = shl i32 1, %sub
  store i32 %shl, i32* %high_bit, align 4, !tbaa !6
  %3 = bitcast i32* %byte_high_bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  store i32 128, i32* %byte_high_bit, align 4, !tbaa !6
  %4 = bitcast i32* %value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  store i32 0, i32* %value, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc16, %entry
  %5 = load i32, i32* %value, align 4, !tbaa !6
  %cmp = icmp ult i32 %5, 256
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %6 = bitcast i32* %value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #2
  br label %for.end17

for.body:                                         ; preds = %for.cond
  %7 = bitcast i32* %remainder to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  store i32 0, i32* %remainder, align 4, !tbaa !6
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %mask) #2
  store i8 -128, i8* %mask, align 1, !tbaa !13
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %8 = load i8, i8* %mask, align 1, !tbaa !13
  %conv = zext i8 %8 to i32
  %cmp2 = icmp ne i32 %conv, 0
  br i1 %cmp2, label %for.body5, label %for.cond.cleanup4

for.cond.cleanup4:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %mask) #2
  br label %for.end

for.body5:                                        ; preds = %for.cond1
  %9 = load i32, i32* %value, align 4, !tbaa !6
  %10 = load i8, i8* %mask, align 1, !tbaa !13
  %conv6 = zext i8 %10 to i32
  %and = and i32 %9, %conv6
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body5
  %11 = load i32, i32* %high_bit, align 4, !tbaa !6
  %12 = load i32, i32* %remainder, align 4, !tbaa !6
  %xor = xor i32 %12, %11
  store i32 %xor, i32* %remainder, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body5
  %13 = load i32, i32* %remainder, align 4, !tbaa !6
  %14 = load i32, i32* %high_bit, align 4, !tbaa !6
  %and7 = and i32 %13, %14
  %tobool8 = icmp ne i32 %and7, 0
  br i1 %tobool8, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.end
  %15 = load i32, i32* %remainder, align 4, !tbaa !6
  %shl10 = shl i32 %15, 1
  store i32 %shl10, i32* %remainder, align 4, !tbaa !6
  %16 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %trunc_poly = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %16, i32 0, i32 1
  %17 = load i32, i32* %trunc_poly, align 4, !tbaa !11
  %18 = load i32, i32* %remainder, align 4, !tbaa !6
  %xor11 = xor i32 %18, %17
  store i32 %xor11, i32* %remainder, align 4, !tbaa !6
  br label %if.end13

if.else:                                          ; preds = %if.end
  %19 = load i32, i32* %remainder, align 4, !tbaa !6
  %shl12 = shl i32 %19, 1
  store i32 %shl12, i32* %remainder, align 4, !tbaa !6
  br label %if.end13

if.end13:                                         ; preds = %if.else, %if.then9
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %20 = load i8, i8* %mask, align 1, !tbaa !13
  %conv14 = zext i8 %20 to i32
  %shr = ashr i32 %conv14, 1
  %conv15 = trunc i32 %shr to i8
  store i8 %conv15, i8* %mask, align 1, !tbaa !13
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup4
  %21 = load i32, i32* %remainder, align 4, !tbaa !6
  %22 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %table = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %22, i32 0, i32 3
  %23 = load i32, i32* %value, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [256 x i32], [256 x i32]* %table, i32 0, i32 %23
  store i32 %21, i32* %arrayidx, align 4, !tbaa !6
  %24 = bitcast i32* %remainder to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #2
  br label %for.inc16

for.inc16:                                        ; preds = %for.end
  %25 = load i32, i32* %value, align 4, !tbaa !6
  %inc = add i32 %25, 1
  store i32 %inc, i32* %value, align 4, !tbaa !6
  br label %for.cond

for.end17:                                        ; preds = %for.cond.cleanup
  %26 = bitcast i32* %byte_high_bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #2
  %27 = bitcast i32* %high_bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @av1_get_crc_value(%struct._crc_calculator* %p_crc_calculator, i8* %p, i32 %length) #0 {
entry:
  %p_crc_calculator.addr = alloca %struct._crc_calculator*, align 4
  %p.addr = alloca i8*, align 4
  %length.addr = alloca i32, align 4
  store %struct._crc_calculator* %p_crc_calculator, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  store i8* %p, i8** %p.addr, align 4, !tbaa !2
  store i32 %length, i32* %length.addr, align 4, !tbaa !6
  %0 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  call void @crc_calculator_reset(%struct._crc_calculator* %0)
  %1 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %2 = load i8*, i8** %p.addr, align 4, !tbaa !2
  %3 = load i32, i32* %length.addr, align 4, !tbaa !6
  call void @crc_calculator_process_data(%struct._crc_calculator* %1, i8* %2, i32 %3)
  %4 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %call = call i32 @crc_calculator_get_crc(%struct._crc_calculator* %4)
  ret i32 %call
}

; Function Attrs: nounwind
define internal void @crc_calculator_reset(%struct._crc_calculator* %p_crc_calculator) #0 {
entry:
  %p_crc_calculator.addr = alloca %struct._crc_calculator*, align 4
  store %struct._crc_calculator* %p_crc_calculator, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %0 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %remainder = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %0, i32 0, i32 0
  store i32 0, i32* %remainder, align 4, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define internal void @crc_calculator_process_data(%struct._crc_calculator* %p_crc_calculator, i8* %pData, i32 %dataLength) #0 {
entry:
  %p_crc_calculator.addr = alloca %struct._crc_calculator*, align 4
  %pData.addr = alloca i8*, align 4
  %dataLength.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %index = alloca i8, align 1
  store %struct._crc_calculator* %p_crc_calculator, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  store i8* %pData, i8** %pData.addr, align 4, !tbaa !2
  store i32 %dataLength, i32* %dataLength.addr, align 4, !tbaa !6
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %dataLength.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %index) #2
  %4 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %remainder = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %4, i32 0, i32 0
  %5 = load i32, i32* %remainder, align 4, !tbaa !8
  %6 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %bits = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %6, i32 0, i32 2
  %7 = load i32, i32* %bits, align 4, !tbaa !10
  %sub = sub i32 %7, 8
  %shr = lshr i32 %5, %sub
  %8 = load i8*, i8** %pData.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !13
  %conv = zext i8 %10 to i32
  %xor = xor i32 %shr, %conv
  %conv1 = trunc i32 %xor to i8
  store i8 %conv1, i8* %index, align 1, !tbaa !13
  %11 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %remainder2 = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %11, i32 0, i32 0
  %12 = load i32, i32* %remainder2, align 4, !tbaa !8
  %shl = shl i32 %12, 8
  store i32 %shl, i32* %remainder2, align 4, !tbaa !8
  %13 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %table = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %13, i32 0, i32 3
  %14 = load i8, i8* %index, align 1, !tbaa !13
  %idxprom = zext i8 %14 to i32
  %arrayidx3 = getelementptr inbounds [256 x i32], [256 x i32]* %table, i32 0, i32 %idxprom
  %15 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %16 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %remainder4 = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %16, i32 0, i32 0
  %17 = load i32, i32* %remainder4, align 4, !tbaa !8
  %xor5 = xor i32 %17, %15
  store i32 %xor5, i32* %remainder4, align 4, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %index) #2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal i32 @crc_calculator_get_crc(%struct._crc_calculator* %p_crc_calculator) #0 {
entry:
  %p_crc_calculator.addr = alloca %struct._crc_calculator*, align 4
  store %struct._crc_calculator* %p_crc_calculator, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %0 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %remainder = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %0, i32 0, i32 0
  %1 = load i32, i32* %remainder, align 4, !tbaa !8
  %2 = load %struct._crc_calculator*, %struct._crc_calculator** %p_crc_calculator.addr, align 4, !tbaa !2
  %final_result_mask = getelementptr inbounds %struct._crc_calculator, %struct._crc_calculator* %2, i32 0, i32 4
  %3 = load i32, i32* %final_result_mask, align 4, !tbaa !12
  %and = and i32 %1, %3
  ret i32 %and
}

; Function Attrs: nounwind
define hidden void @av1_crc32c_calculator_init(%struct._CRC32C* %p_crc32c) #0 {
entry:
  %p_crc32c.addr = alloca %struct._CRC32C*, align 4
  %crc = alloca i32, align 4
  %n = alloca i32, align 4
  %n66 = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct._CRC32C* %p_crc32c, %struct._CRC32C** %p_crc32c.addr, align 4, !tbaa !2
  %0 = bitcast i32* %crc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  store i32 0, i32* %n, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %n, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 256
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %n, align 4, !tbaa !6
  store i32 %4, i32* %crc, align 4, !tbaa !6
  %5 = load i32, i32* %crc, align 4, !tbaa !6
  %and = and i32 %5, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %6 = load i32, i32* %crc, align 4, !tbaa !6
  %shr = lshr i32 %6, 1
  %xor = xor i32 %shr, -2097792136
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %7 = load i32, i32* %crc, align 4, !tbaa !6
  %shr1 = lshr i32 %7, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %xor, %cond.true ], [ %shr1, %cond.false ]
  store i32 %cond, i32* %crc, align 4, !tbaa !6
  %8 = load i32, i32* %crc, align 4, !tbaa !6
  %and2 = and i32 %8, 1
  %tobool3 = icmp ne i32 %and2, 0
  br i1 %tobool3, label %cond.true4, label %cond.false7

cond.true4:                                       ; preds = %cond.end
  %9 = load i32, i32* %crc, align 4, !tbaa !6
  %shr5 = lshr i32 %9, 1
  %xor6 = xor i32 %shr5, -2097792136
  br label %cond.end9

cond.false7:                                      ; preds = %cond.end
  %10 = load i32, i32* %crc, align 4, !tbaa !6
  %shr8 = lshr i32 %10, 1
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false7, %cond.true4
  %cond10 = phi i32 [ %xor6, %cond.true4 ], [ %shr8, %cond.false7 ]
  store i32 %cond10, i32* %crc, align 4, !tbaa !6
  %11 = load i32, i32* %crc, align 4, !tbaa !6
  %and11 = and i32 %11, 1
  %tobool12 = icmp ne i32 %and11, 0
  br i1 %tobool12, label %cond.true13, label %cond.false16

cond.true13:                                      ; preds = %cond.end9
  %12 = load i32, i32* %crc, align 4, !tbaa !6
  %shr14 = lshr i32 %12, 1
  %xor15 = xor i32 %shr14, -2097792136
  br label %cond.end18

cond.false16:                                     ; preds = %cond.end9
  %13 = load i32, i32* %crc, align 4, !tbaa !6
  %shr17 = lshr i32 %13, 1
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false16, %cond.true13
  %cond19 = phi i32 [ %xor15, %cond.true13 ], [ %shr17, %cond.false16 ]
  store i32 %cond19, i32* %crc, align 4, !tbaa !6
  %14 = load i32, i32* %crc, align 4, !tbaa !6
  %and20 = and i32 %14, 1
  %tobool21 = icmp ne i32 %and20, 0
  br i1 %tobool21, label %cond.true22, label %cond.false25

cond.true22:                                      ; preds = %cond.end18
  %15 = load i32, i32* %crc, align 4, !tbaa !6
  %shr23 = lshr i32 %15, 1
  %xor24 = xor i32 %shr23, -2097792136
  br label %cond.end27

cond.false25:                                     ; preds = %cond.end18
  %16 = load i32, i32* %crc, align 4, !tbaa !6
  %shr26 = lshr i32 %16, 1
  br label %cond.end27

cond.end27:                                       ; preds = %cond.false25, %cond.true22
  %cond28 = phi i32 [ %xor24, %cond.true22 ], [ %shr26, %cond.false25 ]
  store i32 %cond28, i32* %crc, align 4, !tbaa !6
  %17 = load i32, i32* %crc, align 4, !tbaa !6
  %and29 = and i32 %17, 1
  %tobool30 = icmp ne i32 %and29, 0
  br i1 %tobool30, label %cond.true31, label %cond.false34

cond.true31:                                      ; preds = %cond.end27
  %18 = load i32, i32* %crc, align 4, !tbaa !6
  %shr32 = lshr i32 %18, 1
  %xor33 = xor i32 %shr32, -2097792136
  br label %cond.end36

cond.false34:                                     ; preds = %cond.end27
  %19 = load i32, i32* %crc, align 4, !tbaa !6
  %shr35 = lshr i32 %19, 1
  br label %cond.end36

cond.end36:                                       ; preds = %cond.false34, %cond.true31
  %cond37 = phi i32 [ %xor33, %cond.true31 ], [ %shr35, %cond.false34 ]
  store i32 %cond37, i32* %crc, align 4, !tbaa !6
  %20 = load i32, i32* %crc, align 4, !tbaa !6
  %and38 = and i32 %20, 1
  %tobool39 = icmp ne i32 %and38, 0
  br i1 %tobool39, label %cond.true40, label %cond.false43

cond.true40:                                      ; preds = %cond.end36
  %21 = load i32, i32* %crc, align 4, !tbaa !6
  %shr41 = lshr i32 %21, 1
  %xor42 = xor i32 %shr41, -2097792136
  br label %cond.end45

cond.false43:                                     ; preds = %cond.end36
  %22 = load i32, i32* %crc, align 4, !tbaa !6
  %shr44 = lshr i32 %22, 1
  br label %cond.end45

cond.end45:                                       ; preds = %cond.false43, %cond.true40
  %cond46 = phi i32 [ %xor42, %cond.true40 ], [ %shr44, %cond.false43 ]
  store i32 %cond46, i32* %crc, align 4, !tbaa !6
  %23 = load i32, i32* %crc, align 4, !tbaa !6
  %and47 = and i32 %23, 1
  %tobool48 = icmp ne i32 %and47, 0
  br i1 %tobool48, label %cond.true49, label %cond.false52

cond.true49:                                      ; preds = %cond.end45
  %24 = load i32, i32* %crc, align 4, !tbaa !6
  %shr50 = lshr i32 %24, 1
  %xor51 = xor i32 %shr50, -2097792136
  br label %cond.end54

cond.false52:                                     ; preds = %cond.end45
  %25 = load i32, i32* %crc, align 4, !tbaa !6
  %shr53 = lshr i32 %25, 1
  br label %cond.end54

cond.end54:                                       ; preds = %cond.false52, %cond.true49
  %cond55 = phi i32 [ %xor51, %cond.true49 ], [ %shr53, %cond.false52 ]
  store i32 %cond55, i32* %crc, align 4, !tbaa !6
  %26 = load i32, i32* %crc, align 4, !tbaa !6
  %and56 = and i32 %26, 1
  %tobool57 = icmp ne i32 %and56, 0
  br i1 %tobool57, label %cond.true58, label %cond.false61

cond.true58:                                      ; preds = %cond.end54
  %27 = load i32, i32* %crc, align 4, !tbaa !6
  %shr59 = lshr i32 %27, 1
  %xor60 = xor i32 %shr59, -2097792136
  br label %cond.end63

cond.false61:                                     ; preds = %cond.end54
  %28 = load i32, i32* %crc, align 4, !tbaa !6
  %shr62 = lshr i32 %28, 1
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false61, %cond.true58
  %cond64 = phi i32 [ %xor60, %cond.true58 ], [ %shr62, %cond.false61 ]
  store i32 %cond64, i32* %crc, align 4, !tbaa !6
  %29 = load i32, i32* %crc, align 4, !tbaa !6
  %30 = load %struct._CRC32C*, %struct._CRC32C** %p_crc32c.addr, align 4, !tbaa !2
  %table = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %30, i32 0, i32 0
  %arrayidx = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table, i32 0, i32 0
  %31 = load i32, i32* %n, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx, i32 0, i32 %31
  store i32 %29, i32* %arrayidx65, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %cond.end63
  %32 = load i32, i32* %n, align 4, !tbaa !6
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %n, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %33 = bitcast i32* %n66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #2
  store i32 0, i32* %n66, align 4, !tbaa !6
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc90, %for.end
  %34 = load i32, i32* %n66, align 4, !tbaa !6
  %cmp68 = icmp slt i32 %34, 256
  br i1 %cmp68, label %for.body70, label %for.cond.cleanup69

for.cond.cleanup69:                               ; preds = %for.cond67
  store i32 5, i32* %cleanup.dest.slot, align 4
  %35 = bitcast i32* %n66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #2
  br label %for.end92

for.body70:                                       ; preds = %for.cond67
  %36 = load %struct._CRC32C*, %struct._CRC32C** %p_crc32c.addr, align 4, !tbaa !2
  %table71 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %36, i32 0, i32 0
  %arrayidx72 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table71, i32 0, i32 0
  %37 = load i32, i32* %n66, align 4, !tbaa !6
  %arrayidx73 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx72, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx73, align 4, !tbaa !6
  store i32 %38, i32* %crc, align 4, !tbaa !6
  %39 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #2
  store i32 1, i32* %k, align 4, !tbaa !6
  br label %for.cond74

for.cond74:                                       ; preds = %for.inc87, %for.body70
  %40 = load i32, i32* %k, align 4, !tbaa !6
  %cmp75 = icmp slt i32 %40, 8
  br i1 %cmp75, label %for.body77, label %for.cond.cleanup76

for.cond.cleanup76:                               ; preds = %for.cond74
  store i32 8, i32* %cleanup.dest.slot, align 4
  %41 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #2
  br label %for.end89

for.body77:                                       ; preds = %for.cond74
  %42 = load %struct._CRC32C*, %struct._CRC32C** %p_crc32c.addr, align 4, !tbaa !2
  %table78 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %42, i32 0, i32 0
  %arrayidx79 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table78, i32 0, i32 0
  %43 = load i32, i32* %crc, align 4, !tbaa !6
  %and80 = and i32 %43, 255
  %arrayidx81 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx79, i32 0, i32 %and80
  %44 = load i32, i32* %arrayidx81, align 4, !tbaa !6
  %45 = load i32, i32* %crc, align 4, !tbaa !6
  %shr82 = lshr i32 %45, 8
  %xor83 = xor i32 %44, %shr82
  store i32 %xor83, i32* %crc, align 4, !tbaa !6
  %46 = load i32, i32* %crc, align 4, !tbaa !6
  %47 = load %struct._CRC32C*, %struct._CRC32C** %p_crc32c.addr, align 4, !tbaa !2
  %table84 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %47, i32 0, i32 0
  %48 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx85 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table84, i32 0, i32 %48
  %49 = load i32, i32* %n66, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx85, i32 0, i32 %49
  store i32 %46, i32* %arrayidx86, align 4, !tbaa !6
  br label %for.inc87

for.inc87:                                        ; preds = %for.body77
  %50 = load i32, i32* %k, align 4, !tbaa !6
  %inc88 = add nsw i32 %50, 1
  store i32 %inc88, i32* %k, align 4, !tbaa !6
  br label %for.cond74

for.end89:                                        ; preds = %for.cond.cleanup76
  br label %for.inc90

for.inc90:                                        ; preds = %for.end89
  %51 = load i32, i32* %n66, align 4, !tbaa !6
  %inc91 = add nsw i32 %51, 1
  store i32 %inc91, i32* %n66, align 4, !tbaa !6
  br label %for.cond67

for.end92:                                        ; preds = %for.cond.cleanup69
  %52 = bitcast i32* %crc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @av1_get_crc32c_value_c(i8* %c, i8* %buf, i32 %len) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %buf.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %next = alloca i8*, align 4
  %crc = alloca i64, align 8
  %p = alloca %struct._CRC32C*, align 4
  store i8* %c, i8** %c.addr, align 4, !tbaa !2
  store i8* %buf, i8** %buf.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !14
  %0 = bitcast i8** %next to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i8*, i8** %buf.addr, align 4, !tbaa !2
  store i8* %1, i8** %next, align 4, !tbaa !2
  %2 = bitcast i64* %crc to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #2
  %3 = bitcast %struct._CRC32C** %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = load i8*, i8** %c.addr, align 4, !tbaa !2
  %5 = bitcast i8* %4 to %struct._CRC32C*
  store %struct._CRC32C* %5, %struct._CRC32C** %p, align 4, !tbaa !2
  store i64 4294967295, i64* %crc, align 8, !tbaa !16
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %6 = load i32, i32* %len.addr, align 4, !tbaa !14
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %7 = load i8*, i8** %next, align 4, !tbaa !2
  %8 = ptrtoint i8* %7 to i32
  %and = and i32 %8, 7
  %cmp = icmp ne i32 %and, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %9 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %9, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %10 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %10, i32 0, i32 0
  %arrayidx = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table, i32 0, i32 0
  %11 = load i64, i64* %crc, align 8, !tbaa !16
  %12 = load i8*, i8** %next, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %12, i32 1
  store i8* %incdec.ptr, i8** %next, align 4, !tbaa !2
  %13 = load i8, i8* %12, align 1, !tbaa !13
  %conv = zext i8 %13 to i64
  %xor = xor i64 %11, %conv
  %and1 = and i64 %xor, 255
  %idxprom = trunc i64 %and1 to i32
  %arrayidx2 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx, i32 0, i32 %idxprom
  %14 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  %conv3 = zext i32 %14 to i64
  %15 = load i64, i64* %crc, align 8, !tbaa !16
  %shr = lshr i64 %15, 8
  %xor4 = xor i64 %conv3, %shr
  store i64 %xor4, i64* %crc, align 8, !tbaa !16
  %16 = load i32, i32* %len.addr, align 4, !tbaa !14
  %dec = add i32 %16, -1
  store i32 %dec, i32* %len.addr, align 4, !tbaa !14
  br label %while.cond

while.end:                                        ; preds = %land.end
  br label %while.cond5

while.cond5:                                      ; preds = %while.body8, %while.end
  %17 = load i32, i32* %len.addr, align 4, !tbaa !14
  %cmp6 = icmp uge i32 %17, 8
  br i1 %cmp6, label %while.body8, label %while.end64

while.body8:                                      ; preds = %while.cond5
  %18 = load i8*, i8** %next, align 4, !tbaa !2
  %19 = bitcast i8* %18 to i64*
  %20 = load i64, i64* %19, align 8, !tbaa !16
  %21 = load i64, i64* %crc, align 8, !tbaa !16
  %xor9 = xor i64 %21, %20
  store i64 %xor9, i64* %crc, align 8, !tbaa !16
  %22 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table10 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %22, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table10, i32 0, i32 7
  %23 = load i64, i64* %crc, align 8, !tbaa !16
  %and12 = and i64 %23, 255
  %idxprom13 = trunc i64 %and12 to i32
  %arrayidx14 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx11, i32 0, i32 %idxprom13
  %24 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  %25 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table15 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %25, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table15, i32 0, i32 6
  %26 = load i64, i64* %crc, align 8, !tbaa !16
  %shr17 = lshr i64 %26, 8
  %and18 = and i64 %shr17, 255
  %idxprom19 = trunc i64 %and18 to i32
  %arrayidx20 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx16, i32 0, i32 %idxprom19
  %27 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %xor21 = xor i32 %24, %27
  %28 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table22 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %28, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table22, i32 0, i32 5
  %29 = load i64, i64* %crc, align 8, !tbaa !16
  %shr24 = lshr i64 %29, 16
  %and25 = and i64 %shr24, 255
  %idxprom26 = trunc i64 %and25 to i32
  %arrayidx27 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx23, i32 0, i32 %idxprom26
  %30 = load i32, i32* %arrayidx27, align 4, !tbaa !6
  %xor28 = xor i32 %xor21, %30
  %31 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table29 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %31, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table29, i32 0, i32 4
  %32 = load i64, i64* %crc, align 8, !tbaa !16
  %shr31 = lshr i64 %32, 24
  %and32 = and i64 %shr31, 255
  %idxprom33 = trunc i64 %and32 to i32
  %arrayidx34 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx30, i32 0, i32 %idxprom33
  %33 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %xor35 = xor i32 %xor28, %33
  %34 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table36 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %34, i32 0, i32 0
  %arrayidx37 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table36, i32 0, i32 3
  %35 = load i64, i64* %crc, align 8, !tbaa !16
  %shr38 = lshr i64 %35, 32
  %and39 = and i64 %shr38, 255
  %idxprom40 = trunc i64 %and39 to i32
  %arrayidx41 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx37, i32 0, i32 %idxprom40
  %36 = load i32, i32* %arrayidx41, align 4, !tbaa !6
  %xor42 = xor i32 %xor35, %36
  %37 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table43 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %37, i32 0, i32 0
  %arrayidx44 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table43, i32 0, i32 2
  %38 = load i64, i64* %crc, align 8, !tbaa !16
  %shr45 = lshr i64 %38, 40
  %and46 = and i64 %shr45, 255
  %idxprom47 = trunc i64 %and46 to i32
  %arrayidx48 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx44, i32 0, i32 %idxprom47
  %39 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %xor49 = xor i32 %xor42, %39
  %40 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table50 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %40, i32 0, i32 0
  %arrayidx51 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table50, i32 0, i32 1
  %41 = load i64, i64* %crc, align 8, !tbaa !16
  %shr52 = lshr i64 %41, 48
  %and53 = and i64 %shr52, 255
  %idxprom54 = trunc i64 %and53 to i32
  %arrayidx55 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx51, i32 0, i32 %idxprom54
  %42 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %xor56 = xor i32 %xor49, %42
  %43 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table57 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %43, i32 0, i32 0
  %arrayidx58 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table57, i32 0, i32 0
  %44 = load i64, i64* %crc, align 8, !tbaa !16
  %shr59 = lshr i64 %44, 56
  %idxprom60 = trunc i64 %shr59 to i32
  %arrayidx61 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx58, i32 0, i32 %idxprom60
  %45 = load i32, i32* %arrayidx61, align 4, !tbaa !6
  %xor62 = xor i32 %xor56, %45
  %conv63 = zext i32 %xor62 to i64
  store i64 %conv63, i64* %crc, align 8, !tbaa !16
  %46 = load i8*, i8** %next, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %46, i32 8
  store i8* %add.ptr, i8** %next, align 4, !tbaa !2
  %47 = load i32, i32* %len.addr, align 4, !tbaa !14
  %sub = sub i32 %47, 8
  store i32 %sub, i32* %len.addr, align 4, !tbaa !14
  br label %while.cond5

while.end64:                                      ; preds = %while.cond5
  br label %while.cond65

while.cond65:                                     ; preds = %while.body67, %while.end64
  %48 = load i32, i32* %len.addr, align 4, !tbaa !14
  %tobool66 = icmp ne i32 %48, 0
  br i1 %tobool66, label %while.body67, label %while.end80

while.body67:                                     ; preds = %while.cond65
  %49 = load %struct._CRC32C*, %struct._CRC32C** %p, align 4, !tbaa !2
  %table68 = getelementptr inbounds %struct._CRC32C, %struct._CRC32C* %49, i32 0, i32 0
  %arrayidx69 = getelementptr inbounds [8 x [256 x i32]], [8 x [256 x i32]]* %table68, i32 0, i32 0
  %50 = load i64, i64* %crc, align 8, !tbaa !16
  %51 = load i8*, i8** %next, align 4, !tbaa !2
  %incdec.ptr70 = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr70, i8** %next, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !13
  %conv71 = zext i8 %52 to i64
  %xor72 = xor i64 %50, %conv71
  %and73 = and i64 %xor72, 255
  %idxprom74 = trunc i64 %and73 to i32
  %arrayidx75 = getelementptr inbounds [256 x i32], [256 x i32]* %arrayidx69, i32 0, i32 %idxprom74
  %53 = load i32, i32* %arrayidx75, align 4, !tbaa !6
  %conv76 = zext i32 %53 to i64
  %54 = load i64, i64* %crc, align 8, !tbaa !16
  %shr77 = lshr i64 %54, 8
  %xor78 = xor i64 %conv76, %shr77
  store i64 %xor78, i64* %crc, align 8, !tbaa !16
  %55 = load i32, i32* %len.addr, align 4, !tbaa !14
  %dec79 = add i32 %55, -1
  store i32 %dec79, i32* %len.addr, align 4, !tbaa !14
  br label %while.cond65

while.end80:                                      ; preds = %while.cond65
  %56 = load i64, i64* %crc, align 8, !tbaa !16
  %conv81 = trunc i64 %56 to i32
  %xor82 = xor i32 %conv81, -1
  %57 = bitcast %struct._CRC32C** %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #2
  %58 = bitcast i64* %crc to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %58) #2
  %59 = bitcast i8** %next to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #2
  ret i32 %xor82
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 0}
!9 = !{!"_crc_calculator", !7, i64 0, !7, i64 4, !7, i64 8, !4, i64 12, !7, i64 1036}
!10 = !{!9, !7, i64 8}
!11 = !{!9, !7, i64 4}
!12 = !{!9, !7, i64 1036}
!13 = !{!4, !4, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"long", !4, i64 0}
!16 = !{!17, !17, i64 0}
!17 = !{!"long long", !4, i64 0}
