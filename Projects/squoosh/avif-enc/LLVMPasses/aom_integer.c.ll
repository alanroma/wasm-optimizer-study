; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_integer.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_integer.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden i32 @aom_uleb_size_in_bytes(i64 %value) #0 {
entry:
  %value.addr = alloca i64, align 8
  %size = alloca i32, align 4
  store i64 %value, i64* %value.addr, align 8, !tbaa !2
  %0 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 0, i32* %size, align 4, !tbaa !6
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %1 = load i32, i32* %size, align 4, !tbaa !6
  %inc = add i32 %1, 1
  store i32 %inc, i32* %size, align 4, !tbaa !6
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %2 = load i64, i64* %value.addr, align 8, !tbaa !2
  %shr = lshr i64 %2, 7
  store i64 %shr, i64* %value.addr, align 8, !tbaa !2
  %cmp = icmp ne i64 %shr, 0
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %3 = load i32, i32* %size, align 4, !tbaa !6
  %4 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #2
  ret i32 %3
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @aom_uleb_decode(i8* %buffer, i32 %available, i64* %value, i32* %length) #0 {
entry:
  %retval = alloca i32, align 4
  %buffer.addr = alloca i8*, align 4
  %available.addr = alloca i32, align 4
  %value.addr = alloca i64*, align 4
  %length.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %decoded_byte = alloca i8, align 1
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !8
  store i32 %available, i32* %available.addr, align 4, !tbaa !6
  store i64* %value, i64** %value.addr, align 4, !tbaa !8
  store i32* %length, i32** %length.addr, align 4, !tbaa !8
  %0 = load i8*, i8** %buffer.addr, align 4, !tbaa !8
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end19

land.lhs.true:                                    ; preds = %entry
  %1 = load i64*, i64** %value.addr, align 4, !tbaa !8
  %tobool1 = icmp ne i64* %1, null
  br i1 %tobool1, label %if.then, label %if.end19

if.then:                                          ; preds = %land.lhs.true
  %2 = load i64*, i64** %value.addr, align 4, !tbaa !8
  store i64 0, i64* %2, align 8, !tbaa !2
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp ult i32 %4, 8
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %available.addr, align 4, !tbaa !6
  %cmp2 = icmp ult i32 %5, %6
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %7 = phi i1 [ false, %for.cond ], [ %cmp2, %land.rhs ]
  br i1 %7, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %land.end
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

for.body:                                         ; preds = %land.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %decoded_byte) #2
  %8 = load i8*, i8** %buffer.addr, align 4, !tbaa !8
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %9
  %10 = load i8, i8* %add.ptr, align 1, !tbaa !10
  %conv = zext i8 %10 to i32
  %and = and i32 %conv, 127
  %conv3 = trunc i32 %and to i8
  store i8 %conv3, i8* %decoded_byte, align 1, !tbaa !10
  %11 = load i8, i8* %decoded_byte, align 1, !tbaa !10
  %conv4 = zext i8 %11 to i64
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %mul = mul i32 %12, 7
  %sh_prom = zext i32 %mul to i64
  %shl = shl i64 %conv4, %sh_prom
  %13 = load i64*, i64** %value.addr, align 4, !tbaa !8
  %14 = load i64, i64* %13, align 8, !tbaa !2
  %or = or i64 %14, %shl
  store i64 %or, i64* %13, align 8, !tbaa !2
  %15 = load i8*, i8** %buffer.addr, align 4, !tbaa !8
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr5 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %add.ptr5, align 1, !tbaa !10
  %conv6 = zext i8 %17 to i32
  %shr = ashr i32 %conv6, 7
  %cmp7 = icmp eq i32 %shr, 0
  br i1 %cmp7, label %if.then9, label %if.end16

if.then9:                                         ; preds = %for.body
  %18 = load i32*, i32** %length.addr, align 4, !tbaa !8
  %tobool10 = icmp ne i32* %18, null
  br i1 %tobool10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then9
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %add = add i32 %19, 1
  %20 = load i32*, i32** %length.addr, align 4, !tbaa !8
  store i32 %add, i32* %20, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then9
  %21 = load i64*, i64** %value.addr, align 4, !tbaa !8
  %22 = load i64, i64* %21, align 8, !tbaa !2
  %cmp12 = icmp ugt i64 %22, 4294967295
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.end15, %if.then14
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %decoded_byte) #2
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup17 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup17:                                        ; preds = %cleanup, %for.cond.cleanup
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #2
  %cleanup.dest18 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest18, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup17
  br label %if.end19

if.end19:                                         ; preds = %for.end, %land.lhs.true, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end19, %cleanup17
  %25 = load i32, i32* %retval, align 4
  ret i32 %25

unreachable:                                      ; preds = %cleanup17
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @aom_uleb_encode(i64 %value, i32 %available, i8* %coded_value, i32* %coded_size) #0 {
entry:
  %retval = alloca i32, align 4
  %value.addr = alloca i64, align 8
  %available.addr = alloca i32, align 4
  %coded_value.addr = alloca i8*, align 4
  %coded_size.addr = alloca i32*, align 4
  %leb_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %byte = alloca i8, align 1
  store i64 %value, i64* %value.addr, align 8, !tbaa !2
  store i32 %available, i32* %available.addr, align 4, !tbaa !6
  store i8* %coded_value, i8** %coded_value.addr, align 4, !tbaa !8
  store i32* %coded_size, i32** %coded_size.addr, align 4, !tbaa !8
  %0 = bitcast i32* %leb_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load i64, i64* %value.addr, align 8, !tbaa !2
  %call = call i32 @aom_uleb_size_in_bytes(i64 %1)
  store i32 %call, i32* %leb_size, align 4, !tbaa !6
  %2 = load i64, i64* %value.addr, align 8, !tbaa !2
  %cmp = icmp ugt i64 %2, 4294967295
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load i32, i32* %leb_size, align 4, !tbaa !6
  %cmp1 = icmp ugt i32 %3, 8
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %4 = load i32, i32* %leb_size, align 4, !tbaa !6
  %5 = load i32, i32* %available.addr, align 4, !tbaa !6
  %cmp3 = icmp ugt i32 %4, %5
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %6 = load i8*, i8** %coded_value.addr, align 4, !tbaa !8
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %lor.lhs.false5, label %if.then

lor.lhs.false5:                                   ; preds = %lor.lhs.false4
  %7 = load i32*, i32** %coded_size.addr, align 4, !tbaa !8
  %tobool6 = icmp ne i32* %7, null
  br i1 %tobool6, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false5, %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false5
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load i32, i32* %leb_size, align 4, !tbaa !6
  %cmp7 = icmp ult i32 %9, %10
  br i1 %cmp7, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %byte) #2
  %12 = load i64, i64* %value.addr, align 8, !tbaa !2
  %and = and i64 %12, 127
  %conv = trunc i64 %and to i8
  store i8 %conv, i8* %byte, align 1, !tbaa !10
  %13 = load i64, i64* %value.addr, align 8, !tbaa !2
  %shr = lshr i64 %13, 7
  store i64 %shr, i64* %value.addr, align 8, !tbaa !2
  %14 = load i64, i64* %value.addr, align 8, !tbaa !2
  %cmp8 = icmp ne i64 %14, 0
  br i1 %cmp8, label %if.then10, label %if.end13

if.then10:                                        ; preds = %for.body
  %15 = load i8, i8* %byte, align 1, !tbaa !10
  %conv11 = zext i8 %15 to i32
  %or = or i32 %conv11, 128
  %conv12 = trunc i32 %or to i8
  store i8 %conv12, i8* %byte, align 1, !tbaa !10
  br label %if.end13

if.end13:                                         ; preds = %if.then10, %for.body
  %16 = load i8, i8* %byte, align 1, !tbaa !10
  %17 = load i8*, i8** %coded_value.addr, align 4, !tbaa !8
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %17, i32 %18
  store i8 %16, i8* %add.ptr, align 1, !tbaa !10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %byte) #2
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = load i32, i32* %leb_size, align 4, !tbaa !6
  %21 = load i32*, i32** %coded_size.addr, align 4, !tbaa !8
  store i32 %20, i32* %21, align 4, !tbaa !6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then
  %22 = bitcast i32* %leb_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #2
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: nounwind
define hidden i32 @aom_uleb_encode_fixed_size(i64 %value, i32 %available, i32 %pad_to_size, i8* %coded_value, i32* %coded_size) #0 {
entry:
  %retval = alloca i32, align 4
  %value.addr = alloca i64, align 8
  %available.addr = alloca i32, align 4
  %pad_to_size.addr = alloca i32, align 4
  %coded_value.addr = alloca i8*, align 4
  %coded_size.addr = alloca i32*, align 4
  %limit = alloca i64, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %byte = alloca i8, align 1
  store i64 %value, i64* %value.addr, align 8, !tbaa !2
  store i32 %available, i32* %available.addr, align 4, !tbaa !6
  store i32 %pad_to_size, i32* %pad_to_size.addr, align 4, !tbaa !6
  store i8* %coded_value, i8** %coded_value.addr, align 4, !tbaa !8
  store i32* %coded_size, i32** %coded_size.addr, align 4, !tbaa !8
  %0 = load i64, i64* %value.addr, align 8, !tbaa !2
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %coded_value.addr, align 4, !tbaa !8
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %lor.lhs.false1, label %if.then

lor.lhs.false1:                                   ; preds = %lor.lhs.false
  %2 = load i32*, i32** %coded_size.addr, align 4, !tbaa !8
  %tobool2 = icmp ne i32* %2, null
  br i1 %tobool2, label %lor.lhs.false3, label %if.then

lor.lhs.false3:                                   ; preds = %lor.lhs.false1
  %3 = load i32, i32* %available.addr, align 4, !tbaa !6
  %4 = load i32, i32* %pad_to_size.addr, align 4, !tbaa !6
  %cmp4 = icmp ult i32 %3, %4
  br i1 %cmp4, label %if.then, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %lor.lhs.false3
  %5 = load i32, i32* %pad_to_size.addr, align 4, !tbaa !6
  %cmp6 = icmp ugt i32 %5, 8
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false5, %lor.lhs.false3, %lor.lhs.false1, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false5
  %6 = bitcast i64* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #2
  %7 = load i32, i32* %pad_to_size.addr, align 4, !tbaa !6
  %mul = mul i32 7, %7
  %sh_prom = zext i32 %mul to i64
  %shl = shl i64 1, %sh_prom
  store i64 %shl, i64* %limit, align 8, !tbaa !2
  %8 = load i64, i64* %value.addr, align 8, !tbaa !2
  %9 = load i64, i64* %limit, align 8, !tbaa !2
  %cmp7 = icmp uge i64 %8, %9
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end9
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %pad_to_size.addr, align 4, !tbaa !6
  %cmp10 = icmp ult i32 %11, %12
  br i1 %cmp10, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %byte) #2
  %14 = load i64, i64* %value.addr, align 8, !tbaa !2
  %and = and i64 %14, 127
  %conv = trunc i64 %and to i8
  store i8 %conv, i8* %byte, align 1, !tbaa !10
  %15 = load i64, i64* %value.addr, align 8, !tbaa !2
  %shr = lshr i64 %15, 7
  store i64 %shr, i64* %value.addr, align 8, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load i32, i32* %pad_to_size.addr, align 4, !tbaa !6
  %sub = sub i32 %17, 1
  %cmp11 = icmp ult i32 %16, %sub
  br i1 %cmp11, label %if.then13, label %if.end16

if.then13:                                        ; preds = %for.body
  %18 = load i8, i8* %byte, align 1, !tbaa !10
  %conv14 = zext i8 %18 to i32
  %or = or i32 %conv14, 128
  %conv15 = trunc i32 %or to i8
  store i8 %conv15, i8* %byte, align 1, !tbaa !10
  br label %if.end16

if.end16:                                         ; preds = %if.then13, %for.body
  %19 = load i8, i8* %byte, align 1, !tbaa !10
  %20 = load i8*, i8** %coded_value.addr, align 4, !tbaa !8
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %20, i32 %21
  store i8 %19, i8* %add.ptr, align 1, !tbaa !10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %byte) #2
  br label %for.inc

for.inc:                                          ; preds = %if.end16
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %23 = load i32, i32* %pad_to_size.addr, align 4, !tbaa !6
  %24 = load i32*, i32** %coded_size.addr, align 4, !tbaa !8
  store i32 %23, i32* %24, align 4, !tbaa !6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then8
  %25 = bitcast i64* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %25) #2
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"long long", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"any pointer", !4, i64 0}
!10 = !{!4, !4, i64 0}
