; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_util/aom_thread.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_util/aom_thread.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.AVxWorkerInterface = type { void (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)* }
%struct.AVxWorker = type { %struct.AVxWorkerImpl*, i32, i8*, i32 (i8*, i8*)*, i8*, i8*, i32 }
%struct.AVxWorkerImpl = type opaque

@g_worker_interface = internal global %struct.AVxWorkerInterface { void (%struct.AVxWorker*)* @init, i32 (%struct.AVxWorker*)* @reset, i32 (%struct.AVxWorker*)* @sync, void (%struct.AVxWorker*)* @launch, void (%struct.AVxWorker*)* @execute, void (%struct.AVxWorker*)* @end }, align 4

; Function Attrs: nounwind
define hidden i32 @aom_set_worker_interface(%struct.AVxWorkerInterface* %winterface) #0 {
entry:
  %retval = alloca i32, align 4
  %winterface.addr = alloca %struct.AVxWorkerInterface*, align 4
  store %struct.AVxWorkerInterface* %winterface, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %cmp = icmp eq %struct.AVxWorkerInterface* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %init = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %1, i32 0, i32 0
  %2 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %init, align 4, !tbaa !6
  %cmp1 = icmp eq void (%struct.AVxWorker*)* %2, null
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %3 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %reset = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %3, i32 0, i32 1
  %4 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %reset, align 4, !tbaa !8
  %cmp3 = icmp eq i32 (%struct.AVxWorker*)* %4, null
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %5 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %sync = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %5, i32 0, i32 2
  %6 = load i32 (%struct.AVxWorker*)*, i32 (%struct.AVxWorker*)** %sync, align 4, !tbaa !9
  %cmp5 = icmp eq i32 (%struct.AVxWorker*)* %6, null
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false4
  %7 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %launch = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %7, i32 0, i32 3
  %8 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %launch, align 4, !tbaa !10
  %cmp7 = icmp eq void (%struct.AVxWorker*)* %8, null
  br i1 %cmp7, label %if.then, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %lor.lhs.false6
  %9 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %execute = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %9, i32 0, i32 4
  %10 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %execute, align 4, !tbaa !11
  %cmp9 = icmp eq void (%struct.AVxWorker*)* %10, null
  br i1 %cmp9, label %if.then, label %lor.lhs.false10

lor.lhs.false10:                                  ; preds = %lor.lhs.false8
  %11 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %end = getelementptr inbounds %struct.AVxWorkerInterface, %struct.AVxWorkerInterface* %11, i32 0, i32 5
  %12 = load void (%struct.AVxWorker*)*, void (%struct.AVxWorker*)** %end, align 4, !tbaa !12
  %cmp11 = icmp eq void (%struct.AVxWorker*)* %12, null
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false10, %lor.lhs.false8, %lor.lhs.false6, %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false10
  %13 = load %struct.AVxWorkerInterface*, %struct.AVxWorkerInterface** %winterface.addr, align 4, !tbaa !2
  %14 = bitcast %struct.AVxWorkerInterface* %13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 bitcast (%struct.AVxWorkerInterface* @g_worker_interface to i8*), i8* align 4 %14, i32 24, i1 false), !tbaa.struct !13
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden %struct.AVxWorkerInterface* @aom_get_worker_interface() #0 {
entry:
  ret %struct.AVxWorkerInterface* @g_worker_interface
}

; Function Attrs: nounwind
define internal void @init(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %1 = bitcast %struct.AVxWorker* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 28, i1 false)
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %2, i32 0, i32 1
  store i32 0, i32* %status_, align 4, !tbaa !14
  ret void
}

; Function Attrs: nounwind
define internal i32 @reset(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  %ok = alloca i32, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ok to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 1, i32* %ok, align 4, !tbaa !17
  %1 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %had_error = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %1, i32 0, i32 6
  store i32 0, i32* %had_error, align 4, !tbaa !18
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %2, i32 0, i32 1
  %3 = load i32, i32* %status_, align 4, !tbaa !14
  %cmp = icmp ult i32 %3, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %4, i32 0, i32 1
  store i32 1, i32* %status_1, align 4, !tbaa !14
  br label %if.end5

if.else:                                          ; preds = %entry
  %5 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_2 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %5, i32 0, i32 1
  %6 = load i32, i32* %status_2, align 4, !tbaa !14
  %cmp3 = icmp ugt i32 %6, 1
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %7 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %call = call i32 @sync(%struct.AVxWorker* %7)
  store i32 %call, i32* %ok, align 4, !tbaa !17
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  %8 = load i32, i32* %ok, align 4, !tbaa !17
  %9 = bitcast i32* %ok to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #3
  ret i32 %8
}

; Function Attrs: nounwind
define internal i32 @sync(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %had_error = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %0, i32 0, i32 6
  %1 = load i32, i32* %had_error, align 4, !tbaa !18
  %tobool = icmp ne i32 %1, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  ret i32 %lnot.ext
}

; Function Attrs: nounwind
define internal void @launch(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  call void @execute(%struct.AVxWorker* %0)
  ret void
}

; Function Attrs: nounwind
define internal void @execute(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %hook = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %0, i32 0, i32 3
  %1 = load i32 (i8*, i8*)*, i32 (i8*, i8*)** %hook, align 4, !tbaa !19
  %cmp = icmp ne i32 (i8*, i8*)* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %hook1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %2, i32 0, i32 3
  %3 = load i32 (i8*, i8*)*, i32 (i8*, i8*)** %hook1, align 4, !tbaa !19
  %4 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %data1 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %4, i32 0, i32 4
  %5 = load i8*, i8** %data1, align 4, !tbaa !20
  %6 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %data2 = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %6, i32 0, i32 5
  %7 = load i8*, i8** %data2, align 4, !tbaa !21
  %call = call i32 %3(i8* %5, i8* %7)
  %tobool = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %8 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %had_error = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %8, i32 0, i32 6
  %9 = load i32, i32* %had_error, align 4, !tbaa !18
  %or = or i32 %9, %lnot.ext
  store i32 %or, i32* %had_error, align 4, !tbaa !18
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define internal void @end(%struct.AVxWorker* %worker) #0 {
entry:
  %worker.addr = alloca %struct.AVxWorker*, align 4
  store %struct.AVxWorker* %worker, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %0 = load %struct.AVxWorker*, %struct.AVxWorker** %worker.addr, align 4, !tbaa !2
  %status_ = getelementptr inbounds %struct.AVxWorker, %struct.AVxWorker* %0, i32 0, i32 1
  store i32 0, i32* %status_, align 4, !tbaa !14
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 0}
!7 = !{!"", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20}
!8 = !{!7, !3, i64 4}
!9 = !{!7, !3, i64 8}
!10 = !{!7, !3, i64 12}
!11 = !{!7, !3, i64 16}
!12 = !{!7, !3, i64 20}
!13 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 16, i64 4, !2, i64 20, i64 4, !2}
!14 = !{!15, !4, i64 4}
!15 = !{!"", !3, i64 0, !4, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !16, i64 24}
!16 = !{!"int", !4, i64 0}
!17 = !{!16, !16, i64 0}
!18 = !{!15, !16, i64 24}
!19 = !{!15, !3, i64 12}
!20 = !{!15, !3, i64 16}
!21 = !{!15, !3, i64 20}
