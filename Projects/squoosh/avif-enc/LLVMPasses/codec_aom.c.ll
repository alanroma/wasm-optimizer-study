; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/codec_aom.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libavif-enc/src/codec_aom.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.avifCodec = type { %struct.avifCodecDecodeInput*, %struct.avifCodecConfigurationBox, %struct.avifCodecInternal*, i32 (%struct.avifCodec*, i32)*, i32 (%struct.avifCodec*, %struct.avifImage*)*, i32 (%struct.avifCodec*, %struct.avifEncoder*, %struct.avifImage*, i32, i32, %struct.avifCodecEncodeOutput*)*, i32 (%struct.avifCodec*, %struct.avifCodecEncodeOutput*)*, void (%struct.avifCodec*)* }
%struct.avifCodecDecodeInput = type { %struct.avifDecodeSampleArray, i32 }
%struct.avifDecodeSampleArray = type { %struct.avifDecodeSample*, i32, i32, i32 }
%struct.avifDecodeSample = type { %struct.avifROData, i32 }
%struct.avifROData = type { i8*, i32 }
%struct.avifCodecConfigurationBox = type { i8, i8, i8, i8, i8, i8, i8, i8, i8 }
%struct.avifCodecInternal = type { i32, %struct.aom_codec_ctx, i8*, i32, %struct.aom_image*, i32, %struct.aom_codec_ctx, %struct.avifPixelFormatInfo, i32, i32 }
%struct.aom_image = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.aom_metadata_array*, i8* }
%struct.aom_metadata_array = type opaque
%struct.aom_codec_ctx = type { i8*, %struct.aom_codec_iface*, i32, i8*, i32, %union.anon, %struct.aom_codec_priv* }
%struct.aom_codec_iface = type opaque
%union.anon = type { %struct.aom_codec_dec_cfg* }
%struct.aom_codec_dec_cfg = type { i32, i32, i32, i32 }
%struct.aom_codec_priv = type opaque
%struct.avifPixelFormatInfo = type { i32, i32, i32 }
%struct.avifImage = type { i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i8*, i32, i32, %struct.avifRWData, i32, i32, i32, i32, %struct.avifPixelAspectRatioBox, %struct.avifCleanApertureBox, %struct.avifImageRotation, %struct.avifImageMirror, %struct.avifRWData, %struct.avifRWData }
%struct.avifPixelAspectRatioBox = type { i32, i32 }
%struct.avifCleanApertureBox = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifImageRotation = type { i8 }
%struct.avifImageMirror = type { i8 }
%struct.avifRWData = type { i8*, i32 }
%struct.avifEncoder = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i64, %struct.avifIOStats, %struct.avifEncoderData* }
%struct.avifIOStats = type { i32, i32 }
%struct.avifEncoderData = type opaque
%struct.avifCodecEncodeOutput = type { %struct.avifEncodeSampleArray }
%struct.avifEncodeSampleArray = type { %struct.avifEncodeSample*, i32, i32, i32 }
%struct.avifEncodeSample = type { %struct.avifRWData, i32 }
%struct.aom_codec_enc_cfg = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_rational, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.aom_fixed_buf, %struct.aom_fixed_buf, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [64 x i32], [64 x i32], i32, [5 x i32], %struct.cfg_options }
%struct.aom_rational = type { i32, i32 }
%struct.aom_fixed_buf = type { i8*, i32 }
%struct.cfg_options = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.aom_codec_cx_pkt = type { i32, %union.anon.0 }
%union.anon.0 = type { %struct.aom_psnr_pkt, [48 x i8] }
%struct.aom_psnr_pkt = type { [4 x i32], [4 x i64], [4 x double] }
%struct.anon = type { i8*, i32, i64, i32, i32, i32, i32 }

@aomCodecEncodeImage.aomVersion_2_0_0 = internal constant i32 131072, align 4

; Function Attrs: nounwind
define hidden i8* @avifCodecVersionAOM() #0 {
entry:
  %call = call i8* @aom_codec_version_str()
  ret i8* %call
}

declare i8* @aom_codec_version_str() #1

; Function Attrs: nounwind
define hidden %struct.avifCodec* @avifCodecCreateAOM() #0 {
entry:
  %codec = alloca %struct.avifCodec*, align 4
  %0 = bitcast %struct.avifCodec** %codec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call i8* @avifAlloc(i32 40)
  %1 = bitcast i8* %call to %struct.avifCodec*
  store %struct.avifCodec* %1, %struct.avifCodec** %codec, align 4, !tbaa !2
  %2 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %3 = bitcast %struct.avifCodec* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 40, i1 false)
  %4 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %open = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %4, i32 0, i32 3
  store i32 (%struct.avifCodec*, i32)* @aomCodecOpen, i32 (%struct.avifCodec*, i32)** %open, align 4, !tbaa !6
  %5 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %getNextImage = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %5, i32 0, i32 4
  store i32 (%struct.avifCodec*, %struct.avifImage*)* @aomCodecGetNextImage, i32 (%struct.avifCodec*, %struct.avifImage*)** %getNextImage, align 4, !tbaa !9
  %6 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %encodeImage = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %6, i32 0, i32 5
  store i32 (%struct.avifCodec*, %struct.avifEncoder*, %struct.avifImage*, i32, i32, %struct.avifCodecEncodeOutput*)* @aomCodecEncodeImage, i32 (%struct.avifCodec*, %struct.avifEncoder*, %struct.avifImage*, i32, i32, %struct.avifCodecEncodeOutput*)** %encodeImage, align 4, !tbaa !10
  %7 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %encodeFinish = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %7, i32 0, i32 6
  store i32 (%struct.avifCodec*, %struct.avifCodecEncodeOutput*)* @aomCodecEncodeFinish, i32 (%struct.avifCodec*, %struct.avifCodecEncodeOutput*)** %encodeFinish, align 4, !tbaa !11
  %8 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %destroyInternal = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %8, i32 0, i32 7
  store void (%struct.avifCodec*)* @aomCodecDestroyInternal, void (%struct.avifCodec*)** %destroyInternal, align 4, !tbaa !12
  %call1 = call i8* @avifAlloc(i32 96)
  %9 = bitcast i8* %call1 to %struct.avifCodecInternal*
  %10 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %internal = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %10, i32 0, i32 2
  store %struct.avifCodecInternal* %9, %struct.avifCodecInternal** %internal, align 4, !tbaa !13
  %11 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %internal2 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %11, i32 0, i32 2
  %12 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal2, align 4, !tbaa !13
  %13 = bitcast %struct.avifCodecInternal* %12 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %13, i8 0, i32 96, i1 false)
  %14 = load %struct.avifCodec*, %struct.avifCodec** %codec, align 4, !tbaa !2
  %15 = bitcast %struct.avifCodec** %codec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  ret %struct.avifCodec* %14
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @avifAlloc(i32) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define internal i32 @aomCodecOpen(%struct.avifCodec* %codec, i32 %firstSampleIndex) #0 {
entry:
  %retval = alloca i32, align 4
  %codec.addr = alloca %struct.avifCodec*, align 4
  %firstSampleIndex.addr = alloca i32, align 4
  %decoder_interface = alloca %struct.aom_codec_iface*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifCodec* %codec, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  store i32 %firstSampleIndex, i32* %firstSampleIndex.addr, align 4, !tbaa !14
  %0 = bitcast %struct.aom_codec_iface** %decoder_interface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %call = call %struct.aom_codec_iface* @aom_codec_av1_dx()
  store %struct.aom_codec_iface* %call, %struct.aom_codec_iface** %decoder_interface, align 4, !tbaa !2
  %1 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %1, i32 0, i32 2
  %2 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal, align 4, !tbaa !13
  %decoder = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %2, i32 0, i32 1
  %3 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %decoder_interface, align 4, !tbaa !2
  %call1 = call i32 @aom_codec_dec_init_ver(%struct.aom_codec_ctx* %decoder, %struct.aom_codec_iface* %3, %struct.aom_codec_dec_cfg* null, i32 0, i32 20)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal2 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %4, i32 0, i32 2
  %5 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal2, align 4, !tbaa !13
  %decoderInitialized = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %5, i32 0, i32 0
  store i32 1, i32* %decoderInitialized, align 4, !tbaa !16
  %6 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal3 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %6, i32 0, i32 2
  %7 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal3, align 4, !tbaa !13
  %decoder4 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %7, i32 0, i32 1
  %call5 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %decoder4, i32 280, i32 1)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end
  %8 = load i32, i32* %firstSampleIndex.addr, align 4, !tbaa !14
  %9 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal9 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %9, i32 0, i32 2
  %10 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal9, align 4, !tbaa !13
  %inputSampleIndex = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %10, i32 0, i32 3
  store i32 %8, i32* %inputSampleIndex, align 4, !tbaa !21
  %11 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal10 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %11, i32 0, i32 2
  %12 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal10, align 4, !tbaa !13
  %iter = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %12, i32 0, i32 2
  store i8* null, i8** %iter, align 4, !tbaa !22
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then7, %if.then
  %13 = bitcast %struct.aom_codec_iface** %decoder_interface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: nounwind
define internal i32 @aomCodecGetNextImage(%struct.avifCodec* %codec, %struct.avifImage* %image) #0 {
entry:
  %retval = alloca i32, align 4
  %codec.addr = alloca %struct.avifCodec*, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %nextFrame = alloca %struct.aom_image*, align 4
  %sample = alloca %struct.avifDecodeSample*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %isColor = alloca i32, align 4
  %yuvFormat = alloca i32, align 4
  %formatInfo = alloca %struct.avifPixelFormatInfo, align 4
  %yuvPlaneCount = alloca i32, align 4
  %yuvPlane = alloca i32, align 4
  store %struct.avifCodec* %codec, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %0 = bitcast %struct.aom_image** %nextFrame to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store %struct.aom_image* null, %struct.aom_image** %nextFrame, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %if.end22, %entry
  %1 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %1, i32 0, i32 2
  %2 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal, align 4, !tbaa !13
  %decoder = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %2, i32 0, i32 1
  %3 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal1 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %3, i32 0, i32 2
  %4 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal1, align 4, !tbaa !13
  %iter = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %4, i32 0, i32 2
  %call = call %struct.aom_image* @aom_codec_get_frame(%struct.aom_codec_ctx* %decoder, i8** %iter)
  store %struct.aom_image* %call, %struct.aom_image** %nextFrame, align 4, !tbaa !2
  %5 = load %struct.aom_image*, %struct.aom_image** %nextFrame, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_image* %5, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.cond
  br label %for.end

if.else:                                          ; preds = %for.cond
  %6 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal2 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %6, i32 0, i32 2
  %7 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal2, align 4, !tbaa !13
  %inputSampleIndex = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %7, i32 0, i32 3
  %8 = load i32, i32* %inputSampleIndex, align 4, !tbaa !21
  %9 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %decodeInput = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %9, i32 0, i32 0
  %10 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput, align 4, !tbaa !23
  %samples = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %10, i32 0, i32 0
  %count = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples, i32 0, i32 2
  %11 = load i32, i32* %count, align 4, !tbaa !24
  %cmp = icmp ult i32 %8, %11
  br i1 %cmp, label %if.then3, label %if.else20

if.then3:                                         ; preds = %if.else
  %12 = bitcast %struct.avifDecodeSample** %sample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %decodeInput4 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %13, i32 0, i32 0
  %14 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput4, align 4, !tbaa !23
  %samples5 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %14, i32 0, i32 0
  %sample6 = getelementptr inbounds %struct.avifDecodeSampleArray, %struct.avifDecodeSampleArray* %samples5, i32 0, i32 0
  %15 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample6, align 4, !tbaa !27
  %16 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal7 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %16, i32 0, i32 2
  %17 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal7, align 4, !tbaa !13
  %inputSampleIndex8 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %17, i32 0, i32 3
  %18 = load i32, i32* %inputSampleIndex8, align 4, !tbaa !21
  %arrayidx = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %15, i32 %18
  store %struct.avifDecodeSample* %arrayidx, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %19 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal9 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %19, i32 0, i32 2
  %20 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal9, align 4, !tbaa !13
  %inputSampleIndex10 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %20, i32 0, i32 3
  %21 = load i32, i32* %inputSampleIndex10, align 4, !tbaa !21
  %inc = add i32 %21, 1
  store i32 %inc, i32* %inputSampleIndex10, align 4, !tbaa !21
  %22 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal11 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %22, i32 0, i32 2
  %23 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal11, align 4, !tbaa !13
  %iter12 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %23, i32 0, i32 2
  store i8* null, i8** %iter12, align 4, !tbaa !22
  %24 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal13 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %24, i32 0, i32 2
  %25 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal13, align 4, !tbaa !13
  %decoder14 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %25, i32 0, i32 1
  %26 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %26, i32 0, i32 0
  %data15 = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data, i32 0, i32 0
  %27 = load i8*, i8** %data15, align 4, !tbaa !28
  %28 = load %struct.avifDecodeSample*, %struct.avifDecodeSample** %sample, align 4, !tbaa !2
  %data16 = getelementptr inbounds %struct.avifDecodeSample, %struct.avifDecodeSample* %28, i32 0, i32 0
  %size = getelementptr inbounds %struct.avifROData, %struct.avifROData* %data16, i32 0, i32 1
  %29 = load i32, i32* %size, align 4, !tbaa !31
  %call17 = call i32 @aom_codec_decode(%struct.aom_codec_ctx* %decoder14, i8* %27, i32 %29, i8* null)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.then19, label %if.end

if.then19:                                        ; preds = %if.then3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then3
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then19
  %30 = bitcast %struct.avifDecodeSample** %sample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup168 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end21

if.else20:                                        ; preds = %if.else
  br label %for.end

if.end21:                                         ; preds = %cleanup.cont
  br label %if.end22

if.end22:                                         ; preds = %if.end21
  br label %for.cond

for.end:                                          ; preds = %if.else20, %if.then
  %31 = load %struct.aom_image*, %struct.aom_image** %nextFrame, align 4, !tbaa !2
  %tobool23 = icmp ne %struct.aom_image* %31, null
  br i1 %tobool23, label %if.then24, label %if.else27

if.then24:                                        ; preds = %for.end
  %32 = load %struct.aom_image*, %struct.aom_image** %nextFrame, align 4, !tbaa !2
  %33 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal25 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %33, i32 0, i32 2
  %34 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal25, align 4, !tbaa !13
  %image26 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %34, i32 0, i32 4
  store %struct.aom_image* %32, %struct.aom_image** %image26, align 4, !tbaa !32
  br label %if.end36

if.else27:                                        ; preds = %for.end
  %35 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %decodeInput28 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %35, i32 0, i32 0
  %36 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput28, align 4, !tbaa !23
  %alpha = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %36, i32 0, i32 1
  %37 = load i32, i32* %alpha, align 4, !tbaa !33
  %tobool29 = icmp ne i32 %37, 0
  br i1 %tobool29, label %land.lhs.true, label %if.else34

land.lhs.true:                                    ; preds = %if.else27
  %38 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal30 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %38, i32 0, i32 2
  %39 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal30, align 4, !tbaa !13
  %image31 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %39, i32 0, i32 4
  %40 = load %struct.aom_image*, %struct.aom_image** %image31, align 4, !tbaa !32
  %tobool32 = icmp ne %struct.aom_image* %40, null
  br i1 %tobool32, label %if.then33, label %if.else34

if.then33:                                        ; preds = %land.lhs.true
  br label %if.end35

if.else34:                                        ; preds = %land.lhs.true, %if.else27
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup168

if.end35:                                         ; preds = %if.then33
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.then24
  %41 = bitcast i32* %isColor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  %42 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %decodeInput37 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %42, i32 0, i32 0
  %43 = load %struct.avifCodecDecodeInput*, %struct.avifCodecDecodeInput** %decodeInput37, align 4, !tbaa !23
  %alpha38 = getelementptr inbounds %struct.avifCodecDecodeInput, %struct.avifCodecDecodeInput* %43, i32 0, i32 1
  %44 = load i32, i32* %alpha38, align 4, !tbaa !33
  %tobool39 = icmp ne i32 %44, 0
  %lnot = xor i1 %tobool39, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %isColor, align 4, !tbaa !14
  %45 = load i32, i32* %isColor, align 4, !tbaa !14
  %tobool40 = icmp ne i32 %45, 0
  br i1 %tobool40, label %if.then41, label %if.else114

if.then41:                                        ; preds = %if.end36
  %46 = bitcast i32* %yuvFormat to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #4
  store i32 0, i32* %yuvFormat, align 4, !tbaa !34
  %47 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal42 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %47, i32 0, i32 2
  %48 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal42, align 4, !tbaa !13
  %image43 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %48, i32 0, i32 4
  %49 = load %struct.aom_image*, %struct.aom_image** %image43, align 4, !tbaa !32
  %fmt = getelementptr inbounds %struct.aom_image, %struct.aom_image* %49, i32 0, i32 0
  %50 = load i32, i32* %fmt, align 4, !tbaa !35
  switch i32 %50, label %sw.default [
    i32 258, label %sw.bb
    i32 260, label %sw.bb
    i32 2306, label %sw.bb
    i32 261, label %sw.bb44
    i32 2309, label %sw.bb44
    i32 262, label %sw.bb45
    i32 2310, label %sw.bb45
    i32 0, label %sw.bb46
    i32 769, label %sw.bb46
    i32 771, label %sw.bb46
    i32 2817, label %sw.bb46
  ]

sw.bb:                                            ; preds = %if.then41, %if.then41, %if.then41
  store i32 3, i32* %yuvFormat, align 4, !tbaa !34
  br label %sw.epilog

sw.bb44:                                          ; preds = %if.then41, %if.then41
  store i32 2, i32* %yuvFormat, align 4, !tbaa !34
  br label %sw.epilog

sw.bb45:                                          ; preds = %if.then41, %if.then41
  store i32 1, i32* %yuvFormat, align 4, !tbaa !34
  br label %sw.epilog

sw.bb46:                                          ; preds = %if.then41, %if.then41, %if.then41, %if.then41
  br label %sw.default

sw.default:                                       ; preds = %if.then41, %sw.bb46
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

sw.epilog:                                        ; preds = %sw.bb45, %sw.bb44, %sw.bb
  %51 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal47 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %51, i32 0, i32 2
  %52 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal47, align 4, !tbaa !13
  %image48 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %52, i32 0, i32 4
  %53 = load %struct.aom_image*, %struct.aom_image** %image48, align 4, !tbaa !32
  %monochrome = getelementptr inbounds %struct.aom_image, %struct.aom_image* %53, i32 0, i32 4
  %54 = load i32, i32* %monochrome, align 4, !tbaa !37
  %tobool49 = icmp ne i32 %54, 0
  br i1 %tobool49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %sw.epilog
  store i32 4, i32* %yuvFormat, align 4, !tbaa !34
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %sw.epilog
  %55 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %55, i32 0, i32 0
  %56 = load i32, i32* %width, align 4, !tbaa !38
  %tobool52 = icmp ne i32 %56, 0
  br i1 %tobool52, label %land.lhs.true53, label %if.end73

land.lhs.true53:                                  ; preds = %if.end51
  %57 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %57, i32 0, i32 1
  %58 = load i32, i32* %height, align 4, !tbaa !45
  %tobool54 = icmp ne i32 %58, 0
  br i1 %tobool54, label %if.then55, label %if.end73

if.then55:                                        ; preds = %land.lhs.true53
  %59 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width56 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %59, i32 0, i32 0
  %60 = load i32, i32* %width56, align 4, !tbaa !38
  %61 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal57 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %61, i32 0, i32 2
  %62 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal57, align 4, !tbaa !13
  %image58 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %62, i32 0, i32 4
  %63 = load %struct.aom_image*, %struct.aom_image** %image58, align 4, !tbaa !32
  %d_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %63, i32 0, i32 10
  %64 = load i32, i32* %d_w, align 4, !tbaa !46
  %cmp59 = icmp ne i32 %60, %64
  br i1 %cmp59, label %if.then71, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then55
  %65 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height60 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %65, i32 0, i32 1
  %66 = load i32, i32* %height60, align 4, !tbaa !45
  %67 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal61 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %67, i32 0, i32 2
  %68 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal61, align 4, !tbaa !13
  %image62 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %68, i32 0, i32 4
  %69 = load %struct.aom_image*, %struct.aom_image** %image62, align 4, !tbaa !32
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %69, i32 0, i32 11
  %70 = load i32, i32* %d_h, align 4, !tbaa !47
  %cmp63 = icmp ne i32 %66, %70
  br i1 %cmp63, label %if.then71, label %lor.lhs.false64

lor.lhs.false64:                                  ; preds = %lor.lhs.false
  %71 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %71, i32 0, i32 2
  %72 = load i32, i32* %depth, align 4, !tbaa !48
  %73 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal65 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %73, i32 0, i32 2
  %74 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal65, align 4, !tbaa !13
  %image66 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %74, i32 0, i32 4
  %75 = load %struct.aom_image*, %struct.aom_image** %image66, align 4, !tbaa !32
  %bit_depth = getelementptr inbounds %struct.aom_image, %struct.aom_image* %75, i32 0, i32 9
  %76 = load i32, i32* %bit_depth, align 4, !tbaa !49
  %cmp67 = icmp ne i32 %72, %76
  br i1 %cmp67, label %if.then71, label %lor.lhs.false68

lor.lhs.false68:                                  ; preds = %lor.lhs.false64
  %77 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat69 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %77, i32 0, i32 3
  %78 = load i32, i32* %yuvFormat69, align 4, !tbaa !50
  %79 = load i32, i32* %yuvFormat, align 4, !tbaa !34
  %cmp70 = icmp ne i32 %78, %79
  br i1 %cmp70, label %if.then71, label %if.end72

if.then71:                                        ; preds = %lor.lhs.false68, %lor.lhs.false64, %lor.lhs.false, %if.then55
  %80 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  call void @avifImageFreePlanes(%struct.avifImage* %80, i32 255)
  br label %if.end72

if.end72:                                         ; preds = %if.then71, %lor.lhs.false68
  br label %if.end73

if.end73:                                         ; preds = %if.end72, %land.lhs.true53, %if.end51
  %81 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal74 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %81, i32 0, i32 2
  %82 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal74, align 4, !tbaa !13
  %image75 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %82, i32 0, i32 4
  %83 = load %struct.aom_image*, %struct.aom_image** %image75, align 4, !tbaa !32
  %d_w76 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %83, i32 0, i32 10
  %84 = load i32, i32* %d_w76, align 4, !tbaa !46
  %85 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width77 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %85, i32 0, i32 0
  store i32 %84, i32* %width77, align 4, !tbaa !38
  %86 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal78 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %86, i32 0, i32 2
  %87 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal78, align 4, !tbaa !13
  %image79 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %87, i32 0, i32 4
  %88 = load %struct.aom_image*, %struct.aom_image** %image79, align 4, !tbaa !32
  %d_h80 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %88, i32 0, i32 11
  %89 = load i32, i32* %d_h80, align 4, !tbaa !47
  %90 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height81 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %90, i32 0, i32 1
  store i32 %89, i32* %height81, align 4, !tbaa !45
  %91 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal82 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %91, i32 0, i32 2
  %92 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal82, align 4, !tbaa !13
  %image83 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %92, i32 0, i32 4
  %93 = load %struct.aom_image*, %struct.aom_image** %image83, align 4, !tbaa !32
  %bit_depth84 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %93, i32 0, i32 9
  %94 = load i32, i32* %bit_depth84, align 4, !tbaa !49
  %95 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth85 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %95, i32 0, i32 2
  store i32 %94, i32* %depth85, align 4, !tbaa !48
  %96 = load i32, i32* %yuvFormat, align 4, !tbaa !34
  %97 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat86 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %97, i32 0, i32 3
  store i32 %96, i32* %yuvFormat86, align 4, !tbaa !50
  %98 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal87 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %98, i32 0, i32 2
  %99 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal87, align 4, !tbaa !13
  %image88 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %99, i32 0, i32 4
  %100 = load %struct.aom_image*, %struct.aom_image** %image88, align 4, !tbaa !32
  %range = getelementptr inbounds %struct.aom_image, %struct.aom_image* %100, i32 0, i32 6
  %101 = load i32, i32* %range, align 4, !tbaa !51
  %cmp89 = icmp eq i32 %101, 0
  %102 = zext i1 %cmp89 to i64
  %cond = select i1 %cmp89, i32 0, i32 1
  %103 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %103, i32 0, i32 4
  store i32 %cond, i32* %yuvRange, align 4, !tbaa !52
  %104 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal90 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %104, i32 0, i32 2
  %105 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal90, align 4, !tbaa !13
  %image91 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %105, i32 0, i32 4
  %106 = load %struct.aom_image*, %struct.aom_image** %image91, align 4, !tbaa !32
  %cp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %106, i32 0, i32 1
  %107 = load i32, i32* %cp, align 4, !tbaa !53
  %108 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %colorPrimaries = getelementptr inbounds %struct.avifImage, %struct.avifImage* %108, i32 0, i32 14
  store i32 %107, i32* %colorPrimaries, align 4, !tbaa !54
  %109 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal92 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %109, i32 0, i32 2
  %110 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal92, align 4, !tbaa !13
  %image93 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %110, i32 0, i32 4
  %111 = load %struct.aom_image*, %struct.aom_image** %image93, align 4, !tbaa !32
  %tc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %111, i32 0, i32 2
  %112 = load i32, i32* %tc, align 4, !tbaa !55
  %113 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %transferCharacteristics = getelementptr inbounds %struct.avifImage, %struct.avifImage* %113, i32 0, i32 15
  store i32 %112, i32* %transferCharacteristics, align 4, !tbaa !56
  %114 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal94 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %114, i32 0, i32 2
  %115 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal94, align 4, !tbaa !13
  %image95 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %115, i32 0, i32 4
  %116 = load %struct.aom_image*, %struct.aom_image** %image95, align 4, !tbaa !32
  %mc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %116, i32 0, i32 3
  %117 = load i32, i32* %mc, align 4, !tbaa !57
  %118 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %118, i32 0, i32 16
  store i32 %117, i32* %matrixCoefficients, align 4, !tbaa !58
  %119 = bitcast %struct.avifPixelFormatInfo* %formatInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %119) #4
  %120 = load i32, i32* %yuvFormat, align 4, !tbaa !34
  call void @avifGetPixelFormatInfo(i32 %120, %struct.avifPixelFormatInfo* %formatInfo)
  %121 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  call void @avifImageFreePlanes(%struct.avifImage* %121, i32 1)
  %122 = bitcast i32* %yuvPlaneCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #4
  %123 = load i32, i32* %yuvFormat, align 4, !tbaa !34
  %cmp96 = icmp eq i32 %123, 4
  %124 = zext i1 %cmp96 to i64
  %cond97 = select i1 %cmp96, i32 1, i32 3
  store i32 %cond97, i32* %yuvPlaneCount, align 4, !tbaa !14
  %125 = bitcast i32* %yuvPlane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #4
  store i32 0, i32* %yuvPlane, align 4, !tbaa !14
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc, %if.end73
  %126 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %127 = load i32, i32* %yuvPlaneCount, align 4, !tbaa !14
  %cmp99 = icmp slt i32 %126, %127
  br i1 %cmp99, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond98
  store i32 5, i32* %cleanup.dest.slot, align 4
  %128 = bitcast i32* %yuvPlane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #4
  br label %for.end110

for.body:                                         ; preds = %for.cond98
  %129 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal100 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %129, i32 0, i32 2
  %130 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal100, align 4, !tbaa !13
  %image101 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %130, i32 0, i32 4
  %131 = load %struct.aom_image*, %struct.aom_image** %image101, align 4, !tbaa !32
  %planes = getelementptr inbounds %struct.aom_image, %struct.aom_image* %131, i32 0, i32 16
  %132 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx102 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes, i32 0, i32 %132
  %133 = load i8*, i8** %arrayidx102, align 4, !tbaa !2
  %134 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %134, i32 0, i32 6
  %135 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx103 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 %135
  store i8* %133, i8** %arrayidx103, align 4, !tbaa !2
  %136 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal104 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %136, i32 0, i32 2
  %137 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal104, align 4, !tbaa !13
  %image105 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %137, i32 0, i32 4
  %138 = load %struct.aom_image*, %struct.aom_image** %image105, align 4, !tbaa !32
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %138, i32 0, i32 17
  %139 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx106 = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 %139
  %140 = load i32, i32* %arrayidx106, align 4, !tbaa !14
  %141 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %141, i32 0, i32 7
  %142 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx107 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 %142
  store i32 %140, i32* %arrayidx107, align 4, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %143 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %inc108 = add nsw i32 %143, 1
  store i32 %inc108, i32* %yuvPlane, align 4, !tbaa !14
  br label %for.cond98

for.end110:                                       ; preds = %for.cond.cleanup
  %144 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %imageOwnsYUVPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %144, i32 0, i32 8
  store i32 0, i32* %imageOwnsYUVPlanes, align 4, !tbaa !59
  %145 = bitcast i32* %yuvPlaneCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast %struct.avifPixelFormatInfo* %formatInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %146) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup111

cleanup111:                                       ; preds = %for.end110, %sw.default
  %147 = bitcast i32* %yuvFormat to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %cleanup.dest112 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest112, label %cleanup167 [
    i32 0, label %cleanup.cont113
  ]

cleanup.cont113:                                  ; preds = %cleanup111
  br label %if.end166

if.else114:                                       ; preds = %if.end36
  %148 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width115 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %148, i32 0, i32 0
  %149 = load i32, i32* %width115, align 4, !tbaa !38
  %tobool116 = icmp ne i32 %149, 0
  br i1 %tobool116, label %land.lhs.true117, label %if.end140

land.lhs.true117:                                 ; preds = %if.else114
  %150 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height118 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %150, i32 0, i32 1
  %151 = load i32, i32* %height118, align 4, !tbaa !45
  %tobool119 = icmp ne i32 %151, 0
  br i1 %tobool119, label %if.then120, label %if.end140

if.then120:                                       ; preds = %land.lhs.true117
  %152 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width121 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %152, i32 0, i32 0
  %153 = load i32, i32* %width121, align 4, !tbaa !38
  %154 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal122 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %154, i32 0, i32 2
  %155 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal122, align 4, !tbaa !13
  %image123 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %155, i32 0, i32 4
  %156 = load %struct.aom_image*, %struct.aom_image** %image123, align 4, !tbaa !32
  %d_w124 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %156, i32 0, i32 10
  %157 = load i32, i32* %d_w124, align 4, !tbaa !46
  %cmp125 = icmp ne i32 %153, %157
  br i1 %cmp125, label %if.then138, label %lor.lhs.false126

lor.lhs.false126:                                 ; preds = %if.then120
  %158 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height127 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %158, i32 0, i32 1
  %159 = load i32, i32* %height127, align 4, !tbaa !45
  %160 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal128 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %160, i32 0, i32 2
  %161 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal128, align 4, !tbaa !13
  %image129 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %161, i32 0, i32 4
  %162 = load %struct.aom_image*, %struct.aom_image** %image129, align 4, !tbaa !32
  %d_h130 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %162, i32 0, i32 11
  %163 = load i32, i32* %d_h130, align 4, !tbaa !47
  %cmp131 = icmp ne i32 %159, %163
  br i1 %cmp131, label %if.then138, label %lor.lhs.false132

lor.lhs.false132:                                 ; preds = %lor.lhs.false126
  %164 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth133 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %164, i32 0, i32 2
  %165 = load i32, i32* %depth133, align 4, !tbaa !48
  %166 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal134 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %166, i32 0, i32 2
  %167 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal134, align 4, !tbaa !13
  %image135 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %167, i32 0, i32 4
  %168 = load %struct.aom_image*, %struct.aom_image** %image135, align 4, !tbaa !32
  %bit_depth136 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %168, i32 0, i32 9
  %169 = load i32, i32* %bit_depth136, align 4, !tbaa !49
  %cmp137 = icmp ne i32 %165, %169
  br i1 %cmp137, label %if.then138, label %if.end139

if.then138:                                       ; preds = %lor.lhs.false132, %lor.lhs.false126, %if.then120
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup167

if.end139:                                        ; preds = %lor.lhs.false132
  br label %if.end140

if.end140:                                        ; preds = %if.end139, %land.lhs.true117, %if.else114
  %170 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal141 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %170, i32 0, i32 2
  %171 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal141, align 4, !tbaa !13
  %image142 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %171, i32 0, i32 4
  %172 = load %struct.aom_image*, %struct.aom_image** %image142, align 4, !tbaa !32
  %d_w143 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %172, i32 0, i32 10
  %173 = load i32, i32* %d_w143, align 4, !tbaa !46
  %174 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width144 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %174, i32 0, i32 0
  store i32 %173, i32* %width144, align 4, !tbaa !38
  %175 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal145 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %175, i32 0, i32 2
  %176 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal145, align 4, !tbaa !13
  %image146 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %176, i32 0, i32 4
  %177 = load %struct.aom_image*, %struct.aom_image** %image146, align 4, !tbaa !32
  %d_h147 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %177, i32 0, i32 11
  %178 = load i32, i32* %d_h147, align 4, !tbaa !47
  %179 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height148 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %179, i32 0, i32 1
  store i32 %178, i32* %height148, align 4, !tbaa !45
  %180 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal149 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %180, i32 0, i32 2
  %181 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal149, align 4, !tbaa !13
  %image150 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %181, i32 0, i32 4
  %182 = load %struct.aom_image*, %struct.aom_image** %image150, align 4, !tbaa !32
  %bit_depth151 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %182, i32 0, i32 9
  %183 = load i32, i32* %bit_depth151, align 4, !tbaa !49
  %184 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth152 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %184, i32 0, i32 2
  store i32 %183, i32* %depth152, align 4, !tbaa !48
  %185 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  call void @avifImageFreePlanes(%struct.avifImage* %185, i32 2)
  %186 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal153 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %186, i32 0, i32 2
  %187 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal153, align 4, !tbaa !13
  %image154 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %187, i32 0, i32 4
  %188 = load %struct.aom_image*, %struct.aom_image** %image154, align 4, !tbaa !32
  %planes155 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %188, i32 0, i32 16
  %arrayidx156 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes155, i32 0, i32 0
  %189 = load i8*, i8** %arrayidx156, align 4, !tbaa !2
  %190 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %190, i32 0, i32 10
  store i8* %189, i8** %alphaPlane, align 4, !tbaa !60
  %191 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal157 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %191, i32 0, i32 2
  %192 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal157, align 4, !tbaa !13
  %image158 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %192, i32 0, i32 4
  %193 = load %struct.aom_image*, %struct.aom_image** %image158, align 4, !tbaa !32
  %stride159 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %193, i32 0, i32 17
  %arrayidx160 = getelementptr inbounds [3 x i32], [3 x i32]* %stride159, i32 0, i32 0
  %194 = load i32, i32* %arrayidx160, align 4, !tbaa !14
  %195 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %195, i32 0, i32 11
  store i32 %194, i32* %alphaRowBytes, align 4, !tbaa !61
  %196 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal161 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %196, i32 0, i32 2
  %197 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal161, align 4, !tbaa !13
  %image162 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %197, i32 0, i32 4
  %198 = load %struct.aom_image*, %struct.aom_image** %image162, align 4, !tbaa !32
  %range163 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %198, i32 0, i32 6
  %199 = load i32, i32* %range163, align 4, !tbaa !51
  %cmp164 = icmp eq i32 %199, 0
  %200 = zext i1 %cmp164 to i64
  %cond165 = select i1 %cmp164, i32 0, i32 1
  %201 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %201, i32 0, i32 9
  store i32 %cond165, i32* %alphaRange, align 4, !tbaa !62
  %202 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %imageOwnsAlphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %202, i32 0, i32 12
  store i32 0, i32* %imageOwnsAlphaPlane, align 4, !tbaa !63
  br label %if.end166

if.end166:                                        ; preds = %if.end140, %cleanup.cont113
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup167

cleanup167:                                       ; preds = %if.end166, %if.then138, %cleanup111
  %203 = bitcast i32* %isColor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  br label %cleanup168

cleanup168:                                       ; preds = %cleanup167, %if.else34, %cleanup
  %204 = bitcast %struct.aom_image** %nextFrame to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = load i32, i32* %retval, align 4
  ret i32 %205
}

; Function Attrs: nounwind
define internal i32 @aomCodecEncodeImage(%struct.avifCodec* %codec, %struct.avifEncoder* %encoder, %struct.avifImage* %image, i32 %alpha, i32 %addImageFlags, %struct.avifCodecEncodeOutput* %output) #0 {
entry:
  %retval = alloca i32, align 4
  %codec.addr = alloca %struct.avifCodec*, align 4
  %encoder.addr = alloca %struct.avifEncoder*, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %alpha.addr = alloca i32, align 4
  %addImageFlags.addr = alloca i32, align 4
  %output.addr = alloca %struct.avifCodecEncodeOutput*, align 4
  %aomUsage = alloca i32, align 4
  %aomCpuUsed = alloca i32, align 4
  %aomVersion = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %encoderInterface = alloca %struct.aom_codec_iface*, align 4
  %cfg = alloca %struct.aom_codec_enc_cfg, align 4
  %minQuantizer = alloca i32, align 4
  %maxQuantizer = alloca i32, align 4
  %lossless = alloca i32, align 4
  %encoderFlags = alloca i32, align 4
  %tileRowsLog2162 = alloca i32, align 4
  %tileColsLog2185 = alloca i32, align 4
  %yShift = alloca i32, align 4
  %uvHeight = alloca i32, align 4
  %aomImage = alloca %struct.aom_image*, align 4
  %monochromeRequested = alloca i32, align 4
  %j = alloca i32, align 4
  %srcAlphaRow = alloca i8*, align 4
  %dstAlphaRow = alloca i8*, align 4
  %yuvPlaneCount = alloca i32, align 4
  %yuvPlane = alloca i32, align 4
  %planeHeight = alloca i32, align 4
  %j267 = alloca i32, align 4
  %srcRow = alloca i8*, align 4
  %dstRow = alloca i8*, align 4
  %monoUVWidth = alloca i32, align 4
  %monoUVHeight = alloca i32, align 4
  %yuvPlane322 = alloca i32, align 4
  %half = alloca i16, align 2
  %j335 = alloca i32, align 4
  %dstRow341 = alloca i16*, align 4
  %i = alloca i32, align 4
  %half363 = alloca i8, align 1
  %planeSize = alloca i32, align 4
  %encodeFlags = alloca i32, align 4
  %iter = alloca i8*, align 4
  %pkt = alloca %struct.aom_codec_cx_pkt*, align 4
  store %struct.avifCodec* %codec, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  store %struct.avifEncoder* %encoder, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store i32 %alpha, i32* %alpha.addr, align 4, !tbaa !14
  store i32 %addImageFlags, i32* %addImageFlags.addr, align 4, !tbaa !14
  store %struct.avifCodecEncodeOutput* %output, %struct.avifCodecEncodeOutput** %output.addr, align 4, !tbaa !2
  %0 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %0, i32 0, i32 2
  %1 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal, align 4, !tbaa !13
  %encoderInitialized = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %1, i32 0, i32 5
  %2 = load i32, i32* %encoderInitialized, align 4, !tbaa !64
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.end214, label %if.then

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %aomUsage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i32 0, i32* %aomUsage, align 4, !tbaa !14
  %4 = bitcast i32* %aomCpuUsed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  store i32 -1, i32* %aomCpuUsed, align 4, !tbaa !14
  %5 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %speed = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %5, i32 0, i32 8
  %6 = load i32, i32* %speed, align 8, !tbaa !65
  %cmp = icmp ne i32 %6, -1
  br i1 %cmp, label %if.then1, label %if.end29

if.then1:                                         ; preds = %if.then
  %7 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %speed2 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %7, i32 0, i32 8
  %8 = load i32, i32* %speed2, align 8, !tbaa !65
  %cmp3 = icmp slt i32 %8, 8
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then1
  store i32 0, i32* %aomUsage, align 4, !tbaa !14
  %9 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %speed5 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %9, i32 0, i32 8
  %10 = load i32, i32* %speed5, align 8, !tbaa !65
  %cmp6 = icmp slt i32 %10, 0
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then4
  br label %cond.end12

cond.false:                                       ; preds = %if.then4
  %11 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %speed7 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %11, i32 0, i32 8
  %12 = load i32, i32* %speed7, align 8, !tbaa !65
  %cmp8 = icmp slt i32 6, %12
  br i1 %cmp8, label %cond.true9, label %cond.false10

cond.true9:                                       ; preds = %cond.false
  br label %cond.end

cond.false10:                                     ; preds = %cond.false
  %13 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %speed11 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %13, i32 0, i32 8
  %14 = load i32, i32* %speed11, align 8, !tbaa !65
  br label %cond.end

cond.end:                                         ; preds = %cond.false10, %cond.true9
  %cond = phi i32 [ 6, %cond.true9 ], [ %14, %cond.false10 ]
  br label %cond.end12

cond.end12:                                       ; preds = %cond.end, %cond.true
  %cond13 = phi i32 [ 0, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond13, i32* %aomCpuUsed, align 4, !tbaa !14
  br label %if.end

if.else:                                          ; preds = %if.then1
  store i32 1, i32* %aomUsage, align 4, !tbaa !14
  %15 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %speed14 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %15, i32 0, i32 8
  %16 = load i32, i32* %speed14, align 8, !tbaa !65
  %sub = sub nsw i32 %16, 2
  %cmp15 = icmp slt i32 %sub, 6
  br i1 %cmp15, label %cond.true16, label %cond.false17

cond.true16:                                      ; preds = %if.else
  br label %cond.end27

cond.false17:                                     ; preds = %if.else
  %17 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %speed18 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %17, i32 0, i32 8
  %18 = load i32, i32* %speed18, align 8, !tbaa !65
  %sub19 = sub nsw i32 %18, 2
  %cmp20 = icmp slt i32 8, %sub19
  br i1 %cmp20, label %cond.true21, label %cond.false22

cond.true21:                                      ; preds = %cond.false17
  br label %cond.end25

cond.false22:                                     ; preds = %cond.false17
  %19 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %speed23 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %19, i32 0, i32 8
  %20 = load i32, i32* %speed23, align 8, !tbaa !65
  %sub24 = sub nsw i32 %20, 2
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false22, %cond.true21
  %cond26 = phi i32 [ 8, %cond.true21 ], [ %sub24, %cond.false22 ]
  br label %cond.end27

cond.end27:                                       ; preds = %cond.end25, %cond.true16
  %cond28 = phi i32 [ 6, %cond.true16 ], [ %cond26, %cond.end25 ]
  store i32 %cond28, i32* %aomCpuUsed, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %cond.end27, %cond.end12
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then
  %21 = bitcast i32* %aomVersion to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %call = call i32 @aom_codec_version()
  store i32 %call, i32* %aomVersion, align 4, !tbaa !14
  %22 = load i32, i32* %aomVersion, align 4, !tbaa !14
  %cmp30 = icmp slt i32 %22, 131072
  br i1 %cmp30, label %land.lhs.true, label %if.end36

land.lhs.true:                                    ; preds = %if.end29
  %23 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %23, i32 0, i32 2
  %24 = load i32, i32* %depth, align 4, !tbaa !48
  %cmp31 = icmp ugt i32 %24, 8
  br i1 %cmp31, label %if.then32, label %if.end36

if.then32:                                        ; preds = %land.lhs.true
  %25 = load i32, i32* %aomCpuUsed, align 4, !tbaa !14
  %cmp33 = icmp sgt i32 %25, 6
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.then32
  store i32 6, i32* %aomCpuUsed, align 4, !tbaa !14
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.then32
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %land.lhs.true, %if.end29
  %26 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %27 = load i32, i32* %alpha.addr, align 4, !tbaa !14
  %call37 = call i32 @avifImageCalcAOMFmt(%struct.avifImage* %26, i32 %27)
  %28 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal38 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %28, i32 0, i32 2
  %29 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal38, align 4, !tbaa !13
  %aomFormat = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %29, i32 0, i32 8
  store i32 %call37, i32* %aomFormat, align 4, !tbaa !69
  %30 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal39 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %30, i32 0, i32 2
  %31 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal39, align 4, !tbaa !13
  %aomFormat40 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %31, i32 0, i32 8
  %32 = load i32, i32* %aomFormat40, align 4, !tbaa !69
  %cmp41 = icmp eq i32 %32, 0
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.end36
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end43:                                         ; preds = %if.end36
  %33 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %33, i32 0, i32 3
  %34 = load i32, i32* %yuvFormat, align 4, !tbaa !50
  %35 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal44 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %35, i32 0, i32 2
  %36 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal44, align 4, !tbaa !13
  %formatInfo = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %36, i32 0, i32 7
  call void @avifGetPixelFormatInfo(i32 %34, %struct.avifPixelFormatInfo* %formatInfo)
  %37 = bitcast %struct.aom_codec_iface** %encoderInterface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  %call45 = call %struct.aom_codec_iface* @aom_codec_av1_cx()
  store %struct.aom_codec_iface* %call45, %struct.aom_codec_iface** %encoderInterface, align 4, !tbaa !2
  %38 = bitcast %struct.aom_codec_enc_cfg* %cfg to i8*
  call void @llvm.lifetime.start.p0i8(i64 884, i8* %38) #4
  %39 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %encoderInterface, align 4, !tbaa !2
  %40 = load i32, i32* %aomUsage, align 4, !tbaa !14
  %call46 = call i32 @aom_codec_enc_config_default(%struct.aom_codec_iface* %39, %struct.aom_codec_enc_cfg* %cfg, i32 %40)
  %41 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %configBox = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %41, i32 0, i32 1
  %seqProfile = getelementptr inbounds %struct.avifCodecConfigurationBox, %struct.avifCodecConfigurationBox* %configBox, i32 0, i32 0
  %42 = load i8, i8* %seqProfile, align 4, !tbaa !70
  %conv = zext i8 %42 to i32
  %g_profile = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 2
  store i32 %conv, i32* %g_profile, align 4, !tbaa !71
  %43 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth47 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %43, i32 0, i32 2
  %44 = load i32, i32* %depth47, align 4, !tbaa !48
  %g_bit_depth = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 8
  store i32 %44, i32* %g_bit_depth, align 4, !tbaa !76
  %45 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth48 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %45, i32 0, i32 2
  %46 = load i32, i32* %depth48, align 4, !tbaa !48
  %g_input_bit_depth = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 9
  store i32 %46, i32* %g_input_bit_depth, align 4, !tbaa !77
  %47 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.avifImage, %struct.avifImage* %47, i32 0, i32 0
  %48 = load i32, i32* %width, align 4, !tbaa !38
  %g_w = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 3
  store i32 %48, i32* %g_w, align 4, !tbaa !78
  %49 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.avifImage, %struct.avifImage* %49, i32 0, i32 1
  %50 = load i32, i32* %height, align 4, !tbaa !45
  %g_h = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 4
  store i32 %50, i32* %g_h, align 4, !tbaa !79
  %51 = load i32, i32* %addImageFlags.addr, align 4, !tbaa !14
  %and = and i32 %51, 2
  %tobool49 = icmp ne i32 %and, 0
  br i1 %tobool49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end43
  %g_limit = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 5
  store i32 1, i32* %g_limit, align 4, !tbaa !80
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %if.end43
  %52 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxThreads = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %52, i32 0, i32 1
  %53 = load i32, i32* %maxThreads, align 4, !tbaa !81
  %cmp52 = icmp sgt i32 %53, 1
  br i1 %cmp52, label %if.then54, label %if.end56

if.then54:                                        ; preds = %if.end51
  %54 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxThreads55 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %54, i32 0, i32 1
  %55 = load i32, i32* %maxThreads55, align 4, !tbaa !81
  %g_threads = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 1
  store i32 %55, i32* %g_threads, align 4, !tbaa !82
  br label %if.end56

if.end56:                                         ; preds = %if.then54, %if.end51
  %56 = bitcast i32* %minQuantizer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #4
  %57 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %minQuantizer57 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %57, i32 0, i32 2
  %58 = load i32, i32* %minQuantizer57, align 8, !tbaa !83
  %cmp58 = icmp slt i32 %58, 0
  br i1 %cmp58, label %cond.true60, label %cond.false61

cond.true60:                                      ; preds = %if.end56
  br label %cond.end70

cond.false61:                                     ; preds = %if.end56
  %59 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %minQuantizer62 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %59, i32 0, i32 2
  %60 = load i32, i32* %minQuantizer62, align 8, !tbaa !83
  %cmp63 = icmp slt i32 63, %60
  br i1 %cmp63, label %cond.true65, label %cond.false66

cond.true65:                                      ; preds = %cond.false61
  br label %cond.end68

cond.false66:                                     ; preds = %cond.false61
  %61 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %minQuantizer67 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %61, i32 0, i32 2
  %62 = load i32, i32* %minQuantizer67, align 8, !tbaa !83
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false66, %cond.true65
  %cond69 = phi i32 [ 63, %cond.true65 ], [ %62, %cond.false66 ]
  br label %cond.end70

cond.end70:                                       ; preds = %cond.end68, %cond.true60
  %cond71 = phi i32 [ 0, %cond.true60 ], [ %cond69, %cond.end68 ]
  store i32 %cond71, i32* %minQuantizer, align 4, !tbaa !14
  %63 = bitcast i32* %maxQuantizer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #4
  %64 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxQuantizer72 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %64, i32 0, i32 3
  %65 = load i32, i32* %maxQuantizer72, align 4, !tbaa !84
  %cmp73 = icmp slt i32 %65, 0
  br i1 %cmp73, label %cond.true75, label %cond.false76

cond.true75:                                      ; preds = %cond.end70
  br label %cond.end85

cond.false76:                                     ; preds = %cond.end70
  %66 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxQuantizer77 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %66, i32 0, i32 3
  %67 = load i32, i32* %maxQuantizer77, align 4, !tbaa !84
  %cmp78 = icmp slt i32 63, %67
  br i1 %cmp78, label %cond.true80, label %cond.false81

cond.true80:                                      ; preds = %cond.false76
  br label %cond.end83

cond.false81:                                     ; preds = %cond.false76
  %68 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxQuantizer82 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %68, i32 0, i32 3
  %69 = load i32, i32* %maxQuantizer82, align 4, !tbaa !84
  br label %cond.end83

cond.end83:                                       ; preds = %cond.false81, %cond.true80
  %cond84 = phi i32 [ 63, %cond.true80 ], [ %69, %cond.false81 ]
  br label %cond.end85

cond.end85:                                       ; preds = %cond.end83, %cond.true75
  %cond86 = phi i32 [ 0, %cond.true75 ], [ %cond84, %cond.end83 ]
  store i32 %cond86, i32* %maxQuantizer, align 4, !tbaa !14
  %70 = load i32, i32* %alpha.addr, align 4, !tbaa !14
  %tobool87 = icmp ne i32 %70, 0
  br i1 %tobool87, label %if.then88, label %if.end117

if.then88:                                        ; preds = %cond.end85
  %71 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %minQuantizerAlpha = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %71, i32 0, i32 4
  %72 = load i32, i32* %minQuantizerAlpha, align 8, !tbaa !85
  %cmp89 = icmp slt i32 %72, 0
  br i1 %cmp89, label %cond.true91, label %cond.false92

cond.true91:                                      ; preds = %if.then88
  br label %cond.end101

cond.false92:                                     ; preds = %if.then88
  %73 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %minQuantizerAlpha93 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %73, i32 0, i32 4
  %74 = load i32, i32* %minQuantizerAlpha93, align 8, !tbaa !85
  %cmp94 = icmp slt i32 63, %74
  br i1 %cmp94, label %cond.true96, label %cond.false97

cond.true96:                                      ; preds = %cond.false92
  br label %cond.end99

cond.false97:                                     ; preds = %cond.false92
  %75 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %minQuantizerAlpha98 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %75, i32 0, i32 4
  %76 = load i32, i32* %minQuantizerAlpha98, align 8, !tbaa !85
  br label %cond.end99

cond.end99:                                       ; preds = %cond.false97, %cond.true96
  %cond100 = phi i32 [ 63, %cond.true96 ], [ %76, %cond.false97 ]
  br label %cond.end101

cond.end101:                                      ; preds = %cond.end99, %cond.true91
  %cond102 = phi i32 [ 0, %cond.true91 ], [ %cond100, %cond.end99 ]
  store i32 %cond102, i32* %minQuantizer, align 4, !tbaa !14
  %77 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxQuantizerAlpha = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %77, i32 0, i32 5
  %78 = load i32, i32* %maxQuantizerAlpha, align 4, !tbaa !86
  %cmp103 = icmp slt i32 %78, 0
  br i1 %cmp103, label %cond.true105, label %cond.false106

cond.true105:                                     ; preds = %cond.end101
  br label %cond.end115

cond.false106:                                    ; preds = %cond.end101
  %79 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxQuantizerAlpha107 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %79, i32 0, i32 5
  %80 = load i32, i32* %maxQuantizerAlpha107, align 4, !tbaa !86
  %cmp108 = icmp slt i32 63, %80
  br i1 %cmp108, label %cond.true110, label %cond.false111

cond.true110:                                     ; preds = %cond.false106
  br label %cond.end113

cond.false111:                                    ; preds = %cond.false106
  %81 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxQuantizerAlpha112 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %81, i32 0, i32 5
  %82 = load i32, i32* %maxQuantizerAlpha112, align 4, !tbaa !86
  br label %cond.end113

cond.end113:                                      ; preds = %cond.false111, %cond.true110
  %cond114 = phi i32 [ 63, %cond.true110 ], [ %82, %cond.false111 ]
  br label %cond.end115

cond.end115:                                      ; preds = %cond.end113, %cond.true105
  %cond116 = phi i32 [ 0, %cond.true105 ], [ %cond114, %cond.end113 ]
  store i32 %cond116, i32* %maxQuantizer, align 4, !tbaa !14
  br label %if.end117

if.end117:                                        ; preds = %cond.end115, %cond.end85
  %83 = bitcast i32* %lossless to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #4
  %84 = load i32, i32* %minQuantizer, align 4, !tbaa !14
  %cmp118 = icmp eq i32 %84, 0
  br i1 %cmp118, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end117
  %85 = load i32, i32* %maxQuantizer, align 4, !tbaa !14
  %cmp120 = icmp eq i32 %85, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end117
  %86 = phi i1 [ false, %if.end117 ], [ %cmp120, %land.rhs ]
  %land.ext = zext i1 %86 to i32
  store i32 %land.ext, i32* %lossless, align 4, !tbaa !14
  %87 = load i32, i32* %minQuantizer, align 4, !tbaa !14
  %rc_min_quantizer = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 27
  store i32 %87, i32* %rc_min_quantizer, align 4, !tbaa !87
  %88 = load i32, i32* %maxQuantizer, align 4, !tbaa !14
  %rc_max_quantizer = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 28
  store i32 %88, i32* %rc_max_quantizer, align 4, !tbaa !88
  %89 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal122 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %89, i32 0, i32 2
  %90 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal122, align 4, !tbaa !13
  %monochromeEnabled = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %90, i32 0, i32 9
  store i32 0, i32* %monochromeEnabled, align 4, !tbaa !89
  %91 = load i32, i32* %aomVersion, align 4, !tbaa !14
  %cmp123 = icmp sgt i32 %91, 131072
  br i1 %cmp123, label %if.then125, label %if.end134

if.then125:                                       ; preds = %land.end
  %92 = load i32, i32* %alpha.addr, align 4, !tbaa !14
  %tobool126 = icmp ne i32 %92, 0
  br i1 %tobool126, label %if.then130, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then125
  %93 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat127 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %93, i32 0, i32 3
  %94 = load i32, i32* %yuvFormat127, align 4, !tbaa !50
  %cmp128 = icmp eq i32 %94, 4
  br i1 %cmp128, label %if.then130, label %if.end133

if.then130:                                       ; preds = %lor.lhs.false, %if.then125
  %95 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal131 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %95, i32 0, i32 2
  %96 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal131, align 4, !tbaa !13
  %monochromeEnabled132 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %96, i32 0, i32 9
  store i32 1, i32* %monochromeEnabled132, align 4, !tbaa !89
  %monochrome = getelementptr inbounds %struct.aom_codec_enc_cfg, %struct.aom_codec_enc_cfg* %cfg, i32 0, i32 44
  store i32 1, i32* %monochrome, align 4, !tbaa !90
  br label %if.end133

if.end133:                                        ; preds = %if.then130, %lor.lhs.false
  br label %if.end134

if.end134:                                        ; preds = %if.end133, %land.end
  %97 = bitcast i32* %encoderFlags to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #4
  store i32 0, i32* %encoderFlags, align 4, !tbaa !91
  %98 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth135 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %98, i32 0, i32 2
  %99 = load i32, i32* %depth135, align 4, !tbaa !48
  %cmp136 = icmp ugt i32 %99, 8
  br i1 %cmp136, label %if.then138, label %if.end139

if.then138:                                       ; preds = %if.end134
  %100 = load i32, i32* %encoderFlags, align 4, !tbaa !91
  %or = or i32 %100, 262144
  store i32 %or, i32* %encoderFlags, align 4, !tbaa !91
  br label %if.end139

if.end139:                                        ; preds = %if.then138, %if.end134
  %101 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal140 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %101, i32 0, i32 2
  %102 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal140, align 4, !tbaa !13
  %encoder141 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %102, i32 0, i32 6
  %103 = load %struct.aom_codec_iface*, %struct.aom_codec_iface** %encoderInterface, align 4, !tbaa !2
  %104 = load i32, i32* %encoderFlags, align 4, !tbaa !91
  %call142 = call i32 @aom_codec_enc_init_ver(%struct.aom_codec_ctx* %encoder141, %struct.aom_codec_iface* %103, %struct.aom_codec_enc_cfg* %cfg, i32 %104, i32 22)
  %105 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal143 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %105, i32 0, i32 2
  %106 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal143, align 4, !tbaa !13
  %encoderInitialized144 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %106, i32 0, i32 5
  store i32 1, i32* %encoderInitialized144, align 4, !tbaa !64
  %107 = load i32, i32* %lossless, align 4, !tbaa !14
  %tobool145 = icmp ne i32 %107, 0
  br i1 %tobool145, label %if.then146, label %if.end150

if.then146:                                       ; preds = %if.end139
  %108 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal147 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %108, i32 0, i32 2
  %109 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal147, align 4, !tbaa !13
  %encoder148 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %109, i32 0, i32 6
  %call149 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder148, i32 31, i32 1)
  br label %if.end150

if.end150:                                        ; preds = %if.then146, %if.end139
  %110 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %maxThreads151 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %110, i32 0, i32 1
  %111 = load i32, i32* %maxThreads151, align 4, !tbaa !81
  %cmp152 = icmp sgt i32 %111, 1
  br i1 %cmp152, label %if.then154, label %if.end158

if.then154:                                       ; preds = %if.end150
  %112 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal155 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %112, i32 0, i32 2
  %113 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal155, align 4, !tbaa !13
  %encoder156 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %113, i32 0, i32 6
  %call157 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder156, i32 32, i32 1)
  br label %if.end158

if.end158:                                        ; preds = %if.then154, %if.end150
  %114 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %tileRowsLog2 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %114, i32 0, i32 6
  %115 = load i32, i32* %tileRowsLog2, align 8, !tbaa !92
  %cmp159 = icmp ne i32 %115, 0
  br i1 %cmp159, label %if.then161, label %if.end181

if.then161:                                       ; preds = %if.end158
  %116 = bitcast i32* %tileRowsLog2162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #4
  %117 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %tileRowsLog2163 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %117, i32 0, i32 6
  %118 = load i32, i32* %tileRowsLog2163, align 8, !tbaa !92
  %cmp164 = icmp slt i32 %118, 0
  br i1 %cmp164, label %cond.true166, label %cond.false167

cond.true166:                                     ; preds = %if.then161
  br label %cond.end176

cond.false167:                                    ; preds = %if.then161
  %119 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %tileRowsLog2168 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %119, i32 0, i32 6
  %120 = load i32, i32* %tileRowsLog2168, align 8, !tbaa !92
  %cmp169 = icmp slt i32 6, %120
  br i1 %cmp169, label %cond.true171, label %cond.false172

cond.true171:                                     ; preds = %cond.false167
  br label %cond.end174

cond.false172:                                    ; preds = %cond.false167
  %121 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %tileRowsLog2173 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %121, i32 0, i32 6
  %122 = load i32, i32* %tileRowsLog2173, align 8, !tbaa !92
  br label %cond.end174

cond.end174:                                      ; preds = %cond.false172, %cond.true171
  %cond175 = phi i32 [ 6, %cond.true171 ], [ %122, %cond.false172 ]
  br label %cond.end176

cond.end176:                                      ; preds = %cond.end174, %cond.true166
  %cond177 = phi i32 [ 0, %cond.true166 ], [ %cond175, %cond.end174 ]
  store i32 %cond177, i32* %tileRowsLog2162, align 4, !tbaa !14
  %123 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal178 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %123, i32 0, i32 2
  %124 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal178, align 4, !tbaa !13
  %encoder179 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %124, i32 0, i32 6
  %125 = load i32, i32* %tileRowsLog2162, align 4, !tbaa !14
  %call180 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder179, i32 34, i32 %125)
  %126 = bitcast i32* %tileRowsLog2162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #4
  br label %if.end181

if.end181:                                        ; preds = %cond.end176, %if.end158
  %127 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %tileColsLog2 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %127, i32 0, i32 7
  %128 = load i32, i32* %tileColsLog2, align 4, !tbaa !93
  %cmp182 = icmp ne i32 %128, 0
  br i1 %cmp182, label %if.then184, label %if.end204

if.then184:                                       ; preds = %if.end181
  %129 = bitcast i32* %tileColsLog2185 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #4
  %130 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %tileColsLog2186 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %130, i32 0, i32 7
  %131 = load i32, i32* %tileColsLog2186, align 4, !tbaa !93
  %cmp187 = icmp slt i32 %131, 0
  br i1 %cmp187, label %cond.true189, label %cond.false190

cond.true189:                                     ; preds = %if.then184
  br label %cond.end199

cond.false190:                                    ; preds = %if.then184
  %132 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %tileColsLog2191 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %132, i32 0, i32 7
  %133 = load i32, i32* %tileColsLog2191, align 4, !tbaa !93
  %cmp192 = icmp slt i32 6, %133
  br i1 %cmp192, label %cond.true194, label %cond.false195

cond.true194:                                     ; preds = %cond.false190
  br label %cond.end197

cond.false195:                                    ; preds = %cond.false190
  %134 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder.addr, align 4, !tbaa !2
  %tileColsLog2196 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %134, i32 0, i32 7
  %135 = load i32, i32* %tileColsLog2196, align 4, !tbaa !93
  br label %cond.end197

cond.end197:                                      ; preds = %cond.false195, %cond.true194
  %cond198 = phi i32 [ 6, %cond.true194 ], [ %135, %cond.false195 ]
  br label %cond.end199

cond.end199:                                      ; preds = %cond.end197, %cond.true189
  %cond200 = phi i32 [ 0, %cond.true189 ], [ %cond198, %cond.end197 ]
  store i32 %cond200, i32* %tileColsLog2185, align 4, !tbaa !14
  %136 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal201 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %136, i32 0, i32 2
  %137 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal201, align 4, !tbaa !13
  %encoder202 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %137, i32 0, i32 6
  %138 = load i32, i32* %tileColsLog2185, align 4, !tbaa !14
  %call203 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder202, i32 33, i32 %138)
  %139 = bitcast i32* %tileColsLog2185 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  br label %if.end204

if.end204:                                        ; preds = %cond.end199, %if.end181
  %140 = load i32, i32* %aomCpuUsed, align 4, !tbaa !14
  %cmp205 = icmp ne i32 %140, -1
  br i1 %cmp205, label %if.then207, label %if.end211

if.then207:                                       ; preds = %if.end204
  %141 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal208 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %141, i32 0, i32 2
  %142 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal208, align 4, !tbaa !13
  %encoder209 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %142, i32 0, i32 6
  %143 = load i32, i32* %aomCpuUsed, align 4, !tbaa !14
  %call210 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder209, i32 13, i32 %143)
  br label %if.end211

if.end211:                                        ; preds = %if.then207, %if.end204
  %144 = bitcast i32* %encoderFlags to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i32* %lossless to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i32* %maxQuantizer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast i32* %minQuantizer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast %struct.aom_codec_enc_cfg* %cfg to i8*
  call void @llvm.lifetime.end.p0i8(i64 884, i8* %148) #4
  %149 = bitcast %struct.aom_codec_iface** %encoderInterface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end211, %if.then42
  %150 = bitcast i32* %aomVersion to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast i32* %aomCpuUsed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  %152 = bitcast i32* %aomUsage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end214

if.end214:                                        ; preds = %cleanup.cont, %entry
  %153 = bitcast i32* %yShift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %153) #4
  %154 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal215 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %154, i32 0, i32 2
  %155 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal215, align 4, !tbaa !13
  %formatInfo216 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %155, i32 0, i32 7
  %chromaShiftY = getelementptr inbounds %struct.avifPixelFormatInfo, %struct.avifPixelFormatInfo* %formatInfo216, i32 0, i32 2
  %156 = load i32, i32* %chromaShiftY, align 4, !tbaa !94
  store i32 %156, i32* %yShift, align 4, !tbaa !14
  %157 = bitcast i32* %uvHeight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #4
  %158 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height217 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %158, i32 0, i32 1
  %159 = load i32, i32* %height217, align 4, !tbaa !45
  %160 = load i32, i32* %yShift, align 4, !tbaa !14
  %add = add i32 %159, %160
  %161 = load i32, i32* %yShift, align 4, !tbaa !14
  %shr = lshr i32 %add, %161
  store i32 %shr, i32* %uvHeight, align 4, !tbaa !14
  %162 = bitcast %struct.aom_image** %aomImage to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #4
  %163 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal218 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %163, i32 0, i32 2
  %164 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal218, align 4, !tbaa !13
  %aomFormat219 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %164, i32 0, i32 8
  %165 = load i32, i32* %aomFormat219, align 4, !tbaa !69
  %166 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width220 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %166, i32 0, i32 0
  %167 = load i32, i32* %width220, align 4, !tbaa !38
  %168 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height221 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %168, i32 0, i32 1
  %169 = load i32, i32* %height221, align 4, !tbaa !45
  %call222 = call %struct.aom_image* @aom_img_alloc(%struct.aom_image* null, i32 %165, i32 %167, i32 %169, i32 16)
  store %struct.aom_image* %call222, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %170 = bitcast i32* %monochromeRequested to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #4
  store i32 0, i32* %monochromeRequested, align 4, !tbaa !14
  %171 = load i32, i32* %alpha.addr, align 4, !tbaa !14
  %tobool223 = icmp ne i32 %171, 0
  br i1 %tobool223, label %if.then224, label %if.else241

if.then224:                                       ; preds = %if.end214
  %172 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %172, i32 0, i32 9
  %173 = load i32, i32* %alphaRange, align 4, !tbaa !62
  %cmp225 = icmp eq i32 %173, 1
  %174 = zext i1 %cmp225 to i64
  %cond227 = select i1 %cmp225, i32 1, i32 0
  %175 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %range = getelementptr inbounds %struct.aom_image, %struct.aom_image* %175, i32 0, i32 6
  store i32 %cond227, i32* %range, align 4, !tbaa !51
  %176 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal228 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %176, i32 0, i32 2
  %177 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal228, align 4, !tbaa !13
  %encoder229 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %177, i32 0, i32 6
  %178 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %range230 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %178, i32 0, i32 6
  %179 = load i32, i32* %range230, align 4, !tbaa !51
  %call231 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder229, i32 52, i32 %179)
  store i32 1, i32* %monochromeRequested, align 4, !tbaa !14
  %180 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %180) #4
  store i32 0, i32* %j, align 4, !tbaa !14
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then224
  %181 = load i32, i32* %j, align 4, !tbaa !14
  %182 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height232 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %182, i32 0, i32 1
  %183 = load i32, i32* %height232, align 4, !tbaa !45
  %cmp233 = icmp ult i32 %181, %183
  br i1 %cmp233, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %184 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #4
  br label %for.end

for.body:                                         ; preds = %for.cond
  %185 = bitcast i8** %srcAlphaRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #4
  %186 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaPlane = getelementptr inbounds %struct.avifImage, %struct.avifImage* %186, i32 0, i32 10
  %187 = load i8*, i8** %alphaPlane, align 4, !tbaa !60
  %188 = load i32, i32* %j, align 4, !tbaa !14
  %189 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %189, i32 0, i32 11
  %190 = load i32, i32* %alphaRowBytes, align 4, !tbaa !61
  %mul = mul i32 %188, %190
  %arrayidx = getelementptr inbounds i8, i8* %187, i32 %mul
  store i8* %arrayidx, i8** %srcAlphaRow, align 4, !tbaa !2
  %191 = bitcast i8** %dstAlphaRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %191) #4
  %192 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %planes = getelementptr inbounds %struct.aom_image, %struct.aom_image* %192, i32 0, i32 16
  %arrayidx235 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes, i32 0, i32 0
  %193 = load i8*, i8** %arrayidx235, align 4, !tbaa !2
  %194 = load i32, i32* %j, align 4, !tbaa !14
  %195 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %195, i32 0, i32 17
  %arrayidx236 = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 0
  %196 = load i32, i32* %arrayidx236, align 4, !tbaa !14
  %mul237 = mul i32 %194, %196
  %arrayidx238 = getelementptr inbounds i8, i8* %193, i32 %mul237
  store i8* %arrayidx238, i8** %dstAlphaRow, align 4, !tbaa !2
  %197 = load i8*, i8** %dstAlphaRow, align 4, !tbaa !2
  %198 = load i8*, i8** %srcAlphaRow, align 4, !tbaa !2
  %199 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %alphaRowBytes239 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %199, i32 0, i32 11
  %200 = load i32, i32* %alphaRowBytes239, align 4, !tbaa !61
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %197, i8* align 1 %198, i32 %200, i1 false)
  %201 = bitcast i8** %dstAlphaRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #4
  %202 = bitcast i8** %srcAlphaRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %203 = load i32, i32* %j, align 4, !tbaa !14
  %inc = add i32 %203, 1
  store i32 %inc, i32* %j, align 4, !tbaa !14
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end309

if.else241:                                       ; preds = %if.end214
  %204 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRange = getelementptr inbounds %struct.avifImage, %struct.avifImage* %204, i32 0, i32 4
  %205 = load i32, i32* %yuvRange, align 4, !tbaa !52
  %cmp242 = icmp eq i32 %205, 1
  %206 = zext i1 %cmp242 to i64
  %cond244 = select i1 %cmp242, i32 1, i32 0
  %207 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %range245 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %207, i32 0, i32 6
  store i32 %cond244, i32* %range245, align 4, !tbaa !51
  %208 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal246 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %208, i32 0, i32 2
  %209 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal246, align 4, !tbaa !13
  %encoder247 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %209, i32 0, i32 6
  %210 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %range248 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %210, i32 0, i32 6
  %211 = load i32, i32* %range248, align 4, !tbaa !51
  %call249 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder247, i32 52, i32 %211)
  %212 = bitcast i32* %yuvPlaneCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %212) #4
  store i32 3, i32* %yuvPlaneCount, align 4, !tbaa !14
  %213 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat250 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %213, i32 0, i32 3
  %214 = load i32, i32* %yuvFormat250, align 4, !tbaa !50
  %cmp251 = icmp eq i32 %214, 4
  br i1 %cmp251, label %if.then253, label %if.end254

if.then253:                                       ; preds = %if.else241
  store i32 1, i32* %yuvPlaneCount, align 4, !tbaa !14
  store i32 1, i32* %monochromeRequested, align 4, !tbaa !14
  br label %if.end254

if.end254:                                        ; preds = %if.then253, %if.else241
  %215 = bitcast i32* %yuvPlane to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %215) #4
  store i32 0, i32* %yuvPlane, align 4, !tbaa !14
  br label %for.cond255

for.cond255:                                      ; preds = %for.inc289, %if.end254
  %216 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %217 = load i32, i32* %yuvPlaneCount, align 4, !tbaa !14
  %cmp256 = icmp slt i32 %216, %217
  br i1 %cmp256, label %for.body259, label %for.cond.cleanup258

for.cond.cleanup258:                              ; preds = %for.cond255
  store i32 5, i32* %cleanup.dest.slot, align 4
  %218 = bitcast i32* %yuvPlane to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  br label %for.end292

for.body259:                                      ; preds = %for.cond255
  %219 = bitcast i32* %planeHeight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %219) #4
  %220 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %cmp260 = icmp eq i32 %220, 0
  br i1 %cmp260, label %cond.true262, label %cond.false264

cond.true262:                                     ; preds = %for.body259
  %221 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height263 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %221, i32 0, i32 1
  %222 = load i32, i32* %height263, align 4, !tbaa !45
  br label %cond.end265

cond.false264:                                    ; preds = %for.body259
  %223 = load i32, i32* %uvHeight, align 4, !tbaa !14
  br label %cond.end265

cond.end265:                                      ; preds = %cond.false264, %cond.true262
  %cond266 = phi i32 [ %222, %cond.true262 ], [ %223, %cond.false264 ]
  store i32 %cond266, i32* %planeHeight, align 4, !tbaa !14
  %224 = bitcast i32* %j267 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %224) #4
  store i32 0, i32* %j267, align 4, !tbaa !14
  br label %for.cond268

for.cond268:                                      ; preds = %for.inc285, %cond.end265
  %225 = load i32, i32* %j267, align 4, !tbaa !14
  %226 = load i32, i32* %planeHeight, align 4, !tbaa !14
  %cmp269 = icmp ult i32 %225, %226
  br i1 %cmp269, label %for.body272, label %for.cond.cleanup271

for.cond.cleanup271:                              ; preds = %for.cond268
  store i32 8, i32* %cleanup.dest.slot, align 4
  %227 = bitcast i32* %j267 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #4
  br label %for.end288

for.body272:                                      ; preds = %for.cond268
  %228 = bitcast i8** %srcRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %228) #4
  %229 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvPlanes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %229, i32 0, i32 6
  %230 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx273 = getelementptr inbounds [3 x i8*], [3 x i8*]* %yuvPlanes, i32 0, i32 %230
  %231 = load i8*, i8** %arrayidx273, align 4, !tbaa !2
  %232 = load i32, i32* %j267, align 4, !tbaa !14
  %233 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes = getelementptr inbounds %struct.avifImage, %struct.avifImage* %233, i32 0, i32 7
  %234 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx274 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes, i32 0, i32 %234
  %235 = load i32, i32* %arrayidx274, align 4, !tbaa !14
  %mul275 = mul i32 %232, %235
  %arrayidx276 = getelementptr inbounds i8, i8* %231, i32 %mul275
  store i8* %arrayidx276, i8** %srcRow, align 4, !tbaa !2
  %236 = bitcast i8** %dstRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %236) #4
  %237 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %planes277 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %237, i32 0, i32 16
  %238 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx278 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes277, i32 0, i32 %238
  %239 = load i8*, i8** %arrayidx278, align 4, !tbaa !2
  %240 = load i32, i32* %j267, align 4, !tbaa !14
  %241 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %stride279 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %241, i32 0, i32 17
  %242 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx280 = getelementptr inbounds [3 x i32], [3 x i32]* %stride279, i32 0, i32 %242
  %243 = load i32, i32* %arrayidx280, align 4, !tbaa !14
  %mul281 = mul i32 %240, %243
  %arrayidx282 = getelementptr inbounds i8, i8* %239, i32 %mul281
  store i8* %arrayidx282, i8** %dstRow, align 4, !tbaa !2
  %244 = load i8*, i8** %dstRow, align 4, !tbaa !2
  %245 = load i8*, i8** %srcRow, align 4, !tbaa !2
  %246 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvRowBytes283 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %246, i32 0, i32 7
  %247 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %arrayidx284 = getelementptr inbounds [3 x i32], [3 x i32]* %yuvRowBytes283, i32 0, i32 %247
  %248 = load i32, i32* %arrayidx284, align 4, !tbaa !14
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %244, i8* align 1 %245, i32 %248, i1 false)
  %249 = bitcast i8** %dstRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #4
  %250 = bitcast i8** %srcRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #4
  br label %for.inc285

for.inc285:                                       ; preds = %for.body272
  %251 = load i32, i32* %j267, align 4, !tbaa !14
  %inc286 = add i32 %251, 1
  store i32 %inc286, i32* %j267, align 4, !tbaa !14
  br label %for.cond268

for.end288:                                       ; preds = %for.cond.cleanup271
  %252 = bitcast i32* %planeHeight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #4
  br label %for.inc289

for.inc289:                                       ; preds = %for.end288
  %253 = load i32, i32* %yuvPlane, align 4, !tbaa !14
  %inc290 = add nsw i32 %253, 1
  store i32 %inc290, i32* %yuvPlane, align 4, !tbaa !14
  br label %for.cond255

for.end292:                                       ; preds = %for.cond.cleanup258
  %254 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %colorPrimaries = getelementptr inbounds %struct.avifImage, %struct.avifImage* %254, i32 0, i32 14
  %255 = load i32, i32* %colorPrimaries, align 4, !tbaa !54
  %256 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %cp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %256, i32 0, i32 1
  store i32 %255, i32* %cp, align 4, !tbaa !53
  %257 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %transferCharacteristics = getelementptr inbounds %struct.avifImage, %struct.avifImage* %257, i32 0, i32 15
  %258 = load i32, i32* %transferCharacteristics, align 4, !tbaa !56
  %259 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %tc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %259, i32 0, i32 2
  store i32 %258, i32* %tc, align 4, !tbaa !55
  %260 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %260, i32 0, i32 16
  %261 = load i32, i32* %matrixCoefficients, align 4, !tbaa !58
  %262 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %mc = getelementptr inbounds %struct.aom_image, %struct.aom_image* %262, i32 0, i32 3
  store i32 %261, i32* %mc, align 4, !tbaa !57
  %263 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvChromaSamplePosition = getelementptr inbounds %struct.avifImage, %struct.avifImage* %263, i32 0, i32 5
  %264 = load i32, i32* %yuvChromaSamplePosition, align 4, !tbaa !95
  %265 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %csp = getelementptr inbounds %struct.aom_image, %struct.aom_image* %265, i32 0, i32 5
  store i32 %264, i32* %csp, align 4, !tbaa !96
  %266 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal293 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %266, i32 0, i32 2
  %267 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal293, align 4, !tbaa !13
  %encoder294 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %267, i32 0, i32 6
  %268 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %cp295 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %268, i32 0, i32 1
  %269 = load i32, i32* %cp295, align 4, !tbaa !53
  %call296 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder294, i32 45, i32 %269)
  %270 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal297 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %270, i32 0, i32 2
  %271 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal297, align 4, !tbaa !13
  %encoder298 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %271, i32 0, i32 6
  %272 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %tc299 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %272, i32 0, i32 2
  %273 = load i32, i32* %tc299, align 4, !tbaa !55
  %call300 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder298, i32 46, i32 %273)
  %274 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal301 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %274, i32 0, i32 2
  %275 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal301, align 4, !tbaa !13
  %encoder302 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %275, i32 0, i32 6
  %276 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %mc303 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %276, i32 0, i32 3
  %277 = load i32, i32* %mc303, align 4, !tbaa !57
  %call304 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder302, i32 47, i32 %277)
  %278 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal305 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %278, i32 0, i32 2
  %279 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal305, align 4, !tbaa !13
  %encoder306 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %279, i32 0, i32 6
  %280 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %csp307 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %280, i32 0, i32 5
  %281 = load i32, i32* %csp307, align 4, !tbaa !96
  %call308 = call i32 (%struct.aom_codec_ctx*, i32, ...) @aom_codec_control(%struct.aom_codec_ctx* %encoder306, i32 48, i32 %281)
  %282 = bitcast i32* %yuvPlaneCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #4
  br label %if.end309

if.end309:                                        ; preds = %for.end292, %for.end
  %283 = load i32, i32* %monochromeRequested, align 4, !tbaa !14
  %tobool310 = icmp ne i32 %283, 0
  br i1 %tobool310, label %land.lhs.true311, label %if.end374

land.lhs.true311:                                 ; preds = %if.end309
  %284 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal312 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %284, i32 0, i32 2
  %285 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal312, align 4, !tbaa !13
  %monochromeEnabled313 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %285, i32 0, i32 9
  %286 = load i32, i32* %monochromeEnabled313, align 4, !tbaa !89
  %tobool314 = icmp ne i32 %286, 0
  br i1 %tobool314, label %if.end374, label %if.then315

if.then315:                                       ; preds = %land.lhs.true311
  %287 = bitcast i32* %monoUVWidth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %287) #4
  %288 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %width316 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %288, i32 0, i32 0
  %289 = load i32, i32* %width316, align 4, !tbaa !38
  %add317 = add i32 %289, 1
  %shr318 = lshr i32 %add317, 1
  store i32 %shr318, i32* %monoUVWidth, align 4, !tbaa !14
  %290 = bitcast i32* %monoUVHeight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %290) #4
  %291 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %height319 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %291, i32 0, i32 1
  %292 = load i32, i32* %height319, align 4, !tbaa !45
  %add320 = add i32 %292, 1
  %shr321 = lshr i32 %add320, 1
  store i32 %shr321, i32* %monoUVHeight, align 4, !tbaa !14
  %293 = bitcast i32* %yuvPlane322 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %293) #4
  store i32 1, i32* %yuvPlane322, align 4, !tbaa !14
  br label %for.cond323

for.cond323:                                      ; preds = %for.inc370, %if.then315
  %294 = load i32, i32* %yuvPlane322, align 4, !tbaa !14
  %cmp324 = icmp slt i32 %294, 3
  br i1 %cmp324, label %for.body327, label %for.cond.cleanup326

for.cond.cleanup326:                              ; preds = %for.cond323
  store i32 11, i32* %cleanup.dest.slot, align 4
  %295 = bitcast i32* %yuvPlane322 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %295) #4
  br label %for.end373

for.body327:                                      ; preds = %for.cond323
  %296 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth328 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %296, i32 0, i32 2
  %297 = load i32, i32* %depth328, align 4, !tbaa !48
  %cmp329 = icmp ugt i32 %297, 8
  br i1 %cmp329, label %if.then331, label %if.else362

if.then331:                                       ; preds = %for.body327
  %298 = bitcast i16* %half to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %298) #4
  %299 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth332 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %299, i32 0, i32 2
  %300 = load i32, i32* %depth332, align 4, !tbaa !48
  %sub333 = sub i32 %300, 1
  %shl = shl i32 1, %sub333
  %conv334 = trunc i32 %shl to i16
  store i16 %conv334, i16* %half, align 2, !tbaa !97
  %301 = bitcast i32* %j335 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %301) #4
  store i32 0, i32* %j335, align 4, !tbaa !14
  br label %for.cond336

for.cond336:                                      ; preds = %for.inc358, %if.then331
  %302 = load i32, i32* %j335, align 4, !tbaa !14
  %303 = load i32, i32* %monoUVHeight, align 4, !tbaa !14
  %cmp337 = icmp ult i32 %302, %303
  br i1 %cmp337, label %for.body340, label %for.cond.cleanup339

for.cond.cleanup339:                              ; preds = %for.cond336
  store i32 14, i32* %cleanup.dest.slot, align 4
  %304 = bitcast i32* %j335 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %304) #4
  br label %for.end361

for.body340:                                      ; preds = %for.cond336
  %305 = bitcast i16** %dstRow341 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %305) #4
  %306 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %planes342 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %306, i32 0, i32 16
  %307 = load i32, i32* %yuvPlane322, align 4, !tbaa !14
  %arrayidx343 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes342, i32 0, i32 %307
  %308 = load i8*, i8** %arrayidx343, align 4, !tbaa !2
  %309 = load i32, i32* %j335, align 4, !tbaa !14
  %310 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %stride344 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %310, i32 0, i32 17
  %311 = load i32, i32* %yuvPlane322, align 4, !tbaa !14
  %arrayidx345 = getelementptr inbounds [3 x i32], [3 x i32]* %stride344, i32 0, i32 %311
  %312 = load i32, i32* %arrayidx345, align 4, !tbaa !14
  %mul346 = mul i32 %309, %312
  %arrayidx347 = getelementptr inbounds i8, i8* %308, i32 %mul346
  %313 = bitcast i8* %arrayidx347 to i16*
  store i16* %313, i16** %dstRow341, align 4, !tbaa !2
  %314 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %314) #4
  store i32 0, i32* %i, align 4, !tbaa !14
  br label %for.cond348

for.cond348:                                      ; preds = %for.inc354, %for.body340
  %315 = load i32, i32* %i, align 4, !tbaa !14
  %316 = load i32, i32* %monoUVWidth, align 4, !tbaa !14
  %cmp349 = icmp ult i32 %315, %316
  br i1 %cmp349, label %for.body352, label %for.cond.cleanup351

for.cond.cleanup351:                              ; preds = %for.cond348
  store i32 17, i32* %cleanup.dest.slot, align 4
  %317 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #4
  br label %for.end357

for.body352:                                      ; preds = %for.cond348
  %318 = load i16, i16* %half, align 2, !tbaa !97
  %319 = load i16*, i16** %dstRow341, align 4, !tbaa !2
  %320 = load i32, i32* %i, align 4, !tbaa !14
  %arrayidx353 = getelementptr inbounds i16, i16* %319, i32 %320
  store i16 %318, i16* %arrayidx353, align 2, !tbaa !97
  br label %for.inc354

for.inc354:                                       ; preds = %for.body352
  %321 = load i32, i32* %i, align 4, !tbaa !14
  %inc355 = add i32 %321, 1
  store i32 %inc355, i32* %i, align 4, !tbaa !14
  br label %for.cond348

for.end357:                                       ; preds = %for.cond.cleanup351
  %322 = bitcast i16** %dstRow341 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %322) #4
  br label %for.inc358

for.inc358:                                       ; preds = %for.end357
  %323 = load i32, i32* %j335, align 4, !tbaa !14
  %inc359 = add i32 %323, 1
  store i32 %inc359, i32* %j335, align 4, !tbaa !14
  br label %for.cond336

for.end361:                                       ; preds = %for.cond.cleanup339
  %324 = bitcast i16* %half to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %324) #4
  br label %if.end369

if.else362:                                       ; preds = %for.body327
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %half363) #4
  store i8 -128, i8* %half363, align 1, !tbaa !34
  %325 = bitcast i32* %planeSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %325) #4
  %326 = load i32, i32* %monoUVHeight, align 4, !tbaa !14
  %327 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %stride364 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %327, i32 0, i32 17
  %328 = load i32, i32* %yuvPlane322, align 4, !tbaa !14
  %arrayidx365 = getelementptr inbounds [3 x i32], [3 x i32]* %stride364, i32 0, i32 %328
  %329 = load i32, i32* %arrayidx365, align 4, !tbaa !14
  %mul366 = mul i32 %326, %329
  store i32 %mul366, i32* %planeSize, align 4, !tbaa !91
  %330 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %planes367 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %330, i32 0, i32 16
  %331 = load i32, i32* %yuvPlane322, align 4, !tbaa !14
  %arrayidx368 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes367, i32 0, i32 %331
  %332 = load i8*, i8** %arrayidx368, align 4, !tbaa !2
  %333 = load i32, i32* %planeSize, align 4, !tbaa !91
  call void @llvm.memset.p0i8.i32(i8* align 1 %332, i8 -128, i32 %333, i1 false)
  %334 = bitcast i32* %planeSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %half363) #4
  br label %if.end369

if.end369:                                        ; preds = %if.else362, %for.end361
  br label %for.inc370

for.inc370:                                       ; preds = %if.end369
  %335 = load i32, i32* %yuvPlane322, align 4, !tbaa !14
  %inc371 = add nsw i32 %335, 1
  store i32 %inc371, i32* %yuvPlane322, align 4, !tbaa !14
  br label %for.cond323

for.end373:                                       ; preds = %for.cond.cleanup326
  %336 = bitcast i32* %monoUVHeight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #4
  %337 = bitcast i32* %monoUVWidth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #4
  br label %if.end374

if.end374:                                        ; preds = %for.end373, %land.lhs.true311, %if.end309
  %338 = bitcast i32* %encodeFlags to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %338) #4
  store i32 0, i32* %encodeFlags, align 4, !tbaa !91
  %339 = load i32, i32* %addImageFlags.addr, align 4, !tbaa !14
  %and375 = and i32 %339, 1
  %tobool376 = icmp ne i32 %and375, 0
  br i1 %tobool376, label %if.then377, label %if.end379

if.then377:                                       ; preds = %if.end374
  %340 = load i32, i32* %encodeFlags, align 4, !tbaa !91
  %or378 = or i32 %340, 1
  store i32 %or378, i32* %encodeFlags, align 4, !tbaa !91
  br label %if.end379

if.end379:                                        ; preds = %if.then377, %if.end374
  %341 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal380 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %341, i32 0, i32 2
  %342 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal380, align 4, !tbaa !13
  %encoder381 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %342, i32 0, i32 6
  %343 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  %344 = load i32, i32* %encodeFlags, align 4, !tbaa !91
  %call382 = call i32 @aom_codec_encode(%struct.aom_codec_ctx* %encoder381, %struct.aom_image* %343, i64 0, i32 1, i32 %344)
  %345 = bitcast i8** %iter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %345) #4
  store i8* null, i8** %iter, align 4, !tbaa !2
  br label %for.cond383

for.cond383:                                      ; preds = %cleanup.cont402, %if.end379
  %346 = bitcast %struct.aom_codec_cx_pkt** %pkt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %346) #4
  %347 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal384 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %347, i32 0, i32 2
  %348 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal384, align 4, !tbaa !13
  %encoder385 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %348, i32 0, i32 6
  %call386 = call %struct.aom_codec_cx_pkt* @aom_codec_get_cx_data(%struct.aom_codec_ctx* %encoder385, i8** %iter)
  store %struct.aom_codec_cx_pkt* %call386, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %349 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %cmp387 = icmp eq %struct.aom_codec_cx_pkt* %349, null
  br i1 %cmp387, label %if.then389, label %if.end390

if.then389:                                       ; preds = %for.cond383
  store i32 20, i32* %cleanup.dest.slot, align 4
  br label %cleanup400

if.end390:                                        ; preds = %for.cond383
  %350 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %kind = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %350, i32 0, i32 0
  %351 = load i32, i32* %kind, align 8, !tbaa !99
  %cmp391 = icmp eq i32 %351, 0
  br i1 %cmp391, label %if.then393, label %if.end399

if.then393:                                       ; preds = %if.end390
  %352 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %output.addr, align 4, !tbaa !2
  %353 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %353, i32 0, i32 1
  %frame = bitcast %union.anon.0* %data to %struct.anon*
  %buf = getelementptr inbounds %struct.anon, %struct.anon* %frame, i32 0, i32 0
  %354 = load i8*, i8** %buf, align 8, !tbaa !34
  %355 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data394 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %355, i32 0, i32 1
  %frame395 = bitcast %union.anon.0* %data394 to %struct.anon*
  %sz = getelementptr inbounds %struct.anon, %struct.anon* %frame395, i32 0, i32 1
  %356 = load i32, i32* %sz, align 4, !tbaa !34
  %357 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data396 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %357, i32 0, i32 1
  %frame397 = bitcast %union.anon.0* %data396 to %struct.anon*
  %flags = getelementptr inbounds %struct.anon, %struct.anon* %frame397, i32 0, i32 4
  %358 = load i32, i32* %flags, align 4, !tbaa !34
  %and398 = and i32 %358, 1
  call void @avifCodecEncodeOutputAddSample(%struct.avifCodecEncodeOutput* %352, i8* %354, i32 %356, i32 %and398)
  br label %if.end399

if.end399:                                        ; preds = %if.then393, %if.end390
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup400

cleanup400:                                       ; preds = %if.end399, %if.then389
  %359 = bitcast %struct.aom_codec_cx_pkt** %pkt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %359) #4
  %cleanup.dest401 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest401, label %unreachable [
    i32 0, label %cleanup.cont402
    i32 20, label %for.end403
  ]

cleanup.cont402:                                  ; preds = %cleanup400
  br label %for.cond383

for.end403:                                       ; preds = %cleanup400
  %360 = load %struct.aom_image*, %struct.aom_image** %aomImage, align 4, !tbaa !2
  call void @aom_img_free(%struct.aom_image* %360)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %361 = bitcast i8** %iter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %361) #4
  %362 = bitcast i32* %encodeFlags to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %362) #4
  %363 = bitcast i32* %monochromeRequested to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %363) #4
  %364 = bitcast %struct.aom_image** %aomImage to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %364) #4
  %365 = bitcast i32* %uvHeight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %365) #4
  %366 = bitcast i32* %yShift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %366) #4
  br label %return

return:                                           ; preds = %for.end403, %cleanup
  %367 = load i32, i32* %retval, align 4
  ret i32 %367

unreachable:                                      ; preds = %cleanup400, %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal i32 @aomCodecEncodeFinish(%struct.avifCodec* %codec, %struct.avifCodecEncodeOutput* %output) #0 {
entry:
  %codec.addr = alloca %struct.avifCodec*, align 4
  %output.addr = alloca %struct.avifCodecEncodeOutput*, align 4
  %gotPacket = alloca i32, align 4
  %iter = alloca i8*, align 4
  %pkt = alloca %struct.aom_codec_cx_pkt*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifCodec* %codec, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  store %struct.avifCodecEncodeOutput* %output, %struct.avifCodecEncodeOutput** %output.addr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont17, %entry
  %0 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %0, i32 0, i32 2
  %1 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal, align 4, !tbaa !13
  %encoder = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %1, i32 0, i32 6
  %call = call i32 @aom_codec_encode(%struct.aom_codec_ctx* %encoder, %struct.aom_image* null, i64 0, i32 1, i32 0)
  %2 = bitcast i32* %gotPacket to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  store i32 0, i32* %gotPacket, align 4, !tbaa !14
  %3 = bitcast i8** %iter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  store i8* null, i8** %iter, align 4, !tbaa !2
  br label %for.cond1

for.cond1:                                        ; preds = %cleanup.cont, %for.cond
  %4 = bitcast %struct.aom_codec_cx_pkt** %pkt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal2 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %5, i32 0, i32 2
  %6 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal2, align 4, !tbaa !13
  %encoder3 = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %6, i32 0, i32 6
  %call4 = call %struct.aom_codec_cx_pkt* @aom_codec_get_cx_data(%struct.aom_codec_ctx* %encoder3, i8** %iter)
  store %struct.aom_codec_cx_pkt* %call4, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %7 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %cmp = icmp eq %struct.aom_codec_cx_pkt* %7, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %for.cond1
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.cond1
  %8 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %kind = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %8, i32 0, i32 0
  %9 = load i32, i32* %kind, align 8, !tbaa !99
  %cmp5 = icmp eq i32 %9, 0
  br i1 %cmp5, label %if.then6, label %if.end11

if.then6:                                         ; preds = %if.end
  store i32 1, i32* %gotPacket, align 4, !tbaa !14
  %10 = load %struct.avifCodecEncodeOutput*, %struct.avifCodecEncodeOutput** %output.addr, align 4, !tbaa !2
  %11 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %11, i32 0, i32 1
  %frame = bitcast %union.anon.0* %data to %struct.anon*
  %buf = getelementptr inbounds %struct.anon, %struct.anon* %frame, i32 0, i32 0
  %12 = load i8*, i8** %buf, align 8, !tbaa !34
  %13 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data7 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %13, i32 0, i32 1
  %frame8 = bitcast %union.anon.0* %data7 to %struct.anon*
  %sz = getelementptr inbounds %struct.anon, %struct.anon* %frame8, i32 0, i32 1
  %14 = load i32, i32* %sz, align 4, !tbaa !34
  %15 = load %struct.aom_codec_cx_pkt*, %struct.aom_codec_cx_pkt** %pkt, align 4, !tbaa !2
  %data9 = getelementptr inbounds %struct.aom_codec_cx_pkt, %struct.aom_codec_cx_pkt* %15, i32 0, i32 1
  %frame10 = bitcast %union.anon.0* %data9 to %struct.anon*
  %flags = getelementptr inbounds %struct.anon, %struct.anon* %frame10, i32 0, i32 4
  %16 = load i32, i32* %flags, align 4, !tbaa !34
  %and = and i32 %16, 1
  call void @avifCodecEncodeOutputAddSample(%struct.avifCodecEncodeOutput* %10, i8* %12, i32 %14, i32 %and)
  br label %if.end11

if.end11:                                         ; preds = %if.then6, %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end11, %if.then
  %17 = bitcast %struct.aom_codec_cx_pkt** %pkt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond1

for.end:                                          ; preds = %cleanup
  %18 = load i32, i32* %gotPacket, align 4, !tbaa !14
  %tobool = icmp ne i32 %18, 0
  br i1 %tobool, label %if.end13, label %if.then12

if.then12:                                        ; preds = %for.end
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

if.end13:                                         ; preds = %for.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

cleanup14:                                        ; preds = %if.end13, %if.then12
  %19 = bitcast i8** %iter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %20 = bitcast i32* %gotPacket to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %cleanup.dest16 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest16, label %unreachable [
    i32 0, label %cleanup.cont17
    i32 2, label %for.end18
  ]

cleanup.cont17:                                   ; preds = %cleanup14
  br label %for.cond

for.end18:                                        ; preds = %cleanup14
  ret i32 1

unreachable:                                      ; preds = %cleanup14, %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @aomCodecDestroyInternal(%struct.avifCodec* %codec) #0 {
entry:
  %codec.addr = alloca %struct.avifCodec*, align 4
  store %struct.avifCodec* %codec, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %0 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %0, i32 0, i32 2
  %1 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal, align 4, !tbaa !13
  %decoderInitialized = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %1, i32 0, i32 0
  %2 = load i32, i32* %decoderInitialized, align 4, !tbaa !16
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal1 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %3, i32 0, i32 2
  %4 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal1, align 4, !tbaa !13
  %decoder = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %4, i32 0, i32 1
  %call = call i32 @aom_codec_destroy(%struct.aom_codec_ctx* %decoder)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal2 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %5, i32 0, i32 2
  %6 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal2, align 4, !tbaa !13
  %encoderInitialized = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %6, i32 0, i32 5
  %7 = load i32, i32* %encoderInitialized, align 4, !tbaa !64
  %tobool3 = icmp ne i32 %7, 0
  br i1 %tobool3, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.end
  %8 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal5 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %8, i32 0, i32 2
  %9 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal5, align 4, !tbaa !13
  %encoder = getelementptr inbounds %struct.avifCodecInternal, %struct.avifCodecInternal* %9, i32 0, i32 6
  %call6 = call i32 @aom_codec_destroy(%struct.aom_codec_ctx* %encoder)
  br label %if.end7

if.end7:                                          ; preds = %if.then4, %if.end
  %10 = load %struct.avifCodec*, %struct.avifCodec** %codec.addr, align 4, !tbaa !2
  %internal8 = getelementptr inbounds %struct.avifCodec, %struct.avifCodec* %10, i32 0, i32 2
  %11 = load %struct.avifCodecInternal*, %struct.avifCodecInternal** %internal8, align 4, !tbaa !13
  %12 = bitcast %struct.avifCodecInternal* %11 to i8*
  call void @avifFree(i8* %12)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

declare %struct.aom_codec_iface* @aom_codec_av1_dx() #1

declare i32 @aom_codec_dec_init_ver(%struct.aom_codec_ctx*, %struct.aom_codec_iface*, %struct.aom_codec_dec_cfg*, i32, i32) #1

declare i32 @aom_codec_control(%struct.aom_codec_ctx*, i32, ...) #1

declare %struct.aom_image* @aom_codec_get_frame(%struct.aom_codec_ctx*, i8**) #1

declare i32 @aom_codec_decode(%struct.aom_codec_ctx*, i8*, i32, i8*) #1

declare void @avifImageFreePlanes(%struct.avifImage*, i32) #1

declare void @avifGetPixelFormatInfo(i32, %struct.avifPixelFormatInfo*) #1

declare i32 @aom_codec_version() #1

; Function Attrs: nounwind
define internal i32 @avifImageCalcAOMFmt(%struct.avifImage* %image, i32 %alpha) #0 {
entry:
  %retval = alloca i32, align 4
  %image.addr = alloca %struct.avifImage*, align 4
  %alpha.addr = alloca i32, align 4
  %fmt = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.avifImage* %image, %struct.avifImage** %image.addr, align 4, !tbaa !2
  store i32 %alpha, i32* %alpha.addr, align 4, !tbaa !14
  %0 = bitcast i32* %fmt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %alpha.addr, align 4, !tbaa !14
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 258, i32* %fmt, align 4, !tbaa !34
  br label %if.end

if.else:                                          ; preds = %entry
  %2 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %yuvFormat = getelementptr inbounds %struct.avifImage, %struct.avifImage* %2, i32 0, i32 3
  %3 = load i32, i32* %yuvFormat, align 4, !tbaa !50
  switch i32 %3, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 3, label %sw.bb2
    i32 4, label %sw.bb2
    i32 0, label %sw.bb3
  ]

sw.bb:                                            ; preds = %if.else
  store i32 262, i32* %fmt, align 4, !tbaa !34
  br label %sw.epilog

sw.bb1:                                           ; preds = %if.else
  store i32 261, i32* %fmt, align 4, !tbaa !34
  br label %sw.epilog

sw.bb2:                                           ; preds = %if.else, %if.else
  store i32 258, i32* %fmt, align 4, !tbaa !34
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.else
  br label %sw.default

sw.default:                                       ; preds = %if.else, %sw.bb3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

sw.epilog:                                        ; preds = %sw.bb2, %sw.bb1, %sw.bb
  br label %if.end

if.end:                                           ; preds = %sw.epilog, %if.then
  %4 = load %struct.avifImage*, %struct.avifImage** %image.addr, align 4, !tbaa !2
  %depth = getelementptr inbounds %struct.avifImage, %struct.avifImage* %4, i32 0, i32 2
  %5 = load i32, i32* %depth, align 4, !tbaa !48
  %cmp = icmp ugt i32 %5, 8
  br i1 %cmp, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %6 = load i32, i32* %fmt, align 4, !tbaa !34
  %or = or i32 %6, 2048
  store i32 %or, i32* %fmt, align 4, !tbaa !34
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.end
  %7 = load i32, i32* %fmt, align 4, !tbaa !34
  store i32 %7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end5, %sw.default
  %8 = bitcast i32* %fmt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

declare %struct.aom_codec_iface* @aom_codec_av1_cx() #1

declare i32 @aom_codec_enc_config_default(%struct.aom_codec_iface*, %struct.aom_codec_enc_cfg*, i32) #1

declare i32 @aom_codec_enc_init_ver(%struct.aom_codec_ctx*, %struct.aom_codec_iface*, %struct.aom_codec_enc_cfg*, i32, i32) #1

declare %struct.aom_image* @aom_img_alloc(%struct.aom_image*, i32, i32, i32, i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

declare i32 @aom_codec_encode(%struct.aom_codec_ctx*, %struct.aom_image*, i64, i32, i32) #1

declare %struct.aom_codec_cx_pkt* @aom_codec_get_cx_data(%struct.aom_codec_ctx*, i8**) #1

declare void @avifCodecEncodeOutputAddSample(%struct.avifCodecEncodeOutput*, i8*, i32, i32) #1

declare void @aom_img_free(%struct.aom_image*) #1

declare i32 @aom_codec_destroy(%struct.aom_codec_ctx*) #1

declare void @avifFree(i8*) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 20}
!7 = !{!"avifCodec", !3, i64 0, !8, i64 4, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36}
!8 = !{!"avifCodecConfigurationBox", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3, !4, i64 4, !4, i64 5, !4, i64 6, !4, i64 7, !4, i64 8}
!9 = !{!7, !3, i64 24}
!10 = !{!7, !3, i64 28}
!11 = !{!7, !3, i64 32}
!12 = !{!7, !3, i64 36}
!13 = !{!7, !3, i64 16}
!14 = !{!15, !15, i64 0}
!15 = !{!"int", !4, i64 0}
!16 = !{!17, !15, i64 0}
!17 = !{!"avifCodecInternal", !15, i64 0, !18, i64 4, !3, i64 32, !15, i64 36, !3, i64 40, !15, i64 44, !18, i64 48, !20, i64 76, !4, i64 88, !15, i64 92}
!18 = !{!"aom_codec_ctx", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 12, !19, i64 16, !4, i64 20, !3, i64 24}
!19 = !{!"long", !4, i64 0}
!20 = !{!"avifPixelFormatInfo", !15, i64 0, !15, i64 4, !15, i64 8}
!21 = !{!17, !15, i64 36}
!22 = !{!17, !3, i64 32}
!23 = !{!7, !3, i64 0}
!24 = !{!25, !15, i64 8}
!25 = !{!"avifCodecDecodeInput", !26, i64 0, !15, i64 16}
!26 = !{!"avifDecodeSampleArray", !3, i64 0, !15, i64 4, !15, i64 8, !15, i64 12}
!27 = !{!25, !3, i64 0}
!28 = !{!29, !3, i64 0}
!29 = !{!"avifDecodeSample", !30, i64 0, !15, i64 8}
!30 = !{!"avifROData", !3, i64 0, !19, i64 4}
!31 = !{!29, !19, i64 4}
!32 = !{!17, !3, i64 40}
!33 = !{!25, !15, i64 16}
!34 = !{!4, !4, i64 0}
!35 = !{!36, !4, i64 0}
!36 = !{!"aom_image", !4, i64 0, !4, i64 4, !4, i64 8, !4, i64 12, !15, i64 16, !4, i64 20, !4, i64 24, !15, i64 28, !15, i64 32, !15, i64 36, !15, i64 40, !15, i64 44, !15, i64 48, !15, i64 52, !15, i64 56, !15, i64 60, !4, i64 64, !4, i64 76, !19, i64 88, !15, i64 92, !15, i64 96, !15, i64 100, !3, i64 104, !3, i64 108, !15, i64 112, !15, i64 116, !3, i64 120, !3, i64 124}
!37 = !{!36, !15, i64 16}
!38 = !{!39, !15, i64 0}
!39 = !{!"avifImage", !15, i64 0, !15, i64 4, !15, i64 8, !4, i64 12, !4, i64 16, !4, i64 20, !4, i64 24, !4, i64 36, !15, i64 48, !4, i64 52, !3, i64 56, !15, i64 60, !15, i64 64, !40, i64 68, !4, i64 76, !4, i64 80, !4, i64 84, !15, i64 88, !41, i64 92, !42, i64 100, !43, i64 132, !44, i64 133, !40, i64 136, !40, i64 144}
!40 = !{!"avifRWData", !3, i64 0, !19, i64 4}
!41 = !{!"avifPixelAspectRatioBox", !15, i64 0, !15, i64 4}
!42 = !{!"avifCleanApertureBox", !15, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !15, i64 20, !15, i64 24, !15, i64 28}
!43 = !{!"avifImageRotation", !4, i64 0}
!44 = !{!"avifImageMirror", !4, i64 0}
!45 = !{!39, !15, i64 4}
!46 = !{!36, !15, i64 40}
!47 = !{!36, !15, i64 44}
!48 = !{!39, !15, i64 8}
!49 = !{!36, !15, i64 36}
!50 = !{!39, !4, i64 12}
!51 = !{!36, !4, i64 24}
!52 = !{!39, !4, i64 16}
!53 = !{!36, !4, i64 4}
!54 = !{!39, !4, i64 76}
!55 = !{!36, !4, i64 8}
!56 = !{!39, !4, i64 80}
!57 = !{!36, !4, i64 12}
!58 = !{!39, !4, i64 84}
!59 = !{!39, !15, i64 48}
!60 = !{!39, !3, i64 56}
!61 = !{!39, !15, i64 60}
!62 = !{!39, !4, i64 52}
!63 = !{!39, !15, i64 64}
!64 = !{!17, !15, i64 44}
!65 = !{!66, !15, i64 32}
!66 = !{!"avifEncoder", !4, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !15, i64 20, !15, i64 24, !15, i64 28, !15, i64 32, !15, i64 36, !67, i64 40, !68, i64 48, !3, i64 56}
!67 = !{!"long long", !4, i64 0}
!68 = !{!"avifIOStats", !19, i64 0, !19, i64 4}
!69 = !{!17, !4, i64 88}
!70 = !{!7, !4, i64 4}
!71 = !{!72, !15, i64 8}
!72 = !{!"aom_codec_enc_cfg", !15, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !15, i64 20, !15, i64 24, !15, i64 28, !4, i64 32, !15, i64 36, !73, i64 40, !15, i64 48, !4, i64 52, !15, i64 56, !15, i64 60, !15, i64 64, !15, i64 68, !15, i64 72, !15, i64 76, !15, i64 80, !15, i64 84, !15, i64 88, !15, i64 92, !4, i64 96, !74, i64 100, !74, i64 108, !15, i64 116, !15, i64 120, !15, i64 124, !15, i64 128, !15, i64 132, !15, i64 136, !15, i64 140, !15, i64 144, !15, i64 148, !15, i64 152, !15, i64 156, !15, i64 160, !4, i64 164, !15, i64 168, !15, i64 172, !15, i64 176, !15, i64 180, !15, i64 184, !15, i64 188, !15, i64 192, !15, i64 196, !15, i64 200, !15, i64 204, !4, i64 208, !4, i64 464, !15, i64 720, !4, i64 724, !75, i64 744}
!73 = !{!"aom_rational", !15, i64 0, !15, i64 4}
!74 = !{!"aom_fixed_buf", !3, i64 0, !19, i64 4}
!75 = !{!"cfg_options", !15, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !15, i64 20, !15, i64 24, !15, i64 28, !15, i64 32, !15, i64 36, !15, i64 40, !15, i64 44, !15, i64 48, !15, i64 52, !15, i64 56, !15, i64 60, !15, i64 64, !15, i64 68, !15, i64 72, !15, i64 76, !15, i64 80, !15, i64 84, !15, i64 88, !15, i64 92, !15, i64 96, !15, i64 100, !15, i64 104, !15, i64 108, !15, i64 112, !15, i64 116, !15, i64 120, !15, i64 124, !15, i64 128, !15, i64 132, !15, i64 136}
!76 = !{!72, !4, i64 32}
!77 = !{!72, !15, i64 36}
!78 = !{!72, !15, i64 12}
!79 = !{!72, !15, i64 16}
!80 = !{!72, !15, i64 20}
!81 = !{!66, !15, i64 4}
!82 = !{!72, !15, i64 4}
!83 = !{!66, !15, i64 8}
!84 = !{!66, !15, i64 12}
!85 = !{!66, !15, i64 16}
!86 = !{!66, !15, i64 20}
!87 = !{!72, !15, i64 120}
!88 = !{!72, !15, i64 124}
!89 = !{!17, !15, i64 92}
!90 = !{!72, !15, i64 188}
!91 = !{!19, !19, i64 0}
!92 = !{!66, !15, i64 24}
!93 = !{!66, !15, i64 28}
!94 = !{!17, !15, i64 84}
!95 = !{!39, !4, i64 20}
!96 = !{!36, !4, i64 20}
!97 = !{!98, !98, i64 0}
!98 = !{!"short", !4, i64 0}
!99 = !{!100, !4, i64 0}
!100 = !{!"aom_codec_cx_pkt", !4, i64 0, !4, i64 8}
