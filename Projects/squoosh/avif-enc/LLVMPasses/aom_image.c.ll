; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_image.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom/src/aom_image.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_image = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i32, i32, i8*, i8*, i32, i32, %struct.aom_metadata_array*, i8* }
%struct.aom_metadata_array = type { i32, %struct.aom_metadata** }
%struct.aom_metadata = type { i32, i8*, i32, i32 }

; Function Attrs: nounwind
define hidden %struct.aom_image* @aom_img_alloc(%struct.aom_image* %img, i32 %fmt, i32 %d_w, i32 %d_h, i32 %align) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  %fmt.addr = alloca i32, align 4
  %d_w.addr = alloca i32, align 4
  %d_h.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %fmt, i32* %fmt.addr, align 4, !tbaa !6
  store i32 %d_w, i32* %d_w.addr, align 4, !tbaa !7
  store i32 %d_h, i32* %d_h.addr, align 4, !tbaa !7
  store i32 %align, i32* %align.addr, align 4, !tbaa !7
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %1 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  %2 = load i32, i32* %d_w.addr, align 4, !tbaa !7
  %3 = load i32, i32* %d_h.addr, align 4, !tbaa !7
  %4 = load i32, i32* %align.addr, align 4, !tbaa !7
  %5 = load i32, i32* %align.addr, align 4, !tbaa !7
  %call = call %struct.aom_image* @img_alloc_helper(%struct.aom_image* %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 1, i32 0, i8* null, i8* (i8*, i32)* null, i8* null)
  ret %struct.aom_image* %call
}

; Function Attrs: nounwind
define internal %struct.aom_image* @img_alloc_helper(%struct.aom_image* %img, i32 %fmt, i32 %d_w, i32 %d_h, i32 %buf_align, i32 %stride_align, i32 %size_align, i32 %border, i8* %img_data, i8* (i8*, i32)* %alloc_cb, i8* %cb_priv) #0 {
entry:
  %retval = alloca %struct.aom_image*, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  %fmt.addr = alloca i32, align 4
  %d_w.addr = alloca i32, align 4
  %d_h.addr = alloca i32, align 4
  %buf_align.addr = alloca i32, align 4
  %stride_align.addr = alloca i32, align 4
  %size_align.addr = alloca i32, align 4
  %border.addr = alloca i32, align 4
  %img_data.addr = alloca i8*, align 4
  %alloc_cb.addr = alloca i8* (i8*, i32)*, align 4
  %cb_priv.addr = alloca i8*, align 4
  %h = alloca i32, align 4
  %w = alloca i32, align 4
  %s = alloca i32, align 4
  %xcs = alloca i32, align 4
  %ycs = alloca i32, align 4
  %bps = alloca i32, align 4
  %bit_depth = alloca i32, align 4
  %stride_in_bytes = alloca i32, align 4
  %alloc_size = alloca i64, align 8
  %padded_alloc_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %fmt, i32* %fmt.addr, align 4, !tbaa !6
  store i32 %d_w, i32* %d_w.addr, align 4, !tbaa !7
  store i32 %d_h, i32* %d_h.addr, align 4, !tbaa !7
  store i32 %buf_align, i32* %buf_align.addr, align 4, !tbaa !7
  store i32 %stride_align, i32* %stride_align.addr, align 4, !tbaa !7
  store i32 %size_align, i32* %size_align.addr, align 4, !tbaa !7
  store i32 %border, i32* %border.addr, align 4, !tbaa !7
  store i8* %img_data, i8** %img_data.addr, align 4, !tbaa !2
  store i8* (i8*, i32)* %alloc_cb, i8* (i8*, i32)** %alloc_cb.addr, align 4, !tbaa !2
  store i8* %cb_priv, i8** %cb_priv.addr, align 4, !tbaa !2
  %0 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = bitcast i32* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = bitcast i32* %xcs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  %4 = bitcast i32* %ycs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #5
  %5 = bitcast i32* %bps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #5
  %6 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = bitcast i32* %stride_in_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #5
  %8 = load i32, i32* %buf_align.addr, align 4, !tbaa !7
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %buf_align.addr, align 4, !tbaa !7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load i32, i32* %buf_align.addr, align 4, !tbaa !7
  %10 = load i32, i32* %buf_align.addr, align 4, !tbaa !7
  %sub = sub i32 %10, 1
  %and = and i32 %9, %sub
  %tobool1 = icmp ne i32 %and, 0
  br i1 %tobool1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  br label %fail

if.end3:                                          ; preds = %if.end
  %11 = load i32, i32* %stride_align.addr, align 4, !tbaa !7
  %tobool4 = icmp ne i32 %11, 0
  br i1 %tobool4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end3
  store i32 1, i32* %stride_align.addr, align 4, !tbaa !7
  br label %if.end6

if.end6:                                          ; preds = %if.then5, %if.end3
  %12 = load i32, i32* %stride_align.addr, align 4, !tbaa !7
  %13 = load i32, i32* %stride_align.addr, align 4, !tbaa !7
  %sub7 = sub i32 %13, 1
  %and8 = and i32 %12, %sub7
  %tobool9 = icmp ne i32 %and8, 0
  br i1 %tobool9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end6
  br label %fail

if.end11:                                         ; preds = %if.end6
  %14 = load i32, i32* %size_align.addr, align 4, !tbaa !7
  %tobool12 = icmp ne i32 %14, 0
  br i1 %tobool12, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.end11
  store i32 1, i32* %size_align.addr, align 4, !tbaa !7
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.end11
  %15 = load i32, i32* %size_align.addr, align 4, !tbaa !7
  %16 = load i32, i32* %size_align.addr, align 4, !tbaa !7
  %sub15 = sub i32 %16, 1
  %and16 = and i32 %15, %sub15
  %tobool17 = icmp ne i32 %and16, 0
  br i1 %tobool17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end14
  br label %fail

if.end19:                                         ; preds = %if.end14
  %17 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  switch i32 %17, label %sw.default [
    i32 258, label %sw.bb
    i32 769, label %sw.bb
    i32 260, label %sw.bb
    i32 771, label %sw.bb
    i32 261, label %sw.bb20
    i32 262, label %sw.bb21
    i32 2817, label %sw.bb22
    i32 2306, label %sw.bb22
    i32 2309, label %sw.bb23
    i32 2310, label %sw.bb24
  ]

sw.bb:                                            ; preds = %if.end19, %if.end19, %if.end19, %if.end19
  store i32 12, i32* %bps, align 4, !tbaa !7
  br label %sw.epilog

sw.bb20:                                          ; preds = %if.end19
  store i32 16, i32* %bps, align 4, !tbaa !7
  br label %sw.epilog

sw.bb21:                                          ; preds = %if.end19
  store i32 24, i32* %bps, align 4, !tbaa !7
  br label %sw.epilog

sw.bb22:                                          ; preds = %if.end19, %if.end19
  store i32 24, i32* %bps, align 4, !tbaa !7
  br label %sw.epilog

sw.bb23:                                          ; preds = %if.end19
  store i32 32, i32* %bps, align 4, !tbaa !7
  br label %sw.epilog

sw.bb24:                                          ; preds = %if.end19
  store i32 48, i32* %bps, align 4, !tbaa !7
  br label %sw.epilog

sw.default:                                       ; preds = %if.end19
  store i32 16, i32* %bps, align 4, !tbaa !7
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb24, %sw.bb23, %sw.bb22, %sw.bb21, %sw.bb20, %sw.bb
  %18 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  %and25 = and i32 %18, 2048
  %tobool26 = icmp ne i32 %and25, 0
  %19 = zext i1 %tobool26 to i64
  %cond = select i1 %tobool26, i32 16, i32 8
  store i32 %cond, i32* %bit_depth, align 4, !tbaa !7
  %20 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  switch i32 %20, label %sw.default28 [
    i32 258, label %sw.bb27
    i32 769, label %sw.bb27
    i32 260, label %sw.bb27
    i32 771, label %sw.bb27
    i32 261, label %sw.bb27
    i32 2306, label %sw.bb27
    i32 2817, label %sw.bb27
    i32 2309, label %sw.bb27
  ]

sw.bb27:                                          ; preds = %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog
  store i32 1, i32* %xcs, align 4, !tbaa !7
  br label %sw.epilog29

sw.default28:                                     ; preds = %sw.epilog
  store i32 0, i32* %xcs, align 4, !tbaa !7
  br label %sw.epilog29

sw.epilog29:                                      ; preds = %sw.default28, %sw.bb27
  %21 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  switch i32 %21, label %sw.default31 [
    i32 258, label %sw.bb30
    i32 769, label %sw.bb30
    i32 260, label %sw.bb30
    i32 771, label %sw.bb30
    i32 2817, label %sw.bb30
    i32 2306, label %sw.bb30
  ]

sw.bb30:                                          ; preds = %sw.epilog29, %sw.epilog29, %sw.epilog29, %sw.epilog29, %sw.epilog29, %sw.epilog29
  store i32 1, i32* %ycs, align 4, !tbaa !7
  br label %sw.epilog32

sw.default31:                                     ; preds = %sw.epilog29
  store i32 0, i32* %ycs, align 4, !tbaa !7
  br label %sw.epilog32

sw.epilog32:                                      ; preds = %sw.default31, %sw.bb30
  %22 = load i32, i32* %d_w.addr, align 4, !tbaa !7
  %23 = load i32, i32* %xcs, align 4, !tbaa !7
  %24 = load i32, i32* %size_align.addr, align 4, !tbaa !7
  %call = call i32 @align_image_dimension(i32 %22, i32 %23, i32 %24)
  store i32 %call, i32* %w, align 4, !tbaa !7
  %25 = load i32, i32* %d_h.addr, align 4, !tbaa !7
  %26 = load i32, i32* %ycs, align 4, !tbaa !7
  %27 = load i32, i32* %size_align.addr, align 4, !tbaa !7
  %call33 = call i32 @align_image_dimension(i32 %25, i32 %26, i32 %27)
  store i32 %call33, i32* %h, align 4, !tbaa !7
  %28 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  %and34 = and i32 %28, 256
  %tobool35 = icmp ne i32 %and34, 0
  br i1 %tobool35, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.epilog32
  %29 = load i32, i32* %w, align 4, !tbaa !7
  br label %cond.end

cond.false:                                       ; preds = %sw.epilog32
  %30 = load i32, i32* %bps, align 4, !tbaa !7
  %31 = load i32, i32* %w, align 4, !tbaa !7
  %mul = mul i32 %30, %31
  %32 = load i32, i32* %bit_depth, align 4, !tbaa !7
  %div = udiv i32 %mul, %32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond36 = phi i32 [ %29, %cond.true ], [ %div, %cond.false ]
  store i32 %cond36, i32* %s, align 4, !tbaa !7
  %33 = load i32, i32* %s, align 4, !tbaa !7
  %34 = load i32, i32* %border.addr, align 4, !tbaa !7
  %mul37 = mul i32 2, %34
  %add = add i32 %33, %mul37
  %35 = load i32, i32* %stride_align.addr, align 4, !tbaa !7
  %add38 = add i32 %add, %35
  %sub39 = sub i32 %add38, 1
  %36 = load i32, i32* %stride_align.addr, align 4, !tbaa !7
  %sub40 = sub i32 %36, 1
  %neg = xor i32 %sub40, -1
  %and41 = and i32 %sub39, %neg
  store i32 %and41, i32* %s, align 4, !tbaa !7
  %37 = load i32, i32* %s, align 4, !tbaa !7
  %38 = load i32, i32* %bit_depth, align 4, !tbaa !7
  %mul42 = mul i32 %37, %38
  %div43 = udiv i32 %mul42, 8
  store i32 %div43, i32* %stride_in_bytes, align 4, !tbaa !7
  %39 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool44 = icmp ne %struct.aom_image* %39, null
  br i1 %tobool44, label %if.else, label %if.then45

if.then45:                                        ; preds = %cond.end
  %call46 = call i8* @calloc(i32 1, i32 128)
  %40 = bitcast i8* %call46 to %struct.aom_image*
  store %struct.aom_image* %40, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %41 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool47 = icmp ne %struct.aom_image* %41, null
  br i1 %tobool47, label %if.end49, label %if.then48

if.then48:                                        ; preds = %if.then45
  br label %fail

if.end49:                                         ; preds = %if.then45
  %42 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %self_allocd = getelementptr inbounds %struct.aom_image, %struct.aom_image* %42, i32 0, i32 25
  store i32 1, i32* %self_allocd, align 4, !tbaa !9
  br label %if.end50

if.else:                                          ; preds = %cond.end
  %43 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %44 = bitcast %struct.aom_image* %43 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %44, i8 0, i32 128, i1 false)
  br label %if.end50

if.end50:                                         ; preds = %if.else, %if.end49
  %45 = load i8*, i8** %img_data.addr, align 4, !tbaa !2
  %46 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data51 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %46, i32 0, i32 23
  store i8* %45, i8** %img_data51, align 4, !tbaa !12
  %47 = load i8*, i8** %img_data.addr, align 4, !tbaa !2
  %tobool52 = icmp ne i8* %47, null
  br i1 %tobool52, label %if.end103, label %if.then53

if.then53:                                        ; preds = %if.end50
  %48 = bitcast i64* %alloc_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %48) #5
  %49 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  %and54 = and i32 %49, 256
  %tobool55 = icmp ne i32 %and54, 0
  br i1 %tobool55, label %cond.true56, label %cond.false65

cond.true56:                                      ; preds = %if.then53
  %50 = load i32, i32* %h, align 4, !tbaa !7
  %51 = load i32, i32* %border.addr, align 4, !tbaa !7
  %mul57 = mul i32 2, %51
  %add58 = add i32 %50, %mul57
  %conv = zext i32 %add58 to i64
  %52 = load i32, i32* %stride_in_bytes, align 4, !tbaa !7
  %conv59 = zext i32 %52 to i64
  %mul60 = mul i64 %conv, %conv59
  %53 = load i32, i32* %bps, align 4, !tbaa !7
  %conv61 = zext i32 %53 to i64
  %mul62 = mul i64 %mul60, %conv61
  %54 = load i32, i32* %bit_depth, align 4, !tbaa !7
  %conv63 = zext i32 %54 to i64
  %div64 = udiv i64 %mul62, %conv63
  br label %cond.end71

cond.false65:                                     ; preds = %if.then53
  %55 = load i32, i32* %h, align 4, !tbaa !7
  %56 = load i32, i32* %border.addr, align 4, !tbaa !7
  %mul66 = mul i32 2, %56
  %add67 = add i32 %55, %mul66
  %conv68 = zext i32 %add67 to i64
  %57 = load i32, i32* %stride_in_bytes, align 4, !tbaa !7
  %conv69 = zext i32 %57 to i64
  %mul70 = mul i64 %conv68, %conv69
  br label %cond.end71

cond.end71:                                       ; preds = %cond.false65, %cond.true56
  %cond72 = phi i64 [ %div64, %cond.true56 ], [ %mul70, %cond.false65 ]
  store i64 %cond72, i64* %alloc_size, align 8, !tbaa !13
  %58 = load i64, i64* %alloc_size, align 8, !tbaa !13
  %59 = load i64, i64* %alloc_size, align 8, !tbaa !13
  %conv73 = trunc i64 %59 to i32
  %conv74 = zext i32 %conv73 to i64
  %cmp = icmp ne i64 %58, %conv74
  br i1 %cmp, label %if.then76, label %if.end77

if.then76:                                        ; preds = %cond.end71
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end77:                                         ; preds = %cond.end71
  %60 = load i8* (i8*, i32)*, i8* (i8*, i32)** %alloc_cb.addr, align 4, !tbaa !2
  %tobool78 = icmp ne i8* (i8*, i32)* %60, null
  br i1 %tobool78, label %if.then79, label %if.else96

if.then79:                                        ; preds = %if.end77
  %61 = bitcast i32* %padded_alloc_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #5
  %62 = load i64, i64* %alloc_size, align 8, !tbaa !13
  %conv80 = trunc i64 %62 to i32
  %63 = load i32, i32* %buf_align.addr, align 4, !tbaa !7
  %add81 = add i32 %conv80, %63
  %sub82 = sub i32 %add81, 1
  store i32 %sub82, i32* %padded_alloc_size, align 4, !tbaa !15
  %64 = load i8* (i8*, i32)*, i8* (i8*, i32)** %alloc_cb.addr, align 4, !tbaa !2
  %65 = load i8*, i8** %cb_priv.addr, align 4, !tbaa !2
  %66 = load i32, i32* %padded_alloc_size, align 4, !tbaa !15
  %call83 = call i8* %64(i8* %65, i32 %66)
  %67 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data84 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %67, i32 0, i32 23
  store i8* %call83, i8** %img_data84, align 4, !tbaa !12
  %68 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data85 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %68, i32 0, i32 23
  %69 = load i8*, i8** %img_data85, align 4, !tbaa !12
  %tobool86 = icmp ne i8* %69, null
  br i1 %tobool86, label %if.then87, label %if.end95

if.then87:                                        ; preds = %if.then79
  %70 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data88 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %70, i32 0, i32 23
  %71 = load i8*, i8** %img_data88, align 4, !tbaa !12
  %72 = ptrtoint i8* %71 to i32
  %73 = load i32, i32* %buf_align.addr, align 4, !tbaa !7
  %sub89 = sub i32 %73, 1
  %add90 = add i32 %72, %sub89
  %74 = load i32, i32* %buf_align.addr, align 4, !tbaa !7
  %sub91 = sub i32 %74, 1
  %neg92 = xor i32 %sub91, -1
  %and93 = and i32 %add90, %neg92
  %75 = inttoptr i32 %and93 to i8*
  %76 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data94 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %76, i32 0, i32 23
  store i8* %75, i8** %img_data94, align 4, !tbaa !12
  br label %if.end95

if.end95:                                         ; preds = %if.then87, %if.then79
  %77 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data_owner = getelementptr inbounds %struct.aom_image, %struct.aom_image* %77, i32 0, i32 24
  store i32 0, i32* %img_data_owner, align 4, !tbaa !16
  %78 = bitcast i32* %padded_alloc_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #5
  br label %if.end101

if.else96:                                        ; preds = %if.end77
  %79 = load i32, i32* %buf_align.addr, align 4, !tbaa !7
  %80 = load i64, i64* %alloc_size, align 8, !tbaa !13
  %conv97 = trunc i64 %80 to i32
  %call98 = call i8* @aom_memalign(i32 %79, i32 %conv97)
  %81 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data99 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %81, i32 0, i32 23
  store i8* %call98, i8** %img_data99, align 4, !tbaa !12
  %82 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data_owner100 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %82, i32 0, i32 24
  store i32 1, i32* %img_data_owner100, align 4, !tbaa !16
  br label %if.end101

if.end101:                                        ; preds = %if.else96, %if.end95
  %83 = load i64, i64* %alloc_size, align 8, !tbaa !13
  %conv102 = trunc i64 %83 to i32
  %84 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %sz = getelementptr inbounds %struct.aom_image, %struct.aom_image* %84, i32 0, i32 18
  store i32 %conv102, i32* %sz, align 4, !tbaa !17
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then76, %if.end101
  %85 = bitcast i64* %alloc_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %85) #5
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup118 [
    i32 0, label %cleanup.cont
    i32 2, label %fail
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end103

if.end103:                                        ; preds = %cleanup.cont, %if.end50
  %86 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data104 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %86, i32 0, i32 23
  %87 = load i8*, i8** %img_data104, align 4, !tbaa !12
  %tobool105 = icmp ne i8* %87, null
  br i1 %tobool105, label %if.end107, label %if.then106

if.then106:                                       ; preds = %if.end103
  br label %fail

if.end107:                                        ; preds = %if.end103
  %88 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  %89 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt108 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %89, i32 0, i32 0
  store i32 %88, i32* %fmt108, align 4, !tbaa !18
  %90 = load i32, i32* %bit_depth, align 4, !tbaa !7
  %91 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %bit_depth109 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %91, i32 0, i32 9
  store i32 %90, i32* %bit_depth109, align 4, !tbaa !19
  %92 = load i32, i32* %w, align 4, !tbaa !7
  %93 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %w110 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %93, i32 0, i32 7
  store i32 %92, i32* %w110, align 4, !tbaa !20
  %94 = load i32, i32* %h, align 4, !tbaa !7
  %95 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %h111 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %95, i32 0, i32 8
  store i32 %94, i32* %h111, align 4, !tbaa !21
  %96 = load i32, i32* %xcs, align 4, !tbaa !7
  %97 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %x_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %97, i32 0, i32 14
  store i32 %96, i32* %x_chroma_shift, align 4, !tbaa !22
  %98 = load i32, i32* %ycs, align 4, !tbaa !7
  %99 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %99, i32 0, i32 15
  store i32 %98, i32* %y_chroma_shift, align 4, !tbaa !23
  %100 = load i32, i32* %bps, align 4, !tbaa !7
  %101 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %bps112 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %101, i32 0, i32 19
  store i32 %100, i32* %bps112, align 4, !tbaa !24
  %102 = load i32, i32* %stride_in_bytes, align 4, !tbaa !7
  %103 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %103, i32 0, i32 17
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 0
  store i32 %102, i32* %arrayidx, align 4, !tbaa !7
  %104 = load i32, i32* %stride_in_bytes, align 4, !tbaa !7
  %105 = load i32, i32* %xcs, align 4, !tbaa !7
  %shr = lshr i32 %104, %105
  %106 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride113 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %106, i32 0, i32 17
  %arrayidx114 = getelementptr inbounds [3 x i32], [3 x i32]* %stride113, i32 0, i32 2
  store i32 %shr, i32* %arrayidx114, align 4, !tbaa !7
  %107 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride115 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %107, i32 0, i32 17
  %arrayidx116 = getelementptr inbounds [3 x i32], [3 x i32]* %stride115, i32 0, i32 1
  store i32 %shr, i32* %arrayidx116, align 4, !tbaa !7
  %108 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %109 = load i32, i32* %d_w.addr, align 4, !tbaa !7
  %110 = load i32, i32* %d_h.addr, align 4, !tbaa !7
  %111 = load i32, i32* %border.addr, align 4, !tbaa !7
  %call117 = call i32 @aom_img_set_rect(%struct.aom_image* %108, i32 0, i32 0, i32 %109, i32 %110, i32 %111)
  %112 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store %struct.aom_image* %112, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

fail:                                             ; preds = %cleanup, %if.then106, %if.then48, %if.then18, %if.then10, %if.then2
  %113 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  call void @aom_img_free(%struct.aom_image* %113)
  store %struct.aom_image* null, %struct.aom_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup118

cleanup118:                                       ; preds = %fail, %if.end107, %cleanup
  %114 = bitcast i32* %stride_in_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #5
  %115 = bitcast i32* %bit_depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #5
  %116 = bitcast i32* %bps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #5
  %117 = bitcast i32* %ycs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #5
  %118 = bitcast i32* %xcs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #5
  %119 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #5
  %120 = bitcast i32* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #5
  %121 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #5
  %122 = load %struct.aom_image*, %struct.aom_image** %retval, align 4
  ret %struct.aom_image* %122
}

; Function Attrs: nounwind
define hidden %struct.aom_image* @aom_img_alloc_with_cb(%struct.aom_image* %img, i32 %fmt, i32 %d_w, i32 %d_h, i32 %align, i8* (i8*, i32)* %alloc_cb, i8* %cb_priv) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  %fmt.addr = alloca i32, align 4
  %d_w.addr = alloca i32, align 4
  %d_h.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  %alloc_cb.addr = alloca i8* (i8*, i32)*, align 4
  %cb_priv.addr = alloca i8*, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %fmt, i32* %fmt.addr, align 4, !tbaa !6
  store i32 %d_w, i32* %d_w.addr, align 4, !tbaa !7
  store i32 %d_h, i32* %d_h.addr, align 4, !tbaa !7
  store i32 %align, i32* %align.addr, align 4, !tbaa !7
  store i8* (i8*, i32)* %alloc_cb, i8* (i8*, i32)** %alloc_cb.addr, align 4, !tbaa !2
  store i8* %cb_priv, i8** %cb_priv.addr, align 4, !tbaa !2
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %1 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  %2 = load i32, i32* %d_w.addr, align 4, !tbaa !7
  %3 = load i32, i32* %d_h.addr, align 4, !tbaa !7
  %4 = load i32, i32* %align.addr, align 4, !tbaa !7
  %5 = load i32, i32* %align.addr, align 4, !tbaa !7
  %6 = load i8* (i8*, i32)*, i8* (i8*, i32)** %alloc_cb.addr, align 4, !tbaa !2
  %7 = load i8*, i8** %cb_priv.addr, align 4, !tbaa !2
  %call = call %struct.aom_image* @img_alloc_helper(%struct.aom_image* %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 1, i32 0, i8* null, i8* (i8*, i32)* %6, i8* %7)
  ret %struct.aom_image* %call
}

; Function Attrs: nounwind
define hidden %struct.aom_image* @aom_img_wrap(%struct.aom_image* %img, i32 %fmt, i32 %d_w, i32 %d_h, i32 %stride_align, i8* %img_data) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  %fmt.addr = alloca i32, align 4
  %d_w.addr = alloca i32, align 4
  %d_h.addr = alloca i32, align 4
  %stride_align.addr = alloca i32, align 4
  %img_data.addr = alloca i8*, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %fmt, i32* %fmt.addr, align 4, !tbaa !6
  store i32 %d_w, i32* %d_w.addr, align 4, !tbaa !7
  store i32 %d_h, i32* %d_h.addr, align 4, !tbaa !7
  store i32 %stride_align, i32* %stride_align.addr, align 4, !tbaa !7
  store i8* %img_data, i8** %img_data.addr, align 4, !tbaa !2
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %1 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  %2 = load i32, i32* %d_w.addr, align 4, !tbaa !7
  %3 = load i32, i32* %d_h.addr, align 4, !tbaa !7
  %4 = load i32, i32* %stride_align.addr, align 4, !tbaa !7
  %5 = load i8*, i8** %img_data.addr, align 4, !tbaa !2
  %call = call %struct.aom_image* @img_alloc_helper(%struct.aom_image* %0, i32 %1, i32 %2, i32 %3, i32 1, i32 %4, i32 1, i32 0, i8* %5, i8* (i8*, i32)* null, i8* null)
  ret %struct.aom_image* %call
}

; Function Attrs: nounwind
define hidden %struct.aom_image* @aom_img_alloc_with_border(%struct.aom_image* %img, i32 %fmt, i32 %d_w, i32 %d_h, i32 %align, i32 %size_align, i32 %border) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  %fmt.addr = alloca i32, align 4
  %d_w.addr = alloca i32, align 4
  %d_h.addr = alloca i32, align 4
  %align.addr = alloca i32, align 4
  %size_align.addr = alloca i32, align 4
  %border.addr = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %fmt, i32* %fmt.addr, align 4, !tbaa !6
  store i32 %d_w, i32* %d_w.addr, align 4, !tbaa !7
  store i32 %d_h, i32* %d_h.addr, align 4, !tbaa !7
  store i32 %align, i32* %align.addr, align 4, !tbaa !7
  store i32 %size_align, i32* %size_align.addr, align 4, !tbaa !7
  store i32 %border, i32* %border.addr, align 4, !tbaa !7
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %1 = load i32, i32* %fmt.addr, align 4, !tbaa !6
  %2 = load i32, i32* %d_w.addr, align 4, !tbaa !7
  %3 = load i32, i32* %d_h.addr, align 4, !tbaa !7
  %4 = load i32, i32* %align.addr, align 4, !tbaa !7
  %5 = load i32, i32* %align.addr, align 4, !tbaa !7
  %6 = load i32, i32* %size_align.addr, align 4, !tbaa !7
  %7 = load i32, i32* %border.addr, align 4, !tbaa !7
  %call = call %struct.aom_image* @img_alloc_helper(%struct.aom_image* %0, i32 %1, i32 %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7, i8* null, i8* (i8*, i32)* null, i8* null)
  ret %struct.aom_image* %call
}

; Function Attrs: nounwind
define hidden i32 @aom_img_set_rect(%struct.aom_image* %img, i32 %x, i32 %y, i32 %w, i32 %h, i32 %border) #0 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %w.addr = alloca i32, align 4
  %h.addr = alloca i32, align 4
  %border.addr = alloca i32, align 4
  %data = alloca i8*, align 4
  %bytes_per_sample = alloca i32, align 4
  %uv_border_h = alloca i32, align 4
  %uv_x = alloca i32, align 4
  %uv_y = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %x, i32* %x.addr, align 4, !tbaa !7
  store i32 %y, i32* %y.addr, align 4, !tbaa !7
  store i32 %w, i32* %w.addr, align 4, !tbaa !7
  store i32 %h, i32* %h.addr, align 4, !tbaa !7
  store i32 %border, i32* %border.addr, align 4, !tbaa !7
  %0 = bitcast i8** %data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %x.addr, align 4, !tbaa !7
  %2 = load i32, i32* %w.addr, align 4, !tbaa !7
  %add = add i32 %1, %2
  %3 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %w1 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %3, i32 0, i32 7
  %4 = load i32, i32* %w1, align 4, !tbaa !20
  %cmp = icmp ule i32 %add, %4
  br i1 %cmp, label %land.lhs.true, label %if.end89

land.lhs.true:                                    ; preds = %entry
  %5 = load i32, i32* %y.addr, align 4, !tbaa !7
  %6 = load i32, i32* %h.addr, align 4, !tbaa !7
  %add2 = add i32 %5, %6
  %7 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %h3 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %7, i32 0, i32 8
  %8 = load i32, i32* %h3, align 4, !tbaa !21
  %cmp4 = icmp ule i32 %add2, %8
  br i1 %cmp4, label %if.then, label %if.end89

if.then:                                          ; preds = %land.lhs.true
  %9 = load i32, i32* %w.addr, align 4, !tbaa !7
  %10 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %10, i32 0, i32 10
  store i32 %9, i32* %d_w, align 4, !tbaa !25
  %11 = load i32, i32* %h.addr, align 4, !tbaa !7
  %12 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %12, i32 0, i32 11
  store i32 %11, i32* %d_h, align 4, !tbaa !26
  %13 = load i32, i32* %border.addr, align 4, !tbaa !7
  %14 = load i32, i32* %x.addr, align 4, !tbaa !7
  %add5 = add i32 %14, %13
  store i32 %add5, i32* %x.addr, align 4, !tbaa !7
  %15 = load i32, i32* %border.addr, align 4, !tbaa !7
  %16 = load i32, i32* %y.addr, align 4, !tbaa !7
  %add6 = add i32 %16, %15
  store i32 %add6, i32* %y.addr, align 4, !tbaa !7
  %17 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt = getelementptr inbounds %struct.aom_image, %struct.aom_image* %17, i32 0, i32 0
  %18 = load i32, i32* %fmt, align 4, !tbaa !18
  %and = and i32 %18, 256
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.else, label %if.then7

if.then7:                                         ; preds = %if.then
  %19 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data = getelementptr inbounds %struct.aom_image, %struct.aom_image* %19, i32 0, i32 23
  %20 = load i8*, i8** %img_data, align 4, !tbaa !12
  %21 = load i32, i32* %x.addr, align 4, !tbaa !7
  %22 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %bps = getelementptr inbounds %struct.aom_image, %struct.aom_image* %22, i32 0, i32 19
  %23 = load i32, i32* %bps, align 4, !tbaa !24
  %mul = mul i32 %21, %23
  %div = udiv i32 %mul, 8
  %add.ptr = getelementptr inbounds i8, i8* %20, i32 %div
  %24 = load i32, i32* %y.addr, align 4, !tbaa !7
  %25 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %25, i32 0, i32 17
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 0
  %26 = load i32, i32* %arrayidx, align 4, !tbaa !7
  %mul8 = mul i32 %24, %26
  %add.ptr9 = getelementptr inbounds i8, i8* %add.ptr, i32 %mul8
  %27 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes = getelementptr inbounds %struct.aom_image, %struct.aom_image* %27, i32 0, i32 16
  %arrayidx10 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes, i32 0, i32 0
  store i8* %add.ptr9, i8** %arrayidx10, align 4, !tbaa !2
  br label %if.end88

if.else:                                          ; preds = %if.then
  %28 = bitcast i32* %bytes_per_sample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #5
  %29 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt11 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %29, i32 0, i32 0
  %30 = load i32, i32* %fmt11, align 4, !tbaa !18
  %and12 = and i32 %30, 2048
  %tobool13 = icmp ne i32 %and12, 0
  %31 = zext i1 %tobool13 to i64
  %cond = select i1 %tobool13, i32 2, i32 1
  store i32 %cond, i32* %bytes_per_sample, align 4, !tbaa !7
  %32 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data14 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %32, i32 0, i32 23
  %33 = load i8*, i8** %img_data14, align 4, !tbaa !12
  store i8* %33, i8** %data, align 4, !tbaa !2
  %34 = load i8*, i8** %data, align 4, !tbaa !2
  %35 = load i32, i32* %x.addr, align 4, !tbaa !7
  %36 = load i32, i32* %bytes_per_sample, align 4, !tbaa !7
  %mul15 = mul i32 %35, %36
  %add.ptr16 = getelementptr inbounds i8, i8* %34, i32 %mul15
  %37 = load i32, i32* %y.addr, align 4, !tbaa !7
  %38 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride17 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %38, i32 0, i32 17
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %stride17, i32 0, i32 0
  %39 = load i32, i32* %arrayidx18, align 4, !tbaa !7
  %mul19 = mul i32 %37, %39
  %add.ptr20 = getelementptr inbounds i8, i8* %add.ptr16, i32 %mul19
  %40 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes21 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %40, i32 0, i32 16
  %arrayidx22 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes21, i32 0, i32 0
  store i8* %add.ptr20, i8** %arrayidx22, align 4, !tbaa !2
  %41 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %h23 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %41, i32 0, i32 8
  %42 = load i32, i32* %h23, align 4, !tbaa !21
  %43 = load i32, i32* %border.addr, align 4, !tbaa !7
  %mul24 = mul i32 2, %43
  %add25 = add i32 %42, %mul24
  %44 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride26 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %44, i32 0, i32 17
  %arrayidx27 = getelementptr inbounds [3 x i32], [3 x i32]* %stride26, i32 0, i32 0
  %45 = load i32, i32* %arrayidx27, align 4, !tbaa !7
  %mul28 = mul i32 %add25, %45
  %46 = load i8*, i8** %data, align 4, !tbaa !2
  %add.ptr29 = getelementptr inbounds i8, i8* %46, i32 %mul28
  store i8* %add.ptr29, i8** %data, align 4, !tbaa !2
  %47 = bitcast i32* %uv_border_h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #5
  %48 = load i32, i32* %border.addr, align 4, !tbaa !7
  %49 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %49, i32 0, i32 15
  %50 = load i32, i32* %y_chroma_shift, align 4, !tbaa !23
  %shr = lshr i32 %48, %50
  store i32 %shr, i32* %uv_border_h, align 4, !tbaa !7
  %51 = bitcast i32* %uv_x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #5
  %52 = load i32, i32* %x.addr, align 4, !tbaa !7
  %53 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %x_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %53, i32 0, i32 14
  %54 = load i32, i32* %x_chroma_shift, align 4, !tbaa !22
  %shr30 = lshr i32 %52, %54
  store i32 %shr30, i32* %uv_x, align 4, !tbaa !7
  %55 = bitcast i32* %uv_y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #5
  %56 = load i32, i32* %y.addr, align 4, !tbaa !7
  %57 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift31 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %57, i32 0, i32 15
  %58 = load i32, i32* %y_chroma_shift31, align 4, !tbaa !23
  %shr32 = lshr i32 %56, %58
  store i32 %shr32, i32* %uv_y, align 4, !tbaa !7
  %59 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %fmt33 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %59, i32 0, i32 0
  %60 = load i32, i32* %fmt33, align 4, !tbaa !18
  %and34 = and i32 %60, 512
  %tobool35 = icmp ne i32 %and34, 0
  br i1 %tobool35, label %if.else62, label %if.then36

if.then36:                                        ; preds = %if.else
  %61 = load i8*, i8** %data, align 4, !tbaa !2
  %62 = load i32, i32* %uv_x, align 4, !tbaa !7
  %63 = load i32, i32* %bytes_per_sample, align 4, !tbaa !7
  %mul37 = mul i32 %62, %63
  %add.ptr38 = getelementptr inbounds i8, i8* %61, i32 %mul37
  %64 = load i32, i32* %uv_y, align 4, !tbaa !7
  %65 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride39 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %65, i32 0, i32 17
  %arrayidx40 = getelementptr inbounds [3 x i32], [3 x i32]* %stride39, i32 0, i32 1
  %66 = load i32, i32* %arrayidx40, align 4, !tbaa !7
  %mul41 = mul i32 %64, %66
  %add.ptr42 = getelementptr inbounds i8, i8* %add.ptr38, i32 %mul41
  %67 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes43 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %67, i32 0, i32 16
  %arrayidx44 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes43, i32 0, i32 1
  store i8* %add.ptr42, i8** %arrayidx44, align 4, !tbaa !2
  %68 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %h45 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %68, i32 0, i32 8
  %69 = load i32, i32* %h45, align 4, !tbaa !21
  %70 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift46 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %70, i32 0, i32 15
  %71 = load i32, i32* %y_chroma_shift46, align 4, !tbaa !23
  %shr47 = lshr i32 %69, %71
  %72 = load i32, i32* %uv_border_h, align 4, !tbaa !7
  %mul48 = mul i32 2, %72
  %add49 = add i32 %shr47, %mul48
  %73 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride50 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %73, i32 0, i32 17
  %arrayidx51 = getelementptr inbounds [3 x i32], [3 x i32]* %stride50, i32 0, i32 1
  %74 = load i32, i32* %arrayidx51, align 4, !tbaa !7
  %mul52 = mul i32 %add49, %74
  %75 = load i8*, i8** %data, align 4, !tbaa !2
  %add.ptr53 = getelementptr inbounds i8, i8* %75, i32 %mul52
  store i8* %add.ptr53, i8** %data, align 4, !tbaa !2
  %76 = load i8*, i8** %data, align 4, !tbaa !2
  %77 = load i32, i32* %uv_x, align 4, !tbaa !7
  %78 = load i32, i32* %bytes_per_sample, align 4, !tbaa !7
  %mul54 = mul i32 %77, %78
  %add.ptr55 = getelementptr inbounds i8, i8* %76, i32 %mul54
  %79 = load i32, i32* %uv_y, align 4, !tbaa !7
  %80 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride56 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %80, i32 0, i32 17
  %arrayidx57 = getelementptr inbounds [3 x i32], [3 x i32]* %stride56, i32 0, i32 2
  %81 = load i32, i32* %arrayidx57, align 4, !tbaa !7
  %mul58 = mul i32 %79, %81
  %add.ptr59 = getelementptr inbounds i8, i8* %add.ptr55, i32 %mul58
  %82 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes60 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %82, i32 0, i32 16
  %arrayidx61 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes60, i32 0, i32 2
  store i8* %add.ptr59, i8** %arrayidx61, align 4, !tbaa !2
  br label %if.end

if.else62:                                        ; preds = %if.else
  %83 = load i8*, i8** %data, align 4, !tbaa !2
  %84 = load i32, i32* %uv_x, align 4, !tbaa !7
  %85 = load i32, i32* %bytes_per_sample, align 4, !tbaa !7
  %mul63 = mul i32 %84, %85
  %add.ptr64 = getelementptr inbounds i8, i8* %83, i32 %mul63
  %86 = load i32, i32* %uv_y, align 4, !tbaa !7
  %87 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride65 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %87, i32 0, i32 17
  %arrayidx66 = getelementptr inbounds [3 x i32], [3 x i32]* %stride65, i32 0, i32 2
  %88 = load i32, i32* %arrayidx66, align 4, !tbaa !7
  %mul67 = mul i32 %86, %88
  %add.ptr68 = getelementptr inbounds i8, i8* %add.ptr64, i32 %mul67
  %89 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes69 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %89, i32 0, i32 16
  %arrayidx70 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes69, i32 0, i32 2
  store i8* %add.ptr68, i8** %arrayidx70, align 4, !tbaa !2
  %90 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %h71 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %90, i32 0, i32 8
  %91 = load i32, i32* %h71, align 4, !tbaa !21
  %92 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift72 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %92, i32 0, i32 15
  %93 = load i32, i32* %y_chroma_shift72, align 4, !tbaa !23
  %shr73 = lshr i32 %91, %93
  %94 = load i32, i32* %uv_border_h, align 4, !tbaa !7
  %mul74 = mul i32 2, %94
  %add75 = add i32 %shr73, %mul74
  %95 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride76 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %95, i32 0, i32 17
  %arrayidx77 = getelementptr inbounds [3 x i32], [3 x i32]* %stride76, i32 0, i32 2
  %96 = load i32, i32* %arrayidx77, align 4, !tbaa !7
  %mul78 = mul i32 %add75, %96
  %97 = load i8*, i8** %data, align 4, !tbaa !2
  %add.ptr79 = getelementptr inbounds i8, i8* %97, i32 %mul78
  store i8* %add.ptr79, i8** %data, align 4, !tbaa !2
  %98 = load i8*, i8** %data, align 4, !tbaa !2
  %99 = load i32, i32* %uv_x, align 4, !tbaa !7
  %100 = load i32, i32* %bytes_per_sample, align 4, !tbaa !7
  %mul80 = mul i32 %99, %100
  %add.ptr81 = getelementptr inbounds i8, i8* %98, i32 %mul80
  %101 = load i32, i32* %uv_y, align 4, !tbaa !7
  %102 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride82 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %102, i32 0, i32 17
  %arrayidx83 = getelementptr inbounds [3 x i32], [3 x i32]* %stride82, i32 0, i32 1
  %103 = load i32, i32* %arrayidx83, align 4, !tbaa !7
  %mul84 = mul i32 %101, %103
  %add.ptr85 = getelementptr inbounds i8, i8* %add.ptr81, i32 %mul84
  %104 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes86 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %104, i32 0, i32 16
  %arrayidx87 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes86, i32 0, i32 1
  store i8* %add.ptr85, i8** %arrayidx87, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else62, %if.then36
  %105 = bitcast i32* %uv_y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #5
  %106 = bitcast i32* %uv_x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #5
  %107 = bitcast i32* %uv_border_h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #5
  %108 = bitcast i32* %bytes_per_sample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #5
  br label %if.end88

if.end88:                                         ; preds = %if.end, %if.then7
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end89:                                         ; preds = %land.lhs.true, %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end89, %if.end88
  %109 = bitcast i8** %data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #5
  %110 = load i32, i32* %retval, align 4
  ret i32 %110
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @aom_img_flip(%struct.aom_image* %img) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %0, i32 0, i32 11
  %1 = load i32, i32* %d_h, align 4, !tbaa !26
  %sub = sub i32 %1, 1
  %2 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride = getelementptr inbounds %struct.aom_image, %struct.aom_image* %2, i32 0, i32 17
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %stride, i32 0, i32 0
  %3 = load i32, i32* %arrayidx, align 4, !tbaa !7
  %mul = mul nsw i32 %sub, %3
  %4 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes = getelementptr inbounds %struct.aom_image, %struct.aom_image* %4, i32 0, i32 16
  %arrayidx1 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes, i32 0, i32 0
  %5 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  store i8* %add.ptr, i8** %arrayidx1, align 4, !tbaa !2
  %6 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride2 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %6, i32 0, i32 17
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %stride2, i32 0, i32 0
  %7 = load i32, i32* %arrayidx3, align 4, !tbaa !7
  %sub4 = sub nsw i32 0, %7
  %8 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride5 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %8, i32 0, i32 17
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %stride5, i32 0, i32 0
  store i32 %sub4, i32* %arrayidx6, align 4, !tbaa !7
  %9 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h7 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %9, i32 0, i32 11
  %10 = load i32, i32* %d_h7, align 4, !tbaa !26
  %11 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %11, i32 0, i32 15
  %12 = load i32, i32* %y_chroma_shift, align 4, !tbaa !23
  %shr = lshr i32 %10, %12
  %sub8 = sub i32 %shr, 1
  %13 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride9 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %13, i32 0, i32 17
  %arrayidx10 = getelementptr inbounds [3 x i32], [3 x i32]* %stride9, i32 0, i32 1
  %14 = load i32, i32* %arrayidx10, align 4, !tbaa !7
  %mul11 = mul nsw i32 %sub8, %14
  %15 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes12 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %15, i32 0, i32 16
  %arrayidx13 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes12, i32 0, i32 1
  %16 = load i8*, i8** %arrayidx13, align 4, !tbaa !2
  %add.ptr14 = getelementptr inbounds i8, i8* %16, i32 %mul11
  store i8* %add.ptr14, i8** %arrayidx13, align 4, !tbaa !2
  %17 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride15 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %17, i32 0, i32 17
  %arrayidx16 = getelementptr inbounds [3 x i32], [3 x i32]* %stride15, i32 0, i32 1
  %18 = load i32, i32* %arrayidx16, align 4, !tbaa !7
  %sub17 = sub nsw i32 0, %18
  %19 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride18 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %19, i32 0, i32 17
  %arrayidx19 = getelementptr inbounds [3 x i32], [3 x i32]* %stride18, i32 0, i32 1
  store i32 %sub17, i32* %arrayidx19, align 4, !tbaa !7
  %20 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h20 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %20, i32 0, i32 11
  %21 = load i32, i32* %d_h20, align 4, !tbaa !26
  %22 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift21 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %22, i32 0, i32 15
  %23 = load i32, i32* %y_chroma_shift21, align 4, !tbaa !23
  %shr22 = lshr i32 %21, %23
  %sub23 = sub i32 %shr22, 1
  %24 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride24 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %24, i32 0, i32 17
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %stride24, i32 0, i32 2
  %25 = load i32, i32* %arrayidx25, align 4, !tbaa !7
  %mul26 = mul nsw i32 %sub23, %25
  %26 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %planes27 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %26, i32 0, i32 16
  %arrayidx28 = getelementptr inbounds [3 x i8*], [3 x i8*]* %planes27, i32 0, i32 2
  %27 = load i8*, i8** %arrayidx28, align 4, !tbaa !2
  %add.ptr29 = getelementptr inbounds i8, i8* %27, i32 %mul26
  store i8* %add.ptr29, i8** %arrayidx28, align 4, !tbaa !2
  %28 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride30 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %28, i32 0, i32 17
  %arrayidx31 = getelementptr inbounds [3 x i32], [3 x i32]* %stride30, i32 0, i32 2
  %29 = load i32, i32* %arrayidx31, align 4, !tbaa !7
  %sub32 = sub nsw i32 0, %29
  %30 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %stride33 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %30, i32 0, i32 17
  %arrayidx34 = getelementptr inbounds [3 x i32], [3 x i32]* %stride33, i32 0, i32 2
  store i32 %sub32, i32* %arrayidx34, align 4, !tbaa !7
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_img_free(%struct.aom_image* %img) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_image* %0, null
  br i1 %tobool, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %1 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  call void @aom_img_remove_metadata(%struct.aom_image* %1)
  %2 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data = getelementptr inbounds %struct.aom_image, %struct.aom_image* %2, i32 0, i32 23
  %3 = load i8*, i8** %img_data, align 4, !tbaa !12
  %tobool1 = icmp ne i8* %3, null
  br i1 %tobool1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %4 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data_owner = getelementptr inbounds %struct.aom_image, %struct.aom_image* %4, i32 0, i32 24
  %5 = load i32, i32* %img_data_owner, align 4, !tbaa !16
  %tobool2 = icmp ne i32 %5, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %land.lhs.true
  %6 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %img_data4 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %6, i32 0, i32 23
  %7 = load i8*, i8** %img_data4, align 4, !tbaa !12
  call void @aom_free(i8* %7)
  br label %if.end

if.end:                                           ; preds = %if.then3, %land.lhs.true, %if.then
  %8 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %self_allocd = getelementptr inbounds %struct.aom_image, %struct.aom_image* %8, i32 0, i32 25
  %9 = load i32, i32* %self_allocd, align 4, !tbaa !9
  %tobool5 = icmp ne i32 %9, 0
  br i1 %tobool5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  %10 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %11 = bitcast %struct.aom_image* %10 to i8*
  call void @free(i8* %11)
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.end
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @aom_img_remove_metadata(%struct.aom_image* %img) #0 {
entry:
  %img.addr = alloca %struct.aom_image*, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_image* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.aom_image, %struct.aom_image* %1, i32 0, i32 26
  %2 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata, align 4, !tbaa !27
  %tobool1 = icmp ne %struct.aom_metadata_array* %2, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata2 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %3, i32 0, i32 26
  %4 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata2, align 4, !tbaa !27
  call void @aom_img_metadata_array_free(%struct.aom_metadata_array* %4)
  %5 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata3 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %5, i32 0, i32 26
  store %struct.aom_metadata_array* null, %struct.aom_metadata_array** %metadata3, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

declare void @aom_free(i8*) #2

declare void @free(i8*) #2

; Function Attrs: nounwind
define hidden i32 @aom_img_plane_width(%struct.aom_image* %img, i32 %plane) #0 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  %plane.addr = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !7
  %0 = load i32, i32* %plane.addr, align 4, !tbaa !7
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %x_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %1, i32 0, i32 14
  %2 = load i32, i32* %x_chroma_shift, align 4, !tbaa !22
  %cmp1 = icmp ugt i32 %2, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_w = getelementptr inbounds %struct.aom_image, %struct.aom_image* %3, i32 0, i32 10
  %4 = load i32, i32* %d_w, align 4, !tbaa !25
  %add = add i32 %4, 1
  %5 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %x_chroma_shift2 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %5, i32 0, i32 14
  %6 = load i32, i32* %x_chroma_shift2, align 4, !tbaa !22
  %shr = lshr i32 %add, %6
  store i32 %shr, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %land.lhs.true, %entry
  %7 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_w3 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %7, i32 0, i32 10
  %8 = load i32, i32* %d_w3, align 4, !tbaa !25
  store i32 %8, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: nounwind
define hidden i32 @aom_img_plane_height(%struct.aom_image* %img, i32 %plane) #0 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  %plane.addr = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %plane, i32* %plane.addr, align 4, !tbaa !7
  %0 = load i32, i32* %plane.addr, align 4, !tbaa !7
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift = getelementptr inbounds %struct.aom_image, %struct.aom_image* %1, i32 0, i32 15
  %2 = load i32, i32* %y_chroma_shift, align 4, !tbaa !23
  %cmp1 = icmp ugt i32 %2, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h = getelementptr inbounds %struct.aom_image, %struct.aom_image* %3, i32 0, i32 11
  %4 = load i32, i32* %d_h, align 4, !tbaa !26
  %add = add i32 %4, 1
  %5 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %y_chroma_shift2 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %5, i32 0, i32 15
  %6 = load i32, i32* %y_chroma_shift2, align 4, !tbaa !23
  %shr = lshr i32 %add, %6
  store i32 %shr, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %land.lhs.true, %entry
  %7 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %d_h3 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %7, i32 0, i32 11
  %8 = load i32, i32* %d_h3, align 4, !tbaa !26
  store i32 %8, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: nounwind
define hidden %struct.aom_metadata* @aom_img_metadata_alloc(i32 %type, i8* %data, i32 %sz, i32 %insert_flag) #0 {
entry:
  %retval = alloca %struct.aom_metadata*, align 4
  %type.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  %sz.addr = alloca i32, align 4
  %insert_flag.addr = alloca i32, align 4
  %metadata = alloca %struct.aom_metadata*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %type, i32* %type.addr, align 4, !tbaa !7
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %sz, i32* %sz.addr, align 4, !tbaa !15
  store i32 %insert_flag, i32* %insert_flag.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %sz.addr, align 4, !tbaa !15
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %struct.aom_metadata* null, %struct.aom_metadata** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = bitcast %struct.aom_metadata** %metadata to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %call = call i8* @malloc(i32 16)
  %3 = bitcast i8* %call to %struct.aom_metadata*
  store %struct.aom_metadata* %3, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %4 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_metadata* %4, null
  br i1 %tobool1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store %struct.aom_metadata* null, %struct.aom_metadata** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %5 = load i32, i32* %type.addr, align 4, !tbaa !7
  %6 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %type4 = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %6, i32 0, i32 0
  store i32 %5, i32* %type4, align 4, !tbaa !28
  %7 = load i32, i32* %sz.addr, align 4, !tbaa !15
  %call5 = call i8* @malloc(i32 %7)
  %8 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %payload = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %8, i32 0, i32 1
  store i8* %call5, i8** %payload, align 4, !tbaa !30
  %9 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %payload6 = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %9, i32 0, i32 1
  %10 = load i8*, i8** %payload6, align 4, !tbaa !30
  %tobool7 = icmp ne i8* %10, null
  br i1 %tobool7, label %if.end9, label %if.then8

if.then8:                                         ; preds = %if.end3
  %11 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %12 = bitcast %struct.aom_metadata* %11 to i8*
  call void @free(i8* %12)
  store %struct.aom_metadata* null, %struct.aom_metadata** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end3
  %13 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %payload10 = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %13, i32 0, i32 1
  %14 = load i8*, i8** %payload10, align 4, !tbaa !30
  %15 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %16 = load i32, i32* %sz.addr, align 4, !tbaa !15
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %14, i8* align 1 %15, i32 %16, i1 false)
  %17 = load i32, i32* %sz.addr, align 4, !tbaa !15
  %18 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %sz11 = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %18, i32 0, i32 2
  store i32 %17, i32* %sz11, align 4, !tbaa !31
  %19 = load i32, i32* %insert_flag.addr, align 4, !tbaa !6
  %20 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  %insert_flag12 = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %20, i32 0, i32 3
  store i32 %19, i32* %insert_flag12, align 4, !tbaa !32
  %21 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata, align 4, !tbaa !2
  store %struct.aom_metadata* %21, %struct.aom_metadata** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end9, %if.then8, %if.then2
  %22 = bitcast %struct.aom_metadata** %metadata to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #5
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %23 = load %struct.aom_metadata*, %struct.aom_metadata** %retval, align 4
  ret %struct.aom_metadata* %23
}

declare i8* @malloc(i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @aom_img_metadata_free(%struct.aom_metadata* %metadata) #0 {
entry:
  %metadata.addr = alloca %struct.aom_metadata*, align 4
  store %struct.aom_metadata* %metadata, %struct.aom_metadata** %metadata.addr, align 4, !tbaa !2
  %0 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_metadata* %0, null
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata.addr, align 4, !tbaa !2
  %payload = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %1, i32 0, i32 1
  %2 = load i8*, i8** %payload, align 4, !tbaa !30
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %3 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata.addr, align 4, !tbaa !2
  %payload3 = getelementptr inbounds %struct.aom_metadata, %struct.aom_metadata* %3, i32 0, i32 1
  %4 = load i8*, i8** %payload3, align 4, !tbaa !30
  call void @free(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %5 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata.addr, align 4, !tbaa !2
  %6 = bitcast %struct.aom_metadata* %5 to i8*
  call void @free(i8* %6)
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: nounwind
define hidden %struct.aom_metadata_array* @aom_img_metadata_array_alloc(i32 %sz) #0 {
entry:
  %retval = alloca %struct.aom_metadata_array*, align 4
  %sz.addr = alloca i32, align 4
  %arr = alloca %struct.aom_metadata_array*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store i32 %sz, i32* %sz.addr, align 4, !tbaa !15
  %0 = bitcast %struct.aom_metadata_array** %arr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %call = call i8* @calloc(i32 1, i32 8)
  %1 = bitcast i8* %call to %struct.aom_metadata_array*
  store %struct.aom_metadata_array* %1, %struct.aom_metadata_array** %arr, align 4, !tbaa !2
  %2 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_metadata_array* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.aom_metadata_array* null, %struct.aom_metadata_array** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %sz.addr, align 4, !tbaa !15
  %cmp = icmp ugt i32 %3, 0
  br i1 %cmp, label %if.then1, label %if.end8

if.then1:                                         ; preds = %if.end
  %4 = load i32, i32* %sz.addr, align 4, !tbaa !15
  %call2 = call i8* @calloc(i32 %4, i32 4)
  %5 = bitcast i8* %call2 to %struct.aom_metadata**
  %6 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr, align 4, !tbaa !2
  %metadata_array = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %6, i32 0, i32 1
  store %struct.aom_metadata** %5, %struct.aom_metadata*** %metadata_array, align 4, !tbaa !33
  %7 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr, align 4, !tbaa !2
  %metadata_array3 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %7, i32 0, i32 1
  %8 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array3, align 4, !tbaa !33
  %tobool4 = icmp ne %struct.aom_metadata** %8, null
  br i1 %tobool4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.then1
  %9 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr, align 4, !tbaa !2
  call void @aom_img_metadata_array_free(%struct.aom_metadata_array* %9)
  store %struct.aom_metadata_array* null, %struct.aom_metadata_array** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.then1
  %10 = load i32, i32* %sz.addr, align 4, !tbaa !15
  %11 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr, align 4, !tbaa !2
  %sz7 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %11, i32 0, i32 0
  store i32 %10, i32* %sz7, align 4, !tbaa !35
  br label %if.end8

if.end8:                                          ; preds = %if.end6, %if.end
  %12 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr, align 4, !tbaa !2
  store %struct.aom_metadata_array* %12, %struct.aom_metadata_array** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then5, %if.then
  %13 = bitcast %struct.aom_metadata_array** %arr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  %14 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %retval, align 4
  ret %struct.aom_metadata_array* %14
}

declare i8* @calloc(i32, i32) #2

; Function Attrs: nounwind
define hidden void @aom_img_metadata_array_free(%struct.aom_metadata_array* %arr) #0 {
entry:
  %arr.addr = alloca %struct.aom_metadata_array*, align 4
  %i = alloca i32, align 4
  store %struct.aom_metadata_array* %arr, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %0 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_metadata_array* %0, null
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %metadata_array = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %1, i32 0, i32 1
  %2 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array, align 4, !tbaa !33
  %tobool1 = icmp ne %struct.aom_metadata** %2, null
  br i1 %tobool1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #5
  store i32 0, i32* %i, align 4, !tbaa !15
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %4 = load i32, i32* %i, align 4, !tbaa !15
  %5 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %sz = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %5, i32 0, i32 0
  %6 = load i32, i32* %sz, align 4, !tbaa !35
  %cmp = icmp ult i32 %4, %6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #5
  br label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %metadata_array3 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %8, i32 0, i32 1
  %9 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array3, align 4, !tbaa !33
  %10 = load i32, i32* %i, align 4, !tbaa !15
  %arrayidx = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %9, i32 %10
  %11 = load %struct.aom_metadata*, %struct.aom_metadata** %arrayidx, align 4, !tbaa !2
  call void @aom_img_metadata_free(%struct.aom_metadata* %11)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !15
  %inc = add i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !15
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %13 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %metadata_array4 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %13, i32 0, i32 1
  %14 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array4, align 4, !tbaa !33
  %15 = bitcast %struct.aom_metadata** %14 to i8*
  call void @free(i8* %15)
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %16 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %arr.addr, align 4, !tbaa !2
  %17 = bitcast %struct.aom_metadata_array* %16 to i8*
  call void @free(i8* %17)
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: nounwind
define hidden i32 @aom_img_add_metadata(%struct.aom_image* %img, i32 %type, i8* %data, i32 %sz, i32 %insert_flag) #0 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  %type.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  %sz.addr = alloca i32, align 4
  %insert_flag.addr = alloca i32, align 4
  %metadata9 = alloca %struct.aom_metadata*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %type, i32* %type.addr, align 4, !tbaa !7
  store i8* %data, i8** %data.addr, align 4, !tbaa !2
  store i32 %sz, i32* %sz.addr, align 4, !tbaa !15
  store i32 %insert_flag, i32* %insert_flag.addr, align 4, !tbaa !6
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_image* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.aom_image, %struct.aom_image* %1, i32 0, i32 26
  %2 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata, align 4, !tbaa !27
  %tobool1 = icmp ne %struct.aom_metadata_array* %2, null
  br i1 %tobool1, label %if.end8, label %if.then2

if.then2:                                         ; preds = %if.end
  %call = call %struct.aom_metadata_array* @aom_img_metadata_array_alloc(i32 0)
  %3 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata3 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %3, i32 0, i32 26
  store %struct.aom_metadata_array* %call, %struct.aom_metadata_array** %metadata3, align 4, !tbaa !27
  %4 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata4 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %4, i32 0, i32 26
  %5 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata4, align 4, !tbaa !27
  %tobool5 = icmp ne %struct.aom_metadata_array* %5, null
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %if.then2
  store i32 -1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.then2
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %if.end
  %6 = bitcast %struct.aom_metadata** %metadata9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #5
  %7 = load i32, i32* %type.addr, align 4, !tbaa !7
  %8 = load i8*, i8** %data.addr, align 4, !tbaa !2
  %9 = load i32, i32* %sz.addr, align 4, !tbaa !15
  %10 = load i32, i32* %insert_flag.addr, align 4, !tbaa !6
  %call10 = call %struct.aom_metadata* @aom_img_metadata_alloc(i32 %7, i8* %8, i32 %9, i32 %10)
  store %struct.aom_metadata* %call10, %struct.aom_metadata** %metadata9, align 4, !tbaa !2
  %11 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata9, align 4, !tbaa !2
  %tobool11 = icmp ne %struct.aom_metadata* %11, null
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %if.end8
  br label %fail

if.end13:                                         ; preds = %if.end8
  %12 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata14 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %12, i32 0, i32 26
  %13 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata14, align 4, !tbaa !27
  %metadata_array = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %13, i32 0, i32 1
  %14 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array, align 4, !tbaa !33
  %tobool15 = icmp ne %struct.aom_metadata** %14, null
  br i1 %tobool15, label %if.else, label %if.then16

if.then16:                                        ; preds = %if.end13
  %call17 = call i8* @calloc(i32 1, i32 4)
  %15 = bitcast i8* %call17 to %struct.aom_metadata**
  %16 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata18 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %16, i32 0, i32 26
  %17 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata18, align 4, !tbaa !27
  %metadata_array19 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %17, i32 0, i32 1
  store %struct.aom_metadata** %15, %struct.aom_metadata*** %metadata_array19, align 4, !tbaa !33
  %18 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata20 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %18, i32 0, i32 26
  %19 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata20, align 4, !tbaa !27
  %metadata_array21 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %19, i32 0, i32 1
  %20 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array21, align 4, !tbaa !33
  %tobool22 = icmp ne %struct.aom_metadata** %20, null
  br i1 %tobool22, label %lor.lhs.false, label %if.then25

lor.lhs.false:                                    ; preds = %if.then16
  %21 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata23 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %21, i32 0, i32 26
  %22 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata23, align 4, !tbaa !27
  %sz24 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %22, i32 0, i32 0
  %23 = load i32, i32* %sz24, align 4, !tbaa !35
  %cmp = icmp ne i32 %23, 0
  br i1 %cmp, label %if.then25, label %if.end26

if.then25:                                        ; preds = %lor.lhs.false, %if.then16
  %24 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata9, align 4, !tbaa !2
  call void @aom_img_metadata_free(%struct.aom_metadata* %24)
  br label %fail

if.end26:                                         ; preds = %lor.lhs.false
  br label %if.end34

if.else:                                          ; preds = %if.end13
  %25 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata27 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %25, i32 0, i32 26
  %26 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata27, align 4, !tbaa !27
  %metadata_array28 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %26, i32 0, i32 1
  %27 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array28, align 4, !tbaa !33
  %28 = bitcast %struct.aom_metadata** %27 to i8*
  %29 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata29 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %29, i32 0, i32 26
  %30 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata29, align 4, !tbaa !27
  %sz30 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %30, i32 0, i32 0
  %31 = load i32, i32* %sz30, align 4, !tbaa !35
  %add = add i32 %31, 1
  %mul = mul i32 %add, 4
  %call31 = call i8* @realloc(i8* %28, i32 %mul)
  %32 = bitcast i8* %call31 to %struct.aom_metadata**
  %33 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata32 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %33, i32 0, i32 26
  %34 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata32, align 4, !tbaa !27
  %metadata_array33 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %34, i32 0, i32 1
  store %struct.aom_metadata** %32, %struct.aom_metadata*** %metadata_array33, align 4, !tbaa !33
  br label %if.end34

if.end34:                                         ; preds = %if.else, %if.end26
  %35 = load %struct.aom_metadata*, %struct.aom_metadata** %metadata9, align 4, !tbaa !2
  %36 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata35 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %36, i32 0, i32 26
  %37 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata35, align 4, !tbaa !27
  %metadata_array36 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %37, i32 0, i32 1
  %38 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array36, align 4, !tbaa !33
  %39 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata37 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %39, i32 0, i32 26
  %40 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata37, align 4, !tbaa !27
  %sz38 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %40, i32 0, i32 0
  %41 = load i32, i32* %sz38, align 4, !tbaa !35
  %arrayidx = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %38, i32 %41
  store %struct.aom_metadata* %35, %struct.aom_metadata** %arrayidx, align 4, !tbaa !2
  %42 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata39 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %42, i32 0, i32 26
  %43 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata39, align 4, !tbaa !27
  %sz40 = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %43, i32 0, i32 0
  %44 = load i32, i32* %sz40, align 4, !tbaa !35
  %inc = add i32 %44, 1
  store i32 %inc, i32* %sz40, align 4, !tbaa !35
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

fail:                                             ; preds = %if.then25, %if.then12
  %45 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata41 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %45, i32 0, i32 26
  %46 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata41, align 4, !tbaa !27
  call void @aom_img_metadata_array_free(%struct.aom_metadata_array* %46)
  %47 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata42 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %47, i32 0, i32 26
  store %struct.aom_metadata_array* null, %struct.aom_metadata_array** %metadata42, align 4, !tbaa !27
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %fail, %if.end34
  %48 = bitcast %struct.aom_metadata** %metadata9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #5
  br label %return

return:                                           ; preds = %cleanup, %if.then6, %if.then
  %49 = load i32, i32* %retval, align 4
  ret i32 %49
}

declare i8* @realloc(i8*, i32) #2

; Function Attrs: nounwind
define hidden %struct.aom_metadata* @aom_img_get_metadata(%struct.aom_image* %img, i32 %index) #0 {
entry:
  %retval = alloca %struct.aom_metadata*, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  %index.addr = alloca i32, align 4
  %array = alloca %struct.aom_metadata_array*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !15
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_image* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.aom_metadata* null, %struct.aom_metadata** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %struct.aom_metadata_array** %array to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #5
  %2 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.aom_image, %struct.aom_image* %2, i32 0, i32 26
  %3 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata, align 4, !tbaa !27
  store %struct.aom_metadata_array* %3, %struct.aom_metadata_array** %array, align 4, !tbaa !2
  %4 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %array, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.aom_metadata_array* %4, null
  br i1 %tobool1, label %land.lhs.true, label %if.end3

land.lhs.true:                                    ; preds = %if.end
  %5 = load i32, i32* %index.addr, align 4, !tbaa !15
  %6 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %array, align 4, !tbaa !2
  %sz = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %6, i32 0, i32 0
  %7 = load i32, i32* %sz, align 4, !tbaa !35
  %cmp = icmp ult i32 %5, %7
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %land.lhs.true
  %8 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %array, align 4, !tbaa !2
  %metadata_array = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %8, i32 0, i32 1
  %9 = load %struct.aom_metadata**, %struct.aom_metadata*** %metadata_array, align 4, !tbaa !33
  %10 = load i32, i32* %index.addr, align 4, !tbaa !15
  %arrayidx = getelementptr inbounds %struct.aom_metadata*, %struct.aom_metadata** %9, i32 %10
  %11 = load %struct.aom_metadata*, %struct.aom_metadata** %arrayidx, align 4, !tbaa !2
  store %struct.aom_metadata* %11, %struct.aom_metadata** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %land.lhs.true, %if.end
  store %struct.aom_metadata* null, %struct.aom_metadata** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end3, %if.then2
  %12 = bitcast %struct.aom_metadata_array** %array to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %13 = load %struct.aom_metadata*, %struct.aom_metadata** %retval, align 4
  ret %struct.aom_metadata* %13
}

; Function Attrs: nounwind
define hidden i32 @aom_img_num_metadata(%struct.aom_image* %img) #0 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.aom_image*, align 4
  store %struct.aom_image* %img, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.aom_image* %0, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata = getelementptr inbounds %struct.aom_image, %struct.aom_image* %1, i32 0, i32 26
  %2 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata, align 4, !tbaa !27
  %tobool1 = icmp ne %struct.aom_metadata_array* %2, null
  br i1 %tobool1, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load %struct.aom_image*, %struct.aom_image** %img.addr, align 4, !tbaa !2
  %metadata2 = getelementptr inbounds %struct.aom_image, %struct.aom_image* %3, i32 0, i32 26
  %4 = load %struct.aom_metadata_array*, %struct.aom_metadata_array** %metadata2, align 4, !tbaa !27
  %sz = getelementptr inbounds %struct.aom_metadata_array, %struct.aom_metadata_array* %4, i32 0, i32 0
  %5 = load i32, i32* %sz, align 4, !tbaa !35
  store i32 %5, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: inlinehint nounwind
define internal i32 @align_image_dimension(i32 %d, i32 %subsampling, i32 %size_align) #3 {
entry:
  %d.addr = alloca i32, align 4
  %subsampling.addr = alloca i32, align 4
  %size_align.addr = alloca i32, align 4
  %align = alloca i32, align 4
  store i32 %d, i32* %d.addr, align 4, !tbaa !7
  store i32 %subsampling, i32* %subsampling.addr, align 4, !tbaa !7
  store i32 %size_align, i32* %size_align.addr, align 4, !tbaa !7
  %0 = bitcast i32* %align to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %subsampling.addr, align 4, !tbaa !7
  %shl = shl i32 1, %1
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %align, align 4, !tbaa !7
  %2 = load i32, i32* %size_align.addr, align 4, !tbaa !7
  %sub1 = sub i32 %2, 1
  %3 = load i32, i32* %align, align 4, !tbaa !7
  %cmp = icmp ugt i32 %sub1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32, i32* %size_align.addr, align 4, !tbaa !7
  %sub2 = sub i32 %4, 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32, i32* %align, align 4, !tbaa !7
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub2, %cond.true ], [ %5, %cond.false ]
  store i32 %cond, i32* %align, align 4, !tbaa !7
  %6 = load i32, i32* %d.addr, align 4, !tbaa !7
  %7 = load i32, i32* %align, align 4, !tbaa !7
  %add = add i32 %6, %7
  %8 = load i32, i32* %align, align 4, !tbaa !7
  %neg = xor i32 %8, -1
  %and = and i32 %add, %neg
  %9 = bitcast i32* %align to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #5
  ret i32 %and
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

declare i8* @aom_memalign(i32, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!4, !4, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"int", !4, i64 0}
!9 = !{!10, !8, i64 116}
!10 = !{!"aom_image", !4, i64 0, !4, i64 4, !4, i64 8, !4, i64 12, !8, i64 16, !4, i64 20, !4, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !4, i64 64, !4, i64 76, !11, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !3, i64 104, !3, i64 108, !8, i64 112, !8, i64 116, !3, i64 120, !3, i64 124}
!11 = !{!"long", !4, i64 0}
!12 = !{!10, !3, i64 108}
!13 = !{!14, !14, i64 0}
!14 = !{!"long long", !4, i64 0}
!15 = !{!11, !11, i64 0}
!16 = !{!10, !8, i64 112}
!17 = !{!10, !11, i64 88}
!18 = !{!10, !4, i64 0}
!19 = !{!10, !8, i64 36}
!20 = !{!10, !8, i64 28}
!21 = !{!10, !8, i64 32}
!22 = !{!10, !8, i64 56}
!23 = !{!10, !8, i64 60}
!24 = !{!10, !8, i64 92}
!25 = !{!10, !8, i64 40}
!26 = !{!10, !8, i64 44}
!27 = !{!10, !3, i64 120}
!28 = !{!29, !8, i64 0}
!29 = !{!"aom_metadata", !8, i64 0, !3, i64 4, !11, i64 8, !4, i64 12}
!30 = !{!29, !3, i64 4}
!31 = !{!29, !11, i64 8}
!32 = !{!29, !4, i64 12}
!33 = !{!34, !3, i64 4}
!34 = !{!"aom_metadata_array", !11, i64 0, !3, i64 4}
!35 = !{!34, !11, i64 0}
