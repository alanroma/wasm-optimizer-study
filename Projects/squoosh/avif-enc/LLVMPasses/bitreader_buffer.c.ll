; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/bitreader_buffer.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/aom_dsp/bitreader_buffer.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.aom_read_bit_buffer = type { i8*, i8*, i32, i8*, void (i8*)* }

; Function Attrs: nounwind
define hidden i32 @aom_rb_bytes_read(%struct.aom_read_bit_buffer* %rb) #0 {
entry:
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %0 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %bit_offset = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %0, i32 0, i32 2
  %1 = load i32, i32* %bit_offset, align 4, !tbaa !6
  %add = add i32 %1, 7
  %shr = lshr i32 %add, 3
  ret i32 %shr
}

; Function Attrs: nounwind
define hidden i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %rb) #0 {
entry:
  %retval = alloca i32, align 4
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %off = alloca i32, align 4
  %p = alloca i32, align 4
  %q = alloca i32, align 4
  %bit = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %0 = bitcast i32* %off to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %bit_offset = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %1, i32 0, i32 2
  %2 = load i32, i32* %bit_offset, align 4, !tbaa !6
  store i32 %2, i32* %off, align 4, !tbaa !9
  %3 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %off, align 4, !tbaa !9
  %shr = lshr i32 %4, 3
  store i32 %shr, i32* %p, align 4, !tbaa !9
  %5 = bitcast i32* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load i32, i32* %off, align 4, !tbaa !9
  %and = and i32 %6, 7
  %sub = sub nsw i32 7, %and
  store i32 %sub, i32* %q, align 4, !tbaa !9
  %7 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %bit_buffer = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %7, i32 0, i32 0
  %8 = load i8*, i8** %bit_buffer, align 4, !tbaa !10
  %9 = load i32, i32* %p, align 4, !tbaa !9
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %9
  %10 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %bit_buffer_end = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %10, i32 0, i32 1
  %11 = load i8*, i8** %bit_buffer_end, align 4, !tbaa !11
  %cmp = icmp ult i8* %add.ptr, %11
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %12 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %bit_buffer1 = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %13, i32 0, i32 0
  %14 = load i8*, i8** %bit_buffer1, align 4, !tbaa !10
  %15 = load i32, i32* %p, align 4, !tbaa !9
  %arrayidx = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8, i8* %arrayidx, align 1, !tbaa !12
  %conv = zext i8 %16 to i32
  %17 = load i32, i32* %q, align 4, !tbaa !9
  %shr2 = ashr i32 %conv, %17
  %and3 = and i32 %shr2, 1
  store i32 %and3, i32* %bit, align 4, !tbaa !9
  %18 = load i32, i32* %off, align 4, !tbaa !9
  %add = add i32 %18, 1
  %19 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %bit_offset4 = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %19, i32 0, i32 2
  store i32 %add, i32* %bit_offset4, align 4, !tbaa !6
  %20 = load i32, i32* %bit, align 4, !tbaa !9
  store i32 %20, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  br label %cleanup

if.else:                                          ; preds = %entry
  %22 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %error_handler = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %22, i32 0, i32 4
  %23 = load void (i8*)*, void (i8*)** %error_handler, align 4, !tbaa !13
  %tobool = icmp ne void (i8*)* %23, null
  br i1 %tobool, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %24 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %error_handler6 = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %24, i32 0, i32 4
  %25 = load void (i8*)*, void (i8*)** %error_handler6, align 4, !tbaa !13
  %26 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %error_handler_data = getelementptr inbounds %struct.aom_read_bit_buffer, %struct.aom_read_bit_buffer* %26, i32 0, i32 3
  %27 = load i8*, i8** %error_handler_data, align 4, !tbaa !14
  call void %25(i8* %27)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %28 = bitcast i32* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  %29 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  %30 = bitcast i32* %off to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %rb, i32 %bits) #0 {
entry:
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %bits.addr = alloca i32, align 4
  %value = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !9
  %0 = bitcast i32* %value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %value, align 4, !tbaa !9
  %1 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %bits.addr, align 4, !tbaa !9
  %sub = sub nsw i32 %2, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %bit, align 4, !tbaa !9
  %cmp = icmp sge i32 %3, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %4)
  %5 = load i32, i32* %bit, align 4, !tbaa !9
  %shl = shl i32 %call, %5
  %6 = load i32, i32* %value, align 4, !tbaa !9
  %or = or i32 %6, %shl
  store i32 %or, i32* %value, align 4, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %bit, align 4, !tbaa !9
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i32, i32* %value, align 4, !tbaa !9
  %9 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  ret i32 %8
}

; Function Attrs: nounwind
define hidden i32 @aom_rb_read_unsigned_literal(%struct.aom_read_bit_buffer* %rb, i32 %bits) #0 {
entry:
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %bits.addr = alloca i32, align 4
  %value = alloca i32, align 4
  %bit = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !9
  %0 = bitcast i32* %value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %value, align 4, !tbaa !9
  %1 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i32, i32* %bits.addr, align 4, !tbaa !9
  %sub = sub nsw i32 %2, 1
  store i32 %sub, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %bit, align 4, !tbaa !9
  %cmp = icmp sge i32 %3, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %4)
  %5 = load i32, i32* %bit, align 4, !tbaa !9
  %shl = shl i32 %call, %5
  %6 = load i32, i32* %value, align 4, !tbaa !9
  %or = or i32 %6, %shl
  store i32 %or, i32* %value, align 4, !tbaa !9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %bit, align 4, !tbaa !9
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %bit, align 4, !tbaa !9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i32, i32* %value, align 4, !tbaa !9
  %9 = bitcast i32* %bit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  ret i32 %8
}

; Function Attrs: nounwind
define hidden i32 @aom_rb_read_inv_signed_literal(%struct.aom_read_bit_buffer* %rb, i32 %bits) #0 {
entry:
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %bits.addr = alloca i32, align 4
  %nbits = alloca i32, align 4
  %value = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !9
  %0 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %bits.addr, align 4, !tbaa !9
  %sub = sub i32 32, %1
  %sub1 = sub i32 %sub, 1
  store i32 %sub1, i32* %nbits, align 4, !tbaa !9
  %2 = bitcast i32* %value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %4 = load i32, i32* %bits.addr, align 4, !tbaa !9
  %add = add nsw i32 %4, 1
  %call = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %3, i32 %add)
  %5 = load i32, i32* %nbits, align 4, !tbaa !9
  %shl = shl i32 %call, %5
  store i32 %shl, i32* %value, align 4, !tbaa !9
  %6 = load i32, i32* %value, align 4, !tbaa !9
  %7 = load i32, i32* %nbits, align 4, !tbaa !9
  %shr = ashr i32 %6, %7
  %8 = bitcast i32* %value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  ret i32 %shr
}

; Function Attrs: nounwind
define hidden i32 @aom_rb_read_uvlc(%struct.aom_read_bit_buffer* %rb) #0 {
entry:
  %retval = alloca i32, align 4
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %leading_zeros = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %base = alloca i32, align 4
  %value = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %0 = bitcast i32* %leading_zeros to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %leading_zeros, align 4, !tbaa !9
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %leading_zeros, align 4, !tbaa !9
  %cmp = icmp slt i32 %1, 32
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %2 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %2)
  %tobool = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %3 = phi i1 [ false, %while.cond ], [ %lnot, %land.rhs ]
  br i1 %3, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %4 = load i32, i32* %leading_zeros, align 4, !tbaa !9
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %leading_zeros, align 4, !tbaa !9
  br label %while.cond

while.end:                                        ; preds = %land.end
  %5 = load i32, i32* %leading_zeros, align 4, !tbaa !9
  %cmp1 = icmp eq i32 %5, 32
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.end
  %6 = bitcast i32* %base to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load i32, i32* %leading_zeros, align 4, !tbaa !9
  %shl = shl i32 1, %7
  %sub = sub i32 %shl, 1
  store i32 %sub, i32* %base, align 4, !tbaa !9
  %8 = bitcast i32* %value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %10 = load i32, i32* %leading_zeros, align 4, !tbaa !9
  %call2 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %9, i32 %10)
  store i32 %call2, i32* %value, align 4, !tbaa !9
  %11 = load i32, i32* %base, align 4, !tbaa !9
  %12 = load i32, i32* %value, align 4, !tbaa !9
  %add = add i32 %11, %12
  store i32 %add, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %base to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %15 = bitcast i32* %leading_zeros to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: nounwind
define hidden signext i16 @aom_rb_read_signed_primitive_refsubexpfin(%struct.aom_read_bit_buffer* %rb, i16 zeroext %n, i16 zeroext %k, i16 signext %ref) #0 {
entry:
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  %scaled_n = alloca i16, align 2
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !15
  store i16 %k, i16* %k.addr, align 2, !tbaa !15
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !15
  %0 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv = zext i16 %0 to i32
  %sub = sub nsw i32 %conv, 1
  %1 = load i16, i16* %ref.addr, align 2, !tbaa !15
  %conv1 = sext i16 %1 to i32
  %add = add nsw i32 %conv1, %sub
  %conv2 = trunc i32 %add to i16
  store i16 %conv2, i16* %ref.addr, align 2, !tbaa !15
  %2 = bitcast i16* %scaled_n to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #4
  %3 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv3 = zext i16 %3 to i32
  %shl = shl i32 %conv3, 1
  %sub4 = sub nsw i32 %shl, 1
  %conv5 = trunc i32 %sub4 to i16
  store i16 %conv5, i16* %scaled_n, align 2, !tbaa !15
  %4 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %5 = load i16, i16* %scaled_n, align 2, !tbaa !15
  %6 = load i16, i16* %k.addr, align 2, !tbaa !15
  %7 = load i16, i16* %ref.addr, align 2, !tbaa !15
  %call = call zeroext i16 @aom_rb_read_primitive_refsubexpfin(%struct.aom_read_bit_buffer* %4, i16 zeroext %5, i16 zeroext %6, i16 zeroext %7)
  %conv6 = zext i16 %call to i32
  %8 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv7 = zext i16 %8 to i32
  %sub8 = sub nsw i32 %conv6, %conv7
  %add9 = add nsw i32 %sub8, 1
  %conv10 = trunc i32 %add9 to i16
  %9 = bitcast i16* %scaled_n to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %9) #4
  ret i16 %conv10
}

; Function Attrs: nounwind
define internal zeroext i16 @aom_rb_read_primitive_refsubexpfin(%struct.aom_read_bit_buffer* %rb, i16 zeroext %n, i16 zeroext %k, i16 zeroext %ref) #0 {
entry:
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %ref.addr = alloca i16, align 2
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !15
  store i16 %k, i16* %k.addr, align 2, !tbaa !15
  store i16 %ref, i16* %ref.addr, align 2, !tbaa !15
  %0 = load i16, i16* %n.addr, align 2, !tbaa !15
  %1 = load i16, i16* %ref.addr, align 2, !tbaa !15
  %2 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %3 = load i16, i16* %n.addr, align 2, !tbaa !15
  %4 = load i16, i16* %k.addr, align 2, !tbaa !15
  %call = call zeroext i16 @aom_rb_read_primitive_subexpfin(%struct.aom_read_bit_buffer* %2, i16 zeroext %3, i16 zeroext %4)
  %call1 = call zeroext i16 @inv_recenter_finite_nonneg(i16 zeroext %0, i16 zeroext %1, i16 zeroext %call)
  ret i16 %call1
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @inv_recenter_finite_nonneg(i16 zeroext %n, i16 zeroext %r, i16 zeroext %v) #2 {
entry:
  %retval = alloca i16, align 2
  %n.addr = alloca i16, align 2
  %r.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %n, i16* %n.addr, align 2, !tbaa !15
  store i16 %r, i16* %r.addr, align 2, !tbaa !15
  store i16 %v, i16* %v.addr, align 2, !tbaa !15
  %0 = load i16, i16* %r.addr, align 2, !tbaa !15
  %conv = zext i16 %0 to i32
  %shl = shl i32 %conv, 1
  %1 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv1 = zext i16 %1 to i32
  %cmp = icmp sle i32 %shl, %conv1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %r.addr, align 2, !tbaa !15
  %3 = load i16, i16* %v.addr, align 2, !tbaa !15
  %call = call zeroext i16 @inv_recenter_nonneg(i16 zeroext %2, i16 zeroext %3)
  store i16 %call, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv3 = zext i16 %4 to i32
  %sub = sub nsw i32 %conv3, 1
  %5 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv4 = zext i16 %5 to i32
  %sub5 = sub nsw i32 %conv4, 1
  %6 = load i16, i16* %r.addr, align 2, !tbaa !15
  %conv6 = zext i16 %6 to i32
  %sub7 = sub nsw i32 %sub5, %conv6
  %conv8 = trunc i32 %sub7 to i16
  %7 = load i16, i16* %v.addr, align 2, !tbaa !15
  %call9 = call zeroext i16 @inv_recenter_nonneg(i16 zeroext %conv8, i16 zeroext %7)
  %conv10 = zext i16 %call9 to i32
  %sub11 = sub nsw i32 %sub, %conv10
  %conv12 = trunc i32 %sub11 to i16
  store i16 %conv12, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.else, %if.then
  %8 = load i16, i16* %retval, align 2
  ret i16 %8
}

; Function Attrs: nounwind
define internal zeroext i16 @aom_rb_read_primitive_subexpfin(%struct.aom_read_bit_buffer* %rb, i16 zeroext %n, i16 zeroext %k) #0 {
entry:
  %retval = alloca i16, align 2
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %n.addr = alloca i16, align 2
  %k.addr = alloca i16, align 2
  %i = alloca i32, align 4
  %mk = alloca i32, align 4
  %b = alloca i32, align 4
  %a = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !15
  store i16 %k, i16* %k.addr, align 2, !tbaa !15
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %i, align 4, !tbaa !9
  %1 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  store i32 0, i32* %mk, align 4, !tbaa !9
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  br label %while.body

while.body:                                       ; preds = %while.cond
  %2 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load i32, i32* %i, align 4, !tbaa !9
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %4 = load i16, i16* %k.addr, align 2, !tbaa !15
  %conv = zext i16 %4 to i32
  %5 = load i32, i32* %i, align 4, !tbaa !9
  %add = add nsw i32 %conv, %5
  %sub = sub nsw i32 %add, 1
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %6 = load i16, i16* %k.addr, align 2, !tbaa !15
  %conv1 = zext i16 %6 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ %conv1, %cond.false ]
  store i32 %cond, i32* %b, align 4, !tbaa !9
  %7 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load i32, i32* %b, align 4, !tbaa !9
  %shl = shl i32 1, %8
  store i32 %shl, i32* %a, align 4, !tbaa !9
  %9 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv2 = zext i16 %9 to i32
  %10 = load i32, i32* %mk, align 4, !tbaa !9
  %11 = load i32, i32* %a, align 4, !tbaa !9
  %mul = mul nsw i32 3, %11
  %add3 = add nsw i32 %10, %mul
  %cmp = icmp sle i32 %conv2, %add3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %12 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %13 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv5 = zext i16 %13 to i32
  %14 = load i32, i32* %mk, align 4, !tbaa !9
  %sub6 = sub nsw i32 %conv5, %14
  %conv7 = trunc i32 %sub6 to i16
  %call = call zeroext i16 @aom_rb_read_primitive_quniform(%struct.aom_read_bit_buffer* %12, i16 zeroext %conv7)
  %conv8 = zext i16 %call to i32
  %15 = load i32, i32* %mk, align 4, !tbaa !9
  %add9 = add nsw i32 %conv8, %15
  %conv10 = trunc i32 %add9 to i16
  store i16 %conv10, i16* %retval, align 2
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end
  %16 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call11 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %16)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.end17, label %if.then13

if.then13:                                        ; preds = %if.end
  %17 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %18 = load i32, i32* %b, align 4, !tbaa !9
  %call14 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %17, i32 %18)
  %19 = load i32, i32* %mk, align 4, !tbaa !9
  %add15 = add nsw i32 %call14, %19
  %conv16 = trunc i32 %add15 to i16
  store i16 %conv16, i16* %retval, align 2
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %if.end
  %20 = load i32, i32* %i, align 4, !tbaa !9
  %add18 = add nsw i32 %20, 1
  store i32 %add18, i32* %i, align 4, !tbaa !9
  %21 = load i32, i32* %a, align 4, !tbaa !9
  %22 = load i32, i32* %mk, align 4, !tbaa !9
  %add19 = add nsw i32 %22, %21
  store i32 %add19, i32* %mk, align 4, !tbaa !9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end17, %if.then13, %if.then
  %23 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup21 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

cleanup21:                                        ; preds = %cleanup
  %25 = bitcast i32* %mk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = load i16, i16* %retval, align 2
  ret i16 %27
}

; Function Attrs: inlinehint nounwind
define internal zeroext i16 @inv_recenter_nonneg(i16 zeroext %r, i16 zeroext %v) #2 {
entry:
  %retval = alloca i16, align 2
  %r.addr = alloca i16, align 2
  %v.addr = alloca i16, align 2
  store i16 %r, i16* %r.addr, align 2, !tbaa !15
  store i16 %v, i16* %v.addr, align 2, !tbaa !15
  %0 = load i16, i16* %v.addr, align 2, !tbaa !15
  %conv = zext i16 %0 to i32
  %1 = load i16, i16* %r.addr, align 2, !tbaa !15
  %conv1 = zext i16 %1 to i32
  %shl = shl i32 %conv1, 1
  %cmp = icmp sgt i32 %conv, %shl
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %v.addr, align 2, !tbaa !15
  store i16 %2, i16* %retval, align 2
  br label %return

if.else:                                          ; preds = %entry
  %3 = load i16, i16* %v.addr, align 2, !tbaa !15
  %conv3 = zext i16 %3 to i32
  %and = and i32 %conv3, 1
  %cmp4 = icmp eq i32 %and, 0
  br i1 %cmp4, label %if.then6, label %if.else10

if.then6:                                         ; preds = %if.else
  %4 = load i16, i16* %v.addr, align 2, !tbaa !15
  %conv7 = zext i16 %4 to i32
  %shr = ashr i32 %conv7, 1
  %5 = load i16, i16* %r.addr, align 2, !tbaa !15
  %conv8 = zext i16 %5 to i32
  %add = add nsw i32 %shr, %conv8
  %conv9 = trunc i32 %add to i16
  store i16 %conv9, i16* %retval, align 2
  br label %return

if.else10:                                        ; preds = %if.else
  %6 = load i16, i16* %r.addr, align 2, !tbaa !15
  %conv11 = zext i16 %6 to i32
  %7 = load i16, i16* %v.addr, align 2, !tbaa !15
  %conv12 = zext i16 %7 to i32
  %add13 = add nsw i32 %conv12, 1
  %shr14 = ashr i32 %add13, 1
  %sub = sub nsw i32 %conv11, %shr14
  %conv15 = trunc i32 %sub to i16
  store i16 %conv15, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.else10, %if.then6, %if.then
  %8 = load i16, i16* %retval, align 2
  ret i16 %8
}

; Function Attrs: nounwind
define internal zeroext i16 @aom_rb_read_primitive_quniform(%struct.aom_read_bit_buffer* %rb, i16 zeroext %n) #0 {
entry:
  %retval = alloca i16, align 2
  %rb.addr = alloca %struct.aom_read_bit_buffer*, align 4
  %n.addr = alloca i16, align 2
  %l = alloca i32, align 4
  %m = alloca i32, align 4
  %v = alloca i32, align 4
  store %struct.aom_read_bit_buffer* %rb, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  store i16 %n, i16* %n.addr, align 2, !tbaa !15
  %0 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv = zext i16 %0 to i32
  %cmp = icmp sle i32 %conv, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i16 0, i16* %retval, align 2
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv2 = zext i16 %2 to i32
  %call = call i32 @get_msb(i32 %conv2)
  %add = add nsw i32 %call, 1
  store i32 %add, i32* %l, align 4, !tbaa !9
  %3 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %l, align 4, !tbaa !9
  %shl = shl i32 1, %4
  %5 = load i16, i16* %n.addr, align 2, !tbaa !15
  %conv3 = zext i16 %5 to i32
  %sub = sub nsw i32 %shl, %conv3
  store i32 %sub, i32* %m, align 4, !tbaa !9
  %6 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %8 = load i32, i32* %l, align 4, !tbaa !9
  %sub4 = sub nsw i32 %8, 1
  %call5 = call i32 @aom_rb_read_literal(%struct.aom_read_bit_buffer* %7, i32 %sub4)
  store i32 %call5, i32* %v, align 4, !tbaa !9
  %9 = load i32, i32* %v, align 4, !tbaa !9
  %10 = load i32, i32* %m, align 4, !tbaa !9
  %cmp6 = icmp slt i32 %9, %10
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %11 = load i32, i32* %v, align 4, !tbaa !9
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %12 = load i32, i32* %v, align 4, !tbaa !9
  %shl8 = shl i32 %12, 1
  %13 = load i32, i32* %m, align 4, !tbaa !9
  %sub9 = sub nsw i32 %shl8, %13
  %14 = load %struct.aom_read_bit_buffer*, %struct.aom_read_bit_buffer** %rb.addr, align 4, !tbaa !2
  %call10 = call i32 @aom_rb_read_bit(%struct.aom_read_bit_buffer* %14)
  %add11 = add nsw i32 %sub9, %call10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %11, %cond.true ], [ %add11, %cond.false ]
  %conv12 = trunc i32 %cond to i16
  store i16 %conv12, i16* %retval, align 2
  %15 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %18 = load i16, i16* %retval, align 2
  ret i16 %18
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #2 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !9
  %0 = load i32, i32* %n.addr, align 4, !tbaa !9
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone speculatable willreturn }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 8}
!7 = !{!"aom_read_bit_buffer", !3, i64 0, !3, i64 4, !8, i64 8, !3, i64 12, !3, i64 16}
!8 = !{!"int", !4, i64 0}
!9 = !{!8, !8, i64 0}
!10 = !{!7, !3, i64 0}
!11 = !{!7, !3, i64 4}
!12 = !{!4, !4, i64 0}
!13 = !{!7, !3, i64 16}
!14 = !{!7, !3, i64 12}
!15 = !{!16, !16, i64 0}
!16 = !{!"short", !4, i64 0}
