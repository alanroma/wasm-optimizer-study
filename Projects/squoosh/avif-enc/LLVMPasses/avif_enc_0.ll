; ModuleID = 'enc/avif_enc.cpp'
source_filename = "enc/avif_enc.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.emscripten::val" = type { %"struct.emscripten::internal::_EM_VAL"* }
%"struct.emscripten::internal::_EM_VAL" = type opaque
%struct.EmscriptenBindingInitializer_my_module = type { i8 }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%struct.AvifOptions = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifRWData = type { i8*, i32 }
%struct.avifImage = type { i32, i32, i32, i32, i32, i32, [3 x i8*], [3 x i32], i32, i32, i8*, i32, i32, %struct.avifRWData, i32, i32, i32, i32, %struct.avifPixelAspectRatioBox, %struct.avifCleanApertureBox, %struct.avifImageRotation, %struct.avifImageMirror, %struct.avifRWData, %struct.avifRWData }
%struct.avifPixelAspectRatioBox = type { i32, i32 }
%struct.avifCleanApertureBox = type { i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.avifImageRotation = type { i8 }
%struct.avifImageMirror = type { i8 }
%struct.avifRGBImage = type { i32, i32, i32, i32, i32, i32, i8*, i32 }
%struct.avifEncoder = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i64, %struct.avifIOStats, %struct.avifEncoderData* }
%struct.avifIOStats = type { i32, i32 }
%struct.avifEncoderData = type opaque
%"struct.emscripten::memory_view" = type { i32, i8* }
%"class.emscripten::value_object" = type { i8 }
%"class.emscripten::internal::noncopyable" = type { i8 }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2" = type { i8 }
%struct.anon.3 = type { i32, [1 x i8] }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList" = type { i8 }
%"struct.emscripten::internal::WireTypePack" = type { %"struct.std::__2::array" }
%"struct.std::__2::array" = type { [1 x %"union.emscripten::internal::GenericWireType"] }
%"union.emscripten::internal::GenericWireType" = type { double }
%union.anon.1 = type { i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator" = type { i8 }

$_ZN10emscripten3val6globalEPKc = comdat any

$_ZN10emscripten3valD2Ev = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv = comdat any

$_ZN10emscripten3val4nullEv = comdat any

$_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_ = comdat any

$_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_ = comdat any

$_ZN10emscripten3valaSEOS0_ = comdat any

$_ZN10emscripten12value_objectI11AvifOptionsEC2EPKc = comdat any

$_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_ = comdat any

$_ZN10emscripten12value_objectI11AvifOptionsED2Ev = comdat any

$_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEii11AvifOptionsEJEEEvPKcPFT_DpT0_EDpT1_ = comdat any

$_ZN10emscripten3valC2EPNS_8internal7_EM_VALE = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZN10emscripten11memory_viewIhEC2EmPKh = comdat any

$_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_ = comdat any

$_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_ = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv = comdat any

$_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv = comdat any

$_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_ = comdat any

$_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE = comdat any

$_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_ = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv = comdat any

$_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv = comdat any

$_ZN10emscripten8internal11noncopyableC2Ev = comdat any

$_ZN10emscripten8internal15raw_constructorI11AvifOptionsJEEEPT_DpNS0_11BindingTypeIT0_vE8WireTypeE = comdat any

$_ZN10emscripten8internal14raw_destructorI11AvifOptionsEEvPT_ = comdat any

$_ZN10emscripten8internal6TypeIDI11AvifOptionsvE3getEv = comdat any

$_ZN10emscripten8internal12getSignatureIP11AvifOptionsJEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal12getSignatureIvJP11AvifOptionsEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11LightTypeIDI11AvifOptionsE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJP11AvifOptionsEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiEEEPKcv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJvP11AvifOptionsEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJviEEEPKcv = comdat any

$_ZN10emscripten8internal11noncopyableD2Ev = comdat any

$_ZN10emscripten8internal12MemberAccessI11AvifOptionsiE7getWireIS2_EEiRKMS2_iRKT_ = comdat any

$_ZN10emscripten8internal12MemberAccessI11AvifOptionsiE7setWireIS2_EEvRKMS2_iRT_i = comdat any

$_ZN10emscripten8internal6TypeIDIivE3getEv = comdat any

$_ZN10emscripten8internal12getSignatureIiJRKM11AvifOptionsiRKS2_EEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal10getContextIM11AvifOptionsiEEPT_RKS4_ = comdat any

$_ZN10emscripten8internal12getSignatureIvJRKM11AvifOptionsiRS2_iEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi = comdat any

$_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi = comdat any

$_ZN10emscripten8internal11LightTypeIDIiE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJiRKM11AvifOptionsiRKS2_EEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJvRKM11AvifOptionsiRS2_iEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcv = comdat any

$_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEii11AvifOptionsEE6invokeEPFS2_S9_iiSA_EPNS0_11BindingTypeIS9_vEUt_EiiPSA_ = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEE8getTypesEv = comdat any

$_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_ = comdat any

$_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E = comdat any

$_ZN10emscripten8internal18GenericBindingTypeI11AvifOptionsE12fromWireTypeEPS2_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii11AvifOptionsEEEE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcv = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types = comdat any

$_ZTSN10emscripten11memory_viewIhEE = comdat any

$_ZTIN10emscripten11memory_viewIhEE = comdat any

$_ZTS11AvifOptions = comdat any

$_ZTI11AvifOptions = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJviEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii11AvifOptionsEEEE3getEvE5types = comdat any

$_ZTSN10emscripten3valE = comdat any

$_ZTIN10emscripten3valE = comdat any

$_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTSNSt3__221__basic_string_commonILb1EEE = comdat any

$_ZTINSt3__221__basic_string_commonILb1EEE = comdat any

$_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcvE9signature = comdat any

@_ZL10Uint8Array = internal thread_local global %"class.emscripten::val" zeroinitializer, align 4
@.str = private unnamed_addr constant [11 x i8] c"Uint8Array\00", align 1
@__dso_handle = external hidden global i8
@_ZL47EmscriptenBindingInitializer_my_module_instance = internal global %struct.EmscriptenBindingInitializer_my_module zeroinitializer, align 1
@.str.2 = private unnamed_addr constant [12 x i8] c"AvifOptions\00", align 1
@.str.3 = private unnamed_addr constant [13 x i8] c"minQuantizer\00", align 1
@.str.4 = private unnamed_addr constant [13 x i8] c"maxQuantizer\00", align 1
@.str.5 = private unnamed_addr constant [18 x i8] c"minQuantizerAlpha\00", align 1
@.str.6 = private unnamed_addr constant [18 x i8] c"maxQuantizerAlpha\00", align 1
@.str.7 = private unnamed_addr constant [13 x i8] c"tileRowsLog2\00", align 1
@.str.8 = private unnamed_addr constant [13 x i8] c"tileColsLog2\00", align 1
@.str.9 = private unnamed_addr constant [6 x i8] c"speed\00", align 1
@.str.10 = private unnamed_addr constant [10 x i8] c"subsample\00", align 1
@.str.11 = private unnamed_addr constant [7 x i8] c"encode\00", align 1
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types = linkonce_odr hidden constant [1 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten11memory_viewIhEE to i8*)], comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN10emscripten11memory_viewIhEE = linkonce_odr hidden constant [31 x i8] c"N10emscripten11memory_viewIhEE\00", comdat, align 1
@_ZTIN10emscripten11memory_viewIhEE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTSN10emscripten11memory_viewIhEE, i32 0, i32 0) }, comdat, align 4
@_ZTS11AvifOptions = linkonce_odr hidden constant [14 x i8] c"11AvifOptions\00", comdat, align 1
@_ZTI11AvifOptions = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @_ZTS11AvifOptions, i32 0, i32 0) }, comdat, align 4
@_ZZN10emscripten8internal19getGenericSignatureIJiEEEPKcvE9signature = linkonce_odr hidden constant [2 x i8] c"i\00", comdat, align 1
@_ZZN10emscripten8internal19getGenericSignatureIJviEEEPKcvE9signature = linkonce_odr hidden constant [3 x i8] c"vi\00", comdat, align 1
@_ZTIi = external constant i8*
@_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature = linkonce_odr hidden constant [4 x i8] c"iii\00", comdat, align 1
@_ZZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcvE9signature = linkonce_odr hidden constant [5 x i8] c"viii\00", comdat, align 1
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii11AvifOptionsEEEE3getEvE5types = linkonce_odr hidden constant [5 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten3valE to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i8*, i32 }* @_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast ({ i8*, i8* }* @_ZTI11AvifOptions to i8*)], comdat, align 16
@_ZTSN10emscripten3valE = linkonce_odr hidden constant [19 x i8] c"N10emscripten3valE\00", comdat, align 1
@_ZTIN10emscripten3valE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN10emscripten3valE, i32 0, i32 0) }, comdat, align 4
@_ZTVN10__cxxabiv121__vmi_class_type_infoE = external global i8*
@_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant [63 x i8] c"NSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE\00", comdat, align 1
@_ZTSNSt3__221__basic_string_commonILb1EEE = linkonce_odr constant [38 x i8] c"NSt3__221__basic_string_commonILb1EEE\00", comdat, align 1
@_ZTINSt3__221__basic_string_commonILb1EEE = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([38 x i8], [38 x i8]* @_ZTSNSt3__221__basic_string_commonILb1EEE, i32 0, i32 0) }, comdat, align 4
@_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant { i8*, i8*, i32, i32, i8*, i32 } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv121__vmi_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([63 x i8], [63 x i8]* @_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, i32 0), i32 0, i32 1, i8* bitcast ({ i8*, i8* }* @_ZTINSt3__221__basic_string_commonILb1EEE to i8*), i32 0 }, comdat, align 4
@_ZZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcvE9signature = linkonce_odr hidden constant [7 x i8] c"iiiiii\00", comdat, align 1
@__tls_guard = internal thread_local global i8 0, align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_avif_enc.cpp, i8* null }]

@_ZN38EmscriptenBindingInitializer_my_moduleC1Ev = hidden unnamed_addr alias %struct.EmscriptenBindingInitializer_my_module* (%struct.EmscriptenBindingInitializer_my_module*), %struct.EmscriptenBindingInitializer_my_module* (%struct.EmscriptenBindingInitializer_my_module*)* @_ZN38EmscriptenBindingInitializer_my_moduleC2Ev
@_ZTHL10Uint8Array = internal alias void (), void ()* @__tls_init

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* sret align 4 @_ZL10Uint8Array, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i32 0, i32 0))
  %0 = call i32 @__cxa_thread_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #3
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* noalias sret align 4 %agg.result, i8* %name) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %name.addr = alloca i8*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i8* %name, i8** %name.addr, align 4
  %1 = load i8*, i8** %name.addr, align 4
  %call = call %"struct.emscripten::internal::_EM_VAL"* @_emval_get_global(i8* %1)
  %call1 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call)
  ret void
}

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* @_ZL10Uint8Array) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"* %0)
  ret %"class.emscripten::val"* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_thread_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: noinline optnone
define hidden void @_Z6encodeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEii11AvifOptions(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.std::__2::basic_string"* %buffer, i32 %width, i32 %height, %struct.AvifOptions* byval(%struct.AvifOptions) align 4 %options) #1 {
entry:
  %result.ptr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %output = alloca %struct.avifRWData, align 4
  %depth = alloca i32, align 4
  %format = alloca i32, align 4
  %image = alloca %struct.avifImage*, align 4
  %rgba = alloca i8*, align 4
  %srcRGB = alloca %struct.avifRGBImage, align 4
  %encoder = alloca %struct.avifEncoder*, align 4
  %encodeResult = alloca i32, align 4
  %nrvo = alloca i1, align 1
  %ref.tmp = alloca %"class.emscripten::val", align 4
  %ref.tmp30 = alloca %"struct.emscripten::memory_view", align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i32 %width, i32* %width.addr, align 4
  store i32 %height, i32* %height.addr, align 4
  %1 = bitcast %struct.avifRWData* %output to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 8, i1 false)
  store i32 8, i32* %depth, align 4
  %subsample = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 7
  %2 = load i32, i32* %subsample, align 4
  switch i32 %2, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
    i32 2, label %sw.bb2
    i32 3, label %sw.bb3
  ]

sw.bb:                                            ; preds = %entry
  store i32 4, i32* %format, align 4
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  store i32 3, i32* %format, align 4
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  store i32 2, i32* %format, align 4
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  store i32 1, i32* %format, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %3 = load i32, i32* %width.addr, align 4
  %4 = load i32, i32* %height.addr, align 4
  %5 = load i32, i32* %depth, align 4
  %6 = load i32, i32* %format, align 4
  %call = call %struct.avifImage* @avifImageCreate(i32 %3, i32 %4, i32 %5, i32 %6)
  store %struct.avifImage* %call, %struct.avifImage** %image, align 4
  %maxQuantizer = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 1
  %7 = load i32, i32* %maxQuantizer, align 4
  %cmp = icmp eq i32 %7, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %sw.epilog
  %minQuantizer = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 0
  %8 = load i32, i32* %minQuantizer, align 4
  %cmp4 = icmp eq i32 %8, 0
  br i1 %cmp4, label %land.lhs.true5, label %if.else

land.lhs.true5:                                   ; preds = %land.lhs.true
  %minQuantizerAlpha = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 2
  %9 = load i32, i32* %minQuantizerAlpha, align 4
  %cmp6 = icmp eq i32 %9, 0
  br i1 %cmp6, label %land.lhs.true7, label %if.else

land.lhs.true7:                                   ; preds = %land.lhs.true5
  %maxQuantizerAlpha = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 3
  %10 = load i32, i32* %maxQuantizerAlpha, align 4
  %cmp8 = icmp eq i32 %10, 0
  br i1 %cmp8, label %land.lhs.true9, label %if.else

land.lhs.true9:                                   ; preds = %land.lhs.true7
  %11 = load i32, i32* %format, align 4
  %cmp10 = icmp eq i32 %11, 1
  br i1 %cmp10, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true9
  %12 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  %matrixCoefficients = getelementptr inbounds %struct.avifImage, %struct.avifImage* %12, i32 0, i32 16
  store i32 0, i32* %matrixCoefficients, align 4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true9, %land.lhs.true7, %land.lhs.true5, %land.lhs.true, %sw.epilog
  %13 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  %matrixCoefficients11 = getelementptr inbounds %struct.avifImage, %struct.avifImage* %13, i32 0, i32 16
  store i32 1, i32* %matrixCoefficients11, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %call12 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %buffer) #3
  store i8* %call12, i8** %rgba, align 4
  %14 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  call void @avifRGBImageSetDefaults(%struct.avifRGBImage* %srcRGB, %struct.avifImage* %14)
  %15 = load i8*, i8** %rgba, align 4
  %pixels = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %srcRGB, i32 0, i32 6
  store i8* %15, i8** %pixels, align 4
  %16 = load i32, i32* %width.addr, align 4
  %mul = mul nsw i32 %16, 4
  %rowBytes = getelementptr inbounds %struct.avifRGBImage, %struct.avifRGBImage* %srcRGB, i32 0, i32 7
  store i32 %mul, i32* %rowBytes, align 4
  %17 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  %call13 = call i32 @avifImageRGBToYUV(%struct.avifImage* %17, %struct.avifRGBImage* %srcRGB)
  %call14 = call %struct.avifEncoder* @avifEncoderCreate()
  store %struct.avifEncoder* %call14, %struct.avifEncoder** %encoder, align 4
  %call15 = call i32 @emscripten_num_logical_cores()
  %18 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %maxThreads = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %18, i32 0, i32 1
  store i32 %call15, i32* %maxThreads, align 4
  %minQuantizer16 = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 0
  %19 = load i32, i32* %minQuantizer16, align 4
  %20 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %minQuantizer17 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %20, i32 0, i32 2
  store i32 %19, i32* %minQuantizer17, align 8
  %maxQuantizer18 = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 1
  %21 = load i32, i32* %maxQuantizer18, align 4
  %22 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %maxQuantizer19 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %22, i32 0, i32 3
  store i32 %21, i32* %maxQuantizer19, align 4
  %minQuantizerAlpha20 = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 2
  %23 = load i32, i32* %minQuantizerAlpha20, align 4
  %24 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %minQuantizerAlpha21 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %24, i32 0, i32 4
  store i32 %23, i32* %minQuantizerAlpha21, align 8
  %maxQuantizerAlpha22 = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 3
  %25 = load i32, i32* %maxQuantizerAlpha22, align 4
  %26 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %maxQuantizerAlpha23 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %26, i32 0, i32 5
  store i32 %25, i32* %maxQuantizerAlpha23, align 4
  %tileRowsLog2 = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 4
  %27 = load i32, i32* %tileRowsLog2, align 4
  %28 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %tileRowsLog224 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %28, i32 0, i32 6
  store i32 %27, i32* %tileRowsLog224, align 8
  %tileColsLog2 = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 5
  %29 = load i32, i32* %tileColsLog2, align 4
  %30 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %tileColsLog225 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %30, i32 0, i32 7
  store i32 %29, i32* %tileColsLog225, align 4
  %speed = getelementptr inbounds %struct.AvifOptions, %struct.AvifOptions* %options, i32 0, i32 6
  %31 = load i32, i32* %speed, align 4
  %32 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %speed26 = getelementptr inbounds %struct.avifEncoder, %struct.avifEncoder* %32, i32 0, i32 8
  store i32 %31, i32* %speed26, align 8
  %33 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  %34 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  %call27 = call i32 @avifEncoderWrite(%struct.avifEncoder* %33, %struct.avifImage* %34, %struct.avifRWData* %output)
  store i32 %call27, i32* %encodeResult, align 4
  store i1 false, i1* %nrvo, align 1
  call void @_ZN10emscripten3val4nullEv(%"class.emscripten::val"* sret align 4 %agg.result)
  %35 = load i32, i32* %encodeResult, align 4
  %cmp28 = icmp eq i32 %35, 0
  br i1 %cmp28, label %if.then29, label %if.end33

if.then29:                                        ; preds = %if.end
  %36 = call %"class.emscripten::val"* @_ZTWL10Uint8Array()
  %size = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %output, i32 0, i32 1
  %37 = load i32, i32* %size, align 4
  %data = getelementptr inbounds %struct.avifRWData, %struct.avifRWData* %output, i32 0, i32 0
  %38 = load i8*, i8** %data, align 4
  call void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp30, i32 %37, i8* %38)
  call void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* sret align 4 %ref.tmp, %"class.emscripten::val"* %36, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp30)
  %call31 = call nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZN10emscripten3valaSEOS0_(%"class.emscripten::val"* %agg.result, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call32 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %ref.tmp) #3
  br label %if.end33

if.end33:                                         ; preds = %if.then29, %if.end
  %39 = load %struct.avifImage*, %struct.avifImage** %image, align 4
  call void @avifImageDestroy(%struct.avifImage* %39)
  %40 = load %struct.avifEncoder*, %struct.avifEncoder** %encoder, align 4
  call void @avifEncoderDestroy(%struct.avifEncoder* %40)
  call void @avifRWDataFree(%struct.avifRWData* %output)
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %if.end33
  %call34 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %agg.result) #3
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %if.end33
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

declare %struct.avifImage* @avifImageCreate(i32, i32, i32, i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #3
  ret i8* %call
}

declare void @avifRGBImageSetDefaults(%struct.avifRGBImage*, %struct.avifImage*) #5

declare i32 @avifImageRGBToYUV(%struct.avifImage*, %struct.avifRGBImage*) #5

declare %struct.avifEncoder* @avifEncoderCreate() #5

declare i32 @emscripten_num_logical_cores() #5

declare i32 @avifEncoderWrite(%struct.avifEncoder*, %struct.avifImage*, %struct.avifRWData*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten3val4nullEv(%"class.emscripten::val"* noalias sret align 4 %agg.result) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %call = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* inttoptr (i32 2 to %"struct.emscripten::internal::_EM_VAL"*))
  ret void
}

; Function Attrs: noinline
define internal %"class.emscripten::val"* @_ZTWL10Uint8Array() #6 {
  call void @_ZTHL10Uint8Array()
  ret %"class.emscripten::val"* @_ZL10Uint8Array
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  call void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_(%"class.emscripten::val"* sret align 4 %agg.result, %"class.emscripten::val"* %this1, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* @_emval_new, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* noalias sret align 4 %agg.result, i32 %size, i8* %data) #1 comdat {
entry:
  %size.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %1 = load i8*, i8** %data.addr, align 4
  %call = call %"struct.emscripten::memory_view"* @_ZN10emscripten11memory_viewIhEC2EmPKh(%"struct.emscripten::memory_view"* %agg.result, i32 %0, i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.emscripten::val"* @_ZN10emscripten3valaSEOS0_(%"class.emscripten::val"* %this, %"class.emscripten::val"* nonnull align 4 dereferenceable(4) %v) #1 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %v.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"class.emscripten::val"* %v, %"class.emscripten::val"** %v.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"* %0)
  %1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle2 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %1, i32 0, i32 0
  %2 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle2, align 4
  %handle3 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  store %"struct.emscripten::internal::_EM_VAL"* %2, %"struct.emscripten::internal::_EM_VAL"** %handle3, align 4
  %3 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle4 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %3, i32 0, i32 0
  store %"struct.emscripten::internal::_EM_VAL"* null, %"struct.emscripten::internal::_EM_VAL"** %handle4, align 4
  ret %"class.emscripten::val"* %this1
}

declare void @avifImageDestroy(%struct.avifImage*) #5

declare void @avifEncoderDestroy(%struct.avifEncoder*) #5

declare void @avifRWDataFree(%struct.avifRWData*) #5

; Function Attrs: noinline
define internal void @__cxx_global_var_init.1() #0 {
entry:
  %call = call %struct.EmscriptenBindingInitializer_my_module* @_ZN38EmscriptenBindingInitializer_my_moduleC1Ev(%struct.EmscriptenBindingInitializer_my_module* @_ZL47EmscriptenBindingInitializer_my_module_instance)
  ret void
}

; Function Attrs: noinline optnone
define hidden %struct.EmscriptenBindingInitializer_my_module* @_ZN38EmscriptenBindingInitializer_my_moduleC2Ev(%struct.EmscriptenBindingInitializer_my_module* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.EmscriptenBindingInitializer_my_module*, align 4
  %ref.tmp = alloca %"class.emscripten::value_object", align 1
  store %struct.EmscriptenBindingInitializer_my_module* %this, %struct.EmscriptenBindingInitializer_my_module** %this.addr, align 4
  %this1 = load %struct.EmscriptenBindingInitializer_my_module*, %struct.EmscriptenBindingInitializer_my_module** %this.addr, align 4
  %call = call %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsEC2EPKc(%"class.emscripten::value_object"* %ref.tmp, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0))
  %call2 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %ref.tmp, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.3, i32 0, i32 0), i32 0)
  %call3 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call2, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.4, i32 0, i32 0), i32 4)
  %call4 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call3, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.5, i32 0, i32 0), i32 8)
  %call5 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call4, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.6, i32 0, i32 0), i32 12)
  %call6 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call5, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.7, i32 0, i32 0), i32 16)
  %call7 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call6, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.8, i32 0, i32 0), i32 20)
  %call8 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call7, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.9, i32 0, i32 0), i32 24)
  %call9 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call8, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.10, i32 0, i32 0), i32 28)
  %call10 = call %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsED2Ev(%"class.emscripten::value_object"* %ref.tmp) #3
  call void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEii11AvifOptionsEJEEEvPKcPFT_DpT0_EDpT1_(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.11, i32 0, i32 0), void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)* @_Z6encodeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEii11AvifOptions)
  ret %struct.EmscriptenBindingInitializer_my_module* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsEC2EPKc(%"class.emscripten::value_object"* returned %this, i8* %name) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.emscripten::value_object"*, align 4
  %name.addr = alloca i8*, align 4
  %ctor = alloca %struct.AvifOptions* ()*, align 4
  %dtor = alloca void (%struct.AvifOptions*)*, align 4
  store %"class.emscripten::value_object"* %this, %"class.emscripten::value_object"** %this.addr, align 4
  store i8* %name, i8** %name.addr, align 4
  %this1 = load %"class.emscripten::value_object"*, %"class.emscripten::value_object"** %this.addr, align 4
  %0 = bitcast %"class.emscripten::value_object"* %this1 to %"class.emscripten::internal::noncopyable"*
  %call = call %"class.emscripten::internal::noncopyable"* @_ZN10emscripten8internal11noncopyableC2Ev(%"class.emscripten::internal::noncopyable"* %0)
  store %struct.AvifOptions* ()* @_ZN10emscripten8internal15raw_constructorI11AvifOptionsJEEEPT_DpNS0_11BindingTypeIT0_vE8WireTypeE, %struct.AvifOptions* ()** %ctor, align 4
  store void (%struct.AvifOptions*)* @_ZN10emscripten8internal14raw_destructorI11AvifOptionsEEvPT_, void (%struct.AvifOptions*)** %dtor, align 4
  %call2 = call i8* @_ZN10emscripten8internal6TypeIDI11AvifOptionsvE3getEv()
  %1 = load i8*, i8** %name.addr, align 4
  %2 = load %struct.AvifOptions* ()*, %struct.AvifOptions* ()** %ctor, align 4
  %call3 = call i8* @_ZN10emscripten8internal12getSignatureIP11AvifOptionsJEEEPKcPFT_DpT0_E(%struct.AvifOptions* ()* %2)
  %3 = load %struct.AvifOptions* ()*, %struct.AvifOptions* ()** %ctor, align 4
  %4 = bitcast %struct.AvifOptions* ()* %3 to i8*
  %5 = load void (%struct.AvifOptions*)*, void (%struct.AvifOptions*)** %dtor, align 4
  %call4 = call i8* @_ZN10emscripten8internal12getSignatureIvJP11AvifOptionsEEEPKcPFT_DpT0_E(void (%struct.AvifOptions*)* %5)
  %6 = load void (%struct.AvifOptions*)*, void (%struct.AvifOptions*)** %dtor, align 4
  %7 = bitcast void (%struct.AvifOptions*)* %6 to i8*
  call void @_embind_register_value_object(i8* %call2, i8* %1, i8* %call3, i8* %4, i8* %call4, i8* %7)
  ret %"class.emscripten::value_object"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %this, i8* %fieldName, i32 %field) #1 comdat {
entry:
  %this.addr = alloca %"class.emscripten::value_object"*, align 4
  %fieldName.addr = alloca i8*, align 4
  %field.addr = alloca i32, align 4
  %getter = alloca i32 (i32*, %struct.AvifOptions*)*, align 4
  %setter = alloca void (i32*, %struct.AvifOptions*, i32)*, align 4
  store %"class.emscripten::value_object"* %this, %"class.emscripten::value_object"** %this.addr, align 4
  store i8* %fieldName, i8** %fieldName.addr, align 4
  store i32 %field, i32* %field.addr, align 4
  %this1 = load %"class.emscripten::value_object"*, %"class.emscripten::value_object"** %this.addr, align 4
  store i32 (i32*, %struct.AvifOptions*)* @_ZN10emscripten8internal12MemberAccessI11AvifOptionsiE7getWireIS2_EEiRKMS2_iRKT_, i32 (i32*, %struct.AvifOptions*)** %getter, align 4
  store void (i32*, %struct.AvifOptions*, i32)* @_ZN10emscripten8internal12MemberAccessI11AvifOptionsiE7setWireIS2_EEvRKMS2_iRT_i, void (i32*, %struct.AvifOptions*, i32)** %setter, align 4
  %call = call i8* @_ZN10emscripten8internal6TypeIDI11AvifOptionsvE3getEv()
  %0 = load i8*, i8** %fieldName.addr, align 4
  %call2 = call i8* @_ZN10emscripten8internal6TypeIDIivE3getEv()
  %1 = load i32 (i32*, %struct.AvifOptions*)*, i32 (i32*, %struct.AvifOptions*)** %getter, align 4
  %call3 = call i8* @_ZN10emscripten8internal12getSignatureIiJRKM11AvifOptionsiRKS2_EEEPKcPFT_DpT0_E(i32 (i32*, %struct.AvifOptions*)* %1)
  %2 = load i32 (i32*, %struct.AvifOptions*)*, i32 (i32*, %struct.AvifOptions*)** %getter, align 4
  %3 = bitcast i32 (i32*, %struct.AvifOptions*)* %2 to i8*
  %call4 = call i32* @_ZN10emscripten8internal10getContextIM11AvifOptionsiEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %field.addr)
  %4 = bitcast i32* %call4 to i8*
  %call5 = call i8* @_ZN10emscripten8internal6TypeIDIivE3getEv()
  %5 = load void (i32*, %struct.AvifOptions*, i32)*, void (i32*, %struct.AvifOptions*, i32)** %setter, align 4
  %call6 = call i8* @_ZN10emscripten8internal12getSignatureIvJRKM11AvifOptionsiRS2_iEEEPKcPFT_DpT0_E(void (i32*, %struct.AvifOptions*, i32)* %5)
  %6 = load void (i32*, %struct.AvifOptions*, i32)*, void (i32*, %struct.AvifOptions*, i32)** %setter, align 4
  %7 = bitcast void (i32*, %struct.AvifOptions*, i32)* %6 to i8*
  %call7 = call i32* @_ZN10emscripten8internal10getContextIM11AvifOptionsiEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %field.addr)
  %8 = bitcast i32* %call7 to i8*
  call void @_embind_register_value_object_field(i8* %call, i8* %0, i8* %call2, i8* %call3, i8* %3, i8* %4, i8* %call5, i8* %call6, i8* %7, i8* %8)
  ret %"class.emscripten::value_object"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI11AvifOptionsED2Ev(%"class.emscripten::value_object"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.emscripten::value_object"*, align 4
  store %"class.emscripten::value_object"* %this, %"class.emscripten::value_object"** %this.addr, align 4
  %this1 = load %"class.emscripten::value_object"*, %"class.emscripten::value_object"** %this.addr, align 4
  %call = call i8* @_ZN10emscripten8internal6TypeIDI11AvifOptionsvE3getEv()
  call void @_embind_finalize_value_object(i8* %call)
  %0 = bitcast %"class.emscripten::value_object"* %this1 to %"class.emscripten::internal::noncopyable"*
  %call2 = call %"class.emscripten::internal::noncopyable"* @_ZN10emscripten8internal11noncopyableD2Ev(%"class.emscripten::internal::noncopyable"* %0) #3
  ret %"class.emscripten::value_object"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEii11AvifOptionsEJEEEvPKcPFT_DpT0_EDpT1_(i8* %name, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)* %fn) #1 comdat {
entry:
  %name.addr = alloca i8*, align 4
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, align 4
  %args = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2", align 1
  %invoker = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)*, align 4
  store i8* %name, i8** %name.addr, align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)** %fn.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEii11AvifOptionsEE6invokeEPFS2_S9_iiSA_EPNS0_11BindingTypeIS9_vEUt_EiiPSA_, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)** %invoker, align 4
  %0 = load i8*, i8** %name.addr, align 4
  %call = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %args)
  %call1 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %args)
  %1 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)** %invoker, align 4
  %call2 = call i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)* %1)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)** %invoker, align 4
  %3 = bitcast %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)* %2 to i8*
  %4 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)** %fn.addr, align 4
  %5 = bitcast void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)* %4 to i8*
  call void @_embind_register_function(i8* %0, i32 %call, i8** %call1, i8* %call2, i8* %3, i8* %5)
  ret void
}

declare %"struct.emscripten::internal::_EM_VAL"* @_emval_get_global(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* returned %this, %"struct.emscripten::internal::_EM_VAL"* %handle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %handle.addr = alloca %"struct.emscripten::internal::_EM_VAL"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %handle, %"struct.emscripten::internal::_EM_VAL"** %handle.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle2 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %0, %"struct.emscripten::internal::_EM_VAL"** %handle2, align 4
  ret %"class.emscripten::val"* %this1
}

declare void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #2 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #3
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #3
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #2 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #2 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.emscripten::memory_view"* @_ZN10emscripten11memory_viewIhEC2EmPKh(%"struct.emscripten::memory_view"* returned %this, i32 %size, i8* %data) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %size.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  store %"struct.emscripten::memory_view"* %this, %"struct.emscripten::memory_view"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %this1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %this.addr, align 4
  %size2 = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  store i32 %0, i32* %size2, align 4
  %data3 = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %this1, i32 0, i32 1
  %1 = load i8*, i8** %data.addr, align 4
  store i8* %1, i8** %data3, align 4
  ret %"struct.emscripten::memory_view"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %impl.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %argList = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList", align 1
  %argv = alloca %"struct.emscripten::internal::WireTypePack", align 8
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  %call2 = call %"struct.emscripten::internal::WireTypePack"* @_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_(%"struct.emscripten::internal::WireTypePack"* %argv, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %3 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  %call3 = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %argList)
  %call4 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %argList)
  %call5 = call i8* @_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv(%"struct.emscripten::internal::WireTypePack"* %argv)
  %call6 = call %"struct.emscripten::internal::_EM_VAL"* %2(%"struct.emscripten::internal::_EM_VAL"* %3, i32 %call3, i8** %call4, i8* %call5)
  %call7 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call6)
  ret void
}

declare %"struct.emscripten::internal::_EM_VAL"* @_emval_new(%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %__t) #2 comdat {
entry:
  %__t.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"struct.emscripten::memory_view"* %__t, %"struct.emscripten::memory_view"** %__t.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %__t.addr, align 4
  ret %"struct.emscripten::memory_view"* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::WireTypePack"* @_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_(%"struct.emscripten::internal::WireTypePack"* returned %this, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack"*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %cursor = alloca %"union.emscripten::internal::GenericWireType"*, align 4
  store %"struct.emscripten::internal::WireTypePack"* %this, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack"*, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %elements2 = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %elements2) #3
  store %"union.emscripten::internal::GenericWireType"* %call, %"union.emscripten::internal::GenericWireType"** %cursor, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %0) #3
  call void @_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call3)
  ret %"struct.emscripten::internal::WireTypePack"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv()
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv(%"struct.emscripten::internal::WireTypePack"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack"*, align 4
  store %"struct.emscripten::internal::WireTypePack"* %this, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack"*, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %elements) #3
  %0 = bitcast %"union.emscripten::internal::GenericWireType"* %call to i8*
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1 x %"union.emscripten::internal::GenericWireType"], [1 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: alwaysinline
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %first) #7 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %first.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %ref.tmp = alloca %"struct.emscripten::memory_view", align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::memory_view"* %first, %"struct.emscripten::memory_view"** %first.addr, align 4
  %0 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %first.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  call void @_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  call void @_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  call void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %wt) #2 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %wt.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::memory_view"* %wt, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %size = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %0, i32 0, i32 0
  %1 = load i32, i32* %size, align 4
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %3 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %2, align 4
  %w = bitcast %"union.emscripten::internal::GenericWireType"* %3 to [2 x %union.anon.1]*
  %arrayidx = getelementptr inbounds [2 x %union.anon.1], [2 x %union.anon.1]* %w, i32 0, i32 0
  %u = bitcast %union.anon.1* %arrayidx to i32*
  store i32 %1, i32* %u, align 8
  %4 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %data = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %4, i32 0, i32 1
  %5 = load i8*, i8** %data, align 4
  %6 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %7 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %6, align 4
  %w1 = bitcast %"union.emscripten::internal::GenericWireType"* %7 to [2 x %union.anon.1]*
  %arrayidx2 = getelementptr inbounds [2 x %union.anon.1], [2 x %union.anon.1]* %w1, i32 0, i32 1
  %p = bitcast %union.anon.1* %arrayidx2 to i8**
  store i8* %5, i8** %p, align 4
  %8 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %9 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %8, align 4
  %incdec.ptr = getelementptr inbounds %"union.emscripten::internal::GenericWireType", %"union.emscripten::internal::GenericWireType"* %9, i32 1
  store %"union.emscripten::internal::GenericWireType"* %incdec.ptr, %"union.emscripten::internal::GenericWireType"** %8, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_(%"struct.emscripten::memory_view"* noalias sret align 4 %agg.result, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %mv) #2 comdat {
entry:
  %mv.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"struct.emscripten::memory_view"* %mv, %"struct.emscripten::memory_view"** %mv.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %mv.addr, align 4
  %1 = bitcast %"struct.emscripten::memory_view"* %agg.result to i8*
  %2 = bitcast %"struct.emscripten::memory_view"* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0) #2 comdat {
entry:
  %.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  store %"union.emscripten::internal::GenericWireType"** %0, %"union.emscripten::internal::GenericWireType"*** %.addr, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv() #2 comdat {
entry:
  ret i8** getelementptr inbounds ([1 x i8*], [1 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1 x %"union.emscripten::internal::GenericWireType"], [1 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::internal::noncopyable"* @_ZN10emscripten8internal11noncopyableC2Ev(%"class.emscripten::internal::noncopyable"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.emscripten::internal::noncopyable"*, align 4
  store %"class.emscripten::internal::noncopyable"* %this, %"class.emscripten::internal::noncopyable"** %this.addr, align 4
  %this1 = load %"class.emscripten::internal::noncopyable"*, %"class.emscripten::internal::noncopyable"** %this.addr, align 4
  ret %"class.emscripten::internal::noncopyable"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.AvifOptions* @_ZN10emscripten8internal15raw_constructorI11AvifOptionsJEEEPT_DpNS0_11BindingTypeIT0_vE8WireTypeE() #1 comdat {
entry:
  %call = call noalias nonnull i8* @_Znwm(i32 32) #12
  %0 = bitcast i8* %call to %struct.AvifOptions*
  %1 = bitcast %struct.AvifOptions* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %1, i8 0, i32 32, i1 false)
  ret %struct.AvifOptions* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal14raw_destructorI11AvifOptionsEEvPT_(%struct.AvifOptions* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca %struct.AvifOptions*, align 4
  store %struct.AvifOptions* %ptr, %struct.AvifOptions** %ptr.addr, align 4
  %0 = load %struct.AvifOptions*, %struct.AvifOptions** %ptr.addr, align 4
  %isnull = icmp eq %struct.AvifOptions* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %struct.AvifOptions* %0 to i8*
  call void @_ZdlPv(i8* %1) #13
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

declare void @_embind_register_value_object(i8*, i8*, i8*, i8*, i8*, i8*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal6TypeIDI11AvifOptionsvE3getEv() #1 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal11LightTypeIDI11AvifOptionsE3getEv()
  ret i8* %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIP11AvifOptionsJEEEPKcPFT_DpT0_E(%struct.AvifOptions* ()* %0) #7 comdat {
entry:
  %.addr = alloca %struct.AvifOptions* ()*, align 4
  store %struct.AvifOptions* ()* %0, %struct.AvifOptions* ()** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJP11AvifOptionsEEEPKcv()
  ret i8* %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIvJP11AvifOptionsEEEPKcPFT_DpT0_E(void (%struct.AvifOptions*)* %0) #7 comdat {
entry:
  %.addr = alloca void (%struct.AvifOptions*)*, align 4
  store void (%struct.AvifOptions*)* %0, void (%struct.AvifOptions*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJvP11AvifOptionsEEEPKcv()
  ret i8* %call
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #9

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #10

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal11LightTypeIDI11AvifOptionsE3getEv() #2 comdat {
entry:
  ret i8* bitcast ({ i8*, i8* }* @_ZTI11AvifOptions to i8*)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJP11AvifOptionsEEEPKcv() #7 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiEEEPKcv() #2 comdat {
entry:
  ret i8* getelementptr inbounds ([2 x i8], [2 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJvP11AvifOptionsEEEPKcv() #7 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJviEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJviEEEPKcv() #2 comdat {
entry:
  ret i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJviEEEPKcvE9signature, i32 0, i32 0)
}

declare void @_embind_finalize_value_object(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::internal::noncopyable"* @_ZN10emscripten8internal11noncopyableD2Ev(%"class.emscripten::internal::noncopyable"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.emscripten::internal::noncopyable"*, align 4
  store %"class.emscripten::internal::noncopyable"* %this, %"class.emscripten::internal::noncopyable"** %this.addr, align 4
  %this1 = load %"class.emscripten::internal::noncopyable"*, %"class.emscripten::internal::noncopyable"** %this.addr, align 4
  ret %"class.emscripten::internal::noncopyable"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal12MemberAccessI11AvifOptionsiE7getWireIS2_EEiRKMS2_iRKT_(i32* nonnull align 4 dereferenceable(4) %field, %struct.AvifOptions* nonnull align 4 dereferenceable(32) %ptr) #1 comdat {
entry:
  %field.addr = alloca i32*, align 4
  %ptr.addr = alloca %struct.AvifOptions*, align 4
  store i32* %field, i32** %field.addr, align 4
  store %struct.AvifOptions* %ptr, %struct.AvifOptions** %ptr.addr, align 4
  %0 = load %struct.AvifOptions*, %struct.AvifOptions** %ptr.addr, align 4
  %1 = load i32*, i32** %field.addr, align 4
  %2 = load i32, i32* %1, align 4
  %3 = bitcast %struct.AvifOptions* %0 to i8*
  %memptr.offset = getelementptr inbounds i8, i8* %3, i32 %2
  %4 = bitcast i8* %memptr.offset to i32*
  %call = call i32 @_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi(i32* nonnull align 4 dereferenceable(4) %4)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8internal12MemberAccessI11AvifOptionsiE7setWireIS2_EEvRKMS2_iRT_i(i32* nonnull align 4 dereferenceable(4) %field, %struct.AvifOptions* nonnull align 4 dereferenceable(32) %ptr, i32 %value) #1 comdat {
entry:
  %field.addr = alloca i32*, align 4
  %ptr.addr = alloca %struct.AvifOptions*, align 4
  %value.addr = alloca i32, align 4
  store i32* %field, i32** %field.addr, align 4
  store %struct.AvifOptions* %ptr, %struct.AvifOptions** %ptr.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %0 = load i32, i32* %value.addr, align 4
  %call = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %0)
  %1 = load %struct.AvifOptions*, %struct.AvifOptions** %ptr.addr, align 4
  %2 = load i32*, i32** %field.addr, align 4
  %3 = load i32, i32* %2, align 4
  %4 = bitcast %struct.AvifOptions* %1 to i8*
  %memptr.offset = getelementptr inbounds i8, i8* %4, i32 %3
  %5 = bitcast i8* %memptr.offset to i32*
  store i32 %call, i32* %5, align 4
  ret void
}

declare void @_embind_register_value_object_field(i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal6TypeIDIivE3getEv() #1 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal11LightTypeIDIiE3getEv()
  ret i8* %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIiJRKM11AvifOptionsiRKS2_EEEPKcPFT_DpT0_E(i32 (i32*, %struct.AvifOptions*)* %0) #7 comdat {
entry:
  %.addr = alloca i32 (i32*, %struct.AvifOptions*)*, align 4
  store i32 (i32*, %struct.AvifOptions*)* %0, i32 (i32*, %struct.AvifOptions*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJiRKM11AvifOptionsiRKS2_EEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN10emscripten8internal10getContextIM11AvifOptionsiEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %t) #1 comdat {
entry:
  %t.addr = alloca i32*, align 4
  store i32* %t, i32** %t.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 4) #12
  %0 = bitcast i8* %call to i32*
  %1 = load i32*, i32** %t.addr, align 4
  %2 = load i32, i32* %1, align 4
  store i32 %2, i32* %0, align 4
  ret i32* %0
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIvJRKM11AvifOptionsiRS2_iEEEPKcPFT_DpT0_E(void (i32*, %struct.AvifOptions*, i32)* %0) #7 comdat {
entry:
  %.addr = alloca void (i32*, %struct.AvifOptions*, i32)*, align 4
  store void (i32*, %struct.AvifOptions*, i32)* %0, void (i32*, %struct.AvifOptions*, i32)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJvRKM11AvifOptionsiRS2_iEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi(i32* nonnull align 4 dereferenceable(4) %v) #2 comdat {
entry:
  %v.addr = alloca i32*, align 4
  store i32* %v, i32** %v.addr, align 4
  %0 = load i32*, i32** %v.addr, align 4
  %1 = load i32, i32* %0, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %v) #2 comdat {
entry:
  %v.addr = alloca i32, align 4
  store i32 %v, i32* %v.addr, align 4
  %0 = load i32, i32* %v.addr, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal11LightTypeIDIiE3getEv() #2 comdat {
entry:
  ret i8* bitcast (i8** @_ZTIi to i8*)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJiRKM11AvifOptionsiRKS2_EEEPKcv() #7 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv() #2 comdat {
entry:
  ret i8* getelementptr inbounds ([4 x i8], [4 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJvRKM11AvifOptionsiRS2_iEEEPKcv() #7 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcv() #2 comdat {
entry:
  ret i8* getelementptr inbounds ([5 x i8], [5 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEii11AvifOptionsEE6invokeEPFS2_S9_iiSA_EPNS0_11BindingTypeIS9_vEUt_EiiPSA_(void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)* %fn, %struct.anon.3* %args, i32 %args1, i32 %args3, %struct.AvifOptions* %args5) #1 comdat {
entry:
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, align 4
  %args.addr = alloca %struct.anon.3*, align 4
  %args.addr2 = alloca i32, align 4
  %args.addr4 = alloca i32, align 4
  %args.addr6 = alloca %struct.AvifOptions*, align 4
  %ref.tmp = alloca %"class.emscripten::val", align 4
  %agg.tmp = alloca %"class.std::__2::basic_string", align 4
  %agg.tmp8 = alloca %struct.AvifOptions, align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)** %fn.addr, align 4
  store %struct.anon.3* %args, %struct.anon.3** %args.addr, align 4
  store i32 %args1, i32* %args.addr2, align 4
  store i32 %args3, i32* %args.addr4, align 4
  store %struct.AvifOptions* %args5, %struct.AvifOptions** %args.addr6, align 4
  %0 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)** %fn.addr, align 4
  %1 = load %struct.anon.3*, %struct.anon.3** %args.addr, align 4
  call void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* sret align 4 %agg.tmp, %struct.anon.3* %1)
  %2 = load i32, i32* %args.addr2, align 4
  %call = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %2)
  %3 = load i32, i32* %args.addr4, align 4
  %call7 = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %3)
  %4 = load %struct.AvifOptions*, %struct.AvifOptions** %args.addr6, align 4
  %call9 = call nonnull align 4 dereferenceable(32) %struct.AvifOptions* @_ZN10emscripten8internal18GenericBindingTypeI11AvifOptionsE12fromWireTypeEPS2_(%struct.AvifOptions* %4)
  %5 = bitcast %struct.AvifOptions* %agg.tmp8 to i8*
  %6 = bitcast %struct.AvifOptions* %call9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 32, i1 false)
  call void %0(%"class.emscripten::val"* sret align 4 %ref.tmp, %"class.std::__2::basic_string"* %agg.tmp, i32 %call, i32 %call7, %struct.AvifOptions* byval(%struct.AvifOptions) align 4 %agg.tmp8)
  %call10 = call %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call11 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %ref.tmp) #3
  %call12 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %agg.tmp) #3
  ret %"struct.emscripten::internal::_EM_VAL"* %call10
}

declare void @_embind_register_function(i8*, i32, i8**, i8*, i8*, i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"** %this.addr, align 4
  ret i32 5
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.2"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii11AvifOptionsEEEE3getEv()
  ret i8** %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)* %0) #7 comdat {
entry:
  %.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)*, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)* %0, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.AvifOptions*)*, %struct.anon.3*, i32, i32, %struct.AvifOptions*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %v) #1 comdat {
entry:
  %v.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %v, %"class.emscripten::val"** %v.addr, align 4
  %0 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %0, i32 0, i32 0
  %1 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_incref(%"struct.emscripten::internal::_EM_VAL"* %1)
  %2 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle1 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %2, i32 0, i32 0
  %3 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle1, align 4
  ret %"struct.emscripten::internal::_EM_VAL"* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, %struct.anon.3* %v) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %v.addr = alloca %struct.anon.3*, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.anon.3* %v, %struct.anon.3** %v.addr, align 4
  %1 = load %struct.anon.3*, %struct.anon.3** %v.addr, align 4
  %data = getelementptr inbounds %struct.anon.3, %struct.anon.3* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [1 x i8], [1 x i8]* %data, i32 0, i32 0
  %2 = load %struct.anon.3*, %struct.anon.3** %v.addr, align 4
  %length = getelementptr inbounds %struct.anon.3, %struct.anon.3* %2, i32 0, i32 0
  %3 = load i32, i32* %length, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm(%"class.std::__2::basic_string"* %agg.result, i8* %arraydecay, i32 %3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %struct.AvifOptions* @_ZN10emscripten8internal18GenericBindingTypeI11AvifOptionsE12fromWireTypeEPS2_(%struct.AvifOptions* %p) #2 comdat {
entry:
  %p.addr = alloca %struct.AvifOptions*, align 4
  store %struct.AvifOptions* %p, %struct.AvifOptions** %p.addr, align 4
  %0 = load %struct.AvifOptions*, %struct.AvifOptions** %p.addr, align 4
  ret %struct.AvifOptions* %0
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #11

declare void @_emval_incref(%"struct.emscripten::internal::_EM_VAL"*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm(%"class.std::__2::basic_string"* returned %this, i8* %__s, i32 %__n) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %2)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call5 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #2 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* returned %this) unnamed_addr #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* %1) #3
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii11AvifOptionsEEEE3getEv() #2 comdat {
entry:
  ret i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii11AvifOptionsEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii11AvifOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcv() #7 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcv() #2 comdat {
entry:
  ret i8* getelementptr inbounds ([7 x i8], [7 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_avif_enc.cpp() #0 {
entry:
  call void @__cxx_global_var_init.1()
  ret void
}

; Function Attrs: noinline
define internal void @__tls_init() #0 {
entry:
  %0 = load i8, i8* @__tls_guard, align 1
  %guard.uninitialized = icmp eq i8 %0, 0
  br i1 %guard.uninitialized, label %init, label %exit, !prof !2

init:                                             ; preds = %entry
  store i8 1, i8* @__tls_guard, align 1
  call void @__cxx_global_var_init()
  br label %exit

exit:                                             ; preds = %init, %entry
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { alwaysinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { argmemonly nounwind willreturn }
attributes #9 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #12 = { builtin allocsize(0) }
attributes #13 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1023}
