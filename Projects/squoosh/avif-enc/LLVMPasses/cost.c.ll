; ModuleID = '/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/cost.c'
source_filename = "/data/Code/wasm-optimizer-study/Projects/squoosh/Source/squoosh/codecs/avif/node_modules/libaom/av1/encoder/cost.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@av1_prob_cost = hidden constant [128 x i16] [i16 512, i16 506, i16 501, i16 495, i16 489, i16 484, i16 478, i16 473, i16 467, i16 462, i16 456, i16 451, i16 446, i16 441, i16 435, i16 430, i16 425, i16 420, i16 415, i16 410, i16 405, i16 400, i16 395, i16 390, i16 385, i16 380, i16 375, i16 371, i16 366, i16 361, i16 356, i16 352, i16 347, i16 343, i16 338, i16 333, i16 329, i16 324, i16 320, i16 316, i16 311, i16 307, i16 302, i16 298, i16 294, i16 289, i16 285, i16 281, i16 277, i16 273, i16 268, i16 264, i16 260, i16 256, i16 252, i16 248, i16 244, i16 240, i16 236, i16 232, i16 228, i16 224, i16 220, i16 216, i16 212, i16 209, i16 205, i16 201, i16 197, i16 194, i16 190, i16 186, i16 182, i16 179, i16 175, i16 171, i16 168, i16 164, i16 161, i16 157, i16 153, i16 150, i16 146, i16 143, i16 139, i16 136, i16 132, i16 129, i16 125, i16 122, i16 119, i16 115, i16 112, i16 109, i16 105, i16 102, i16 99, i16 95, i16 92, i16 89, i16 86, i16 82, i16 79, i16 76, i16 73, i16 70, i16 66, i16 63, i16 60, i16 57, i16 54, i16 51, i16 48, i16 45, i16 42, i16 38, i16 35, i16 32, i16 29, i16 26, i16 23, i16 20, i16 18, i16 15, i16 12, i16 9, i16 6, i16 3], align 16

; Function Attrs: nounwind
define hidden void @av1_cost_tokens_from_cdf(i32* %costs, i16* %cdf, i32* %inv_map) #0 {
entry:
  %costs.addr = alloca i32*, align 4
  %cdf.addr = alloca i16*, align 4
  %inv_map.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %prev_cdf = alloca i16, align 2
  %p15 = alloca i16, align 2
  %cleanup.dest.slot = alloca i32, align 4
  store i32* %costs, i32** %costs.addr, align 4, !tbaa !2
  store i16* %cdf, i16** %cdf.addr, align 4, !tbaa !2
  store i32* %inv_map, i32** %inv_map.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i16* %prev_cdf to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %1) #4
  store i16 0, i16* %prev_cdf, align 2, !tbaa !6
  store i32 0, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = bitcast i16* %p15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #4
  %3 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 %4
  %5 = load i16, i16* %arrayidx, align 2, !tbaa !6
  %conv = zext i16 %5 to i32
  %sub = sub nsw i32 32768, %conv
  %6 = load i16, i16* %prev_cdf, align 2, !tbaa !6
  %conv1 = zext i16 %6 to i32
  %sub2 = sub nsw i32 %sub, %conv1
  %conv3 = trunc i32 %sub2 to i16
  store i16 %conv3, i16* %p15, align 2, !tbaa !6
  %7 = load i16, i16* %p15, align 2, !tbaa !6
  %conv4 = zext i16 %7 to i32
  %cmp = icmp slt i32 %conv4, 4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.cond
  br label %cond.end

cond.false:                                       ; preds = %for.cond
  %8 = load i16, i16* %p15, align 2, !tbaa !6
  %conv6 = zext i16 %8 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 4, %cond.true ], [ %conv6, %cond.false ]
  %conv7 = trunc i32 %cond to i16
  store i16 %conv7, i16* %p15, align 2, !tbaa !6
  %9 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx8 = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx8, align 2, !tbaa !6
  %conv9 = zext i16 %11 to i32
  %sub10 = sub nsw i32 32768, %conv9
  %conv11 = trunc i32 %sub10 to i16
  store i16 %conv11, i16* %prev_cdf, align 2, !tbaa !6
  %12 = load i32*, i32** %inv_map.addr, align 4, !tbaa !2
  %tobool = icmp ne i32* %12, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %13 = load i16, i16* %p15, align 2, !tbaa !6
  %call = call i32 @av1_cost_symbol(i16 zeroext %13)
  %14 = load i32*, i32** %costs.addr, align 4, !tbaa !2
  %15 = load i32*, i32** %inv_map.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx12 = getelementptr inbounds i32, i32* %15, i32 %16
  %17 = load i32, i32* %arrayidx12, align 4, !tbaa !8
  %arrayidx13 = getelementptr inbounds i32, i32* %14, i32 %17
  store i32 %call, i32* %arrayidx13, align 4, !tbaa !8
  br label %if.end

if.else:                                          ; preds = %cond.end
  %18 = load i16, i16* %p15, align 2, !tbaa !6
  %call14 = call i32 @av1_cost_symbol(i16 zeroext %18)
  %19 = load i32*, i32** %costs.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx15 = getelementptr inbounds i32, i32* %19, i32 %20
  store i32 %call14, i32* %arrayidx15, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %21 = load i16*, i16** %cdf.addr, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !8
  %arrayidx16 = getelementptr inbounds i16, i16* %21, i32 %22
  %23 = load i16, i16* %arrayidx16, align 2, !tbaa !6
  %conv17 = zext i16 %23 to i32
  %cmp18 = icmp eq i32 %conv17, 0
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end21:                                         ; preds = %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end21, %if.then20
  %24 = bitcast i16* %p15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %24) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %25 = load i32, i32* %i, align 4, !tbaa !8
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4, !tbaa !8
  br label %for.cond

for.end:                                          ; preds = %cleanup
  %26 = bitcast i16* %prev_cdf to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %26) #4
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @av1_cost_symbol(i16 zeroext %p15) #2 {
entry:
  %p15.addr = alloca i16, align 2
  %shift = alloca i32, align 4
  %prob = alloca i32, align 4
  store i16 %p15, i16* %p15.addr, align 2, !tbaa !6
  %0 = load i16, i16* %p15.addr, align 2, !tbaa !6
  %conv = zext i16 %0 to i32
  %call = call i32 @clamp(i32 %conv, i32 1, i32 32767)
  %conv1 = trunc i32 %call to i16
  store i16 %conv1, i16* %p15.addr, align 2, !tbaa !6
  %1 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load i16, i16* %p15.addr, align 2, !tbaa !6
  %conv2 = zext i16 %2 to i32
  %call3 = call i32 @get_msb(i32 %conv2)
  %sub = sub nsw i32 14, %call3
  store i32 %sub, i32* %shift, align 4, !tbaa !8
  %3 = bitcast i32* %prob to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load i16, i16* %p15.addr, align 2, !tbaa !6
  %conv4 = zext i16 %4 to i32
  %5 = load i32, i32* %shift, align 4, !tbaa !8
  %shl = shl i32 %conv4, %5
  %call5 = call zeroext i8 @get_prob(i32 %shl, i32 32768)
  %conv6 = zext i8 %call5 to i32
  store i32 %conv6, i32* %prob, align 4, !tbaa !8
  %6 = load i32, i32* %prob, align 4, !tbaa !8
  %sub7 = sub nsw i32 %6, 128
  %arrayidx = getelementptr inbounds [128 x i16], [128 x i16]* @av1_prob_cost, i32 0, i32 %sub7
  %7 = load i16, i16* %arrayidx, align 2, !tbaa !6
  %conv8 = zext i16 %7 to i32
  %8 = load i32, i32* %shift, align 4, !tbaa !8
  %mul = mul nsw i32 %8, 512
  %add = add nsw i32 %conv8, %mul
  %9 = bitcast i32* %prob to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %shift to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  ret i32 %add
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: inlinehint nounwind
define internal i32 @clamp(i32 %value, i32 %low, i32 %high) #2 {
entry:
  %value.addr = alloca i32, align 4
  %low.addr = alloca i32, align 4
  %high.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4, !tbaa !8
  store i32 %low, i32* %low.addr, align 4, !tbaa !8
  store i32 %high, i32* %high.addr, align 4, !tbaa !8
  %0 = load i32, i32* %value.addr, align 4, !tbaa !8
  %1 = load i32, i32* %low.addr, align 4, !tbaa !8
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %low.addr, align 4, !tbaa !8
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4, !tbaa !8
  %4 = load i32, i32* %high.addr, align 4, !tbaa !8
  %cmp1 = icmp sgt i32 %3, %4
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %5 = load i32, i32* %high.addr, align 4, !tbaa !8
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %6 = load i32, i32* %value.addr, align 4, !tbaa !8
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond = phi i32 [ %5, %cond.true2 ], [ %6, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond5 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  ret i32 %cond5
}

; Function Attrs: inlinehint nounwind
define internal i32 @get_msb(i32 %n) #2 {
entry:
  %n.addr = alloca i32, align 4
  store i32 %n, i32* %n.addr, align 4, !tbaa !8
  %0 = load i32, i32* %n.addr, align 4, !tbaa !8
  %1 = call i32 @llvm.ctlz.i32(i32 %0, i1 false)
  %xor = xor i32 31, %1
  ret i32 %xor
}

; Function Attrs: inlinehint nounwind
define internal zeroext i8 @get_prob(i32 %num, i32 %den) #2 {
entry:
  %num.addr = alloca i32, align 4
  %den.addr = alloca i32, align 4
  %p = alloca i32, align 4
  %clipped_prob = alloca i32, align 4
  store i32 %num, i32* %num.addr, align 4, !tbaa !8
  store i32 %den, i32* %den.addr, align 4, !tbaa !8
  %0 = bitcast i32* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %num.addr, align 4, !tbaa !8
  %conv = zext i32 %1 to i64
  %mul = mul i64 %conv, 256
  %2 = load i32, i32* %den.addr, align 4, !tbaa !8
  %shr = lshr i32 %2, 1
  %conv1 = zext i32 %shr to i64
  %add = add i64 %mul, %conv1
  %3 = load i32, i32* %den.addr, align 4, !tbaa !8
  %conv2 = zext i32 %3 to i64
  %div = udiv i64 %add, %conv2
  %conv3 = trunc i64 %div to i32
  store i32 %conv3, i32* %p, align 4, !tbaa !8
  %4 = bitcast i32* %clipped_prob to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = load i32, i32* %p, align 4, !tbaa !8
  %6 = load i32, i32* %p, align 4, !tbaa !8
  %sub = sub nsw i32 255, %6
  %shr4 = ashr i32 %sub, 23
  %or = or i32 %5, %shr4
  %7 = load i32, i32* %p, align 4, !tbaa !8
  %cmp = icmp eq i32 %7, 0
  %conv5 = zext i1 %cmp to i32
  %or6 = or i32 %or, %conv5
  store i32 %or6, i32* %clipped_prob, align 4, !tbaa !8
  %8 = load i32, i32* %clipped_prob, align 4, !tbaa !8
  %conv7 = trunc i32 %8 to i8
  %9 = bitcast i32* %clipped_prob to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #4
  ret i8 %conv7
}

; Function Attrs: nounwind readnone speculatable willreturn
declare i32 @llvm.ctlz.i32(i32, i1 immarg) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone speculatable willreturn }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"short", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"int", !4, i64 0}
