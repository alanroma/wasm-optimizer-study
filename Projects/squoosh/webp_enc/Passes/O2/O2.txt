[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.00793714 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 6.8073e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0161296 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0167919 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00266089 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0148216 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00373843 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.010749 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00450826 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0347236 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0202814 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00480646 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0132256 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0200779 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0457274 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0203636 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00456625 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0202264 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00450536 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0200352 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0130406 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0135689 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00255883 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0126663 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0102721 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.014062 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0198347 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0197364 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0314835 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0705559 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00152129 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   2.2754e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.0018698 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00255346 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      9.289e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00207055 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0201179 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.521888 seconds.
[PassRunner] (final validation)
