[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    1.73e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000539752 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    2.224e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00247648 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00240269 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 7.6713e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.00553518 seconds.
[PassRunner] (final validation)
