[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.00805452 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 6.8611e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.0401929 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0159274 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0168062 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0026094 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0154546 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00362251 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.0648551 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0055144 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0399973 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0213339 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00565101 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0135508 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                   0.0283331 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0233087 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0468744 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0202781 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00458642 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0199445 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00446562 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0200925 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                   0.00484629 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0130116 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0138098 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00241644 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0124556 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.0673527 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0139725 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0202299 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0196534 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0404795 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0843215 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00151103 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   2.3539e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00193603 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00257897 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      1.0804e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00231437 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0545364 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.776982 seconds.
[PassRunner] (final validation)
