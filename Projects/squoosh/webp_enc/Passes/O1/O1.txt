[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.00234047 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 7.053e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0160271 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.016621 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0025686 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0148477 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0102793 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0353226 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.020732 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00485072 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0131729 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0198872 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0454139 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0201424 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00451088 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0204418 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0044879 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0194996 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0126889 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0132868 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00240809 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0123581 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00997485 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0141209 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0195423 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00224455 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   2.4014e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals...               0.00192073 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00289086 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      2.5361e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.362702 seconds.
[PassRunner] (final validation)
