[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        1.7937e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.000536894 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        2.2056e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.00249405 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.00238439 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     7.095e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.0397302 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.0158293 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0165601 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.00258603 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.0153602 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.00362194 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.0642049 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.0698549 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.00537414 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.0389783 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.020802 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00585701 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0129989 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.0290923 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0238583 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.0470121 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0202473 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00445585 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0203162 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00452968 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0195422 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                       0.00483052 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                       0.0128536 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0133158 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.0024618 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                       0.0123249 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.532125 seconds.
[PassRunner] (final validation)
