[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.00545972 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 3.1079e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00883149 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00874749 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00129118 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00836802 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00197123 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00590405 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00255843 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0196631 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0114503 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00269181 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00687426 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00978421 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0229911 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0112606 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00248473 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00996908 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00248855 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0110076 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00741749 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00710926 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0012764 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00712171 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00563109 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00778882 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00737006 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0108559 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0105692 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0269744 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00102082 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   1.4911e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00120938 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00134939 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      4.885e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00114028 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0110951 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.261777 seconds.
[PassRunner] (final validation)
