[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    1.1632e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000310076 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    1.7765e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000931695 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00148201 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.7952e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00660693 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00829105 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000990496 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00455488 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00148779 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00441504 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00088141 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00229795 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0163805 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00808553 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00204534 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00527525 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00775628 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0167848 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00728993 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00179281 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00796452 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00166854 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0070876 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00432182 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00611412 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000909201 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00416277 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00382979 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00406188 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00667885 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00675403 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0337358 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0230649 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00111976 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   1.6225e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000920278 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.000978918 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      4.958e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.000830732 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.00781434 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-debug...                    0.00222702 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-producers...                5.344e-06 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.221988 seconds.
[PassRunner] (final validation)
