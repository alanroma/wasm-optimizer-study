; ModuleID = 'blur.c'
source_filename = "blur.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @liq_max3(i8* %src, i8* %dst, i32 %width, i32 %height) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %row = alloca i8*, align 4
  %prevrow = alloca i8*, align 4
  %nextrow = alloca i8*, align 4
  %prev = alloca i8, align 1
  %curr = alloca i8, align 1
  %next = alloca i8, align 1
  %i = alloca i32, align 4
  %t1 = alloca i8, align 1
  %t2 = alloca i8, align 1
  %t176 = alloca i8, align 1
  %t288 = alloca i8, align 1
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc120, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !6
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #2
  br label %for.end122

for.body:                                         ; preds = %for.cond
  %4 = bitcast i8** %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul = mul i32 %6, %7
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  store i8* %add.ptr, i8** %row, align 4, !tbaa !2
  %8 = bitcast i8** %prevrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %cmp1 = icmp ugt i32 %10, 1
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %sub = sub i32 %11, 1
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ 0, %cond.false ]
  %12 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul2 = mul i32 %cond, %12
  %add.ptr3 = getelementptr inbounds i8, i8* %9, i32 %mul2
  store i8* %add.ptr3, i8** %prevrow, align 4, !tbaa !2
  %13 = bitcast i8** %nextrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %15 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub4 = sub i32 %15, 1
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %add = add i32 %16, 1
  %cmp5 = icmp ult i32 %sub4, %add
  br i1 %cmp5, label %cond.true6, label %cond.false8

cond.true6:                                       ; preds = %cond.end
  %17 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub7 = sub i32 %17, 1
  br label %cond.end10

cond.false8:                                      ; preds = %cond.end
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %add9 = add i32 %18, 1
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false8, %cond.true6
  %cond11 = phi i32 [ %sub7, %cond.true6 ], [ %add9, %cond.false8 ]
  %19 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul12 = mul i32 %cond11, %19
  %add.ptr13 = getelementptr inbounds i8, i8* %14, i32 %mul12
  store i8* %add.ptr13, i8** %nextrow, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %prev) #2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %curr) #2
  %20 = load i8*, i8** %row, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %20, i32 0
  %21 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %21, i8* %curr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %next) #2
  %22 = load i8*, i8** %row, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %22, i32 0
  %23 = load i8, i8* %arrayidx14, align 1, !tbaa !8
  store i8 %23, i8* %next, align 1, !tbaa !8
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %cond.end10
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %26 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub16 = sub i32 %26, 1
  %cmp17 = icmp ult i32 %25, %sub16
  br i1 %cmp17, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  store i32 5, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #2
  br label %for.end

for.body19:                                       ; preds = %for.cond15
  %28 = load i8, i8* %curr, align 1, !tbaa !8
  store i8 %28, i8* %prev, align 1, !tbaa !8
  %29 = load i8, i8* %next, align 1, !tbaa !8
  store i8 %29, i8* %curr, align 1, !tbaa !8
  %30 = load i8*, i8** %row, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %add20 = add i32 %31, 1
  %arrayidx21 = getelementptr inbounds i8, i8* %30, i32 %add20
  %32 = load i8, i8* %arrayidx21, align 1, !tbaa !8
  store i8 %32, i8* %next, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %t1) #2
  %33 = load i8, i8* %prev, align 1, !tbaa !8
  %conv = zext i8 %33 to i32
  %34 = load i8, i8* %next, align 1, !tbaa !8
  %conv22 = zext i8 %34 to i32
  %cmp23 = icmp sgt i32 %conv, %conv22
  br i1 %cmp23, label %cond.true25, label %cond.false27

cond.true25:                                      ; preds = %for.body19
  %35 = load i8, i8* %prev, align 1, !tbaa !8
  %conv26 = zext i8 %35 to i32
  br label %cond.end29

cond.false27:                                     ; preds = %for.body19
  %36 = load i8, i8* %next, align 1, !tbaa !8
  %conv28 = zext i8 %36 to i32
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false27, %cond.true25
  %cond30 = phi i32 [ %conv26, %cond.true25 ], [ %conv28, %cond.false27 ]
  %conv31 = trunc i32 %cond30 to i8
  store i8 %conv31, i8* %t1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %t2) #2
  %37 = load i8*, i8** %nextrow, align 4, !tbaa !2
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds i8, i8* %37, i32 %38
  %39 = load i8, i8* %arrayidx32, align 1, !tbaa !8
  %conv33 = zext i8 %39 to i32
  %40 = load i8*, i8** %prevrow, align 4, !tbaa !2
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds i8, i8* %40, i32 %41
  %42 = load i8, i8* %arrayidx34, align 1, !tbaa !8
  %conv35 = zext i8 %42 to i32
  %cmp36 = icmp sgt i32 %conv33, %conv35
  br i1 %cmp36, label %cond.true38, label %cond.false41

cond.true38:                                      ; preds = %cond.end29
  %43 = load i8*, i8** %nextrow, align 4, !tbaa !2
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds i8, i8* %43, i32 %44
  %45 = load i8, i8* %arrayidx39, align 1, !tbaa !8
  %conv40 = zext i8 %45 to i32
  br label %cond.end44

cond.false41:                                     ; preds = %cond.end29
  %46 = load i8*, i8** %prevrow, align 4, !tbaa !2
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds i8, i8* %46, i32 %47
  %48 = load i8, i8* %arrayidx42, align 1, !tbaa !8
  %conv43 = zext i8 %48 to i32
  br label %cond.end44

cond.end44:                                       ; preds = %cond.false41, %cond.true38
  %cond45 = phi i32 [ %conv40, %cond.true38 ], [ %conv43, %cond.false41 ]
  %conv46 = trunc i32 %cond45 to i8
  store i8 %conv46, i8* %t2, align 1, !tbaa !8
  %49 = load i8, i8* %curr, align 1, !tbaa !8
  %conv47 = zext i8 %49 to i32
  %50 = load i8, i8* %t1, align 1, !tbaa !8
  %conv48 = zext i8 %50 to i32
  %51 = load i8, i8* %t2, align 1, !tbaa !8
  %conv49 = zext i8 %51 to i32
  %cmp50 = icmp sgt i32 %conv48, %conv49
  br i1 %cmp50, label %cond.true52, label %cond.false54

cond.true52:                                      ; preds = %cond.end44
  %52 = load i8, i8* %t1, align 1, !tbaa !8
  %conv53 = zext i8 %52 to i32
  br label %cond.end56

cond.false54:                                     ; preds = %cond.end44
  %53 = load i8, i8* %t2, align 1, !tbaa !8
  %conv55 = zext i8 %53 to i32
  br label %cond.end56

cond.end56:                                       ; preds = %cond.false54, %cond.true52
  %cond57 = phi i32 [ %conv53, %cond.true52 ], [ %conv55, %cond.false54 ]
  %cmp58 = icmp sgt i32 %conv47, %cond57
  br i1 %cmp58, label %cond.true60, label %cond.false62

cond.true60:                                      ; preds = %cond.end56
  %54 = load i8, i8* %curr, align 1, !tbaa !8
  %conv61 = zext i8 %54 to i32
  br label %cond.end73

cond.false62:                                     ; preds = %cond.end56
  %55 = load i8, i8* %t1, align 1, !tbaa !8
  %conv63 = zext i8 %55 to i32
  %56 = load i8, i8* %t2, align 1, !tbaa !8
  %conv64 = zext i8 %56 to i32
  %cmp65 = icmp sgt i32 %conv63, %conv64
  br i1 %cmp65, label %cond.true67, label %cond.false69

cond.true67:                                      ; preds = %cond.false62
  %57 = load i8, i8* %t1, align 1, !tbaa !8
  %conv68 = zext i8 %57 to i32
  br label %cond.end71

cond.false69:                                     ; preds = %cond.false62
  %58 = load i8, i8* %t2, align 1, !tbaa !8
  %conv70 = zext i8 %58 to i32
  br label %cond.end71

cond.end71:                                       ; preds = %cond.false69, %cond.true67
  %cond72 = phi i32 [ %conv68, %cond.true67 ], [ %conv70, %cond.false69 ]
  br label %cond.end73

cond.end73:                                       ; preds = %cond.end71, %cond.true60
  %cond74 = phi i32 [ %conv61, %cond.true60 ], [ %cond72, %cond.end71 ]
  %conv75 = trunc i32 %cond74 to i8
  %59 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr, i8** %dst.addr, align 4, !tbaa !2
  store i8 %conv75, i8* %59, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %t2) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %t1) #2
  br label %for.inc

for.inc:                                          ; preds = %cond.end73
  %60 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %60, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond15

for.end:                                          ; preds = %for.cond.cleanup18
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %t176) #2
  %61 = load i8, i8* %curr, align 1, !tbaa !8
  %conv77 = zext i8 %61 to i32
  %62 = load i8, i8* %next, align 1, !tbaa !8
  %conv78 = zext i8 %62 to i32
  %cmp79 = icmp sgt i32 %conv77, %conv78
  br i1 %cmp79, label %cond.true81, label %cond.false83

cond.true81:                                      ; preds = %for.end
  %63 = load i8, i8* %curr, align 1, !tbaa !8
  %conv82 = zext i8 %63 to i32
  br label %cond.end85

cond.false83:                                     ; preds = %for.end
  %64 = load i8, i8* %next, align 1, !tbaa !8
  %conv84 = zext i8 %64 to i32
  br label %cond.end85

cond.end85:                                       ; preds = %cond.false83, %cond.true81
  %cond86 = phi i32 [ %conv82, %cond.true81 ], [ %conv84, %cond.false83 ]
  %conv87 = trunc i32 %cond86 to i8
  store i8 %conv87, i8* %t176, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %t288) #2
  %65 = load i8*, i8** %nextrow, align 4, !tbaa !2
  %66 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub89 = sub i32 %66, 1
  %arrayidx90 = getelementptr inbounds i8, i8* %65, i32 %sub89
  %67 = load i8, i8* %arrayidx90, align 1, !tbaa !8
  %conv91 = zext i8 %67 to i32
  %68 = load i8*, i8** %prevrow, align 4, !tbaa !2
  %69 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub92 = sub i32 %69, 1
  %arrayidx93 = getelementptr inbounds i8, i8* %68, i32 %sub92
  %70 = load i8, i8* %arrayidx93, align 1, !tbaa !8
  %conv94 = zext i8 %70 to i32
  %cmp95 = icmp sgt i32 %conv91, %conv94
  br i1 %cmp95, label %cond.true97, label %cond.false101

cond.true97:                                      ; preds = %cond.end85
  %71 = load i8*, i8** %nextrow, align 4, !tbaa !2
  %72 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub98 = sub i32 %72, 1
  %arrayidx99 = getelementptr inbounds i8, i8* %71, i32 %sub98
  %73 = load i8, i8* %arrayidx99, align 1, !tbaa !8
  %conv100 = zext i8 %73 to i32
  br label %cond.end105

cond.false101:                                    ; preds = %cond.end85
  %74 = load i8*, i8** %prevrow, align 4, !tbaa !2
  %75 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub102 = sub i32 %75, 1
  %arrayidx103 = getelementptr inbounds i8, i8* %74, i32 %sub102
  %76 = load i8, i8* %arrayidx103, align 1, !tbaa !8
  %conv104 = zext i8 %76 to i32
  br label %cond.end105

cond.end105:                                      ; preds = %cond.false101, %cond.true97
  %cond106 = phi i32 [ %conv100, %cond.true97 ], [ %conv104, %cond.false101 ]
  %conv107 = trunc i32 %cond106 to i8
  store i8 %conv107, i8* %t288, align 1, !tbaa !8
  %77 = load i8, i8* %t176, align 1, !tbaa !8
  %conv108 = zext i8 %77 to i32
  %78 = load i8, i8* %t288, align 1, !tbaa !8
  %conv109 = zext i8 %78 to i32
  %cmp110 = icmp sgt i32 %conv108, %conv109
  br i1 %cmp110, label %cond.true112, label %cond.false114

cond.true112:                                     ; preds = %cond.end105
  %79 = load i8, i8* %t176, align 1, !tbaa !8
  %conv113 = zext i8 %79 to i32
  br label %cond.end116

cond.false114:                                    ; preds = %cond.end105
  %80 = load i8, i8* %t288, align 1, !tbaa !8
  %conv115 = zext i8 %80 to i32
  br label %cond.end116

cond.end116:                                      ; preds = %cond.false114, %cond.true112
  %cond117 = phi i32 [ %conv113, %cond.true112 ], [ %conv115, %cond.false114 ]
  %conv118 = trunc i32 %cond117 to i8
  %81 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr119 = getelementptr inbounds i8, i8* %81, i32 1
  store i8* %incdec.ptr119, i8** %dst.addr, align 4, !tbaa !2
  store i8 %conv118, i8* %81, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %t288) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %t176) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %next) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %curr) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %prev) #2
  %82 = bitcast i8** %nextrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #2
  %83 = bitcast i8** %prevrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #2
  %84 = bitcast i8** %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #2
  br label %for.inc120

for.inc120:                                       ; preds = %cond.end116
  %85 = load i32, i32* %j, align 4, !tbaa !6
  %inc121 = add i32 %85, 1
  store i32 %inc121, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end122:                                       ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @liq_min3(i8* %src, i8* %dst, i32 %width, i32 %height) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %row = alloca i8*, align 4
  %prevrow = alloca i8*, align 4
  %nextrow = alloca i8*, align 4
  %prev = alloca i8, align 1
  %curr = alloca i8, align 1
  %next = alloca i8, align 1
  %i = alloca i32, align 4
  %t1 = alloca i8, align 1
  %t2 = alloca i8, align 1
  %t176 = alloca i8, align 1
  %t288 = alloca i8, align 1
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc120, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !6
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #2
  br label %for.end122

for.body:                                         ; preds = %for.cond
  %4 = bitcast i8** %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul = mul i32 %6, %7
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  store i8* %add.ptr, i8** %row, align 4, !tbaa !2
  %8 = bitcast i8** %prevrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %10 = load i32, i32* %j, align 4, !tbaa !6
  %cmp1 = icmp ugt i32 %10, 1
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %11 = load i32, i32* %j, align 4, !tbaa !6
  %sub = sub i32 %11, 1
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ 0, %cond.false ]
  %12 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul2 = mul i32 %cond, %12
  %add.ptr3 = getelementptr inbounds i8, i8* %9, i32 %mul2
  store i8* %add.ptr3, i8** %prevrow, align 4, !tbaa !2
  %13 = bitcast i8** %nextrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %15 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub4 = sub i32 %15, 1
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %add = add i32 %16, 1
  %cmp5 = icmp ult i32 %sub4, %add
  br i1 %cmp5, label %cond.true6, label %cond.false8

cond.true6:                                       ; preds = %cond.end
  %17 = load i32, i32* %height.addr, align 4, !tbaa !6
  %sub7 = sub i32 %17, 1
  br label %cond.end10

cond.false8:                                      ; preds = %cond.end
  %18 = load i32, i32* %j, align 4, !tbaa !6
  %add9 = add i32 %18, 1
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false8, %cond.true6
  %cond11 = phi i32 [ %sub7, %cond.true6 ], [ %add9, %cond.false8 ]
  %19 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul12 = mul i32 %cond11, %19
  %add.ptr13 = getelementptr inbounds i8, i8* %14, i32 %mul12
  store i8* %add.ptr13, i8** %nextrow, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %prev) #2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %curr) #2
  %20 = load i8*, i8** %row, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %20, i32 0
  %21 = load i8, i8* %arrayidx, align 1, !tbaa !8
  store i8 %21, i8* %curr, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %next) #2
  %22 = load i8*, i8** %row, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i8, i8* %22, i32 0
  %23 = load i8, i8* %arrayidx14, align 1, !tbaa !8
  store i8 %23, i8* %next, align 1, !tbaa !8
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %cond.end10
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %26 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub16 = sub i32 %26, 1
  %cmp17 = icmp ult i32 %25, %sub16
  br i1 %cmp17, label %for.body19, label %for.cond.cleanup18

for.cond.cleanup18:                               ; preds = %for.cond15
  store i32 5, i32* %cleanup.dest.slot, align 4
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #2
  br label %for.end

for.body19:                                       ; preds = %for.cond15
  %28 = load i8, i8* %curr, align 1, !tbaa !8
  store i8 %28, i8* %prev, align 1, !tbaa !8
  %29 = load i8, i8* %next, align 1, !tbaa !8
  store i8 %29, i8* %curr, align 1, !tbaa !8
  %30 = load i8*, i8** %row, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %add20 = add i32 %31, 1
  %arrayidx21 = getelementptr inbounds i8, i8* %30, i32 %add20
  %32 = load i8, i8* %arrayidx21, align 1, !tbaa !8
  store i8 %32, i8* %next, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %t1) #2
  %33 = load i8, i8* %prev, align 1, !tbaa !8
  %conv = zext i8 %33 to i32
  %34 = load i8, i8* %next, align 1, !tbaa !8
  %conv22 = zext i8 %34 to i32
  %cmp23 = icmp slt i32 %conv, %conv22
  br i1 %cmp23, label %cond.true25, label %cond.false27

cond.true25:                                      ; preds = %for.body19
  %35 = load i8, i8* %prev, align 1, !tbaa !8
  %conv26 = zext i8 %35 to i32
  br label %cond.end29

cond.false27:                                     ; preds = %for.body19
  %36 = load i8, i8* %next, align 1, !tbaa !8
  %conv28 = zext i8 %36 to i32
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false27, %cond.true25
  %cond30 = phi i32 [ %conv26, %cond.true25 ], [ %conv28, %cond.false27 ]
  %conv31 = trunc i32 %cond30 to i8
  store i8 %conv31, i8* %t1, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %t2) #2
  %37 = load i8*, i8** %nextrow, align 4, !tbaa !2
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds i8, i8* %37, i32 %38
  %39 = load i8, i8* %arrayidx32, align 1, !tbaa !8
  %conv33 = zext i8 %39 to i32
  %40 = load i8*, i8** %prevrow, align 4, !tbaa !2
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds i8, i8* %40, i32 %41
  %42 = load i8, i8* %arrayidx34, align 1, !tbaa !8
  %conv35 = zext i8 %42 to i32
  %cmp36 = icmp slt i32 %conv33, %conv35
  br i1 %cmp36, label %cond.true38, label %cond.false41

cond.true38:                                      ; preds = %cond.end29
  %43 = load i8*, i8** %nextrow, align 4, !tbaa !2
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds i8, i8* %43, i32 %44
  %45 = load i8, i8* %arrayidx39, align 1, !tbaa !8
  %conv40 = zext i8 %45 to i32
  br label %cond.end44

cond.false41:                                     ; preds = %cond.end29
  %46 = load i8*, i8** %prevrow, align 4, !tbaa !2
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds i8, i8* %46, i32 %47
  %48 = load i8, i8* %arrayidx42, align 1, !tbaa !8
  %conv43 = zext i8 %48 to i32
  br label %cond.end44

cond.end44:                                       ; preds = %cond.false41, %cond.true38
  %cond45 = phi i32 [ %conv40, %cond.true38 ], [ %conv43, %cond.false41 ]
  %conv46 = trunc i32 %cond45 to i8
  store i8 %conv46, i8* %t2, align 1, !tbaa !8
  %49 = load i8, i8* %curr, align 1, !tbaa !8
  %conv47 = zext i8 %49 to i32
  %50 = load i8, i8* %t1, align 1, !tbaa !8
  %conv48 = zext i8 %50 to i32
  %51 = load i8, i8* %t2, align 1, !tbaa !8
  %conv49 = zext i8 %51 to i32
  %cmp50 = icmp slt i32 %conv48, %conv49
  br i1 %cmp50, label %cond.true52, label %cond.false54

cond.true52:                                      ; preds = %cond.end44
  %52 = load i8, i8* %t1, align 1, !tbaa !8
  %conv53 = zext i8 %52 to i32
  br label %cond.end56

cond.false54:                                     ; preds = %cond.end44
  %53 = load i8, i8* %t2, align 1, !tbaa !8
  %conv55 = zext i8 %53 to i32
  br label %cond.end56

cond.end56:                                       ; preds = %cond.false54, %cond.true52
  %cond57 = phi i32 [ %conv53, %cond.true52 ], [ %conv55, %cond.false54 ]
  %cmp58 = icmp slt i32 %conv47, %cond57
  br i1 %cmp58, label %cond.true60, label %cond.false62

cond.true60:                                      ; preds = %cond.end56
  %54 = load i8, i8* %curr, align 1, !tbaa !8
  %conv61 = zext i8 %54 to i32
  br label %cond.end73

cond.false62:                                     ; preds = %cond.end56
  %55 = load i8, i8* %t1, align 1, !tbaa !8
  %conv63 = zext i8 %55 to i32
  %56 = load i8, i8* %t2, align 1, !tbaa !8
  %conv64 = zext i8 %56 to i32
  %cmp65 = icmp slt i32 %conv63, %conv64
  br i1 %cmp65, label %cond.true67, label %cond.false69

cond.true67:                                      ; preds = %cond.false62
  %57 = load i8, i8* %t1, align 1, !tbaa !8
  %conv68 = zext i8 %57 to i32
  br label %cond.end71

cond.false69:                                     ; preds = %cond.false62
  %58 = load i8, i8* %t2, align 1, !tbaa !8
  %conv70 = zext i8 %58 to i32
  br label %cond.end71

cond.end71:                                       ; preds = %cond.false69, %cond.true67
  %cond72 = phi i32 [ %conv68, %cond.true67 ], [ %conv70, %cond.false69 ]
  br label %cond.end73

cond.end73:                                       ; preds = %cond.end71, %cond.true60
  %cond74 = phi i32 [ %conv61, %cond.true60 ], [ %cond72, %cond.end71 ]
  %conv75 = trunc i32 %cond74 to i8
  %59 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr, i8** %dst.addr, align 4, !tbaa !2
  store i8 %conv75, i8* %59, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %t2) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %t1) #2
  br label %for.inc

for.inc:                                          ; preds = %cond.end73
  %60 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %60, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond15

for.end:                                          ; preds = %for.cond.cleanup18
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %t176) #2
  %61 = load i8, i8* %curr, align 1, !tbaa !8
  %conv77 = zext i8 %61 to i32
  %62 = load i8, i8* %next, align 1, !tbaa !8
  %conv78 = zext i8 %62 to i32
  %cmp79 = icmp slt i32 %conv77, %conv78
  br i1 %cmp79, label %cond.true81, label %cond.false83

cond.true81:                                      ; preds = %for.end
  %63 = load i8, i8* %curr, align 1, !tbaa !8
  %conv82 = zext i8 %63 to i32
  br label %cond.end85

cond.false83:                                     ; preds = %for.end
  %64 = load i8, i8* %next, align 1, !tbaa !8
  %conv84 = zext i8 %64 to i32
  br label %cond.end85

cond.end85:                                       ; preds = %cond.false83, %cond.true81
  %cond86 = phi i32 [ %conv82, %cond.true81 ], [ %conv84, %cond.false83 ]
  %conv87 = trunc i32 %cond86 to i8
  store i8 %conv87, i8* %t176, align 1, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %t288) #2
  %65 = load i8*, i8** %nextrow, align 4, !tbaa !2
  %66 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub89 = sub i32 %66, 1
  %arrayidx90 = getelementptr inbounds i8, i8* %65, i32 %sub89
  %67 = load i8, i8* %arrayidx90, align 1, !tbaa !8
  %conv91 = zext i8 %67 to i32
  %68 = load i8*, i8** %prevrow, align 4, !tbaa !2
  %69 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub92 = sub i32 %69, 1
  %arrayidx93 = getelementptr inbounds i8, i8* %68, i32 %sub92
  %70 = load i8, i8* %arrayidx93, align 1, !tbaa !8
  %conv94 = zext i8 %70 to i32
  %cmp95 = icmp slt i32 %conv91, %conv94
  br i1 %cmp95, label %cond.true97, label %cond.false101

cond.true97:                                      ; preds = %cond.end85
  %71 = load i8*, i8** %nextrow, align 4, !tbaa !2
  %72 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub98 = sub i32 %72, 1
  %arrayidx99 = getelementptr inbounds i8, i8* %71, i32 %sub98
  %73 = load i8, i8* %arrayidx99, align 1, !tbaa !8
  %conv100 = zext i8 %73 to i32
  br label %cond.end105

cond.false101:                                    ; preds = %cond.end85
  %74 = load i8*, i8** %prevrow, align 4, !tbaa !2
  %75 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub102 = sub i32 %75, 1
  %arrayidx103 = getelementptr inbounds i8, i8* %74, i32 %sub102
  %76 = load i8, i8* %arrayidx103, align 1, !tbaa !8
  %conv104 = zext i8 %76 to i32
  br label %cond.end105

cond.end105:                                      ; preds = %cond.false101, %cond.true97
  %cond106 = phi i32 [ %conv100, %cond.true97 ], [ %conv104, %cond.false101 ]
  %conv107 = trunc i32 %cond106 to i8
  store i8 %conv107, i8* %t288, align 1, !tbaa !8
  %77 = load i8, i8* %t176, align 1, !tbaa !8
  %conv108 = zext i8 %77 to i32
  %78 = load i8, i8* %t288, align 1, !tbaa !8
  %conv109 = zext i8 %78 to i32
  %cmp110 = icmp slt i32 %conv108, %conv109
  br i1 %cmp110, label %cond.true112, label %cond.false114

cond.true112:                                     ; preds = %cond.end105
  %79 = load i8, i8* %t176, align 1, !tbaa !8
  %conv113 = zext i8 %79 to i32
  br label %cond.end116

cond.false114:                                    ; preds = %cond.end105
  %80 = load i8, i8* %t288, align 1, !tbaa !8
  %conv115 = zext i8 %80 to i32
  br label %cond.end116

cond.end116:                                      ; preds = %cond.false114, %cond.true112
  %cond117 = phi i32 [ %conv113, %cond.true112 ], [ %conv115, %cond.false114 ]
  %conv118 = trunc i32 %cond117 to i8
  %81 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %incdec.ptr119 = getelementptr inbounds i8, i8* %81, i32 1
  store i8* %incdec.ptr119, i8** %dst.addr, align 4, !tbaa !2
  store i8 %conv118, i8* %81, align 1, !tbaa !8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %t288) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %t176) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %next) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %curr) #2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %prev) #2
  %82 = bitcast i8** %nextrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #2
  %83 = bitcast i8** %prevrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #2
  %84 = bitcast i8** %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #2
  br label %for.inc120

for.inc120:                                       ; preds = %cond.end116
  %85 = load i32, i32* %j, align 4, !tbaa !6
  %inc121 = add i32 %85, 1
  store i32 %inc121, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end122:                                       ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define hidden void @liq_blur(i8* %src, i8* %tmp, i8* %dst, i32 %width, i32 %height, i32 %size) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %tmp.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i8* %tmp, i8** %tmp.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %0 = load i32, i32* %width.addr, align 4, !tbaa !6
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul i32 2, %1
  %add = add i32 %mul, 1
  %cmp = icmp ult i32 %0, %add
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %3 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul1 = mul i32 2, %3
  %add2 = add i32 %mul1, 1
  %cmp3 = icmp ult i32 %2, %add2
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %4 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %tmp.addr, align 4, !tbaa !2
  %6 = load i32, i32* %width.addr, align 4, !tbaa !6
  %7 = load i32, i32* %height.addr, align 4, !tbaa !6
  %8 = load i32, i32* %size.addr, align 4, !tbaa !6
  call void @transposing_1d_blur(i8* %4, i8* %5, i32 %6, i32 %7, i32 %8)
  %9 = load i8*, i8** %tmp.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %11 = load i32, i32* %height.addr, align 4, !tbaa !6
  %12 = load i32, i32* %width.addr, align 4, !tbaa !6
  %13 = load i32, i32* %size.addr, align 4, !tbaa !6
  call void @transposing_1d_blur(i8* %9, i8* %10, i32 %11, i32 %12, i32 %13)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @transposing_1d_blur(i8* noalias %src, i8* noalias %dst, i32 %width, i32 %height, i32 %size) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %row = alloca i8*, align 4
  %sum = alloca i32, align 4
  %i = alloca i32, align 4
  %i9 = alloca i32, align 4
  %i29 = alloca i32, align 4
  %i53 = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !6
  store i32 %height, i32* %height.addr, align 4, !tbaa !6
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %0 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc77, %entry
  %1 = load i32, i32* %j, align 4, !tbaa !6
  %2 = load i32, i32* %height.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #2
  br label %for.end79

for.body:                                         ; preds = %for.cond
  %4 = bitcast i8** %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %6 = load i32, i32* %j, align 4, !tbaa !6
  %7 = load i32, i32* %width.addr, align 4, !tbaa !6
  %mul = mul i32 %6, %7
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  store i8* %add.ptr, i8** %row, align 4, !tbaa !2
  %8 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = load i8*, i8** %row, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 0
  %10 = load i8, i8* %arrayidx, align 1, !tbaa !8
  %conv = zext i8 %10 to i32
  %11 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul1 = mul i32 %conv, %11
  store i32 %mul1, i32* %sum, align 4, !tbaa !6
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %14 = load i32, i32* %size.addr, align 4, !tbaa !6
  %cmp3 = icmp ult i32 %13, %14
  br i1 %cmp3, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond2
  store i32 5, i32* %cleanup.dest.slot, align 4
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #2
  br label %for.end

for.body6:                                        ; preds = %for.cond2
  %16 = load i8*, i8** %row, align 4, !tbaa !2
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds i8, i8* %16, i32 %17
  %18 = load i8, i8* %arrayidx7, align 1, !tbaa !8
  %conv8 = zext i8 %18 to i32
  %19 = load i32, i32* %sum, align 4, !tbaa !6
  %add = add i32 %19, %conv8
  store i32 %add, i32* %sum, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond2

for.end:                                          ; preds = %for.cond.cleanup5
  %21 = bitcast i32* %i9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  store i32 0, i32* %i9, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc26, %for.end
  %22 = load i32, i32* %i9, align 4, !tbaa !6
  %23 = load i32, i32* %size.addr, align 4, !tbaa !6
  %cmp11 = icmp ult i32 %22, %23
  br i1 %cmp11, label %for.body14, label %for.cond.cleanup13

for.cond.cleanup13:                               ; preds = %for.cond10
  store i32 8, i32* %cleanup.dest.slot, align 4
  %24 = bitcast i32* %i9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #2
  br label %for.end28

for.body14:                                       ; preds = %for.cond10
  %25 = load i8*, i8** %row, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %25, i32 0
  %26 = load i8, i8* %arrayidx15, align 1, !tbaa !8
  %conv16 = zext i8 %26 to i32
  %27 = load i32, i32* %sum, align 4, !tbaa !6
  %sub = sub i32 %27, %conv16
  store i32 %sub, i32* %sum, align 4, !tbaa !6
  %28 = load i8*, i8** %row, align 4, !tbaa !2
  %29 = load i32, i32* %i9, align 4, !tbaa !6
  %30 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add17 = add i32 %29, %30
  %arrayidx18 = getelementptr inbounds i8, i8* %28, i32 %add17
  %31 = load i8, i8* %arrayidx18, align 1, !tbaa !8
  %conv19 = zext i8 %31 to i32
  %32 = load i32, i32* %sum, align 4, !tbaa !6
  %add20 = add i32 %32, %conv19
  store i32 %add20, i32* %sum, align 4, !tbaa !6
  %33 = load i32, i32* %sum, align 4, !tbaa !6
  %34 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul21 = mul i32 %34, 2
  %div = udiv i32 %33, %mul21
  %conv22 = trunc i32 %div to i8
  %35 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %36 = load i32, i32* %i9, align 4, !tbaa !6
  %37 = load i32, i32* %height.addr, align 4, !tbaa !6
  %mul23 = mul i32 %36, %37
  %38 = load i32, i32* %j, align 4, !tbaa !6
  %add24 = add i32 %mul23, %38
  %arrayidx25 = getelementptr inbounds i8, i8* %35, i32 %add24
  store i8 %conv22, i8* %arrayidx25, align 1, !tbaa !8
  br label %for.inc26

for.inc26:                                        ; preds = %for.body14
  %39 = load i32, i32* %i9, align 4, !tbaa !6
  %inc27 = add i32 %39, 1
  store i32 %inc27, i32* %i9, align 4, !tbaa !6
  br label %for.cond10

for.end28:                                        ; preds = %for.cond.cleanup13
  %40 = bitcast i32* %i29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #2
  %41 = load i32, i32* %size.addr, align 4, !tbaa !6
  store i32 %41, i32* %i29, align 4, !tbaa !6
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc50, %for.end28
  %42 = load i32, i32* %i29, align 4, !tbaa !6
  %43 = load i32, i32* %width.addr, align 4, !tbaa !6
  %44 = load i32, i32* %size.addr, align 4, !tbaa !6
  %sub31 = sub i32 %43, %44
  %cmp32 = icmp ult i32 %42, %sub31
  br i1 %cmp32, label %for.body35, label %for.cond.cleanup34

for.cond.cleanup34:                               ; preds = %for.cond30
  store i32 11, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i32* %i29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #2
  br label %for.end52

for.body35:                                       ; preds = %for.cond30
  %46 = load i8*, i8** %row, align 4, !tbaa !2
  %47 = load i32, i32* %i29, align 4, !tbaa !6
  %48 = load i32, i32* %size.addr, align 4, !tbaa !6
  %sub36 = sub i32 %47, %48
  %arrayidx37 = getelementptr inbounds i8, i8* %46, i32 %sub36
  %49 = load i8, i8* %arrayidx37, align 1, !tbaa !8
  %conv38 = zext i8 %49 to i32
  %50 = load i32, i32* %sum, align 4, !tbaa !6
  %sub39 = sub i32 %50, %conv38
  store i32 %sub39, i32* %sum, align 4, !tbaa !6
  %51 = load i8*, i8** %row, align 4, !tbaa !2
  %52 = load i32, i32* %i29, align 4, !tbaa !6
  %53 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add40 = add i32 %52, %53
  %arrayidx41 = getelementptr inbounds i8, i8* %51, i32 %add40
  %54 = load i8, i8* %arrayidx41, align 1, !tbaa !8
  %conv42 = zext i8 %54 to i32
  %55 = load i32, i32* %sum, align 4, !tbaa !6
  %add43 = add i32 %55, %conv42
  store i32 %add43, i32* %sum, align 4, !tbaa !6
  %56 = load i32, i32* %sum, align 4, !tbaa !6
  %57 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul44 = mul i32 %57, 2
  %div45 = udiv i32 %56, %mul44
  %conv46 = trunc i32 %div45 to i8
  %58 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %59 = load i32, i32* %i29, align 4, !tbaa !6
  %60 = load i32, i32* %height.addr, align 4, !tbaa !6
  %mul47 = mul i32 %59, %60
  %61 = load i32, i32* %j, align 4, !tbaa !6
  %add48 = add i32 %mul47, %61
  %arrayidx49 = getelementptr inbounds i8, i8* %58, i32 %add48
  store i8 %conv46, i8* %arrayidx49, align 1, !tbaa !8
  br label %for.inc50

for.inc50:                                        ; preds = %for.body35
  %62 = load i32, i32* %i29, align 4, !tbaa !6
  %inc51 = add i32 %62, 1
  store i32 %inc51, i32* %i29, align 4, !tbaa !6
  br label %for.cond30

for.end52:                                        ; preds = %for.cond.cleanup34
  %63 = bitcast i32* %i53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #2
  %64 = load i32, i32* %width.addr, align 4, !tbaa !6
  %65 = load i32, i32* %size.addr, align 4, !tbaa !6
  %sub54 = sub i32 %64, %65
  store i32 %sub54, i32* %i53, align 4, !tbaa !6
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc74, %for.end52
  %66 = load i32, i32* %i53, align 4, !tbaa !6
  %67 = load i32, i32* %width.addr, align 4, !tbaa !6
  %cmp56 = icmp ult i32 %66, %67
  br i1 %cmp56, label %for.body59, label %for.cond.cleanup58

for.cond.cleanup58:                               ; preds = %for.cond55
  store i32 14, i32* %cleanup.dest.slot, align 4
  %68 = bitcast i32* %i53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #2
  br label %for.end76

for.body59:                                       ; preds = %for.cond55
  %69 = load i8*, i8** %row, align 4, !tbaa !2
  %70 = load i32, i32* %i53, align 4, !tbaa !6
  %71 = load i32, i32* %size.addr, align 4, !tbaa !6
  %sub60 = sub i32 %70, %71
  %arrayidx61 = getelementptr inbounds i8, i8* %69, i32 %sub60
  %72 = load i8, i8* %arrayidx61, align 1, !tbaa !8
  %conv62 = zext i8 %72 to i32
  %73 = load i32, i32* %sum, align 4, !tbaa !6
  %sub63 = sub i32 %73, %conv62
  store i32 %sub63, i32* %sum, align 4, !tbaa !6
  %74 = load i8*, i8** %row, align 4, !tbaa !2
  %75 = load i32, i32* %width.addr, align 4, !tbaa !6
  %sub64 = sub i32 %75, 1
  %arrayidx65 = getelementptr inbounds i8, i8* %74, i32 %sub64
  %76 = load i8, i8* %arrayidx65, align 1, !tbaa !8
  %conv66 = zext i8 %76 to i32
  %77 = load i32, i32* %sum, align 4, !tbaa !6
  %add67 = add i32 %77, %conv66
  store i32 %add67, i32* %sum, align 4, !tbaa !6
  %78 = load i32, i32* %sum, align 4, !tbaa !6
  %79 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul68 = mul i32 %79, 2
  %div69 = udiv i32 %78, %mul68
  %conv70 = trunc i32 %div69 to i8
  %80 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %81 = load i32, i32* %i53, align 4, !tbaa !6
  %82 = load i32, i32* %height.addr, align 4, !tbaa !6
  %mul71 = mul i32 %81, %82
  %83 = load i32, i32* %j, align 4, !tbaa !6
  %add72 = add i32 %mul71, %83
  %arrayidx73 = getelementptr inbounds i8, i8* %80, i32 %add72
  store i8 %conv70, i8* %arrayidx73, align 1, !tbaa !8
  br label %for.inc74

for.inc74:                                        ; preds = %for.body59
  %84 = load i32, i32* %i53, align 4, !tbaa !6
  %inc75 = add i32 %84, 1
  store i32 %inc75, i32* %i53, align 4, !tbaa !6
  br label %for.cond55

for.end76:                                        ; preds = %for.cond.cleanup58
  %85 = bitcast i32* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #2
  %86 = bitcast i8** %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #2
  br label %for.inc77

for.inc77:                                        ; preds = %for.end76
  %87 = load i32, i32* %j, align 4, !tbaa !6
  %inc78 = add i32 %87, 1
  store i32 %inc78, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end79:                                        ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!4, !4, i64 0}
