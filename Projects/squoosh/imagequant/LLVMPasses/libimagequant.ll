; ModuleID = 'libimagequant.c'
source_filename = "libimagequant.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.liq_attr = type { i8*, i8* (i32)*, void (i8*)*, double, double, double, float, i32, i32, i32, i32, i32, i32, i8, i8, i8, i8, i8, i8, i8, i32 (float, i8*)*, i8*, void (%struct.liq_attr*, i8*, i8*)*, i8*, void (%struct.liq_attr*, i8*)*, i8* }
%struct.liq_result = type { i8*, i8* (i32)*, void (i8*)*, %struct.liq_remapping_result*, %struct.colormap*, i32 (float, i8*)*, i8*, %struct.liq_palette, float, double, double, i32, i8 }
%struct.liq_remapping_result = type { i8*, i8* (i32)*, void (i8*)*, i8*, %struct.colormap*, i32 (float, i8*)*, i8*, %struct.liq_palette, double, double, float, i8, i8 }
%struct.colormap = type { i32, i8* (i32)*, void (i8*)*, [0 x %struct.colormap_item] }
%struct.colormap_item = type { %struct.f_pixel, float, i8 }
%struct.f_pixel = type { float, float, float, float }
%struct.liq_palette = type { i32, [256 x %struct.liq_color] }
%struct.liq_color = type { i8, i8, i8, i8 }
%struct.liq_image = type { i8*, i8* (i32)*, void (i8*)*, %struct.f_pixel*, %struct.rgba_pixel**, double, i32, i32, i8*, i8*, i8*, %struct.rgba_pixel*, %struct.rgba_pixel*, %struct.f_pixel*, void (%struct.liq_color*, i32, i32, i8*)*, i8*, %struct.liq_image*, float, [256 x %struct.f_pixel], i16, i8, i8, i8 }
%struct.rgba_pixel = type { i8, i8, i8, i8 }
%struct.liq_histogram = type { i8*, i8* (i32)*, void (i8*)*, %struct.acolorhash_table*, double, [256 x %struct.f_pixel], i16, i16, i8 }
%struct.acolorhash_table = type { %struct.mempool*, i32, i32, i32, i32, i32, i32, i32, [512 x %struct.acolorhist_arr_item*], [0 x %struct.acolorhist_arr_head] }
%struct.mempool = type opaque
%struct.acolorhist_arr_item = type { %union.rgba_as_int, i32 }
%union.rgba_as_int = type { i32 }
%struct.acolorhist_arr_head = type { %struct.acolorhist_arr_item, %struct.acolorhist_arr_item, i32, i32, %struct.acolorhist_arr_item* }
%struct.histogram = type { %struct.hist_item*, void (i8*)*, double, i32, i32 }
%struct.hist_item = type { %struct.f_pixel, float, float, float, %union.anon }
%union.anon = type { i32 }
%struct.liq_histogram_entry = type { %struct.liq_color, i32 }
%struct.nearest_map = type opaque
%struct.kmeans_state = type { double, double, double, double, double }

@liq_freed_magic = internal constant [5 x i8] c"free\00", align 1
@stderr = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [26 x i8] c"%s used after being freed\00", align 1
@liq_attr_magic = internal constant [9 x i8] c"liq_attr\00", align 1
@liq_result_magic = internal constant [11 x i8] c"liq_result\00", align 1
@liq_image_magic = internal constant [10 x i8] c"liq_image\00", align 1
@liq_histogram_magic = internal constant [14 x i8] c"liq_histogram\00", align 1
@.str.1 = private unnamed_addr constant [21 x i8] c"invalid row pointers\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c"invalid bitmap pointer\00", align 1
@.str.3 = private unnamed_addr constant [62 x i8] c"  too many colors! Scaling colors to improve clustering... %d\00", align 1
@.str.4 = private unnamed_addr constant [29 x i8] c"width and height must be > 0\00", align 1
@.str.5 = private unnamed_addr constant [16 x i8] c"image too large\00", align 1
@.str.6 = private unnamed_addr constant [50 x i8] c"gamma must be >= 0 and <= 1 (try 1/gamma instead)\00", align 1
@.str.7 = private unnamed_addr constant [17 x i8] c"missing row data\00", align 1
@.str.8 = private unnamed_addr constant [20 x i8] c"  conserving memory\00", align 1
@.str.9 = private unnamed_addr constant [61 x i8] c"  Working around IE6 bug by making image less transparent...\00", align 1
@.str.10 = private unnamed_addr constant [12 x i8] c"  error: %s\00", align 1
@.str.11 = private unnamed_addr constant [35 x i8] c"  made histogram...%d colors found\00", align 1
@.str.12 = private unnamed_addr constant [40 x i8] c"  moving colormap towards local minimum\00", align 1
@.str.13 = private unnamed_addr constant [64 x i8] c"  image degradation MSE=%.3f (Q=%d) exceeded limit of %.3f (%d)\00", align 1
@.str.14 = private unnamed_addr constant [26 x i8] c"  selecting colors...%d%%\00", align 1
@.str.15 = private unnamed_addr constant [63 x i8] c"  eliminated opaque tRNS-chunk entries...%d entr%s transparent\00", align 1
@.str.16 = private unnamed_addr constant [2 x i8] c"y\00", align 1
@.str.17 = private unnamed_addr constant [4 x i8] c"ies\00", align 1
@liq_remapping_result_magic = internal constant [21 x i8] c"liq_remapping_result\00", align 16

; Function Attrs: noinline nounwind
define hidden zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %user_supplied_pointer, i8* %expected_magic_header) #0 {
entry:
  %retval = alloca i1, align 1
  %user_supplied_pointer.addr = alloca %struct.liq_attr*, align 4
  %expected_magic_header.addr = alloca i8*, align 4
  store %struct.liq_attr* %user_supplied_pointer, %struct.liq_attr** %user_supplied_pointer.addr, align 4, !tbaa !2
  store i8* %expected_magic_header, i8** %expected_magic_header.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %user_supplied_pointer.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.liq_attr* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %user_supplied_pointer.addr, align 4, !tbaa !2
  %magic_header = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 0
  %2 = load i8*, i8** %magic_header, align 8, !tbaa !6
  %cmp = icmp eq i8* %2, getelementptr inbounds ([5 x i8], [5 x i8]* @liq_freed_magic, i32 0, i32 0)
  br i1 %cmp, label %if.then1, label %if.end2

if.then1:                                         ; preds = %if.end
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %4 = load i8*, i8** %expected_magic_header.addr, align 4, !tbaa !2
  %call = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0), i8* %4)
  call void @abort() #10
  unreachable

if.end2:                                          ; preds = %if.end
  %5 = load %struct.liq_attr*, %struct.liq_attr** %user_supplied_pointer.addr, align 4, !tbaa !2
  %magic_header3 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %5, i32 0, i32 0
  %6 = load i8*, i8** %magic_header3, align 8, !tbaa !6
  %7 = load i8*, i8** %expected_magic_header.addr, align 4, !tbaa !2
  %cmp4 = icmp eq i8* %6, %7
  store i1 %cmp4, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end2, %if.then
  %8 = load i1, i1* %retval, align 1
  ret i1 %8
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: noreturn
declare void @abort() #2

; Function Attrs: noinline nounwind
define hidden zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %pointer) #0 {
entry:
  %retval = alloca i1, align 1
  %pointer.addr = alloca i8*, align 4
  %test_access = alloca i8, align 1
  store i8* %pointer, i8** %pointer.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %pointer.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %test_access) #8
  %1 = load i8*, i8** %pointer.addr, align 4, !tbaa !2
  %2 = load volatile i8, i8* %1, align 1, !tbaa !12
  store i8 %2, i8* %test_access, align 1, !tbaa !12
  %3 = load i8, i8* %test_access, align 1, !tbaa !12
  %conv = sext i8 %3 to i32
  %tobool1 = icmp ne i32 %conv, 0
  br i1 %tobool1, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.end
  %4 = phi i1 [ true, %if.end ], [ true, %lor.rhs ]
  store i1 %4, i1* %retval, align 1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %test_access) #8
  br label %return

return:                                           ; preds = %lor.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define hidden i32 @liq_set_quality(%struct.liq_attr* nonnull %attr, i32 %minimum, i32 %target) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %minimum.addr = alloca i32, align 4
  %target.addr = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i32 %minimum, i32* %minimum.addr, align 4, !tbaa !13
  store i32 %target, i32* %target.addr, align 4, !tbaa !13
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %target.addr, align 4, !tbaa !13
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %if.then6, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %target.addr, align 4, !tbaa !13
  %cmp1 = icmp sgt i32 %2, 100
  br i1 %cmp1, label %if.then6, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %3 = load i32, i32* %target.addr, align 4, !tbaa !13
  %4 = load i32, i32* %minimum.addr, align 4, !tbaa !13
  %cmp3 = icmp slt i32 %3, %4
  br i1 %cmp3, label %if.then6, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %5 = load i32, i32* %minimum.addr, align 4, !tbaa !13
  %cmp5 = icmp slt i32 %5, 0
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %if.end
  store i32 100, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %lor.lhs.false4
  %6 = load i32, i32* %target.addr, align 4, !tbaa !13
  %call8 = call double @quality_to_mse(i32 %6)
  %7 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %target_mse = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %7, i32 0, i32 3
  store double %call8, double* %target_mse, align 8, !tbaa !14
  %8 = load i32, i32* %minimum.addr, align 4, !tbaa !13
  %call9 = call double @quality_to_mse(i32 %8)
  %9 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %max_mse = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %9, i32 0, i32 4
  store double %call9, double* %max_mse, align 8, !tbaa !15
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end7, %if.then6, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define internal double @quality_to_mse(i32 %quality) #4 {
entry:
  %retval = alloca double, align 8
  %quality.addr = alloca i32, align 4
  %extra_low_quality_fudge = alloca double, align 8
  store i32 %quality, i32* %quality.addr, align 4, !tbaa !16
  %0 = load i32, i32* %quality.addr, align 4, !tbaa !16
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store double 1.000000e+20, double* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %quality.addr, align 4, !tbaa !16
  %cmp1 = icmp eq i32 %1, 100
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store double 0.000000e+00, double* %retval, align 8
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = bitcast double* %extra_low_quality_fudge to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #8
  %3 = load i32, i32* %quality.addr, align 4, !tbaa !16
  %conv = sitofp i32 %3 to double
  %add = fadd double 1.000000e-03, %conv
  %div = fdiv double 1.600000e-02, %add
  %sub = fsub double %div, 1.000000e-03
  %cmp4 = fcmp ogt double 0.000000e+00, %sub
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end3
  br label %cond.end

cond.false:                                       ; preds = %if.end3
  %4 = load i32, i32* %quality.addr, align 4, !tbaa !16
  %conv6 = sitofp i32 %4 to double
  %add7 = fadd double 1.000000e-03, %conv6
  %div8 = fdiv double 1.600000e-02, %add7
  %sub9 = fsub double %div8, 1.000000e-03
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ 0.000000e+00, %cond.true ], [ %sub9, %cond.false ]
  store double %cond, double* %extra_low_quality_fudge, align 8, !tbaa !18
  %5 = load double, double* %extra_low_quality_fudge, align 8, !tbaa !18
  %6 = load i32, i32* %quality.addr, align 4, !tbaa !16
  %conv10 = sitofp i32 %6 to double
  %add11 = fadd double 2.100000e+02, %conv10
  %7 = call double @llvm.pow.f64(double %add11, double 1.200000e+00)
  %div12 = fdiv double 2.500000e+00, %7
  %8 = load i32, i32* %quality.addr, align 4, !tbaa !16
  %conv13 = sitofp i32 %8 to double
  %sub14 = fsub double 1.001000e+02, %conv13
  %mul = fmul double %div12, %sub14
  %div15 = fdiv double %mul, 1.000000e+02
  %add16 = fadd double %5, %div15
  store double %add16, double* %retval, align 8
  %9 = bitcast double* %extra_low_quality_fudge to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %9) #8
  br label %return

return:                                           ; preds = %cond.end, %if.then2, %if.then
  %10 = load double, double* %retval, align 8
  ret double %10
}

; Function Attrs: nounwind
define hidden i32 @liq_get_min_quality(%struct.liq_attr* nonnull %attr) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %max_mse = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 4
  %2 = load double, double* %max_mse, align 8, !tbaa !15
  %call1 = call i32 @mse_to_quality(double %2)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define internal i32 @mse_to_quality(double %mse) #4 {
entry:
  %retval = alloca i32, align 4
  %mse.addr = alloca double, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store double %mse, double* %mse.addr, align 8, !tbaa !18
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 100, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !13
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %2 = load double, double* %mse.addr, align 8, !tbaa !18
  %3 = load i32, i32* %i, align 4, !tbaa !13
  %call = call double @quality_to_mse(i32 %3)
  %add = fadd double %call, 0x3EB0C6F7A0B5ED8D
  %cmp1 = fcmp ole double %2, %add
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4, !tbaa !13
  store i32 %4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %5 = load i32, i32* %i, align 4, !tbaa !13
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %i, align 4, !tbaa !13
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup
  %7 = load i32, i32* %retval, align 4
  ret i32 %7

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @liq_get_max_quality(%struct.liq_attr* nonnull %attr) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %target_mse = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 3
  %2 = load double, double* %target_mse, align 8, !tbaa !14
  %call1 = call i32 @mse_to_quality(double %2)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @liq_set_max_colors(%struct.liq_attr* nonnull %attr, i32 %colors) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %colors.addr = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i32 %colors, i32* %colors.addr, align 4, !tbaa !13
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %colors.addr, align 4, !tbaa !13
  %cmp = icmp slt i32 %1, 2
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %colors.addr, align 4, !tbaa !13
  %cmp1 = icmp sgt i32 %2, 256
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %lor.lhs.false, %if.end
  store i32 100, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %lor.lhs.false
  %3 = load i32, i32* %colors.addr, align 4, !tbaa !13
  %4 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %max_colors = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %4, i32 0, i32 7
  store i32 %3, i32* %max_colors, align 4, !tbaa !19
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: nounwind
define hidden i32 @liq_get_max_colors(%struct.liq_attr* nonnull %attr) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %max_colors = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 7
  %2 = load i32, i32* %max_colors, align 4, !tbaa !19
  store i32 %2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @liq_set_min_posterization(%struct.liq_attr* nonnull %attr, i32 %bits) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %bits.addr = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !13
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %bits.addr, align 4, !tbaa !13
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %bits.addr, align 4, !tbaa !13
  %cmp1 = icmp sgt i32 %2, 4
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %lor.lhs.false, %if.end
  store i32 100, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %lor.lhs.false
  %3 = load i32, i32* %bits.addr, align 4, !tbaa !13
  %4 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_posterization_output = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %4, i32 0, i32 9
  store i32 %3, i32* %min_posterization_output, align 4, !tbaa !20
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: nounwind
define hidden i32 @liq_get_min_posterization(%struct.liq_attr* nonnull %attr) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_posterization_output = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 9
  %2 = load i32, i32* %min_posterization_output, align 4, !tbaa !20
  store i32 %2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @liq_set_speed(%struct.liq_attr* nonnull %attr, i32 %speed) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %speed.addr = alloca i32, align 4
  %iterations = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i32 %speed, i32* %speed.addr, align 4, !tbaa !13
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %cmp = icmp slt i32 %1, 1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %cmp1 = icmp sgt i32 %2, 10
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %lor.lhs.false, %if.end
  store i32 100, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %lor.lhs.false
  %3 = bitcast i32* %iterations to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %sub = sub nsw i32 8, %4
  %cmp4 = icmp sgt i32 %sub, 0
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end3
  %5 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %sub5 = sub nsw i32 8, %5
  br label %cond.end

cond.false:                                       ; preds = %if.end3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub5, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %iterations, align 4, !tbaa !13
  %6 = load i32, i32* %iterations, align 4, !tbaa !13
  %7 = load i32, i32* %iterations, align 4, !tbaa !13
  %mul = mul i32 %6, %7
  %div = udiv i32 %mul, 2
  %8 = load i32, i32* %iterations, align 4, !tbaa !13
  %add = add i32 %8, %div
  store i32 %add, i32* %iterations, align 4, !tbaa !13
  %9 = load i32, i32* %iterations, align 4, !tbaa !13
  %10 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %kmeans_iterations = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %10, i32 0, i32 11
  store i32 %9, i32* %kmeans_iterations, align 4, !tbaa !21
  %11 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %sub6 = sub nsw i32 23, %11
  %shl = shl i32 1, %sub6
  %conv = sitofp i32 %shl to double
  %div7 = fdiv double 1.000000e+00, %conv
  %12 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %kmeans_iteration_limit = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %12, i32 0, i32 5
  store double %div7, double* %kmeans_iteration_limit, align 8, !tbaa !22
  %13 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %mul8 = mul nsw i32 9, %13
  %sub9 = sub nsw i32 56, %mul8
  %cmp10 = icmp sgt i32 %sub9, 0
  br i1 %cmp10, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end
  %14 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %mul13 = mul nsw i32 9, %14
  %sub14 = sub nsw i32 56, %mul13
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi i32 [ %sub14, %cond.true12 ], [ 0, %cond.false15 ]
  %15 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %feedback_loop_trials = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %15, i32 0, i32 12
  store i32 %cond17, i32* %feedback_loop_trials, align 8, !tbaa !23
  %16 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %sub18 = sub nsw i32 10, %16
  %mul19 = mul nsw i32 262144, %sub18
  %add20 = add nsw i32 131072, %mul19
  %17 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %max_histogram_entries = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %17, i32 0, i32 8
  store i32 %add20, i32* %max_histogram_entries, align 8, !tbaa !24
  %18 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %cmp21 = icmp sge i32 %18, 8
  %19 = zext i1 %cmp21 to i64
  %cond23 = select i1 %cmp21, i32 1, i32 0
  %20 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_posterization_input = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %20, i32 0, i32 10
  store i32 %cond23, i32* %min_posterization_input, align 8, !tbaa !25
  %21 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %cmp24 = icmp sle i32 %21, 5
  %conv25 = zext i1 %cmp24 to i32
  %conv26 = trunc i32 %conv25 to i8
  %22 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %use_dither_map = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %22, i32 0, i32 15
  store i8 %conv26, i8* %use_dither_map, align 2, !tbaa !26
  %23 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %use_dither_map27 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %23, i32 0, i32 15
  %24 = load i8, i8* %use_dither_map27, align 2, !tbaa !26
  %conv28 = zext i8 %24 to i32
  %tobool = icmp ne i32 %conv28, 0
  br i1 %tobool, label %land.lhs.true, label %if.end33

land.lhs.true:                                    ; preds = %cond.end16
  %25 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %cmp29 = icmp slt i32 %25, 3
  br i1 %cmp29, label %if.then31, label %if.end33

if.then31:                                        ; preds = %land.lhs.true
  %26 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %use_dither_map32 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %26, i32 0, i32 15
  store i8 2, i8* %use_dither_map32, align 2, !tbaa !26
  br label %if.end33

if.end33:                                         ; preds = %if.then31, %land.lhs.true, %cond.end16
  %27 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %cmp34 = icmp sle i32 %27, 7
  br i1 %cmp34, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end33
  %28 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %use_dither_map36 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %28, i32 0, i32 15
  %29 = load i8, i8* %use_dither_map36, align 2, !tbaa !26
  %conv37 = zext i8 %29 to i32
  %tobool38 = icmp ne i32 %conv37, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.end33
  %30 = phi i1 [ true, %if.end33 ], [ %tobool38, %lor.rhs ]
  %31 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %use_contrast_maps = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %31, i32 0, i32 14
  %frombool = zext i1 %30 to i8
  store i8 %frombool, i8* %use_contrast_maps, align 1, !tbaa !27
  %32 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %conv39 = trunc i32 %32 to i8
  %33 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %speed40 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %33, i32 0, i32 16
  store i8 %conv39, i8* %speed40, align 1, !tbaa !28
  %34 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %use_contrast_maps41 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %34, i32 0, i32 14
  %35 = load i8, i8* %use_contrast_maps41, align 1, !tbaa !27, !range !29
  %tobool42 = trunc i8 %35 to i1
  %36 = zext i1 %tobool42 to i64
  %cond44 = select i1 %tobool42, i32 20, i32 8
  %conv45 = trunc i32 %cond44 to i8
  %37 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_stage1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %37, i32 0, i32 17
  store i8 %conv45, i8* %progress_stage1, align 8, !tbaa !30
  %38 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %feedback_loop_trials46 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %38, i32 0, i32 12
  %39 = load i32, i32* %feedback_loop_trials46, align 8, !tbaa !23
  %cmp47 = icmp ult i32 %39, 2
  br i1 %cmp47, label %if.then49, label %if.end54

if.then49:                                        ; preds = %lor.end
  %40 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_stage150 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %40, i32 0, i32 17
  %41 = load i8, i8* %progress_stage150, align 8, !tbaa !30
  %conv51 = zext i8 %41 to i32
  %add52 = add nsw i32 %conv51, 30
  %conv53 = trunc i32 %add52 to i8
  store i8 %conv53, i8* %progress_stage150, align 8, !tbaa !30
  br label %if.end54

if.end54:                                         ; preds = %if.then49, %lor.end
  %42 = load i32, i32* %speed.addr, align 4, !tbaa !13
  %add55 = add nsw i32 1, %42
  %div56 = sdiv i32 50, %add55
  %conv57 = trunc i32 %div56 to i8
  %43 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_stage3 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %43, i32 0, i32 19
  store i8 %conv57, i8* %progress_stage3, align 2, !tbaa !31
  %44 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_stage158 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %44, i32 0, i32 17
  %45 = load i8, i8* %progress_stage158, align 8, !tbaa !30
  %conv59 = zext i8 %45 to i32
  %sub60 = sub nsw i32 100, %conv59
  %46 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_stage361 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %46, i32 0, i32 19
  %47 = load i8, i8* %progress_stage361, align 2, !tbaa !31
  %conv62 = zext i8 %47 to i32
  %sub63 = sub nsw i32 %sub60, %conv62
  %conv64 = trunc i32 %sub63 to i8
  %48 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_stage2 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %48, i32 0, i32 18
  store i8 %conv64, i8* %progress_stage2, align 1, !tbaa !32
  store i32 0, i32* %retval, align 4
  %49 = bitcast i32* %iterations to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  br label %return

return:                                           ; preds = %if.end54, %if.then2, %if.then
  %50 = load i32, i32* %retval, align 4
  ret i32 %50
}

; Function Attrs: nounwind
define hidden i32 @liq_get_speed(%struct.liq_attr* nonnull %attr) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %speed = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 16
  %2 = load i8, i8* %speed, align 1, !tbaa !28
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: nounwind
define hidden i32 @liq_set_output_gamma(%struct.liq_result* nonnull %res, double %gamma) #4 {
entry:
  %retval = alloca i32, align 4
  %res.addr = alloca %struct.liq_result*, align 4
  %gamma.addr = alloca double, align 8
  store %struct.liq_result* %res, %struct.liq_result** %res.addr, align 4, !tbaa !2
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  %0 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load double, double* %gamma.addr, align 8, !tbaa !18
  %cmp = fcmp ole double %2, 0.000000e+00
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %3 = load double, double* %gamma.addr, align 8, !tbaa !18
  %cmp1 = fcmp oge double %3, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %lor.lhs.false, %if.end
  store i32 100, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %lor.lhs.false
  %4 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping = getelementptr inbounds %struct.liq_result, %struct.liq_result* %4, i32 0, i32 3
  %5 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping, align 4, !tbaa !33
  %tobool = icmp ne %struct.liq_remapping_result* %5, null
  br i1 %tobool, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.end3
  %6 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping5 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %6, i32 0, i32 3
  %7 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping5, align 4, !tbaa !33
  call void @liq_remapping_result_destroy(%struct.liq_remapping_result* %7)
  %8 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping6 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %8, i32 0, i32 3
  store %struct.liq_remapping_result* null, %struct.liq_remapping_result** %remapping6, align 4, !tbaa !33
  br label %if.end7

if.end7:                                          ; preds = %if.then4, %if.end3
  %9 = load double, double* %gamma.addr, align 8, !tbaa !18
  %10 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %gamma8 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %10, i32 0, i32 9
  store double %9, double* %gamma8, align 8, !tbaa !36
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end7, %if.then2, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: nounwind
define internal void @liq_remapping_result_destroy(%struct.liq_remapping_result* nonnull %result) #4 {
entry:
  %result.addr = alloca %struct.liq_remapping_result*, align 4
  store %struct.liq_remapping_result* %result, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %0 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_remapping_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @liq_remapping_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %2, i32 0, i32 4
  %3 = load %struct.colormap*, %struct.colormap** %palette, align 8, !tbaa !37
  %tobool = icmp ne %struct.colormap* %3, null
  br i1 %tobool, label %if.then1, label %if.end3

if.then1:                                         ; preds = %if.end
  %4 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %palette2 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %4, i32 0, i32 4
  %5 = load %struct.colormap*, %struct.colormap** %palette2, align 8, !tbaa !37
  call void @pam_freecolormap(%struct.colormap* %5)
  br label %if.end3

if.end3:                                          ; preds = %if.then1, %if.end
  %6 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %6, i32 0, i32 3
  %7 = load i8*, i8** %pixels, align 4, !tbaa !39
  %tobool4 = icmp ne i8* %7, null
  br i1 %tobool4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end3
  %8 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %8, i32 0, i32 2
  %9 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !40
  %10 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %pixels6 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %10, i32 0, i32 3
  %11 = load i8*, i8** %pixels6, align 4, !tbaa !39
  call void %9(i8* %11)
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.end3
  %12 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %magic_header = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %12, i32 0, i32 0
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @liq_freed_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !41
  %13 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %free8 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %13, i32 0, i32 2
  %14 = load void (i8*)*, void (i8*)** %free8, align 8, !tbaa !40
  %15 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result.addr, align 4, !tbaa !2
  %16 = bitcast %struct.liq_remapping_result* %15 to i8*
  call void %14(i8* %16)
  br label %return

return:                                           ; preds = %if.end7, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden i32 @liq_set_min_opacity(%struct.liq_attr* nonnull %attr, i32 %min) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %min.addr = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i32 %min, i32* %min.addr, align 4, !tbaa !13
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %min.addr, align 4, !tbaa !13
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %min.addr, align 4, !tbaa !13
  %cmp1 = icmp sgt i32 %2, 255
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %lor.lhs.false, %if.end
  store i32 100, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %lor.lhs.false
  %3 = load i32, i32* %min.addr, align 4, !tbaa !13
  %conv = sitofp i32 %3 to double
  %div = fdiv double %conv, 2.550000e+02
  %conv4 = fptrunc double %div to float
  %4 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_opaque_val = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %4, i32 0, i32 6
  store float %conv4, float* %min_opaque_val, align 8, !tbaa !42
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: nounwind
define hidden i32 @liq_get_min_opacity(%struct.liq_attr* nonnull %attr) #4 {
entry:
  %retval = alloca i32, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_opaque_val = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 6
  %2 = load float, float* %min_opaque_val, align 8, !tbaa !42
  %mul = fmul float 2.560000e+02, %2
  %cmp = fcmp olt float 2.550000e+02, %mul
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %3 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_opaque_val1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %3, i32 0, i32 6
  %4 = load float, float* %min_opaque_val1, align 8, !tbaa !42
  %mul2 = fmul float 2.560000e+02, %4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 2.550000e+02, %cond.true ], [ %mul2, %cond.false ]
  %conv = fptosi float %cond to i32
  store i32 %conv, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: nounwind
define hidden void @liq_set_last_index_transparent(%struct.liq_attr* nonnull %attr, i32 %is_last) #4 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  %is_last.addr = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i32 %is_last, i32* %is_last.addr, align 4, !tbaa !13
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %is_last.addr, align 4, !tbaa !13
  %tobool = icmp ne i32 %1, 0
  %lnot = xor i1 %tobool, true
  %lnot1 = xor i1 %lnot, true
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %last_index_transparent = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 13
  %frombool = zext i1 %lnot1 to i8
  store i8 %frombool, i8* %last_index_transparent, align 4, !tbaa !43
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @liq_attr_set_progress_callback(%struct.liq_attr* %attr, i32 (float, i8*)* %callback, i8* %user_info) #4 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  %callback.addr = alloca i32 (float, i8*)*, align 4
  %user_info.addr = alloca i8*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i32 (float, i8*)* %callback, i32 (float, i8*)** %callback.addr, align 4, !tbaa !2
  store i8* %user_info, i8** %user_info.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32 (float, i8*)*, i32 (float, i8*)** %callback.addr, align 4, !tbaa !2
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 20
  store i32 (float, i8*)* %1, i32 (float, i8*)** %progress_callback, align 4, !tbaa !44
  %3 = load i8*, i8** %user_info.addr, align 4, !tbaa !2
  %4 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %4, i32 0, i32 21
  store i8* %3, i8** %progress_callback_user_info, align 8, !tbaa !45
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @liq_result_set_progress_callback(%struct.liq_result* %result, i32 (float, i8*)* %callback, i8* %user_info) #4 {
entry:
  %result.addr = alloca %struct.liq_result*, align 4
  %callback.addr = alloca i32 (float, i8*)*, align 4
  %user_info.addr = alloca i8*, align 4
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  store i32 (float, i8*)* %callback, i32 (float, i8*)** %callback.addr, align 4, !tbaa !2
  store i8* %user_info, i8** %user_info.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32 (float, i8*)*, i32 (float, i8*)** %callback.addr, align 4, !tbaa !2
  %3 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %progress_callback = getelementptr inbounds %struct.liq_result, %struct.liq_result* %3, i32 0, i32 5
  store i32 (float, i8*)* %2, i32 (float, i8*)** %progress_callback, align 4, !tbaa !46
  %4 = load i8*, i8** %user_info.addr, align 4, !tbaa !2
  %5 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %progress_callback_user_info = getelementptr inbounds %struct.liq_result, %struct.liq_result* %5, i32 0, i32 6
  store i8* %4, i8** %progress_callback_user_info, align 8, !tbaa !47
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden void @liq_set_log_callback(%struct.liq_attr* %attr, void (%struct.liq_attr*, i8*, i8*)* %callback, i8* %user_info) #4 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  %callback.addr = alloca void (%struct.liq_attr*, i8*, i8*)*, align 4
  %user_info.addr = alloca i8*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store void (%struct.liq_attr*, i8*, i8*)* %callback, void (%struct.liq_attr*, i8*, i8*)** %callback.addr, align 4, !tbaa !2
  store i8* %user_info, i8** %user_info.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @liq_verbose_printf_flush(%struct.liq_attr* %1)
  %2 = load void (%struct.liq_attr*, i8*, i8*)*, void (%struct.liq_attr*, i8*, i8*)** %callback.addr, align 4, !tbaa !2
  %3 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %3, i32 0, i32 22
  store void (%struct.liq_attr*, i8*, i8*)* %2, void (%struct.liq_attr*, i8*, i8*)** %log_callback, align 4, !tbaa !48
  %4 = load i8*, i8** %user_info.addr, align 4, !tbaa !2
  %5 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %5, i32 0, i32 23
  store i8* %4, i8** %log_callback_user_info, align 8, !tbaa !49
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @liq_verbose_printf_flush(%struct.liq_attr* nonnull %attr) #4 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_flush_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %0, i32 0, i32 24
  %1 = load void (%struct.liq_attr*, i8*)*, void (%struct.liq_attr*, i8*)** %log_flush_callback, align 4, !tbaa !50
  %tobool = icmp ne void (%struct.liq_attr*, i8*)* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_flush_callback1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 24
  %3 = load void (%struct.liq_attr*, i8*)*, void (%struct.liq_attr*, i8*)** %log_flush_callback1, align 4, !tbaa !50
  %4 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %5 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_flush_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %5, i32 0, i32 25
  %6 = load i8*, i8** %log_flush_callback_user_info, align 8, !tbaa !51
  call void %3(%struct.liq_attr* %4, i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @liq_set_log_flush_callback(%struct.liq_attr* %attr, void (%struct.liq_attr*, i8*)* %callback, i8* %user_info) #4 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  %callback.addr = alloca void (%struct.liq_attr*, i8*)*, align 4
  %user_info.addr = alloca i8*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store void (%struct.liq_attr*, i8*)* %callback, void (%struct.liq_attr*, i8*)** %callback.addr, align 4, !tbaa !2
  store i8* %user_info, i8** %user_info.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load void (%struct.liq_attr*, i8*)*, void (%struct.liq_attr*, i8*)** %callback.addr, align 4, !tbaa !2
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_flush_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 24
  store void (%struct.liq_attr*, i8*)* %1, void (%struct.liq_attr*, i8*)** %log_flush_callback, align 4, !tbaa !50
  %3 = load i8*, i8** %user_info.addr, align 4, !tbaa !2
  %4 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_flush_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %4, i32 0, i32 25
  store i8* %3, i8** %log_flush_callback_user_info, align 8, !tbaa !51
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden %struct.liq_attr* @liq_attr_create() #4 {
entry:
  %call = call %struct.liq_attr* @liq_attr_create_with_allocator(i8* (i32)* null, void (i8*)* null)
  ret %struct.liq_attr* %call
}

; Function Attrs: nounwind
define hidden %struct.liq_attr* @liq_attr_create_with_allocator(i8* (i32)* %custom_malloc, void (i8*)* %custom_free) #4 {
entry:
  %retval = alloca %struct.liq_attr*, align 4
  %custom_malloc.addr = alloca i8* (i32)*, align 4
  %custom_free.addr = alloca void (i8*)*, align 4
  %attr = alloca %struct.liq_attr*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %.compoundliteral = alloca %struct.liq_attr, align 8
  store i8* (i32)* %custom_malloc, i8* (i32)** %custom_malloc.addr, align 4, !tbaa !2
  store void (i8*)* %custom_free, void (i8*)** %custom_free.addr, align 4, !tbaa !2
  %0 = load i8* (i32)*, i8* (i32)** %custom_malloc.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* (i32)* %0, null
  br i1 %tobool, label %if.else, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %1 = load void (i8*)*, void (i8*)** %custom_free.addr, align 4, !tbaa !2
  %tobool1 = icmp ne void (i8*)* %1, null
  br i1 %tobool1, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true
  store i8* (i32)* @liq_aligned_malloc, i8* (i32)** %custom_malloc.addr, align 4, !tbaa !2
  store void (i8*)* @liq_aligned_free, void (i8*)** %custom_free.addr, align 4, !tbaa !2
  br label %if.end7

if.else:                                          ; preds = %land.lhs.true, %entry
  %2 = load i8* (i32)*, i8* (i32)** %custom_malloc.addr, align 4, !tbaa !2
  %tobool2 = icmp ne i8* (i32)* %2, null
  %lnot = xor i1 %tobool2, true
  %lnot.ext = zext i1 %lnot to i32
  %3 = load void (i8*)*, void (i8*)** %custom_free.addr, align 4, !tbaa !2
  %tobool3 = icmp ne void (i8*)* %3, null
  %lnot4 = xor i1 %tobool3, true
  %lnot.ext5 = zext i1 %lnot4 to i32
  %cmp = icmp ne i32 %lnot.ext, %lnot.ext5
  br i1 %cmp, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else
  store %struct.liq_attr* null, %struct.liq_attr** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then
  %4 = bitcast %struct.liq_attr** %attr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load i8* (i32)*, i8* (i32)** %custom_malloc.addr, align 4, !tbaa !2
  %call = call i8* %5(i32 104)
  %6 = bitcast i8* %call to %struct.liq_attr*
  store %struct.liq_attr* %6, %struct.liq_attr** %attr, align 4, !tbaa !2
  %7 = load %struct.liq_attr*, %struct.liq_attr** %attr, align 4, !tbaa !2
  %tobool8 = icmp ne %struct.liq_attr* %7, null
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %if.end7
  store %struct.liq_attr* null, %struct.liq_attr** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %if.end7
  %8 = load %struct.liq_attr*, %struct.liq_attr** %attr, align 4, !tbaa !2
  %magic_header = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 0
  store i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !6
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 1
  %9 = load i8* (i32)*, i8* (i32)** %custom_malloc.addr, align 4, !tbaa !2
  store i8* (i32)* %9, i8* (i32)** %malloc, align 4, !tbaa !52
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 2
  %10 = load void (i8*)*, void (i8*)** %custom_free.addr, align 4, !tbaa !2
  store void (i8*)* %10, void (i8*)** %free, align 8, !tbaa !53
  %target_mse = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 3
  store double 0.000000e+00, double* %target_mse, align 8, !tbaa !14
  %max_mse = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 4
  store double 1.000000e+20, double* %max_mse, align 8, !tbaa !15
  %kmeans_iteration_limit = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 5
  store double 0.000000e+00, double* %kmeans_iteration_limit, align 8, !tbaa !22
  %min_opaque_val = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 6
  store float 1.000000e+00, float* %min_opaque_val, align 8, !tbaa !42
  %max_colors = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 7
  store i32 256, i32* %max_colors, align 4, !tbaa !19
  %max_histogram_entries = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 8
  store i32 0, i32* %max_histogram_entries, align 8, !tbaa !24
  %min_posterization_output = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 9
  store i32 0, i32* %min_posterization_output, align 4, !tbaa !20
  %min_posterization_input = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 10
  store i32 0, i32* %min_posterization_input, align 8, !tbaa !25
  %kmeans_iterations = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 11
  store i32 0, i32* %kmeans_iterations, align 4, !tbaa !21
  %feedback_loop_trials = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 12
  store i32 0, i32* %feedback_loop_trials, align 8, !tbaa !23
  %last_index_transparent = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 13
  store i8 0, i8* %last_index_transparent, align 4, !tbaa !43
  %use_contrast_maps = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 14
  store i8 0, i8* %use_contrast_maps, align 1, !tbaa !27
  %use_dither_map = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 15
  store i8 0, i8* %use_dither_map, align 2, !tbaa !26
  %speed = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 16
  store i8 0, i8* %speed, align 1, !tbaa !28
  %progress_stage1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 17
  store i8 0, i8* %progress_stage1, align 8, !tbaa !30
  %progress_stage2 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 18
  store i8 0, i8* %progress_stage2, align 1, !tbaa !32
  %progress_stage3 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 19
  store i8 0, i8* %progress_stage3, align 2, !tbaa !31
  %progress_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 20
  store i32 (float, i8*)* null, i32 (float, i8*)** %progress_callback, align 4, !tbaa !44
  %progress_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 21
  store i8* null, i8** %progress_callback_user_info, align 8, !tbaa !45
  %log_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 22
  store void (%struct.liq_attr*, i8*, i8*)* null, void (%struct.liq_attr*, i8*, i8*)** %log_callback, align 4, !tbaa !48
  %log_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 23
  store i8* null, i8** %log_callback_user_info, align 8, !tbaa !49
  %log_flush_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 24
  store void (%struct.liq_attr*, i8*)* null, void (%struct.liq_attr*, i8*)** %log_flush_callback, align 4, !tbaa !50
  %log_flush_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %.compoundliteral, i32 0, i32 25
  store i8* null, i8** %log_flush_callback_user_info, align 8, !tbaa !51
  %11 = bitcast %struct.liq_attr* %8 to i8*
  %12 = bitcast %struct.liq_attr* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %11, i8* align 8 %12, i32 104, i1 false), !tbaa.struct !54
  %13 = load %struct.liq_attr*, %struct.liq_attr** %attr, align 4, !tbaa !2
  %call11 = call i32 @liq_set_speed(%struct.liq_attr* %13, i32 4)
  %14 = load %struct.liq_attr*, %struct.liq_attr** %attr, align 4, !tbaa !2
  store %struct.liq_attr* %14, %struct.liq_attr** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end10, %if.then9
  %15 = bitcast %struct.liq_attr** %attr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then6
  %16 = load %struct.liq_attr*, %struct.liq_attr** %retval, align 4
  ret %struct.liq_attr* %16
}

; Function Attrs: nounwind
define hidden void @liq_attr_destroy(%struct.liq_attr* nonnull %attr) #4 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @liq_verbose_printf_flush(%struct.liq_attr* %1)
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %magic_header = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 0
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @liq_freed_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !6
  %3 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %3, i32 0, i32 2
  %4 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !53
  %5 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %6 = bitcast %struct.liq_attr* %5 to i8*
  call void %4(i8* %6)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden %struct.liq_attr* @liq_attr_copy(%struct.liq_attr* nonnull %orig) #4 {
entry:
  %retval = alloca %struct.liq_attr*, align 4
  %orig.addr = alloca %struct.liq_attr*, align 4
  %attr = alloca %struct.liq_attr*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.liq_attr* %orig, %struct.liq_attr** %orig.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %orig.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.liq_attr* null, %struct.liq_attr** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %struct.liq_attr** %attr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %struct.liq_attr*, %struct.liq_attr** %orig.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 1
  %3 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %call1 = call i8* %3(i32 104)
  %4 = bitcast i8* %call1 to %struct.liq_attr*
  store %struct.liq_attr* %4, %struct.liq_attr** %attr, align 4, !tbaa !2
  %5 = load %struct.liq_attr*, %struct.liq_attr** %attr, align 4, !tbaa !2
  %tobool = icmp ne %struct.liq_attr* %5, null
  br i1 %tobool, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store %struct.liq_attr* null, %struct.liq_attr** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %6 = load %struct.liq_attr*, %struct.liq_attr** %attr, align 4, !tbaa !2
  %7 = load %struct.liq_attr*, %struct.liq_attr** %orig.addr, align 4, !tbaa !2
  %8 = bitcast %struct.liq_attr* %6 to i8*
  %9 = bitcast %struct.liq_attr* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %8, i8* align 8 %9, i32 104, i1 false), !tbaa.struct !54
  %10 = load %struct.liq_attr*, %struct.liq_attr** %attr, align 4, !tbaa !2
  store %struct.liq_attr* %10, %struct.liq_attr** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end3, %if.then2
  %11 = bitcast %struct.liq_attr** %attr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %12 = load %struct.liq_attr*, %struct.liq_attr** %retval, align 4
  ret %struct.liq_attr* %12
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: nounwind
define internal i8* @liq_aligned_malloc(i32 %size) #4 {
entry:
  %retval = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %ptr = alloca i8*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %offset = alloca i32, align 4
  store i32 %size, i32* %size.addr, align 4, !tbaa !16
  %0 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %size.addr, align 4, !tbaa !16
  %add = add i32 %1, 16
  %call = call i8* @malloc(i32 %add)
  store i8* %call, i8** %ptr, align 4, !tbaa !2
  %2 = load i8*, i8** %ptr, align 4, !tbaa !2
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i8*, i8** %ptr, align 4, !tbaa !2
  %5 = ptrtoint i8* %4 to i32
  %and = and i32 %5, 15
  %sub = sub i32 16, %and
  store i32 %sub, i32* %offset, align 4, !tbaa !16
  %6 = load i32, i32* %offset, align 4, !tbaa !16
  %7 = load i8*, i8** %ptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %6
  store i8* %add.ptr, i8** %ptr, align 4, !tbaa !2
  %8 = load i32, i32* %offset, align 4, !tbaa !16
  %xor = xor i32 %8, 89
  %conv = trunc i32 %xor to i8
  %9 = load i8*, i8** %ptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 -1
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !12
  %10 = load i8*, i8** %ptr, align 4, !tbaa !2
  store i8* %10, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %11 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %12 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = load i8*, i8** %retval, align 4
  ret i8* %13
}

; Function Attrs: nounwind
define internal void @liq_aligned_free(i8* nonnull %inptr) #4 {
entry:
  %inptr.addr = alloca i8*, align 4
  %ptr = alloca i8*, align 4
  %offset = alloca i32, align 4
  store i8* %inptr, i8** %inptr.addr, align 4, !tbaa !2
  %0 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %inptr.addr, align 4, !tbaa !2
  store i8* %1, i8** %ptr, align 4, !tbaa !2
  %2 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i8*, i8** %ptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 -1
  %4 = load i8, i8* %arrayidx, align 1, !tbaa !12
  %conv = zext i8 %4 to i32
  %xor = xor i32 %conv, 89
  store i32 %xor, i32* %offset, align 4, !tbaa !16
  %5 = load i8*, i8** %ptr, align 4, !tbaa !2
  %6 = load i32, i32* %offset, align 4, !tbaa !16
  %idx.neg = sub i32 0, %6
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %idx.neg
  call void @free(i8* %add.ptr)
  %7 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: nounwind
define hidden i32 @liq_image_add_fixed_color(%struct.liq_image* nonnull %img, %struct.liq_color* byval(%struct.liq_color) align 1 %color) #4 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  %gamma_lut = alloca [256 x float], align 16
  %tmp = alloca %struct.f_pixel, align 4
  %.compoundliteral = alloca %struct.rgba_pixel, align 1
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_image* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %fixed_colors_count = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 19
  %3 = load i16, i16* %fixed_colors_count, align 8, !tbaa !57
  %conv = zext i16 %3 to i32
  %cmp = icmp sgt i32 %conv, 255
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 106, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %4) #8
  %arraydecay = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %5 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_image, %struct.liq_image* %5, i32 0, i32 5
  %6 = load double, double* %gamma, align 8, !tbaa !60
  call void @to_f_set_gamma(float* %arraydecay, double %6)
  %7 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %fixed_colors = getelementptr inbounds %struct.liq_image, %struct.liq_image* %7, i32 0, i32 18
  %8 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %fixed_colors_count4 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 19
  %9 = load i16, i16* %fixed_colors_count4, align 8, !tbaa !57
  %inc = add i16 %9, 1
  store i16 %inc, i16* %fixed_colors_count4, align 8, !tbaa !57
  %idxprom = zext i16 %9 to i32
  %arrayidx = getelementptr inbounds [256 x %struct.f_pixel], [256 x %struct.f_pixel]* %fixed_colors, i32 0, i32 %idxprom
  %10 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %arraydecay5 = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %r = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %.compoundliteral, i32 0, i32 0
  %r6 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 0
  %11 = load i8, i8* %r6, align 1, !tbaa !61
  store i8 %11, i8* %r, align 1, !tbaa !63
  %g = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %.compoundliteral, i32 0, i32 1
  %g7 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 1
  %12 = load i8, i8* %g7, align 1, !tbaa !65
  store i8 %12, i8* %g, align 1, !tbaa !66
  %b = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %.compoundliteral, i32 0, i32 2
  %b8 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 2
  %13 = load i8, i8* %b8, align 1, !tbaa !67
  store i8 %13, i8* %b, align 1, !tbaa !68
  %a = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %.compoundliteral, i32 0, i32 3
  %a9 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 3
  %14 = load i8, i8* %a9, align 1, !tbaa !69
  store i8 %14, i8* %a, align 1, !tbaa !70
  call void @rgba_to_f(%struct.f_pixel* sret align 4 %tmp, float* %arraydecay5, %struct.rgba_pixel* byval(%struct.rgba_pixel) align 1 %.compoundliteral)
  %15 = bitcast %struct.f_pixel* %arrayidx to i8*
  %16 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !71
  %17 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #8
  store i32 0, i32* %retval, align 4
  %18 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %18) #8
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

declare hidden void @to_f_set_gamma(float*, double) #1

; Function Attrs: alwaysinline nounwind
define internal void @rgba_to_f(%struct.f_pixel* noalias sret align 4 %agg.result, float* %gamma_lut, %struct.rgba_pixel* byval(%struct.rgba_pixel) align 1 %px) #5 {
entry:
  %gamma_lut.addr = alloca float*, align 4
  %a = alloca float, align 4
  store float* %gamma_lut, float** %gamma_lut.addr, align 4, !tbaa !2
  %0 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %a1 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 3
  %1 = load i8, i8* %a1, align 1, !tbaa !70
  %conv = zext i8 %1 to i32
  %conv2 = sitofp i32 %conv to float
  %div = fdiv float %conv2, 2.550000e+02
  store float %div, float* %a, align 4, !tbaa !55
  %a3 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 0
  %2 = load float, float* %a, align 4, !tbaa !55
  store float %2, float* %a3, align 4, !tbaa !72
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 1
  %3 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %r4 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 0
  %4 = load i8, i8* %r4, align 1, !tbaa !63
  %idxprom = zext i8 %4 to i32
  %arrayidx = getelementptr inbounds float, float* %3, i32 %idxprom
  %5 = load float, float* %arrayidx, align 4, !tbaa !55
  %6 = load float, float* %a, align 4, !tbaa !55
  %mul = fmul float %5, %6
  store float %mul, float* %r, align 4, !tbaa !74
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 2
  %7 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %g5 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 1
  %8 = load i8, i8* %g5, align 1, !tbaa !66
  %idxprom6 = zext i8 %8 to i32
  %arrayidx7 = getelementptr inbounds float, float* %7, i32 %idxprom6
  %9 = load float, float* %arrayidx7, align 4, !tbaa !55
  %10 = load float, float* %a, align 4, !tbaa !55
  %mul8 = fmul float %9, %10
  store float %mul8, float* %g, align 4, !tbaa !75
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 3
  %11 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %b9 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 2
  %12 = load i8, i8* %b9, align 1, !tbaa !68
  %idxprom10 = zext i8 %12 to i32
  %arrayidx11 = getelementptr inbounds float, float* %11, i32 %idxprom10
  %13 = load float, float* %arrayidx11, align 4, !tbaa !55
  %14 = load float, float* %a, align 4, !tbaa !55
  %mul12 = fmul float %13, %14
  store float %mul12, float* %b, align 4, !tbaa !76
  %15 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  ret void
}

; Function Attrs: nounwind
define hidden i32 @liq_histogram_add_fixed_color(%struct.liq_histogram* nonnull %hist, %struct.liq_color* byval(%struct.liq_color) align 1 %color, double %gamma) #4 {
entry:
  %retval = alloca i32, align 4
  %hist.addr = alloca %struct.liq_histogram*, align 4
  %gamma.addr = alloca double, align 8
  %gamma_lut = alloca [256 x float], align 16
  %px = alloca %struct.f_pixel, align 4
  %.compoundliteral = alloca %struct.rgba_pixel, align 1
  store %struct.liq_histogram* %hist, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  %0 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_histogram* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @liq_histogram_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %2) #8
  %arraydecay = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %3 = load double, double* %gamma.addr, align 8, !tbaa !18
  %tobool = fcmp une double %3, 0.000000e+00
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %4 = load double, double* %gamma.addr, align 8, !tbaa !18
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %4, %cond.true ], [ 4.545500e-01, %cond.false ]
  call void @to_f_set_gamma(float* %arraydecay, double %cond)
  %5 = bitcast %struct.f_pixel* %px to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %arraydecay1 = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %r = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %.compoundliteral, i32 0, i32 0
  %r2 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 0
  %6 = load i8, i8* %r2, align 1, !tbaa !61
  store i8 %6, i8* %r, align 1, !tbaa !63
  %g = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %.compoundliteral, i32 0, i32 1
  %g3 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 1
  %7 = load i8, i8* %g3, align 1, !tbaa !65
  store i8 %7, i8* %g, align 1, !tbaa !66
  %b = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %.compoundliteral, i32 0, i32 2
  %b4 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 2
  %8 = load i8, i8* %b4, align 1, !tbaa !67
  store i8 %8, i8* %b, align 1, !tbaa !68
  %a = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %.compoundliteral, i32 0, i32 3
  %a5 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 3
  %9 = load i8, i8* %a5, align 1, !tbaa !69
  store i8 %9, i8* %a, align 1, !tbaa !70
  call void @rgba_to_f(%struct.f_pixel* sret align 4 %px, float* %arraydecay1, %struct.rgba_pixel* byval(%struct.rgba_pixel) align 1 %.compoundliteral)
  %10 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %call6 = call i32 @liq_histogram_add_fixed_color_f(%struct.liq_histogram* %10, %struct.f_pixel* byval(%struct.f_pixel) align 4 %px)
  store i32 %call6, i32* %retval, align 4
  %11 = bitcast %struct.f_pixel* %px to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  %12 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %12) #8
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

; Function Attrs: nounwind
define internal i32 @liq_histogram_add_fixed_color_f(%struct.liq_histogram* nonnull %hist, %struct.f_pixel* byval(%struct.f_pixel) align 4 %color) #4 {
entry:
  %retval = alloca i32, align 4
  %hist.addr = alloca %struct.liq_histogram*, align 4
  store %struct.liq_histogram* %hist, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %0 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %fixed_colors_count = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %0, i32 0, i32 6
  %1 = load i16, i16* %fixed_colors_count, align 8, !tbaa !77
  %conv = zext i16 %1 to i32
  %cmp = icmp sgt i32 %conv, 255
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 106, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %fixed_colors = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %2, i32 0, i32 5
  %3 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %fixed_colors_count2 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %3, i32 0, i32 6
  %4 = load i16, i16* %fixed_colors_count2, align 8, !tbaa !77
  %inc = add i16 %4, 1
  store i16 %inc, i16* %fixed_colors_count2, align 8, !tbaa !77
  %idxprom = zext i16 %4 to i32
  %arrayidx = getelementptr inbounds [256 x %struct.f_pixel], [256 x %struct.f_pixel]* %fixed_colors, i32 0, i32 %idxprom
  %5 = bitcast %struct.f_pixel* %arrayidx to i8*
  %6 = bitcast %struct.f_pixel* %color to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !71
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: nounwind
define hidden i32 @liq_image_set_memory_ownership(%struct.liq_image* nonnull %img, i32 %ownership_flags) #4 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  %ownership_flags.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  store i32 %ownership_flags, i32* %ownership_flags.addr, align 4, !tbaa !13
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_image* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 4
  %3 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 8, !tbaa !79
  %tobool = icmp ne %struct.rgba_pixel** %3, null
  br i1 %tobool, label %lor.lhs.false, label %if.then4

lor.lhs.false:                                    ; preds = %if.end
  %4 = load i32, i32* %ownership_flags.addr, align 4, !tbaa !13
  %tobool1 = icmp ne i32 %4, 0
  br i1 %tobool1, label %lor.lhs.false2, label %if.then4

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %5 = load i32, i32* %ownership_flags.addr, align 4, !tbaa !13
  %and = and i32 %5, -13
  %tobool3 = icmp ne i32 %and, 0
  br i1 %tobool3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %lor.lhs.false2, %lor.lhs.false, %if.end
  store i32 100, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %lor.lhs.false2
  %6 = load i32, i32* %ownership_flags.addr, align 4, !tbaa !13
  %and6 = and i32 %6, 4
  %tobool7 = icmp ne i32 %and6, 0
  br i1 %tobool7, label %if.then8, label %if.end12

if.then8:                                         ; preds = %if.end5
  %7 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %free_rows_internal = getelementptr inbounds %struct.liq_image, %struct.liq_image* %7, i32 0, i32 22
  %8 = load i8, i8* %free_rows_internal, align 4, !tbaa !80, !range !29
  %tobool9 = trunc i8 %8 to i1
  br i1 %tobool9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.then8
  store i32 100, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.then8
  %9 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %free_rows = getelementptr inbounds %struct.liq_image, %struct.liq_image* %9, i32 0, i32 21
  store i8 1, i8* %free_rows, align 1, !tbaa !81
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.end5
  %10 = load i32, i32* %ownership_flags.addr, align 4, !tbaa !13
  %and13 = and i32 %10, 8
  %tobool14 = icmp ne i32 %and13, 0
  br i1 %tobool14, label %if.then15, label %if.end29

if.then15:                                        ; preds = %if.end12
  %11 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %free_pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %11, i32 0, i32 20
  store i8 1, i8* %free_pixels, align 2, !tbaa !82
  %12 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %12, i32 0, i32 11
  %13 = load %struct.rgba_pixel*, %struct.rgba_pixel** %pixels, align 4, !tbaa !83
  %tobool16 = icmp ne %struct.rgba_pixel* %13, null
  br i1 %tobool16, label %if.end28, label %if.then17

if.then17:                                        ; preds = %if.then15
  %14 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows18 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %14, i32 0, i32 4
  %15 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows18, align 8, !tbaa !79
  %arrayidx = getelementptr inbounds %struct.rgba_pixel*, %struct.rgba_pixel** %15, i32 0
  %16 = load %struct.rgba_pixel*, %struct.rgba_pixel** %arrayidx, align 4, !tbaa !2
  %17 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %pixels19 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %17, i32 0, i32 11
  store %struct.rgba_pixel* %16, %struct.rgba_pixel** %pixels19, align 4, !tbaa !83
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store i32 1, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then17
  %19 = load i32, i32* %i, align 4, !tbaa !13
  %20 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %20, i32 0, i32 7
  %21 = load i32, i32* %height, align 4, !tbaa !84
  %cmp = icmp ult i32 %19, %21
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %pixels20 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %23, i32 0, i32 11
  %24 = load %struct.rgba_pixel*, %struct.rgba_pixel** %pixels20, align 4, !tbaa !83
  %25 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows21 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %25, i32 0, i32 4
  %26 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows21, align 8, !tbaa !79
  %27 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx22 = getelementptr inbounds %struct.rgba_pixel*, %struct.rgba_pixel** %26, i32 %27
  %28 = load %struct.rgba_pixel*, %struct.rgba_pixel** %arrayidx22, align 4, !tbaa !2
  %cmp23 = icmp ult %struct.rgba_pixel* %24, %28
  br i1 %cmp23, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %29 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %pixels24 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %29, i32 0, i32 11
  %30 = load %struct.rgba_pixel*, %struct.rgba_pixel** %pixels24, align 4, !tbaa !83
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %31 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows25 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %31, i32 0, i32 4
  %32 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows25, align 8, !tbaa !79
  %33 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx26 = getelementptr inbounds %struct.rgba_pixel*, %struct.rgba_pixel** %32, i32 %33
  %34 = load %struct.rgba_pixel*, %struct.rgba_pixel** %arrayidx26, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.rgba_pixel* [ %30, %cond.true ], [ %34, %cond.false ]
  %35 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %pixels27 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %35, i32 0, i32 11
  store %struct.rgba_pixel* %cond, %struct.rgba_pixel** %pixels27, align 4, !tbaa !83
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %36 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end28

if.end28:                                         ; preds = %for.end, %if.then15
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.end12
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end29, %if.then10, %if.then4, %if.then
  %37 = load i32, i32* %retval, align 4
  ret i32 %37
}

; Function Attrs: nounwind
define hidden i32 @liq_image_set_importance_map(%struct.liq_image* nonnull %img, i8* nonnull %importance_map, i32 %buffer_size, i32 %ownership) #4 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  %importance_map.addr = alloca i8*, align 4
  %buffer_size.addr = alloca i32, align 4
  %ownership.addr = alloca i32, align 4
  %required_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tmp = alloca i8*, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  store i8* %importance_map, i8** %importance_map.addr, align 4, !tbaa !2
  store i32 %buffer_size, i32* %buffer_size.addr, align 4, !tbaa !16
  store i32 %ownership, i32* %ownership.addr, align 4, !tbaa !12
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_image* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %importance_map.addr, align 4, !tbaa !2
  %call1 = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %2)
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 105, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = bitcast i32* %required_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 6
  %5 = load i32, i32* %width, align 8, !tbaa !85
  %6 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 7
  %7 = load i32, i32* %height, align 4, !tbaa !84
  %mul = mul i32 %5, %7
  store i32 %mul, i32* %required_size, align 4, !tbaa !16
  %8 = load i32, i32* %buffer_size.addr, align 4, !tbaa !16
  %9 = load i32, i32* %required_size, align 4, !tbaa !16
  %cmp = icmp ult i32 %8, %9
  br i1 %cmp, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end3
  store i32 104, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup16

if.end5:                                          ; preds = %if.end3
  %10 = load i32, i32* %ownership.addr, align 4, !tbaa !12
  %cmp6 = icmp eq i32 %10, 16
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end5
  %11 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_image, %struct.liq_image* %12, i32 0, i32 1
  %13 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !86
  %14 = load i32, i32* %required_size, align 4, !tbaa !16
  %call8 = call i8* %13(i32 %14)
  store i8* %call8, i8** %tmp, align 4, !tbaa !2
  %15 = load i8*, i8** %tmp, align 4, !tbaa !2
  %tobool = icmp ne i8* %15, null
  br i1 %tobool, label %if.end10, label %if.then9

if.then9:                                         ; preds = %if.then7
  store i32 101, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %if.then7
  %16 = load i8*, i8** %tmp, align 4, !tbaa !2
  %17 = load i8*, i8** %importance_map.addr, align 4, !tbaa !2
  %18 = load i32, i32* %required_size, align 4, !tbaa !16
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %16, i8* align 1 %17, i32 %18, i1 false)
  %19 = load i8*, i8** %tmp, align 4, !tbaa !2
  store i8* %19, i8** %importance_map.addr, align 4, !tbaa !2
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end10, %if.then9
  %20 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup16 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end14

if.else:                                          ; preds = %if.end5
  %21 = load i32, i32* %ownership.addr, align 4, !tbaa !12
  %cmp11 = icmp ne i32 %21, 8
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.else
  store i32 106, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup16

if.end13:                                         ; preds = %if.else
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %cleanup.cont
  %22 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  call void @liq_image_free_importance_map(%struct.liq_image* %22)
  %23 = load i8*, i8** %importance_map.addr, align 4, !tbaa !2
  %24 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %importance_map15 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %24, i32 0, i32 8
  store i8* %23, i8** %importance_map15, align 8, !tbaa !87
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup16

cleanup16:                                        ; preds = %if.end14, %if.then12, %cleanup, %if.then4
  %25 = bitcast i32* %required_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  br label %return

return:                                           ; preds = %cleanup16, %if.then2, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define internal void @liq_image_free_importance_map(%struct.liq_image* nonnull %input_image) #4 {
entry:
  %input_image.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %importance_map = getelementptr inbounds %struct.liq_image, %struct.liq_image* %0, i32 0, i32 8
  %1 = load i8*, i8** %importance_map, align 8, !tbaa !87
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 2
  %3 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !88
  %4 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %importance_map1 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 8
  %5 = load i8*, i8** %importance_map1, align 8, !tbaa !87
  call void %3(i8* %5)
  %6 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %importance_map2 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 8
  store i8* null, i8** %importance_map2, align 8, !tbaa !87
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define hidden i32 @liq_image_set_background(%struct.liq_image* nonnull %img, %struct.liq_image* nonnull %background) #4 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  %background.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  store %struct.liq_image* %background, %struct.liq_image** %background.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_image* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %background.addr, align 4, !tbaa !2
  %3 = bitcast %struct.liq_image* %2 to %struct.liq_attr*
  %call1 = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %3, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 105, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = load %struct.liq_image*, %struct.liq_image** %background.addr, align 4, !tbaa !2
  %background4 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 16
  %5 = load %struct.liq_image*, %struct.liq_image** %background4, align 8, !tbaa !89
  %tobool = icmp ne %struct.liq_image* %5, null
  br i1 %tobool, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 106, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %6 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 6
  %7 = load i32, i32* %width, align 8, !tbaa !85
  %8 = load %struct.liq_image*, %struct.liq_image** %background.addr, align 4, !tbaa !2
  %width7 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 6
  %9 = load i32, i32* %width7, align 8, !tbaa !85
  %cmp = icmp ne i32 %7, %9
  br i1 %cmp, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end6
  %10 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %10, i32 0, i32 7
  %11 = load i32, i32* %height, align 4, !tbaa !84
  %12 = load %struct.liq_image*, %struct.liq_image** %background.addr, align 4, !tbaa !2
  %height8 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %12, i32 0, i32 7
  %13 = load i32, i32* %height8, align 4, !tbaa !84
  %cmp9 = icmp ne i32 %11, %13
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %lor.lhs.false, %if.end6
  store i32 104, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %lor.lhs.false
  %14 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %background12 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %14, i32 0, i32 16
  %15 = load %struct.liq_image*, %struct.liq_image** %background12, align 8, !tbaa !89
  %tobool13 = icmp ne %struct.liq_image* %15, null
  br i1 %tobool13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.end11
  %16 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %background15 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %16, i32 0, i32 16
  %17 = load %struct.liq_image*, %struct.liq_image** %background15, align 8, !tbaa !89
  call void @liq_image_destroy(%struct.liq_image* %17)
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.end11
  %18 = load %struct.liq_image*, %struct.liq_image** %background.addr, align 4, !tbaa !2
  %19 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %background17 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %19, i32 0, i32 16
  store %struct.liq_image* %18, %struct.liq_image** %background17, align 8, !tbaa !89
  %20 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  call void @liq_image_free_maps(%struct.liq_image* %20)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end16, %if.then10, %if.then5, %if.then2, %if.then
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: nounwind
define hidden void @liq_image_destroy(%struct.liq_image* nonnull %input_image) #4 {
entry:
  %input_image.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_image* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  call void @liq_image_free_rgba_source(%struct.liq_image* %2)
  %3 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  call void @liq_image_free_maps(%struct.liq_image* %3)
  %4 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %f_pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 3
  %5 = load %struct.f_pixel*, %struct.f_pixel** %f_pixels, align 4, !tbaa !90
  %tobool = icmp ne %struct.f_pixel* %5, null
  br i1 %tobool, label %if.then1, label %if.end3

if.then1:                                         ; preds = %if.end
  %6 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 2
  %7 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !88
  %8 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %f_pixels2 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 3
  %9 = load %struct.f_pixel*, %struct.f_pixel** %f_pixels2, align 4, !tbaa !90
  %10 = bitcast %struct.f_pixel* %9 to i8*
  call void %7(i8* %10)
  br label %if.end3

if.end3:                                          ; preds = %if.then1, %if.end
  %11 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %temp_row = getelementptr inbounds %struct.liq_image, %struct.liq_image* %11, i32 0, i32 12
  %12 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row, align 8, !tbaa !91
  %tobool4 = icmp ne %struct.rgba_pixel* %12, null
  br i1 %tobool4, label %if.then5, label %if.end8

if.then5:                                         ; preds = %if.end3
  %13 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free6 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %13, i32 0, i32 2
  %14 = load void (i8*)*, void (i8*)** %free6, align 8, !tbaa !88
  %15 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %temp_row7 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %15, i32 0, i32 12
  %16 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row7, align 8, !tbaa !91
  %17 = bitcast %struct.rgba_pixel* %16 to i8*
  call void %14(i8* %17)
  br label %if.end8

if.end8:                                          ; preds = %if.then5, %if.end3
  %18 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %temp_f_row = getelementptr inbounds %struct.liq_image, %struct.liq_image* %18, i32 0, i32 13
  %19 = load %struct.f_pixel*, %struct.f_pixel** %temp_f_row, align 4, !tbaa !92
  %tobool9 = icmp ne %struct.f_pixel* %19, null
  br i1 %tobool9, label %if.then10, label %if.end13

if.then10:                                        ; preds = %if.end8
  %20 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free11 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %20, i32 0, i32 2
  %21 = load void (i8*)*, void (i8*)** %free11, align 8, !tbaa !88
  %22 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %temp_f_row12 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %22, i32 0, i32 13
  %23 = load %struct.f_pixel*, %struct.f_pixel** %temp_f_row12, align 4, !tbaa !92
  %24 = bitcast %struct.f_pixel* %23 to i8*
  call void %21(i8* %24)
  br label %if.end13

if.end13:                                         ; preds = %if.then10, %if.end8
  %25 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background = getelementptr inbounds %struct.liq_image, %struct.liq_image* %25, i32 0, i32 16
  %26 = load %struct.liq_image*, %struct.liq_image** %background, align 8, !tbaa !89
  %tobool14 = icmp ne %struct.liq_image* %26, null
  br i1 %tobool14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %if.end13
  %27 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background16 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %27, i32 0, i32 16
  %28 = load %struct.liq_image*, %struct.liq_image** %background16, align 8, !tbaa !89
  call void @liq_image_destroy(%struct.liq_image* %28)
  br label %if.end17

if.end17:                                         ; preds = %if.then15, %if.end13
  %29 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %magic_header = getelementptr inbounds %struct.liq_image, %struct.liq_image* %29, i32 0, i32 0
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @liq_freed_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !93
  %30 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free18 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %30, i32 0, i32 2
  %31 = load void (i8*)*, void (i8*)** %free18, align 8, !tbaa !88
  %32 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %33 = bitcast %struct.liq_image* %32 to i8*
  call void %31(i8* %33)
  br label %return

return:                                           ; preds = %if.end17, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @liq_image_free_maps(%struct.liq_image* nonnull %input_image) #4 {
entry:
  %input_image.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  call void @liq_image_free_importance_map(%struct.liq_image* %0)
  %1 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges = getelementptr inbounds %struct.liq_image, %struct.liq_image* %1, i32 0, i32 9
  %2 = load i8*, i8** %edges, align 4, !tbaa !94
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_image, %struct.liq_image* %3, i32 0, i32 2
  %4 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !88
  %5 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges1 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %5, i32 0, i32 9
  %6 = load i8*, i8** %edges1, align 4, !tbaa !94
  call void %4(i8* %6)
  %7 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges2 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %7, i32 0, i32 9
  store i8* null, i8** %edges2, align 4, !tbaa !94
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %dither_map = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 10
  %9 = load i8*, i8** %dither_map, align 8, !tbaa !95
  %tobool3 = icmp ne i8* %9, null
  br i1 %tobool3, label %if.then4, label %if.end8

if.then4:                                         ; preds = %if.end
  %10 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free5 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %10, i32 0, i32 2
  %11 = load void (i8*)*, void (i8*)** %free5, align 8, !tbaa !88
  %12 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %dither_map6 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %12, i32 0, i32 10
  %13 = load i8*, i8** %dither_map6, align 8, !tbaa !95
  call void %11(i8* %13)
  %14 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %dither_map7 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %14, i32 0, i32 10
  store i8* null, i8** %dither_map7, align 8, !tbaa !95
  br label %if.end8

if.end8:                                          ; preds = %if.then4, %if.end
  ret void
}

; Function Attrs: nounwind
define hidden %struct.liq_image* @liq_image_create_custom(%struct.liq_attr* %attr, void (%struct.liq_color*, i32, i32, i8*)* %row_callback, i8* %user_info, i32 %width, i32 %height, double %gamma) #4 {
entry:
  %retval = alloca %struct.liq_image*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %row_callback.addr = alloca void (%struct.liq_color*, i32, i32, i8*)*, align 4
  %user_info.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %gamma.addr = alloca double, align 8
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store void (%struct.liq_color*, i32, i32, i8*)* %row_callback, void (%struct.liq_color*, i32, i32, i8*)** %row_callback.addr, align 4, !tbaa !2
  store i8* %user_info, i8** %user_info.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i32 %height, i32* %height.addr, align 4, !tbaa !13
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %1 = load i32, i32* %width.addr, align 4, !tbaa !13
  %2 = load i32, i32* %height.addr, align 4, !tbaa !13
  %call = call zeroext i1 @check_image_size(%struct.liq_attr* %0, i32 %1, i32 %2)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %4 = load void (%struct.liq_color*, i32, i32, i8*)*, void (%struct.liq_color*, i32, i32, i8*)** %row_callback.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %user_info.addr, align 4, !tbaa !2
  %6 = load i32, i32* %width.addr, align 4, !tbaa !13
  %7 = load i32, i32* %height.addr, align 4, !tbaa !13
  %8 = load double, double* %gamma.addr, align 8, !tbaa !18
  %call1 = call %struct.liq_image* @liq_image_create_internal(%struct.liq_attr* %3, %struct.rgba_pixel** null, void (%struct.liq_color*, i32, i32, i8*)* %4, i8* %5, i32 %6, i32 %7, double %8)
  store %struct.liq_image* %call1, %struct.liq_image** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load %struct.liq_image*, %struct.liq_image** %retval, align 4
  ret %struct.liq_image* %9
}

; Function Attrs: nounwind
define internal zeroext i1 @check_image_size(%struct.liq_attr* nonnull %attr, i32 %width, i32 %height) #4 {
entry:
  %retval = alloca i1, align 1
  %attr.addr = alloca %struct.liq_attr*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i32 %height, i32* %height.addr, align 4, !tbaa !13
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %width.addr, align 4, !tbaa !13
  %cmp = icmp sle i32 %1, 0
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %2 = load i32, i32* %height.addr, align 4, !tbaa !13
  %cmp1 = icmp sle i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %lor.lhs.false, %if.end
  %3 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @liq_log_error(%struct.liq_attr* %3, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.4, i32 0, i32 0))
  store i1 false, i1* %retval, align 1
  br label %return

if.end3:                                          ; preds = %lor.lhs.false
  %4 = load i32, i32* %width.addr, align 4, !tbaa !13
  %5 = load i32, i32* %height.addr, align 4, !tbaa !13
  %div = udiv i32 536870911, %5
  %cmp4 = icmp ugt i32 %4, %div
  br i1 %cmp4, label %if.then9, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %if.end3
  %6 = load i32, i32* %width.addr, align 4, !tbaa !13
  %cmp6 = icmp ugt i32 %6, 8388607
  br i1 %cmp6, label %if.then9, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %lor.lhs.false5
  %7 = load i32, i32* %height.addr, align 4, !tbaa !13
  %cmp8 = icmp ugt i32 %7, 536870911
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %lor.lhs.false7, %lor.lhs.false5, %if.end3
  %8 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @liq_log_error(%struct.liq_attr* %8, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.5, i32 0, i32 0))
  store i1 false, i1* %retval, align 1
  br label %return

if.end10:                                         ; preds = %lor.lhs.false7
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end10, %if.then9, %if.then2, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: nounwind
define internal %struct.liq_image* @liq_image_create_internal(%struct.liq_attr* %attr, %struct.rgba_pixel** %rows, void (%struct.liq_color*, i32, i32, i8*)* %row_callback, i8* %row_callback_user_info, i32 %width, i32 %height, double %gamma) #4 {
entry:
  %retval = alloca %struct.liq_image*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %rows.addr = alloca %struct.rgba_pixel**, align 4
  %row_callback.addr = alloca void (%struct.liq_color*, i32, i32, i8*)*, align 4
  %row_callback_user_info.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %gamma.addr = alloca double, align 8
  %img = alloca %struct.liq_image*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %.compoundliteral = alloca %struct.liq_image, align 8
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store %struct.rgba_pixel** %rows, %struct.rgba_pixel*** %rows.addr, align 4, !tbaa !2
  store void (%struct.liq_color*, i32, i32, i8*)* %row_callback, void (%struct.liq_color*, i32, i32, i8*)** %row_callback.addr, align 4, !tbaa !2
  store i8* %row_callback_user_info, i8** %row_callback_user_info.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i32 %height, i32* %height.addr, align 4, !tbaa !13
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  %0 = load double, double* %gamma.addr, align 8, !tbaa !18
  %cmp = fcmp olt double %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load double, double* %gamma.addr, align 8, !tbaa !18
  %cmp1 = fcmp ogt double %1, 1.000000e+00
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @liq_log_error(%struct.liq_attr* %2, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.6, i32 0, i32 0))
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.rgba_pixel** %3, null
  br i1 %tobool, label %if.end4, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end
  %4 = load void (%struct.liq_color*, i32, i32, i8*)*, void (%struct.liq_color*, i32, i32, i8*)** %row_callback.addr, align 4, !tbaa !2
  %tobool2 = icmp ne void (%struct.liq_color*, i32, i32, i8*)* %4, null
  br i1 %tobool2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %land.lhs.true
  %5 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @liq_log_error(%struct.liq_attr* %5, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.7, i32 0, i32 0))
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  br label %return

if.end4:                                          ; preds = %land.lhs.true, %if.end
  %6 = bitcast %struct.liq_image** %img to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %7, i32 0, i32 1
  %8 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %call = call i8* %8(i32 4184)
  %9 = bitcast i8* %call to %struct.liq_image*
  store %struct.liq_image* %9, %struct.liq_image** %img, align 4, !tbaa !2
  %10 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  %tobool5 = icmp ne %struct.liq_image* %10, null
  br i1 %tobool5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %if.end4
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end4
  %11 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  %12 = bitcast %struct.liq_image* %.compoundliteral to i8*
  call void @llvm.memset.p0i8.i64(i8* align 8 %12, i8 0, i64 4184, i1 false)
  %magic_header = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 0
  store i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !93
  %malloc8 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 1
  %13 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %malloc9 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %13, i32 0, i32 1
  %14 = load i8* (i32)*, i8* (i32)** %malloc9, align 4, !tbaa !52
  store i8* (i32)* %14, i8* (i32)** %malloc8, align 4, !tbaa !86
  %free = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 2
  %15 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %free10 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %15, i32 0, i32 2
  %16 = load void (i8*)*, void (i8*)** %free10, align 8, !tbaa !53
  store void (i8*)* %16, void (i8*)** %free, align 8, !tbaa !88
  %rows11 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 4
  %17 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows.addr, align 4, !tbaa !2
  store %struct.rgba_pixel** %17, %struct.rgba_pixel*** %rows11, align 8, !tbaa !79
  %gamma12 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 5
  %18 = load double, double* %gamma.addr, align 8, !tbaa !18
  %tobool13 = fcmp une double %18, 0.000000e+00
  br i1 %tobool13, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end7
  %19 = load double, double* %gamma.addr, align 8, !tbaa !18
  br label %cond.end

cond.false:                                       ; preds = %if.end7
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %19, %cond.true ], [ 4.545500e-01, %cond.false ]
  store double %cond, double* %gamma12, align 8, !tbaa !60
  %width14 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 6
  %20 = load i32, i32* %width.addr, align 4, !tbaa !13
  store i32 %20, i32* %width14, align 8, !tbaa !85
  %height15 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 7
  %21 = load i32, i32* %height.addr, align 4, !tbaa !13
  store i32 %21, i32* %height15, align 4, !tbaa !84
  %row_callback16 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 14
  %22 = load void (%struct.liq_color*, i32, i32, i8*)*, void (%struct.liq_color*, i32, i32, i8*)** %row_callback.addr, align 4, !tbaa !2
  store void (%struct.liq_color*, i32, i32, i8*)* %22, void (%struct.liq_color*, i32, i32, i8*)** %row_callback16, align 8, !tbaa !96
  %row_callback_user_info17 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 15
  %23 = load i8*, i8** %row_callback_user_info.addr, align 4, !tbaa !2
  store i8* %23, i8** %row_callback_user_info17, align 4, !tbaa !97
  %min_opaque_val = getelementptr inbounds %struct.liq_image, %struct.liq_image* %.compoundliteral, i32 0, i32 17
  %24 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_opaque_val18 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %24, i32 0, i32 6
  %25 = load float, float* %min_opaque_val18, align 8, !tbaa !42
  store float %25, float* %min_opaque_val, align 4, !tbaa !98
  %26 = bitcast %struct.liq_image* %11 to i8*
  %27 = bitcast %struct.liq_image* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %26, i8* align 8 %27, i32 4184, i1 false), !tbaa.struct !99
  %28 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows.addr, align 4, !tbaa !2
  %tobool19 = icmp ne %struct.rgba_pixel** %28, null
  br i1 %tobool19, label %lor.lhs.false20, label %if.then23

lor.lhs.false20:                                  ; preds = %cond.end
  %29 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_opaque_val21 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %29, i32 0, i32 6
  %30 = load float, float* %min_opaque_val21, align 8, !tbaa !42
  %cmp22 = fcmp olt float %30, 1.000000e+00
  br i1 %cmp22, label %if.then23, label %if.end31

if.then23:                                        ; preds = %lor.lhs.false20, %cond.end
  %31 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %malloc24 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %31, i32 0, i32 1
  %32 = load i8* (i32)*, i8* (i32)** %malloc24, align 4, !tbaa !52
  %33 = load i32, i32* %width.addr, align 4, !tbaa !13
  %mul = mul i32 4, %33
  %mul25 = mul i32 %mul, 1
  %call26 = call i8* %32(i32 %mul25)
  %34 = bitcast i8* %call26 to %struct.rgba_pixel*
  %35 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  %temp_row = getelementptr inbounds %struct.liq_image, %struct.liq_image* %35, i32 0, i32 12
  store %struct.rgba_pixel* %34, %struct.rgba_pixel** %temp_row, align 8, !tbaa !91
  %36 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  %temp_row27 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %36, i32 0, i32 12
  %37 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row27, align 8, !tbaa !91
  %tobool28 = icmp ne %struct.rgba_pixel* %37, null
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %if.then23
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end30:                                         ; preds = %if.then23
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %lor.lhs.false20
  %38 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  %39 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  %temp_row32 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %39, i32 0, i32 12
  %40 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row32, align 8, !tbaa !91
  %tobool33 = icmp ne %struct.rgba_pixel* %40, null
  br i1 %tobool33, label %land.end, label %land.lhs.true34

land.lhs.true34:                                  ; preds = %if.end31
  %41 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %use_contrast_maps = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %41, i32 0, i32 14
  %42 = load i8, i8* %use_contrast_maps, align 1, !tbaa !27, !range !29
  %tobool35 = trunc i8 %42 to i1
  br i1 %tobool35, label %land.end, label %land.rhs

land.rhs:                                         ; preds = %land.lhs.true34
  %43 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %use_dither_map = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %43, i32 0, i32 15
  %44 = load i8, i8* %use_dither_map, align 2, !tbaa !26
  %tobool36 = icmp ne i8 %44, 0
  %lnot = xor i1 %tobool36, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true34, %if.end31
  %45 = phi i1 [ false, %land.lhs.true34 ], [ false, %if.end31 ], [ %lnot, %land.rhs ]
  %call37 = call zeroext i1 @liq_image_should_use_low_memory(%struct.liq_image* %38, i1 zeroext %45)
  br i1 %call37, label %if.then38, label %if.end42

if.then38:                                        ; preds = %land.end
  %46 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @verbose_print(%struct.liq_attr* %46, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.8, i32 0, i32 0))
  %47 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  %call39 = call zeroext i1 @liq_image_use_low_memory(%struct.liq_image* %47)
  br i1 %call39, label %if.end41, label %if.then40

if.then40:                                        ; preds = %if.then38
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end41:                                         ; preds = %if.then38
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %land.end
  %48 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  %min_opaque_val43 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %48, i32 0, i32 17
  %49 = load float, float* %min_opaque_val43, align 4, !tbaa !98
  %cmp44 = fcmp olt float %49, 1.000000e+00
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.end42
  %50 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @verbose_print(%struct.liq_attr* %50, i8* getelementptr inbounds ([61 x i8], [61 x i8]* @.str.9, i32 0, i32 0))
  br label %if.end46

if.end46:                                         ; preds = %if.then45, %if.end42
  %51 = load %struct.liq_image*, %struct.liq_image** %img, align 4, !tbaa !2
  store %struct.liq_image* %51, %struct.liq_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end46, %if.then40, %if.then29, %if.then6
  %52 = bitcast %struct.liq_image** %img to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then3, %if.then
  %53 = load %struct.liq_image*, %struct.liq_image** %retval, align 4
  ret %struct.liq_image* %53
}

; Function Attrs: nounwind
define hidden %struct.liq_image* @liq_image_create_rgba_rows(%struct.liq_attr* nonnull %attr, i8** nonnull %rows, i32 %width, i32 %height, double %gamma) #4 {
entry:
  %retval = alloca %struct.liq_image*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %rows.addr = alloca i8**, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %gamma.addr = alloca double, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i8** %rows, i8*** %rows.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i32 %height, i32* %height.addr, align 4, !tbaa !13
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %1 = load i32, i32* %width.addr, align 4, !tbaa !13
  %2 = load i32, i32* %height.addr, align 4, !tbaa !13
  %call = call zeroext i1 @check_image_size(%struct.liq_attr* %0, i32 %1, i32 %2)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %4 = load i32, i32* %i, align 4, !tbaa !13
  %5 = load i32, i32* %height.addr, align 4, !tbaa !13
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %6 = load i8**, i8*** %rows.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !13
  %add.ptr = getelementptr inbounds i8*, i8** %6, i32 %7
  %8 = bitcast i8** %add.ptr to i8*
  %call1 = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %8)
  br i1 %call1, label %lor.lhs.false, label %if.then3

lor.lhs.false:                                    ; preds = %for.body
  %9 = load i8**, i8*** %rows.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds i8*, i8** %9, i32 %10
  %11 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %call2 = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %11)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %lor.lhs.false, %for.body
  %12 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @liq_log_error(%struct.liq_attr* %12, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.1, i32 0, i32 0))
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end4
  %13 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

cleanup:                                          ; preds = %if.then3, %for.cond.cleanup
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  %15 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %16 = load i8**, i8*** %rows.addr, align 4, !tbaa !2
  %17 = bitcast i8** %16 to %struct.rgba_pixel**
  %18 = load i32, i32* %width.addr, align 4, !tbaa !13
  %19 = load i32, i32* %height.addr, align 4, !tbaa !13
  %20 = load double, double* %gamma.addr, align 8, !tbaa !18
  %call5 = call %struct.liq_image* @liq_image_create_internal(%struct.liq_attr* %15, %struct.rgba_pixel** %17, void (%struct.liq_color*, i32, i32, i8*)* null, i8* null, i32 %18, i32 %19, double %20)
  store %struct.liq_image* %call5, %struct.liq_image** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup, %if.then
  %21 = load %struct.liq_image*, %struct.liq_image** %retval, align 4
  ret %struct.liq_image* %21

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @liq_log_error(%struct.liq_attr* nonnull %attr, i8* nonnull %msg) #4 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  %msg.addr = alloca i8*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i8* %msg, i8** %msg.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %2 = load i8*, i8** %msg.addr, align 4, !tbaa !2
  call void (%struct.liq_attr*, i8*, ...) @liq_verbose_printf(%struct.liq_attr* %1, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.10, i32 0, i32 0), i8* %2)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: nounwind
define hidden %struct.liq_image* @liq_image_create_rgba(%struct.liq_attr* nonnull %attr, i8* nonnull %bitmap, i32 %width, i32 %height, double %gamma) #4 {
entry:
  %retval = alloca %struct.liq_image*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %bitmap.addr = alloca i8*, align 4
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  %gamma.addr = alloca double, align 8
  %pixels = alloca %struct.rgba_pixel*, align 4
  %rows = alloca %struct.rgba_pixel**, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %image = alloca %struct.liq_image*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i8* %bitmap, i8** %bitmap.addr, align 4, !tbaa !2
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i32 %height, i32* %height.addr, align 4, !tbaa !13
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %1 = load i32, i32* %width.addr, align 4, !tbaa !13
  %2 = load i32, i32* %height.addr, align 4, !tbaa !13
  %call = call zeroext i1 @check_image_size(%struct.liq_attr* %0, i32 %1, i32 %2)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i8*, i8** %bitmap.addr, align 4, !tbaa !2
  %call1 = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %3)
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  %4 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  call void @liq_log_error(%struct.liq_attr* %4, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0))
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %5 = bitcast %struct.rgba_pixel** %pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load i8*, i8** %bitmap.addr, align 4, !tbaa !2
  %7 = bitcast i8* %6 to %struct.rgba_pixel*
  store %struct.rgba_pixel* %7, %struct.rgba_pixel** %pixels, align 4, !tbaa !2
  %8 = bitcast %struct.rgba_pixel*** %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %9, i32 0, i32 1
  %10 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %11 = load i32, i32* %height.addr, align 4, !tbaa !13
  %mul = mul i32 4, %11
  %call4 = call i8* %10(i32 %mul)
  %12 = bitcast i8* %call4 to %struct.rgba_pixel**
  store %struct.rgba_pixel** %12, %struct.rgba_pixel*** %rows, align 4, !tbaa !2
  %13 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 4, !tbaa !2
  %tobool = icmp ne %struct.rgba_pixel** %13, null
  br i1 %tobool, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end3
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup12

if.end6:                                          ; preds = %if.end3
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end6
  %15 = load i32, i32* %i, align 4, !tbaa !13
  %16 = load i32, i32* %height.addr, align 4, !tbaa !13
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load %struct.rgba_pixel*, %struct.rgba_pixel** %pixels, align 4, !tbaa !2
  %19 = load i32, i32* %width.addr, align 4, !tbaa !13
  %20 = load i32, i32* %i, align 4, !tbaa !13
  %mul7 = mul nsw i32 %19, %20
  %add.ptr = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %18, i32 %mul7
  %21 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.rgba_pixel*, %struct.rgba_pixel** %21, i32 %22
  store %struct.rgba_pixel* %add.ptr, %struct.rgba_pixel** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %24 = bitcast %struct.liq_image** %image to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %26 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 4, !tbaa !2
  %27 = load i32, i32* %width.addr, align 4, !tbaa !13
  %28 = load i32, i32* %height.addr, align 4, !tbaa !13
  %29 = load double, double* %gamma.addr, align 8, !tbaa !18
  %call8 = call %struct.liq_image* @liq_image_create_internal(%struct.liq_attr* %25, %struct.rgba_pixel** %26, void (%struct.liq_color*, i32, i32, i8*)* null, i8* null, i32 %27, i32 %28, double %29)
  store %struct.liq_image* %call8, %struct.liq_image** %image, align 4, !tbaa !2
  %30 = load %struct.liq_image*, %struct.liq_image** %image, align 4, !tbaa !2
  %tobool9 = icmp ne %struct.liq_image* %30, null
  br i1 %tobool9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %for.end
  %31 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %31, i32 0, i32 2
  %32 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !53
  %33 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 4, !tbaa !2
  %34 = bitcast %struct.rgba_pixel** %33 to i8*
  call void %32(i8* %34)
  store %struct.liq_image* null, %struct.liq_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %for.end
  %35 = load %struct.liq_image*, %struct.liq_image** %image, align 4, !tbaa !2
  %free_rows = getelementptr inbounds %struct.liq_image, %struct.liq_image* %35, i32 0, i32 21
  store i8 1, i8* %free_rows, align 1, !tbaa !81
  %36 = load %struct.liq_image*, %struct.liq_image** %image, align 4, !tbaa !2
  %free_rows_internal = getelementptr inbounds %struct.liq_image, %struct.liq_image* %36, i32 0, i32 22
  store i8 1, i8* %free_rows_internal, align 4, !tbaa !80
  %37 = load %struct.liq_image*, %struct.liq_image** %image, align 4, !tbaa !2
  store %struct.liq_image* %37, %struct.liq_image** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end11, %if.then10
  %38 = bitcast %struct.liq_image** %image to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  br label %cleanup12

cleanup12:                                        ; preds = %cleanup, %if.then5
  %39 = bitcast %struct.rgba_pixel*** %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast %struct.rgba_pixel** %pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  br label %return

return:                                           ; preds = %cleanup12, %if.then2, %if.then
  %41 = load %struct.liq_image*, %struct.liq_image** %retval, align 4
  ret %struct.liq_image* %41
}

; Function Attrs: noinline nounwind
define hidden void @liq_executing_user_callback(void (%struct.liq_color*, i32, i32, i8*)* %callback, %struct.liq_color* %temp_row, i32 %row, i32 %width, i8* %user_info) #0 {
entry:
  %callback.addr = alloca void (%struct.liq_color*, i32, i32, i8*)*, align 4
  %temp_row.addr = alloca %struct.liq_color*, align 4
  %row.addr = alloca i32, align 4
  %width.addr = alloca i32, align 4
  %user_info.addr = alloca i8*, align 4
  store void (%struct.liq_color*, i32, i32, i8*)* %callback, void (%struct.liq_color*, i32, i32, i8*)** %callback.addr, align 4, !tbaa !2
  store %struct.liq_color* %temp_row, %struct.liq_color** %temp_row.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !13
  store i32 %width, i32* %width.addr, align 4, !tbaa !13
  store i8* %user_info, i8** %user_info.addr, align 4, !tbaa !2
  %0 = load void (%struct.liq_color*, i32, i32, i8*)*, void (%struct.liq_color*, i32, i32, i8*)** %callback.addr, align 4, !tbaa !2
  %1 = load %struct.liq_color*, %struct.liq_color** %temp_row.addr, align 4, !tbaa !2
  %2 = load i32, i32* %row.addr, align 4, !tbaa !13
  %3 = load i32, i32* %width.addr, align 4, !tbaa !13
  %4 = load i8*, i8** %user_info.addr, align 4, !tbaa !2
  call void %0(%struct.liq_color* %1, i32 %2, i32 %3, i8* %4)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @liq_image_get_width(%struct.liq_image* nonnull %input_image) #4 {
entry:
  %retval = alloca i32, align 4
  %input_image.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_image* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 6
  %3 = load i32, i32* %width, align 8, !tbaa !85
  store i32 %3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: nounwind
define hidden i32 @liq_image_get_height(%struct.liq_image* nonnull %input_image) #4 {
entry:
  %retval = alloca i32, align 4
  %input_image.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_image* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 7
  %3 = load i32, i32* %height, align 4, !tbaa !84
  store i32 %3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: nounwind
define internal void @liq_image_free_rgba_source(%struct.liq_image* nonnull %input_image) #4 {
entry:
  %input_image.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free_pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %0, i32 0, i32 20
  %1 = load i8, i8* %free_pixels, align 2, !tbaa !82, !range !29
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 11
  %3 = load %struct.rgba_pixel*, %struct.rgba_pixel** %pixels, align 4, !tbaa !83
  %tobool1 = icmp ne %struct.rgba_pixel* %3, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %4 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %call = call void (i8*)* @get_default_free_func(%struct.liq_image* %4)
  %5 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %pixels2 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %5, i32 0, i32 11
  %6 = load %struct.rgba_pixel*, %struct.rgba_pixel** %pixels2, align 4, !tbaa !83
  %7 = bitcast %struct.rgba_pixel* %6 to i8*
  call void %call(i8* %7)
  %8 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %pixels3 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 11
  store %struct.rgba_pixel* null, %struct.rgba_pixel** %pixels3, align 4, !tbaa !83
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %9 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free_rows = getelementptr inbounds %struct.liq_image, %struct.liq_image* %9, i32 0, i32 21
  %10 = load i8, i8* %free_rows, align 1, !tbaa !81, !range !29
  %tobool4 = trunc i8 %10 to i1
  br i1 %tobool4, label %land.lhs.true5, label %if.end11

land.lhs.true5:                                   ; preds = %if.end
  %11 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.liq_image, %struct.liq_image* %11, i32 0, i32 4
  %12 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 8, !tbaa !79
  %tobool6 = icmp ne %struct.rgba_pixel** %12, null
  br i1 %tobool6, label %if.then7, label %if.end11

if.then7:                                         ; preds = %land.lhs.true5
  %13 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %call8 = call void (i8*)* @get_default_free_func(%struct.liq_image* %13)
  %14 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %rows9 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %14, i32 0, i32 4
  %15 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows9, align 8, !tbaa !79
  %16 = bitcast %struct.rgba_pixel** %15 to i8*
  call void %call8(i8* %16)
  %17 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %rows10 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %17, i32 0, i32 4
  store %struct.rgba_pixel** null, %struct.rgba_pixel*** %rows10, align 8, !tbaa !79
  br label %if.end11

if.end11:                                         ; preds = %if.then7, %land.lhs.true5, %if.end
  ret void
}

; Function Attrs: nounwind
define hidden %struct.liq_histogram* @liq_histogram_create(%struct.liq_attr* %attr) #4 {
entry:
  %retval = alloca %struct.liq_histogram*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %hist = alloca %struct.liq_histogram*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %.compoundliteral = alloca %struct.liq_histogram, align 8
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.liq_histogram* null, %struct.liq_histogram** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %struct.liq_histogram** %hist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 1
  %3 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %call1 = call i8* %3(i32 4128)
  %4 = bitcast i8* %call1 to %struct.liq_histogram*
  store %struct.liq_histogram* %4, %struct.liq_histogram** %hist, align 4, !tbaa !2
  %5 = load %struct.liq_histogram*, %struct.liq_histogram** %hist, align 4, !tbaa !2
  %tobool = icmp ne %struct.liq_histogram* %5, null
  br i1 %tobool, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store %struct.liq_histogram* null, %struct.liq_histogram** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %6 = load %struct.liq_histogram*, %struct.liq_histogram** %hist, align 4, !tbaa !2
  %7 = bitcast %struct.liq_histogram* %.compoundliteral to i8*
  call void @llvm.memset.p0i8.i64(i8* align 8 %7, i8 0, i64 4128, i1 false)
  %magic_header = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %.compoundliteral, i32 0, i32 0
  store i8* getelementptr inbounds ([14 x i8], [14 x i8]* @liq_histogram_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !101
  %malloc4 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %.compoundliteral, i32 0, i32 1
  %8 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %malloc5 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %8, i32 0, i32 1
  %9 = load i8* (i32)*, i8* (i32)** %malloc5, align 4, !tbaa !52
  store i8* (i32)* %9, i8* (i32)** %malloc4, align 4, !tbaa !102
  %free = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %.compoundliteral, i32 0, i32 2
  %10 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %free6 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %10, i32 0, i32 2
  %11 = load void (i8*)*, void (i8*)** %free6, align 8, !tbaa !53
  store void (i8*)* %11, void (i8*)** %free, align 8, !tbaa !103
  %ignorebits = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %.compoundliteral, i32 0, i32 7
  %12 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_posterization_output = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %12, i32 0, i32 9
  %13 = load i32, i32* %min_posterization_output, align 4, !tbaa !20
  %14 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_posterization_input = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %14, i32 0, i32 10
  %15 = load i32, i32* %min_posterization_input, align 8, !tbaa !25
  %cmp = icmp ugt i32 %13, %15
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end3
  %16 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_posterization_output7 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %16, i32 0, i32 9
  %17 = load i32, i32* %min_posterization_output7, align 4, !tbaa !20
  br label %cond.end

cond.false:                                       ; preds = %if.end3
  %18 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %min_posterization_input8 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %18, i32 0, i32 10
  %19 = load i32, i32* %min_posterization_input8, align 8, !tbaa !25
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %17, %cond.true ], [ %19, %cond.false ]
  %conv = trunc i32 %cond to i16
  store i16 %conv, i16* %ignorebits, align 2, !tbaa !104
  %20 = bitcast %struct.liq_histogram* %6 to i8*
  %21 = bitcast %struct.liq_histogram* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %20, i8* align 8 %21, i32 4128, i1 false), !tbaa.struct !105
  %22 = load %struct.liq_histogram*, %struct.liq_histogram** %hist, align 4, !tbaa !2
  store %struct.liq_histogram* %22, %struct.liq_histogram** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end, %if.then2
  %23 = bitcast %struct.liq_histogram** %hist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %24 = load %struct.liq_histogram*, %struct.liq_histogram** %retval, align 4
  ret %struct.liq_histogram* %24
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i1 immarg) #6

; Function Attrs: nounwind
define hidden void @liq_histogram_destroy(%struct.liq_histogram* nonnull %hist) #4 {
entry:
  %hist.addr = alloca %struct.liq_histogram*, align 4
  store %struct.liq_histogram* %hist, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %0 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_histogram* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @liq_histogram_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %magic_header = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %2, i32 0, i32 0
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @liq_freed_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !101
  %3 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %acht = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %3, i32 0, i32 3
  %4 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht, align 4, !tbaa !106
  call void @pam_freeacolorhash(%struct.acolorhash_table* %4)
  %5 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %5, i32 0, i32 2
  %6 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !103
  %7 = load %struct.liq_histogram*, %struct.liq_histogram** %hist.addr, align 4, !tbaa !2
  %8 = bitcast %struct.liq_histogram* %7 to i8*
  call void %6(i8* %8)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

declare hidden void @pam_freeacolorhash(%struct.acolorhash_table*) #1

; Function Attrs: nounwind
define hidden %struct.liq_result* @liq_quantize_image(%struct.liq_attr* nonnull %attr, %struct.liq_image* nonnull %img) #4 {
entry:
  %retval = alloca %struct.liq_result*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  %res = alloca %struct.liq_result*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %0 = bitcast %struct.liq_result** %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call i32 @liq_image_quantize(%struct.liq_image* %1, %struct.liq_attr* %2, %struct.liq_result** %res)
  %cmp = icmp ne i32 0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.liq_result* null, %struct.liq_result** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = load %struct.liq_result*, %struct.liq_result** %res, align 4, !tbaa !2
  store %struct.liq_result* %3, %struct.liq_result** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %4 = bitcast %struct.liq_result** %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = load %struct.liq_result*, %struct.liq_result** %retval, align 4
  ret %struct.liq_result* %5
}

; Function Attrs: nounwind
define hidden i32 @liq_image_quantize(%struct.liq_image* nonnull %img, %struct.liq_attr* nonnull %attr, %struct.liq_result** nonnull %result_output) #4 {
entry:
  %retval = alloca i32, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %result_output.addr = alloca %struct.liq_result**, align 4
  %hist = alloca %struct.liq_histogram*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %err = alloca i32, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store %struct.liq_result** %result_output, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %call1 = call zeroext i1 @liq_image_has_rgba_pixels(%struct.liq_image* %1)
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 106, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = bitcast %struct.liq_histogram** %hist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call4 = call %struct.liq_histogram* @liq_histogram_create(%struct.liq_attr* %3)
  store %struct.liq_histogram* %call4, %struct.liq_histogram** %hist, align 4, !tbaa !2
  %4 = load %struct.liq_histogram*, %struct.liq_histogram** %hist, align 4, !tbaa !2
  %tobool = icmp ne %struct.liq_histogram* %4, null
  br i1 %tobool, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end3
  store i32 101, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup11

if.end6:                                          ; preds = %if.end3
  %5 = bitcast i32* %err to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %struct.liq_histogram*, %struct.liq_histogram** %hist, align 4, !tbaa !2
  %7 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %8 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %call7 = call i32 @liq_histogram_add_image(%struct.liq_histogram* %6, %struct.liq_attr* %7, %struct.liq_image* %8)
  store i32 %call7, i32* %err, align 4, !tbaa !12
  %9 = load i32, i32* %err, align 4, !tbaa !12
  %cmp = icmp ne i32 0, %9
  br i1 %cmp, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  %10 = load i32, i32* %err, align 4, !tbaa !12
  store i32 %10, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end6
  %11 = load %struct.liq_histogram*, %struct.liq_histogram** %hist, align 4, !tbaa !2
  %12 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %13 = load %struct.liq_result**, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  %call10 = call i32 @liq_histogram_quantize_internal(%struct.liq_histogram* %11, %struct.liq_attr* %12, i1 zeroext false, %struct.liq_result** %13)
  store i32 %call10, i32* %err, align 4, !tbaa !12
  %14 = load %struct.liq_histogram*, %struct.liq_histogram** %hist, align 4, !tbaa !2
  call void @liq_histogram_destroy(%struct.liq_histogram* %14)
  %15 = load i32, i32* %err, align 4, !tbaa !12
  store i32 %15, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end9, %if.then8
  %16 = bitcast i32* %err to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  br label %cleanup11

cleanup11:                                        ; preds = %cleanup, %if.then5
  %17 = bitcast %struct.liq_histogram** %hist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %return

return:                                           ; preds = %cleanup11, %if.then2, %if.then
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

; Function Attrs: inlinehint nounwind
define internal zeroext i1 @liq_image_has_rgba_pixels(%struct.liq_image* nonnull %img) #7 {
entry:
  %retval = alloca i1, align 1
  %img.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_image* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 4
  %3 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 8, !tbaa !79
  %tobool = icmp ne %struct.rgba_pixel** %3, null
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end
  %4 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %temp_row = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 12
  %5 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row, align 8, !tbaa !91
  %tobool1 = icmp ne %struct.rgba_pixel* %5, null
  br i1 %tobool1, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %lor.rhs
  %6 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %row_callback = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 14
  %7 = load void (%struct.liq_color*, i32, i32, i8*)*, void (%struct.liq_color*, i32, i32, i8*)** %row_callback, align 8, !tbaa !96
  %tobool2 = icmp ne void (%struct.liq_color*, i32, i32, i8*)* %7, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %lor.rhs
  %8 = phi i1 [ false, %lor.rhs ], [ %tobool2, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %if.end
  %9 = phi i1 [ true, %if.end ], [ %8, %land.end ]
  store i1 %9, i1* %retval, align 1
  br label %return

return:                                           ; preds = %lor.end, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: nounwind
define hidden i32 @liq_histogram_add_image(%struct.liq_histogram* nonnull %input_hist, %struct.liq_attr* nonnull %options, %struct.liq_image* nonnull %input_image) #4 {
entry:
  %retval = alloca i32, align 4
  %input_hist.addr = alloca %struct.liq_histogram*, align 4
  %options.addr = alloca %struct.liq_attr*, align 4
  %input_image.addr = alloca %struct.liq_image*, align 4
  %cols = alloca i32, align 4
  %rows = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %res = alloca i32, align 4
  %all_rows_at_once = alloca i8, align 1
  %max_histogram_entries = alloca i32, align 4
  %row = alloca i32, align 4
  %added_ok = alloca i8, align 1
  %rows_p = alloca [1 x %struct.rgba_pixel*], align 4
  store %struct.liq_histogram* %input_hist, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  store %struct.liq_attr* %options, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %2 = bitcast %struct.liq_histogram* %1 to %struct.liq_attr*
  %call1 = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %2, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @liq_histogram_magic, i32 0, i32 0))
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 105, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %4 = bitcast %struct.liq_image* %3 to %struct.liq_attr*
  %call4 = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %4, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end3
  store i32 105, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %5 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 6
  %7 = load i32, i32* %width, align 8, !tbaa !85
  store i32 %7, i32* %cols, align 4, !tbaa !13
  %8 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %9, i32 0, i32 7
  %10 = load i32, i32* %height, align 4, !tbaa !84
  store i32 %10, i32* %rows, align 4, !tbaa !13
  %11 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %importance_map = getelementptr inbounds %struct.liq_image, %struct.liq_image* %11, i32 0, i32 8
  %12 = load i8*, i8** %importance_map, align 8, !tbaa !87
  %tobool = icmp ne i8* %12, null
  br i1 %tobool, label %if.end9, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end6
  %13 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %use_contrast_maps = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %13, i32 0, i32 14
  %14 = load i8, i8* %use_contrast_maps, align 1, !tbaa !27, !range !29
  %tobool7 = trunc i8 %14 to i1
  br i1 %tobool7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %land.lhs.true
  %15 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  call void @contrast_maps(%struct.liq_image* %15)
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %land.lhs.true, %if.end6
  %16 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_image, %struct.liq_image* %16, i32 0, i32 5
  %17 = load double, double* %gamma, align 8, !tbaa !60
  %18 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %gamma10 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %18, i32 0, i32 4
  store double %17, double* %gamma10, align 8, !tbaa !107
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end9
  %20 = load i32, i32* %i, align 4, !tbaa !13
  %21 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %fixed_colors_count = getelementptr inbounds %struct.liq_image, %struct.liq_image* %21, i32 0, i32 19
  %22 = load i16, i16* %fixed_colors_count, align 8, !tbaa !57
  %conv = zext i16 %22 to i32
  %cmp = icmp slt i32 %20, %conv
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

for.body:                                         ; preds = %for.cond
  %23 = bitcast i32* %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #8
  %24 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %25 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %fixed_colors = getelementptr inbounds %struct.liq_image, %struct.liq_image* %25, i32 0, i32 18
  %26 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds [256 x %struct.f_pixel], [256 x %struct.f_pixel]* %fixed_colors, i32 0, i32 %26
  %call12 = call i32 @liq_histogram_add_fixed_color_f(%struct.liq_histogram* %24, %struct.f_pixel* byval(%struct.f_pixel) align 4 %arrayidx)
  store i32 %call12, i32* %res, align 4, !tbaa !12
  %27 = load i32, i32* %res, align 4, !tbaa !12
  %cmp13 = icmp ne i32 %27, 0
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %for.body
  %28 = load i32, i32* %res, align 4, !tbaa !12
  store i32 %28, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.then15
  %29 = bitcast i32* %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup17 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %30 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

cleanup17:                                        ; preds = %cleanup, %for.cond.cleanup
  %31 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %cleanup.dest18 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest18, label %cleanup103 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup17
  %32 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %33 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %33, i32 0, i32 17
  %34 = load i8, i8* %progress_stage1, align 8, !tbaa !30
  %conv19 = zext i8 %34 to i32
  %conv20 = sitofp i32 %conv19 to float
  %mul = fmul float %conv20, 0x3FD99999A0000000
  %call21 = call zeroext i1 @liq_progress(%struct.liq_attr* %32, float %mul)
  br i1 %call21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %for.end
  store i32 102, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup103

if.end23:                                         ; preds = %for.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %all_rows_at_once) #8
  %35 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %call24 = call zeroext i1 @liq_image_can_use_rgba_rows(%struct.liq_image* %35)
  %frombool = zext i1 %call24 to i8
  store i8 %frombool, i8* %all_rows_at_once, align 1, !tbaa !56
  %36 = bitcast i32* %max_histogram_entries to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #8
  %37 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %had_image_added = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %37, i32 0, i32 8
  %38 = load i8, i8* %had_image_added, align 4, !tbaa !108, !range !29
  %tobool25 = trunc i8 %38 to i1
  br i1 %tobool25, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end23
  br label %cond.end

cond.false:                                       ; preds = %if.end23
  %39 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %max_histogram_entries27 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %39, i32 0, i32 8
  %40 = load i32, i32* %max_histogram_entries27, align 8, !tbaa !24
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ -1, %cond.true ], [ %40, %cond.false ]
  store i32 %cond, i32* %max_histogram_entries, align 4, !tbaa !13
  br label %do.body

do.body:                                          ; preds = %do.cond, %cond.end
  %41 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %41, i32 0, i32 3
  %42 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht, align 4, !tbaa !106
  %tobool28 = icmp ne %struct.acolorhash_table* %42, null
  br i1 %tobool28, label %if.end34, label %if.then29

if.then29:                                        ; preds = %do.body
  %43 = load i32, i32* %max_histogram_entries, align 4, !tbaa !13
  %44 = load i32, i32* %rows, align 4, !tbaa !13
  %45 = load i32, i32* %cols, align 4, !tbaa !13
  %mul30 = mul i32 %44, %45
  %46 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %ignorebits = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %46, i32 0, i32 7
  %47 = load i16, i16* %ignorebits, align 2, !tbaa !104
  %conv31 = zext i16 %47 to i32
  %48 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %48, i32 0, i32 1
  %49 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %50 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %50, i32 0, i32 2
  %51 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !53
  %call32 = call %struct.acolorhash_table* @pam_allocacolorhash(i32 %43, i32 %mul30, i32 %conv31, i8* (i32)* %49, void (i8*)* %51)
  %52 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht33 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %52, i32 0, i32 3
  store %struct.acolorhash_table* %call32, %struct.acolorhash_table** %acht33, align 4, !tbaa !106
  br label %if.end34

if.end34:                                         ; preds = %if.then29, %do.body
  %53 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht35 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %53, i32 0, i32 3
  %54 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht35, align 4, !tbaa !106
  %tobool36 = icmp ne %struct.acolorhash_table* %54, null
  br i1 %tobool36, label %if.end38, label %if.then37

if.then37:                                        ; preds = %if.end34
  store i32 101, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

if.end38:                                         ; preds = %if.end34
  %55 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #8
  store i32 0, i32* %row, align 4, !tbaa !13
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc87, %if.end38
  %56 = load i32, i32* %row, align 4, !tbaa !13
  %57 = load i32, i32* %rows, align 4, !tbaa !13
  %cmp40 = icmp ult i32 %56, %57
  br i1 %cmp40, label %for.body43, label %for.cond.cleanup42

for.cond.cleanup42:                               ; preds = %for.cond39
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup89

for.body43:                                       ; preds = %for.cond39
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %added_ok) #8
  %58 = load i8, i8* %all_rows_at_once, align 1, !tbaa !56, !range !29
  %tobool44 = trunc i8 %58 to i1
  br i1 %tobool44, label %if.then45, label %if.else

if.then45:                                        ; preds = %for.body43
  %59 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht46 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %59, i32 0, i32 3
  %60 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht46, align 4, !tbaa !106
  %61 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %rows47 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %61, i32 0, i32 4
  %62 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows47, align 8, !tbaa !79
  %63 = load i32, i32* %cols, align 4, !tbaa !13
  %64 = load i32, i32* %rows, align 4, !tbaa !13
  %65 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %importance_map48 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %65, i32 0, i32 8
  %66 = load i8*, i8** %importance_map48, align 8, !tbaa !87
  %call49 = call zeroext i1 @pam_computeacolorhash(%struct.acolorhash_table* %60, %struct.rgba_pixel** %62, i32 %63, i32 %64, i8* %66)
  %frombool50 = zext i1 %call49 to i8
  store i8 %frombool50, i8* %added_ok, align 1, !tbaa !56
  %67 = load i8, i8* %added_ok, align 1, !tbaa !56, !range !29
  %tobool51 = trunc i8 %67 to i1
  br i1 %tobool51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %if.then45
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup84

if.end53:                                         ; preds = %if.then45
  br label %if.end67

if.else:                                          ; preds = %for.body43
  %68 = bitcast [1 x %struct.rgba_pixel*]* %rows_p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #8
  %arrayinit.begin = getelementptr inbounds [1 x %struct.rgba_pixel*], [1 x %struct.rgba_pixel*]* %rows_p, i32 0, i32 0
  %69 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %70 = load i32, i32* %row, align 4, !tbaa !13
  %call54 = call %struct.rgba_pixel* @liq_image_get_row_rgba(%struct.liq_image* %69, i32 %70)
  store %struct.rgba_pixel* %call54, %struct.rgba_pixel** %arrayinit.begin, align 4, !tbaa !2
  %71 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht55 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %71, i32 0, i32 3
  %72 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht55, align 4, !tbaa !106
  %arraydecay = getelementptr inbounds [1 x %struct.rgba_pixel*], [1 x %struct.rgba_pixel*]* %rows_p, i32 0, i32 0
  %73 = load i32, i32* %cols, align 4, !tbaa !13
  %74 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %importance_map56 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %74, i32 0, i32 8
  %75 = load i8*, i8** %importance_map56, align 8, !tbaa !87
  %tobool57 = icmp ne i8* %75, null
  br i1 %tobool57, label %cond.true58, label %cond.false62

cond.true58:                                      ; preds = %if.else
  %76 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %importance_map59 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %76, i32 0, i32 8
  %77 = load i8*, i8** %importance_map59, align 8, !tbaa !87
  %78 = load i32, i32* %row, align 4, !tbaa !13
  %79 = load i32, i32* %cols, align 4, !tbaa !13
  %mul60 = mul i32 %78, %79
  %arrayidx61 = getelementptr inbounds i8, i8* %77, i32 %mul60
  br label %cond.end63

cond.false62:                                     ; preds = %if.else
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false62, %cond.true58
  %cond64 = phi i8* [ %arrayidx61, %cond.true58 ], [ null, %cond.false62 ]
  %call65 = call zeroext i1 @pam_computeacolorhash(%struct.acolorhash_table* %72, %struct.rgba_pixel** %arraydecay, i32 %73, i32 1, i8* %cond64)
  %frombool66 = zext i1 %call65 to i8
  store i8 %frombool66, i8* %added_ok, align 1, !tbaa !56
  %80 = bitcast [1 x %struct.rgba_pixel*]* %rows_p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  br label %if.end67

if.end67:                                         ; preds = %cond.end63, %if.end53
  %81 = load i8, i8* %added_ok, align 1, !tbaa !56, !range !29
  %tobool68 = trunc i8 %81 to i1
  br i1 %tobool68, label %if.end83, label %if.then69

if.then69:                                        ; preds = %if.end67
  %82 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %ignorebits70 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %82, i32 0, i32 7
  %83 = load i16, i16* %ignorebits70, align 2, !tbaa !104
  %inc71 = add i16 %83, 1
  store i16 %inc71, i16* %ignorebits70, align 2, !tbaa !104
  %84 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %85 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %ignorebits72 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %85, i32 0, i32 7
  %86 = load i16, i16* %ignorebits72, align 2, !tbaa !104
  %conv73 = zext i16 %86 to i32
  call void (%struct.liq_attr*, i8*, ...) @liq_verbose_printf(%struct.liq_attr* %84, i8* getelementptr inbounds ([62 x i8], [62 x i8]* @.str.3, i32 0, i32 0), i32 %conv73)
  %87 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht74 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %87, i32 0, i32 3
  %88 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht74, align 4, !tbaa !106
  call void @pam_freeacolorhash(%struct.acolorhash_table* %88)
  %89 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht75 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %89, i32 0, i32 3
  store %struct.acolorhash_table* null, %struct.acolorhash_table** %acht75, align 4, !tbaa !106
  %90 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %91 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage176 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %91, i32 0, i32 17
  %92 = load i8, i8* %progress_stage176, align 8, !tbaa !30
  %conv77 = zext i8 %92 to i32
  %conv78 = sitofp i32 %conv77 to float
  %mul79 = fmul float %conv78, 0x3FE3333340000000
  %call80 = call zeroext i1 @liq_progress(%struct.liq_attr* %90, float %mul79)
  br i1 %call80, label %if.then81, label %if.end82

if.then81:                                        ; preds = %if.then69
  store i32 102, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup84

if.end82:                                         ; preds = %if.then69
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup84

if.end83:                                         ; preds = %if.end67
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup84

cleanup84:                                        ; preds = %if.end83, %if.end82, %if.then81, %if.then52
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %added_ok) #8
  %cleanup.dest85 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest85, label %cleanup89 [
    i32 0, label %cleanup.cont86
  ]

cleanup.cont86:                                   ; preds = %cleanup84
  br label %for.inc87

for.inc87:                                        ; preds = %cleanup.cont86
  %93 = load i32, i32* %row, align 4, !tbaa !13
  %inc88 = add i32 %93, 1
  store i32 %inc88, i32* %row, align 4, !tbaa !13
  br label %for.cond39

cleanup89:                                        ; preds = %cleanup84, %for.cond.cleanup42
  %94 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  %cleanup.dest90 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest90, label %cleanup101 [
    i32 7, label %for.end91
  ]

for.end91:                                        ; preds = %cleanup89
  br label %do.cond

do.cond:                                          ; preds = %for.end91
  %95 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht92 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %95, i32 0, i32 3
  %96 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht92, align 4, !tbaa !106
  %tobool93 = icmp ne %struct.acolorhash_table* %96, null
  %lnot = xor i1 %tobool93, true
  br i1 %lnot, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %97 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %had_image_added94 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %97, i32 0, i32 8
  store i8 1, i8* %had_image_added94, align 4, !tbaa !108
  %98 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  call void @liq_image_free_importance_map(%struct.liq_image* %98)
  %99 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free_pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %99, i32 0, i32 20
  %100 = load i8, i8* %free_pixels, align 2, !tbaa !82, !range !29
  %tobool95 = trunc i8 %100 to i1
  br i1 %tobool95, label %land.lhs.true97, label %if.end100

land.lhs.true97:                                  ; preds = %do.end
  %101 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %f_pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %101, i32 0, i32 3
  %102 = load %struct.f_pixel*, %struct.f_pixel** %f_pixels, align 4, !tbaa !90
  %tobool98 = icmp ne %struct.f_pixel* %102, null
  br i1 %tobool98, label %if.then99, label %if.end100

if.then99:                                        ; preds = %land.lhs.true97
  %103 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  call void @liq_image_free_rgba_source(%struct.liq_image* %103)
  br label %if.end100

if.end100:                                        ; preds = %if.then99, %land.lhs.true97, %do.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

cleanup101:                                       ; preds = %if.end100, %cleanup89, %if.then37
  %104 = bitcast i32* %max_histogram_entries to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %all_rows_at_once) #8
  br label %cleanup103

cleanup103:                                       ; preds = %cleanup101, %if.then22, %cleanup17
  %105 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #8
  %106 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #8
  br label %return

return:                                           ; preds = %cleanup103, %if.then5, %if.then2, %if.then
  %107 = load i32, i32* %retval, align 4
  ret i32 %107
}

; Function Attrs: nounwind
define internal i32 @liq_histogram_quantize_internal(%struct.liq_histogram* nonnull %input_hist, %struct.liq_attr* nonnull %attr, i1 zeroext %fixed_result_colors, %struct.liq_result** nonnull %result_output) #4 {
entry:
  %retval = alloca i32, align 4
  %input_hist.addr = alloca %struct.liq_histogram*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %fixed_result_colors.addr = alloca i8, align 1
  %result_output.addr = alloca %struct.liq_result**, align 4
  %hist = alloca %struct.histogram*, align 4
  %err = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.liq_histogram* %input_hist, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %frombool = zext i1 %fixed_result_colors to i8
  store i8 %frombool, i8* %fixed_result_colors.addr, align 1, !tbaa !56
  store %struct.liq_result** %result_output, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result**, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result** %0 to i8*
  %call = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %1)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result**, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  store %struct.liq_result* null, %struct.liq_result** %2, align 4, !tbaa !2
  %3 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call1 = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %3, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 105, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %5 = bitcast %struct.liq_histogram* %4 to %struct.liq_attr*
  %call4 = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %5, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @liq_histogram_magic, i32 0, i32 0))
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end3
  store i32 105, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %6 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call7 = call zeroext i1 @liq_progress(%struct.liq_attr* %6, float 0.000000e+00)
  br i1 %call7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 102, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end6
  %7 = bitcast %struct.histogram** %hist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = bitcast i32* %err to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %10 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %call10 = call i32 @finalize_histogram(%struct.liq_histogram* %9, %struct.liq_attr* %10, %struct.histogram** %hist)
  store i32 %call10, i32* %err, align 4, !tbaa !12
  %11 = load i32, i32* %err, align 4, !tbaa !12
  %cmp = icmp ne i32 %11, 0
  br i1 %cmp, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  %12 = load i32, i32* %err, align 4, !tbaa !12
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end9
  %13 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %14 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %15 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %fixed_colors_count = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %15, i32 0, i32 6
  %16 = load i16, i16* %fixed_colors_count, align 8, !tbaa !77
  %conv = zext i16 %16 to i32
  %17 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %fixed_colors = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %17, i32 0, i32 5
  %arraydecay = getelementptr inbounds [256 x %struct.f_pixel], [256 x %struct.f_pixel]* %fixed_colors, i32 0, i32 0
  %18 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %18, i32 0, i32 4
  %19 = load double, double* %gamma, align 8, !tbaa !107
  %20 = load i8, i8* %fixed_result_colors.addr, align 1, !tbaa !56, !range !29
  %tobool = trunc i8 %20 to i1
  %21 = load %struct.liq_result**, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  %call13 = call i32 @pngquant_quantize(%struct.histogram* %13, %struct.liq_attr* %14, i32 %conv, %struct.f_pixel* %arraydecay, double %19, i1 zeroext %tobool, %struct.liq_result** %21)
  store i32 %call13, i32* %err, align 4, !tbaa !12
  %22 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  call void @pam_freeacolorhist(%struct.histogram* %22)
  %23 = load i32, i32* %err, align 4, !tbaa !12
  store i32 %23, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end12, %if.then11
  %24 = bitcast i32* %err to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast %struct.histogram** %hist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then8, %if.then5, %if.then2, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define hidden i32 @liq_histogram_quantize(%struct.liq_histogram* nonnull %input_hist, %struct.liq_attr* nonnull %attr, %struct.liq_result** nonnull %result_output) #4 {
entry:
  %input_hist.addr = alloca %struct.liq_histogram*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %result_output.addr = alloca %struct.liq_result**, align 4
  store %struct.liq_histogram* %input_hist, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store %struct.liq_result** %result_output, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  %0 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %1 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %2 = load %struct.liq_result**, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  %call = call i32 @liq_histogram_quantize_internal(%struct.liq_histogram* %0, %struct.liq_attr* %1, i1 zeroext true, %struct.liq_result** %2)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @liq_set_dithering_level(%struct.liq_result* nonnull %res, float %dither_level) #4 {
entry:
  %retval = alloca i32, align 4
  %res.addr = alloca %struct.liq_result*, align 4
  %dither_level.addr = alloca float, align 4
  store %struct.liq_result* %res, %struct.liq_result** %res.addr, align 4, !tbaa !2
  store float %dither_level, float* %dither_level.addr, align 4, !tbaa !55
  %0 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping = getelementptr inbounds %struct.liq_result, %struct.liq_result* %2, i32 0, i32 3
  %3 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping, align 4, !tbaa !33
  %tobool = icmp ne %struct.liq_remapping_result* %3, null
  br i1 %tobool, label %if.then1, label %if.end4

if.then1:                                         ; preds = %if.end
  %4 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping2 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %4, i32 0, i32 3
  %5 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping2, align 4, !tbaa !33
  call void @liq_remapping_result_destroy(%struct.liq_remapping_result* %5)
  %6 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping3 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %6, i32 0, i32 3
  store %struct.liq_remapping_result* null, %struct.liq_remapping_result** %remapping3, align 4, !tbaa !33
  br label %if.end4

if.end4:                                          ; preds = %if.then1, %if.end
  %7 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %dither_level5 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %7, i32 0, i32 8
  %8 = load float, float* %dither_level5, align 8, !tbaa !109
  %cmp = fcmp olt float %8, 0.000000e+00
  br i1 %cmp, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end4
  %9 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %dither_level6 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %9, i32 0, i32 8
  %10 = load float, float* %dither_level6, align 8, !tbaa !109
  %cmp7 = fcmp ogt float %10, 1.000000e+00
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %lor.lhs.false, %if.end4
  store i32 100, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %lor.lhs.false
  %11 = load float, float* %dither_level.addr, align 4, !tbaa !55
  %12 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %dither_level10 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %12, i32 0, i32 8
  store float %11, float* %dither_level10, align 8, !tbaa !109
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.then8, %if.then
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

; Function Attrs: nounwind
define hidden double @liq_get_output_gamma(%struct.liq_result* nonnull %result) #4 {
entry:
  %retval = alloca double, align 8
  %result.addr = alloca %struct.liq_result*, align 4
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store double -1.000000e+00, double* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_result, %struct.liq_result* %2, i32 0, i32 9
  %3 = load double, double* %gamma, align 8, !tbaa !36
  store double %3, double* %retval, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load double, double* %retval, align 8
  ret double %4
}

; Function Attrs: nounwind
define hidden void @liq_result_destroy(%struct.liq_result* nonnull %res) #4 {
entry:
  %res.addr = alloca %struct.liq_result*, align 4
  store %struct.liq_result* %res, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %int_palette = getelementptr inbounds %struct.liq_result, %struct.liq_result* %2, i32 0, i32 7
  %3 = bitcast %struct.liq_palette* %int_palette to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 1028, i1 false)
  %4 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping = getelementptr inbounds %struct.liq_result, %struct.liq_result* %4, i32 0, i32 3
  %5 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping, align 4, !tbaa !33
  %tobool = icmp ne %struct.liq_remapping_result* %5, null
  br i1 %tobool, label %if.then1, label %if.end5

if.then1:                                         ; preds = %if.end
  %6 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping2 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %6, i32 0, i32 3
  %7 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping2, align 4, !tbaa !33
  %int_palette3 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %7, i32 0, i32 7
  %8 = bitcast %struct.liq_palette* %int_palette3 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %8, i8 0, i32 1028, i1 false)
  %9 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %remapping4 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %9, i32 0, i32 3
  %10 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping4, align 4, !tbaa !33
  call void @liq_remapping_result_destroy(%struct.liq_remapping_result* %10)
  br label %if.end5

if.end5:                                          ; preds = %if.then1, %if.end
  %11 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.liq_result, %struct.liq_result* %11, i32 0, i32 4
  %12 = load %struct.colormap*, %struct.colormap** %palette, align 8, !tbaa !110
  call void @pam_freecolormap(%struct.colormap* %12)
  %13 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %magic_header = getelementptr inbounds %struct.liq_result, %struct.liq_result* %13, i32 0, i32 0
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @liq_freed_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !111
  %14 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_result, %struct.liq_result* %14, i32 0, i32 2
  %15 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !112
  %16 = load %struct.liq_result*, %struct.liq_result** %res.addr, align 4, !tbaa !2
  %17 = bitcast %struct.liq_result* %16 to i8*
  call void %15(i8* %17)
  br label %return

return:                                           ; preds = %if.end5, %if.then
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #6

declare hidden void @pam_freecolormap(%struct.colormap*) #1

; Function Attrs: nounwind
define hidden double @liq_get_quantization_error(%struct.liq_result* nonnull %result) #4 {
entry:
  %retval = alloca double, align 8
  %result.addr = alloca %struct.liq_result*, align 4
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store double -1.000000e+00, double* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %palette_error = getelementptr inbounds %struct.liq_result, %struct.liq_result* %2, i32 0, i32 10
  %3 = load double, double* %palette_error, align 8, !tbaa !113
  %cmp = fcmp oge double %3, 0.000000e+00
  br i1 %cmp, label %if.then1, label %if.end4

if.then1:                                         ; preds = %if.end
  %4 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %palette_error2 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %4, i32 0, i32 10
  %5 = load double, double* %palette_error2, align 8, !tbaa !113
  %call3 = call double @mse_to_standard_mse(double %5)
  store double %call3, double* %retval, align 8
  br label %return

if.end4:                                          ; preds = %if.end
  store double -1.000000e+00, double* %retval, align 8
  br label %return

return:                                           ; preds = %if.end4, %if.then1, %if.then
  %6 = load double, double* %retval, align 8
  ret double %6
}

; Function Attrs: nounwind
define internal double @mse_to_standard_mse(double %mse) #4 {
entry:
  %mse.addr = alloca double, align 8
  store double %mse, double* %mse.addr, align 8, !tbaa !18
  %0 = load double, double* %mse.addr, align 8, !tbaa !18
  %mul = fmul double %0, 6.553600e+04
  %div = fdiv double %mul, 6.000000e+00
  ret double %div
}

; Function Attrs: nounwind
define hidden double @liq_get_remapping_error(%struct.liq_result* nonnull %result) #4 {
entry:
  %retval = alloca double, align 8
  %result.addr = alloca %struct.liq_result*, align 4
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store double -1.000000e+00, double* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping = getelementptr inbounds %struct.liq_result, %struct.liq_result* %2, i32 0, i32 3
  %3 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping, align 4, !tbaa !33
  %tobool = icmp ne %struct.liq_remapping_result* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end6

land.lhs.true:                                    ; preds = %if.end
  %4 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping1 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %4, i32 0, i32 3
  %5 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping1, align 4, !tbaa !33
  %palette_error = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %5, i32 0, i32 9
  %6 = load double, double* %palette_error, align 8, !tbaa !114
  %cmp = fcmp oge double %6, 0.000000e+00
  br i1 %cmp, label %if.then2, label %if.end6

if.then2:                                         ; preds = %land.lhs.true
  %7 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping3 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %7, i32 0, i32 3
  %8 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping3, align 4, !tbaa !33
  %palette_error4 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %8, i32 0, i32 9
  %9 = load double, double* %palette_error4, align 8, !tbaa !114
  %call5 = call double @mse_to_standard_mse(double %9)
  store double %call5, double* %retval, align 8
  br label %return

if.end6:                                          ; preds = %land.lhs.true, %if.end
  store double -1.000000e+00, double* %retval, align 8
  br label %return

return:                                           ; preds = %if.end6, %if.then2, %if.then
  %10 = load double, double* %retval, align 8
  ret double %10
}

; Function Attrs: nounwind
define hidden i32 @liq_get_quantization_quality(%struct.liq_result* nonnull %result) #4 {
entry:
  %retval = alloca i32, align 4
  %result.addr = alloca %struct.liq_result*, align 4
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %palette_error = getelementptr inbounds %struct.liq_result, %struct.liq_result* %2, i32 0, i32 10
  %3 = load double, double* %palette_error, align 8, !tbaa !113
  %cmp = fcmp oge double %3, 0.000000e+00
  br i1 %cmp, label %if.then1, label %if.end4

if.then1:                                         ; preds = %if.end
  %4 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %palette_error2 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %4, i32 0, i32 10
  %5 = load double, double* %palette_error2, align 8, !tbaa !113
  %call3 = call i32 @mse_to_quality(double %5)
  store i32 %call3, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then1, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: nounwind
define hidden i32 @liq_get_remapping_quality(%struct.liq_result* nonnull %result) #4 {
entry:
  %retval = alloca i32, align 4
  %result.addr = alloca %struct.liq_result*, align 4
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping = getelementptr inbounds %struct.liq_result, %struct.liq_result* %2, i32 0, i32 3
  %3 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping, align 4, !tbaa !33
  %tobool = icmp ne %struct.liq_remapping_result* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end6

land.lhs.true:                                    ; preds = %if.end
  %4 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping1 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %4, i32 0, i32 3
  %5 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping1, align 4, !tbaa !33
  %palette_error = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %5, i32 0, i32 9
  %6 = load double, double* %palette_error, align 8, !tbaa !114
  %cmp = fcmp oge double %6, 0.000000e+00
  br i1 %cmp, label %if.then2, label %if.end6

if.then2:                                         ; preds = %land.lhs.true
  %7 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping3 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %7, i32 0, i32 3
  %8 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping3, align 4, !tbaa !33
  %palette_error4 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %8, i32 0, i32 9
  %9 = load double, double* %palette_error4, align 8, !tbaa !114
  %call5 = call i32 @mse_to_quality(double %9)
  store i32 %call5, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %land.lhs.true, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then2, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: nounwind
define hidden %struct.liq_palette* @liq_get_palette(%struct.liq_result* nonnull %result) #4 {
entry:
  %retval = alloca %struct.liq_palette*, align 4
  %result.addr = alloca %struct.liq_result*, align 4
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.liq_palette* null, %struct.liq_palette** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping = getelementptr inbounds %struct.liq_result, %struct.liq_result* %2, i32 0, i32 3
  %3 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping, align 4, !tbaa !33
  %tobool = icmp ne %struct.liq_remapping_result* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end6

land.lhs.true:                                    ; preds = %if.end
  %4 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping1 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %4, i32 0, i32 3
  %5 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping1, align 4, !tbaa !33
  %int_palette = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %5, i32 0, i32 7
  %count = getelementptr inbounds %struct.liq_palette, %struct.liq_palette* %int_palette, i32 0, i32 0
  %6 = load i32, i32* %count, align 4, !tbaa !115
  %tobool2 = icmp ne i32 %6, 0
  br i1 %tobool2, label %if.then3, label %if.end6

if.then3:                                         ; preds = %land.lhs.true
  %7 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %remapping4 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %7, i32 0, i32 3
  %8 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping4, align 4, !tbaa !33
  %int_palette5 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %8, i32 0, i32 7
  store %struct.liq_palette* %int_palette5, %struct.liq_palette** %retval, align 4
  br label %return

if.end6:                                          ; preds = %land.lhs.true, %if.end
  %9 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %int_palette7 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %9, i32 0, i32 7
  %count8 = getelementptr inbounds %struct.liq_palette, %struct.liq_palette* %int_palette7, i32 0, i32 0
  %10 = load i32, i32* %count8, align 4, !tbaa !116
  %tobool9 = icmp ne i32 %10, 0
  br i1 %tobool9, label %if.end12, label %if.then10

if.then10:                                        ; preds = %if.end6
  %11 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %int_palette11 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %11, i32 0, i32 7
  %12 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.liq_result, %struct.liq_result* %12, i32 0, i32 4
  %13 = load %struct.colormap*, %struct.colormap** %palette, align 8, !tbaa !110
  %14 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_result, %struct.liq_result* %14, i32 0, i32 9
  %15 = load double, double* %gamma, align 8, !tbaa !36
  %16 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %min_posterization_output = getelementptr inbounds %struct.liq_result, %struct.liq_result* %16, i32 0, i32 11
  %17 = load i32, i32* %min_posterization_output, align 8, !tbaa !117
  call void @set_rounded_palette(%struct.liq_palette* %int_palette11, %struct.colormap* %13, double %15, i32 %17)
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.end6
  %18 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %int_palette13 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %18, i32 0, i32 7
  store %struct.liq_palette* %int_palette13, %struct.liq_palette** %retval, align 4
  br label %return

return:                                           ; preds = %if.end12, %if.then3, %if.then
  %19 = load %struct.liq_palette*, %struct.liq_palette** %retval, align 4
  ret %struct.liq_palette* %19
}

; Function Attrs: nounwind
define internal void @set_rounded_palette(%struct.liq_palette* nonnull %dest, %struct.colormap* nonnull %map, double %gamma, i32 %posterize) #4 {
entry:
  %dest.addr = alloca %struct.liq_palette*, align 4
  %map.addr = alloca %struct.colormap*, align 4
  %gamma.addr = alloca double, align 8
  %posterize.addr = alloca i32, align 4
  %gamma_lut = alloca [256 x float], align 16
  %x = alloca i32, align 4
  %px = alloca %struct.rgba_pixel, align 1
  %tmp = alloca %struct.f_pixel, align 4
  %.compoundliteral = alloca %struct.liq_color, align 1
  store %struct.liq_palette* %dest, %struct.liq_palette** %dest.addr, align 4, !tbaa !2
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  store i32 %posterize, i32* %posterize.addr, align 4, !tbaa !13
  %0 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %0) #8
  %arraydecay = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %1 = load double, double* %gamma.addr, align 8, !tbaa !18
  call void @to_f_set_gamma(float* %arraydecay, double %1)
  %2 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %2, i32 0, i32 0
  %3 = load i32, i32* %colors, align 4, !tbaa !13
  %4 = load %struct.liq_palette*, %struct.liq_palette** %dest.addr, align 4, !tbaa !2
  %count = getelementptr inbounds %struct.liq_palette, %struct.liq_palette* %4, i32 0, i32 0
  store i32 %3, i32* %count, align 4, !tbaa !118
  %5 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store i32 0, i32* %x, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %x, align 4, !tbaa !13
  %7 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors1 = getelementptr inbounds %struct.colormap, %struct.colormap* %7, i32 0, i32 0
  %8 = load i32, i32* %colors1, align 4, !tbaa !13
  %cmp = icmp ult i32 %6, %8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %9 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %struct.rgba_pixel* %px to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load double, double* %gamma.addr, align 8, !tbaa !18
  %conv = fptrunc double %11 to float
  %12 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %12, i32 0, i32 3
  %13 = load i32, i32* %x, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 %13
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx, i32 0, i32 0
  call void @f_to_rgb(%struct.rgba_pixel* sret align 1 %px, float %conv, %struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor)
  %r = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 0
  %14 = load i8, i8* %r, align 1, !tbaa !63
  %conv2 = zext i8 %14 to i32
  %15 = load i32, i32* %posterize.addr, align 4, !tbaa !13
  %call = call i32 @posterize_channel(i32 %conv2, i32 %15)
  %conv3 = trunc i32 %call to i8
  %r4 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 0
  store i8 %conv3, i8* %r4, align 1, !tbaa !63
  %g = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 1
  %16 = load i8, i8* %g, align 1, !tbaa !66
  %conv5 = zext i8 %16 to i32
  %17 = load i32, i32* %posterize.addr, align 4, !tbaa !13
  %call6 = call i32 @posterize_channel(i32 %conv5, i32 %17)
  %conv7 = trunc i32 %call6 to i8
  %g8 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 1
  store i8 %conv7, i8* %g8, align 1, !tbaa !66
  %b = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 2
  %18 = load i8, i8* %b, align 1, !tbaa !68
  %conv9 = zext i8 %18 to i32
  %19 = load i32, i32* %posterize.addr, align 4, !tbaa !13
  %call10 = call i32 @posterize_channel(i32 %conv9, i32 %19)
  %conv11 = trunc i32 %call10 to i8
  %b12 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 2
  store i8 %conv11, i8* %b12, align 1, !tbaa !68
  %a = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 3
  %20 = load i8, i8* %a, align 1, !tbaa !70
  %conv13 = zext i8 %20 to i32
  %21 = load i32, i32* %posterize.addr, align 4, !tbaa !13
  %call14 = call i32 @posterize_channel(i32 %conv13, i32 %21)
  %conv15 = trunc i32 %call14 to i8
  %a16 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 3
  store i8 %conv15, i8* %a16, align 1, !tbaa !70
  %22 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette17 = getelementptr inbounds %struct.colormap, %struct.colormap* %22, i32 0, i32 3
  %23 = load i32, i32* %x, align 4, !tbaa !13
  %arrayidx18 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette17, i32 0, i32 %23
  %acolor19 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx18, i32 0, i32 0
  %24 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #8
  %arraydecay20 = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  call void @rgba_to_f(%struct.f_pixel* sret align 4 %tmp, float* %arraydecay20, %struct.rgba_pixel* byval(%struct.rgba_pixel) align 1 %px)
  %25 = bitcast %struct.f_pixel* %acolor19 to i8*
  %26 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false), !tbaa.struct !71
  %27 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #8
  %a21 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 3
  %28 = load i8, i8* %a21, align 1, !tbaa !70
  %tobool = icmp ne i8 %28, 0
  br i1 %tobool, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body
  %29 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette22 = getelementptr inbounds %struct.colormap, %struct.colormap* %29, i32 0, i32 3
  %30 = load i32, i32* %x, align 4, !tbaa !13
  %arrayidx23 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette22, i32 0, i32 %30
  %fixed = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx23, i32 0, i32 2
  %31 = load i8, i8* %fixed, align 4, !tbaa !119, !range !29
  %tobool24 = trunc i8 %31 to i1
  br i1 %tobool24, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %r25 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 0
  store i8 71, i8* %r25, align 1, !tbaa !63
  %g26 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 1
  store i8 112, i8* %g26, align 1, !tbaa !66
  %b27 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 2
  store i8 76, i8* %b27, align 1, !tbaa !68
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  %32 = load %struct.liq_palette*, %struct.liq_palette** %dest.addr, align 4, !tbaa !2
  %entries = getelementptr inbounds %struct.liq_palette, %struct.liq_palette* %32, i32 0, i32 1
  %33 = load i32, i32* %x, align 4, !tbaa !13
  %arrayidx28 = getelementptr inbounds [256 x %struct.liq_color], [256 x %struct.liq_color]* %entries, i32 0, i32 %33
  %r29 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %.compoundliteral, i32 0, i32 0
  %r30 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 0
  %34 = load i8, i8* %r30, align 1, !tbaa !63
  store i8 %34, i8* %r29, align 1, !tbaa !61
  %g31 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %.compoundliteral, i32 0, i32 1
  %g32 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 1
  %35 = load i8, i8* %g32, align 1, !tbaa !66
  store i8 %35, i8* %g31, align 1, !tbaa !65
  %b33 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %.compoundliteral, i32 0, i32 2
  %b34 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 2
  %36 = load i8, i8* %b34, align 1, !tbaa !68
  store i8 %36, i8* %b33, align 1, !tbaa !67
  %a35 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %.compoundliteral, i32 0, i32 3
  %a36 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 3
  %37 = load i8, i8* %a36, align 1, !tbaa !70
  store i8 %37, i8* %a35, align 1, !tbaa !69
  %38 = bitcast %struct.liq_color* %arrayidx28 to i8*
  %39 = bitcast %struct.liq_color* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 1 %39, i32 4, i1 false), !tbaa.struct !121
  %40 = bitcast %struct.rgba_pixel* %px to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %41 = load i32, i32* %x, align 4, !tbaa !13
  %inc = add i32 %41, 1
  store i32 %inc, i32* %x, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %42 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %42) #8
  ret void
}

; Function Attrs: nounwind
define hidden i32 @liq_histogram_add_colors(%struct.liq_histogram* nonnull %input_hist, %struct.liq_attr* nonnull %options, %struct.liq_histogram_entry* nonnull %entries, i32 %num_entries, double %gamma) #4 {
entry:
  %retval = alloca i32, align 4
  %input_hist.addr = alloca %struct.liq_histogram*, align 4
  %options.addr = alloca %struct.liq_attr*, align 4
  %entries.addr = alloca %struct.liq_histogram_entry*, align 4
  %num_entries.addr = alloca i32, align 4
  %gamma.addr = alloca double, align 8
  %hash_size = alloca i32, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %rgba = alloca %struct.rgba_pixel, align 1
  %px = alloca %union.rgba_as_int, align 4
  %hash = alloca i32, align 4
  store %struct.liq_histogram* %input_hist, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  store %struct.liq_attr* %options, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  store %struct.liq_histogram_entry* %entries, %struct.liq_histogram_entry** %entries.addr, align 4, !tbaa !2
  store i32 %num_entries, i32* %num_entries.addr, align 4, !tbaa !13
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  %0 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %0, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @liq_attr_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %2 = bitcast %struct.liq_histogram* %1 to %struct.liq_attr*
  %call1 = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %2, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @liq_histogram_magic, i32 0, i32 0))
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 105, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load %struct.liq_histogram_entry*, %struct.liq_histogram_entry** %entries.addr, align 4, !tbaa !2
  %4 = bitcast %struct.liq_histogram_entry* %3 to i8*
  %call4 = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %4)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end3
  store i32 105, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %5 = load double, double* %gamma.addr, align 8, !tbaa !18
  %cmp = fcmp olt double %5, 0.000000e+00
  br i1 %cmp, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end6
  %6 = load double, double* %gamma.addr, align 8, !tbaa !18
  %cmp7 = fcmp oge double %6, 1.000000e+00
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %lor.lhs.false, %if.end6
  store i32 100, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %lor.lhs.false
  %7 = load i32, i32* %num_entries.addr, align 4, !tbaa !13
  %cmp10 = icmp sle i32 %7, 0
  br i1 %cmp10, label %if.then13, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %if.end9
  %8 = load i32, i32* %num_entries.addr, align 4, !tbaa !13
  %cmp12 = icmp sgt i32 %8, 1073741824
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %lor.lhs.false11, %if.end9
  store i32 100, i32* %retval, align 4
  br label %return

if.end14:                                         ; preds = %lor.lhs.false11
  %9 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %ignorebits = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %9, i32 0, i32 7
  %10 = load i16, i16* %ignorebits, align 2, !tbaa !104
  %conv = zext i16 %10 to i32
  %cmp15 = icmp sgt i32 %conv, 0
  br i1 %cmp15, label %land.lhs.true, label %if.end19

land.lhs.true:                                    ; preds = %if.end14
  %11 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %had_image_added = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %11, i32 0, i32 8
  %12 = load i8, i8* %had_image_added, align 4, !tbaa !108, !range !29
  %tobool = trunc i8 %12 to i1
  br i1 %tobool, label %if.then18, label %if.end19

if.then18:                                        ; preds = %land.lhs.true
  store i32 106, i32* %retval, align 4
  br label %return

if.end19:                                         ; preds = %land.lhs.true, %if.end14
  %13 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %ignorebits20 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %13, i32 0, i32 7
  store i16 0, i16* %ignorebits20, align 2, !tbaa !104
  %14 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %had_image_added21 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %14, i32 0, i32 8
  store i8 1, i8* %had_image_added21, align 4, !tbaa !108
  %15 = load double, double* %gamma.addr, align 8, !tbaa !18
  %tobool22 = fcmp une double %15, 0.000000e+00
  br i1 %tobool22, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end19
  %16 = load double, double* %gamma.addr, align 8, !tbaa !18
  br label %cond.end

cond.false:                                       ; preds = %if.end19
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %16, %cond.true ], [ 4.545500e-01, %cond.false ]
  %17 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %gamma23 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %17, i32 0, i32 4
  store double %cond, double* %gamma23, align 8, !tbaa !107
  %18 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %18, i32 0, i32 3
  %19 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht, align 4, !tbaa !106
  %tobool24 = icmp ne %struct.acolorhash_table* %19, null
  br i1 %tobool24, label %if.end32, label %if.then25

if.then25:                                        ; preds = %cond.end
  %20 = load i32, i32* %num_entries.addr, align 4, !tbaa !13
  %21 = load i32, i32* %num_entries.addr, align 4, !tbaa !13
  %mul = mul nsw i32 %20, %21
  %22 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %22, i32 0, i32 1
  %23 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %24 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %24, i32 0, i32 2
  %25 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !53
  %call26 = call %struct.acolorhash_table* @pam_allocacolorhash(i32 -1, i32 %mul, i32 0, i8* (i32)* %23, void (i8*)* %25)
  %26 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht27 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %26, i32 0, i32 3
  store %struct.acolorhash_table* %call26, %struct.acolorhash_table** %acht27, align 4, !tbaa !106
  %27 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht28 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %27, i32 0, i32 3
  %28 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht28, align 4, !tbaa !106
  %tobool29 = icmp ne %struct.acolorhash_table* %28, null
  br i1 %tobool29, label %if.end31, label %if.then30

if.then30:                                        ; preds = %if.then25
  store i32 101, i32* %retval, align 4
  br label %return

if.end31:                                         ; preds = %if.then25
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %cond.end
  %29 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht33 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %29, i32 0, i32 3
  %30 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht33, align 4, !tbaa !106
  %cols = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %30, i32 0, i32 4
  %31 = load i32, i32* %cols, align 4, !tbaa !13
  %tobool34 = icmp ne i32 %31, 0
  br i1 %tobool34, label %if.end38, label %if.then35

if.then35:                                        ; preds = %if.end32
  %32 = load i32, i32* %num_entries.addr, align 4, !tbaa !13
  %33 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht36 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %33, i32 0, i32 3
  %34 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht36, align 4, !tbaa !106
  %cols37 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %34, i32 0, i32 4
  store i32 %32, i32* %cols37, align 4, !tbaa !13
  br label %if.end38

if.end38:                                         ; preds = %if.then35, %if.end32
  %35 = load i32, i32* %num_entries.addr, align 4, !tbaa !13
  %36 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht39 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %36, i32 0, i32 3
  %37 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht39, align 4, !tbaa !106
  %rows = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %37, i32 0, i32 5
  %38 = load i32, i32* %rows, align 4, !tbaa !13
  %add = add i32 %38, %35
  store i32 %add, i32* %rows, align 4, !tbaa !13
  %39 = bitcast i32* %hash_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht40 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %40, i32 0, i32 3
  %41 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht40, align 4, !tbaa !106
  %hash_size41 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %41, i32 0, i32 6
  %42 = load i32, i32* %hash_size41, align 4, !tbaa !13
  store i32 %42, i32* %hash_size, align 4, !tbaa !13
  %43 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end38
  %44 = load i32, i32* %i, align 4, !tbaa !13
  %45 = load i32, i32* %num_entries.addr, align 4, !tbaa !13
  %cmp42 = icmp slt i32 %44, %45
  br i1 %cmp42, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup68

for.body:                                         ; preds = %for.cond
  %46 = bitcast %struct.rgba_pixel* %rgba to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %r = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %rgba, i32 0, i32 0
  %47 = load %struct.liq_histogram_entry*, %struct.liq_histogram_entry** %entries.addr, align 4, !tbaa !2
  %48 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %47, i32 %48
  %color = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %arrayidx, i32 0, i32 0
  %r44 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 0
  %49 = load i8, i8* %r44, align 4, !tbaa !122
  store i8 %49, i8* %r, align 1, !tbaa !63
  %g = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %rgba, i32 0, i32 1
  %50 = load %struct.liq_histogram_entry*, %struct.liq_histogram_entry** %entries.addr, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx45 = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %50, i32 %51
  %color46 = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %arrayidx45, i32 0, i32 0
  %g47 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color46, i32 0, i32 1
  %52 = load i8, i8* %g47, align 1, !tbaa !124
  store i8 %52, i8* %g, align 1, !tbaa !66
  %b = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %rgba, i32 0, i32 2
  %53 = load %struct.liq_histogram_entry*, %struct.liq_histogram_entry** %entries.addr, align 4, !tbaa !2
  %54 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx48 = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %53, i32 %54
  %color49 = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %arrayidx48, i32 0, i32 0
  %b50 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color49, i32 0, i32 2
  %55 = load i8, i8* %b50, align 2, !tbaa !125
  store i8 %55, i8* %b, align 1, !tbaa !68
  %a = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %rgba, i32 0, i32 3
  %56 = load %struct.liq_histogram_entry*, %struct.liq_histogram_entry** %entries.addr, align 4, !tbaa !2
  %57 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx51 = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %56, i32 %57
  %color52 = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %arrayidx51, i32 0, i32 0
  %a53 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color52, i32 0, i32 3
  %58 = load i8, i8* %a53, align 1, !tbaa !126
  store i8 %58, i8* %a, align 1, !tbaa !70
  %59 = bitcast %union.rgba_as_int* %px to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #8
  %rgba54 = bitcast %union.rgba_as_int* %px to %struct.rgba_pixel*
  %60 = bitcast %struct.rgba_pixel* %rgba54 to i8*
  %61 = bitcast %struct.rgba_pixel* %rgba to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 1 %61, i32 4, i1 false), !tbaa.struct !121
  %62 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #8
  %rgba55 = bitcast %union.rgba_as_int* %px to %struct.rgba_pixel*
  %a56 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %rgba55, i32 0, i32 3
  %63 = load i8, i8* %a56, align 1, !tbaa !12
  %tobool57 = icmp ne i8 %63, 0
  br i1 %tobool57, label %if.then58, label %if.else

if.then58:                                        ; preds = %for.body
  %l = bitcast %union.rgba_as_int* %px to i32*
  %64 = load i32, i32* %l, align 4, !tbaa !12
  %65 = load i32, i32* %hash_size, align 4, !tbaa !13
  %rem = urem i32 %64, %65
  store i32 %rem, i32* %hash, align 4, !tbaa !13
  br label %if.end60

if.else:                                          ; preds = %for.body
  store i32 0, i32* %hash, align 4, !tbaa !13
  %l59 = bitcast %union.rgba_as_int* %px to i32*
  store i32 0, i32* %l59, align 4, !tbaa !12
  br label %if.end60

if.end60:                                         ; preds = %if.else, %if.then58
  %66 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht61 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %66, i32 0, i32 3
  %67 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht61, align 4, !tbaa !106
  %68 = load i32, i32* %hash, align 4, !tbaa !13
  %69 = load %struct.liq_histogram_entry*, %struct.liq_histogram_entry** %entries.addr, align 4, !tbaa !2
  %70 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx62 = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %69, i32 %70
  %count = getelementptr inbounds %struct.liq_histogram_entry, %struct.liq_histogram_entry* %arrayidx62, i32 0, i32 1
  %71 = load i32, i32* %count, align 4, !tbaa !127
  %72 = load i32, i32* %i, align 4, !tbaa !13
  %73 = load i32, i32* %num_entries.addr, align 4, !tbaa !13
  %call63 = call zeroext i1 @pam_add_to_hash(%struct.acolorhash_table* %67, i32 %68, i32 %71, %union.rgba_as_int* byval(%union.rgba_as_int) align 4 %px, i32 %72, i32 %73)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %if.end60
  store i32 101, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end65:                                         ; preds = %if.end60
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end65, %if.then64
  %74 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  %75 = bitcast %union.rgba_as_int* %px to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  %76 = bitcast %struct.rgba_pixel* %rgba to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup68 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %77 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add nsw i32 %77, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

cleanup68:                                        ; preds = %cleanup, %for.cond.cleanup
  %78 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  %cleanup.dest69 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest69, label %cleanup70 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup68
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup70

cleanup70:                                        ; preds = %for.end, %cleanup68
  %79 = bitcast i32* %hash_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #8
  br label %return

return:                                           ; preds = %cleanup70, %if.then30, %if.then18, %if.then13, %if.then8, %if.then5, %if.then2, %if.then
  %80 = load i32, i32* %retval, align 4
  ret i32 %80
}

declare hidden %struct.acolorhash_table* @pam_allocacolorhash(i32, i32, i32, i8* (i32)*, void (i8*)*) #1

declare hidden zeroext i1 @pam_add_to_hash(%struct.acolorhash_table*, i32, i32, %union.rgba_as_int* byval(%union.rgba_as_int) align 4, i32, i32) #1

; Function Attrs: nounwind
define internal void @contrast_maps(%struct.liq_image* nonnull %image) #4 {
entry:
  %image.addr = alloca %struct.liq_image*, align 4
  %cols = alloca i32, align 4
  %rows = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %noise = alloca i8*, align 4
  %edges = alloca i8*, align 4
  %tmp = alloca i8*, align 4
  %curr_row = alloca %struct.f_pixel*, align 4
  %prev_row = alloca %struct.f_pixel*, align 4
  %next_row = alloca %struct.f_pixel*, align 4
  %j = alloca i32, align 4
  %prev = alloca %struct.f_pixel, align 4
  %curr = alloca %struct.f_pixel, align 4
  %next = alloca %struct.f_pixel, align 4
  %i = alloca i32, align 4
  %a = alloca float, align 4
  %r = alloca float, align 4
  %g = alloca float, align 4
  %b = alloca float, align 4
  %prevl = alloca %struct.f_pixel, align 4
  %nextl = alloca %struct.f_pixel, align 4
  %a1 = alloca float, align 4
  %r1 = alloca float, align 4
  %g1 = alloca float, align 4
  %b1 = alloca float, align 4
  %horiz = alloca float, align 4
  %vert = alloca float, align 4
  %edge = alloca float, align 4
  %z = alloca float, align 4
  %z_int = alloca i32, align 4
  %e_int = alloca i32, align 4
  %i217 = alloca i32, align 4
  store %struct.liq_image* %image, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %0 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %1, i32 0, i32 6
  %2 = load i32, i32* %width, align 8, !tbaa !85
  store i32 %2, i32* %cols, align 4, !tbaa !13
  %3 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 7
  %5 = load i32, i32* %height, align 4, !tbaa !84
  store i32 %5, i32* %rows, align 4, !tbaa !13
  %6 = load i32, i32* %cols, align 4, !tbaa !13
  %cmp = icmp ult i32 %6, 4
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %7 = load i32, i32* %rows, align 4, !tbaa !13
  %cmp1 = icmp ult i32 %7, 4
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %8 = load i32, i32* %cols, align 4, !tbaa !13
  %mul = mul i32 3, %8
  %9 = load i32, i32* %rows, align 4, !tbaa !13
  %mul3 = mul i32 %mul, %9
  %cmp4 = icmp ugt i32 %mul3, 67108864
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup248

if.end:                                           ; preds = %lor.lhs.false2
  %10 = bitcast i8** %noise to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %importance_map = getelementptr inbounds %struct.liq_image, %struct.liq_image* %11, i32 0, i32 8
  %12 = load i8*, i8** %importance_map, align 8, !tbaa !87
  %tobool = icmp ne i8* %12, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %13 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %importance_map5 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %13, i32 0, i32 8
  %14 = load i8*, i8** %importance_map5, align 8, !tbaa !87
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %15 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_image, %struct.liq_image* %15, i32 0, i32 1
  %16 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !86
  %17 = load i32, i32* %cols, align 4, !tbaa !13
  %18 = load i32, i32* %rows, align 4, !tbaa !13
  %mul6 = mul i32 %17, %18
  %call = call i8* %16(i32 %mul6)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %14, %cond.true ], [ %call, %cond.false ]
  store i8* %cond, i8** %noise, align 4, !tbaa !2
  %19 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %importance_map7 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %19, i32 0, i32 8
  store i8* null, i8** %importance_map7, align 8, !tbaa !87
  %20 = bitcast i8** %edges to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %21 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %edges8 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %21, i32 0, i32 9
  %22 = load i8*, i8** %edges8, align 4, !tbaa !94
  %tobool9 = icmp ne i8* %22, null
  br i1 %tobool9, label %cond.true10, label %cond.false12

cond.true10:                                      ; preds = %cond.end
  %23 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %edges11 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %23, i32 0, i32 9
  %24 = load i8*, i8** %edges11, align 4, !tbaa !94
  br label %cond.end16

cond.false12:                                     ; preds = %cond.end
  %25 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %malloc13 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %25, i32 0, i32 1
  %26 = load i8* (i32)*, i8* (i32)** %malloc13, align 4, !tbaa !86
  %27 = load i32, i32* %cols, align 4, !tbaa !13
  %28 = load i32, i32* %rows, align 4, !tbaa !13
  %mul14 = mul i32 %27, %28
  %call15 = call i8* %26(i32 %mul14)
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false12, %cond.true10
  %cond17 = phi i8* [ %24, %cond.true10 ], [ %call15, %cond.false12 ]
  store i8* %cond17, i8** %edges, align 4, !tbaa !2
  %29 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %edges18 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %29, i32 0, i32 9
  store i8* null, i8** %edges18, align 4, !tbaa !94
  %30 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %31 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %malloc19 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %31, i32 0, i32 1
  %32 = load i8* (i32)*, i8* (i32)** %malloc19, align 4, !tbaa !86
  %33 = load i32, i32* %cols, align 4, !tbaa !13
  %34 = load i32, i32* %rows, align 4, !tbaa !13
  %mul20 = mul i32 %33, %34
  %call21 = call i8* %32(i32 %mul20)
  store i8* %call21, i8** %tmp, align 4, !tbaa !2
  %35 = load i8*, i8** %noise, align 4, !tbaa !2
  %tobool22 = icmp ne i8* %35, null
  br i1 %tobool22, label %lor.lhs.false23, label %if.then29

lor.lhs.false23:                                  ; preds = %cond.end16
  %36 = load i8*, i8** %edges, align 4, !tbaa !2
  %tobool24 = icmp ne i8* %36, null
  br i1 %tobool24, label %lor.lhs.false25, label %if.then29

lor.lhs.false25:                                  ; preds = %lor.lhs.false23
  %37 = load i8*, i8** %tmp, align 4, !tbaa !2
  %tobool26 = icmp ne i8* %37, null
  br i1 %tobool26, label %lor.lhs.false27, label %if.then29

lor.lhs.false27:                                  ; preds = %lor.lhs.false25
  %38 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %call28 = call zeroext i1 @liq_image_get_row_f_init(%struct.liq_image* %38)
  br i1 %call28, label %if.end32, label %if.then29

if.then29:                                        ; preds = %lor.lhs.false27, %lor.lhs.false25, %lor.lhs.false23, %cond.end16
  %39 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_image, %struct.liq_image* %39, i32 0, i32 2
  %40 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !88
  %41 = load i8*, i8** %noise, align 4, !tbaa !2
  call void %40(i8* %41)
  %42 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %free30 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %42, i32 0, i32 2
  %43 = load void (i8*)*, void (i8*)** %free30, align 8, !tbaa !88
  %44 = load i8*, i8** %edges, align 4, !tbaa !2
  call void %43(i8* %44)
  %45 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %free31 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %45, i32 0, i32 2
  %46 = load void (i8*)*, void (i8*)** %free31, align 8, !tbaa !88
  %47 = load i8*, i8** %tmp, align 4, !tbaa !2
  call void %46(i8* %47)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end32:                                         ; preds = %lor.lhs.false27
  %48 = bitcast %struct.f_pixel** %curr_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #8
  %49 = bitcast %struct.f_pixel** %prev_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #8
  %50 = bitcast %struct.f_pixel** %next_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  %51 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %call33 = call %struct.f_pixel* @liq_image_get_row_f(%struct.liq_image* %51, i32 0)
  store %struct.f_pixel* %call33, %struct.f_pixel** %next_row, align 4, !tbaa !2
  store %struct.f_pixel* %call33, %struct.f_pixel** %prev_row, align 4, !tbaa !2
  store %struct.f_pixel* %call33, %struct.f_pixel** %curr_row, align 4, !tbaa !2
  %52 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #8
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc214, %if.end32
  %53 = load i32, i32* %j, align 4, !tbaa !13
  %54 = load i32, i32* %rows, align 4, !tbaa !13
  %cmp34 = icmp ult i32 %53, %54
  br i1 %cmp34, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %55 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #8
  br label %for.end216

for.body:                                         ; preds = %for.cond
  %56 = load %struct.f_pixel*, %struct.f_pixel** %curr_row, align 4, !tbaa !2
  store %struct.f_pixel* %56, %struct.f_pixel** %prev_row, align 4, !tbaa !2
  %57 = load %struct.f_pixel*, %struct.f_pixel** %next_row, align 4, !tbaa !2
  store %struct.f_pixel* %57, %struct.f_pixel** %curr_row, align 4, !tbaa !2
  %58 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %59 = load i32, i32* %rows, align 4, !tbaa !13
  %sub = sub i32 %59, 1
  %60 = load i32, i32* %j, align 4, !tbaa !13
  %add = add i32 %60, 1
  %cmp35 = icmp ult i32 %sub, %add
  br i1 %cmp35, label %cond.true36, label %cond.false38

cond.true36:                                      ; preds = %for.body
  %61 = load i32, i32* %rows, align 4, !tbaa !13
  %sub37 = sub i32 %61, 1
  br label %cond.end40

cond.false38:                                     ; preds = %for.body
  %62 = load i32, i32* %j, align 4, !tbaa !13
  %add39 = add i32 %62, 1
  br label %cond.end40

cond.end40:                                       ; preds = %cond.false38, %cond.true36
  %cond41 = phi i32 [ %sub37, %cond.true36 ], [ %add39, %cond.false38 ]
  %call42 = call %struct.f_pixel* @liq_image_get_row_f(%struct.liq_image* %58, i32 %cond41)
  store %struct.f_pixel* %call42, %struct.f_pixel** %next_row, align 4, !tbaa !2
  %63 = bitcast %struct.f_pixel* %prev to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %63) #8
  %64 = bitcast %struct.f_pixel* %curr to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #8
  %65 = load %struct.f_pixel*, %struct.f_pixel** %curr_row, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %65, i32 0
  %66 = bitcast %struct.f_pixel* %curr to i8*
  %67 = bitcast %struct.f_pixel* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %66, i8* align 4 %67, i32 16, i1 false), !tbaa.struct !71
  %68 = bitcast %struct.f_pixel* %next to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %68) #8
  %69 = bitcast %struct.f_pixel* %next to i8*
  %70 = bitcast %struct.f_pixel* %curr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 16, i1 false), !tbaa.struct !71
  %71 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc, %cond.end40
  %72 = load i32, i32* %i, align 4, !tbaa !13
  %73 = load i32, i32* %cols, align 4, !tbaa !13
  %cmp44 = icmp ult i32 %72, %73
  br i1 %cmp44, label %for.body46, label %for.cond.cleanup45

for.cond.cleanup45:                               ; preds = %for.cond43
  store i32 5, i32* %cleanup.dest.slot, align 4
  %74 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  br label %for.end

for.body46:                                       ; preds = %for.cond43
  %75 = bitcast %struct.f_pixel* %prev to i8*
  %76 = bitcast %struct.f_pixel* %curr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %75, i8* align 4 %76, i32 16, i1 false), !tbaa.struct !71
  %77 = bitcast %struct.f_pixel* %curr to i8*
  %78 = bitcast %struct.f_pixel* %next to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %77, i8* align 4 %78, i32 16, i1 false), !tbaa.struct !71
  %79 = load %struct.f_pixel*, %struct.f_pixel** %curr_row, align 4, !tbaa !2
  %80 = load i32, i32* %cols, align 4, !tbaa !13
  %sub47 = sub i32 %80, 1
  %81 = load i32, i32* %i, align 4, !tbaa !13
  %add48 = add i32 %81, 1
  %cmp49 = icmp ult i32 %sub47, %add48
  br i1 %cmp49, label %cond.true50, label %cond.false52

cond.true50:                                      ; preds = %for.body46
  %82 = load i32, i32* %cols, align 4, !tbaa !13
  %sub51 = sub i32 %82, 1
  br label %cond.end54

cond.false52:                                     ; preds = %for.body46
  %83 = load i32, i32* %i, align 4, !tbaa !13
  %add53 = add i32 %83, 1
  br label %cond.end54

cond.end54:                                       ; preds = %cond.false52, %cond.true50
  %cond55 = phi i32 [ %sub51, %cond.true50 ], [ %add53, %cond.false52 ]
  %arrayidx56 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %79, i32 %cond55
  %84 = bitcast %struct.f_pixel* %next to i8*
  %85 = bitcast %struct.f_pixel* %arrayidx56 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 16, i1 false), !tbaa.struct !71
  %86 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #8
  %a57 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %prev, i32 0, i32 0
  %87 = load float, float* %a57, align 4, !tbaa !72
  %a58 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %next, i32 0, i32 0
  %88 = load float, float* %a58, align 4, !tbaa !72
  %add59 = fadd float %87, %88
  %a60 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %curr, i32 0, i32 0
  %89 = load float, float* %a60, align 4, !tbaa !72
  %mul61 = fmul float %89, 2.000000e+00
  %sub62 = fsub float %add59, %mul61
  %90 = call float @llvm.fabs.f32(float %sub62)
  store float %90, float* %a, align 4, !tbaa !55
  %91 = bitcast float* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #8
  %r63 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %prev, i32 0, i32 1
  %92 = load float, float* %r63, align 4, !tbaa !74
  %r64 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %next, i32 0, i32 1
  %93 = load float, float* %r64, align 4, !tbaa !74
  %add65 = fadd float %92, %93
  %r66 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %curr, i32 0, i32 1
  %94 = load float, float* %r66, align 4, !tbaa !74
  %mul67 = fmul float %94, 2.000000e+00
  %sub68 = fsub float %add65, %mul67
  %95 = call float @llvm.fabs.f32(float %sub68)
  store float %95, float* %r, align 4, !tbaa !55
  %96 = bitcast float* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #8
  %g69 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %prev, i32 0, i32 2
  %97 = load float, float* %g69, align 4, !tbaa !75
  %g70 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %next, i32 0, i32 2
  %98 = load float, float* %g70, align 4, !tbaa !75
  %add71 = fadd float %97, %98
  %g72 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %curr, i32 0, i32 2
  %99 = load float, float* %g72, align 4, !tbaa !75
  %mul73 = fmul float %99, 2.000000e+00
  %sub74 = fsub float %add71, %mul73
  %100 = call float @llvm.fabs.f32(float %sub74)
  store float %100, float* %g, align 4, !tbaa !55
  %101 = bitcast float* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #8
  %b75 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %prev, i32 0, i32 3
  %102 = load float, float* %b75, align 4, !tbaa !76
  %b76 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %next, i32 0, i32 3
  %103 = load float, float* %b76, align 4, !tbaa !76
  %add77 = fadd float %102, %103
  %b78 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %curr, i32 0, i32 3
  %104 = load float, float* %b78, align 4, !tbaa !76
  %mul79 = fmul float %104, 2.000000e+00
  %sub80 = fsub float %add77, %mul79
  %105 = call float @llvm.fabs.f32(float %sub80)
  store float %105, float* %b, align 4, !tbaa !55
  %106 = bitcast %struct.f_pixel* %prevl to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %106) #8
  %107 = load %struct.f_pixel*, %struct.f_pixel** %prev_row, align 4, !tbaa !2
  %108 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx81 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %107, i32 %108
  %109 = bitcast %struct.f_pixel* %prevl to i8*
  %110 = bitcast %struct.f_pixel* %arrayidx81 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %109, i8* align 4 %110, i32 16, i1 false), !tbaa.struct !71
  %111 = bitcast %struct.f_pixel* %nextl to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %111) #8
  %112 = load %struct.f_pixel*, %struct.f_pixel** %next_row, align 4, !tbaa !2
  %113 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx82 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %112, i32 %113
  %114 = bitcast %struct.f_pixel* %nextl to i8*
  %115 = bitcast %struct.f_pixel* %arrayidx82 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %114, i8* align 4 %115, i32 16, i1 false), !tbaa.struct !71
  %116 = bitcast float* %a1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #8
  %a83 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %prevl, i32 0, i32 0
  %117 = load float, float* %a83, align 4, !tbaa !72
  %a84 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %nextl, i32 0, i32 0
  %118 = load float, float* %a84, align 4, !tbaa !72
  %add85 = fadd float %117, %118
  %a86 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %curr, i32 0, i32 0
  %119 = load float, float* %a86, align 4, !tbaa !72
  %mul87 = fmul float %119, 2.000000e+00
  %sub88 = fsub float %add85, %mul87
  %120 = call float @llvm.fabs.f32(float %sub88)
  store float %120, float* %a1, align 4, !tbaa !55
  %121 = bitcast float* %r1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #8
  %r89 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %prevl, i32 0, i32 1
  %122 = load float, float* %r89, align 4, !tbaa !74
  %r90 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %nextl, i32 0, i32 1
  %123 = load float, float* %r90, align 4, !tbaa !74
  %add91 = fadd float %122, %123
  %r92 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %curr, i32 0, i32 1
  %124 = load float, float* %r92, align 4, !tbaa !74
  %mul93 = fmul float %124, 2.000000e+00
  %sub94 = fsub float %add91, %mul93
  %125 = call float @llvm.fabs.f32(float %sub94)
  store float %125, float* %r1, align 4, !tbaa !55
  %126 = bitcast float* %g1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %126) #8
  %g95 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %prevl, i32 0, i32 2
  %127 = load float, float* %g95, align 4, !tbaa !75
  %g96 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %nextl, i32 0, i32 2
  %128 = load float, float* %g96, align 4, !tbaa !75
  %add97 = fadd float %127, %128
  %g98 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %curr, i32 0, i32 2
  %129 = load float, float* %g98, align 4, !tbaa !75
  %mul99 = fmul float %129, 2.000000e+00
  %sub100 = fsub float %add97, %mul99
  %130 = call float @llvm.fabs.f32(float %sub100)
  store float %130, float* %g1, align 4, !tbaa !55
  %131 = bitcast float* %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %131) #8
  %b101 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %prevl, i32 0, i32 3
  %132 = load float, float* %b101, align 4, !tbaa !76
  %b102 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %nextl, i32 0, i32 3
  %133 = load float, float* %b102, align 4, !tbaa !76
  %add103 = fadd float %132, %133
  %b104 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %curr, i32 0, i32 3
  %134 = load float, float* %b104, align 4, !tbaa !76
  %mul105 = fmul float %134, 2.000000e+00
  %sub106 = fsub float %add103, %mul105
  %135 = call float @llvm.fabs.f32(float %sub106)
  store float %135, float* %b1, align 4, !tbaa !55
  %136 = bitcast float* %horiz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #8
  %137 = load float, float* %a, align 4, !tbaa !55
  %138 = load float, float* %r, align 4, !tbaa !55
  %cmp107 = fcmp ogt float %137, %138
  br i1 %cmp107, label %cond.true108, label %cond.false109

cond.true108:                                     ; preds = %cond.end54
  %139 = load float, float* %a, align 4, !tbaa !55
  br label %cond.end110

cond.false109:                                    ; preds = %cond.end54
  %140 = load float, float* %r, align 4, !tbaa !55
  br label %cond.end110

cond.end110:                                      ; preds = %cond.false109, %cond.true108
  %cond111 = phi float [ %139, %cond.true108 ], [ %140, %cond.false109 ]
  %141 = load float, float* %g, align 4, !tbaa !55
  %142 = load float, float* %b, align 4, !tbaa !55
  %cmp112 = fcmp ogt float %141, %142
  br i1 %cmp112, label %cond.true113, label %cond.false114

cond.true113:                                     ; preds = %cond.end110
  %143 = load float, float* %g, align 4, !tbaa !55
  br label %cond.end115

cond.false114:                                    ; preds = %cond.end110
  %144 = load float, float* %b, align 4, !tbaa !55
  br label %cond.end115

cond.end115:                                      ; preds = %cond.false114, %cond.true113
  %cond116 = phi float [ %143, %cond.true113 ], [ %144, %cond.false114 ]
  %cmp117 = fcmp ogt float %cond111, %cond116
  br i1 %cmp117, label %cond.true118, label %cond.false124

cond.true118:                                     ; preds = %cond.end115
  %145 = load float, float* %a, align 4, !tbaa !55
  %146 = load float, float* %r, align 4, !tbaa !55
  %cmp119 = fcmp ogt float %145, %146
  br i1 %cmp119, label %cond.true120, label %cond.false121

cond.true120:                                     ; preds = %cond.true118
  %147 = load float, float* %a, align 4, !tbaa !55
  br label %cond.end122

cond.false121:                                    ; preds = %cond.true118
  %148 = load float, float* %r, align 4, !tbaa !55
  br label %cond.end122

cond.end122:                                      ; preds = %cond.false121, %cond.true120
  %cond123 = phi float [ %147, %cond.true120 ], [ %148, %cond.false121 ]
  br label %cond.end130

cond.false124:                                    ; preds = %cond.end115
  %149 = load float, float* %g, align 4, !tbaa !55
  %150 = load float, float* %b, align 4, !tbaa !55
  %cmp125 = fcmp ogt float %149, %150
  br i1 %cmp125, label %cond.true126, label %cond.false127

cond.true126:                                     ; preds = %cond.false124
  %151 = load float, float* %g, align 4, !tbaa !55
  br label %cond.end128

cond.false127:                                    ; preds = %cond.false124
  %152 = load float, float* %b, align 4, !tbaa !55
  br label %cond.end128

cond.end128:                                      ; preds = %cond.false127, %cond.true126
  %cond129 = phi float [ %151, %cond.true126 ], [ %152, %cond.false127 ]
  br label %cond.end130

cond.end130:                                      ; preds = %cond.end128, %cond.end122
  %cond131 = phi float [ %cond123, %cond.end122 ], [ %cond129, %cond.end128 ]
  store float %cond131, float* %horiz, align 4, !tbaa !55
  %153 = bitcast float* %vert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %153) #8
  %154 = load float, float* %a1, align 4, !tbaa !55
  %155 = load float, float* %r1, align 4, !tbaa !55
  %cmp132 = fcmp ogt float %154, %155
  br i1 %cmp132, label %cond.true133, label %cond.false134

cond.true133:                                     ; preds = %cond.end130
  %156 = load float, float* %a1, align 4, !tbaa !55
  br label %cond.end135

cond.false134:                                    ; preds = %cond.end130
  %157 = load float, float* %r1, align 4, !tbaa !55
  br label %cond.end135

cond.end135:                                      ; preds = %cond.false134, %cond.true133
  %cond136 = phi float [ %156, %cond.true133 ], [ %157, %cond.false134 ]
  %158 = load float, float* %g1, align 4, !tbaa !55
  %159 = load float, float* %b1, align 4, !tbaa !55
  %cmp137 = fcmp ogt float %158, %159
  br i1 %cmp137, label %cond.true138, label %cond.false139

cond.true138:                                     ; preds = %cond.end135
  %160 = load float, float* %g1, align 4, !tbaa !55
  br label %cond.end140

cond.false139:                                    ; preds = %cond.end135
  %161 = load float, float* %b1, align 4, !tbaa !55
  br label %cond.end140

cond.end140:                                      ; preds = %cond.false139, %cond.true138
  %cond141 = phi float [ %160, %cond.true138 ], [ %161, %cond.false139 ]
  %cmp142 = fcmp ogt float %cond136, %cond141
  br i1 %cmp142, label %cond.true143, label %cond.false149

cond.true143:                                     ; preds = %cond.end140
  %162 = load float, float* %a1, align 4, !tbaa !55
  %163 = load float, float* %r1, align 4, !tbaa !55
  %cmp144 = fcmp ogt float %162, %163
  br i1 %cmp144, label %cond.true145, label %cond.false146

cond.true145:                                     ; preds = %cond.true143
  %164 = load float, float* %a1, align 4, !tbaa !55
  br label %cond.end147

cond.false146:                                    ; preds = %cond.true143
  %165 = load float, float* %r1, align 4, !tbaa !55
  br label %cond.end147

cond.end147:                                      ; preds = %cond.false146, %cond.true145
  %cond148 = phi float [ %164, %cond.true145 ], [ %165, %cond.false146 ]
  br label %cond.end155

cond.false149:                                    ; preds = %cond.end140
  %166 = load float, float* %g1, align 4, !tbaa !55
  %167 = load float, float* %b1, align 4, !tbaa !55
  %cmp150 = fcmp ogt float %166, %167
  br i1 %cmp150, label %cond.true151, label %cond.false152

cond.true151:                                     ; preds = %cond.false149
  %168 = load float, float* %g1, align 4, !tbaa !55
  br label %cond.end153

cond.false152:                                    ; preds = %cond.false149
  %169 = load float, float* %b1, align 4, !tbaa !55
  br label %cond.end153

cond.end153:                                      ; preds = %cond.false152, %cond.true151
  %cond154 = phi float [ %168, %cond.true151 ], [ %169, %cond.false152 ]
  br label %cond.end155

cond.end155:                                      ; preds = %cond.end153, %cond.end147
  %cond156 = phi float [ %cond148, %cond.end147 ], [ %cond154, %cond.end153 ]
  store float %cond156, float* %vert, align 4, !tbaa !55
  %170 = bitcast float* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #8
  %171 = load float, float* %horiz, align 4, !tbaa !55
  %172 = load float, float* %vert, align 4, !tbaa !55
  %cmp157 = fcmp ogt float %171, %172
  br i1 %cmp157, label %cond.true158, label %cond.false159

cond.true158:                                     ; preds = %cond.end155
  %173 = load float, float* %horiz, align 4, !tbaa !55
  br label %cond.end160

cond.false159:                                    ; preds = %cond.end155
  %174 = load float, float* %vert, align 4, !tbaa !55
  br label %cond.end160

cond.end160:                                      ; preds = %cond.false159, %cond.true158
  %cond161 = phi float [ %173, %cond.true158 ], [ %174, %cond.false159 ]
  store float %cond161, float* %edge, align 4, !tbaa !55
  %175 = bitcast float* %z to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %175) #8
  %176 = load float, float* %edge, align 4, !tbaa !55
  %177 = load float, float* %horiz, align 4, !tbaa !55
  %178 = load float, float* %vert, align 4, !tbaa !55
  %sub162 = fsub float %177, %178
  %179 = call float @llvm.fabs.f32(float %sub162)
  %mul163 = fmul float %179, 5.000000e-01
  %sub164 = fsub float %176, %mul163
  store float %sub164, float* %z, align 4, !tbaa !55
  %180 = load float, float* %z, align 4, !tbaa !55
  %181 = load float, float* %horiz, align 4, !tbaa !55
  %182 = load float, float* %vert, align 4, !tbaa !55
  %cmp165 = fcmp olt float %181, %182
  br i1 %cmp165, label %cond.true166, label %cond.false167

cond.true166:                                     ; preds = %cond.end160
  %183 = load float, float* %horiz, align 4, !tbaa !55
  br label %cond.end168

cond.false167:                                    ; preds = %cond.end160
  %184 = load float, float* %vert, align 4, !tbaa !55
  br label %cond.end168

cond.end168:                                      ; preds = %cond.false167, %cond.true166
  %cond169 = phi float [ %183, %cond.true166 ], [ %184, %cond.false167 ]
  %cmp170 = fcmp ogt float %180, %cond169
  br i1 %cmp170, label %cond.true171, label %cond.false172

cond.true171:                                     ; preds = %cond.end168
  %185 = load float, float* %z, align 4, !tbaa !55
  br label %cond.end178

cond.false172:                                    ; preds = %cond.end168
  %186 = load float, float* %horiz, align 4, !tbaa !55
  %187 = load float, float* %vert, align 4, !tbaa !55
  %cmp173 = fcmp olt float %186, %187
  br i1 %cmp173, label %cond.true174, label %cond.false175

cond.true174:                                     ; preds = %cond.false172
  %188 = load float, float* %horiz, align 4, !tbaa !55
  br label %cond.end176

cond.false175:                                    ; preds = %cond.false172
  %189 = load float, float* %vert, align 4, !tbaa !55
  br label %cond.end176

cond.end176:                                      ; preds = %cond.false175, %cond.true174
  %cond177 = phi float [ %188, %cond.true174 ], [ %189, %cond.false175 ]
  br label %cond.end178

cond.end178:                                      ; preds = %cond.end176, %cond.true171
  %cond179 = phi float [ %185, %cond.true171 ], [ %cond177, %cond.end176 ]
  %sub180 = fsub float 1.000000e+00, %cond179
  store float %sub180, float* %z, align 4, !tbaa !55
  %190 = load float, float* %z, align 4, !tbaa !55
  %191 = load float, float* %z, align 4, !tbaa !55
  %mul181 = fmul float %191, %190
  store float %mul181, float* %z, align 4, !tbaa !55
  %192 = load float, float* %z, align 4, !tbaa !55
  %193 = load float, float* %z, align 4, !tbaa !55
  %mul182 = fmul float %193, %192
  store float %mul182, float* %z, align 4, !tbaa !55
  %194 = bitcast i32* %z_int to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %194) #8
  %195 = load float, float* %z, align 4, !tbaa !55
  %mul183 = fmul float %195, 1.710000e+02
  %conv = fptoui float %mul183 to i32
  %add184 = add i32 85, %conv
  store i32 %add184, i32* %z_int, align 4, !tbaa !13
  %196 = load i32, i32* %z_int, align 4, !tbaa !13
  %cmp185 = icmp ult i32 %196, 255
  br i1 %cmp185, label %cond.true187, label %cond.false188

cond.true187:                                     ; preds = %cond.end178
  %197 = load i32, i32* %z_int, align 4, !tbaa !13
  br label %cond.end189

cond.false188:                                    ; preds = %cond.end178
  br label %cond.end189

cond.end189:                                      ; preds = %cond.false188, %cond.true187
  %cond190 = phi i32 [ %197, %cond.true187 ], [ 255, %cond.false188 ]
  %conv191 = trunc i32 %cond190 to i8
  %198 = load i8*, i8** %noise, align 4, !tbaa !2
  %199 = load i32, i32* %j, align 4, !tbaa !13
  %200 = load i32, i32* %cols, align 4, !tbaa !13
  %mul192 = mul i32 %199, %200
  %201 = load i32, i32* %i, align 4, !tbaa !13
  %add193 = add i32 %mul192, %201
  %arrayidx194 = getelementptr inbounds i8, i8* %198, i32 %add193
  store i8 %conv191, i8* %arrayidx194, align 1, !tbaa !12
  %202 = bitcast i32* %e_int to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %202) #8
  %203 = load float, float* %edge, align 4, !tbaa !55
  %mul195 = fmul float %203, 2.560000e+02
  %conv196 = fptosi float %mul195 to i32
  %sub197 = sub nsw i32 255, %conv196
  store i32 %sub197, i32* %e_int, align 4, !tbaa !13
  %204 = load i32, i32* %e_int, align 4, !tbaa !13
  %cmp198 = icmp sgt i32 %204, 0
  br i1 %cmp198, label %cond.true200, label %cond.false207

cond.true200:                                     ; preds = %cond.end189
  %205 = load i32, i32* %e_int, align 4, !tbaa !13
  %cmp201 = icmp slt i32 %205, 255
  br i1 %cmp201, label %cond.true203, label %cond.false204

cond.true203:                                     ; preds = %cond.true200
  %206 = load i32, i32* %e_int, align 4, !tbaa !13
  br label %cond.end205

cond.false204:                                    ; preds = %cond.true200
  br label %cond.end205

cond.end205:                                      ; preds = %cond.false204, %cond.true203
  %cond206 = phi i32 [ %206, %cond.true203 ], [ 255, %cond.false204 ]
  br label %cond.end208

cond.false207:                                    ; preds = %cond.end189
  br label %cond.end208

cond.end208:                                      ; preds = %cond.false207, %cond.end205
  %cond209 = phi i32 [ %cond206, %cond.end205 ], [ 0, %cond.false207 ]
  %conv210 = trunc i32 %cond209 to i8
  %207 = load i8*, i8** %edges, align 4, !tbaa !2
  %208 = load i32, i32* %j, align 4, !tbaa !13
  %209 = load i32, i32* %cols, align 4, !tbaa !13
  %mul211 = mul i32 %208, %209
  %210 = load i32, i32* %i, align 4, !tbaa !13
  %add212 = add i32 %mul211, %210
  %arrayidx213 = getelementptr inbounds i8, i8* %207, i32 %add212
  store i8 %conv210, i8* %arrayidx213, align 1, !tbaa !12
  %211 = bitcast i32* %e_int to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #8
  %212 = bitcast i32* %z_int to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #8
  %213 = bitcast float* %z to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #8
  %214 = bitcast float* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #8
  %215 = bitcast float* %vert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #8
  %216 = bitcast float* %horiz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #8
  %217 = bitcast float* %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #8
  %218 = bitcast float* %g1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #8
  %219 = bitcast float* %r1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #8
  %220 = bitcast float* %a1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #8
  %221 = bitcast %struct.f_pixel* %nextl to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %221) #8
  %222 = bitcast %struct.f_pixel* %prevl to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %222) #8
  %223 = bitcast float* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #8
  %224 = bitcast float* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #8
  %225 = bitcast float* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #8
  %226 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #8
  br label %for.inc

for.inc:                                          ; preds = %cond.end208
  %227 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %227, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond43

for.end:                                          ; preds = %for.cond.cleanup45
  %228 = bitcast %struct.f_pixel* %next to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %228) #8
  %229 = bitcast %struct.f_pixel* %curr to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %229) #8
  %230 = bitcast %struct.f_pixel* %prev to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %230) #8
  br label %for.inc214

for.inc214:                                       ; preds = %for.end
  %231 = load i32, i32* %j, align 4, !tbaa !13
  %inc215 = add i32 %231, 1
  store i32 %inc215, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.end216:                                       ; preds = %for.cond.cleanup
  %232 = load i8*, i8** %noise, align 4, !tbaa !2
  %233 = load i8*, i8** %tmp, align 4, !tbaa !2
  %234 = load i32, i32* %cols, align 4, !tbaa !13
  %235 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_max3(i8* %232, i8* %233, i32 %234, i32 %235)
  %236 = load i8*, i8** %tmp, align 4, !tbaa !2
  %237 = load i8*, i8** %noise, align 4, !tbaa !2
  %238 = load i32, i32* %cols, align 4, !tbaa !13
  %239 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_max3(i8* %236, i8* %237, i32 %238, i32 %239)
  %240 = load i8*, i8** %noise, align 4, !tbaa !2
  %241 = load i8*, i8** %tmp, align 4, !tbaa !2
  %242 = load i8*, i8** %noise, align 4, !tbaa !2
  %243 = load i32, i32* %cols, align 4, !tbaa !13
  %244 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_blur(i8* %240, i8* %241, i8* %242, i32 %243, i32 %244, i32 3)
  %245 = load i8*, i8** %noise, align 4, !tbaa !2
  %246 = load i8*, i8** %tmp, align 4, !tbaa !2
  %247 = load i32, i32* %cols, align 4, !tbaa !13
  %248 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_max3(i8* %245, i8* %246, i32 %247, i32 %248)
  %249 = load i8*, i8** %tmp, align 4, !tbaa !2
  %250 = load i8*, i8** %noise, align 4, !tbaa !2
  %251 = load i32, i32* %cols, align 4, !tbaa !13
  %252 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_min3(i8* %249, i8* %250, i32 %251, i32 %252)
  %253 = load i8*, i8** %noise, align 4, !tbaa !2
  %254 = load i8*, i8** %tmp, align 4, !tbaa !2
  %255 = load i32, i32* %cols, align 4, !tbaa !13
  %256 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_min3(i8* %253, i8* %254, i32 %255, i32 %256)
  %257 = load i8*, i8** %tmp, align 4, !tbaa !2
  %258 = load i8*, i8** %noise, align 4, !tbaa !2
  %259 = load i32, i32* %cols, align 4, !tbaa !13
  %260 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_min3(i8* %257, i8* %258, i32 %259, i32 %260)
  %261 = load i8*, i8** %edges, align 4, !tbaa !2
  %262 = load i8*, i8** %tmp, align 4, !tbaa !2
  %263 = load i32, i32* %cols, align 4, !tbaa !13
  %264 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_min3(i8* %261, i8* %262, i32 %263, i32 %264)
  %265 = load i8*, i8** %tmp, align 4, !tbaa !2
  %266 = load i8*, i8** %edges, align 4, !tbaa !2
  %267 = load i32, i32* %cols, align 4, !tbaa !13
  %268 = load i32, i32* %rows, align 4, !tbaa !13
  call void @liq_max3(i8* %265, i8* %266, i32 %267, i32 %268)
  %269 = bitcast i32* %i217 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %269) #8
  store i32 0, i32* %i217, align 4, !tbaa !13
  br label %for.cond218

for.cond218:                                      ; preds = %for.inc240, %for.end216
  %270 = load i32, i32* %i217, align 4, !tbaa !13
  %271 = load i32, i32* %cols, align 4, !tbaa !13
  %272 = load i32, i32* %rows, align 4, !tbaa !13
  %mul219 = mul i32 %271, %272
  %cmp220 = icmp ult i32 %270, %mul219
  br i1 %cmp220, label %for.body223, label %for.cond.cleanup222

for.cond.cleanup222:                              ; preds = %for.cond218
  store i32 8, i32* %cleanup.dest.slot, align 4
  %273 = bitcast i32* %i217 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #8
  br label %for.end242

for.body223:                                      ; preds = %for.cond218
  %274 = load i8*, i8** %noise, align 4, !tbaa !2
  %275 = load i32, i32* %i217, align 4, !tbaa !13
  %arrayidx224 = getelementptr inbounds i8, i8* %274, i32 %275
  %276 = load i8, i8* %arrayidx224, align 1, !tbaa !12
  %conv225 = zext i8 %276 to i32
  %277 = load i8*, i8** %edges, align 4, !tbaa !2
  %278 = load i32, i32* %i217, align 4, !tbaa !13
  %arrayidx226 = getelementptr inbounds i8, i8* %277, i32 %278
  %279 = load i8, i8* %arrayidx226, align 1, !tbaa !12
  %conv227 = zext i8 %279 to i32
  %cmp228 = icmp slt i32 %conv225, %conv227
  br i1 %cmp228, label %cond.true230, label %cond.false233

cond.true230:                                     ; preds = %for.body223
  %280 = load i8*, i8** %noise, align 4, !tbaa !2
  %281 = load i32, i32* %i217, align 4, !tbaa !13
  %arrayidx231 = getelementptr inbounds i8, i8* %280, i32 %281
  %282 = load i8, i8* %arrayidx231, align 1, !tbaa !12
  %conv232 = zext i8 %282 to i32
  br label %cond.end236

cond.false233:                                    ; preds = %for.body223
  %283 = load i8*, i8** %edges, align 4, !tbaa !2
  %284 = load i32, i32* %i217, align 4, !tbaa !13
  %arrayidx234 = getelementptr inbounds i8, i8* %283, i32 %284
  %285 = load i8, i8* %arrayidx234, align 1, !tbaa !12
  %conv235 = zext i8 %285 to i32
  br label %cond.end236

cond.end236:                                      ; preds = %cond.false233, %cond.true230
  %cond237 = phi i32 [ %conv232, %cond.true230 ], [ %conv235, %cond.false233 ]
  %conv238 = trunc i32 %cond237 to i8
  %286 = load i8*, i8** %edges, align 4, !tbaa !2
  %287 = load i32, i32* %i217, align 4, !tbaa !13
  %arrayidx239 = getelementptr inbounds i8, i8* %286, i32 %287
  store i8 %conv238, i8* %arrayidx239, align 1, !tbaa !12
  br label %for.inc240

for.inc240:                                       ; preds = %cond.end236
  %288 = load i32, i32* %i217, align 4, !tbaa !13
  %inc241 = add i32 %288, 1
  store i32 %inc241, i32* %i217, align 4, !tbaa !13
  br label %for.cond218

for.end242:                                       ; preds = %for.cond.cleanup222
  %289 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %free243 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %289, i32 0, i32 2
  %290 = load void (i8*)*, void (i8*)** %free243, align 8, !tbaa !88
  %291 = load i8*, i8** %tmp, align 4, !tbaa !2
  call void %290(i8* %291)
  %292 = load i8*, i8** %noise, align 4, !tbaa !2
  %293 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %importance_map244 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %293, i32 0, i32 8
  store i8* %292, i8** %importance_map244, align 8, !tbaa !87
  %294 = load i8*, i8** %edges, align 4, !tbaa !2
  %295 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4, !tbaa !2
  %edges245 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %295, i32 0, i32 9
  store i8* %294, i8** %edges245, align 4, !tbaa !94
  %296 = bitcast %struct.f_pixel** %next_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #8
  %297 = bitcast %struct.f_pixel** %prev_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #8
  %298 = bitcast %struct.f_pixel** %curr_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end242, %if.then29
  %299 = bitcast i8** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #8
  %300 = bitcast i8** %edges to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %300) #8
  %301 = bitcast i8** %noise to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %301) #8
  br label %cleanup248

cleanup248:                                       ; preds = %cleanup, %if.then
  %302 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %302) #8
  %303 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %303) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup248, %cleanup248
  ret void

unreachable:                                      ; preds = %cleanup248
  unreachable
}

; Function Attrs: nounwind
define internal zeroext i1 @liq_progress(%struct.liq_attr* nonnull %attr, float %percent) #4 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  %percent.addr = alloca float, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store float %percent, float* %percent.addr, align 4, !tbaa !55
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %0, i32 0, i32 20
  %1 = load i32 (float, i8*)*, i32 (float, i8*)** %progress_callback, align 4, !tbaa !44
  %tobool = icmp ne i32 (float, i8*)* %1, null
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_callback1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 20
  %3 = load i32 (float, i8*)*, i32 (float, i8*)** %progress_callback1, align 4, !tbaa !44
  %4 = load float, float* %percent.addr, align 4, !tbaa !55
  %5 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %progress_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %5, i32 0, i32 21
  %6 = load i8*, i8** %progress_callback_user_info, align 8, !tbaa !45
  %call = call i32 %3(float %4, i8* %6)
  %tobool2 = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool2, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %7 = phi i1 [ false, %entry ], [ %lnot, %land.rhs ]
  ret i1 %7
}

; Function Attrs: inlinehint nounwind
define internal zeroext i1 @liq_image_can_use_rgba_rows(%struct.liq_image* nonnull %img) #7 {
entry:
  %img.addr = alloca %struct.liq_image*, align 4
  %iebug = alloca i8, align 1
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %iebug) #8
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %min_opaque_val = getelementptr inbounds %struct.liq_image, %struct.liq_image* %0, i32 0, i32 17
  %1 = load float, float* %min_opaque_val, align 4, !tbaa !98
  %cmp = fcmp olt float %1, 1.000000e+00
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %iebug, align 1, !tbaa !56
  %2 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 4
  %3 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 8, !tbaa !79
  %tobool = icmp ne %struct.rgba_pixel** %3, null
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load i8, i8* %iebug, align 1, !tbaa !56, !range !29
  %tobool1 = trunc i8 %4 to i1
  %lnot = xor i1 %tobool1, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %5 = phi i1 [ false, %entry ], [ %lnot, %land.rhs ]
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %iebug) #8
  ret i1 %5
}

declare hidden zeroext i1 @pam_computeacolorhash(%struct.acolorhash_table*, %struct.rgba_pixel**, i32, i32, i8*) #1

; Function Attrs: nounwind
define internal %struct.rgba_pixel* @liq_image_get_row_rgba(%struct.liq_image* nonnull %img, i32 %row) #4 {
entry:
  %retval = alloca %struct.rgba_pixel*, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  %row.addr = alloca i32, align 4
  %temp_row = alloca %struct.rgba_pixel*, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !13
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_image_can_use_rgba_rows(%struct.liq_image* %0)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.liq_image, %struct.liq_image* %1, i32 0, i32 4
  %2 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows, align 8, !tbaa !79
  %3 = load i32, i32* %row.addr, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.rgba_pixel*, %struct.rgba_pixel** %2, i32 %3
  %4 = load %struct.rgba_pixel*, %struct.rgba_pixel** %arrayidx, align 4, !tbaa !2
  store %struct.rgba_pixel* %4, %struct.rgba_pixel** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %5 = bitcast %struct.rgba_pixel** %temp_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %temp_row1 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 12
  %7 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row1, align 8, !tbaa !91
  %8 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 6
  %9 = load i32, i32* %width, align 8, !tbaa !85
  %mul = mul i32 %9, 0
  %add.ptr = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %7, i32 %mul
  store %struct.rgba_pixel* %add.ptr, %struct.rgba_pixel** %temp_row, align 4, !tbaa !2
  %10 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows2 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %10, i32 0, i32 4
  %11 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows2, align 8, !tbaa !79
  %tobool = icmp ne %struct.rgba_pixel** %11, null
  br i1 %tobool, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %12 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row, align 4, !tbaa !2
  %13 = bitcast %struct.rgba_pixel* %12 to i8*
  %14 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %rows4 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %14, i32 0, i32 4
  %15 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %rows4, align 8, !tbaa !79
  %16 = load i32, i32* %row.addr, align 4, !tbaa !13
  %arrayidx5 = getelementptr inbounds %struct.rgba_pixel*, %struct.rgba_pixel** %15, i32 %16
  %17 = load %struct.rgba_pixel*, %struct.rgba_pixel** %arrayidx5, align 4, !tbaa !2
  %18 = bitcast %struct.rgba_pixel* %17 to i8*
  %19 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width6 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %19, i32 0, i32 6
  %20 = load i32, i32* %width6, align 8, !tbaa !85
  %mul7 = mul i32 %20, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %13, i8* align 1 %18, i32 %mul7, i1 false)
  br label %if.end9

if.else:                                          ; preds = %if.end
  %21 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %row_callback = getelementptr inbounds %struct.liq_image, %struct.liq_image* %21, i32 0, i32 14
  %22 = load void (%struct.liq_color*, i32, i32, i8*)*, void (%struct.liq_color*, i32, i32, i8*)** %row_callback, align 8, !tbaa !96
  %23 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row, align 4, !tbaa !2
  %24 = bitcast %struct.rgba_pixel* %23 to %struct.liq_color*
  %25 = load i32, i32* %row.addr, align 4, !tbaa !13
  %26 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width8 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %26, i32 0, i32 6
  %27 = load i32, i32* %width8, align 8, !tbaa !85
  %28 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %row_callback_user_info = getelementptr inbounds %struct.liq_image, %struct.liq_image* %28, i32 0, i32 15
  %29 = load i8*, i8** %row_callback_user_info, align 4, !tbaa !97
  call void @liq_executing_user_callback(void (%struct.liq_color*, i32, i32, i8*)* %22, %struct.liq_color* %24, i32 %25, i32 %27, i8* %29)
  br label %if.end9

if.end9:                                          ; preds = %if.else, %if.then3
  %30 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %min_opaque_val = getelementptr inbounds %struct.liq_image, %struct.liq_image* %30, i32 0, i32 17
  %31 = load float, float* %min_opaque_val, align 4, !tbaa !98
  %cmp = fcmp olt float %31, 1.000000e+00
  br i1 %cmp, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end9
  %32 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %33 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row, align 4, !tbaa !2
  call void @modify_alpha(%struct.liq_image* %32, %struct.rgba_pixel* %33)
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %if.end9
  %34 = load %struct.rgba_pixel*, %struct.rgba_pixel** %temp_row, align 4, !tbaa !2
  store %struct.rgba_pixel* %34, %struct.rgba_pixel** %retval, align 4
  %35 = bitcast %struct.rgba_pixel** %temp_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  br label %return

return:                                           ; preds = %if.end11, %if.then
  %36 = load %struct.rgba_pixel*, %struct.rgba_pixel** %retval, align 4
  ret %struct.rgba_pixel* %36
}

; Function Attrs: nounwind
define internal void @liq_verbose_printf(%struct.liq_attr* nonnull %context, i8* nonnull %fmt, ...) #4 {
entry:
  %context.addr = alloca %struct.liq_attr*, align 4
  %fmt.addr = alloca i8*, align 4
  %va = alloca i8*, align 4
  %required_space = alloca i32, align 4
  %saved_stack = alloca i8*, align 4
  %__vla_expr0 = alloca i32, align 4
  store %struct.liq_attr* %context, %struct.liq_attr** %context.addr, align 4, !tbaa !2
  store i8* %fmt, i8** %fmt.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %context.addr, align 4, !tbaa !2
  %log_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %0, i32 0, i32 22
  %1 = load void (%struct.liq_attr*, i8*, i8*)*, void (%struct.liq_attr*, i8*, i8*)** %log_callback, align 4, !tbaa !48
  %tobool = icmp ne void (%struct.liq_attr*, i8*, i8*)* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast i8** %va to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %va1 = bitcast i8** %va to i8*
  call void @llvm.va_start(i8* %va1)
  %3 = bitcast i32* %required_space to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i8*, i8** %fmt.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %va, align 4, !tbaa !2
  %call = call i32 @vsnprintf(i8* null, i32 0, i8* %4, i8* %5)
  %add = add nsw i32 %call, 1
  store i32 %add, i32* %required_space, align 4, !tbaa !13
  %va2 = bitcast i8** %va to i8*
  call void @llvm.va_end(i8* %va2)
  %6 = load i32, i32* %required_space, align 4, !tbaa !13
  %7 = call i8* @llvm.stacksave()
  store i8* %7, i8** %saved_stack, align 4
  %vla = alloca i8, i32 %6, align 16
  store i32 %6, i32* %__vla_expr0, align 4
  %va3 = bitcast i8** %va to i8*
  call void @llvm.va_start(i8* %va3)
  %8 = load i32, i32* %required_space, align 4, !tbaa !13
  %9 = load i8*, i8** %fmt.addr, align 4, !tbaa !2
  %10 = load i8*, i8** %va, align 4, !tbaa !2
  %call4 = call i32 @vsnprintf(i8* %vla, i32 %8, i8* %9, i8* %10)
  %va5 = bitcast i8** %va to i8*
  call void @llvm.va_end(i8* %va5)
  %11 = load %struct.liq_attr*, %struct.liq_attr** %context.addr, align 4, !tbaa !2
  %log_callback6 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %11, i32 0, i32 22
  %12 = load void (%struct.liq_attr*, i8*, i8*)*, void (%struct.liq_attr*, i8*, i8*)** %log_callback6, align 4, !tbaa !48
  %13 = load %struct.liq_attr*, %struct.liq_attr** %context.addr, align 4, !tbaa !2
  %14 = load %struct.liq_attr*, %struct.liq_attr** %context.addr, align 4, !tbaa !2
  %log_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %14, i32 0, i32 23
  %15 = load i8*, i8** %log_callback_user_info, align 8, !tbaa !49
  call void %12(%struct.liq_attr* %13, i8* %vla, i8* %15)
  %16 = load i8*, i8** %saved_stack, align 4
  call void @llvm.stackrestore(i8* %16)
  %17 = bitcast i32* %required_space to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast i8** %va to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define hidden i32 @liq_write_remapped_image(%struct.liq_result* nonnull %result, %struct.liq_image* nonnull %input_image, i8* nonnull %buffer, i32 %buffer_size) #4 {
entry:
  %retval = alloca i32, align 4
  %result.addr = alloca %struct.liq_result*, align 4
  %input_image.addr = alloca %struct.liq_image*, align 4
  %buffer.addr = alloca i8*, align 4
  %buffer_size.addr = alloca i32, align 4
  %required_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %saved_stack = alloca i8*, align 4
  %__vla_expr0 = alloca i32, align 4
  %buffer_bytes = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %buffer_size, i32* %buffer_size.addr, align 4, !tbaa !16
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %3 = bitcast %struct.liq_image* %2 to %struct.liq_attr*
  %call1 = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %3, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 105, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %call4 = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %4)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end3
  store i32 105, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %5 = bitcast i32* %required_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 6
  %7 = load i32, i32* %width, align 8, !tbaa !85
  %8 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 7
  %9 = load i32, i32* %height, align 4, !tbaa !84
  %mul = mul i32 %7, %9
  store i32 %mul, i32* %required_size, align 4, !tbaa !16
  %10 = load i32, i32* %buffer_size.addr, align 4, !tbaa !16
  %11 = load i32, i32* %required_size, align 4, !tbaa !16
  %cmp = icmp ult i32 %10, %11
  br i1 %cmp, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end6
  store i32 104, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end8:                                          ; preds = %if.end6
  %12 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height9 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %12, i32 0, i32 7
  %13 = load i32, i32* %height9, align 4, !tbaa !84
  %14 = call i8* @llvm.stacksave()
  store i8* %14, i8** %saved_stack, align 4
  %vla = alloca i8*, i32 %13, align 16
  store i32 %13, i32* %__vla_expr0, align 4
  %15 = bitcast i8** %buffer_bytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  store i8* %16, i8** %buffer_bytes, align 4, !tbaa !2
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %18 = load i32, i32* %i, align 4, !tbaa !13
  %19 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height10 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %19, i32 0, i32 7
  %20 = load i32, i32* %height10, align 4, !tbaa !84
  %cmp11 = icmp ult i32 %18, %20
  br i1 %cmp11, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %22 = load i8*, i8** %buffer_bytes, align 4, !tbaa !2
  %23 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width12 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %23, i32 0, i32 6
  %24 = load i32, i32* %width12, align 8, !tbaa !85
  %25 = load i32, i32* %i, align 4, !tbaa !13
  %mul13 = mul i32 %24, %25
  %arrayidx = getelementptr inbounds i8, i8* %22, i32 %mul13
  %26 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx14 = getelementptr inbounds i8*, i8** %vla, i32 %26
  store i8* %arrayidx, i8** %arrayidx14, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %29 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %call15 = call i32 @liq_write_remapped_image_rows(%struct.liq_result* %28, %struct.liq_image* %29, i8** %vla)
  store i32 %call15, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %30 = bitcast i8** %buffer_bytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = load i8*, i8** %saved_stack, align 4
  call void @llvm.stackrestore(i8* %31)
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then7
  %32 = bitcast i32* %required_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then5, %if.then2, %if.then
  %33 = load i32, i32* %retval, align 4
  ret i32 %33
}

; Function Attrs: nounwind
declare i8* @llvm.stacksave() #8

; Function Attrs: nounwind
define hidden i32 @liq_write_remapped_image_rows(%struct.liq_result* nonnull %quant, %struct.liq_image* nonnull %input_image, i8** nonnull %row_pointers) #4 {
entry:
  %retval = alloca i32, align 4
  %quant.addr = alloca %struct.liq_result*, align 4
  %input_image.addr = alloca %struct.liq_image*, align 4
  %row_pointers.addr = alloca i8**, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %result = alloca %struct.liq_remapping_result*, align 4
  %remapping_error = alloca float, align 4
  %is_image_huge = alloca i8, align 1
  %allow_dither_map = alloca i8, align 1
  %generate_dither_map = alloca i8, align 1
  store %struct.liq_result* %quant, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  store i8** %row_pointers, i8*** %row_pointers.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 105, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %3 = bitcast %struct.liq_image* %2 to %struct.liq_attr*
  %call1 = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %3, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @liq_image_magic, i32 0, i32 0))
  br i1 %call1, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 105, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end3
  %5 = load i32, i32* %i, align 4, !tbaa !13
  %6 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 7
  %7 = load i32, i32* %height, align 4, !tbaa !84
  %cmp = icmp ult i32 %5, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %8 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !13
  %add.ptr = getelementptr inbounds i8*, i8** %8, i32 %9
  %10 = bitcast i8** %add.ptr to i8*
  %call4 = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %10)
  br i1 %call4, label %lor.lhs.false, label %if.then6

lor.lhs.false:                                    ; preds = %for.body
  %11 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds i8*, i8** %11, i32 %12
  %13 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %call5 = call zeroext i1 @liq_crash_if_invalid_pointer_given(i8* %13)
  br i1 %call5, label %if.end7, label %if.then6

if.then6:                                         ; preds = %lor.lhs.false, %for.body
  store i32 105, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end7
  %14 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

cleanup:                                          ; preds = %if.then6, %for.cond.cleanup
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  %16 = load %struct.liq_result*, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  %remapping = getelementptr inbounds %struct.liq_result, %struct.liq_result* %16, i32 0, i32 3
  %17 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping, align 4, !tbaa !33
  %tobool = icmp ne %struct.liq_remapping_result* %17, null
  br i1 %tobool, label %if.then8, label %if.end10

if.then8:                                         ; preds = %for.end
  %18 = load %struct.liq_result*, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  %remapping9 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %18, i32 0, i32 3
  %19 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %remapping9, align 4, !tbaa !33
  call void @liq_remapping_result_destroy(%struct.liq_remapping_result* %19)
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %for.end
  %20 = bitcast %struct.liq_remapping_result** %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %21 = load %struct.liq_result*, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  %call11 = call %struct.liq_remapping_result* @liq_remapping_result_create(%struct.liq_result* %21)
  %22 = load %struct.liq_result*, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  %remapping12 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %22, i32 0, i32 3
  store %struct.liq_remapping_result* %call11, %struct.liq_remapping_result** %remapping12, align 4, !tbaa !33
  store %struct.liq_remapping_result* %call11, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %23 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %tobool13 = icmp ne %struct.liq_remapping_result* %23, null
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %if.end10
  store i32 101, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup98

if.end15:                                         ; preds = %if.end10
  %24 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges = getelementptr inbounds %struct.liq_image, %struct.liq_image* %24, i32 0, i32 9
  %25 = load i8*, i8** %edges, align 4, !tbaa !94
  %tobool16 = icmp ne i8* %25, null
  br i1 %tobool16, label %if.end21, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end15
  %26 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %dither_map = getelementptr inbounds %struct.liq_image, %struct.liq_image* %26, i32 0, i32 10
  %27 = load i8*, i8** %dither_map, align 8, !tbaa !95
  %tobool17 = icmp ne i8* %27, null
  br i1 %tobool17, label %if.end21, label %land.lhs.true18

land.lhs.true18:                                  ; preds = %land.lhs.true
  %28 = load %struct.liq_result*, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  %use_dither_map = getelementptr inbounds %struct.liq_result, %struct.liq_result* %28, i32 0, i32 12
  %29 = load i8, i8* %use_dither_map, align 4, !tbaa !128
  %conv = zext i8 %29 to i32
  %tobool19 = icmp ne i32 %conv, 0
  br i1 %tobool19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true18
  %30 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  call void @contrast_maps(%struct.liq_image* %30)
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %land.lhs.true18, %land.lhs.true, %if.end15
  %31 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %32 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %progress_stage1 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %32, i32 0, i32 12
  %33 = load i8, i8* %progress_stage1, align 1, !tbaa !129
  %conv22 = zext i8 %33 to i32
  %conv23 = sitofp i32 %conv22 to float
  %mul = fmul float %conv23, 2.500000e-01
  %call24 = call zeroext i1 @liq_remap_progress(%struct.liq_remapping_result* %31, float %mul)
  br i1 %call24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end21
  store i32 102, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup98

if.end26:                                         ; preds = %if.end21
  %34 = bitcast float* %remapping_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  %35 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %palette_error = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %35, i32 0, i32 9
  %36 = load double, double* %palette_error, align 8, !tbaa !114
  %conv27 = fptrunc double %36 to float
  store float %conv27, float* %remapping_error, align 4, !tbaa !55
  %37 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %dither_level = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %37, i32 0, i32 10
  %38 = load float, float* %dither_level, align 8, !tbaa !130
  %cmp28 = fcmp oeq float %38, 0.000000e+00
  br i1 %cmp28, label %if.then30, label %if.else

if.then30:                                        ; preds = %if.end26
  %39 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %int_palette = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %39, i32 0, i32 7
  %40 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %40, i32 0, i32 4
  %41 = load %struct.colormap*, %struct.colormap** %palette, align 8, !tbaa !37
  %42 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %42, i32 0, i32 8
  %43 = load double, double* %gamma, align 8, !tbaa !131
  %44 = load %struct.liq_result*, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  %min_posterization_output = getelementptr inbounds %struct.liq_result, %struct.liq_result* %44, i32 0, i32 11
  %45 = load i32, i32* %min_posterization_output, align 8, !tbaa !117
  call void @set_rounded_palette(%struct.liq_palette* %int_palette, %struct.colormap* %41, double %43, i32 %45)
  %46 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %47 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %48 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %palette31 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %48, i32 0, i32 4
  %49 = load %struct.colormap*, %struct.colormap** %palette31, align 8, !tbaa !37
  %call32 = call float @remap_to_palette(%struct.liq_image* %46, i8** %47, %struct.colormap* %49)
  store float %call32, float* %remapping_error, align 4, !tbaa !55
  br label %if.end89

if.else:                                          ; preds = %if.end26
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %is_image_huge) #8
  %50 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %50, i32 0, i32 6
  %51 = load i32, i32* %width, align 8, !tbaa !85
  %52 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height33 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %52, i32 0, i32 7
  %53 = load i32, i32* %height33, align 4, !tbaa !84
  %mul34 = mul i32 %51, %53
  %cmp35 = icmp ugt i32 %mul34, 4000000
  %frombool = zext i1 %cmp35 to i8
  store i8 %frombool, i8* %is_image_huge, align 1, !tbaa !56
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %allow_dither_map) #8
  %54 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %use_dither_map37 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %54, i32 0, i32 11
  %55 = load i8, i8* %use_dither_map37, align 4, !tbaa !132
  %conv38 = zext i8 %55 to i32
  %cmp39 = icmp eq i32 %conv38, 2
  br i1 %cmp39, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.else
  %56 = load i8, i8* %is_image_huge, align 1, !tbaa !56, !range !29
  %tobool41 = trunc i8 %56 to i1
  br i1 %tobool41, label %land.end, label %land.rhs

land.rhs:                                         ; preds = %lor.rhs
  %57 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %use_dither_map42 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %57, i32 0, i32 11
  %58 = load i8, i8* %use_dither_map42, align 4, !tbaa !132
  %conv43 = zext i8 %58 to i32
  %tobool44 = icmp ne i32 %conv43, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %lor.rhs
  %59 = phi i1 [ false, %lor.rhs ], [ %tobool44, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %if.else
  %60 = phi i1 [ true, %if.else ], [ %59, %land.end ]
  %frombool45 = zext i1 %60 to i8
  store i8 %frombool45, i8* %allow_dither_map, align 1, !tbaa !56
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %generate_dither_map) #8
  %61 = load i8, i8* %allow_dither_map, align 1, !tbaa !56, !range !29
  %tobool46 = trunc i8 %61 to i1
  br i1 %tobool46, label %land.rhs48, label %land.end55

land.rhs48:                                       ; preds = %lor.end
  %62 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges49 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %62, i32 0, i32 9
  %63 = load i8*, i8** %edges49, align 4, !tbaa !94
  %tobool50 = icmp ne i8* %63, null
  br i1 %tobool50, label %land.rhs51, label %land.end54

land.rhs51:                                       ; preds = %land.rhs48
  %64 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %dither_map52 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %64, i32 0, i32 10
  %65 = load i8*, i8** %dither_map52, align 8, !tbaa !95
  %tobool53 = icmp ne i8* %65, null
  %lnot = xor i1 %tobool53, true
  br label %land.end54

land.end54:                                       ; preds = %land.rhs51, %land.rhs48
  %66 = phi i1 [ false, %land.rhs48 ], [ %lnot, %land.rhs51 ]
  br label %land.end55

land.end55:                                       ; preds = %land.end54, %lor.end
  %67 = phi i1 [ false, %lor.end ], [ %66, %land.end54 ]
  %frombool56 = zext i1 %67 to i8
  store i8 %frombool56, i8* %generate_dither_map, align 1, !tbaa !56
  %68 = load i8, i8* %generate_dither_map, align 1, !tbaa !56, !range !29
  %tobool57 = trunc i8 %68 to i1
  br i1 %tobool57, label %if.then58, label %if.end62

if.then58:                                        ; preds = %land.end55
  %69 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %70 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %71 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %palette59 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %71, i32 0, i32 4
  %72 = load %struct.colormap*, %struct.colormap** %palette59, align 8, !tbaa !37
  %call60 = call float @remap_to_palette(%struct.liq_image* %69, i8** %70, %struct.colormap* %72)
  store float %call60, float* %remapping_error, align 4, !tbaa !55
  %73 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %74 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %75 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %palette61 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %75, i32 0, i32 4
  %76 = load %struct.colormap*, %struct.colormap** %palette61, align 8, !tbaa !37
  call void @update_dither_map(%struct.liq_image* %73, i8** %74, %struct.colormap* %76)
  br label %if.end62

if.end62:                                         ; preds = %if.then58, %land.end55
  %77 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %78 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %progress_stage163 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %78, i32 0, i32 12
  %79 = load i8, i8* %progress_stage163, align 1, !tbaa !129
  %conv64 = zext i8 %79 to i32
  %conv65 = sitofp i32 %conv64 to float
  %mul66 = fmul float %conv65, 5.000000e-01
  %call67 = call zeroext i1 @liq_remap_progress(%struct.liq_remapping_result* %77, float %mul66)
  br i1 %call67, label %if.then68, label %if.end69

if.then68:                                        ; preds = %if.end62
  store i32 102, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup85

if.end69:                                         ; preds = %if.end62
  %80 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %int_palette70 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %80, i32 0, i32 7
  %81 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %palette71 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %81, i32 0, i32 4
  %82 = load %struct.colormap*, %struct.colormap** %palette71, align 8, !tbaa !37
  %83 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %gamma72 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %83, i32 0, i32 8
  %84 = load double, double* %gamma72, align 8, !tbaa !131
  %85 = load %struct.liq_result*, %struct.liq_result** %quant.addr, align 4, !tbaa !2
  %min_posterization_output73 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %85, i32 0, i32 11
  %86 = load i32, i32* %min_posterization_output73, align 8, !tbaa !117
  call void @set_rounded_palette(%struct.liq_palette* %int_palette70, %struct.colormap* %82, double %84, i32 %86)
  %87 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %88 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %89 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %90 = load float, float* %remapping_error, align 4, !tbaa !55
  %conv74 = fpext float %90 to double
  %mul75 = fmul double %conv74, 2.400000e+00
  %cmp76 = fcmp ogt double %mul75, 6.250000e-02
  br i1 %cmp76, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end69
  %91 = load float, float* %remapping_error, align 4, !tbaa !55
  %conv78 = fpext float %91 to double
  %mul79 = fmul double %conv78, 2.400000e+00
  br label %cond.end

cond.false:                                       ; preds = %if.end69
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %mul79, %cond.true ], [ 6.250000e-02, %cond.false ]
  %conv80 = fptrunc double %cond to float
  %92 = load i8, i8* %generate_dither_map, align 1, !tbaa !56, !range !29
  %tobool81 = trunc i8 %92 to i1
  %call82 = call zeroext i1 @remap_to_palette_floyd(%struct.liq_image* %87, i8** %88, %struct.liq_remapping_result* %89, float %conv80, i1 zeroext %tobool81)
  br i1 %call82, label %if.end84, label %if.then83

if.then83:                                        ; preds = %cond.end
  store i32 102, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup85

if.end84:                                         ; preds = %cond.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup85

cleanup85:                                        ; preds = %if.end84, %if.then83, %if.then68
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %generate_dither_map) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %allow_dither_map) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %is_image_huge) #8
  %cleanup.dest88 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest88, label %cleanup97 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup85
  br label %if.end89

if.end89:                                         ; preds = %cleanup.cont, %if.then30
  %93 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %palette_error90 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %93, i32 0, i32 9
  %94 = load double, double* %palette_error90, align 8, !tbaa !114
  %cmp91 = fcmp olt double %94, 0.000000e+00
  br i1 %cmp91, label %if.then93, label %if.end96

if.then93:                                        ; preds = %if.end89
  %95 = load float, float* %remapping_error, align 4, !tbaa !55
  %conv94 = fpext float %95 to double
  %96 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %result, align 4, !tbaa !2
  %palette_error95 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %96, i32 0, i32 9
  store double %conv94, double* %palette_error95, align 8, !tbaa !114
  br label %if.end96

if.end96:                                         ; preds = %if.then93, %if.end89
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup97

cleanup97:                                        ; preds = %if.end96, %cleanup85
  %97 = bitcast float* %remapping_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #8
  br label %cleanup98

cleanup98:                                        ; preds = %cleanup97, %if.then25, %if.then14
  %98 = bitcast %struct.liq_remapping_result** %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #8
  br label %return

return:                                           ; preds = %cleanup98, %cleanup, %if.then2, %if.then
  %99 = load i32, i32* %retval, align 4
  ret i32 %99

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
declare void @llvm.stackrestore(i8*) #8

; Function Attrs: nounwind
define internal %struct.liq_remapping_result* @liq_remapping_result_create(%struct.liq_result* nonnull %result) #4 {
entry:
  %retval = alloca %struct.liq_remapping_result*, align 4
  %result.addr = alloca %struct.liq_result*, align 4
  %res = alloca %struct.liq_remapping_result*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %.compoundliteral = alloca %struct.liq_remapping_result, align 8
  store %struct.liq_result* %result, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %0 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %1 = bitcast %struct.liq_result* %0 to %struct.liq_attr*
  %call = call zeroext i1 @liq_crash_if_invalid_handle_pointer_given(%struct.liq_attr* %1, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0))
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.liq_remapping_result* null, %struct.liq_remapping_result** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %struct.liq_remapping_result** %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_result, %struct.liq_result* %3, i32 0, i32 1
  %4 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !133
  %call1 = call i8* %4(i32 1080)
  %5 = bitcast i8* %call1 to %struct.liq_remapping_result*
  store %struct.liq_remapping_result* %5, %struct.liq_remapping_result** %res, align 4, !tbaa !2
  %6 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %res, align 4, !tbaa !2
  %tobool = icmp ne %struct.liq_remapping_result* %6, null
  br i1 %tobool, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store %struct.liq_remapping_result* null, %struct.liq_remapping_result** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %if.end
  %7 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %res, align 4, !tbaa !2
  %8 = bitcast %struct.liq_remapping_result* %.compoundliteral to i8*
  call void @llvm.memset.p0i8.i64(i8* align 8 %8, i8 0, i64 1080, i1 false)
  %magic_header = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 0
  store i8* getelementptr inbounds ([21 x i8], [21 x i8]* @liq_remapping_result_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !41
  %malloc4 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 1
  %9 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %malloc5 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %9, i32 0, i32 1
  %10 = load i8* (i32)*, i8* (i32)** %malloc5, align 4, !tbaa !133
  store i8* (i32)* %10, i8* (i32)** %malloc4, align 4, !tbaa !134
  %free = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 2
  %11 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %free6 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %11, i32 0, i32 2
  %12 = load void (i8*)*, void (i8*)** %free6, align 8, !tbaa !112
  store void (i8*)* %12, void (i8*)** %free, align 8, !tbaa !40
  %palette = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 4
  %13 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %palette7 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %13, i32 0, i32 4
  %14 = load %struct.colormap*, %struct.colormap** %palette7, align 8, !tbaa !110
  %call8 = call %struct.colormap* @pam_duplicate_colormap(%struct.colormap* %14)
  store %struct.colormap* %call8, %struct.colormap** %palette, align 8, !tbaa !37
  %progress_callback = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 5
  %15 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %progress_callback9 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %15, i32 0, i32 5
  %16 = load i32 (float, i8*)*, i32 (float, i8*)** %progress_callback9, align 4, !tbaa !46
  store i32 (float, i8*)* %16, i32 (float, i8*)** %progress_callback, align 4, !tbaa !135
  %progress_callback_user_info = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 6
  %17 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %progress_callback_user_info10 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %17, i32 0, i32 6
  %18 = load i8*, i8** %progress_callback_user_info10, align 8, !tbaa !47
  store i8* %18, i8** %progress_callback_user_info, align 8, !tbaa !136
  %gamma = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 8
  %19 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %gamma11 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %19, i32 0, i32 9
  %20 = load double, double* %gamma11, align 8, !tbaa !36
  store double %20, double* %gamma, align 8, !tbaa !131
  %palette_error = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 9
  %21 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %palette_error12 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %21, i32 0, i32 10
  %22 = load double, double* %palette_error12, align 8, !tbaa !113
  store double %22, double* %palette_error, align 8, !tbaa !114
  %dither_level = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 10
  %23 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %dither_level13 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %23, i32 0, i32 8
  %24 = load float, float* %dither_level13, align 8, !tbaa !109
  store float %24, float* %dither_level, align 8, !tbaa !130
  %use_dither_map = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 11
  %25 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %use_dither_map14 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %25, i32 0, i32 12
  %26 = load i8, i8* %use_dither_map14, align 4, !tbaa !128
  store i8 %26, i8* %use_dither_map, align 4, !tbaa !132
  %progress_stage1 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %.compoundliteral, i32 0, i32 12
  %27 = load %struct.liq_result*, %struct.liq_result** %result.addr, align 4, !tbaa !2
  %use_dither_map15 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %27, i32 0, i32 12
  %28 = load i8, i8* %use_dither_map15, align 4, !tbaa !128
  %conv = zext i8 %28 to i32
  %tobool16 = icmp ne i32 %conv, 0
  %29 = zext i1 %tobool16 to i64
  %cond = select i1 %tobool16, i32 20, i32 0
  %conv17 = trunc i32 %cond to i8
  store i8 %conv17, i8* %progress_stage1, align 1, !tbaa !129
  %30 = bitcast %struct.liq_remapping_result* %7 to i8*
  %31 = bitcast %struct.liq_remapping_result* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %30, i8* align 8 %31, i32 1080, i1 false), !tbaa.struct !137
  %32 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %res, align 4, !tbaa !2
  store %struct.liq_remapping_result* %32, %struct.liq_remapping_result** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end3, %if.then2
  %33 = bitcast %struct.liq_remapping_result** %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %34 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %retval, align 4
  ret %struct.liq_remapping_result* %34
}

; Function Attrs: nounwind
define internal zeroext i1 @liq_remap_progress(%struct.liq_remapping_result* nonnull %quant, float %percent) #4 {
entry:
  %quant.addr = alloca %struct.liq_remapping_result*, align 4
  %percent.addr = alloca float, align 4
  store %struct.liq_remapping_result* %quant, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  store float %percent, float* %percent.addr, align 4, !tbaa !55
  %0 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %progress_callback = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %0, i32 0, i32 5
  %1 = load i32 (float, i8*)*, i32 (float, i8*)** %progress_callback, align 4, !tbaa !135
  %tobool = icmp ne i32 (float, i8*)* %1, null
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %progress_callback1 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %2, i32 0, i32 5
  %3 = load i32 (float, i8*)*, i32 (float, i8*)** %progress_callback1, align 4, !tbaa !135
  %4 = load float, float* %percent.addr, align 4, !tbaa !55
  %5 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %progress_callback_user_info = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %5, i32 0, i32 6
  %6 = load i8*, i8** %progress_callback_user_info, align 8, !tbaa !136
  %call = call i32 %3(float %4, i8* %6)
  %tobool2 = icmp ne i32 %call, 0
  %lnot = xor i1 %tobool2, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %7 = phi i1 [ false, %entry ], [ %lnot, %land.rhs ]
  ret i1 %7
}

; Function Attrs: nounwind
define internal float @remap_to_palette(%struct.liq_image* nonnull %input_image, i8** nonnull %output_pixels, %struct.colormap* nonnull %map) #4 {
entry:
  %retval = alloca float, align 4
  %input_image.addr = alloca %struct.liq_image*, align 4
  %output_pixels.addr = alloca i8**, align 4
  %map.addr = alloca %struct.colormap*, align 4
  %rows = alloca i32, align 4
  %cols = alloca i32, align 4
  %remapping_error = alloca double, align 8
  %cleanup.dest.slot = alloca i32, align 4
  %acolormap = alloca %struct.colormap_item*, align 4
  %n = alloca %struct.nearest_map*, align 4
  %transparent_index = alloca i32, align 4
  %.compoundliteral = alloca %struct.f_pixel, align 4
  %max_threads = alloca i32, align 4
  %saved_stack = alloca i8*, align 4
  %__vla_expr0 = alloca i32, align 4
  %row = alloca i32, align 4
  %row_pixels = alloca %struct.f_pixel*, align 4
  %bg_pixels = alloca %struct.f_pixel*, align 4
  %last_match = alloca i32, align 4
  %col = alloca i32, align 4
  %diff = alloca float, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  store i8** %output_pixels, i8*** %output_pixels.addr, align 4, !tbaa !2
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  %0 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %1, i32 0, i32 7
  %2 = load i32, i32* %height, align 4, !tbaa !84
  store i32 %2, i32* %rows, align 4, !tbaa !13
  %3 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 6
  %5 = load i32, i32* %width, align 8, !tbaa !85
  store i32 %5, i32* %cols, align 4, !tbaa !13
  %6 = bitcast double* %remapping_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #8
  store double 0.000000e+00, double* %remapping_error, align 8, !tbaa !18
  %7 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_image_get_row_f_init(%struct.liq_image* %7)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %8 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 16
  %9 = load %struct.liq_image*, %struct.liq_image** %background, align 8, !tbaa !89
  %tobool = icmp ne %struct.liq_image* %9, null
  br i1 %tobool, label %land.lhs.true, label %if.end4

land.lhs.true:                                    ; preds = %if.end
  %10 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background1 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %10, i32 0, i32 16
  %11 = load %struct.liq_image*, %struct.liq_image** %background1, align 8, !tbaa !89
  %call2 = call zeroext i1 @liq_image_get_row_f_init(%struct.liq_image* %11)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %land.lhs.true
  store float -1.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end4:                                          ; preds = %land.lhs.true, %if.end
  %12 = bitcast %struct.colormap_item** %acolormap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %13, i32 0, i32 3
  %arraydecay = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 0
  store %struct.colormap_item* %arraydecay, %struct.colormap_item** %acolormap, align 4, !tbaa !2
  %14 = bitcast %struct.nearest_map** %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %call5 = call %struct.nearest_map* @nearest_init(%struct.colormap* %15)
  store %struct.nearest_map* %call5, %struct.nearest_map** %n, align 4, !tbaa !2
  %16 = bitcast i32* %transparent_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background6 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %17, i32 0, i32 16
  %18 = load %struct.liq_image*, %struct.liq_image** %background6, align 8, !tbaa !89
  %tobool7 = icmp ne %struct.liq_image* %18, null
  br i1 %tobool7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end4
  %19 = load %struct.nearest_map*, %struct.nearest_map** %n, align 4, !tbaa !2
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 0
  store float 0.000000e+00, float* %a, align 4, !tbaa !72
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 1
  store float 0.000000e+00, float* %r, align 4, !tbaa !74
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 2
  store float 0.000000e+00, float* %g, align 4, !tbaa !75
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 3
  store float 0.000000e+00, float* %b, align 4, !tbaa !76
  %call8 = call i32 @nearest_search(%struct.nearest_map* %19, %struct.f_pixel* %.compoundliteral, i32 0, float* null)
  br label %cond.end

cond.false:                                       ; preds = %if.end4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call8, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %transparent_index, align 4, !tbaa !13
  %20 = bitcast i32* %max_threads to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  store i32 1, i32* %max_threads, align 4, !tbaa !13
  %21 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %21, i32 0, i32 0
  %22 = load i32, i32* %colors, align 4, !tbaa !13
  %add = add i32 2, %22
  %mul = mul i32 %add, 1
  %23 = call i8* @llvm.stacksave()
  store i8* %23, i8** %saved_stack, align 4
  %vla = alloca %struct.kmeans_state, i32 %mul, align 16
  store i32 %mul, i32* %__vla_expr0, align 4
  %24 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  call void @kmeans_init(%struct.colormap* %24, i32 1, %struct.kmeans_state* %vla)
  %25 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  store i32 0, i32* %row, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc41, %cond.end
  %26 = load i32, i32* %row, align 4, !tbaa !13
  %27 = load i32, i32* %rows, align 4, !tbaa !13
  %cmp = icmp slt i32 %26, %27
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %28 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  br label %for.end43

for.body:                                         ; preds = %for.cond
  %29 = bitcast %struct.f_pixel** %row_pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %31 = load i32, i32* %row, align 4, !tbaa !13
  %call9 = call %struct.f_pixel* @liq_image_get_row_f(%struct.liq_image* %30, i32 %31)
  store %struct.f_pixel* %call9, %struct.f_pixel** %row_pixels, align 4, !tbaa !2
  %32 = bitcast %struct.f_pixel** %bg_pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  %33 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background10 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %33, i32 0, i32 16
  %34 = load %struct.liq_image*, %struct.liq_image** %background10, align 8, !tbaa !89
  %tobool11 = icmp ne %struct.liq_image* %34, null
  br i1 %tobool11, label %land.lhs.true12, label %cond.false18

land.lhs.true12:                                  ; preds = %for.body
  %35 = load %struct.colormap_item*, %struct.colormap_item** %acolormap, align 4, !tbaa !2
  %36 = load i32, i32* %transparent_index, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %35, i32 %36
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx, i32 0, i32 0
  %a13 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor, i32 0, i32 0
  %37 = load float, float* %a13, align 4, !tbaa !138
  %cmp14 = fcmp olt float %37, 3.906250e-03
  br i1 %cmp14, label %cond.true15, label %cond.false18

cond.true15:                                      ; preds = %land.lhs.true12
  %38 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background16 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %38, i32 0, i32 16
  %39 = load %struct.liq_image*, %struct.liq_image** %background16, align 8, !tbaa !89
  %40 = load i32, i32* %row, align 4, !tbaa !13
  %call17 = call %struct.f_pixel* @liq_image_get_row_f(%struct.liq_image* %39, i32 %40)
  br label %cond.end19

cond.false18:                                     ; preds = %land.lhs.true12, %for.body
  br label %cond.end19

cond.end19:                                       ; preds = %cond.false18, %cond.true15
  %cond20 = phi %struct.f_pixel* [ %call17, %cond.true15 ], [ null, %cond.false18 ]
  store %struct.f_pixel* %cond20, %struct.f_pixel** %bg_pixels, align 4, !tbaa !2
  %41 = bitcast i32* %last_match to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  store i32 0, i32* %last_match, align 4, !tbaa !13
  %42 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #8
  store i32 0, i32* %col, align 4, !tbaa !13
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %cond.end19
  %43 = load i32, i32* %col, align 4, !tbaa !13
  %44 = load i32, i32* %cols, align 4, !tbaa !13
  %cmp22 = icmp ult i32 %43, %44
  br i1 %cmp22, label %for.body24, label %for.cond.cleanup23

for.cond.cleanup23:                               ; preds = %for.cond21
  store i32 5, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #8
  br label %for.end

for.body24:                                       ; preds = %for.cond21
  %46 = bitcast float* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %47 = load %struct.nearest_map*, %struct.nearest_map** %n, align 4, !tbaa !2
  %48 = load %struct.f_pixel*, %struct.f_pixel** %row_pixels, align 4, !tbaa !2
  %49 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx25 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %48, i32 %49
  %50 = load i32, i32* %last_match, align 4, !tbaa !13
  %call26 = call i32 @nearest_search(%struct.nearest_map* %47, %struct.f_pixel* %arrayidx25, i32 %50, float* %diff)
  store i32 %call26, i32* %last_match, align 4, !tbaa !13
  %51 = load %struct.f_pixel*, %struct.f_pixel** %bg_pixels, align 4, !tbaa !2
  %tobool27 = icmp ne %struct.f_pixel* %51, null
  br i1 %tobool27, label %land.lhs.true28, label %if.end35

land.lhs.true28:                                  ; preds = %for.body24
  %52 = load %struct.f_pixel*, %struct.f_pixel** %bg_pixels, align 4, !tbaa !2
  %53 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx29 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %52, i32 %53
  %54 = load %struct.colormap_item*, %struct.colormap_item** %acolormap, align 4, !tbaa !2
  %55 = load i32, i32* %last_match, align 4, !tbaa !13
  %arrayidx30 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %54, i32 %55
  %acolor31 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx30, i32 0, i32 0
  %call32 = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %arrayidx29, %struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor31)
  %56 = load float, float* %diff, align 4, !tbaa !55
  %cmp33 = fcmp ole float %call32, %56
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %land.lhs.true28
  %57 = load i32, i32* %transparent_index, align 4, !tbaa !13
  store i32 %57, i32* %last_match, align 4, !tbaa !13
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %land.lhs.true28, %for.body24
  %58 = load i32, i32* %last_match, align 4, !tbaa !13
  %conv = trunc i32 %58 to i8
  %59 = load i8**, i8*** %output_pixels.addr, align 4, !tbaa !2
  %60 = load i32, i32* %row, align 4, !tbaa !13
  %arrayidx36 = getelementptr inbounds i8*, i8** %59, i32 %60
  %61 = load i8*, i8** %arrayidx36, align 4, !tbaa !2
  %62 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx37 = getelementptr inbounds i8, i8* %61, i32 %62
  store i8 %conv, i8* %arrayidx37, align 1, !tbaa !12
  %63 = load float, float* %diff, align 4, !tbaa !55
  %conv38 = fpext float %63 to double
  %64 = load double, double* %remapping_error, align 8, !tbaa !18
  %add39 = fadd double %64, %conv38
  store double %add39, double* %remapping_error, align 8, !tbaa !18
  %65 = load %struct.f_pixel*, %struct.f_pixel** %row_pixels, align 4, !tbaa !2
  %66 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx40 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %65, i32 %66
  %67 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %68 = load i32, i32* %last_match, align 4, !tbaa !13
  call void @kmeans_update_color(%struct.f_pixel* byval(%struct.f_pixel) align 4 %arrayidx40, float 1.000000e+00, %struct.colormap* %67, i32 %68, i32 0, %struct.kmeans_state* %vla)
  %69 = bitcast float* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end35
  %70 = load i32, i32* %col, align 4, !tbaa !13
  %inc = add i32 %70, 1
  store i32 %inc, i32* %col, align 4, !tbaa !13
  br label %for.cond21

for.end:                                          ; preds = %for.cond.cleanup23
  %71 = bitcast i32* %last_match to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  %72 = bitcast %struct.f_pixel** %bg_pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #8
  %73 = bitcast %struct.f_pixel** %row_pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #8
  br label %for.inc41

for.inc41:                                        ; preds = %for.end
  %74 = load i32, i32* %row, align 4, !tbaa !13
  %inc42 = add nsw i32 %74, 1
  store i32 %inc42, i32* %row, align 4, !tbaa !13
  br label %for.cond

for.end43:                                        ; preds = %for.cond.cleanup
  %75 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  call void @kmeans_finalize(%struct.colormap* %75, i32 1, %struct.kmeans_state* %vla)
  %76 = load %struct.nearest_map*, %struct.nearest_map** %n, align 4, !tbaa !2
  call void @nearest_free(%struct.nearest_map* %76)
  %77 = load double, double* %remapping_error, align 8, !tbaa !18
  %78 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width44 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %78, i32 0, i32 6
  %79 = load i32, i32* %width44, align 8, !tbaa !85
  %80 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height45 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %80, i32 0, i32 7
  %81 = load i32, i32* %height45, align 4, !tbaa !84
  %mul46 = mul i32 %79, %81
  %conv47 = uitofp i32 %mul46 to double
  %div = fdiv double %77, %conv47
  %conv48 = fptrunc double %div to float
  store float %conv48, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %82 = load i8*, i8** %saved_stack, align 4
  call void @llvm.stackrestore(i8* %82)
  %83 = bitcast i32* %max_threads to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #8
  %84 = bitcast i32* %transparent_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #8
  %85 = bitcast %struct.nearest_map** %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  %86 = bitcast %struct.colormap_item** %acolormap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  br label %cleanup

cleanup:                                          ; preds = %for.end43, %if.then3, %if.then
  %87 = bitcast double* %remapping_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %87) #8
  %88 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  %89 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  %90 = load float, float* %retval, align 4
  ret float %90
}

; Function Attrs: nounwind
define internal void @update_dither_map(%struct.liq_image* nonnull %input_image, i8** nonnull %row_pointers, %struct.colormap* nonnull %map) #4 {
entry:
  %input_image.addr = alloca %struct.liq_image*, align 4
  %row_pointers.addr = alloca i8**, align 4
  %map.addr = alloca %struct.colormap*, align 4
  %width = alloca i32, align 4
  %height = alloca i32, align 4
  %edges = alloca i8*, align 4
  %row = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %lastpixel = alloca i8, align 1
  %lastcol = alloca i32, align 4
  %col = alloca i32, align 4
  %px = alloca i8, align 1
  %neighbor_count = alloca i32, align 4
  %i = alloca i32, align 4
  %pixelabove = alloca i8, align 1
  %pixelbelow = alloca i8, align 1
  %e = alloca i32, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  store i8** %row_pointers, i8*** %row_pointers.addr, align 4, !tbaa !2
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  %0 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width1 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %1, i32 0, i32 6
  %2 = load i32, i32* %width1, align 8, !tbaa !85
  store i32 %2, i32* %width, align 4, !tbaa !13
  %3 = bitcast i32* %height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height2 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 7
  %5 = load i32, i32* %height2, align 4, !tbaa !84
  store i32 %5, i32* %height, align 4, !tbaa !13
  %6 = bitcast i8** %edges to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges3 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %7, i32 0, i32 9
  %8 = load i8*, i8** %edges3, align 4, !tbaa !94
  store i8* %8, i8** %edges, align 4, !tbaa !2
  %9 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %row, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc74, %entry
  %10 = load i32, i32* %row, align 4, !tbaa !13
  %11 = load i32, i32* %height, align 4, !tbaa !13
  %cmp = icmp ult i32 %10, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %12 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  br label %for.end77

for.body:                                         ; preds = %for.cond
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %lastpixel) #8
  %13 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %14 = load i32, i32* %row, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds i8*, i8** %13, i32 %14
  %15 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %15, i32 0
  %16 = load i8, i8* %arrayidx4, align 1, !tbaa !12
  store i8 %16, i8* %lastpixel, align 1, !tbaa !12
  %17 = bitcast i32* %lastcol to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  store i32 0, i32* %lastcol, align 4, !tbaa !13
  %18 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store i32 1, i32* %col, align 4, !tbaa !13
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %19 = load i32, i32* %col, align 4, !tbaa !13
  %20 = load i32, i32* %width, align 4, !tbaa !13
  %cmp6 = icmp ult i32 %19, %20
  br i1 %cmp6, label %for.body8, label %for.cond.cleanup7

for.cond.cleanup7:                                ; preds = %for.cond5
  store i32 5, i32* %cleanup.dest.slot, align 4
  %21 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %for.end

for.body8:                                        ; preds = %for.cond5
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %px) #8
  %22 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %23 = load i32, i32* %row, align 4, !tbaa !13
  %arrayidx9 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  %25 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx10 = getelementptr inbounds i8, i8* %24, i32 %25
  %26 = load i8, i8* %arrayidx10, align 1, !tbaa !12
  store i8 %26, i8* %px, align 1, !tbaa !12
  %27 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background = getelementptr inbounds %struct.liq_image, %struct.liq_image* %27, i32 0, i32 16
  %28 = load %struct.liq_image*, %struct.liq_image** %background, align 8, !tbaa !89
  %tobool = icmp ne %struct.liq_image* %28, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body8
  %29 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %29, i32 0, i32 3
  %30 = load i8, i8* %px, align 1, !tbaa !12
  %idxprom = zext i8 %30 to i32
  %arrayidx11 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 %idxprom
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx11, i32 0, i32 0
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor, i32 0, i32 0
  %31 = load float, float* %a, align 4, !tbaa !138
  %cmp12 = fcmp olt float %31, 3.906250e-03
  br i1 %cmp12, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 7, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %for.body8
  %32 = load i8, i8* %px, align 1, !tbaa !12
  %conv = zext i8 %32 to i32
  %33 = load i8, i8* %lastpixel, align 1, !tbaa !12
  %conv13 = zext i8 %33 to i32
  %cmp14 = icmp ne i32 %conv, %conv13
  br i1 %cmp14, label %if.then18, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %34 = load i32, i32* %col, align 4, !tbaa !13
  %35 = load i32, i32* %width, align 4, !tbaa !13
  %sub = sub i32 %35, 1
  %cmp16 = icmp eq i32 %34, %sub
  br i1 %cmp16, label %if.then18, label %if.end71

if.then18:                                        ; preds = %lor.lhs.false, %if.end
  %36 = bitcast i32* %neighbor_count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #8
  %37 = load i32, i32* %col, align 4, !tbaa !13
  %38 = load i32, i32* %lastcol, align 4, !tbaa !13
  %sub19 = sub i32 %37, %38
  %mul = mul i32 10, %sub19
  store i32 %mul, i32* %neighbor_count, align 4, !tbaa !13
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = load i32, i32* %lastcol, align 4, !tbaa !13
  store i32 %40, i32* %i, align 4, !tbaa !13
  br label %while.cond

while.cond:                                       ; preds = %if.end49, %if.then18
  %41 = load i32, i32* %i, align 4, !tbaa !13
  %42 = load i32, i32* %col, align 4, !tbaa !13
  %cmp20 = icmp ult i32 %41, %42
  br i1 %cmp20, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %43 = load i32, i32* %row, align 4, !tbaa !13
  %cmp22 = icmp ugt i32 %43, 0
  br i1 %cmp22, label %if.then24, label %if.end34

if.then24:                                        ; preds = %while.body
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %pixelabove) #8
  %44 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %45 = load i32, i32* %row, align 4, !tbaa !13
  %sub25 = sub i32 %45, 1
  %arrayidx26 = getelementptr inbounds i8*, i8** %44, i32 %sub25
  %46 = load i8*, i8** %arrayidx26, align 4, !tbaa !2
  %47 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx27 = getelementptr inbounds i8, i8* %46, i32 %47
  %48 = load i8, i8* %arrayidx27, align 1, !tbaa !12
  store i8 %48, i8* %pixelabove, align 1, !tbaa !12
  %49 = load i8, i8* %pixelabove, align 1, !tbaa !12
  %conv28 = zext i8 %49 to i32
  %50 = load i8, i8* %lastpixel, align 1, !tbaa !12
  %conv29 = zext i8 %50 to i32
  %cmp30 = icmp eq i32 %conv28, %conv29
  br i1 %cmp30, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.then24
  %51 = load i32, i32* %neighbor_count, align 4, !tbaa !13
  %add = add nsw i32 %51, 15
  store i32 %add, i32* %neighbor_count, align 4, !tbaa !13
  br label %if.end33

if.end33:                                         ; preds = %if.then32, %if.then24
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %pixelabove) #8
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %while.body
  %52 = load i32, i32* %row, align 4, !tbaa !13
  %53 = load i32, i32* %height, align 4, !tbaa !13
  %sub35 = sub i32 %53, 1
  %cmp36 = icmp ult i32 %52, %sub35
  br i1 %cmp36, label %if.then38, label %if.end49

if.then38:                                        ; preds = %if.end34
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %pixelbelow) #8
  %54 = load i8**, i8*** %row_pointers.addr, align 4, !tbaa !2
  %55 = load i32, i32* %row, align 4, !tbaa !13
  %add39 = add i32 %55, 1
  %arrayidx40 = getelementptr inbounds i8*, i8** %54, i32 %add39
  %56 = load i8*, i8** %arrayidx40, align 4, !tbaa !2
  %57 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx41 = getelementptr inbounds i8, i8* %56, i32 %57
  %58 = load i8, i8* %arrayidx41, align 1, !tbaa !12
  store i8 %58, i8* %pixelbelow, align 1, !tbaa !12
  %59 = load i8, i8* %pixelbelow, align 1, !tbaa !12
  %conv42 = zext i8 %59 to i32
  %60 = load i8, i8* %lastpixel, align 1, !tbaa !12
  %conv43 = zext i8 %60 to i32
  %cmp44 = icmp eq i32 %conv42, %conv43
  br i1 %cmp44, label %if.then46, label %if.end48

if.then46:                                        ; preds = %if.then38
  %61 = load i32, i32* %neighbor_count, align 4, !tbaa !13
  %add47 = add nsw i32 %61, 15
  store i32 %add47, i32* %neighbor_count, align 4, !tbaa !13
  br label %if.end48

if.end48:                                         ; preds = %if.then46, %if.then38
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %pixelbelow) #8
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.end34
  %62 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %62, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond50

while.cond50:                                     ; preds = %while.body53, %while.end
  %63 = load i32, i32* %lastcol, align 4, !tbaa !13
  %64 = load i32, i32* %col, align 4, !tbaa !13
  %cmp51 = icmp ule i32 %63, %64
  br i1 %cmp51, label %while.body53, label %while.end70

while.body53:                                     ; preds = %while.cond50
  %65 = bitcast i32* %e to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #8
  %66 = load i8*, i8** %edges, align 4, !tbaa !2
  %67 = load i32, i32* %row, align 4, !tbaa !13
  %68 = load i32, i32* %width, align 4, !tbaa !13
  %mul54 = mul i32 %67, %68
  %69 = load i32, i32* %lastcol, align 4, !tbaa !13
  %add55 = add i32 %mul54, %69
  %arrayidx56 = getelementptr inbounds i8, i8* %66, i32 %add55
  %70 = load i8, i8* %arrayidx56, align 1, !tbaa !12
  %conv57 = zext i8 %70 to i32
  store i32 %conv57, i32* %e, align 4, !tbaa !13
  %71 = load i32, i32* %e, align 4, !tbaa !13
  %add58 = add nsw i32 %71, 128
  %conv59 = sitofp i32 %add58 to float
  %mul60 = fmul float %conv59, 0x3FE54E3420000000
  %72 = load i32, i32* %neighbor_count, align 4, !tbaa !13
  %add61 = add nsw i32 20, %72
  %conv62 = sitofp i32 %add61 to float
  %div = fdiv float 2.000000e+01, %conv62
  %sub63 = fsub float 1.000000e+00, %div
  %mul64 = fmul float %mul60, %sub63
  %conv65 = fptoui float %mul64 to i8
  %73 = load i8*, i8** %edges, align 4, !tbaa !2
  %74 = load i32, i32* %row, align 4, !tbaa !13
  %75 = load i32, i32* %width, align 4, !tbaa !13
  %mul66 = mul i32 %74, %75
  %76 = load i32, i32* %lastcol, align 4, !tbaa !13
  %inc67 = add i32 %76, 1
  store i32 %inc67, i32* %lastcol, align 4, !tbaa !13
  %add68 = add i32 %mul66, %76
  %arrayidx69 = getelementptr inbounds i8, i8* %73, i32 %add68
  store i8 %conv65, i8* %arrayidx69, align 1, !tbaa !12
  %77 = bitcast i32* %e to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  br label %while.cond50

while.end70:                                      ; preds = %while.cond50
  %78 = load i8, i8* %px, align 1, !tbaa !12
  store i8 %78, i8* %lastpixel, align 1, !tbaa !12
  %79 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #8
  %80 = bitcast i32* %neighbor_count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  br label %if.end71

if.end71:                                         ; preds = %while.end70, %lor.lhs.false
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end71, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %px) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 7, label %for.inc
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %81 = load i32, i32* %col, align 4, !tbaa !13
  %inc72 = add i32 %81, 1
  store i32 %inc72, i32* %col, align 4, !tbaa !13
  br label %for.cond5

for.end:                                          ; preds = %for.cond.cleanup7
  %82 = bitcast i32* %lastcol to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %lastpixel) #8
  br label %for.inc74

for.inc74:                                        ; preds = %for.end
  %83 = load i32, i32* %row, align 4, !tbaa !13
  %inc75 = add i32 %83, 1
  store i32 %inc75, i32* %row, align 4, !tbaa !13
  br label %for.cond

for.end77:                                        ; preds = %for.cond.cleanup
  %84 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges78 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %84, i32 0, i32 9
  %85 = load i8*, i8** %edges78, align 4, !tbaa !94
  %86 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %dither_map = getelementptr inbounds %struct.liq_image, %struct.liq_image* %86, i32 0, i32 10
  store i8* %85, i8** %dither_map, align 8, !tbaa !95
  %87 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges79 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %87, i32 0, i32 9
  store i8* null, i8** %edges79, align 4, !tbaa !94
  %88 = bitcast i8** %edges to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  %89 = bitcast i32* %height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  %90 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal zeroext i1 @remap_to_palette_floyd(%struct.liq_image* nonnull %input_image, i8** nonnull %output_pixels, %struct.liq_remapping_result* nonnull %quant, float %max_dither_error, i1 zeroext %output_image_is_remapped) #4 {
entry:
  %retval = alloca i1, align 1
  %input_image.addr = alloca %struct.liq_image*, align 4
  %output_pixels.addr = alloca i8**, align 4
  %quant.addr = alloca %struct.liq_remapping_result*, align 4
  %max_dither_error.addr = alloca float, align 4
  %output_image_is_remapped.addr = alloca i8, align 1
  %rows = alloca i32, align 4
  %cols = alloca i32, align 4
  %dither_map = alloca i8*, align 4
  %map = alloca %struct.colormap*, align 4
  %acolormap = alloca %struct.colormap_item*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %errwidth = alloca i32, align 4
  %thiserr = alloca %struct.f_pixel*, align 4
  %nexterr = alloca %struct.f_pixel*, align 4
  %ok = alloca i8, align 1
  %n = alloca %struct.nearest_map*, align 4
  %transparent_index = alloca i32, align 4
  %.compoundliteral = alloca %struct.f_pixel, align 4
  %base_dithering_level = alloca float, align 4
  %fs_direction = alloca i32, align 4
  %last_match = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %row_pixels = alloca %struct.f_pixel*, align 4
  %bg_pixels = alloca %struct.f_pixel*, align 4
  %dither_level71 = alloca float, align 4
  %spx = alloca %struct.f_pixel, align 4
  %guessed_match = alloca i32, align 4
  %diff = alloca float, align 4
  %output_px = alloca %struct.f_pixel, align 4
  %err = alloca %struct.f_pixel, align 4
  %temperr = alloca %struct.f_pixel*, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  store i8** %output_pixels, i8*** %output_pixels.addr, align 4, !tbaa !2
  store %struct.liq_remapping_result* %quant, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  store float %max_dither_error, float* %max_dither_error.addr, align 4, !tbaa !55
  %frombool = zext i1 %output_image_is_remapped to i8
  store i8 %frombool, i8* %output_image_is_remapped.addr, align 1, !tbaa !56
  %0 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %1, i32 0, i32 7
  %2 = load i32, i32* %height, align 4, !tbaa !84
  store i32 %2, i32* %rows, align 4, !tbaa !13
  %3 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 6
  %5 = load i32, i32* %width, align 8, !tbaa !85
  store i32 %5, i32* %cols, align 4, !tbaa !13
  %6 = bitcast i8** %dither_map to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %use_dither_map = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %7, i32 0, i32 11
  %8 = load i8, i8* %use_dither_map, align 4, !tbaa !132
  %conv = zext i8 %8 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %cond.true, label %cond.false5

cond.true:                                        ; preds = %entry
  %9 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %dither_map1 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %9, i32 0, i32 10
  %10 = load i8*, i8** %dither_map1, align 8, !tbaa !95
  %tobool2 = icmp ne i8* %10, null
  br i1 %tobool2, label %cond.true3, label %cond.false

cond.true3:                                       ; preds = %cond.true
  %11 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %dither_map4 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %11, i32 0, i32 10
  %12 = load i8*, i8** %dither_map4, align 8, !tbaa !95
  br label %cond.end

cond.false:                                       ; preds = %cond.true
  %13 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %edges = getelementptr inbounds %struct.liq_image, %struct.liq_image* %13, i32 0, i32 9
  %14 = load i8*, i8** %edges, align 4, !tbaa !94
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true3
  %cond = phi i8* [ %12, %cond.true3 ], [ %14, %cond.false ]
  br label %cond.end6

cond.false5:                                      ; preds = %entry
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.end
  %cond7 = phi i8* [ %cond, %cond.end ], [ null, %cond.false5 ]
  store i8* %cond7, i8** %dither_map, align 4, !tbaa !2
  %15 = bitcast %struct.colormap** %map to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %16, i32 0, i32 4
  %17 = load %struct.colormap*, %struct.colormap** %palette, align 8, !tbaa !37
  store %struct.colormap* %17, %struct.colormap** %map, align 4, !tbaa !2
  %18 = bitcast %struct.colormap_item** %acolormap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %19 = load %struct.colormap*, %struct.colormap** %map, align 4, !tbaa !2
  %palette8 = getelementptr inbounds %struct.colormap, %struct.colormap* %19, i32 0, i32 3
  %arraydecay = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette8, i32 0, i32 0
  store %struct.colormap_item* %arraydecay, %struct.colormap_item** %acolormap, align 4, !tbaa !2
  %20 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_image_get_row_f_init(%struct.liq_image* %20)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %cond.end6
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup368

if.end:                                           ; preds = %cond.end6
  %21 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background = getelementptr inbounds %struct.liq_image, %struct.liq_image* %21, i32 0, i32 16
  %22 = load %struct.liq_image*, %struct.liq_image** %background, align 8, !tbaa !89
  %tobool9 = icmp ne %struct.liq_image* %22, null
  br i1 %tobool9, label %land.lhs.true, label %if.end13

land.lhs.true:                                    ; preds = %if.end
  %23 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background10 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %23, i32 0, i32 16
  %24 = load %struct.liq_image*, %struct.liq_image** %background10, align 8, !tbaa !89
  %call11 = call zeroext i1 @liq_image_get_row_f_init(%struct.liq_image* %24)
  br i1 %call11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %land.lhs.true
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup368

if.end13:                                         ; preds = %land.lhs.true, %if.end
  %25 = bitcast i32* %errwidth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load i32, i32* %cols, align 4, !tbaa !13
  %add = add nsw i32 %26, 2
  store i32 %add, i32* %errwidth, align 4, !tbaa !16
  %27 = bitcast %struct.f_pixel** %thiserr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %28 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_image, %struct.liq_image* %28, i32 0, i32 1
  %29 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !86
  %30 = load i32, i32* %errwidth, align 4, !tbaa !16
  %mul = mul i32 %30, 16
  %mul14 = mul i32 %mul, 2
  %call15 = call i8* %29(i32 %mul14)
  %31 = bitcast i8* %call15 to %struct.f_pixel*
  store %struct.f_pixel* %31, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %32 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %tobool16 = icmp ne %struct.f_pixel* %32, null
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %if.end13
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup366

if.end18:                                         ; preds = %if.end13
  %33 = bitcast %struct.f_pixel** %nexterr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %34 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %35 = load i32, i32* %errwidth, align 4, !tbaa !16
  %add.ptr = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %34, i32 %35
  store %struct.f_pixel* %add.ptr, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %36 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %37 = bitcast %struct.f_pixel* %36 to i8*
  %38 = load i32, i32* %errwidth, align 4, !tbaa !16
  %mul19 = mul i32 %38, 16
  call void @llvm.memset.p0i8.i32(i8* align 4 %37, i8 0, i32 %mul19, i1 false)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ok) #8
  store i8 1, i8* %ok, align 1, !tbaa !56
  %39 = bitcast %struct.nearest_map** %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = load %struct.colormap*, %struct.colormap** %map, align 4, !tbaa !2
  %call20 = call %struct.nearest_map* @nearest_init(%struct.colormap* %40)
  store %struct.nearest_map* %call20, %struct.nearest_map** %n, align 4, !tbaa !2
  %41 = bitcast i32* %transparent_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  %42 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background21 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %42, i32 0, i32 16
  %43 = load %struct.liq_image*, %struct.liq_image** %background21, align 8, !tbaa !89
  %tobool22 = icmp ne %struct.liq_image* %43, null
  br i1 %tobool22, label %cond.true23, label %cond.false25

cond.true23:                                      ; preds = %if.end18
  %44 = load %struct.nearest_map*, %struct.nearest_map** %n, align 4, !tbaa !2
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 0
  store float 0.000000e+00, float* %a, align 4, !tbaa !72
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 1
  store float 0.000000e+00, float* %r, align 4, !tbaa !74
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 2
  store float 0.000000e+00, float* %g, align 4, !tbaa !75
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 3
  store float 0.000000e+00, float* %b, align 4, !tbaa !76
  %call24 = call i32 @nearest_search(%struct.nearest_map* %44, %struct.f_pixel* %.compoundliteral, i32 0, float* null)
  br label %cond.end26

cond.false25:                                     ; preds = %if.end18
  br label %cond.end26

cond.end26:                                       ; preds = %cond.false25, %cond.true23
  %cond27 = phi i32 [ %call24, %cond.true23 ], [ 0, %cond.false25 ]
  store i32 %cond27, i32* %transparent_index, align 4, !tbaa !13
  %45 = bitcast float* %base_dithering_level to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #8
  %46 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %dither_level = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %46, i32 0, i32 10
  %47 = load float, float* %dither_level, align 8, !tbaa !130
  store float %47, float* %base_dithering_level, align 4, !tbaa !55
  %48 = load float, float* %base_dithering_level, align 4, !tbaa !55
  %sub = fsub float 1.000000e+00, %48
  %49 = load float, float* %base_dithering_level, align 4, !tbaa !55
  %sub28 = fsub float 1.000000e+00, %49
  %mul29 = fmul float %sub, %sub28
  %sub30 = fsub float 1.000000e+00, %mul29
  store float %sub30, float* %base_dithering_level, align 4, !tbaa !55
  %50 = load i8*, i8** %dither_map, align 4, !tbaa !2
  %tobool31 = icmp ne i8* %50, null
  br i1 %tobool31, label %if.then32, label %if.end34

if.then32:                                        ; preds = %cond.end26
  %51 = load float, float* %base_dithering_level, align 4, !tbaa !55
  %mul33 = fmul float %51, 0x3F70101020000000
  store float %mul33, float* %base_dithering_level, align 4, !tbaa !55
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %cond.end26
  %52 = load float, float* %base_dithering_level, align 4, !tbaa !55
  %mul35 = fmul float %52, 9.375000e-01
  store float %mul35, float* %base_dithering_level, align 4, !tbaa !55
  %53 = bitcast i32* %fs_direction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #8
  store i32 1, i32* %fs_direction, align 4, !tbaa !13
  %54 = bitcast i32* %last_match to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #8
  store i32 0, i32* %last_match, align 4, !tbaa !13
  %55 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #8
  store i32 0, i32* %row, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end34
  %56 = load i32, i32* %row, align 4, !tbaa !13
  %57 = load i32, i32* %rows, align 4, !tbaa !13
  %cmp = icmp slt i32 %56, %57
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup351

for.body:                                         ; preds = %for.cond
  %58 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %59 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %progress_stage1 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %59, i32 0, i32 12
  %60 = load i8, i8* %progress_stage1, align 1, !tbaa !129
  %conv37 = zext i8 %60 to i32
  %conv38 = sitofp i32 %conv37 to float
  %61 = load i32, i32* %row, align 4, !tbaa !13
  %conv39 = sitofp i32 %61 to float
  %62 = load %struct.liq_remapping_result*, %struct.liq_remapping_result** %quant.addr, align 4, !tbaa !2
  %progress_stage140 = getelementptr inbounds %struct.liq_remapping_result, %struct.liq_remapping_result* %62, i32 0, i32 12
  %63 = load i8, i8* %progress_stage140, align 1, !tbaa !129
  %conv41 = zext i8 %63 to i32
  %conv42 = sitofp i32 %conv41 to float
  %sub43 = fsub float 1.000000e+02, %conv42
  %mul44 = fmul float %conv39, %sub43
  %64 = load i32, i32* %rows, align 4, !tbaa !13
  %conv45 = sitofp i32 %64 to float
  %div = fdiv float %mul44, %conv45
  %add46 = fadd float %conv38, %div
  %call47 = call zeroext i1 @liq_remap_progress(%struct.liq_remapping_result* %58, float %add46)
  br i1 %call47, label %if.then48, label %if.end49

if.then48:                                        ; preds = %for.body
  store i8 0, i8* %ok, align 1, !tbaa !56
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup351

if.end49:                                         ; preds = %for.body
  %65 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %66 = bitcast %struct.f_pixel* %65 to i8*
  %67 = load i32, i32* %errwidth, align 4, !tbaa !16
  %mul50 = mul i32 %67, 16
  call void @llvm.memset.p0i8.i32(i8* align 4 %66, i8 0, i32 %mul50, i1 false)
  %68 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #8
  %69 = load i32, i32* %fs_direction, align 4, !tbaa !13
  %cmp51 = icmp sgt i32 %69, 0
  br i1 %cmp51, label %cond.true53, label %cond.false54

cond.true53:                                      ; preds = %if.end49
  br label %cond.end56

cond.false54:                                     ; preds = %if.end49
  %70 = load i32, i32* %cols, align 4, !tbaa !13
  %sub55 = sub nsw i32 %70, 1
  br label %cond.end56

cond.end56:                                       ; preds = %cond.false54, %cond.true53
  %cond57 = phi i32 [ 0, %cond.true53 ], [ %sub55, %cond.false54 ]
  store i32 %cond57, i32* %col, align 4, !tbaa !13
  %71 = bitcast %struct.f_pixel** %row_pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #8
  %72 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %73 = load i32, i32* %row, align 4, !tbaa !13
  %call58 = call %struct.f_pixel* @liq_image_get_row_f(%struct.liq_image* %72, i32 %73)
  store %struct.f_pixel* %call58, %struct.f_pixel** %row_pixels, align 4, !tbaa !2
  %74 = bitcast %struct.f_pixel** %bg_pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #8
  %75 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background59 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %75, i32 0, i32 16
  %76 = load %struct.liq_image*, %struct.liq_image** %background59, align 8, !tbaa !89
  %tobool60 = icmp ne %struct.liq_image* %76, null
  br i1 %tobool60, label %land.lhs.true61, label %cond.false68

land.lhs.true61:                                  ; preds = %cond.end56
  %77 = load %struct.colormap_item*, %struct.colormap_item** %acolormap, align 4, !tbaa !2
  %78 = load i32, i32* %transparent_index, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %77, i32 %78
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx, i32 0, i32 0
  %a62 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor, i32 0, i32 0
  %79 = load float, float* %a62, align 4, !tbaa !138
  %cmp63 = fcmp olt float %79, 3.906250e-03
  br i1 %cmp63, label %cond.true65, label %cond.false68

cond.true65:                                      ; preds = %land.lhs.true61
  %80 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %background66 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %80, i32 0, i32 16
  %81 = load %struct.liq_image*, %struct.liq_image** %background66, align 8, !tbaa !89
  %82 = load i32, i32* %row, align 4, !tbaa !13
  %call67 = call %struct.f_pixel* @liq_image_get_row_f(%struct.liq_image* %81, i32 %82)
  br label %cond.end69

cond.false68:                                     ; preds = %land.lhs.true61, %cond.end56
  br label %cond.end69

cond.end69:                                       ; preds = %cond.false68, %cond.true65
  %cond70 = phi %struct.f_pixel* [ %call67, %cond.true65 ], [ null, %cond.false68 ]
  store %struct.f_pixel* %cond70, %struct.f_pixel** %bg_pixels, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %do.cond, %cond.end69
  %83 = bitcast float* %dither_level71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #8
  %84 = load float, float* %base_dithering_level, align 4, !tbaa !55
  store float %84, float* %dither_level71, align 4, !tbaa !55
  %85 = load i8*, i8** %dither_map, align 4, !tbaa !2
  %tobool72 = icmp ne i8* %85, null
  br i1 %tobool72, label %if.then73, label %if.end80

if.then73:                                        ; preds = %do.body
  %86 = load i8*, i8** %dither_map, align 4, !tbaa !2
  %87 = load i32, i32* %row, align 4, !tbaa !13
  %88 = load i32, i32* %cols, align 4, !tbaa !13
  %mul74 = mul nsw i32 %87, %88
  %89 = load i32, i32* %col, align 4, !tbaa !13
  %add75 = add nsw i32 %mul74, %89
  %arrayidx76 = getelementptr inbounds i8, i8* %86, i32 %add75
  %90 = load i8, i8* %arrayidx76, align 1, !tbaa !12
  %conv77 = zext i8 %90 to i32
  %conv78 = sitofp i32 %conv77 to float
  %91 = load float, float* %dither_level71, align 4, !tbaa !55
  %mul79 = fmul float %91, %conv78
  store float %mul79, float* %dither_level71, align 4, !tbaa !55
  br label %if.end80

if.end80:                                         ; preds = %if.then73, %do.body
  %92 = bitcast %struct.f_pixel* %spx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %92) #8
  %93 = load float, float* %dither_level71, align 4, !tbaa !55
  %94 = load float, float* %max_dither_error.addr, align 4, !tbaa !55
  %95 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %96 = load i32, i32* %col, align 4, !tbaa !13
  %add81 = add nsw i32 %96, 1
  %arrayidx82 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %95, i32 %add81
  %97 = load %struct.f_pixel*, %struct.f_pixel** %row_pixels, align 4, !tbaa !2
  %98 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx83 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %97, i32 %98
  call void @get_dithered_pixel(%struct.f_pixel* sret align 4 %spx, float %93, float %94, %struct.f_pixel* byval(%struct.f_pixel) align 4 %arrayidx82, %struct.f_pixel* byval(%struct.f_pixel) align 4 %arrayidx83)
  %99 = bitcast i32* %guessed_match to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #8
  %100 = load i8, i8* %output_image_is_remapped.addr, align 1, !tbaa !56, !range !29
  %tobool84 = trunc i8 %100 to i1
  br i1 %tobool84, label %cond.true86, label %cond.false90

cond.true86:                                      ; preds = %if.end80
  %101 = load i8**, i8*** %output_pixels.addr, align 4, !tbaa !2
  %102 = load i32, i32* %row, align 4, !tbaa !13
  %arrayidx87 = getelementptr inbounds i8*, i8** %101, i32 %102
  %103 = load i8*, i8** %arrayidx87, align 4, !tbaa !2
  %104 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx88 = getelementptr inbounds i8, i8* %103, i32 %104
  %105 = load i8, i8* %arrayidx88, align 1, !tbaa !12
  %conv89 = zext i8 %105 to i32
  br label %cond.end91

cond.false90:                                     ; preds = %if.end80
  %106 = load i32, i32* %last_match, align 4, !tbaa !13
  br label %cond.end91

cond.end91:                                       ; preds = %cond.false90, %cond.true86
  %cond92 = phi i32 [ %conv89, %cond.true86 ], [ %106, %cond.false90 ]
  store i32 %cond92, i32* %guessed_match, align 4, !tbaa !13
  %107 = bitcast float* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #8
  %108 = load %struct.nearest_map*, %struct.nearest_map** %n, align 4, !tbaa !2
  %109 = load i32, i32* %guessed_match, align 4, !tbaa !13
  %call93 = call i32 @nearest_search(%struct.nearest_map* %108, %struct.f_pixel* %spx, i32 %109, float* %diff)
  store i32 %call93, i32* %last_match, align 4, !tbaa !13
  %110 = bitcast %struct.f_pixel* %output_px to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %110) #8
  %111 = load %struct.colormap_item*, %struct.colormap_item** %acolormap, align 4, !tbaa !2
  %112 = load i32, i32* %last_match, align 4, !tbaa !13
  %arrayidx94 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %111, i32 %112
  %acolor95 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx94, i32 0, i32 0
  %113 = bitcast %struct.f_pixel* %output_px to i8*
  %114 = bitcast %struct.f_pixel* %acolor95 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %113, i8* align 4 %114, i32 16, i1 false), !tbaa.struct !71
  %115 = load %struct.f_pixel*, %struct.f_pixel** %bg_pixels, align 4, !tbaa !2
  %tobool96 = icmp ne %struct.f_pixel* %115, null
  br i1 %tobool96, label %land.lhs.true97, label %if.else

land.lhs.true97:                                  ; preds = %cond.end91
  %116 = load %struct.f_pixel*, %struct.f_pixel** %bg_pixels, align 4, !tbaa !2
  %117 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx98 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %116, i32 %117
  %call99 = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %arrayidx98, %struct.f_pixel* byval(%struct.f_pixel) align 4 %output_px)
  %118 = load float, float* %diff, align 4, !tbaa !55
  %cmp100 = fcmp ole float %call99, %118
  br i1 %cmp100, label %if.then102, label %if.else

if.then102:                                       ; preds = %land.lhs.true97
  %119 = load %struct.f_pixel*, %struct.f_pixel** %bg_pixels, align 4, !tbaa !2
  %120 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx103 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %119, i32 %120
  %121 = bitcast %struct.f_pixel* %output_px to i8*
  %122 = bitcast %struct.f_pixel* %arrayidx103 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %121, i8* align 4 %122, i32 16, i1 false), !tbaa.struct !71
  %123 = load i32, i32* %transparent_index, align 4, !tbaa !13
  %conv104 = trunc i32 %123 to i8
  %124 = load i8**, i8*** %output_pixels.addr, align 4, !tbaa !2
  %125 = load i32, i32* %row, align 4, !tbaa !13
  %arrayidx105 = getelementptr inbounds i8*, i8** %124, i32 %125
  %126 = load i8*, i8** %arrayidx105, align 4, !tbaa !2
  %127 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx106 = getelementptr inbounds i8, i8* %126, i32 %127
  store i8 %conv104, i8* %arrayidx106, align 1, !tbaa !12
  br label %if.end110

if.else:                                          ; preds = %land.lhs.true97, %cond.end91
  %128 = load i32, i32* %last_match, align 4, !tbaa !13
  %conv107 = trunc i32 %128 to i8
  %129 = load i8**, i8*** %output_pixels.addr, align 4, !tbaa !2
  %130 = load i32, i32* %row, align 4, !tbaa !13
  %arrayidx108 = getelementptr inbounds i8*, i8** %129, i32 %130
  %131 = load i8*, i8** %arrayidx108, align 4, !tbaa !2
  %132 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx109 = getelementptr inbounds i8, i8* %131, i32 %132
  store i8 %conv107, i8* %arrayidx109, align 1, !tbaa !12
  br label %if.end110

if.end110:                                        ; preds = %if.else, %if.then102
  %133 = bitcast %struct.f_pixel* %err to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %133) #8
  %a111 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %a112 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %spx, i32 0, i32 0
  %134 = load float, float* %a112, align 4, !tbaa !72
  %a113 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %output_px, i32 0, i32 0
  %135 = load float, float* %a113, align 4, !tbaa !72
  %sub114 = fsub float %134, %135
  store float %sub114, float* %a111, align 4, !tbaa !72
  %r115 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %r116 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %spx, i32 0, i32 1
  %136 = load float, float* %r116, align 4, !tbaa !74
  %r117 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %output_px, i32 0, i32 1
  %137 = load float, float* %r117, align 4, !tbaa !74
  %sub118 = fsub float %136, %137
  store float %sub118, float* %r115, align 4, !tbaa !74
  %g119 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %g120 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %spx, i32 0, i32 2
  %138 = load float, float* %g120, align 4, !tbaa !75
  %g121 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %output_px, i32 0, i32 2
  %139 = load float, float* %g121, align 4, !tbaa !75
  %sub122 = fsub float %138, %139
  store float %sub122, float* %g119, align 4, !tbaa !75
  %b123 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %b124 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %spx, i32 0, i32 3
  %140 = load float, float* %b124, align 4, !tbaa !76
  %b125 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %output_px, i32 0, i32 3
  %141 = load float, float* %b125, align 4, !tbaa !76
  %sub126 = fsub float %140, %141
  store float %sub126, float* %b123, align 4, !tbaa !76
  %r127 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %142 = load float, float* %r127, align 4, !tbaa !74
  %r128 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %143 = load float, float* %r128, align 4, !tbaa !74
  %mul129 = fmul float %142, %143
  %g130 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %144 = load float, float* %g130, align 4, !tbaa !75
  %g131 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %145 = load float, float* %g131, align 4, !tbaa !75
  %mul132 = fmul float %144, %145
  %add133 = fadd float %mul129, %mul132
  %b134 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %146 = load float, float* %b134, align 4, !tbaa !76
  %b135 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %147 = load float, float* %b135, align 4, !tbaa !76
  %mul136 = fmul float %146, %147
  %add137 = fadd float %add133, %mul136
  %a138 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %148 = load float, float* %a138, align 4, !tbaa !72
  %a139 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %149 = load float, float* %a139, align 4, !tbaa !72
  %mul140 = fmul float %148, %149
  %add141 = fadd float %add137, %mul140
  %150 = load float, float* %max_dither_error.addr, align 4, !tbaa !55
  %cmp142 = fcmp ogt float %add141, %150
  br i1 %cmp142, label %if.then144, label %if.end153

if.then144:                                       ; preds = %if.end110
  %r145 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %151 = load float, float* %r145, align 4, !tbaa !74
  %mul146 = fmul float %151, 7.500000e-01
  store float %mul146, float* %r145, align 4, !tbaa !74
  %g147 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %152 = load float, float* %g147, align 4, !tbaa !75
  %mul148 = fmul float %152, 7.500000e-01
  store float %mul148, float* %g147, align 4, !tbaa !75
  %b149 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %153 = load float, float* %b149, align 4, !tbaa !76
  %mul150 = fmul float %153, 7.500000e-01
  store float %mul150, float* %b149, align 4, !tbaa !76
  %a151 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %154 = load float, float* %a151, align 4, !tbaa !72
  %mul152 = fmul float %154, 7.500000e-01
  store float %mul152, float* %a151, align 4, !tbaa !72
  br label %if.end153

if.end153:                                        ; preds = %if.then144, %if.end110
  %155 = load i32, i32* %fs_direction, align 4, !tbaa !13
  %cmp154 = icmp sgt i32 %155, 0
  br i1 %cmp154, label %if.then156, label %if.else245

if.then156:                                       ; preds = %if.end153
  %a157 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %156 = load float, float* %a157, align 4, !tbaa !72
  %mul158 = fmul float %156, 4.375000e-01
  %157 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %158 = load i32, i32* %col, align 4, !tbaa !13
  %add159 = add nsw i32 %158, 2
  %arrayidx160 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %157, i32 %add159
  %a161 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx160, i32 0, i32 0
  %159 = load float, float* %a161, align 4, !tbaa !72
  %add162 = fadd float %159, %mul158
  store float %add162, float* %a161, align 4, !tbaa !72
  %r163 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %160 = load float, float* %r163, align 4, !tbaa !74
  %mul164 = fmul float %160, 4.375000e-01
  %161 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %162 = load i32, i32* %col, align 4, !tbaa !13
  %add165 = add nsw i32 %162, 2
  %arrayidx166 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %161, i32 %add165
  %r167 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx166, i32 0, i32 1
  %163 = load float, float* %r167, align 4, !tbaa !74
  %add168 = fadd float %163, %mul164
  store float %add168, float* %r167, align 4, !tbaa !74
  %g169 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %164 = load float, float* %g169, align 4, !tbaa !75
  %mul170 = fmul float %164, 4.375000e-01
  %165 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %166 = load i32, i32* %col, align 4, !tbaa !13
  %add171 = add nsw i32 %166, 2
  %arrayidx172 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %165, i32 %add171
  %g173 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx172, i32 0, i32 2
  %167 = load float, float* %g173, align 4, !tbaa !75
  %add174 = fadd float %167, %mul170
  store float %add174, float* %g173, align 4, !tbaa !75
  %b175 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %168 = load float, float* %b175, align 4, !tbaa !76
  %mul176 = fmul float %168, 4.375000e-01
  %169 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %170 = load i32, i32* %col, align 4, !tbaa !13
  %add177 = add nsw i32 %170, 2
  %arrayidx178 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %169, i32 %add177
  %b179 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx178, i32 0, i32 3
  %171 = load float, float* %b179, align 4, !tbaa !76
  %add180 = fadd float %171, %mul176
  store float %add180, float* %b179, align 4, !tbaa !76
  %a181 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %172 = load float, float* %a181, align 4, !tbaa !72
  %mul182 = fmul float %172, 6.250000e-02
  %173 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %174 = load i32, i32* %col, align 4, !tbaa !13
  %add183 = add nsw i32 %174, 2
  %arrayidx184 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %173, i32 %add183
  %a185 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx184, i32 0, i32 0
  store float %mul182, float* %a185, align 4, !tbaa !72
  %r186 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %175 = load float, float* %r186, align 4, !tbaa !74
  %mul187 = fmul float %175, 6.250000e-02
  %176 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %177 = load i32, i32* %col, align 4, !tbaa !13
  %add188 = add nsw i32 %177, 2
  %arrayidx189 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %176, i32 %add188
  %r190 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx189, i32 0, i32 1
  store float %mul187, float* %r190, align 4, !tbaa !74
  %g191 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %178 = load float, float* %g191, align 4, !tbaa !75
  %mul192 = fmul float %178, 6.250000e-02
  %179 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %180 = load i32, i32* %col, align 4, !tbaa !13
  %add193 = add nsw i32 %180, 2
  %arrayidx194 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %179, i32 %add193
  %g195 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx194, i32 0, i32 2
  store float %mul192, float* %g195, align 4, !tbaa !75
  %b196 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %181 = load float, float* %b196, align 4, !tbaa !76
  %mul197 = fmul float %181, 6.250000e-02
  %182 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %183 = load i32, i32* %col, align 4, !tbaa !13
  %add198 = add nsw i32 %183, 2
  %arrayidx199 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %182, i32 %add198
  %b200 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx199, i32 0, i32 3
  store float %mul197, float* %b200, align 4, !tbaa !76
  %a201 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %184 = load float, float* %a201, align 4, !tbaa !72
  %mul202 = fmul float %184, 3.125000e-01
  %185 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %186 = load i32, i32* %col, align 4, !tbaa !13
  %add203 = add nsw i32 %186, 1
  %arrayidx204 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %185, i32 %add203
  %a205 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx204, i32 0, i32 0
  %187 = load float, float* %a205, align 4, !tbaa !72
  %add206 = fadd float %187, %mul202
  store float %add206, float* %a205, align 4, !tbaa !72
  %r207 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %188 = load float, float* %r207, align 4, !tbaa !74
  %mul208 = fmul float %188, 3.125000e-01
  %189 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %190 = load i32, i32* %col, align 4, !tbaa !13
  %add209 = add nsw i32 %190, 1
  %arrayidx210 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %189, i32 %add209
  %r211 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx210, i32 0, i32 1
  %191 = load float, float* %r211, align 4, !tbaa !74
  %add212 = fadd float %191, %mul208
  store float %add212, float* %r211, align 4, !tbaa !74
  %g213 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %192 = load float, float* %g213, align 4, !tbaa !75
  %mul214 = fmul float %192, 3.125000e-01
  %193 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %194 = load i32, i32* %col, align 4, !tbaa !13
  %add215 = add nsw i32 %194, 1
  %arrayidx216 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %193, i32 %add215
  %g217 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx216, i32 0, i32 2
  %195 = load float, float* %g217, align 4, !tbaa !75
  %add218 = fadd float %195, %mul214
  store float %add218, float* %g217, align 4, !tbaa !75
  %b219 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %196 = load float, float* %b219, align 4, !tbaa !76
  %mul220 = fmul float %196, 3.125000e-01
  %197 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %198 = load i32, i32* %col, align 4, !tbaa !13
  %add221 = add nsw i32 %198, 1
  %arrayidx222 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %197, i32 %add221
  %b223 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx222, i32 0, i32 3
  %199 = load float, float* %b223, align 4, !tbaa !76
  %add224 = fadd float %199, %mul220
  store float %add224, float* %b223, align 4, !tbaa !76
  %a225 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %200 = load float, float* %a225, align 4, !tbaa !72
  %mul226 = fmul float %200, 1.875000e-01
  %201 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %202 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx227 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %201, i32 %202
  %a228 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx227, i32 0, i32 0
  %203 = load float, float* %a228, align 4, !tbaa !72
  %add229 = fadd float %203, %mul226
  store float %add229, float* %a228, align 4, !tbaa !72
  %r230 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %204 = load float, float* %r230, align 4, !tbaa !74
  %mul231 = fmul float %204, 1.875000e-01
  %205 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %206 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx232 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %205, i32 %206
  %r233 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx232, i32 0, i32 1
  %207 = load float, float* %r233, align 4, !tbaa !74
  %add234 = fadd float %207, %mul231
  store float %add234, float* %r233, align 4, !tbaa !74
  %g235 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %208 = load float, float* %g235, align 4, !tbaa !75
  %mul236 = fmul float %208, 1.875000e-01
  %209 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %210 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx237 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %209, i32 %210
  %g238 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx237, i32 0, i32 2
  %211 = load float, float* %g238, align 4, !tbaa !75
  %add239 = fadd float %211, %mul236
  store float %add239, float* %g238, align 4, !tbaa !75
  %b240 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %212 = load float, float* %b240, align 4, !tbaa !76
  %mul241 = fmul float %212, 1.875000e-01
  %213 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %214 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx242 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %213, i32 %214
  %b243 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx242, i32 0, i32 3
  %215 = load float, float* %b243, align 4, !tbaa !76
  %add244 = fadd float %215, %mul241
  store float %add244, float* %b243, align 4, !tbaa !76
  br label %if.end330

if.else245:                                       ; preds = %if.end153
  %a246 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %216 = load float, float* %a246, align 4, !tbaa !72
  %mul247 = fmul float %216, 4.375000e-01
  %217 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %218 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx248 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %217, i32 %218
  %a249 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx248, i32 0, i32 0
  %219 = load float, float* %a249, align 4, !tbaa !72
  %add250 = fadd float %219, %mul247
  store float %add250, float* %a249, align 4, !tbaa !72
  %r251 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %220 = load float, float* %r251, align 4, !tbaa !74
  %mul252 = fmul float %220, 4.375000e-01
  %221 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %222 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx253 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %221, i32 %222
  %r254 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx253, i32 0, i32 1
  %223 = load float, float* %r254, align 4, !tbaa !74
  %add255 = fadd float %223, %mul252
  store float %add255, float* %r254, align 4, !tbaa !74
  %g256 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %224 = load float, float* %g256, align 4, !tbaa !75
  %mul257 = fmul float %224, 4.375000e-01
  %225 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %226 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx258 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %225, i32 %226
  %g259 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx258, i32 0, i32 2
  %227 = load float, float* %g259, align 4, !tbaa !75
  %add260 = fadd float %227, %mul257
  store float %add260, float* %g259, align 4, !tbaa !75
  %b261 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %228 = load float, float* %b261, align 4, !tbaa !76
  %mul262 = fmul float %228, 4.375000e-01
  %229 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %230 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx263 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %229, i32 %230
  %b264 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx263, i32 0, i32 3
  %231 = load float, float* %b264, align 4, !tbaa !76
  %add265 = fadd float %231, %mul262
  store float %add265, float* %b264, align 4, !tbaa !76
  %a266 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %232 = load float, float* %a266, align 4, !tbaa !72
  %mul267 = fmul float %232, 6.250000e-02
  %233 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %234 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx268 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %233, i32 %234
  %a269 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx268, i32 0, i32 0
  store float %mul267, float* %a269, align 4, !tbaa !72
  %r270 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %235 = load float, float* %r270, align 4, !tbaa !74
  %mul271 = fmul float %235, 6.250000e-02
  %236 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %237 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx272 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %236, i32 %237
  %r273 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx272, i32 0, i32 1
  store float %mul271, float* %r273, align 4, !tbaa !74
  %g274 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %238 = load float, float* %g274, align 4, !tbaa !75
  %mul275 = fmul float %238, 6.250000e-02
  %239 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %240 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx276 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %239, i32 %240
  %g277 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx276, i32 0, i32 2
  store float %mul275, float* %g277, align 4, !tbaa !75
  %b278 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %241 = load float, float* %b278, align 4, !tbaa !76
  %mul279 = fmul float %241, 6.250000e-02
  %242 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %243 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx280 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %242, i32 %243
  %b281 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx280, i32 0, i32 3
  store float %mul279, float* %b281, align 4, !tbaa !76
  %a282 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %244 = load float, float* %a282, align 4, !tbaa !72
  %mul283 = fmul float %244, 3.125000e-01
  %245 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %246 = load i32, i32* %col, align 4, !tbaa !13
  %add284 = add nsw i32 %246, 1
  %arrayidx285 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %245, i32 %add284
  %a286 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx285, i32 0, i32 0
  %247 = load float, float* %a286, align 4, !tbaa !72
  %add287 = fadd float %247, %mul283
  store float %add287, float* %a286, align 4, !tbaa !72
  %r288 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %248 = load float, float* %r288, align 4, !tbaa !74
  %mul289 = fmul float %248, 3.125000e-01
  %249 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %250 = load i32, i32* %col, align 4, !tbaa !13
  %add290 = add nsw i32 %250, 1
  %arrayidx291 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %249, i32 %add290
  %r292 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx291, i32 0, i32 1
  %251 = load float, float* %r292, align 4, !tbaa !74
  %add293 = fadd float %251, %mul289
  store float %add293, float* %r292, align 4, !tbaa !74
  %g294 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %252 = load float, float* %g294, align 4, !tbaa !75
  %mul295 = fmul float %252, 3.125000e-01
  %253 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %254 = load i32, i32* %col, align 4, !tbaa !13
  %add296 = add nsw i32 %254, 1
  %arrayidx297 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %253, i32 %add296
  %g298 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx297, i32 0, i32 2
  %255 = load float, float* %g298, align 4, !tbaa !75
  %add299 = fadd float %255, %mul295
  store float %add299, float* %g298, align 4, !tbaa !75
  %b300 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %256 = load float, float* %b300, align 4, !tbaa !76
  %mul301 = fmul float %256, 3.125000e-01
  %257 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %258 = load i32, i32* %col, align 4, !tbaa !13
  %add302 = add nsw i32 %258, 1
  %arrayidx303 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %257, i32 %add302
  %b304 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx303, i32 0, i32 3
  %259 = load float, float* %b304, align 4, !tbaa !76
  %add305 = fadd float %259, %mul301
  store float %add305, float* %b304, align 4, !tbaa !76
  %a306 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 0
  %260 = load float, float* %a306, align 4, !tbaa !72
  %mul307 = fmul float %260, 1.875000e-01
  %261 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %262 = load i32, i32* %col, align 4, !tbaa !13
  %add308 = add nsw i32 %262, 2
  %arrayidx309 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %261, i32 %add308
  %a310 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx309, i32 0, i32 0
  %263 = load float, float* %a310, align 4, !tbaa !72
  %add311 = fadd float %263, %mul307
  store float %add311, float* %a310, align 4, !tbaa !72
  %r312 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 1
  %264 = load float, float* %r312, align 4, !tbaa !74
  %mul313 = fmul float %264, 1.875000e-01
  %265 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %266 = load i32, i32* %col, align 4, !tbaa !13
  %add314 = add nsw i32 %266, 2
  %arrayidx315 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %265, i32 %add314
  %r316 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx315, i32 0, i32 1
  %267 = load float, float* %r316, align 4, !tbaa !74
  %add317 = fadd float %267, %mul313
  store float %add317, float* %r316, align 4, !tbaa !74
  %g318 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 2
  %268 = load float, float* %g318, align 4, !tbaa !75
  %mul319 = fmul float %268, 1.875000e-01
  %269 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %270 = load i32, i32* %col, align 4, !tbaa !13
  %add320 = add nsw i32 %270, 2
  %arrayidx321 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %269, i32 %add320
  %g322 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx321, i32 0, i32 2
  %271 = load float, float* %g322, align 4, !tbaa !75
  %add323 = fadd float %271, %mul319
  store float %add323, float* %g322, align 4, !tbaa !75
  %b324 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %err, i32 0, i32 3
  %272 = load float, float* %b324, align 4, !tbaa !76
  %mul325 = fmul float %272, 1.875000e-01
  %273 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %274 = load i32, i32* %col, align 4, !tbaa !13
  %add326 = add nsw i32 %274, 2
  %arrayidx327 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %273, i32 %add326
  %b328 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %arrayidx327, i32 0, i32 3
  %275 = load float, float* %b328, align 4, !tbaa !76
  %add329 = fadd float %275, %mul325
  store float %add329, float* %b328, align 4, !tbaa !76
  br label %if.end330

if.end330:                                        ; preds = %if.else245, %if.then156
  %276 = load i32, i32* %fs_direction, align 4, !tbaa !13
  %277 = load i32, i32* %col, align 4, !tbaa !13
  %add331 = add nsw i32 %277, %276
  store i32 %add331, i32* %col, align 4, !tbaa !13
  %278 = load i32, i32* %fs_direction, align 4, !tbaa !13
  %cmp332 = icmp sgt i32 %278, 0
  br i1 %cmp332, label %if.then334, label %if.else339

if.then334:                                       ; preds = %if.end330
  %279 = load i32, i32* %col, align 4, !tbaa !13
  %280 = load i32, i32* %cols, align 4, !tbaa !13
  %cmp335 = icmp sge i32 %279, %280
  br i1 %cmp335, label %if.then337, label %if.end338

if.then337:                                       ; preds = %if.then334
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end338:                                        ; preds = %if.then334
  br label %if.end344

if.else339:                                       ; preds = %if.end330
  %281 = load i32, i32* %col, align 4, !tbaa !13
  %cmp340 = icmp slt i32 %281, 0
  br i1 %cmp340, label %if.then342, label %if.end343

if.then342:                                       ; preds = %if.else339
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end343:                                        ; preds = %if.else339
  br label %if.end344

if.end344:                                        ; preds = %if.end343, %if.end338
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end344, %if.then342, %if.then337
  %282 = bitcast %struct.f_pixel* %err to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %282) #8
  %283 = bitcast %struct.f_pixel* %output_px to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %283) #8
  %284 = bitcast float* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #8
  %285 = bitcast i32* %guessed_match to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %285) #8
  %286 = bitcast %struct.f_pixel* %spx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %286) #8
  %287 = bitcast float* %dither_level71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 5, label %do.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %do.cond

do.cond:                                          ; preds = %cleanup.cont
  br i1 true, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %cleanup
  %288 = bitcast %struct.f_pixel** %temperr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %288) #8
  %289 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  store %struct.f_pixel* %289, %struct.f_pixel** %temperr, align 4, !tbaa !2
  %290 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  store %struct.f_pixel* %290, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %291 = load %struct.f_pixel*, %struct.f_pixel** %temperr, align 4, !tbaa !2
  store %struct.f_pixel* %291, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %292 = load i32, i32* %fs_direction, align 4, !tbaa !13
  %sub350 = sub nsw i32 0, %292
  store i32 %sub350, i32* %fs_direction, align 4, !tbaa !13
  %293 = bitcast %struct.f_pixel** %temperr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #8
  %294 = bitcast %struct.f_pixel** %bg_pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #8
  %295 = bitcast %struct.f_pixel** %row_pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %295) #8
  %296 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #8
  br label %for.inc

for.inc:                                          ; preds = %do.end
  %297 = load i32, i32* %row, align 4, !tbaa !13
  %inc = add nsw i32 %297, 1
  store i32 %inc, i32* %row, align 4, !tbaa !13
  br label %for.cond

cleanup351:                                       ; preds = %if.then48, %for.cond.cleanup
  %298 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #8
  br label %for.end

for.end:                                          ; preds = %cleanup351
  %299 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_image, %struct.liq_image* %299, i32 0, i32 2
  %300 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !88
  %301 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  %302 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  %cmp352 = icmp ult %struct.f_pixel* %301, %302
  br i1 %cmp352, label %cond.true354, label %cond.false355

cond.true354:                                     ; preds = %for.end
  %303 = load %struct.f_pixel*, %struct.f_pixel** %thiserr, align 4, !tbaa !2
  br label %cond.end356

cond.false355:                                    ; preds = %for.end
  %304 = load %struct.f_pixel*, %struct.f_pixel** %nexterr, align 4, !tbaa !2
  br label %cond.end356

cond.end356:                                      ; preds = %cond.false355, %cond.true354
  %cond357 = phi %struct.f_pixel* [ %303, %cond.true354 ], [ %304, %cond.false355 ]
  %305 = bitcast %struct.f_pixel* %cond357 to i8*
  call void %300(i8* %305)
  %306 = load %struct.nearest_map*, %struct.nearest_map** %n, align 4, !tbaa !2
  call void @nearest_free(%struct.nearest_map* %306)
  %307 = load i8, i8* %ok, align 1, !tbaa !56, !range !29
  %tobool358 = trunc i8 %307 to i1
  store i1 %tobool358, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %308 = bitcast i32* %last_match to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #8
  %309 = bitcast i32* %fs_direction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %309) #8
  %310 = bitcast float* %base_dithering_level to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %310) #8
  %311 = bitcast i32* %transparent_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %311) #8
  %312 = bitcast %struct.nearest_map** %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %312) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ok) #8
  %313 = bitcast %struct.f_pixel** %nexterr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #8
  br label %cleanup366

cleanup366:                                       ; preds = %cond.end356, %if.then17
  %314 = bitcast %struct.f_pixel** %thiserr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #8
  %315 = bitcast i32* %errwidth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #8
  br label %cleanup368

cleanup368:                                       ; preds = %cleanup366, %if.then12, %if.then
  %316 = bitcast %struct.colormap_item** %acolormap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #8
  %317 = bitcast %struct.colormap** %map to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #8
  %318 = bitcast i8** %dither_map to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %318) #8
  %319 = bitcast i32* %cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #8
  %320 = bitcast i32* %rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #8
  %321 = load i1, i1* %retval, align 1
  ret i1 %321

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden i32 @liq_version() #4 {
entry:
  ret i32 21200
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #9

declare i8* @malloc(i32) #1

declare void @free(i8*) #1

; Function Attrs: nounwind
define internal zeroext i1 @liq_image_should_use_low_memory(%struct.liq_image* nonnull %img, i1 zeroext %low_memory_hint) #4 {
entry:
  %img.addr = alloca %struct.liq_image*, align 4
  %low_memory_hint.addr = alloca i8, align 1
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %frombool = zext i1 %low_memory_hint to i8
  store i8 %frombool, i8* %low_memory_hint.addr, align 1, !tbaa !56
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %0, i32 0, i32 6
  %1 = load i32, i32* %width, align 8, !tbaa !85
  %2 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 7
  %3 = load i32, i32* %height, align 4, !tbaa !84
  %mul = mul i32 %1, %3
  %4 = load i8, i8* %low_memory_hint.addr, align 1, !tbaa !56, !range !29
  %tobool = trunc i8 %4 to i1
  %5 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 8388608, i32 67108864
  %div = udiv i32 %cond, 16
  %cmp = icmp ugt i32 %mul, %div
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define internal void @verbose_print(%struct.liq_attr* nonnull %attr, i8* nonnull %msg) #7 {
entry:
  %attr.addr = alloca %struct.liq_attr*, align 4
  %msg.addr = alloca i8*, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  store i8* %msg, i8** %msg.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_callback = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %0, i32 0, i32 22
  %1 = load void (%struct.liq_attr*, i8*, i8*)*, void (%struct.liq_attr*, i8*, i8*)** %log_callback, align 4, !tbaa !48
  %tobool = icmp ne void (%struct.liq_attr*, i8*, i8*)* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_callback1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %2, i32 0, i32 22
  %3 = load void (%struct.liq_attr*, i8*, i8*)*, void (%struct.liq_attr*, i8*, i8*)** %log_callback1, align 4, !tbaa !48
  %4 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %5 = load i8*, i8** %msg.addr, align 4, !tbaa !2
  %6 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4, !tbaa !2
  %log_callback_user_info = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %6, i32 0, i32 23
  %7 = load i8*, i8** %log_callback_user_info, align 8, !tbaa !49
  call void %3(%struct.liq_attr* %4, i8* %5, i8* %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define internal zeroext i1 @liq_image_use_low_memory(%struct.liq_image* nonnull %img) #4 {
entry:
  %img.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_image, %struct.liq_image* %0, i32 0, i32 1
  %1 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !86
  %2 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 6
  %3 = load i32, i32* %width, align 8, !tbaa !85
  %mul = mul i32 16, %3
  %mul1 = mul i32 %mul, 1
  %call = call i8* %1(i32 %mul1)
  %4 = bitcast i8* %call to %struct.f_pixel*
  %5 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %temp_f_row = getelementptr inbounds %struct.liq_image, %struct.liq_image* %5, i32 0, i32 13
  store %struct.f_pixel* %4, %struct.f_pixel** %temp_f_row, align 4, !tbaa !92
  %6 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %temp_f_row2 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 13
  %7 = load %struct.f_pixel*, %struct.f_pixel** %temp_f_row2, align 4, !tbaa !92
  %cmp = icmp ne %struct.f_pixel* %7, null
  ret i1 %cmp
}

; Function Attrs: nounwind
define internal void (i8*)* @get_default_free_func(%struct.liq_image* nonnull %img) #4 {
entry:
  %retval = alloca void (i8*)*, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %free_rows_internal = getelementptr inbounds %struct.liq_image, %struct.liq_image* %0, i32 0, i32 22
  %1 = load i8, i8* %free_rows_internal, align 4, !tbaa !80, !range !29
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_image, %struct.liq_image* %2, i32 0, i32 2
  %3 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !88
  %cmp = icmp ne void (i8*)* %3, @liq_aligned_free
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %4 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %free1 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %4, i32 0, i32 2
  %5 = load void (i8*)*, void (i8*)** %free1, align 8, !tbaa !88
  store void (i8*)* %5, void (i8*)** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  store void (i8*)* @free, void (i8*)** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load void (i8*)*, void (i8*)** %retval, align 4
  ret void (i8*)* %6
}

; Function Attrs: nounwind
define internal i32 @finalize_histogram(%struct.liq_histogram* nonnull %input_hist, %struct.liq_attr* nonnull %options, %struct.histogram** nonnull %hist_output) #4 {
entry:
  %retval = alloca i32, align 4
  %input_hist.addr = alloca %struct.liq_histogram*, align 4
  %options.addr = alloca %struct.liq_attr*, align 4
  %hist_output.addr = alloca %struct.histogram**, align 4
  %hist = alloca %struct.histogram*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.liq_histogram* %input_hist, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  store %struct.liq_attr* %options, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  store %struct.histogram** %hist_output, %struct.histogram*** %hist_output.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %1 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 17
  %2 = load i8, i8* %progress_stage1, align 8, !tbaa !30
  %conv = zext i8 %2 to i32
  %conv1 = sitofp i32 %conv to float
  %mul = fmul float %conv1, 0x3FECCCCCC0000000
  %call = call zeroext i1 @liq_progress(%struct.liq_attr* %0, float %mul)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 102, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %3, i32 0, i32 3
  %4 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht, align 4, !tbaa !106
  %tobool = icmp ne %struct.acolorhash_table* %4, null
  br i1 %tobool, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i32 103, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %5 = bitcast %struct.histogram** %hist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht4 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %6, i32 0, i32 3
  %7 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht4, align 4, !tbaa !106
  %8 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %8, i32 0, i32 4
  %9 = load double, double* %gamma, align 8, !tbaa !107
  %10 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %10, i32 0, i32 1
  %11 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %12 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %12, i32 0, i32 2
  %13 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !53
  %call5 = call %struct.histogram* @pam_acolorhashtoacolorhist(%struct.acolorhash_table* %7, double %9, i8* (i32)* %11, void (i8*)* %13)
  store %struct.histogram* %call5, %struct.histogram** %hist, align 4, !tbaa !2
  %14 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht6 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %14, i32 0, i32 3
  %15 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht6, align 4, !tbaa !106
  call void @pam_freeacolorhash(%struct.acolorhash_table* %15)
  %16 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %acht7 = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %16, i32 0, i32 3
  store %struct.acolorhash_table* null, %struct.acolorhash_table** %acht7, align 4, !tbaa !106
  %17 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %tobool8 = icmp ne %struct.histogram* %17, null
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %if.end3
  store i32 101, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %if.end3
  %18 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %19 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.histogram, %struct.histogram* %19, i32 0, i32 3
  %20 = load i32, i32* %size, align 8, !tbaa !139
  call void (%struct.liq_attr*, i8*, ...) @liq_verbose_printf(%struct.liq_attr* %18, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.11, i32 0, i32 0), i32 %20)
  %21 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %22 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %fixed_colors_count = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %22, i32 0, i32 6
  %23 = load i16, i16* %fixed_colors_count, align 8, !tbaa !77
  %conv11 = zext i16 %23 to i32
  %24 = load %struct.liq_histogram*, %struct.liq_histogram** %input_hist.addr, align 4, !tbaa !2
  %fixed_colors = getelementptr inbounds %struct.liq_histogram, %struct.liq_histogram* %24, i32 0, i32 5
  %arraydecay = getelementptr inbounds [256 x %struct.f_pixel], [256 x %struct.f_pixel]* %fixed_colors, i32 0, i32 0
  %25 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %target_mse = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %25, i32 0, i32 3
  %26 = load double, double* %target_mse, align 8, !tbaa !14
  %conv12 = fptrunc double %26 to float
  call void @remove_fixed_colors_from_histogram(%struct.histogram* %21, i32 %conv11, %struct.f_pixel* %arraydecay, float %conv12)
  %27 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %28 = load %struct.histogram**, %struct.histogram*** %hist_output.addr, align 4, !tbaa !2
  store %struct.histogram* %27, %struct.histogram** %28, align 4, !tbaa !2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end10, %if.then9
  %29 = bitcast %struct.histogram** %hist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then2, %if.then
  %30 = load i32, i32* %retval, align 4
  ret i32 %30
}

; Function Attrs: nounwind
define internal i32 @pngquant_quantize(%struct.histogram* nonnull %hist, %struct.liq_attr* nonnull %options, i32 %fixed_colors_count, %struct.f_pixel* nonnull %fixed_colors, double %gamma, i1 zeroext %fixed_result_colors, %struct.liq_result** nonnull %result_output) #4 {
entry:
  %retval = alloca i32, align 4
  %hist.addr = alloca %struct.histogram*, align 4
  %options.addr = alloca %struct.liq_attr*, align 4
  %fixed_colors_count.addr = alloca i32, align 4
  %fixed_colors.addr = alloca %struct.f_pixel*, align 4
  %gamma.addr = alloca double, align 8
  %fixed_result_colors.addr = alloca i8, align 1
  %result_output.addr = alloca %struct.liq_result**, align 4
  %acolormap = alloca %struct.colormap*, align 4
  %palette_error = alloca double, align 8
  %few_input_colors = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %max_mse = alloca double, align 8
  %iteration_limit = alloca double, align 8
  %iterations = alloca i32, align 4
  %j = alloca i32, align 4
  %previous_palette_error = alloca double, align 8
  %i = alloca i32, align 4
  %i144 = alloca i32, align 4
  %result = alloca %struct.liq_result*, align 4
  %.compoundliteral = alloca %struct.liq_result, align 8
  store %struct.histogram* %hist, %struct.histogram** %hist.addr, align 4, !tbaa !2
  store %struct.liq_attr* %options, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  store i32 %fixed_colors_count, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  store %struct.f_pixel* %fixed_colors, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  store double %gamma, double* %gamma.addr, align 8, !tbaa !18
  %frombool = zext i1 %fixed_result_colors to i8
  store i8 %frombool, i8* %fixed_result_colors.addr, align 1, !tbaa !56
  store %struct.liq_result** %result_output, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  %0 = bitcast %struct.colormap** %acolormap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast double* %palette_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #8
  store double -1.000000e+00, double* %palette_error, align 8, !tbaa !18
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %few_input_colors) #8
  %2 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.histogram, %struct.histogram* %2, i32 0, i32 3
  %3 = load i32, i32* %size, align 8, !tbaa !139
  %4 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %add = add i32 %3, %4
  %5 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %max_colors = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %5, i32 0, i32 7
  %6 = load i32, i32* %max_colors, align 4, !tbaa !19
  %cmp = icmp ule i32 %add, %6
  %frombool1 = zext i1 %cmp to i8
  store i8 %frombool1, i8* %few_input_colors, align 1, !tbaa !56
  %7 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %8 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %8, i32 0, i32 17
  %9 = load i8, i8* %progress_stage1, align 8, !tbaa !30
  %conv = uitofp i8 %9 to float
  %call = call zeroext i1 @liq_progress(%struct.liq_attr* %7, float %conv)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 102, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup172

if.end:                                           ; preds = %entry
  %10 = load i8, i8* %few_input_colors, align 1, !tbaa !56, !range !29
  %tobool = trunc i8 %10 to i1
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.end
  %11 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %target_mse = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %11, i32 0, i32 3
  %12 = load double, double* %target_mse, align 8, !tbaa !14
  %cmp3 = fcmp oeq double %12, 0.000000e+00
  br i1 %cmp3, label %if.then5, label %if.else

if.then5:                                         ; preds = %land.lhs.true
  %13 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %14 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %call6 = call %struct.colormap* @histogram_to_palette(%struct.histogram* %13, %struct.liq_attr* %14)
  %15 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %max_colors7 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %15, i32 0, i32 7
  %16 = load i32, i32* %max_colors7, align 4, !tbaa !19
  %17 = load %struct.f_pixel*, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  %18 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %19 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %19, i32 0, i32 1
  %20 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %21 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %21, i32 0, i32 2
  %22 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !53
  %call8 = call %struct.colormap* @add_fixed_colors_to_palette(%struct.colormap* %call6, i32 %16, %struct.f_pixel* %17, i32 %18, i8* (i32)* %20, void (i8*)* %22)
  store %struct.colormap* %call8, %struct.colormap** %acolormap, align 4, !tbaa !2
  store double 0.000000e+00, double* %palette_error, align 8, !tbaa !18
  br label %if.end127

if.else:                                          ; preds = %land.lhs.true, %if.end
  %23 = bitcast double* %max_mse to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %23) #8
  %24 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %max_mse9 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %24, i32 0, i32 4
  %25 = load double, double* %max_mse9, align 8, !tbaa !15
  %26 = load i8, i8* %few_input_colors, align 1, !tbaa !56, !range !29
  %tobool10 = trunc i8 %26 to i1
  %27 = zext i1 %tobool10 to i64
  %cond = select i1 %tobool10, double 3.300000e-01, double 1.000000e+00
  %mul = fmul double %25, %cond
  store double %mul, double* %max_mse, align 8, !tbaa !18
  %28 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %29 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %30 = load double, double* %max_mse, align 8, !tbaa !18
  %31 = load %struct.f_pixel*, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  %32 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %call12 = call %struct.colormap* @find_best_palette(%struct.histogram* %28, %struct.liq_attr* %29, double %30, %struct.f_pixel* %31, i32 %32, double* %palette_error)
  store %struct.colormap* %call12, %struct.colormap** %acolormap, align 4, !tbaa !2
  %33 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %tobool13 = icmp ne %struct.colormap* %33, null
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %if.else
  store i32 100, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup126

if.end15:                                         ; preds = %if.else
  %34 = bitcast double* %iteration_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %34) #8
  %35 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %kmeans_iteration_limit = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %35, i32 0, i32 5
  %36 = load double, double* %kmeans_iteration_limit, align 8, !tbaa !22
  store double %36, double* %iteration_limit, align 8, !tbaa !18
  %37 = bitcast i32* %iterations to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %kmeans_iterations = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %38, i32 0, i32 11
  %39 = load i32, i32* %kmeans_iterations, align 4, !tbaa !21
  store i32 %39, i32* %iterations, align 4, !tbaa !13
  %40 = load i32, i32* %iterations, align 4, !tbaa !13
  %tobool16 = icmp ne i32 %40, 0
  br i1 %tobool16, label %if.end24, label %land.lhs.true17

land.lhs.true17:                                  ; preds = %if.end15
  %41 = load double, double* %palette_error, align 8, !tbaa !18
  %cmp18 = fcmp olt double %41, 0.000000e+00
  br i1 %cmp18, label %land.lhs.true20, label %if.end24

land.lhs.true20:                                  ; preds = %land.lhs.true17
  %42 = load double, double* %max_mse, align 8, !tbaa !18
  %cmp21 = fcmp olt double %42, 1.000000e+20
  br i1 %cmp21, label %if.then23, label %if.end24

if.then23:                                        ; preds = %land.lhs.true20
  store i32 1, i32* %iterations, align 4, !tbaa !13
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %land.lhs.true20, %land.lhs.true17, %if.end15
  %43 = load i32, i32* %iterations, align 4, !tbaa !13
  %tobool25 = icmp ne i32 %43, 0
  br i1 %tobool25, label %if.then26, label %if.end115

if.then26:                                        ; preds = %if.end24
  %44 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %44, i32 0, i32 0
  %45 = load i32, i32* %colors, align 4, !tbaa !13
  %cmp27 = icmp ult i32 %45, 256
  br i1 %cmp27, label %if.then29, label %if.end43

if.then29:                                        ; preds = %if.then26
  %46 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then29
  %47 = load i32, i32* %j, align 4, !tbaa !13
  %48 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size30 = getelementptr inbounds %struct.histogram, %struct.histogram* %48, i32 0, i32 3
  %49 = load i32, i32* %size30, align 8, !tbaa !139
  %cmp31 = icmp ult i32 %47, %49
  br i1 %cmp31, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %50 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %51 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv = getelementptr inbounds %struct.histogram, %struct.histogram* %51, i32 0, i32 0
  %52 = load %struct.hist_item*, %struct.hist_item** %achv, align 8, !tbaa !141
  %53 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %52, i32 %53
  %tmp = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 4
  %likely_colormap_index = bitcast %union.anon* %tmp to i8*
  %54 = load i8, i8* %likely_colormap_index, align 4, !tbaa !12
  %conv33 = zext i8 %54 to i32
  %55 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %colors34 = getelementptr inbounds %struct.colormap, %struct.colormap* %55, i32 0, i32 0
  %56 = load i32, i32* %colors34, align 4, !tbaa !13
  %cmp35 = icmp uge i32 %conv33, %56
  br i1 %cmp35, label %if.then37, label %if.end42

if.then37:                                        ; preds = %for.body
  %57 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv38 = getelementptr inbounds %struct.histogram, %struct.histogram* %57, i32 0, i32 0
  %58 = load %struct.hist_item*, %struct.hist_item** %achv38, align 8, !tbaa !141
  %59 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx39 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %58, i32 %59
  %tmp40 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx39, i32 0, i32 4
  %likely_colormap_index41 = bitcast %union.anon* %tmp40 to i8*
  store i8 0, i8* %likely_colormap_index41, align 4, !tbaa !12
  br label %if.end42

if.end42:                                         ; preds = %if.then37, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end42
  %60 = load i32, i32* %j, align 4, !tbaa !13
  %inc = add i32 %60, 1
  store i32 %inc, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end43

if.end43:                                         ; preds = %for.end, %if.then26
  %61 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size44 = getelementptr inbounds %struct.histogram, %struct.histogram* %61, i32 0, i32 3
  %62 = load i32, i32* %size44, align 8, !tbaa !139
  %cmp45 = icmp ugt i32 %62, 5000
  br i1 %cmp45, label %if.then47, label %if.end50

if.then47:                                        ; preds = %if.end43
  %63 = load i32, i32* %iterations, align 4, !tbaa !13
  %mul48 = mul i32 %63, 3
  %add49 = add i32 %mul48, 3
  %div = udiv i32 %add49, 4
  store i32 %div, i32* %iterations, align 4, !tbaa !13
  br label %if.end50

if.end50:                                         ; preds = %if.then47, %if.end43
  %64 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size51 = getelementptr inbounds %struct.histogram, %struct.histogram* %64, i32 0, i32 3
  %65 = load i32, i32* %size51, align 8, !tbaa !139
  %cmp52 = icmp ugt i32 %65, 25000
  br i1 %cmp52, label %if.then54, label %if.end58

if.then54:                                        ; preds = %if.end50
  %66 = load i32, i32* %iterations, align 4, !tbaa !13
  %mul55 = mul i32 %66, 3
  %add56 = add i32 %mul55, 3
  %div57 = udiv i32 %add56, 4
  store i32 %div57, i32* %iterations, align 4, !tbaa !13
  br label %if.end58

if.end58:                                         ; preds = %if.then54, %if.end50
  %67 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size59 = getelementptr inbounds %struct.histogram, %struct.histogram* %67, i32 0, i32 3
  %68 = load i32, i32* %size59, align 8, !tbaa !139
  %cmp60 = icmp ugt i32 %68, 50000
  br i1 %cmp60, label %if.then62, label %if.end66

if.then62:                                        ; preds = %if.end58
  %69 = load i32, i32* %iterations, align 4, !tbaa !13
  %mul63 = mul i32 %69, 3
  %add64 = add i32 %mul63, 3
  %div65 = udiv i32 %add64, 4
  store i32 %div65, i32* %iterations, align 4, !tbaa !13
  br label %if.end66

if.end66:                                         ; preds = %if.then62, %if.end58
  %70 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size67 = getelementptr inbounds %struct.histogram, %struct.histogram* %70, i32 0, i32 3
  %71 = load i32, i32* %size67, align 8, !tbaa !139
  %cmp68 = icmp ugt i32 %71, 100000
  br i1 %cmp68, label %if.then70, label %if.end75

if.then70:                                        ; preds = %if.end66
  %72 = load i32, i32* %iterations, align 4, !tbaa !13
  %mul71 = mul i32 %72, 3
  %add72 = add i32 %mul71, 3
  %div73 = udiv i32 %add72, 4
  store i32 %div73, i32* %iterations, align 4, !tbaa !13
  %73 = load double, double* %iteration_limit, align 8, !tbaa !18
  %mul74 = fmul double %73, 2.000000e+00
  store double %mul74, double* %iteration_limit, align 8, !tbaa !18
  br label %if.end75

if.end75:                                         ; preds = %if.then70, %if.end66
  %74 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  call void @verbose_print(%struct.liq_attr* %74, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.12, i32 0, i32 0))
  %75 = bitcast double* %previous_palette_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %75) #8
  store double 1.000000e+20, double* %previous_palette_error, align 8, !tbaa !18
  %76 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond76

for.cond76:                                       ; preds = %for.inc112, %if.end75
  %77 = load i32, i32* %i, align 4, !tbaa !13
  %78 = load i32, i32* %iterations, align 4, !tbaa !13
  %cmp77 = icmp ult i32 %77, %78
  br i1 %cmp77, label %for.body80, label %for.cond.cleanup79

for.cond.cleanup79:                               ; preds = %for.cond76
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body80:                                       ; preds = %for.cond76
  %79 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %80 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %call81 = call double @kmeans_do_iteration(%struct.histogram* %79, %struct.colormap* %80, void (%struct.hist_item*, float)* null)
  store double %call81, double* %palette_error, align 8, !tbaa !18
  %81 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %82 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage182 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %82, i32 0, i32 17
  %83 = load i8, i8* %progress_stage182, align 8, !tbaa !30
  %conv83 = zext i8 %83 to i32
  %84 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage2 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %84, i32 0, i32 18
  %85 = load i8, i8* %progress_stage2, align 1, !tbaa !32
  %conv84 = zext i8 %85 to i32
  %add85 = add nsw i32 %conv83, %conv84
  %conv86 = sitofp i32 %add85 to float
  %86 = load i32, i32* %i, align 4, !tbaa !13
  %87 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage3 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %87, i32 0, i32 19
  %88 = load i8, i8* %progress_stage3, align 2, !tbaa !31
  %conv87 = zext i8 %88 to i32
  %mul88 = mul i32 %86, %conv87
  %conv89 = uitofp i32 %mul88 to float
  %mul90 = fmul float %conv89, 0x3FECCCCCC0000000
  %89 = load i32, i32* %iterations, align 4, !tbaa !13
  %conv91 = uitofp i32 %89 to float
  %div92 = fdiv float %mul90, %conv91
  %add93 = fadd float %conv86, %div92
  %call94 = call zeroext i1 @liq_progress(%struct.liq_attr* %81, float %add93)
  br i1 %call94, label %if.then95, label %if.end96

if.then95:                                        ; preds = %for.body80
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end96:                                         ; preds = %for.body80
  %90 = load double, double* %previous_palette_error, align 8, !tbaa !18
  %91 = load double, double* %palette_error, align 8, !tbaa !18
  %sub = fsub double %90, %91
  %92 = call double @llvm.fabs.f64(double %sub)
  %93 = load double, double* %iteration_limit, align 8, !tbaa !18
  %cmp97 = fcmp olt double %92, %93
  br i1 %cmp97, label %if.then99, label %if.end100

if.then99:                                        ; preds = %if.end96
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end100:                                        ; preds = %if.end96
  %94 = load double, double* %palette_error, align 8, !tbaa !18
  %95 = load double, double* %max_mse, align 8, !tbaa !18
  %mul101 = fmul double %95, 1.500000e+00
  %cmp102 = fcmp ogt double %94, %mul101
  br i1 %cmp102, label %if.then104, label %if.end111

if.then104:                                       ; preds = %if.end100
  %96 = load double, double* %palette_error, align 8, !tbaa !18
  %97 = load double, double* %max_mse, align 8, !tbaa !18
  %mul105 = fmul double %97, 3.000000e+00
  %cmp106 = fcmp ogt double %96, %mul105
  br i1 %cmp106, label %if.then108, label %if.end109

if.then108:                                       ; preds = %if.then104
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end109:                                        ; preds = %if.then104
  %98 = load i32, i32* %i, align 4, !tbaa !13
  %inc110 = add i32 %98, 1
  store i32 %inc110, i32* %i, align 4, !tbaa !13
  br label %if.end111

if.end111:                                        ; preds = %if.end109, %if.end100
  %99 = load double, double* %palette_error, align 8, !tbaa !18
  store double %99, double* %previous_palette_error, align 8, !tbaa !18
  br label %for.inc112

for.inc112:                                       ; preds = %if.end111
  %100 = load i32, i32* %i, align 4, !tbaa !13
  %inc113 = add i32 %100, 1
  store i32 %inc113, i32* %i, align 4, !tbaa !13
  br label %for.cond76

cleanup:                                          ; preds = %if.then108, %if.then99, %if.then95, %for.cond.cleanup79
  %101 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  br label %for.end114

for.end114:                                       ; preds = %cleanup
  %102 = bitcast double* %previous_palette_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %102) #8
  br label %if.end115

if.end115:                                        ; preds = %for.end114, %if.end24
  %103 = load double, double* %palette_error, align 8, !tbaa !18
  %104 = load double, double* %max_mse, align 8, !tbaa !18
  %cmp116 = fcmp ogt double %103, %104
  br i1 %cmp116, label %if.then118, label %if.end123

if.then118:                                       ; preds = %if.end115
  %105 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %106 = load double, double* %palette_error, align 8, !tbaa !18
  %call119 = call double @mse_to_standard_mse(double %106)
  %107 = load double, double* %palette_error, align 8, !tbaa !18
  %call120 = call i32 @mse_to_quality(double %107)
  %108 = load double, double* %max_mse, align 8, !tbaa !18
  %call121 = call double @mse_to_standard_mse(double %108)
  %109 = load double, double* %max_mse, align 8, !tbaa !18
  %call122 = call i32 @mse_to_quality(double %109)
  call void (%struct.liq_attr*, i8*, ...) @liq_verbose_printf(%struct.liq_attr* %105, i8* getelementptr inbounds ([64 x i8], [64 x i8]* @.str.13, i32 0, i32 0), double %call119, i32 %call120, double %call121, i32 %call122)
  %110 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  call void @pam_freecolormap(%struct.colormap* %110)
  store i32 99, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup124

if.end123:                                        ; preds = %if.end115
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup124

cleanup124:                                       ; preds = %if.end123, %if.then118
  %111 = bitcast i32* %iterations to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #8
  %112 = bitcast double* %iteration_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %112) #8
  br label %cleanup126

cleanup126:                                       ; preds = %cleanup124, %if.then14
  %113 = bitcast double* %max_mse to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %113) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup172 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup126
  br label %if.end127

if.end127:                                        ; preds = %cleanup.cont, %if.then5
  %114 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %115 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage1128 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %115, i32 0, i32 17
  %116 = load i8, i8* %progress_stage1128, align 8, !tbaa !30
  %conv129 = zext i8 %116 to i32
  %117 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage2130 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %117, i32 0, i32 18
  %118 = load i8, i8* %progress_stage2130, align 1, !tbaa !32
  %conv131 = zext i8 %118 to i32
  %add132 = add nsw i32 %conv129, %conv131
  %conv133 = sitofp i32 %add132 to float
  %119 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage3134 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %119, i32 0, i32 19
  %120 = load i8, i8* %progress_stage3134, align 2, !tbaa !31
  %conv135 = zext i8 %120 to i32
  %conv136 = sitofp i32 %conv135 to float
  %mul137 = fmul float %conv136, 0x3FEE666660000000
  %add138 = fadd float %conv133, %mul137
  %call139 = call zeroext i1 @liq_progress(%struct.liq_attr* %114, float %add138)
  br i1 %call139, label %if.then140, label %if.end141

if.then140:                                       ; preds = %if.end127
  %121 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  call void @pam_freecolormap(%struct.colormap* %121)
  store i32 102, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup172

if.end141:                                        ; preds = %if.end127
  %122 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %123 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  call void @sort_palette(%struct.colormap* %122, %struct.liq_attr* %123)
  %124 = load i8, i8* %fixed_result_colors.addr, align 1, !tbaa !56, !range !29
  %tobool142 = trunc i8 %124 to i1
  br i1 %tobool142, label %if.then143, label %if.end156

if.then143:                                       ; preds = %if.end141
  %125 = bitcast i32* %i144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #8
  store i32 0, i32* %i144, align 4, !tbaa !13
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc152, %if.then143
  %126 = load i32, i32* %i144, align 4, !tbaa !13
  %127 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %colors146 = getelementptr inbounds %struct.colormap, %struct.colormap* %127, i32 0, i32 0
  %128 = load i32, i32* %colors146, align 4, !tbaa !13
  %cmp147 = icmp ult i32 %126, %128
  br i1 %cmp147, label %for.body150, label %for.cond.cleanup149

for.cond.cleanup149:                              ; preds = %for.cond145
  store i32 8, i32* %cleanup.dest.slot, align 4
  %129 = bitcast i32* %i144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #8
  br label %for.end155

for.body150:                                      ; preds = %for.cond145
  %130 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %130, i32 0, i32 3
  %131 = load i32, i32* %i144, align 4, !tbaa !13
  %arrayidx151 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 %131
  %fixed = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx151, i32 0, i32 2
  store i8 1, i8* %fixed, align 4, !tbaa !119
  br label %for.inc152

for.inc152:                                       ; preds = %for.body150
  %132 = load i32, i32* %i144, align 4, !tbaa !13
  %inc153 = add i32 %132, 1
  store i32 %inc153, i32* %i144, align 4, !tbaa !13
  br label %for.cond145

for.end155:                                       ; preds = %for.cond.cleanup149
  br label %if.end156

if.end156:                                        ; preds = %for.end155, %if.end141
  %133 = bitcast %struct.liq_result** %result to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %133) #8
  %134 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc157 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %134, i32 0, i32 1
  %135 = load i8* (i32)*, i8* (i32)** %malloc157, align 4, !tbaa !52
  %call158 = call i8* %135(i32 1088)
  %136 = bitcast i8* %call158 to %struct.liq_result*
  store %struct.liq_result* %136, %struct.liq_result** %result, align 4, !tbaa !2
  %137 = load %struct.liq_result*, %struct.liq_result** %result, align 4, !tbaa !2
  %tobool159 = icmp ne %struct.liq_result* %137, null
  br i1 %tobool159, label %if.end161, label %if.then160

if.then160:                                       ; preds = %if.end156
  store i32 101, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

if.end161:                                        ; preds = %if.end156
  %138 = load %struct.liq_result*, %struct.liq_result** %result, align 4, !tbaa !2
  %139 = bitcast %struct.liq_result* %.compoundliteral to i8*
  call void @llvm.memset.p0i8.i64(i8* align 8 %139, i8 0, i64 1088, i1 false)
  %magic_header = getelementptr inbounds %struct.liq_result, %struct.liq_result* %.compoundliteral, i32 0, i32 0
  store i8* getelementptr inbounds ([11 x i8], [11 x i8]* @liq_result_magic, i32 0, i32 0), i8** %magic_header, align 8, !tbaa !111
  %malloc162 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %.compoundliteral, i32 0, i32 1
  %140 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc163 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %140, i32 0, i32 1
  %141 = load i8* (i32)*, i8* (i32)** %malloc163, align 4, !tbaa !52
  store i8* (i32)* %141, i8* (i32)** %malloc162, align 4, !tbaa !133
  %free164 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %.compoundliteral, i32 0, i32 2
  %142 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %free165 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %142, i32 0, i32 2
  %143 = load void (i8*)*, void (i8*)** %free165, align 8, !tbaa !53
  store void (i8*)* %143, void (i8*)** %free164, align 8, !tbaa !112
  %palette166 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %.compoundliteral, i32 0, i32 4
  %144 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  store %struct.colormap* %144, %struct.colormap** %palette166, align 8, !tbaa !110
  %gamma167 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %.compoundliteral, i32 0, i32 9
  %145 = load double, double* %gamma.addr, align 8, !tbaa !18
  store double %145, double* %gamma167, align 8, !tbaa !36
  %palette_error168 = getelementptr inbounds %struct.liq_result, %struct.liq_result* %.compoundliteral, i32 0, i32 10
  %146 = load double, double* %palette_error, align 8, !tbaa !18
  store double %146, double* %palette_error168, align 8, !tbaa !113
  %min_posterization_output = getelementptr inbounds %struct.liq_result, %struct.liq_result* %.compoundliteral, i32 0, i32 11
  %147 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %min_posterization_output169 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %147, i32 0, i32 9
  %148 = load i32, i32* %min_posterization_output169, align 4, !tbaa !20
  store i32 %148, i32* %min_posterization_output, align 8, !tbaa !117
  %use_dither_map = getelementptr inbounds %struct.liq_result, %struct.liq_result* %.compoundliteral, i32 0, i32 12
  %149 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %use_dither_map170 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %149, i32 0, i32 15
  %150 = load i8, i8* %use_dither_map170, align 2, !tbaa !26
  store i8 %150, i8* %use_dither_map, align 4, !tbaa !128
  %151 = bitcast %struct.liq_result* %138 to i8*
  %152 = bitcast %struct.liq_result* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %151, i8* align 8 %152, i32 1088, i1 false), !tbaa.struct !142
  %153 = load %struct.liq_result*, %struct.liq_result** %result, align 4, !tbaa !2
  %154 = load %struct.liq_result**, %struct.liq_result*** %result_output.addr, align 4, !tbaa !2
  store %struct.liq_result* %153, %struct.liq_result** %154, align 4, !tbaa !2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup171

cleanup171:                                       ; preds = %if.end161, %if.then160
  %155 = bitcast %struct.liq_result** %result to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #8
  br label %cleanup172

cleanup172:                                       ; preds = %cleanup171, %if.then140, %cleanup126, %if.then
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %few_input_colors) #8
  %156 = bitcast double* %palette_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %156) #8
  %157 = bitcast %struct.colormap** %acolormap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #8
  %158 = load i32, i32* %retval, align 4
  ret i32 %158
}

declare hidden void @pam_freeacolorhist(%struct.histogram*) #1

declare hidden %struct.histogram* @pam_acolorhashtoacolorhist(%struct.acolorhash_table*, double, i8* (i32)*, void (i8*)*) #1

; Function Attrs: nounwind
define internal void @remove_fixed_colors_from_histogram(%struct.histogram* nonnull %hist, i32 %fixed_colors_count, %struct.f_pixel* nonnull %fixed_colors, float %target_mse) #4 {
entry:
  %hist.addr = alloca %struct.histogram*, align 4
  %fixed_colors_count.addr = alloca i32, align 4
  %fixed_colors.addr = alloca %struct.f_pixel*, align 4
  %target_mse.addr = alloca float, align 4
  %max_difference = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.histogram* %hist, %struct.histogram** %hist.addr, align 4, !tbaa !2
  store i32 %fixed_colors_count, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  store %struct.f_pixel* %fixed_colors, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  store float %target_mse, float* %target_mse.addr, align 4, !tbaa !55
  %0 = bitcast float* %max_difference to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float, float* %target_mse.addr, align 4, !tbaa !55
  %div = fdiv float %1, 2.000000e+00
  %cmp = fcmp ogt float %div, 0x3F00000000000000
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load float, float* %target_mse.addr, align 4, !tbaa !55
  %div1 = fdiv float %2, 2.000000e+00
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div1, %cond.true ], [ 0x3F00000000000000, %cond.false ]
  store float %cond, float* %max_difference, align 4, !tbaa !55
  %3 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.end20

if.then:                                          ; preds = %cond.end
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc16, %if.then
  %5 = load i32, i32* %j, align 4, !tbaa !13
  %6 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.histogram, %struct.histogram* %6, i32 0, i32 3
  %7 = load i32, i32* %size, align 8, !tbaa !139
  %cmp2 = icmp ult i32 %5, %7
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %8 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  br label %for.end19

for.body:                                         ; preds = %for.cond
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !13
  %11 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %cmp4 = icmp ult i32 %10, %11
  br i1 %cmp4, label %for.body6, label %for.cond.cleanup5

for.cond.cleanup5:                                ; preds = %for.cond3
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body6:                                        ; preds = %for.cond3
  %12 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv = getelementptr inbounds %struct.histogram, %struct.histogram* %12, i32 0, i32 0
  %13 = load %struct.hist_item*, %struct.hist_item** %achv, align 8, !tbaa !141
  %14 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %13, i32 %14
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 0
  %15 = load %struct.f_pixel*, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx7 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %15, i32 %16
  %call = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor, %struct.f_pixel* byval(%struct.f_pixel) align 4 %arrayidx7)
  %17 = load float, float* %max_difference, align 4, !tbaa !55
  %cmp8 = fcmp olt float %call, %17
  br i1 %cmp8, label %if.then9, label %if.end

if.then9:                                         ; preds = %for.body6
  %18 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv10 = getelementptr inbounds %struct.histogram, %struct.histogram* %18, i32 0, i32 0
  %19 = load %struct.hist_item*, %struct.hist_item** %achv10, align 8, !tbaa !141
  %20 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx11 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %19, i32 %20
  %21 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv12 = getelementptr inbounds %struct.histogram, %struct.histogram* %21, i32 0, i32 0
  %22 = load %struct.hist_item*, %struct.hist_item** %achv12, align 8, !tbaa !141
  %23 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size13 = getelementptr inbounds %struct.histogram, %struct.histogram* %23, i32 0, i32 3
  %24 = load i32, i32* %size13, align 8, !tbaa !139
  %dec = add i32 %24, -1
  store i32 %dec, i32* %size13, align 8, !tbaa !139
  %arrayidx14 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %22, i32 %dec
  %25 = bitcast %struct.hist_item* %arrayidx11 to i8*
  %26 = bitcast %struct.hist_item* %arrayidx14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 32, i1 false), !tbaa.struct !143
  %27 = load i32, i32* %j, align 4, !tbaa !13
  %dec15 = add nsw i32 %27, -1
  store i32 %dec15, i32* %j, align 4, !tbaa !13
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %28 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond3

cleanup:                                          ; preds = %if.then9, %for.cond.cleanup5
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  br label %for.end

for.end:                                          ; preds = %cleanup
  br label %for.inc16

for.inc16:                                        ; preds = %for.end
  %30 = load i32, i32* %j, align 4, !tbaa !13
  %inc17 = add nsw i32 %30, 1
  store i32 %inc17, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.end19:                                        ; preds = %for.cond.cleanup
  br label %if.end20

if.end20:                                         ; preds = %for.end19, %cond.end
  %31 = bitcast float* %max_difference to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py) #5 {
entry:
  %call = call float @colordifference_stdc(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py)
  ret float %call
}

; Function Attrs: alwaysinline nounwind
define internal float @colordifference_stdc(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py) #5 {
entry:
  %alphas = alloca double, align 8
  %0 = bitcast double* %alphas to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #8
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 0
  %1 = load float, float* %a, align 4, !tbaa !72
  %a1 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %2 = load float, float* %a1, align 4, !tbaa !72
  %sub = fsub float %1, %2
  %conv = fpext float %sub to double
  store double %conv, double* %alphas, align 8, !tbaa !18
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %3 = load float, float* %r, align 4, !tbaa !74
  %conv2 = fpext float %3 to double
  %r3 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 1
  %4 = load float, float* %r3, align 4, !tbaa !74
  %conv4 = fpext float %4 to double
  %5 = load double, double* %alphas, align 8, !tbaa !18
  %call = call double @colordifference_ch(double %conv2, double %conv4, double %5)
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %6 = load float, float* %g, align 4, !tbaa !75
  %conv5 = fpext float %6 to double
  %g6 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 2
  %7 = load float, float* %g6, align 4, !tbaa !75
  %conv7 = fpext float %7 to double
  %8 = load double, double* %alphas, align 8, !tbaa !18
  %call8 = call double @colordifference_ch(double %conv5, double %conv7, double %8)
  %add = fadd double %call, %call8
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %9 = load float, float* %b, align 4, !tbaa !76
  %conv9 = fpext float %9 to double
  %b10 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 3
  %10 = load float, float* %b10, align 4, !tbaa !76
  %conv11 = fpext float %10 to double
  %11 = load double, double* %alphas, align 8, !tbaa !18
  %call12 = call double @colordifference_ch(double %conv9, double %conv11, double %11)
  %add13 = fadd double %add, %call12
  %conv14 = fptrunc double %add13 to float
  %12 = bitcast double* %alphas to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #8
  ret float %conv14
}

; Function Attrs: alwaysinline nounwind
define internal double @colordifference_ch(double %x, double %y, double %alphas) #5 {
entry:
  %x.addr = alloca double, align 8
  %y.addr = alloca double, align 8
  %alphas.addr = alloca double, align 8
  %black = alloca double, align 8
  %white = alloca double, align 8
  store double %x, double* %x.addr, align 8, !tbaa !18
  store double %y, double* %y.addr, align 8, !tbaa !18
  store double %alphas, double* %alphas.addr, align 8, !tbaa !18
  %0 = bitcast double* %black to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #8
  %1 = load double, double* %x.addr, align 8, !tbaa !18
  %2 = load double, double* %y.addr, align 8, !tbaa !18
  %sub = fsub double %1, %2
  store double %sub, double* %black, align 8, !tbaa !18
  %3 = bitcast double* %white to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #8
  %4 = load double, double* %black, align 8, !tbaa !18
  %5 = load double, double* %alphas.addr, align 8, !tbaa !18
  %add = fadd double %4, %5
  store double %add, double* %white, align 8, !tbaa !18
  %6 = load double, double* %black, align 8, !tbaa !18
  %7 = load double, double* %black, align 8, !tbaa !18
  %mul = fmul double %6, %7
  %8 = load double, double* %white, align 8, !tbaa !18
  %9 = load double, double* %white, align 8, !tbaa !18
  %mul1 = fmul double %8, %9
  %cmp = fcmp ogt double %mul, %mul1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load double, double* %black, align 8, !tbaa !18
  %11 = load double, double* %black, align 8, !tbaa !18
  %mul2 = fmul double %10, %11
  br label %cond.end

cond.false:                                       ; preds = %entry
  %12 = load double, double* %white, align 8, !tbaa !18
  %13 = load double, double* %white, align 8, !tbaa !18
  %mul3 = fmul double %12, %13
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %mul2, %cond.true ], [ %mul3, %cond.false ]
  %14 = bitcast double* %white to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %14) #8
  %15 = bitcast double* %black to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #8
  ret double %cond
}

; Function Attrs: nounwind
define internal %struct.colormap* @add_fixed_colors_to_palette(%struct.colormap* %palette, i32 %max_colors, %struct.f_pixel* %fixed_colors, i32 %fixed_colors_count, i8* (i32)* %malloc, void (i8*)* %free) #4 {
entry:
  %retval = alloca %struct.colormap*, align 4
  %palette.addr = alloca %struct.colormap*, align 4
  %max_colors.addr = alloca i32, align 4
  %fixed_colors.addr = alloca %struct.f_pixel*, align 4
  %fixed_colors_count.addr = alloca i32, align 4
  %malloc.addr = alloca i8* (i32)*, align 4
  %free.addr = alloca void (i8*)*, align 4
  %newpal = alloca %struct.colormap*, align 4
  %i = alloca i32, align 4
  %palette_max = alloca i32, align 4
  %j = alloca i32, align 4
  %.compoundliteral = alloca %struct.colormap_item, align 4
  store %struct.colormap* %palette, %struct.colormap** %palette.addr, align 4, !tbaa !2
  store i32 %max_colors, i32* %max_colors.addr, align 4, !tbaa !13
  store %struct.f_pixel* %fixed_colors, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  store i32 %fixed_colors_count, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  store i8* (i32)* %malloc, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  store void (i8*)* %free, void (i8*)** %free.addr, align 4, !tbaa !2
  %0 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %1 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  store %struct.colormap* %1, %struct.colormap** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %struct.colormap** %newpal to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load i32, i32* %max_colors.addr, align 4, !tbaa !13
  %4 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.colormap* %4, null
  br i1 %tobool1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %5 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %5, i32 0, i32 0
  %6 = load i32, i32* %colors, align 4, !tbaa !13
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %6, %cond.true ], [ 0, %cond.false ]
  %7 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %add = add i32 %cond, %7
  %cmp = icmp ult i32 %3, %add
  br i1 %cmp, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.end
  %8 = load i32, i32* %max_colors.addr, align 4, !tbaa !13
  br label %cond.end11

cond.false3:                                      ; preds = %cond.end
  %9 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %tobool4 = icmp ne %struct.colormap* %9, null
  br i1 %tobool4, label %cond.true5, label %cond.false7

cond.true5:                                       ; preds = %cond.false3
  %10 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %colors6 = getelementptr inbounds %struct.colormap, %struct.colormap* %10, i32 0, i32 0
  %11 = load i32, i32* %colors6, align 4, !tbaa !13
  br label %cond.end8

cond.false7:                                      ; preds = %cond.false3
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true5
  %cond9 = phi i32 [ %11, %cond.true5 ], [ 0, %cond.false7 ]
  %12 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %add10 = add i32 %cond9, %12
  br label %cond.end11

cond.end11:                                       ; preds = %cond.end8, %cond.true2
  %cond12 = phi i32 [ %8, %cond.true2 ], [ %add10, %cond.end8 ]
  %13 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  %14 = load void (i8*)*, void (i8*)** %free.addr, align 4, !tbaa !2
  %call = call %struct.colormap* @pam_colormap(i32 %cond12, i8* (i32)* %13, void (i8*)* %14)
  store %struct.colormap* %call, %struct.colormap** %newpal, align 4, !tbaa !2
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  %16 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %tobool13 = icmp ne %struct.colormap* %16, null
  br i1 %tobool13, label %land.lhs.true, label %if.end28

land.lhs.true:                                    ; preds = %cond.end11
  %17 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %18 = load i32, i32* %max_colors.addr, align 4, !tbaa !13
  %cmp14 = icmp slt i32 %17, %18
  br i1 %cmp14, label %if.then15, label %if.end28

if.then15:                                        ; preds = %land.lhs.true
  %19 = bitcast i32* %palette_max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %colors16 = getelementptr inbounds %struct.colormap, %struct.colormap* %20, i32 0, i32 0
  %21 = load i32, i32* %colors16, align 4, !tbaa !13
  %22 = load i32, i32* %max_colors.addr, align 4, !tbaa !13
  %23 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %sub = sub nsw i32 %22, %23
  %cmp17 = icmp ult i32 %21, %sub
  br i1 %cmp17, label %cond.true18, label %cond.false20

cond.true18:                                      ; preds = %if.then15
  %24 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %colors19 = getelementptr inbounds %struct.colormap, %struct.colormap* %24, i32 0, i32 0
  %25 = load i32, i32* %colors19, align 4, !tbaa !13
  br label %cond.end22

cond.false20:                                     ; preds = %if.then15
  %26 = load i32, i32* %max_colors.addr, align 4, !tbaa !13
  %27 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %sub21 = sub nsw i32 %26, %27
  br label %cond.end22

cond.end22:                                       ; preds = %cond.false20, %cond.true18
  %cond23 = phi i32 [ %25, %cond.true18 ], [ %sub21, %cond.false20 ]
  store i32 %cond23, i32* %palette_max, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end22
  %28 = load i32, i32* %i, align 4, !tbaa !13
  %29 = load i32, i32* %palette_max, align 4, !tbaa !13
  %cmp24 = icmp ult i32 %28, %29
  br i1 %cmp24, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %30 = load %struct.colormap*, %struct.colormap** %newpal, align 4, !tbaa !2
  %palette25 = getelementptr inbounds %struct.colormap, %struct.colormap* %30, i32 0, i32 3
  %31 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette25, i32 0, i32 %31
  %32 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %palette26 = getelementptr inbounds %struct.colormap, %struct.colormap* %32, i32 0, i32 3
  %33 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx27 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette26, i32 0, i32 %33
  %34 = bitcast %struct.colormap_item* %arrayidx to i8*
  %35 = bitcast %struct.colormap_item* %arrayidx27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 24, i1 false), !tbaa.struct !144
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %36 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = bitcast i32* %palette_max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  br label %if.end28

if.end28:                                         ; preds = %for.end, %land.lhs.true, %cond.end11
  %38 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #8
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc41, %if.end28
  %39 = load i32, i32* %j, align 4, !tbaa !13
  %40 = load i32, i32* %max_colors.addr, align 4, !tbaa !13
  %41 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %cmp30 = icmp slt i32 %40, %41
  br i1 %cmp30, label %cond.true31, label %cond.false32

cond.true31:                                      ; preds = %for.cond29
  %42 = load i32, i32* %max_colors.addr, align 4, !tbaa !13
  br label %cond.end33

cond.false32:                                     ; preds = %for.cond29
  %43 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  br label %cond.end33

cond.end33:                                       ; preds = %cond.false32, %cond.true31
  %cond34 = phi i32 [ %42, %cond.true31 ], [ %43, %cond.false32 ]
  %cmp35 = icmp slt i32 %39, %cond34
  br i1 %cmp35, label %for.body36, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %cond.end33
  %44 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #8
  br label %for.end43

for.body36:                                       ; preds = %cond.end33
  %45 = load %struct.colormap*, %struct.colormap** %newpal, align 4, !tbaa !2
  %palette37 = getelementptr inbounds %struct.colormap, %struct.colormap* %45, i32 0, i32 3
  %46 = load i32, i32* %i, align 4, !tbaa !13
  %inc38 = add i32 %46, 1
  store i32 %inc38, i32* %i, align 4, !tbaa !13
  %arrayidx39 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette37, i32 0, i32 %46
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %.compoundliteral, i32 0, i32 0
  %47 = load %struct.f_pixel*, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  %48 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx40 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %47, i32 %48
  %49 = bitcast %struct.f_pixel* %acolor to i8*
  %50 = bitcast %struct.f_pixel* %arrayidx40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 4 %50, i32 16, i1 false), !tbaa.struct !71
  %popularity = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %.compoundliteral, i32 0, i32 1
  store float 0.000000e+00, float* %popularity, align 4, !tbaa !145
  %fixed = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %.compoundliteral, i32 0, i32 2
  store i8 1, i8* %fixed, align 4, !tbaa !119
  %51 = bitcast %struct.colormap_item* %arrayidx39 to i8*
  %52 = bitcast %struct.colormap_item* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %51, i8* align 4 %52, i32 24, i1 false), !tbaa.struct !144
  br label %for.inc41

for.inc41:                                        ; preds = %for.body36
  %53 = load i32, i32* %j, align 4, !tbaa !13
  %inc42 = add nsw i32 %53, 1
  store i32 %inc42, i32* %j, align 4, !tbaa !13
  br label %for.cond29

for.end43:                                        ; preds = %for.cond.cleanup
  %54 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  %tobool44 = icmp ne %struct.colormap* %54, null
  br i1 %tobool44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %for.end43
  %55 = load %struct.colormap*, %struct.colormap** %palette.addr, align 4, !tbaa !2
  call void @pam_freecolormap(%struct.colormap* %55)
  br label %if.end46

if.end46:                                         ; preds = %if.then45, %for.end43
  %56 = load %struct.colormap*, %struct.colormap** %newpal, align 4, !tbaa !2
  store %struct.colormap* %56, %struct.colormap** %retval, align 4
  %57 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #8
  %58 = bitcast %struct.colormap** %newpal to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #8
  br label %return

return:                                           ; preds = %if.end46, %if.then
  %59 = load %struct.colormap*, %struct.colormap** %retval, align 4
  ret %struct.colormap* %59
}

; Function Attrs: nounwind
define internal %struct.colormap* @histogram_to_palette(%struct.histogram* %hist, %struct.liq_attr* %options) #4 {
entry:
  %retval = alloca %struct.colormap*, align 4
  %hist.addr = alloca %struct.histogram*, align 4
  %options.addr = alloca %struct.liq_attr*, align 4
  %acolormap = alloca %struct.colormap*, align 4
  %i = alloca i32, align 4
  store %struct.histogram* %hist, %struct.histogram** %hist.addr, align 4, !tbaa !2
  store %struct.liq_attr* %options, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %0 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.histogram, %struct.histogram* %0, i32 0, i32 3
  %1 = load i32, i32* %size, align 8, !tbaa !139
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.colormap* null, %struct.colormap** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %struct.colormap** %acolormap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size1 = getelementptr inbounds %struct.histogram, %struct.histogram* %3, i32 0, i32 3
  %4 = load i32, i32* %size1, align 8, !tbaa !139
  %5 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %5, i32 0, i32 1
  %6 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %7 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %7, i32 0, i32 2
  %8 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !53
  %call = call %struct.colormap* @pam_colormap(i32 %4, i8* (i32)* %6, void (i8*)* %8)
  store %struct.colormap* %call, %struct.colormap** %acolormap, align 4, !tbaa !2
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %10 = load i32, i32* %i, align 4, !tbaa !13
  %11 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size2 = getelementptr inbounds %struct.histogram, %struct.histogram* %11, i32 0, i32 3
  %12 = load i32, i32* %size2, align 8, !tbaa !139
  %cmp = icmp ult i32 %10, %12
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %14, i32 0, i32 3
  %15 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 %15
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx, i32 0, i32 0
  %16 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv = getelementptr inbounds %struct.histogram, %struct.histogram* %16, i32 0, i32 0
  %17 = load %struct.hist_item*, %struct.hist_item** %achv, align 8, !tbaa !141
  %18 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx3 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %17, i32 %18
  %acolor4 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx3, i32 0, i32 0
  %19 = bitcast %struct.f_pixel* %acolor to i8*
  %20 = bitcast %struct.f_pixel* %acolor4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false), !tbaa.struct !71
  %21 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv5 = getelementptr inbounds %struct.histogram, %struct.histogram* %21, i32 0, i32 0
  %22 = load %struct.hist_item*, %struct.hist_item** %achv5, align 8, !tbaa !141
  %23 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx6 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %22, i32 %23
  %perceptual_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx6, i32 0, i32 2
  %24 = load float, float* %perceptual_weight, align 4, !tbaa !146
  %25 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %palette7 = getelementptr inbounds %struct.colormap, %struct.colormap* %25, i32 0, i32 3
  %26 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx8 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette7, i32 0, i32 %26
  %popularity = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx8, i32 0, i32 1
  store float %24, float* %popularity, align 4, !tbaa !145
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %28 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  store %struct.colormap* %28, %struct.colormap** %retval, align 4
  %29 = bitcast %struct.colormap** %acolormap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  br label %return

return:                                           ; preds = %for.end, %if.then
  %30 = load %struct.colormap*, %struct.colormap** %retval, align 4
  ret %struct.colormap* %30
}

; Function Attrs: nounwind
define internal %struct.colormap* @find_best_palette(%struct.histogram* %hist, %struct.liq_attr* %options, double %max_mse, %struct.f_pixel* %fixed_colors, i32 %fixed_colors_count, double* %palette_error_p) #4 {
entry:
  %retval = alloca %struct.colormap*, align 4
  %hist.addr = alloca %struct.histogram*, align 4
  %options.addr = alloca %struct.liq_attr*, align 4
  %max_mse.addr = alloca double, align 8
  %fixed_colors.addr = alloca %struct.f_pixel*, align 4
  %fixed_colors_count.addr = alloca i32, align 4
  %palette_error_p.addr = alloca double*, align 4
  %max_colors = alloca i32, align 4
  %target_mse = alloca double, align 8
  %feedback_loop_trials = alloca i32, align 4
  %acolormap = alloca %struct.colormap*, align 4
  %least_error = alloca double, align 8
  %target_mse_overshoot = alloca double, align 8
  %total_trials = alloca float, align 4
  %newmap = alloca %struct.colormap*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %first_run_of_target_mse = alloca i8, align 1
  %total_error = alloca double, align 8
  %j = alloca i32, align 4
  %fraction_done = alloca float, align 4
  store %struct.histogram* %hist, %struct.histogram** %hist.addr, align 4, !tbaa !2
  store %struct.liq_attr* %options, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  store double %max_mse, double* %max_mse.addr, align 8, !tbaa !18
  store %struct.f_pixel* %fixed_colors, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  store i32 %fixed_colors_count, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  store double* %palette_error_p, double** %palette_error_p.addr, align 4, !tbaa !2
  %0 = bitcast i32* %max_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %max_colors1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %1, i32 0, i32 7
  %2 = load i32, i32* %max_colors1, align 4, !tbaa !19
  store i32 %2, i32* %max_colors, align 4, !tbaa !13
  %3 = bitcast double* %target_mse to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #8
  %4 = load double, double* %max_mse.addr, align 8, !tbaa !18
  %5 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %target_mse2 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %5, i32 0, i32 3
  %6 = load double, double* %target_mse2, align 8, !tbaa !14
  %7 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %min_posterization_output = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %7, i32 0, i32 9
  %8 = load i32, i32* %min_posterization_output, align 4, !tbaa !20
  %shl = shl i32 1, %8
  %conv = sitofp i32 %shl to double
  %div = fdiv double %conv, 1.024000e+03
  %9 = call double @llvm.pow.f64(double %div, double 2.000000e+00)
  %cmp = fcmp ogt double %6, %9
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %target_mse4 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %10, i32 0, i32 3
  %11 = load double, double* %target_mse4, align 8, !tbaa !14
  br label %cond.end

cond.false:                                       ; preds = %entry
  %12 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %min_posterization_output5 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %12, i32 0, i32 9
  %13 = load i32, i32* %min_posterization_output5, align 4, !tbaa !20
  %shl6 = shl i32 1, %13
  %conv7 = sitofp i32 %shl6 to double
  %div8 = fdiv double %conv7, 1.024000e+03
  %14 = call double @llvm.pow.f64(double %div8, double 2.000000e+00)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %11, %cond.true ], [ %14, %cond.false ]
  %cmp9 = fcmp olt double %4, %cond
  br i1 %cmp9, label %cond.true11, label %cond.false12

cond.true11:                                      ; preds = %cond.end
  %15 = load double, double* %max_mse.addr, align 8, !tbaa !18
  br label %cond.end29

cond.false12:                                     ; preds = %cond.end
  %16 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %target_mse13 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %16, i32 0, i32 3
  %17 = load double, double* %target_mse13, align 8, !tbaa !14
  %18 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %min_posterization_output14 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %18, i32 0, i32 9
  %19 = load i32, i32* %min_posterization_output14, align 4, !tbaa !20
  %shl15 = shl i32 1, %19
  %conv16 = sitofp i32 %shl15 to double
  %div17 = fdiv double %conv16, 1.024000e+03
  %20 = call double @llvm.pow.f64(double %div17, double 2.000000e+00)
  %cmp18 = fcmp ogt double %17, %20
  br i1 %cmp18, label %cond.true20, label %cond.false22

cond.true20:                                      ; preds = %cond.false12
  %21 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %target_mse21 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %21, i32 0, i32 3
  %22 = load double, double* %target_mse21, align 8, !tbaa !14
  br label %cond.end27

cond.false22:                                     ; preds = %cond.false12
  %23 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %min_posterization_output23 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %23, i32 0, i32 9
  %24 = load i32, i32* %min_posterization_output23, align 4, !tbaa !20
  %shl24 = shl i32 1, %24
  %conv25 = sitofp i32 %shl24 to double
  %div26 = fdiv double %conv25, 1.024000e+03
  %25 = call double @llvm.pow.f64(double %div26, double 2.000000e+00)
  br label %cond.end27

cond.end27:                                       ; preds = %cond.false22, %cond.true20
  %cond28 = phi double [ %22, %cond.true20 ], [ %25, %cond.false22 ]
  br label %cond.end29

cond.end29:                                       ; preds = %cond.end27, %cond.true11
  %cond30 = phi double [ %15, %cond.true11 ], [ %cond28, %cond.end27 ]
  store double %cond30, double* %target_mse, align 8, !tbaa !18
  %26 = bitcast i32* %feedback_loop_trials to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %27 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %feedback_loop_trials31 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %27, i32 0, i32 12
  %28 = load i32, i32* %feedback_loop_trials31, align 8, !tbaa !23
  store i32 %28, i32* %feedback_loop_trials, align 4, !tbaa !13
  %29 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.histogram, %struct.histogram* %29, i32 0, i32 3
  %30 = load i32, i32* %size, align 8, !tbaa !139
  %cmp32 = icmp ugt i32 %30, 5000
  br i1 %cmp32, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end29
  %31 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %mul = mul nsw i32 %31, 3
  %add = add nsw i32 %mul, 3
  %div34 = sdiv i32 %add, 4
  store i32 %div34, i32* %feedback_loop_trials, align 4, !tbaa !13
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end29
  %32 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size35 = getelementptr inbounds %struct.histogram, %struct.histogram* %32, i32 0, i32 3
  %33 = load i32, i32* %size35, align 8, !tbaa !139
  %cmp36 = icmp ugt i32 %33, 25000
  br i1 %cmp36, label %if.then38, label %if.end42

if.then38:                                        ; preds = %if.end
  %34 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %mul39 = mul nsw i32 %34, 3
  %add40 = add nsw i32 %mul39, 3
  %div41 = sdiv i32 %add40, 4
  store i32 %div41, i32* %feedback_loop_trials, align 4, !tbaa !13
  br label %if.end42

if.end42:                                         ; preds = %if.then38, %if.end
  %35 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size43 = getelementptr inbounds %struct.histogram, %struct.histogram* %35, i32 0, i32 3
  %36 = load i32, i32* %size43, align 8, !tbaa !139
  %cmp44 = icmp ugt i32 %36, 50000
  br i1 %cmp44, label %if.then46, label %if.end50

if.then46:                                        ; preds = %if.end42
  %37 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %mul47 = mul nsw i32 %37, 3
  %add48 = add nsw i32 %mul47, 3
  %div49 = sdiv i32 %add48, 4
  store i32 %div49, i32* %feedback_loop_trials, align 4, !tbaa !13
  br label %if.end50

if.end50:                                         ; preds = %if.then46, %if.end42
  %38 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size51 = getelementptr inbounds %struct.histogram, %struct.histogram* %38, i32 0, i32 3
  %39 = load i32, i32* %size51, align 8, !tbaa !139
  %cmp52 = icmp ugt i32 %39, 100000
  br i1 %cmp52, label %if.then54, label %if.end58

if.then54:                                        ; preds = %if.end50
  %40 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %mul55 = mul nsw i32 %40, 3
  %add56 = add nsw i32 %mul55, 3
  %div57 = sdiv i32 %add56, 4
  store i32 %div57, i32* %feedback_loop_trials, align 4, !tbaa !13
  br label %if.end58

if.end58:                                         ; preds = %if.then54, %if.end50
  %41 = bitcast %struct.colormap** %acolormap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  store %struct.colormap* null, %struct.colormap** %acolormap, align 4, !tbaa !2
  %42 = bitcast double* %least_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %42) #8
  store double 1.000000e+20, double* %least_error, align 8, !tbaa !18
  %43 = bitcast double* %target_mse_overshoot to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %43) #8
  %44 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %cmp59 = icmp sgt i32 %44, 0
  %45 = zext i1 %cmp59 to i64
  %cond61 = select i1 %cmp59, double 1.050000e+00, double 1.000000e+00
  store double %cond61, double* %target_mse_overshoot, align 8, !tbaa !18
  %46 = bitcast float* %total_trials to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %47 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %cmp62 = icmp sgt i32 %47, 0
  br i1 %cmp62, label %cond.true64, label %cond.false65

cond.true64:                                      ; preds = %if.end58
  %48 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  br label %cond.end66

cond.false65:                                     ; preds = %if.end58
  br label %cond.end66

cond.end66:                                       ; preds = %cond.false65, %cond.true64
  %cond67 = phi i32 [ %48, %cond.true64 ], [ 1, %cond.false65 ]
  %conv68 = sitofp i32 %cond67 to float
  store float %conv68, float* %total_trials, align 4, !tbaa !55
  br label %do.body

do.body:                                          ; preds = %do.cond, %cond.end66
  %49 = bitcast %struct.colormap** %newmap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #8
  %50 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size69 = getelementptr inbounds %struct.histogram, %struct.histogram* %50, i32 0, i32 3
  %51 = load i32, i32* %size69, align 8, !tbaa !139
  %tobool = icmp ne i32 %51, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %do.body
  %52 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %53 = load i32, i32* %max_colors, align 4, !tbaa !13
  %cmp70 = icmp ult i32 %52, %53
  br i1 %cmp70, label %if.then72, label %if.else

if.then72:                                        ; preds = %land.lhs.true
  %54 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %55 = load i32, i32* %max_colors, align 4, !tbaa !13
  %56 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %sub = sub i32 %55, %56
  %57 = load double, double* %target_mse, align 8, !tbaa !18
  %58 = load double, double* %target_mse_overshoot, align 8, !tbaa !18
  %mul73 = fmul double %57, %58
  %59 = load double, double* %target_mse, align 8, !tbaa !18
  %cmp74 = fcmp ogt double 0x3F46800000000000, %59
  br i1 %cmp74, label %cond.true76, label %cond.false77

cond.true76:                                      ; preds = %if.then72
  br label %cond.end78

cond.false77:                                     ; preds = %if.then72
  %60 = load double, double* %target_mse, align 8, !tbaa !18
  br label %cond.end78

cond.end78:                                       ; preds = %cond.false77, %cond.true76
  %cond79 = phi double [ 0x3F46800000000000, %cond.true76 ], [ %60, %cond.false77 ]
  %61 = load double, double* %least_error, align 8, !tbaa !18
  %cmp80 = fcmp ogt double %cond79, %61
  br i1 %cmp80, label %cond.true82, label %cond.false89

cond.true82:                                      ; preds = %cond.end78
  %62 = load double, double* %target_mse, align 8, !tbaa !18
  %cmp83 = fcmp ogt double 0x3F46800000000000, %62
  br i1 %cmp83, label %cond.true85, label %cond.false86

cond.true85:                                      ; preds = %cond.true82
  br label %cond.end87

cond.false86:                                     ; preds = %cond.true82
  %63 = load double, double* %target_mse, align 8, !tbaa !18
  br label %cond.end87

cond.end87:                                       ; preds = %cond.false86, %cond.true85
  %cond88 = phi double [ 0x3F46800000000000, %cond.true85 ], [ %63, %cond.false86 ]
  br label %cond.end90

cond.false89:                                     ; preds = %cond.end78
  %64 = load double, double* %least_error, align 8, !tbaa !18
  br label %cond.end90

cond.end90:                                       ; preds = %cond.false89, %cond.end87
  %cond91 = phi double [ %cond88, %cond.end87 ], [ %64, %cond.false89 ]
  %mul92 = fmul double %cond91, 1.200000e+00
  %65 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %65, i32 0, i32 1
  %66 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !52
  %67 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %67, i32 0, i32 2
  %68 = load void (i8*)*, void (i8*)** %free, align 8, !tbaa !53
  %call = call %struct.colormap* @mediancut(%struct.histogram* %54, i32 %sub, double %mul73, double %mul92, i8* (i32)* %66, void (i8*)* %68)
  store %struct.colormap* %call, %struct.colormap** %newmap, align 4, !tbaa !2
  br label %if.end93

if.else:                                          ; preds = %land.lhs.true, %do.body
  store i32 0, i32* %feedback_loop_trials, align 4, !tbaa !13
  store %struct.colormap* null, %struct.colormap** %newmap, align 4, !tbaa !2
  br label %if.end93

if.end93:                                         ; preds = %if.else, %cond.end90
  %69 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  %70 = load i32, i32* %max_colors, align 4, !tbaa !13
  %71 = load %struct.f_pixel*, %struct.f_pixel** %fixed_colors.addr, align 4, !tbaa !2
  %72 = load i32, i32* %fixed_colors_count.addr, align 4, !tbaa !13
  %73 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %malloc94 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %73, i32 0, i32 1
  %74 = load i8* (i32)*, i8* (i32)** %malloc94, align 4, !tbaa !52
  %75 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %free95 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %75, i32 0, i32 2
  %76 = load void (i8*)*, void (i8*)** %free95, align 8, !tbaa !53
  %call96 = call %struct.colormap* @add_fixed_colors_to_palette(%struct.colormap* %69, i32 %70, %struct.f_pixel* %71, i32 %72, i8* (i32)* %74, void (i8*)* %76)
  store %struct.colormap* %call96, %struct.colormap** %newmap, align 4, !tbaa !2
  %77 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  %tobool97 = icmp ne %struct.colormap* %77, null
  br i1 %tobool97, label %if.end99, label %if.then98

if.then98:                                        ; preds = %if.end93
  store %struct.colormap* null, %struct.colormap** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup197

if.end99:                                         ; preds = %if.end93
  %78 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %cmp100 = icmp sle i32 %78, 0
  br i1 %cmp100, label %if.then102, label %if.end103

if.then102:                                       ; preds = %if.end99
  %79 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  store %struct.colormap* %79, %struct.colormap** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup197

if.end103:                                        ; preds = %if.end99
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %first_run_of_target_mse) #8
  %80 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %tobool104 = icmp ne %struct.colormap* %80, null
  br i1 %tobool104, label %land.end, label %land.rhs

land.rhs:                                         ; preds = %if.end103
  %81 = load double, double* %target_mse, align 8, !tbaa !18
  %cmp105 = fcmp ogt double %81, 0.000000e+00
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end103
  %82 = phi i1 [ false, %if.end103 ], [ %cmp105, %land.rhs ]
  %frombool = zext i1 %82 to i8
  store i8 %frombool, i8* %first_run_of_target_mse, align 1, !tbaa !56
  %83 = bitcast double* %total_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %83) #8
  %84 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %85 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  %86 = load i8, i8* %first_run_of_target_mse, align 1, !tbaa !56, !range !29
  %tobool107 = trunc i8 %86 to i1
  %87 = zext i1 %tobool107 to i64
  %cond109 = select i1 %tobool107, void (%struct.hist_item*, float)* null, void (%struct.hist_item*, float)* @adjust_histogram_callback
  %call110 = call double @kmeans_do_iteration(%struct.histogram* %84, %struct.colormap* %85, void (%struct.hist_item*, float)* %cond109)
  store double %call110, double* %total_error, align 8, !tbaa !18
  %88 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %tobool111 = icmp ne %struct.colormap* %88, null
  br i1 %tobool111, label %lor.lhs.false, label %if.then120

lor.lhs.false:                                    ; preds = %land.end
  %89 = load double, double* %total_error, align 8, !tbaa !18
  %90 = load double, double* %least_error, align 8, !tbaa !18
  %cmp112 = fcmp olt double %89, %90
  br i1 %cmp112, label %if.then120, label %lor.lhs.false114

lor.lhs.false114:                                 ; preds = %lor.lhs.false
  %91 = load double, double* %total_error, align 8, !tbaa !18
  %92 = load double, double* %target_mse, align 8, !tbaa !18
  %cmp115 = fcmp ole double %91, %92
  br i1 %cmp115, label %land.lhs.true117, label %if.else152

land.lhs.true117:                                 ; preds = %lor.lhs.false114
  %93 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %93, i32 0, i32 0
  %94 = load i32, i32* %colors, align 4, !tbaa !13
  %95 = load i32, i32* %max_colors, align 4, !tbaa !13
  %cmp118 = icmp ult i32 %94, %95
  br i1 %cmp118, label %if.then120, label %if.else152

if.then120:                                       ; preds = %land.lhs.true117, %lor.lhs.false, %land.end
  %96 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  %tobool121 = icmp ne %struct.colormap* %96, null
  br i1 %tobool121, label %if.then122, label %if.end123

if.then122:                                       ; preds = %if.then120
  %97 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  call void @pam_freecolormap(%struct.colormap* %97)
  br label %if.end123

if.end123:                                        ; preds = %if.then122, %if.then120
  %98 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  store %struct.colormap* %98, %struct.colormap** %acolormap, align 4, !tbaa !2
  %99 = load double, double* %total_error, align 8, !tbaa !18
  %100 = load double, double* %target_mse, align 8, !tbaa !18
  %cmp124 = fcmp olt double %99, %100
  br i1 %cmp124, label %land.lhs.true126, label %if.end140

land.lhs.true126:                                 ; preds = %if.end123
  %101 = load double, double* %total_error, align 8, !tbaa !18
  %cmp127 = fcmp ogt double %101, 0.000000e+00
  br i1 %cmp127, label %if.then129, label %if.end140

if.then129:                                       ; preds = %land.lhs.true126
  %102 = load double, double* %target_mse_overshoot, align 8, !tbaa !18
  %mul130 = fmul double %102, 1.250000e+00
  %103 = load double, double* %target_mse, align 8, !tbaa !18
  %104 = load double, double* %total_error, align 8, !tbaa !18
  %div131 = fdiv double %103, %104
  %cmp132 = fcmp olt double %mul130, %div131
  br i1 %cmp132, label %cond.true134, label %cond.false136

cond.true134:                                     ; preds = %if.then129
  %105 = load double, double* %target_mse_overshoot, align 8, !tbaa !18
  %mul135 = fmul double %105, 1.250000e+00
  br label %cond.end138

cond.false136:                                    ; preds = %if.then129
  %106 = load double, double* %target_mse, align 8, !tbaa !18
  %107 = load double, double* %total_error, align 8, !tbaa !18
  %div137 = fdiv double %106, %107
  br label %cond.end138

cond.end138:                                      ; preds = %cond.false136, %cond.true134
  %cond139 = phi double [ %mul135, %cond.true134 ], [ %div137, %cond.false136 ]
  store double %cond139, double* %target_mse_overshoot, align 8, !tbaa !18
  br label %if.end140

if.end140:                                        ; preds = %cond.end138, %land.lhs.true126, %if.end123
  %108 = load double, double* %total_error, align 8, !tbaa !18
  store double %108, double* %least_error, align 8, !tbaa !18
  %109 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  %colors141 = getelementptr inbounds %struct.colormap, %struct.colormap* %109, i32 0, i32 0
  %110 = load i32, i32* %colors141, align 4, !tbaa !13
  %add142 = add i32 %110, 1
  %111 = load i32, i32* %max_colors, align 4, !tbaa !13
  %cmp143 = icmp ult i32 %add142, %111
  br i1 %cmp143, label %cond.true145, label %cond.false148

cond.true145:                                     ; preds = %if.end140
  %112 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  %colors146 = getelementptr inbounds %struct.colormap, %struct.colormap* %112, i32 0, i32 0
  %113 = load i32, i32* %colors146, align 4, !tbaa !13
  %add147 = add i32 %113, 1
  br label %cond.end149

cond.false148:                                    ; preds = %if.end140
  %114 = load i32, i32* %max_colors, align 4, !tbaa !13
  br label %cond.end149

cond.end149:                                      ; preds = %cond.false148, %cond.true145
  %cond150 = phi i32 [ %add147, %cond.true145 ], [ %114, %cond.false148 ]
  store i32 %cond150, i32* %max_colors, align 4, !tbaa !13
  %115 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %sub151 = sub nsw i32 %115, 1
  store i32 %sub151, i32* %feedback_loop_trials, align 4, !tbaa !13
  br label %if.end172

if.else152:                                       ; preds = %land.lhs.true117, %lor.lhs.false114
  %116 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #8
  store i32 0, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else152
  %117 = load i32, i32* %j, align 4, !tbaa !13
  %118 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size153 = getelementptr inbounds %struct.histogram, %struct.histogram* %118, i32 0, i32 3
  %119 = load i32, i32* %size153, align 8, !tbaa !139
  %cmp154 = icmp ult i32 %117, %119
  br i1 %cmp154, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 4, i32* %cleanup.dest.slot, align 4
  %120 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %121 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv = getelementptr inbounds %struct.histogram, %struct.histogram* %121, i32 0, i32 0
  %122 = load %struct.hist_item*, %struct.hist_item** %achv, align 8, !tbaa !141
  %123 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %122, i32 %123
  %perceptual_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 2
  %124 = load float, float* %perceptual_weight, align 4, !tbaa !146
  %125 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv156 = getelementptr inbounds %struct.histogram, %struct.histogram* %125, i32 0, i32 0
  %126 = load %struct.hist_item*, %struct.hist_item** %achv156, align 8, !tbaa !141
  %127 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx157 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %126, i32 %127
  %adjusted_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx157, i32 0, i32 1
  %128 = load float, float* %adjusted_weight, align 4, !tbaa !148
  %add158 = fadd float %124, %128
  %conv159 = fpext float %add158 to double
  %div160 = fdiv double %conv159, 2.000000e+00
  %conv161 = fptrunc double %div160 to float
  %129 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv162 = getelementptr inbounds %struct.histogram, %struct.histogram* %129, i32 0, i32 0
  %130 = load %struct.hist_item*, %struct.hist_item** %achv162, align 8, !tbaa !141
  %131 = load i32, i32* %j, align 4, !tbaa !13
  %arrayidx163 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %130, i32 %131
  %adjusted_weight164 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx163, i32 0, i32 1
  store float %conv161, float* %adjusted_weight164, align 4, !tbaa !148
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %132 = load i32, i32* %j, align 4, !tbaa !13
  %inc = add i32 %132, 1
  store i32 %inc, i32* %j, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  store double 1.000000e+00, double* %target_mse_overshoot, align 8, !tbaa !18
  %133 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %sub165 = sub nsw i32 %133, 6
  store i32 %sub165, i32* %feedback_loop_trials, align 4, !tbaa !13
  %134 = load double, double* %total_error, align 8, !tbaa !18
  %135 = load double, double* %least_error, align 8, !tbaa !18
  %mul166 = fmul double %135, 4.000000e+00
  %cmp167 = fcmp ogt double %134, %mul166
  br i1 %cmp167, label %if.then169, label %if.end171

if.then169:                                       ; preds = %for.end
  %136 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %sub170 = sub nsw i32 %136, 3
  store i32 %sub170, i32* %feedback_loop_trials, align 4, !tbaa !13
  br label %if.end171

if.end171:                                        ; preds = %if.then169, %for.end
  %137 = load %struct.colormap*, %struct.colormap** %newmap, align 4, !tbaa !2
  call void @pam_freecolormap(%struct.colormap* %137)
  br label %if.end172

if.end172:                                        ; preds = %if.end171, %cond.end149
  %138 = bitcast float* %fraction_done to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #8
  %139 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %conv173 = sitofp i32 %139 to float
  %140 = load float, float* %total_trials, align 4, !tbaa !55
  %div174 = fdiv float %conv173, %140
  %cmp175 = fcmp ogt float 0.000000e+00, %div174
  br i1 %cmp175, label %cond.true177, label %cond.false178

cond.true177:                                     ; preds = %if.end172
  br label %cond.end181

cond.false178:                                    ; preds = %if.end172
  %141 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %conv179 = sitofp i32 %141 to float
  %142 = load float, float* %total_trials, align 4, !tbaa !55
  %div180 = fdiv float %conv179, %142
  br label %cond.end181

cond.end181:                                      ; preds = %cond.false178, %cond.true177
  %cond182 = phi float [ 0.000000e+00, %cond.true177 ], [ %div180, %cond.false178 ]
  %sub183 = fsub float 1.000000e+00, %cond182
  store float %sub183, float* %fraction_done, align 4, !tbaa !55
  %143 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %144 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage1 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %144, i32 0, i32 17
  %145 = load i8, i8* %progress_stage1, align 8, !tbaa !30
  %conv184 = zext i8 %145 to i32
  %conv185 = sitofp i32 %conv184 to float
  %146 = load float, float* %fraction_done, align 4, !tbaa !55
  %147 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %progress_stage2 = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %147, i32 0, i32 18
  %148 = load i8, i8* %progress_stage2, align 1, !tbaa !32
  %conv186 = zext i8 %148 to i32
  %conv187 = sitofp i32 %conv186 to float
  %mul188 = fmul float %146, %conv187
  %add189 = fadd float %conv185, %mul188
  %call190 = call zeroext i1 @liq_progress(%struct.liq_attr* %143, float %add189)
  br i1 %call190, label %if.then191, label %if.end192

if.then191:                                       ; preds = %cond.end181
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end192:                                        ; preds = %cond.end181
  %149 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %150 = load float, float* %fraction_done, align 4, !tbaa !55
  %mul193 = fmul float 1.000000e+02, %150
  %conv194 = fptosi float %mul193 to i32
  call void (%struct.liq_attr*, i8*, ...) @liq_verbose_printf(%struct.liq_attr* %149, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.14, i32 0, i32 0), i32 %conv194)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end192, %if.then191
  %151 = bitcast float* %fraction_done to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #8
  %152 = bitcast double* %total_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %152) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %first_run_of_target_mse) #8
  br label %cleanup197

cleanup197:                                       ; preds = %cleanup, %if.then102, %if.then98
  %153 = bitcast %struct.colormap** %newmap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup200 [
    i32 0, label %cleanup.cont
    i32 2, label %do.end
  ]

cleanup.cont:                                     ; preds = %cleanup197
  br label %do.cond

do.cond:                                          ; preds = %cleanup.cont
  %154 = load i32, i32* %feedback_loop_trials, align 4, !tbaa !13
  %cmp198 = icmp sgt i32 %154, 0
  br i1 %cmp198, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %cleanup197
  %155 = load double, double* %least_error, align 8, !tbaa !18
  %156 = load double*, double** %palette_error_p.addr, align 4, !tbaa !2
  store double %155, double* %156, align 8, !tbaa !18
  %157 = load %struct.colormap*, %struct.colormap** %acolormap, align 4, !tbaa !2
  store %struct.colormap* %157, %struct.colormap** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup200

cleanup200:                                       ; preds = %do.end, %cleanup197
  %158 = bitcast float* %total_trials to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #8
  %159 = bitcast double* %target_mse_overshoot to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %159) #8
  %160 = bitcast double* %least_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %160) #8
  %161 = bitcast %struct.colormap** %acolormap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #8
  %162 = bitcast i32* %feedback_loop_trials to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #8
  %163 = bitcast double* %target_mse to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %163) #8
  %164 = bitcast i32* %max_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #8
  %165 = load %struct.colormap*, %struct.colormap** %retval, align 4
  ret %struct.colormap* %165
}

declare hidden double @kmeans_do_iteration(%struct.histogram*, %struct.colormap*, void (%struct.hist_item*, float)*) #1

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #9

; Function Attrs: nounwind
define internal void @sort_palette(%struct.colormap* nonnull %map, %struct.liq_attr* nonnull %options) #4 {
entry:
  %map.addr = alloca %struct.colormap*, align 4
  %options.addr = alloca %struct.liq_attr*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %old = alloca i32, align 4
  %transparent_dest = alloca i32, align 4
  %tmp = alloca %struct.colormap_item, align 4
  %non_fixed_colors = alloca i32, align 4
  %i15 = alloca i32, align 4
  %num_transparent = alloca i32, align 4
  %i31 = alloca i32, align 4
  %tmp44 = alloca %struct.colormap_item, align 4
  %tmp66 = alloca %struct.colormap_item, align 4
  %tmp75 = alloca %struct.colormap_item, align 4
  %tmp84 = alloca %struct.colormap_item, align 4
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  store %struct.liq_attr* %options, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %0 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %last_index_transparent = getelementptr inbounds %struct.liq_attr, %struct.liq_attr* %0, i32 0, i32 13
  %1 = load i8, i8* %last_index_transparent, align 4, !tbaa !43, !range !29
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4, !tbaa !13
  %4 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %4, i32 0, i32 0
  %5 = load i32, i32* %colors, align 4, !tbaa !13
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %6 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %6, i32 0, i32 3
  %7 = load i32, i32* %i, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 %7
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx, i32 0, i32 0
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor, i32 0, i32 0
  %8 = load float, float* %a, align 4, !tbaa !138
  %cmp1 = fcmp olt float %8, 3.906250e-03
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %for.body
  %9 = bitcast i32* %old to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load i32, i32* %i, align 4, !tbaa !13
  store i32 %10, i32* %old, align 4, !tbaa !13
  %11 = bitcast i32* %transparent_dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors3 = getelementptr inbounds %struct.colormap, %struct.colormap* %12, i32 0, i32 0
  %13 = load i32, i32* %colors3, align 4, !tbaa !13
  %sub = sub i32 %13, 1
  store i32 %sub, i32* %transparent_dest, align 4, !tbaa !13
  %14 = bitcast %struct.colormap_item* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %14) #8
  %15 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette4 = getelementptr inbounds %struct.colormap, %struct.colormap* %15, i32 0, i32 3
  %16 = load i32, i32* %transparent_dest, align 4, !tbaa !13
  %arrayidx5 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette4, i32 0, i32 %16
  %17 = bitcast %struct.colormap_item* %tmp to i8*
  %18 = bitcast %struct.colormap_item* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 24, i1 false), !tbaa.struct !144
  %19 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette6 = getelementptr inbounds %struct.colormap, %struct.colormap* %19, i32 0, i32 3
  %20 = load i32, i32* %transparent_dest, align 4, !tbaa !13
  %arrayidx7 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette6, i32 0, i32 %20
  %21 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette8 = getelementptr inbounds %struct.colormap, %struct.colormap* %21, i32 0, i32 3
  %22 = load i32, i32* %old, align 4, !tbaa !13
  %arrayidx9 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette8, i32 0, i32 %22
  %23 = bitcast %struct.colormap_item* %arrayidx7 to i8*
  %24 = bitcast %struct.colormap_item* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 24, i1 false), !tbaa.struct !144
  %25 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette10 = getelementptr inbounds %struct.colormap, %struct.colormap* %25, i32 0, i32 3
  %26 = load i32, i32* %old, align 4, !tbaa !13
  %arrayidx11 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette10, i32 0, i32 %26
  %27 = bitcast %struct.colormap_item* %arrayidx11 to i8*
  %28 = bitcast %struct.colormap_item* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 24, i1 false), !tbaa.struct !144
  %29 = bitcast %struct.colormap_item* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %29) #8
  %30 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %31 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors12 = getelementptr inbounds %struct.colormap, %struct.colormap* %31, i32 0, i32 0
  %32 = load i32, i32* %colors12, align 4, !tbaa !13
  %sub13 = sub i32 %32, 1
  call void @sort_palette_qsort(%struct.colormap* %30, i32 0, i32 %sub13)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %transparent_dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast i32* %old to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  br label %cleanup

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %35 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

cleanup:                                          ; preds = %if.then2, %for.cond.cleanup
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 2, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup
  br label %if.end14

if.end14:                                         ; preds = %for.end, %entry
  %37 = bitcast i32* %non_fixed_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  store i32 0, i32* %non_fixed_colors, align 4, !tbaa !13
  %38 = bitcast i32* %i15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #8
  store i32 0, i32* %i15, align 4, !tbaa !13
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc27, %if.end14
  %39 = load i32, i32* %i15, align 4, !tbaa !13
  %40 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors17 = getelementptr inbounds %struct.colormap, %struct.colormap* %40, i32 0, i32 0
  %41 = load i32, i32* %colors17, align 4, !tbaa !13
  %cmp18 = icmp ult i32 %39, %41
  br i1 %cmp18, label %for.body20, label %for.cond.cleanup19

for.cond.cleanup19:                               ; preds = %for.cond16
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

for.body20:                                       ; preds = %for.cond16
  %42 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette21 = getelementptr inbounds %struct.colormap, %struct.colormap* %42, i32 0, i32 3
  %43 = load i32, i32* %i15, align 4, !tbaa !13
  %arrayidx22 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette21, i32 0, i32 %43
  %fixed = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx22, i32 0, i32 2
  %44 = load i8, i8* %fixed, align 4, !tbaa !119, !range !29
  %tobool23 = trunc i8 %44 to i1
  br i1 %tobool23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %for.body20
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

if.end25:                                         ; preds = %for.body20
  %45 = load i32, i32* %non_fixed_colors, align 4, !tbaa !13
  %inc26 = add i32 %45, 1
  store i32 %inc26, i32* %non_fixed_colors, align 4, !tbaa !13
  br label %for.inc27

for.inc27:                                        ; preds = %if.end25
  %46 = load i32, i32* %i15, align 4, !tbaa !13
  %inc28 = add i32 %46, 1
  store i32 %inc28, i32* %i15, align 4, !tbaa !13
  br label %for.cond16

cleanup29:                                        ; preds = %if.then24, %for.cond.cleanup19
  %47 = bitcast i32* %i15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #8
  br label %for.end30

for.end30:                                        ; preds = %cleanup29
  %48 = bitcast i32* %num_transparent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #8
  store i32 0, i32* %num_transparent, align 4, !tbaa !13
  %49 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #8
  store i32 0, i32* %i31, align 4, !tbaa !13
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc56, %for.end30
  %50 = load i32, i32* %i31, align 4, !tbaa !13
  %51 = load i32, i32* %non_fixed_colors, align 4, !tbaa !13
  %cmp33 = icmp ult i32 %50, %51
  br i1 %cmp33, label %for.body35, label %for.cond.cleanup34

for.cond.cleanup34:                               ; preds = %for.cond32
  store i32 8, i32* %cleanup.dest.slot, align 4
  %52 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  br label %for.end59

for.body35:                                       ; preds = %for.cond32
  %53 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette36 = getelementptr inbounds %struct.colormap, %struct.colormap* %53, i32 0, i32 3
  %54 = load i32, i32* %i31, align 4, !tbaa !13
  %arrayidx37 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette36, i32 0, i32 %54
  %acolor38 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx37, i32 0, i32 0
  %a39 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor38, i32 0, i32 0
  %55 = load float, float* %a39, align 4, !tbaa !138
  %cmp40 = fcmp olt float %55, 0x3FEFE00000000000
  br i1 %cmp40, label %if.then41, label %if.end55

if.then41:                                        ; preds = %for.body35
  %56 = load i32, i32* %i31, align 4, !tbaa !13
  %57 = load i32, i32* %num_transparent, align 4, !tbaa !13
  %cmp42 = icmp ne i32 %56, %57
  br i1 %cmp42, label %if.then43, label %if.end53

if.then43:                                        ; preds = %if.then41
  %58 = bitcast %struct.colormap_item* %tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %58) #8
  %59 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette45 = getelementptr inbounds %struct.colormap, %struct.colormap* %59, i32 0, i32 3
  %60 = load i32, i32* %num_transparent, align 4, !tbaa !13
  %arrayidx46 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette45, i32 0, i32 %60
  %61 = bitcast %struct.colormap_item* %tmp44 to i8*
  %62 = bitcast %struct.colormap_item* %arrayidx46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %61, i8* align 4 %62, i32 24, i1 false), !tbaa.struct !144
  %63 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette47 = getelementptr inbounds %struct.colormap, %struct.colormap* %63, i32 0, i32 3
  %64 = load i32, i32* %num_transparent, align 4, !tbaa !13
  %arrayidx48 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette47, i32 0, i32 %64
  %65 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette49 = getelementptr inbounds %struct.colormap, %struct.colormap* %65, i32 0, i32 3
  %66 = load i32, i32* %i31, align 4, !tbaa !13
  %arrayidx50 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette49, i32 0, i32 %66
  %67 = bitcast %struct.colormap_item* %arrayidx48 to i8*
  %68 = bitcast %struct.colormap_item* %arrayidx50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 4 %68, i32 24, i1 false), !tbaa.struct !144
  %69 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette51 = getelementptr inbounds %struct.colormap, %struct.colormap* %69, i32 0, i32 3
  %70 = load i32, i32* %i31, align 4, !tbaa !13
  %arrayidx52 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette51, i32 0, i32 %70
  %71 = bitcast %struct.colormap_item* %arrayidx52 to i8*
  %72 = bitcast %struct.colormap_item* %tmp44 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %71, i8* align 4 %72, i32 24, i1 false), !tbaa.struct !144
  %73 = bitcast %struct.colormap_item* %tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %73) #8
  %74 = load i32, i32* %i31, align 4, !tbaa !13
  %dec = add i32 %74, -1
  store i32 %dec, i32* %i31, align 4, !tbaa !13
  br label %if.end53

if.end53:                                         ; preds = %if.then43, %if.then41
  %75 = load i32, i32* %num_transparent, align 4, !tbaa !13
  %inc54 = add i32 %75, 1
  store i32 %inc54, i32* %num_transparent, align 4, !tbaa !13
  br label %if.end55

if.end55:                                         ; preds = %if.end53, %for.body35
  br label %for.inc56

for.inc56:                                        ; preds = %if.end55
  %76 = load i32, i32* %i31, align 4, !tbaa !13
  %inc57 = add i32 %76, 1
  store i32 %inc57, i32* %i31, align 4, !tbaa !13
  br label %for.cond32

for.end59:                                        ; preds = %for.cond.cleanup34
  %77 = load %struct.liq_attr*, %struct.liq_attr** %options.addr, align 4, !tbaa !2
  %78 = load i32, i32* %num_transparent, align 4, !tbaa !13
  %79 = load i32, i32* %num_transparent, align 4, !tbaa !13
  %cmp60 = icmp eq i32 %79, 1
  %80 = zext i1 %cmp60 to i64
  %cond = select i1 %cmp60, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.16, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.17, i32 0, i32 0)
  call void (%struct.liq_attr*, i8*, ...) @liq_verbose_printf(%struct.liq_attr* %77, i8* getelementptr inbounds ([63 x i8], [63 x i8]* @.str.15, i32 0, i32 0), i32 %78, i8* %cond)
  %81 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %82 = load i32, i32* %num_transparent, align 4, !tbaa !13
  call void @sort_palette_qsort(%struct.colormap* %81, i32 0, i32 %82)
  %83 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %84 = load i32, i32* %num_transparent, align 4, !tbaa !13
  %85 = load i32, i32* %non_fixed_colors, align 4, !tbaa !13
  %86 = load i32, i32* %num_transparent, align 4, !tbaa !13
  %sub61 = sub i32 %85, %86
  call void @sort_palette_qsort(%struct.colormap* %83, i32 %84, i32 %sub61)
  %87 = load i32, i32* %non_fixed_colors, align 4, !tbaa !13
  %cmp62 = icmp ugt i32 %87, 9
  br i1 %cmp62, label %land.lhs.true, label %if.end93

land.lhs.true:                                    ; preds = %for.end59
  %88 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors63 = getelementptr inbounds %struct.colormap, %struct.colormap* %88, i32 0, i32 0
  %89 = load i32, i32* %colors63, align 4, !tbaa !13
  %cmp64 = icmp ugt i32 %89, 16
  br i1 %cmp64, label %if.then65, label %if.end93

if.then65:                                        ; preds = %land.lhs.true
  %90 = bitcast %struct.colormap_item* %tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %90) #8
  %91 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette67 = getelementptr inbounds %struct.colormap, %struct.colormap* %91, i32 0, i32 3
  %arrayidx68 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette67, i32 0, i32 7
  %92 = bitcast %struct.colormap_item* %tmp66 to i8*
  %93 = bitcast %struct.colormap_item* %arrayidx68 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %92, i8* align 4 %93, i32 24, i1 false), !tbaa.struct !144
  %94 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette69 = getelementptr inbounds %struct.colormap, %struct.colormap* %94, i32 0, i32 3
  %arrayidx70 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette69, i32 0, i32 7
  %95 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette71 = getelementptr inbounds %struct.colormap, %struct.colormap* %95, i32 0, i32 3
  %arrayidx72 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette71, i32 0, i32 1
  %96 = bitcast %struct.colormap_item* %arrayidx70 to i8*
  %97 = bitcast %struct.colormap_item* %arrayidx72 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %96, i8* align 4 %97, i32 24, i1 false), !tbaa.struct !144
  %98 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette73 = getelementptr inbounds %struct.colormap, %struct.colormap* %98, i32 0, i32 3
  %arrayidx74 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette73, i32 0, i32 1
  %99 = bitcast %struct.colormap_item* %arrayidx74 to i8*
  %100 = bitcast %struct.colormap_item* %tmp66 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %99, i8* align 4 %100, i32 24, i1 false), !tbaa.struct !144
  %101 = bitcast %struct.colormap_item* %tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %101) #8
  %102 = bitcast %struct.colormap_item* %tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %102) #8
  %103 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette76 = getelementptr inbounds %struct.colormap, %struct.colormap* %103, i32 0, i32 3
  %arrayidx77 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette76, i32 0, i32 8
  %104 = bitcast %struct.colormap_item* %tmp75 to i8*
  %105 = bitcast %struct.colormap_item* %arrayidx77 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %104, i8* align 4 %105, i32 24, i1 false), !tbaa.struct !144
  %106 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette78 = getelementptr inbounds %struct.colormap, %struct.colormap* %106, i32 0, i32 3
  %arrayidx79 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette78, i32 0, i32 8
  %107 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette80 = getelementptr inbounds %struct.colormap, %struct.colormap* %107, i32 0, i32 3
  %arrayidx81 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette80, i32 0, i32 2
  %108 = bitcast %struct.colormap_item* %arrayidx79 to i8*
  %109 = bitcast %struct.colormap_item* %arrayidx81 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %108, i8* align 4 %109, i32 24, i1 false), !tbaa.struct !144
  %110 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette82 = getelementptr inbounds %struct.colormap, %struct.colormap* %110, i32 0, i32 3
  %arrayidx83 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette82, i32 0, i32 2
  %111 = bitcast %struct.colormap_item* %arrayidx83 to i8*
  %112 = bitcast %struct.colormap_item* %tmp75 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %111, i8* align 4 %112, i32 24, i1 false), !tbaa.struct !144
  %113 = bitcast %struct.colormap_item* %tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %113) #8
  %114 = bitcast %struct.colormap_item* %tmp84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %114) #8
  %115 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette85 = getelementptr inbounds %struct.colormap, %struct.colormap* %115, i32 0, i32 3
  %arrayidx86 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette85, i32 0, i32 9
  %116 = bitcast %struct.colormap_item* %tmp84 to i8*
  %117 = bitcast %struct.colormap_item* %arrayidx86 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %116, i8* align 4 %117, i32 24, i1 false), !tbaa.struct !144
  %118 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette87 = getelementptr inbounds %struct.colormap, %struct.colormap* %118, i32 0, i32 3
  %arrayidx88 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette87, i32 0, i32 9
  %119 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette89 = getelementptr inbounds %struct.colormap, %struct.colormap* %119, i32 0, i32 3
  %arrayidx90 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette89, i32 0, i32 3
  %120 = bitcast %struct.colormap_item* %arrayidx88 to i8*
  %121 = bitcast %struct.colormap_item* %arrayidx90 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %120, i8* align 4 %121, i32 24, i1 false), !tbaa.struct !144
  %122 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette91 = getelementptr inbounds %struct.colormap, %struct.colormap* %122, i32 0, i32 3
  %arrayidx92 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette91, i32 0, i32 3
  %123 = bitcast %struct.colormap_item* %arrayidx92 to i8*
  %124 = bitcast %struct.colormap_item* %tmp84 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %123, i8* align 4 %124, i32 24, i1 false), !tbaa.struct !144
  %125 = bitcast %struct.colormap_item* %tmp84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %125) #8
  br label %if.end93

if.end93:                                         ; preds = %if.then65, %land.lhs.true, %for.end59
  %126 = bitcast i32* %num_transparent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #8
  %127 = bitcast i32* %non_fixed_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #8
  br label %return

return:                                           ; preds = %if.end93, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare hidden %struct.colormap* @pam_colormap(i32, i8* (i32)*, void (i8*)*) #1

declare hidden %struct.colormap* @mediancut(%struct.histogram*, i32, double, double, i8* (i32)*, void (i8*)*) #1

; Function Attrs: nounwind
define internal void @adjust_histogram_callback(%struct.hist_item* nonnull %item, float %diff) #4 {
entry:
  %item.addr = alloca %struct.hist_item*, align 4
  %diff.addr = alloca float, align 4
  store %struct.hist_item* %item, %struct.hist_item** %item.addr, align 4, !tbaa !2
  store float %diff, float* %diff.addr, align 4, !tbaa !55
  %0 = load %struct.hist_item*, %struct.hist_item** %item.addr, align 4, !tbaa !2
  %perceptual_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %0, i32 0, i32 2
  %1 = load float, float* %perceptual_weight, align 4, !tbaa !146
  %2 = load %struct.hist_item*, %struct.hist_item** %item.addr, align 4, !tbaa !2
  %adjusted_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %2, i32 0, i32 1
  %3 = load float, float* %adjusted_weight, align 4, !tbaa !148
  %add = fadd float %1, %3
  %4 = load float, float* %diff.addr, align 4, !tbaa !55
  %add1 = fadd float 1.000000e+00, %4
  %5 = call float @llvm.sqrt.f32(float %add1)
  %mul = fmul float %add, %5
  %6 = load %struct.hist_item*, %struct.hist_item** %item.addr, align 4, !tbaa !2
  %adjusted_weight2 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %6, i32 0, i32 1
  store float %mul, float* %adjusted_weight2, align 4, !tbaa !148
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: nounwind
define internal void @sort_palette_qsort(%struct.colormap* nonnull %map, i32 %start, i32 %nelem) #4 {
entry:
  %map.addr = alloca %struct.colormap*, align 4
  %start.addr = alloca i32, align 4
  %nelem.addr = alloca i32, align 4
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !13
  store i32 %nelem, i32* %nelem.addr, align 4, !tbaa !13
  %0 = load i32, i32* %nelem.addr, align 4, !tbaa !13
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %1, i32 0, i32 3
  %arraydecay = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 0
  %2 = load i32, i32* %start.addr, align 4, !tbaa !13
  %add.ptr = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arraydecay, i32 %2
  %3 = bitcast %struct.colormap_item* %add.ptr to i8*
  %4 = load i32, i32* %nelem.addr, align 4, !tbaa !13
  call void @qsort(i8* %3, i32 %4, i32 24, i32 (i8*, i8*)* @compare_popularity)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

declare void @qsort(i8*, i32, i32, i32 (i8*, i8*)*) #1

; Function Attrs: nounwind
define internal i32 @compare_popularity(i8* nonnull %ch1, i8* nonnull %ch2) #4 {
entry:
  %ch1.addr = alloca i8*, align 4
  %ch2.addr = alloca i8*, align 4
  %v1 = alloca float, align 4
  %v2 = alloca float, align 4
  store i8* %ch1, i8** %ch1.addr, align 4, !tbaa !2
  store i8* %ch2, i8** %ch2.addr, align 4, !tbaa !2
  %0 = bitcast float* %v1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %ch1.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.colormap_item*
  %popularity = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %2, i32 0, i32 1
  %3 = load float, float* %popularity, align 4, !tbaa !145
  store float %3, float* %v1, align 4, !tbaa !55
  %4 = bitcast float* %v2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load i8*, i8** %ch2.addr, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %struct.colormap_item*
  %popularity1 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %6, i32 0, i32 1
  %7 = load float, float* %popularity1, align 4, !tbaa !145
  store float %7, float* %v2, align 4, !tbaa !55
  %8 = load float, float* %v1, align 4, !tbaa !55
  %9 = load float, float* %v2, align 4, !tbaa !55
  %cmp = fcmp ogt float %8, %9
  %10 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 -1, i32 1
  %11 = bitcast float* %v2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %v1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define internal void @f_to_rgb(%struct.rgba_pixel* noalias sret align 1 %agg.result, float %gamma, %struct.f_pixel* byval(%struct.f_pixel) align 4 %px) #7 {
entry:
  %gamma.addr = alloca float, align 4
  %r2 = alloca float, align 4
  %g5 = alloca float, align 4
  %b9 = alloca float, align 4
  %a13 = alloca float, align 4
  store float %gamma, float* %gamma.addr, align 4, !tbaa !55
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %0 = load float, float* %a, align 4, !tbaa !72
  %cmp = fcmp olt float %0, 3.906250e-03
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %r = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %agg.result, i32 0, i32 0
  store i8 0, i8* %r, align 1, !tbaa !63
  %g = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %agg.result, i32 0, i32 1
  store i8 0, i8* %g, align 1, !tbaa !66
  %b = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %agg.result, i32 0, i32 2
  store i8 0, i8* %b, align 1, !tbaa !68
  %a1 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %agg.result, i32 0, i32 3
  store i8 0, i8* %a1, align 1, !tbaa !70
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast float* %r2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %r3 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %2 = load float, float* %r3, align 4, !tbaa !74
  %a4 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %3 = load float, float* %a4, align 4, !tbaa !72
  %div = fdiv float %2, %3
  store float %div, float* %r2, align 4, !tbaa !55
  %4 = bitcast float* %g5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %g6 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %5 = load float, float* %g6, align 4, !tbaa !75
  %a7 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %6 = load float, float* %a7, align 4, !tbaa !72
  %div8 = fdiv float %5, %6
  store float %div8, float* %g5, align 4, !tbaa !55
  %7 = bitcast float* %b9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %b10 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %8 = load float, float* %b10, align 4, !tbaa !76
  %a11 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %9 = load float, float* %a11, align 4, !tbaa !72
  %div12 = fdiv float %8, %9
  store float %div12, float* %b9, align 4, !tbaa !55
  %10 = bitcast float* %a13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %a14 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %11 = load float, float* %a14, align 4, !tbaa !72
  store float %11, float* %a13, align 4, !tbaa !55
  %12 = load float, float* %r2, align 4, !tbaa !55
  %13 = load float, float* %gamma.addr, align 4, !tbaa !55
  %div15 = fdiv float %13, 0x3FE198C7E0000000
  %14 = call float @llvm.pow.f32(float %12, float %div15)
  store float %14, float* %r2, align 4, !tbaa !55
  %15 = load float, float* %g5, align 4, !tbaa !55
  %16 = load float, float* %gamma.addr, align 4, !tbaa !55
  %div16 = fdiv float %16, 0x3FE198C7E0000000
  %17 = call float @llvm.pow.f32(float %15, float %div16)
  store float %17, float* %g5, align 4, !tbaa !55
  %18 = load float, float* %b9, align 4, !tbaa !55
  %19 = load float, float* %gamma.addr, align 4, !tbaa !55
  %div17 = fdiv float %19, 0x3FE198C7E0000000
  %20 = call float @llvm.pow.f32(float %18, float %div17)
  store float %20, float* %b9, align 4, !tbaa !55
  %21 = load float, float* %r2, align 4, !tbaa !55
  %mul = fmul float %21, 2.560000e+02
  store float %mul, float* %r2, align 4, !tbaa !55
  %22 = load float, float* %g5, align 4, !tbaa !55
  %mul18 = fmul float %22, 2.560000e+02
  store float %mul18, float* %g5, align 4, !tbaa !55
  %23 = load float, float* %b9, align 4, !tbaa !55
  %mul19 = fmul float %23, 2.560000e+02
  store float %mul19, float* %b9, align 4, !tbaa !55
  %24 = load float, float* %a13, align 4, !tbaa !55
  %mul20 = fmul float %24, 2.560000e+02
  store float %mul20, float* %a13, align 4, !tbaa !55
  %r21 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %agg.result, i32 0, i32 0
  %25 = load float, float* %r2, align 4, !tbaa !55
  %cmp22 = fcmp oge float %25, 2.550000e+02
  br i1 %cmp22, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %26 = load float, float* %r2, align 4, !tbaa !55
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 2.550000e+02, %cond.true ], [ %26, %cond.false ]
  %conv = fptoui float %cond to i8
  store i8 %conv, i8* %r21, align 1, !tbaa !63
  %g23 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %agg.result, i32 0, i32 1
  %27 = load float, float* %g5, align 4, !tbaa !55
  %cmp24 = fcmp oge float %27, 2.550000e+02
  br i1 %cmp24, label %cond.true26, label %cond.false27

cond.true26:                                      ; preds = %cond.end
  br label %cond.end28

cond.false27:                                     ; preds = %cond.end
  %28 = load float, float* %g5, align 4, !tbaa !55
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false27, %cond.true26
  %cond29 = phi float [ 2.550000e+02, %cond.true26 ], [ %28, %cond.false27 ]
  %conv30 = fptoui float %cond29 to i8
  store i8 %conv30, i8* %g23, align 1, !tbaa !66
  %b31 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %agg.result, i32 0, i32 2
  %29 = load float, float* %b9, align 4, !tbaa !55
  %cmp32 = fcmp oge float %29, 2.550000e+02
  br i1 %cmp32, label %cond.true34, label %cond.false35

cond.true34:                                      ; preds = %cond.end28
  br label %cond.end36

cond.false35:                                     ; preds = %cond.end28
  %30 = load float, float* %b9, align 4, !tbaa !55
  br label %cond.end36

cond.end36:                                       ; preds = %cond.false35, %cond.true34
  %cond37 = phi float [ 2.550000e+02, %cond.true34 ], [ %30, %cond.false35 ]
  %conv38 = fptoui float %cond37 to i8
  store i8 %conv38, i8* %b31, align 1, !tbaa !68
  %a39 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %agg.result, i32 0, i32 3
  %31 = load float, float* %a13, align 4, !tbaa !55
  %cmp40 = fcmp oge float %31, 2.550000e+02
  br i1 %cmp40, label %cond.true42, label %cond.false43

cond.true42:                                      ; preds = %cond.end36
  br label %cond.end44

cond.false43:                                     ; preds = %cond.end36
  %32 = load float, float* %a13, align 4, !tbaa !55
  br label %cond.end44

cond.end44:                                       ; preds = %cond.false43, %cond.true42
  %cond45 = phi float [ 2.550000e+02, %cond.true42 ], [ %32, %cond.false43 ]
  %conv46 = fptoui float %cond45 to i8
  store i8 %conv46, i8* %a39, align 1, !tbaa !70
  %33 = bitcast float* %a13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %b9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %g5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %r2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  br label %return

return:                                           ; preds = %cond.end44, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define internal i32 @posterize_channel(i32 %color, i32 %bits) #7 {
entry:
  %color.addr = alloca i32, align 4
  %bits.addr = alloca i32, align 4
  store i32 %color, i32* %color.addr, align 4, !tbaa !13
  store i32 %bits, i32* %bits.addr, align 4, !tbaa !13
  %0 = load i32, i32* %color.addr, align 4, !tbaa !13
  %1 = load i32, i32* %bits.addr, align 4, !tbaa !13
  %shl = shl i32 1, %1
  %sub = sub nsw i32 %shl, 1
  %neg = xor i32 %sub, -1
  %and = and i32 %0, %neg
  %2 = load i32, i32* %color.addr, align 4, !tbaa !13
  %3 = load i32, i32* %bits.addr, align 4, !tbaa !13
  %sub1 = sub i32 8, %3
  %shr = lshr i32 %2, %sub1
  %or = or i32 %and, %shr
  ret i32 %or
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.pow.f32(float, float) #9

; Function Attrs: nounwind
define internal void @modify_alpha(%struct.liq_image* nonnull %input_image, %struct.rgba_pixel* nonnull %row_pixels) #4 {
entry:
  %input_image.addr = alloca %struct.liq_image*, align 4
  %row_pixels.addr = alloca %struct.rgba_pixel*, align 4
  %min_opaque_val = alloca float, align 4
  %almost_opaque_val = alloca float, align 4
  %almost_opaque_val_int = alloca i32, align 4
  %col = alloca i32, align 4
  %px = alloca %struct.rgba_pixel, align 1
  %al = alloca float, align 4
  store %struct.liq_image* %input_image, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  store %struct.rgba_pixel* %row_pixels, %struct.rgba_pixel** %row_pixels.addr, align 4, !tbaa !2
  %0 = bitcast float* %min_opaque_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %min_opaque_val1 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %1, i32 0, i32 17
  %2 = load float, float* %min_opaque_val1, align 4, !tbaa !98
  store float %2, float* %min_opaque_val, align 4, !tbaa !55
  %3 = bitcast float* %almost_opaque_val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load float, float* %min_opaque_val, align 4, !tbaa !55
  %mul = fmul float %4, 1.690000e+02
  %div = fdiv float %mul, 2.560000e+02
  store float %div, float* %almost_opaque_val, align 4, !tbaa !55
  %5 = bitcast i32* %almost_opaque_val_int to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load float, float* %min_opaque_val, align 4, !tbaa !55
  %mul2 = fmul float %6, 1.690000e+02
  %div3 = fdiv float %mul2, 2.560000e+02
  %mul4 = fmul float %div3, 2.550000e+02
  %conv = fptoui float %mul4 to i32
  store i32 %conv, i32* %almost_opaque_val_int, align 4, !tbaa !13
  %7 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store i32 0, i32* %col, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %col, align 4, !tbaa !13
  %9 = load %struct.liq_image*, %struct.liq_image** %input_image.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %9, i32 0, i32 6
  %10 = load i32, i32* %width, align 8, !tbaa !85
  %cmp = icmp ult i32 %8, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %struct.rgba_pixel* %px to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %struct.rgba_pixel*, %struct.rgba_pixel** %row_pixels.addr, align 4, !tbaa !2
  %14 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %13, i32 %14
  %15 = bitcast %struct.rgba_pixel* %px to i8*
  %16 = bitcast %struct.rgba_pixel* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %15, i8* align 1 %16, i32 4, i1 false), !tbaa.struct !121
  %a = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 3
  %17 = load i8, i8* %a, align 1, !tbaa !70
  %conv6 = zext i8 %17 to i32
  %18 = load i32, i32* %almost_opaque_val_int, align 4, !tbaa !13
  %cmp7 = icmp uge i32 %conv6, %18
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %19 = bitcast float* %al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %a9 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 3
  %20 = load i8, i8* %a9, align 1, !tbaa !70
  %conv10 = zext i8 %20 to i32
  %conv11 = sitofp i32 %conv10 to float
  %div12 = fdiv float %conv11, 2.550000e+02
  store float %div12, float* %al, align 4, !tbaa !55
  %21 = load float, float* %almost_opaque_val, align 4, !tbaa !55
  %22 = load float, float* %al, align 4, !tbaa !55
  %23 = load float, float* %almost_opaque_val, align 4, !tbaa !55
  %sub = fsub float %22, %23
  %24 = load float, float* %almost_opaque_val, align 4, !tbaa !55
  %sub13 = fsub float 1.000000e+00, %24
  %mul14 = fmul float %sub, %sub13
  %25 = load float, float* %min_opaque_val, align 4, !tbaa !55
  %26 = load float, float* %almost_opaque_val, align 4, !tbaa !55
  %sub15 = fsub float %25, %26
  %div16 = fdiv float %mul14, %sub15
  %add = fadd float %21, %div16
  store float %add, float* %al, align 4, !tbaa !55
  %27 = load float, float* %al, align 4, !tbaa !55
  %mul17 = fmul float %27, 2.560000e+02
  store float %mul17, float* %al, align 4, !tbaa !55
  %28 = load float, float* %al, align 4, !tbaa !55
  %cmp18 = fcmp oge float %28, 2.550000e+02
  br i1 %cmp18, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %29 = load float, float* %al, align 4, !tbaa !55
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 2.550000e+02, %cond.true ], [ %29, %cond.false ]
  %conv20 = fptoui float %cond to i8
  %30 = load %struct.rgba_pixel*, %struct.rgba_pixel** %row_pixels.addr, align 4, !tbaa !2
  %31 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx21 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %30, i32 %31
  %a22 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %arrayidx21, i32 0, i32 3
  store i8 %conv20, i8* %a22, align 1, !tbaa !70
  %32 = bitcast float* %al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  br label %if.end

if.end:                                           ; preds = %cond.end, %for.body
  %33 = bitcast %struct.rgba_pixel* %px to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %34 = load i32, i32* %col, align 4, !tbaa !13
  %inc = add i32 %34, 1
  store i32 %inc, i32* %col, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %35 = bitcast i32* %almost_opaque_val_int to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %almost_opaque_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast float* %min_opaque_val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  ret void
}

; Function Attrs: nounwind
declare void @llvm.va_start(i8*) #8

declare i32 @vsnprintf(i8*, i32, i8*, i8*) #1

; Function Attrs: nounwind
declare void @llvm.va_end(i8*) #8

; Function Attrs: nounwind
define internal zeroext i1 @liq_image_get_row_f_init(%struct.liq_image* nonnull %img) #4 {
entry:
  %retval = alloca i1, align 1
  %img.addr = alloca %struct.liq_image*, align 4
  %gamma_lut = alloca [256 x float], align 16
  %i = alloca i32, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %f_pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %0, i32 0, i32 3
  %1 = load %struct.f_pixel*, %struct.f_pixel** %f_pixels, align 4, !tbaa !90
  %tobool = icmp ne %struct.f_pixel* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %call = call zeroext i1 @liq_image_should_use_low_memory(%struct.liq_image* %2, i1 zeroext false)
  br i1 %call, label %if.end5, label %if.then1

if.then1:                                         ; preds = %if.end
  %3 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.liq_image, %struct.liq_image* %3, i32 0, i32 1
  %4 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !86
  %5 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %5, i32 0, i32 6
  %6 = load i32, i32* %width, align 8, !tbaa !85
  %mul = mul i32 16, %6
  %7 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %height = getelementptr inbounds %struct.liq_image, %struct.liq_image* %7, i32 0, i32 7
  %8 = load i32, i32* %height, align 4, !tbaa !84
  %mul2 = mul i32 %mul, %8
  %call3 = call i8* %4(i32 %mul2)
  %9 = bitcast i8* %call3 to %struct.f_pixel*
  %10 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %f_pixels4 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %10, i32 0, i32 3
  store %struct.f_pixel* %9, %struct.f_pixel** %f_pixels4, align 4, !tbaa !90
  br label %if.end5

if.end5:                                          ; preds = %if.then1, %if.end
  %11 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %f_pixels6 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %11, i32 0, i32 3
  %12 = load %struct.f_pixel*, %struct.f_pixel** %f_pixels6, align 4, !tbaa !90
  %tobool7 = icmp ne %struct.f_pixel* %12, null
  br i1 %tobool7, label %if.end10, label %if.then8

if.then8:                                         ; preds = %if.end5
  %13 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %call9 = call zeroext i1 @liq_image_use_low_memory(%struct.liq_image* %13)
  store i1 %call9, i1* %retval, align 1
  br label %return

if.end10:                                         ; preds = %if.end5
  %14 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %call11 = call zeroext i1 @liq_image_has_rgba_pixels(%struct.liq_image* %14)
  br i1 %call11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %if.end10
  store i1 false, i1* %retval, align 1
  br label %return

if.end13:                                         ; preds = %if.end10
  %15 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %15) #8
  %arraydecay = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %16 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_image, %struct.liq_image* %16, i32 0, i32 5
  %17 = load double, double* %gamma, align 8, !tbaa !60
  call void @to_f_set_gamma(float* %arraydecay, double %17)
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store i32 0, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end13
  %19 = load i32, i32* %i, align 4, !tbaa !13
  %20 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %height14 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %20, i32 0, i32 7
  %21 = load i32, i32* %height14, align 4, !tbaa !84
  %cmp = icmp ult i32 %19, %21
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %24 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %f_pixels15 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %24, i32 0, i32 3
  %25 = load %struct.f_pixel*, %struct.f_pixel** %f_pixels15, align 4, !tbaa !90
  %26 = load i32, i32* %i, align 4, !tbaa !13
  %27 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width16 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %27, i32 0, i32 6
  %28 = load i32, i32* %width16, align 8, !tbaa !85
  %mul17 = mul i32 %26, %28
  %arrayidx = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %25, i32 %mul17
  %29 = load i32, i32* %i, align 4, !tbaa !13
  %arraydecay18 = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  call void @convert_row_to_f(%struct.liq_image* %23, %struct.f_pixel* %arrayidx, i32 %29, float* %arraydecay18)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !13
  %inc = add i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  store i1 true, i1* %retval, align 1
  %31 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %31) #8
  br label %return

return:                                           ; preds = %for.end, %if.then12, %if.then8, %if.then
  %32 = load i1, i1* %retval, align 1
  ret i1 %32
}

; Function Attrs: nounwind
define internal %struct.f_pixel* @liq_image_get_row_f(%struct.liq_image* nonnull %img, i32 %row) #4 {
entry:
  %retval = alloca %struct.f_pixel*, align 4
  %img.addr = alloca %struct.liq_image*, align 4
  %row.addr = alloca i32, align 4
  %gamma_lut = alloca [256 x float], align 16
  %row_for_thread = alloca %struct.f_pixel*, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !13
  %0 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %f_pixels = getelementptr inbounds %struct.liq_image, %struct.liq_image* %0, i32 0, i32 3
  %1 = load %struct.f_pixel*, %struct.f_pixel** %f_pixels, align 4, !tbaa !90
  %tobool = icmp ne %struct.f_pixel* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %2 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %2) #8
  %arraydecay = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %3 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %gamma = getelementptr inbounds %struct.liq_image, %struct.liq_image* %3, i32 0, i32 5
  %4 = load double, double* %gamma, align 8, !tbaa !60
  call void @to_f_set_gamma(float* %arraydecay, double %4)
  %5 = bitcast %struct.f_pixel** %row_for_thread to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %temp_f_row = getelementptr inbounds %struct.liq_image, %struct.liq_image* %6, i32 0, i32 13
  %7 = load %struct.f_pixel*, %struct.f_pixel** %temp_f_row, align 4, !tbaa !92
  %8 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %8, i32 0, i32 6
  %9 = load i32, i32* %width, align 8, !tbaa !85
  %mul = mul i32 %9, 0
  %add.ptr = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %7, i32 %mul
  store %struct.f_pixel* %add.ptr, %struct.f_pixel** %row_for_thread, align 4, !tbaa !2
  %10 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %11 = load %struct.f_pixel*, %struct.f_pixel** %row_for_thread, align 4, !tbaa !2
  %12 = load i32, i32* %row.addr, align 4, !tbaa !13
  %arraydecay1 = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  call void @convert_row_to_f(%struct.liq_image* %10, %struct.f_pixel* %11, i32 %12, float* %arraydecay1)
  %13 = load %struct.f_pixel*, %struct.f_pixel** %row_for_thread, align 4, !tbaa !2
  store %struct.f_pixel* %13, %struct.f_pixel** %retval, align 4
  %14 = bitcast %struct.f_pixel** %row_for_thread to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %15) #8
  br label %return

if.end:                                           ; preds = %entry
  %16 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %f_pixels2 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %16, i32 0, i32 3
  %17 = load %struct.f_pixel*, %struct.f_pixel** %f_pixels2, align 4, !tbaa !90
  %18 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width3 = getelementptr inbounds %struct.liq_image, %struct.liq_image* %18, i32 0, i32 6
  %19 = load i32, i32* %width3, align 8, !tbaa !85
  %20 = load i32, i32* %row.addr, align 4, !tbaa !13
  %mul4 = mul i32 %19, %20
  %add.ptr5 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %17, i32 %mul4
  store %struct.f_pixel* %add.ptr5, %struct.f_pixel** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %21 = load %struct.f_pixel*, %struct.f_pixel** %retval, align 4
  ret %struct.f_pixel* %21
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #9

declare hidden void @liq_max3(i8*, i8*, i32, i32) #1

declare hidden void @liq_blur(i8*, i8*, i8*, i32, i32, i32) #1

declare hidden void @liq_min3(i8*, i8*, i32, i32) #1

; Function Attrs: nounwind
define internal void @convert_row_to_f(%struct.liq_image* nonnull %img, %struct.f_pixel* nonnull %row_f_pixels, i32 %row, float* nonnull %gamma_lut) #4 {
entry:
  %img.addr = alloca %struct.liq_image*, align 4
  %row_f_pixels.addr = alloca %struct.f_pixel*, align 4
  %row.addr = alloca i32, align 4
  %gamma_lut.addr = alloca float*, align 4
  %row_pixels = alloca %struct.rgba_pixel*, align 4
  %col = alloca i32, align 4
  %tmp = alloca %struct.f_pixel, align 4
  store %struct.liq_image* %img, %struct.liq_image** %img.addr, align 4, !tbaa !2
  store %struct.f_pixel* %row_f_pixels, %struct.f_pixel** %row_f_pixels.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !13
  store float* %gamma_lut, float** %gamma_lut.addr, align 4, !tbaa !2
  %0 = bitcast %struct.rgba_pixel** %row_pixels to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %2 = load i32, i32* %row.addr, align 4, !tbaa !13
  %call = call %struct.rgba_pixel* @liq_image_get_row_rgba(%struct.liq_image* %1, i32 %2)
  store %struct.rgba_pixel* %call, %struct.rgba_pixel** %row_pixels, align 4, !tbaa !2
  %3 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store i32 0, i32* %col, align 4, !tbaa !13
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %col, align 4, !tbaa !13
  %5 = load %struct.liq_image*, %struct.liq_image** %img.addr, align 4, !tbaa !2
  %width = getelementptr inbounds %struct.liq_image, %struct.liq_image* %5, i32 0, i32 6
  %6 = load i32, i32* %width, align 8, !tbaa !85
  %cmp = icmp ult i32 %4, %6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %struct.f_pixel*, %struct.f_pixel** %row_f_pixels.addr, align 4, !tbaa !2
  %9 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %8, i32 %9
  %10 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %11 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %12 = load %struct.rgba_pixel*, %struct.rgba_pixel** %row_pixels, align 4, !tbaa !2
  %13 = load i32, i32* %col, align 4, !tbaa !13
  %arrayidx1 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %12, i32 %13
  call void @rgba_to_f(%struct.f_pixel* sret align 4 %tmp, float* %11, %struct.rgba_pixel* byval(%struct.rgba_pixel) align 1 %arrayidx1)
  %14 = bitcast %struct.f_pixel* %arrayidx to i8*
  %15 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !71
  %16 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %col, align 4, !tbaa !13
  %inc = add i32 %17, 1
  store i32 %inc, i32* %col, align 4, !tbaa !13
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %18 = bitcast %struct.rgba_pixel** %row_pixels to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  ret void
}

declare hidden %struct.colormap* @pam_duplicate_colormap(%struct.colormap*) #1

declare hidden %struct.nearest_map* @nearest_init(%struct.colormap*) #1

declare hidden i32 @nearest_search(%struct.nearest_map*, %struct.f_pixel*, i32, float*) #1

declare hidden void @kmeans_init(%struct.colormap*, i32, %struct.kmeans_state*) #1

declare hidden void @kmeans_update_color(%struct.f_pixel* byval(%struct.f_pixel) align 4, float, %struct.colormap*, i32, i32, %struct.kmeans_state*) #1

declare hidden void @kmeans_finalize(%struct.colormap*, i32, %struct.kmeans_state*) #1

declare hidden void @nearest_free(%struct.nearest_map*) #1

; Function Attrs: inlinehint nounwind
define internal void @get_dithered_pixel(%struct.f_pixel* noalias sret align 4 %agg.result, float %dither_level, float %max_dither_error, %struct.f_pixel* byval(%struct.f_pixel) align 4 %thiserr, %struct.f_pixel* byval(%struct.f_pixel) align 4 %px) #7 {
entry:
  %dither_level.addr = alloca float, align 4
  %max_dither_error.addr = alloca float, align 4
  %sr = alloca float, align 4
  %sg = alloca float, align 4
  %sb = alloca float, align 4
  %sa = alloca float, align 4
  %ratio = alloca float, align 4
  %max_overflow = alloca float, align 4
  %max_underflow = alloca float, align 4
  %a92 = alloca float, align 4
  %dither_error = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store float %dither_level, float* %dither_level.addr, align 4, !tbaa !55
  store float %max_dither_error, float* %max_dither_error.addr, align 4, !tbaa !55
  %0 = bitcast float* %sr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %thiserr, i32 0, i32 1
  %1 = load float, float* %r, align 4, !tbaa !74
  %2 = load float, float* %dither_level.addr, align 4, !tbaa !55
  %mul = fmul float %1, %2
  store float %mul, float* %sr, align 4, !tbaa !55
  %3 = bitcast float* %sg to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %thiserr, i32 0, i32 2
  %4 = load float, float* %g, align 4, !tbaa !75
  %5 = load float, float* %dither_level.addr, align 4, !tbaa !55
  %mul1 = fmul float %4, %5
  store float %mul1, float* %sg, align 4, !tbaa !55
  %6 = bitcast float* %sb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %thiserr, i32 0, i32 3
  %7 = load float, float* %b, align 4, !tbaa !76
  %8 = load float, float* %dither_level.addr, align 4, !tbaa !55
  %mul2 = fmul float %7, %8
  store float %mul2, float* %sb, align 4, !tbaa !55
  %9 = bitcast float* %sa to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %thiserr, i32 0, i32 0
  %10 = load float, float* %a, align 4, !tbaa !72
  %11 = load float, float* %dither_level.addr, align 4, !tbaa !55
  %mul3 = fmul float %10, %11
  store float %mul3, float* %sa, align 4, !tbaa !55
  %12 = bitcast float* %ratio to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float 1.000000e+00, float* %ratio, align 4, !tbaa !55
  %13 = bitcast float* %max_overflow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  store float 0x3FF19999A0000000, float* %max_overflow, align 4, !tbaa !55
  %14 = bitcast float* %max_underflow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 0xBFB99999A0000000, float* %max_underflow, align 4, !tbaa !55
  %r4 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %15 = load float, float* %r4, align 4, !tbaa !74
  %16 = load float, float* %sr, align 4, !tbaa !55
  %add = fadd float %15, %16
  %cmp = fcmp ogt float %add, 0x3FF19999A0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %17 = load float, float* %ratio, align 4, !tbaa !55
  %r5 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %18 = load float, float* %r5, align 4, !tbaa !74
  %sub = fsub float 0x3FF19999A0000000, %18
  %19 = load float, float* %sr, align 4, !tbaa !55
  %div = fdiv float %sub, %19
  %cmp6 = fcmp olt float %17, %div
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %20 = load float, float* %ratio, align 4, !tbaa !55
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %r7 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %21 = load float, float* %r7, align 4, !tbaa !74
  %sub8 = fsub float 0x3FF19999A0000000, %21
  %22 = load float, float* %sr, align 4, !tbaa !55
  %div9 = fdiv float %sub8, %22
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %20, %cond.true ], [ %div9, %cond.false ]
  store float %cond, float* %ratio, align 4, !tbaa !55
  br label %if.end25

if.else:                                          ; preds = %entry
  %r10 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %23 = load float, float* %r10, align 4, !tbaa !74
  %24 = load float, float* %sr, align 4, !tbaa !55
  %add11 = fadd float %23, %24
  %cmp12 = fcmp olt float %add11, 0xBFB99999A0000000
  br i1 %cmp12, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.else
  %25 = load float, float* %ratio, align 4, !tbaa !55
  %r14 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %26 = load float, float* %r14, align 4, !tbaa !74
  %sub15 = fsub float 0xBFB99999A0000000, %26
  %27 = load float, float* %sr, align 4, !tbaa !55
  %div16 = fdiv float %sub15, %27
  %cmp17 = fcmp olt float %25, %div16
  br i1 %cmp17, label %cond.true18, label %cond.false19

cond.true18:                                      ; preds = %if.then13
  %28 = load float, float* %ratio, align 4, !tbaa !55
  br label %cond.end23

cond.false19:                                     ; preds = %if.then13
  %r20 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %29 = load float, float* %r20, align 4, !tbaa !74
  %sub21 = fsub float 0xBFB99999A0000000, %29
  %30 = load float, float* %sr, align 4, !tbaa !55
  %div22 = fdiv float %sub21, %30
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false19, %cond.true18
  %cond24 = phi float [ %28, %cond.true18 ], [ %div22, %cond.false19 ]
  store float %cond24, float* %ratio, align 4, !tbaa !55
  br label %if.end

if.end:                                           ; preds = %cond.end23, %if.else
  br label %if.end25

if.end25:                                         ; preds = %if.end, %cond.end
  %g26 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %31 = load float, float* %g26, align 4, !tbaa !75
  %32 = load float, float* %sg, align 4, !tbaa !55
  %add27 = fadd float %31, %32
  %cmp28 = fcmp ogt float %add27, 0x3FF19999A0000000
  br i1 %cmp28, label %if.then29, label %if.else41

if.then29:                                        ; preds = %if.end25
  %33 = load float, float* %ratio, align 4, !tbaa !55
  %g30 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %34 = load float, float* %g30, align 4, !tbaa !75
  %sub31 = fsub float 0x3FF19999A0000000, %34
  %35 = load float, float* %sg, align 4, !tbaa !55
  %div32 = fdiv float %sub31, %35
  %cmp33 = fcmp olt float %33, %div32
  br i1 %cmp33, label %cond.true34, label %cond.false35

cond.true34:                                      ; preds = %if.then29
  %36 = load float, float* %ratio, align 4, !tbaa !55
  br label %cond.end39

cond.false35:                                     ; preds = %if.then29
  %g36 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %37 = load float, float* %g36, align 4, !tbaa !75
  %sub37 = fsub float 0x3FF19999A0000000, %37
  %38 = load float, float* %sg, align 4, !tbaa !55
  %div38 = fdiv float %sub37, %38
  br label %cond.end39

cond.end39:                                       ; preds = %cond.false35, %cond.true34
  %cond40 = phi float [ %36, %cond.true34 ], [ %div38, %cond.false35 ]
  store float %cond40, float* %ratio, align 4, !tbaa !55
  br label %if.end58

if.else41:                                        ; preds = %if.end25
  %g42 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %39 = load float, float* %g42, align 4, !tbaa !75
  %40 = load float, float* %sg, align 4, !tbaa !55
  %add43 = fadd float %39, %40
  %cmp44 = fcmp olt float %add43, 0xBFB99999A0000000
  br i1 %cmp44, label %if.then45, label %if.end57

if.then45:                                        ; preds = %if.else41
  %41 = load float, float* %ratio, align 4, !tbaa !55
  %g46 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %42 = load float, float* %g46, align 4, !tbaa !75
  %sub47 = fsub float 0xBFB99999A0000000, %42
  %43 = load float, float* %sg, align 4, !tbaa !55
  %div48 = fdiv float %sub47, %43
  %cmp49 = fcmp olt float %41, %div48
  br i1 %cmp49, label %cond.true50, label %cond.false51

cond.true50:                                      ; preds = %if.then45
  %44 = load float, float* %ratio, align 4, !tbaa !55
  br label %cond.end55

cond.false51:                                     ; preds = %if.then45
  %g52 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %45 = load float, float* %g52, align 4, !tbaa !75
  %sub53 = fsub float 0xBFB99999A0000000, %45
  %46 = load float, float* %sg, align 4, !tbaa !55
  %div54 = fdiv float %sub53, %46
  br label %cond.end55

cond.end55:                                       ; preds = %cond.false51, %cond.true50
  %cond56 = phi float [ %44, %cond.true50 ], [ %div54, %cond.false51 ]
  store float %cond56, float* %ratio, align 4, !tbaa !55
  br label %if.end57

if.end57:                                         ; preds = %cond.end55, %if.else41
  br label %if.end58

if.end58:                                         ; preds = %if.end57, %cond.end39
  %b59 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %47 = load float, float* %b59, align 4, !tbaa !76
  %48 = load float, float* %sb, align 4, !tbaa !55
  %add60 = fadd float %47, %48
  %cmp61 = fcmp ogt float %add60, 0x3FF19999A0000000
  br i1 %cmp61, label %if.then62, label %if.else74

if.then62:                                        ; preds = %if.end58
  %49 = load float, float* %ratio, align 4, !tbaa !55
  %b63 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %50 = load float, float* %b63, align 4, !tbaa !76
  %sub64 = fsub float 0x3FF19999A0000000, %50
  %51 = load float, float* %sb, align 4, !tbaa !55
  %div65 = fdiv float %sub64, %51
  %cmp66 = fcmp olt float %49, %div65
  br i1 %cmp66, label %cond.true67, label %cond.false68

cond.true67:                                      ; preds = %if.then62
  %52 = load float, float* %ratio, align 4, !tbaa !55
  br label %cond.end72

cond.false68:                                     ; preds = %if.then62
  %b69 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %53 = load float, float* %b69, align 4, !tbaa !76
  %sub70 = fsub float 0x3FF19999A0000000, %53
  %54 = load float, float* %sb, align 4, !tbaa !55
  %div71 = fdiv float %sub70, %54
  br label %cond.end72

cond.end72:                                       ; preds = %cond.false68, %cond.true67
  %cond73 = phi float [ %52, %cond.true67 ], [ %div71, %cond.false68 ]
  store float %cond73, float* %ratio, align 4, !tbaa !55
  br label %if.end91

if.else74:                                        ; preds = %if.end58
  %b75 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %55 = load float, float* %b75, align 4, !tbaa !76
  %56 = load float, float* %sb, align 4, !tbaa !55
  %add76 = fadd float %55, %56
  %cmp77 = fcmp olt float %add76, 0xBFB99999A0000000
  br i1 %cmp77, label %if.then78, label %if.end90

if.then78:                                        ; preds = %if.else74
  %57 = load float, float* %ratio, align 4, !tbaa !55
  %b79 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %58 = load float, float* %b79, align 4, !tbaa !76
  %sub80 = fsub float 0xBFB99999A0000000, %58
  %59 = load float, float* %sb, align 4, !tbaa !55
  %div81 = fdiv float %sub80, %59
  %cmp82 = fcmp olt float %57, %div81
  br i1 %cmp82, label %cond.true83, label %cond.false84

cond.true83:                                      ; preds = %if.then78
  %60 = load float, float* %ratio, align 4, !tbaa !55
  br label %cond.end88

cond.false84:                                     ; preds = %if.then78
  %b85 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %61 = load float, float* %b85, align 4, !tbaa !76
  %sub86 = fsub float 0xBFB99999A0000000, %61
  %62 = load float, float* %sb, align 4, !tbaa !55
  %div87 = fdiv float %sub86, %62
  br label %cond.end88

cond.end88:                                       ; preds = %cond.false84, %cond.true83
  %cond89 = phi float [ %60, %cond.true83 ], [ %div87, %cond.false84 ]
  store float %cond89, float* %ratio, align 4, !tbaa !55
  br label %if.end90

if.end90:                                         ; preds = %cond.end88, %if.else74
  br label %if.end91

if.end91:                                         ; preds = %if.end90, %cond.end72
  %63 = bitcast float* %a92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #8
  %a93 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %64 = load float, float* %a93, align 4, !tbaa !72
  %65 = load float, float* %sa, align 4, !tbaa !55
  %add94 = fadd float %64, %65
  store float %add94, float* %a92, align 4, !tbaa !55
  %66 = load float, float* %a92, align 4, !tbaa !55
  %cmp95 = fcmp ogt float %66, 1.000000e+00
  br i1 %cmp95, label %if.then96, label %if.else97

if.then96:                                        ; preds = %if.end91
  store float 1.000000e+00, float* %a92, align 4, !tbaa !55
  br label %if.end101

if.else97:                                        ; preds = %if.end91
  %67 = load float, float* %a92, align 4, !tbaa !55
  %cmp98 = fcmp olt float %67, 0.000000e+00
  br i1 %cmp98, label %if.then99, label %if.end100

if.then99:                                        ; preds = %if.else97
  store float 0.000000e+00, float* %a92, align 4, !tbaa !55
  br label %if.end100

if.end100:                                        ; preds = %if.then99, %if.else97
  br label %if.end101

if.end101:                                        ; preds = %if.end100, %if.then96
  %68 = bitcast float* %dither_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #8
  %69 = load float, float* %sr, align 4, !tbaa !55
  %70 = load float, float* %sr, align 4, !tbaa !55
  %mul102 = fmul float %69, %70
  %71 = load float, float* %sg, align 4, !tbaa !55
  %72 = load float, float* %sg, align 4, !tbaa !55
  %mul103 = fmul float %71, %72
  %add104 = fadd float %mul102, %mul103
  %73 = load float, float* %sb, align 4, !tbaa !55
  %74 = load float, float* %sb, align 4, !tbaa !55
  %mul105 = fmul float %73, %74
  %add106 = fadd float %add104, %mul105
  %75 = load float, float* %sa, align 4, !tbaa !55
  %76 = load float, float* %sa, align 4, !tbaa !55
  %mul107 = fmul float %75, %76
  %add108 = fadd float %add106, %mul107
  store float %add108, float* %dither_error, align 4, !tbaa !55
  %77 = load float, float* %dither_error, align 4, !tbaa !55
  %78 = load float, float* %max_dither_error.addr, align 4, !tbaa !55
  %cmp109 = fcmp ogt float %77, %78
  br i1 %cmp109, label %if.then110, label %if.else112

if.then110:                                       ; preds = %if.end101
  %79 = load float, float* %ratio, align 4, !tbaa !55
  %mul111 = fmul float %79, 0x3FE99999A0000000
  store float %mul111, float* %ratio, align 4, !tbaa !55
  br label %if.end116

if.else112:                                       ; preds = %if.end101
  %80 = load float, float* %dither_error, align 4, !tbaa !55
  %cmp113 = fcmp olt float %80, 0x3F00000000000000
  br i1 %cmp113, label %if.then114, label %if.end115

if.then114:                                       ; preds = %if.else112
  %81 = bitcast %struct.f_pixel* %agg.result to i8*
  %82 = bitcast %struct.f_pixel* %px to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 16, i1 false), !tbaa.struct !71
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end115:                                        ; preds = %if.else112
  br label %if.end116

if.end116:                                        ; preds = %if.end115, %if.then110
  %a117 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 0
  %83 = load float, float* %a92, align 4, !tbaa !55
  store float %83, float* %a117, align 4, !tbaa !72
  %r118 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 1
  %r119 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %84 = load float, float* %r119, align 4, !tbaa !74
  %85 = load float, float* %sr, align 4, !tbaa !55
  %86 = load float, float* %ratio, align 4, !tbaa !55
  %mul120 = fmul float %85, %86
  %add121 = fadd float %84, %mul120
  store float %add121, float* %r118, align 4, !tbaa !74
  %g122 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 2
  %g123 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %87 = load float, float* %g123, align 4, !tbaa !75
  %88 = load float, float* %sg, align 4, !tbaa !55
  %89 = load float, float* %ratio, align 4, !tbaa !55
  %mul124 = fmul float %88, %89
  %add125 = fadd float %87, %mul124
  store float %add125, float* %g122, align 4, !tbaa !75
  %b126 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 3
  %b127 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %90 = load float, float* %b127, align 4, !tbaa !76
  %91 = load float, float* %sb, align 4, !tbaa !55
  %92 = load float, float* %ratio, align 4, !tbaa !55
  %mul128 = fmul float %91, %92
  %add129 = fadd float %90, %mul128
  store float %add129, float* %b126, align 4, !tbaa !76
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end116, %if.then114
  %93 = bitcast float* %dither_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #8
  %94 = bitcast float* %a92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  %95 = bitcast float* %max_underflow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #8
  %96 = bitcast float* %max_overflow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #8
  %97 = bitcast float* %ratio to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #8
  %98 = bitcast float* %sa to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #8
  %99 = bitcast float* %sb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #8
  %100 = bitcast float* %sg to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #8
  %101 = bitcast float* %sr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  ret void
}

attributes #0 = { noinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn writeonly }
attributes #7 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 0}
!7 = !{!"liq_attr", !3, i64 0, !3, i64 4, !3, i64 8, !8, i64 16, !8, i64 24, !8, i64 32, !9, i64 40, !10, i64 44, !10, i64 48, !10, i64 52, !10, i64 56, !10, i64 60, !10, i64 64, !11, i64 68, !11, i64 69, !4, i64 70, !4, i64 71, !4, i64 72, !4, i64 73, !4, i64 74, !3, i64 76, !3, i64 80, !3, i64 84, !3, i64 88, !3, i64 92, !3, i64 96}
!8 = !{!"double", !4, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!"int", !4, i64 0}
!11 = !{!"_Bool", !4, i64 0}
!12 = !{!4, !4, i64 0}
!13 = !{!10, !10, i64 0}
!14 = !{!7, !8, i64 16}
!15 = !{!7, !8, i64 24}
!16 = !{!17, !17, i64 0}
!17 = !{!"long", !4, i64 0}
!18 = !{!8, !8, i64 0}
!19 = !{!7, !10, i64 44}
!20 = !{!7, !10, i64 52}
!21 = !{!7, !10, i64 60}
!22 = !{!7, !8, i64 32}
!23 = !{!7, !10, i64 64}
!24 = !{!7, !10, i64 48}
!25 = !{!7, !10, i64 56}
!26 = !{!7, !4, i64 70}
!27 = !{!7, !11, i64 69}
!28 = !{!7, !4, i64 71}
!29 = !{i8 0, i8 2}
!30 = !{!7, !4, i64 72}
!31 = !{!7, !4, i64 74}
!32 = !{!7, !4, i64 73}
!33 = !{!34, !3, i64 12}
!34 = !{!"liq_result", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !35, i64 28, !9, i64 1056, !8, i64 1064, !8, i64 1072, !10, i64 1080, !4, i64 1084}
!35 = !{!"liq_palette", !10, i64 0, !4, i64 4}
!36 = !{!34, !8, i64 1064}
!37 = !{!38, !3, i64 16}
!38 = !{!"liq_remapping_result", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !35, i64 28, !8, i64 1056, !8, i64 1064, !9, i64 1072, !4, i64 1076, !4, i64 1077}
!39 = !{!38, !3, i64 12}
!40 = !{!38, !3, i64 8}
!41 = !{!38, !3, i64 0}
!42 = !{!7, !9, i64 40}
!43 = !{!7, !11, i64 68}
!44 = !{!7, !3, i64 76}
!45 = !{!7, !3, i64 80}
!46 = !{!34, !3, i64 20}
!47 = !{!34, !3, i64 24}
!48 = !{!7, !3, i64 84}
!49 = !{!7, !3, i64 88}
!50 = !{!7, !3, i64 92}
!51 = !{!7, !3, i64 96}
!52 = !{!7, !3, i64 4}
!53 = !{!7, !3, i64 8}
!54 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 16, i64 8, !18, i64 24, i64 8, !18, i64 32, i64 8, !18, i64 40, i64 4, !55, i64 44, i64 4, !13, i64 48, i64 4, !13, i64 52, i64 4, !13, i64 56, i64 4, !13, i64 60, i64 4, !13, i64 64, i64 4, !13, i64 68, i64 1, !56, i64 69, i64 1, !56, i64 70, i64 1, !12, i64 71, i64 1, !12, i64 72, i64 1, !12, i64 73, i64 1, !12, i64 74, i64 1, !12, i64 76, i64 4, !2, i64 80, i64 4, !2, i64 84, i64 4, !2, i64 88, i64 4, !2, i64 92, i64 4, !2, i64 96, i64 4, !2}
!55 = !{!9, !9, i64 0}
!56 = !{!11, !11, i64 0}
!57 = !{!58, !59, i64 4176}
!58 = !{!"liq_image", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 24, !10, i64 32, !10, i64 36, !3, i64 40, !3, i64 44, !3, i64 48, !3, i64 52, !3, i64 56, !3, i64 60, !3, i64 64, !3, i64 68, !3, i64 72, !9, i64 76, !4, i64 80, !59, i64 4176, !11, i64 4178, !11, i64 4179, !11, i64 4180}
!59 = !{!"short", !4, i64 0}
!60 = !{!58, !8, i64 24}
!61 = !{!62, !4, i64 0}
!62 = !{!"liq_color", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3}
!63 = !{!64, !4, i64 0}
!64 = !{!"", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3}
!65 = !{!62, !4, i64 1}
!66 = !{!64, !4, i64 1}
!67 = !{!62, !4, i64 2}
!68 = !{!64, !4, i64 2}
!69 = !{!62, !4, i64 3}
!70 = !{!64, !4, i64 3}
!71 = !{i64 0, i64 4, !55, i64 4, i64 4, !55, i64 8, i64 4, !55, i64 12, i64 4, !55}
!72 = !{!73, !9, i64 0}
!73 = !{!"", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!74 = !{!73, !9, i64 4}
!75 = !{!73, !9, i64 8}
!76 = !{!73, !9, i64 12}
!77 = !{!78, !59, i64 4120}
!78 = !{!"liq_histogram", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !4, i64 24, !59, i64 4120, !59, i64 4122, !11, i64 4124}
!79 = !{!58, !3, i64 16}
!80 = !{!58, !11, i64 4180}
!81 = !{!58, !11, i64 4179}
!82 = !{!58, !11, i64 4178}
!83 = !{!58, !3, i64 52}
!84 = !{!58, !10, i64 36}
!85 = !{!58, !10, i64 32}
!86 = !{!58, !3, i64 4}
!87 = !{!58, !3, i64 40}
!88 = !{!58, !3, i64 8}
!89 = !{!58, !3, i64 72}
!90 = !{!58, !3, i64 12}
!91 = !{!58, !3, i64 56}
!92 = !{!58, !3, i64 60}
!93 = !{!58, !3, i64 0}
!94 = !{!58, !3, i64 44}
!95 = !{!58, !3, i64 48}
!96 = !{!58, !3, i64 64}
!97 = !{!58, !3, i64 68}
!98 = !{!58, !9, i64 76}
!99 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 16, i64 4, !2, i64 24, i64 8, !18, i64 32, i64 4, !13, i64 36, i64 4, !13, i64 40, i64 4, !2, i64 44, i64 4, !2, i64 48, i64 4, !2, i64 52, i64 4, !2, i64 56, i64 4, !2, i64 60, i64 4, !2, i64 64, i64 4, !2, i64 68, i64 4, !2, i64 72, i64 4, !2, i64 76, i64 4, !55, i64 80, i64 4096, !12, i64 4176, i64 2, !100, i64 4178, i64 1, !56, i64 4179, i64 1, !56, i64 4180, i64 1, !56}
!100 = !{!59, !59, i64 0}
!101 = !{!78, !3, i64 0}
!102 = !{!78, !3, i64 4}
!103 = !{!78, !3, i64 8}
!104 = !{!78, !59, i64 4122}
!105 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 16, i64 8, !18, i64 24, i64 4096, !12, i64 4120, i64 2, !100, i64 4122, i64 2, !100, i64 4124, i64 1, !56}
!106 = !{!78, !3, i64 12}
!107 = !{!78, !8, i64 16}
!108 = !{!78, !11, i64 4124}
!109 = !{!34, !9, i64 1056}
!110 = !{!34, !3, i64 16}
!111 = !{!34, !3, i64 0}
!112 = !{!34, !3, i64 8}
!113 = !{!34, !8, i64 1072}
!114 = !{!38, !8, i64 1064}
!115 = !{!38, !10, i64 28}
!116 = !{!34, !10, i64 28}
!117 = !{!34, !10, i64 1080}
!118 = !{!35, !10, i64 0}
!119 = !{!120, !11, i64 20}
!120 = !{!"", !73, i64 0, !9, i64 16, !11, i64 20}
!121 = !{i64 0, i64 1, !12, i64 1, i64 1, !12, i64 2, i64 1, !12, i64 3, i64 1, !12}
!122 = !{!123, !4, i64 0}
!123 = !{!"liq_histogram_entry", !62, i64 0, !10, i64 4}
!124 = !{!123, !4, i64 1}
!125 = !{!123, !4, i64 2}
!126 = !{!123, !4, i64 3}
!127 = !{!123, !10, i64 4}
!128 = !{!34, !4, i64 1084}
!129 = !{!38, !4, i64 1077}
!130 = !{!38, !9, i64 1072}
!131 = !{!38, !8, i64 1056}
!132 = !{!38, !4, i64 1076}
!133 = !{!34, !3, i64 4}
!134 = !{!38, !3, i64 4}
!135 = !{!38, !3, i64 20}
!136 = !{!38, !3, i64 24}
!137 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 16, i64 4, !2, i64 20, i64 4, !2, i64 24, i64 4, !2, i64 28, i64 4, !13, i64 32, i64 1024, !12, i64 1056, i64 8, !18, i64 1064, i64 8, !18, i64 1072, i64 4, !55, i64 1076, i64 1, !12, i64 1077, i64 1, !12}
!138 = !{!120, !9, i64 0}
!139 = !{!140, !10, i64 16}
!140 = !{!"", !3, i64 0, !3, i64 4, !8, i64 8, !10, i64 16, !10, i64 20}
!141 = !{!140, !3, i64 0}
!142 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 16, i64 4, !2, i64 20, i64 4, !2, i64 24, i64 4, !2, i64 28, i64 4, !13, i64 32, i64 1024, !12, i64 1056, i64 4, !55, i64 1064, i64 8, !18, i64 1072, i64 8, !18, i64 1080, i64 4, !13, i64 1084, i64 1, !12}
!143 = !{i64 0, i64 4, !55, i64 4, i64 4, !55, i64 8, i64 4, !55, i64 12, i64 4, !55, i64 16, i64 4, !55, i64 20, i64 4, !55, i64 24, i64 4, !55, i64 28, i64 4, !13, i64 28, i64 1, !12}
!144 = !{i64 0, i64 4, !55, i64 4, i64 4, !55, i64 8, i64 4, !55, i64 12, i64 4, !55, i64 16, i64 4, !55, i64 20, i64 1, !56}
!145 = !{!120, !9, i64 16}
!146 = !{!147, !9, i64 20}
!147 = !{!"", !73, i64 0, !9, i64 16, !9, i64 20, !9, i64 24, !4, i64 28}
!148 = !{!147, !9, i64 16}
