; ModuleID = 'imagequant.cpp'
source_filename = "imagequant.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.emscripten::val" = type { %"struct.emscripten::internal::_EM_VAL"* }
%"struct.emscripten::internal::_EM_VAL" = type opaque
%struct.liq_color = type { i8, i8, i8, i8 }
%struct.EmscriptenBindingInitializer_my_module = type { i8 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %struct.liq_result* }
%struct.liq_result = type opaque
%struct.liq_image = type opaque
%struct.liq_attr = type opaque
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"class.std::__2::unique_ptr.4" = type { %"class.std::__2::__compressed_pair.5" }
%"class.std::__2::__compressed_pair.5" = type { %"struct.std::__2::__compressed_pair_elem.6" }
%"struct.std::__2::__compressed_pair_elem.6" = type { %struct.liq_attr* }
%"class.std::__2::unique_ptr.9" = type { %"class.std::__2::__compressed_pair.10" }
%"class.std::__2::__compressed_pair.10" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"struct.std::__2::__compressed_pair_elem.11" = type { %struct.liq_image* }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i8*, i8*, %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { i8* }
%"class.std::__2::vector.19" = type { %"class.std::__2::__vector_base.20" }
%"class.std::__2::__vector_base.20" = type { %struct.liq_color*, %struct.liq_color*, %"class.std::__2::__compressed_pair.21" }
%"class.std::__2::__compressed_pair.21" = type { %"struct.std::__2::__compressed_pair_elem.22" }
%"struct.std::__2::__compressed_pair_elem.22" = type { %struct.liq_color* }
%struct.liq_palette = type { i32, [256 x %struct.liq_color] }
%"struct.emscripten::memory_view" = type { i32, i8* }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31" = type { i8 }
%struct.anon.32 = type { i32, [1 x i8] }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33" = type { i8 }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"struct.std::__2::__compressed_pair_elem.7" = type { i8 }
%"struct.std::__2::integral_constant.8" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.12" = type { i8 }
%"struct.std::__2::integral_constant.13" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::allocator.17" = type { i8 }
%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i8*, i8* }
%"struct.std::__2::__compressed_pair_elem.16" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::integral_constant.26" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"class.std::length_error" = type { %"class.std::logic_error" }
%"class.std::logic_error" = type { %"class.std::exception", %"class.std::__2::__libcpp_refstring" }
%"class.std::exception" = type { i32 (...)** }
%"class.std::__2::__libcpp_refstring" = type { i8* }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::allocator.24" = type { i8 }
%"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction" = type { %"class.std::__2::vector.19"*, %struct.liq_color*, %struct.liq_color* }
%"struct.std::__2::__compressed_pair_elem.23" = type { i8 }
%"struct.std::__2::__has_max_size.27" = type { i8 }
%"struct.std::__2::__has_construct.28" = type { i8 }
%"struct.std::__2::__has_destroy.29" = type { i8 }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList" = type { i8 }
%"struct.emscripten::internal::WireTypePack" = type { %"struct.std::__2::array" }
%"struct.std::__2::array" = type { [1 x %"union.emscripten::internal::GenericWireType"] }
%"union.emscripten::internal::GenericWireType" = type { double }
%union.anon.30 = type { i32 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }
%"class.std::__2::allocator" = type { i8 }

$_ZN10emscripten3val6globalEPKc = comdat any

$_ZN10emscripten3valD2Ev = comdat any

$_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEEC2ILb1EvEES3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv = comdat any

$_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEEC2ILb1EvEES3_ = comdat any

$_ZNKSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE3getEv = comdat any

$_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEEC2ILb1EvEES3_ = comdat any

$_ZNKSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE3getEv = comdat any

$_ZNKSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE3getEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEC2Em = comdat any

$_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEEC2Em = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEEixEm = comdat any

$_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_ = comdat any

$_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_ = comdat any

$_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4sizeEv = comdat any

$_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv = comdat any

$_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEED2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEED2Ev = comdat any

$_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEED2Ev = comdat any

$_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEED2Ev = comdat any

$_Z3powIiiENSt3__29_MetaBaseIXaasr3std13is_arithmeticIT_EE5valuesr3std13is_arithmeticIT0_EE5valueEE13_EnableIfImplINS0_9__promoteIS2_S3_vEEE4typeES2_S3_ = comdat any

$_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEiiifEJEEEvPKcPFT_DpT0_EDpT1_ = comdat any

$_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEiifEJEEEvPKcPFT_DpT0_EDpT1_ = comdat any

$_ZN10emscripten8functionIiJEJEEEvPKcPFT_DpT0_EDpT1_ = comdat any

$_ZN10emscripten3valC2EPNS_8internal7_EM_VALE = comdat any

$_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRP10liq_resultEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EEC2IRS2_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE5resetES3_ = comdat any

$_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE6secondEv = comdat any

$_ZNKSt3__217integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEcvS4_Ev = comdat any

$_ZNSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRP8liq_attrEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EEC2IRS2_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE5resetES3_ = comdat any

$_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE6secondEv = comdat any

$_ZNKSt3__217integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEcvS4_Ev = comdat any

$_ZNSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRP9liq_imageEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EEC2IRS2_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE5resetES3_ = comdat any

$_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE6secondEv = comdat any

$_ZNKSt3__217integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEcvS4_Ev = comdat any

$_ZNSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endEm = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIhEC2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIhE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorIhE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt12length_errorC2EPKc = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9constructIhJEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE11__constructIhJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIhE9constructIhJEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEEC2Ev = comdat any

$_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE11__vallocateEm = comdat any

$_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE18__construct_at_endEm = comdat any

$_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorI9liq_colorEC2Ev = comdat any

$_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE8allocateERS3_m = comdat any

$_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE9__end_capEv = comdat any

$_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE14__annotate_newEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE8max_sizeERKS3_ = comdat any

$_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE10__max_sizeENS_17integral_constantIbLb1EEERKS3_ = comdat any

$_ZNKSt3__29allocatorI9liq_colorE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorI9liq_colorE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE31__annotate_contiguous_containerEPKvS6_S6_S6_ = comdat any

$_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv = comdat any

$_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8capacityEv = comdat any

$_ZNSt3__212__to_addressI9liq_colorEEPT_S3_ = comdat any

$_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE21_ConstructTransactionC2ERS4_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE9constructIS2_JEEEvRS3_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE11__constructIS2_JEEEvNS_17integral_constantIbLb1EEERS3_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorI9liq_colorE9constructIS1_JEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE10deallocateERS3_PS2_m = comdat any

$_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE17__destruct_at_endEPS1_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE7destroyIS2_EEvRS3_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE9__destroyIS2_EEvNS_17integral_constantIbLb1EEERS3_PT_ = comdat any

$_ZNSt3__29allocatorI9liq_colorE7destroyEPS1_ = comdat any

$_ZNSt3__29allocatorI9liq_colorE10deallocateEPS1_m = comdat any

$_ZN10emscripten11memory_viewIhEC2EmPKh = comdat any

$_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_ = comdat any

$_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_ = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv = comdat any

$_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv = comdat any

$_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_ = comdat any

$_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE = comdat any

$_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_ = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv = comdat any

$_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv = comdat any

$_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEiiifEE6invokeEPFS2_S9_iiifEPNS0_11BindingTypeIS9_vEUt_Eiiif = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEE8getTypesEv = comdat any

$_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEPNS0_11BindingTypeISB_vEUt_EiiifEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_ = comdat any

$_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E = comdat any

$_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi = comdat any

$_ZN10emscripten8internal11BindingTypeIfvE12fromWireTypeEf = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiiifEEEE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEPNS0_11BindingTypeISB_vEUt_EiiifEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiiiiifEEEPKcv = comdat any

$_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEiifEE6invokeEPFS2_S9_iifEPNS0_11BindingTypeIS9_vEUt_Eiif = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEE8getTypesEv = comdat any

$_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEPNS0_11BindingTypeISB_vEUt_EiifEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiifEEEE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEPNS0_11BindingTypeISB_vEUt_EiifEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiiiifEEEPKcv = comdat any

$_ZN10emscripten8internal7InvokerIiJEE6invokeEPFivE = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getTypesEv = comdat any

$_ZN10emscripten8internal12getSignatureIiJPFivEEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJiPFivEEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiEEEPKcv = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types = comdat any

$_ZTSN10emscripten11memory_viewIhEE = comdat any

$_ZTIN10emscripten11memory_viewIhEE = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiiifEEEE3getEvE5types = comdat any

$_ZTSN10emscripten3valE = comdat any

$_ZTIN10emscripten3valE = comdat any

$_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTSNSt3__221__basic_string_commonILb1EEE = comdat any

$_ZTINSt3__221__basic_string_commonILb1EEE = comdat any

$_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiiiiifEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiifEEEE3getEvE5types = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiiiifEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEvE5types = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiEEEPKcvE9signature = comdat any

@_ZL17Uint8ClampedArray = internal thread_local global %"class.emscripten::val" zeroinitializer, align 4
@.str = private unnamed_addr constant [18 x i8] c"Uint8ClampedArray\00", align 1
@__dso_handle = external hidden global i8
@_ZL9zx_colors = internal constant [15 x %struct.liq_color] [%struct.liq_color { i8 0, i8 0, i8 0, i8 -1 }, %struct.liq_color { i8 0, i8 0, i8 -41, i8 -1 }, %struct.liq_color { i8 -41, i8 0, i8 0, i8 -1 }, %struct.liq_color { i8 -41, i8 0, i8 -41, i8 -1 }, %struct.liq_color { i8 0, i8 -41, i8 0, i8 -1 }, %struct.liq_color { i8 0, i8 -41, i8 -41, i8 -1 }, %struct.liq_color { i8 -41, i8 -41, i8 0, i8 -1 }, %struct.liq_color { i8 -41, i8 -41, i8 -41, i8 -1 }, %struct.liq_color { i8 0, i8 0, i8 -1, i8 -1 }, %struct.liq_color { i8 -1, i8 0, i8 0, i8 -1 }, %struct.liq_color { i8 -1, i8 0, i8 -1, i8 -1 }, %struct.liq_color { i8 0, i8 -1, i8 0, i8 -1 }, %struct.liq_color { i8 0, i8 -1, i8 -1, i8 -1 }, %struct.liq_color { i8 -1, i8 -1, i8 0, i8 -1 }, %struct.liq_color { i8 -1, i8 -1, i8 -1, i8 -1 }], align 16
@_ZL47EmscriptenBindingInitializer_my_module_instance = internal global %struct.EmscriptenBindingInitializer_my_module zeroinitializer, align 1
@.str.2 = private unnamed_addr constant [9 x i8] c"quantize\00", align 1
@.str.3 = private unnamed_addr constant [12 x i8] c"zx_quantize\00", align 1
@.str.4 = private unnamed_addr constant [8 x i8] c"version\00", align 1
@.str.5 = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1
@_ZTISt12length_error = external constant i8*
@_ZTVSt12length_error = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types = linkonce_odr hidden constant [1 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten11memory_viewIhEE to i8*)], comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN10emscripten11memory_viewIhEE = linkonce_odr hidden constant [31 x i8] c"N10emscripten11memory_viewIhEE\00", comdat, align 1
@_ZTIN10emscripten11memory_viewIhEE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTSN10emscripten11memory_viewIhEE, i32 0, i32 0) }, comdat, align 4
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiiifEEEE3getEvE5types = linkonce_odr hidden constant [6 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten3valE to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i8*, i32 }* @_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast (i8** @_ZTIf to i8*)], comdat, align 16
@_ZTSN10emscripten3valE = linkonce_odr hidden constant [19 x i8] c"N10emscripten3valE\00", comdat, align 1
@_ZTIN10emscripten3valE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN10emscripten3valE, i32 0, i32 0) }, comdat, align 4
@_ZTVN10__cxxabiv121__vmi_class_type_infoE = external global i8*
@_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant [63 x i8] c"NSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE\00", comdat, align 1
@_ZTSNSt3__221__basic_string_commonILb1EEE = linkonce_odr constant [38 x i8] c"NSt3__221__basic_string_commonILb1EEE\00", comdat, align 1
@_ZTINSt3__221__basic_string_commonILb1EEE = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([38 x i8], [38 x i8]* @_ZTSNSt3__221__basic_string_commonILb1EEE, i32 0, i32 0) }, comdat, align 4
@_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant { i8*, i8*, i32, i32, i8*, i32 } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv121__vmi_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([63 x i8], [63 x i8]* @_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, i32 0), i32 0, i32 1, i8* bitcast ({ i8*, i8* }* @_ZTINSt3__221__basic_string_commonILb1EEE to i8*), i32 0 }, comdat, align 4
@_ZTIi = external constant i8*
@_ZTIf = external constant i8*
@_ZZN10emscripten8internal19getGenericSignatureIJiiiiiifEEEPKcvE9signature = linkonce_odr hidden constant [8 x i8] c"iiiiiif\00", comdat, align 1
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiifEEEE3getEvE5types = linkonce_odr hidden constant [5 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten3valE to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i8*, i32 }* @_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast (i8** @_ZTIf to i8*)], comdat, align 16
@_ZZN10emscripten8internal19getGenericSignatureIJiiiiifEEEPKcvE9signature = linkonce_odr hidden constant [7 x i8] c"iiiiif\00", comdat, align 1
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEvE5types = linkonce_odr hidden constant [1 x i8*] [i8* bitcast (i8** @_ZTIi to i8*)], comdat, align 4
@_ZZN10emscripten8internal19getGenericSignatureIJiiEEEPKcvE9signature = linkonce_odr hidden constant [3 x i8] c"ii\00", comdat, align 1
@__tls_guard = internal thread_local global i8 0, align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_imagequant.cpp, i8* null }]

@_ZN38EmscriptenBindingInitializer_my_moduleC1Ev = hidden unnamed_addr alias %struct.EmscriptenBindingInitializer_my_module* (%struct.EmscriptenBindingInitializer_my_module*), %struct.EmscriptenBindingInitializer_my_module* (%struct.EmscriptenBindingInitializer_my_module*)* @_ZN38EmscriptenBindingInitializer_my_moduleC2Ev
@_ZTHL17Uint8ClampedArray = internal alias void (), void ()* @__tls_init

; Function Attrs: noinline nounwind optnone
define hidden i32 @_Z7versionv() #0 {
entry:
  ret i32 134144
}

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #1 {
entry:
  call void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* sret align 4 @_ZL17Uint8ClampedArray, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str, i32 0, i32 0))
  %0 = call i32 @__cxa_thread_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #3
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* noalias sret align 4 %agg.result, i8* %name) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %name.addr = alloca i8*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i8* %name, i8** %name.addr, align 4
  %1 = load i8*, i8** %name.addr, align 4
  %call = call %"struct.emscripten::internal::_EM_VAL"* @_emval_get_global(i8* %1)
  %call1 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call)
  ret void
}

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor(i8* %0) #1 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* @_ZL17Uint8ClampedArray) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"* %0)
  ret %"class.emscripten::val"* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_thread_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: noinline optnone
define hidden void @_Z18liq_image_quantizeP9liq_imageP8liq_attr(%"class.std::__2::unique_ptr"* noalias sret align 4 %agg.result, %struct.liq_image* %image, %struct.liq_attr* %attr) #2 {
entry:
  %result.ptr = alloca i8*, align 4
  %image.addr = alloca %struct.liq_image*, align 4
  %attr.addr = alloca %struct.liq_attr*, align 4
  %res = alloca %struct.liq_result*, align 4
  %0 = bitcast %"class.std::__2::unique_ptr"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.liq_image* %image, %struct.liq_image** %image.addr, align 4
  store %struct.liq_attr* %attr, %struct.liq_attr** %attr.addr, align 4
  store %struct.liq_result* null, %struct.liq_result** %res, align 4
  %1 = load %struct.liq_image*, %struct.liq_image** %image.addr, align 4
  %2 = load %struct.liq_attr*, %struct.liq_attr** %attr.addr, align 4
  %call = call i32 @liq_image_quantize(%struct.liq_image* %1, %struct.liq_attr* %2, %struct.liq_result** %res)
  %3 = load %struct.liq_result*, %struct.liq_result** %res, align 4
  %call1 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEEC2ILb1EvEES3_(%"class.std::__2::unique_ptr"* %agg.result, %struct.liq_result* %3) #3
  ret void
}

declare i32 @liq_image_quantize(%struct.liq_image*, %struct.liq_attr*, %struct.liq_result**) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEEC2ILb1EvEES3_(%"class.std::__2::unique_ptr"* returned %this, %struct.liq_result* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %struct.liq_result*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %struct.liq_result* %__p, %struct.liq_result** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__ptr_, %struct.liq_result** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline optnone
define hidden void @_Z8quantizeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEiiif(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.std::__2::basic_string"* %rawimage, i32 %image_width, i32 %image_height, i32 %num_colors, float %dithering) #2 {
entry:
  %result.ptr = alloca i8*, align 4
  %image_width.addr = alloca i32, align 4
  %image_height.addr = alloca i32, align 4
  %num_colors.addr = alloca i32, align 4
  %dithering.addr = alloca float, align 4
  %image_buffer = alloca %struct.liq_color*, align 4
  %size = alloca i32, align 4
  %attr = alloca %"class.std::__2::unique_ptr.4", align 4
  %image = alloca %"class.std::__2::unique_ptr.9", align 4
  %res = alloca %"class.std::__2::unique_ptr", align 4
  %image8bit = alloca %"class.std::__2::vector", align 4
  %result = alloca %"class.std::__2::vector.19", align 4
  %pal = alloca %struct.liq_palette*, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %"struct.emscripten::memory_view", align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i32 %image_width, i32* %image_width.addr, align 4
  store i32 %image_height, i32* %image_height.addr, align 4
  store i32 %num_colors, i32* %num_colors.addr, align 4
  store float %dithering, float* %dithering.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %rawimage) #3
  %1 = bitcast i8* %call to %struct.liq_color*
  store %struct.liq_color* %1, %struct.liq_color** %image_buffer, align 4
  %2 = load i32, i32* %image_width.addr, align 4
  %3 = load i32, i32* %image_height.addr, align 4
  %mul = mul nsw i32 %2, %3
  store i32 %mul, i32* %size, align 4
  %call1 = call %struct.liq_attr* @liq_attr_create()
  %call2 = call %"class.std::__2::unique_ptr.4"* @_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEEC2ILb1EvEES3_(%"class.std::__2::unique_ptr.4"* %attr, %struct.liq_attr* %call1) #3
  %call3 = call %struct.liq_attr* @_ZNKSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.4"* %attr) #3
  %4 = load %struct.liq_color*, %struct.liq_color** %image_buffer, align 4
  %5 = bitcast %struct.liq_color* %4 to i8*
  %6 = load i32, i32* %image_width.addr, align 4
  %7 = load i32, i32* %image_height.addr, align 4
  %call4 = call %struct.liq_image* @liq_image_create_rgba(%struct.liq_attr* %call3, i8* %5, i32 %6, i32 %7, double 0.000000e+00)
  %call5 = call %"class.std::__2::unique_ptr.9"* @_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEEC2ILb1EvEES3_(%"class.std::__2::unique_ptr.9"* %image, %struct.liq_image* %call4) #3
  %call6 = call %struct.liq_attr* @_ZNKSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.4"* %attr) #3
  %8 = load i32, i32* %num_colors.addr, align 4
  %call7 = call i32 @liq_set_max_colors(%struct.liq_attr* %call6, i32 %8)
  %call8 = call %struct.liq_image* @_ZNKSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.9"* %image) #3
  %call9 = call %struct.liq_attr* @_ZNKSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.4"* %attr) #3
  call void @_Z18liq_image_quantizeP9liq_imageP8liq_attr(%"class.std::__2::unique_ptr"* sret align 4 %res, %struct.liq_image* %call8, %struct.liq_attr* %call9)
  %call10 = call %struct.liq_result* @_ZNKSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE3getEv(%"class.std::__2::unique_ptr"* %res) #3
  %9 = load float, float* %dithering.addr, align 4
  %call11 = call i32 @liq_set_dithering_level(%struct.liq_result* %call10, float %9)
  %10 = load i32, i32* %size, align 4
  %call12 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEEC2Em(%"class.std::__2::vector"* %image8bit, i32 %10)
  %11 = load i32, i32* %size, align 4
  %call13 = call %"class.std::__2::vector.19"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEEC2Em(%"class.std::__2::vector.19"* %result, i32 %11)
  %call14 = call %struct.liq_result* @_ZNKSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE3getEv(%"class.std::__2::unique_ptr"* %res) #3
  %call15 = call %struct.liq_image* @_ZNKSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.9"* %image) #3
  %call16 = call i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %image8bit) #3
  %call17 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %image8bit) #3
  %call18 = call i32 @liq_write_remapped_image(%struct.liq_result* %call14, %struct.liq_image* %call15, i8* %call16, i32 %call17)
  %call19 = call %struct.liq_result* @_ZNKSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE3getEv(%"class.std::__2::unique_ptr"* %res) #3
  %call20 = call %struct.liq_palette* @liq_get_palette(%struct.liq_result* %call19)
  store %struct.liq_palette* %call20, %struct.liq_palette** %pal, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %size, align 4
  %cmp = icmp slt i32 %12, %13
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %struct.liq_palette*, %struct.liq_palette** %pal, align 4
  %entries = getelementptr inbounds %struct.liq_palette, %struct.liq_palette* %14, i32 0, i32 1
  %15 = load i32, i32* %i, align 4
  %call21 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector"* %image8bit, i32 %15) #3
  %16 = load i8, i8* %call21, align 1
  %idxprom = zext i8 %16 to i32
  %arrayidx = getelementptr inbounds [256 x %struct.liq_color], [256 x %struct.liq_color]* %entries, i32 0, i32 %idxprom
  %17 = load i32, i32* %i, align 4
  %call22 = call nonnull align 1 dereferenceable(4) %struct.liq_color* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEEixEm(%"class.std::__2::vector.19"* %result, i32 %17) #3
  %18 = bitcast %struct.liq_color* %call22 to i8*
  %19 = bitcast %struct.liq_color* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %18, i8* align 4 %19, i32 4, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = call %"class.emscripten::val"* @_ZTWL17Uint8ClampedArray()
  %call23 = call i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4sizeEv(%"class.std::__2::vector.19"* %result) #3
  %mul24 = mul i32 %call23, 4
  %call25 = call %struct.liq_color* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %result) #3
  %22 = bitcast %struct.liq_color* %call25 to i8*
  call void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp, i32 %mul24, i8* %22)
  call void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* sret align 4 %agg.result, %"class.emscripten::val"* %21, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  %call26 = call %"class.std::__2::vector.19"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEED2Ev(%"class.std::__2::vector.19"* %result) #3
  %call27 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector"* %image8bit) #3
  %call28 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEED2Ev(%"class.std::__2::unique_ptr"* %res) #3
  %call29 = call %"class.std::__2::unique_ptr.9"* @_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEED2Ev(%"class.std::__2::unique_ptr.9"* %image) #3
  %call30 = call %"class.std::__2::unique_ptr.4"* @_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEED2Ev(%"class.std::__2::unique_ptr.4"* %attr) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #3
  ret i8* %call
}

declare %struct.liq_attr* @liq_attr_create() #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.4"* @_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEEC2ILb1EvEES3_(%"class.std::__2::unique_ptr.4"* returned %this, %struct.liq_attr* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.4"*, align 4
  %__p.addr = alloca %struct.liq_attr*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.4"* %this, %"class.std::__2::unique_ptr.4"** %this.addr, align 4
  store %struct.liq_attr* %__p, %struct.liq_attr** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.4"*, %"class.std::__2::unique_ptr.4"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.4", %"class.std::__2::unique_ptr.4"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.5"* @_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.5"* %__ptr_, %struct.liq_attr** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.4"* %this1
}

declare %struct.liq_image* @liq_image_create_rgba(%struct.liq_attr*, i8*, i32, i32, double) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.liq_attr* @_ZNKSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.4"*, align 4
  store %"class.std::__2::unique_ptr.4"* %this, %"class.std::__2::unique_ptr.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.4"*, %"class.std::__2::unique_ptr.4"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.4", %"class.std::__2::unique_ptr.4"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNKSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.5"* %__ptr_) #3
  %0 = load %struct.liq_attr*, %struct.liq_attr** %call, align 4
  ret %struct.liq_attr* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.9"* @_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEEC2ILb1EvEES3_(%"class.std::__2::unique_ptr.9"* returned %this, %struct.liq_image* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.9"*, align 4
  %__p.addr = alloca %struct.liq_image*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.9"* %this, %"class.std::__2::unique_ptr.9"** %this.addr, align 4
  store %struct.liq_image* %__p, %struct.liq_image** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.9"*, %"class.std::__2::unique_ptr.9"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.9", %"class.std::__2::unique_ptr.9"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.10"* @_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.10"* %__ptr_, %struct.liq_image** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.9"* %this1
}

declare i32 @liq_set_max_colors(%struct.liq_attr*, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.liq_image* @_ZNKSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.9"*, align 4
  store %"class.std::__2::unique_ptr.9"* %this, %"class.std::__2::unique_ptr.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.9"*, %"class.std::__2::unique_ptr.9"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.9", %"class.std::__2::unique_ptr.9"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNKSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.10"* %__ptr_) #3
  %0 = load %struct.liq_image*, %struct.liq_image** %call, align 4
  ret %struct.liq_image* %0
}

declare i32 @liq_set_dithering_level(%struct.liq_result*, float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.liq_result* @_ZNKSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE3getEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNKSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #3
  %0 = load %struct.liq_result*, %struct.liq_result** %call, align 4
  ret %struct.liq_result* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEEC2Em(%"class.std::__2::vector"* returned %this, i32 %__n) unnamed_addr #2 comdat {
entry:
  %retval = alloca %"class.std::__2::vector"*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %this1, %"class.std::__2::vector"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2Ev(%"class.std::__2::__vector_base"* %0) #3
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm(%"class.std::__2::vector"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endEm(%"class.std::__2::vector"* %this1, i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %retval, align 4
  ret %"class.std::__2::vector"* %4
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::vector.19"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEEC2Em(%"class.std::__2::vector.19"* returned %this, i32 %__n) unnamed_addr #2 comdat {
entry:
  %retval = alloca %"class.std::__2::vector.19"*, align 4
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  store %"class.std::__2::vector.19"* %this1, %"class.std::__2::vector.19"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %call = call %"class.std::__2::__vector_base.20"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEEC2Ev(%"class.std::__2::__vector_base.20"* %0) #3
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE11__vallocateEm(%"class.std::__2::vector.19"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE18__construct_at_endEm(%"class.std::__2::vector.19"* %this1, i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %retval, align 4
  ret %"class.std::__2::vector.19"* %4
}

declare i32 @liq_write_remapped_image(%struct.liq_result*, %struct.liq_image*, i8*, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

declare %struct.liq_palette* @liq_get_palette(%struct.liq_result*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(4) %struct.liq_color* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEEixEm(%"class.std::__2::vector.19"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %0, i32 0, i32 0
  %1 = load %struct.liq_color*, %struct.liq_color** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.liq_color, %struct.liq_color* %1, i32 %2
  ret %struct.liq_color* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline
define internal %"class.emscripten::val"* @_ZTWL17Uint8ClampedArray() #6 {
  call void @_ZTHL17Uint8ClampedArray()
  ret %"class.emscripten::val"* @_ZL17Uint8ClampedArray
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  call void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_(%"class.emscripten::val"* sret align 4 %agg.result, %"class.emscripten::val"* %this1, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* @_emval_new, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* noalias sret align 4 %agg.result, i32 %size, i8* %data) #2 comdat {
entry:
  %size.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %1 = load i8*, i8** %data.addr, align 4
  %call = call %"struct.emscripten::memory_view"* @_ZN10emscripten11memory_viewIhEC2EmPKh(%"struct.emscripten::memory_view"* %agg.result, i32 %0, i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4sizeEv(%"class.std::__2::vector.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %0, i32 0, i32 1
  %1 = load %struct.liq_color*, %struct.liq_color** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %2, i32 0, i32 0
  %3 = load %struct.liq_color*, %struct.liq_color** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %struct.liq_color* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.liq_color* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.liq_color* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %0, i32 0, i32 0
  %1 = load %struct.liq_color*, %struct.liq_color** %__begin_, align 4
  %call = call %struct.liq_color* @_ZNSt3__212__to_addressI9liq_colorEEPT_S3_(%struct.liq_color* %1) #3
  ret %struct.liq_color* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.19"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEED2Ev(%"class.std::__2::vector.19"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  call void @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE17__annotate_deleteEv(%"class.std::__2::vector.19"* %this1) #3
  %0 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %call = call %"class.std::__2::__vector_base.20"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEED2Ev(%"class.std::__2::__vector_base.20"* %0) #3
  ret %"class.std::__2::vector.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #3
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base"* %0) #3
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE5resetES3_(%"class.std::__2::unique_ptr"* %this1, %struct.liq_result* null) #3
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.9"* @_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEED2Ev(%"class.std::__2::unique_ptr.9"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.9"*, align 4
  store %"class.std::__2::unique_ptr.9"* %this, %"class.std::__2::unique_ptr.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.9"*, %"class.std::__2::unique_ptr.9"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE5resetES3_(%"class.std::__2::unique_ptr.9"* %this1, %struct.liq_image* null) #3
  ret %"class.std::__2::unique_ptr.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.4"* @_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEED2Ev(%"class.std::__2::unique_ptr.4"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.4"*, align 4
  store %"class.std::__2::unique_ptr.4"* %this, %"class.std::__2::unique_ptr.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.4"*, %"class.std::__2::unique_ptr.4"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE5resetES3_(%"class.std::__2::unique_ptr.4"* %this1, %struct.liq_attr* null) #3
  ret %"class.std::__2::unique_ptr.4"* %this1
}

; Function Attrs: noinline optnone
define hidden void @_Z11zx_quantizeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEiif(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.std::__2::basic_string"* %rawimage, i32 %image_width, i32 %image_height, float %dithering) #2 {
entry:
  %result.ptr = alloca i8*, align 4
  %image_width.addr = alloca i32, align 4
  %image_height.addr = alloca i32, align 4
  %dithering.addr = alloca float, align 4
  %image_buffer = alloca %struct.liq_color*, align 4
  %size = alloca i32, align 4
  %block = alloca [64 x %struct.liq_color], align 16
  %image8bit = alloca [64 x i8], align 16
  %result = alloca %"class.std::__2::vector.19", align 4
  %block_start_y = alloca i32, align 4
  %block_start_x = alloca i32, align 4
  %color_popularity = alloca [15 x i32], align 16
  %block_index = alloca i32, align 4
  %block_width = alloca i32, align 4
  %block_height = alloca i32, align 4
  %y = alloca i32, align 4
  %x = alloca i32, align 4
  %pixel_start = alloca i32, align 4
  %smallest_distance = alloca i32, align 4
  %winning_index = alloca i32, align 4
  %color_index = alloca i32, align 4
  %color = alloca %struct.liq_color, align 1
  %pixel = alloca %struct.liq_color, align 1
  %distance = alloca i32, align 4
  %first_color_index = alloca i32, align 4
  %second_color_index = alloca i32, align 4
  %third_color_index = alloca i32, align 4
  %highest_popularity = alloca i32, align 4
  %second_highest_popularity = alloca i32, align 4
  %third_highest_popularity = alloca i32, align 4
  %color_index56 = alloca i32, align 4
  %attr = alloca %"class.std::__2::unique_ptr.4", align 4
  %image = alloca %"class.std::__2::unique_ptr.9", align 4
  %agg.tmp = alloca %struct.liq_color, align 1
  %agg.tmp111 = alloca %struct.liq_color, align 1
  %res = alloca %"class.std::__2::unique_ptr", align 4
  %pal = alloca %struct.liq_palette*, align 4
  %y124 = alloca i32, align 4
  %x128 = alloca i32, align 4
  %image8BitPos = alloca i32, align 4
  %resultStartPos = alloca i32, align 4
  %ref.tmp = alloca %"struct.emscripten::memory_view", align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i32 %image_width, i32* %image_width.addr, align 4
  store i32 %image_height, i32* %image_height.addr, align 4
  store float %dithering, float* %dithering.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %rawimage) #3
  %1 = bitcast i8* %call to %struct.liq_color*
  store %struct.liq_color* %1, %struct.liq_color** %image_buffer, align 4
  %2 = load i32, i32* %image_width.addr, align 4
  %3 = load i32, i32* %image_height.addr, align 4
  %mul = mul nsw i32 %2, %3
  store i32 %mul, i32* %size, align 4
  %4 = load i32, i32* %size, align 4
  %call1 = call %"class.std::__2::vector.19"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEEC2Em(%"class.std::__2::vector.19"* %result, i32 %4)
  store i32 0, i32* %block_start_y, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc153, %entry
  %5 = load i32, i32* %block_start_y, align 4
  %6 = load i32, i32* %image_height.addr, align 4
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end155

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %block_start_x, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc150, %for.body
  %7 = load i32, i32* %block_start_x, align 4
  %8 = load i32, i32* %image_width.addr, align 4
  %cmp3 = icmp slt i32 %7, %8
  br i1 %cmp3, label %for.body4, label %for.end152

for.body4:                                        ; preds = %for.cond2
  %9 = bitcast [15 x i32]* %color_popularity to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %9, i8 0, i32 60, i1 false)
  store i32 0, i32* %block_index, align 4
  store i32 8, i32* %block_width, align 4
  store i32 8, i32* %block_height, align 4
  %10 = load i32, i32* %block_start_y, align 4
  %11 = load i32, i32* %block_height, align 4
  %add = add nsw i32 %10, %11
  %12 = load i32, i32* %image_height.addr, align 4
  %cmp5 = icmp sgt i32 %add, %12
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  %13 = load i32, i32* %image_height.addr, align 4
  %14 = load i32, i32* %block_start_y, align 4
  %sub = sub nsw i32 %13, %14
  store i32 %sub, i32* %block_height, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body4
  %15 = load i32, i32* %block_start_x, align 4
  %16 = load i32, i32* %block_width, align 4
  %add6 = add nsw i32 %15, %16
  %17 = load i32, i32* %image_width.addr, align 4
  %cmp7 = icmp sgt i32 %add6, %17
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end
  %18 = load i32, i32* %image_width.addr, align 4
  %19 = load i32, i32* %block_start_x, align 4
  %sub9 = sub nsw i32 %18, %19
  store i32 %sub9, i32* %block_width, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.end
  %20 = load i32, i32* %block_start_y, align 4
  store i32 %20, i32* %y, align 4
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc53, %if.end10
  %21 = load i32, i32* %y, align 4
  %22 = load i32, i32* %block_start_y, align 4
  %23 = load i32, i32* %block_height, align 4
  %add12 = add nsw i32 %22, %23
  %cmp13 = icmp slt i32 %21, %add12
  br i1 %cmp13, label %for.body14, label %for.end55

for.body14:                                       ; preds = %for.cond11
  %24 = load i32, i32* %block_start_x, align 4
  store i32 %24, i32* %x, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc50, %for.body14
  %25 = load i32, i32* %x, align 4
  %26 = load i32, i32* %block_start_x, align 4
  %27 = load i32, i32* %block_width, align 4
  %add16 = add nsw i32 %26, %27
  %cmp17 = icmp slt i32 %25, %add16
  br i1 %cmp17, label %for.body18, label %for.end52

for.body18:                                       ; preds = %for.cond15
  %28 = load i32, i32* %y, align 4
  %29 = load i32, i32* %image_width.addr, align 4
  %mul19 = mul nsw i32 %28, %29
  %30 = load i32, i32* %x, align 4
  %add20 = add nsw i32 %mul19, %30
  store i32 %add20, i32* %pixel_start, align 4
  store i32 2147483647, i32* %smallest_distance, align 4
  store i32 -1, i32* %winning_index, align 4
  %31 = load %struct.liq_color*, %struct.liq_color** %image_buffer, align 4
  %32 = load i32, i32* %pixel_start, align 4
  %arrayidx = getelementptr inbounds %struct.liq_color, %struct.liq_color* %31, i32 %32
  %33 = load i32, i32* %block_index, align 4
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %block_index, align 4
  %arrayidx21 = getelementptr inbounds [64 x %struct.liq_color], [64 x %struct.liq_color]* %block, i32 0, i32 %33
  %34 = bitcast %struct.liq_color* %arrayidx21 to i8*
  %35 = bitcast %struct.liq_color* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 1 %35, i32 4, i1 false)
  store i32 0, i32* %color_index, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %for.body18
  %36 = load i32, i32* %color_index, align 4
  %cmp23 = icmp slt i32 %36, 15
  br i1 %cmp23, label %for.body24, label %for.end

for.body24:                                       ; preds = %for.cond22
  %37 = load i32, i32* %color_index, align 4
  %arrayidx25 = getelementptr inbounds [15 x %struct.liq_color], [15 x %struct.liq_color]* @_ZL9zx_colors, i32 0, i32 %37
  %38 = bitcast %struct.liq_color* %color to i8*
  %39 = bitcast %struct.liq_color* %arrayidx25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %38, i8* align 4 %39, i32 4, i1 false)
  %40 = load %struct.liq_color*, %struct.liq_color** %image_buffer, align 4
  %41 = load i32, i32* %pixel_start, align 4
  %arrayidx26 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %40, i32 %41
  %42 = bitcast %struct.liq_color* %pixel to i8*
  %43 = bitcast %struct.liq_color* %arrayidx26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %42, i8* align 1 %43, i32 4, i1 false)
  %r = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 0
  %44 = load i8, i8* %r, align 1
  %conv = zext i8 %44 to i32
  %r27 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %pixel, i32 0, i32 0
  %45 = load i8, i8* %r27, align 1
  %conv28 = zext i8 %45 to i32
  %sub29 = sub nsw i32 %conv, %conv28
  %call30 = call double @_Z3powIiiENSt3__29_MetaBaseIXaasr3std13is_arithmeticIT_EE5valuesr3std13is_arithmeticIT0_EE5valueEE13_EnableIfImplINS0_9__promoteIS2_S3_vEEE4typeES2_S3_(i32 %sub29, i32 2) #3
  %g = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 1
  %46 = load i8, i8* %g, align 1
  %conv31 = zext i8 %46 to i32
  %g32 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %pixel, i32 0, i32 1
  %47 = load i8, i8* %g32, align 1
  %conv33 = zext i8 %47 to i32
  %sub34 = sub nsw i32 %conv31, %conv33
  %call35 = call double @_Z3powIiiENSt3__29_MetaBaseIXaasr3std13is_arithmeticIT_EE5valuesr3std13is_arithmeticIT0_EE5valueEE13_EnableIfImplINS0_9__promoteIS2_S3_vEEE4typeES2_S3_(i32 %sub34, i32 2) #3
  %add36 = fadd double %call30, %call35
  %b = getelementptr inbounds %struct.liq_color, %struct.liq_color* %color, i32 0, i32 2
  %48 = load i8, i8* %b, align 1
  %conv37 = zext i8 %48 to i32
  %b38 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %pixel, i32 0, i32 2
  %49 = load i8, i8* %b38, align 1
  %conv39 = zext i8 %49 to i32
  %sub40 = sub nsw i32 %conv37, %conv39
  %call41 = call double @_Z3powIiiENSt3__29_MetaBaseIXaasr3std13is_arithmeticIT_EE5valuesr3std13is_arithmeticIT0_EE5valueEE13_EnableIfImplINS0_9__promoteIS2_S3_vEEE4typeES2_S3_(i32 %sub40, i32 2) #3
  %add42 = fadd double %add36, %call41
  %conv43 = fptosi double %add42 to i32
  store i32 %conv43, i32* %distance, align 4
  %50 = load i32, i32* %distance, align 4
  %51 = load i32, i32* %smallest_distance, align 4
  %cmp44 = icmp slt i32 %50, %51
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %for.body24
  %52 = load i32, i32* %color_index, align 4
  store i32 %52, i32* %winning_index, align 4
  %53 = load i32, i32* %distance, align 4
  store i32 %53, i32* %smallest_distance, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.then45, %for.body24
  br label %for.inc

for.inc:                                          ; preds = %if.end46
  %54 = load i32, i32* %color_index, align 4
  %inc47 = add nsw i32 %54, 1
  store i32 %inc47, i32* %color_index, align 4
  br label %for.cond22

for.end:                                          ; preds = %for.cond22
  %55 = load i32, i32* %winning_index, align 4
  %arrayidx48 = getelementptr inbounds [15 x i32], [15 x i32]* %color_popularity, i32 0, i32 %55
  %56 = load i32, i32* %arrayidx48, align 4
  %inc49 = add nsw i32 %56, 1
  store i32 %inc49, i32* %arrayidx48, align 4
  br label %for.inc50

for.inc50:                                        ; preds = %for.end
  %57 = load i32, i32* %x, align 4
  %inc51 = add nsw i32 %57, 1
  store i32 %inc51, i32* %x, align 4
  br label %for.cond15

for.end52:                                        ; preds = %for.cond15
  br label %for.inc53

for.inc53:                                        ; preds = %for.end52
  %58 = load i32, i32* %y, align 4
  %inc54 = add nsw i32 %58, 1
  store i32 %inc54, i32* %y, align 4
  br label %for.cond11

for.end55:                                        ; preds = %for.cond11
  store i32 0, i32* %first_color_index, align 4
  store i32 0, i32* %second_color_index, align 4
  store i32 0, i32* %third_color_index, align 4
  store i32 -1, i32* %highest_popularity, align 4
  store i32 -1, i32* %second_highest_popularity, align 4
  store i32 -1, i32* %third_highest_popularity, align 4
  store i32 0, i32* %color_index56, align 4
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc76, %for.end55
  %59 = load i32, i32* %color_index56, align 4
  %cmp58 = icmp slt i32 %59, 15
  br i1 %cmp58, label %for.body59, label %for.end78

for.body59:                                       ; preds = %for.cond57
  %60 = load i32, i32* %color_index56, align 4
  %arrayidx60 = getelementptr inbounds [15 x i32], [15 x i32]* %color_popularity, i32 0, i32 %60
  %61 = load i32, i32* %arrayidx60, align 4
  %62 = load i32, i32* %highest_popularity, align 4
  %cmp61 = icmp sgt i32 %61, %62
  br i1 %cmp61, label %if.then62, label %if.else

if.then62:                                        ; preds = %for.body59
  %63 = load i32, i32* %second_color_index, align 4
  store i32 %63, i32* %third_color_index, align 4
  %64 = load i32, i32* %second_highest_popularity, align 4
  store i32 %64, i32* %third_highest_popularity, align 4
  %65 = load i32, i32* %first_color_index, align 4
  store i32 %65, i32* %second_color_index, align 4
  %66 = load i32, i32* %highest_popularity, align 4
  store i32 %66, i32* %second_highest_popularity, align 4
  %67 = load i32, i32* %color_index56, align 4
  store i32 %67, i32* %first_color_index, align 4
  %68 = load i32, i32* %color_index56, align 4
  %arrayidx63 = getelementptr inbounds [15 x i32], [15 x i32]* %color_popularity, i32 0, i32 %68
  %69 = load i32, i32* %arrayidx63, align 4
  store i32 %69, i32* %highest_popularity, align 4
  br label %if.end75

if.else:                                          ; preds = %for.body59
  %70 = load i32, i32* %color_index56, align 4
  %arrayidx64 = getelementptr inbounds [15 x i32], [15 x i32]* %color_popularity, i32 0, i32 %70
  %71 = load i32, i32* %arrayidx64, align 4
  %72 = load i32, i32* %second_highest_popularity, align 4
  %cmp65 = icmp sgt i32 %71, %72
  br i1 %cmp65, label %if.then66, label %if.else68

if.then66:                                        ; preds = %if.else
  %73 = load i32, i32* %second_color_index, align 4
  store i32 %73, i32* %third_color_index, align 4
  %74 = load i32, i32* %second_highest_popularity, align 4
  store i32 %74, i32* %third_highest_popularity, align 4
  %75 = load i32, i32* %color_index56, align 4
  store i32 %75, i32* %second_color_index, align 4
  %76 = load i32, i32* %color_index56, align 4
  %arrayidx67 = getelementptr inbounds [15 x i32], [15 x i32]* %color_popularity, i32 0, i32 %76
  %77 = load i32, i32* %arrayidx67, align 4
  store i32 %77, i32* %second_highest_popularity, align 4
  br label %if.end74

if.else68:                                        ; preds = %if.else
  %78 = load i32, i32* %color_index56, align 4
  %arrayidx69 = getelementptr inbounds [15 x i32], [15 x i32]* %color_popularity, i32 0, i32 %78
  %79 = load i32, i32* %arrayidx69, align 4
  %80 = load i32, i32* %third_highest_popularity, align 4
  %cmp70 = icmp sgt i32 %79, %80
  br i1 %cmp70, label %if.then71, label %if.end73

if.then71:                                        ; preds = %if.else68
  %81 = load i32, i32* %color_index56, align 4
  store i32 %81, i32* %third_color_index, align 4
  %82 = load i32, i32* %color_index56, align 4
  %arrayidx72 = getelementptr inbounds [15 x i32], [15 x i32]* %color_popularity, i32 0, i32 %82
  %83 = load i32, i32* %arrayidx72, align 4
  store i32 %83, i32* %third_highest_popularity, align 4
  br label %if.end73

if.end73:                                         ; preds = %if.then71, %if.else68
  br label %if.end74

if.end74:                                         ; preds = %if.end73, %if.then66
  br label %if.end75

if.end75:                                         ; preds = %if.end74, %if.then62
  br label %for.inc76

for.inc76:                                        ; preds = %if.end75
  %84 = load i32, i32* %color_index56, align 4
  %inc77 = add nsw i32 %84, 1
  store i32 %inc77, i32* %color_index56, align 4
  br label %for.cond57

for.end78:                                        ; preds = %for.cond57
  br label %while.cond

while.cond:                                       ; preds = %if.end99, %for.end78
  br label %while.body

while.body:                                       ; preds = %while.cond
  %85 = load i32, i32* %first_color_index, align 4
  %cmp79 = icmp ne i32 %85, 0
  br i1 %cmp79, label %land.lhs.true, label %if.end95

land.lhs.true:                                    ; preds = %while.body
  %86 = load i32, i32* %second_color_index, align 4
  %cmp80 = icmp ne i32 %86, 0
  br i1 %cmp80, label %if.then81, label %if.end95

if.then81:                                        ; preds = %land.lhs.true
  %87 = load i32, i32* %first_color_index, align 4
  %cmp82 = icmp sge i32 %87, 8
  br i1 %cmp82, label %land.lhs.true83, label %if.else87

land.lhs.true83:                                  ; preds = %if.then81
  %88 = load i32, i32* %second_color_index, align 4
  %cmp84 = icmp slt i32 %88, 8
  br i1 %cmp84, label %if.then85, label %if.else87

if.then85:                                        ; preds = %land.lhs.true83
  %89 = load i32, i32* %second_color_index, align 4
  %add86 = add nsw i32 %89, 7
  store i32 %add86, i32* %second_color_index, align 4
  br label %if.end94

if.else87:                                        ; preds = %land.lhs.true83, %if.then81
  %90 = load i32, i32* %first_color_index, align 4
  %cmp88 = icmp slt i32 %90, 8
  br i1 %cmp88, label %land.lhs.true89, label %if.end93

land.lhs.true89:                                  ; preds = %if.else87
  %91 = load i32, i32* %second_color_index, align 4
  %cmp90 = icmp sge i32 %91, 8
  br i1 %cmp90, label %if.then91, label %if.end93

if.then91:                                        ; preds = %land.lhs.true89
  %92 = load i32, i32* %second_color_index, align 4
  %sub92 = sub nsw i32 %92, 7
  store i32 %sub92, i32* %second_color_index, align 4
  br label %if.end93

if.end93:                                         ; preds = %if.then91, %land.lhs.true89, %if.else87
  br label %if.end94

if.end94:                                         ; preds = %if.end93, %if.then85
  br label %if.end95

if.end95:                                         ; preds = %if.end94, %land.lhs.true, %while.body
  %93 = load i32, i32* %first_color_index, align 4
  %94 = load i32, i32* %second_color_index, align 4
  %cmp96 = icmp eq i32 %93, %94
  br i1 %cmp96, label %if.then97, label %if.else98

if.then97:                                        ; preds = %if.end95
  %95 = load i32, i32* %third_color_index, align 4
  store i32 %95, i32* %second_color_index, align 4
  br label %if.end99

if.else98:                                        ; preds = %if.end95
  br label %while.end

if.end99:                                         ; preds = %if.then97
  br label %while.cond

while.end:                                        ; preds = %if.else98
  %call100 = call %struct.liq_attr* @liq_attr_create()
  %call101 = call %"class.std::__2::unique_ptr.4"* @_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEEC2ILb1EvEES3_(%"class.std::__2::unique_ptr.4"* %attr, %struct.liq_attr* %call100) #3
  %call102 = call %struct.liq_attr* @_ZNKSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.4"* %attr) #3
  %arraydecay = getelementptr inbounds [64 x %struct.liq_color], [64 x %struct.liq_color]* %block, i32 0, i32 0
  %96 = bitcast %struct.liq_color* %arraydecay to i8*
  %97 = load i32, i32* %block_width, align 4
  %98 = load i32, i32* %block_height, align 4
  %call103 = call %struct.liq_image* @liq_image_create_rgba(%struct.liq_attr* %call102, i8* %96, i32 %97, i32 %98, double 0.000000e+00)
  %call104 = call %"class.std::__2::unique_ptr.9"* @_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEEC2ILb1EvEES3_(%"class.std::__2::unique_ptr.9"* %image, %struct.liq_image* %call103) #3
  %call105 = call %struct.liq_attr* @_ZNKSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.4"* %attr) #3
  %call106 = call i32 @liq_set_max_colors(%struct.liq_attr* %call105, i32 2)
  %call107 = call %struct.liq_image* @_ZNKSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.9"* %image) #3
  %99 = load i32, i32* %first_color_index, align 4
  %arrayidx108 = getelementptr inbounds [15 x %struct.liq_color], [15 x %struct.liq_color]* @_ZL9zx_colors, i32 0, i32 %99
  %100 = bitcast %struct.liq_color* %agg.tmp to i8*
  %101 = bitcast %struct.liq_color* %arrayidx108 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %100, i8* align 4 %101, i32 4, i1 false)
  %call109 = call i32 @liq_image_add_fixed_color(%struct.liq_image* %call107, %struct.liq_color* byval(%struct.liq_color) align 1 %agg.tmp)
  %call110 = call %struct.liq_image* @_ZNKSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.9"* %image) #3
  %102 = load i32, i32* %second_color_index, align 4
  %arrayidx112 = getelementptr inbounds [15 x %struct.liq_color], [15 x %struct.liq_color]* @_ZL9zx_colors, i32 0, i32 %102
  %103 = bitcast %struct.liq_color* %agg.tmp111 to i8*
  %104 = bitcast %struct.liq_color* %arrayidx112 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %103, i8* align 4 %104, i32 4, i1 false)
  %call113 = call i32 @liq_image_add_fixed_color(%struct.liq_image* %call110, %struct.liq_color* byval(%struct.liq_color) align 1 %agg.tmp111)
  %call114 = call %struct.liq_image* @_ZNKSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.9"* %image) #3
  %call115 = call %struct.liq_attr* @_ZNKSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.4"* %attr) #3
  call void @_Z18liq_image_quantizeP9liq_imageP8liq_attr(%"class.std::__2::unique_ptr"* sret align 4 %res, %struct.liq_image* %call114, %struct.liq_attr* %call115)
  %call116 = call %struct.liq_result* @_ZNKSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE3getEv(%"class.std::__2::unique_ptr"* %res) #3
  %105 = load float, float* %dithering.addr, align 4
  %call117 = call i32 @liq_set_dithering_level(%struct.liq_result* %call116, float %105)
  %call118 = call %struct.liq_result* @_ZNKSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE3getEv(%"class.std::__2::unique_ptr"* %res) #3
  %call119 = call %struct.liq_image* @_ZNKSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE3getEv(%"class.std::__2::unique_ptr.9"* %image) #3
  %arraydecay120 = getelementptr inbounds [64 x i8], [64 x i8]* %image8bit, i32 0, i32 0
  %106 = load i32, i32* %size, align 4
  %call121 = call i32 @liq_write_remapped_image(%struct.liq_result* %call118, %struct.liq_image* %call119, i8* %arraydecay120, i32 %106)
  %call122 = call %struct.liq_result* @_ZNKSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE3getEv(%"class.std::__2::unique_ptr"* %res) #3
  %call123 = call %struct.liq_palette* @liq_get_palette(%struct.liq_result* %call122)
  store %struct.liq_palette* %call123, %struct.liq_palette** %pal, align 4
  store i32 0, i32* %y124, align 4
  br label %for.cond125

for.cond125:                                      ; preds = %for.inc144, %while.end
  %107 = load i32, i32* %y124, align 4
  %108 = load i32, i32* %block_height, align 4
  %cmp126 = icmp slt i32 %107, %108
  br i1 %cmp126, label %for.body127, label %for.end146

for.body127:                                      ; preds = %for.cond125
  store i32 0, i32* %x128, align 4
  br label %for.cond129

for.cond129:                                      ; preds = %for.inc141, %for.body127
  %109 = load i32, i32* %x128, align 4
  %110 = load i32, i32* %block_width, align 4
  %cmp130 = icmp slt i32 %109, %110
  br i1 %cmp130, label %for.body131, label %for.end143

for.body131:                                      ; preds = %for.cond129
  %111 = load i32, i32* %y124, align 4
  %112 = load i32, i32* %block_width, align 4
  %mul132 = mul nsw i32 %111, %112
  %113 = load i32, i32* %x128, align 4
  %add133 = add nsw i32 %mul132, %113
  store i32 %add133, i32* %image8BitPos, align 4
  %114 = load i32, i32* %block_start_y, align 4
  %115 = load i32, i32* %y124, align 4
  %add134 = add nsw i32 %114, %115
  %116 = load i32, i32* %image_width.addr, align 4
  %mul135 = mul nsw i32 %add134, %116
  %117 = load i32, i32* %block_start_x, align 4
  %118 = load i32, i32* %x128, align 4
  %add136 = add nsw i32 %117, %118
  %add137 = add nsw i32 %mul135, %add136
  store i32 %add137, i32* %resultStartPos, align 4
  %119 = load %struct.liq_palette*, %struct.liq_palette** %pal, align 4
  %entries = getelementptr inbounds %struct.liq_palette, %struct.liq_palette* %119, i32 0, i32 1
  %120 = load i32, i32* %image8BitPos, align 4
  %arrayidx138 = getelementptr inbounds [64 x i8], [64 x i8]* %image8bit, i32 0, i32 %120
  %121 = load i8, i8* %arrayidx138, align 1
  %idxprom = zext i8 %121 to i32
  %arrayidx139 = getelementptr inbounds [256 x %struct.liq_color], [256 x %struct.liq_color]* %entries, i32 0, i32 %idxprom
  %122 = load i32, i32* %resultStartPos, align 4
  %call140 = call nonnull align 1 dereferenceable(4) %struct.liq_color* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEEixEm(%"class.std::__2::vector.19"* %result, i32 %122) #3
  %123 = bitcast %struct.liq_color* %call140 to i8*
  %124 = bitcast %struct.liq_color* %arrayidx139 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %123, i8* align 4 %124, i32 4, i1 false)
  br label %for.inc141

for.inc141:                                       ; preds = %for.body131
  %125 = load i32, i32* %x128, align 4
  %inc142 = add nsw i32 %125, 1
  store i32 %inc142, i32* %x128, align 4
  br label %for.cond129

for.end143:                                       ; preds = %for.cond129
  br label %for.inc144

for.inc144:                                       ; preds = %for.end143
  %126 = load i32, i32* %y124, align 4
  %inc145 = add nsw i32 %126, 1
  store i32 %inc145, i32* %y124, align 4
  br label %for.cond125

for.end146:                                       ; preds = %for.cond125
  %call147 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEED2Ev(%"class.std::__2::unique_ptr"* %res) #3
  %call148 = call %"class.std::__2::unique_ptr.9"* @_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEED2Ev(%"class.std::__2::unique_ptr.9"* %image) #3
  %call149 = call %"class.std::__2::unique_ptr.4"* @_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEED2Ev(%"class.std::__2::unique_ptr.4"* %attr) #3
  br label %for.inc150

for.inc150:                                       ; preds = %for.end146
  %127 = load i32, i32* %block_start_x, align 4
  %add151 = add nsw i32 %127, 8
  store i32 %add151, i32* %block_start_x, align 4
  br label %for.cond2

for.end152:                                       ; preds = %for.cond2
  br label %for.inc153

for.inc153:                                       ; preds = %for.end152
  %128 = load i32, i32* %block_start_y, align 4
  %add154 = add nsw i32 %128, 8
  store i32 %add154, i32* %block_start_y, align 4
  br label %for.cond

for.end155:                                       ; preds = %for.cond
  %129 = call %"class.emscripten::val"* @_ZTWL17Uint8ClampedArray()
  %call156 = call i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4sizeEv(%"class.std::__2::vector.19"* %result) #3
  %mul157 = mul i32 %call156, 4
  %call158 = call %struct.liq_color* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %result) #3
  %130 = bitcast %struct.liq_color* %call158 to i8*
  call void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp, i32 %mul157, i8* %130)
  call void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* sret align 4 %agg.result, %"class.emscripten::val"* %129, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  %call159 = call %"class.std::__2::vector.19"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEED2Ev(%"class.std::__2::vector.19"* %result) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden double @_Z3powIiiENSt3__29_MetaBaseIXaasr3std13is_arithmeticIT_EE5valuesr3std13is_arithmeticIT0_EE5valueEE13_EnableIfImplINS0_9__promoteIS2_S3_vEEE4typeES2_S3_(i32 %__lcpp_x, i32 %__lcpp_y) #0 comdat {
entry:
  %__lcpp_x.addr = alloca i32, align 4
  %__lcpp_y.addr = alloca i32, align 4
  store i32 %__lcpp_x, i32* %__lcpp_x.addr, align 4
  store i32 %__lcpp_y, i32* %__lcpp_y.addr, align 4
  %0 = load i32, i32* %__lcpp_x.addr, align 4
  %conv = sitofp i32 %0 to double
  %1 = load i32, i32* %__lcpp_y.addr, align 4
  %conv1 = sitofp i32 %1 to double
  %2 = call double @llvm.pow.f64(double %conv, double %conv1)
  ret double %2
}

declare i32 @liq_image_add_fixed_color(%struct.liq_image*, %struct.liq_color* byval(%struct.liq_color) align 1) #4

; Function Attrs: noinline
define internal void @__cxx_global_var_init.1() #1 {
entry:
  %call = call %struct.EmscriptenBindingInitializer_my_module* @_ZN38EmscriptenBindingInitializer_my_moduleC1Ev(%struct.EmscriptenBindingInitializer_my_module* @_ZL47EmscriptenBindingInitializer_my_module_instance)
  ret void
}

; Function Attrs: noinline optnone
define hidden %struct.EmscriptenBindingInitializer_my_module* @_ZN38EmscriptenBindingInitializer_my_moduleC2Ev(%struct.EmscriptenBindingInitializer_my_module* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.EmscriptenBindingInitializer_my_module*, align 4
  store %struct.EmscriptenBindingInitializer_my_module* %this, %struct.EmscriptenBindingInitializer_my_module** %this.addr, align 4
  %this1 = load %struct.EmscriptenBindingInitializer_my_module*, %struct.EmscriptenBindingInitializer_my_module** %this.addr, align 4
  call void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEiiifEJEEEvPKcPFT_DpT0_EDpT1_(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0), void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)* @_Z8quantizeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEiiif)
  call void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEiifEJEEEvPKcPFT_DpT0_EDpT1_(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.3, i32 0, i32 0), void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)* @_Z11zx_quantizeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEiif)
  call void @_ZN10emscripten8functionIiJEJEEEvPKcPFT_DpT0_EDpT1_(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.4, i32 0, i32 0), i32 ()* @_Z7versionv)
  ret %struct.EmscriptenBindingInitializer_my_module* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEiiifEJEEEvPKcPFT_DpT0_EDpT1_(i8* %name, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)* %fn) #2 comdat {
entry:
  %name.addr = alloca i8*, align 4
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, align 4
  %args = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31", align 1
  %invoker = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)*, align 4
  store i8* %name, i8** %name.addr, align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)** %fn.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEiiifEE6invokeEPFS2_S9_iiifEPNS0_11BindingTypeIS9_vEUt_Eiiif, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)** %invoker, align 4
  %0 = load i8*, i8** %name.addr, align 4
  %call = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"* %args)
  %call1 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"* %args)
  %1 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)** %invoker, align 4
  %call2 = call i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEPNS0_11BindingTypeISB_vEUt_EiiifEEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)* %1)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)** %invoker, align 4
  %3 = bitcast %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)* %2 to i8*
  %4 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)** %fn.addr, align 4
  %5 = bitcast void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)* %4 to i8*
  call void @_embind_register_function(i8* %0, i32 %call, i8** %call1, i8* %call2, i8* %3, i8* %5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEiifEJEEEvPKcPFT_DpT0_EDpT1_(i8* %name, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)* %fn) #2 comdat {
entry:
  %name.addr = alloca i8*, align 4
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, align 4
  %args = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33", align 1
  %invoker = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)*, align 4
  store i8* %name, i8** %name.addr, align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)** %fn.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEiifEE6invokeEPFS2_S9_iifEPNS0_11BindingTypeIS9_vEUt_Eiif, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)** %invoker, align 4
  %0 = load i8*, i8** %name.addr, align 4
  %call = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"* %args)
  %call1 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"* %args)
  %1 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)** %invoker, align 4
  %call2 = call i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEPNS0_11BindingTypeISB_vEUt_EiifEEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)* %1)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)** %invoker, align 4
  %3 = bitcast %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)* %2 to i8*
  %4 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)** %fn.addr, align 4
  %5 = bitcast void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)* %4 to i8*
  call void @_embind_register_function(i8* %0, i32 %call, i8** %call1, i8* %call2, i8* %3, i8* %5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8functionIiJEJEEEvPKcPFT_DpT0_EDpT1_(i8* %name, i32 ()* %fn) #2 comdat {
entry:
  %name.addr = alloca i8*, align 4
  %fn.addr = alloca i32 ()*, align 4
  %args = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34", align 1
  %invoker = alloca i32 (i32 ()*)*, align 4
  store i8* %name, i8** %name.addr, align 4
  store i32 ()* %fn, i32 ()** %fn.addr, align 4
  store i32 (i32 ()*)* @_ZN10emscripten8internal7InvokerIiJEE6invokeEPFivE, i32 (i32 ()*)** %invoker, align 4
  %0 = load i8*, i8** %name.addr, align 4
  %call = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"* %args)
  %call1 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"* %args)
  %1 = load i32 (i32 ()*)*, i32 (i32 ()*)** %invoker, align 4
  %call2 = call i8* @_ZN10emscripten8internal12getSignatureIiJPFivEEEEPKcPFT_DpT0_E(i32 (i32 ()*)* %1)
  %2 = load i32 (i32 ()*)*, i32 (i32 ()*)** %invoker, align 4
  %3 = bitcast i32 (i32 ()*)* %2 to i8*
  %4 = load i32 ()*, i32 ()** %fn.addr, align 4
  %5 = bitcast i32 ()* %4 to i8*
  call void @_embind_register_function(i8* %0, i32 %call, i8** %call1, i8* %call2, i8* %3, i8* %5)
  ret void
}

declare %"struct.emscripten::internal::_EM_VAL"* @_emval_get_global(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* returned %this, %"struct.emscripten::internal::_EM_VAL"* %handle) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %handle.addr = alloca %"struct.emscripten::internal::_EM_VAL"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %handle, %"struct.emscripten::internal::_EM_VAL"** %handle.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle2 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %0, %"struct.emscripten::internal::_EM_VAL"** %handle2, align 4
  ret %"class.emscripten::val"* %this1
}

declare void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %struct.liq_result** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %struct.liq_result**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %struct.liq_result** %__t1, %struct.liq_result*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %struct.liq_result**, %struct.liq_result*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNSt3__27forwardIRP10liq_resultEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_result** nonnull align 4 dereferenceable(4) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EEC2IRS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, %struct.liq_result** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNSt3__27forwardIRP10liq_resultEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_result** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %struct.liq_result**, align 4
  store %struct.liq_result** %__t, %struct.liq_result*** %__t.addr, align 4
  %0 = load %struct.liq_result**, %struct.liq_result*** %__t.addr, align 4
  ret %struct.liq_result** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EEC2IRS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, %struct.liq_result** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca %struct.liq_result**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store %struct.liq_result** %__u, %struct.liq_result*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load %struct.liq_result**, %struct.liq_result*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNSt3__27forwardIRP10liq_resultEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_result** nonnull align 4 dereferenceable(4) %0) #3
  %1 = load %struct.liq_result*, %struct.liq_result** %call, align 4
  store %struct.liq_result* %1, %struct.liq_result** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"struct.std::__2::integral_constant"*
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrI10liq_resultNS_17integral_constantIPFvPS1_EXadL_Z18liq_result_destroyEEEEE5resetES3_(%"class.std::__2::unique_ptr"* %this, %struct.liq_result* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %struct.liq_result*, align 4
  %__tmp = alloca %struct.liq_result*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %struct.liq_result* %__p, %struct.liq_result** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #3
  %0 = load %struct.liq_result*, %struct.liq_result** %call, align 4
  store %struct.liq_result* %0, %struct.liq_result** %__tmp, align 4
  %1 = load %struct.liq_result*, %struct.liq_result** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #3
  store %struct.liq_result* %1, %struct.liq_result** %call3, align 4
  %2 = load %struct.liq_result*, %struct.liq_result** %__tmp, align 4
  %tobool = icmp ne %struct.liq_result* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant"* @_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_4) #3
  %call6 = call void (%struct.liq_result*)* @_ZNKSt3__217integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEcvS4_Ev(%"struct.std::__2::integral_constant"* %call5) #3
  %3 = load %struct.liq_result*, %struct.liq_result** %__tmp, align 4
  call void %call6(%struct.liq_result* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #3
  ret %struct.liq_result** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant"* @_ZNSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #3
  ret %"struct.std::__2::integral_constant"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void (%struct.liq_result*)* @_ZNKSt3__217integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEcvS4_Ev(%"struct.std::__2::integral_constant"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::integral_constant"*, align 4
  store %"struct.std::__2::integral_constant"* %this, %"struct.std::__2::integral_constant"** %this.addr, align 4
  %this1 = load %"struct.std::__2::integral_constant"*, %"struct.std::__2::integral_constant"** %this.addr, align 4
  ret void (%struct.liq_result*)* @liq_result_destroy
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %struct.liq_result** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP10liq_resultEXadL_Z18liq_result_destroyEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"struct.std::__2::integral_constant"*
  ret %"struct.std::__2::integral_constant"* %0
}

declare void @liq_result_destroy(%struct.liq_result*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #3
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.1"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.1"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.1"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #3
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #0 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.5"* @_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.5"* returned %this, %struct.liq_attr** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.5"*, align 4
  %__t1.addr = alloca %struct.liq_attr**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.5"* %this, %"class.std::__2::__compressed_pair.5"** %this.addr, align 4
  store %struct.liq_attr** %__t1, %struct.liq_attr*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.5"*, %"class.std::__2::__compressed_pair.5"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.5"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %1 = load %struct.liq_attr**, %struct.liq_attr*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNSt3__27forwardIRP8liq_attrEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_attr** nonnull align 4 dereferenceable(4) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem.6"* @_ZNSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EEC2IRS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.6"* %0, %struct.liq_attr** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.5"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call4 = call %"struct.std::__2::__compressed_pair_elem.7"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.7"* %2)
  ret %"class.std::__2::__compressed_pair.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNSt3__27forwardIRP8liq_attrEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_attr** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %struct.liq_attr**, align 4
  store %struct.liq_attr** %__t, %struct.liq_attr*** %__t.addr, align 4
  %0 = load %struct.liq_attr**, %struct.liq_attr*** %__t.addr, align 4
  ret %struct.liq_attr** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.6"* @_ZNSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EEC2IRS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.6"* returned %this, %struct.liq_attr** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  %__u.addr = alloca %struct.liq_attr**, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  store %struct.liq_attr** %__u, %struct.liq_attr*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.6", %"struct.std::__2::__compressed_pair_elem.6"* %this1, i32 0, i32 0
  %0 = load %struct.liq_attr**, %struct.liq_attr*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNSt3__27forwardIRP8liq_attrEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_attr** nonnull align 4 dereferenceable(4) %0) #3
  %1 = load %struct.liq_attr*, %struct.liq_attr** %call, align 4
  store %struct.liq_attr* %1, %struct.liq_attr** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.6"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.7"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.7"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.7"* %this1 to %"struct.std::__2::integral_constant.8"*
  ret %"struct.std::__2::__compressed_pair_elem.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrI8liq_attrNS_17integral_constantIPFvPS1_EXadL_Z16liq_attr_destroyEEEEE5resetES3_(%"class.std::__2::unique_ptr.4"* %this, %struct.liq_attr* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.4"*, align 4
  %__p.addr = alloca %struct.liq_attr*, align 4
  %__tmp = alloca %struct.liq_attr*, align 4
  store %"class.std::__2::unique_ptr.4"* %this, %"class.std::__2::unique_ptr.4"** %this.addr, align 4
  store %struct.liq_attr* %__p, %struct.liq_attr** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.4"*, %"class.std::__2::unique_ptr.4"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.4", %"class.std::__2::unique_ptr.4"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.5"* %__ptr_) #3
  %0 = load %struct.liq_attr*, %struct.liq_attr** %call, align 4
  store %struct.liq_attr* %0, %struct.liq_attr** %__tmp, align 4
  %1 = load %struct.liq_attr*, %struct.liq_attr** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.4", %"class.std::__2::unique_ptr.4"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.5"* %__ptr_2) #3
  store %struct.liq_attr* %1, %struct.liq_attr** %call3, align 4
  %2 = load %struct.liq_attr*, %struct.liq_attr** %__tmp, align 4
  %tobool = icmp ne %struct.liq_attr* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.4", %"class.std::__2::unique_ptr.4"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant.8"* @_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE6secondEv(%"class.std::__2::__compressed_pair.5"* %__ptr_4) #3
  %call6 = call void (%struct.liq_attr*)* @_ZNKSt3__217integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEcvS4_Ev(%"struct.std::__2::integral_constant.8"* %call5) #3
  %3 = load %struct.liq_attr*, %struct.liq_attr** %__tmp, align 4
  call void %call6(%struct.liq_attr* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.5"*, align 4
  store %"class.std::__2::__compressed_pair.5"* %this, %"class.std::__2::__compressed_pair.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.5"*, %"class.std::__2::__compressed_pair.5"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.5"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %0) #3
  ret %struct.liq_attr** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant.8"* @_ZNSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE6secondEv(%"class.std::__2::__compressed_pair.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.5"*, align 4
  store %"class.std::__2::__compressed_pair.5"* %this, %"class.std::__2::__compressed_pair.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.5"*, %"class.std::__2::__compressed_pair.5"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.5"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant.8"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %0) #3
  ret %"struct.std::__2::integral_constant.8"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void (%struct.liq_attr*)* @_ZNKSt3__217integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEcvS4_Ev(%"struct.std::__2::integral_constant.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::integral_constant.8"*, align 4
  store %"struct.std::__2::integral_constant.8"* %this, %"struct.std::__2::integral_constant.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::integral_constant.8"*, %"struct.std::__2::integral_constant.8"** %this.addr, align 4
  ret void (%struct.liq_attr*)* @liq_attr_destroy
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.6", %"struct.std::__2::__compressed_pair_elem.6"* %this1, i32 0, i32 0
  ret %struct.liq_attr** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant.8"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP8liq_attrEXadL_Z16liq_attr_destroyEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.7"* %this1 to %"struct.std::__2::integral_constant.8"*
  ret %"struct.std::__2::integral_constant.8"* %0
}

declare void @liq_attr_destroy(%struct.liq_attr*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNKSt3__217__compressed_pairIP8liq_attrNS_17integral_constantIPFvS2_EXadL_Z16liq_attr_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.5"*, align 4
  store %"class.std::__2::__compressed_pair.5"* %this, %"class.std::__2::__compressed_pair.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.5"*, %"class.std::__2::__compressed_pair.5"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.5"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNKSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %0) #3
  ret %struct.liq_attr** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_attr** @_ZNKSt3__222__compressed_pair_elemIP8liq_attrLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.6", %"struct.std::__2::__compressed_pair_elem.6"* %this1, i32 0, i32 0
  ret %struct.liq_attr** %__value_
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.10"* @_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEEC2IRS2_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.10"* returned %this, %struct.liq_image** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  %__t1.addr = alloca %struct.liq_image**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  store %struct.liq_image** %__t1, %struct.liq_image*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %1 = load %struct.liq_image**, %struct.liq_image*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNSt3__27forwardIRP9liq_imageEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_image** nonnull align 4 dereferenceable(4) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem.11"* @_ZNSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EEC2IRS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.11"* %0, %struct.liq_image** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.12"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call4 = call %"struct.std::__2::__compressed_pair_elem.12"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.12"* %2)
  ret %"class.std::__2::__compressed_pair.10"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNSt3__27forwardIRP9liq_imageEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_image** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %struct.liq_image**, align 4
  store %struct.liq_image** %__t, %struct.liq_image*** %__t.addr, align 4
  %0 = load %struct.liq_image**, %struct.liq_image*** %__t.addr, align 4
  ret %struct.liq_image** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.11"* @_ZNSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EEC2IRS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.11"* returned %this, %struct.liq_image** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.11"*, align 4
  %__u.addr = alloca %struct.liq_image**, align 4
  store %"struct.std::__2::__compressed_pair_elem.11"* %this, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  store %struct.liq_image** %__u, %struct.liq_image*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.11"*, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.11"* %this1, i32 0, i32 0
  %0 = load %struct.liq_image**, %struct.liq_image*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNSt3__27forwardIRP9liq_imageEEOT_RNS_16remove_referenceIS4_E4typeE(%struct.liq_image** nonnull align 4 dereferenceable(4) %0) #3
  %1 = load %struct.liq_image*, %struct.liq_image** %call, align 4
  store %struct.liq_image* %1, %struct.liq_image** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.12"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.12"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.12"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.12"* %this, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.12"*, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.12"* %this1 to %"struct.std::__2::integral_constant.13"*
  ret %"struct.std::__2::__compressed_pair_elem.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrI9liq_imageNS_17integral_constantIPFvPS1_EXadL_Z17liq_image_destroyEEEEE5resetES3_(%"class.std::__2::unique_ptr.9"* %this, %struct.liq_image* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.9"*, align 4
  %__p.addr = alloca %struct.liq_image*, align 4
  %__tmp = alloca %struct.liq_image*, align 4
  store %"class.std::__2::unique_ptr.9"* %this, %"class.std::__2::unique_ptr.9"** %this.addr, align 4
  store %struct.liq_image* %__p, %struct.liq_image** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.9"*, %"class.std::__2::unique_ptr.9"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.9", %"class.std::__2::unique_ptr.9"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.10"* %__ptr_) #3
  %0 = load %struct.liq_image*, %struct.liq_image** %call, align 4
  store %struct.liq_image* %0, %struct.liq_image** %__tmp, align 4
  %1 = load %struct.liq_image*, %struct.liq_image** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.9", %"class.std::__2::unique_ptr.9"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.10"* %__ptr_2) #3
  store %struct.liq_image* %1, %struct.liq_image** %call3, align 4
  %2 = load %struct.liq_image*, %struct.liq_image** %__tmp, align 4
  %tobool = icmp ne %struct.liq_image* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.9", %"class.std::__2::unique_ptr.9"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant.13"* @_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE6secondEv(%"class.std::__2::__compressed_pair.10"* %__ptr_4) #3
  %call6 = call void (%struct.liq_image*)* @_ZNKSt3__217integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEcvS4_Ev(%"struct.std::__2::integral_constant.13"* %call5) #3
  %3 = load %struct.liq_image*, %struct.liq_image** %__tmp, align 4
  call void %call6(%struct.liq_image* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %0) #3
  ret %struct.liq_image** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant.13"* @_ZNSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE6secondEv(%"class.std::__2::__compressed_pair.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.12"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant.13"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %0) #3
  ret %"struct.std::__2::integral_constant.13"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void (%struct.liq_image*)* @_ZNKSt3__217integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEcvS4_Ev(%"struct.std::__2::integral_constant.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::integral_constant.13"*, align 4
  store %"struct.std::__2::integral_constant.13"* %this, %"struct.std::__2::integral_constant.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::integral_constant.13"*, %"struct.std::__2::integral_constant.13"** %this.addr, align 4
  ret void (%struct.liq_image*)* @liq_image_destroy
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.11"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.11"* %this, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.11"*, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.11"* %this1, i32 0, i32 0
  ret %struct.liq_image** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::integral_constant.13"* @_ZNSt3__222__compressed_pair_elemINS_17integral_constantIPFvP9liq_imageEXadL_Z17liq_image_destroyEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.12"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.12"* %this, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.12"*, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.12"* %this1 to %"struct.std::__2::integral_constant.13"*
  ret %"struct.std::__2::integral_constant.13"* %0
}

declare void @liq_image_destroy(%struct.liq_image*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNKSt3__217__compressed_pairIP9liq_imageNS_17integral_constantIPFvS2_EXadL_Z17liq_image_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNKSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %0) #3
  ret %struct.liq_image** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_image** @_ZNKSt3__222__compressed_pair_elemIP9liq_imageLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.11"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.11"* %this, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.11"*, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.11"* %this1, i32 0, i32 0
  ret %struct.liq_image** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNKSt3__217__compressed_pairIP10liq_resultNS_17integral_constantIPFvS2_EXadL_Z18liq_result_destroyEEEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNKSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #3
  ret %struct.liq_result** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_result** @_ZNKSt3__222__compressed_pair_elemIP10liq_resultLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %struct.liq_result** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i8* null, i8** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i8* null, i8** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.14"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.14"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm(%"class.std::__2::vector"* %this, i32 %__n) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #3
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #15
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %2) #3
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call i8* @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %4, i32 0, i32 1
  store i8* %call3, i8** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 0
  store i8* %call3, i8** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load i8*, i8** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call5 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %9) #3
  store i8* %add.ptr, i8** %call5, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 0) #3
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endEm(%"class.std::__2::vector"* %this, i32 %__n) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i8*, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load i8*, i8** %__new_end_, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %3) #3
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load i8*, i8** %__pos_3, align 4
  %call4 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %4) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9constructIhJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %call2, i8* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load i8*, i8** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.14"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.14"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.14"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.14"* %this, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.14"*, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.14"* %this1 to %"struct.std::__2::__compressed_pair_elem.15"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem.15"* @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.15"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.14"* %this1 to %"struct.std::__2::__compressed_pair_elem.16"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call4 = call %"struct.std::__2::__compressed_pair_elem.16"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.16"* %2)
  ret %"class.std::__2::__compressed_pair.14"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.15"* @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.15"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.15"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.15"* %this, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.15"*, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.15", %"struct.std::__2::__compressed_pair_elem.15"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #3
  store i8* null, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.15"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.16"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.16"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.16"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.16"* %this, %"struct.std::__2::__compressed_pair_elem.16"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.16"*, %"struct.std::__2::__compressed_pair_elem.16"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.16"* %this1 to %"class.std::__2::allocator.17"*
  %call = call %"class.std::__2::allocator.17"* @_ZNSt3__29allocatorIhEC2Ev(%"class.std::__2::allocator.17"* %1) #3
  ret %"struct.std::__2::__compressed_pair_elem.16"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.17"* @_ZNSt3__29allocatorIhEC2Ev(%"class.std::__2::allocator.17"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.17"*, align 4
  store %"class.std::__2::allocator.17"* %this, %"class.std::__2::allocator.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %this.addr, align 4
  ret %"class.std::__2::allocator.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #3
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %call) #3
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #3
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #8

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.17"* %__a, %"class.std::__2::allocator.17"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i8* @_ZNSt3__29allocatorIhE8allocateEmPKv(%"class.std::__2::allocator.17"* %0, i32 %1, i8* null)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.14"* %__end_cap_) #3
  ret %"class.std::__2::allocator.17"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.14"* %__end_cap_) #3
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %0 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i8, i8* %call7, i32 %0
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr8) #3
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #2 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.26", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.17"* %__a, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant.26"*
  %1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %1) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.14"* %__end_cap_) #3
  ret %"class.std::__2::allocator.17"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #3
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #2 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.26", align 1
  %__a.addr = alloca %"class.std::__2::allocator.17"*, align 4
  store %"class.std::__2::allocator.17"* %__a, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator.17"* %1) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.17"*, align 4
  store %"class.std::__2::allocator.17"* %this, %"class.std::__2::allocator.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %this.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.14"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.14"*, align 4
  store %"class.std::__2::__compressed_pair.14"* %this, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.14"*, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.14"* %this1 to %"struct.std::__2::__compressed_pair_elem.16"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.16"* %0) #3
  ret %"class.std::__2::allocator.17"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.16"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.16"* %this, %"struct.std::__2::__compressed_pair_elem.16"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.16"*, %"struct.std::__2::__compressed_pair_elem.16"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.16"* %this1 to %"class.std::__2::allocator.17"*
  ret %"class.std::__2::allocator.17"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNSt3__29allocatorIhE8allocateEmPKv(%"class.std::__2::allocator.17"* %this, i32 %__n, i8* %0) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.17"* %this, %"class.std::__2::allocator.17"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator.17"* %this1) #3
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.5, i32 0, i32 0)) #15
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 1
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  ret i8* %call2
}

; Function Attrs: noinline noreturn optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #9 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  %exception = call i8* @__cxa_allocate_exception(i32 8) #3
  %0 = bitcast i8* %exception to %"class.std::length_error"*
  %1 = load i8*, i8** %__msg.addr, align 4
  %call = call %"class.std::length_error"* @_ZNSt12length_errorC2EPKc(%"class.std::length_error"* %0, i8* %1)
  call void @__cxa_throw(i8* %exception, i8* bitcast (i8** @_ZTISt12length_error to i8*), i8* bitcast (%"class.std::length_error"* (%"class.std::length_error"*)* @_ZNSt12length_errorD1Ev to i8*)) #15
  unreachable
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #2 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #16
  ret i8* %call
}

declare i8* @__cxa_allocate_exception(i32)

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::length_error"* @_ZNSt12length_errorC2EPKc(%"class.std::length_error"* returned %this, i8* %__s) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::length_error"*, align 4
  %__s.addr = alloca i8*, align 4
  store %"class.std::length_error"* %this, %"class.std::length_error"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %this1 = load %"class.std::length_error"*, %"class.std::length_error"** %this.addr, align 4
  %0 = bitcast %"class.std::length_error"* %this1 to %"class.std::logic_error"*
  %1 = load i8*, i8** %__s.addr, align 4
  %call = call %"class.std::logic_error"* @_ZNSt11logic_errorC2EPKc(%"class.std::logic_error"* %0, i8* %1)
  %2 = bitcast %"class.std::length_error"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVSt12length_error, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  ret %"class.std::length_error"* %this1
}

; Function Attrs: nounwind
declare %"class.std::length_error"* @_ZNSt12length_errorD1Ev(%"class.std::length_error"* returned) unnamed_addr #10

declare void @__cxa_throw(i8*, i8*, i8*)

declare %"class.std::logic_error"* @_ZNSt11logic_errorC2EPKc(%"class.std::logic_error"* returned, i8*) unnamed_addr #4

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #11

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.14"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.14"*, align 4
  store %"class.std::__2::__compressed_pair.14"* %this, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.14"*, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.14"* %this1 to %"struct.std::__2::__compressed_pair_elem.16"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.16"* %0) #3
  ret %"class.std::__2::allocator.17"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.16"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.16"* %this, %"struct.std::__2::__compressed_pair_elem.16"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.16"*, %"struct.std::__2::__compressed_pair_elem.16"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.16"* %this1 to %"class.std::__2::allocator.17"*
  ret %"class.std::__2::allocator.17"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.14"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.14"*, align 4
  store %"class.std::__2::__compressed_pair.14"* %this, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.14"*, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.14"* %this1 to %"struct.std::__2::__compressed_pair_elem.15"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.15"* %0) #3
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.15"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.15"* %this, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.15"*, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.15", %"struct.std::__2::__compressed_pair_elem.15"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #3
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.14"* %__end_cap_) #3
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.14"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.14"*, align 4
  store %"class.std::__2::__compressed_pair.14"* %this, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.14"*, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.14"* %this1 to %"struct.std::__2::__compressed_pair_elem.15"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.15"* %0) #3
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.15"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.15"* %this, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.15"*, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.15", %"struct.std::__2::__compressed_pair_elem.15"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i8*, i8** %__end_, align 4
  store i8* %3, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i8*, i8** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr, i8** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9constructIhJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.26", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.17"* %__a, %"class.std::__2::allocator.17"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant.26"*
  %1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE11__constructIhJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i8* %0, i8** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE11__constructIhJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.26", align 1
  %__a.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.17"* %__a, %"class.std::__2::allocator.17"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE9constructIhJEEEvPT_DpOT0_(%"class.std::__2::allocator.17"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE9constructIhJEEEvPT_DpOT0_(%"class.std::__2::allocator.17"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.17"* %this, %"class.std::__2::allocator.17"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  store i8 0, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #3
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base"* %this1, i8* %0) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.17"* %__a, %"class.std::__2::allocator.17"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.17"* %0, i8* %1, i32 %2) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.17"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #3
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.26", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.17"* %__a, %"class.std::__2::allocator.17"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant.26"*
  %1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.17"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.26", align 1
  %__a.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.17"* %__a, %"class.std::__2::allocator.17"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.17"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.17"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.17"* %this, %"class.std::__2::allocator.17"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.17"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.17"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.17"* %this, %"class.std::__2::allocator.17"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.17"*, %"class.std::__2::allocator.17"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #2 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #2 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #2 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #17
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #12

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.20"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEEC2Ev(%"class.std::__2::__vector_base.20"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.20"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 0
  store %struct.liq_color* null, %struct.liq_color** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 1
  store %struct.liq_color* null, %struct.liq_color** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.21"* @_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.21"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.20"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE11__vallocateEm(%"class.std::__2::vector.19"* %this, i32 %__n) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8max_sizeEv(%"class.std::__2::vector.19"* %this1) #3
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #15
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv(%"class.std::__2::__vector_base.20"* %2) #3
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call %struct.liq_color* @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE8allocateERS3_m(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %4, i32 0, i32 1
  store %struct.liq_color* %call3, %struct.liq_color** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %5, i32 0, i32 0
  store %struct.liq_color* %call3, %struct.liq_color** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %6, i32 0, i32 0
  %7 = load %struct.liq_color*, %struct.liq_color** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %struct.liq_color, %struct.liq_color* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %call5 = call nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE9__end_capEv(%"class.std::__2::__vector_base.20"* %9) #3
  store %struct.liq_color* %add.ptr, %struct.liq_color** %call5, align 4
  call void @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE14__annotate_newEm(%"class.std::__2::vector.19"* %this1, i32 0) #3
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE18__construct_at_endEm(%"class.std::__2::vector.19"* %this, i32 %__n) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE21_ConstructTransactionC2ERS4_m(%"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.19"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %struct.liq_color*, %struct.liq_color** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %struct.liq_color*, %struct.liq_color** %__new_end_, align 4
  %cmp = icmp ne %struct.liq_color* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv(%"class.std::__2::__vector_base.20"* %3) #3
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %struct.liq_color*, %struct.liq_color** %__pos_3, align 4
  %call4 = call %struct.liq_color* @_ZNSt3__212__to_addressI9liq_colorEEPT_S3_(%struct.liq_color* %4) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE9constructIS2_JEEEvRS3_PT_DpOT0_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %call2, %struct.liq_color* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %struct.liq_color*, %struct.liq_color** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %struct.liq_color, %struct.liq_color* %5, i32 1
  store %struct.liq_color* %incdec.ptr, %struct.liq_color** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %__tx) #3
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.21"* @_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.21"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.22"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem.22"* @_ZNSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.22"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.23"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call4 = call %"struct.std::__2::__compressed_pair_elem.23"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.23"* %2)
  ret %"class.std::__2::__compressed_pair.21"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.22"* @_ZNSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.22"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.22"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.22"* %this, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.22"*, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.22", %"struct.std::__2::__compressed_pair_elem.22"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #3
  store %struct.liq_color* null, %struct.liq_color** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.22"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.23"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.23"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.23"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.23"* %this, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.23"*, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.23"* %this1 to %"class.std::__2::allocator.24"*
  %call = call %"class.std::__2::allocator.24"* @_ZNSt3__29allocatorI9liq_colorEC2Ev(%"class.std::__2::allocator.24"* %1) #3
  ret %"struct.std::__2::__compressed_pair_elem.23"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.24"* @_ZNSt3__29allocatorI9liq_colorEC2Ev(%"class.std::__2::allocator.24"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.24"*, align 4
  store %"class.std::__2::allocator.24"* %this, %"class.std::__2::allocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %this.addr, align 4
  ret %"class.std::__2::allocator.24"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8max_sizeEv(%"class.std::__2::vector.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv(%"class.std::__2::__vector_base.20"* %0) #3
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE8max_sizeERKS3_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %call) #3
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #3
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.liq_color* @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE8allocateERS3_m(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %struct.liq_color* @_ZNSt3__29allocatorI9liq_colorE8allocateEmPKv(%"class.std::__2::allocator.24"* %0, i32 %1, i8* null)
  ret %struct.liq_color* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv(%"class.std::__2::__vector_base.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE6secondEv(%"class.std::__2::__compressed_pair.21"* %__end_cap_) #3
  ret %"class.std::__2::allocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE9__end_capEv(%"class.std::__2::__vector_base.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE5firstEv(%"class.std::__2::__compressed_pair.21"* %__end_cap_) #3
  ret %struct.liq_color** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE14__annotate_newEm(%"class.std::__2::vector.19"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %call = call %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this1) #3
  %0 = bitcast %struct.liq_color* %call to i8*
  %call2 = call %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this1) #3
  %call3 = call i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8capacityEv(%"class.std::__2::vector.19"* %this1) #3
  %add.ptr = getelementptr inbounds %struct.liq_color, %struct.liq_color* %call2, i32 %call3
  %1 = bitcast %struct.liq_color* %add.ptr to i8*
  %call4 = call %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this1) #3
  %call5 = call i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8capacityEv(%"class.std::__2::vector.19"* %this1) #3
  %add.ptr6 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %call4, i32 %call5
  %2 = bitcast %struct.liq_color* %add.ptr6 to i8*
  %call7 = call %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this1) #3
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %call7, i32 %3
  %4 = bitcast %struct.liq_color* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE31__annotate_contiguous_containerEPKvS6_S6_S6_(%"class.std::__2::vector.19"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE8max_sizeERKS3_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.26", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.27", align 1
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.27"* %ref.tmp to %"struct.std::__2::integral_constant.26"*
  %1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE10__max_sizeENS_17integral_constantIbLb1EEERKS3_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %1) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv(%"class.std::__2::__vector_base.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNKSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE6secondEv(%"class.std::__2::__compressed_pair.21"* %__end_cap_) #3
  ret %"class.std::__2::allocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE10__max_sizeENS_17integral_constantIbLb1EEERKS3_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.26", align 1
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorI9liq_colorE8max_sizeEv(%"class.std::__2::allocator.24"* %1) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorI9liq_colorE8max_sizeEv(%"class.std::__2::allocator.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.24"*, align 4
  store %"class.std::__2::allocator.24"* %this, %"class.std::__2::allocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNKSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE6secondEv(%"class.std::__2::__compressed_pair.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.23"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %0) #3
  ret %"class.std::__2::allocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.23"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.23"* %this, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.23"*, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.23"* %this1 to %"class.std::__2::allocator.24"*
  ret %"class.std::__2::allocator.24"* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.liq_color* @_ZNSt3__29allocatorI9liq_colorE8allocateEmPKv(%"class.std::__2::allocator.24"* %this, i32 %__n, i8* %0) #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.24"* %this, %"class.std::__2::allocator.24"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorI9liq_colorE8max_sizeEv(%"class.std::__2::allocator.24"* %this1) #3
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.5, i32 0, i32 0)) #15
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  %3 = bitcast i8* %call2 to %struct.liq_color*
  ret %struct.liq_color* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE6secondEv(%"class.std::__2::__compressed_pair.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.23"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %0) #3
  ret %"class.std::__2::allocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorI9liq_colorEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.23"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.23"* %this, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.23"*, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.23"* %this1 to %"class.std::__2::allocator.24"*
  ret %"class.std::__2::allocator.24"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE5firstEv(%"class.std::__2::__compressed_pair.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.22"*
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.22"* %0) #3
  ret %struct.liq_color** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.22"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.22"* %this, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.22"*, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.22", %"struct.std::__2::__compressed_pair_elem.22"* %this1, i32 0, i32 0
  ret %struct.liq_color** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE31__annotate_contiguous_containerEPKvS6_S6_S6_(%"class.std::__2::vector.19"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %0, i32 0, i32 0
  %1 = load %struct.liq_color*, %struct.liq_color** %__begin_, align 4
  %call = call %struct.liq_color* @_ZNSt3__212__to_addressI9liq_colorEEPT_S3_(%struct.liq_color* %1) #3
  ret %struct.liq_color* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8capacityEv(%"class.std::__2::vector.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.19"* %this1 to %"class.std::__2::__vector_base.20"*
  %call = call i32 @_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE8capacityEv(%"class.std::__2::__vector_base.20"* %0) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.liq_color* @_ZNSt3__212__to_addressI9liq_colorEEPT_S3_(%struct.liq_color* %__p) #0 comdat {
entry:
  %__p.addr = alloca %struct.liq_color*, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  %0 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  ret %struct.liq_color* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE8capacityEv(%"class.std::__2::__vector_base.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE9__end_capEv(%"class.std::__2::__vector_base.20"* %this1) #3
  %0 = load %struct.liq_color*, %struct.liq_color** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 0
  %1 = load %struct.liq_color*, %struct.liq_color** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %struct.liq_color* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.liq_color* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE9__end_capEv(%"class.std::__2::__vector_base.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNKSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE5firstEv(%"class.std::__2::__compressed_pair.21"* %__end_cap_) #3
  ret %struct.liq_color** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNKSt3__217__compressed_pairIP9liq_colorNS_9allocatorIS1_EEE5firstEv(%"class.std::__2::__compressed_pair.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.22"*
  %call = call nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNKSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.22"* %0) #3
  ret %struct.liq_color** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.liq_color** @_ZNKSt3__222__compressed_pair_elemIP9liq_colorLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.22"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.22"* %this, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.22"*, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.22", %"struct.std::__2::__compressed_pair_elem.22"* %this1, i32 0, i32 0
  ret %struct.liq_color** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE21_ConstructTransactionC2ERS4_m(%"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.19"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.19"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.19"* %__v, %"class.std::__2::vector.19"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"*, %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %__v.addr, align 4
  store %"class.std::__2::vector.19"* %0, %"class.std::__2::vector.19"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.19"* %1 to %"class.std::__2::__vector_base.20"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %2, i32 0, i32 1
  %3 = load %struct.liq_color*, %struct.liq_color** %__end_, align 4
  store %struct.liq_color* %3, %struct.liq_color** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.19"* %4 to %"class.std::__2::__vector_base.20"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %5, i32 0, i32 1
  %6 = load %struct.liq_color*, %struct.liq_color** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %struct.liq_color, %struct.liq_color* %6, i32 %7
  store %struct.liq_color* %add.ptr, %struct.liq_color** %__new_end_, align 4
  ret %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE9constructIS2_JEEEvRS3_PT_DpOT0_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a, %struct.liq_color* %__p) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %struct.liq_color*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.26", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.28", align 1
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.28"* %ref.tmp to %"struct.std::__2::integral_constant.26"*
  %1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %2 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE11__constructIS2_JEEEvNS_17integral_constantIbLb1EEERS3_PT_DpOT0_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %1, %struct.liq_color* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* @_ZNSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"*, %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %struct.liq_color*, %struct.liq_color** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction", %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.19"* %1 to %"class.std::__2::__vector_base.20"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %2, i32 0, i32 1
  store %struct.liq_color* %0, %struct.liq_color** %__end_, align 4
  ret %"struct.std::__2::vector<liq_color, std::__2::allocator<liq_color>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE11__constructIS2_JEEEvNS_17integral_constantIbLb1EEERS3_PT_DpOT0_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a, %struct.liq_color* %__p) #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.26", align 1
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %struct.liq_color*, align 4
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %2 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  call void @_ZNSt3__29allocatorI9liq_colorE9constructIS1_JEEEvPT_DpOT0_(%"class.std::__2::allocator.24"* %1, %struct.liq_color* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorI9liq_colorE9constructIS1_JEEEvPT_DpOT0_(%"class.std::__2::allocator.24"* %this, %struct.liq_color* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %struct.liq_color*, align 4
  store %"class.std::__2::allocator.24"* %this, %"class.std::__2::allocator.24"** %this.addr, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %this.addr, align 4
  %0 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  %1 = bitcast %struct.liq_color* %0 to i8*
  %2 = bitcast i8* %1 to %struct.liq_color*
  %3 = bitcast %struct.liq_color* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %3, i8 0, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE17__annotate_deleteEv(%"class.std::__2::vector.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.19"*, align 4
  store %"class.std::__2::vector.19"* %this, %"class.std::__2::vector.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.19"*, %"class.std::__2::vector.19"** %this.addr, align 4
  %call = call %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this1) #3
  %0 = bitcast %struct.liq_color* %call to i8*
  %call2 = call %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this1) #3
  %call3 = call i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8capacityEv(%"class.std::__2::vector.19"* %this1) #3
  %add.ptr = getelementptr inbounds %struct.liq_color, %struct.liq_color* %call2, i32 %call3
  %1 = bitcast %struct.liq_color* %add.ptr to i8*
  %call4 = call %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this1) #3
  %call5 = call i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4sizeEv(%"class.std::__2::vector.19"* %this1) #3
  %add.ptr6 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %call4, i32 %call5
  %2 = bitcast %struct.liq_color* %add.ptr6 to i8*
  %call7 = call %struct.liq_color* @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE4dataEv(%"class.std::__2::vector.19"* %this1) #3
  %call8 = call i32 @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE8capacityEv(%"class.std::__2::vector.19"* %this1) #3
  %add.ptr9 = getelementptr inbounds %struct.liq_color, %struct.liq_color* %call7, i32 %call8
  %3 = bitcast %struct.liq_color* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorI9liq_colorNS_9allocatorIS1_EEE31__annotate_contiguous_containerEPKvS6_S6_S6_(%"class.std::__2::vector.19"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.20"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEED2Ev(%"class.std::__2::__vector_base.20"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.20"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  store %"class.std::__2::__vector_base.20"* %this1, %"class.std::__2::__vector_base.20"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 0
  %0 = load %struct.liq_color*, %struct.liq_color** %__begin_, align 4
  %cmp = icmp ne %struct.liq_color* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE5clearEv(%"class.std::__2::__vector_base.20"* %this1) #3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv(%"class.std::__2::__vector_base.20"* %this1) #3
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 0
  %1 = load %struct.liq_color*, %struct.liq_color** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE8capacityEv(%"class.std::__2::__vector_base.20"* %this1) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE10deallocateERS3_PS2_m(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %call, %struct.liq_color* %1, i32 %call3) #3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %retval, align 4
  ret %"class.std::__2::__vector_base.20"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE5clearEv(%"class.std::__2::__vector_base.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 0
  %0 = load %struct.liq_color*, %struct.liq_color** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE17__destruct_at_endEPS1_(%"class.std::__2::__vector_base.20"* %this1, %struct.liq_color* %0) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE10deallocateERS3_PS2_m(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a, %struct.liq_color* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %struct.liq_color*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %1 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorI9liq_colorE10deallocateEPS1_m(%"class.std::__2::allocator.24"* %0, %struct.liq_color* %1, i32 %2) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE17__destruct_at_endEPS1_(%"class.std::__2::__vector_base.20"* %this, %struct.liq_color* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.20"*, align 4
  %__new_last.addr = alloca %struct.liq_color*, align 4
  %__soon_to_be_end = alloca %struct.liq_color*, align 4
  store %"class.std::__2::__vector_base.20"* %this, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  store %struct.liq_color* %__new_last, %struct.liq_color** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.20"*, %"class.std::__2::__vector_base.20"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 1
  %0 = load %struct.liq_color*, %struct.liq_color** %__end_, align 4
  store %struct.liq_color* %0, %struct.liq_color** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %struct.liq_color*, %struct.liq_color** %__new_last.addr, align 4
  %2 = load %struct.liq_color*, %struct.liq_color** %__soon_to_be_end, align 4
  %cmp = icmp ne %struct.liq_color* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__213__vector_baseI9liq_colorNS_9allocatorIS1_EEE7__allocEv(%"class.std::__2::__vector_base.20"* %this1) #3
  %3 = load %struct.liq_color*, %struct.liq_color** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %struct.liq_color, %struct.liq_color* %3, i32 -1
  store %struct.liq_color* %incdec.ptr, %struct.liq_color** %__soon_to_be_end, align 4
  %call2 = call %struct.liq_color* @_ZNSt3__212__to_addressI9liq_colorEEPT_S3_(%struct.liq_color* %incdec.ptr) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE7destroyIS2_EEvRS3_PT_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %call, %struct.liq_color* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %struct.liq_color*, %struct.liq_color** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.20", %"class.std::__2::__vector_base.20"* %this1, i32 0, i32 1
  store %struct.liq_color* %4, %struct.liq_color** %__end_3, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE7destroyIS2_EEvRS3_PT_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a, %struct.liq_color* %__p) #2 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %struct.liq_color*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.26", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.29", align 1
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.29"* %ref.tmp to %"struct.std::__2::integral_constant.26"*
  %1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %2 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE9__destroyIS2_EEvNS_17integral_constantIbLb1EEERS3_PT_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %1, %struct.liq_color* %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorI9liq_colorEEE9__destroyIS2_EEvNS_17integral_constantIbLb1EEERS3_PT_(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a, %struct.liq_color* %__p) #2 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.26", align 1
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %struct.liq_color*, align 4
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %2 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  call void @_ZNSt3__29allocatorI9liq_colorE7destroyEPS1_(%"class.std::__2::allocator.24"* %1, %struct.liq_color* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorI9liq_colorE7destroyEPS1_(%"class.std::__2::allocator.24"* %this, %struct.liq_color* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %struct.liq_color*, align 4
  store %"class.std::__2::allocator.24"* %this, %"class.std::__2::allocator.24"** %this.addr, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %this.addr, align 4
  %0 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorI9liq_colorE10deallocateEPS1_m(%"class.std::__2::allocator.24"* %this, %struct.liq_color* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %struct.liq_color*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.24"* %this, %"class.std::__2::allocator.24"** %this.addr, align 4
  store %struct.liq_color* %__p, %struct.liq_color** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %this.addr, align 4
  %0 = load %struct.liq_color*, %struct.liq_color** %__p.addr, align 4
  %1 = bitcast %struct.liq_color* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.emscripten::memory_view"* @_ZN10emscripten11memory_viewIhEC2EmPKh(%"struct.emscripten::memory_view"* returned %this, i32 %size, i8* %data) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %size.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  store %"struct.emscripten::memory_view"* %this, %"struct.emscripten::memory_view"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %this1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %this.addr, align 4
  %size2 = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  store i32 %0, i32* %size2, align 4
  %data3 = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %this1, i32 0, i32 1
  %1 = load i8*, i8** %data.addr, align 4
  store i8* %1, i8** %data3, align 4
  ret %"struct.emscripten::memory_view"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %impl.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %argList = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList", align 1
  %argv = alloca %"struct.emscripten::internal::WireTypePack", align 8
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  %call2 = call %"struct.emscripten::internal::WireTypePack"* @_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_(%"struct.emscripten::internal::WireTypePack"* %argv, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %3 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  %call3 = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %argList)
  %call4 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %argList)
  %call5 = call i8* @_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv(%"struct.emscripten::internal::WireTypePack"* %argv)
  %call6 = call %"struct.emscripten::internal::_EM_VAL"* %2(%"struct.emscripten::internal::_EM_VAL"* %3, i32 %call3, i8** %call4, i8* %call5)
  %call7 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call6)
  ret void
}

declare %"struct.emscripten::internal::_EM_VAL"* @_emval_new(%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"struct.emscripten::memory_view"* %__t, %"struct.emscripten::memory_view"** %__t.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %__t.addr, align 4
  ret %"struct.emscripten::memory_view"* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::WireTypePack"* @_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_(%"struct.emscripten::internal::WireTypePack"* returned %this, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack"*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %cursor = alloca %"union.emscripten::internal::GenericWireType"*, align 4
  store %"struct.emscripten::internal::WireTypePack"* %this, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack"*, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %elements2 = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %elements2) #3
  store %"union.emscripten::internal::GenericWireType"* %call, %"union.emscripten::internal::GenericWireType"** %cursor, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %0) #3
  call void @_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call3)
  ret %"struct.emscripten::internal::WireTypePack"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv()
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv(%"struct.emscripten::internal::WireTypePack"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack"*, align 4
  store %"struct.emscripten::internal::WireTypePack"* %this, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack"*, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %elements) #3
  %0 = bitcast %"union.emscripten::internal::GenericWireType"* %call to i8*
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1 x %"union.emscripten::internal::GenericWireType"], [1 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: alwaysinline
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %first) #13 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %first.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %ref.tmp = alloca %"struct.emscripten::memory_view", align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::memory_view"* %first, %"struct.emscripten::memory_view"** %first.addr, align 4
  %0 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %first.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #3
  call void @_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  call void @_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  call void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %wt) #0 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %wt.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::memory_view"* %wt, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %size = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %0, i32 0, i32 0
  %1 = load i32, i32* %size, align 4
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %3 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %2, align 4
  %w = bitcast %"union.emscripten::internal::GenericWireType"* %3 to [2 x %union.anon.30]*
  %arrayidx = getelementptr inbounds [2 x %union.anon.30], [2 x %union.anon.30]* %w, i32 0, i32 0
  %u = bitcast %union.anon.30* %arrayidx to i32*
  store i32 %1, i32* %u, align 8
  %4 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %data = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %4, i32 0, i32 1
  %5 = load i8*, i8** %data, align 4
  %6 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %7 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %6, align 4
  %w1 = bitcast %"union.emscripten::internal::GenericWireType"* %7 to [2 x %union.anon.30]*
  %arrayidx2 = getelementptr inbounds [2 x %union.anon.30], [2 x %union.anon.30]* %w1, i32 0, i32 1
  %p = bitcast %union.anon.30* %arrayidx2 to i8**
  store i8* %5, i8** %p, align 4
  %8 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %9 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %8, align 4
  %incdec.ptr = getelementptr inbounds %"union.emscripten::internal::GenericWireType", %"union.emscripten::internal::GenericWireType"* %9, i32 1
  store %"union.emscripten::internal::GenericWireType"* %incdec.ptr, %"union.emscripten::internal::GenericWireType"** %8, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_(%"struct.emscripten::memory_view"* noalias sret align 4 %agg.result, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %mv) #0 comdat {
entry:
  %mv.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"struct.emscripten::memory_view"* %mv, %"struct.emscripten::memory_view"** %mv.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %mv.addr, align 4
  %1 = bitcast %"struct.emscripten::memory_view"* %agg.result to i8*
  %2 = bitcast %"struct.emscripten::memory_view"* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0) #0 comdat {
entry:
  %.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  store %"union.emscripten::internal::GenericWireType"** %0, %"union.emscripten::internal::GenericWireType"*** %.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv() #0 comdat {
entry:
  ret i8** getelementptr inbounds ([1 x i8*], [1 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1 x %"union.emscripten::internal::GenericWireType"], [1 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #14

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEiiifEE6invokeEPFS2_S9_iiifEPNS0_11BindingTypeIS9_vEUt_Eiiif(void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)* %fn, %struct.anon.32* %args, i32 %args1, i32 %args3, i32 %args5, float %args7) #2 comdat {
entry:
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, align 4
  %args.addr = alloca %struct.anon.32*, align 4
  %args.addr2 = alloca i32, align 4
  %args.addr4 = alloca i32, align 4
  %args.addr6 = alloca i32, align 4
  %args.addr8 = alloca float, align 4
  %ref.tmp = alloca %"class.emscripten::val", align 4
  %agg.tmp = alloca %"class.std::__2::basic_string", align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)** %fn.addr, align 4
  store %struct.anon.32* %args, %struct.anon.32** %args.addr, align 4
  store i32 %args1, i32* %args.addr2, align 4
  store i32 %args3, i32* %args.addr4, align 4
  store i32 %args5, i32* %args.addr6, align 4
  store float %args7, float* %args.addr8, align 4
  %0 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)** %fn.addr, align 4
  %1 = load %struct.anon.32*, %struct.anon.32** %args.addr, align 4
  call void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* sret align 4 %agg.tmp, %struct.anon.32* %1)
  %2 = load i32, i32* %args.addr2, align 4
  %call = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %2)
  %3 = load i32, i32* %args.addr4, align 4
  %call9 = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %3)
  %4 = load i32, i32* %args.addr6, align 4
  %call10 = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %4)
  %5 = load float, float* %args.addr8, align 4
  %call11 = call float @_ZN10emscripten8internal11BindingTypeIfvE12fromWireTypeEf(float %5)
  call void %0(%"class.emscripten::val"* sret align 4 %ref.tmp, %"class.std::__2::basic_string"* %agg.tmp, i32 %call, i32 %call9, i32 %call10, float %call11)
  %call12 = call %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call13 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %ref.tmp) #3
  %call14 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %agg.tmp) #3
  ret %"struct.emscripten::internal::_EM_VAL"* %call12
}

declare void @_embind_register_function(i8*, i32, i8**, i8*, i8*, i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"** %this.addr, align 4
  ret i32 6
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.31"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiiifEEEE3getEv()
  ret i8** %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEPNS0_11BindingTypeISB_vEUt_EiiifEEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)* %0) #13 comdat {
entry:
  %.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)*, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)* %0, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, i32, float)*, %struct.anon.32*, i32, i32, i32, float)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEPNS0_11BindingTypeISB_vEUt_EiiifEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %v) #2 comdat {
entry:
  %v.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %v, %"class.emscripten::val"** %v.addr, align 4
  %0 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %0, i32 0, i32 0
  %1 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_incref(%"struct.emscripten::internal::_EM_VAL"* %1)
  %2 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle1 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %2, i32 0, i32 0
  %3 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle1, align 4
  ret %"struct.emscripten::internal::_EM_VAL"* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, %struct.anon.32* %v) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %v.addr = alloca %struct.anon.32*, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.anon.32* %v, %struct.anon.32** %v.addr, align 4
  %1 = load %struct.anon.32*, %struct.anon.32** %v.addr, align 4
  %data = getelementptr inbounds %struct.anon.32, %struct.anon.32* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [1 x i8], [1 x i8]* %data, i32 0, i32 0
  %2 = load %struct.anon.32*, %struct.anon.32** %v.addr, align 4
  %length = getelementptr inbounds %struct.anon.32, %struct.anon.32* %2, i32 0, i32 0
  %3 = load i32, i32* %length, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm(%"class.std::__2::basic_string"* %agg.result, i8* %arraydecay, i32 %3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %v) #0 comdat {
entry:
  %v.addr = alloca i32, align 4
  store i32 %v, i32* %v.addr, align 4
  %0 = load i32, i32* %v.addr, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN10emscripten8internal11BindingTypeIfvE12fromWireTypeEf(float %v) #0 comdat {
entry:
  %v.addr = alloca float, align 4
  store float %v, float* %v.addr, align 4
  %0 = load float, float* %v.addr, align 4
  ret float %0
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #10

declare void @_emval_incref(%"struct.emscripten::internal::_EM_VAL"*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm(%"class.std::__2::basic_string"* returned %this, i8* %__s, i32 %__n) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %2)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.2"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call5 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* %2)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.2"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* %1) #3
  ret %"struct.std::__2::__compressed_pair_elem.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiiifEEEE3getEv() #0 comdat {
entry:
  ret i8** getelementptr inbounds ([6 x i8*], [6 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiiifEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiiifEPNS0_11BindingTypeISB_vEUt_EiiifEEEPKcv() #13 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiiiifEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiiiifEEEPKcv() #0 comdat {
entry:
  ret i8* getelementptr inbounds ([8 x i8], [8 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiiiiifEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEiifEE6invokeEPFS2_S9_iifEPNS0_11BindingTypeIS9_vEUt_Eiif(void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)* %fn, %struct.anon.32* %args, i32 %args1, i32 %args3, float %args5) #2 comdat {
entry:
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, align 4
  %args.addr = alloca %struct.anon.32*, align 4
  %args.addr2 = alloca i32, align 4
  %args.addr4 = alloca i32, align 4
  %args.addr6 = alloca float, align 4
  %ref.tmp = alloca %"class.emscripten::val", align 4
  %agg.tmp = alloca %"class.std::__2::basic_string", align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)** %fn.addr, align 4
  store %struct.anon.32* %args, %struct.anon.32** %args.addr, align 4
  store i32 %args1, i32* %args.addr2, align 4
  store i32 %args3, i32* %args.addr4, align 4
  store float %args5, float* %args.addr6, align 4
  %0 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)** %fn.addr, align 4
  %1 = load %struct.anon.32*, %struct.anon.32** %args.addr, align 4
  call void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* sret align 4 %agg.tmp, %struct.anon.32* %1)
  %2 = load i32, i32* %args.addr2, align 4
  %call = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %2)
  %3 = load i32, i32* %args.addr4, align 4
  %call7 = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %3)
  %4 = load float, float* %args.addr6, align 4
  %call8 = call float @_ZN10emscripten8internal11BindingTypeIfvE12fromWireTypeEf(float %4)
  call void %0(%"class.emscripten::val"* sret align 4 %ref.tmp, %"class.std::__2::basic_string"* %agg.tmp, i32 %call, i32 %call7, float %call8)
  %call9 = call %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call10 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %ref.tmp) #3
  %call11 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %agg.tmp) #3
  ret %"struct.emscripten::internal::_EM_VAL"* %call9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"** %this.addr, align 4
  ret i32 5
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.33"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiifEEEE3getEv()
  ret i8** %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEPNS0_11BindingTypeISB_vEUt_EiifEEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)* %0) #13 comdat {
entry:
  %.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)*, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)* %0, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, float)*, %struct.anon.32*, i32, i32, float)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEPNS0_11BindingTypeISB_vEUt_EiifEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiifEEEE3getEv() #0 comdat {
entry:
  ret i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEiifEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEiifEPNS0_11BindingTypeISB_vEUt_EiifEEEPKcv() #13 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiiifEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiiifEEEPKcv() #0 comdat {
entry:
  ret i8* getelementptr inbounds ([7 x i8], [7 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiiiifEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal7InvokerIiJEE6invokeEPFivE(i32 ()* %fn) #2 comdat {
entry:
  %fn.addr = alloca i32 ()*, align 4
  %ref.tmp = alloca i32, align 4
  store i32 ()* %fn, i32 ()** %fn.addr, align 4
  %0 = load i32 ()*, i32 ()** %fn.addr, align 4
  %call = call i32 %0()
  store i32 %call, i32* %ref.tmp, align 4
  %call1 = call i32 @_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi(i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret i32 %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.34"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEv()
  ret i8** %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIiJPFivEEEEPKcPFT_DpT0_E(i32 (i32 ()*)* %0) #13 comdat {
entry:
  %.addr = alloca i32 (i32 ()*)*, align 4
  store i32 (i32 ()*)* %0, i32 (i32 ()*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJiPFivEEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi(i32* nonnull align 4 dereferenceable(4) %v) #0 comdat {
entry:
  %v.addr = alloca i32*, align 4
  store i32* %v, i32** %v.addr, align 4
  %0 = load i32*, i32** %v.addr, align 4
  %1 = load i32, i32* %0, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEv() #0 comdat {
entry:
  ret i8** getelementptr inbounds ([1 x i8*], [1 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJiPFivEEEEPKcv() #13 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiEEEPKcv() #0 comdat {
entry:
  ret i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_imagequant.cpp() #1 {
entry:
  call void @__cxx_global_var_init.1()
  ret void
}

; Function Attrs: noinline
define internal void @__tls_init() #1 {
entry:
  %0 = load i8, i8* @__tls_guard, align 1
  %guard.uninitialized = icmp eq i8 %0, 0
  br i1 %guard.uninitialized, label %init, label %exit, !prof !2

init:                                             ; preds = %entry
  store i8 1, i8* @__tls_guard, align 1
  call void @__cxx_global_var_init()
  br label %exit

exit:                                             ; preds = %init, %entry
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { noinline noreturn optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #12 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #13 = { alwaysinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #14 = { nounwind readnone speculatable willreturn }
attributes #15 = { noreturn }
attributes #16 = { builtin allocsize(0) }
attributes #17 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1023}
