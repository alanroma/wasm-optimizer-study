; ModuleID = 'mediancut.c'
source_filename = "mediancut.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.colormap = type { i32, i8* (i32)*, void (i8*)*, [0 x %struct.colormap_item] }
%struct.colormap_item = type { %struct.f_pixel, float, i8 }
%struct.f_pixel = type { float, float, float, float }
%struct.histogram = type { %struct.hist_item*, void (i8*)*, double, i32, i32 }
%struct.hist_item = type { %struct.f_pixel, float, float, float, %union.anon }
%union.anon = type { i32 }
%struct.box = type { %struct.f_pixel, %struct.f_pixel, double, double, double, i32, i32 }
%struct.channelvariance = type { i32, float }

; Function Attrs: nounwind
define hidden %struct.colormap* @mediancut(%struct.histogram* %hist, i32 %newcolors, double %target_mse, double %max_mse, i8* (i32)* %malloc, void (i8*)* %free) #0 {
entry:
  %hist.addr = alloca %struct.histogram*, align 4
  %newcolors.addr = alloca i32, align 4
  %target_mse.addr = alloca double, align 8
  %max_mse.addr = alloca double, align 8
  %malloc.addr = alloca i8* (i32)*, align 4
  %free.addr = alloca void (i8*)*, align 4
  %achv = alloca %struct.hist_item*, align 4
  %saved_stack = alloca i8*, align 4
  %__vla_expr0 = alloca i32, align 4
  %boxes = alloca i32, align 4
  %sum = alloca double, align 8
  %i = alloca i32, align 4
  %current_max_mse = alloca double, align 8
  %bi = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %indx = alloca i32, align 4
  %clrs = alloca i32, align 4
  %halfvar = alloca double, align 8
  %lowervar = alloca double, align 8
  %break_p = alloca %struct.hist_item*, align 4
  %break_at = alloca i32, align 4
  %sm = alloca double, align 8
  %lowersum = alloca double, align 8
  %i31 = alloca i32, align 4
  %map = alloca %struct.colormap*, align 4
  store %struct.histogram* %hist, %struct.histogram** %hist.addr, align 4, !tbaa !2
  store i32 %newcolors, i32* %newcolors.addr, align 4, !tbaa !6
  store double %target_mse, double* %target_mse.addr, align 8, !tbaa !8
  store double %max_mse, double* %max_mse.addr, align 8, !tbaa !8
  store i8* (i32)* %malloc, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  store void (i8*)* %free, void (i8*)** %free.addr, align 4, !tbaa !2
  %0 = bitcast %struct.hist_item** %achv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv1 = getelementptr inbounds %struct.histogram, %struct.histogram* %1, i32 0, i32 0
  %2 = load %struct.hist_item*, %struct.hist_item** %achv1, align 8, !tbaa !10
  store %struct.hist_item* %2, %struct.hist_item** %achv, align 4, !tbaa !2
  %3 = load i32, i32* %newcolors.addr, align 4, !tbaa !6
  %4 = call i8* @llvm.stacksave()
  store i8* %4, i8** %saved_stack, align 4
  %vla = alloca %struct.box, i32 %3, align 16
  store i32 %3, i32* %__vla_expr0, align 4
  %5 = bitcast i32* %boxes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  store i32 1, i32* %boxes, align 4, !tbaa !6
  %6 = bitcast double* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #2
  store double 0.000000e+00, double* %sum, align 8, !tbaa !8
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.histogram, %struct.histogram* %9, i32 0, i32 3
  %10 = load i32, i32* %size, align 8, !tbaa !12
  %cmp = icmp ult i32 %8, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %12, i32 %13
  %adjusted_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 1
  %14 = load float, float* %adjusted_weight, align 4, !tbaa !13
  %conv = fpext float %14 to double
  %15 = load double, double* %sum, align 8, !tbaa !8
  %add = fadd double %15, %conv
  store double %add, double* %sum, align 8, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %16, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %arrayidx2 = getelementptr inbounds %struct.box, %struct.box* %vla, i32 0
  %17 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %18 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size3 = getelementptr inbounds %struct.histogram, %struct.histogram* %18, i32 0, i32 3
  %19 = load i32, i32* %size3, align 8, !tbaa !12
  %20 = load double, double* %sum, align 8, !tbaa !8
  call void @box_init(%struct.box* %arrayidx2, %struct.hist_item* %17, i32 0, i32 %19, double %20)
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %for.end
  %21 = load i32, i32* %boxes, align 4, !tbaa !6
  %22 = load i32, i32* %newcolors.addr, align 4, !tbaa !6
  %cmp4 = icmp ult i32 %21, %22
  br i1 %cmp4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %23 = bitcast double* %current_max_mse to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %23) #2
  %24 = load double, double* %max_mse.addr, align 8, !tbaa !8
  %25 = load i32, i32* %boxes, align 4, !tbaa !6
  %conv6 = uitofp i32 %25 to double
  %26 = load i32, i32* %newcolors.addr, align 4, !tbaa !6
  %conv7 = uitofp i32 %26 to double
  %div = fdiv double %conv6, %conv7
  %mul = fmul double %div, 1.600000e+01
  %27 = load double, double* %max_mse.addr, align 8, !tbaa !8
  %mul8 = fmul double %mul, %27
  %add9 = fadd double %24, %mul8
  store double %add9, double* %current_max_mse, align 8, !tbaa !8
  %28 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #2
  %29 = load i32, i32* %boxes, align 4, !tbaa !6
  %30 = load double, double* %current_max_mse, align 8, !tbaa !8
  %call = call i32 @best_splittable_box(%struct.box* %vla, i32 %29, double %30)
  store i32 %call, i32* %bi, align 4, !tbaa !6
  %31 = load i32, i32* %bi, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %31, 0
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end:                                           ; preds = %while.body
  %32 = bitcast i32* %indx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #2
  %33 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.box, %struct.box* %vla, i32 %33
  %ind = getelementptr inbounds %struct.box, %struct.box* %arrayidx12, i32 0, i32 5
  %34 = load i32, i32* %ind, align 8, !tbaa !17
  store i32 %34, i32* %indx, align 4, !tbaa !6
  %35 = bitcast i32* %clrs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #2
  %36 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds %struct.box, %struct.box* %vla, i32 %36
  %colors = getelementptr inbounds %struct.box, %struct.box* %arrayidx13, i32 0, i32 6
  %37 = load i32, i32* %colors, align 4, !tbaa !19
  store i32 %37, i32* %clrs, align 4, !tbaa !6
  %38 = bitcast double* %halfvar to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %38) #2
  %39 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds %struct.box, %struct.box* %vla, i32 %39
  %40 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %call15 = call double @prepare_sort(%struct.box* %arrayidx14, %struct.hist_item* %40)
  store double %call15, double* %halfvar, align 8, !tbaa !8
  %41 = bitcast double* %lowervar to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %41) #2
  store double 0.000000e+00, double* %lowervar, align 8, !tbaa !8
  %42 = bitcast %struct.hist_item** %break_p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #2
  %43 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %44 = load i32, i32* %indx, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %43, i32 %44
  %45 = load i32, i32* %clrs, align 4, !tbaa !6
  %46 = load double, double* %halfvar, align 8, !tbaa !8
  %call17 = call %struct.hist_item* @hist_item_sort_halfvar(%struct.hist_item* %arrayidx16, i32 %45, double* %lowervar, double %46)
  store %struct.hist_item* %call17, %struct.hist_item** %break_p, align 4, !tbaa !2
  %47 = bitcast i32* %break_at to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #2
  %48 = load i32, i32* %clrs, align 4, !tbaa !6
  %sub = sub i32 %48, 1
  %49 = load %struct.hist_item*, %struct.hist_item** %break_p, align 4, !tbaa !2
  %50 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %51 = load i32, i32* %indx, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %50, i32 %51
  %sub.ptr.lhs.cast = ptrtoint %struct.hist_item* %49 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.hist_item* %arrayidx18 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 32
  %add19 = add nsw i32 %sub.ptr.div, 1
  %cmp20 = icmp ult i32 %sub, %add19
  br i1 %cmp20, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %52 = load i32, i32* %clrs, align 4, !tbaa !6
  %sub22 = sub i32 %52, 1
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %53 = load %struct.hist_item*, %struct.hist_item** %break_p, align 4, !tbaa !2
  %54 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %55 = load i32, i32* %indx, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %54, i32 %55
  %sub.ptr.lhs.cast24 = ptrtoint %struct.hist_item* %53 to i32
  %sub.ptr.rhs.cast25 = ptrtoint %struct.hist_item* %arrayidx23 to i32
  %sub.ptr.sub26 = sub i32 %sub.ptr.lhs.cast24, %sub.ptr.rhs.cast25
  %sub.ptr.div27 = sdiv exact i32 %sub.ptr.sub26, 32
  %add28 = add nsw i32 %sub.ptr.div27, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub22, %cond.true ], [ %add28, %cond.false ]
  store i32 %cond, i32* %break_at, align 4, !tbaa !6
  %56 = bitcast double* %sm to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %56) #2
  %57 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds %struct.box, %struct.box* %vla, i32 %57
  %sum30 = getelementptr inbounds %struct.box, %struct.box* %arrayidx29, i32 0, i32 2
  %58 = load double, double* %sum30, align 16, !tbaa !20
  store double %58, double* %sm, align 8, !tbaa !8
  %59 = bitcast double* %lowersum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %59) #2
  store double 0.000000e+00, double* %lowersum, align 8, !tbaa !8
  %60 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #2
  store i32 0, i32* %i31, align 4, !tbaa !6
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc42, %cond.end
  %61 = load i32, i32* %i31, align 4, !tbaa !6
  %62 = load i32, i32* %break_at, align 4, !tbaa !6
  %cmp33 = icmp ult i32 %61, %62
  br i1 %cmp33, label %for.body36, label %for.cond.cleanup35

for.cond.cleanup35:                               ; preds = %for.cond32
  store i32 7, i32* %cleanup.dest.slot, align 4
  %63 = bitcast i32* %i31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #2
  br label %for.end44

for.body36:                                       ; preds = %for.cond32
  %64 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %65 = load i32, i32* %indx, align 4, !tbaa !6
  %66 = load i32, i32* %i31, align 4, !tbaa !6
  %add37 = add i32 %65, %66
  %arrayidx38 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %64, i32 %add37
  %adjusted_weight39 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx38, i32 0, i32 1
  %67 = load float, float* %adjusted_weight39, align 4, !tbaa !13
  %conv40 = fpext float %67 to double
  %68 = load double, double* %lowersum, align 8, !tbaa !8
  %add41 = fadd double %68, %conv40
  store double %add41, double* %lowersum, align 8, !tbaa !8
  br label %for.inc42

for.inc42:                                        ; preds = %for.body36
  %69 = load i32, i32* %i31, align 4, !tbaa !6
  %inc43 = add i32 %69, 1
  store i32 %inc43, i32* %i31, align 4, !tbaa !6
  br label %for.cond32

for.end44:                                        ; preds = %for.cond.cleanup35
  %70 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx45 = getelementptr inbounds %struct.box, %struct.box* %vla, i32 %70
  %71 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %72 = load i32, i32* %indx, align 4, !tbaa !6
  %73 = load i32, i32* %break_at, align 4, !tbaa !6
  %74 = load double, double* %lowersum, align 8, !tbaa !8
  call void @box_init(%struct.box* %arrayidx45, %struct.hist_item* %71, i32 %72, i32 %73, double %74)
  %75 = load i32, i32* %boxes, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds %struct.box, %struct.box* %vla, i32 %75
  %76 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %77 = load i32, i32* %indx, align 4, !tbaa !6
  %78 = load i32, i32* %break_at, align 4, !tbaa !6
  %add47 = add i32 %77, %78
  %79 = load i32, i32* %clrs, align 4, !tbaa !6
  %80 = load i32, i32* %break_at, align 4, !tbaa !6
  %sub48 = sub i32 %79, %80
  %81 = load double, double* %sm, align 8, !tbaa !8
  %82 = load double, double* %lowersum, align 8, !tbaa !8
  %sub49 = fsub double %81, %82
  call void @box_init(%struct.box* %arrayidx46, %struct.hist_item* %76, i32 %add47, i32 %sub48, double %sub49)
  %83 = load i32, i32* %boxes, align 4, !tbaa !6
  %inc50 = add i32 %83, 1
  store i32 %inc50, i32* %boxes, align 4, !tbaa !6
  %84 = load double, double* %target_mse.addr, align 8, !tbaa !8
  %85 = load i32, i32* %boxes, align 4, !tbaa !6
  %86 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %call51 = call zeroext i1 @total_box_error_below_target(double %84, %struct.box* %vla, i32 %85, %struct.histogram* %86)
  br i1 %call51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %for.end44
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %for.end44
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end53, %if.then52
  %87 = bitcast double* %lowersum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %87) #2
  %88 = bitcast double* %sm to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %88) #2
  %89 = bitcast i32* %break_at to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #2
  %90 = bitcast %struct.hist_item** %break_p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #2
  %91 = bitcast double* %lowervar to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %91) #2
  %92 = bitcast double* %halfvar to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %92) #2
  %93 = bitcast i32* %clrs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #2
  %94 = bitcast i32* %indx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #2
  br label %cleanup61

cleanup61:                                        ; preds = %cleanup, %if.then
  %95 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #2
  %96 = bitcast double* %current_max_mse to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %96) #2
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 6, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup61
  br label %while.cond

while.end:                                        ; preds = %cleanup61, %while.cond
  %97 = bitcast double* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %97) #2
  %98 = bitcast %struct.colormap** %map to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #2
  %99 = load i32, i32* %boxes, align 4, !tbaa !6
  %100 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  %101 = load void (i8*)*, void (i8*)** %free.addr, align 4, !tbaa !2
  %call63 = call %struct.colormap* @pam_colormap(i32 %99, i8* (i32)* %100, void (i8*)* %101)
  store %struct.colormap* %call63, %struct.colormap** %map, align 4, !tbaa !2
  %102 = load %struct.colormap*, %struct.colormap** %map, align 4, !tbaa !2
  %103 = load i32, i32* %boxes, align 4, !tbaa !6
  %104 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  call void @set_colormap_from_boxes(%struct.colormap* %102, %struct.box* %vla, i32 %103, %struct.hist_item* %104)
  %105 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %106 = load i32, i32* %boxes, align 4, !tbaa !6
  call void @adjust_histogram(%struct.hist_item* %105, %struct.box* %vla, i32 %106)
  %107 = load %struct.colormap*, %struct.colormap** %map, align 4, !tbaa !2
  store i32 1, i32* %cleanup.dest.slot, align 4
  %108 = bitcast %struct.colormap** %map to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #2
  %109 = bitcast i32* %boxes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #2
  %110 = load i8*, i8** %saved_stack, align 4
  call void @llvm.stackrestore(i8* %110)
  %111 = bitcast %struct.hist_item** %achv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #2
  ret %struct.colormap* %107

unreachable:                                      ; preds = %cleanup61
  unreachable
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
declare i8* @llvm.stacksave() #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @box_init(%struct.box* %box, %struct.hist_item* %achv, i32 %ind, i32 %colors, double %sum) #0 {
entry:
  %box.addr = alloca %struct.box*, align 4
  %achv.addr = alloca %struct.hist_item*, align 4
  %ind.addr = alloca i32, align 4
  %colors.addr = alloca i32, align 4
  %sum.addr = alloca double, align 8
  %tmp = alloca %struct.f_pixel, align 4
  %tmp4 = alloca %struct.f_pixel, align 4
  store %struct.box* %box, %struct.box** %box.addr, align 4, !tbaa !2
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  store i32 %ind, i32* %ind.addr, align 4, !tbaa !6
  store i32 %colors, i32* %colors.addr, align 4, !tbaa !6
  store double %sum, double* %sum.addr, align 8, !tbaa !8
  %0 = load i32, i32* %ind.addr, align 4, !tbaa !6
  %1 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %ind1 = getelementptr inbounds %struct.box, %struct.box* %1, i32 0, i32 5
  store i32 %0, i32* %ind1, align 8, !tbaa !17
  %2 = load i32, i32* %colors.addr, align 4, !tbaa !6
  %3 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %colors2 = getelementptr inbounds %struct.box, %struct.box* %3, i32 0, i32 6
  store i32 %2, i32* %colors2, align 4, !tbaa !19
  %4 = load double, double* %sum.addr, align 8, !tbaa !8
  %5 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %sum3 = getelementptr inbounds %struct.box, %struct.box* %5, i32 0, i32 2
  store double %4, double* %sum3, align 8, !tbaa !20
  %6 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %total_error = getelementptr inbounds %struct.box, %struct.box* %6, i32 0, i32 3
  store double -1.000000e+00, double* %total_error, align 8, !tbaa !21
  %7 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %color = getelementptr inbounds %struct.box, %struct.box* %7, i32 0, i32 0
  %8 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #2
  %9 = load i32, i32* %colors.addr, align 4, !tbaa !6
  %10 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %11 = load i32, i32* %ind.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %10, i32 %11
  call void @averagepixels(%struct.f_pixel* sret align 4 %tmp, i32 %9, %struct.hist_item* %arrayidx)
  %12 = bitcast %struct.f_pixel* %color to i8*
  %13 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !22
  %14 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #2
  %15 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %variance = getelementptr inbounds %struct.box, %struct.box* %15, i32 0, i32 1
  %16 = bitcast %struct.f_pixel* %tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #2
  %17 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %18 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  call void @box_variance(%struct.f_pixel* sret align 4 %tmp4, %struct.hist_item* %17, %struct.box* %18)
  %19 = bitcast %struct.f_pixel* %variance to i8*
  %20 = bitcast %struct.f_pixel* %tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %19, i8* align 4 %20, i32 16, i1 false), !tbaa.struct !22
  %21 = bitcast %struct.f_pixel* %tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #2
  %22 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %23 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %call = call double @box_max_error(%struct.hist_item* %22, %struct.box* %23)
  %24 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %max_error = getelementptr inbounds %struct.box, %struct.box* %24, i32 0, i32 4
  store double %call, double* %max_error, align 8, !tbaa !24
  ret void
}

; Function Attrs: nounwind
define internal i32 @best_splittable_box(%struct.box* %bv, i32 %boxes, double %max_mse) #0 {
entry:
  %bv.addr = alloca %struct.box*, align 4
  %boxes.addr = alloca i32, align 4
  %max_mse.addr = alloca double, align 8
  %bi = alloca i32, align 4
  %maxsum = alloca double, align 8
  %i = alloca i32, align 4
  %cv = alloca double, align 8
  %thissum = alloca double, align 8
  store %struct.box* %bv, %struct.box** %bv.addr, align 4, !tbaa !2
  store i32 %boxes, i32* %boxes.addr, align 4, !tbaa !6
  store double %max_mse, double* %max_mse.addr, align 8, !tbaa !8
  %0 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 -1, i32* %bi, align 4, !tbaa !6
  %1 = bitcast double* %maxsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #2
  store double 0.000000e+00, double* %maxsum, align 8, !tbaa !8
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %4 = load i32, i32* %boxes.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %3, %4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.box, %struct.box* %6, i32 %7
  %colors = getelementptr inbounds %struct.box, %struct.box* %arrayidx, i32 0, i32 6
  %8 = load i32, i32* %colors, align 4, !tbaa !19
  %cmp1 = icmp ult i32 %8, 2
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %9 = bitcast double* %cv to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %9) #2
  %10 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.box, %struct.box* %10, i32 %11
  %variance = getelementptr inbounds %struct.box, %struct.box* %arrayidx2, i32 0, i32 1
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance, i32 0, i32 1
  %12 = load float, float* %r, align 4, !tbaa !25
  %13 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.box, %struct.box* %13, i32 %14
  %variance4 = getelementptr inbounds %struct.box, %struct.box* %arrayidx3, i32 0, i32 1
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance4, i32 0, i32 2
  %15 = load float, float* %g, align 8, !tbaa !26
  %16 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %struct.box, %struct.box* %16, i32 %17
  %variance6 = getelementptr inbounds %struct.box, %struct.box* %arrayidx5, i32 0, i32 1
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance6, i32 0, i32 3
  %18 = load float, float* %b, align 4, !tbaa !27
  %cmp7 = fcmp ogt float %15, %18
  br i1 %cmp7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %19 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds %struct.box, %struct.box* %19, i32 %20
  %variance9 = getelementptr inbounds %struct.box, %struct.box* %arrayidx8, i32 0, i32 1
  %g10 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance9, i32 0, i32 2
  %21 = load float, float* %g10, align 8, !tbaa !26
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %22 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds %struct.box, %struct.box* %22, i32 %23
  %variance12 = getelementptr inbounds %struct.box, %struct.box* %arrayidx11, i32 0, i32 1
  %b13 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance12, i32 0, i32 3
  %24 = load float, float* %b13, align 4, !tbaa !27
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %21, %cond.true ], [ %24, %cond.false ]
  %cmp14 = fcmp ogt float %12, %cond
  br i1 %cmp14, label %cond.true15, label %cond.false19

cond.true15:                                      ; preds = %cond.end
  %25 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds %struct.box, %struct.box* %25, i32 %26
  %variance17 = getelementptr inbounds %struct.box, %struct.box* %arrayidx16, i32 0, i32 1
  %r18 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance17, i32 0, i32 1
  %27 = load float, float* %r18, align 4, !tbaa !25
  br label %cond.end37

cond.false19:                                     ; preds = %cond.end
  %28 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds %struct.box, %struct.box* %28, i32 %29
  %variance21 = getelementptr inbounds %struct.box, %struct.box* %arrayidx20, i32 0, i32 1
  %g22 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance21, i32 0, i32 2
  %30 = load float, float* %g22, align 8, !tbaa !26
  %31 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds %struct.box, %struct.box* %31, i32 %32
  %variance24 = getelementptr inbounds %struct.box, %struct.box* %arrayidx23, i32 0, i32 1
  %b25 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance24, i32 0, i32 3
  %33 = load float, float* %b25, align 4, !tbaa !27
  %cmp26 = fcmp ogt float %30, %33
  br i1 %cmp26, label %cond.true27, label %cond.false31

cond.true27:                                      ; preds = %cond.false19
  %34 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %35 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds %struct.box, %struct.box* %34, i32 %35
  %variance29 = getelementptr inbounds %struct.box, %struct.box* %arrayidx28, i32 0, i32 1
  %g30 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance29, i32 0, i32 2
  %36 = load float, float* %g30, align 8, !tbaa !26
  br label %cond.end35

cond.false31:                                     ; preds = %cond.false19
  %37 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds %struct.box, %struct.box* %37, i32 %38
  %variance33 = getelementptr inbounds %struct.box, %struct.box* %arrayidx32, i32 0, i32 1
  %b34 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance33, i32 0, i32 3
  %39 = load float, float* %b34, align 4, !tbaa !27
  br label %cond.end35

cond.end35:                                       ; preds = %cond.false31, %cond.true27
  %cond36 = phi float [ %36, %cond.true27 ], [ %39, %cond.false31 ]
  br label %cond.end37

cond.end37:                                       ; preds = %cond.end35, %cond.true15
  %cond38 = phi float [ %27, %cond.true15 ], [ %cond36, %cond.end35 ]
  %conv = fpext float %cond38 to double
  store double %conv, double* %cv, align 8, !tbaa !8
  %40 = bitcast double* %thissum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %40) #2
  %41 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %42 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds %struct.box, %struct.box* %41, i32 %42
  %sum = getelementptr inbounds %struct.box, %struct.box* %arrayidx39, i32 0, i32 2
  %43 = load double, double* %sum, align 8, !tbaa !20
  %44 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds %struct.box, %struct.box* %44, i32 %45
  %variance41 = getelementptr inbounds %struct.box, %struct.box* %arrayidx40, i32 0, i32 1
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance41, i32 0, i32 0
  %46 = load float, float* %a, align 8, !tbaa !28
  %conv42 = fpext float %46 to double
  %47 = load double, double* %cv, align 8, !tbaa !8
  %cmp43 = fcmp ogt double %conv42, %47
  br i1 %cmp43, label %cond.true45, label %cond.false50

cond.true45:                                      ; preds = %cond.end37
  %48 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx46 = getelementptr inbounds %struct.box, %struct.box* %48, i32 %49
  %variance47 = getelementptr inbounds %struct.box, %struct.box* %arrayidx46, i32 0, i32 1
  %a48 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance47, i32 0, i32 0
  %50 = load float, float* %a48, align 8, !tbaa !28
  %conv49 = fpext float %50 to double
  br label %cond.end51

cond.false50:                                     ; preds = %cond.end37
  %51 = load double, double* %cv, align 8, !tbaa !8
  br label %cond.end51

cond.end51:                                       ; preds = %cond.false50, %cond.true45
  %cond52 = phi double [ %conv49, %cond.true45 ], [ %51, %cond.false50 ]
  %mul = fmul double %43, %cond52
  store double %mul, double* %thissum, align 8, !tbaa !8
  %52 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx53 = getelementptr inbounds %struct.box, %struct.box* %52, i32 %53
  %max_error = getelementptr inbounds %struct.box, %struct.box* %arrayidx53, i32 0, i32 4
  %54 = load double, double* %max_error, align 8, !tbaa !24
  %55 = load double, double* %max_mse.addr, align 8, !tbaa !8
  %cmp54 = fcmp ogt double %54, %55
  br i1 %cmp54, label %if.then56, label %if.end60

if.then56:                                        ; preds = %cond.end51
  %56 = load double, double* %thissum, align 8, !tbaa !8
  %57 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx57 = getelementptr inbounds %struct.box, %struct.box* %57, i32 %58
  %max_error58 = getelementptr inbounds %struct.box, %struct.box* %arrayidx57, i32 0, i32 4
  %59 = load double, double* %max_error58, align 8, !tbaa !24
  %mul59 = fmul double %56, %59
  %60 = load double, double* %max_mse.addr, align 8, !tbaa !8
  %div = fdiv double %mul59, %60
  store double %div, double* %thissum, align 8, !tbaa !8
  br label %if.end60

if.end60:                                         ; preds = %if.then56, %cond.end51
  %61 = load double, double* %thissum, align 8, !tbaa !8
  %62 = load double, double* %maxsum, align 8, !tbaa !8
  %cmp61 = fcmp ogt double %61, %62
  br i1 %cmp61, label %if.then63, label %if.end64

if.then63:                                        ; preds = %if.end60
  %63 = load double, double* %thissum, align 8, !tbaa !8
  store double %63, double* %maxsum, align 8, !tbaa !8
  %64 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %64, i32* %bi, align 4, !tbaa !6
  br label %if.end64

if.end64:                                         ; preds = %if.then63, %if.end60
  %65 = bitcast double* %thissum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %65) #2
  %66 = bitcast double* %cv to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %66) #2
  br label %for.inc

for.inc:                                          ; preds = %if.end64, %if.then
  %67 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %67, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %68 = load i32, i32* %bi, align 4, !tbaa !6
  %69 = bitcast double* %maxsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %69) #2
  %70 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #2
  ret i32 %68
}

; Function Attrs: nounwind
define internal double @prepare_sort(%struct.box* %b, %struct.hist_item* %achv) #0 {
entry:
  %b.addr = alloca %struct.box*, align 4
  %achv.addr = alloca %struct.hist_item*, align 4
  %channels = alloca [4 x %struct.channelvariance], align 16
  %ind1 = alloca i32, align 4
  %colors = alloca i32, align 4
  %i = alloca i32, align 4
  %chans = alloca float*, align 4
  %median = alloca %struct.f_pixel, align 4
  %ind38 = alloca i32, align 4
  %end = alloca i32, align 4
  %totalvar = alloca double, align 8
  %j = alloca i32, align 4
  store %struct.box* %b, %struct.box** %b.addr, align 4, !tbaa !2
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %0 = bitcast [4 x %struct.channelvariance]* %channels to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %0) #2
  %arrayinit.begin = getelementptr inbounds [4 x %struct.channelvariance], [4 x %struct.channelvariance]* %channels, i32 0, i32 0
  %chan = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.begin, i32 0, i32 0
  store i32 0, i32* %chan, align 8, !tbaa !29
  %variance = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.begin, i32 0, i32 1
  %1 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %variance1 = getelementptr inbounds %struct.box, %struct.box* %1, i32 0, i32 1
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance1, i32 0, i32 0
  %2 = load float, float* %a, align 8, !tbaa !28
  store float %2, float* %variance, align 4, !tbaa !31
  %arrayinit.element = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.begin, i32 1
  %chan2 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.element, i32 0, i32 0
  store i32 1, i32* %chan2, align 8, !tbaa !29
  %variance3 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.element, i32 0, i32 1
  %3 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %variance4 = getelementptr inbounds %struct.box, %struct.box* %3, i32 0, i32 1
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance4, i32 0, i32 1
  %4 = load float, float* %r, align 4, !tbaa !25
  store float %4, float* %variance3, align 4, !tbaa !31
  %arrayinit.element5 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.element, i32 1
  %chan6 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.element5, i32 0, i32 0
  store i32 2, i32* %chan6, align 8, !tbaa !29
  %variance7 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.element5, i32 0, i32 1
  %5 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %variance8 = getelementptr inbounds %struct.box, %struct.box* %5, i32 0, i32 1
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance8, i32 0, i32 2
  %6 = load float, float* %g, align 8, !tbaa !26
  store float %6, float* %variance7, align 4, !tbaa !31
  %arrayinit.element9 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.element5, i32 1
  %chan10 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.element9, i32 0, i32 0
  store i32 3, i32* %chan10, align 8, !tbaa !29
  %variance11 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayinit.element9, i32 0, i32 1
  %7 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %variance12 = getelementptr inbounds %struct.box, %struct.box* %7, i32 0, i32 1
  %b13 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %variance12, i32 0, i32 3
  %8 = load float, float* %b13, align 4, !tbaa !27
  store float %8, float* %variance11, align 4, !tbaa !31
  %arraydecay = getelementptr inbounds [4 x %struct.channelvariance], [4 x %struct.channelvariance]* %channels, i32 0, i32 0
  %9 = bitcast %struct.channelvariance* %arraydecay to i8*
  call void @qsort(i8* %9, i32 4, i32 8, i32 (i8*, i8*)* @comparevariance)
  %10 = bitcast i32* %ind1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %ind = getelementptr inbounds %struct.box, %struct.box* %11, i32 0, i32 5
  %12 = load i32, i32* %ind, align 8, !tbaa !17
  store i32 %12, i32* %ind1, align 4, !tbaa !6
  %13 = bitcast i32* %colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %colors14 = getelementptr inbounds %struct.box, %struct.box* %14, i32 0, i32 6
  %15 = load i32, i32* %colors14, align 4, !tbaa !19
  store i32 %15, i32* %colors, align 4, !tbaa !6
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load i32, i32* %colors, align 4, !tbaa !6
  %cmp = icmp ult i32 %17, %18
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  %20 = bitcast float** %chans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %22 = load i32, i32* %ind1, align 4, !tbaa !6
  %23 = load i32, i32* %i, align 4, !tbaa !6
  %add = add i32 %22, %23
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %21, i32 %add
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 0
  %24 = bitcast %struct.f_pixel* %acolor to float*
  store float* %24, float** %chans, align 4, !tbaa !2
  %25 = load float*, float** %chans, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds [4 x %struct.channelvariance], [4 x %struct.channelvariance]* %channels, i32 0, i32 0
  %chan16 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayidx15, i32 0, i32 0
  %26 = load i32, i32* %chan16, align 16, !tbaa !29
  %arrayidx17 = getelementptr inbounds float, float* %25, i32 %26
  %27 = load float, float* %arrayidx17, align 4, !tbaa !23
  %conv = fpext float %27 to double
  %mul = fmul double %conv, 6.553500e+04
  %conv18 = fptoui double %mul to i32
  %shl = shl i32 %conv18, 16
  %28 = load float*, float** %chans, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds [4 x %struct.channelvariance], [4 x %struct.channelvariance]* %channels, i32 0, i32 2
  %chan20 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayidx19, i32 0, i32 0
  %29 = load i32, i32* %chan20, align 16, !tbaa !29
  %arrayidx21 = getelementptr inbounds float, float* %28, i32 %29
  %30 = load float, float* %arrayidx21, align 4, !tbaa !23
  %conv22 = fpext float %30 to double
  %31 = load float*, float** %chans, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds [4 x %struct.channelvariance], [4 x %struct.channelvariance]* %channels, i32 0, i32 1
  %chan24 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayidx23, i32 0, i32 0
  %32 = load i32, i32* %chan24, align 8, !tbaa !29
  %arrayidx25 = getelementptr inbounds float, float* %31, i32 %32
  %33 = load float, float* %arrayidx25, align 4, !tbaa !23
  %conv26 = fpext float %33 to double
  %div = fdiv double %conv26, 2.000000e+00
  %add27 = fadd double %conv22, %div
  %34 = load float*, float** %chans, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds [4 x %struct.channelvariance], [4 x %struct.channelvariance]* %channels, i32 0, i32 3
  %chan29 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %arrayidx28, i32 0, i32 0
  %35 = load i32, i32* %chan29, align 8, !tbaa !29
  %arrayidx30 = getelementptr inbounds float, float* %34, i32 %35
  %36 = load float, float* %arrayidx30, align 4, !tbaa !23
  %conv31 = fpext float %36 to double
  %div32 = fdiv double %conv31, 4.000000e+00
  %add33 = fadd double %add27, %div32
  %mul34 = fmul double %add33, 6.553500e+04
  %conv35 = fptoui double %mul34 to i32
  %or = or i32 %shl, %conv35
  %37 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %38 = load i32, i32* %ind1, align 4, !tbaa !6
  %39 = load i32, i32* %i, align 4, !tbaa !6
  %add36 = add i32 %38, %39
  %arrayidx37 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %37, i32 %add36
  %tmp = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx37, i32 0, i32 4
  %sort_value = bitcast %union.anon* %tmp to i32*
  store i32 %or, i32* %sort_value, align 4, !tbaa !32
  %40 = bitcast float** %chans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %41, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %42 = bitcast %struct.f_pixel* %median to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #2
  %43 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %44 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  call void @get_median(%struct.f_pixel* sret align 4 %median, %struct.box* %43, %struct.hist_item* %44)
  %45 = bitcast i32* %ind38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #2
  %46 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %ind39 = getelementptr inbounds %struct.box, %struct.box* %46, i32 0, i32 5
  %47 = load i32, i32* %ind39, align 8, !tbaa !17
  store i32 %47, i32* %ind38, align 4, !tbaa !6
  %48 = bitcast i32* %end to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #2
  %49 = load i32, i32* %ind38, align 4, !tbaa !6
  %50 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %colors40 = getelementptr inbounds %struct.box, %struct.box* %50, i32 0, i32 6
  %51 = load i32, i32* %colors40, align 4, !tbaa !19
  %add41 = add i32 %49, %51
  store i32 %add41, i32* %end, align 4, !tbaa !6
  %52 = bitcast double* %totalvar to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %52) #2
  store double 0.000000e+00, double* %totalvar, align 8, !tbaa !8
  %53 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #2
  %54 = load i32, i32* %ind38, align 4, !tbaa !6
  store i32 %54, i32* %j, align 4, !tbaa !6
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc52, %for.end
  %55 = load i32, i32* %j, align 4, !tbaa !6
  %56 = load i32, i32* %end, align 4, !tbaa !6
  %cmp43 = icmp ult i32 %55, %56
  br i1 %cmp43, label %for.body46, label %for.cond.cleanup45

for.cond.cleanup45:                               ; preds = %for.cond42
  %57 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #2
  br label %for.end54

for.body46:                                       ; preds = %for.cond42
  %58 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %59 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx47 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %58, i32 %59
  %call = call double @color_weight(%struct.f_pixel* byval(%struct.f_pixel) align 4 %median, %struct.hist_item* byval(%struct.hist_item) align 4 %arrayidx47)
  %conv48 = fptrunc double %call to float
  %60 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %61 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx49 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %60, i32 %61
  %color_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx49, i32 0, i32 3
  store float %conv48, float* %color_weight, align 4, !tbaa !33
  %conv50 = fpext float %conv48 to double
  %62 = load double, double* %totalvar, align 8, !tbaa !8
  %add51 = fadd double %62, %conv50
  store double %add51, double* %totalvar, align 8, !tbaa !8
  br label %for.inc52

for.inc52:                                        ; preds = %for.body46
  %63 = load i32, i32* %j, align 4, !tbaa !6
  %inc53 = add i32 %63, 1
  store i32 %inc53, i32* %j, align 4, !tbaa !6
  br label %for.cond42

for.end54:                                        ; preds = %for.cond.cleanup45
  %64 = load double, double* %totalvar, align 8, !tbaa !8
  %div55 = fdiv double %64, 2.000000e+00
  %65 = bitcast double* %totalvar to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %65) #2
  %66 = bitcast i32* %end to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #2
  %67 = bitcast i32* %ind38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #2
  %68 = bitcast %struct.f_pixel* %median to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #2
  %69 = bitcast i32* %colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #2
  %70 = bitcast i32* %ind1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #2
  %71 = bitcast [4 x %struct.channelvariance]* %channels to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %71) #2
  ret double %div55
}

; Function Attrs: nounwind
define internal %struct.hist_item* @hist_item_sort_halfvar(%struct.hist_item* %base, i32 %len, double* %lowervar, double %halfvar) #0 {
entry:
  %retval = alloca %struct.hist_item*, align 4
  %base.addr = alloca %struct.hist_item*, align 4
  %len.addr = alloca i32, align 4
  %lowervar.addr = alloca double*, align 4
  %halfvar.addr = alloca double, align 8
  %l = alloca i32, align 4
  %r = alloca i32, align 4
  %t = alloca i32, align 4
  %tmpsum = alloca double, align 8
  %res = alloca %struct.hist_item*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.hist_item* %base, %struct.hist_item** %base.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !6
  store double* %lowervar, double** %lowervar.addr, align 4, !tbaa !2
  store double %halfvar, double* %halfvar.addr, align 8, !tbaa !8
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %2 = load i32, i32* %len.addr, align 4, !tbaa !6
  %call = call i32 @qsort_partition(%struct.hist_item* %1, i32 %2)
  store i32 %call, i32* %l, align 4, !tbaa !6
  %3 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = load i32, i32* %l, align 4, !tbaa !6
  %add = add i32 %4, 1
  store i32 %add, i32* %r, align 4, !tbaa !6
  %5 = bitcast i32* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  store i32 0, i32* %t, align 4, !tbaa !6
  %6 = bitcast double* %tmpsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #2
  %7 = load double*, double** %lowervar.addr, align 4, !tbaa !2
  %8 = load double, double* %7, align 8, !tbaa !8
  store double %8, double* %tmpsum, align 8, !tbaa !8
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %9 = load i32, i32* %t, align 4, !tbaa !6
  %10 = load i32, i32* %l, align 4, !tbaa !6
  %cmp = icmp ule i32 %9, %10
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %11 = load double, double* %tmpsum, align 8, !tbaa !8
  %12 = load double, double* %halfvar.addr, align 8, !tbaa !8
  %cmp1 = fcmp olt double %11, %12
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %13 = phi i1 [ false, %while.cond ], [ %cmp1, %land.rhs ]
  br i1 %13, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %14 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %15 = load i32, i32* %t, align 4, !tbaa !6
  %inc = add i32 %15, 1
  store i32 %inc, i32* %t, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %14, i32 %15
  %color_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 3
  %16 = load float, float* %color_weight, align 4, !tbaa !33
  %conv = fpext float %16 to double
  %17 = load double, double* %tmpsum, align 8, !tbaa !8
  %add2 = fadd double %17, %conv
  store double %add2, double* %tmpsum, align 8, !tbaa !8
  br label %while.cond

while.end:                                        ; preds = %land.end
  %18 = load double, double* %tmpsum, align 8, !tbaa !8
  %19 = load double, double* %halfvar.addr, align 8, !tbaa !8
  %cmp3 = fcmp olt double %18, %19
  br i1 %cmp3, label %if.then, label %if.else

if.then:                                          ; preds = %while.end
  %20 = load double, double* %tmpsum, align 8, !tbaa !8
  %21 = load double*, double** %lowervar.addr, align 4, !tbaa !2
  store double %20, double* %21, align 8, !tbaa !8
  br label %if.end21

if.else:                                          ; preds = %while.end
  %22 = load i32, i32* %l, align 4, !tbaa !6
  %cmp5 = icmp ugt i32 %22, 0
  br i1 %cmp5, label %if.then7, label %if.else10

if.then7:                                         ; preds = %if.else
  %23 = bitcast %struct.hist_item** %res to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #2
  %24 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %25 = load i32, i32* %l, align 4, !tbaa !6
  %26 = load double*, double** %lowervar.addr, align 4, !tbaa !2
  %27 = load double, double* %halfvar.addr, align 8, !tbaa !8
  %call8 = call %struct.hist_item* @hist_item_sort_halfvar(%struct.hist_item* %24, i32 %25, double* %26, double %27)
  store %struct.hist_item* %call8, %struct.hist_item** %res, align 4, !tbaa !2
  %28 = load %struct.hist_item*, %struct.hist_item** %res, align 4, !tbaa !2
  %tobool = icmp ne %struct.hist_item* %28, null
  br i1 %tobool, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.then7
  %29 = load %struct.hist_item*, %struct.hist_item** %res, align 4, !tbaa !2
  store %struct.hist_item* %29, %struct.hist_item** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then7
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then9
  %30 = bitcast %struct.hist_item** %res to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #2
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup34 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end20

if.else10:                                        ; preds = %if.else
  %31 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %31, i32 0
  %color_weight12 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx11, i32 0, i32 3
  %32 = load float, float* %color_weight12, align 4, !tbaa !33
  %conv13 = fpext float %32 to double
  %33 = load double*, double** %lowervar.addr, align 4, !tbaa !2
  %34 = load double, double* %33, align 8, !tbaa !8
  %add14 = fadd double %34, %conv13
  store double %add14, double* %33, align 8, !tbaa !8
  %35 = load double*, double** %lowervar.addr, align 4, !tbaa !2
  %36 = load double, double* %35, align 8, !tbaa !8
  %37 = load double, double* %halfvar.addr, align 8, !tbaa !8
  %cmp15 = fcmp ogt double %36, %37
  br i1 %cmp15, label %if.then17, label %if.end19

if.then17:                                        ; preds = %if.else10
  %38 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %38, i32 0
  store %struct.hist_item* %arrayidx18, %struct.hist_item** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

if.end19:                                         ; preds = %if.else10
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %cleanup.cont
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.then
  %39 = load i32, i32* %len.addr, align 4, !tbaa !6
  %40 = load i32, i32* %r, align 4, !tbaa !6
  %cmp22 = icmp ugt i32 %39, %40
  br i1 %cmp22, label %if.then24, label %if.else25

if.then24:                                        ; preds = %if.end21
  %41 = load i32, i32* %r, align 4, !tbaa !6
  %42 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.hist_item, %struct.hist_item* %42, i32 %41
  store %struct.hist_item* %add.ptr, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %43 = load i32, i32* %r, align 4, !tbaa !6
  %44 = load i32, i32* %len.addr, align 4, !tbaa !6
  %sub = sub i32 %44, %43
  store i32 %sub, i32* %len.addr, align 4, !tbaa !6
  br label %if.end33

if.else25:                                        ; preds = %if.end21
  %45 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %46 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %45, i32 %46
  %color_weight27 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx26, i32 0, i32 3
  %47 = load float, float* %color_weight27, align 4, !tbaa !33
  %conv28 = fpext float %47 to double
  %48 = load double*, double** %lowervar.addr, align 4, !tbaa !2
  %49 = load double, double* %48, align 8, !tbaa !8
  %add29 = fadd double %49, %conv28
  store double %add29, double* %48, align 8, !tbaa !8
  %50 = load double*, double** %lowervar.addr, align 4, !tbaa !2
  %51 = load double, double* %50, align 8, !tbaa !8
  %52 = load double, double* %halfvar.addr, align 8, !tbaa !8
  %cmp30 = fcmp ogt double %51, %52
  br i1 %cmp30, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else25
  %53 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %54 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx32 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %53, i32 %54
  br label %cond.end

cond.false:                                       ; preds = %if.else25
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.hist_item* [ %arrayidx32, %cond.true ], [ null, %cond.false ]
  store %struct.hist_item* %cond, %struct.hist_item** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

if.end33:                                         ; preds = %if.then24
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup34

cleanup34:                                        ; preds = %if.end33, %cond.end, %if.then17, %cleanup
  %55 = bitcast double* %tmpsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %55) #2
  %56 = bitcast i32* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #2
  %57 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #2
  %58 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #2
  %cleanup.dest38 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest38, label %unreachable [
    i32 0, label %cleanup.cont39
    i32 1, label %do.end
  ]

cleanup.cont39:                                   ; preds = %cleanup34
  br label %do.cond

do.cond:                                          ; preds = %cleanup.cont39
  br i1 true, label %do.body, label %do.end

do.end:                                           ; preds = %cleanup34, %do.cond
  %59 = load %struct.hist_item*, %struct.hist_item** %retval, align 4
  ret %struct.hist_item* %59

unreachable:                                      ; preds = %cleanup34
  unreachable
}

; Function Attrs: nounwind
define internal zeroext i1 @total_box_error_below_target(double %target_mse, %struct.box* %bv, i32 %boxes, %struct.histogram* %hist) #0 {
entry:
  %retval = alloca i1, align 1
  %target_mse.addr = alloca double, align 8
  %bv.addr = alloca %struct.box*, align 4
  %boxes.addr = alloca i32, align 4
  %hist.addr = alloca %struct.histogram*, align 4
  %total_error = alloca double, align 8
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i8 = alloca i32, align 4
  store double %target_mse, double* %target_mse.addr, align 8, !tbaa !8
  store %struct.box* %bv, %struct.box** %bv.addr, align 4, !tbaa !2
  store i32 %boxes, i32* %boxes.addr, align 4, !tbaa !6
  store %struct.histogram* %hist, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %0 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %total_perceptual_weight = getelementptr inbounds %struct.histogram, %struct.histogram* %0, i32 0, i32 2
  %1 = load double, double* %total_perceptual_weight, align 8, !tbaa !34
  %2 = load double, double* %target_mse.addr, align 8, !tbaa !8
  %mul = fmul double %2, %1
  store double %mul, double* %target_mse.addr, align 8, !tbaa !8
  %3 = bitcast double* %total_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #2
  store double 0.000000e+00, double* %total_error, align 8, !tbaa !8
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %boxes.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %5, %6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %7 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.box, %struct.box* %7, i32 %8
  %total_error1 = getelementptr inbounds %struct.box, %struct.box* %arrayidx, i32 0, i32 3
  %9 = load double, double* %total_error1, align 8, !tbaa !21
  %cmp2 = fcmp oge double %9, 0.000000e+00
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.box, %struct.box* %10, i32 %11
  %total_error4 = getelementptr inbounds %struct.box, %struct.box* %arrayidx3, i32 0, i32 3
  %12 = load double, double* %total_error4, align 8, !tbaa !21
  %13 = load double, double* %total_error, align 8, !tbaa !8
  %add = fadd double %13, %12
  store double %add, double* %total_error, align 8, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %14 = load double, double* %total_error, align 8, !tbaa !8
  %15 = load double, double* %target_mse.addr, align 8, !tbaa !8
  %cmp5 = fcmp ogt double %14, %15
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end7
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %16, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup:                                          ; preds = %if.then6, %for.cond.cleanup
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #2
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup32 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  %18 = bitcast i32* %i8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  store i32 0, i32* %i8, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc27, %for.end
  %19 = load i32, i32* %i8, align 4, !tbaa !6
  %20 = load i32, i32* %boxes.addr, align 4, !tbaa !6
  %cmp10 = icmp ult i32 %19, %20
  br i1 %cmp10, label %for.body12, label %for.cond.cleanup11

for.cond.cleanup11:                               ; preds = %for.cond9
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

for.body12:                                       ; preds = %for.cond9
  %21 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %22 = load i32, i32* %i8, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds %struct.box, %struct.box* %21, i32 %22
  %total_error14 = getelementptr inbounds %struct.box, %struct.box* %arrayidx13, i32 0, i32 3
  %23 = load double, double* %total_error14, align 8, !tbaa !21
  %cmp15 = fcmp olt double %23, 0.000000e+00
  br i1 %cmp15, label %if.then16, label %if.end23

if.then16:                                        ; preds = %for.body12
  %24 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i8, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds %struct.box, %struct.box* %24, i32 %25
  %26 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv = getelementptr inbounds %struct.histogram, %struct.histogram* %26, i32 0, i32 0
  %27 = load %struct.hist_item*, %struct.hist_item** %achv, align 8, !tbaa !10
  %call = call double @box_error(%struct.box* %arrayidx17, %struct.hist_item* %27)
  %28 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %29 = load i32, i32* %i8, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds %struct.box, %struct.box* %28, i32 %29
  %total_error19 = getelementptr inbounds %struct.box, %struct.box* %arrayidx18, i32 0, i32 3
  store double %call, double* %total_error19, align 8, !tbaa !21
  %30 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %31 = load i32, i32* %i8, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds %struct.box, %struct.box* %30, i32 %31
  %total_error21 = getelementptr inbounds %struct.box, %struct.box* %arrayidx20, i32 0, i32 3
  %32 = load double, double* %total_error21, align 8, !tbaa !21
  %33 = load double, double* %total_error, align 8, !tbaa !8
  %add22 = fadd double %33, %32
  store double %add22, double* %total_error, align 8, !tbaa !8
  br label %if.end23

if.end23:                                         ; preds = %if.then16, %for.body12
  %34 = load double, double* %total_error, align 8, !tbaa !8
  %35 = load double, double* %target_mse.addr, align 8, !tbaa !8
  %cmp24 = fcmp ogt double %34, %35
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end23
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup29

if.end26:                                         ; preds = %if.end23
  br label %for.inc27

for.inc27:                                        ; preds = %if.end26
  %36 = load i32, i32* %i8, align 4, !tbaa !6
  %inc28 = add i32 %36, 1
  store i32 %inc28, i32* %i8, align 4, !tbaa !6
  br label %for.cond9

cleanup29:                                        ; preds = %if.then25, %for.cond.cleanup11
  %37 = bitcast i32* %i8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #2
  %cleanup.dest30 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest30, label %cleanup32 [
    i32 5, label %for.end31
  ]

for.end31:                                        ; preds = %cleanup29
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup32

cleanup32:                                        ; preds = %for.end31, %cleanup29, %cleanup
  %38 = bitcast double* %total_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %38) #2
  %39 = load i1, i1* %retval, align 1
  ret i1 %39
}

declare hidden %struct.colormap* @pam_colormap(i32, i8* (i32)*, void (i8*)*) #3

; Function Attrs: nounwind
define internal void @set_colormap_from_boxes(%struct.colormap* %map, %struct.box* %bv, i32 %boxes, %struct.hist_item* %achv) #0 {
entry:
  %map.addr = alloca %struct.colormap*, align 4
  %bv.addr = alloca %struct.box*, align 4
  %boxes.addr = alloca i32, align 4
  %achv.addr = alloca %struct.hist_item*, align 4
  %bi = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  store %struct.box* %bv, %struct.box** %bv.addr, align 4, !tbaa !2
  store i32 %boxes, i32* %boxes.addr, align 4, !tbaa !6
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %0 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %entry
  %1 = load i32, i32* %bi, align 4, !tbaa !6
  %2 = load i32, i32* %boxes.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #2
  br label %for.end19

for.body:                                         ; preds = %for.cond
  %4 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %4, i32 0, i32 3
  %5 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 %5
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx, i32 0, i32 0
  %6 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %7 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds %struct.box, %struct.box* %6, i32 %7
  %color = getelementptr inbounds %struct.box, %struct.box* %arrayidx1, i32 0, i32 0
  %8 = bitcast %struct.f_pixel* %acolor to i8*
  %9 = bitcast %struct.f_pixel* %color to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 8 %9, i32 16, i1 false), !tbaa.struct !22
  %10 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette2 = getelementptr inbounds %struct.colormap, %struct.colormap* %10, i32 0, i32 3
  %11 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette2, i32 0, i32 %11
  %popularity = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx3, i32 0, i32 1
  store float 0.000000e+00, float* %popularity, align 4, !tbaa !35
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %14 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds %struct.box, %struct.box* %13, i32 %14
  %ind = getelementptr inbounds %struct.box, %struct.box* %arrayidx4, i32 0, i32 5
  %15 = load i32, i32* %ind, align 8, !tbaa !17
  store i32 %15, i32* %i, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %17 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %18 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds %struct.box, %struct.box* %17, i32 %18
  %ind7 = getelementptr inbounds %struct.box, %struct.box* %arrayidx6, i32 0, i32 5
  %19 = load i32, i32* %ind7, align 8, !tbaa !17
  %20 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %21 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds %struct.box, %struct.box* %20, i32 %21
  %colors = getelementptr inbounds %struct.box, %struct.box* %arrayidx8, i32 0, i32 6
  %22 = load i32, i32* %colors, align 4, !tbaa !19
  %add = add i32 %19, %22
  %cmp9 = icmp ult i32 %16, %add
  br i1 %cmp9, label %for.body11, label %for.cond.cleanup10

for.cond.cleanup10:                               ; preds = %for.cond5
  store i32 5, i32* %cleanup.dest.slot, align 4
  %23 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #2
  br label %for.end

for.body11:                                       ; preds = %for.cond5
  %24 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %24, i32 %25
  %perceptual_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx12, i32 0, i32 2
  %26 = load float, float* %perceptual_weight, align 4, !tbaa !38
  %27 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette13 = getelementptr inbounds %struct.colormap, %struct.colormap* %27, i32 0, i32 3
  %28 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette13, i32 0, i32 %28
  %popularity15 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx14, i32 0, i32 1
  %29 = load float, float* %popularity15, align 4, !tbaa !35
  %add16 = fadd float %29, %26
  store float %add16, float* %popularity15, align 4, !tbaa !35
  br label %for.inc

for.inc:                                          ; preds = %for.body11
  %30 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond5

for.end:                                          ; preds = %for.cond.cleanup10
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %31 = load i32, i32* %bi, align 4, !tbaa !6
  %inc18 = add i32 %31, 1
  store i32 %inc18, i32* %bi, align 4, !tbaa !6
  br label %for.cond

for.end19:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define internal void @adjust_histogram(%struct.hist_item* %achv, %struct.box* %bv, i32 %boxes) #0 {
entry:
  %achv.addr = alloca %struct.hist_item*, align 4
  %bv.addr = alloca %struct.box*, align 4
  %boxes.addr = alloca i32, align 4
  %bi = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  store %struct.box* %bv, %struct.box** %bv.addr, align 4, !tbaa !2
  store i32 %boxes, i32* %boxes.addr, align 4, !tbaa !6
  %0 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %entry
  %1 = load i32, i32* %bi, align 4, !tbaa !6
  %2 = load i32, i32* %boxes.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %3 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #2
  br label %for.end11

for.body:                                         ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %6 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.box, %struct.box* %5, i32 %6
  %ind = getelementptr inbounds %struct.box, %struct.box* %arrayidx, i32 0, i32 5
  %7 = load i32, i32* %ind, align 8, !tbaa !17
  store i32 %7, i32* %i, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %10 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.box, %struct.box* %9, i32 %10
  %ind3 = getelementptr inbounds %struct.box, %struct.box* %arrayidx2, i32 0, i32 5
  %11 = load i32, i32* %ind3, align 8, !tbaa !17
  %12 = load %struct.box*, %struct.box** %bv.addr, align 4, !tbaa !2
  %13 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds %struct.box, %struct.box* %12, i32 %13
  %colors = getelementptr inbounds %struct.box, %struct.box* %arrayidx4, i32 0, i32 6
  %14 = load i32, i32* %colors, align 4, !tbaa !19
  %add = add i32 %11, %14
  %cmp5 = icmp ult i32 %8, %add
  br i1 %cmp5, label %for.body7, label %for.cond.cleanup6

for.cond.cleanup6:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #2
  br label %for.end

for.body7:                                        ; preds = %for.cond1
  %16 = load i32, i32* %bi, align 4, !tbaa !6
  %conv = trunc i32 %16 to i8
  %17 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %17, i32 %18
  %tmp = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx8, i32 0, i32 4
  %likely_colormap_index = bitcast %union.anon* %tmp to i8*
  store i8 %conv, i8* %likely_colormap_index, align 4, !tbaa !32
  br label %for.inc

for.inc:                                          ; preds = %for.body7
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup6
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %20 = load i32, i32* %bi, align 4, !tbaa !6
  %inc10 = add i32 %20, 1
  store i32 %inc10, i32* %bi, align 4, !tbaa !6
  br label %for.cond

for.end11:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
declare void @llvm.stackrestore(i8*) #2

; Function Attrs: nounwind
define internal void @averagepixels(%struct.f_pixel* noalias sret align 4 %agg.result, i32 %clrs, %struct.hist_item* %achv) #0 {
entry:
  %clrs.addr = alloca i32, align 4
  %achv.addr = alloca %struct.hist_item*, align 4
  %r = alloca double, align 8
  %g = alloca double, align 8
  %b = alloca double, align 8
  %a = alloca double, align 8
  %sum = alloca double, align 8
  %i = alloca i32, align 4
  %px = alloca %struct.f_pixel, align 4
  %weight = alloca double, align 8
  store i32 %clrs, i32* %clrs.addr, align 4, !tbaa !6
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %0 = bitcast double* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #2
  store double 0.000000e+00, double* %r, align 8, !tbaa !8
  %1 = bitcast double* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #2
  store double 0.000000e+00, double* %g, align 8, !tbaa !8
  %2 = bitcast double* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %2) #2
  store double 0.000000e+00, double* %b, align 8, !tbaa !8
  %3 = bitcast double* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #2
  store double 0.000000e+00, double* %a, align 8, !tbaa !8
  %4 = bitcast double* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #2
  store double 0.000000e+00, double* %sum, align 8, !tbaa !8
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %7 = load i32, i32* %clrs.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  %9 = bitcast %struct.f_pixel* %px to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #2
  %10 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %10, i32 %11
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 0
  %12 = bitcast %struct.f_pixel* %px to i8*
  %13 = bitcast %struct.f_pixel* %acolor to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !22
  %14 = bitcast double* %weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %14) #2
  %15 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %15, i32 %16
  %adjusted_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx1, i32 0, i32 1
  %17 = load float, float* %adjusted_weight, align 4, !tbaa !13
  %conv = fpext float %17 to double
  store double %conv, double* %weight, align 8, !tbaa !8
  %18 = load double, double* %weight, align 8, !tbaa !8
  %19 = load double, double* %sum, align 8, !tbaa !8
  %add = fadd double %19, %18
  store double %add, double* %sum, align 8, !tbaa !8
  %a2 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %20 = load float, float* %a2, align 4, !tbaa !39
  %conv3 = fpext float %20 to double
  %21 = load double, double* %weight, align 8, !tbaa !8
  %mul = fmul double %conv3, %21
  %22 = load double, double* %a, align 8, !tbaa !8
  %add4 = fadd double %22, %mul
  store double %add4, double* %a, align 8, !tbaa !8
  %r5 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %23 = load float, float* %r5, align 4, !tbaa !40
  %conv6 = fpext float %23 to double
  %24 = load double, double* %weight, align 8, !tbaa !8
  %mul7 = fmul double %conv6, %24
  %25 = load double, double* %r, align 8, !tbaa !8
  %add8 = fadd double %25, %mul7
  store double %add8, double* %r, align 8, !tbaa !8
  %g9 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %26 = load float, float* %g9, align 4, !tbaa !41
  %conv10 = fpext float %26 to double
  %27 = load double, double* %weight, align 8, !tbaa !8
  %mul11 = fmul double %conv10, %27
  %28 = load double, double* %g, align 8, !tbaa !8
  %add12 = fadd double %28, %mul11
  store double %add12, double* %g, align 8, !tbaa !8
  %b13 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %29 = load float, float* %b13, align 4, !tbaa !42
  %conv14 = fpext float %29 to double
  %30 = load double, double* %weight, align 8, !tbaa !8
  %mul15 = fmul double %conv14, %30
  %31 = load double, double* %b, align 8, !tbaa !8
  %add16 = fadd double %31, %mul15
  store double %add16, double* %b, align 8, !tbaa !8
  %32 = bitcast double* %weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %32) #2
  %33 = bitcast %struct.f_pixel* %px to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %34, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %35 = load double, double* %sum, align 8, !tbaa !8
  %tobool = fcmp une double %35, 0.000000e+00
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %36 = load double, double* %sum, align 8, !tbaa !8
  %37 = load double, double* %a, align 8, !tbaa !8
  %div = fdiv double %37, %36
  store double %div, double* %a, align 8, !tbaa !8
  %38 = load double, double* %sum, align 8, !tbaa !8
  %39 = load double, double* %r, align 8, !tbaa !8
  %div17 = fdiv double %39, %38
  store double %div17, double* %r, align 8, !tbaa !8
  %40 = load double, double* %sum, align 8, !tbaa !8
  %41 = load double, double* %g, align 8, !tbaa !8
  %div18 = fdiv double %41, %40
  store double %div18, double* %g, align 8, !tbaa !8
  %42 = load double, double* %sum, align 8, !tbaa !8
  %43 = load double, double* %b, align 8, !tbaa !8
  %div19 = fdiv double %43, %42
  store double %div19, double* %b, align 8, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %a20 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 0
  %44 = load double, double* %a, align 8, !tbaa !8
  %conv21 = fptrunc double %44 to float
  store float %conv21, float* %a20, align 4, !tbaa !39
  %r22 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 1
  %45 = load double, double* %r, align 8, !tbaa !8
  %conv23 = fptrunc double %45 to float
  store float %conv23, float* %r22, align 4, !tbaa !40
  %g24 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 2
  %46 = load double, double* %g, align 8, !tbaa !8
  %conv25 = fptrunc double %46 to float
  store float %conv25, float* %g24, align 4, !tbaa !41
  %b26 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 3
  %47 = load double, double* %b, align 8, !tbaa !8
  %conv27 = fptrunc double %47 to float
  store float %conv27, float* %b26, align 4, !tbaa !42
  %48 = bitcast double* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %48) #2
  %49 = bitcast double* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %49) #2
  %50 = bitcast double* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %50) #2
  %51 = bitcast double* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %51) #2
  %52 = bitcast double* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %52) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal void @box_variance(%struct.f_pixel* noalias sret align 4 %agg.result, %struct.hist_item* %achv, %struct.box* %box) #0 {
entry:
  %achv.addr = alloca %struct.hist_item*, align 4
  %box.addr = alloca %struct.box*, align 4
  %mean = alloca %struct.f_pixel, align 4
  %variancea = alloca double, align 8
  %variancer = alloca double, align 8
  %varianceg = alloca double, align 8
  %varianceb = alloca double, align 8
  %i = alloca i32, align 4
  %px = alloca %struct.f_pixel, align 4
  %weight = alloca double, align 8
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  store %struct.box* %box, %struct.box** %box.addr, align 4, !tbaa !2
  %0 = bitcast %struct.f_pixel* %mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #2
  %1 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %color = getelementptr inbounds %struct.box, %struct.box* %1, i32 0, i32 0
  %2 = bitcast %struct.f_pixel* %mean to i8*
  %3 = bitcast %struct.f_pixel* %color to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 8 %3, i32 16, i1 false), !tbaa.struct !22
  %4 = bitcast double* %variancea to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #2
  store double 0.000000e+00, double* %variancea, align 8, !tbaa !8
  %5 = bitcast double* %variancer to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #2
  store double 0.000000e+00, double* %variancer, align 8, !tbaa !8
  %6 = bitcast double* %varianceg to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #2
  store double 0.000000e+00, double* %varianceg, align 8, !tbaa !8
  %7 = bitcast double* %varianceb to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #2
  store double 0.000000e+00, double* %varianceb, align 8, !tbaa !8
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %10 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.box, %struct.box* %10, i32 0, i32 6
  %11 = load i32, i32* %colors, align 4, !tbaa !19
  %cmp = icmp ult i32 %9, %11
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast %struct.f_pixel* %px to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #2
  %14 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %15 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %ind = getelementptr inbounds %struct.box, %struct.box* %15, i32 0, i32 5
  %16 = load i32, i32* %ind, align 8, !tbaa !17
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %add = add i32 %16, %17
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %14, i32 %add
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 0
  %18 = bitcast %struct.f_pixel* %px to i8*
  %19 = bitcast %struct.f_pixel* %acolor to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !22
  %20 = bitcast double* %weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %20) #2
  %21 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %22 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %ind1 = getelementptr inbounds %struct.box, %struct.box* %22, i32 0, i32 5
  %23 = load i32, i32* %ind1, align 8, !tbaa !17
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %add2 = add i32 %23, %24
  %arrayidx3 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %21, i32 %add2
  %adjusted_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx3, i32 0, i32 1
  %25 = load float, float* %adjusted_weight, align 4, !tbaa !13
  %conv = fpext float %25 to double
  store double %conv, double* %weight, align 8, !tbaa !8
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %mean, i32 0, i32 0
  %26 = load float, float* %a, align 4, !tbaa !39
  %a4 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %27 = load float, float* %a4, align 4, !tbaa !39
  %sub = fsub float %26, %27
  %conv5 = fpext float %sub to double
  %call = call double @variance_diff(double %conv5, double 7.812500e-03)
  %28 = load double, double* %weight, align 8, !tbaa !8
  %mul = fmul double %call, %28
  %29 = load double, double* %variancea, align 8, !tbaa !8
  %add6 = fadd double %29, %mul
  store double %add6, double* %variancea, align 8, !tbaa !8
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %mean, i32 0, i32 1
  %30 = load float, float* %r, align 4, !tbaa !40
  %r7 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %31 = load float, float* %r7, align 4, !tbaa !40
  %sub8 = fsub float %30, %31
  %conv9 = fpext float %sub8 to double
  %call10 = call double @variance_diff(double %conv9, double 3.906250e-03)
  %32 = load double, double* %weight, align 8, !tbaa !8
  %mul11 = fmul double %call10, %32
  %33 = load double, double* %variancer, align 8, !tbaa !8
  %add12 = fadd double %33, %mul11
  store double %add12, double* %variancer, align 8, !tbaa !8
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %mean, i32 0, i32 2
  %34 = load float, float* %g, align 4, !tbaa !41
  %g13 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %35 = load float, float* %g13, align 4, !tbaa !41
  %sub14 = fsub float %34, %35
  %conv15 = fpext float %sub14 to double
  %call16 = call double @variance_diff(double %conv15, double 3.906250e-03)
  %36 = load double, double* %weight, align 8, !tbaa !8
  %mul17 = fmul double %call16, %36
  %37 = load double, double* %varianceg, align 8, !tbaa !8
  %add18 = fadd double %37, %mul17
  store double %add18, double* %varianceg, align 8, !tbaa !8
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %mean, i32 0, i32 3
  %38 = load float, float* %b, align 4, !tbaa !42
  %b19 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %39 = load float, float* %b19, align 4, !tbaa !42
  %sub20 = fsub float %38, %39
  %conv21 = fpext float %sub20 to double
  %call22 = call double @variance_diff(double %conv21, double 3.906250e-03)
  %40 = load double, double* %weight, align 8, !tbaa !8
  %mul23 = fmul double %call22, %40
  %41 = load double, double* %varianceb, align 8, !tbaa !8
  %add24 = fadd double %41, %mul23
  store double %add24, double* %varianceb, align 8, !tbaa !8
  %42 = bitcast double* %weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %42) #2
  %43 = bitcast %struct.f_pixel* %px to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %44, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %a25 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 0
  %45 = load double, double* %variancea, align 8, !tbaa !8
  %mul26 = fmul double %45, 2.500000e-01
  %conv27 = fptrunc double %mul26 to float
  store float %conv27, float* %a25, align 4, !tbaa !39
  %r28 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 1
  %46 = load double, double* %variancer, align 8, !tbaa !8
  %mul29 = fmul double %46, 4.375000e-01
  %conv30 = fptrunc double %mul29 to float
  store float %conv30, float* %r28, align 4, !tbaa !40
  %g31 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 2
  %47 = load double, double* %varianceg, align 8, !tbaa !8
  %mul32 = fmul double %47, 5.625000e-01
  %conv33 = fptrunc double %mul32 to float
  store float %conv33, float* %g31, align 4, !tbaa !41
  %b34 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 3
  %48 = load double, double* %varianceb, align 8, !tbaa !8
  %mul35 = fmul double %48, 3.125000e-01
  %conv36 = fptrunc double %mul35 to float
  store float %conv36, float* %b34, align 4, !tbaa !42
  %49 = bitcast double* %varianceb to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %49) #2
  %50 = bitcast double* %varianceg to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %50) #2
  %51 = bitcast double* %variancer to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %51) #2
  %52 = bitcast double* %variancea to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %52) #2
  %53 = bitcast %struct.f_pixel* %mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #2
  ret void
}

; Function Attrs: nounwind
define internal double @box_max_error(%struct.hist_item* %achv, %struct.box* %box) #0 {
entry:
  %achv.addr = alloca %struct.hist_item*, align 4
  %box.addr = alloca %struct.box*, align 4
  %mean = alloca %struct.f_pixel, align 4
  %max_error = alloca double, align 8
  %i = alloca i32, align 4
  %diff = alloca double, align 8
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  store %struct.box* %box, %struct.box** %box.addr, align 4, !tbaa !2
  %0 = bitcast %struct.f_pixel* %mean to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #2
  %1 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %color = getelementptr inbounds %struct.box, %struct.box* %1, i32 0, i32 0
  %2 = bitcast %struct.f_pixel* %mean to i8*
  %3 = bitcast %struct.f_pixel* %color to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 8 %3, i32 16, i1 false), !tbaa.struct !22
  %4 = bitcast double* %max_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #2
  store double 0.000000e+00, double* %max_error, align 8, !tbaa !8
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %7 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.box, %struct.box* %7, i32 0, i32 6
  %8 = load i32, i32* %colors, align 4, !tbaa !19
  %cmp = icmp ult i32 %6, %8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast double* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %10) #2
  %11 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %12 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %ind = getelementptr inbounds %struct.box, %struct.box* %12, i32 0, i32 5
  %13 = load i32, i32* %ind, align 8, !tbaa !17
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %add = add i32 %13, %14
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %11, i32 %add
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 0
  %call = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %mean, %struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor)
  %conv = fpext float %call to double
  store double %conv, double* %diff, align 8, !tbaa !8
  %15 = load double, double* %diff, align 8, !tbaa !8
  %16 = load double, double* %max_error, align 8, !tbaa !8
  %cmp1 = fcmp ogt double %15, %16
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %17 = load double, double* %diff, align 8, !tbaa !8
  store double %17, double* %max_error, align 8, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %18 = bitcast double* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %18) #2
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = load double, double* %max_error, align 8, !tbaa !8
  %21 = bitcast double* %max_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %21) #2
  %22 = bitcast %struct.f_pixel* %mean to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #2
  ret double %20
}

; Function Attrs: alwaysinline nounwind
define internal double @variance_diff(double %val, double %good_enough) #4 {
entry:
  %retval = alloca double, align 8
  %val.addr = alloca double, align 8
  %good_enough.addr = alloca double, align 8
  store double %val, double* %val.addr, align 8, !tbaa !8
  store double %good_enough, double* %good_enough.addr, align 8, !tbaa !8
  %0 = load double, double* %val.addr, align 8, !tbaa !8
  %1 = load double, double* %val.addr, align 8, !tbaa !8
  %mul = fmul double %1, %0
  store double %mul, double* %val.addr, align 8, !tbaa !8
  %2 = load double, double* %val.addr, align 8, !tbaa !8
  %3 = load double, double* %good_enough.addr, align 8, !tbaa !8
  %4 = load double, double* %good_enough.addr, align 8, !tbaa !8
  %mul1 = fmul double %3, %4
  %cmp = fcmp olt double %2, %mul1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load double, double* %val.addr, align 8, !tbaa !8
  %mul2 = fmul double %5, 2.500000e-01
  store double %mul2, double* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %6 = load double, double* %val.addr, align 8, !tbaa !8
  store double %6, double* %retval, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load double, double* %retval, align 8
  ret double %7
}

; Function Attrs: alwaysinline nounwind
define internal float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py) #4 {
entry:
  %call = call float @colordifference_stdc(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py)
  ret float %call
}

; Function Attrs: alwaysinline nounwind
define internal float @colordifference_stdc(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py) #4 {
entry:
  %alphas = alloca double, align 8
  %0 = bitcast double* %alphas to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #2
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 0
  %1 = load float, float* %a, align 4, !tbaa !39
  %a1 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %2 = load float, float* %a1, align 4, !tbaa !39
  %sub = fsub float %1, %2
  %conv = fpext float %sub to double
  store double %conv, double* %alphas, align 8, !tbaa !8
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %3 = load float, float* %r, align 4, !tbaa !40
  %conv2 = fpext float %3 to double
  %r3 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 1
  %4 = load float, float* %r3, align 4, !tbaa !40
  %conv4 = fpext float %4 to double
  %5 = load double, double* %alphas, align 8, !tbaa !8
  %call = call double @colordifference_ch(double %conv2, double %conv4, double %5)
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %6 = load float, float* %g, align 4, !tbaa !41
  %conv5 = fpext float %6 to double
  %g6 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 2
  %7 = load float, float* %g6, align 4, !tbaa !41
  %conv7 = fpext float %7 to double
  %8 = load double, double* %alphas, align 8, !tbaa !8
  %call8 = call double @colordifference_ch(double %conv5, double %conv7, double %8)
  %add = fadd double %call, %call8
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %9 = load float, float* %b, align 4, !tbaa !42
  %conv9 = fpext float %9 to double
  %b10 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 3
  %10 = load float, float* %b10, align 4, !tbaa !42
  %conv11 = fpext float %10 to double
  %11 = load double, double* %alphas, align 8, !tbaa !8
  %call12 = call double @colordifference_ch(double %conv9, double %conv11, double %11)
  %add13 = fadd double %add, %call12
  %conv14 = fptrunc double %add13 to float
  %12 = bitcast double* %alphas to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #2
  ret float %conv14
}

; Function Attrs: alwaysinline nounwind
define internal double @colordifference_ch(double %x, double %y, double %alphas) #4 {
entry:
  %x.addr = alloca double, align 8
  %y.addr = alloca double, align 8
  %alphas.addr = alloca double, align 8
  %black = alloca double, align 8
  %white = alloca double, align 8
  store double %x, double* %x.addr, align 8, !tbaa !8
  store double %y, double* %y.addr, align 8, !tbaa !8
  store double %alphas, double* %alphas.addr, align 8, !tbaa !8
  %0 = bitcast double* %black to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #2
  %1 = load double, double* %x.addr, align 8, !tbaa !8
  %2 = load double, double* %y.addr, align 8, !tbaa !8
  %sub = fsub double %1, %2
  store double %sub, double* %black, align 8, !tbaa !8
  %3 = bitcast double* %white to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #2
  %4 = load double, double* %black, align 8, !tbaa !8
  %5 = load double, double* %alphas.addr, align 8, !tbaa !8
  %add = fadd double %4, %5
  store double %add, double* %white, align 8, !tbaa !8
  %6 = load double, double* %black, align 8, !tbaa !8
  %7 = load double, double* %black, align 8, !tbaa !8
  %mul = fmul double %6, %7
  %8 = load double, double* %white, align 8, !tbaa !8
  %9 = load double, double* %white, align 8, !tbaa !8
  %mul1 = fmul double %8, %9
  %cmp = fcmp ogt double %mul, %mul1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load double, double* %black, align 8, !tbaa !8
  %11 = load double, double* %black, align 8, !tbaa !8
  %mul2 = fmul double %10, %11
  br label %cond.end

cond.false:                                       ; preds = %entry
  %12 = load double, double* %white, align 8, !tbaa !8
  %13 = load double, double* %white, align 8, !tbaa !8
  %mul3 = fmul double %12, %13
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %mul2, %cond.true ], [ %mul3, %cond.false ]
  %14 = bitcast double* %white to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %14) #2
  %15 = bitcast double* %black to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #2
  ret double %cond
}

declare void @qsort(i8*, i32, i32, i32 (i8*, i8*)*) #3

; Function Attrs: nounwind
define internal i32 @comparevariance(i8* %ch1, i8* %ch2) #0 {
entry:
  %ch1.addr = alloca i8*, align 4
  %ch2.addr = alloca i8*, align 4
  store i8* %ch1, i8** %ch1.addr, align 4, !tbaa !2
  store i8* %ch2, i8** %ch2.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ch1.addr, align 4, !tbaa !2
  %1 = bitcast i8* %0 to %struct.channelvariance*
  %variance = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %1, i32 0, i32 1
  %2 = load float, float* %variance, align 4, !tbaa !31
  %3 = load i8*, i8** %ch2.addr, align 4, !tbaa !2
  %4 = bitcast i8* %3 to %struct.channelvariance*
  %variance1 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %4, i32 0, i32 1
  %5 = load float, float* %variance1, align 4, !tbaa !31
  %cmp = fcmp ogt float %2, %5
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i8*, i8** %ch1.addr, align 4, !tbaa !2
  %7 = bitcast i8* %6 to %struct.channelvariance*
  %variance2 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %7, i32 0, i32 1
  %8 = load float, float* %variance2, align 4, !tbaa !31
  %9 = load i8*, i8** %ch2.addr, align 4, !tbaa !2
  %10 = bitcast i8* %9 to %struct.channelvariance*
  %variance3 = getelementptr inbounds %struct.channelvariance, %struct.channelvariance* %10, i32 0, i32 1
  %11 = load float, float* %variance3, align 4, !tbaa !31
  %cmp4 = fcmp olt float %8, %11
  %12 = zext i1 %cmp4 to i64
  %cond = select i1 %cmp4, i32 1, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond5 = phi i32 [ -1, %cond.true ], [ %cond, %cond.false ]
  ret i32 %cond5
}

; Function Attrs: nounwind
define internal void @get_median(%struct.f_pixel* noalias sret align 4 %agg.result, %struct.box* %b, %struct.hist_item* %achv) #0 {
entry:
  %b.addr = alloca %struct.box*, align 4
  %achv.addr = alloca %struct.hist_item*, align 4
  %median_start = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.box* %b, %struct.box** %b.addr, align 4, !tbaa !2
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %0 = bitcast i32* %median_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.box, %struct.box* %1, i32 0, i32 6
  %2 = load i32, i32* %colors, align 4, !tbaa !19
  %sub = sub i32 %2, 1
  %div = udiv i32 %sub, 2
  store i32 %div, i32* %median_start, align 4, !tbaa !6
  %3 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %4 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %ind = getelementptr inbounds %struct.box, %struct.box* %4, i32 0, i32 5
  %5 = load i32, i32* %ind, align 8, !tbaa !17
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %3, i32 %5
  %6 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %colors1 = getelementptr inbounds %struct.box, %struct.box* %6, i32 0, i32 6
  %7 = load i32, i32* %colors1, align 4, !tbaa !19
  %8 = load i32, i32* %median_start, align 4, !tbaa !6
  call void @hist_item_sort_range(%struct.hist_item* %arrayidx, i32 %7, i32 %8)
  %9 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %colors2 = getelementptr inbounds %struct.box, %struct.box* %9, i32 0, i32 6
  %10 = load i32, i32* %colors2, align 4, !tbaa !19
  %and = and i32 %10, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %12 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %ind3 = getelementptr inbounds %struct.box, %struct.box* %12, i32 0, i32 5
  %13 = load i32, i32* %ind3, align 8, !tbaa !17
  %14 = load i32, i32* %median_start, align 4, !tbaa !6
  %add = add i32 %13, %14
  %arrayidx4 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %11, i32 %add
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx4, i32 0, i32 0
  %15 = bitcast %struct.f_pixel* %agg.result to i8*
  %16 = bitcast %struct.f_pixel* %acolor to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false), !tbaa.struct !22
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %17 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %18 = load %struct.box*, %struct.box** %b.addr, align 4, !tbaa !2
  %ind5 = getelementptr inbounds %struct.box, %struct.box* %18, i32 0, i32 5
  %19 = load i32, i32* %ind5, align 8, !tbaa !17
  %20 = load i32, i32* %median_start, align 4, !tbaa !6
  %add6 = add i32 %19, %20
  %arrayidx7 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %17, i32 %add6
  call void @averagepixels(%struct.f_pixel* sret align 4 %agg.result, i32 2, %struct.hist_item* %arrayidx7)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %21 = bitcast i32* %median_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #2
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal double @color_weight(%struct.f_pixel* byval(%struct.f_pixel) align 4 %median, %struct.hist_item* byval(%struct.hist_item) align 4 %h) #4 {
entry:
  %diff = alloca float, align 4
  %0 = bitcast float* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %h, i32 0, i32 0
  %call = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %median, %struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor)
  store float %call, float* %diff, align 4, !tbaa !23
  %1 = load float, float* %diff, align 4, !tbaa !23
  %conv = fpext float %1 to double
  %2 = call double @llvm.sqrt.f64(double %conv)
  %adjusted_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %h, i32 0, i32 1
  %3 = load float, float* %adjusted_weight, align 4, !tbaa !13
  %conv1 = fpext float %3 to double
  %add = fadd double 1.000000e+00, %conv1
  %4 = call double @llvm.sqrt.f64(double %add)
  %sub = fsub double %4, 1.000000e+00
  %mul = fmul double %2, %sub
  %5 = bitcast float* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #2
  ret double %mul
}

; Function Attrs: nounwind
define internal void @hist_item_sort_range(%struct.hist_item* %base, i32 %len, i32 %sort_start) #0 {
entry:
  %base.addr = alloca %struct.hist_item*, align 4
  %len.addr = alloca i32, align 4
  %sort_start.addr = alloca i32, align 4
  %l = alloca i32, align 4
  %r = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.hist_item* %base, %struct.hist_item** %base.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !6
  store i32 %sort_start, i32* %sort_start.addr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %entry
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %2 = load i32, i32* %len.addr, align 4, !tbaa !6
  %call = call i32 @qsort_partition(%struct.hist_item* %1, i32 %2)
  store i32 %call, i32* %l, align 4, !tbaa !6
  %3 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = load i32, i32* %l, align 4, !tbaa !6
  %add = add i32 %4, 1
  store i32 %add, i32* %r, align 4, !tbaa !6
  %5 = load i32, i32* %l, align 4, !tbaa !6
  %cmp = icmp ugt i32 %5, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.cond
  %6 = load i32, i32* %sort_start.addr, align 4, !tbaa !6
  %7 = load i32, i32* %l, align 4, !tbaa !6
  %cmp1 = icmp ult i32 %6, %7
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %8 = load i32, i32* %l, align 4, !tbaa !6
  store i32 %8, i32* %len.addr, align 4, !tbaa !6
  br label %if.end8

if.else:                                          ; preds = %land.lhs.true, %for.cond
  %9 = load i32, i32* %r, align 4, !tbaa !6
  %10 = load i32, i32* %len.addr, align 4, !tbaa !6
  %cmp2 = icmp ult i32 %9, %10
  br i1 %cmp2, label %land.lhs.true3, label %if.else7

land.lhs.true3:                                   ; preds = %if.else
  %11 = load i32, i32* %sort_start.addr, align 4, !tbaa !6
  %12 = load i32, i32* %r, align 4, !tbaa !6
  %cmp4 = icmp ugt i32 %11, %12
  br i1 %cmp4, label %if.then5, label %if.else7

if.then5:                                         ; preds = %land.lhs.true3
  %13 = load i32, i32* %r, align 4, !tbaa !6
  %14 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.hist_item, %struct.hist_item* %14, i32 %13
  store %struct.hist_item* %add.ptr, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %15 = load i32, i32* %r, align 4, !tbaa !6
  %16 = load i32, i32* %len.addr, align 4, !tbaa !6
  %sub = sub i32 %16, %15
  store i32 %sub, i32* %len.addr, align 4, !tbaa !6
  %17 = load i32, i32* %r, align 4, !tbaa !6
  %18 = load i32, i32* %sort_start.addr, align 4, !tbaa !6
  %sub6 = sub i32 %18, %17
  store i32 %sub6, i32* %sort_start.addr, align 4, !tbaa !6
  br label %if.end

if.else7:                                         ; preds = %land.lhs.true3, %if.else
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then5
  br label %if.end8

if.end8:                                          ; preds = %if.end, %if.then
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.else7
  %19 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #2
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond

for.end:                                          ; preds = %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: alwaysinline nounwind
define internal i32 @qsort_partition(%struct.hist_item* %base, i32 %len) #4 {
entry:
  %base.addr = alloca %struct.hist_item*, align 4
  %len.addr = alloca i32, align 4
  %l = alloca i32, align 4
  %r = alloca i32, align 4
  %pivot_value = alloca i32, align 4
  store %struct.hist_item* %base, %struct.hist_item** %base.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !6
  %0 = bitcast i32* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  store i32 1, i32* %l, align 4, !tbaa !6
  %1 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = load i32, i32* %len.addr, align 4, !tbaa !6
  store i32 %2, i32* %r, align 4, !tbaa !6
  %3 = load i32, i32* %len.addr, align 4, !tbaa !6
  %cmp = icmp uge i32 %3, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %4, i32 0
  %5 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %6 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %7 = load i32, i32* %len.addr, align 4, !tbaa !6
  %call = call i32 @qsort_pivot(%struct.hist_item* %6, i32 %7)
  %arrayidx1 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %5, i32 %call
  call void @hist_item_swap(%struct.hist_item* %arrayidx, %struct.hist_item* %arrayidx1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = bitcast i32* %pivot_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %9, i32 0
  %tmp = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx2, i32 0, i32 4
  %sort_value = bitcast %union.anon* %tmp to i32*
  %10 = load i32, i32* %sort_value, align 4, !tbaa !32
  store i32 %10, i32* %pivot_value, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end18, %if.end
  %11 = load i32, i32* %l, align 4, !tbaa !6
  %12 = load i32, i32* %r, align 4, !tbaa !6
  %cmp3 = icmp ult i32 %11, %12
  br i1 %cmp3, label %while.body, label %while.end19

while.body:                                       ; preds = %while.cond
  %13 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %14 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %13, i32 %14
  %tmp5 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx4, i32 0, i32 4
  %sort_value6 = bitcast %union.anon* %tmp5 to i32*
  %15 = load i32, i32* %sort_value6, align 4, !tbaa !32
  %16 = load i32, i32* %pivot_value, align 4, !tbaa !6
  %cmp7 = icmp uge i32 %15, %16
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %while.body
  %17 = load i32, i32* %l, align 4, !tbaa !6
  %inc = add i32 %17, 1
  store i32 %inc, i32* %l, align 4, !tbaa !6
  br label %if.end18

if.else:                                          ; preds = %while.body
  br label %while.cond9

while.cond9:                                      ; preds = %while.body15, %if.else
  %18 = load i32, i32* %l, align 4, !tbaa !6
  %19 = load i32, i32* %r, align 4, !tbaa !6
  %dec = add i32 %19, -1
  store i32 %dec, i32* %r, align 4, !tbaa !6
  %cmp10 = icmp ult i32 %18, %dec
  br i1 %cmp10, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond9
  %20 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %21 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %20, i32 %21
  %tmp12 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx11, i32 0, i32 4
  %sort_value13 = bitcast %union.anon* %tmp12 to i32*
  %22 = load i32, i32* %sort_value13, align 4, !tbaa !32
  %23 = load i32, i32* %pivot_value, align 4, !tbaa !6
  %cmp14 = icmp ule i32 %22, %23
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond9
  %24 = phi i1 [ false, %while.cond9 ], [ %cmp14, %land.rhs ]
  br i1 %24, label %while.body15, label %while.end

while.body15:                                     ; preds = %land.end
  br label %while.cond9

while.end:                                        ; preds = %land.end
  %25 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %26 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %25, i32 %26
  %27 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %28 = load i32, i32* %r, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %27, i32 %28
  call void @hist_item_swap(%struct.hist_item* %arrayidx16, %struct.hist_item* %arrayidx17)
  br label %if.end18

if.end18:                                         ; preds = %while.end, %if.then8
  br label %while.cond

while.end19:                                      ; preds = %while.cond
  %29 = load i32, i32* %l, align 4, !tbaa !6
  %dec20 = add i32 %29, -1
  store i32 %dec20, i32* %l, align 4, !tbaa !6
  %30 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %30, i32 0
  %31 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %32 = load i32, i32* %l, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %31, i32 %32
  call void @hist_item_swap(%struct.hist_item* %arrayidx21, %struct.hist_item* %arrayidx22)
  %33 = load i32, i32* %l, align 4, !tbaa !6
  %34 = bitcast i32* %pivot_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #2
  %35 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #2
  %36 = bitcast i32* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #2
  ret i32 %33
}

; Function Attrs: inlinehint nounwind
define internal void @hist_item_swap(%struct.hist_item* %l, %struct.hist_item* %r) #5 {
entry:
  %l.addr = alloca %struct.hist_item*, align 4
  %r.addr = alloca %struct.hist_item*, align 4
  %t = alloca %struct.hist_item, align 4
  store %struct.hist_item* %l, %struct.hist_item** %l.addr, align 4, !tbaa !2
  store %struct.hist_item* %r, %struct.hist_item** %r.addr, align 4, !tbaa !2
  %0 = load %struct.hist_item*, %struct.hist_item** %l.addr, align 4, !tbaa !2
  %1 = load %struct.hist_item*, %struct.hist_item** %r.addr, align 4, !tbaa !2
  %cmp = icmp ne %struct.hist_item* %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %struct.hist_item* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %2) #2
  %3 = load %struct.hist_item*, %struct.hist_item** %l.addr, align 4, !tbaa !2
  %4 = bitcast %struct.hist_item* %t to i8*
  %5 = bitcast %struct.hist_item* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 32, i1 false), !tbaa.struct !43
  %6 = load %struct.hist_item*, %struct.hist_item** %l.addr, align 4, !tbaa !2
  %7 = load %struct.hist_item*, %struct.hist_item** %r.addr, align 4, !tbaa !2
  %8 = bitcast %struct.hist_item* %6 to i8*
  %9 = bitcast %struct.hist_item* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 32, i1 false), !tbaa.struct !43
  %10 = load %struct.hist_item*, %struct.hist_item** %r.addr, align 4, !tbaa !2
  %11 = bitcast %struct.hist_item* %10 to i8*
  %12 = bitcast %struct.hist_item* %t to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 32, i1 false), !tbaa.struct !43
  %13 = bitcast %struct.hist_item* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %13) #2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal i32 @qsort_pivot(%struct.hist_item* %base, i32 %len) #4 {
entry:
  %retval = alloca i32, align 4
  %base.addr = alloca %struct.hist_item*, align 4
  %len.addr = alloca i32, align 4
  %aidx = alloca i32, align 4
  %bidx = alloca i32, align 4
  %cidx = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  store %struct.hist_item* %base, %struct.hist_item** %base.addr, align 4, !tbaa !2
  store i32 %len, i32* %len.addr, align 4, !tbaa !6
  %0 = load i32, i32* %len.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %0, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %len.addr, align 4, !tbaa !6
  %div = udiv i32 %1, 2
  store i32 %div, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast i32* %aidx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  store i32 8, i32* %aidx, align 4, !tbaa !6
  %3 = bitcast i32* %bidx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = load i32, i32* %len.addr, align 4, !tbaa !6
  %div1 = udiv i32 %4, 2
  store i32 %div1, i32* %bidx, align 4, !tbaa !6
  %5 = bitcast i32* %cidx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = load i32, i32* %len.addr, align 4, !tbaa !6
  %sub = sub i32 %6, 1
  store i32 %sub, i32* %cidx, align 4, !tbaa !6
  %7 = bitcast i32* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %8, i32 8
  %tmp = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 4
  %sort_value = bitcast %union.anon* %tmp to i32*
  %9 = load i32, i32* %sort_value, align 4, !tbaa !32
  store i32 %9, i32* %a, align 4, !tbaa !6
  %10 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %12 = load i32, i32* %bidx, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %11, i32 %12
  %tmp3 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx2, i32 0, i32 4
  %sort_value4 = bitcast %union.anon* %tmp3 to i32*
  %13 = load i32, i32* %sort_value4, align 4, !tbaa !32
  store i32 %13, i32* %b, align 4, !tbaa !6
  %14 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = load %struct.hist_item*, %struct.hist_item** %base.addr, align 4, !tbaa !2
  %16 = load i32, i32* %cidx, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %15, i32 %16
  %tmp6 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx5, i32 0, i32 4
  %sort_value7 = bitcast %union.anon* %tmp6 to i32*
  %17 = load i32, i32* %sort_value7, align 4, !tbaa !32
  store i32 %17, i32* %c, align 4, !tbaa !6
  %18 = load i32, i32* %a, align 4, !tbaa !6
  %19 = load i32, i32* %b, align 4, !tbaa !6
  %cmp8 = icmp ult i32 %18, %19
  br i1 %cmp8, label %cond.true, label %cond.false16

cond.true:                                        ; preds = %if.end
  %20 = load i32, i32* %b, align 4, !tbaa !6
  %21 = load i32, i32* %c, align 4, !tbaa !6
  %cmp9 = icmp ult i32 %20, %21
  br i1 %cmp9, label %cond.true10, label %cond.false

cond.true10:                                      ; preds = %cond.true
  %22 = load i32, i32* %bidx, align 4, !tbaa !6
  br label %cond.end14

cond.false:                                       ; preds = %cond.true
  %23 = load i32, i32* %a, align 4, !tbaa !6
  %24 = load i32, i32* %c, align 4, !tbaa !6
  %cmp11 = icmp ult i32 %23, %24
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %cond.false
  %25 = load i32, i32* %cidx, align 4, !tbaa !6
  br label %cond.end

cond.false13:                                     ; preds = %cond.false
  br label %cond.end

cond.end:                                         ; preds = %cond.false13, %cond.true12
  %cond = phi i32 [ %25, %cond.true12 ], [ 8, %cond.false13 ]
  br label %cond.end14

cond.end14:                                       ; preds = %cond.end, %cond.true10
  %cond15 = phi i32 [ %22, %cond.true10 ], [ %cond, %cond.end ]
  br label %cond.end27

cond.false16:                                     ; preds = %if.end
  %26 = load i32, i32* %b, align 4, !tbaa !6
  %27 = load i32, i32* %c, align 4, !tbaa !6
  %cmp17 = icmp ugt i32 %26, %27
  br i1 %cmp17, label %cond.true18, label %cond.false19

cond.true18:                                      ; preds = %cond.false16
  %28 = load i32, i32* %bidx, align 4, !tbaa !6
  br label %cond.end25

cond.false19:                                     ; preds = %cond.false16
  %29 = load i32, i32* %a, align 4, !tbaa !6
  %30 = load i32, i32* %c, align 4, !tbaa !6
  %cmp20 = icmp ult i32 %29, %30
  br i1 %cmp20, label %cond.true21, label %cond.false22

cond.true21:                                      ; preds = %cond.false19
  br label %cond.end23

cond.false22:                                     ; preds = %cond.false19
  %31 = load i32, i32* %cidx, align 4, !tbaa !6
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true21
  %cond24 = phi i32 [ 8, %cond.true21 ], [ %31, %cond.false22 ]
  br label %cond.end25

cond.end25:                                       ; preds = %cond.end23, %cond.true18
  %cond26 = phi i32 [ %28, %cond.true18 ], [ %cond24, %cond.end23 ]
  br label %cond.end27

cond.end27:                                       ; preds = %cond.end25, %cond.end14
  %cond28 = phi i32 [ %cond15, %cond.end14 ], [ %cond26, %cond.end25 ]
  store i32 %cond28, i32* %retval, align 4
  %32 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #2
  %33 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #2
  %34 = bitcast i32* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #2
  %35 = bitcast i32* %cidx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #2
  %36 = bitcast i32* %bidx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #2
  %37 = bitcast i32* %aidx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #2
  br label %return

return:                                           ; preds = %cond.end27, %if.then
  %38 = load i32, i32* %retval, align 4
  ret i32 %38
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sqrt.f64(double) #6

; Function Attrs: nounwind
define internal double @box_error(%struct.box* %box, %struct.hist_item* %achv) #0 {
entry:
  %box.addr = alloca %struct.box*, align 4
  %achv.addr = alloca %struct.hist_item*, align 4
  %avg = alloca %struct.f_pixel, align 4
  %total_error = alloca double, align 8
  %i = alloca i32, align 4
  store %struct.box* %box, %struct.box** %box.addr, align 4, !tbaa !2
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %0 = bitcast %struct.f_pixel* %avg to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #2
  %1 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %color = getelementptr inbounds %struct.box, %struct.box* %1, i32 0, i32 0
  %2 = bitcast %struct.f_pixel* %avg to i8*
  %3 = bitcast %struct.f_pixel* %color to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 8 %3, i32 16, i1 false), !tbaa.struct !22
  %4 = bitcast double* %total_error to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #2
  store double 0.000000e+00, double* %total_error, align 8, !tbaa !8
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %7 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.box, %struct.box* %7, i32 0, i32 6
  %8 = load i32, i32* %colors, align 4, !tbaa !19
  %cmp = icmp ult i32 %6, %8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #2
  br label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %11 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %ind = getelementptr inbounds %struct.box, %struct.box* %11, i32 0, i32 5
  %12 = load i32, i32* %ind, align 8, !tbaa !17
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %add = add i32 %12, %13
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %10, i32 %add
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 0
  %call = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %avg, %struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor)
  %14 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %15 = load %struct.box*, %struct.box** %box.addr, align 4, !tbaa !2
  %ind1 = getelementptr inbounds %struct.box, %struct.box* %15, i32 0, i32 5
  %16 = load i32, i32* %ind1, align 8, !tbaa !17
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %add2 = add i32 %16, %17
  %arrayidx3 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %14, i32 %add2
  %perceptual_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx3, i32 0, i32 2
  %18 = load float, float* %perceptual_weight, align 4, !tbaa !38
  %mul = fmul float %call, %18
  %conv = fpext float %mul to double
  %19 = load double, double* %total_error, align 8, !tbaa !8
  %add4 = fadd double %19, %conv
  store double %add4, double* %total_error, align 8, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %21 = load double, double* %total_error, align 8, !tbaa !8
  %22 = bitcast double* %total_error to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %22) #2
  %23 = bitcast %struct.f_pixel* %avg to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #2
  ret double %21
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!11, !3, i64 0}
!11 = !{!"", !3, i64 0, !3, i64 4, !9, i64 8, !7, i64 16, !7, i64 20}
!12 = !{!11, !7, i64 16}
!13 = !{!14, !16, i64 16}
!14 = !{!"", !15, i64 0, !16, i64 16, !16, i64 20, !16, i64 24, !4, i64 28}
!15 = !{!"", !16, i64 0, !16, i64 4, !16, i64 8, !16, i64 12}
!16 = !{!"float", !4, i64 0}
!17 = !{!18, !7, i64 56}
!18 = !{!"box", !15, i64 0, !15, i64 16, !9, i64 32, !9, i64 40, !9, i64 48, !7, i64 56, !7, i64 60}
!19 = !{!18, !7, i64 60}
!20 = !{!18, !9, i64 32}
!21 = !{!18, !9, i64 40}
!22 = !{i64 0, i64 4, !23, i64 4, i64 4, !23, i64 8, i64 4, !23, i64 12, i64 4, !23}
!23 = !{!16, !16, i64 0}
!24 = !{!18, !9, i64 48}
!25 = !{!18, !16, i64 20}
!26 = !{!18, !16, i64 24}
!27 = !{!18, !16, i64 28}
!28 = !{!18, !16, i64 16}
!29 = !{!30, !7, i64 0}
!30 = !{!"", !7, i64 0, !16, i64 4}
!31 = !{!30, !16, i64 4}
!32 = !{!4, !4, i64 0}
!33 = !{!14, !16, i64 24}
!34 = !{!11, !9, i64 8}
!35 = !{!36, !16, i64 16}
!36 = !{!"", !15, i64 0, !16, i64 16, !37, i64 20}
!37 = !{!"_Bool", !4, i64 0}
!38 = !{!14, !16, i64 20}
!39 = !{!15, !16, i64 0}
!40 = !{!15, !16, i64 4}
!41 = !{!15, !16, i64 8}
!42 = !{!15, !16, i64 12}
!43 = !{i64 0, i64 4, !23, i64 4, i64 4, !23, i64 8, i64 4, !23, i64 12, i64 4, !23, i64 16, i64 4, !23, i64 20, i64 4, !23, i64 24, i64 4, !23, i64 28, i64 4, !6, i64 28, i64 1, !32}
