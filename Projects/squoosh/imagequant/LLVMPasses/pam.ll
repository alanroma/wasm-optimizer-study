; ModuleID = 'pam.c'
source_filename = "pam.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.acolorhash_table = type { %struct.mempool*, i32, i32, i32, i32, i32, i32, i32, [512 x %struct.acolorhist_arr_item*], [0 x %struct.acolorhist_arr_head] }
%struct.mempool = type opaque
%struct.acolorhist_arr_item = type { %union.rgba_as_int, i32 }
%union.rgba_as_int = type { i32 }
%struct.acolorhist_arr_head = type { %struct.acolorhist_arr_item, %struct.acolorhist_arr_item, i32, i32, %struct.acolorhist_arr_item* }
%struct.rgba_pixel = type { i8, i8, i8, i8 }
%struct.histogram = type { %struct.hist_item*, void (i8*)*, double, i32, i32 }
%struct.hist_item = type { %struct.f_pixel, float, float, float, %union.anon }
%struct.f_pixel = type { float, float, float, float }
%union.anon = type { i32 }
%struct.colormap = type { i32, i8* (i32)*, void (i8*)*, [0 x %struct.colormap_item] }
%struct.colormap_item = type { %struct.f_pixel, float, i8 }

; Function Attrs: nounwind
define hidden zeroext i1 @pam_computeacolorhash(%struct.acolorhash_table* %acht, %struct.rgba_pixel** %pixels, i32 %cols, i32 %rows, i8* %importance_map) #0 {
entry:
  %retval = alloca i1, align 1
  %acht.addr = alloca %struct.acolorhash_table*, align 4
  %pixels.addr = alloca %struct.rgba_pixel**, align 4
  %cols.addr = alloca i32, align 4
  %rows.addr = alloca i32, align 4
  %importance_map.addr = alloca i8*, align 4
  %ignorebits = alloca i32, align 4
  %channel_mask = alloca i32, align 4
  %channel_hmask = alloca i32, align 4
  %posterize_mask = alloca i32, align 4
  %posterize_high_mask = alloca i32, align 4
  %hash_size = alloca i32, align 4
  %row = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %col = alloca i32, align 4
  %boost = alloca i32, align 4
  %px = alloca %union.rgba_as_int, align 4
  %hash = alloca i32, align 4
  store %struct.acolorhash_table* %acht, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  store %struct.rgba_pixel** %pixels, %struct.rgba_pixel*** %pixels.addr, align 4, !tbaa !2
  store i32 %cols, i32* %cols.addr, align 4, !tbaa !6
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !6
  store i8* %importance_map, i8** %importance_map.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ignorebits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %ignorebits1 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %1, i32 0, i32 1
  %2 = load i32, i32* %ignorebits1, align 4, !tbaa !6
  store i32 %2, i32* %ignorebits, align 4, !tbaa !6
  %3 = bitcast i32* %channel_mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %ignorebits, align 4, !tbaa !6
  %shr = lshr i32 255, %4
  %5 = load i32, i32* %ignorebits, align 4, !tbaa !6
  %shl = shl i32 %shr, %5
  store i32 %shl, i32* %channel_mask, align 4, !tbaa !6
  %6 = bitcast i32* %channel_hmask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  %7 = load i32, i32* %ignorebits, align 4, !tbaa !6
  %shr2 = lshr i32 255, %7
  %xor = xor i32 %shr2, 255
  store i32 %xor, i32* %channel_hmask, align 4, !tbaa !6
  %8 = bitcast i32* %posterize_mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  %9 = load i32, i32* %channel_mask, align 4, !tbaa !6
  %shl3 = shl i32 %9, 24
  %10 = load i32, i32* %channel_mask, align 4, !tbaa !6
  %shl4 = shl i32 %10, 16
  %or = or i32 %shl3, %shl4
  %11 = load i32, i32* %channel_mask, align 4, !tbaa !6
  %shl5 = shl i32 %11, 8
  %or6 = or i32 %or, %shl5
  %12 = load i32, i32* %channel_mask, align 4, !tbaa !6
  %or7 = or i32 %or6, %12
  store i32 %or7, i32* %posterize_mask, align 4, !tbaa !6
  %13 = bitcast i32* %posterize_high_mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %channel_hmask, align 4, !tbaa !6
  %shl8 = shl i32 %14, 24
  %15 = load i32, i32* %channel_hmask, align 4, !tbaa !6
  %shl9 = shl i32 %15, 16
  %or10 = or i32 %shl8, %shl9
  %16 = load i32, i32* %channel_hmask, align 4, !tbaa !6
  %shl11 = shl i32 %16, 8
  %or12 = or i32 %or10, %shl11
  %17 = load i32, i32* %channel_hmask, align 4, !tbaa !6
  %or13 = or i32 %or12, %17
  store i32 %or13, i32* %posterize_high_mask, align 4, !tbaa !6
  %18 = bitcast i32* %hash_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %hash_size14 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %19, i32 0, i32 6
  %20 = load i32, i32* %hash_size14, align 4, !tbaa !6
  store i32 %20, i32* %hash_size, align 4, !tbaa !6
  %21 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc42, %entry
  %22 = load i32, i32* %row, align 4, !tbaa !6
  %23 = load i32, i32* %rows.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %22, %23
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup44

for.body:                                         ; preds = %for.cond
  %24 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  store i32 0, i32* %col, align 4, !tbaa !6
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %for.body
  %25 = load i32, i32* %col, align 4, !tbaa !6
  %26 = load i32, i32* %cols.addr, align 4, !tbaa !6
  %cmp16 = icmp ult i32 %25, %26
  br i1 %cmp16, label %for.body18, label %for.cond.cleanup17

for.cond.cleanup17:                               ; preds = %for.cond15
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup40

for.body18:                                       ; preds = %for.cond15
  %27 = bitcast i32* %boost to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  %28 = bitcast %union.rgba_as_int* %px to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  %rgba = bitcast %union.rgba_as_int* %px to %struct.rgba_pixel*
  %29 = load %struct.rgba_pixel**, %struct.rgba_pixel*** %pixels.addr, align 4, !tbaa !2
  %30 = load i32, i32* %row, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.rgba_pixel*, %struct.rgba_pixel** %29, i32 %30
  %31 = load %struct.rgba_pixel*, %struct.rgba_pixel** %arrayidx, align 4, !tbaa !2
  %32 = load i32, i32* %col, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %31, i32 %32
  %33 = bitcast %struct.rgba_pixel* %rgba to i8*
  %34 = bitcast %struct.rgba_pixel* %arrayidx19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 1 %34, i32 4, i1 false), !tbaa.struct !8
  %35 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %rgba20 = bitcast %union.rgba_as_int* %px to %struct.rgba_pixel*
  %a = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %rgba20, i32 0, i32 3
  %36 = load i8, i8* %a, align 1, !tbaa !9
  %tobool = icmp ne i8 %36, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %for.body18
  %l = bitcast %union.rgba_as_int* %px to i32*
  store i32 0, i32* %l, align 4, !tbaa !9
  store i32 0, i32* %hash, align 4, !tbaa !6
  store i32 2000, i32* %boost, align 4, !tbaa !6
  %37 = load i8*, i8** %importance_map.addr, align 4, !tbaa !2
  %tobool21 = icmp ne i8* %37, null
  br i1 %tobool21, label %if.then22, label %if.end

if.then22:                                        ; preds = %if.then
  %38 = load i8*, i8** %importance_map.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %38, i32 1
  store i8* %incdec.ptr, i8** %importance_map.addr, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then22, %if.then
  br label %if.end35

if.else:                                          ; preds = %for.body18
  %l23 = bitcast %union.rgba_as_int* %px to i32*
  %39 = load i32, i32* %l23, align 4, !tbaa !9
  %40 = load i32, i32* %posterize_mask, align 4, !tbaa !6
  %and = and i32 %39, %40
  %l24 = bitcast %union.rgba_as_int* %px to i32*
  %41 = load i32, i32* %l24, align 4, !tbaa !9
  %42 = load i32, i32* %posterize_high_mask, align 4, !tbaa !6
  %and25 = and i32 %41, %42
  %43 = load i32, i32* %ignorebits, align 4, !tbaa !6
  %sub = sub i32 8, %43
  %shr26 = lshr i32 %and25, %sub
  %or27 = or i32 %and, %shr26
  %l28 = bitcast %union.rgba_as_int* %px to i32*
  store i32 %or27, i32* %l28, align 4, !tbaa !9
  %l29 = bitcast %union.rgba_as_int* %px to i32*
  %44 = load i32, i32* %l29, align 4, !tbaa !9
  %45 = load i32, i32* %hash_size, align 4, !tbaa !6
  %rem = urem i32 %44, %45
  store i32 %rem, i32* %hash, align 4, !tbaa !6
  %46 = load i8*, i8** %importance_map.addr, align 4, !tbaa !2
  %tobool30 = icmp ne i8* %46, null
  br i1 %tobool30, label %if.then31, label %if.else33

if.then31:                                        ; preds = %if.else
  %47 = load i8*, i8** %importance_map.addr, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr32, i8** %importance_map.addr, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !9
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %boost, align 4, !tbaa !6
  br label %if.end34

if.else33:                                        ; preds = %if.else
  store i32 255, i32* %boost, align 4, !tbaa !6
  br label %if.end34

if.end34:                                         ; preds = %if.else33, %if.then31
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %if.end
  %49 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %50 = load i32, i32* %hash, align 4, !tbaa !6
  %51 = load i32, i32* %boost, align 4, !tbaa !6
  %52 = load i32, i32* %row, align 4, !tbaa !6
  %53 = load i32, i32* %rows.addr, align 4, !tbaa !6
  %call = call zeroext i1 @pam_add_to_hash(%struct.acolorhash_table* %49, i32 %50, i32 %51, %union.rgba_as_int* byval(%union.rgba_as_int) align 4 %px, i32 %52, i32 %53)
  br i1 %call, label %if.end37, label %if.then36

if.then36:                                        ; preds = %if.end35
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end37:                                         ; preds = %if.end35
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end37, %if.then36
  %54 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  %55 = bitcast %union.rgba_as_int* %px to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #6
  %56 = bitcast i32* %boost to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup40 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %57 = load i32, i32* %col, align 4, !tbaa !6
  %inc = add i32 %57, 1
  store i32 %inc, i32* %col, align 4, !tbaa !6
  br label %for.cond15

cleanup40:                                        ; preds = %cleanup, %for.cond.cleanup17
  %58 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #6
  %cleanup.dest41 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest41, label %cleanup44 [
    i32 5, label %for.end
  ]

for.end:                                          ; preds = %cleanup40
  br label %for.inc42

for.inc42:                                        ; preds = %for.end
  %59 = load i32, i32* %row, align 4, !tbaa !6
  %inc43 = add i32 %59, 1
  store i32 %inc43, i32* %row, align 4, !tbaa !6
  br label %for.cond

cleanup44:                                        ; preds = %cleanup40, %for.cond.cleanup
  %60 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #6
  %cleanup.dest45 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest45, label %cleanup49 [
    i32 2, label %for.end46
  ]

for.end46:                                        ; preds = %cleanup44
  %61 = load i32, i32* %cols.addr, align 4, !tbaa !6
  %62 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %cols47 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %62, i32 0, i32 4
  store i32 %61, i32* %cols47, align 4, !tbaa !6
  %63 = load i32, i32* %rows.addr, align 4, !tbaa !6
  %64 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %rows48 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %64, i32 0, i32 5
  %65 = load i32, i32* %rows48, align 4, !tbaa !6
  %add = add i32 %65, %63
  store i32 %add, i32* %rows48, align 4, !tbaa !6
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup49

cleanup49:                                        ; preds = %for.end46, %cleanup44
  %66 = bitcast i32* %hash_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = bitcast i32* %posterize_high_mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #6
  %68 = bitcast i32* %posterize_mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #6
  %69 = bitcast i32* %channel_hmask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #6
  %70 = bitcast i32* %channel_mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #6
  %71 = bitcast i32* %ignorebits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #6
  %72 = load i1, i1* %retval, align 1
  ret i1 %72
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden zeroext i1 @pam_add_to_hash(%struct.acolorhash_table* %acht, i32 %hash, i32 %boost, %union.rgba_as_int* byval(%union.rgba_as_int) align 4 %px, i32 %row, i32 %rows) #0 {
entry:
  %retval = alloca i1, align 1
  %acht.addr = alloca %struct.acolorhash_table*, align 4
  %hash.addr = alloca i32, align 4
  %boost.addr = alloca i32, align 4
  %row.addr = alloca i32, align 4
  %rows.addr = alloca i32, align 4
  %achl = alloca %struct.acolorhist_arr_head*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %other_items = alloca %struct.acolorhist_arr_item*, align 4
  %i = alloca i32, align 4
  %.compoundliteral = alloca %struct.acolorhist_arr_item, align 4
  %new_items = alloca %struct.acolorhist_arr_item*, align 4
  %capacity45 = alloca i32, align 4
  %mempool_size = alloca i32, align 4
  %stacksize = alloca i32, align 4
  %mempool_size76 = alloca i32, align 4
  %.compoundliteral103 = alloca %struct.acolorhist_arr_item, align 4
  store %struct.acolorhash_table* %acht, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  store i32 %hash, i32* %hash.addr, align 4, !tbaa !6
  store i32 %boost, i32* %boost.addr, align 4, !tbaa !6
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !6
  %0 = bitcast %struct.acolorhist_arr_head** %achl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %buckets = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %1, i32 0, i32 9
  %2 = load i32, i32* %hash.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [0 x %struct.acolorhist_arr_head], [0 x %struct.acolorhist_arr_head]* %buckets, i32 0, i32 %2
  store %struct.acolorhist_arr_head* %arrayidx, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %3 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline1 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %3, i32 0, i32 0
  %color = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %inline1, i32 0, i32 0
  %l = bitcast %union.rgba_as_int* %color to i32*
  %4 = load i32, i32* %l, align 4, !tbaa !9
  %l1 = bitcast %union.rgba_as_int* %px to i32*
  %5 = load i32, i32* %l1, align 4, !tbaa !9
  %cmp = icmp eq i32 %4, %5
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %6 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %6, i32 0, i32 2
  %7 = load i32, i32* %used, align 4, !tbaa !10
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %8 = load i32, i32* %boost.addr, align 4, !tbaa !6
  %9 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline12 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %9, i32 0, i32 0
  %perceptual_weight = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %inline12, i32 0, i32 1
  %10 = load i32, i32* %perceptual_weight, align 4, !tbaa !13
  %add = add i32 %10, %8
  store i32 %add, i32* %perceptual_weight, align 4, !tbaa !13
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup136

if.end:                                           ; preds = %land.lhs.true, %entry
  %11 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used3 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %11, i32 0, i32 2
  %12 = load i32, i32* %used3, align 4, !tbaa !10
  %tobool4 = icmp ne i32 %12, 0
  br i1 %tobool4, label %if.then5, label %if.else125

if.then5:                                         ; preds = %if.end
  %13 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used6 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %13, i32 0, i32 2
  %14 = load i32, i32* %used6, align 4, !tbaa !10
  %cmp7 = icmp ugt i32 %14, 1
  br i1 %cmp7, label %if.then8, label %if.else114

if.then8:                                         ; preds = %if.then5
  %15 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline2 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %15, i32 0, i32 1
  %color9 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %inline2, i32 0, i32 0
  %l10 = bitcast %union.rgba_as_int* %color9 to i32*
  %16 = load i32, i32* %l10, align 4, !tbaa !9
  %l11 = bitcast %union.rgba_as_int* %px to i32*
  %17 = load i32, i32* %l11, align 4, !tbaa !9
  %cmp12 = icmp eq i32 %16, %17
  br i1 %cmp12, label %if.then13, label %if.end17

if.then13:                                        ; preds = %if.then8
  %18 = load i32, i32* %boost.addr, align 4, !tbaa !6
  %19 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline214 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %19, i32 0, i32 1
  %perceptual_weight15 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %inline214, i32 0, i32 1
  %20 = load i32, i32* %perceptual_weight15, align 4, !tbaa !14
  %add16 = add i32 %20, %18
  store i32 %add16, i32* %perceptual_weight15, align 4, !tbaa !14
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup136

if.end17:                                         ; preds = %if.then8
  %21 = bitcast %struct.acolorhist_arr_item** %other_items to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %other_items18 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %22, i32 0, i32 4
  %23 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %other_items18, align 4, !tbaa !15
  store %struct.acolorhist_arr_item* %23, %struct.acolorhist_arr_item** %other_items, align 4, !tbaa !2
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end17
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %26 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used19 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %26, i32 0, i32 2
  %27 = load i32, i32* %used19, align 4, !tbaa !10
  %sub = sub i32 %27, 2
  %cmp20 = icmp ult i32 %25, %sub
  br i1 %cmp20, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %28 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %other_items, align 4, !tbaa !2
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %28, i32 %29
  %color22 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %arrayidx21, i32 0, i32 0
  %l23 = bitcast %union.rgba_as_int* %color22 to i32*
  %30 = load i32, i32* %l23, align 4, !tbaa !9
  %l24 = bitcast %union.rgba_as_int* %px to i32*
  %31 = load i32, i32* %l24, align 4, !tbaa !9
  %cmp25 = icmp eq i32 %30, %31
  br i1 %cmp25, label %if.then26, label %if.end30

if.then26:                                        ; preds = %for.body
  %32 = load i32, i32* %boost.addr, align 4, !tbaa !6
  %33 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %other_items, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx27 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %33, i32 %34
  %perceptual_weight28 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %arrayidx27, i32 0, i32 1
  %35 = load i32, i32* %perceptual_weight28, align 4, !tbaa !16
  %add29 = add i32 %35, %32
  store i32 %add29, i32* %perceptual_weight28, align 4, !tbaa !16
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup110

if.end30:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end30
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %38 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %capacity = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %38, i32 0, i32 3
  %39 = load i32, i32* %capacity, align 4, !tbaa !17
  %cmp31 = icmp ult i32 %37, %39
  br i1 %cmp31, label %if.then32, label %if.end39

if.then32:                                        ; preds = %for.end
  %40 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %other_items, align 4, !tbaa !2
  %41 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %40, i32 %41
  %color34 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %.compoundliteral, i32 0, i32 0
  %42 = bitcast %union.rgba_as_int* %color34 to i8*
  %43 = bitcast %union.rgba_as_int* %px to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 4, i1 false), !tbaa.struct !18
  %perceptual_weight35 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %.compoundliteral, i32 0, i32 1
  %44 = load i32, i32* %boost.addr, align 4, !tbaa !6
  store i32 %44, i32* %perceptual_weight35, align 4, !tbaa !16
  %45 = bitcast %struct.acolorhist_arr_item* %arrayidx33 to i8*
  %46 = bitcast %struct.acolorhist_arr_item* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 8, i1 false), !tbaa.struct !19
  %47 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used36 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %47, i32 0, i32 2
  %48 = load i32, i32* %used36, align 4, !tbaa !10
  %inc37 = add i32 %48, 1
  store i32 %inc37, i32* %used36, align 4, !tbaa !10
  %49 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %49, i32 0, i32 3
  %50 = load i32, i32* %colors, align 4, !tbaa !6
  %inc38 = add i32 %50, 1
  store i32 %inc38, i32* %colors, align 4, !tbaa !6
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup110

if.end39:                                         ; preds = %for.end
  %51 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors40 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %51, i32 0, i32 3
  %52 = load i32, i32* %colors40, align 4, !tbaa !6
  %inc41 = add i32 %52, 1
  store i32 %inc41, i32* %colors40, align 4, !tbaa !6
  %53 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %maxcolors = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %53, i32 0, i32 2
  %54 = load i32, i32* %maxcolors, align 4, !tbaa !6
  %cmp42 = icmp ugt i32 %inc41, %54
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %if.end39
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup110

if.end44:                                         ; preds = %if.end39
  %55 = bitcast %struct.acolorhist_arr_item** %new_items to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #6
  %56 = bitcast i32* %capacity45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #6
  %57 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %other_items, align 4, !tbaa !2
  %tobool46 = icmp ne %struct.acolorhist_arr_item* %57, null
  br i1 %tobool46, label %if.else64, label %if.then47

if.then47:                                        ; preds = %if.end44
  store i32 8, i32* %capacity45, align 4, !tbaa !6
  %58 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %freestackp = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %58, i32 0, i32 7
  %59 = load i32, i32* %freestackp, align 4, !tbaa !6
  %cmp48 = icmp ule i32 %59, 0
  br i1 %cmp48, label %if.then49, label %if.else

if.then49:                                        ; preds = %if.then47
  %60 = bitcast i32* %mempool_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #6
  %61 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %rows50 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %61, i32 0, i32 5
  %62 = load i32, i32* %rows50, align 4, !tbaa !6
  %63 = load i32, i32* %rows.addr, align 4, !tbaa !6
  %add51 = add i32 %62, %63
  %64 = load i32, i32* %row.addr, align 4, !tbaa !6
  %sub52 = sub i32 %add51, %64
  %mul = mul i32 %sub52, 2
  %65 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors53 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %65, i32 0, i32 3
  %66 = load i32, i32* %colors53, align 4, !tbaa !6
  %mul54 = mul i32 %mul, %66
  %67 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %rows55 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %67, i32 0, i32 5
  %68 = load i32, i32* %rows55, align 4, !tbaa !6
  %69 = load i32, i32* %row.addr, align 4, !tbaa !6
  %add56 = add i32 %68, %69
  %add57 = add i32 %add56, 1
  %div = udiv i32 %mul54, %add57
  %add58 = add i32 %div, 1024
  %mul59 = mul i32 %add58, 8
  store i32 %mul59, i32* %mempool_size, align 4, !tbaa !20
  %70 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %mempool = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %70, i32 0, i32 0
  %71 = load i32, i32* %capacity45, align 4, !tbaa !6
  %mul60 = mul i32 8, %71
  %72 = load i32, i32* %mempool_size, align 4, !tbaa !20
  %call = call i8* @mempool_alloc(%struct.mempool** %mempool, i32 %mul60, i32 %72)
  %73 = bitcast i8* %call to %struct.acolorhist_arr_item*
  store %struct.acolorhist_arr_item* %73, %struct.acolorhist_arr_item** %new_items, align 4, !tbaa !2
  %74 = bitcast i32* %mempool_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #6
  br label %if.end63

if.else:                                          ; preds = %if.then47
  %75 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %freestack = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %75, i32 0, i32 8
  %76 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %freestackp61 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %76, i32 0, i32 7
  %77 = load i32, i32* %freestackp61, align 4, !tbaa !6
  %dec = add i32 %77, -1
  store i32 %dec, i32* %freestackp61, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds [512 x %struct.acolorhist_arr_item*], [512 x %struct.acolorhist_arr_item*]* %freestack, i32 0, i32 %dec
  %78 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %arrayidx62, align 4, !tbaa !2
  store %struct.acolorhist_arr_item* %78, %struct.acolorhist_arr_item** %new_items, align 4, !tbaa !2
  br label %if.end63

if.end63:                                         ; preds = %if.else, %if.then49
  br label %if.end99

if.else64:                                        ; preds = %if.end44
  %79 = bitcast i32* %stacksize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  store i32 512, i32* %stacksize, align 4, !tbaa !6
  %80 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %capacity65 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %80, i32 0, i32 3
  %81 = load i32, i32* %capacity65, align 4, !tbaa !17
  %mul66 = mul i32 %81, 2
  %add67 = add i32 %mul66, 16
  store i32 %add67, i32* %capacity45, align 4, !tbaa !6
  %82 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %freestackp68 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %82, i32 0, i32 7
  %83 = load i32, i32* %freestackp68, align 4, !tbaa !6
  %cmp69 = icmp ult i32 %83, 511
  br i1 %cmp69, label %if.then70, label %if.end75

if.then70:                                        ; preds = %if.else64
  %84 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %other_items, align 4, !tbaa !2
  %85 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %freestack71 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %85, i32 0, i32 8
  %86 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %freestackp72 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %86, i32 0, i32 7
  %87 = load i32, i32* %freestackp72, align 4, !tbaa !6
  %inc73 = add i32 %87, 1
  store i32 %inc73, i32* %freestackp72, align 4, !tbaa !6
  %arrayidx74 = getelementptr inbounds [512 x %struct.acolorhist_arr_item*], [512 x %struct.acolorhist_arr_item*]* %freestack71, i32 0, i32 %87
  store %struct.acolorhist_arr_item* %84, %struct.acolorhist_arr_item** %arrayidx74, align 4, !tbaa !2
  br label %if.end75

if.end75:                                         ; preds = %if.then70, %if.else64
  %88 = bitcast i32* %mempool_size76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #6
  %89 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %rows77 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %89, i32 0, i32 5
  %90 = load i32, i32* %rows77, align 4, !tbaa !6
  %91 = load i32, i32* %rows.addr, align 4, !tbaa !6
  %add78 = add i32 %90, %91
  %92 = load i32, i32* %row.addr, align 4, !tbaa !6
  %sub79 = sub i32 %add78, %92
  %mul80 = mul i32 %sub79, 2
  %93 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors81 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %93, i32 0, i32 3
  %94 = load i32, i32* %colors81, align 4, !tbaa !6
  %mul82 = mul i32 %mul80, %94
  %95 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %rows83 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %95, i32 0, i32 5
  %96 = load i32, i32* %rows83, align 4, !tbaa !6
  %97 = load i32, i32* %row.addr, align 4, !tbaa !6
  %add84 = add i32 %96, %97
  %add85 = add i32 %add84, 1
  %div86 = udiv i32 %mul82, %add85
  %98 = load i32, i32* %capacity45, align 4, !tbaa !6
  %mul87 = mul i32 32, %98
  %add88 = add i32 %div86, %mul87
  %mul89 = mul i32 %add88, 8
  store i32 %mul89, i32* %mempool_size76, align 4, !tbaa !20
  %99 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %mempool90 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %99, i32 0, i32 0
  %100 = load i32, i32* %capacity45, align 4, !tbaa !6
  %mul91 = mul i32 8, %100
  %101 = load i32, i32* %mempool_size76, align 4, !tbaa !20
  %call92 = call i8* @mempool_alloc(%struct.mempool** %mempool90, i32 %mul91, i32 %101)
  %102 = bitcast i8* %call92 to %struct.acolorhist_arr_item*
  store %struct.acolorhist_arr_item* %102, %struct.acolorhist_arr_item** %new_items, align 4, !tbaa !2
  %103 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %new_items, align 4, !tbaa !2
  %tobool93 = icmp ne %struct.acolorhist_arr_item* %103, null
  br i1 %tobool93, label %if.end95, label %if.then94

if.then94:                                        ; preds = %if.end75
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end95:                                         ; preds = %if.end75
  %104 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %new_items, align 4, !tbaa !2
  %105 = bitcast %struct.acolorhist_arr_item* %104 to i8*
  %106 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %other_items, align 4, !tbaa !2
  %107 = bitcast %struct.acolorhist_arr_item* %106 to i8*
  %108 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %capacity96 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %108, i32 0, i32 3
  %109 = load i32, i32* %capacity96, align 4, !tbaa !17
  %mul97 = mul i32 8, %109
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %105, i8* align 4 %107, i32 %mul97, i1 false)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end95, %if.then94
  %110 = bitcast i32* %mempool_size76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #6
  %111 = bitcast i32* %stacksize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup108 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end99

if.end99:                                         ; preds = %cleanup.cont, %if.end63
  %112 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %new_items, align 4, !tbaa !2
  %113 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %other_items100 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %113, i32 0, i32 4
  store %struct.acolorhist_arr_item* %112, %struct.acolorhist_arr_item** %other_items100, align 4, !tbaa !15
  %114 = load i32, i32* %capacity45, align 4, !tbaa !6
  %115 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %capacity101 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %115, i32 0, i32 3
  store i32 %114, i32* %capacity101, align 4, !tbaa !17
  %116 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %new_items, align 4, !tbaa !2
  %117 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx102 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %116, i32 %117
  %color104 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %.compoundliteral103, i32 0, i32 0
  %118 = bitcast %union.rgba_as_int* %color104 to i8*
  %119 = bitcast %union.rgba_as_int* %px to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %118, i8* align 4 %119, i32 4, i1 false), !tbaa.struct !18
  %perceptual_weight105 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %.compoundliteral103, i32 0, i32 1
  %120 = load i32, i32* %boost.addr, align 4, !tbaa !6
  store i32 %120, i32* %perceptual_weight105, align 4, !tbaa !16
  %121 = bitcast %struct.acolorhist_arr_item* %arrayidx102 to i8*
  %122 = bitcast %struct.acolorhist_arr_item* %.compoundliteral103 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %121, i8* align 4 %122, i32 8, i1 false), !tbaa.struct !19
  %123 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used106 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %123, i32 0, i32 2
  %124 = load i32, i32* %used106, align 4, !tbaa !10
  %inc107 = add i32 %124, 1
  store i32 %inc107, i32* %used106, align 4, !tbaa !10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup108

cleanup108:                                       ; preds = %if.end99, %cleanup
  %125 = bitcast i32* %capacity45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #6
  %126 = bitcast %struct.acolorhist_arr_item** %new_items to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #6
  br label %cleanup110

cleanup110:                                       ; preds = %cleanup108, %if.then43, %if.then32, %if.then26
  %127 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #6
  %128 = bitcast %struct.acolorhist_arr_item** %other_items to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #6
  %cleanup.dest112 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest112, label %cleanup136 [
    i32 0, label %cleanup.cont113
  ]

cleanup.cont113:                                  ; preds = %cleanup110
  br label %if.end124

if.else114:                                       ; preds = %if.then5
  %l115 = bitcast %union.rgba_as_int* %px to i32*
  %129 = load i32, i32* %l115, align 4, !tbaa !9
  %130 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline2116 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %130, i32 0, i32 1
  %color117 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %inline2116, i32 0, i32 0
  %l118 = bitcast %union.rgba_as_int* %color117 to i32*
  store i32 %129, i32* %l118, align 4, !tbaa !9
  %131 = load i32, i32* %boost.addr, align 4, !tbaa !6
  %132 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline2119 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %132, i32 0, i32 1
  %perceptual_weight120 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %inline2119, i32 0, i32 1
  store i32 %131, i32* %perceptual_weight120, align 4, !tbaa !14
  %133 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used121 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %133, i32 0, i32 2
  store i32 2, i32* %used121, align 4, !tbaa !10
  %134 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors122 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %134, i32 0, i32 3
  %135 = load i32, i32* %colors122, align 4, !tbaa !6
  %inc123 = add i32 %135, 1
  store i32 %inc123, i32* %colors122, align 4, !tbaa !6
  br label %if.end124

if.end124:                                        ; preds = %if.else114, %cleanup.cont113
  br label %if.end135

if.else125:                                       ; preds = %if.end
  %l126 = bitcast %union.rgba_as_int* %px to i32*
  %136 = load i32, i32* %l126, align 4, !tbaa !9
  %137 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline1127 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %137, i32 0, i32 0
  %color128 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %inline1127, i32 0, i32 0
  %l129 = bitcast %union.rgba_as_int* %color128 to i32*
  store i32 %136, i32* %l129, align 4, !tbaa !9
  %138 = load i32, i32* %boost.addr, align 4, !tbaa !6
  %139 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline1130 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %139, i32 0, i32 0
  %perceptual_weight131 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %inline1130, i32 0, i32 1
  store i32 %138, i32* %perceptual_weight131, align 4, !tbaa !13
  %140 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used132 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %140, i32 0, i32 2
  store i32 1, i32* %used132, align 4, !tbaa !10
  %141 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors133 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %141, i32 0, i32 3
  %142 = load i32, i32* %colors133, align 4, !tbaa !6
  %inc134 = add i32 %142, 1
  store i32 %inc134, i32* %colors133, align 4, !tbaa !6
  br label %if.end135

if.end135:                                        ; preds = %if.else125, %if.end124
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup136

cleanup136:                                       ; preds = %if.end135, %cleanup110, %if.then13, %if.then
  %143 = bitcast %struct.acolorhist_arr_head** %achl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #6
  %144 = load i1, i1* %retval, align 1
  ret i1 %144
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare hidden i8* @mempool_alloc(%struct.mempool**, i32, i32) #2

; Function Attrs: nounwind
define hidden %struct.acolorhash_table* @pam_allocacolorhash(i32 %maxcolors, i32 %surface, i32 %ignorebits, i8* (i32)* %malloc, void (i8*)* %free) #0 {
entry:
  %retval = alloca %struct.acolorhash_table*, align 4
  %maxcolors.addr = alloca i32, align 4
  %surface.addr = alloca i32, align 4
  %ignorebits.addr = alloca i32, align 4
  %malloc.addr = alloca i8* (i32)*, align 4
  %free.addr = alloca void (i8*)*, align 4
  %estimated_colors = alloca i32, align 4
  %hash_size = alloca i32, align 4
  %m = alloca %struct.mempool*, align 4
  %buckets_size = alloca i32, align 4
  %mempool_size = alloca i32, align 4
  %t = alloca %struct.acolorhash_table*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %.compoundliteral = alloca %struct.acolorhash_table, align 4
  store i32 %maxcolors, i32* %maxcolors.addr, align 4, !tbaa !6
  store i32 %surface, i32* %surface.addr, align 4, !tbaa !6
  store i32 %ignorebits, i32* %ignorebits.addr, align 4, !tbaa !6
  store i8* (i32)* %malloc, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  store void (i8*)* %free, void (i8*)** %free.addr, align 4, !tbaa !2
  %0 = bitcast i32* %estimated_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %maxcolors.addr, align 4, !tbaa !6
  %2 = load i32, i32* %surface.addr, align 4, !tbaa !6
  %3 = load i32, i32* %ignorebits.addr, align 4, !tbaa !6
  %4 = load i32, i32* %surface.addr, align 4, !tbaa !6
  %cmp = icmp ugt i32 %4, 262144
  %5 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 6, i32 5
  %add = add i32 %3, %cond
  %div = udiv i32 %2, %add
  %cmp1 = icmp ult i32 %1, %div
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load i32, i32* %maxcolors.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %7 = load i32, i32* %surface.addr, align 4, !tbaa !6
  %8 = load i32, i32* %ignorebits.addr, align 4, !tbaa !6
  %9 = load i32, i32* %surface.addr, align 4, !tbaa !6
  %cmp2 = icmp ugt i32 %9, 262144
  %10 = zext i1 %cmp2 to i64
  %cond3 = select i1 %cmp2, i32 6, i32 5
  %add4 = add i32 %8, %cond3
  %div5 = udiv i32 %7, %add4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond6 = phi i32 [ %6, %cond.true ], [ %div5, %cond.false ]
  store i32 %cond6, i32* %estimated_colors, align 4, !tbaa !20
  %11 = bitcast i32* %hash_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  %12 = load i32, i32* %estimated_colors, align 4, !tbaa !20
  %cmp7 = icmp ult i32 %12, 66000
  br i1 %cmp7, label %cond.true8, label %cond.false9

cond.true8:                                       ; preds = %cond.end
  br label %cond.end12

cond.false9:                                      ; preds = %cond.end
  %13 = load i32, i32* %estimated_colors, align 4, !tbaa !20
  %cmp10 = icmp ult i32 %13, 200000
  %14 = zext i1 %cmp10 to i64
  %cond11 = select i1 %cmp10, i32 12011, i32 24019
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false9, %cond.true8
  %cond13 = phi i32 [ 6673, %cond.true8 ], [ %cond11, %cond.false9 ]
  store i32 %cond13, i32* %hash_size, align 4, !tbaa !20
  %15 = bitcast %struct.mempool** %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store %struct.mempool* null, %struct.mempool** %m, align 4, !tbaa !2
  %16 = bitcast i32* %buckets_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  %17 = load i32, i32* %hash_size, align 4, !tbaa !20
  %mul = mul i32 %17, 28
  store i32 %mul, i32* %buckets_size, align 4, !tbaa !20
  %18 = bitcast i32* %mempool_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  %19 = load i32, i32* %buckets_size, align 4, !tbaa !20
  %add14 = add i32 2080, %19
  %20 = load i32, i32* %estimated_colors, align 4, !tbaa !20
  %mul15 = mul i32 %20, 8
  %add16 = add i32 %add14, %mul15
  store i32 %add16, i32* %mempool_size, align 4, !tbaa !20
  %21 = bitcast %struct.acolorhash_table** %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  %22 = load i32, i32* %buckets_size, align 4, !tbaa !20
  %add17 = add i32 2080, %22
  %23 = load i32, i32* %mempool_size, align 4, !tbaa !20
  %24 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  %25 = load void (i8*)*, void (i8*)** %free.addr, align 4, !tbaa !2
  %call = call i8* @mempool_create(%struct.mempool** %m, i32 %add17, i32 %23, i8* (i32)* %24, void (i8*)* %25)
  %26 = bitcast i8* %call to %struct.acolorhash_table*
  store %struct.acolorhash_table* %26, %struct.acolorhash_table** %t, align 4, !tbaa !2
  %27 = load %struct.acolorhash_table*, %struct.acolorhash_table** %t, align 4, !tbaa !2
  %tobool = icmp ne %struct.acolorhash_table* %27, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %cond.end12
  store %struct.acolorhash_table* null, %struct.acolorhash_table** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %cond.end12
  %28 = load %struct.acolorhash_table*, %struct.acolorhash_table** %t, align 4, !tbaa !2
  %29 = bitcast %struct.acolorhash_table* %.compoundliteral to i8*
  call void @llvm.memset.p0i8.i64(i8* align 4 %29, i8 0, i64 2080, i1 false)
  %mempool = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %.compoundliteral, i32 0, i32 0
  %30 = load %struct.mempool*, %struct.mempool** %m, align 4, !tbaa !2
  store %struct.mempool* %30, %struct.mempool** %mempool, align 4, !tbaa !2
  %ignorebits18 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %.compoundliteral, i32 0, i32 1
  %31 = load i32, i32* %ignorebits.addr, align 4, !tbaa !6
  store i32 %31, i32* %ignorebits18, align 4, !tbaa !6
  %maxcolors19 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %.compoundliteral, i32 0, i32 2
  %32 = load i32, i32* %maxcolors.addr, align 4, !tbaa !6
  store i32 %32, i32* %maxcolors19, align 4, !tbaa !6
  %hash_size20 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %.compoundliteral, i32 0, i32 6
  %33 = load i32, i32* %hash_size, align 4, !tbaa !20
  store i32 %33, i32* %hash_size20, align 4, !tbaa !6
  %34 = bitcast %struct.acolorhash_table* %28 to i8*
  %35 = bitcast %struct.acolorhash_table* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 2080, i1 false)
  %36 = load %struct.acolorhash_table*, %struct.acolorhash_table** %t, align 4, !tbaa !2
  %buckets = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %36, i32 0, i32 9
  %arraydecay = getelementptr inbounds [0 x %struct.acolorhist_arr_head], [0 x %struct.acolorhist_arr_head]* %buckets, i32 0, i32 0
  %37 = bitcast %struct.acolorhist_arr_head* %arraydecay to i8*
  %38 = load i32, i32* %buckets_size, align 4, !tbaa !20
  call void @llvm.memset.p0i8.i32(i8* align 4 %37, i8 0, i32 %38, i1 false)
  %39 = load %struct.acolorhash_table*, %struct.acolorhash_table** %t, align 4, !tbaa !2
  store %struct.acolorhash_table* %39, %struct.acolorhash_table** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %40 = bitcast %struct.acolorhash_table** %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #6
  %41 = bitcast i32* %mempool_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #6
  %42 = bitcast i32* %buckets_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #6
  %43 = bitcast %struct.mempool** %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #6
  %44 = bitcast i32* %hash_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #6
  %45 = bitcast i32* %estimated_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #6
  %46 = load %struct.acolorhash_table*, %struct.acolorhash_table** %retval, align 4
  ret %struct.acolorhash_table* %46
}

declare hidden i8* @mempool_create(%struct.mempool**, i32, i32, i8* (i32)*, void (i8*)*) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i1 immarg) #3

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: nounwind
define hidden %struct.histogram* @pam_acolorhashtoacolorhist(%struct.acolorhash_table* %acht, double %gamma, i8* (i32)* %malloc, void (i8*)* %free) #0 {
entry:
  %retval = alloca %struct.histogram*, align 4
  %acht.addr = alloca %struct.acolorhash_table*, align 4
  %gamma.addr = alloca double, align 8
  %malloc.addr = alloca i8* (i32)*, align 4
  %free.addr = alloca void (i8*)*, align 4
  %hist = alloca %struct.histogram*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %.compoundliteral = alloca %struct.histogram, align 8
  %gamma_lut = alloca [256 x float], align 16
  %max_perceptual_weight = alloca float, align 4
  %total_weight = alloca double, align 8
  %j = alloca i32, align 4
  %i = alloca i32, align 4
  %achl = alloca %struct.acolorhist_arr_head*, align 4
  %k = alloca i32, align 4
  store %struct.acolorhash_table* %acht, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  store double %gamma, double* %gamma.addr, align 8, !tbaa !22
  store i8* (i32)* %malloc, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  store void (i8*)* %free, void (i8*)** %free.addr, align 4, !tbaa !2
  %0 = bitcast %struct.histogram** %hist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  %call = call i8* %1(i32 24)
  %2 = bitcast i8* %call to %struct.histogram*
  store %struct.histogram* %2, %struct.histogram** %hist, align 4, !tbaa !2
  %3 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %tobool = icmp ne %struct.histogram* %3, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %4 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.acolorhash_table* %4, null
  br i1 %tobool1, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %struct.histogram* null, %struct.histogram** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

if.end:                                           ; preds = %lor.lhs.false
  %5 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %achv = getelementptr inbounds %struct.histogram, %struct.histogram* %.compoundliteral, i32 0, i32 0
  %6 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  %7 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %7, i32 0, i32 3
  %8 = load i32, i32* %colors, align 4, !tbaa !6
  %cmp = icmp ugt i32 1, %8
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %9 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors2 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %9, i32 0, i32 3
  %10 = load i32, i32* %colors2, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %10, %cond.false ]
  %mul = mul i32 %cond, 32
  %call3 = call i8* %6(i32 %mul)
  %11 = bitcast i8* %call3 to %struct.hist_item*
  store %struct.hist_item* %11, %struct.hist_item** %achv, align 8, !tbaa !24
  %free4 = getelementptr inbounds %struct.histogram, %struct.histogram* %.compoundliteral, i32 0, i32 1
  %12 = load void (i8*)*, void (i8*)** %free.addr, align 4, !tbaa !2
  store void (i8*)* %12, void (i8*)** %free4, align 4, !tbaa !26
  %total_perceptual_weight = getelementptr inbounds %struct.histogram, %struct.histogram* %.compoundliteral, i32 0, i32 2
  store double 0.000000e+00, double* %total_perceptual_weight, align 8, !tbaa !27
  %size = getelementptr inbounds %struct.histogram, %struct.histogram* %.compoundliteral, i32 0, i32 3
  %13 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %colors5 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %13, i32 0, i32 3
  %14 = load i32, i32* %colors5, align 4, !tbaa !6
  store i32 %14, i32* %size, align 8, !tbaa !28
  %ignorebits = getelementptr inbounds %struct.histogram, %struct.histogram* %.compoundliteral, i32 0, i32 4
  %15 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %ignorebits6 = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %15, i32 0, i32 1
  %16 = load i32, i32* %ignorebits6, align 4, !tbaa !6
  store i32 %16, i32* %ignorebits, align 4, !tbaa !29
  %17 = bitcast %struct.histogram* %5 to i8*
  %18 = bitcast %struct.histogram* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %17, i8* align 8 %18, i32 24, i1 false), !tbaa.struct !30
  %19 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %achv7 = getelementptr inbounds %struct.histogram, %struct.histogram* %19, i32 0, i32 0
  %20 = load %struct.hist_item*, %struct.hist_item** %achv7, align 8, !tbaa !24
  %tobool8 = icmp ne %struct.hist_item* %20, null
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %cond.end
  store %struct.histogram* null, %struct.histogram** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

if.end10:                                         ; preds = %cond.end
  %21 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %21) #6
  %arraydecay = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %22 = load double, double* %gamma.addr, align 8, !tbaa !22
  call void @to_f_set_gamma(float* %arraydecay, double %22)
  %23 = bitcast float* %max_perceptual_weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  %24 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %cols = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %24, i32 0, i32 4
  %25 = load i32, i32* %cols, align 4, !tbaa !6
  %conv = uitofp i32 %25 to float
  %mul11 = fmul float 0x3FB99999A0000000, %conv
  %26 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %rows = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %26, i32 0, i32 5
  %27 = load i32, i32* %rows, align 4, !tbaa !6
  %conv12 = uitofp i32 %27 to float
  %mul13 = fmul float %mul11, %conv12
  store float %mul13, float* %max_perceptual_weight, align 4, !tbaa !31
  %28 = bitcast double* %total_weight to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %28) #6
  store double 0.000000e+00, double* %total_weight, align 8, !tbaa !22
  %29 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  store i32 0, i32* %j, align 4, !tbaa !6
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc45, %if.end10
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %hash_size = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %32, i32 0, i32 6
  %33 = load i32, i32* %hash_size, align 4, !tbaa !6
  %cmp14 = icmp ult i32 %31, %33
  br i1 %cmp14, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #6
  br label %for.end47

for.body:                                         ; preds = %for.cond
  %35 = bitcast %struct.acolorhist_arr_head** %achl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  %36 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %buckets = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %36, i32 0, i32 9
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [0 x %struct.acolorhist_arr_head], [0 x %struct.acolorhist_arr_head]* %buckets, i32 0, i32 %37
  store %struct.acolorhist_arr_head* %arrayidx, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %38 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %38, i32 0, i32 2
  %39 = load i32, i32* %used, align 4, !tbaa !10
  %tobool16 = icmp ne i32 %39, 0
  br i1 %tobool16, label %if.then17, label %if.end44

if.then17:                                        ; preds = %for.body
  %arraydecay18 = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %40 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %achv19 = getelementptr inbounds %struct.histogram, %struct.histogram* %40, i32 0, i32 0
  %41 = load %struct.hist_item*, %struct.hist_item** %achv19, align 8, !tbaa !24
  %42 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline1 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %42, i32 0, i32 0
  %43 = load float, float* %max_perceptual_weight, align 4, !tbaa !31
  %call20 = call float @pam_add_to_hist(float* %arraydecay18, %struct.hist_item* %41, i32* %j, %struct.acolorhist_arr_item* %inline1, float %43)
  %conv21 = fpext float %call20 to double
  %44 = load double, double* %total_weight, align 8, !tbaa !22
  %add = fadd double %44, %conv21
  store double %add, double* %total_weight, align 8, !tbaa !22
  %45 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used22 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %45, i32 0, i32 2
  %46 = load i32, i32* %used22, align 4, !tbaa !10
  %cmp23 = icmp ugt i32 %46, 1
  br i1 %cmp23, label %if.then25, label %if.end43

if.then25:                                        ; preds = %if.then17
  %arraydecay26 = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %47 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %achv27 = getelementptr inbounds %struct.histogram, %struct.histogram* %47, i32 0, i32 0
  %48 = load %struct.hist_item*, %struct.hist_item** %achv27, align 8, !tbaa !24
  %49 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %inline2 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %49, i32 0, i32 1
  %50 = load float, float* %max_perceptual_weight, align 4, !tbaa !31
  %call28 = call float @pam_add_to_hist(float* %arraydecay26, %struct.hist_item* %48, i32* %j, %struct.acolorhist_arr_item* %inline2, float %50)
  %conv29 = fpext float %call28 to double
  %51 = load double, double* %total_weight, align 8, !tbaa !22
  %add30 = fadd double %51, %conv29
  store double %add30, double* %total_weight, align 8, !tbaa !22
  %52 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  store i32 0, i32* %k, align 4, !tbaa !6
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc, %if.then25
  %53 = load i32, i32* %k, align 4, !tbaa !6
  %54 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %used32 = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %54, i32 0, i32 2
  %55 = load i32, i32* %used32, align 4, !tbaa !10
  %sub = sub i32 %55, 2
  %cmp33 = icmp ult i32 %53, %sub
  br i1 %cmp33, label %for.body36, label %for.cond.cleanup35

for.cond.cleanup35:                               ; preds = %for.cond31
  store i32 5, i32* %cleanup.dest.slot, align 4
  %56 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #6
  br label %for.end

for.body36:                                       ; preds = %for.cond31
  %arraydecay37 = getelementptr inbounds [256 x float], [256 x float]* %gamma_lut, i32 0, i32 0
  %57 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %achv38 = getelementptr inbounds %struct.histogram, %struct.histogram* %57, i32 0, i32 0
  %58 = load %struct.hist_item*, %struct.hist_item** %achv38, align 8, !tbaa !24
  %59 = load %struct.acolorhist_arr_head*, %struct.acolorhist_arr_head** %achl, align 4, !tbaa !2
  %other_items = getelementptr inbounds %struct.acolorhist_arr_head, %struct.acolorhist_arr_head* %59, i32 0, i32 4
  %60 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %other_items, align 4, !tbaa !15
  %61 = load i32, i32* %k, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %60, i32 %61
  %62 = load float, float* %max_perceptual_weight, align 4, !tbaa !31
  %call40 = call float @pam_add_to_hist(float* %arraydecay37, %struct.hist_item* %58, i32* %j, %struct.acolorhist_arr_item* %arrayidx39, float %62)
  %conv41 = fpext float %call40 to double
  %63 = load double, double* %total_weight, align 8, !tbaa !22
  %add42 = fadd double %63, %conv41
  store double %add42, double* %total_weight, align 8, !tbaa !22
  br label %for.inc

for.inc:                                          ; preds = %for.body36
  %64 = load i32, i32* %k, align 4, !tbaa !6
  %inc = add i32 %64, 1
  store i32 %inc, i32* %k, align 4, !tbaa !6
  br label %for.cond31

for.end:                                          ; preds = %for.cond.cleanup35
  br label %if.end43

if.end43:                                         ; preds = %for.end, %if.then17
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %for.body
  %65 = bitcast %struct.acolorhist_arr_head** %achl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #6
  br label %for.inc45

for.inc45:                                        ; preds = %if.end44
  %66 = load i32, i32* %i, align 4, !tbaa !6
  %inc46 = add i32 %66, 1
  store i32 %inc46, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end47:                                        ; preds = %for.cond.cleanup
  %67 = load i32, i32* %j, align 4, !tbaa !6
  %68 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %size48 = getelementptr inbounds %struct.histogram, %struct.histogram* %68, i32 0, i32 3
  store i32 %67, i32* %size48, align 8, !tbaa !28
  %69 = load double, double* %total_weight, align 8, !tbaa !22
  %70 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  %total_perceptual_weight49 = getelementptr inbounds %struct.histogram, %struct.histogram* %70, i32 0, i32 2
  store double %69, double* %total_perceptual_weight49, align 8, !tbaa !27
  %71 = load i32, i32* %j, align 4, !tbaa !6
  %tobool50 = icmp ne i32 %71, 0
  br i1 %tobool50, label %if.end52, label %if.then51

if.then51:                                        ; preds = %for.end47
  %72 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  call void @pam_freeacolorhist(%struct.histogram* %72)
  store %struct.histogram* null, %struct.histogram** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end52:                                         ; preds = %for.end47
  %73 = load %struct.histogram*, %struct.histogram** %hist, align 4, !tbaa !2
  store %struct.histogram* %73, %struct.histogram** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end52, %if.then51
  %74 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #6
  %75 = bitcast double* %total_weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %75) #6
  %76 = bitcast float* %max_perceptual_weight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #6
  %77 = bitcast [256 x float]* %gamma_lut to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %77) #6
  br label %cleanup56

cleanup56:                                        ; preds = %cleanup, %if.then9, %if.then
  %78 = bitcast %struct.histogram** %hist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #6
  %79 = load %struct.histogram*, %struct.histogram** %retval, align 4
  ret %struct.histogram* %79
}

; Function Attrs: nounwind
define hidden void @to_f_set_gamma(float* %gamma_lut, double %gamma) #0 {
entry:
  %gamma_lut.addr = alloca float*, align 4
  %gamma.addr = alloca double, align 8
  %i = alloca i32, align 4
  store float* %gamma_lut, float** %gamma_lut.addr, align 4, !tbaa !2
  store double %gamma, double* %gamma.addr, align 8, !tbaa !22
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 256
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %conv = sitofp i32 %3 to double
  %div = fdiv double %conv, 2.550000e+02
  %4 = load double, double* %gamma.addr, align 8, !tbaa !22
  %div1 = fdiv double 0x3FE198C7E0000000, %4
  %5 = call double @llvm.pow.f64(double %div, double %div1)
  %conv2 = fptrunc double %5 to float
  %6 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %6, i32 %7
  store float %conv2, float* %arrayidx, align 4, !tbaa !31
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal float @pam_add_to_hist(float* %gamma_lut, %struct.hist_item* %achv, i32* %j, %struct.acolorhist_arr_item* %entry1, float %max_perceptual_weight) #4 {
entry:
  %retval = alloca float, align 4
  %gamma_lut.addr = alloca float*, align 4
  %achv.addr = alloca %struct.hist_item*, align 4
  %j.addr = alloca i32*, align 4
  %entry.addr = alloca %struct.acolorhist_arr_item*, align 4
  %max_perceptual_weight.addr = alloca float, align 4
  %w = alloca float, align 4
  %tmp = alloca %struct.f_pixel, align 4
  store float* %gamma_lut, float** %gamma_lut.addr, align 4, !tbaa !2
  store %struct.hist_item* %achv, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  store i32* %j, i32** %j.addr, align 4, !tbaa !2
  store %struct.acolorhist_arr_item* %entry1, %struct.acolorhist_arr_item** %entry.addr, align 4, !tbaa !2
  store float %max_perceptual_weight, float* %max_perceptual_weight.addr, align 4, !tbaa !31
  %0 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %entry.addr, align 4, !tbaa !2
  %perceptual_weight = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %0, i32 0, i32 1
  %1 = load i32, i32* %perceptual_weight, align 4, !tbaa !16
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast float* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %3 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %entry.addr, align 4, !tbaa !2
  %perceptual_weight2 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %3, i32 0, i32 1
  %4 = load i32, i32* %perceptual_weight2, align 4, !tbaa !16
  %conv = uitofp i32 %4 to float
  %div = fdiv float %conv, 1.280000e+02
  %5 = load float, float* %max_perceptual_weight.addr, align 4, !tbaa !31
  %cmp3 = fcmp olt float %div, %5
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %6 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %entry.addr, align 4, !tbaa !2
  %perceptual_weight5 = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %6, i32 0, i32 1
  %7 = load i32, i32* %perceptual_weight5, align 4, !tbaa !16
  %conv6 = uitofp i32 %7 to float
  %div7 = fdiv float %conv6, 1.280000e+02
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %8 = load float, float* %max_perceptual_weight.addr, align 4, !tbaa !31
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div7, %cond.true ], [ %8, %cond.false ]
  store float %cond, float* %w, align 4, !tbaa !31
  %9 = load float, float* %w, align 4, !tbaa !31
  %10 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %11 = load i32*, i32** %j.addr, align 4, !tbaa !2
  %12 = load i32, i32* %11, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %10, i32 %12
  %perceptual_weight8 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 2
  store float %9, float* %perceptual_weight8, align 4, !tbaa !33
  %13 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %14 = load i32*, i32** %j.addr, align 4, !tbaa !2
  %15 = load i32, i32* %14, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %13, i32 %15
  %adjusted_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx9, i32 0, i32 1
  store float %9, float* %adjusted_weight, align 4, !tbaa !36
  %16 = load %struct.hist_item*, %struct.hist_item** %achv.addr, align 4, !tbaa !2
  %17 = load i32*, i32** %j.addr, align 4, !tbaa !2
  %18 = load i32, i32* %17, align 4, !tbaa !6
  %arrayidx10 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %16, i32 %18
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx10, i32 0, i32 0
  %19 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #6
  %20 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %21 = load %struct.acolorhist_arr_item*, %struct.acolorhist_arr_item** %entry.addr, align 4, !tbaa !2
  %color = getelementptr inbounds %struct.acolorhist_arr_item, %struct.acolorhist_arr_item* %21, i32 0, i32 0
  %rgba = bitcast %union.rgba_as_int* %color to %struct.rgba_pixel*
  call void @rgba_to_f(%struct.f_pixel* sret align 4 %tmp, float* %20, %struct.rgba_pixel* byval(%struct.rgba_pixel) align 1 %rgba)
  %22 = bitcast %struct.f_pixel* %acolor to i8*
  %23 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false), !tbaa.struct !37
  %24 = bitcast %struct.f_pixel* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #6
  %25 = load i32*, i32** %j.addr, align 4, !tbaa !2
  %26 = load i32, i32* %25, align 4, !tbaa !6
  %add = add i32 %26, 1
  store i32 %add, i32* %25, align 4, !tbaa !6
  %27 = load float, float* %w, align 4, !tbaa !31
  store float %27, float* %retval, align 4
  %28 = bitcast float* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #6
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %29 = load float, float* %retval, align 4
  ret float %29
}

; Function Attrs: nounwind
define hidden void @pam_freeacolorhist(%struct.histogram* %hist) #0 {
entry:
  %hist.addr = alloca %struct.histogram*, align 4
  store %struct.histogram* %hist, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %0 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.histogram, %struct.histogram* %0, i32 0, i32 1
  %1 = load void (i8*)*, void (i8*)** %free, align 4, !tbaa !26
  %2 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv = getelementptr inbounds %struct.histogram, %struct.histogram* %2, i32 0, i32 0
  %3 = load %struct.hist_item*, %struct.hist_item** %achv, align 8, !tbaa !24
  %4 = bitcast %struct.hist_item* %3 to i8*
  call void %1(i8* %4)
  %5 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %free1 = getelementptr inbounds %struct.histogram, %struct.histogram* %5, i32 0, i32 1
  %6 = load void (i8*)*, void (i8*)** %free1, align 4, !tbaa !26
  %7 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %8 = bitcast %struct.histogram* %7 to i8*
  call void %6(i8* %8)
  ret void
}

; Function Attrs: nounwind
define hidden void @pam_freeacolorhash(%struct.acolorhash_table* %acht) #0 {
entry:
  %acht.addr = alloca %struct.acolorhash_table*, align 4
  store %struct.acolorhash_table* %acht, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %0 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.acolorhash_table* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.acolorhash_table*, %struct.acolorhash_table** %acht.addr, align 4, !tbaa !2
  %mempool = getelementptr inbounds %struct.acolorhash_table, %struct.acolorhash_table* %1, i32 0, i32 0
  %2 = load %struct.mempool*, %struct.mempool** %mempool, align 4, !tbaa !2
  call void @mempool_destroy(%struct.mempool* %2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare hidden void @mempool_destroy(%struct.mempool*) #2

; Function Attrs: nounwind
define hidden %struct.colormap* @pam_colormap(i32 %colors, i8* (i32)* %malloc, void (i8*)* %free) #0 {
entry:
  %retval = alloca %struct.colormap*, align 4
  %colors.addr = alloca i32, align 4
  %malloc.addr = alloca i8* (i32)*, align 4
  %free.addr = alloca void (i8*)*, align 4
  %map = alloca %struct.colormap*, align 4
  %colors_size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %.compoundliteral = alloca %struct.colormap, align 4
  store i32 %colors, i32* %colors.addr, align 4, !tbaa !6
  store i8* (i32)* %malloc, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  store void (i8*)* %free, void (i8*)** %free.addr, align 4, !tbaa !2
  %0 = bitcast %struct.colormap** %map to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = bitcast i32* %colors_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %colors.addr, align 4, !tbaa !6
  %mul = mul i32 %2, 24
  store i32 %mul, i32* %colors_size, align 4, !tbaa !20
  %3 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  %4 = load i32, i32* %colors_size, align 4, !tbaa !20
  %add = add i32 12, %4
  %call = call i8* %3(i32 %add)
  %5 = bitcast i8* %call to %struct.colormap*
  store %struct.colormap* %5, %struct.colormap** %map, align 4, !tbaa !2
  %6 = load %struct.colormap*, %struct.colormap** %map, align 4, !tbaa !2
  %tobool = icmp ne %struct.colormap* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store %struct.colormap* null, %struct.colormap** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %7 = load %struct.colormap*, %struct.colormap** %map, align 4, !tbaa !2
  %colors1 = getelementptr inbounds %struct.colormap, %struct.colormap* %.compoundliteral, i32 0, i32 0
  %8 = load i32, i32* %colors.addr, align 4, !tbaa !6
  store i32 %8, i32* %colors1, align 4, !tbaa !6
  %malloc2 = getelementptr inbounds %struct.colormap, %struct.colormap* %.compoundliteral, i32 0, i32 1
  %9 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  store i8* (i32)* %9, i8* (i32)** %malloc2, align 4, !tbaa !2
  %free3 = getelementptr inbounds %struct.colormap, %struct.colormap* %.compoundliteral, i32 0, i32 2
  %10 = load void (i8*)*, void (i8*)** %free.addr, align 4, !tbaa !2
  store void (i8*)* %10, void (i8*)** %free3, align 4, !tbaa !2
  %11 = bitcast %struct.colormap* %7 to i8*
  %12 = bitcast %struct.colormap* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 12, i1 false)
  %13 = load %struct.colormap*, %struct.colormap** %map, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %13, i32 0, i32 3
  %arraydecay = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 0
  %14 = bitcast %struct.colormap_item* %arraydecay to i8*
  %15 = load i32, i32* %colors_size, align 4, !tbaa !20
  call void @llvm.memset.p0i8.i32(i8* align 4 %14, i8 0, i32 %15, i1 false)
  %16 = load %struct.colormap*, %struct.colormap** %map, align 4, !tbaa !2
  store %struct.colormap* %16, %struct.colormap** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %17 = bitcast i32* %colors_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  %18 = bitcast %struct.colormap** %map to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #6
  %19 = load %struct.colormap*, %struct.colormap** %retval, align 4
  ret %struct.colormap* %19
}

; Function Attrs: nounwind
define hidden %struct.colormap* @pam_duplicate_colormap(%struct.colormap* %map) #0 {
entry:
  %map.addr = alloca %struct.colormap*, align 4
  %dupe = alloca %struct.colormap*, align 4
  %i = alloca i32, align 4
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  %0 = bitcast %struct.colormap** %dupe to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %1, i32 0, i32 0
  %2 = load i32, i32* %colors, align 4, !tbaa !6
  %3 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.colormap, %struct.colormap* %3, i32 0, i32 1
  %4 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !2
  %5 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.colormap, %struct.colormap* %5, i32 0, i32 2
  %6 = load void (i8*)*, void (i8*)** %free, align 4, !tbaa !2
  %call = call %struct.colormap* @pam_colormap(i32 %2, i8* (i32)* %4, void (i8*)* %6)
  store %struct.colormap* %call, %struct.colormap** %dupe, align 4, !tbaa !2
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %9 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors1 = getelementptr inbounds %struct.colormap, %struct.colormap* %9, i32 0, i32 0
  %10 = load i32, i32* %colors1, align 4, !tbaa !6
  %cmp = icmp ult i32 %8, %10
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %struct.colormap*, %struct.colormap** %dupe, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %12, i32 0, i32 3
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 %13
  %14 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette2 = getelementptr inbounds %struct.colormap, %struct.colormap* %14, i32 0, i32 3
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette2, i32 0, i32 %15
  %16 = bitcast %struct.colormap_item* %arrayidx to i8*
  %17 = bitcast %struct.colormap_item* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 24, i1 false), !tbaa.struct !38
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %19 = load %struct.colormap*, %struct.colormap** %dupe, align 4, !tbaa !2
  %20 = bitcast %struct.colormap** %dupe to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #6
  ret %struct.colormap* %19
}

; Function Attrs: nounwind
define hidden void @pam_freecolormap(%struct.colormap* %c) #0 {
entry:
  %c.addr = alloca %struct.colormap*, align 4
  store %struct.colormap* %c, %struct.colormap** %c.addr, align 4, !tbaa !2
  %0 = load %struct.colormap*, %struct.colormap** %c.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.colormap, %struct.colormap* %0, i32 0, i32 2
  %1 = load void (i8*)*, void (i8*)** %free, align 4, !tbaa !2
  %2 = load %struct.colormap*, %struct.colormap** %c.addr, align 4, !tbaa !2
  %3 = bitcast %struct.colormap* %2 to i8*
  call void %1(i8* %3)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #5

; Function Attrs: alwaysinline nounwind
define internal void @rgba_to_f(%struct.f_pixel* noalias sret align 4 %agg.result, float* %gamma_lut, %struct.rgba_pixel* byval(%struct.rgba_pixel) align 1 %px) #4 {
entry:
  %gamma_lut.addr = alloca float*, align 4
  %a = alloca float, align 4
  store float* %gamma_lut, float** %gamma_lut.addr, align 4, !tbaa !2
  %0 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %a1 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 3
  %1 = load i8, i8* %a1, align 1, !tbaa !41
  %conv = zext i8 %1 to i32
  %conv2 = sitofp i32 %conv to float
  %div = fdiv float %conv2, 2.550000e+02
  store float %div, float* %a, align 4, !tbaa !31
  %a3 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 0
  %2 = load float, float* %a, align 4, !tbaa !31
  store float %2, float* %a3, align 4, !tbaa !43
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 1
  %3 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %r4 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 0
  %4 = load i8, i8* %r4, align 1, !tbaa !44
  %idxprom = zext i8 %4 to i32
  %arrayidx = getelementptr inbounds float, float* %3, i32 %idxprom
  %5 = load float, float* %arrayidx, align 4, !tbaa !31
  %6 = load float, float* %a, align 4, !tbaa !31
  %mul = fmul float %5, %6
  store float %mul, float* %r, align 4, !tbaa !45
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 2
  %7 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %g5 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 1
  %8 = load i8, i8* %g5, align 1, !tbaa !46
  %idxprom6 = zext i8 %8 to i32
  %arrayidx7 = getelementptr inbounds float, float* %7, i32 %idxprom6
  %9 = load float, float* %arrayidx7, align 4, !tbaa !31
  %10 = load float, float* %a, align 4, !tbaa !31
  %mul8 = fmul float %9, %10
  store float %mul8, float* %g, align 4, !tbaa !47
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %agg.result, i32 0, i32 3
  %11 = load float*, float** %gamma_lut.addr, align 4, !tbaa !2
  %b9 = getelementptr inbounds %struct.rgba_pixel, %struct.rgba_pixel* %px, i32 0, i32 2
  %12 = load i8, i8* %b9, align 1, !tbaa !48
  %idxprom10 = zext i8 %12 to i32
  %arrayidx11 = getelementptr inbounds float, float* %11, i32 %idxprom10
  %13 = load float, float* %arrayidx11, align 4, !tbaa !31
  %14 = load float, float* %a, align 4, !tbaa !31
  %mul12 = fmul float %13, %14
  store float %mul12, float* %b, align 4, !tbaa !49
  %15 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{i64 0, i64 1, !9, i64 1, i64 1, !9, i64 2, i64 1, !9, i64 3, i64 1, !9}
!9 = !{!4, !4, i64 0}
!10 = !{!11, !7, i64 16}
!11 = !{!"acolorhist_arr_head", !12, i64 0, !12, i64 8, !7, i64 16, !7, i64 20, !3, i64 24}
!12 = !{!"acolorhist_arr_item", !4, i64 0, !7, i64 4}
!13 = !{!11, !7, i64 4}
!14 = !{!11, !7, i64 12}
!15 = !{!11, !3, i64 24}
!16 = !{!12, !7, i64 4}
!17 = !{!11, !7, i64 20}
!18 = !{i64 0, i64 1, !9, i64 1, i64 1, !9, i64 2, i64 1, !9, i64 3, i64 1, !9, i64 0, i64 4, !6}
!19 = !{i64 0, i64 1, !9, i64 1, i64 1, !9, i64 2, i64 1, !9, i64 3, i64 1, !9, i64 0, i64 4, !6, i64 4, i64 4, !6}
!20 = !{!21, !21, i64 0}
!21 = !{!"long", !4, i64 0}
!22 = !{!23, !23, i64 0}
!23 = !{!"double", !4, i64 0}
!24 = !{!25, !3, i64 0}
!25 = !{!"", !3, i64 0, !3, i64 4, !23, i64 8, !7, i64 16, !7, i64 20}
!26 = !{!25, !3, i64 4}
!27 = !{!25, !23, i64 8}
!28 = !{!25, !7, i64 16}
!29 = !{!25, !7, i64 20}
!30 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 8, !22, i64 16, i64 4, !6, i64 20, i64 4, !6}
!31 = !{!32, !32, i64 0}
!32 = !{!"float", !4, i64 0}
!33 = !{!34, !32, i64 20}
!34 = !{!"", !35, i64 0, !32, i64 16, !32, i64 20, !32, i64 24, !4, i64 28}
!35 = !{!"", !32, i64 0, !32, i64 4, !32, i64 8, !32, i64 12}
!36 = !{!34, !32, i64 16}
!37 = !{i64 0, i64 4, !31, i64 4, i64 4, !31, i64 8, i64 4, !31, i64 12, i64 4, !31}
!38 = !{i64 0, i64 4, !31, i64 4, i64 4, !31, i64 8, i64 4, !31, i64 12, i64 4, !31, i64 16, i64 4, !31, i64 20, i64 1, !39}
!39 = !{!40, !40, i64 0}
!40 = !{!"_Bool", !4, i64 0}
!41 = !{!42, !4, i64 3}
!42 = !{!"", !4, i64 0, !4, i64 1, !4, i64 2, !4, i64 3}
!43 = !{!35, !32, i64 0}
!44 = !{!42, !4, i64 0}
!45 = !{!35, !32, i64 4}
!46 = !{!42, !4, i64 1}
!47 = !{!35, !32, i64 8}
!48 = !{!42, !4, i64 2}
!49 = !{!35, !32, i64 12}
