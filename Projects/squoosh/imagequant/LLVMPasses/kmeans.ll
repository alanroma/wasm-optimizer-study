; ModuleID = 'kmeans.c'
source_filename = "kmeans.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.colormap = type { i32, i8* (i32)*, void (i8*)*, [0 x %struct.colormap_item] }
%struct.colormap_item = type { %struct.f_pixel, float, i8 }
%struct.f_pixel = type { float, float, float, float }
%struct.kmeans_state = type { double, double, double, double, double }
%struct.histogram = type { %struct.hist_item*, void (i8*)*, double, i32, i32 }
%struct.hist_item = type { %struct.f_pixel, float, float, float, %union.anon }
%union.anon = type { i32 }
%struct.nearest_map = type opaque

; Function Attrs: nounwind
define hidden void @kmeans_init(%struct.colormap* %map, i32 %max_threads, %struct.kmeans_state* %average_color) #0 {
entry:
  %map.addr = alloca %struct.colormap*, align 4
  %max_threads.addr = alloca i32, align 4
  %average_color.addr = alloca %struct.kmeans_state*, align 4
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  store i32 %max_threads, i32* %max_threads.addr, align 4, !tbaa !6
  store %struct.kmeans_state* %average_color, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %0 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %1 = bitcast %struct.kmeans_state* %0 to i8*
  %2 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %2, i32 0, i32 0
  %3 = load i32, i32* %colors, align 4, !tbaa !6
  %add = add i32 2, %3
  %mul = mul i32 40, %add
  %4 = load i32, i32* %max_threads.addr, align 4, !tbaa !6
  %mul1 = mul i32 %mul, %4
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 0, i32 %mul1, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden void @kmeans_update_color(%struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor, float %value, %struct.colormap* %map, i32 %match, i32 %thread, %struct.kmeans_state* %average_color) #0 {
entry:
  %value.addr = alloca float, align 4
  %map.addr = alloca %struct.colormap*, align 4
  %match.addr = alloca i32, align 4
  %thread.addr = alloca i32, align 4
  %average_color.addr = alloca %struct.kmeans_state*, align 4
  store float %value, float* %value.addr, align 4, !tbaa !8
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  store i32 %match, i32* %match.addr, align 4, !tbaa !6
  store i32 %thread, i32* %thread.addr, align 4, !tbaa !6
  store %struct.kmeans_state* %average_color, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %0 = load i32, i32* %thread.addr, align 4, !tbaa !6
  %1 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %1, i32 0, i32 0
  %2 = load i32, i32* %colors, align 4, !tbaa !6
  %add = add i32 2, %2
  %mul = mul i32 %0, %add
  %3 = load i32, i32* %match.addr, align 4, !tbaa !6
  %add1 = add i32 %3, %mul
  store i32 %add1, i32* %match.addr, align 4, !tbaa !6
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor, i32 0, i32 0
  %4 = load float, float* %a, align 4, !tbaa !10
  %5 = load float, float* %value.addr, align 4, !tbaa !8
  %mul2 = fmul float %4, %5
  %conv = fpext float %mul2 to double
  %6 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %7 = load i32, i32* %match.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %6, i32 %7
  %a3 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx, i32 0, i32 0
  %8 = load double, double* %a3, align 8, !tbaa !12
  %add4 = fadd double %8, %conv
  store double %add4, double* %a3, align 8, !tbaa !12
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor, i32 0, i32 1
  %9 = load float, float* %r, align 4, !tbaa !15
  %10 = load float, float* %value.addr, align 4, !tbaa !8
  %mul5 = fmul float %9, %10
  %conv6 = fpext float %mul5 to double
  %11 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %12 = load i32, i32* %match.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %11, i32 %12
  %r8 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx7, i32 0, i32 1
  %13 = load double, double* %r8, align 8, !tbaa !16
  %add9 = fadd double %13, %conv6
  store double %add9, double* %r8, align 8, !tbaa !16
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor, i32 0, i32 2
  %14 = load float, float* %g, align 4, !tbaa !17
  %15 = load float, float* %value.addr, align 4, !tbaa !8
  %mul10 = fmul float %14, %15
  %conv11 = fpext float %mul10 to double
  %16 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %17 = load i32, i32* %match.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %16, i32 %17
  %g13 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx12, i32 0, i32 2
  %18 = load double, double* %g13, align 8, !tbaa !18
  %add14 = fadd double %18, %conv11
  store double %add14, double* %g13, align 8, !tbaa !18
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %acolor, i32 0, i32 3
  %19 = load float, float* %b, align 4, !tbaa !19
  %20 = load float, float* %value.addr, align 4, !tbaa !8
  %mul15 = fmul float %19, %20
  %conv16 = fpext float %mul15 to double
  %21 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %22 = load i32, i32* %match.addr, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %21, i32 %22
  %b18 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx17, i32 0, i32 3
  %23 = load double, double* %b18, align 8, !tbaa !20
  %add19 = fadd double %23, %conv16
  store double %add19, double* %b18, align 8, !tbaa !20
  %24 = load float, float* %value.addr, align 4, !tbaa !8
  %conv20 = fpext float %24 to double
  %25 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %26 = load i32, i32* %match.addr, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %25, i32 %26
  %total = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx21, i32 0, i32 4
  %27 = load double, double* %total, align 8, !tbaa !21
  %add22 = fadd double %27, %conv20
  store double %add22, double* %total, align 8, !tbaa !21
  ret void
}

; Function Attrs: nounwind
define hidden void @kmeans_finalize(%struct.colormap* %map, i32 %max_threads, %struct.kmeans_state* %average_color) #0 {
entry:
  %map.addr = alloca %struct.colormap*, align 4
  %max_threads.addr = alloca i32, align 4
  %average_color.addr = alloca %struct.kmeans_state*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %a = alloca double, align 8
  %r = alloca double, align 8
  %g = alloca double, align 8
  %b = alloca double, align 8
  %total = alloca double, align 8
  %t = alloca i32, align 4
  %offset = alloca i32, align 4
  %.compoundliteral = alloca %struct.f_pixel, align 4
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  store i32 %max_threads, i32* %max_threads.addr, align 4, !tbaa !6
  store %struct.kmeans_state* %average_color, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc38, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %2, i32 0, i32 0
  %3 = load i32, i32* %colors, align 4, !tbaa !6
  %cmp = icmp ult i32 %1, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #3
  br label %for.end40

for.body:                                         ; preds = %for.cond
  %5 = bitcast double* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #3
  store double 0.000000e+00, double* %a, align 8, !tbaa !22
  %6 = bitcast double* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %6) #3
  store double 0.000000e+00, double* %r, align 8, !tbaa !22
  %7 = bitcast double* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #3
  store double 0.000000e+00, double* %g, align 8, !tbaa !22
  %8 = bitcast double* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %8) #3
  store double 0.000000e+00, double* %b, align 8, !tbaa !22
  %9 = bitcast double* %total to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %9) #3
  store double 0.000000e+00, double* %total, align 8, !tbaa !22
  %10 = bitcast i32* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  store i32 0, i32* %t, align 4, !tbaa !6
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %11 = load i32, i32* %t, align 4, !tbaa !6
  %12 = load i32, i32* %max_threads.addr, align 4, !tbaa !6
  %cmp2 = icmp ult i32 %11, %12
  br i1 %cmp2, label %for.body4, label %for.cond.cleanup3

for.cond.cleanup3:                                ; preds = %for.cond1
  store i32 5, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #3
  br label %for.end

for.body4:                                        ; preds = %for.cond1
  %14 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors5 = getelementptr inbounds %struct.colormap, %struct.colormap* %15, i32 0, i32 0
  %16 = load i32, i32* %colors5, align 4, !tbaa !6
  %add = add i32 2, %16
  %17 = load i32, i32* %t, align 4, !tbaa !6
  %mul = mul i32 %add, %17
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %add6 = add i32 %mul, %18
  store i32 %add6, i32* %offset, align 4, !tbaa !6
  %19 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %20 = load i32, i32* %offset, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %19, i32 %20
  %a7 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx, i32 0, i32 0
  %21 = load double, double* %a7, align 8, !tbaa !12
  %22 = load double, double* %a, align 8, !tbaa !22
  %add8 = fadd double %22, %21
  store double %add8, double* %a, align 8, !tbaa !22
  %23 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %24 = load i32, i32* %offset, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %23, i32 %24
  %r10 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx9, i32 0, i32 1
  %25 = load double, double* %r10, align 8, !tbaa !16
  %26 = load double, double* %r, align 8, !tbaa !22
  %add11 = fadd double %26, %25
  store double %add11, double* %r, align 8, !tbaa !22
  %27 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %28 = load i32, i32* %offset, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %27, i32 %28
  %g13 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx12, i32 0, i32 2
  %29 = load double, double* %g13, align 8, !tbaa !18
  %30 = load double, double* %g, align 8, !tbaa !22
  %add14 = fadd double %30, %29
  store double %add14, double* %g, align 8, !tbaa !22
  %31 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %32 = load i32, i32* %offset, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %31, i32 %32
  %b16 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx15, i32 0, i32 3
  %33 = load double, double* %b16, align 8, !tbaa !20
  %34 = load double, double* %b, align 8, !tbaa !22
  %add17 = fadd double %34, %33
  store double %add17, double* %b, align 8, !tbaa !22
  %35 = load %struct.kmeans_state*, %struct.kmeans_state** %average_color.addr, align 4, !tbaa !2
  %36 = load i32, i32* %offset, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %35, i32 %36
  %total19 = getelementptr inbounds %struct.kmeans_state, %struct.kmeans_state* %arrayidx18, i32 0, i32 4
  %37 = load double, double* %total19, align 8, !tbaa !21
  %38 = load double, double* %total, align 8, !tbaa !22
  %add20 = fadd double %38, %37
  store double %add20, double* %total, align 8, !tbaa !22
  %39 = bitcast i32* %offset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %40 = load i32, i32* %t, align 4, !tbaa !6
  %inc = add i32 %40, 1
  store i32 %inc, i32* %t, align 4, !tbaa !6
  br label %for.cond1

for.end:                                          ; preds = %for.cond.cleanup3
  %41 = load double, double* %total, align 8, !tbaa !22
  %tobool = fcmp une double %41, 0.000000e+00
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.end
  %42 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %42, i32 0, i32 3
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 %43
  %fixed = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx21, i32 0, i32 2
  %44 = load i8, i8* %fixed, align 4, !tbaa !23, !range !26
  %tobool22 = trunc i8 %44 to i1
  br i1 %tobool22, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %45 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette23 = getelementptr inbounds %struct.colormap, %struct.colormap* %45, i32 0, i32 3
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette23, i32 0, i32 %46
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx24, i32 0, i32 0
  %a25 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 0
  %47 = load double, double* %a, align 8, !tbaa !22
  %48 = load double, double* %total, align 8, !tbaa !22
  %div = fdiv double %47, %48
  %conv = fptrunc double %div to float
  store float %conv, float* %a25, align 4, !tbaa !10
  %r26 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 1
  %49 = load double, double* %r, align 8, !tbaa !22
  %50 = load double, double* %total, align 8, !tbaa !22
  %div27 = fdiv double %49, %50
  %conv28 = fptrunc double %div27 to float
  store float %conv28, float* %r26, align 4, !tbaa !15
  %g29 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 2
  %51 = load double, double* %g, align 8, !tbaa !22
  %52 = load double, double* %total, align 8, !tbaa !22
  %div30 = fdiv double %51, %52
  %conv31 = fptrunc double %div30 to float
  store float %conv31, float* %g29, align 4, !tbaa !17
  %b32 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %.compoundliteral, i32 0, i32 3
  %53 = load double, double* %b, align 8, !tbaa !22
  %54 = load double, double* %total, align 8, !tbaa !22
  %div33 = fdiv double %53, %54
  %conv34 = fptrunc double %div33 to float
  store float %conv34, float* %b32, align 4, !tbaa !19
  %55 = bitcast %struct.f_pixel* %acolor to i8*
  %56 = bitcast %struct.f_pixel* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false), !tbaa.struct !27
  %57 = load double, double* %total, align 8, !tbaa !22
  %conv35 = fptrunc double %57 to float
  %58 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette36 = getelementptr inbounds %struct.colormap, %struct.colormap* %58, i32 0, i32 3
  %59 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette36, i32 0, i32 %59
  %popularity = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx37, i32 0, i32 1
  store float %conv35, float* %popularity, align 4, !tbaa !28
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.end
  %60 = bitcast double* %total to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %60) #3
  %61 = bitcast double* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %61) #3
  %62 = bitcast double* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %62) #3
  %63 = bitcast double* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %63) #3
  %64 = bitcast double* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %64) #3
  br label %for.inc38

for.inc38:                                        ; preds = %if.end
  %65 = load i32, i32* %i, align 4, !tbaa !6
  %inc39 = add i32 %65, 1
  store i32 %inc39, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end40:                                        ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: nounwind
define hidden double @kmeans_do_iteration(%struct.histogram* %hist, %struct.colormap* %map, void (%struct.hist_item*, float)* %callback) #0 {
entry:
  %hist.addr = alloca %struct.histogram*, align 4
  %map.addr = alloca %struct.colormap*, align 4
  %callback.addr = alloca void (%struct.hist_item*, float)*, align 4
  %max_threads = alloca i32, align 4
  %saved_stack = alloca i8*, align 4
  %__vla_expr0 = alloca i32, align 4
  %n = alloca %struct.nearest_map*, align 4
  %achv = alloca %struct.hist_item*, align 4
  %hist_size = alloca i32, align 4
  %total_diff = alloca double, align 8
  %j = alloca i32, align 4
  %diff = alloca float, align 4
  %match = alloca i32, align 4
  store %struct.histogram* %hist, %struct.histogram** %hist.addr, align 4, !tbaa !2
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  store void (%struct.hist_item*, float)* %callback, void (%struct.hist_item*, float)** %callback.addr, align 4, !tbaa !2
  %0 = bitcast i32* %max_threads to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 1, i32* %max_threads, align 4, !tbaa !6
  %1 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %1, i32 0, i32 0
  %2 = load i32, i32* %colors, align 4, !tbaa !6
  %add = add i32 2, %2
  %mul = mul i32 %add, 1
  %3 = call i8* @llvm.stacksave()
  store i8* %3, i8** %saved_stack, align 4
  %vla = alloca %struct.kmeans_state, i32 %mul, align 16
  store i32 %mul, i32* %__vla_expr0, align 4
  %4 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  call void @kmeans_init(%struct.colormap* %4, i32 1, %struct.kmeans_state* %vla)
  %5 = bitcast %struct.nearest_map** %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %call = call %struct.nearest_map* @nearest_init(%struct.colormap* %6)
  store %struct.nearest_map* %call, %struct.nearest_map** %n, align 4, !tbaa !2
  %7 = bitcast %struct.hist_item** %achv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %achv1 = getelementptr inbounds %struct.histogram, %struct.histogram* %8, i32 0, i32 0
  %9 = load %struct.hist_item*, %struct.hist_item** %achv1, align 8, !tbaa !29
  store %struct.hist_item* %9, %struct.hist_item** %achv, align 4, !tbaa !2
  %10 = bitcast i32* %hist_size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %size = getelementptr inbounds %struct.histogram, %struct.histogram* %11, i32 0, i32 3
  %12 = load i32, i32* %size, align 8, !tbaa !31
  store i32 %12, i32* %hist_size, align 4, !tbaa !6
  %13 = bitcast double* %total_diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #3
  store double 0.000000e+00, double* %total_diff, align 8, !tbaa !22
  %14 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %j, align 4, !tbaa !6
  %16 = load i32, i32* %hist_size, align 4, !tbaa !6
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %17 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %18 = bitcast float* %diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %match to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load %struct.nearest_map*, %struct.nearest_map** %n, align 4, !tbaa !2
  %21 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %22 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.hist_item, %struct.hist_item* %21, i32 %22
  %acolor = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx, i32 0, i32 0
  %23 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %24 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %23, i32 %24
  %tmp = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx2, i32 0, i32 4
  %likely_colormap_index = bitcast %union.anon* %tmp to i8*
  %25 = load i8, i8* %likely_colormap_index, align 4, !tbaa !32
  %conv = zext i8 %25 to i32
  %call3 = call i32 @nearest_search(%struct.nearest_map* %20, %struct.f_pixel* %acolor, i32 %conv, float* %diff)
  store i32 %call3, i32* %match, align 4, !tbaa !6
  %26 = load i32, i32* %match, align 4, !tbaa !6
  %conv4 = trunc i32 %26 to i8
  %27 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %28 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %27, i32 %28
  %tmp6 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx5, i32 0, i32 4
  %likely_colormap_index7 = bitcast %union.anon* %tmp6 to i8*
  store i8 %conv4, i8* %likely_colormap_index7, align 4, !tbaa !32
  %29 = load float, float* %diff, align 4, !tbaa !8
  %30 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %31 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx8 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %30, i32 %31
  %perceptual_weight = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx8, i32 0, i32 2
  %32 = load float, float* %perceptual_weight, align 4, !tbaa !33
  %mul9 = fmul float %29, %32
  %conv10 = fpext float %mul9 to double
  %33 = load double, double* %total_diff, align 8, !tbaa !22
  %add11 = fadd double %33, %conv10
  store double %add11, double* %total_diff, align 8, !tbaa !22
  %34 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %35 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %34, i32 %35
  %acolor13 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx12, i32 0, i32 0
  %36 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %37 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx14 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %36, i32 %37
  %perceptual_weight15 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %arrayidx14, i32 0, i32 2
  %38 = load float, float* %perceptual_weight15, align 4, !tbaa !33
  %39 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %40 = load i32, i32* %match, align 4, !tbaa !6
  call void @kmeans_update_color(%struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor13, float %38, %struct.colormap* %39, i32 %40, i32 0, %struct.kmeans_state* %vla)
  %41 = load void (%struct.hist_item*, float)*, void (%struct.hist_item*, float)** %callback.addr, align 4, !tbaa !2
  %tobool = icmp ne void (%struct.hist_item*, float)* %41, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %42 = load void (%struct.hist_item*, float)*, void (%struct.hist_item*, float)** %callback.addr, align 4, !tbaa !2
  %43 = load %struct.hist_item*, %struct.hist_item** %achv, align 4, !tbaa !2
  %44 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds %struct.hist_item, %struct.hist_item* %43, i32 %44
  %45 = load float, float* %diff, align 4, !tbaa !8
  call void %42(%struct.hist_item* %arrayidx16, float %45)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %46 = bitcast i32* %match to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast float* %diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %48 = load i32, i32* %j, align 4, !tbaa !6
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %j, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %49 = load %struct.nearest_map*, %struct.nearest_map** %n, align 4, !tbaa !2
  call void @nearest_free(%struct.nearest_map* %49)
  %50 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  call void @kmeans_finalize(%struct.colormap* %50, i32 1, %struct.kmeans_state* %vla)
  %51 = load double, double* %total_diff, align 8, !tbaa !22
  %52 = load %struct.histogram*, %struct.histogram** %hist.addr, align 4, !tbaa !2
  %total_perceptual_weight = getelementptr inbounds %struct.histogram, %struct.histogram* %52, i32 0, i32 2
  %53 = load double, double* %total_perceptual_weight, align 8, !tbaa !35
  %div = fdiv double %51, %53
  %54 = bitcast double* %total_diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %54) #3
  %55 = bitcast i32* %hist_size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast %struct.hist_item** %achv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast %struct.nearest_map** %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = load i8*, i8** %saved_stack, align 4
  call void @llvm.stackrestore(i8* %58)
  %59 = bitcast i32* %max_threads to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  ret double %div
}

; Function Attrs: nounwind
declare i8* @llvm.stacksave() #3

declare hidden %struct.nearest_map* @nearest_init(%struct.colormap*) #4

declare hidden i32 @nearest_search(%struct.nearest_map*, %struct.f_pixel*, i32, float*) #4

declare hidden void @nearest_free(%struct.nearest_map*) #4

; Function Attrs: nounwind
declare void @llvm.stackrestore(i8*) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !9, i64 0}
!11 = !{!"", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12}
!12 = !{!13, !14, i64 0}
!13 = !{!"", !14, i64 0, !14, i64 8, !14, i64 16, !14, i64 24, !14, i64 32}
!14 = !{!"double", !4, i64 0}
!15 = !{!11, !9, i64 4}
!16 = !{!13, !14, i64 8}
!17 = !{!11, !9, i64 8}
!18 = !{!13, !14, i64 16}
!19 = !{!11, !9, i64 12}
!20 = !{!13, !14, i64 24}
!21 = !{!13, !14, i64 32}
!22 = !{!14, !14, i64 0}
!23 = !{!24, !25, i64 20}
!24 = !{!"", !11, i64 0, !9, i64 16, !25, i64 20}
!25 = !{!"_Bool", !4, i64 0}
!26 = !{i8 0, i8 2}
!27 = !{i64 0, i64 4, !8, i64 4, i64 4, !8, i64 8, i64 4, !8, i64 12, i64 4, !8}
!28 = !{!24, !9, i64 16}
!29 = !{!30, !3, i64 0}
!30 = !{!"", !3, i64 0, !3, i64 4, !14, i64 8, !7, i64 16, !7, i64 20}
!31 = !{!30, !7, i64 16}
!32 = !{!4, !4, i64 0}
!33 = !{!34, !9, i64 20}
!34 = !{!"", !11, i64 0, !9, i64 16, !9, i64 20, !9, i64 24, !4, i64 28}
!35 = !{!30, !14, i64 8}
