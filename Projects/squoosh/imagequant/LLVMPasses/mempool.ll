; ModuleID = 'mempool.c'
source_filename = "mempool.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.mempool = type { i32, i32, i8* (i32)*, void (i8*)*, %struct.mempool* }

; Function Attrs: nounwind
define hidden i8* @mempool_create(%struct.mempool** %mptr, i32 %size, i32 %max_size, i8* (i32)* %malloc, void (i8*)* %free) #0 {
entry:
  %retval = alloca i8*, align 4
  %mptr.addr = alloca %struct.mempool**, align 4
  %size.addr = alloca i32, align 4
  %max_size.addr = alloca i32, align 4
  %malloc.addr = alloca i8* (i32)*, align 4
  %free.addr = alloca void (i8*)*, align 4
  %prevused = alloca i32, align 4
  %old = alloca %struct.mempool*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %.compoundliteral = alloca %struct.mempool, align 4
  %mptr_used_start = alloca i32, align 4
  store %struct.mempool** %mptr, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  store i32 %max_size, i32* %max_size.addr, align 4, !tbaa !6
  store i8* (i32)* %malloc, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  store void (i8*)* %free, void (i8*)** %free.addr, align 4, !tbaa !2
  %0 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %1 = load %struct.mempool*, %struct.mempool** %0, align 4, !tbaa !2
  %tobool = icmp ne %struct.mempool* %1, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %3 = load %struct.mempool*, %struct.mempool** %2, align 4, !tbaa !2
  %used = getelementptr inbounds %struct.mempool, %struct.mempool* %3, i32 0, i32 0
  %4 = load i32, i32* %used, align 4, !tbaa !8
  %5 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add = add i32 %4, %5
  %6 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %7 = load %struct.mempool*, %struct.mempool** %6, align 4, !tbaa !2
  %size1 = getelementptr inbounds %struct.mempool, %struct.mempool* %7, i32 0, i32 1
  %8 = load i32, i32* %size1, align 4, !tbaa !10
  %cmp = icmp ule i32 %add, %8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %9 = bitcast i32* %prevused to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %11 = load %struct.mempool*, %struct.mempool** %10, align 4, !tbaa !2
  %used2 = getelementptr inbounds %struct.mempool, %struct.mempool* %11, i32 0, i32 0
  %12 = load i32, i32* %used2, align 4, !tbaa !8
  store i32 %12, i32* %prevused, align 4, !tbaa !6
  %13 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add3 = add i32 %13, 15
  %and = and i32 %add3, -16
  %14 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %15 = load %struct.mempool*, %struct.mempool** %14, align 4, !tbaa !2
  %used4 = getelementptr inbounds %struct.mempool, %struct.mempool* %15, i32 0, i32 0
  %16 = load i32, i32* %used4, align 4, !tbaa !8
  %add5 = add i32 %16, %and
  store i32 %add5, i32* %used4, align 4, !tbaa !8
  %17 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %18 = load %struct.mempool*, %struct.mempool** %17, align 4, !tbaa !2
  %19 = bitcast %struct.mempool* %18 to i8*
  %20 = load i32, i32* %prevused, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %19, i32 %20
  store i8* %add.ptr, i8** %retval, align 4
  %21 = bitcast i32* %prevused to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #2
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %22 = bitcast %struct.mempool** %old to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #2
  %23 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %24 = load %struct.mempool*, %struct.mempool** %23, align 4, !tbaa !2
  store %struct.mempool* %24, %struct.mempool** %old, align 4, !tbaa !2
  %25 = load i32, i32* %max_size.addr, align 4, !tbaa !6
  %tobool6 = icmp ne i32 %25, 0
  br i1 %tobool6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end
  store i32 131072, i32* %max_size.addr, align 4, !tbaa !6
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %if.end
  %26 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add9 = add i32 %26, 15
  %27 = load i32, i32* %max_size.addr, align 4, !tbaa !6
  %cmp10 = icmp ugt i32 %add9, %27
  br i1 %cmp10, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end8
  %28 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add11 = add i32 %28, 15
  br label %cond.end

cond.false:                                       ; preds = %if.end8
  %29 = load i32, i32* %max_size.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add11, %cond.true ], [ %29, %cond.false ]
  store i32 %cond, i32* %max_size.addr, align 4, !tbaa !6
  %30 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  %31 = load i32, i32* %max_size.addr, align 4, !tbaa !6
  %add12 = add i32 32, %31
  %call = call i8* %30(i32 %add12)
  %32 = bitcast i8* %call to %struct.mempool*
  %33 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  store %struct.mempool* %32, %struct.mempool** %33, align 4, !tbaa !2
  %34 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %35 = load %struct.mempool*, %struct.mempool** %34, align 4, !tbaa !2
  %tobool13 = icmp ne %struct.mempool* %35, null
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %cond.end
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %cond.end
  %36 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %37 = load %struct.mempool*, %struct.mempool** %36, align 4, !tbaa !2
  %used16 = getelementptr inbounds %struct.mempool, %struct.mempool* %.compoundliteral, i32 0, i32 0
  store i32 20, i32* %used16, align 4, !tbaa !8
  %size17 = getelementptr inbounds %struct.mempool, %struct.mempool* %.compoundliteral, i32 0, i32 1
  %38 = load i32, i32* %max_size.addr, align 4, !tbaa !6
  %add18 = add i32 32, %38
  store i32 %add18, i32* %size17, align 4, !tbaa !10
  %malloc19 = getelementptr inbounds %struct.mempool, %struct.mempool* %.compoundliteral, i32 0, i32 2
  %39 = load i8* (i32)*, i8* (i32)** %malloc.addr, align 4, !tbaa !2
  store i8* (i32)* %39, i8* (i32)** %malloc19, align 4, !tbaa !11
  %free20 = getelementptr inbounds %struct.mempool, %struct.mempool* %.compoundliteral, i32 0, i32 3
  %40 = load void (i8*)*, void (i8*)** %free.addr, align 4, !tbaa !2
  store void (i8*)* %40, void (i8*)** %free20, align 4, !tbaa !12
  %next = getelementptr inbounds %struct.mempool, %struct.mempool* %.compoundliteral, i32 0, i32 4
  %41 = load %struct.mempool*, %struct.mempool** %old, align 4, !tbaa !2
  store %struct.mempool* %41, %struct.mempool** %next, align 4, !tbaa !13
  %42 = bitcast %struct.mempool* %37 to i8*
  %43 = bitcast %struct.mempool* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 20, i1 false), !tbaa.struct !14
  %44 = bitcast i32* %mptr_used_start to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #2
  %45 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %46 = load %struct.mempool*, %struct.mempool** %45, align 4, !tbaa !2
  %47 = ptrtoint %struct.mempool* %46 to i32
  %48 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %49 = load %struct.mempool*, %struct.mempool** %48, align 4, !tbaa !2
  %used21 = getelementptr inbounds %struct.mempool, %struct.mempool* %49, i32 0, i32 0
  %50 = load i32, i32* %used21, align 4, !tbaa !8
  %add22 = add i32 %47, %50
  store i32 %add22, i32* %mptr_used_start, align 4, !tbaa !15
  %51 = load i32, i32* %mptr_used_start, align 4, !tbaa !15
  %and23 = and i32 %51, 15
  %sub = sub i32 16, %and23
  %and24 = and i32 %sub, 15
  %52 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %53 = load %struct.mempool*, %struct.mempool** %52, align 4, !tbaa !2
  %used25 = getelementptr inbounds %struct.mempool, %struct.mempool* %53, i32 0, i32 0
  %54 = load i32, i32* %used25, align 4, !tbaa !8
  %add26 = add i32 %54, %and24
  store i32 %add26, i32* %used25, align 4, !tbaa !8
  %55 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %56 = load i32, i32* %size.addr, align 4, !tbaa !6
  %57 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call27 = call i8* @mempool_alloc(%struct.mempool** %55, i32 %56, i32 %57)
  store i8* %call27, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %58 = bitcast i32* %mptr_used_start to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #2
  br label %cleanup

cleanup:                                          ; preds = %if.end15, %if.then14
  %59 = bitcast %struct.mempool** %old to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #2
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %60 = load i8*, i8** %retval, align 4
  ret i8* %60
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define hidden i8* @mempool_alloc(%struct.mempool** %mptr, i32 %size, i32 %max_size) #0 {
entry:
  %retval = alloca i8*, align 4
  %mptr.addr = alloca %struct.mempool**, align 4
  %size.addr = alloca i32, align 4
  %max_size.addr = alloca i32, align 4
  %prevused = alloca i32, align 4
  store %struct.mempool** %mptr, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  store i32 %max_size, i32* %max_size.addr, align 4, !tbaa !6
  %0 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %1 = load %struct.mempool*, %struct.mempool** %0, align 4, !tbaa !2
  %used = getelementptr inbounds %struct.mempool, %struct.mempool* %1, i32 0, i32 0
  %2 = load i32, i32* %used, align 4, !tbaa !8
  %3 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add = add i32 %2, %3
  %4 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %5 = load %struct.mempool*, %struct.mempool** %4, align 4, !tbaa !2
  %size1 = getelementptr inbounds %struct.mempool, %struct.mempool* %5, i32 0, i32 1
  %6 = load i32, i32* %size1, align 4, !tbaa !10
  %cmp = icmp ule i32 %add, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = bitcast i32* %prevused to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %9 = load %struct.mempool*, %struct.mempool** %8, align 4, !tbaa !2
  %used2 = getelementptr inbounds %struct.mempool, %struct.mempool* %9, i32 0, i32 0
  %10 = load i32, i32* %used2, align 4, !tbaa !8
  store i32 %10, i32* %prevused, align 4, !tbaa !6
  %11 = load i32, i32* %size.addr, align 4, !tbaa !6
  %add3 = add i32 %11, 15
  %and = and i32 %add3, -16
  %12 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %13 = load %struct.mempool*, %struct.mempool** %12, align 4, !tbaa !2
  %used4 = getelementptr inbounds %struct.mempool, %struct.mempool* %13, i32 0, i32 0
  %14 = load i32, i32* %used4, align 4, !tbaa !8
  %add5 = add i32 %14, %and
  store i32 %add5, i32* %used4, align 4, !tbaa !8
  %15 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %16 = load %struct.mempool*, %struct.mempool** %15, align 4, !tbaa !2
  %17 = bitcast %struct.mempool* %16 to i8*
  %18 = load i32, i32* %prevused, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %17, i32 %18
  store i8* %add.ptr, i8** %retval, align 4
  %19 = bitcast i32* %prevused to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #2
  br label %return

if.end:                                           ; preds = %entry
  %20 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %21 = load i32, i32* %size.addr, align 4, !tbaa !6
  %22 = load i32, i32* %max_size.addr, align 4, !tbaa !6
  %23 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %24 = load %struct.mempool*, %struct.mempool** %23, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.mempool, %struct.mempool* %24, i32 0, i32 2
  %25 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !11
  %26 = load %struct.mempool**, %struct.mempool*** %mptr.addr, align 4, !tbaa !2
  %27 = load %struct.mempool*, %struct.mempool** %26, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.mempool, %struct.mempool* %27, i32 0, i32 3
  %28 = load void (i8*)*, void (i8*)** %free, align 4, !tbaa !12
  %call = call i8* @mempool_create(%struct.mempool** %20, i32 %21, i32 %22, i8* (i32)* %25, void (i8*)* %28)
  store i8* %call, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %29 = load i8*, i8** %retval, align 4
  ret i8* %29
}

; Function Attrs: nounwind
define hidden void @mempool_destroy(%struct.mempool* %m) #0 {
entry:
  %m.addr = alloca %struct.mempool*, align 4
  %next = alloca %struct.mempool*, align 4
  store %struct.mempool* %m, %struct.mempool** %m.addr, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %struct.mempool*, %struct.mempool** %m.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.mempool* %0, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = bitcast %struct.mempool** %next to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = load %struct.mempool*, %struct.mempool** %m.addr, align 4, !tbaa !2
  %next1 = getelementptr inbounds %struct.mempool, %struct.mempool* %2, i32 0, i32 4
  %3 = load %struct.mempool*, %struct.mempool** %next1, align 4, !tbaa !13
  store %struct.mempool* %3, %struct.mempool** %next, align 4, !tbaa !2
  %4 = load %struct.mempool*, %struct.mempool** %m.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.mempool, %struct.mempool* %4, i32 0, i32 3
  %5 = load void (i8*)*, void (i8*)** %free, align 4, !tbaa !12
  %6 = load %struct.mempool*, %struct.mempool** %m.addr, align 4, !tbaa !2
  %7 = bitcast %struct.mempool* %6 to i8*
  call void %5(i8* %7)
  %8 = load %struct.mempool*, %struct.mempool** %next, align 4, !tbaa !2
  store %struct.mempool* %8, %struct.mempool** %m.addr, align 4, !tbaa !2
  %9 = bitcast %struct.mempool** %next to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 0}
!9 = !{!"mempool", !7, i64 0, !7, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!10 = !{!9, !7, i64 4}
!11 = !{!9, !3, i64 8}
!12 = !{!9, !3, i64 12}
!13 = !{!9, !3, i64 16}
!14 = !{i64 0, i64 4, !6, i64 4, i64 4, !6, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 16, i64 4, !2}
!15 = !{!16, !16, i64 0}
!16 = !{!"long", !4, i64 0}
