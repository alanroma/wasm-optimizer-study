; ModuleID = 'nearest.c'
source_filename = "nearest.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.nearest_map = type { %struct.vp_node*, %struct.colormap_item*, [256 x float], %struct.mempool* }
%struct.vp_node = type { %struct.vp_node*, %struct.vp_node*, %struct.f_pixel, float, i32 }
%struct.f_pixel = type { float, float, float, float }
%struct.colormap_item = type { %struct.f_pixel, float, i8 }
%struct.mempool = type opaque
%struct.colormap = type { i32, i8* (i32)*, void (i8*)*, [0 x %struct.colormap_item] }
%struct.vp_search_tmp = type { float, i32, i32 }
%struct.vp_sort_tmp = type { float, i32 }

; Function Attrs: nounwind
define hidden %struct.nearest_map* @nearest_init(%struct.colormap* %map) #0 {
entry:
  %map.addr = alloca %struct.colormap*, align 4
  %m = alloca %struct.mempool*, align 4
  %handle = alloca %struct.nearest_map*, align 4
  %saved_stack = alloca i8*, align 4
  %__vla_expr0 = alloca i32, align 4
  %i = alloca i32, align 4
  %root = alloca %struct.vp_node*, align 4
  %.compoundliteral = alloca %struct.nearest_map, align 4
  %i10 = alloca i32, align 4
  %best = alloca %struct.vp_search_tmp, align 4
  store %struct.colormap* %map, %struct.colormap** %map.addr, align 4, !tbaa !2
  %0 = bitcast %struct.mempool** %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store %struct.mempool* null, %struct.mempool** %m, align 4, !tbaa !2
  %1 = bitcast %struct.nearest_map** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors = getelementptr inbounds %struct.colormap, %struct.colormap* %2, i32 0, i32 0
  %3 = load i32, i32* %colors, align 4, !tbaa !6
  %mul = mul i32 32, %3
  %add = add i32 1036, %mul
  %add1 = add i32 %add, 16
  %4 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %malloc = getelementptr inbounds %struct.colormap, %struct.colormap* %4, i32 0, i32 1
  %5 = load i8* (i32)*, i8* (i32)** %malloc, align 4, !tbaa !2
  %6 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %free = getelementptr inbounds %struct.colormap, %struct.colormap* %6, i32 0, i32 2
  %7 = load void (i8*)*, void (i8*)** %free, align 4, !tbaa !2
  %call = call i8* @mempool_create(%struct.mempool** %m, i32 1036, i32 %add1, i8* (i32)* %5, void (i8*)* %7)
  %8 = bitcast i8* %call to %struct.nearest_map*
  store %struct.nearest_map* %8, %struct.nearest_map** %handle, align 4, !tbaa !2
  %9 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors2 = getelementptr inbounds %struct.colormap, %struct.colormap* %9, i32 0, i32 0
  %10 = load i32, i32* %colors2, align 4, !tbaa !6
  %11 = call i8* @llvm.stacksave()
  store i8* %11, i8** %saved_stack, align 4
  %vla = alloca %struct.vp_sort_tmp, i32 %10, align 16
  store i32 %10, i32* %__vla_expr0, align 4
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %14 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors3 = getelementptr inbounds %struct.colormap, %struct.colormap* %14, i32 0, i32 0
  %15 = load i32, i32* %colors3, align 4, !tbaa !6
  %cmp = icmp ult i32 %13, %15
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %vla, i32 %18
  %idx = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx, i32 0, i32 1
  store i32 %17, i32* %idx, align 4, !tbaa !8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %20 = bitcast %struct.vp_node** %root to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors4 = getelementptr inbounds %struct.colormap, %struct.colormap* %21, i32 0, i32 0
  %22 = load i32, i32* %colors4, align 4, !tbaa !6
  %23 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.colormap, %struct.colormap* %23, i32 0, i32 3
  %arraydecay = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette, i32 0, i32 0
  %call5 = call %struct.vp_node* @vp_create_node(%struct.mempool** %m, %struct.vp_sort_tmp* %vla, i32 %22, %struct.colormap_item* %arraydecay)
  store %struct.vp_node* %call5, %struct.vp_node** %root, align 4, !tbaa !2
  %24 = load %struct.nearest_map*, %struct.nearest_map** %handle, align 4, !tbaa !2
  %25 = bitcast %struct.nearest_map* %.compoundliteral to i8*
  call void @llvm.memset.p0i8.i64(i8* align 4 %25, i8 0, i64 1036, i1 false)
  %root6 = getelementptr inbounds %struct.nearest_map, %struct.nearest_map* %.compoundliteral, i32 0, i32 0
  %26 = load %struct.vp_node*, %struct.vp_node** %root, align 4, !tbaa !2
  store %struct.vp_node* %26, %struct.vp_node** %root6, align 4, !tbaa !11
  %palette7 = getelementptr inbounds %struct.nearest_map, %struct.nearest_map* %.compoundliteral, i32 0, i32 1
  %27 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette8 = getelementptr inbounds %struct.colormap, %struct.colormap* %27, i32 0, i32 3
  %arraydecay9 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette8, i32 0, i32 0
  store %struct.colormap_item* %arraydecay9, %struct.colormap_item** %palette7, align 4, !tbaa !13
  %mempool = getelementptr inbounds %struct.nearest_map, %struct.nearest_map* %.compoundliteral, i32 0, i32 3
  %28 = load %struct.mempool*, %struct.mempool** %m, align 4, !tbaa !2
  store %struct.mempool* %28, %struct.mempool** %mempool, align 4, !tbaa !14
  %29 = bitcast %struct.nearest_map* %24 to i8*
  %30 = bitcast %struct.nearest_map* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 1036, i1 false), !tbaa.struct !15
  %31 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #3
  store i32 0, i32* %i10, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc24, %for.end
  %32 = load i32, i32* %i10, align 4, !tbaa !6
  %33 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %colors12 = getelementptr inbounds %struct.colormap, %struct.colormap* %33, i32 0, i32 0
  %34 = load i32, i32* %colors12, align 4, !tbaa !6
  %cmp13 = icmp ult i32 %32, %34
  br i1 %cmp13, label %for.body15, label %for.cond.cleanup14

for.cond.cleanup14:                               ; preds = %for.cond11
  %35 = bitcast i32* %i10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  br label %for.end26

for.body15:                                       ; preds = %for.cond11
  %36 = bitcast %struct.vp_search_tmp* %best to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %36) #3
  %distance = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best, i32 0, i32 0
  store float 0x4415AF1D80000000, float* %distance, align 4, !tbaa !17
  %idx16 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best, i32 0, i32 1
  store i32 0, i32* %idx16, align 4, !tbaa !19
  %exclude = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best, i32 0, i32 2
  %37 = load i32, i32* %i10, align 4, !tbaa !6
  store i32 %37, i32* %exclude, align 4, !tbaa !20
  %38 = load %struct.vp_node*, %struct.vp_node** %root, align 4, !tbaa !2
  %39 = load %struct.colormap*, %struct.colormap** %map.addr, align 4, !tbaa !2
  %palette17 = getelementptr inbounds %struct.colormap, %struct.colormap* %39, i32 0, i32 3
  %40 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds [0 x %struct.colormap_item], [0 x %struct.colormap_item]* %palette17, i32 0, i32 %40
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx18, i32 0, i32 0
  call void @vp_search_node(%struct.vp_node* %38, %struct.f_pixel* %acolor, %struct.vp_search_tmp* %best)
  %distance19 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best, i32 0, i32 0
  %41 = load float, float* %distance19, align 4, !tbaa !17
  %distance20 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best, i32 0, i32 0
  %42 = load float, float* %distance20, align 4, !tbaa !17
  %mul21 = fmul float %41, %42
  %conv = fpext float %mul21 to double
  %div = fdiv double %conv, 4.000000e+00
  %conv22 = fptrunc double %div to float
  %43 = load %struct.nearest_map*, %struct.nearest_map** %handle, align 4, !tbaa !2
  %nearest_other_color_dist = getelementptr inbounds %struct.nearest_map, %struct.nearest_map* %43, i32 0, i32 2
  %44 = load i32, i32* %i10, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds [256 x float], [256 x float]* %nearest_other_color_dist, i32 0, i32 %44
  store float %conv22, float* %arrayidx23, align 4, !tbaa !21
  %45 = bitcast %struct.vp_search_tmp* %best to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %45) #3
  br label %for.inc24

for.inc24:                                        ; preds = %for.body15
  %46 = load i32, i32* %i10, align 4, !tbaa !6
  %inc25 = add i32 %46, 1
  store i32 %inc25, i32* %i10, align 4, !tbaa !6
  br label %for.cond11

for.end26:                                        ; preds = %for.cond.cleanup14
  %47 = load %struct.nearest_map*, %struct.nearest_map** %handle, align 4, !tbaa !2
  %48 = bitcast %struct.vp_node** %root to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  %49 = load i8*, i8** %saved_stack, align 4
  call void @llvm.stackrestore(i8* %49)
  %50 = bitcast %struct.nearest_map** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast %struct.mempool** %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  ret %struct.nearest_map* %47
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare hidden i8* @mempool_create(%struct.mempool**, i32, i32, i8* (i32)*, void (i8*)*) #2

; Function Attrs: nounwind
declare i8* @llvm.stacksave() #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal %struct.vp_node* @vp_create_node(%struct.mempool** %m, %struct.vp_sort_tmp* %indexes, i32 %num_indexes, %struct.colormap_item* %items) #0 {
entry:
  %retval = alloca %struct.vp_node*, align 4
  %m.addr = alloca %struct.mempool**, align 4
  %indexes.addr = alloca %struct.vp_sort_tmp*, align 4
  %num_indexes.addr = alloca i32, align 4
  %items.addr = alloca %struct.colormap_item*, align 4
  %node = alloca %struct.vp_node*, align 4
  %.compoundliteral = alloca %struct.vp_node, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref = alloca i32, align 4
  %ref_idx = alloca i32, align 4
  %half_idx = alloca i32, align 4
  %.compoundliteral15 = alloca %struct.vp_node, align 4
  store %struct.mempool** %m, %struct.mempool*** %m.addr, align 4, !tbaa !2
  store %struct.vp_sort_tmp* %indexes, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  store i32 %num_indexes, i32* %num_indexes.addr, align 4, !tbaa !6
  store %struct.colormap_item* %items, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %0 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %cmp = icmp sle i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.vp_node* null, %struct.vp_node** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %struct.vp_node** %node to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.mempool**, %struct.mempool*** %m.addr, align 4, !tbaa !2
  %call = call i8* @mempool_alloc(%struct.mempool** %2, i32 32, i32 0)
  %3 = bitcast i8* %call to %struct.vp_node*
  store %struct.vp_node* %3, %struct.vp_node** %node, align 4, !tbaa !2
  %4 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %4, 1
  br i1 %cmp1, label %if.then2, label %if.end7

if.then2:                                         ; preds = %if.end
  %5 = load %struct.vp_node*, %struct.vp_node** %node, align 4, !tbaa !2
  %near = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral, i32 0, i32 0
  store %struct.vp_node* null, %struct.vp_node** %near, align 4, !tbaa !22
  %far = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral, i32 0, i32 1
  store %struct.vp_node* null, %struct.vp_node** %far, align 4, !tbaa !25
  %vantage_point = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral, i32 0, i32 2
  %6 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %7 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %7, i32 0
  %idx = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx, i32 0, i32 1
  %8 = load i32, i32* %idx, align 4, !tbaa !8
  %arrayidx3 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %6, i32 %8
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx3, i32 0, i32 0
  %9 = bitcast %struct.f_pixel* %vantage_point to i8*
  %10 = bitcast %struct.f_pixel* %acolor to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !26
  %radius = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral, i32 0, i32 3
  store float 0x4415AF1D80000000, float* %radius, align 4, !tbaa !27
  %idx4 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral, i32 0, i32 4
  %11 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %11, i32 0
  %idx6 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx5, i32 0, i32 1
  %12 = load i32, i32* %idx6, align 4, !tbaa !8
  store i32 %12, i32* %idx4, align 4, !tbaa !28
  %13 = bitcast %struct.vp_node* %5 to i8*
  %14 = bitcast %struct.vp_node* %.compoundliteral to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 32, i1 false), !tbaa.struct !29
  %15 = load %struct.vp_node*, %struct.vp_node** %node, align 4, !tbaa !2
  store %struct.vp_node* %15, %struct.vp_node** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  %16 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %18 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %19 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %call8 = call i32 @vp_find_best_vantage_point_index(%struct.vp_sort_tmp* %17, i32 %18, %struct.colormap_item* %19)
  store i32 %call8, i32* %ref, align 4, !tbaa !6
  %20 = bitcast i32* %ref_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %22 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %21, i32 %22
  %idx10 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx9, i32 0, i32 1
  %23 = load i32, i32* %idx10, align 4, !tbaa !8
  store i32 %23, i32* %ref_idx, align 4, !tbaa !6
  %24 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %24, 1
  store i32 %sub, i32* %num_indexes.addr, align 4, !tbaa !6
  %25 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %26 = load i32, i32* %ref, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %25, i32 %26
  %27 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %28 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %27, i32 %28
  %29 = bitcast %struct.vp_sort_tmp* %arrayidx11 to i8*
  %30 = bitcast %struct.vp_sort_tmp* %arrayidx12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 8, i1 false), !tbaa.struct !30
  %31 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %32 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %31, i32 %32
  %acolor14 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx13, i32 0, i32 0
  %33 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %34 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %35 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  call void @vp_sort_indexes_by_distance(%struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor14, %struct.vp_sort_tmp* %33, i32 %34, %struct.colormap_item* %35)
  %36 = bitcast i32* %half_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %div = sdiv i32 %37, 2
  store i32 %div, i32* %half_idx, align 4, !tbaa !6
  %38 = load %struct.vp_node*, %struct.vp_node** %node, align 4, !tbaa !2
  %near16 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral15, i32 0, i32 0
  store %struct.vp_node* null, %struct.vp_node** %near16, align 4, !tbaa !22
  %far17 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral15, i32 0, i32 1
  store %struct.vp_node* null, %struct.vp_node** %far17, align 4, !tbaa !25
  %vantage_point18 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral15, i32 0, i32 2
  %39 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %40 = load i32, i32* %ref_idx, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %39, i32 %40
  %acolor20 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx19, i32 0, i32 0
  %41 = bitcast %struct.f_pixel* %vantage_point18 to i8*
  %42 = bitcast %struct.f_pixel* %acolor20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %42, i32 16, i1 false), !tbaa.struct !26
  %radius21 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral15, i32 0, i32 3
  %43 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %44 = load i32, i32* %half_idx, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %43, i32 %44
  %distance_squared = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx22, i32 0, i32 0
  %45 = load float, float* %distance_squared, align 4, !tbaa !31
  %46 = call float @llvm.sqrt.f32(float %45)
  store float %46, float* %radius21, align 4, !tbaa !27
  %idx23 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %.compoundliteral15, i32 0, i32 4
  %47 = load i32, i32* %ref_idx, align 4, !tbaa !6
  store i32 %47, i32* %idx23, align 4, !tbaa !28
  %48 = bitcast %struct.vp_node* %38 to i8*
  %49 = bitcast %struct.vp_node* %.compoundliteral15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 32, i1 false), !tbaa.struct !29
  %50 = load %struct.mempool**, %struct.mempool*** %m.addr, align 4, !tbaa !2
  %51 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %52 = load i32, i32* %half_idx, align 4, !tbaa !6
  %53 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %call24 = call %struct.vp_node* @vp_create_node(%struct.mempool** %50, %struct.vp_sort_tmp* %51, i32 %52, %struct.colormap_item* %53)
  %54 = load %struct.vp_node*, %struct.vp_node** %node, align 4, !tbaa !2
  %near25 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %54, i32 0, i32 0
  store %struct.vp_node* %call24, %struct.vp_node** %near25, align 4, !tbaa !22
  %55 = load %struct.mempool**, %struct.mempool*** %m.addr, align 4, !tbaa !2
  %56 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %57 = load i32, i32* %half_idx, align 4, !tbaa !6
  %arrayidx26 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %56, i32 %57
  %58 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %59 = load i32, i32* %half_idx, align 4, !tbaa !6
  %sub27 = sub nsw i32 %58, %59
  %60 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %call28 = call %struct.vp_node* @vp_create_node(%struct.mempool** %55, %struct.vp_sort_tmp* %arrayidx26, i32 %sub27, %struct.colormap_item* %60)
  %61 = load %struct.vp_node*, %struct.vp_node** %node, align 4, !tbaa !2
  %far29 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %61, i32 0, i32 1
  store %struct.vp_node* %call28, %struct.vp_node** %far29, align 4, !tbaa !25
  %62 = load %struct.vp_node*, %struct.vp_node** %node, align 4, !tbaa !2
  store %struct.vp_node* %62, %struct.vp_node** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %63 = bitcast i32* %half_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #3
  %64 = bitcast i32* %ref_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #3
  %65 = bitcast i32* %ref to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #3
  br label %cleanup

cleanup:                                          ; preds = %if.end7, %if.then2
  %66 = bitcast %struct.vp_node** %node to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #3
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %67 = load %struct.vp_node*, %struct.vp_node** %retval, align 4
  ret %struct.vp_node* %67
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i1 immarg) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal void @vp_search_node(%struct.vp_node* %node, %struct.f_pixel* %needle, %struct.vp_search_tmp* %best_candidate) #0 {
entry:
  %node.addr = alloca %struct.vp_node*, align 4
  %needle.addr = alloca %struct.f_pixel*, align 4
  %best_candidate.addr = alloca %struct.vp_search_tmp*, align 4
  %distance = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.vp_node* %node, %struct.vp_node** %node.addr, align 4, !tbaa !2
  store %struct.f_pixel* %needle, %struct.f_pixel** %needle.addr, align 4, !tbaa !2
  store %struct.vp_search_tmp* %best_candidate, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %0 = bitcast float* %distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %vantage_point = getelementptr inbounds %struct.vp_node, %struct.vp_node* %1, i32 0, i32 2
  %2 = load %struct.f_pixel*, %struct.f_pixel** %needle.addr, align 4, !tbaa !2
  %call = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %vantage_point, %struct.f_pixel* byval(%struct.f_pixel) align 4 %2)
  %3 = call float @llvm.sqrt.f32(float %call)
  store float %3, float* %distance, align 4, !tbaa !21
  %4 = load float, float* %distance, align 4, !tbaa !21
  %5 = load %struct.vp_search_tmp*, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  %distance1 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %5, i32 0, i32 0
  %6 = load float, float* %distance1, align 4, !tbaa !17
  %cmp = fcmp olt float %4, %6
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %do.body
  %7 = load %struct.vp_search_tmp*, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  %exclude = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %7, i32 0, i32 2
  %8 = load i32, i32* %exclude, align 4, !tbaa !20
  %9 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %idx = getelementptr inbounds %struct.vp_node, %struct.vp_node* %9, i32 0, i32 4
  %10 = load i32, i32* %idx, align 4, !tbaa !28
  %cmp2 = icmp ne i32 %8, %10
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %11 = load float, float* %distance, align 4, !tbaa !21
  %12 = load %struct.vp_search_tmp*, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  %distance3 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %12, i32 0, i32 0
  store float %11, float* %distance3, align 4, !tbaa !17
  %13 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %idx4 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %13, i32 0, i32 4
  %14 = load i32, i32* %idx4, align 4, !tbaa !28
  %15 = load %struct.vp_search_tmp*, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  %idx5 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %15, i32 0, i32 1
  store i32 %14, i32* %idx5, align 4, !tbaa !19
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %do.body
  %16 = load float, float* %distance, align 4, !tbaa !21
  %17 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %radius = getelementptr inbounds %struct.vp_node, %struct.vp_node* %17, i32 0, i32 3
  %18 = load float, float* %radius, align 4, !tbaa !27
  %cmp6 = fcmp olt float %16, %18
  br i1 %cmp6, label %if.then7, label %if.else19

if.then7:                                         ; preds = %if.end
  %19 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %near = getelementptr inbounds %struct.vp_node, %struct.vp_node* %19, i32 0, i32 0
  %20 = load %struct.vp_node*, %struct.vp_node** %near, align 4, !tbaa !22
  %tobool = icmp ne %struct.vp_node* %20, null
  br i1 %tobool, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.then7
  %21 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %near9 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %21, i32 0, i32 0
  %22 = load %struct.vp_node*, %struct.vp_node** %near9, align 4, !tbaa !22
  %23 = load %struct.f_pixel*, %struct.f_pixel** %needle.addr, align 4, !tbaa !2
  %24 = load %struct.vp_search_tmp*, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  call void @vp_search_node(%struct.vp_node* %22, %struct.f_pixel* %23, %struct.vp_search_tmp* %24)
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.then7
  %25 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %far = getelementptr inbounds %struct.vp_node, %struct.vp_node* %25, i32 0, i32 1
  %26 = load %struct.vp_node*, %struct.vp_node** %far, align 4, !tbaa !25
  %tobool11 = icmp ne %struct.vp_node* %26, null
  br i1 %tobool11, label %land.lhs.true12, label %if.else

land.lhs.true12:                                  ; preds = %if.end10
  %27 = load float, float* %distance, align 4, !tbaa !21
  %28 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %radius13 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %28, i32 0, i32 3
  %29 = load float, float* %radius13, align 4, !tbaa !27
  %30 = load %struct.vp_search_tmp*, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  %distance14 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %30, i32 0, i32 0
  %31 = load float, float* %distance14, align 4, !tbaa !17
  %sub = fsub float %29, %31
  %cmp15 = fcmp oge float %27, %sub
  br i1 %cmp15, label %if.then16, label %if.else

if.then16:                                        ; preds = %land.lhs.true12
  %32 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %far17 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %32, i32 0, i32 1
  %33 = load %struct.vp_node*, %struct.vp_node** %far17, align 4, !tbaa !25
  store %struct.vp_node* %33, %struct.vp_node** %node.addr, align 4, !tbaa !2
  br label %if.end18

if.else:                                          ; preds = %land.lhs.true12, %if.end10
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end18:                                         ; preds = %if.then16
  br label %if.end35

if.else19:                                        ; preds = %if.end
  %34 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %far20 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %34, i32 0, i32 1
  %35 = load %struct.vp_node*, %struct.vp_node** %far20, align 4, !tbaa !25
  %tobool21 = icmp ne %struct.vp_node* %35, null
  br i1 %tobool21, label %if.then22, label %if.end24

if.then22:                                        ; preds = %if.else19
  %36 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %far23 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %36, i32 0, i32 1
  %37 = load %struct.vp_node*, %struct.vp_node** %far23, align 4, !tbaa !25
  %38 = load %struct.f_pixel*, %struct.f_pixel** %needle.addr, align 4, !tbaa !2
  %39 = load %struct.vp_search_tmp*, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  call void @vp_search_node(%struct.vp_node* %37, %struct.f_pixel* %38, %struct.vp_search_tmp* %39)
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.else19
  %40 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %near25 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %40, i32 0, i32 0
  %41 = load %struct.vp_node*, %struct.vp_node** %near25, align 4, !tbaa !22
  %tobool26 = icmp ne %struct.vp_node* %41, null
  br i1 %tobool26, label %land.lhs.true27, label %if.else33

land.lhs.true27:                                  ; preds = %if.end24
  %42 = load float, float* %distance, align 4, !tbaa !21
  %43 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %radius28 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %43, i32 0, i32 3
  %44 = load float, float* %radius28, align 4, !tbaa !27
  %45 = load %struct.vp_search_tmp*, %struct.vp_search_tmp** %best_candidate.addr, align 4, !tbaa !2
  %distance29 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %45, i32 0, i32 0
  %46 = load float, float* %distance29, align 4, !tbaa !17
  %add = fadd float %44, %46
  %cmp30 = fcmp ole float %42, %add
  br i1 %cmp30, label %if.then31, label %if.else33

if.then31:                                        ; preds = %land.lhs.true27
  %47 = load %struct.vp_node*, %struct.vp_node** %node.addr, align 4, !tbaa !2
  %near32 = getelementptr inbounds %struct.vp_node, %struct.vp_node* %47, i32 0, i32 0
  %48 = load %struct.vp_node*, %struct.vp_node** %near32, align 4, !tbaa !22
  store %struct.vp_node* %48, %struct.vp_node** %node.addr, align 4, !tbaa !2
  br label %if.end34

if.else33:                                        ; preds = %land.lhs.true27, %if.end24
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end34:                                         ; preds = %if.then31
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %if.end18
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end35, %if.else33, %if.else
  %49 = bitcast float* %distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 2, label %do.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %do.cond

do.cond:                                          ; preds = %cleanup.cont
  br i1 true, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
declare void @llvm.stackrestore(i8*) #3

; Function Attrs: nounwind
define hidden i32 @nearest_search(%struct.nearest_map* %handle, %struct.f_pixel* %px, i32 %likely_colormap_index, float* %diff) #0 {
entry:
  %retval = alloca i32, align 4
  %handle.addr = alloca %struct.nearest_map*, align 4
  %px.addr = alloca %struct.f_pixel*, align 4
  %likely_colormap_index.addr = alloca i32, align 4
  %diff.addr = alloca float*, align 4
  %guess_diff = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %best_candidate = alloca %struct.vp_search_tmp, align 4
  store %struct.nearest_map* %handle, %struct.nearest_map** %handle.addr, align 4, !tbaa !2
  store %struct.f_pixel* %px, %struct.f_pixel** %px.addr, align 4, !tbaa !2
  store i32 %likely_colormap_index, i32* %likely_colormap_index.addr, align 4, !tbaa !6
  store float* %diff, float** %diff.addr, align 4, !tbaa !2
  %0 = bitcast float* %guess_diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.nearest_map*, %struct.nearest_map** %handle.addr, align 4, !tbaa !2
  %palette = getelementptr inbounds %struct.nearest_map, %struct.nearest_map* %1, i32 0, i32 1
  %2 = load %struct.colormap_item*, %struct.colormap_item** %palette, align 4, !tbaa !13
  %3 = load i32, i32* %likely_colormap_index.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %2, i32 %3
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx, i32 0, i32 0
  %4 = load %struct.f_pixel*, %struct.f_pixel** %px.addr, align 4, !tbaa !2
  %call = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor, %struct.f_pixel* byval(%struct.f_pixel) align 4 %4)
  store float %call, float* %guess_diff, align 4, !tbaa !21
  %5 = load float, float* %guess_diff, align 4, !tbaa !21
  %6 = load %struct.nearest_map*, %struct.nearest_map** %handle.addr, align 4, !tbaa !2
  %nearest_other_color_dist = getelementptr inbounds %struct.nearest_map, %struct.nearest_map* %6, i32 0, i32 2
  %7 = load i32, i32* %likely_colormap_index.addr, align 4, !tbaa !6
  %arrayidx1 = getelementptr inbounds [256 x float], [256 x float]* %nearest_other_color_dist, i32 0, i32 %7
  %8 = load float, float* %arrayidx1, align 4, !tbaa !21
  %cmp = fcmp olt float %5, %8
  br i1 %cmp, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %9 = load float*, float** %diff.addr, align 4, !tbaa !2
  %tobool = icmp ne float* %9, null
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %10 = load float, float* %guess_diff, align 4, !tbaa !21
  %11 = load float*, float** %diff.addr, align 4, !tbaa !2
  store float %10, float* %11, align 4, !tbaa !21
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %12 = load i32, i32* %likely_colormap_index.addr, align 4, !tbaa !6
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end3:                                          ; preds = %entry
  %13 = bitcast %struct.vp_search_tmp* %best_candidate to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %13) #3
  %distance = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best_candidate, i32 0, i32 0
  %14 = load float, float* %guess_diff, align 4, !tbaa !21
  %15 = call float @llvm.sqrt.f32(float %14)
  store float %15, float* %distance, align 4, !tbaa !17
  %idx = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best_candidate, i32 0, i32 1
  %16 = load i32, i32* %likely_colormap_index.addr, align 4, !tbaa !6
  store i32 %16, i32* %idx, align 4, !tbaa !19
  %exclude = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best_candidate, i32 0, i32 2
  store i32 -1, i32* %exclude, align 4, !tbaa !20
  %17 = load %struct.nearest_map*, %struct.nearest_map** %handle.addr, align 4, !tbaa !2
  %root = getelementptr inbounds %struct.nearest_map, %struct.nearest_map* %17, i32 0, i32 0
  %18 = load %struct.vp_node*, %struct.vp_node** %root, align 4, !tbaa !11
  %19 = load %struct.f_pixel*, %struct.f_pixel** %px.addr, align 4, !tbaa !2
  call void @vp_search_node(%struct.vp_node* %18, %struct.f_pixel* %19, %struct.vp_search_tmp* %best_candidate)
  %20 = load float*, float** %diff.addr, align 4, !tbaa !2
  %tobool4 = icmp ne float* %20, null
  br i1 %tobool4, label %if.then5, label %if.end8

if.then5:                                         ; preds = %if.end3
  %distance6 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best_candidate, i32 0, i32 0
  %21 = load float, float* %distance6, align 4, !tbaa !17
  %distance7 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best_candidate, i32 0, i32 0
  %22 = load float, float* %distance7, align 4, !tbaa !17
  %mul = fmul float %21, %22
  %23 = load float*, float** %diff.addr, align 4, !tbaa !2
  store float %mul, float* %23, align 4, !tbaa !21
  br label %if.end8

if.end8:                                          ; preds = %if.then5, %if.end3
  %idx9 = getelementptr inbounds %struct.vp_search_tmp, %struct.vp_search_tmp* %best_candidate, i32 0, i32 1
  %24 = load i32, i32* %idx9, align 4, !tbaa !19
  store i32 %24, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %25 = bitcast %struct.vp_search_tmp* %best_candidate to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %25) #3
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.end
  %26 = bitcast float* %guess_diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #3
  %27 = load i32, i32* %retval, align 4
  ret i32 %27
}

; Function Attrs: alwaysinline nounwind
define internal float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py) #5 {
entry:
  %call = call float @colordifference_stdc(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: nounwind
define hidden void @nearest_free(%struct.nearest_map* %centroids) #0 {
entry:
  %centroids.addr = alloca %struct.nearest_map*, align 4
  store %struct.nearest_map* %centroids, %struct.nearest_map** %centroids.addr, align 4, !tbaa !2
  %0 = load %struct.nearest_map*, %struct.nearest_map** %centroids.addr, align 4, !tbaa !2
  %mempool = getelementptr inbounds %struct.nearest_map, %struct.nearest_map* %0, i32 0, i32 3
  %1 = load %struct.mempool*, %struct.mempool** %mempool, align 4, !tbaa !14
  call void @mempool_destroy(%struct.mempool* %1)
  ret void
}

declare hidden void @mempool_destroy(%struct.mempool*) #2

declare hidden i8* @mempool_alloc(%struct.mempool**, i32, i32) #2

; Function Attrs: nounwind
define internal i32 @vp_find_best_vantage_point_index(%struct.vp_sort_tmp* %indexes, i32 %num_indexes, %struct.colormap_item* %items) #0 {
entry:
  %indexes.addr = alloca %struct.vp_sort_tmp*, align 4
  %num_indexes.addr = alloca i32, align 4
  %items.addr = alloca %struct.colormap_item*, align 4
  %best = alloca i32, align 4
  %best_popularity = alloca float, align 4
  %i = alloca i32, align 4
  store %struct.vp_sort_tmp* %indexes, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  store i32 %num_indexes, i32* %num_indexes.addr, align 4, !tbaa !6
  store %struct.colormap_item* %items, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %0 = bitcast i32* %best to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %best, align 4, !tbaa !6
  %1 = bitcast float* %best_popularity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %3 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %3, i32 0
  %idx = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx, i32 0, i32 1
  %4 = load i32, i32* %idx, align 4, !tbaa !8
  %arrayidx1 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %2, i32 %4
  %popularity = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx1, i32 0, i32 1
  %5 = load float, float* %popularity, align 4, !tbaa !32
  store float %5, float* %best_popularity, align 4, !tbaa !21
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %8 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %7, %8
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %11 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %11, i32 %12
  %idx3 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx2, i32 0, i32 1
  %13 = load i32, i32* %idx3, align 4, !tbaa !8
  %arrayidx4 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %10, i32 %13
  %popularity5 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx4, i32 0, i32 1
  %14 = load float, float* %popularity5, align 4, !tbaa !32
  %15 = load float, float* %best_popularity, align 4, !tbaa !21
  %cmp6 = fcmp ogt float %14, %15
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %16 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %17 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %17, i32 %18
  %idx8 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx7, i32 0, i32 1
  %19 = load i32, i32* %idx8, align 4, !tbaa !8
  %arrayidx9 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %16, i32 %19
  %popularity10 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx9, i32 0, i32 1
  %20 = load float, float* %popularity10, align 4, !tbaa !32
  store float %20, float* %best_popularity, align 4, !tbaa !21
  %21 = load i32, i32* %i, align 4, !tbaa !6
  store i32 %21, i32* %best, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %23 = load i32, i32* %best, align 4, !tbaa !6
  %24 = bitcast float* %best_popularity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  %25 = bitcast i32* %best to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #3
  ret i32 %23
}

; Function Attrs: nounwind
define internal void @vp_sort_indexes_by_distance(%struct.f_pixel* byval(%struct.f_pixel) align 4 %vantage_point, %struct.vp_sort_tmp* %indexes, i32 %num_indexes, %struct.colormap_item* %items) #0 {
entry:
  %indexes.addr = alloca %struct.vp_sort_tmp*, align 4
  %num_indexes.addr = alloca i32, align 4
  %items.addr = alloca %struct.colormap_item*, align 4
  %i = alloca i32, align 4
  store %struct.vp_sort_tmp* %indexes, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  store i32 %num_indexes, i32* %num_indexes.addr, align 4, !tbaa !6
  store %struct.colormap_item* %items, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #3
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.colormap_item*, %struct.colormap_item** %items.addr, align 4, !tbaa !2
  %5 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %5, i32 %6
  %idx = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx, i32 0, i32 1
  %7 = load i32, i32* %idx, align 4, !tbaa !8
  %arrayidx1 = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %4, i32 %7
  %acolor = getelementptr inbounds %struct.colormap_item, %struct.colormap_item* %arrayidx1, i32 0, i32 0
  %call = call float @colordifference(%struct.f_pixel* byval(%struct.f_pixel) align 4 %vantage_point, %struct.f_pixel* byval(%struct.f_pixel) align 4 %acolor)
  %8 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %8, i32 %9
  %distance_squared = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %arrayidx2, i32 0, i32 0
  store float %call, float* %distance_squared, align 4, !tbaa !31
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %11 = load %struct.vp_sort_tmp*, %struct.vp_sort_tmp** %indexes.addr, align 4, !tbaa !2
  %12 = bitcast %struct.vp_sort_tmp* %11 to i8*
  %13 = load i32, i32* %num_indexes.addr, align 4, !tbaa !6
  call void @qsort(i8* %12, i32 %13, i32 8, i32 (i8*, i8*)* @vp_compare_distance)
  ret void
}

declare void @qsort(i8*, i32, i32, i32 (i8*, i8*)*) #2

; Function Attrs: nounwind
define internal i32 @vp_compare_distance(i8* %ap, i8* %bp) #0 {
entry:
  %ap.addr = alloca i8*, align 4
  %bp.addr = alloca i8*, align 4
  %a = alloca float, align 4
  %b = alloca float, align 4
  store i8* %ap, i8** %ap.addr, align 4, !tbaa !2
  store i8* %bp, i8** %bp.addr, align 4, !tbaa !2
  %0 = bitcast float* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8*, i8** %ap.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.vp_sort_tmp*
  %distance_squared = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %2, i32 0, i32 0
  %3 = load float, float* %distance_squared, align 4, !tbaa !31
  store float %3, float* %a, align 4, !tbaa !21
  %4 = bitcast float* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i8*, i8** %bp.addr, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %struct.vp_sort_tmp*
  %distance_squared1 = getelementptr inbounds %struct.vp_sort_tmp, %struct.vp_sort_tmp* %6, i32 0, i32 0
  %7 = load float, float* %distance_squared1, align 4, !tbaa !31
  store float %7, float* %b, align 4, !tbaa !21
  %8 = load float, float* %a, align 4, !tbaa !21
  %9 = load float, float* %b, align 4, !tbaa !21
  %cmp = fcmp ogt float %8, %9
  %10 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 -1
  %11 = bitcast float* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  %12 = bitcast float* %a to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #3
  ret i32 %cond
}

; Function Attrs: alwaysinline nounwind
define internal float @colordifference_stdc(%struct.f_pixel* byval(%struct.f_pixel) align 4 %px, %struct.f_pixel* byval(%struct.f_pixel) align 4 %py) #5 {
entry:
  %alphas = alloca double, align 8
  %0 = bitcast double* %alphas to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #3
  %a = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 0
  %1 = load float, float* %a, align 4, !tbaa !35
  %a1 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 0
  %2 = load float, float* %a1, align 4, !tbaa !35
  %sub = fsub float %1, %2
  %conv = fpext float %sub to double
  store double %conv, double* %alphas, align 8, !tbaa !36
  %r = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 1
  %3 = load float, float* %r, align 4, !tbaa !38
  %conv2 = fpext float %3 to double
  %r3 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 1
  %4 = load float, float* %r3, align 4, !tbaa !38
  %conv4 = fpext float %4 to double
  %5 = load double, double* %alphas, align 8, !tbaa !36
  %call = call double @colordifference_ch(double %conv2, double %conv4, double %5)
  %g = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 2
  %6 = load float, float* %g, align 4, !tbaa !39
  %conv5 = fpext float %6 to double
  %g6 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 2
  %7 = load float, float* %g6, align 4, !tbaa !39
  %conv7 = fpext float %7 to double
  %8 = load double, double* %alphas, align 8, !tbaa !36
  %call8 = call double @colordifference_ch(double %conv5, double %conv7, double %8)
  %add = fadd double %call, %call8
  %b = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %px, i32 0, i32 3
  %9 = load float, float* %b, align 4, !tbaa !40
  %conv9 = fpext float %9 to double
  %b10 = getelementptr inbounds %struct.f_pixel, %struct.f_pixel* %py, i32 0, i32 3
  %10 = load float, float* %b10, align 4, !tbaa !40
  %conv11 = fpext float %10 to double
  %11 = load double, double* %alphas, align 8, !tbaa !36
  %call12 = call double @colordifference_ch(double %conv9, double %conv11, double %11)
  %add13 = fadd double %add, %call12
  %conv14 = fptrunc double %add13 to float
  %12 = bitcast double* %alphas to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #3
  ret float %conv14
}

; Function Attrs: alwaysinline nounwind
define internal double @colordifference_ch(double %x, double %y, double %alphas) #5 {
entry:
  %x.addr = alloca double, align 8
  %y.addr = alloca double, align 8
  %alphas.addr = alloca double, align 8
  %black = alloca double, align 8
  %white = alloca double, align 8
  store double %x, double* %x.addr, align 8, !tbaa !36
  store double %y, double* %y.addr, align 8, !tbaa !36
  store double %alphas, double* %alphas.addr, align 8, !tbaa !36
  %0 = bitcast double* %black to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #3
  %1 = load double, double* %x.addr, align 8, !tbaa !36
  %2 = load double, double* %y.addr, align 8, !tbaa !36
  %sub = fsub double %1, %2
  store double %sub, double* %black, align 8, !tbaa !36
  %3 = bitcast double* %white to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %3) #3
  %4 = load double, double* %black, align 8, !tbaa !36
  %5 = load double, double* %alphas.addr, align 8, !tbaa !36
  %add = fadd double %4, %5
  store double %add, double* %white, align 8, !tbaa !36
  %6 = load double, double* %black, align 8, !tbaa !36
  %7 = load double, double* %black, align 8, !tbaa !36
  %mul = fmul double %6, %7
  %8 = load double, double* %white, align 8, !tbaa !36
  %9 = load double, double* %white, align 8, !tbaa !36
  %mul1 = fmul double %8, %9
  %cmp = fcmp ogt double %mul, %mul1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load double, double* %black, align 8, !tbaa !36
  %11 = load double, double* %black, align 8, !tbaa !36
  %mul2 = fmul double %10, %11
  br label %cond.end

cond.false:                                       ; preds = %entry
  %12 = load double, double* %white, align 8, !tbaa !36
  %13 = load double, double* %white, align 8, !tbaa !36
  %mul3 = fmul double %12, %13
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %mul2, %cond.true ], [ %mul3, %cond.false ]
  %14 = bitcast double* %white to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %14) #3
  %15 = bitcast double* %black to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #3
  ret double %cond
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !7, i64 4}
!9 = !{!"vp_sort_tmp", !10, i64 0, !7, i64 4}
!10 = !{!"float", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"nearest_map", !3, i64 0, !3, i64 4, !4, i64 8, !3, i64 1032}
!13 = !{!12, !3, i64 4}
!14 = !{!12, !3, i64 1032}
!15 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 1024, !16, i64 1032, i64 4, !2}
!16 = !{!4, !4, i64 0}
!17 = !{!18, !10, i64 0}
!18 = !{!"vp_search_tmp", !10, i64 0, !7, i64 4, !7, i64 8}
!19 = !{!18, !7, i64 4}
!20 = !{!18, !7, i64 8}
!21 = !{!10, !10, i64 0}
!22 = !{!23, !3, i64 0}
!23 = !{!"vp_node", !3, i64 0, !3, i64 4, !24, i64 8, !10, i64 24, !7, i64 28}
!24 = !{!"", !10, i64 0, !10, i64 4, !10, i64 8, !10, i64 12}
!25 = !{!23, !3, i64 4}
!26 = !{i64 0, i64 4, !21, i64 4, i64 4, !21, i64 8, i64 4, !21, i64 12, i64 4, !21}
!27 = !{!23, !10, i64 24}
!28 = !{!23, !7, i64 28}
!29 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !21, i64 12, i64 4, !21, i64 16, i64 4, !21, i64 20, i64 4, !21, i64 24, i64 4, !21, i64 28, i64 4, !6}
!30 = !{i64 0, i64 4, !21, i64 4, i64 4, !6}
!31 = !{!9, !10, i64 0}
!32 = !{!33, !10, i64 16}
!33 = !{!"", !24, i64 0, !10, i64 16, !34, i64 20}
!34 = !{!"_Bool", !4, i64 0}
!35 = !{!24, !10, i64 0}
!36 = !{!37, !37, i64 0}
!37 = !{!"double", !4, i64 0}
!38 = !{!24, !10, i64 4}
!39 = !{!24, !10, i64 8}
!40 = !{!24, !10, i64 12}
