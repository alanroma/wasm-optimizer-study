; ModuleID = 'jquant2.c'
source_filename = "jquant2.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.my_cquantizer = type { %struct.jpeg_color_quantizer, i8**, i32, [32 x i16]**, i32, i16*, i32, i32* }
%struct.box = type { i32, i32, i32, i32, i32, i32, i32, i32 }

@c_scales = internal constant [3 x i32] [i32 2, i32 3, i32 1], align 4
@rgb_red = internal constant [17 x i32] [i32 -1, i32 -1, i32 0, i32 -1, i32 -1, i32 -1, i32 0, i32 0, i32 2, i32 2, i32 3, i32 1, i32 0, i32 2, i32 3, i32 1, i32 -1], align 16
@rgb_green = internal constant [17 x i32] [i32 -1, i32 -1, i32 1, i32 -1, i32 -1, i32 -1, i32 1, i32 1, i32 1, i32 1, i32 2, i32 2, i32 1, i32 1, i32 2, i32 2, i32 -1], align 16
@rgb_blue = internal constant [17 x i32] [i32 -1, i32 -1, i32 2, i32 -1, i32 -1, i32 -1, i32 2, i32 2, i32 0, i32 0, i32 1, i32 3, i32 2, i32 0, i32 1, i32 3, i32 -1], align 16

; Function Attrs: nounwind
define hidden void @jinit_2pass_quantizer(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %i = alloca i32, align 4
  %desired = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 1
  %3 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %3, i32 0, i32 0
  %4 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = bitcast %struct.jpeg_decompress_struct* %5 to %struct.jpeg_common_struct*
  %call = call i8* %4(%struct.jpeg_common_struct* %6, i32 1, i32 44)
  %7 = bitcast i8* %call to %struct.my_cquantizer*
  store %struct.my_cquantizer* %7, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %8 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %9 = bitcast %struct.my_cquantizer* %8 to %struct.jpeg_color_quantizer*
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 87
  store %struct.jpeg_color_quantizer* %9, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %11 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %11, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub, i32 0, i32 0
  store void (%struct.jpeg_decompress_struct*, i32)* @start_pass_2_quant, void (%struct.jpeg_decompress_struct*, i32)** %start_pass, align 4, !tbaa !15
  %12 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %12, i32 0, i32 0
  %new_color_map = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub2, i32 0, i32 3
  %new_color_map3 = bitcast {}** %new_color_map to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @new_color_map_2_quant, void (%struct.jpeg_decompress_struct*)** %new_color_map3, align 4, !tbaa !18
  %13 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %13, i32 0, i32 5
  store i16* null, i16** %fserrors, align 4, !tbaa !19
  %14 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %error_limiter = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %14, i32 0, i32 7
  store i32* null, i32** %error_limiter, align 4, !tbaa !20
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 29
  %16 = load i32, i32* %out_color_components, align 8, !tbaa !21
  %cmp = icmp ne i32 %16, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !22
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 5
  store i32 47, i32* %msg_code, align 4, !tbaa !23
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !22
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 0
  %21 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !25
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = bitcast %struct.jpeg_decompress_struct* %22 to %struct.jpeg_common_struct*
  call void %21(%struct.jpeg_common_struct* %23)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 1
  %25 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem5, align 4, !tbaa !6
  %alloc_small6 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %25, i32 0, i32 0
  %26 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small6, align 4, !tbaa !11
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = bitcast %struct.jpeg_decompress_struct* %27 to %struct.jpeg_common_struct*
  %call7 = call i8* %26(%struct.jpeg_common_struct* %28, i32 1, i32 128)
  %29 = bitcast i8* %call7 to [32 x i16]**
  %30 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %30, i32 0, i32 3
  store [32 x i16]** %29, [32 x i16]*** %histogram, align 4, !tbaa !26
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %31 = load i32, i32* %i, align 4, !tbaa !27
  %cmp8 = icmp slt i32 %31, 32
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 1
  %33 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem9, align 4, !tbaa !6
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %33, i32 0, i32 1
  %34 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !28
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %36 = bitcast %struct.jpeg_decompress_struct* %35 to %struct.jpeg_common_struct*
  %call10 = call i8* %34(%struct.jpeg_common_struct* %36, i32 1, i32 4096)
  %37 = bitcast i8* %call10 to [32 x i16]*
  %38 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram11 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %38, i32 0, i32 3
  %39 = load [32 x i16]**, [32 x i16]*** %histogram11, align 4, !tbaa !26
  %40 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [32 x i16]*, [32 x i16]** %39, i32 %40
  store [32 x i16]* %37, [32 x i16]** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %41 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %42 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %needs_zeroed = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %42, i32 0, i32 4
  store i32 1, i32* %needs_zeroed, align 4, !tbaa !29
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 26
  %44 = load i32, i32* %enable_2pass_quant, align 4, !tbaa !30
  %tobool = icmp ne i32 %44, 0
  br i1 %tobool, label %if.then12, label %if.else

if.then12:                                        ; preds = %for.end
  %45 = bitcast i32* %desired to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #3
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %desired_number_of_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 23
  %47 = load i32, i32* %desired_number_of_colors, align 8, !tbaa !31
  store i32 %47, i32* %desired, align 4, !tbaa !27
  %48 = load i32, i32* %desired, align 4, !tbaa !27
  %cmp13 = icmp slt i32 %48, 8
  br i1 %cmp13, label %if.then14, label %if.end22

if.then14:                                        ; preds = %if.then12
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 0
  %50 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 8, !tbaa !22
  %msg_code16 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %50, i32 0, i32 5
  store i32 56, i32* %msg_code16, align 4, !tbaa !23
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 0
  %52 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err17, align 8, !tbaa !22
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %52, i32 0, i32 6
  %i18 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx19 = getelementptr inbounds [8 x i32], [8 x i32]* %i18, i32 0, i32 0
  store i32 8, i32* %arrayidx19, align 4, !tbaa !32
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 0
  %54 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err20, align 8, !tbaa !22
  %error_exit21 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %54, i32 0, i32 0
  %55 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit21, align 4, !tbaa !25
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %57 = bitcast %struct.jpeg_decompress_struct* %56 to %struct.jpeg_common_struct*
  call void %55(%struct.jpeg_common_struct* %57)
  br label %if.end22

if.end22:                                         ; preds = %if.then14, %if.then12
  %58 = load i32, i32* %desired, align 4, !tbaa !27
  %cmp23 = icmp sgt i32 %58, 256
  br i1 %cmp23, label %if.then24, label %if.end33

if.then24:                                        ; preds = %if.end22
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 0
  %60 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err25, align 8, !tbaa !22
  %msg_code26 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %60, i32 0, i32 5
  store i32 57, i32* %msg_code26, align 4, !tbaa !23
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 0
  %62 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err27, align 8, !tbaa !22
  %msg_parm28 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %62, i32 0, i32 6
  %i29 = bitcast %union.anon* %msg_parm28 to [8 x i32]*
  %arrayidx30 = getelementptr inbounds [8 x i32], [8 x i32]* %i29, i32 0, i32 0
  store i32 256, i32* %arrayidx30, align 4, !tbaa !32
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %63, i32 0, i32 0
  %64 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err31, align 8, !tbaa !22
  %error_exit32 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %64, i32 0, i32 0
  %65 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit32, align 4, !tbaa !25
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %67 = bitcast %struct.jpeg_decompress_struct* %66 to %struct.jpeg_common_struct*
  call void %65(%struct.jpeg_common_struct* %67)
  br label %if.end33

if.end33:                                         ; preds = %if.then24, %if.end22
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem34 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %68, i32 0, i32 1
  %69 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem34, align 4, !tbaa !6
  %alloc_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %69, i32 0, i32 2
  %70 = load i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_sarray, align 4, !tbaa !33
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %72 = bitcast %struct.jpeg_decompress_struct* %71 to %struct.jpeg_common_struct*
  %73 = load i32, i32* %desired, align 4, !tbaa !27
  %call35 = call i8** %70(%struct.jpeg_common_struct* %72, i32 1, i32 %73, i32 3)
  %74 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_colormap = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %74, i32 0, i32 1
  store i8** %call35, i8*** %sv_colormap, align 4, !tbaa !34
  %75 = load i32, i32* %desired, align 4, !tbaa !27
  %76 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %desired36 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %76, i32 0, i32 2
  store i32 %75, i32* %desired36, align 4, !tbaa !35
  %77 = bitcast i32* %desired to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  br label %if.end38

if.else:                                          ; preds = %for.end
  %78 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_colormap37 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %78, i32 0, i32 1
  store i8** null, i8*** %sv_colormap37, align 4, !tbaa !34
  br label %if.end38

if.end38:                                         ; preds = %if.else, %if.end33
  %79 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %79, i32 0, i32 21
  %80 = load i32, i32* %dither_mode, align 8, !tbaa !36
  %cmp39 = icmp ne i32 %80, 0
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %if.end38
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode41 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 21
  store i32 2, i32* %dither_mode41, align 8, !tbaa !36
  br label %if.end42

if.end42:                                         ; preds = %if.then40, %if.end38
  %82 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode43 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %82, i32 0, i32 21
  %83 = load i32, i32* %dither_mode43, align 8, !tbaa !36
  %cmp44 = icmp eq i32 %83, 2
  br i1 %cmp44, label %if.then45, label %if.end50

if.then45:                                        ; preds = %if.end42
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %84, i32 0, i32 1
  %85 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem46, align 4, !tbaa !6
  %alloc_large47 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %85, i32 0, i32 1
  %86 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large47, align 4, !tbaa !28
  %87 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %88 = bitcast %struct.jpeg_decompress_struct* %87 to %struct.jpeg_common_struct*
  %89 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %89, i32 0, i32 27
  %90 = load i32, i32* %output_width, align 8, !tbaa !37
  %add = add i32 %90, 2
  %mul = mul i32 %add, 6
  %call48 = call i8* %86(%struct.jpeg_common_struct* %88, i32 1, i32 %mul)
  %91 = bitcast i8* %call48 to i16*
  %92 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors49 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %92, i32 0, i32 5
  store i16* %91, i16** %fserrors49, align 4, !tbaa !19
  %93 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @init_error_limit(%struct.jpeg_decompress_struct* %93)
  br label %if.end50

if.end50:                                         ; preds = %if.then45, %if.end42
  %94 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #3
  %95 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_2_quant(%struct.jpeg_decompress_struct* %cinfo, i32 %is_pre_scan) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %is_pre_scan.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %histogram = alloca [32 x i16]**, align 4
  %i = alloca i32, align 4
  %arraysize = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %is_pre_scan, i32* %is_pre_scan.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %5, i32 0, i32 3
  %6 = load [32 x i16]**, [32 x i16]*** %histogram2, align 4, !tbaa !26
  store [32 x i16]** %6, [32 x i16]*** %histogram, align 4, !tbaa !2
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 21
  %9 = load i32, i32* %dither_mode, align 8, !tbaa !36
  %cmp = icmp ne i32 %9, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 21
  store i32 2, i32* %dither_mode3, align 8, !tbaa !36
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load i32, i32* %is_pre_scan.addr, align 4, !tbaa !27
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %12 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %12, i32 0, i32 0
  %color_quantize = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)* @prescan_quantize, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)** %color_quantize, align 4, !tbaa !38
  %13 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %13, i32 0, i32 0
  %finish_pass = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub5, i32 0, i32 2
  %finish_pass6 = bitcast {}** %finish_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @finish_pass1, void (%struct.jpeg_decompress_struct*)** %finish_pass6, align 4, !tbaa !39
  %14 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %needs_zeroed = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %14, i32 0, i32 4
  store i32 1, i32* %needs_zeroed, align 4, !tbaa !29
  br label %if.end48

if.else:                                          ; preds = %if.end
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 21
  %16 = load i32, i32* %dither_mode7, align 8, !tbaa !36
  %cmp8 = icmp eq i32 %16, 2
  br i1 %cmp8, label %if.then9, label %if.else12

if.then9:                                         ; preds = %if.else
  %17 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub10 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %17, i32 0, i32 0
  %color_quantize11 = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub10, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)* @pass2_fs_dither, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)** %color_quantize11, align 4, !tbaa !38
  br label %if.end15

if.else12:                                        ; preds = %if.else
  %18 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub13 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %18, i32 0, i32 0
  %color_quantize14 = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub13, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)* @pass2_no_dither, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)** %color_quantize14, align 4, !tbaa !38
  br label %if.end15

if.end15:                                         ; preds = %if.else12, %if.then9
  %19 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub16 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %19, i32 0, i32 0
  %finish_pass17 = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub16, i32 0, i32 2
  %finish_pass18 = bitcast {}** %finish_pass17 to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @finish_pass2, void (%struct.jpeg_decompress_struct*)** %finish_pass18, align 4, !tbaa !39
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %actual_number_of_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 32
  %21 = load i32, i32* %actual_number_of_colors, align 4, !tbaa !40
  store i32 %21, i32* %i, align 4, !tbaa !27
  %22 = load i32, i32* %i, align 4, !tbaa !27
  %cmp19 = icmp slt i32 %22, 1
  br i1 %cmp19, label %if.then20, label %if.end24

if.then20:                                        ; preds = %if.end15
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !22
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 5
  store i32 56, i32* %msg_code, align 4, !tbaa !23
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 0
  %26 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err21, align 8, !tbaa !22
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %26, i32 0, i32 6
  %i22 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i22, i32 0, i32 0
  store i32 1, i32* %arrayidx, align 4, !tbaa !32
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err23 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err23, align 8, !tbaa !22
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 0
  %29 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !25
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_decompress_struct* %30 to %struct.jpeg_common_struct*
  call void %29(%struct.jpeg_common_struct* %31)
  br label %if.end24

if.end24:                                         ; preds = %if.then20, %if.end15
  %32 = load i32, i32* %i, align 4, !tbaa !27
  %cmp25 = icmp sgt i32 %32, 256
  br i1 %cmp25, label %if.then26, label %if.end35

if.then26:                                        ; preds = %if.end24
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 0
  %34 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err27, align 8, !tbaa !22
  %msg_code28 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %34, i32 0, i32 5
  store i32 57, i32* %msg_code28, align 4, !tbaa !23
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 0
  %36 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err29, align 8, !tbaa !22
  %msg_parm30 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %36, i32 0, i32 6
  %i31 = bitcast %union.anon* %msg_parm30 to [8 x i32]*
  %arrayidx32 = getelementptr inbounds [8 x i32], [8 x i32]* %i31, i32 0, i32 0
  store i32 256, i32* %arrayidx32, align 4, !tbaa !32
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err33 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 0
  %38 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err33, align 8, !tbaa !22
  %error_exit34 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %38, i32 0, i32 0
  %39 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit34, align 4, !tbaa !25
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %41 = bitcast %struct.jpeg_decompress_struct* %40 to %struct.jpeg_common_struct*
  call void %39(%struct.jpeg_common_struct* %41)
  br label %if.end35

if.end35:                                         ; preds = %if.then26, %if.end24
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode36 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 21
  %43 = load i32, i32* %dither_mode36, align 8, !tbaa !36
  %cmp37 = icmp eq i32 %43, 2
  br i1 %cmp37, label %if.then38, label %if.end47

if.then38:                                        ; preds = %if.end35
  %44 = bitcast i32* %arraysize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #3
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 27
  %46 = load i32, i32* %output_width, align 8, !tbaa !37
  %add = add i32 %46, 2
  %mul = mul i32 %add, 6
  store i32 %mul, i32* %arraysize, align 4, !tbaa !41
  %47 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %47, i32 0, i32 5
  %48 = load i16*, i16** %fserrors, align 4, !tbaa !19
  %cmp39 = icmp eq i16* %48, null
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %if.then38
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 1
  %50 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %50, i32 0, i32 1
  %51 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !28
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %53 = bitcast %struct.jpeg_decompress_struct* %52 to %struct.jpeg_common_struct*
  %54 = load i32, i32* %arraysize, align 4, !tbaa !41
  %call = call i8* %51(%struct.jpeg_common_struct* %53, i32 1, i32 %54)
  %55 = bitcast i8* %call to i16*
  %56 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors41 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %56, i32 0, i32 5
  store i16* %55, i16** %fserrors41, align 4, !tbaa !19
  br label %if.end42

if.end42:                                         ; preds = %if.then40, %if.then38
  %57 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors43 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %57, i32 0, i32 5
  %58 = load i16*, i16** %fserrors43, align 4, !tbaa !19
  %59 = bitcast i16* %58 to i8*
  %60 = load i32, i32* %arraysize, align 4, !tbaa !41
  call void @jzero_far(i8* %59, i32 %60)
  %61 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %error_limiter = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %61, i32 0, i32 7
  %62 = load i32*, i32** %error_limiter, align 4, !tbaa !20
  %cmp44 = icmp eq i32* %62, null
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.end42
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @init_error_limit(%struct.jpeg_decompress_struct* %63)
  br label %if.end46

if.end46:                                         ; preds = %if.then45, %if.end42
  %64 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %on_odd_row = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %64, i32 0, i32 6
  store i32 0, i32* %on_odd_row, align 4, !tbaa !42
  %65 = bitcast i32* %arraysize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #3
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.end35
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.then4
  %66 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %needs_zeroed49 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %66, i32 0, i32 4
  %67 = load i32, i32* %needs_zeroed49, align 4, !tbaa !29
  %tobool50 = icmp ne i32 %67, 0
  br i1 %tobool50, label %if.then51, label %if.end55

if.then51:                                        ; preds = %if.end48
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then51
  %68 = load i32, i32* %i, align 4, !tbaa !27
  %cmp52 = icmp slt i32 %68, 32
  br i1 %cmp52, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %69 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %70 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx53 = getelementptr inbounds [32 x i16]*, [32 x i16]** %69, i32 %70
  %71 = load [32 x i16]*, [32 x i16]** %arrayidx53, align 4, !tbaa !2
  %72 = bitcast [32 x i16]* %71 to i8*
  call void @jzero_far(i8* %72, i32 4096)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %73 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %73, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %74 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %needs_zeroed54 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %74, i32 0, i32 4
  store i32 0, i32* %needs_zeroed54, align 4, !tbaa !29
  br label %if.end55

if.end55:                                         ; preds = %for.end, %if.end48
  %75 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %77 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  ret void
}

; Function Attrs: nounwind
define internal void @new_color_map_2_quant(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %needs_zeroed = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %4, i32 0, i32 4
  store i32 1, i32* %needs_zeroed, align 4, !tbaa !29
  %5 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @init_error_limit(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %table = alloca i32*, align 4
  %in = alloca i32, align 4
  %out = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i32** %table to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %in to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %out to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 1
  %8 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %8, i32 0, i32 0
  %9 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = bitcast %struct.jpeg_decompress_struct* %10 to %struct.jpeg_common_struct*
  %call = call i8* %9(%struct.jpeg_common_struct* %11, i32 1, i32 2044)
  %12 = bitcast i8* %call to i32*
  store i32* %12, i32** %table, align 4, !tbaa !2
  %13 = load i32*, i32** %table, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %13, i32 255
  store i32* %add.ptr, i32** %table, align 4, !tbaa !2
  %14 = load i32*, i32** %table, align 4, !tbaa !2
  %15 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %error_limiter = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %15, i32 0, i32 7
  store i32* %14, i32** %error_limiter, align 4, !tbaa !20
  store i32 0, i32* %out, align 4, !tbaa !27
  store i32 0, i32* %in, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %16 = load i32, i32* %in, align 4, !tbaa !27
  %cmp = icmp slt i32 %16, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load i32, i32* %out, align 4, !tbaa !27
  %18 = load i32*, i32** %table, align 4, !tbaa !2
  %19 = load i32, i32* %in, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds i32, i32* %18, i32 %19
  store i32 %17, i32* %arrayidx, align 4, !tbaa !27
  %20 = load i32, i32* %out, align 4, !tbaa !27
  %sub = sub nsw i32 0, %20
  %21 = load i32*, i32** %table, align 4, !tbaa !2
  %22 = load i32, i32* %in, align 4, !tbaa !27
  %sub2 = sub nsw i32 0, %22
  %arrayidx3 = getelementptr inbounds i32, i32* %21, i32 %sub2
  store i32 %sub, i32* %arrayidx3, align 4, !tbaa !27
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %in, align 4, !tbaa !27
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %in, align 4, !tbaa !27
  %24 = load i32, i32* %out, align 4, !tbaa !27
  %inc4 = add nsw i32 %24, 1
  store i32 %inc4, i32* %out, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc12, %for.end
  %25 = load i32, i32* %in, align 4, !tbaa !27
  %cmp6 = icmp slt i32 %25, 48
  br i1 %cmp6, label %for.body7, label %for.end14

for.body7:                                        ; preds = %for.cond5
  %26 = load i32, i32* %out, align 4, !tbaa !27
  %27 = load i32*, i32** %table, align 4, !tbaa !2
  %28 = load i32, i32* %in, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i32, i32* %27, i32 %28
  store i32 %26, i32* %arrayidx8, align 4, !tbaa !27
  %29 = load i32, i32* %out, align 4, !tbaa !27
  %sub9 = sub nsw i32 0, %29
  %30 = load i32*, i32** %table, align 4, !tbaa !2
  %31 = load i32, i32* %in, align 4, !tbaa !27
  %sub10 = sub nsw i32 0, %31
  %arrayidx11 = getelementptr inbounds i32, i32* %30, i32 %sub10
  store i32 %sub9, i32* %arrayidx11, align 4, !tbaa !27
  br label %for.inc12

for.inc12:                                        ; preds = %for.body7
  %32 = load i32, i32* %in, align 4, !tbaa !27
  %inc13 = add nsw i32 %32, 1
  store i32 %inc13, i32* %in, align 4, !tbaa !27
  %33 = load i32, i32* %in, align 4, !tbaa !27
  %and = and i32 %33, 1
  %tobool = icmp ne i32 %and, 0
  %34 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 0, i32 1
  %35 = load i32, i32* %out, align 4, !tbaa !27
  %add = add nsw i32 %35, %cond
  store i32 %add, i32* %out, align 4, !tbaa !27
  br label %for.cond5

for.end14:                                        ; preds = %for.cond5
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc22, %for.end14
  %36 = load i32, i32* %in, align 4, !tbaa !27
  %cmp16 = icmp sle i32 %36, 255
  br i1 %cmp16, label %for.body17, label %for.end24

for.body17:                                       ; preds = %for.cond15
  %37 = load i32, i32* %out, align 4, !tbaa !27
  %38 = load i32*, i32** %table, align 4, !tbaa !2
  %39 = load i32, i32* %in, align 4, !tbaa !27
  %arrayidx18 = getelementptr inbounds i32, i32* %38, i32 %39
  store i32 %37, i32* %arrayidx18, align 4, !tbaa !27
  %40 = load i32, i32* %out, align 4, !tbaa !27
  %sub19 = sub nsw i32 0, %40
  %41 = load i32*, i32** %table, align 4, !tbaa !2
  %42 = load i32, i32* %in, align 4, !tbaa !27
  %sub20 = sub nsw i32 0, %42
  %arrayidx21 = getelementptr inbounds i32, i32* %41, i32 %sub20
  store i32 %sub19, i32* %arrayidx21, align 4, !tbaa !27
  br label %for.inc22

for.inc22:                                        ; preds = %for.body17
  %43 = load i32, i32* %in, align 4, !tbaa !27
  %inc23 = add nsw i32 %43, 1
  store i32 %inc23, i32* %in, align 4, !tbaa !27
  br label %for.cond15

for.end24:                                        ; preds = %for.cond15
  %44 = bitcast i32* %out to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast i32* %in to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast i32** %table to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  ret void
}

; Function Attrs: nounwind
define internal void @prescan_quantize(%struct.jpeg_decompress_struct* %cinfo, i8** %input_buf, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %ptr = alloca i8*, align 4
  %histp = alloca i16*, align 4
  %histogram = alloca [32 x i16]**, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i16** %histp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %7, i32 0, i32 3
  %8 = load [32 x i16]**, [32 x i16]*** %histogram2, align 4, !tbaa !26
  store [32 x i16]** %8, [32 x i16]*** %histogram, align 4, !tbaa !2
  %9 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 27
  %13 = load i32, i32* %output_width, align 8, !tbaa !37
  store i32 %13, i32* %width, align 4, !tbaa !27
  store i32 0, i32* %row, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc20, %entry
  %14 = load i32, i32* %row, align 4, !tbaa !27
  %15 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp slt i32 %14, %15
  br i1 %cmp, label %for.body, label %for.end22

for.body:                                         ; preds = %for.cond
  %16 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %17 = load i32, i32* %row, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds i8*, i8** %16, i32 %17
  %18 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %18, i8** %ptr, align 4, !tbaa !2
  %19 = load i32, i32* %width, align 4, !tbaa !27
  store i32 %19, i32* %col, align 4, !tbaa !27
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %20 = load i32, i32* %col, align 4, !tbaa !27
  %cmp4 = icmp ugt i32 %20, 0
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond3
  %21 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %22 = load i8*, i8** %ptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %22, i32 0
  %23 = load i8, i8* %arrayidx6, align 1, !tbaa !32
  %conv = zext i8 %23 to i32
  %shr = ashr i32 %conv, 3
  %arrayidx7 = getelementptr inbounds [32 x i16]*, [32 x i16]** %21, i32 %shr
  %24 = load [32 x i16]*, [32 x i16]** %arrayidx7, align 4, !tbaa !2
  %25 = load i8*, i8** %ptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %25, i32 1
  %26 = load i8, i8* %arrayidx8, align 1, !tbaa !32
  %conv9 = zext i8 %26 to i32
  %shr10 = ashr i32 %conv9, 2
  %arrayidx11 = getelementptr inbounds [32 x i16], [32 x i16]* %24, i32 %shr10
  %27 = load i8*, i8** %ptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %27, i32 2
  %28 = load i8, i8* %arrayidx12, align 1, !tbaa !32
  %conv13 = zext i8 %28 to i32
  %shr14 = ashr i32 %conv13, 3
  %arrayidx15 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx11, i32 0, i32 %shr14
  store i16* %arrayidx15, i16** %histp, align 4, !tbaa !2
  %29 = load i16*, i16** %histp, align 4, !tbaa !2
  %30 = load i16, i16* %29, align 2, !tbaa !43
  %inc = add i16 %30, 1
  store i16 %inc, i16* %29, align 2, !tbaa !43
  %conv16 = zext i16 %inc to i32
  %cmp17 = icmp sle i32 %conv16, 0
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %for.body5
  %31 = load i16*, i16** %histp, align 4, !tbaa !2
  %32 = load i16, i16* %31, align 2, !tbaa !43
  %dec = add i16 %32, -1
  store i16 %dec, i16* %31, align 2, !tbaa !43
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body5
  %33 = load i8*, i8** %ptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %33, i32 3
  store i8* %add.ptr, i8** %ptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %34 = load i32, i32* %col, align 4, !tbaa !27
  %dec19 = add i32 %34, -1
  store i32 %dec19, i32* %col, align 4, !tbaa !27
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc20

for.inc20:                                        ; preds = %for.end
  %35 = load i32, i32* %row, align 4, !tbaa !27
  %inc21 = add nsw i32 %35, 1
  store i32 %inc21, i32* %row, align 4, !tbaa !27
  br label %for.cond

for.end22:                                        ; preds = %for.cond
  %36 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  %37 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #3
  %38 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  %39 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  %40 = bitcast i16** %histp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  %41 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  %42 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  ret void
}

; Function Attrs: nounwind
define internal void @finish_pass1(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_colormap = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %4, i32 0, i32 1
  %5 = load i8**, i8*** %sv_colormap, align 4, !tbaa !34
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 33
  store i8** %5, i8*** %colormap, align 8, !tbaa !44
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %desired = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %8, i32 0, i32 2
  %9 = load i32, i32* %desired, align 4, !tbaa !35
  call void @select_colors(%struct.jpeg_decompress_struct* %7, i32 %9)
  %10 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %needs_zeroed = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %10, i32 0, i32 4
  store i32 1, i32* %needs_zeroed, align 4, !tbaa !29
  %11 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #3
  ret void
}

; Function Attrs: nounwind
define internal void @pass2_fs_dither(%struct.jpeg_decompress_struct* %cinfo, i8** %input_buf, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %histogram = alloca [32 x i16]**, align 4
  %cur0 = alloca i32, align 4
  %cur1 = alloca i32, align 4
  %cur2 = alloca i32, align 4
  %belowerr0 = alloca i32, align 4
  %belowerr1 = alloca i32, align 4
  %belowerr2 = alloca i32, align 4
  %bpreverr0 = alloca i32, align 4
  %bpreverr1 = alloca i32, align 4
  %bpreverr2 = alloca i32, align 4
  %errorptr = alloca i16*, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %cachep = alloca i16*, align 4
  %dir = alloca i32, align 4
  %dir3 = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %error_limit = alloca i32*, align 4
  %colormap0 = alloca i8*, align 4
  %colormap1 = alloca i8*, align 4
  %colormap2 = alloca i8*, align 4
  %pixcode = alloca i32, align 4
  %bnexterr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %5, i32 0, i32 3
  %6 = load [32 x i16]**, [32 x i16]*** %histogram2, align 4, !tbaa !26
  store [32 x i16]** %6, [32 x i16]*** %histogram, align 4, !tbaa !2
  %7 = bitcast i32* %cur0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %cur1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %cur2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %belowerr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %belowerr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %belowerr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %bpreverr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %bpreverr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %bpreverr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i16** %errorptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i16** %cachep to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i32* %dir3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 27
  %26 = load i32, i32* %output_width, align 8, !tbaa !37
  store i32 %26, i32* %width, align 4, !tbaa !27
  %27 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 65
  %29 = load i8*, i8** %sample_range_limit, align 4, !tbaa !45
  store i8* %29, i8** %range_limit, align 4, !tbaa !2
  %30 = bitcast i32** %error_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %error_limiter = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %31, i32 0, i32 7
  %32 = load i32*, i32** %error_limiter, align 4, !tbaa !20
  store i32* %32, i32** %error_limit, align 4, !tbaa !2
  %33 = bitcast i8** %colormap0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #3
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 33
  %35 = load i8**, i8*** %colormap, align 8, !tbaa !44
  %arrayidx = getelementptr inbounds i8*, i8** %35, i32 0
  %36 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %36, i8** %colormap0, align 4, !tbaa !2
  %37 = bitcast i8** %colormap1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #3
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 33
  %39 = load i8**, i8*** %colormap3, align 8, !tbaa !44
  %arrayidx4 = getelementptr inbounds i8*, i8** %39, i32 1
  %40 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %40, i8** %colormap1, align 4, !tbaa !2
  %41 = bitcast i8** %colormap2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #3
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 33
  %43 = load i8**, i8*** %colormap5, align 8, !tbaa !44
  %arrayidx6 = getelementptr inbounds i8*, i8** %43, i32 2
  %44 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %44, i8** %colormap2, align 4, !tbaa !2
  store i32 0, i32* %row, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc109, %entry
  %45 = load i32, i32* %row, align 4, !tbaa !27
  %46 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp slt i32 %45, %46
  br i1 %cmp, label %for.body, label %for.end110

for.body:                                         ; preds = %for.cond
  %47 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %48 = load i32, i32* %row, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8*, i8** %47, i32 %48
  %49 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %49, i8** %inptr, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %51 = load i32, i32* %row, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8*, i8** %50, i32 %51
  %52 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %52, i8** %outptr, align 4, !tbaa !2
  %53 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %on_odd_row = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %53, i32 0, i32 6
  %54 = load i32, i32* %on_odd_row, align 4, !tbaa !42
  %tobool = icmp ne i32 %54, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %55 = load i32, i32* %width, align 4, !tbaa !27
  %sub = sub i32 %55, 1
  %mul = mul i32 %sub, 3
  %56 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %56, i32 %mul
  store i8* %add.ptr, i8** %inptr, align 4, !tbaa !2
  %57 = load i32, i32* %width, align 4, !tbaa !27
  %sub9 = sub i32 %57, 1
  %58 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i8, i8* %58, i32 %sub9
  store i8* %add.ptr10, i8** %outptr, align 4, !tbaa !2
  store i32 -1, i32* %dir, align 4, !tbaa !27
  store i32 -3, i32* %dir3, align 4, !tbaa !27
  %59 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %59, i32 0, i32 5
  %60 = load i16*, i16** %fserrors, align 4, !tbaa !19
  %61 = load i32, i32* %width, align 4, !tbaa !27
  %add = add i32 %61, 1
  %mul11 = mul i32 %add, 3
  %add.ptr12 = getelementptr inbounds i16, i16* %60, i32 %mul11
  store i16* %add.ptr12, i16** %errorptr, align 4, !tbaa !2
  %62 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %on_odd_row13 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %62, i32 0, i32 6
  store i32 0, i32* %on_odd_row13, align 4, !tbaa !42
  br label %if.end

if.else:                                          ; preds = %for.body
  store i32 1, i32* %dir, align 4, !tbaa !27
  store i32 3, i32* %dir3, align 4, !tbaa !27
  %63 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors14 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %63, i32 0, i32 5
  %64 = load i16*, i16** %fserrors14, align 4, !tbaa !19
  store i16* %64, i16** %errorptr, align 4, !tbaa !2
  %65 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %on_odd_row15 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %65, i32 0, i32 6
  store i32 1, i32* %on_odd_row15, align 4, !tbaa !42
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  store i32 0, i32* %cur2, align 4, !tbaa !27
  store i32 0, i32* %cur1, align 4, !tbaa !27
  store i32 0, i32* %cur0, align 4, !tbaa !27
  store i32 0, i32* %belowerr2, align 4, !tbaa !27
  store i32 0, i32* %belowerr1, align 4, !tbaa !27
  store i32 0, i32* %belowerr0, align 4, !tbaa !27
  store i32 0, i32* %bpreverr2, align 4, !tbaa !27
  store i32 0, i32* %bpreverr1, align 4, !tbaa !27
  store i32 0, i32* %bpreverr0, align 4, !tbaa !27
  %66 = load i32, i32* %width, align 4, !tbaa !27
  store i32 %66, i32* %col, align 4, !tbaa !27
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc, %if.end
  %67 = load i32, i32* %col, align 4, !tbaa !27
  %cmp17 = icmp ugt i32 %67, 0
  br i1 %cmp17, label %for.body18, label %for.end

for.body18:                                       ; preds = %for.cond16
  %68 = load i32, i32* %cur0, align 4, !tbaa !27
  %69 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %70 = load i32, i32* %dir3, align 4, !tbaa !27
  %add19 = add nsw i32 %70, 0
  %arrayidx20 = getelementptr inbounds i16, i16* %69, i32 %add19
  %71 = load i16, i16* %arrayidx20, align 2, !tbaa !43
  %conv = sext i16 %71 to i32
  %add21 = add nsw i32 %68, %conv
  %add22 = add nsw i32 %add21, 8
  %shr = ashr i32 %add22, 4
  store i32 %shr, i32* %cur0, align 4, !tbaa !27
  %72 = load i32, i32* %cur1, align 4, !tbaa !27
  %73 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %74 = load i32, i32* %dir3, align 4, !tbaa !27
  %add23 = add nsw i32 %74, 1
  %arrayidx24 = getelementptr inbounds i16, i16* %73, i32 %add23
  %75 = load i16, i16* %arrayidx24, align 2, !tbaa !43
  %conv25 = sext i16 %75 to i32
  %add26 = add nsw i32 %72, %conv25
  %add27 = add nsw i32 %add26, 8
  %shr28 = ashr i32 %add27, 4
  store i32 %shr28, i32* %cur1, align 4, !tbaa !27
  %76 = load i32, i32* %cur2, align 4, !tbaa !27
  %77 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %78 = load i32, i32* %dir3, align 4, !tbaa !27
  %add29 = add nsw i32 %78, 2
  %arrayidx30 = getelementptr inbounds i16, i16* %77, i32 %add29
  %79 = load i16, i16* %arrayidx30, align 2, !tbaa !43
  %conv31 = sext i16 %79 to i32
  %add32 = add nsw i32 %76, %conv31
  %add33 = add nsw i32 %add32, 8
  %shr34 = ashr i32 %add33, 4
  store i32 %shr34, i32* %cur2, align 4, !tbaa !27
  %80 = load i32*, i32** %error_limit, align 4, !tbaa !2
  %81 = load i32, i32* %cur0, align 4, !tbaa !27
  %arrayidx35 = getelementptr inbounds i32, i32* %80, i32 %81
  %82 = load i32, i32* %arrayidx35, align 4, !tbaa !27
  store i32 %82, i32* %cur0, align 4, !tbaa !27
  %83 = load i32*, i32** %error_limit, align 4, !tbaa !2
  %84 = load i32, i32* %cur1, align 4, !tbaa !27
  %arrayidx36 = getelementptr inbounds i32, i32* %83, i32 %84
  %85 = load i32, i32* %arrayidx36, align 4, !tbaa !27
  store i32 %85, i32* %cur1, align 4, !tbaa !27
  %86 = load i32*, i32** %error_limit, align 4, !tbaa !2
  %87 = load i32, i32* %cur2, align 4, !tbaa !27
  %arrayidx37 = getelementptr inbounds i32, i32* %86, i32 %87
  %88 = load i32, i32* %arrayidx37, align 4, !tbaa !27
  store i32 %88, i32* %cur2, align 4, !tbaa !27
  %89 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i8, i8* %89, i32 0
  %90 = load i8, i8* %arrayidx38, align 1, !tbaa !32
  %conv39 = zext i8 %90 to i32
  %91 = load i32, i32* %cur0, align 4, !tbaa !27
  %add40 = add nsw i32 %91, %conv39
  store i32 %add40, i32* %cur0, align 4, !tbaa !27
  %92 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i8, i8* %92, i32 1
  %93 = load i8, i8* %arrayidx41, align 1, !tbaa !32
  %conv42 = zext i8 %93 to i32
  %94 = load i32, i32* %cur1, align 4, !tbaa !27
  %add43 = add nsw i32 %94, %conv42
  store i32 %add43, i32* %cur1, align 4, !tbaa !27
  %95 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i8, i8* %95, i32 2
  %96 = load i8, i8* %arrayidx44, align 1, !tbaa !32
  %conv45 = zext i8 %96 to i32
  %97 = load i32, i32* %cur2, align 4, !tbaa !27
  %add46 = add nsw i32 %97, %conv45
  store i32 %add46, i32* %cur2, align 4, !tbaa !27
  %98 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %99 = load i32, i32* %cur0, align 4, !tbaa !27
  %arrayidx47 = getelementptr inbounds i8, i8* %98, i32 %99
  %100 = load i8, i8* %arrayidx47, align 1, !tbaa !32
  %conv48 = zext i8 %100 to i32
  store i32 %conv48, i32* %cur0, align 4, !tbaa !27
  %101 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %102 = load i32, i32* %cur1, align 4, !tbaa !27
  %arrayidx49 = getelementptr inbounds i8, i8* %101, i32 %102
  %103 = load i8, i8* %arrayidx49, align 1, !tbaa !32
  %conv50 = zext i8 %103 to i32
  store i32 %conv50, i32* %cur1, align 4, !tbaa !27
  %104 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %105 = load i32, i32* %cur2, align 4, !tbaa !27
  %arrayidx51 = getelementptr inbounds i8, i8* %104, i32 %105
  %106 = load i8, i8* %arrayidx51, align 1, !tbaa !32
  %conv52 = zext i8 %106 to i32
  store i32 %conv52, i32* %cur2, align 4, !tbaa !27
  %107 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %108 = load i32, i32* %cur0, align 4, !tbaa !27
  %shr53 = ashr i32 %108, 3
  %arrayidx54 = getelementptr inbounds [32 x i16]*, [32 x i16]** %107, i32 %shr53
  %109 = load [32 x i16]*, [32 x i16]** %arrayidx54, align 4, !tbaa !2
  %110 = load i32, i32* %cur1, align 4, !tbaa !27
  %shr55 = ashr i32 %110, 2
  %arrayidx56 = getelementptr inbounds [32 x i16], [32 x i16]* %109, i32 %shr55
  %111 = load i32, i32* %cur2, align 4, !tbaa !27
  %shr57 = ashr i32 %111, 3
  %arrayidx58 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx56, i32 0, i32 %shr57
  store i16* %arrayidx58, i16** %cachep, align 4, !tbaa !2
  %112 = load i16*, i16** %cachep, align 4, !tbaa !2
  %113 = load i16, i16* %112, align 2, !tbaa !43
  %conv59 = zext i16 %113 to i32
  %cmp60 = icmp eq i32 %conv59, 0
  br i1 %cmp60, label %if.then62, label %if.end66

if.then62:                                        ; preds = %for.body18
  %114 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %115 = load i32, i32* %cur0, align 4, !tbaa !27
  %shr63 = ashr i32 %115, 3
  %116 = load i32, i32* %cur1, align 4, !tbaa !27
  %shr64 = ashr i32 %116, 2
  %117 = load i32, i32* %cur2, align 4, !tbaa !27
  %shr65 = ashr i32 %117, 3
  call void @fill_inverse_cmap(%struct.jpeg_decompress_struct* %114, i32 %shr63, i32 %shr64, i32 %shr65)
  br label %if.end66

if.end66:                                         ; preds = %if.then62, %for.body18
  %118 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #3
  %119 = load i16*, i16** %cachep, align 4, !tbaa !2
  %120 = load i16, i16* %119, align 2, !tbaa !43
  %conv67 = zext i16 %120 to i32
  %sub68 = sub nsw i32 %conv67, 1
  store i32 %sub68, i32* %pixcode, align 4, !tbaa !27
  %121 = load i32, i32* %pixcode, align 4, !tbaa !27
  %conv69 = trunc i32 %121 to i8
  %122 = load i8*, i8** %outptr, align 4, !tbaa !2
  store i8 %conv69, i8* %122, align 1, !tbaa !32
  %123 = load i8*, i8** %colormap0, align 4, !tbaa !2
  %124 = load i32, i32* %pixcode, align 4, !tbaa !27
  %arrayidx70 = getelementptr inbounds i8, i8* %123, i32 %124
  %125 = load i8, i8* %arrayidx70, align 1, !tbaa !32
  %conv71 = zext i8 %125 to i32
  %126 = load i32, i32* %cur0, align 4, !tbaa !27
  %sub72 = sub nsw i32 %126, %conv71
  store i32 %sub72, i32* %cur0, align 4, !tbaa !27
  %127 = load i8*, i8** %colormap1, align 4, !tbaa !2
  %128 = load i32, i32* %pixcode, align 4, !tbaa !27
  %arrayidx73 = getelementptr inbounds i8, i8* %127, i32 %128
  %129 = load i8, i8* %arrayidx73, align 1, !tbaa !32
  %conv74 = zext i8 %129 to i32
  %130 = load i32, i32* %cur1, align 4, !tbaa !27
  %sub75 = sub nsw i32 %130, %conv74
  store i32 %sub75, i32* %cur1, align 4, !tbaa !27
  %131 = load i8*, i8** %colormap2, align 4, !tbaa !2
  %132 = load i32, i32* %pixcode, align 4, !tbaa !27
  %arrayidx76 = getelementptr inbounds i8, i8* %131, i32 %132
  %133 = load i8, i8* %arrayidx76, align 1, !tbaa !32
  %conv77 = zext i8 %133 to i32
  %134 = load i32, i32* %cur2, align 4, !tbaa !27
  %sub78 = sub nsw i32 %134, %conv77
  store i32 %sub78, i32* %cur2, align 4, !tbaa !27
  %135 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #3
  %136 = bitcast i32* %bnexterr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #3
  %137 = load i32, i32* %cur0, align 4, !tbaa !27
  store i32 %137, i32* %bnexterr, align 4, !tbaa !27
  %138 = load i32, i32* %bpreverr0, align 4, !tbaa !27
  %139 = load i32, i32* %cur0, align 4, !tbaa !27
  %mul79 = mul nsw i32 %139, 3
  %add80 = add nsw i32 %138, %mul79
  %conv81 = trunc i32 %add80 to i16
  %140 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i16, i16* %140, i32 0
  store i16 %conv81, i16* %arrayidx82, align 2, !tbaa !43
  %141 = load i32, i32* %belowerr0, align 4, !tbaa !27
  %142 = load i32, i32* %cur0, align 4, !tbaa !27
  %mul83 = mul nsw i32 %142, 5
  %add84 = add nsw i32 %141, %mul83
  store i32 %add84, i32* %bpreverr0, align 4, !tbaa !27
  %143 = load i32, i32* %bnexterr, align 4, !tbaa !27
  store i32 %143, i32* %belowerr0, align 4, !tbaa !27
  %144 = load i32, i32* %cur0, align 4, !tbaa !27
  %mul85 = mul nsw i32 %144, 7
  store i32 %mul85, i32* %cur0, align 4, !tbaa !27
  %145 = load i32, i32* %cur1, align 4, !tbaa !27
  store i32 %145, i32* %bnexterr, align 4, !tbaa !27
  %146 = load i32, i32* %bpreverr1, align 4, !tbaa !27
  %147 = load i32, i32* %cur1, align 4, !tbaa !27
  %mul86 = mul nsw i32 %147, 3
  %add87 = add nsw i32 %146, %mul86
  %conv88 = trunc i32 %add87 to i16
  %148 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i16, i16* %148, i32 1
  store i16 %conv88, i16* %arrayidx89, align 2, !tbaa !43
  %149 = load i32, i32* %belowerr1, align 4, !tbaa !27
  %150 = load i32, i32* %cur1, align 4, !tbaa !27
  %mul90 = mul nsw i32 %150, 5
  %add91 = add nsw i32 %149, %mul90
  store i32 %add91, i32* %bpreverr1, align 4, !tbaa !27
  %151 = load i32, i32* %bnexterr, align 4, !tbaa !27
  store i32 %151, i32* %belowerr1, align 4, !tbaa !27
  %152 = load i32, i32* %cur1, align 4, !tbaa !27
  %mul92 = mul nsw i32 %152, 7
  store i32 %mul92, i32* %cur1, align 4, !tbaa !27
  %153 = load i32, i32* %cur2, align 4, !tbaa !27
  store i32 %153, i32* %bnexterr, align 4, !tbaa !27
  %154 = load i32, i32* %bpreverr2, align 4, !tbaa !27
  %155 = load i32, i32* %cur2, align 4, !tbaa !27
  %mul93 = mul nsw i32 %155, 3
  %add94 = add nsw i32 %154, %mul93
  %conv95 = trunc i32 %add94 to i16
  %156 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i16, i16* %156, i32 2
  store i16 %conv95, i16* %arrayidx96, align 2, !tbaa !43
  %157 = load i32, i32* %belowerr2, align 4, !tbaa !27
  %158 = load i32, i32* %cur2, align 4, !tbaa !27
  %mul97 = mul nsw i32 %158, 5
  %add98 = add nsw i32 %157, %mul97
  store i32 %add98, i32* %bpreverr2, align 4, !tbaa !27
  %159 = load i32, i32* %bnexterr, align 4, !tbaa !27
  store i32 %159, i32* %belowerr2, align 4, !tbaa !27
  %160 = load i32, i32* %cur2, align 4, !tbaa !27
  %mul99 = mul nsw i32 %160, 7
  store i32 %mul99, i32* %cur2, align 4, !tbaa !27
  %161 = bitcast i32* %bnexterr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #3
  %162 = load i32, i32* %dir3, align 4, !tbaa !27
  %163 = load i8*, i8** %inptr, align 4, !tbaa !2
  %add.ptr100 = getelementptr inbounds i8, i8* %163, i32 %162
  store i8* %add.ptr100, i8** %inptr, align 4, !tbaa !2
  %164 = load i32, i32* %dir, align 4, !tbaa !27
  %165 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr101 = getelementptr inbounds i8, i8* %165, i32 %164
  store i8* %add.ptr101, i8** %outptr, align 4, !tbaa !2
  %166 = load i32, i32* %dir3, align 4, !tbaa !27
  %167 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %add.ptr102 = getelementptr inbounds i16, i16* %167, i32 %166
  store i16* %add.ptr102, i16** %errorptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %if.end66
  %168 = load i32, i32* %col, align 4, !tbaa !27
  %dec = add i32 %168, -1
  store i32 %dec, i32* %col, align 4, !tbaa !27
  br label %for.cond16

for.end:                                          ; preds = %for.cond16
  %169 = load i32, i32* %bpreverr0, align 4, !tbaa !27
  %conv103 = trunc i32 %169 to i16
  %170 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %arrayidx104 = getelementptr inbounds i16, i16* %170, i32 0
  store i16 %conv103, i16* %arrayidx104, align 2, !tbaa !43
  %171 = load i32, i32* %bpreverr1, align 4, !tbaa !27
  %conv105 = trunc i32 %171 to i16
  %172 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i16, i16* %172, i32 1
  store i16 %conv105, i16* %arrayidx106, align 2, !tbaa !43
  %173 = load i32, i32* %bpreverr2, align 4, !tbaa !27
  %conv107 = trunc i32 %173 to i16
  %174 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i16, i16* %174, i32 2
  store i16 %conv107, i16* %arrayidx108, align 2, !tbaa !43
  br label %for.inc109

for.inc109:                                       ; preds = %for.end
  %175 = load i32, i32* %row, align 4, !tbaa !27
  %inc = add nsw i32 %175, 1
  store i32 %inc, i32* %row, align 4, !tbaa !27
  br label %for.cond

for.end110:                                       ; preds = %for.cond
  %176 = bitcast i8** %colormap2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #3
  %177 = bitcast i8** %colormap1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #3
  %178 = bitcast i8** %colormap0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #3
  %179 = bitcast i32** %error_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #3
  %180 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #3
  %181 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #3
  %182 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #3
  %183 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #3
  %184 = bitcast i32* %dir3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #3
  %185 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #3
  %186 = bitcast i16** %cachep to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #3
  %187 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #3
  %188 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #3
  %189 = bitcast i16** %errorptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #3
  %190 = bitcast i32* %bpreverr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #3
  %191 = bitcast i32* %bpreverr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #3
  %192 = bitcast i32* %bpreverr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #3
  %193 = bitcast i32* %belowerr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #3
  %194 = bitcast i32* %belowerr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #3
  %195 = bitcast i32* %belowerr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #3
  %196 = bitcast i32* %cur2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #3
  %197 = bitcast i32* %cur1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #3
  %198 = bitcast i32* %cur0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #3
  %199 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #3
  %200 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #3
  ret void
}

; Function Attrs: nounwind
define internal void @pass2_no_dither(%struct.jpeg_decompress_struct* %cinfo, i8** %input_buf, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %histogram = alloca [32 x i16]**, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %cachep = alloca i16*, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %5, i32 0, i32 3
  %6 = load [32 x i16]**, [32 x i16]*** %histogram2, align 4, !tbaa !26
  store [32 x i16]** %6, [32 x i16]*** %histogram, align 4, !tbaa !2
  %7 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i16** %cachep to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 27
  %17 = load i32, i32* %output_width, align 8, !tbaa !37
  store i32 %17, i32* %width, align 4, !tbaa !27
  store i32 0, i32* %row, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc22, %entry
  %18 = load i32, i32* %row, align 4, !tbaa !27
  %19 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp slt i32 %18, %19
  br i1 %cmp, label %for.body, label %for.end23

for.body:                                         ; preds = %for.cond
  %20 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %21 = load i32, i32* %row, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %22, i8** %inptr, align 4, !tbaa !2
  %23 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %24 = load i32, i32* %row, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %23, i32 %24
  %25 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %25, i8** %outptr, align 4, !tbaa !2
  %26 = load i32, i32* %width, align 4, !tbaa !27
  store i32 %26, i32* %col, align 4, !tbaa !27
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %cmp5 = icmp ugt i32 %27, 0
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %28, i32 1
  store i8* %incdec.ptr, i8** %inptr, align 4, !tbaa !2
  %29 = load i8, i8* %28, align 1, !tbaa !32
  %conv = zext i8 %29 to i32
  %shr = ashr i32 %conv, 3
  store i32 %shr, i32* %c0, align 4, !tbaa !27
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr7 = getelementptr inbounds i8, i8* %30, i32 1
  store i8* %incdec.ptr7, i8** %inptr, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !32
  %conv8 = zext i8 %31 to i32
  %shr9 = ashr i32 %conv8, 2
  store i32 %shr9, i32* %c1, align 4, !tbaa !27
  %32 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr10 = getelementptr inbounds i8, i8* %32, i32 1
  store i8* %incdec.ptr10, i8** %inptr, align 4, !tbaa !2
  %33 = load i8, i8* %32, align 1, !tbaa !32
  %conv11 = zext i8 %33 to i32
  %shr12 = ashr i32 %conv11, 3
  store i32 %shr12, i32* %c2, align 4, !tbaa !27
  %34 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %35 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds [32 x i16]*, [32 x i16]** %34, i32 %35
  %36 = load [32 x i16]*, [32 x i16]** %arrayidx13, align 4, !tbaa !2
  %37 = load i32, i32* %c1, align 4, !tbaa !27
  %arrayidx14 = getelementptr inbounds [32 x i16], [32 x i16]* %36, i32 %37
  %38 = load i32, i32* %c2, align 4, !tbaa !27
  %arrayidx15 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx14, i32 0, i32 %38
  store i16* %arrayidx15, i16** %cachep, align 4, !tbaa !2
  %39 = load i16*, i16** %cachep, align 4, !tbaa !2
  %40 = load i16, i16* %39, align 2, !tbaa !43
  %conv16 = zext i16 %40 to i32
  %cmp17 = icmp eq i32 %conv16, 0
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %for.body6
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %42 = load i32, i32* %c0, align 4, !tbaa !27
  %43 = load i32, i32* %c1, align 4, !tbaa !27
  %44 = load i32, i32* %c2, align 4, !tbaa !27
  call void @fill_inverse_cmap(%struct.jpeg_decompress_struct* %41, i32 %42, i32 %43, i32 %44)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body6
  %45 = load i16*, i16** %cachep, align 4, !tbaa !2
  %46 = load i16, i16* %45, align 2, !tbaa !43
  %conv19 = zext i16 %46 to i32
  %sub = sub nsw i32 %conv19, 1
  %conv20 = trunc i32 %sub to i8
  %47 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr21 = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr21, i8** %outptr, align 4, !tbaa !2
  store i8 %conv20, i8* %47, align 1, !tbaa !32
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %48 = load i32, i32* %col, align 4, !tbaa !27
  %dec = add i32 %48, -1
  store i32 %dec, i32* %col, align 4, !tbaa !27
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %49 = load i32, i32* %row, align 4, !tbaa !27
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %row, align 4, !tbaa !27
  br label %for.cond

for.end23:                                        ; preds = %for.cond
  %50 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i16** %cachep to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  %60 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  ret void
}

; Function Attrs: nounwind
define internal void @finish_pass2(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

declare void @jzero_far(i8*, i32) #2

; Function Attrs: nounwind
define internal void @select_colors(%struct.jpeg_decompress_struct* %cinfo, i32 %desired_colors) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %desired_colors.addr = alloca i32, align 4
  %boxlist = alloca %struct.box*, align 4
  %numboxes = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %desired_colors, i32* %desired_colors.addr, align 4, !tbaa !27
  %0 = bitcast %struct.box** %boxlist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %numboxes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 1
  %4 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %4, i32 0, i32 0
  %5 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_decompress_struct* %6 to %struct.jpeg_common_struct*
  %8 = load i32, i32* %desired_colors.addr, align 4, !tbaa !27
  %mul = mul i32 %8, 32
  %call = call i8* %5(%struct.jpeg_common_struct* %7, i32 1, i32 %mul)
  %9 = bitcast i8* %call to %struct.box*
  store %struct.box* %9, %struct.box** %boxlist, align 4, !tbaa !2
  store i32 1, i32* %numboxes, align 4, !tbaa !27
  %10 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %struct.box, %struct.box* %10, i32 0
  %c0min = getelementptr inbounds %struct.box, %struct.box* %arrayidx, i32 0, i32 0
  store i32 0, i32* %c0min, align 4, !tbaa !46
  %11 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds %struct.box, %struct.box* %11, i32 0
  %c0max = getelementptr inbounds %struct.box, %struct.box* %arrayidx1, i32 0, i32 1
  store i32 31, i32* %c0max, align 4, !tbaa !48
  %12 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds %struct.box, %struct.box* %12, i32 0
  %c1min = getelementptr inbounds %struct.box, %struct.box* %arrayidx2, i32 0, i32 2
  store i32 0, i32* %c1min, align 4, !tbaa !49
  %13 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds %struct.box, %struct.box* %13, i32 0
  %c1max = getelementptr inbounds %struct.box, %struct.box* %arrayidx3, i32 0, i32 3
  store i32 63, i32* %c1max, align 4, !tbaa !50
  %14 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds %struct.box, %struct.box* %14, i32 0
  %c2min = getelementptr inbounds %struct.box, %struct.box* %arrayidx4, i32 0, i32 4
  store i32 0, i32* %c2min, align 4, !tbaa !51
  %15 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds %struct.box, %struct.box* %15, i32 0
  %c2max = getelementptr inbounds %struct.box, %struct.box* %arrayidx5, i32 0, i32 5
  store i32 31, i32* %c2max, align 4, !tbaa !52
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds %struct.box, %struct.box* %17, i32 0
  call void @update_box(%struct.jpeg_decompress_struct* %16, %struct.box* %arrayidx6)
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %20 = load i32, i32* %numboxes, align 4, !tbaa !27
  %21 = load i32, i32* %desired_colors.addr, align 4, !tbaa !27
  %call7 = call i32 @median_cut(%struct.jpeg_decompress_struct* %18, %struct.box* %19, i32 %20, i32 %21)
  store i32 %call7, i32* %numboxes, align 4, !tbaa !27
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %22 = load i32, i32* %i, align 4, !tbaa !27
  %23 = load i32, i32* %numboxes, align 4, !tbaa !27
  %cmp = icmp slt i32 %22, %23
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = load %struct.box*, %struct.box** %boxlist, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds %struct.box, %struct.box* %25, i32 %26
  %27 = load i32, i32* %i, align 4, !tbaa !27
  call void @compute_color(%struct.jpeg_decompress_struct* %24, %struct.box* %arrayidx8, i32 %27)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %29 = load i32, i32* %numboxes, align 4, !tbaa !27
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %actual_number_of_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 32
  store i32 %29, i32* %actual_number_of_colors, align 4, !tbaa !40
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 0
  %32 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !22
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %32, i32 0, i32 5
  store i32 96, i32* %msg_code, align 4, !tbaa !23
  %33 = load i32, i32* %numboxes, align 4, !tbaa !27
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !22
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 6
  %i10 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx11 = getelementptr inbounds [8 x i32], [8 x i32]* %i10, i32 0, i32 0
  store i32 %33, i32* %arrayidx11, align 4, !tbaa !32
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !22
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 1
  %38 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !53
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = bitcast %struct.jpeg_decompress_struct* %39 to %struct.jpeg_common_struct*
  call void %38(%struct.jpeg_common_struct* %40, i32 1)
  %41 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  %42 = bitcast i32* %numboxes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  %43 = bitcast %struct.box** %boxlist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  ret void
}

; Function Attrs: nounwind
define internal void @update_box(%struct.jpeg_decompress_struct* %cinfo, %struct.box* %boxp) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %boxp.addr = alloca %struct.box*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %histogram = alloca [32 x i16]**, align 4
  %histp = alloca i16*, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %c0min = alloca i32, align 4
  %c0max = alloca i32, align 4
  %c1min = alloca i32, align 4
  %c1max = alloca i32, align 4
  %c2min = alloca i32, align 4
  %c2max = alloca i32, align 4
  %dist0 = alloca i32, align 4
  %dist1 = alloca i32, align 4
  %dist2 = alloca i32, align 4
  %ccount = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.box* %boxp, %struct.box** %boxp.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %5, i32 0, i32 3
  %6 = load [32 x i16]**, [32 x i16]*** %histogram2, align 4, !tbaa !26
  store [32 x i16]** %6, [32 x i16]*** %histogram, align 4, !tbaa !2
  %7 = bitcast i16** %histp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %c0min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %c0max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %c1min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %c1max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %c2min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %c2max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %dist0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %dist1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %dist2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %ccount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c0min3 = getelementptr inbounds %struct.box, %struct.box* %21, i32 0, i32 0
  %22 = load i32, i32* %c0min3, align 4, !tbaa !46
  store i32 %22, i32* %c0min, align 4, !tbaa !27
  %23 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c0max4 = getelementptr inbounds %struct.box, %struct.box* %23, i32 0, i32 1
  %24 = load i32, i32* %c0max4, align 4, !tbaa !48
  store i32 %24, i32* %c0max, align 4, !tbaa !27
  %25 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c1min5 = getelementptr inbounds %struct.box, %struct.box* %25, i32 0, i32 2
  %26 = load i32, i32* %c1min5, align 4, !tbaa !49
  store i32 %26, i32* %c1min, align 4, !tbaa !27
  %27 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c1max6 = getelementptr inbounds %struct.box, %struct.box* %27, i32 0, i32 3
  %28 = load i32, i32* %c1max6, align 4, !tbaa !50
  store i32 %28, i32* %c1max, align 4, !tbaa !27
  %29 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c2min7 = getelementptr inbounds %struct.box, %struct.box* %29, i32 0, i32 4
  %30 = load i32, i32* %c2min7, align 4, !tbaa !51
  store i32 %30, i32* %c2min, align 4, !tbaa !27
  %31 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c2max8 = getelementptr inbounds %struct.box, %struct.box* %31, i32 0, i32 5
  %32 = load i32, i32* %c2max8, align 4, !tbaa !52
  store i32 %32, i32* %c2max, align 4, !tbaa !27
  %33 = load i32, i32* %c0max, align 4, !tbaa !27
  %34 = load i32, i32* %c0min, align 4, !tbaa !27
  %cmp = icmp sgt i32 %33, %34
  br i1 %cmp, label %if.then, label %if.end28

if.then:                                          ; preds = %entry
  %35 = load i32, i32* %c0min, align 4, !tbaa !27
  store i32 %35, i32* %c0, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %if.then
  %36 = load i32, i32* %c0, align 4, !tbaa !27
  %37 = load i32, i32* %c0max, align 4, !tbaa !27
  %cmp9 = icmp sle i32 %36, %37
  br i1 %cmp9, label %for.body, label %for.end27

for.body:                                         ; preds = %for.cond
  %38 = load i32, i32* %c1min, align 4, !tbaa !27
  store i32 %38, i32* %c1, align 4, !tbaa !27
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc22, %for.body
  %39 = load i32, i32* %c1, align 4, !tbaa !27
  %40 = load i32, i32* %c1max, align 4, !tbaa !27
  %cmp11 = icmp sle i32 %39, %40
  br i1 %cmp11, label %for.body12, label %for.end24

for.body12:                                       ; preds = %for.cond10
  %41 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %42 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [32 x i16]*, [32 x i16]** %41, i32 %42
  %43 = load [32 x i16]*, [32 x i16]** %arrayidx, align 4, !tbaa !2
  %44 = load i32, i32* %c1, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds [32 x i16], [32 x i16]* %43, i32 %44
  %45 = load i32, i32* %c2min, align 4, !tbaa !27
  %arrayidx14 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx13, i32 0, i32 %45
  store i16* %arrayidx14, i16** %histp, align 4, !tbaa !2
  %46 = load i32, i32* %c2min, align 4, !tbaa !27
  store i32 %46, i32* %c2, align 4, !tbaa !27
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %for.body12
  %47 = load i32, i32* %c2, align 4, !tbaa !27
  %48 = load i32, i32* %c2max, align 4, !tbaa !27
  %cmp16 = icmp sle i32 %47, %48
  br i1 %cmp16, label %for.body17, label %for.end

for.body17:                                       ; preds = %for.cond15
  %49 = load i16*, i16** %histp, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %49, i32 1
  store i16* %incdec.ptr, i16** %histp, align 4, !tbaa !2
  %50 = load i16, i16* %49, align 2, !tbaa !43
  %conv = zext i16 %50 to i32
  %cmp18 = icmp ne i32 %conv, 0
  br i1 %cmp18, label %if.then20, label %if.end

if.then20:                                        ; preds = %for.body17
  %51 = load i32, i32* %c0, align 4, !tbaa !27
  store i32 %51, i32* %c0min, align 4, !tbaa !27
  %52 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c0min21 = getelementptr inbounds %struct.box, %struct.box* %52, i32 0, i32 0
  store i32 %51, i32* %c0min21, align 4, !tbaa !46
  br label %have_c0min

if.end:                                           ; preds = %for.body17
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %53 = load i32, i32* %c2, align 4, !tbaa !27
  %inc = add nsw i32 %53, 1
  store i32 %inc, i32* %c2, align 4, !tbaa !27
  br label %for.cond15

for.end:                                          ; preds = %for.cond15
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %54 = load i32, i32* %c1, align 4, !tbaa !27
  %inc23 = add nsw i32 %54, 1
  store i32 %inc23, i32* %c1, align 4, !tbaa !27
  br label %for.cond10

for.end24:                                        ; preds = %for.cond10
  br label %for.inc25

for.inc25:                                        ; preds = %for.end24
  %55 = load i32, i32* %c0, align 4, !tbaa !27
  %inc26 = add nsw i32 %55, 1
  store i32 %inc26, i32* %c0, align 4, !tbaa !27
  br label %for.cond

for.end27:                                        ; preds = %for.cond
  br label %if.end28

if.end28:                                         ; preds = %for.end27, %entry
  br label %have_c0min

have_c0min:                                       ; preds = %if.end28, %if.then20
  %56 = load i32, i32* %c0max, align 4, !tbaa !27
  %57 = load i32, i32* %c0min, align 4, !tbaa !27
  %cmp29 = icmp sgt i32 %56, %57
  br i1 %cmp29, label %if.then31, label %if.end62

if.then31:                                        ; preds = %have_c0min
  %58 = load i32, i32* %c0max, align 4, !tbaa !27
  store i32 %58, i32* %c0, align 4, !tbaa !27
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc60, %if.then31
  %59 = load i32, i32* %c0, align 4, !tbaa !27
  %60 = load i32, i32* %c0min, align 4, !tbaa !27
  %cmp33 = icmp sge i32 %59, %60
  br i1 %cmp33, label %for.body35, label %for.end61

for.body35:                                       ; preds = %for.cond32
  %61 = load i32, i32* %c1min, align 4, !tbaa !27
  store i32 %61, i32* %c1, align 4, !tbaa !27
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc57, %for.body35
  %62 = load i32, i32* %c1, align 4, !tbaa !27
  %63 = load i32, i32* %c1max, align 4, !tbaa !27
  %cmp37 = icmp sle i32 %62, %63
  br i1 %cmp37, label %for.body39, label %for.end59

for.body39:                                       ; preds = %for.cond36
  %64 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %65 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx40 = getelementptr inbounds [32 x i16]*, [32 x i16]** %64, i32 %65
  %66 = load [32 x i16]*, [32 x i16]** %arrayidx40, align 4, !tbaa !2
  %67 = load i32, i32* %c1, align 4, !tbaa !27
  %arrayidx41 = getelementptr inbounds [32 x i16], [32 x i16]* %66, i32 %67
  %68 = load i32, i32* %c2min, align 4, !tbaa !27
  %arrayidx42 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx41, i32 0, i32 %68
  store i16* %arrayidx42, i16** %histp, align 4, !tbaa !2
  %69 = load i32, i32* %c2min, align 4, !tbaa !27
  store i32 %69, i32* %c2, align 4, !tbaa !27
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc54, %for.body39
  %70 = load i32, i32* %c2, align 4, !tbaa !27
  %71 = load i32, i32* %c2max, align 4, !tbaa !27
  %cmp44 = icmp sle i32 %70, %71
  br i1 %cmp44, label %for.body46, label %for.end56

for.body46:                                       ; preds = %for.cond43
  %72 = load i16*, i16** %histp, align 4, !tbaa !2
  %incdec.ptr47 = getelementptr inbounds i16, i16* %72, i32 1
  store i16* %incdec.ptr47, i16** %histp, align 4, !tbaa !2
  %73 = load i16, i16* %72, align 2, !tbaa !43
  %conv48 = zext i16 %73 to i32
  %cmp49 = icmp ne i32 %conv48, 0
  br i1 %cmp49, label %if.then51, label %if.end53

if.then51:                                        ; preds = %for.body46
  %74 = load i32, i32* %c0, align 4, !tbaa !27
  store i32 %74, i32* %c0max, align 4, !tbaa !27
  %75 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c0max52 = getelementptr inbounds %struct.box, %struct.box* %75, i32 0, i32 1
  store i32 %74, i32* %c0max52, align 4, !tbaa !48
  br label %have_c0max

if.end53:                                         ; preds = %for.body46
  br label %for.inc54

for.inc54:                                        ; preds = %if.end53
  %76 = load i32, i32* %c2, align 4, !tbaa !27
  %inc55 = add nsw i32 %76, 1
  store i32 %inc55, i32* %c2, align 4, !tbaa !27
  br label %for.cond43

for.end56:                                        ; preds = %for.cond43
  br label %for.inc57

for.inc57:                                        ; preds = %for.end56
  %77 = load i32, i32* %c1, align 4, !tbaa !27
  %inc58 = add nsw i32 %77, 1
  store i32 %inc58, i32* %c1, align 4, !tbaa !27
  br label %for.cond36

for.end59:                                        ; preds = %for.cond36
  br label %for.inc60

for.inc60:                                        ; preds = %for.end59
  %78 = load i32, i32* %c0, align 4, !tbaa !27
  %dec = add nsw i32 %78, -1
  store i32 %dec, i32* %c0, align 4, !tbaa !27
  br label %for.cond32

for.end61:                                        ; preds = %for.cond32
  br label %if.end62

if.end62:                                         ; preds = %for.end61, %have_c0min
  br label %have_c0max

have_c0max:                                       ; preds = %if.end62, %if.then51
  %79 = load i32, i32* %c1max, align 4, !tbaa !27
  %80 = load i32, i32* %c1min, align 4, !tbaa !27
  %cmp63 = icmp sgt i32 %79, %80
  br i1 %cmp63, label %if.then65, label %if.end97

if.then65:                                        ; preds = %have_c0max
  %81 = load i32, i32* %c1min, align 4, !tbaa !27
  store i32 %81, i32* %c1, align 4, !tbaa !27
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc94, %if.then65
  %82 = load i32, i32* %c1, align 4, !tbaa !27
  %83 = load i32, i32* %c1max, align 4, !tbaa !27
  %cmp67 = icmp sle i32 %82, %83
  br i1 %cmp67, label %for.body69, label %for.end96

for.body69:                                       ; preds = %for.cond66
  %84 = load i32, i32* %c0min, align 4, !tbaa !27
  store i32 %84, i32* %c0, align 4, !tbaa !27
  br label %for.cond70

for.cond70:                                       ; preds = %for.inc91, %for.body69
  %85 = load i32, i32* %c0, align 4, !tbaa !27
  %86 = load i32, i32* %c0max, align 4, !tbaa !27
  %cmp71 = icmp sle i32 %85, %86
  br i1 %cmp71, label %for.body73, label %for.end93

for.body73:                                       ; preds = %for.cond70
  %87 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %88 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx74 = getelementptr inbounds [32 x i16]*, [32 x i16]** %87, i32 %88
  %89 = load [32 x i16]*, [32 x i16]** %arrayidx74, align 4, !tbaa !2
  %90 = load i32, i32* %c1, align 4, !tbaa !27
  %arrayidx75 = getelementptr inbounds [32 x i16], [32 x i16]* %89, i32 %90
  %91 = load i32, i32* %c2min, align 4, !tbaa !27
  %arrayidx76 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx75, i32 0, i32 %91
  store i16* %arrayidx76, i16** %histp, align 4, !tbaa !2
  %92 = load i32, i32* %c2min, align 4, !tbaa !27
  store i32 %92, i32* %c2, align 4, !tbaa !27
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc88, %for.body73
  %93 = load i32, i32* %c2, align 4, !tbaa !27
  %94 = load i32, i32* %c2max, align 4, !tbaa !27
  %cmp78 = icmp sle i32 %93, %94
  br i1 %cmp78, label %for.body80, label %for.end90

for.body80:                                       ; preds = %for.cond77
  %95 = load i16*, i16** %histp, align 4, !tbaa !2
  %incdec.ptr81 = getelementptr inbounds i16, i16* %95, i32 1
  store i16* %incdec.ptr81, i16** %histp, align 4, !tbaa !2
  %96 = load i16, i16* %95, align 2, !tbaa !43
  %conv82 = zext i16 %96 to i32
  %cmp83 = icmp ne i32 %conv82, 0
  br i1 %cmp83, label %if.then85, label %if.end87

if.then85:                                        ; preds = %for.body80
  %97 = load i32, i32* %c1, align 4, !tbaa !27
  store i32 %97, i32* %c1min, align 4, !tbaa !27
  %98 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c1min86 = getelementptr inbounds %struct.box, %struct.box* %98, i32 0, i32 2
  store i32 %97, i32* %c1min86, align 4, !tbaa !49
  br label %have_c1min

if.end87:                                         ; preds = %for.body80
  br label %for.inc88

for.inc88:                                        ; preds = %if.end87
  %99 = load i32, i32* %c2, align 4, !tbaa !27
  %inc89 = add nsw i32 %99, 1
  store i32 %inc89, i32* %c2, align 4, !tbaa !27
  br label %for.cond77

for.end90:                                        ; preds = %for.cond77
  br label %for.inc91

for.inc91:                                        ; preds = %for.end90
  %100 = load i32, i32* %c0, align 4, !tbaa !27
  %inc92 = add nsw i32 %100, 1
  store i32 %inc92, i32* %c0, align 4, !tbaa !27
  br label %for.cond70

for.end93:                                        ; preds = %for.cond70
  br label %for.inc94

for.inc94:                                        ; preds = %for.end93
  %101 = load i32, i32* %c1, align 4, !tbaa !27
  %inc95 = add nsw i32 %101, 1
  store i32 %inc95, i32* %c1, align 4, !tbaa !27
  br label %for.cond66

for.end96:                                        ; preds = %for.cond66
  br label %if.end97

if.end97:                                         ; preds = %for.end96, %have_c0max
  br label %have_c1min

have_c1min:                                       ; preds = %if.end97, %if.then85
  %102 = load i32, i32* %c1max, align 4, !tbaa !27
  %103 = load i32, i32* %c1min, align 4, !tbaa !27
  %cmp98 = icmp sgt i32 %102, %103
  br i1 %cmp98, label %if.then100, label %if.end132

if.then100:                                       ; preds = %have_c1min
  %104 = load i32, i32* %c1max, align 4, !tbaa !27
  store i32 %104, i32* %c1, align 4, !tbaa !27
  br label %for.cond101

for.cond101:                                      ; preds = %for.inc129, %if.then100
  %105 = load i32, i32* %c1, align 4, !tbaa !27
  %106 = load i32, i32* %c1min, align 4, !tbaa !27
  %cmp102 = icmp sge i32 %105, %106
  br i1 %cmp102, label %for.body104, label %for.end131

for.body104:                                      ; preds = %for.cond101
  %107 = load i32, i32* %c0min, align 4, !tbaa !27
  store i32 %107, i32* %c0, align 4, !tbaa !27
  br label %for.cond105

for.cond105:                                      ; preds = %for.inc126, %for.body104
  %108 = load i32, i32* %c0, align 4, !tbaa !27
  %109 = load i32, i32* %c0max, align 4, !tbaa !27
  %cmp106 = icmp sle i32 %108, %109
  br i1 %cmp106, label %for.body108, label %for.end128

for.body108:                                      ; preds = %for.cond105
  %110 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %111 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx109 = getelementptr inbounds [32 x i16]*, [32 x i16]** %110, i32 %111
  %112 = load [32 x i16]*, [32 x i16]** %arrayidx109, align 4, !tbaa !2
  %113 = load i32, i32* %c1, align 4, !tbaa !27
  %arrayidx110 = getelementptr inbounds [32 x i16], [32 x i16]* %112, i32 %113
  %114 = load i32, i32* %c2min, align 4, !tbaa !27
  %arrayidx111 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx110, i32 0, i32 %114
  store i16* %arrayidx111, i16** %histp, align 4, !tbaa !2
  %115 = load i32, i32* %c2min, align 4, !tbaa !27
  store i32 %115, i32* %c2, align 4, !tbaa !27
  br label %for.cond112

for.cond112:                                      ; preds = %for.inc123, %for.body108
  %116 = load i32, i32* %c2, align 4, !tbaa !27
  %117 = load i32, i32* %c2max, align 4, !tbaa !27
  %cmp113 = icmp sle i32 %116, %117
  br i1 %cmp113, label %for.body115, label %for.end125

for.body115:                                      ; preds = %for.cond112
  %118 = load i16*, i16** %histp, align 4, !tbaa !2
  %incdec.ptr116 = getelementptr inbounds i16, i16* %118, i32 1
  store i16* %incdec.ptr116, i16** %histp, align 4, !tbaa !2
  %119 = load i16, i16* %118, align 2, !tbaa !43
  %conv117 = zext i16 %119 to i32
  %cmp118 = icmp ne i32 %conv117, 0
  br i1 %cmp118, label %if.then120, label %if.end122

if.then120:                                       ; preds = %for.body115
  %120 = load i32, i32* %c1, align 4, !tbaa !27
  store i32 %120, i32* %c1max, align 4, !tbaa !27
  %121 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c1max121 = getelementptr inbounds %struct.box, %struct.box* %121, i32 0, i32 3
  store i32 %120, i32* %c1max121, align 4, !tbaa !50
  br label %have_c1max

if.end122:                                        ; preds = %for.body115
  br label %for.inc123

for.inc123:                                       ; preds = %if.end122
  %122 = load i32, i32* %c2, align 4, !tbaa !27
  %inc124 = add nsw i32 %122, 1
  store i32 %inc124, i32* %c2, align 4, !tbaa !27
  br label %for.cond112

for.end125:                                       ; preds = %for.cond112
  br label %for.inc126

for.inc126:                                       ; preds = %for.end125
  %123 = load i32, i32* %c0, align 4, !tbaa !27
  %inc127 = add nsw i32 %123, 1
  store i32 %inc127, i32* %c0, align 4, !tbaa !27
  br label %for.cond105

for.end128:                                       ; preds = %for.cond105
  br label %for.inc129

for.inc129:                                       ; preds = %for.end128
  %124 = load i32, i32* %c1, align 4, !tbaa !27
  %dec130 = add nsw i32 %124, -1
  store i32 %dec130, i32* %c1, align 4, !tbaa !27
  br label %for.cond101

for.end131:                                       ; preds = %for.cond101
  br label %if.end132

if.end132:                                        ; preds = %for.end131, %have_c1min
  br label %have_c1max

have_c1max:                                       ; preds = %if.end132, %if.then120
  %125 = load i32, i32* %c2max, align 4, !tbaa !27
  %126 = load i32, i32* %c2min, align 4, !tbaa !27
  %cmp133 = icmp sgt i32 %125, %126
  br i1 %cmp133, label %if.then135, label %if.end166

if.then135:                                       ; preds = %have_c1max
  %127 = load i32, i32* %c2min, align 4, !tbaa !27
  store i32 %127, i32* %c2, align 4, !tbaa !27
  br label %for.cond136

for.cond136:                                      ; preds = %for.inc163, %if.then135
  %128 = load i32, i32* %c2, align 4, !tbaa !27
  %129 = load i32, i32* %c2max, align 4, !tbaa !27
  %cmp137 = icmp sle i32 %128, %129
  br i1 %cmp137, label %for.body139, label %for.end165

for.body139:                                      ; preds = %for.cond136
  %130 = load i32, i32* %c0min, align 4, !tbaa !27
  store i32 %130, i32* %c0, align 4, !tbaa !27
  br label %for.cond140

for.cond140:                                      ; preds = %for.inc160, %for.body139
  %131 = load i32, i32* %c0, align 4, !tbaa !27
  %132 = load i32, i32* %c0max, align 4, !tbaa !27
  %cmp141 = icmp sle i32 %131, %132
  br i1 %cmp141, label %for.body143, label %for.end162

for.body143:                                      ; preds = %for.cond140
  %133 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %134 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx144 = getelementptr inbounds [32 x i16]*, [32 x i16]** %133, i32 %134
  %135 = load [32 x i16]*, [32 x i16]** %arrayidx144, align 4, !tbaa !2
  %136 = load i32, i32* %c1min, align 4, !tbaa !27
  %arrayidx145 = getelementptr inbounds [32 x i16], [32 x i16]* %135, i32 %136
  %137 = load i32, i32* %c2, align 4, !tbaa !27
  %arrayidx146 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx145, i32 0, i32 %137
  store i16* %arrayidx146, i16** %histp, align 4, !tbaa !2
  %138 = load i32, i32* %c1min, align 4, !tbaa !27
  store i32 %138, i32* %c1, align 4, !tbaa !27
  br label %for.cond147

for.cond147:                                      ; preds = %for.inc157, %for.body143
  %139 = load i32, i32* %c1, align 4, !tbaa !27
  %140 = load i32, i32* %c1max, align 4, !tbaa !27
  %cmp148 = icmp sle i32 %139, %140
  br i1 %cmp148, label %for.body150, label %for.end159

for.body150:                                      ; preds = %for.cond147
  %141 = load i16*, i16** %histp, align 4, !tbaa !2
  %142 = load i16, i16* %141, align 2, !tbaa !43
  %conv151 = zext i16 %142 to i32
  %cmp152 = icmp ne i32 %conv151, 0
  br i1 %cmp152, label %if.then154, label %if.end156

if.then154:                                       ; preds = %for.body150
  %143 = load i32, i32* %c2, align 4, !tbaa !27
  store i32 %143, i32* %c2min, align 4, !tbaa !27
  %144 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c2min155 = getelementptr inbounds %struct.box, %struct.box* %144, i32 0, i32 4
  store i32 %143, i32* %c2min155, align 4, !tbaa !51
  br label %have_c2min

if.end156:                                        ; preds = %for.body150
  br label %for.inc157

for.inc157:                                       ; preds = %if.end156
  %145 = load i32, i32* %c1, align 4, !tbaa !27
  %inc158 = add nsw i32 %145, 1
  store i32 %inc158, i32* %c1, align 4, !tbaa !27
  %146 = load i16*, i16** %histp, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i16, i16* %146, i32 32
  store i16* %add.ptr, i16** %histp, align 4, !tbaa !2
  br label %for.cond147

for.end159:                                       ; preds = %for.cond147
  br label %for.inc160

for.inc160:                                       ; preds = %for.end159
  %147 = load i32, i32* %c0, align 4, !tbaa !27
  %inc161 = add nsw i32 %147, 1
  store i32 %inc161, i32* %c0, align 4, !tbaa !27
  br label %for.cond140

for.end162:                                       ; preds = %for.cond140
  br label %for.inc163

for.inc163:                                       ; preds = %for.end162
  %148 = load i32, i32* %c2, align 4, !tbaa !27
  %inc164 = add nsw i32 %148, 1
  store i32 %inc164, i32* %c2, align 4, !tbaa !27
  br label %for.cond136

for.end165:                                       ; preds = %for.cond136
  br label %if.end166

if.end166:                                        ; preds = %for.end165, %have_c1max
  br label %have_c2min

have_c2min:                                       ; preds = %if.end166, %if.then154
  %149 = load i32, i32* %c2max, align 4, !tbaa !27
  %150 = load i32, i32* %c2min, align 4, !tbaa !27
  %cmp167 = icmp sgt i32 %149, %150
  br i1 %cmp167, label %if.then169, label %if.end201

if.then169:                                       ; preds = %have_c2min
  %151 = load i32, i32* %c2max, align 4, !tbaa !27
  store i32 %151, i32* %c2, align 4, !tbaa !27
  br label %for.cond170

for.cond170:                                      ; preds = %for.inc198, %if.then169
  %152 = load i32, i32* %c2, align 4, !tbaa !27
  %153 = load i32, i32* %c2min, align 4, !tbaa !27
  %cmp171 = icmp sge i32 %152, %153
  br i1 %cmp171, label %for.body173, label %for.end200

for.body173:                                      ; preds = %for.cond170
  %154 = load i32, i32* %c0min, align 4, !tbaa !27
  store i32 %154, i32* %c0, align 4, !tbaa !27
  br label %for.cond174

for.cond174:                                      ; preds = %for.inc195, %for.body173
  %155 = load i32, i32* %c0, align 4, !tbaa !27
  %156 = load i32, i32* %c0max, align 4, !tbaa !27
  %cmp175 = icmp sle i32 %155, %156
  br i1 %cmp175, label %for.body177, label %for.end197

for.body177:                                      ; preds = %for.cond174
  %157 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %158 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx178 = getelementptr inbounds [32 x i16]*, [32 x i16]** %157, i32 %158
  %159 = load [32 x i16]*, [32 x i16]** %arrayidx178, align 4, !tbaa !2
  %160 = load i32, i32* %c1min, align 4, !tbaa !27
  %arrayidx179 = getelementptr inbounds [32 x i16], [32 x i16]* %159, i32 %160
  %161 = load i32, i32* %c2, align 4, !tbaa !27
  %arrayidx180 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx179, i32 0, i32 %161
  store i16* %arrayidx180, i16** %histp, align 4, !tbaa !2
  %162 = load i32, i32* %c1min, align 4, !tbaa !27
  store i32 %162, i32* %c1, align 4, !tbaa !27
  br label %for.cond181

for.cond181:                                      ; preds = %for.inc191, %for.body177
  %163 = load i32, i32* %c1, align 4, !tbaa !27
  %164 = load i32, i32* %c1max, align 4, !tbaa !27
  %cmp182 = icmp sle i32 %163, %164
  br i1 %cmp182, label %for.body184, label %for.end194

for.body184:                                      ; preds = %for.cond181
  %165 = load i16*, i16** %histp, align 4, !tbaa !2
  %166 = load i16, i16* %165, align 2, !tbaa !43
  %conv185 = zext i16 %166 to i32
  %cmp186 = icmp ne i32 %conv185, 0
  br i1 %cmp186, label %if.then188, label %if.end190

if.then188:                                       ; preds = %for.body184
  %167 = load i32, i32* %c2, align 4, !tbaa !27
  store i32 %167, i32* %c2max, align 4, !tbaa !27
  %168 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c2max189 = getelementptr inbounds %struct.box, %struct.box* %168, i32 0, i32 5
  store i32 %167, i32* %c2max189, align 4, !tbaa !52
  br label %have_c2max

if.end190:                                        ; preds = %for.body184
  br label %for.inc191

for.inc191:                                       ; preds = %if.end190
  %169 = load i32, i32* %c1, align 4, !tbaa !27
  %inc192 = add nsw i32 %169, 1
  store i32 %inc192, i32* %c1, align 4, !tbaa !27
  %170 = load i16*, i16** %histp, align 4, !tbaa !2
  %add.ptr193 = getelementptr inbounds i16, i16* %170, i32 32
  store i16* %add.ptr193, i16** %histp, align 4, !tbaa !2
  br label %for.cond181

for.end194:                                       ; preds = %for.cond181
  br label %for.inc195

for.inc195:                                       ; preds = %for.end194
  %171 = load i32, i32* %c0, align 4, !tbaa !27
  %inc196 = add nsw i32 %171, 1
  store i32 %inc196, i32* %c0, align 4, !tbaa !27
  br label %for.cond174

for.end197:                                       ; preds = %for.cond174
  br label %for.inc198

for.inc198:                                       ; preds = %for.end197
  %172 = load i32, i32* %c2, align 4, !tbaa !27
  %dec199 = add nsw i32 %172, -1
  store i32 %dec199, i32* %c2, align 4, !tbaa !27
  br label %for.cond170

for.end200:                                       ; preds = %for.cond170
  br label %if.end201

if.end201:                                        ; preds = %for.end200, %have_c2min
  br label %have_c2max

have_c2max:                                       ; preds = %if.end201, %if.then188
  %173 = load i32, i32* %c0max, align 4, !tbaa !27
  %174 = load i32, i32* %c0min, align 4, !tbaa !27
  %sub = sub nsw i32 %173, %174
  %shl = shl i32 %sub, 3
  %175 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %175, i32 0, i32 11
  %176 = load i32, i32* %out_color_space, align 4, !tbaa !54
  %arrayidx202 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %176
  %177 = load i32, i32* %arrayidx202, align 4, !tbaa !27
  %arrayidx203 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %177
  %178 = load i32, i32* %arrayidx203, align 4, !tbaa !27
  %mul = mul nsw i32 %shl, %178
  store i32 %mul, i32* %dist0, align 4, !tbaa !41
  %179 = load i32, i32* %c1max, align 4, !tbaa !27
  %180 = load i32, i32* %c1min, align 4, !tbaa !27
  %sub204 = sub nsw i32 %179, %180
  %shl205 = shl i32 %sub204, 2
  %181 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space206 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %181, i32 0, i32 11
  %182 = load i32, i32* %out_color_space206, align 4, !tbaa !54
  %arrayidx207 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %182
  %183 = load i32, i32* %arrayidx207, align 4, !tbaa !27
  %arrayidx208 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %183
  %184 = load i32, i32* %arrayidx208, align 4, !tbaa !27
  %mul209 = mul nsw i32 %shl205, %184
  store i32 %mul209, i32* %dist1, align 4, !tbaa !41
  %185 = load i32, i32* %c2max, align 4, !tbaa !27
  %186 = load i32, i32* %c2min, align 4, !tbaa !27
  %sub210 = sub nsw i32 %185, %186
  %shl211 = shl i32 %sub210, 3
  %187 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space212 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %187, i32 0, i32 11
  %188 = load i32, i32* %out_color_space212, align 4, !tbaa !54
  %arrayidx213 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %188
  %189 = load i32, i32* %arrayidx213, align 4, !tbaa !27
  %arrayidx214 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %189
  %190 = load i32, i32* %arrayidx214, align 4, !tbaa !27
  %mul215 = mul nsw i32 %shl211, %190
  store i32 %mul215, i32* %dist2, align 4, !tbaa !41
  %191 = load i32, i32* %dist0, align 4, !tbaa !41
  %192 = load i32, i32* %dist0, align 4, !tbaa !41
  %mul216 = mul nsw i32 %191, %192
  %193 = load i32, i32* %dist1, align 4, !tbaa !41
  %194 = load i32, i32* %dist1, align 4, !tbaa !41
  %mul217 = mul nsw i32 %193, %194
  %add = add nsw i32 %mul216, %mul217
  %195 = load i32, i32* %dist2, align 4, !tbaa !41
  %196 = load i32, i32* %dist2, align 4, !tbaa !41
  %mul218 = mul nsw i32 %195, %196
  %add219 = add nsw i32 %add, %mul218
  %197 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %volume = getelementptr inbounds %struct.box, %struct.box* %197, i32 0, i32 6
  store i32 %add219, i32* %volume, align 4, !tbaa !55
  store i32 0, i32* %ccount, align 4, !tbaa !41
  %198 = load i32, i32* %c0min, align 4, !tbaa !27
  store i32 %198, i32* %c0, align 4, !tbaa !27
  br label %for.cond220

for.cond220:                                      ; preds = %for.inc248, %have_c2max
  %199 = load i32, i32* %c0, align 4, !tbaa !27
  %200 = load i32, i32* %c0max, align 4, !tbaa !27
  %cmp221 = icmp sle i32 %199, %200
  br i1 %cmp221, label %for.body223, label %for.end250

for.body223:                                      ; preds = %for.cond220
  %201 = load i32, i32* %c1min, align 4, !tbaa !27
  store i32 %201, i32* %c1, align 4, !tbaa !27
  br label %for.cond224

for.cond224:                                      ; preds = %for.inc245, %for.body223
  %202 = load i32, i32* %c1, align 4, !tbaa !27
  %203 = load i32, i32* %c1max, align 4, !tbaa !27
  %cmp225 = icmp sle i32 %202, %203
  br i1 %cmp225, label %for.body227, label %for.end247

for.body227:                                      ; preds = %for.cond224
  %204 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %205 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx228 = getelementptr inbounds [32 x i16]*, [32 x i16]** %204, i32 %205
  %206 = load [32 x i16]*, [32 x i16]** %arrayidx228, align 4, !tbaa !2
  %207 = load i32, i32* %c1, align 4, !tbaa !27
  %arrayidx229 = getelementptr inbounds [32 x i16], [32 x i16]* %206, i32 %207
  %208 = load i32, i32* %c2min, align 4, !tbaa !27
  %arrayidx230 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx229, i32 0, i32 %208
  store i16* %arrayidx230, i16** %histp, align 4, !tbaa !2
  %209 = load i32, i32* %c2min, align 4, !tbaa !27
  store i32 %209, i32* %c2, align 4, !tbaa !27
  br label %for.cond231

for.cond231:                                      ; preds = %for.inc241, %for.body227
  %210 = load i32, i32* %c2, align 4, !tbaa !27
  %211 = load i32, i32* %c2max, align 4, !tbaa !27
  %cmp232 = icmp sle i32 %210, %211
  br i1 %cmp232, label %for.body234, label %for.end244

for.body234:                                      ; preds = %for.cond231
  %212 = load i16*, i16** %histp, align 4, !tbaa !2
  %213 = load i16, i16* %212, align 2, !tbaa !43
  %conv235 = zext i16 %213 to i32
  %cmp236 = icmp ne i32 %conv235, 0
  br i1 %cmp236, label %if.then238, label %if.end240

if.then238:                                       ; preds = %for.body234
  %214 = load i32, i32* %ccount, align 4, !tbaa !41
  %inc239 = add nsw i32 %214, 1
  store i32 %inc239, i32* %ccount, align 4, !tbaa !41
  br label %if.end240

if.end240:                                        ; preds = %if.then238, %for.body234
  br label %for.inc241

for.inc241:                                       ; preds = %if.end240
  %215 = load i32, i32* %c2, align 4, !tbaa !27
  %inc242 = add nsw i32 %215, 1
  store i32 %inc242, i32* %c2, align 4, !tbaa !27
  %216 = load i16*, i16** %histp, align 4, !tbaa !2
  %incdec.ptr243 = getelementptr inbounds i16, i16* %216, i32 1
  store i16* %incdec.ptr243, i16** %histp, align 4, !tbaa !2
  br label %for.cond231

for.end244:                                       ; preds = %for.cond231
  br label %for.inc245

for.inc245:                                       ; preds = %for.end244
  %217 = load i32, i32* %c1, align 4, !tbaa !27
  %inc246 = add nsw i32 %217, 1
  store i32 %inc246, i32* %c1, align 4, !tbaa !27
  br label %for.cond224

for.end247:                                       ; preds = %for.cond224
  br label %for.inc248

for.inc248:                                       ; preds = %for.end247
  %218 = load i32, i32* %c0, align 4, !tbaa !27
  %inc249 = add nsw i32 %218, 1
  store i32 %inc249, i32* %c0, align 4, !tbaa !27
  br label %for.cond220

for.end250:                                       ; preds = %for.cond220
  %219 = load i32, i32* %ccount, align 4, !tbaa !41
  %220 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %colorcount = getelementptr inbounds %struct.box, %struct.box* %220, i32 0, i32 7
  store i32 %219, i32* %colorcount, align 4, !tbaa !56
  %221 = bitcast i32* %ccount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #3
  %222 = bitcast i32* %dist2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #3
  %223 = bitcast i32* %dist1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #3
  %224 = bitcast i32* %dist0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #3
  %225 = bitcast i32* %c2max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #3
  %226 = bitcast i32* %c2min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #3
  %227 = bitcast i32* %c1max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #3
  %228 = bitcast i32* %c1min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #3
  %229 = bitcast i32* %c0max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #3
  %230 = bitcast i32* %c0min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #3
  %231 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #3
  %232 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #3
  %233 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #3
  %234 = bitcast i16** %histp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #3
  %235 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #3
  %236 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @median_cut(%struct.jpeg_decompress_struct* %cinfo, %struct.box* %boxlist, i32 %numboxes, i32 %desired_colors) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %boxlist.addr = alloca %struct.box*, align 4
  %numboxes.addr = alloca i32, align 4
  %desired_colors.addr = alloca i32, align 4
  %n = alloca i32, align 4
  %lb = alloca i32, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %cmax = alloca i32, align 4
  %b1 = alloca %struct.box*, align 4
  %b2 = alloca %struct.box*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.box* %boxlist, %struct.box** %boxlist.addr, align 4, !tbaa !2
  store i32 %numboxes, i32* %numboxes.addr, align 4, !tbaa !27
  store i32 %desired_colors, i32* %desired_colors.addr, align 4, !tbaa !27
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %lb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %cmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast %struct.box** %b1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast %struct.box** %b2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  br label %while.cond

while.cond:                                       ; preds = %sw.epilog, %entry
  %8 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %9 = load i32, i32* %desired_colors.addr, align 4, !tbaa !27
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %mul = mul nsw i32 %10, 2
  %11 = load i32, i32* %desired_colors.addr, align 4, !tbaa !27
  %cmp1 = icmp sle i32 %mul, %11
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %12 = load %struct.box*, %struct.box** %boxlist.addr, align 4, !tbaa !2
  %13 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %call = call %struct.box* @find_biggest_color_pop(%struct.box* %12, i32 %13)
  store %struct.box* %call, %struct.box** %b1, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %while.body
  %14 = load %struct.box*, %struct.box** %boxlist.addr, align 4, !tbaa !2
  %15 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %call2 = call %struct.box* @find_biggest_volume(%struct.box* %14, i32 %15)
  store %struct.box* %call2, %struct.box** %b1, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %16 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %cmp3 = icmp eq %struct.box* %16, null
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  br label %while.end

if.end5:                                          ; preds = %if.end
  %17 = load %struct.box*, %struct.box** %boxlist.addr, align 4, !tbaa !2
  %18 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds %struct.box, %struct.box* %17, i32 %18
  store %struct.box* %arrayidx, %struct.box** %b2, align 4, !tbaa !2
  %19 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c0max = getelementptr inbounds %struct.box, %struct.box* %19, i32 0, i32 1
  %20 = load i32, i32* %c0max, align 4, !tbaa !48
  %21 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c0max6 = getelementptr inbounds %struct.box, %struct.box* %21, i32 0, i32 1
  store i32 %20, i32* %c0max6, align 4, !tbaa !48
  %22 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c1max = getelementptr inbounds %struct.box, %struct.box* %22, i32 0, i32 3
  %23 = load i32, i32* %c1max, align 4, !tbaa !50
  %24 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c1max7 = getelementptr inbounds %struct.box, %struct.box* %24, i32 0, i32 3
  store i32 %23, i32* %c1max7, align 4, !tbaa !50
  %25 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c2max = getelementptr inbounds %struct.box, %struct.box* %25, i32 0, i32 5
  %26 = load i32, i32* %c2max, align 4, !tbaa !52
  %27 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c2max8 = getelementptr inbounds %struct.box, %struct.box* %27, i32 0, i32 5
  store i32 %26, i32* %c2max8, align 4, !tbaa !52
  %28 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c0min = getelementptr inbounds %struct.box, %struct.box* %28, i32 0, i32 0
  %29 = load i32, i32* %c0min, align 4, !tbaa !46
  %30 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c0min9 = getelementptr inbounds %struct.box, %struct.box* %30, i32 0, i32 0
  store i32 %29, i32* %c0min9, align 4, !tbaa !46
  %31 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c1min = getelementptr inbounds %struct.box, %struct.box* %31, i32 0, i32 2
  %32 = load i32, i32* %c1min, align 4, !tbaa !49
  %33 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c1min10 = getelementptr inbounds %struct.box, %struct.box* %33, i32 0, i32 2
  store i32 %32, i32* %c1min10, align 4, !tbaa !49
  %34 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c2min = getelementptr inbounds %struct.box, %struct.box* %34, i32 0, i32 4
  %35 = load i32, i32* %c2min, align 4, !tbaa !51
  %36 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c2min11 = getelementptr inbounds %struct.box, %struct.box* %36, i32 0, i32 4
  store i32 %35, i32* %c2min11, align 4, !tbaa !51
  %37 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c0max12 = getelementptr inbounds %struct.box, %struct.box* %37, i32 0, i32 1
  %38 = load i32, i32* %c0max12, align 4, !tbaa !48
  %39 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c0min13 = getelementptr inbounds %struct.box, %struct.box* %39, i32 0, i32 0
  %40 = load i32, i32* %c0min13, align 4, !tbaa !46
  %sub = sub nsw i32 %38, %40
  %shl = shl i32 %sub, 3
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 11
  %42 = load i32, i32* %out_color_space, align 4, !tbaa !54
  %arrayidx14 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %42
  %43 = load i32, i32* %arrayidx14, align 4, !tbaa !27
  %arrayidx15 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %43
  %44 = load i32, i32* %arrayidx15, align 4, !tbaa !27
  %mul16 = mul nsw i32 %shl, %44
  store i32 %mul16, i32* %c0, align 4, !tbaa !27
  %45 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c1max17 = getelementptr inbounds %struct.box, %struct.box* %45, i32 0, i32 3
  %46 = load i32, i32* %c1max17, align 4, !tbaa !50
  %47 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c1min18 = getelementptr inbounds %struct.box, %struct.box* %47, i32 0, i32 2
  %48 = load i32, i32* %c1min18, align 4, !tbaa !49
  %sub19 = sub nsw i32 %46, %48
  %shl20 = shl i32 %sub19, 2
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 11
  %50 = load i32, i32* %out_color_space21, align 4, !tbaa !54
  %arrayidx22 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx22, align 4, !tbaa !27
  %arrayidx23 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %51
  %52 = load i32, i32* %arrayidx23, align 4, !tbaa !27
  %mul24 = mul nsw i32 %shl20, %52
  store i32 %mul24, i32* %c1, align 4, !tbaa !27
  %53 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c2max25 = getelementptr inbounds %struct.box, %struct.box* %53, i32 0, i32 5
  %54 = load i32, i32* %c2max25, align 4, !tbaa !52
  %55 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c2min26 = getelementptr inbounds %struct.box, %struct.box* %55, i32 0, i32 4
  %56 = load i32, i32* %c2min26, align 4, !tbaa !51
  %sub27 = sub nsw i32 %54, %56
  %shl28 = shl i32 %sub27, 3
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 11
  %58 = load i32, i32* %out_color_space29, align 4, !tbaa !54
  %arrayidx30 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %58
  %59 = load i32, i32* %arrayidx30, align 4, !tbaa !27
  %arrayidx31 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %59
  %60 = load i32, i32* %arrayidx31, align 4, !tbaa !27
  %mul32 = mul nsw i32 %shl28, %60
  store i32 %mul32, i32* %c2, align 4, !tbaa !27
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space33 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 11
  %62 = load i32, i32* %out_color_space33, align 4, !tbaa !54
  %arrayidx34 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %62
  %63 = load i32, i32* %arrayidx34, align 4, !tbaa !27
  %cmp35 = icmp eq i32 %63, 0
  br i1 %cmp35, label %if.then36, label %if.else43

if.then36:                                        ; preds = %if.end5
  %64 = load i32, i32* %c1, align 4, !tbaa !27
  store i32 %64, i32* %cmax, align 4, !tbaa !27
  store i32 1, i32* %n, align 4, !tbaa !27
  %65 = load i32, i32* %c0, align 4, !tbaa !27
  %66 = load i32, i32* %cmax, align 4, !tbaa !27
  %cmp37 = icmp sgt i32 %65, %66
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.then36
  %67 = load i32, i32* %c0, align 4, !tbaa !27
  store i32 %67, i32* %cmax, align 4, !tbaa !27
  store i32 0, i32* %n, align 4, !tbaa !27
  br label %if.end39

if.end39:                                         ; preds = %if.then38, %if.then36
  %68 = load i32, i32* %c2, align 4, !tbaa !27
  %69 = load i32, i32* %cmax, align 4, !tbaa !27
  %cmp40 = icmp sgt i32 %68, %69
  br i1 %cmp40, label %if.then41, label %if.end42

if.then41:                                        ; preds = %if.end39
  store i32 2, i32* %n, align 4, !tbaa !27
  br label %if.end42

if.end42:                                         ; preds = %if.then41, %if.end39
  br label %if.end50

if.else43:                                        ; preds = %if.end5
  %70 = load i32, i32* %c1, align 4, !tbaa !27
  store i32 %70, i32* %cmax, align 4, !tbaa !27
  store i32 1, i32* %n, align 4, !tbaa !27
  %71 = load i32, i32* %c2, align 4, !tbaa !27
  %72 = load i32, i32* %cmax, align 4, !tbaa !27
  %cmp44 = icmp sgt i32 %71, %72
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.else43
  %73 = load i32, i32* %c2, align 4, !tbaa !27
  store i32 %73, i32* %cmax, align 4, !tbaa !27
  store i32 2, i32* %n, align 4, !tbaa !27
  br label %if.end46

if.end46:                                         ; preds = %if.then45, %if.else43
  %74 = load i32, i32* %c0, align 4, !tbaa !27
  %75 = load i32, i32* %cmax, align 4, !tbaa !27
  %cmp47 = icmp sgt i32 %74, %75
  br i1 %cmp47, label %if.then48, label %if.end49

if.then48:                                        ; preds = %if.end46
  store i32 0, i32* %n, align 4, !tbaa !27
  br label %if.end49

if.end49:                                         ; preds = %if.then48, %if.end46
  br label %if.end50

if.end50:                                         ; preds = %if.end49, %if.end42
  %76 = load i32, i32* %n, align 4, !tbaa !27
  switch i32 %76, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb56
    i32 2, label %sw.bb64
  ]

sw.bb:                                            ; preds = %if.end50
  %77 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c0max51 = getelementptr inbounds %struct.box, %struct.box* %77, i32 0, i32 1
  %78 = load i32, i32* %c0max51, align 4, !tbaa !48
  %79 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c0min52 = getelementptr inbounds %struct.box, %struct.box* %79, i32 0, i32 0
  %80 = load i32, i32* %c0min52, align 4, !tbaa !46
  %add = add nsw i32 %78, %80
  %div = sdiv i32 %add, 2
  store i32 %div, i32* %lb, align 4, !tbaa !27
  %81 = load i32, i32* %lb, align 4, !tbaa !27
  %82 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c0max53 = getelementptr inbounds %struct.box, %struct.box* %82, i32 0, i32 1
  store i32 %81, i32* %c0max53, align 4, !tbaa !48
  %83 = load i32, i32* %lb, align 4, !tbaa !27
  %add54 = add nsw i32 %83, 1
  %84 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c0min55 = getelementptr inbounds %struct.box, %struct.box* %84, i32 0, i32 0
  store i32 %add54, i32* %c0min55, align 4, !tbaa !46
  br label %sw.epilog

sw.bb56:                                          ; preds = %if.end50
  %85 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c1max57 = getelementptr inbounds %struct.box, %struct.box* %85, i32 0, i32 3
  %86 = load i32, i32* %c1max57, align 4, !tbaa !50
  %87 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c1min58 = getelementptr inbounds %struct.box, %struct.box* %87, i32 0, i32 2
  %88 = load i32, i32* %c1min58, align 4, !tbaa !49
  %add59 = add nsw i32 %86, %88
  %div60 = sdiv i32 %add59, 2
  store i32 %div60, i32* %lb, align 4, !tbaa !27
  %89 = load i32, i32* %lb, align 4, !tbaa !27
  %90 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c1max61 = getelementptr inbounds %struct.box, %struct.box* %90, i32 0, i32 3
  store i32 %89, i32* %c1max61, align 4, !tbaa !50
  %91 = load i32, i32* %lb, align 4, !tbaa !27
  %add62 = add nsw i32 %91, 1
  %92 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c1min63 = getelementptr inbounds %struct.box, %struct.box* %92, i32 0, i32 2
  store i32 %add62, i32* %c1min63, align 4, !tbaa !49
  br label %sw.epilog

sw.bb64:                                          ; preds = %if.end50
  %93 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c2max65 = getelementptr inbounds %struct.box, %struct.box* %93, i32 0, i32 5
  %94 = load i32, i32* %c2max65, align 4, !tbaa !52
  %95 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c2min66 = getelementptr inbounds %struct.box, %struct.box* %95, i32 0, i32 4
  %96 = load i32, i32* %c2min66, align 4, !tbaa !51
  %add67 = add nsw i32 %94, %96
  %div68 = sdiv i32 %add67, 2
  store i32 %div68, i32* %lb, align 4, !tbaa !27
  %97 = load i32, i32* %lb, align 4, !tbaa !27
  %98 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  %c2max69 = getelementptr inbounds %struct.box, %struct.box* %98, i32 0, i32 5
  store i32 %97, i32* %c2max69, align 4, !tbaa !52
  %99 = load i32, i32* %lb, align 4, !tbaa !27
  %add70 = add nsw i32 %99, 1
  %100 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  %c2min71 = getelementptr inbounds %struct.box, %struct.box* %100, i32 0, i32 4
  store i32 %add70, i32* %c2min71, align 4, !tbaa !51
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end50, %sw.bb64, %sw.bb56, %sw.bb
  %101 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %102 = load %struct.box*, %struct.box** %b1, align 4, !tbaa !2
  call void @update_box(%struct.jpeg_decompress_struct* %101, %struct.box* %102)
  %103 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %104 = load %struct.box*, %struct.box** %b2, align 4, !tbaa !2
  call void @update_box(%struct.jpeg_decompress_struct* %103, %struct.box* %104)
  %105 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %inc = add nsw i32 %105, 1
  store i32 %inc, i32* %numboxes.addr, align 4, !tbaa !27
  br label %while.cond

while.end:                                        ; preds = %if.then4, %while.cond
  %106 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %107 = bitcast %struct.box** %b2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast %struct.box** %b1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast i32* %cmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %112 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  %113 = bitcast i32* %lb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #3
  %114 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #3
  ret i32 %106
}

; Function Attrs: nounwind
define internal void @compute_color(%struct.jpeg_decompress_struct* %cinfo, %struct.box* %boxp, i32 %icolor) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %boxp.addr = alloca %struct.box*, align 4
  %icolor.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %histogram = alloca [32 x i16]**, align 4
  %histp = alloca i16*, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %c0min = alloca i32, align 4
  %c0max = alloca i32, align 4
  %c1min = alloca i32, align 4
  %c1max = alloca i32, align 4
  %c2min = alloca i32, align 4
  %c2max = alloca i32, align 4
  %count = alloca i32, align 4
  %total = alloca i32, align 4
  %c0total = alloca i32, align 4
  %c1total = alloca i32, align 4
  %c2total = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.box* %boxp, %struct.box** %boxp.addr, align 4, !tbaa !2
  store i32 %icolor, i32* %icolor.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %5, i32 0, i32 3
  %6 = load [32 x i16]**, [32 x i16]*** %histogram2, align 4, !tbaa !26
  store [32 x i16]** %6, [32 x i16]*** %histogram, align 4, !tbaa !2
  %7 = bitcast i16** %histp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %c0min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %c0max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %c1min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %c1max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %c2min to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %c2max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %total to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  store i32 0, i32* %total, align 4, !tbaa !41
  %19 = bitcast i32* %c0total to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  store i32 0, i32* %c0total, align 4, !tbaa !41
  %20 = bitcast i32* %c1total to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  store i32 0, i32* %c1total, align 4, !tbaa !41
  %21 = bitcast i32* %c2total to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  store i32 0, i32* %c2total, align 4, !tbaa !41
  %22 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c0min3 = getelementptr inbounds %struct.box, %struct.box* %22, i32 0, i32 0
  %23 = load i32, i32* %c0min3, align 4, !tbaa !46
  store i32 %23, i32* %c0min, align 4, !tbaa !27
  %24 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c0max4 = getelementptr inbounds %struct.box, %struct.box* %24, i32 0, i32 1
  %25 = load i32, i32* %c0max4, align 4, !tbaa !48
  store i32 %25, i32* %c0max, align 4, !tbaa !27
  %26 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c1min5 = getelementptr inbounds %struct.box, %struct.box* %26, i32 0, i32 2
  %27 = load i32, i32* %c1min5, align 4, !tbaa !49
  store i32 %27, i32* %c1min, align 4, !tbaa !27
  %28 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c1max6 = getelementptr inbounds %struct.box, %struct.box* %28, i32 0, i32 3
  %29 = load i32, i32* %c1max6, align 4, !tbaa !50
  store i32 %29, i32* %c1max, align 4, !tbaa !27
  %30 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c2min7 = getelementptr inbounds %struct.box, %struct.box* %30, i32 0, i32 4
  %31 = load i32, i32* %c2min7, align 4, !tbaa !51
  store i32 %31, i32* %c2min, align 4, !tbaa !27
  %32 = load %struct.box*, %struct.box** %boxp.addr, align 4, !tbaa !2
  %c2max8 = getelementptr inbounds %struct.box, %struct.box* %32, i32 0, i32 5
  %33 = load i32, i32* %c2max8, align 4, !tbaa !52
  store i32 %33, i32* %c2max, align 4, !tbaa !27
  %34 = load i32, i32* %c0min, align 4, !tbaa !27
  store i32 %34, i32* %c0, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc32, %entry
  %35 = load i32, i32* %c0, align 4, !tbaa !27
  %36 = load i32, i32* %c0max, align 4, !tbaa !27
  %cmp = icmp sle i32 %35, %36
  br i1 %cmp, label %for.body, label %for.end34

for.body:                                         ; preds = %for.cond
  %37 = load i32, i32* %c1min, align 4, !tbaa !27
  store i32 %37, i32* %c1, align 4, !tbaa !27
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc29, %for.body
  %38 = load i32, i32* %c1, align 4, !tbaa !27
  %39 = load i32, i32* %c1max, align 4, !tbaa !27
  %cmp10 = icmp sle i32 %38, %39
  br i1 %cmp10, label %for.body11, label %for.end31

for.body11:                                       ; preds = %for.cond9
  %40 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %41 = load i32, i32* %c0, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [32 x i16]*, [32 x i16]** %40, i32 %41
  %42 = load [32 x i16]*, [32 x i16]** %arrayidx, align 4, !tbaa !2
  %43 = load i32, i32* %c1, align 4, !tbaa !27
  %arrayidx12 = getelementptr inbounds [32 x i16], [32 x i16]* %42, i32 %43
  %44 = load i32, i32* %c2min, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx12, i32 0, i32 %44
  store i16* %arrayidx13, i16** %histp, align 4, !tbaa !2
  %45 = load i32, i32* %c2min, align 4, !tbaa !27
  store i32 %45, i32* %c2, align 4, !tbaa !27
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %for.body11
  %46 = load i32, i32* %c2, align 4, !tbaa !27
  %47 = load i32, i32* %c2max, align 4, !tbaa !27
  %cmp15 = icmp sle i32 %46, %47
  br i1 %cmp15, label %for.body16, label %for.end

for.body16:                                       ; preds = %for.cond14
  %48 = load i16*, i16** %histp, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %48, i32 1
  store i16* %incdec.ptr, i16** %histp, align 4, !tbaa !2
  %49 = load i16, i16* %48, align 2, !tbaa !43
  %conv = zext i16 %49 to i32
  store i32 %conv, i32* %count, align 4, !tbaa !41
  %cmp17 = icmp ne i32 %conv, 0
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %for.body16
  %50 = load i32, i32* %count, align 4, !tbaa !41
  %51 = load i32, i32* %total, align 4, !tbaa !41
  %add = add nsw i32 %51, %50
  store i32 %add, i32* %total, align 4, !tbaa !41
  %52 = load i32, i32* %c0, align 4, !tbaa !27
  %shl = shl i32 %52, 3
  %add19 = add nsw i32 %shl, 4
  %53 = load i32, i32* %count, align 4, !tbaa !41
  %mul = mul nsw i32 %add19, %53
  %54 = load i32, i32* %c0total, align 4, !tbaa !41
  %add20 = add nsw i32 %54, %mul
  store i32 %add20, i32* %c0total, align 4, !tbaa !41
  %55 = load i32, i32* %c1, align 4, !tbaa !27
  %shl21 = shl i32 %55, 2
  %add22 = add nsw i32 %shl21, 2
  %56 = load i32, i32* %count, align 4, !tbaa !41
  %mul23 = mul nsw i32 %add22, %56
  %57 = load i32, i32* %c1total, align 4, !tbaa !41
  %add24 = add nsw i32 %57, %mul23
  store i32 %add24, i32* %c1total, align 4, !tbaa !41
  %58 = load i32, i32* %c2, align 4, !tbaa !27
  %shl25 = shl i32 %58, 3
  %add26 = add nsw i32 %shl25, 4
  %59 = load i32, i32* %count, align 4, !tbaa !41
  %mul27 = mul nsw i32 %add26, %59
  %60 = load i32, i32* %c2total, align 4, !tbaa !41
  %add28 = add nsw i32 %60, %mul27
  store i32 %add28, i32* %c2total, align 4, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body16
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %61 = load i32, i32* %c2, align 4, !tbaa !27
  %inc = add nsw i32 %61, 1
  store i32 %inc, i32* %c2, align 4, !tbaa !27
  br label %for.cond14

for.end:                                          ; preds = %for.cond14
  br label %for.inc29

for.inc29:                                        ; preds = %for.end
  %62 = load i32, i32* %c1, align 4, !tbaa !27
  %inc30 = add nsw i32 %62, 1
  store i32 %inc30, i32* %c1, align 4, !tbaa !27
  br label %for.cond9

for.end31:                                        ; preds = %for.cond9
  br label %for.inc32

for.inc32:                                        ; preds = %for.end31
  %63 = load i32, i32* %c0, align 4, !tbaa !27
  %inc33 = add nsw i32 %63, 1
  store i32 %inc33, i32* %c0, align 4, !tbaa !27
  br label %for.cond

for.end34:                                        ; preds = %for.cond
  %64 = load i32, i32* %c0total, align 4, !tbaa !41
  %65 = load i32, i32* %total, align 4, !tbaa !41
  %shr = ashr i32 %65, 1
  %add35 = add nsw i32 %64, %shr
  %66 = load i32, i32* %total, align 4, !tbaa !41
  %div = sdiv i32 %add35, %66
  %conv36 = trunc i32 %div to i8
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %67, i32 0, i32 33
  %68 = load i8**, i8*** %colormap, align 8, !tbaa !44
  %arrayidx37 = getelementptr inbounds i8*, i8** %68, i32 0
  %69 = load i8*, i8** %arrayidx37, align 4, !tbaa !2
  %70 = load i32, i32* %icolor.addr, align 4, !tbaa !27
  %arrayidx38 = getelementptr inbounds i8, i8* %69, i32 %70
  store i8 %conv36, i8* %arrayidx38, align 1, !tbaa !32
  %71 = load i32, i32* %c1total, align 4, !tbaa !41
  %72 = load i32, i32* %total, align 4, !tbaa !41
  %shr39 = ashr i32 %72, 1
  %add40 = add nsw i32 %71, %shr39
  %73 = load i32, i32* %total, align 4, !tbaa !41
  %div41 = sdiv i32 %add40, %73
  %conv42 = trunc i32 %div41 to i8
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap43 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %74, i32 0, i32 33
  %75 = load i8**, i8*** %colormap43, align 8, !tbaa !44
  %arrayidx44 = getelementptr inbounds i8*, i8** %75, i32 1
  %76 = load i8*, i8** %arrayidx44, align 4, !tbaa !2
  %77 = load i32, i32* %icolor.addr, align 4, !tbaa !27
  %arrayidx45 = getelementptr inbounds i8, i8* %76, i32 %77
  store i8 %conv42, i8* %arrayidx45, align 1, !tbaa !32
  %78 = load i32, i32* %c2total, align 4, !tbaa !41
  %79 = load i32, i32* %total, align 4, !tbaa !41
  %shr46 = ashr i32 %79, 1
  %add47 = add nsw i32 %78, %shr46
  %80 = load i32, i32* %total, align 4, !tbaa !41
  %div48 = sdiv i32 %add47, %80
  %conv49 = trunc i32 %div48 to i8
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap50 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 33
  %82 = load i8**, i8*** %colormap50, align 8, !tbaa !44
  %arrayidx51 = getelementptr inbounds i8*, i8** %82, i32 2
  %83 = load i8*, i8** %arrayidx51, align 4, !tbaa !2
  %84 = load i32, i32* %icolor.addr, align 4, !tbaa !27
  %arrayidx52 = getelementptr inbounds i8, i8* %83, i32 %84
  store i8 %conv49, i8* %arrayidx52, align 1, !tbaa !32
  %85 = bitcast i32* %c2total to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #3
  %86 = bitcast i32* %c1total to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #3
  %87 = bitcast i32* %c0total to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #3
  %88 = bitcast i32* %total to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #3
  %89 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #3
  %90 = bitcast i32* %c2max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #3
  %91 = bitcast i32* %c2min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #3
  %92 = bitcast i32* %c1max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #3
  %93 = bitcast i32* %c1min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #3
  %94 = bitcast i32* %c0max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #3
  %95 = bitcast i32* %c0min to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  %96 = bitcast i32* %c2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #3
  %97 = bitcast i32* %c1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #3
  %98 = bitcast i32* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #3
  %99 = bitcast i16** %histp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #3
  %100 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #3
  %101 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #3
  ret void
}

; Function Attrs: nounwind
define internal %struct.box* @find_biggest_color_pop(%struct.box* %boxlist, i32 %numboxes) #0 {
entry:
  %boxlist.addr = alloca %struct.box*, align 4
  %numboxes.addr = alloca i32, align 4
  %boxp = alloca %struct.box*, align 4
  %i = alloca i32, align 4
  %maxc = alloca i32, align 4
  %which = alloca %struct.box*, align 4
  store %struct.box* %boxlist, %struct.box** %boxlist.addr, align 4, !tbaa !2
  store i32 %numboxes, i32* %numboxes.addr, align 4, !tbaa !27
  %0 = bitcast %struct.box** %boxp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %maxc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %maxc, align 4, !tbaa !41
  %3 = bitcast %struct.box** %which to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store %struct.box* null, %struct.box** %which, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !27
  %4 = load %struct.box*, %struct.box** %boxlist.addr, align 4, !tbaa !2
  store %struct.box* %4, %struct.box** %boxp, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !27
  %6 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  %colorcount = getelementptr inbounds %struct.box, %struct.box* %7, i32 0, i32 7
  %8 = load i32, i32* %colorcount, align 4, !tbaa !56
  %9 = load i32, i32* %maxc, align 4, !tbaa !41
  %cmp1 = icmp sgt i32 %8, %9
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %10 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  %volume = getelementptr inbounds %struct.box, %struct.box* %10, i32 0, i32 6
  %11 = load i32, i32* %volume, align 4, !tbaa !55
  %cmp2 = icmp sgt i32 %11, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %12 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  store %struct.box* %12, %struct.box** %which, align 4, !tbaa !2
  %13 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  %colorcount3 = getelementptr inbounds %struct.box, %struct.box* %13, i32 0, i32 7
  %14 = load i32, i32* %colorcount3, align 4, !tbaa !56
  store i32 %14, i32* %maxc, align 4, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %15 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  %16 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.box, %struct.box* %16, i32 1
  store %struct.box* %incdec.ptr, %struct.box** %boxp, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %17 = load %struct.box*, %struct.box** %which, align 4, !tbaa !2
  %18 = bitcast %struct.box** %which to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %maxc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  %21 = bitcast %struct.box** %boxp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  ret %struct.box* %17
}

; Function Attrs: nounwind
define internal %struct.box* @find_biggest_volume(%struct.box* %boxlist, i32 %numboxes) #0 {
entry:
  %boxlist.addr = alloca %struct.box*, align 4
  %numboxes.addr = alloca i32, align 4
  %boxp = alloca %struct.box*, align 4
  %i = alloca i32, align 4
  %maxv = alloca i32, align 4
  %which = alloca %struct.box*, align 4
  store %struct.box* %boxlist, %struct.box** %boxlist.addr, align 4, !tbaa !2
  store i32 %numboxes, i32* %numboxes.addr, align 4, !tbaa !27
  %0 = bitcast %struct.box** %boxp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %maxv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 0, i32* %maxv, align 4, !tbaa !41
  %3 = bitcast %struct.box** %which to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store %struct.box* null, %struct.box** %which, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !27
  %4 = load %struct.box*, %struct.box** %boxlist.addr, align 4, !tbaa !2
  store %struct.box* %4, %struct.box** %boxp, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !27
  %6 = load i32, i32* %numboxes.addr, align 4, !tbaa !27
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  %volume = getelementptr inbounds %struct.box, %struct.box* %7, i32 0, i32 6
  %8 = load i32, i32* %volume, align 4, !tbaa !55
  %9 = load i32, i32* %maxv, align 4, !tbaa !41
  %cmp1 = icmp sgt i32 %8, %9
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  store %struct.box* %10, %struct.box** %which, align 4, !tbaa !2
  %11 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  %volume2 = getelementptr inbounds %struct.box, %struct.box* %11, i32 0, i32 6
  %12 = load i32, i32* %volume2, align 4, !tbaa !55
  store i32 %12, i32* %maxv, align 4, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %13 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  %14 = load %struct.box*, %struct.box** %boxp, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.box, %struct.box* %14, i32 1
  store %struct.box* %incdec.ptr, %struct.box** %boxp, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load %struct.box*, %struct.box** %which, align 4, !tbaa !2
  %16 = bitcast %struct.box** %which to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %maxv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  %19 = bitcast %struct.box** %boxp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  ret %struct.box* %15
}

; Function Attrs: nounwind
define internal void @fill_inverse_cmap(%struct.jpeg_decompress_struct* %cinfo, i32 %c0, i32 %c1, i32 %c2) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %c0.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %histogram = alloca [32 x i16]**, align 4
  %minc0 = alloca i32, align 4
  %minc1 = alloca i32, align 4
  %minc2 = alloca i32, align 4
  %ic0 = alloca i32, align 4
  %ic1 = alloca i32, align 4
  %ic2 = alloca i32, align 4
  %cptr = alloca i8*, align 4
  %cachep = alloca i16*, align 4
  %colorlist = alloca [256 x i8], align 16
  %numcolors = alloca i32, align 4
  %bestcolor = alloca [128 x i8], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %c0, i32* %c0.addr, align 4, !tbaa !27
  store i32 %c1, i32* %c1.addr, align 4, !tbaa !27
  store i32 %c2, i32* %c2.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %histogram2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %5, i32 0, i32 3
  %6 = load [32 x i16]**, [32 x i16]*** %histogram2, align 4, !tbaa !26
  store [32 x i16]** %6, [32 x i16]*** %histogram, align 4, !tbaa !2
  %7 = bitcast i32* %minc0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %minc1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %minc2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %ic0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %ic1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %ic2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i8** %cptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i16** %cachep to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast [256 x i8]* %colorlist to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %15) #3
  %16 = bitcast i32* %numcolors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast [128 x i8]* %bestcolor to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %17) #3
  %18 = load i32, i32* %c0.addr, align 4, !tbaa !27
  %shr = ashr i32 %18, 2
  store i32 %shr, i32* %c0.addr, align 4, !tbaa !27
  %19 = load i32, i32* %c1.addr, align 4, !tbaa !27
  %shr3 = ashr i32 %19, 3
  store i32 %shr3, i32* %c1.addr, align 4, !tbaa !27
  %20 = load i32, i32* %c2.addr, align 4, !tbaa !27
  %shr4 = ashr i32 %20, 2
  store i32 %shr4, i32* %c2.addr, align 4, !tbaa !27
  %21 = load i32, i32* %c0.addr, align 4, !tbaa !27
  %shl = shl i32 %21, 5
  %add = add nsw i32 %shl, 4
  store i32 %add, i32* %minc0, align 4, !tbaa !27
  %22 = load i32, i32* %c1.addr, align 4, !tbaa !27
  %shl5 = shl i32 %22, 5
  %add6 = add nsw i32 %shl5, 2
  store i32 %add6, i32* %minc1, align 4, !tbaa !27
  %23 = load i32, i32* %c2.addr, align 4, !tbaa !27
  %shl7 = shl i32 %23, 5
  %add8 = add nsw i32 %shl7, 4
  store i32 %add8, i32* %minc2, align 4, !tbaa !27
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = load i32, i32* %minc0, align 4, !tbaa !27
  %26 = load i32, i32* %minc1, align 4, !tbaa !27
  %27 = load i32, i32* %minc2, align 4, !tbaa !27
  %arraydecay = getelementptr inbounds [256 x i8], [256 x i8]* %colorlist, i32 0, i32 0
  %call = call i32 @find_nearby_colors(%struct.jpeg_decompress_struct* %24, i32 %25, i32 %26, i32 %27, i8* %arraydecay)
  store i32 %call, i32* %numcolors, align 4, !tbaa !27
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %29 = load i32, i32* %minc0, align 4, !tbaa !27
  %30 = load i32, i32* %minc1, align 4, !tbaa !27
  %31 = load i32, i32* %minc2, align 4, !tbaa !27
  %32 = load i32, i32* %numcolors, align 4, !tbaa !27
  %arraydecay9 = getelementptr inbounds [256 x i8], [256 x i8]* %colorlist, i32 0, i32 0
  %arraydecay10 = getelementptr inbounds [128 x i8], [128 x i8]* %bestcolor, i32 0, i32 0
  call void @find_best_colors(%struct.jpeg_decompress_struct* %28, i32 %29, i32 %30, i32 %31, i32 %32, i8* %arraydecay9, i8* %arraydecay10)
  %33 = load i32, i32* %c0.addr, align 4, !tbaa !27
  %shl11 = shl i32 %33, 2
  store i32 %shl11, i32* %c0.addr, align 4, !tbaa !27
  %34 = load i32, i32* %c1.addr, align 4, !tbaa !27
  %shl12 = shl i32 %34, 3
  store i32 %shl12, i32* %c1.addr, align 4, !tbaa !27
  %35 = load i32, i32* %c2.addr, align 4, !tbaa !27
  %shl13 = shl i32 %35, 2
  store i32 %shl13, i32* %c2.addr, align 4, !tbaa !27
  %arraydecay14 = getelementptr inbounds [128 x i8], [128 x i8]* %bestcolor, i32 0, i32 0
  store i8* %arraydecay14, i8** %cptr, align 4, !tbaa !2
  store i32 0, i32* %ic0, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc31, %entry
  %36 = load i32, i32* %ic0, align 4, !tbaa !27
  %cmp = icmp slt i32 %36, 4
  br i1 %cmp, label %for.body, label %for.end33

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %ic1, align 4, !tbaa !27
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc28, %for.body
  %37 = load i32, i32* %ic1, align 4, !tbaa !27
  %cmp16 = icmp slt i32 %37, 8
  br i1 %cmp16, label %for.body17, label %for.end30

for.body17:                                       ; preds = %for.cond15
  %38 = load [32 x i16]**, [32 x i16]*** %histogram, align 4, !tbaa !2
  %39 = load i32, i32* %c0.addr, align 4, !tbaa !27
  %40 = load i32, i32* %ic0, align 4, !tbaa !27
  %add18 = add nsw i32 %39, %40
  %arrayidx = getelementptr inbounds [32 x i16]*, [32 x i16]** %38, i32 %add18
  %41 = load [32 x i16]*, [32 x i16]** %arrayidx, align 4, !tbaa !2
  %42 = load i32, i32* %c1.addr, align 4, !tbaa !27
  %43 = load i32, i32* %ic1, align 4, !tbaa !27
  %add19 = add nsw i32 %42, %43
  %arrayidx20 = getelementptr inbounds [32 x i16], [32 x i16]* %41, i32 %add19
  %44 = load i32, i32* %c2.addr, align 4, !tbaa !27
  %arrayidx21 = getelementptr inbounds [32 x i16], [32 x i16]* %arrayidx20, i32 0, i32 %44
  store i16* %arrayidx21, i16** %cachep, align 4, !tbaa !2
  store i32 0, i32* %ic2, align 4, !tbaa !27
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %for.body17
  %45 = load i32, i32* %ic2, align 4, !tbaa !27
  %cmp23 = icmp slt i32 %45, 4
  br i1 %cmp23, label %for.body24, label %for.end

for.body24:                                       ; preds = %for.cond22
  %46 = load i8*, i8** %cptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %46, i32 1
  store i8* %incdec.ptr, i8** %cptr, align 4, !tbaa !2
  %47 = load i8, i8* %46, align 1, !tbaa !32
  %conv = zext i8 %47 to i32
  %add25 = add nsw i32 %conv, 1
  %conv26 = trunc i32 %add25 to i16
  %48 = load i16*, i16** %cachep, align 4, !tbaa !2
  %incdec.ptr27 = getelementptr inbounds i16, i16* %48, i32 1
  store i16* %incdec.ptr27, i16** %cachep, align 4, !tbaa !2
  store i16 %conv26, i16* %48, align 2, !tbaa !43
  br label %for.inc

for.inc:                                          ; preds = %for.body24
  %49 = load i32, i32* %ic2, align 4, !tbaa !27
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %ic2, align 4, !tbaa !27
  br label %for.cond22

for.end:                                          ; preds = %for.cond22
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %50 = load i32, i32* %ic1, align 4, !tbaa !27
  %inc29 = add nsw i32 %50, 1
  store i32 %inc29, i32* %ic1, align 4, !tbaa !27
  br label %for.cond15

for.end30:                                        ; preds = %for.cond15
  br label %for.inc31

for.inc31:                                        ; preds = %for.end30
  %51 = load i32, i32* %ic0, align 4, !tbaa !27
  %inc32 = add nsw i32 %51, 1
  store i32 %inc32, i32* %ic0, align 4, !tbaa !27
  br label %for.cond

for.end33:                                        ; preds = %for.cond
  %52 = bitcast [128 x i8]* %bestcolor to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %52) #3
  %53 = bitcast i32* %numcolors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast [256 x i8]* %colorlist to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %54) #3
  %55 = bitcast i16** %cachep to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i8** %cptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i32* %ic2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %ic1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast i32* %ic0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  %60 = bitcast i32* %minc2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  %61 = bitcast i32* %minc1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #3
  %62 = bitcast i32* %minc0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #3
  %63 = bitcast [32 x i16]*** %histogram to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #3
  %64 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @find_nearby_colors(%struct.jpeg_decompress_struct* %cinfo, i32 %minc0, i32 %minc1, i32 %minc2, i8* %colorlist) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %minc0.addr = alloca i32, align 4
  %minc1.addr = alloca i32, align 4
  %minc2.addr = alloca i32, align 4
  %colorlist.addr = alloca i8*, align 4
  %numcolors = alloca i32, align 4
  %maxc0 = alloca i32, align 4
  %maxc1 = alloca i32, align 4
  %maxc2 = alloca i32, align 4
  %centerc0 = alloca i32, align 4
  %centerc1 = alloca i32, align 4
  %centerc2 = alloca i32, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %ncolors = alloca i32, align 4
  %minmaxdist = alloca i32, align 4
  %min_dist = alloca i32, align 4
  %max_dist = alloca i32, align 4
  %tdist = alloca i32, align 4
  %mindist = alloca [256 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %minc0, i32* %minc0.addr, align 4, !tbaa !27
  store i32 %minc1, i32* %minc1.addr, align 4, !tbaa !27
  store i32 %minc2, i32* %minc2.addr, align 4, !tbaa !27
  store i8* %colorlist, i8** %colorlist.addr, align 4, !tbaa !2
  %0 = bitcast i32* %numcolors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %actual_number_of_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 32
  %2 = load i32, i32* %actual_number_of_colors, align 4, !tbaa !40
  store i32 %2, i32* %numcolors, align 4, !tbaa !27
  %3 = bitcast i32* %maxc0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %maxc1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %maxc2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %centerc0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %centerc1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %centerc2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %ncolors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %minmaxdist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %min_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %max_dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %tdist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast [256 x i32]* %mindist to i8*
  call void @llvm.lifetime.start.p0i8(i64 1024, i8* %16) #3
  %17 = load i32, i32* %minc0.addr, align 4, !tbaa !27
  %add = add nsw i32 %17, 24
  store i32 %add, i32* %maxc0, align 4, !tbaa !27
  %18 = load i32, i32* %minc0.addr, align 4, !tbaa !27
  %19 = load i32, i32* %maxc0, align 4, !tbaa !27
  %add1 = add nsw i32 %18, %19
  %shr = ashr i32 %add1, 1
  store i32 %shr, i32* %centerc0, align 4, !tbaa !27
  %20 = load i32, i32* %minc1.addr, align 4, !tbaa !27
  %add2 = add nsw i32 %20, 28
  store i32 %add2, i32* %maxc1, align 4, !tbaa !27
  %21 = load i32, i32* %minc1.addr, align 4, !tbaa !27
  %22 = load i32, i32* %maxc1, align 4, !tbaa !27
  %add3 = add nsw i32 %21, %22
  %shr4 = ashr i32 %add3, 1
  store i32 %shr4, i32* %centerc1, align 4, !tbaa !27
  %23 = load i32, i32* %minc2.addr, align 4, !tbaa !27
  %add5 = add nsw i32 %23, 24
  store i32 %add5, i32* %maxc2, align 4, !tbaa !27
  %24 = load i32, i32* %minc2.addr, align 4, !tbaa !27
  %25 = load i32, i32* %maxc2, align 4, !tbaa !27
  %add6 = add nsw i32 %24, %25
  %shr7 = ashr i32 %add6, 1
  store i32 %shr7, i32* %centerc2, align 4, !tbaa !27
  store i32 2147483647, i32* %minmaxdist, align 4, !tbaa !41
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %26 = load i32, i32* %i, align 4, !tbaa !27
  %27 = load i32, i32* %numcolors, align 4, !tbaa !27
  %cmp = icmp slt i32 %26, %27
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 33
  %29 = load i8**, i8*** %colormap, align 8, !tbaa !44
  %arrayidx = getelementptr inbounds i8*, i8** %29, i32 0
  %30 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %31 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %30, i32 %31
  %32 = load i8, i8* %arrayidx8, align 1, !tbaa !32
  %conv = zext i8 %32 to i32
  store i32 %conv, i32* %x, align 4, !tbaa !27
  %33 = load i32, i32* %x, align 4, !tbaa !27
  %34 = load i32, i32* %minc0.addr, align 4, !tbaa !27
  %cmp9 = icmp slt i32 %33, %34
  br i1 %cmp9, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %35 = load i32, i32* %x, align 4, !tbaa !27
  %36 = load i32, i32* %minc0.addr, align 4, !tbaa !27
  %sub = sub nsw i32 %35, %36
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 11
  %38 = load i32, i32* %out_color_space, align 4, !tbaa !54
  %arrayidx11 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %38
  %39 = load i32, i32* %arrayidx11, align 4, !tbaa !27
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %39
  %40 = load i32, i32* %arrayidx12, align 4, !tbaa !27
  %mul = mul nsw i32 %sub, %40
  store i32 %mul, i32* %tdist, align 4, !tbaa !41
  %41 = load i32, i32* %tdist, align 4, !tbaa !41
  %42 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul13 = mul nsw i32 %41, %42
  store i32 %mul13, i32* %min_dist, align 4, !tbaa !41
  %43 = load i32, i32* %x, align 4, !tbaa !27
  %44 = load i32, i32* %maxc0, align 4, !tbaa !27
  %sub14 = sub nsw i32 %43, %44
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 11
  %46 = load i32, i32* %out_color_space15, align 4, !tbaa !54
  %arrayidx16 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx16, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %47
  %48 = load i32, i32* %arrayidx17, align 4, !tbaa !27
  %mul18 = mul nsw i32 %sub14, %48
  store i32 %mul18, i32* %tdist, align 4, !tbaa !41
  %49 = load i32, i32* %tdist, align 4, !tbaa !41
  %50 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul19 = mul nsw i32 %49, %50
  store i32 %mul19, i32* %max_dist, align 4, !tbaa !41
  br label %if.end53

if.else:                                          ; preds = %for.body
  %51 = load i32, i32* %x, align 4, !tbaa !27
  %52 = load i32, i32* %maxc0, align 4, !tbaa !27
  %cmp20 = icmp sgt i32 %51, %52
  br i1 %cmp20, label %if.then22, label %if.else35

if.then22:                                        ; preds = %if.else
  %53 = load i32, i32* %x, align 4, !tbaa !27
  %54 = load i32, i32* %maxc0, align 4, !tbaa !27
  %sub23 = sub nsw i32 %53, %54
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space24 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 11
  %56 = load i32, i32* %out_color_space24, align 4, !tbaa !54
  %arrayidx25 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %56
  %57 = load i32, i32* %arrayidx25, align 4, !tbaa !27
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %57
  %58 = load i32, i32* %arrayidx26, align 4, !tbaa !27
  %mul27 = mul nsw i32 %sub23, %58
  store i32 %mul27, i32* %tdist, align 4, !tbaa !41
  %59 = load i32, i32* %tdist, align 4, !tbaa !41
  %60 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul28 = mul nsw i32 %59, %60
  store i32 %mul28, i32* %min_dist, align 4, !tbaa !41
  %61 = load i32, i32* %x, align 4, !tbaa !27
  %62 = load i32, i32* %minc0.addr, align 4, !tbaa !27
  %sub29 = sub nsw i32 %61, %62
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space30 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %63, i32 0, i32 11
  %64 = load i32, i32* %out_color_space30, align 4, !tbaa !54
  %arrayidx31 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %64
  %65 = load i32, i32* %arrayidx31, align 4, !tbaa !27
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %65
  %66 = load i32, i32* %arrayidx32, align 4, !tbaa !27
  %mul33 = mul nsw i32 %sub29, %66
  store i32 %mul33, i32* %tdist, align 4, !tbaa !41
  %67 = load i32, i32* %tdist, align 4, !tbaa !41
  %68 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul34 = mul nsw i32 %67, %68
  store i32 %mul34, i32* %max_dist, align 4, !tbaa !41
  br label %if.end52

if.else35:                                        ; preds = %if.else
  store i32 0, i32* %min_dist, align 4, !tbaa !41
  %69 = load i32, i32* %x, align 4, !tbaa !27
  %70 = load i32, i32* %centerc0, align 4, !tbaa !27
  %cmp36 = icmp sle i32 %69, %70
  br i1 %cmp36, label %if.then38, label %if.else45

if.then38:                                        ; preds = %if.else35
  %71 = load i32, i32* %x, align 4, !tbaa !27
  %72 = load i32, i32* %maxc0, align 4, !tbaa !27
  %sub39 = sub nsw i32 %71, %72
  %73 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %73, i32 0, i32 11
  %74 = load i32, i32* %out_color_space40, align 4, !tbaa !54
  %arrayidx41 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %74
  %75 = load i32, i32* %arrayidx41, align 4, !tbaa !27
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %75
  %76 = load i32, i32* %arrayidx42, align 4, !tbaa !27
  %mul43 = mul nsw i32 %sub39, %76
  store i32 %mul43, i32* %tdist, align 4, !tbaa !41
  %77 = load i32, i32* %tdist, align 4, !tbaa !41
  %78 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul44 = mul nsw i32 %77, %78
  store i32 %mul44, i32* %max_dist, align 4, !tbaa !41
  br label %if.end

if.else45:                                        ; preds = %if.else35
  %79 = load i32, i32* %x, align 4, !tbaa !27
  %80 = load i32, i32* %minc0.addr, align 4, !tbaa !27
  %sub46 = sub nsw i32 %79, %80
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space47 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 11
  %82 = load i32, i32* %out_color_space47, align 4, !tbaa !54
  %arrayidx48 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %82
  %83 = load i32, i32* %arrayidx48, align 4, !tbaa !27
  %arrayidx49 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %83
  %84 = load i32, i32* %arrayidx49, align 4, !tbaa !27
  %mul50 = mul nsw i32 %sub46, %84
  store i32 %mul50, i32* %tdist, align 4, !tbaa !41
  %85 = load i32, i32* %tdist, align 4, !tbaa !41
  %86 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul51 = mul nsw i32 %85, %86
  store i32 %mul51, i32* %max_dist, align 4, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.else45, %if.then38
  br label %if.end52

if.end52:                                         ; preds = %if.end, %if.then22
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.then
  %87 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %87, i32 0, i32 33
  %88 = load i8**, i8*** %colormap54, align 8, !tbaa !44
  %arrayidx55 = getelementptr inbounds i8*, i8** %88, i32 1
  %89 = load i8*, i8** %arrayidx55, align 4, !tbaa !2
  %90 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx56 = getelementptr inbounds i8, i8* %89, i32 %90
  %91 = load i8, i8* %arrayidx56, align 1, !tbaa !32
  %conv57 = zext i8 %91 to i32
  store i32 %conv57, i32* %x, align 4, !tbaa !27
  %92 = load i32, i32* %x, align 4, !tbaa !27
  %93 = load i32, i32* %minc1.addr, align 4, !tbaa !27
  %cmp58 = icmp slt i32 %92, %93
  br i1 %cmp58, label %if.then60, label %if.else75

if.then60:                                        ; preds = %if.end53
  %94 = load i32, i32* %x, align 4, !tbaa !27
  %95 = load i32, i32* %minc1.addr, align 4, !tbaa !27
  %sub61 = sub nsw i32 %94, %95
  %96 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space62 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %96, i32 0, i32 11
  %97 = load i32, i32* %out_color_space62, align 4, !tbaa !54
  %arrayidx63 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %97
  %98 = load i32, i32* %arrayidx63, align 4, !tbaa !27
  %arrayidx64 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %98
  %99 = load i32, i32* %arrayidx64, align 4, !tbaa !27
  %mul65 = mul nsw i32 %sub61, %99
  store i32 %mul65, i32* %tdist, align 4, !tbaa !41
  %100 = load i32, i32* %tdist, align 4, !tbaa !41
  %101 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul66 = mul nsw i32 %100, %101
  %102 = load i32, i32* %min_dist, align 4, !tbaa !41
  %add67 = add nsw i32 %102, %mul66
  store i32 %add67, i32* %min_dist, align 4, !tbaa !41
  %103 = load i32, i32* %x, align 4, !tbaa !27
  %104 = load i32, i32* %maxc1, align 4, !tbaa !27
  %sub68 = sub nsw i32 %103, %104
  %105 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space69 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %105, i32 0, i32 11
  %106 = load i32, i32* %out_color_space69, align 4, !tbaa !54
  %arrayidx70 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %106
  %107 = load i32, i32* %arrayidx70, align 4, !tbaa !27
  %arrayidx71 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %107
  %108 = load i32, i32* %arrayidx71, align 4, !tbaa !27
  %mul72 = mul nsw i32 %sub68, %108
  store i32 %mul72, i32* %tdist, align 4, !tbaa !41
  %109 = load i32, i32* %tdist, align 4, !tbaa !41
  %110 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul73 = mul nsw i32 %109, %110
  %111 = load i32, i32* %max_dist, align 4, !tbaa !41
  %add74 = add nsw i32 %111, %mul73
  store i32 %add74, i32* %max_dist, align 4, !tbaa !41
  br label %if.end114

if.else75:                                        ; preds = %if.end53
  %112 = load i32, i32* %x, align 4, !tbaa !27
  %113 = load i32, i32* %maxc1, align 4, !tbaa !27
  %cmp76 = icmp sgt i32 %112, %113
  br i1 %cmp76, label %if.then78, label %if.else93

if.then78:                                        ; preds = %if.else75
  %114 = load i32, i32* %x, align 4, !tbaa !27
  %115 = load i32, i32* %maxc1, align 4, !tbaa !27
  %sub79 = sub nsw i32 %114, %115
  %116 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space80 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %116, i32 0, i32 11
  %117 = load i32, i32* %out_color_space80, align 4, !tbaa !54
  %arrayidx81 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %117
  %118 = load i32, i32* %arrayidx81, align 4, !tbaa !27
  %arrayidx82 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %118
  %119 = load i32, i32* %arrayidx82, align 4, !tbaa !27
  %mul83 = mul nsw i32 %sub79, %119
  store i32 %mul83, i32* %tdist, align 4, !tbaa !41
  %120 = load i32, i32* %tdist, align 4, !tbaa !41
  %121 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul84 = mul nsw i32 %120, %121
  %122 = load i32, i32* %min_dist, align 4, !tbaa !41
  %add85 = add nsw i32 %122, %mul84
  store i32 %add85, i32* %min_dist, align 4, !tbaa !41
  %123 = load i32, i32* %x, align 4, !tbaa !27
  %124 = load i32, i32* %minc1.addr, align 4, !tbaa !27
  %sub86 = sub nsw i32 %123, %124
  %125 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space87 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %125, i32 0, i32 11
  %126 = load i32, i32* %out_color_space87, align 4, !tbaa !54
  %arrayidx88 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %126
  %127 = load i32, i32* %arrayidx88, align 4, !tbaa !27
  %arrayidx89 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %127
  %128 = load i32, i32* %arrayidx89, align 4, !tbaa !27
  %mul90 = mul nsw i32 %sub86, %128
  store i32 %mul90, i32* %tdist, align 4, !tbaa !41
  %129 = load i32, i32* %tdist, align 4, !tbaa !41
  %130 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul91 = mul nsw i32 %129, %130
  %131 = load i32, i32* %max_dist, align 4, !tbaa !41
  %add92 = add nsw i32 %131, %mul91
  store i32 %add92, i32* %max_dist, align 4, !tbaa !41
  br label %if.end113

if.else93:                                        ; preds = %if.else75
  %132 = load i32, i32* %x, align 4, !tbaa !27
  %133 = load i32, i32* %centerc1, align 4, !tbaa !27
  %cmp94 = icmp sle i32 %132, %133
  br i1 %cmp94, label %if.then96, label %if.else104

if.then96:                                        ; preds = %if.else93
  %134 = load i32, i32* %x, align 4, !tbaa !27
  %135 = load i32, i32* %maxc1, align 4, !tbaa !27
  %sub97 = sub nsw i32 %134, %135
  %136 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space98 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %136, i32 0, i32 11
  %137 = load i32, i32* %out_color_space98, align 4, !tbaa !54
  %arrayidx99 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %137
  %138 = load i32, i32* %arrayidx99, align 4, !tbaa !27
  %arrayidx100 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %138
  %139 = load i32, i32* %arrayidx100, align 4, !tbaa !27
  %mul101 = mul nsw i32 %sub97, %139
  store i32 %mul101, i32* %tdist, align 4, !tbaa !41
  %140 = load i32, i32* %tdist, align 4, !tbaa !41
  %141 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul102 = mul nsw i32 %140, %141
  %142 = load i32, i32* %max_dist, align 4, !tbaa !41
  %add103 = add nsw i32 %142, %mul102
  store i32 %add103, i32* %max_dist, align 4, !tbaa !41
  br label %if.end112

if.else104:                                       ; preds = %if.else93
  %143 = load i32, i32* %x, align 4, !tbaa !27
  %144 = load i32, i32* %minc1.addr, align 4, !tbaa !27
  %sub105 = sub nsw i32 %143, %144
  %145 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space106 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %145, i32 0, i32 11
  %146 = load i32, i32* %out_color_space106, align 4, !tbaa !54
  %arrayidx107 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %146
  %147 = load i32, i32* %arrayidx107, align 4, !tbaa !27
  %arrayidx108 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %147
  %148 = load i32, i32* %arrayidx108, align 4, !tbaa !27
  %mul109 = mul nsw i32 %sub105, %148
  store i32 %mul109, i32* %tdist, align 4, !tbaa !41
  %149 = load i32, i32* %tdist, align 4, !tbaa !41
  %150 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul110 = mul nsw i32 %149, %150
  %151 = load i32, i32* %max_dist, align 4, !tbaa !41
  %add111 = add nsw i32 %151, %mul110
  store i32 %add111, i32* %max_dist, align 4, !tbaa !41
  br label %if.end112

if.end112:                                        ; preds = %if.else104, %if.then96
  br label %if.end113

if.end113:                                        ; preds = %if.end112, %if.then78
  br label %if.end114

if.end114:                                        ; preds = %if.end113, %if.then60
  %152 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap115 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %152, i32 0, i32 33
  %153 = load i8**, i8*** %colormap115, align 8, !tbaa !44
  %arrayidx116 = getelementptr inbounds i8*, i8** %153, i32 2
  %154 = load i8*, i8** %arrayidx116, align 4, !tbaa !2
  %155 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx117 = getelementptr inbounds i8, i8* %154, i32 %155
  %156 = load i8, i8* %arrayidx117, align 1, !tbaa !32
  %conv118 = zext i8 %156 to i32
  store i32 %conv118, i32* %x, align 4, !tbaa !27
  %157 = load i32, i32* %x, align 4, !tbaa !27
  %158 = load i32, i32* %minc2.addr, align 4, !tbaa !27
  %cmp119 = icmp slt i32 %157, %158
  br i1 %cmp119, label %if.then121, label %if.else136

if.then121:                                       ; preds = %if.end114
  %159 = load i32, i32* %x, align 4, !tbaa !27
  %160 = load i32, i32* %minc2.addr, align 4, !tbaa !27
  %sub122 = sub nsw i32 %159, %160
  %161 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space123 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %161, i32 0, i32 11
  %162 = load i32, i32* %out_color_space123, align 4, !tbaa !54
  %arrayidx124 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %162
  %163 = load i32, i32* %arrayidx124, align 4, !tbaa !27
  %arrayidx125 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %163
  %164 = load i32, i32* %arrayidx125, align 4, !tbaa !27
  %mul126 = mul nsw i32 %sub122, %164
  store i32 %mul126, i32* %tdist, align 4, !tbaa !41
  %165 = load i32, i32* %tdist, align 4, !tbaa !41
  %166 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul127 = mul nsw i32 %165, %166
  %167 = load i32, i32* %min_dist, align 4, !tbaa !41
  %add128 = add nsw i32 %167, %mul127
  store i32 %add128, i32* %min_dist, align 4, !tbaa !41
  %168 = load i32, i32* %x, align 4, !tbaa !27
  %169 = load i32, i32* %maxc2, align 4, !tbaa !27
  %sub129 = sub nsw i32 %168, %169
  %170 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space130 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %170, i32 0, i32 11
  %171 = load i32, i32* %out_color_space130, align 4, !tbaa !54
  %arrayidx131 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %171
  %172 = load i32, i32* %arrayidx131, align 4, !tbaa !27
  %arrayidx132 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %172
  %173 = load i32, i32* %arrayidx132, align 4, !tbaa !27
  %mul133 = mul nsw i32 %sub129, %173
  store i32 %mul133, i32* %tdist, align 4, !tbaa !41
  %174 = load i32, i32* %tdist, align 4, !tbaa !41
  %175 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul134 = mul nsw i32 %174, %175
  %176 = load i32, i32* %max_dist, align 4, !tbaa !41
  %add135 = add nsw i32 %176, %mul134
  store i32 %add135, i32* %max_dist, align 4, !tbaa !41
  br label %if.end175

if.else136:                                       ; preds = %if.end114
  %177 = load i32, i32* %x, align 4, !tbaa !27
  %178 = load i32, i32* %maxc2, align 4, !tbaa !27
  %cmp137 = icmp sgt i32 %177, %178
  br i1 %cmp137, label %if.then139, label %if.else154

if.then139:                                       ; preds = %if.else136
  %179 = load i32, i32* %x, align 4, !tbaa !27
  %180 = load i32, i32* %maxc2, align 4, !tbaa !27
  %sub140 = sub nsw i32 %179, %180
  %181 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space141 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %181, i32 0, i32 11
  %182 = load i32, i32* %out_color_space141, align 4, !tbaa !54
  %arrayidx142 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %182
  %183 = load i32, i32* %arrayidx142, align 4, !tbaa !27
  %arrayidx143 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %183
  %184 = load i32, i32* %arrayidx143, align 4, !tbaa !27
  %mul144 = mul nsw i32 %sub140, %184
  store i32 %mul144, i32* %tdist, align 4, !tbaa !41
  %185 = load i32, i32* %tdist, align 4, !tbaa !41
  %186 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul145 = mul nsw i32 %185, %186
  %187 = load i32, i32* %min_dist, align 4, !tbaa !41
  %add146 = add nsw i32 %187, %mul145
  store i32 %add146, i32* %min_dist, align 4, !tbaa !41
  %188 = load i32, i32* %x, align 4, !tbaa !27
  %189 = load i32, i32* %minc2.addr, align 4, !tbaa !27
  %sub147 = sub nsw i32 %188, %189
  %190 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space148 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %190, i32 0, i32 11
  %191 = load i32, i32* %out_color_space148, align 4, !tbaa !54
  %arrayidx149 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %191
  %192 = load i32, i32* %arrayidx149, align 4, !tbaa !27
  %arrayidx150 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %192
  %193 = load i32, i32* %arrayidx150, align 4, !tbaa !27
  %mul151 = mul nsw i32 %sub147, %193
  store i32 %mul151, i32* %tdist, align 4, !tbaa !41
  %194 = load i32, i32* %tdist, align 4, !tbaa !41
  %195 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul152 = mul nsw i32 %194, %195
  %196 = load i32, i32* %max_dist, align 4, !tbaa !41
  %add153 = add nsw i32 %196, %mul152
  store i32 %add153, i32* %max_dist, align 4, !tbaa !41
  br label %if.end174

if.else154:                                       ; preds = %if.else136
  %197 = load i32, i32* %x, align 4, !tbaa !27
  %198 = load i32, i32* %centerc2, align 4, !tbaa !27
  %cmp155 = icmp sle i32 %197, %198
  br i1 %cmp155, label %if.then157, label %if.else165

if.then157:                                       ; preds = %if.else154
  %199 = load i32, i32* %x, align 4, !tbaa !27
  %200 = load i32, i32* %maxc2, align 4, !tbaa !27
  %sub158 = sub nsw i32 %199, %200
  %201 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space159 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %201, i32 0, i32 11
  %202 = load i32, i32* %out_color_space159, align 4, !tbaa !54
  %arrayidx160 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %202
  %203 = load i32, i32* %arrayidx160, align 4, !tbaa !27
  %arrayidx161 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %203
  %204 = load i32, i32* %arrayidx161, align 4, !tbaa !27
  %mul162 = mul nsw i32 %sub158, %204
  store i32 %mul162, i32* %tdist, align 4, !tbaa !41
  %205 = load i32, i32* %tdist, align 4, !tbaa !41
  %206 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul163 = mul nsw i32 %205, %206
  %207 = load i32, i32* %max_dist, align 4, !tbaa !41
  %add164 = add nsw i32 %207, %mul163
  store i32 %add164, i32* %max_dist, align 4, !tbaa !41
  br label %if.end173

if.else165:                                       ; preds = %if.else154
  %208 = load i32, i32* %x, align 4, !tbaa !27
  %209 = load i32, i32* %minc2.addr, align 4, !tbaa !27
  %sub166 = sub nsw i32 %208, %209
  %210 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space167 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %210, i32 0, i32 11
  %211 = load i32, i32* %out_color_space167, align 4, !tbaa !54
  %arrayidx168 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %211
  %212 = load i32, i32* %arrayidx168, align 4, !tbaa !27
  %arrayidx169 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %212
  %213 = load i32, i32* %arrayidx169, align 4, !tbaa !27
  %mul170 = mul nsw i32 %sub166, %213
  store i32 %mul170, i32* %tdist, align 4, !tbaa !41
  %214 = load i32, i32* %tdist, align 4, !tbaa !41
  %215 = load i32, i32* %tdist, align 4, !tbaa !41
  %mul171 = mul nsw i32 %214, %215
  %216 = load i32, i32* %max_dist, align 4, !tbaa !41
  %add172 = add nsw i32 %216, %mul171
  store i32 %add172, i32* %max_dist, align 4, !tbaa !41
  br label %if.end173

if.end173:                                        ; preds = %if.else165, %if.then157
  br label %if.end174

if.end174:                                        ; preds = %if.end173, %if.then139
  br label %if.end175

if.end175:                                        ; preds = %if.end174, %if.then121
  %217 = load i32, i32* %min_dist, align 4, !tbaa !41
  %218 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx176 = getelementptr inbounds [256 x i32], [256 x i32]* %mindist, i32 0, i32 %218
  store i32 %217, i32* %arrayidx176, align 4, !tbaa !41
  %219 = load i32, i32* %max_dist, align 4, !tbaa !41
  %220 = load i32, i32* %minmaxdist, align 4, !tbaa !41
  %cmp177 = icmp slt i32 %219, %220
  br i1 %cmp177, label %if.then179, label %if.end180

if.then179:                                       ; preds = %if.end175
  %221 = load i32, i32* %max_dist, align 4, !tbaa !41
  store i32 %221, i32* %minmaxdist, align 4, !tbaa !41
  br label %if.end180

if.end180:                                        ; preds = %if.then179, %if.end175
  br label %for.inc

for.inc:                                          ; preds = %if.end180
  %222 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %222, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %ncolors, align 4, !tbaa !27
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond181

for.cond181:                                      ; preds = %for.inc193, %for.end
  %223 = load i32, i32* %i, align 4, !tbaa !27
  %224 = load i32, i32* %numcolors, align 4, !tbaa !27
  %cmp182 = icmp slt i32 %223, %224
  br i1 %cmp182, label %for.body184, label %for.end195

for.body184:                                      ; preds = %for.cond181
  %225 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx185 = getelementptr inbounds [256 x i32], [256 x i32]* %mindist, i32 0, i32 %225
  %226 = load i32, i32* %arrayidx185, align 4, !tbaa !41
  %227 = load i32, i32* %minmaxdist, align 4, !tbaa !41
  %cmp186 = icmp sle i32 %226, %227
  br i1 %cmp186, label %if.then188, label %if.end192

if.then188:                                       ; preds = %for.body184
  %228 = load i32, i32* %i, align 4, !tbaa !27
  %conv189 = trunc i32 %228 to i8
  %229 = load i8*, i8** %colorlist.addr, align 4, !tbaa !2
  %230 = load i32, i32* %ncolors, align 4, !tbaa !27
  %inc190 = add nsw i32 %230, 1
  store i32 %inc190, i32* %ncolors, align 4, !tbaa !27
  %arrayidx191 = getelementptr inbounds i8, i8* %229, i32 %230
  store i8 %conv189, i8* %arrayidx191, align 1, !tbaa !32
  br label %if.end192

if.end192:                                        ; preds = %if.then188, %for.body184
  br label %for.inc193

for.inc193:                                       ; preds = %if.end192
  %231 = load i32, i32* %i, align 4, !tbaa !27
  %inc194 = add nsw i32 %231, 1
  store i32 %inc194, i32* %i, align 4, !tbaa !27
  br label %for.cond181

for.end195:                                       ; preds = %for.cond181
  %232 = load i32, i32* %ncolors, align 4, !tbaa !27
  %233 = bitcast [256 x i32]* %mindist to i8*
  call void @llvm.lifetime.end.p0i8(i64 1024, i8* %233) #3
  %234 = bitcast i32* %tdist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #3
  %235 = bitcast i32* %max_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #3
  %236 = bitcast i32* %min_dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #3
  %237 = bitcast i32* %minmaxdist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #3
  %238 = bitcast i32* %ncolors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #3
  %239 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #3
  %240 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #3
  %241 = bitcast i32* %centerc2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #3
  %242 = bitcast i32* %centerc1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #3
  %243 = bitcast i32* %centerc0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #3
  %244 = bitcast i32* %maxc2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #3
  %245 = bitcast i32* %maxc1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #3
  %246 = bitcast i32* %maxc0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #3
  %247 = bitcast i32* %numcolors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #3
  ret i32 %232
}

; Function Attrs: nounwind
define internal void @find_best_colors(%struct.jpeg_decompress_struct* %cinfo, i32 %minc0, i32 %minc1, i32 %minc2, i32 %numcolors, i8* %colorlist, i8* %bestcolor) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %minc0.addr = alloca i32, align 4
  %minc1.addr = alloca i32, align 4
  %minc2.addr = alloca i32, align 4
  %numcolors.addr = alloca i32, align 4
  %colorlist.addr = alloca i8*, align 4
  %bestcolor.addr = alloca i8*, align 4
  %ic0 = alloca i32, align 4
  %ic1 = alloca i32, align 4
  %ic2 = alloca i32, align 4
  %i = alloca i32, align 4
  %icolor = alloca i32, align 4
  %bptr = alloca i32*, align 4
  %cptr = alloca i8*, align 4
  %dist0 = alloca i32, align 4
  %dist1 = alloca i32, align 4
  %dist2 = alloca i32, align 4
  %xx0 = alloca i32, align 4
  %xx1 = alloca i32, align 4
  %xx2 = alloca i32, align 4
  %inc0 = alloca i32, align 4
  %inc1 = alloca i32, align 4
  %inc2 = alloca i32, align 4
  %bestdist = alloca [128 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %minc0, i32* %minc0.addr, align 4, !tbaa !27
  store i32 %minc1, i32* %minc1.addr, align 4, !tbaa !27
  store i32 %minc2, i32* %minc2.addr, align 4, !tbaa !27
  store i32 %numcolors, i32* %numcolors.addr, align 4, !tbaa !27
  store i8* %colorlist, i8** %colorlist.addr, align 4, !tbaa !2
  store i8* %bestcolor, i8** %bestcolor.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ic0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %ic1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %ic2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %icolor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32** %bptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i8** %cptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %dist0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %dist1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %dist2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %xx0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %xx1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %xx2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %inc0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %inc1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %inc2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast [128 x i32]* %bestdist to i8*
  call void @llvm.lifetime.start.p0i8(i64 512, i8* %16) #3
  %arraydecay = getelementptr inbounds [128 x i32], [128 x i32]* %bestdist, i32 0, i32 0
  store i32* %arraydecay, i32** %bptr, align 4, !tbaa !2
  store i32 127, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %17 = load i32, i32* %i, align 4, !tbaa !27
  %cmp = icmp sge i32 %17, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load i32*, i32** %bptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %18, i32 1
  store i32* %incdec.ptr, i32** %bptr, align 4, !tbaa !2
  store i32 2147483647, i32* %18, align 4, !tbaa !41
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4, !tbaa !27
  %dec = add nsw i32 %19, -1
  store i32 %dec, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc142, %for.end
  %20 = load i32, i32* %i, align 4, !tbaa !27
  %21 = load i32, i32* %numcolors.addr, align 4, !tbaa !27
  %cmp2 = icmp slt i32 %20, %21
  br i1 %cmp2, label %for.body3, label %for.end143

for.body3:                                        ; preds = %for.cond1
  %22 = load i8*, i8** %colorlist.addr, align 4, !tbaa !2
  %23 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds i8, i8* %22, i32 %23
  %24 = load i8, i8* %arrayidx, align 1, !tbaa !32
  %conv = zext i8 %24 to i32
  store i32 %conv, i32* %icolor, align 4, !tbaa !27
  %25 = load i32, i32* %minc0.addr, align 4, !tbaa !27
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 33
  %27 = load i8**, i8*** %colormap, align 8, !tbaa !44
  %arrayidx4 = getelementptr inbounds i8*, i8** %27, i32 0
  %28 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  %29 = load i32, i32* %icolor, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8, i8* %28, i32 %29
  %30 = load i8, i8* %arrayidx5, align 1, !tbaa !32
  %conv6 = zext i8 %30 to i32
  %sub = sub nsw i32 %25, %conv6
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 11
  %32 = load i32, i32* %out_color_space, align 4, !tbaa !54
  %arrayidx7 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %32
  %33 = load i32, i32* %arrayidx7, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %33
  %34 = load i32, i32* %arrayidx8, align 4, !tbaa !27
  %mul = mul nsw i32 %sub, %34
  store i32 %mul, i32* %inc0, align 4, !tbaa !41
  %35 = load i32, i32* %inc0, align 4, !tbaa !41
  %36 = load i32, i32* %inc0, align 4, !tbaa !41
  %mul9 = mul nsw i32 %35, %36
  store i32 %mul9, i32* %dist0, align 4, !tbaa !41
  %37 = load i32, i32* %minc1.addr, align 4, !tbaa !27
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 33
  %39 = load i8**, i8*** %colormap10, align 8, !tbaa !44
  %arrayidx11 = getelementptr inbounds i8*, i8** %39, i32 1
  %40 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  %41 = load i32, i32* %icolor, align 4, !tbaa !27
  %arrayidx12 = getelementptr inbounds i8, i8* %40, i32 %41
  %42 = load i8, i8* %arrayidx12, align 1, !tbaa !32
  %conv13 = zext i8 %42 to i32
  %sub14 = sub nsw i32 %37, %conv13
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 11
  %44 = load i32, i32* %out_color_space15, align 4, !tbaa !54
  %arrayidx16 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %44
  %45 = load i32, i32* %arrayidx16, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx17, align 4, !tbaa !27
  %mul18 = mul nsw i32 %sub14, %46
  store i32 %mul18, i32* %inc1, align 4, !tbaa !41
  %47 = load i32, i32* %inc1, align 4, !tbaa !41
  %48 = load i32, i32* %inc1, align 4, !tbaa !41
  %mul19 = mul nsw i32 %47, %48
  %49 = load i32, i32* %dist0, align 4, !tbaa !41
  %add = add nsw i32 %49, %mul19
  store i32 %add, i32* %dist0, align 4, !tbaa !41
  %50 = load i32, i32* %minc2.addr, align 4, !tbaa !27
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 33
  %52 = load i8**, i8*** %colormap20, align 8, !tbaa !44
  %arrayidx21 = getelementptr inbounds i8*, i8** %52, i32 2
  %53 = load i8*, i8** %arrayidx21, align 4, !tbaa !2
  %54 = load i32, i32* %icolor, align 4, !tbaa !27
  %arrayidx22 = getelementptr inbounds i8, i8* %53, i32 %54
  %55 = load i8, i8* %arrayidx22, align 1, !tbaa !32
  %conv23 = zext i8 %55 to i32
  %sub24 = sub nsw i32 %50, %conv23
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 11
  %57 = load i32, i32* %out_color_space25, align 4, !tbaa !54
  %arrayidx26 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %57
  %58 = load i32, i32* %arrayidx26, align 4, !tbaa !27
  %arrayidx27 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %58
  %59 = load i32, i32* %arrayidx27, align 4, !tbaa !27
  %mul28 = mul nsw i32 %sub24, %59
  store i32 %mul28, i32* %inc2, align 4, !tbaa !41
  %60 = load i32, i32* %inc2, align 4, !tbaa !41
  %61 = load i32, i32* %inc2, align 4, !tbaa !41
  %mul29 = mul nsw i32 %60, %61
  %62 = load i32, i32* %dist0, align 4, !tbaa !41
  %add30 = add nsw i32 %62, %mul29
  store i32 %add30, i32* %dist0, align 4, !tbaa !41
  %63 = load i32, i32* %inc0, align 4, !tbaa !41
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 11
  %65 = load i32, i32* %out_color_space31, align 4, !tbaa !54
  %arrayidx32 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %65
  %66 = load i32, i32* %arrayidx32, align 4, !tbaa !27
  %arrayidx33 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %66
  %67 = load i32, i32* %arrayidx33, align 4, !tbaa !27
  %mul34 = mul nsw i32 8, %67
  %mul35 = mul nsw i32 2, %mul34
  %mul36 = mul nsw i32 %63, %mul35
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space37 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %68, i32 0, i32 11
  %69 = load i32, i32* %out_color_space37, align 4, !tbaa !54
  %arrayidx38 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %69
  %70 = load i32, i32* %arrayidx38, align 4, !tbaa !27
  %arrayidx39 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %70
  %71 = load i32, i32* %arrayidx39, align 4, !tbaa !27
  %mul40 = mul nsw i32 8, %71
  %72 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space41 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %72, i32 0, i32 11
  %73 = load i32, i32* %out_color_space41, align 4, !tbaa !54
  %arrayidx42 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %73
  %74 = load i32, i32* %arrayidx42, align 4, !tbaa !27
  %arrayidx43 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %74
  %75 = load i32, i32* %arrayidx43, align 4, !tbaa !27
  %mul44 = mul nsw i32 8, %75
  %mul45 = mul nsw i32 %mul40, %mul44
  %add46 = add nsw i32 %mul36, %mul45
  store i32 %add46, i32* %inc0, align 4, !tbaa !41
  %76 = load i32, i32* %inc1, align 4, !tbaa !41
  %77 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space47 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %77, i32 0, i32 11
  %78 = load i32, i32* %out_color_space47, align 4, !tbaa !54
  %arrayidx48 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %78
  %79 = load i32, i32* %arrayidx48, align 4, !tbaa !27
  %arrayidx49 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %79
  %80 = load i32, i32* %arrayidx49, align 4, !tbaa !27
  %mul50 = mul nsw i32 4, %80
  %mul51 = mul nsw i32 2, %mul50
  %mul52 = mul nsw i32 %76, %mul51
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space53 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 11
  %82 = load i32, i32* %out_color_space53, align 4, !tbaa !54
  %arrayidx54 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %82
  %83 = load i32, i32* %arrayidx54, align 4, !tbaa !27
  %arrayidx55 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %83
  %84 = load i32, i32* %arrayidx55, align 4, !tbaa !27
  %mul56 = mul nsw i32 4, %84
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space57 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 11
  %86 = load i32, i32* %out_color_space57, align 4, !tbaa !54
  %arrayidx58 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %86
  %87 = load i32, i32* %arrayidx58, align 4, !tbaa !27
  %arrayidx59 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %87
  %88 = load i32, i32* %arrayidx59, align 4, !tbaa !27
  %mul60 = mul nsw i32 4, %88
  %mul61 = mul nsw i32 %mul56, %mul60
  %add62 = add nsw i32 %mul52, %mul61
  store i32 %add62, i32* %inc1, align 4, !tbaa !41
  %89 = load i32, i32* %inc2, align 4, !tbaa !41
  %90 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space63 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %90, i32 0, i32 11
  %91 = load i32, i32* %out_color_space63, align 4, !tbaa !54
  %arrayidx64 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %91
  %92 = load i32, i32* %arrayidx64, align 4, !tbaa !27
  %arrayidx65 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %92
  %93 = load i32, i32* %arrayidx65, align 4, !tbaa !27
  %mul66 = mul nsw i32 8, %93
  %mul67 = mul nsw i32 2, %mul66
  %mul68 = mul nsw i32 %89, %mul67
  %94 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space69 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %94, i32 0, i32 11
  %95 = load i32, i32* %out_color_space69, align 4, !tbaa !54
  %arrayidx70 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %95
  %96 = load i32, i32* %arrayidx70, align 4, !tbaa !27
  %arrayidx71 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %96
  %97 = load i32, i32* %arrayidx71, align 4, !tbaa !27
  %mul72 = mul nsw i32 8, %97
  %98 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space73 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %98, i32 0, i32 11
  %99 = load i32, i32* %out_color_space73, align 4, !tbaa !54
  %arrayidx74 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %99
  %100 = load i32, i32* %arrayidx74, align 4, !tbaa !27
  %arrayidx75 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %100
  %101 = load i32, i32* %arrayidx75, align 4, !tbaa !27
  %mul76 = mul nsw i32 8, %101
  %mul77 = mul nsw i32 %mul72, %mul76
  %add78 = add nsw i32 %mul68, %mul77
  store i32 %add78, i32* %inc2, align 4, !tbaa !41
  %arraydecay79 = getelementptr inbounds [128 x i32], [128 x i32]* %bestdist, i32 0, i32 0
  store i32* %arraydecay79, i32** %bptr, align 4, !tbaa !2
  %102 = load i8*, i8** %bestcolor.addr, align 4, !tbaa !2
  store i8* %102, i8** %cptr, align 4, !tbaa !2
  %103 = load i32, i32* %inc0, align 4, !tbaa !41
  store i32 %103, i32* %xx0, align 4, !tbaa !41
  store i32 3, i32* %ic0, align 4, !tbaa !27
  br label %for.cond80

for.cond80:                                       ; preds = %for.inc139, %for.body3
  %104 = load i32, i32* %ic0, align 4, !tbaa !27
  %cmp81 = icmp sge i32 %104, 0
  br i1 %cmp81, label %for.body83, label %for.end141

for.body83:                                       ; preds = %for.cond80
  %105 = load i32, i32* %dist0, align 4, !tbaa !41
  store i32 %105, i32* %dist1, align 4, !tbaa !41
  %106 = load i32, i32* %inc1, align 4, !tbaa !41
  store i32 %106, i32* %xx1, align 4, !tbaa !41
  store i32 7, i32* %ic1, align 4, !tbaa !27
  br label %for.cond84

for.cond84:                                       ; preds = %for.inc124, %for.body83
  %107 = load i32, i32* %ic1, align 4, !tbaa !27
  %cmp85 = icmp sge i32 %107, 0
  br i1 %cmp85, label %for.body87, label %for.end126

for.body87:                                       ; preds = %for.cond84
  %108 = load i32, i32* %dist1, align 4, !tbaa !41
  store i32 %108, i32* %dist2, align 4, !tbaa !41
  %109 = load i32, i32* %inc2, align 4, !tbaa !41
  store i32 %109, i32* %xx2, align 4, !tbaa !41
  store i32 3, i32* %ic2, align 4, !tbaa !27
  br label %for.cond88

for.cond88:                                       ; preds = %for.inc109, %for.body87
  %110 = load i32, i32* %ic2, align 4, !tbaa !27
  %cmp89 = icmp sge i32 %110, 0
  br i1 %cmp89, label %for.body91, label %for.end111

for.body91:                                       ; preds = %for.cond88
  %111 = load i32, i32* %dist2, align 4, !tbaa !41
  %112 = load i32*, i32** %bptr, align 4, !tbaa !2
  %113 = load i32, i32* %112, align 4, !tbaa !41
  %cmp92 = icmp slt i32 %111, %113
  br i1 %cmp92, label %if.then, label %if.end

if.then:                                          ; preds = %for.body91
  %114 = load i32, i32* %dist2, align 4, !tbaa !41
  %115 = load i32*, i32** %bptr, align 4, !tbaa !2
  store i32 %114, i32* %115, align 4, !tbaa !41
  %116 = load i32, i32* %icolor, align 4, !tbaa !27
  %conv94 = trunc i32 %116 to i8
  %117 = load i8*, i8** %cptr, align 4, !tbaa !2
  store i8 %conv94, i8* %117, align 1, !tbaa !32
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body91
  %118 = load i32, i32* %xx2, align 4, !tbaa !41
  %119 = load i32, i32* %dist2, align 4, !tbaa !41
  %add95 = add nsw i32 %119, %118
  store i32 %add95, i32* %dist2, align 4, !tbaa !41
  %120 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space96 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %120, i32 0, i32 11
  %121 = load i32, i32* %out_color_space96, align 4, !tbaa !54
  %arrayidx97 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %121
  %122 = load i32, i32* %arrayidx97, align 4, !tbaa !27
  %arrayidx98 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %122
  %123 = load i32, i32* %arrayidx98, align 4, !tbaa !27
  %mul99 = mul nsw i32 8, %123
  %mul100 = mul nsw i32 2, %mul99
  %124 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space101 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %124, i32 0, i32 11
  %125 = load i32, i32* %out_color_space101, align 4, !tbaa !54
  %arrayidx102 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %125
  %126 = load i32, i32* %arrayidx102, align 4, !tbaa !27
  %arrayidx103 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %126
  %127 = load i32, i32* %arrayidx103, align 4, !tbaa !27
  %mul104 = mul nsw i32 8, %127
  %mul105 = mul nsw i32 %mul100, %mul104
  %128 = load i32, i32* %xx2, align 4, !tbaa !41
  %add106 = add nsw i32 %128, %mul105
  store i32 %add106, i32* %xx2, align 4, !tbaa !41
  %129 = load i32*, i32** %bptr, align 4, !tbaa !2
  %incdec.ptr107 = getelementptr inbounds i32, i32* %129, i32 1
  store i32* %incdec.ptr107, i32** %bptr, align 4, !tbaa !2
  %130 = load i8*, i8** %cptr, align 4, !tbaa !2
  %incdec.ptr108 = getelementptr inbounds i8, i8* %130, i32 1
  store i8* %incdec.ptr108, i8** %cptr, align 4, !tbaa !2
  br label %for.inc109

for.inc109:                                       ; preds = %if.end
  %131 = load i32, i32* %ic2, align 4, !tbaa !27
  %dec110 = add nsw i32 %131, -1
  store i32 %dec110, i32* %ic2, align 4, !tbaa !27
  br label %for.cond88

for.end111:                                       ; preds = %for.cond88
  %132 = load i32, i32* %xx1, align 4, !tbaa !41
  %133 = load i32, i32* %dist1, align 4, !tbaa !41
  %add112 = add nsw i32 %133, %132
  store i32 %add112, i32* %dist1, align 4, !tbaa !41
  %134 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space113 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %134, i32 0, i32 11
  %135 = load i32, i32* %out_color_space113, align 4, !tbaa !54
  %arrayidx114 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %135
  %136 = load i32, i32* %arrayidx114, align 4, !tbaa !27
  %arrayidx115 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %136
  %137 = load i32, i32* %arrayidx115, align 4, !tbaa !27
  %mul116 = mul nsw i32 4, %137
  %mul117 = mul nsw i32 2, %mul116
  %138 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space118 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %138, i32 0, i32 11
  %139 = load i32, i32* %out_color_space118, align 4, !tbaa !54
  %arrayidx119 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %139
  %140 = load i32, i32* %arrayidx119, align 4, !tbaa !27
  %arrayidx120 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %140
  %141 = load i32, i32* %arrayidx120, align 4, !tbaa !27
  %mul121 = mul nsw i32 4, %141
  %mul122 = mul nsw i32 %mul117, %mul121
  %142 = load i32, i32* %xx1, align 4, !tbaa !41
  %add123 = add nsw i32 %142, %mul122
  store i32 %add123, i32* %xx1, align 4, !tbaa !41
  br label %for.inc124

for.inc124:                                       ; preds = %for.end111
  %143 = load i32, i32* %ic1, align 4, !tbaa !27
  %dec125 = add nsw i32 %143, -1
  store i32 %dec125, i32* %ic1, align 4, !tbaa !27
  br label %for.cond84

for.end126:                                       ; preds = %for.cond84
  %144 = load i32, i32* %xx0, align 4, !tbaa !41
  %145 = load i32, i32* %dist0, align 4, !tbaa !41
  %add127 = add nsw i32 %145, %144
  store i32 %add127, i32* %dist0, align 4, !tbaa !41
  %146 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space128 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %146, i32 0, i32 11
  %147 = load i32, i32* %out_color_space128, align 4, !tbaa !54
  %arrayidx129 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %147
  %148 = load i32, i32* %arrayidx129, align 4, !tbaa !27
  %arrayidx130 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %148
  %149 = load i32, i32* %arrayidx130, align 4, !tbaa !27
  %mul131 = mul nsw i32 8, %149
  %mul132 = mul nsw i32 2, %mul131
  %150 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space133 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %150, i32 0, i32 11
  %151 = load i32, i32* %out_color_space133, align 4, !tbaa !54
  %arrayidx134 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %151
  %152 = load i32, i32* %arrayidx134, align 4, !tbaa !27
  %arrayidx135 = getelementptr inbounds [3 x i32], [3 x i32]* @c_scales, i32 0, i32 %152
  %153 = load i32, i32* %arrayidx135, align 4, !tbaa !27
  %mul136 = mul nsw i32 8, %153
  %mul137 = mul nsw i32 %mul132, %mul136
  %154 = load i32, i32* %xx0, align 4, !tbaa !41
  %add138 = add nsw i32 %154, %mul137
  store i32 %add138, i32* %xx0, align 4, !tbaa !41
  br label %for.inc139

for.inc139:                                       ; preds = %for.end126
  %155 = load i32, i32* %ic0, align 4, !tbaa !27
  %dec140 = add nsw i32 %155, -1
  store i32 %dec140, i32* %ic0, align 4, !tbaa !27
  br label %for.cond80

for.end141:                                       ; preds = %for.cond80
  br label %for.inc142

for.inc142:                                       ; preds = %for.end141
  %156 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %156, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond1

for.end143:                                       ; preds = %for.cond1
  %157 = bitcast [128 x i32]* %bestdist to i8*
  call void @llvm.lifetime.end.p0i8(i64 512, i8* %157) #3
  %158 = bitcast i32* %inc2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #3
  %159 = bitcast i32* %inc1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #3
  %160 = bitcast i32* %inc0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #3
  %161 = bitcast i32* %xx2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #3
  %162 = bitcast i32* %xx1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #3
  %163 = bitcast i32* %xx0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #3
  %164 = bitcast i32* %dist2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #3
  %165 = bitcast i32* %dist1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #3
  %166 = bitcast i32* %dist0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #3
  %167 = bitcast i8** %cptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #3
  %168 = bitcast i32** %bptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #3
  %169 = bitcast i32* %icolor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #3
  %170 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #3
  %171 = bitcast i32* %ic2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #3
  %172 = bitcast i32* %ic1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #3
  %173 = bitcast i32* %ic0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #3
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 460}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !3, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !3, i64 32, !8, i64 36, !3, i64 40}
!17 = !{!"jpeg_color_quantizer", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!18 = !{!16, !3, i64 12}
!19 = !{!16, !3, i64 32}
!20 = !{!16, !3, i64 40}
!21 = !{!7, !8, i64 120}
!22 = !{!7, !3, i64 0}
!23 = !{!24, !8, i64 20}
!24 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!25 = !{!24, !3, i64 0}
!26 = !{!16, !3, i64 24}
!27 = !{!8, !8, i64 0}
!28 = !{!12, !3, i64 4}
!29 = !{!16, !8, i64 28}
!30 = !{!7, !8, i64 108}
!31 = !{!7, !8, i64 96}
!32 = !{!4, !4, i64 0}
!33 = !{!12, !3, i64 8}
!34 = !{!16, !3, i64 16}
!35 = !{!16, !8, i64 20}
!36 = !{!7, !4, i64 88}
!37 = !{!7, !8, i64 112}
!38 = !{!16, !3, i64 4}
!39 = !{!16, !3, i64 8}
!40 = !{!7, !8, i64 132}
!41 = !{!13, !13, i64 0}
!42 = !{!16, !8, i64 36}
!43 = !{!10, !10, i64 0}
!44 = !{!7, !3, i64 136}
!45 = !{!7, !3, i64 324}
!46 = !{!47, !8, i64 0}
!47 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !13, i64 24, !13, i64 28}
!48 = !{!47, !8, i64 4}
!49 = !{!47, !8, i64 8}
!50 = !{!47, !8, i64 12}
!51 = !{!47, !8, i64 16}
!52 = !{!47, !8, i64 20}
!53 = !{!24, !3, i64 4}
!54 = !{!7, !4, i64 44}
!55 = !{!47, !13, i64 24}
!56 = !{!47, !13, i64 28}
