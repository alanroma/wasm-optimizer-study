; ModuleID = 'jcmarker.c'
source_filename = "jcmarker.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_compress_struct*)*, {}* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { {}*, {}*, {}*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { {}*, {}*, {}*, {}*, {}*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { {}*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { {}*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { {}*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, {}* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.my_marker_writer = type { %struct.jpeg_marker_writer, i32 }

@jpeg_natural_order = external constant [0 x i32], align 4

; Function Attrs: nounwind
define hidden void @jinit_marker_writer(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %marker = alloca %struct.my_marker_writer*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_marker_writer** %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %2, i32 0, i32 0
  %3 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_compress_struct* %4 to %struct.jpeg_common_struct*
  %call = call i8* %3(%struct.jpeg_common_struct* %5, i32 1, i32 32)
  %6 = bitcast i8* %call to %struct.my_marker_writer*
  store %struct.my_marker_writer* %6, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %7 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %8 = bitcast %struct.my_marker_writer* %7 to %struct.jpeg_marker_writer*
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 58
  store %struct.jpeg_marker_writer* %8, %struct.jpeg_marker_writer** %marker1, align 4, !tbaa !14
  %10 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %10, i32 0, i32 0
  %write_file_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %pub, i32 0, i32 0
  %write_file_header2 = bitcast {}** %write_file_header to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @write_file_header, void (%struct.jpeg_compress_struct*)** %write_file_header2, align 4, !tbaa !15
  %11 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %11, i32 0, i32 0
  %write_frame_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %pub3, i32 0, i32 1
  %write_frame_header4 = bitcast {}** %write_frame_header to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @write_frame_header, void (%struct.jpeg_compress_struct*)** %write_frame_header4, align 4, !tbaa !18
  %12 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %12, i32 0, i32 0
  %write_scan_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %pub5, i32 0, i32 2
  %write_scan_header6 = bitcast {}** %write_scan_header to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @write_scan_header, void (%struct.jpeg_compress_struct*)** %write_scan_header6, align 4, !tbaa !19
  %13 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %pub7 = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %13, i32 0, i32 0
  %write_file_trailer = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %pub7, i32 0, i32 3
  %write_file_trailer8 = bitcast {}** %write_file_trailer to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @write_file_trailer, void (%struct.jpeg_compress_struct*)** %write_file_trailer8, align 4, !tbaa !20
  %14 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %pub9 = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %14, i32 0, i32 0
  %write_tables_only = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %pub9, i32 0, i32 4
  %write_tables_only10 = bitcast {}** %write_tables_only to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @write_tables_only, void (%struct.jpeg_compress_struct*)** %write_tables_only10, align 4, !tbaa !21
  %15 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %pub11 = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %15, i32 0, i32 0
  %write_marker_header = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %pub11, i32 0, i32 5
  store void (%struct.jpeg_compress_struct*, i32, i32)* @write_marker_header, void (%struct.jpeg_compress_struct*, i32, i32)** %write_marker_header, align 4, !tbaa !22
  %16 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %pub12 = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %16, i32 0, i32 0
  %write_marker_byte = getelementptr inbounds %struct.jpeg_marker_writer, %struct.jpeg_marker_writer* %pub12, i32 0, i32 6
  store void (%struct.jpeg_compress_struct*, i32)* @write_marker_byte, void (%struct.jpeg_compress_struct*, i32)** %write_marker_byte, align 4, !tbaa !23
  %17 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %last_restart_interval = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %17, i32 0, i32 1
  store i32 0, i32* %last_restart_interval, align 4, !tbaa !24
  %18 = bitcast %struct.my_marker_writer** %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @write_file_header(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %marker = alloca %struct.my_marker_writer*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_marker_writer** %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 58
  %2 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_marker_writer* %2 to %struct.my_marker_writer*
  store %struct.my_marker_writer* %3, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %4, i32 216)
  %5 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %last_restart_interval = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %5, i32 0, i32 1
  store i32 0, i32* %last_restart_interval, align 4, !tbaa !24
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_JFIF_header = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 32
  %7 = load i32, i32* %write_JFIF_header, align 8, !tbaa !25
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_jfif_app0(%struct.jpeg_compress_struct* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %write_Adobe_marker = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 38
  %10 = load i32, i32* %write_Adobe_marker, align 4, !tbaa !26
  %tobool2 = icmp ne i32 %10, 0
  br i1 %tobool2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_adobe_app14(%struct.jpeg_compress_struct* %11)
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  %12 = bitcast %struct.my_marker_writer** %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #3
  ret void
}

; Function Attrs: nounwind
define internal void @write_frame_header(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %ci = alloca i32, align 4
  %prec = alloca i32, align 4
  %is_baseline = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %prec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %is_baseline to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @emit_multi_dqt(%struct.jpeg_compress_struct* %4)
  store i32 %call, i32* %prec, align 4, !tbaa !27
  %5 = load i32, i32* %prec, align 4, !tbaa !27
  %cmp = icmp eq i32 %5, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %prec, align 4, !tbaa !27
  store i32 0, i32* %ci, align 4, !tbaa !27
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 15
  %7 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !28
  store %struct.jpeg_component_info* %7, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %ci, align 4, !tbaa !27
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 13
  %10 = load i32, i32* %num_components, align 4, !tbaa !29
  %cmp1 = icmp slt i32 %8, %10
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %12, i32 0, i32 4
  %13 = load i32, i32* %quant_tbl_no, align 4, !tbaa !30
  %call2 = call i32 @emit_dqt(%struct.jpeg_compress_struct* %11, i32 %13)
  %14 = load i32, i32* %prec, align 4, !tbaa !27
  %add = add nsw i32 %14, %call2
  store i32 %add, i32* %prec, align 4, !tbaa !27
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %ci, align 4, !tbaa !27
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !27
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 25
  %18 = load i32, i32* %arith_code, align 4, !tbaa !32
  %tobool = icmp ne i32 %18, 0
  br i1 %tobool, label %if.then6, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 40
  %20 = load i32, i32* %progressive_mode, align 4, !tbaa !33
  %tobool3 = icmp ne i32 %20, 0
  br i1 %tobool3, label %if.then6, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 12
  %22 = load i32, i32* %data_precision, align 8, !tbaa !34
  %cmp5 = icmp ne i32 %22, 8
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %lor.lhs.false4, %lor.lhs.false, %if.end
  store i32 0, i32* %is_baseline, align 4, !tbaa !27
  br label %if.end26

if.else:                                          ; preds = %lor.lhs.false4
  store i32 1, i32* %is_baseline, align 4, !tbaa !27
  store i32 0, i32* %ci, align 4, !tbaa !27
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 15
  %24 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info7, align 4, !tbaa !28
  store %struct.jpeg_component_info* %24, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc17, %if.else
  %25 = load i32, i32* %ci, align 4, !tbaa !27
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 13
  %27 = load i32, i32* %num_components9, align 4, !tbaa !29
  %cmp10 = icmp slt i32 %25, %27
  br i1 %cmp10, label %for.body11, label %for.end20

for.body11:                                       ; preds = %for.cond8
  %28 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %28, i32 0, i32 5
  %29 = load i32, i32* %dc_tbl_no, align 4, !tbaa !35
  %cmp12 = icmp sgt i32 %29, 1
  br i1 %cmp12, label %if.then15, label %lor.lhs.false13

lor.lhs.false13:                                  ; preds = %for.body11
  %30 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %30, i32 0, i32 6
  %31 = load i32, i32* %ac_tbl_no, align 4, !tbaa !36
  %cmp14 = icmp sgt i32 %31, 1
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %lor.lhs.false13, %for.body11
  store i32 0, i32* %is_baseline, align 4, !tbaa !27
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %lor.lhs.false13
  br label %for.inc17

for.inc17:                                        ; preds = %if.end16
  %32 = load i32, i32* %ci, align 4, !tbaa !27
  %inc18 = add nsw i32 %32, 1
  store i32 %inc18, i32* %ci, align 4, !tbaa !27
  %33 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr19 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %33, i32 1
  store %struct.jpeg_component_info* %incdec.ptr19, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond8

for.end20:                                        ; preds = %for.cond8
  %34 = load i32, i32* %prec, align 4, !tbaa !27
  %tobool21 = icmp ne i32 %34, 0
  br i1 %tobool21, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %for.end20
  %35 = load i32, i32* %is_baseline, align 4, !tbaa !27
  %tobool22 = icmp ne i32 %35, 0
  br i1 %tobool22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %land.lhs.true
  store i32 0, i32* %is_baseline, align 4, !tbaa !27
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !37
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 5
  store i32 75, i32* %msg_code, align 4, !tbaa !38
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err24 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err24, align 8, !tbaa !37
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 1
  %40 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !40
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %42 = bitcast %struct.jpeg_compress_struct* %41 to %struct.jpeg_common_struct*
  call void %40(%struct.jpeg_common_struct* %42, i32 0)
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %land.lhs.true, %for.end20
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then6
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code27 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 25
  %44 = load i32, i32* %arith_code27, align 4, !tbaa !32
  %tobool28 = icmp ne i32 %44, 0
  br i1 %tobool28, label %if.then29, label %if.else35

if.then29:                                        ; preds = %if.end26
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode30 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %45, i32 0, i32 40
  %46 = load i32, i32* %progressive_mode30, align 4, !tbaa !33
  %tobool31 = icmp ne i32 %46, 0
  br i1 %tobool31, label %if.then32, label %if.else33

if.then32:                                        ; preds = %if.then29
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_sof(%struct.jpeg_compress_struct* %47, i32 202)
  br label %if.end34

if.else33:                                        ; preds = %if.then29
  %48 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_sof(%struct.jpeg_compress_struct* %48, i32 201)
  br label %if.end34

if.end34:                                         ; preds = %if.else33, %if.then32
  br label %if.end45

if.else35:                                        ; preds = %if.end26
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode36 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %49, i32 0, i32 40
  %50 = load i32, i32* %progressive_mode36, align 4, !tbaa !33
  %tobool37 = icmp ne i32 %50, 0
  br i1 %tobool37, label %if.then38, label %if.else39

if.then38:                                        ; preds = %if.else35
  %51 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_sof(%struct.jpeg_compress_struct* %51, i32 194)
  br label %if.end44

if.else39:                                        ; preds = %if.else35
  %52 = load i32, i32* %is_baseline, align 4, !tbaa !27
  %tobool40 = icmp ne i32 %52, 0
  br i1 %tobool40, label %if.then41, label %if.else42

if.then41:                                        ; preds = %if.else39
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_sof(%struct.jpeg_compress_struct* %53, i32 192)
  br label %if.end43

if.else42:                                        ; preds = %if.else39
  %54 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_sof(%struct.jpeg_compress_struct* %54, i32 193)
  br label %if.end43

if.end43:                                         ; preds = %if.else42, %if.then41
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %if.then38
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.end34
  %55 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %is_baseline to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i32* %prec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  ret void
}

; Function Attrs: nounwind
define internal void @write_scan_header(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %marker = alloca %struct.my_marker_writer*, align 4
  %i = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_marker_writer** %marker to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 58
  %2 = load %struct.jpeg_marker_writer*, %struct.jpeg_marker_writer** %marker1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_marker_writer* %2 to %struct.my_marker_writer*
  store %struct.my_marker_writer* %3, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 25
  %7 = load i32, i32* %arith_code, align 4, !tbaa !32
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_dac(%struct.jpeg_compress_struct* %8)
  br label %if.end11

if.else:                                          ; preds = %entry
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @emit_multi_dht(%struct.jpeg_compress_struct* %9)
  %tobool2 = icmp ne i32 %call, 0
  br i1 %tobool2, label %if.end10, label %if.then3

if.then3:                                         ; preds = %if.else
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %10 = load i32, i32* %i, align 4, !tbaa !27
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 44
  %12 = load i32, i32* %comps_in_scan, align 4, !tbaa !41
  %cmp = icmp slt i32 %10, %12
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 45
  %14 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %14
  %15 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %15, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 50
  %17 = load i32, i32* %Ss, align 4, !tbaa !42
  %cmp4 = icmp eq i32 %17, 0
  br i1 %cmp4, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 52
  %19 = load i32, i32* %Ah, align 4, !tbaa !43
  %cmp5 = icmp eq i32 %19, 0
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %land.lhs.true
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %21, i32 0, i32 5
  %22 = load i32, i32* %dc_tbl_no, align 4, !tbaa !35
  call void @emit_dht(%struct.jpeg_compress_struct* %20, i32 %22, i32 0)
  br label %if.end

if.end:                                           ; preds = %if.then6, %land.lhs.true, %for.body
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 51
  %24 = load i32, i32* %Se, align 8, !tbaa !44
  %tobool7 = icmp ne i32 %24, 0
  br i1 %tobool7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %26, i32 0, i32 6
  %27 = load i32, i32* %ac_tbl_no, align 4, !tbaa !36
  call void @emit_dht(%struct.jpeg_compress_struct* %25, i32 %27, i32 1)
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end9
  %28 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end10

if.end10:                                         ; preds = %for.end, %if.else
  br label %if.end11

if.end11:                                         ; preds = %if.end10, %if.then
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 30
  %30 = load i32, i32* %restart_interval, align 8, !tbaa !45
  %31 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %last_restart_interval = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %31, i32 0, i32 1
  %32 = load i32, i32* %last_restart_interval, align 4, !tbaa !24
  %cmp12 = icmp ne i32 %30, %32
  br i1 %cmp12, label %if.then13, label %if.end16

if.then13:                                        ; preds = %if.end11
  %33 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_dri(%struct.jpeg_compress_struct* %33)
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 30
  %35 = load i32, i32* %restart_interval14, align 8, !tbaa !45
  %36 = load %struct.my_marker_writer*, %struct.my_marker_writer** %marker, align 4, !tbaa !2
  %last_restart_interval15 = getelementptr inbounds %struct.my_marker_writer, %struct.my_marker_writer* %36, i32 0, i32 1
  store i32 %35, i32* %last_restart_interval15, align 4, !tbaa !24
  br label %if.end16

if.end16:                                         ; preds = %if.then13, %if.end11
  %37 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_sos(%struct.jpeg_compress_struct* %37)
  %38 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  %39 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  %40 = bitcast %struct.my_marker_writer** %marker to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  ret void
}

; Function Attrs: nounwind
define internal void @write_file_trailer(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %0, i32 217)
  ret void
}

; Function Attrs: nounwind
define internal void @write_tables_only(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %1, i32 216)
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !27
  %cmp = icmp slt i32 %2, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 16
  %4 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %4
  %5 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx, align 4, !tbaa !2
  %cmp1 = icmp ne %struct.JQUANT_TBL* %5, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !27
  %call = call i32 @emit_dqt(%struct.jpeg_compress_struct* %6, i32 %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 25
  %10 = load i32, i32* %arith_code, align 4, !tbaa !32
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %if.end17, label %if.then2

if.then2:                                         ; preds = %for.end
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc14, %if.then2
  %11 = load i32, i32* %i, align 4, !tbaa !27
  %cmp4 = icmp slt i32 %11, 4
  br i1 %cmp4, label %for.body5, label %for.end16

for.body5:                                        ; preds = %for.cond3
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 17
  %13 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %13
  %14 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx6, align 4, !tbaa !2
  %cmp7 = icmp ne %struct.JHUFF_TBL* %14, null
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %for.body5
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !27
  call void @emit_dht(%struct.jpeg_compress_struct* %15, i32 %16, i32 0)
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %for.body5
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 18
  %18 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx10 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %18
  %19 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx10, align 4, !tbaa !2
  %cmp11 = icmp ne %struct.JHUFF_TBL* %19, null
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end9
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !27
  call void @emit_dht(%struct.jpeg_compress_struct* %20, i32 %21, i32 1)
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %if.end9
  br label %for.inc14

for.inc14:                                        ; preds = %if.end13
  %22 = load i32, i32* %i, align 4, !tbaa !27
  %inc15 = add nsw i32 %22, 1
  store i32 %inc15, i32* %i, align 4, !tbaa !27
  br label %for.cond3

for.end16:                                        ; preds = %for.cond3
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %23, i32 217)
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #3
  ret void
}

; Function Attrs: nounwind
define internal void @write_marker_header(%struct.jpeg_compress_struct* %cinfo, i32 %marker, i32 %datalen) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %marker.addr = alloca i32, align 4
  %datalen.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %marker, i32* %marker.addr, align 4, !tbaa !27
  store i32 %datalen, i32* %datalen.addr, align 4, !tbaa !27
  %0 = load i32, i32* %datalen.addr, align 4, !tbaa !27
  %cmp = icmp ugt i32 %0, 65533
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 0
  %2 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !37
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %2, i32 0, i32 5
  store i32 11, i32* %msg_code, align 4, !tbaa !38
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !37
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 0
  %5 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !46
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_compress_struct* %6 to %struct.jpeg_common_struct*
  call void %5(%struct.jpeg_common_struct* %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = load i32, i32* %marker.addr, align 4, !tbaa !27
  call void @emit_marker(%struct.jpeg_compress_struct* %8, i32 %9)
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = load i32, i32* %datalen.addr, align 4, !tbaa !27
  %add = add i32 %11, 2
  call void @emit_2bytes(%struct.jpeg_compress_struct* %10, i32 %add)
  ret void
}

; Function Attrs: nounwind
define internal void @write_marker_byte(%struct.jpeg_compress_struct* %cinfo, i32 %val) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %val.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %val, i32* %val.addr, align 4, !tbaa !27
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i32, i32* %val.addr, align 4, !tbaa !27
  call void @emit_byte(%struct.jpeg_compress_struct* %0, i32 %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @emit_marker(%struct.jpeg_compress_struct* %cinfo, i32 %mark) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %mark.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %mark, i32* %mark.addr, align 4, !tbaa !47
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %0, i32 255)
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %2 = load i32, i32* %mark.addr, align 4, !tbaa !47
  call void @emit_byte(%struct.jpeg_compress_struct* %1, i32 %2)
  ret void
}

; Function Attrs: nounwind
define internal void @emit_jfif_app0(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %0, i32 224)
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_2bytes(%struct.jpeg_compress_struct* %1, i32 16)
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %2, i32 74)
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %3, i32 70)
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %4, i32 73)
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %5, i32 70)
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %6, i32 0)
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_major_version = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 33
  %9 = load i8, i8* %JFIF_major_version, align 4, !tbaa !48
  %conv = zext i8 %9 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %7, i32 %conv)
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %JFIF_minor_version = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 34
  %12 = load i8, i8* %JFIF_minor_version, align 1, !tbaa !49
  %conv1 = zext i8 %12 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %10, i32 %conv1)
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %density_unit = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 35
  %15 = load i8, i8* %density_unit, align 2, !tbaa !50
  %conv2 = zext i8 %15 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %13, i32 %conv2)
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %X_density = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 36
  %18 = load i16, i16* %X_density, align 8, !tbaa !51
  %conv3 = zext i16 %18 to i32
  call void @emit_2bytes(%struct.jpeg_compress_struct* %16, i32 %conv3)
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Y_density = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 37
  %21 = load i16, i16* %Y_density, align 2, !tbaa !52
  %conv4 = zext i16 %21 to i32
  call void @emit_2bytes(%struct.jpeg_compress_struct* %19, i32 %conv4)
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %22, i32 0)
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %23, i32 0)
  ret void
}

; Function Attrs: nounwind
define internal void @emit_adobe_app14(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %0, i32 238)
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_2bytes(%struct.jpeg_compress_struct* %1, i32 14)
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %2, i32 65)
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %3, i32 100)
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %4, i32 111)
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %5, i32 98)
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %6, i32 101)
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_2bytes(%struct.jpeg_compress_struct* %7, i32 100)
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_2bytes(%struct.jpeg_compress_struct* %8, i32 0)
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_2bytes(%struct.jpeg_compress_struct* %9, i32 0)
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 14
  %11 = load i32, i32* %jpeg_color_space, align 8, !tbaa !53
  switch i32 %11, label %sw.default [
    i32 3, label %sw.bb
    i32 5, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %12, i32 1)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %13, i32 2)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_byte(%struct.jpeg_compress_struct* %14, i32 0)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @emit_byte(%struct.jpeg_compress_struct* %cinfo, i32 %val) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %val.addr = alloca i32, align 4
  %dest = alloca %struct.jpeg_destination_mgr*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %val, i32* %val.addr, align 4, !tbaa !27
  %0 = bitcast %struct.jpeg_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest1, align 8, !tbaa !54
  store %struct.jpeg_destination_mgr* %2, %struct.jpeg_destination_mgr** %dest, align 4, !tbaa !2
  %3 = load i32, i32* %val.addr, align 4, !tbaa !27
  %conv = trunc i32 %3 to i8
  %4 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 4, !tbaa !2
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %4, i32 0, i32 0
  %5 = load i8*, i8** %next_output_byte, align 4, !tbaa !55
  %incdec.ptr = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %next_output_byte, align 4, !tbaa !55
  store i8 %conv, i8* %5, align 1, !tbaa !47
  %6 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 4, !tbaa !2
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %6, i32 0, i32 1
  %7 = load i32, i32* %free_in_buffer, align 4, !tbaa !57
  %dec = add i32 %7, -1
  store i32 %dec, i32* %free_in_buffer, align 4, !tbaa !57
  %cmp = icmp eq i32 %dec, 0
  br i1 %cmp, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %8 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 4, !tbaa !2
  %empty_output_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %8, i32 0, i32 3
  %9 = load i32 (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)** %empty_output_buffer, align 4, !tbaa !58
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %9(%struct.jpeg_compress_struct* %10)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !37
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 5
  store i32 24, i32* %msg_code, align 4, !tbaa !38
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !37
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 0
  %15 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !46
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = bitcast %struct.jpeg_compress_struct* %16 to %struct.jpeg_common_struct*
  call void %15(%struct.jpeg_common_struct* %17)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  %18 = bitcast %struct.jpeg_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #3
  ret void
}

; Function Attrs: nounwind
define internal void @emit_2bytes(%struct.jpeg_compress_struct* %cinfo, i32 %value) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %value.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %value, i32* %value.addr, align 4, !tbaa !27
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i32, i32* %value.addr, align 4, !tbaa !27
  %shr = ashr i32 %1, 8
  %and = and i32 %shr, 255
  call void @emit_byte(%struct.jpeg_compress_struct* %0, i32 %and)
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i32, i32* %value.addr, align 4, !tbaa !27
  %and1 = and i32 %3, 255
  call void @emit_byte(%struct.jpeg_compress_struct* %2, i32 %and1)
  ret void
}

; Function Attrs: nounwind
define internal i32 @emit_multi_dqt(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %prec = alloca [10 x i32], align 16
  %seen = alloca [10 x i32], align 16
  %fin_prec = alloca i32, align 4
  %ci = alloca i32, align 4
  %size = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %tbl_num = alloca i32, align 4
  %i = alloca i32, align 4
  %qtbl = alloca %struct.JQUANT_TBL*, align 4
  %tbl_num29 = alloca i32, align 4
  %tbl_num51 = alloca i32, align 4
  %i55 = alloca i32, align 4
  %qtbl56 = alloca %struct.JQUANT_TBL*, align 4
  %qval = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast [10 x i32]* %prec to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %0) #3
  %1 = bitcast [10 x i32]* %seen to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %1) #3
  %2 = bitcast [10 x i32]* %seen to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %2, i8 0, i32 40, i1 false)
  %3 = bitcast i32* %fin_prec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  store i32 0, i32* %fin_prec, align 4, !tbaa !27
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  store i32 0, i32* %size, align 4, !tbaa !27
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 54
  %7 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !59
  %compress_profile = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %7, i32 0, i32 16
  %8 = load i32, i32* %compress_profile, align 8, !tbaa !60
  %cmp = icmp eq i32 %8, 720002228
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup90

if.end:                                           ; preds = %entry
  store i32 0, i32* %ci, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %if.end
  %9 = load i32, i32* %ci, align 4, !tbaa !27
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 13
  %11 = load i32, i32* %num_components, align 4, !tbaa !29
  %cmp1 = icmp slt i32 %9, %11
  br i1 %cmp1, label %for.body, label %for.end23

for.body:                                         ; preds = %for.cond
  %12 = bitcast i32* %tbl_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 15
  %14 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !28
  %15 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %14, i32 %15
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx, i32 0, i32 4
  %16 = load i32, i32* %quant_tbl_no, align 4, !tbaa !30
  store i32 %16, i32* %tbl_num, align 4, !tbaa !27
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 16
  %20 = load i32, i32* %tbl_num, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %20
  %21 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx2, align 4, !tbaa !2
  store %struct.JQUANT_TBL* %21, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %22 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %cmp3 = icmp eq %struct.JQUANT_TBL* %22, null
  br i1 %cmp3, label %if.then5, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %23 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %23, i32 0, i32 1
  %24 = load i32, i32* %sent_table, align 4, !tbaa !63
  %cmp4 = icmp eq i32 %24, 1
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %lor.lhs.false, %for.body
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %lor.lhs.false
  %25 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds [10 x i32], [10 x i32]* %prec, i32 0, i32 %25
  store i32 0, i32* %arrayidx7, align 4, !tbaa !27
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.end6
  %26 = load i32, i32* %i, align 4, !tbaa !27
  %cmp9 = icmp slt i32 %26, 64
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %27 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds [10 x i32], [10 x i32]* %prec, i32 0, i32 %27
  %28 = load i32, i32* %arrayidx11, align 4, !tbaa !27
  %29 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %29, i32 0, i32 0
  %30 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx12 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 %30
  %31 = load i16, i16* %arrayidx12, align 2, !tbaa !65
  %conv = zext i16 %31 to i32
  %cmp13 = icmp sgt i32 %conv, 255
  %conv14 = zext i1 %cmp13 to i32
  %add = add nsw i32 %28, %conv14
  %tobool = icmp ne i32 %add, 0
  %lnot = xor i1 %tobool, true
  %lnot15 = xor i1 %lnot, true
  %lnot.ext = zext i1 %lnot15 to i32
  %32 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds [10 x i32], [10 x i32]* %prec, i32 0, i32 %32
  store i32 %lnot.ext, i32* %arrayidx16, align 4, !tbaa !27
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %33 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %34 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds [10 x i32], [10 x i32]* %prec, i32 0, i32 %34
  %35 = load i32, i32* %arrayidx17, align 4, !tbaa !27
  %36 = load i32, i32* %fin_prec, align 4, !tbaa !27
  %add18 = add nsw i32 %36, %35
  store i32 %add18, i32* %fin_prec, align 4, !tbaa !27
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then5
  %37 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #3
  %38 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  %39 = bitcast i32* %tbl_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup90 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc21

for.inc21:                                        ; preds = %cleanup.cont
  %40 = load i32, i32* %ci, align 4, !tbaa !27
  %inc22 = add nsw i32 %40, 1
  store i32 %inc22, i32* %ci, align 4, !tbaa !27
  br label %for.cond

for.end23:                                        ; preds = %for.cond
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %41, i32 219)
  store i32 0, i32* %ci, align 4, !tbaa !27
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc42, %for.end23
  %42 = load i32, i32* %ci, align 4, !tbaa !27
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 13
  %44 = load i32, i32* %num_components25, align 4, !tbaa !29
  %cmp26 = icmp slt i32 %42, %44
  br i1 %cmp26, label %for.body28, label %for.end44

for.body28:                                       ; preds = %for.cond24
  %45 = bitcast i32* %tbl_num29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #3
  %46 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info30 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %46, i32 0, i32 15
  %47 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info30, align 4, !tbaa !28
  %48 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx31 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %47, i32 %48
  %quant_tbl_no32 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx31, i32 0, i32 4
  %49 = load i32, i32* %quant_tbl_no32, align 4, !tbaa !30
  store i32 %49, i32* %tbl_num29, align 4, !tbaa !27
  %50 = load i32, i32* %tbl_num29, align 4, !tbaa !27
  %arrayidx33 = getelementptr inbounds [10 x i32], [10 x i32]* %seen, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx33, align 4, !tbaa !27
  %tobool34 = icmp ne i32 %51, 0
  br i1 %tobool34, label %if.end41, label %if.then35

if.then35:                                        ; preds = %for.body28
  %52 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx36 = getelementptr inbounds [10 x i32], [10 x i32]* %prec, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx36, align 4, !tbaa !27
  %add37 = add nsw i32 %53, 1
  %mul = mul nsw i32 64, %add37
  %add38 = add nsw i32 %mul, 1
  %54 = load i32, i32* %size, align 4, !tbaa !27
  %add39 = add nsw i32 %54, %add38
  store i32 %add39, i32* %size, align 4, !tbaa !27
  %55 = load i32, i32* %tbl_num29, align 4, !tbaa !27
  %arrayidx40 = getelementptr inbounds [10 x i32], [10 x i32]* %seen, i32 0, i32 %55
  store i32 1, i32* %arrayidx40, align 4, !tbaa !27
  br label %if.end41

if.end41:                                         ; preds = %if.then35, %for.body28
  %56 = bitcast i32* %tbl_num29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  br label %for.inc42

for.inc42:                                        ; preds = %if.end41
  %57 = load i32, i32* %ci, align 4, !tbaa !27
  %inc43 = add nsw i32 %57, 1
  store i32 %inc43, i32* %ci, align 4, !tbaa !27
  br label %for.cond24

for.end44:                                        ; preds = %for.cond24
  %58 = load i32, i32* %size, align 4, !tbaa !27
  %add45 = add nsw i32 %58, 2
  store i32 %add45, i32* %size, align 4, !tbaa !27
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %60 = load i32, i32* %size, align 4, !tbaa !27
  call void @emit_2bytes(%struct.jpeg_compress_struct* %59, i32 %60)
  store i32 0, i32* %ci, align 4, !tbaa !27
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc87, %for.end44
  %61 = load i32, i32* %ci, align 4, !tbaa !27
  %62 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components47 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %62, i32 0, i32 13
  %63 = load i32, i32* %num_components47, align 4, !tbaa !29
  %cmp48 = icmp slt i32 %61, %63
  br i1 %cmp48, label %for.body50, label %for.end89

for.body50:                                       ; preds = %for.cond46
  %64 = bitcast i32* %tbl_num51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #3
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info52 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %65, i32 0, i32 15
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info52, align 4, !tbaa !28
  %67 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx53 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 %67
  %quant_tbl_no54 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx53, i32 0, i32 4
  %68 = load i32, i32* %quant_tbl_no54, align 4, !tbaa !30
  store i32 %68, i32* %tbl_num51, align 4, !tbaa !27
  %69 = bitcast i32* %i55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #3
  %70 = bitcast %struct.JQUANT_TBL** %qtbl56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #3
  %71 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs57 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %71, i32 0, i32 16
  %72 = load i32, i32* %tbl_num51, align 4, !tbaa !27
  %arrayidx58 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs57, i32 0, i32 %72
  %73 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx58, align 4, !tbaa !2
  store %struct.JQUANT_TBL* %73, %struct.JQUANT_TBL** %qtbl56, align 4, !tbaa !2
  %74 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl56, align 4, !tbaa !2
  %sent_table59 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %74, i32 0, i32 1
  %75 = load i32, i32* %sent_table59, align 4, !tbaa !63
  %cmp60 = icmp eq i32 %75, 1
  br i1 %cmp60, label %if.then62, label %if.end63

if.then62:                                        ; preds = %for.body50
  store i32 13, i32* %cleanup.dest.slot, align 4
  br label %cleanup82

if.end63:                                         ; preds = %for.body50
  %76 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %77 = load i32, i32* %tbl_num51, align 4, !tbaa !27
  %78 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx64 = getelementptr inbounds [10 x i32], [10 x i32]* %prec, i32 0, i32 %78
  %79 = load i32, i32* %arrayidx64, align 4, !tbaa !27
  %shl = shl i32 %79, 4
  %add65 = add nsw i32 %77, %shl
  call void @emit_byte(%struct.jpeg_compress_struct* %76, i32 %add65)
  store i32 0, i32* %i55, align 4, !tbaa !27
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc78, %if.end63
  %80 = load i32, i32* %i55, align 4, !tbaa !27
  %cmp67 = icmp slt i32 %80, 64
  br i1 %cmp67, label %for.body69, label %for.end80

for.body69:                                       ; preds = %for.cond66
  %81 = bitcast i32* %qval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #3
  %82 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl56, align 4, !tbaa !2
  %quantval70 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %82, i32 0, i32 0
  %83 = load i32, i32* %i55, align 4, !tbaa !27
  %arrayidx71 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %83
  %84 = load i32, i32* %arrayidx71, align 4, !tbaa !27
  %arrayidx72 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval70, i32 0, i32 %84
  %85 = load i16, i16* %arrayidx72, align 2, !tbaa !65
  %conv73 = zext i16 %85 to i32
  store i32 %conv73, i32* %qval, align 4, !tbaa !27
  %86 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx74 = getelementptr inbounds [10 x i32], [10 x i32]* %prec, i32 0, i32 %86
  %87 = load i32, i32* %arrayidx74, align 4, !tbaa !27
  %tobool75 = icmp ne i32 %87, 0
  br i1 %tobool75, label %if.then76, label %if.end77

if.then76:                                        ; preds = %for.body69
  %88 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %89 = load i32, i32* %qval, align 4, !tbaa !27
  %shr = lshr i32 %89, 8
  call void @emit_byte(%struct.jpeg_compress_struct* %88, i32 %shr)
  br label %if.end77

if.end77:                                         ; preds = %if.then76, %for.body69
  %90 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %91 = load i32, i32* %qval, align 4, !tbaa !27
  %and = and i32 %91, 255
  call void @emit_byte(%struct.jpeg_compress_struct* %90, i32 %and)
  %92 = bitcast i32* %qval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #3
  br label %for.inc78

for.inc78:                                        ; preds = %if.end77
  %93 = load i32, i32* %i55, align 4, !tbaa !27
  %inc79 = add nsw i32 %93, 1
  store i32 %inc79, i32* %i55, align 4, !tbaa !27
  br label %for.cond66

for.end80:                                        ; preds = %for.cond66
  %94 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl56, align 4, !tbaa !2
  %sent_table81 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %94, i32 0, i32 1
  store i32 1, i32* %sent_table81, align 4, !tbaa !63
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup82

cleanup82:                                        ; preds = %for.end80, %if.then62
  %95 = bitcast %struct.JQUANT_TBL** %qtbl56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  %96 = bitcast i32* %i55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #3
  %97 = bitcast i32* %tbl_num51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #3
  %cleanup.dest85 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest85, label %unreachable [
    i32 0, label %cleanup.cont86
    i32 13, label %for.inc87
  ]

cleanup.cont86:                                   ; preds = %cleanup82
  br label %for.inc87

for.inc87:                                        ; preds = %cleanup.cont86, %cleanup82
  %98 = load i32, i32* %ci, align 4, !tbaa !27
  %inc88 = add nsw i32 %98, 1
  store i32 %inc88, i32* %ci, align 4, !tbaa !27
  br label %for.cond46

for.end89:                                        ; preds = %for.cond46
  %99 = load i32, i32* %fin_prec, align 4, !tbaa !27
  store i32 %99, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup90

cleanup90:                                        ; preds = %for.end89, %cleanup, %if.then
  %100 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #3
  %101 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #3
  %102 = bitcast i32* %fin_prec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #3
  %103 = bitcast [10 x i32]* %seen to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %103) #3
  %104 = bitcast [10 x i32]* %prec to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %104) #3
  %105 = load i32, i32* %retval, align 4
  ret i32 %105

unreachable:                                      ; preds = %cleanup82
  unreachable
}

; Function Attrs: nounwind
define internal i32 @emit_dqt(%struct.jpeg_compress_struct* %cinfo, i32 %index) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %index.addr = alloca i32, align 4
  %qtbl = alloca %struct.JQUANT_TBL*, align 4
  %prec = alloca i32, align 4
  %i = alloca i32, align 4
  %qval = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !27
  %0 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 16
  %2 = load i32, i32* %index.addr, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %2
  %3 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx, align 4, !tbaa !2
  store %struct.JQUANT_TBL* %3, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %4 = bitcast i32* %prec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %cmp = icmp eq %struct.JQUANT_TBL* %6, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !37
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 5
  store i32 52, i32* %msg_code, align 4, !tbaa !38
  %9 = load i32, i32* %index.addr, align 4, !tbaa !27
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !37
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 6
  %i2 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx3 = getelementptr inbounds [8 x i32], [8 x i32]* %i2, i32 0, i32 0
  store i32 %9, i32* %arrayidx3, align 4, !tbaa !47
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !37
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 0
  %14 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !46
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_compress_struct* %15 to %struct.jpeg_common_struct*
  call void %14(%struct.jpeg_common_struct* %16)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 0, i32* %prec, align 4, !tbaa !27
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %17 = load i32, i32* %i, align 4, !tbaa !27
  %cmp5 = icmp slt i32 %17, 64
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %18, i32 0, i32 0
  %19 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 %19
  %20 = load i16, i16* %arrayidx6, align 2, !tbaa !65
  %conv = zext i16 %20 to i32
  %cmp7 = icmp sgt i32 %conv, 255
  br i1 %cmp7, label %if.then9, label %if.end10

if.then9:                                         ; preds = %for.body
  store i32 1, i32* %prec, align 4, !tbaa !27
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end10
  %21 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %22, i32 0, i32 1
  %23 = load i32, i32* %sent_table, align 4, !tbaa !63
  %tobool = icmp ne i32 %23, 0
  br i1 %tobool, label %if.end28, label %if.then11

if.then11:                                        ; preds = %for.end
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %24, i32 219)
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %26 = load i32, i32* %prec, align 4, !tbaa !27
  %tobool12 = icmp ne i32 %26, 0
  %27 = zext i1 %tobool12 to i64
  %cond = select i1 %tobool12, i32 131, i32 67
  call void @emit_2bytes(%struct.jpeg_compress_struct* %25, i32 %cond)
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %29 = load i32, i32* %index.addr, align 4, !tbaa !27
  %30 = load i32, i32* %prec, align 4, !tbaa !27
  %shl = shl i32 %30, 4
  %add = add nsw i32 %29, %shl
  call void @emit_byte(%struct.jpeg_compress_struct* %28, i32 %add)
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc24, %if.then11
  %31 = load i32, i32* %i, align 4, !tbaa !27
  %cmp14 = icmp slt i32 %31, 64
  br i1 %cmp14, label %for.body16, label %for.end26

for.body16:                                       ; preds = %for.cond13
  %32 = bitcast i32* %qval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %quantval17 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %33, i32 0, i32 0
  %34 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx18 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %34
  %35 = load i32, i32* %arrayidx18, align 4, !tbaa !27
  %arrayidx19 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval17, i32 0, i32 %35
  %36 = load i16, i16* %arrayidx19, align 2, !tbaa !65
  %conv20 = zext i16 %36 to i32
  store i32 %conv20, i32* %qval, align 4, !tbaa !27
  %37 = load i32, i32* %prec, align 4, !tbaa !27
  %tobool21 = icmp ne i32 %37, 0
  br i1 %tobool21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %for.body16
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %39 = load i32, i32* %qval, align 4, !tbaa !27
  %shr = lshr i32 %39, 8
  call void @emit_byte(%struct.jpeg_compress_struct* %38, i32 %shr)
  br label %if.end23

if.end23:                                         ; preds = %if.then22, %for.body16
  %40 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %41 = load i32, i32* %qval, align 4, !tbaa !27
  %and = and i32 %41, 255
  call void @emit_byte(%struct.jpeg_compress_struct* %40, i32 %and)
  %42 = bitcast i32* %qval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  br label %for.inc24

for.inc24:                                        ; preds = %if.end23
  %43 = load i32, i32* %i, align 4, !tbaa !27
  %inc25 = add nsw i32 %43, 1
  store i32 %inc25, i32* %i, align 4, !tbaa !27
  br label %for.cond13

for.end26:                                        ; preds = %for.cond13
  %44 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %sent_table27 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %44, i32 0, i32 1
  store i32 1, i32* %sent_table27, align 4, !tbaa !63
  br label %if.end28

if.end28:                                         ; preds = %for.end26, %for.end
  %45 = load i32, i32* %prec, align 4, !tbaa !27
  %46 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i32* %prec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  ret i32 %45
}

; Function Attrs: nounwind
define internal void @emit_sof(%struct.jpeg_compress_struct* %cinfo, i32 %code) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %code.addr = alloca i32, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %code, i32* %code.addr, align 4, !tbaa !47
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i32, i32* %code.addr, align 4, !tbaa !47
  call void @emit_marker(%struct.jpeg_compress_struct* %2, i32 %3)
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 13
  %6 = load i32, i32* %num_components, align 4, !tbaa !29
  %mul = mul nsw i32 3, %6
  %add = add nsw i32 %mul, 2
  %add1 = add nsw i32 %add, 5
  %add2 = add nsw i32 %add1, 1
  call void @emit_2bytes(%struct.jpeg_compress_struct* %4, i32 %add2)
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 8
  %8 = load i32, i32* %image_height, align 8, !tbaa !66
  %cmp = icmp sgt i32 %8, 65535
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 7
  %10 = load i32, i32* %image_width, align 4, !tbaa !67
  %cmp3 = icmp sgt i32 %10, 65535
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !37
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 5
  store i32 41, i32* %msg_code, align 4, !tbaa !38
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !37
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 65535, i32* %arrayidx, align 4, !tbaa !47
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !37
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 0
  %17 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !46
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = bitcast %struct.jpeg_compress_struct* %18 to %struct.jpeg_common_struct*
  call void %17(%struct.jpeg_common_struct* %19)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %data_precision = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 12
  %22 = load i32, i32* %data_precision, align 8, !tbaa !34
  call void @emit_byte(%struct.jpeg_compress_struct* %20, i32 %22)
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 8
  %25 = load i32, i32* %image_height6, align 8, !tbaa !66
  call void @emit_2bytes(%struct.jpeg_compress_struct* %23, i32 %25)
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 7
  %28 = load i32, i32* %image_width7, align 4, !tbaa !67
  call void @emit_2bytes(%struct.jpeg_compress_struct* %26, i32 %28)
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 13
  %31 = load i32, i32* %num_components8, align 4, !tbaa !29
  call void @emit_byte(%struct.jpeg_compress_struct* %29, i32 %31)
  store i32 0, i32* %ci, align 4, !tbaa !27
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 15
  %33 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !28
  store %struct.jpeg_component_info* %33, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %34 = load i32, i32* %ci, align 4, !tbaa !27
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %35, i32 0, i32 13
  %36 = load i32, i32* %num_components9, align 4, !tbaa !29
  %cmp10 = icmp slt i32 %34, %36
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %37 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %38 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %38, i32 0, i32 0
  %39 = load i32, i32* %component_id, align 4, !tbaa !68
  call void @emit_byte(%struct.jpeg_compress_struct* %37, i32 %39)
  %40 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %41 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %41, i32 0, i32 2
  %42 = load i32, i32* %h_samp_factor, align 4, !tbaa !69
  %shl = shl i32 %42, 4
  %43 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %43, i32 0, i32 3
  %44 = load i32, i32* %v_samp_factor, align 4, !tbaa !70
  %add11 = add nsw i32 %shl, %44
  call void @emit_byte(%struct.jpeg_compress_struct* %40, i32 %add11)
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %46 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %46, i32 0, i32 4
  %47 = load i32, i32* %quant_tbl_no, align 4, !tbaa !30
  call void @emit_byte(%struct.jpeg_compress_struct* %45, i32 %47)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %ci, align 4, !tbaa !27
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !27
  %49 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %49, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %50 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: nounwind
define internal void @emit_dac(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal i32 @emit_multi_dht(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %length = alloca i32, align 4
  %dclens = alloca [4 x i32], align 16
  %aclens = alloca [4 x i32], align 16
  %dcseen = alloca [4 x %struct.JHUFF_TBL*], align 16
  %acseen = alloca [4 x %struct.JHUFF_TBL*], align 16
  %cleanup.dest.slot = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %dcidx = alloca i32, align 4
  %acidx = alloca i32, align 4
  %dctbl = alloca %struct.JHUFF_TBL*, align 4
  %actbl = alloca %struct.JHUFF_TBL*, align 4
  %seen = alloca i32, align 4
  %compptr109 = alloca %struct.jpeg_component_info*, align 4
  %dcidx112 = alloca i32, align 4
  %acidx114 = alloca i32, align 4
  %dctbl116 = alloca %struct.JHUFF_TBL*, align 4
  %actbl119 = alloca %struct.JHUFF_TBL*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  store i32 2, i32* %length, align 4, !tbaa !27
  %3 = bitcast [4 x i32]* %dclens to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #3
  %4 = bitcast [4 x i32]* %dclens to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %4, i8 0, i32 16, i1 false)
  %5 = bitcast [4 x i32]* %aclens to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #3
  %6 = bitcast [4 x i32]* %aclens to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %6, i8 0, i32 16, i1 false)
  %7 = bitcast [4 x %struct.JHUFF_TBL*]* %dcseen to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #3
  %8 = bitcast [4 x %struct.JHUFF_TBL*]* %dcseen to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %8, i8 0, i32 16, i1 false)
  %9 = bitcast [4 x %struct.JHUFF_TBL*]* %acseen to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #3
  %10 = bitcast [4 x %struct.JHUFF_TBL*]* %acseen to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %10, i8 0, i32 16, i1 false)
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 54
  %12 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !59
  %compress_profile = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %12, i32 0, i32 16
  %13 = load i32, i32* %compress_profile, align 8, !tbaa !60
  %cmp = icmp eq i32 %13, 720002228
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup188

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc97, %if.end
  %14 = load i32, i32* %i, align 4, !tbaa !27
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 44
  %16 = load i32, i32* %comps_in_scan, align 4, !tbaa !41
  %cmp1 = icmp slt i32 %14, %16
  br i1 %cmp1, label %for.body, label %for.end99

for.body:                                         ; preds = %for.cond
  %17 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 45
  %19 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %19
  %20 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %20, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %21 = bitcast i32* %dcidx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %22, i32 0, i32 5
  %23 = load i32, i32* %dc_tbl_no, align 4, !tbaa !35
  store i32 %23, i32* %dcidx, align 4, !tbaa !27
  %24 = bitcast i32* %acidx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %25, i32 0, i32 6
  %26 = load i32, i32* %ac_tbl_no, align 4, !tbaa !36
  store i32 %26, i32* %acidx, align 4, !tbaa !27
  %27 = bitcast %struct.JHUFF_TBL** %dctbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 17
  %29 = load i32, i32* %dcidx, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %29
  %30 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx2, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %30, %struct.JHUFF_TBL** %dctbl, align 4, !tbaa !2
  %31 = bitcast %struct.JHUFF_TBL** %actbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #3
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 18
  %33 = load i32, i32* %acidx, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %33
  %34 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx3, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %34, %struct.JHUFF_TBL** %actbl, align 4, !tbaa !2
  %35 = bitcast i32* %seen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #3
  store i32 0, i32* %seen, align 4, !tbaa !27
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 50
  %37 = load i32, i32* %Ss, align 4, !tbaa !42
  %cmp4 = icmp eq i32 %37, 0
  br i1 %cmp4, label %land.lhs.true, label %if.end40

land.lhs.true:                                    ; preds = %for.body
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 52
  %39 = load i32, i32* %Ah, align 4, !tbaa !43
  %cmp5 = icmp eq i32 %39, 0
  br i1 %cmp5, label %if.then6, label %if.end40

if.then6:                                         ; preds = %land.lhs.true
  %40 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl, align 4, !tbaa !2
  %cmp7 = icmp eq %struct.JHUFF_TBL* %40, null
  br i1 %cmp7, label %if.then8, label %if.end13

if.then8:                                         ; preds = %if.then6
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 0
  %42 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !37
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %42, i32 0, i32 5
  store i32 50, i32* %msg_code, align 4, !tbaa !38
  %43 = load i32, i32* %dcidx, align 4, !tbaa !27
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %44, i32 0, i32 0
  %45 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !37
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %45, i32 0, i32 6
  %i10 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx11 = getelementptr inbounds [8 x i32], [8 x i32]* %i10, i32 0, i32 0
  store i32 %43, i32* %arrayidx11, align 4, !tbaa !47
  %46 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %46, i32 0, i32 0
  %47 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !37
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %47, i32 0, i32 0
  %48 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !46
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %50 = bitcast %struct.jpeg_compress_struct* %49 to %struct.jpeg_common_struct*
  call void %48(%struct.jpeg_common_struct* %50)
  br label %if.end13

if.end13:                                         ; preds = %if.then8, %if.then6
  %51 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %51, i32 0, i32 2
  %52 = load i32, i32* %sent_table, align 4, !tbaa !71
  %tobool = icmp ne i32 %52, 0
  br i1 %tobool, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end13
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %if.end13
  store i32 0, i32* %j, align 4, !tbaa !27
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc, %if.end15
  %53 = load i32, i32* %j, align 4, !tbaa !27
  %cmp17 = icmp slt i32 %53, 4
  br i1 %cmp17, label %for.body18, label %for.end

for.body18:                                       ; preds = %for.cond16
  %54 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl, align 4, !tbaa !2
  %55 = load i32, i32* %j, align 4, !tbaa !27
  %arrayidx19 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dcseen, i32 0, i32 %55
  %56 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx19, align 4, !tbaa !2
  %cmp20 = icmp eq %struct.JHUFF_TBL* %54, %56
  %conv = zext i1 %cmp20 to i32
  %57 = load i32, i32* %seen, align 4, !tbaa !27
  %add = add nsw i32 %57, %conv
  store i32 %add, i32* %seen, align 4, !tbaa !27
  br label %for.inc

for.inc:                                          ; preds = %for.body18
  %58 = load i32, i32* %j, align 4, !tbaa !27
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %j, align 4, !tbaa !27
  br label %for.cond16

for.end:                                          ; preds = %for.cond16
  %59 = load i32, i32* %seen, align 4, !tbaa !27
  %tobool21 = icmp ne i32 %59, 0
  br i1 %tobool21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %for.end
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end23:                                         ; preds = %for.end
  %60 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl, align 4, !tbaa !2
  %61 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx24 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dcseen, i32 0, i32 %61
  store %struct.JHUFF_TBL* %60, %struct.JHUFF_TBL** %arrayidx24, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !27
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc33, %if.end23
  %62 = load i32, i32* %j, align 4, !tbaa !27
  %cmp26 = icmp sle i32 %62, 16
  br i1 %cmp26, label %for.body28, label %for.end35

for.body28:                                       ; preds = %for.cond25
  %63 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl, align 4, !tbaa !2
  %bits = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %63, i32 0, i32 0
  %64 = load i32, i32* %j, align 4, !tbaa !27
  %arrayidx29 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 %64
  %65 = load i8, i8* %arrayidx29, align 1, !tbaa !47
  %conv30 = zext i8 %65 to i32
  %66 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx31 = getelementptr inbounds [4 x i32], [4 x i32]* %dclens, i32 0, i32 %66
  %67 = load i32, i32* %arrayidx31, align 4, !tbaa !27
  %add32 = add nsw i32 %67, %conv30
  store i32 %add32, i32* %arrayidx31, align 4, !tbaa !27
  br label %for.inc33

for.inc33:                                        ; preds = %for.body28
  %68 = load i32, i32* %j, align 4, !tbaa !27
  %inc34 = add nsw i32 %68, 1
  store i32 %inc34, i32* %j, align 4, !tbaa !27
  br label %for.cond25

for.end35:                                        ; preds = %for.cond25
  %69 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx36 = getelementptr inbounds [4 x i32], [4 x i32]* %dclens, i32 0, i32 %69
  %70 = load i32, i32* %arrayidx36, align 4, !tbaa !27
  %add37 = add nsw i32 %70, 16
  %add38 = add nsw i32 %add37, 1
  %71 = load i32, i32* %length, align 4, !tbaa !27
  %add39 = add nsw i32 %71, %add38
  store i32 %add39, i32* %length, align 4, !tbaa !27
  br label %if.end40

if.end40:                                         ; preds = %for.end35, %land.lhs.true, %for.body
  %72 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %72, i32 0, i32 51
  %73 = load i32, i32* %Se, align 8, !tbaa !44
  %tobool41 = icmp ne i32 %73, 0
  br i1 %tobool41, label %if.then42, label %if.end91

if.then42:                                        ; preds = %if.end40
  %74 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl, align 4, !tbaa !2
  %cmp43 = icmp eq %struct.JHUFF_TBL* %74, null
  br i1 %cmp43, label %if.then45, label %if.end55

if.then45:                                        ; preds = %if.then42
  %75 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err46 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %75, i32 0, i32 0
  %76 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err46, align 8, !tbaa !37
  %msg_code47 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %76, i32 0, i32 5
  store i32 50, i32* %msg_code47, align 4, !tbaa !38
  %77 = load i32, i32* %acidx, align 4, !tbaa !27
  %add48 = add nsw i32 %77, 16
  %78 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err49 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %78, i32 0, i32 0
  %79 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err49, align 8, !tbaa !37
  %msg_parm50 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %79, i32 0, i32 6
  %i51 = bitcast %union.anon* %msg_parm50 to [8 x i32]*
  %arrayidx52 = getelementptr inbounds [8 x i32], [8 x i32]* %i51, i32 0, i32 0
  store i32 %add48, i32* %arrayidx52, align 4, !tbaa !47
  %80 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err53 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %80, i32 0, i32 0
  %81 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err53, align 8, !tbaa !37
  %error_exit54 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %81, i32 0, i32 0
  %82 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit54, align 4, !tbaa !46
  %83 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %84 = bitcast %struct.jpeg_compress_struct* %83 to %struct.jpeg_common_struct*
  call void %82(%struct.jpeg_common_struct* %84)
  br label %if.end55

if.end55:                                         ; preds = %if.then45, %if.then42
  %85 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl, align 4, !tbaa !2
  %sent_table56 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %85, i32 0, i32 2
  %86 = load i32, i32* %sent_table56, align 4, !tbaa !71
  %tobool57 = icmp ne i32 %86, 0
  br i1 %tobool57, label %if.then58, label %if.end59

if.then58:                                        ; preds = %if.end55
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end59:                                         ; preds = %if.end55
  store i32 0, i32* %seen, align 4, !tbaa !27
  store i32 0, i32* %j, align 4, !tbaa !27
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc68, %if.end59
  %87 = load i32, i32* %j, align 4, !tbaa !27
  %cmp61 = icmp slt i32 %87, 4
  br i1 %cmp61, label %for.body63, label %for.end70

for.body63:                                       ; preds = %for.cond60
  %88 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl, align 4, !tbaa !2
  %89 = load i32, i32* %j, align 4, !tbaa !27
  %arrayidx64 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %acseen, i32 0, i32 %89
  %90 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx64, align 4, !tbaa !2
  %cmp65 = icmp eq %struct.JHUFF_TBL* %88, %90
  %conv66 = zext i1 %cmp65 to i32
  %91 = load i32, i32* %seen, align 4, !tbaa !27
  %add67 = add nsw i32 %91, %conv66
  store i32 %add67, i32* %seen, align 4, !tbaa !27
  br label %for.inc68

for.inc68:                                        ; preds = %for.body63
  %92 = load i32, i32* %j, align 4, !tbaa !27
  %inc69 = add nsw i32 %92, 1
  store i32 %inc69, i32* %j, align 4, !tbaa !27
  br label %for.cond60

for.end70:                                        ; preds = %for.cond60
  %93 = load i32, i32* %seen, align 4, !tbaa !27
  %tobool71 = icmp ne i32 %93, 0
  br i1 %tobool71, label %if.then72, label %if.end73

if.then72:                                        ; preds = %for.end70
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end73:                                         ; preds = %for.end70
  %94 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl, align 4, !tbaa !2
  %95 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx74 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %acseen, i32 0, i32 %95
  store %struct.JHUFF_TBL* %94, %struct.JHUFF_TBL** %arrayidx74, align 4, !tbaa !2
  store i32 1, i32* %j, align 4, !tbaa !27
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc84, %if.end73
  %96 = load i32, i32* %j, align 4, !tbaa !27
  %cmp76 = icmp sle i32 %96, 16
  br i1 %cmp76, label %for.body78, label %for.end86

for.body78:                                       ; preds = %for.cond75
  %97 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl, align 4, !tbaa !2
  %bits79 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %97, i32 0, i32 0
  %98 = load i32, i32* %j, align 4, !tbaa !27
  %arrayidx80 = getelementptr inbounds [17 x i8], [17 x i8]* %bits79, i32 0, i32 %98
  %99 = load i8, i8* %arrayidx80, align 1, !tbaa !47
  %conv81 = zext i8 %99 to i32
  %100 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx82 = getelementptr inbounds [4 x i32], [4 x i32]* %aclens, i32 0, i32 %100
  %101 = load i32, i32* %arrayidx82, align 4, !tbaa !27
  %add83 = add nsw i32 %101, %conv81
  store i32 %add83, i32* %arrayidx82, align 4, !tbaa !27
  br label %for.inc84

for.inc84:                                        ; preds = %for.body78
  %102 = load i32, i32* %j, align 4, !tbaa !27
  %inc85 = add nsw i32 %102, 1
  store i32 %inc85, i32* %j, align 4, !tbaa !27
  br label %for.cond75

for.end86:                                        ; preds = %for.cond75
  %103 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx87 = getelementptr inbounds [4 x i32], [4 x i32]* %aclens, i32 0, i32 %103
  %104 = load i32, i32* %arrayidx87, align 4, !tbaa !27
  %add88 = add nsw i32 %104, 16
  %add89 = add nsw i32 %add88, 1
  %105 = load i32, i32* %length, align 4, !tbaa !27
  %add90 = add nsw i32 %105, %add89
  store i32 %add90, i32* %length, align 4, !tbaa !27
  br label %if.end91

if.end91:                                         ; preds = %for.end86, %if.end40
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end91, %if.then72, %if.then58, %if.then22, %if.then14
  %106 = bitcast i32* %seen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #3
  %107 = bitcast %struct.JHUFF_TBL** %actbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #3
  %108 = bitcast %struct.JHUFF_TBL** %dctbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #3
  %109 = bitcast i32* %acidx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast i32* %dcidx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.inc97
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc97

for.inc97:                                        ; preds = %cleanup.cont, %cleanup
  %112 = load i32, i32* %i, align 4, !tbaa !27
  %inc98 = add nsw i32 %112, 1
  store i32 %inc98, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end99:                                        ; preds = %for.cond
  %113 = load i32, i32* %length, align 4, !tbaa !27
  %cmp100 = icmp sgt i32 %113, 65535
  br i1 %cmp100, label %if.then102, label %if.end103

if.then102:                                       ; preds = %for.end99
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup188

if.end103:                                        ; preds = %for.end99
  %114 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %114, i32 196)
  %115 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %116 = load i32, i32* %length, align 4, !tbaa !27
  call void @emit_2bytes(%struct.jpeg_compress_struct* %115, i32 %116)
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond104

for.cond104:                                      ; preds = %for.inc185, %if.end103
  %117 = load i32, i32* %i, align 4, !tbaa !27
  %118 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan105 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %118, i32 0, i32 44
  %119 = load i32, i32* %comps_in_scan105, align 4, !tbaa !41
  %cmp106 = icmp slt i32 %117, %119
  br i1 %cmp106, label %for.body108, label %for.end187

for.body108:                                      ; preds = %for.cond104
  %120 = bitcast %struct.jpeg_component_info** %compptr109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #3
  %121 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info110 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %121, i32 0, i32 45
  %122 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx111 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info110, i32 0, i32 %122
  %123 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx111, align 4, !tbaa !2
  store %struct.jpeg_component_info* %123, %struct.jpeg_component_info** %compptr109, align 4, !tbaa !2
  %124 = bitcast i32* %dcidx112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #3
  %125 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr109, align 4, !tbaa !2
  %dc_tbl_no113 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %125, i32 0, i32 5
  %126 = load i32, i32* %dc_tbl_no113, align 4, !tbaa !35
  store i32 %126, i32* %dcidx112, align 4, !tbaa !27
  %127 = bitcast i32* %acidx114 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %127) #3
  %128 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr109, align 4, !tbaa !2
  %ac_tbl_no115 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %128, i32 0, i32 6
  %129 = load i32, i32* %ac_tbl_no115, align 4, !tbaa !36
  store i32 %129, i32* %acidx114, align 4, !tbaa !27
  %130 = bitcast %struct.JHUFF_TBL** %dctbl116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #3
  %131 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs117 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %131, i32 0, i32 17
  %132 = load i32, i32* %dcidx112, align 4, !tbaa !27
  %arrayidx118 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs117, i32 0, i32 %132
  %133 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx118, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %133, %struct.JHUFF_TBL** %dctbl116, align 4, !tbaa !2
  %134 = bitcast %struct.JHUFF_TBL** %actbl119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %134) #3
  %135 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs120 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %135, i32 0, i32 18
  %136 = load i32, i32* %acidx114, align 4, !tbaa !27
  %arrayidx121 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs120, i32 0, i32 %136
  %137 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx121, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %137, %struct.JHUFF_TBL** %actbl119, align 4, !tbaa !2
  %138 = load i32, i32* %acidx114, align 4, !tbaa !27
  %add122 = add nsw i32 %138, 16
  store i32 %add122, i32* %acidx114, align 4, !tbaa !27
  %139 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss123 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %139, i32 0, i32 50
  %140 = load i32, i32* %Ss123, align 4, !tbaa !42
  %cmp124 = icmp eq i32 %140, 0
  br i1 %cmp124, label %land.lhs.true126, label %if.end155

land.lhs.true126:                                 ; preds = %for.body108
  %141 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah127 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %141, i32 0, i32 52
  %142 = load i32, i32* %Ah127, align 4, !tbaa !43
  %cmp128 = icmp eq i32 %142, 0
  br i1 %cmp128, label %land.lhs.true130, label %if.end155

land.lhs.true130:                                 ; preds = %land.lhs.true126
  %143 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl116, align 4, !tbaa !2
  %sent_table131 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %143, i32 0, i32 2
  %144 = load i32, i32* %sent_table131, align 4, !tbaa !71
  %tobool132 = icmp ne i32 %144, 0
  br i1 %tobool132, label %if.end155, label %if.then133

if.then133:                                       ; preds = %land.lhs.true130
  %145 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %146 = load i32, i32* %dcidx112, align 4, !tbaa !27
  call void @emit_byte(%struct.jpeg_compress_struct* %145, i32 %146)
  store i32 1, i32* %j, align 4, !tbaa !27
  br label %for.cond134

for.cond134:                                      ; preds = %for.inc141, %if.then133
  %147 = load i32, i32* %j, align 4, !tbaa !27
  %cmp135 = icmp sle i32 %147, 16
  br i1 %cmp135, label %for.body137, label %for.end143

for.body137:                                      ; preds = %for.cond134
  %148 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %149 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl116, align 4, !tbaa !2
  %bits138 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %149, i32 0, i32 0
  %150 = load i32, i32* %j, align 4, !tbaa !27
  %arrayidx139 = getelementptr inbounds [17 x i8], [17 x i8]* %bits138, i32 0, i32 %150
  %151 = load i8, i8* %arrayidx139, align 1, !tbaa !47
  %conv140 = zext i8 %151 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %148, i32 %conv140)
  br label %for.inc141

for.inc141:                                       ; preds = %for.body137
  %152 = load i32, i32* %j, align 4, !tbaa !27
  %inc142 = add nsw i32 %152, 1
  store i32 %inc142, i32* %j, align 4, !tbaa !27
  br label %for.cond134

for.end143:                                       ; preds = %for.cond134
  store i32 0, i32* %j, align 4, !tbaa !27
  br label %for.cond144

for.cond144:                                      ; preds = %for.inc151, %for.end143
  %153 = load i32, i32* %j, align 4, !tbaa !27
  %154 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx145 = getelementptr inbounds [4 x i32], [4 x i32]* %dclens, i32 0, i32 %154
  %155 = load i32, i32* %arrayidx145, align 4, !tbaa !27
  %cmp146 = icmp slt i32 %153, %155
  br i1 %cmp146, label %for.body148, label %for.end153

for.body148:                                      ; preds = %for.cond144
  %156 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %157 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl116, align 4, !tbaa !2
  %huffval = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %157, i32 0, i32 1
  %158 = load i32, i32* %j, align 4, !tbaa !27
  %arrayidx149 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 %158
  %159 = load i8, i8* %arrayidx149, align 1, !tbaa !47
  %conv150 = zext i8 %159 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %156, i32 %conv150)
  br label %for.inc151

for.inc151:                                       ; preds = %for.body148
  %160 = load i32, i32* %j, align 4, !tbaa !27
  %inc152 = add nsw i32 %160, 1
  store i32 %inc152, i32* %j, align 4, !tbaa !27
  br label %for.cond144

for.end153:                                       ; preds = %for.cond144
  %161 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %dctbl116, align 4, !tbaa !2
  %sent_table154 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %161, i32 0, i32 2
  store i32 1, i32* %sent_table154, align 4, !tbaa !71
  br label %if.end155

if.end155:                                        ; preds = %for.end153, %land.lhs.true130, %land.lhs.true126, %for.body108
  %162 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se156 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %162, i32 0, i32 51
  %163 = load i32, i32* %Se156, align 8, !tbaa !44
  %tobool157 = icmp ne i32 %163, 0
  br i1 %tobool157, label %land.lhs.true158, label %if.end184

land.lhs.true158:                                 ; preds = %if.end155
  %164 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl119, align 4, !tbaa !2
  %sent_table159 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %164, i32 0, i32 2
  %165 = load i32, i32* %sent_table159, align 4, !tbaa !71
  %tobool160 = icmp ne i32 %165, 0
  br i1 %tobool160, label %if.end184, label %if.then161

if.then161:                                       ; preds = %land.lhs.true158
  %166 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %167 = load i32, i32* %acidx114, align 4, !tbaa !27
  call void @emit_byte(%struct.jpeg_compress_struct* %166, i32 %167)
  store i32 1, i32* %j, align 4, !tbaa !27
  br label %for.cond162

for.cond162:                                      ; preds = %for.inc169, %if.then161
  %168 = load i32, i32* %j, align 4, !tbaa !27
  %cmp163 = icmp sle i32 %168, 16
  br i1 %cmp163, label %for.body165, label %for.end171

for.body165:                                      ; preds = %for.cond162
  %169 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %170 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl119, align 4, !tbaa !2
  %bits166 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %170, i32 0, i32 0
  %171 = load i32, i32* %j, align 4, !tbaa !27
  %arrayidx167 = getelementptr inbounds [17 x i8], [17 x i8]* %bits166, i32 0, i32 %171
  %172 = load i8, i8* %arrayidx167, align 1, !tbaa !47
  %conv168 = zext i8 %172 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %169, i32 %conv168)
  br label %for.inc169

for.inc169:                                       ; preds = %for.body165
  %173 = load i32, i32* %j, align 4, !tbaa !27
  %inc170 = add nsw i32 %173, 1
  store i32 %inc170, i32* %j, align 4, !tbaa !27
  br label %for.cond162

for.end171:                                       ; preds = %for.cond162
  store i32 0, i32* %j, align 4, !tbaa !27
  br label %for.cond172

for.cond172:                                      ; preds = %for.inc180, %for.end171
  %174 = load i32, i32* %j, align 4, !tbaa !27
  %175 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx173 = getelementptr inbounds [4 x i32], [4 x i32]* %aclens, i32 0, i32 %175
  %176 = load i32, i32* %arrayidx173, align 4, !tbaa !27
  %cmp174 = icmp slt i32 %174, %176
  br i1 %cmp174, label %for.body176, label %for.end182

for.body176:                                      ; preds = %for.cond172
  %177 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %178 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl119, align 4, !tbaa !2
  %huffval177 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %178, i32 0, i32 1
  %179 = load i32, i32* %j, align 4, !tbaa !27
  %arrayidx178 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval177, i32 0, i32 %179
  %180 = load i8, i8* %arrayidx178, align 1, !tbaa !47
  %conv179 = zext i8 %180 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %177, i32 %conv179)
  br label %for.inc180

for.inc180:                                       ; preds = %for.body176
  %181 = load i32, i32* %j, align 4, !tbaa !27
  %inc181 = add nsw i32 %181, 1
  store i32 %inc181, i32* %j, align 4, !tbaa !27
  br label %for.cond172

for.end182:                                       ; preds = %for.cond172
  %182 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %actbl119, align 4, !tbaa !2
  %sent_table183 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %182, i32 0, i32 2
  store i32 1, i32* %sent_table183, align 4, !tbaa !71
  br label %if.end184

if.end184:                                        ; preds = %for.end182, %land.lhs.true158, %if.end155
  %183 = bitcast %struct.JHUFF_TBL** %actbl119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #3
  %184 = bitcast %struct.JHUFF_TBL** %dctbl116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #3
  %185 = bitcast i32* %acidx114 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #3
  %186 = bitcast i32* %dcidx112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #3
  %187 = bitcast %struct.jpeg_component_info** %compptr109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #3
  br label %for.inc185

for.inc185:                                       ; preds = %if.end184
  %188 = load i32, i32* %i, align 4, !tbaa !27
  %inc186 = add nsw i32 %188, 1
  store i32 %inc186, i32* %i, align 4, !tbaa !27
  br label %for.cond104

for.end187:                                       ; preds = %for.cond104
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup188

cleanup188:                                       ; preds = %for.end187, %if.then102, %if.then
  %189 = bitcast [4 x %struct.JHUFF_TBL*]* %acseen to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %189) #3
  %190 = bitcast [4 x %struct.JHUFF_TBL*]* %dcseen to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %190) #3
  %191 = bitcast [4 x i32]* %aclens to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %191) #3
  %192 = bitcast [4 x i32]* %dclens to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %192) #3
  %193 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #3
  %194 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #3
  %195 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #3
  %196 = load i32, i32* %retval, align 4
  ret i32 %196

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @emit_dht(%struct.jpeg_compress_struct* %cinfo, i32 %index, i32 %is_ac) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %index.addr = alloca i32, align 4
  %is_ac.addr = alloca i32, align 4
  %htbl = alloca %struct.JHUFF_TBL*, align 4
  %length = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !27
  store i32 %is_ac, i32* %is_ac.addr, align 4, !tbaa !27
  %0 = bitcast %struct.JHUFF_TBL** %htbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i32, i32* %is_ac.addr, align 4, !tbaa !27
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 18
  %5 = load i32, i32* %index.addr, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %5
  %6 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %6, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %7 = load i32, i32* %index.addr, align 4, !tbaa !27
  %add = add nsw i32 %7, 16
  store i32 %add, i32* %index.addr, align 4, !tbaa !27
  br label %if.end

if.else:                                          ; preds = %entry
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 17
  %9 = load i32, i32* %index.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %9
  %10 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %arrayidx1, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %10, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %11 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %cmp = icmp eq %struct.JHUFF_TBL* %11, null
  br i1 %cmp, label %if.then2, label %if.end7

if.then2:                                         ; preds = %if.end
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !37
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 5
  store i32 50, i32* %msg_code, align 4, !tbaa !38
  %14 = load i32, i32* %index.addr, align 4, !tbaa !27
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !37
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 6
  %i4 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx5 = getelementptr inbounds [8 x i32], [8 x i32]* %i4, i32 0, i32 0
  store i32 %14, i32* %arrayidx5, align 4, !tbaa !47
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !37
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 0
  %19 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !46
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = bitcast %struct.jpeg_compress_struct* %20 to %struct.jpeg_common_struct*
  call void %19(%struct.jpeg_common_struct* %21)
  br label %if.end7

if.end7:                                          ; preds = %if.then2, %if.end
  %22 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %sent_table = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %22, i32 0, i32 2
  %23 = load i32, i32* %sent_table, align 4, !tbaa !71
  %tobool8 = icmp ne i32 %23, 0
  br i1 %tobool8, label %if.end36, label %if.then9

if.then9:                                         ; preds = %if.end7
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %24, i32 196)
  store i32 0, i32* %length, align 4, !tbaa !27
  store i32 1, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then9
  %25 = load i32, i32* %i, align 4, !tbaa !27
  %cmp10 = icmp sle i32 %25, 16
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %bits = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %26, i32 0, i32 0
  %27 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds [17 x i8], [17 x i8]* %bits, i32 0, i32 %27
  %28 = load i8, i8* %arrayidx11, align 1, !tbaa !47
  %conv = zext i8 %28 to i32
  %29 = load i32, i32* %length, align 4, !tbaa !27
  %add12 = add nsw i32 %29, %conv
  store i32 %add12, i32* %length, align 4, !tbaa !27
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %32 = load i32, i32* %length, align 4, !tbaa !27
  %add13 = add nsw i32 %32, 2
  %add14 = add nsw i32 %add13, 1
  %add15 = add nsw i32 %add14, 16
  call void @emit_2bytes(%struct.jpeg_compress_struct* %31, i32 %add15)
  %33 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %34 = load i32, i32* %index.addr, align 4, !tbaa !27
  call void @emit_byte(%struct.jpeg_compress_struct* %33, i32 %34)
  store i32 1, i32* %i, align 4, !tbaa !27
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc23, %for.end
  %35 = load i32, i32* %i, align 4, !tbaa !27
  %cmp17 = icmp sle i32 %35, 16
  br i1 %cmp17, label %for.body19, label %for.end25

for.body19:                                       ; preds = %for.cond16
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %37 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %bits20 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %37, i32 0, i32 0
  %38 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx21 = getelementptr inbounds [17 x i8], [17 x i8]* %bits20, i32 0, i32 %38
  %39 = load i8, i8* %arrayidx21, align 1, !tbaa !47
  %conv22 = zext i8 %39 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %36, i32 %conv22)
  br label %for.inc23

for.inc23:                                        ; preds = %for.body19
  %40 = load i32, i32* %i, align 4, !tbaa !27
  %inc24 = add nsw i32 %40, 1
  store i32 %inc24, i32* %i, align 4, !tbaa !27
  br label %for.cond16

for.end25:                                        ; preds = %for.cond16
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc32, %for.end25
  %41 = load i32, i32* %i, align 4, !tbaa !27
  %42 = load i32, i32* %length, align 4, !tbaa !27
  %cmp27 = icmp slt i32 %41, %42
  br i1 %cmp27, label %for.body29, label %for.end34

for.body29:                                       ; preds = %for.cond26
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %44 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %huffval = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %44, i32 0, i32 1
  %45 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx30 = getelementptr inbounds [256 x i8], [256 x i8]* %huffval, i32 0, i32 %45
  %46 = load i8, i8* %arrayidx30, align 1, !tbaa !47
  %conv31 = zext i8 %46 to i32
  call void @emit_byte(%struct.jpeg_compress_struct* %43, i32 %conv31)
  br label %for.inc32

for.inc32:                                        ; preds = %for.body29
  %47 = load i32, i32* %i, align 4, !tbaa !27
  %inc33 = add nsw i32 %47, 1
  store i32 %inc33, i32* %i, align 4, !tbaa !27
  br label %for.cond26

for.end34:                                        ; preds = %for.cond26
  %48 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %htbl, align 4, !tbaa !2
  %sent_table35 = getelementptr inbounds %struct.JHUFF_TBL, %struct.JHUFF_TBL* %48, i32 0, i32 2
  store i32 1, i32* %sent_table35, align 4, !tbaa !71
  br label %if.end36

if.end36:                                         ; preds = %for.end34, %if.end7
  %49 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %50 = bitcast i32* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast %struct.JHUFF_TBL** %htbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  ret void
}

; Function Attrs: nounwind
define internal void @emit_dri(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %0, i32 221)
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_2bytes(%struct.jpeg_compress_struct* %1, i32 4)
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 30
  %4 = load i32, i32* %restart_interval, align 8, !tbaa !45
  call void @emit_2bytes(%struct.jpeg_compress_struct* %2, i32 %4)
  ret void
}

; Function Attrs: nounwind
define internal void @emit_sos(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %i = alloca i32, align 4
  %td = alloca i32, align 4
  %ta = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %td to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %ta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @emit_marker(%struct.jpeg_compress_struct* %4, i32 218)
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 44
  %7 = load i32, i32* %comps_in_scan, align 4, !tbaa !41
  %mul = mul nsw i32 2, %7
  %add = add nsw i32 %mul, 2
  %add1 = add nsw i32 %add, 1
  %add2 = add nsw i32 %add1, 3
  call void @emit_2bytes(%struct.jpeg_compress_struct* %5, i32 %add2)
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 44
  %10 = load i32, i32* %comps_in_scan3, align 4, !tbaa !41
  call void @emit_byte(%struct.jpeg_compress_struct* %8, i32 %10)
  store i32 0, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !27
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 44
  %13 = load i32, i32* %comps_in_scan4, align 4, !tbaa !41
  %cmp = icmp slt i32 %11, %13
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 45
  %15 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %15
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %16, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_id = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %18, i32 0, i32 0
  %19 = load i32, i32* %component_id, align 4, !tbaa !68
  call void @emit_byte(%struct.jpeg_compress_struct* %17, i32 %19)
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 50
  %21 = load i32, i32* %Ss, align 4, !tbaa !42
  %cmp5 = icmp eq i32 %21, 0
  br i1 %cmp5, label %land.lhs.true, label %cond.false

land.lhs.true:                                    ; preds = %for.body
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 52
  %23 = load i32, i32* %Ah, align 4, !tbaa !43
  %cmp6 = icmp eq i32 %23, 0
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true
  %24 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %24, i32 0, i32 5
  %25 = load i32, i32* %dc_tbl_no, align 4, !tbaa !35
  br label %cond.end

cond.false:                                       ; preds = %land.lhs.true, %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %25, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %td, align 4, !tbaa !27
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 51
  %27 = load i32, i32* %Se, align 8, !tbaa !44
  %tobool = icmp ne i32 %27, 0
  br i1 %tobool, label %cond.true7, label %cond.false8

cond.true7:                                       ; preds = %cond.end
  %28 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %28, i32 0, i32 6
  %29 = load i32, i32* %ac_tbl_no, align 4, !tbaa !36
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true7
  %cond10 = phi i32 [ %29, %cond.true7 ], [ 0, %cond.false8 ]
  store i32 %cond10, i32* %ta, align 4, !tbaa !27
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = load i32, i32* %td, align 4, !tbaa !27
  %shl = shl i32 %31, 4
  %32 = load i32, i32* %ta, align 4, !tbaa !27
  %add11 = add nsw i32 %shl, %32
  call void @emit_byte(%struct.jpeg_compress_struct* %30, i32 %add11)
  br label %for.inc

for.inc:                                          ; preds = %cond.end9
  %33 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %35, i32 0, i32 50
  %36 = load i32, i32* %Ss12, align 4, !tbaa !42
  call void @emit_byte(%struct.jpeg_compress_struct* %34, i32 %36)
  %37 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se13 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 51
  %39 = load i32, i32* %Se13, align 8, !tbaa !44
  call void @emit_byte(%struct.jpeg_compress_struct* %37, i32 %39)
  %40 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 52
  %42 = load i32, i32* %Ah14, align 4, !tbaa !43
  %shl15 = shl i32 %42, 4
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 53
  %44 = load i32, i32* %Al, align 8, !tbaa !73
  %add16 = add nsw i32 %shl15, %44
  call void @emit_byte(%struct.jpeg_compress_struct* %40, i32 %add16)
  %45 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast i32* %ta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i32* %td to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !9, i64 48, !8, i64 56, !8, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !8, i64 168, !3, i64 172, !8, i64 176, !8, i64 180, !8, i64 184, !8, i64 188, !8, i64 192, !4, i64 196, !8, i64 200, !8, i64 204, !8, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !10, i64 216, !10, i64 218, !8, i64 220, !8, i64 224, !8, i64 228, !8, i64 232, !8, i64 236, !8, i64 240, !8, i64 244, !4, i64 248, !8, i64 264, !8, i64 268, !8, i64 272, !4, i64 276, !8, i64 316, !8, i64 320, !8, i64 324, !8, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !8, i64 372}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 348}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !8, i64 28}
!17 = !{!"jpeg_marker_writer", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!18 = !{!16, !3, i64 4}
!19 = !{!16, !3, i64 8}
!20 = !{!16, !3, i64 12}
!21 = !{!16, !3, i64 16}
!22 = !{!16, !3, i64 20}
!23 = !{!16, !3, i64 24}
!24 = !{!16, !8, i64 28}
!25 = !{!7, !8, i64 208}
!26 = !{!7, !8, i64 220}
!27 = !{!8, !8, i64 0}
!28 = !{!7, !3, i64 68}
!29 = !{!7, !8, i64 60}
!30 = !{!31, !8, i64 16}
!31 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!32 = !{!7, !8, i64 180}
!33 = !{!7, !8, i64 228}
!34 = !{!7, !8, i64 56}
!35 = !{!31, !8, i64 20}
!36 = !{!31, !8, i64 24}
!37 = !{!7, !3, i64 0}
!38 = !{!39, !8, i64 20}
!39 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!40 = !{!39, !3, i64 4}
!41 = !{!7, !8, i64 244}
!42 = !{!7, !8, i64 316}
!43 = !{!7, !8, i64 324}
!44 = !{!7, !8, i64 320}
!45 = !{!7, !8, i64 200}
!46 = !{!39, !3, i64 0}
!47 = !{!4, !4, i64 0}
!48 = !{!7, !4, i64 212}
!49 = !{!7, !4, i64 213}
!50 = !{!7, !4, i64 214}
!51 = !{!7, !10, i64 216}
!52 = !{!7, !10, i64 218}
!53 = !{!7, !4, i64 64}
!54 = !{!7, !3, i64 24}
!55 = !{!56, !3, i64 0}
!56 = !{!"jpeg_destination_mgr", !3, i64 0, !13, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!57 = !{!56, !13, i64 4}
!58 = !{!56, !3, i64 12}
!59 = !{!7, !3, i64 332}
!60 = !{!61, !8, i64 4152}
!61 = !{!"jpeg_comp_master", !3, i64 0, !3, i64 4, !3, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !4, i64 56, !4, i64 2104, !8, i64 4152, !8, i64 4156, !8, i64 4160, !8, i64 4164, !8, i64 4168, !8, i64 4172, !8, i64 4176, !8, i64 4180, !8, i64 4184, !8, i64 4188, !8, i64 4192, !62, i64 4196, !62, i64 4200, !62, i64 4204}
!62 = !{!"float", !4, i64 0}
!63 = !{!64, !8, i64 128}
!64 = !{!"", !4, i64 0, !8, i64 128}
!65 = !{!10, !10, i64 0}
!66 = !{!7, !8, i64 32}
!67 = !{!7, !8, i64 28}
!68 = !{!31, !8, i64 0}
!69 = !{!31, !8, i64 8}
!70 = !{!31, !8, i64 12}
!71 = !{!72, !8, i64 276}
!72 = !{!"", !4, i64 0, !4, i64 17, !8, i64 276}
!73 = !{!7, !8, i64 328}
