; ModuleID = 'jdsample.c'
source_filename = "jdsample.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.my_upsampler = type { %struct.jpeg_upsampler, [10 x i8**], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], i32, i32, [10 x i32], [10 x i8], [10 x i8] }

; Function Attrs: nounwind
define hidden void @jinit_upsampler(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %need_buffer = alloca i32, align 4
  %do_fancy = alloca i32, align 4
  %h_in_group = alloca i32, align 4
  %v_in_group = alloca i32, align 4
  %h_out_group = alloca i32, align 4
  %v_out_group = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %need_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %do_fancy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %h_in_group to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %v_in_group to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %h_out_group to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %v_out_group to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 77
  %10 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !6
  %jinit_upsampler_no_alloc = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %10, i32 0, i32 7
  %11 = load i32, i32* %jinit_upsampler_no_alloc, align 4, !tbaa !11
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 1
  %13 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !13
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %13, i32 0, i32 0
  %14 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !14
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_decompress_struct* %15 to %struct.jpeg_common_struct*
  %call = call i8* %14(%struct.jpeg_common_struct* %16, i32 1, i32 160)
  %17 = bitcast i8* %call to %struct.my_upsampler*
  store %struct.my_upsampler* %17, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %18 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %19 = bitcast %struct.my_upsampler* %18 to %struct.jpeg_upsampler*
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 85
  store %struct.jpeg_upsampler* %19, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !17
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub, i32 0, i32 0
  %start_pass2 = bitcast {}** %start_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @start_pass_upsample, void (%struct.jpeg_decompress_struct*)** %start_pass2, align 4, !tbaa !18
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 0
  %upsample4 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub3, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* @sep_upsample, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)** %upsample4, align 4, !tbaa !21
  %23 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %23, i32 0, i32 0
  %need_context_rows = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub5, i32 0, i32 2
  store i32 0, i32* %need_context_rows, align 4, !tbaa !22
  br label %if.end

if.else:                                          ; preds = %entry
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 85
  %25 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample6, align 4, !tbaa !17
  %26 = bitcast %struct.jpeg_upsampler* %25 to %struct.my_upsampler*
  store %struct.my_upsampler* %26, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %CCIR601_sampling = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 59
  %28 = load i32, i32* %CCIR601_sampling, align 4, !tbaa !23
  %tobool7 = icmp ne i32 %28, 0
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 0
  %30 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !24
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %30, i32 0, i32 5
  store i32 25, i32* %msg_code, align 4, !tbaa !25
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 0
  %32 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !24
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %32, i32 0, i32 0
  %33 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !27
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %35 = bitcast %struct.jpeg_decompress_struct* %34 to %struct.jpeg_common_struct*
  call void %33(%struct.jpeg_common_struct* %35)
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.end
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %do_fancy_upsampling = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 18
  %37 = load i32, i32* %do_fancy_upsampling, align 4, !tbaa !28
  %tobool11 = icmp ne i32 %37, 0
  br i1 %tobool11, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end10
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 63
  %39 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !29
  %cmp = icmp sgt i32 %39, 1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end10
  %40 = phi i1 [ false, %if.end10 ], [ %cmp, %land.rhs ]
  %land.ext = zext i1 %40 to i32
  store i32 %land.ext, i32* %do_fancy, align 4, !tbaa !30
  store i32 0, i32* %ci, align 4, !tbaa !30
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 44
  %42 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !31
  store %struct.jpeg_component_info* %42, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %land.end
  %43 = load i32, i32* %ci, align 4, !tbaa !30
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 9
  %45 = load i32, i32* %num_components, align 4, !tbaa !32
  %cmp12 = icmp slt i32 %43, %45
  br i1 %cmp12, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %46 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %46, i32 0, i32 2
  %47 = load i32, i32* %h_samp_factor, align 4, !tbaa !33
  %48 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %48, i32 0, i32 9
  %49 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !35
  %mul = mul nsw i32 %47, %49
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 63
  %51 = load i32, i32* %min_DCT_scaled_size13, align 4, !tbaa !29
  %div = sdiv i32 %mul, %51
  store i32 %div, i32* %h_in_group, align 4, !tbaa !30
  %52 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %52, i32 0, i32 3
  %53 = load i32, i32* %v_samp_factor, align 4, !tbaa !36
  %54 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size14 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %54, i32 0, i32 9
  %55 = load i32, i32* %DCT_scaled_size14, align 4, !tbaa !35
  %mul15 = mul nsw i32 %53, %55
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 63
  %57 = load i32, i32* %min_DCT_scaled_size16, align 4, !tbaa !29
  %div17 = sdiv i32 %mul15, %57
  store i32 %div17, i32* %v_in_group, align 4, !tbaa !30
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %58, i32 0, i32 61
  %59 = load i32, i32* %max_h_samp_factor, align 4, !tbaa !37
  store i32 %59, i32* %h_out_group, align 4, !tbaa !30
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %60, i32 0, i32 62
  %61 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  store i32 %61, i32* %v_out_group, align 4, !tbaa !30
  %62 = load i32, i32* %v_in_group, align 4, !tbaa !30
  %63 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rowgroup_height = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %63, i32 0, i32 5
  %64 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds [10 x i32], [10 x i32]* %rowgroup_height, i32 0, i32 %64
  store i32 %62, i32* %arrayidx, align 4, !tbaa !30
  store i32 1, i32* %need_buffer, align 4, !tbaa !30
  %65 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %65, i32 0, i32 12
  %66 = load i32, i32* %component_needed, align 4, !tbaa !39
  %tobool18 = icmp ne i32 %66, 0
  br i1 %tobool18, label %if.else21, label %if.then19

if.then19:                                        ; preds = %for.body
  %67 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %67, i32 0, i32 2
  %68 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx20 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods, i32 0, i32 %68
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @noop_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx20, align 4, !tbaa !2
  store i32 0, i32* %need_buffer, align 4, !tbaa !30
  br label %if.end126

if.else21:                                        ; preds = %for.body
  %69 = load i32, i32* %h_in_group, align 4, !tbaa !30
  %70 = load i32, i32* %h_out_group, align 4, !tbaa !30
  %cmp22 = icmp eq i32 %69, %70
  br i1 %cmp22, label %land.lhs.true, label %if.else27

land.lhs.true:                                    ; preds = %if.else21
  %71 = load i32, i32* %v_in_group, align 4, !tbaa !30
  %72 = load i32, i32* %v_out_group, align 4, !tbaa !30
  %cmp23 = icmp eq i32 %71, %72
  br i1 %cmp23, label %if.then24, label %if.else27

if.then24:                                        ; preds = %land.lhs.true
  %73 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods25 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %73, i32 0, i32 2
  %74 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx26 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods25, i32 0, i32 %74
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @fullsize_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx26, align 4, !tbaa !2
  store i32 0, i32* %need_buffer, align 4, !tbaa !30
  br label %if.end125

if.else27:                                        ; preds = %land.lhs.true, %if.else21
  %75 = load i32, i32* %h_in_group, align 4, !tbaa !30
  %mul28 = mul nsw i32 %75, 2
  %76 = load i32, i32* %h_out_group, align 4, !tbaa !30
  %cmp29 = icmp eq i32 %mul28, %76
  br i1 %cmp29, label %land.lhs.true30, label %if.else57

land.lhs.true30:                                  ; preds = %if.else27
  %77 = load i32, i32* %v_in_group, align 4, !tbaa !30
  %78 = load i32, i32* %v_out_group, align 4, !tbaa !30
  %cmp31 = icmp eq i32 %77, %78
  br i1 %cmp31, label %if.then32, label %if.else57

if.then32:                                        ; preds = %land.lhs.true30
  %79 = load i32, i32* %do_fancy, align 4, !tbaa !30
  %tobool33 = icmp ne i32 %79, 0
  br i1 %tobool33, label %land.lhs.true34, label %if.else46

land.lhs.true34:                                  ; preds = %if.then32
  %80 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %80, i32 0, i32 10
  %81 = load i32, i32* %downsampled_width, align 4, !tbaa !40
  %cmp35 = icmp ugt i32 %81, 2
  br i1 %cmp35, label %if.then36, label %if.else46

if.then36:                                        ; preds = %land.lhs.true34
  %call37 = call i32 @jsimd_can_h2v1_fancy_upsample()
  %tobool38 = icmp ne i32 %call37, 0
  br i1 %tobool38, label %if.then39, label %if.else42

if.then39:                                        ; preds = %if.then36
  %82 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods40 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %82, i32 0, i32 2
  %83 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx41 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods40, i32 0, i32 %83
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @jsimd_h2v1_fancy_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx41, align 4, !tbaa !2
  br label %if.end45

if.else42:                                        ; preds = %if.then36
  %84 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods43 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %84, i32 0, i32 2
  %85 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx44 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods43, i32 0, i32 %85
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @h2v1_fancy_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx44, align 4, !tbaa !2
  br label %if.end45

if.end45:                                         ; preds = %if.else42, %if.then39
  br label %if.end56

if.else46:                                        ; preds = %land.lhs.true34, %if.then32
  %call47 = call i32 @jsimd_can_h2v1_upsample()
  %tobool48 = icmp ne i32 %call47, 0
  br i1 %tobool48, label %if.then49, label %if.else52

if.then49:                                        ; preds = %if.else46
  %86 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods50 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %86, i32 0, i32 2
  %87 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx51 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods50, i32 0, i32 %87
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @jsimd_h2v1_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx51, align 4, !tbaa !2
  br label %if.end55

if.else52:                                        ; preds = %if.else46
  %88 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods53 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %88, i32 0, i32 2
  %89 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx54 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods53, i32 0, i32 %89
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @h2v1_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx54, align 4, !tbaa !2
  br label %if.end55

if.end55:                                         ; preds = %if.else52, %if.then49
  br label %if.end56

if.end56:                                         ; preds = %if.end55, %if.end45
  br label %if.end124

if.else57:                                        ; preds = %land.lhs.true30, %if.else27
  %90 = load i32, i32* %h_in_group, align 4, !tbaa !30
  %91 = load i32, i32* %h_out_group, align 4, !tbaa !30
  %cmp58 = icmp eq i32 %90, %91
  br i1 %cmp58, label %land.lhs.true59, label %if.else69

land.lhs.true59:                                  ; preds = %if.else57
  %92 = load i32, i32* %v_in_group, align 4, !tbaa !30
  %mul60 = mul nsw i32 %92, 2
  %93 = load i32, i32* %v_out_group, align 4, !tbaa !30
  %cmp61 = icmp eq i32 %mul60, %93
  br i1 %cmp61, label %land.lhs.true62, label %if.else69

land.lhs.true62:                                  ; preds = %land.lhs.true59
  %94 = load i32, i32* %do_fancy, align 4, !tbaa !30
  %tobool63 = icmp ne i32 %94, 0
  br i1 %tobool63, label %if.then64, label %if.else69

if.then64:                                        ; preds = %land.lhs.true62
  %95 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods65 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %95, i32 0, i32 2
  %96 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx66 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods65, i32 0, i32 %96
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @h1v2_fancy_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx66, align 4, !tbaa !2
  %97 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub67 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %97, i32 0, i32 0
  %need_context_rows68 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub67, i32 0, i32 2
  store i32 1, i32* %need_context_rows68, align 4, !tbaa !22
  br label %if.end123

if.else69:                                        ; preds = %land.lhs.true62, %land.lhs.true59, %if.else57
  %98 = load i32, i32* %h_in_group, align 4, !tbaa !30
  %mul70 = mul nsw i32 %98, 2
  %99 = load i32, i32* %h_out_group, align 4, !tbaa !30
  %cmp71 = icmp eq i32 %mul70, %99
  br i1 %cmp71, label %land.lhs.true72, label %if.else103

land.lhs.true72:                                  ; preds = %if.else69
  %100 = load i32, i32* %v_in_group, align 4, !tbaa !30
  %mul73 = mul nsw i32 %100, 2
  %101 = load i32, i32* %v_out_group, align 4, !tbaa !30
  %cmp74 = icmp eq i32 %mul73, %101
  br i1 %cmp74, label %if.then75, label %if.else103

if.then75:                                        ; preds = %land.lhs.true72
  %102 = load i32, i32* %do_fancy, align 4, !tbaa !30
  %tobool76 = icmp ne i32 %102, 0
  br i1 %tobool76, label %land.lhs.true77, label %if.else92

land.lhs.true77:                                  ; preds = %if.then75
  %103 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_width78 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %103, i32 0, i32 10
  %104 = load i32, i32* %downsampled_width78, align 4, !tbaa !40
  %cmp79 = icmp ugt i32 %104, 2
  br i1 %cmp79, label %if.then80, label %if.else92

if.then80:                                        ; preds = %land.lhs.true77
  %call81 = call i32 @jsimd_can_h2v2_fancy_upsample()
  %tobool82 = icmp ne i32 %call81, 0
  br i1 %tobool82, label %if.then83, label %if.else86

if.then83:                                        ; preds = %if.then80
  %105 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods84 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %105, i32 0, i32 2
  %106 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx85 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods84, i32 0, i32 %106
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @jsimd_h2v2_fancy_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx85, align 4, !tbaa !2
  br label %if.end89

if.else86:                                        ; preds = %if.then80
  %107 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods87 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %107, i32 0, i32 2
  %108 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx88 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods87, i32 0, i32 %108
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @h2v2_fancy_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx88, align 4, !tbaa !2
  br label %if.end89

if.end89:                                         ; preds = %if.else86, %if.then83
  %109 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub90 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %109, i32 0, i32 0
  %need_context_rows91 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub90, i32 0, i32 2
  store i32 1, i32* %need_context_rows91, align 4, !tbaa !22
  br label %if.end102

if.else92:                                        ; preds = %land.lhs.true77, %if.then75
  %call93 = call i32 @jsimd_can_h2v2_upsample()
  %tobool94 = icmp ne i32 %call93, 0
  br i1 %tobool94, label %if.then95, label %if.else98

if.then95:                                        ; preds = %if.else92
  %110 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods96 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %110, i32 0, i32 2
  %111 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx97 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods96, i32 0, i32 %111
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @jsimd_h2v2_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx97, align 4, !tbaa !2
  br label %if.end101

if.else98:                                        ; preds = %if.else92
  %112 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods99 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %112, i32 0, i32 2
  %113 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx100 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods99, i32 0, i32 %113
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @h2v2_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx100, align 4, !tbaa !2
  br label %if.end101

if.end101:                                        ; preds = %if.else98, %if.then95
  br label %if.end102

if.end102:                                        ; preds = %if.end101, %if.end89
  br label %if.end122

if.else103:                                       ; preds = %land.lhs.true72, %if.else69
  %114 = load i32, i32* %h_out_group, align 4, !tbaa !30
  %115 = load i32, i32* %h_in_group, align 4, !tbaa !30
  %rem = srem i32 %114, %115
  %cmp104 = icmp eq i32 %rem, 0
  br i1 %cmp104, label %land.lhs.true105, label %if.else116

land.lhs.true105:                                 ; preds = %if.else103
  %116 = load i32, i32* %v_out_group, align 4, !tbaa !30
  %117 = load i32, i32* %v_in_group, align 4, !tbaa !30
  %rem106 = srem i32 %116, %117
  %cmp107 = icmp eq i32 %rem106, 0
  br i1 %cmp107, label %if.then108, label %if.else116

if.then108:                                       ; preds = %land.lhs.true105
  %118 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods109 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %118, i32 0, i32 2
  %119 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx110 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods109, i32 0, i32 %119
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)* @int_upsample, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx110, align 4, !tbaa !2
  %120 = load i32, i32* %h_out_group, align 4, !tbaa !30
  %121 = load i32, i32* %h_in_group, align 4, !tbaa !30
  %div111 = sdiv i32 %120, %121
  %conv = trunc i32 %div111 to i8
  %122 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %h_expand = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %122, i32 0, i32 6
  %123 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx112 = getelementptr inbounds [10 x i8], [10 x i8]* %h_expand, i32 0, i32 %123
  store i8 %conv, i8* %arrayidx112, align 1, !tbaa !41
  %124 = load i32, i32* %v_out_group, align 4, !tbaa !30
  %125 = load i32, i32* %v_in_group, align 4, !tbaa !30
  %div113 = sdiv i32 %124, %125
  %conv114 = trunc i32 %div113 to i8
  %126 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %v_expand = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %126, i32 0, i32 7
  %127 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx115 = getelementptr inbounds [10 x i8], [10 x i8]* %v_expand, i32 0, i32 %127
  store i8 %conv114, i8* %arrayidx115, align 1, !tbaa !41
  br label %if.end121

if.else116:                                       ; preds = %land.lhs.true105, %if.else103
  %128 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err117 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %128, i32 0, i32 0
  %129 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err117, align 8, !tbaa !24
  %msg_code118 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %129, i32 0, i32 5
  store i32 38, i32* %msg_code118, align 4, !tbaa !25
  %130 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err119 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %130, i32 0, i32 0
  %131 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err119, align 8, !tbaa !24
  %error_exit120 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %131, i32 0, i32 0
  %132 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit120, align 4, !tbaa !27
  %133 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %134 = bitcast %struct.jpeg_decompress_struct* %133 to %struct.jpeg_common_struct*
  call void %132(%struct.jpeg_common_struct* %134)
  br label %if.end121

if.end121:                                        ; preds = %if.else116, %if.then108
  br label %if.end122

if.end122:                                        ; preds = %if.end121, %if.end102
  br label %if.end123

if.end123:                                        ; preds = %if.end122, %if.then64
  br label %if.end124

if.end124:                                        ; preds = %if.end123, %if.end56
  br label %if.end125

if.end125:                                        ; preds = %if.end124, %if.then24
  br label %if.end126

if.end126:                                        ; preds = %if.end125, %if.then19
  %135 = load i32, i32* %need_buffer, align 4, !tbaa !30
  %tobool127 = icmp ne i32 %135, 0
  br i1 %tobool127, label %land.lhs.true128, label %if.end139

land.lhs.true128:                                 ; preds = %if.end126
  %136 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master129 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %136, i32 0, i32 77
  %137 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master129, align 4, !tbaa !6
  %jinit_upsampler_no_alloc130 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %137, i32 0, i32 7
  %138 = load i32, i32* %jinit_upsampler_no_alloc130, align 4, !tbaa !11
  %tobool131 = icmp ne i32 %138, 0
  br i1 %tobool131, label %if.end139, label %if.then132

if.then132:                                       ; preds = %land.lhs.true128
  %139 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem133 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %139, i32 0, i32 1
  %140 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem133, align 4, !tbaa !13
  %alloc_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %140, i32 0, i32 2
  %141 = load i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_sarray, align 4, !tbaa !42
  %142 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %143 = bitcast %struct.jpeg_decompress_struct* %142 to %struct.jpeg_common_struct*
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 27
  %145 = load i32, i32* %output_width, align 8, !tbaa !43
  %146 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor134 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %146, i32 0, i32 61
  %147 = load i32, i32* %max_h_samp_factor134, align 4, !tbaa !37
  %call135 = call i32 @jround_up(i32 %145, i32 %147)
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor136 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 62
  %149 = load i32, i32* %max_v_samp_factor136, align 8, !tbaa !38
  %call137 = call i8** %141(%struct.jpeg_common_struct* %143, i32 1, i32 %call135, i32 %149)
  %150 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %color_buf = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %150, i32 0, i32 1
  %151 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx138 = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf, i32 0, i32 %151
  store i8** %call137, i8*** %arrayidx138, align 4, !tbaa !2
  br label %if.end139

if.end139:                                        ; preds = %if.then132, %land.lhs.true128, %if.end126
  br label %for.inc

for.inc:                                          ; preds = %if.end139
  %152 = load i32, i32* %ci, align 4, !tbaa !30
  %inc = add nsw i32 %152, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !30
  %153 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %153, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %154 = bitcast i32* %v_out_group to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #3
  %155 = bitcast i32* %h_out_group to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #3
  %156 = bitcast i32* %v_in_group to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #3
  %157 = bitcast i32* %h_in_group to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #3
  %158 = bitcast i32* %do_fancy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #3
  %159 = bitcast i32* %need_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #3
  %160 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #3
  %161 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #3
  %162 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_upsample(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !17
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 62
  %5 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  %6 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %6, i32 0, i32 3
  store i32 %5, i32* %next_row_out, align 4, !tbaa !44
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 28
  %8 = load i32, i32* %output_height, align 4, !tbaa !45
  %9 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %9, i32 0, i32 4
  store i32 %8, i32* %rows_to_go, align 4, !tbaa !46
  %10 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  ret void
}

; Function Attrs: nounwind
define internal void @sep_upsample(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32* %in_row_group_ctr, i32 %in_row_groups_avail, i8** %output_buf, i32* %out_row_ctr, i32 %out_rows_avail) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32*, align 4
  %in_row_groups_avail.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %out_row_ctr.addr = alloca i32*, align 4
  %out_rows_avail.addr = alloca i32, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %num_rows = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32* %in_row_group_ctr, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  store i32 %in_row_groups_avail, i32* %in_row_groups_avail.addr, align 4, !tbaa !30
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32* %out_row_ctr, i32** %out_row_ctr.addr, align 4, !tbaa !2
  store i32 %out_rows_avail, i32* %out_rows_avail.addr, align 4, !tbaa !30
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !17
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %num_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %7, i32 0, i32 3
  %8 = load i32, i32* %next_row_out, align 4, !tbaa !44
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 62
  %10 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  %cmp = icmp sge i32 %8, %10
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %ci, align 4, !tbaa !30
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 44
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !31
  store %struct.jpeg_component_info* %12, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %13 = load i32, i32* %ci, align 4, !tbaa !30
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 9
  %15 = load i32, i32* %num_components, align 4, !tbaa !32
  %cmp2 = icmp slt i32 %13, %15
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %methods = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %16, i32 0, i32 2
  %17 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*]* %methods, i32 0, i32 %17
  %18 = load void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)** %arrayidx, align 4, !tbaa !2
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %20 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %21 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %22 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx3 = getelementptr inbounds i8**, i8*** %21, i32 %22
  %23 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %24 = load i32*, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  %25 = load i32, i32* %24, align 4, !tbaa !30
  %26 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rowgroup_height = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %26, i32 0, i32 5
  %27 = load i32, i32* %ci, align 4, !tbaa !30
  %arrayidx4 = getelementptr inbounds [10 x i32], [10 x i32]* %rowgroup_height, i32 0, i32 %27
  %28 = load i32, i32* %arrayidx4, align 4, !tbaa !30
  %mul = mul i32 %25, %28
  %add.ptr = getelementptr inbounds i8*, i8** %23, i32 %mul
  %29 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %color_buf = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %29, i32 0, i32 1
  %arraydecay = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf, i32 0, i32 0
  %30 = load i32, i32* %ci, align 4, !tbaa !30
  %add.ptr5 = getelementptr inbounds i8**, i8*** %arraydecay, i32 %30
  call void %18(%struct.jpeg_decompress_struct* %19, %struct.jpeg_component_info* %20, i8** %add.ptr, i8*** %add.ptr5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %31 = load i32, i32* %ci, align 4, !tbaa !30
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !30
  %32 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %32, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %33 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out6 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %33, i32 0, i32 3
  store i32 0, i32* %next_row_out6, align 4, !tbaa !44
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 62
  %35 = load i32, i32* %max_v_samp_factor7, align 8, !tbaa !38
  %36 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out8 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %36, i32 0, i32 3
  %37 = load i32, i32* %next_row_out8, align 4, !tbaa !44
  %sub = sub nsw i32 %35, %37
  store i32 %sub, i32* %num_rows, align 4, !tbaa !30
  %38 = load i32, i32* %num_rows, align 4, !tbaa !30
  %39 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %39, i32 0, i32 4
  %40 = load i32, i32* %rows_to_go, align 4, !tbaa !46
  %cmp9 = icmp ugt i32 %38, %40
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end
  %41 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go11 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %41, i32 0, i32 4
  %42 = load i32, i32* %rows_to_go11, align 4, !tbaa !46
  store i32 %42, i32* %num_rows, align 4, !tbaa !30
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.end
  %43 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %44 = load i32, i32* %43, align 4, !tbaa !30
  %45 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !30
  %sub13 = sub i32 %45, %44
  store i32 %sub13, i32* %out_rows_avail.addr, align 4, !tbaa !30
  %46 = load i32, i32* %num_rows, align 4, !tbaa !30
  %47 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !30
  %cmp14 = icmp ugt i32 %46, %47
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end12
  %48 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !30
  store i32 %48, i32* %num_rows, align 4, !tbaa !30
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %if.end12
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 86
  %50 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert, align 8, !tbaa !47
  %color_convert = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %50, i32 0, i32 1
  %51 = load void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert, align 4, !tbaa !48
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %53 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %color_buf17 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %53, i32 0, i32 1
  %arraydecay18 = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf17, i32 0, i32 0
  %54 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out19 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %54, i32 0, i32 3
  %55 = load i32, i32* %next_row_out19, align 4, !tbaa !44
  %56 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %57 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %58 = load i32, i32* %57, align 4, !tbaa !30
  %add.ptr20 = getelementptr inbounds i8*, i8** %56, i32 %58
  %59 = load i32, i32* %num_rows, align 4, !tbaa !30
  call void %51(%struct.jpeg_decompress_struct* %52, i8*** %arraydecay18, i32 %55, i8** %add.ptr20, i32 %59)
  %60 = load i32, i32* %num_rows, align 4, !tbaa !30
  %61 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %62 = load i32, i32* %61, align 4, !tbaa !30
  %add = add i32 %62, %60
  store i32 %add, i32* %61, align 4, !tbaa !30
  %63 = load i32, i32* %num_rows, align 4, !tbaa !30
  %64 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go21 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %64, i32 0, i32 4
  %65 = load i32, i32* %rows_to_go21, align 4, !tbaa !46
  %sub22 = sub i32 %65, %63
  store i32 %sub22, i32* %rows_to_go21, align 4, !tbaa !46
  %66 = load i32, i32* %num_rows, align 4, !tbaa !30
  %67 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out23 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %67, i32 0, i32 3
  %68 = load i32, i32* %next_row_out23, align 4, !tbaa !44
  %add24 = add i32 %68, %66
  store i32 %add24, i32* %next_row_out23, align 4, !tbaa !44
  %69 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out25 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %69, i32 0, i32 3
  %70 = load i32, i32* %next_row_out25, align 4, !tbaa !44
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor26 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 62
  %72 = load i32, i32* %max_v_samp_factor26, align 8, !tbaa !38
  %cmp27 = icmp sge i32 %70, %72
  br i1 %cmp27, label %if.then28, label %if.end30

if.then28:                                        ; preds = %if.end16
  %73 = load i32*, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  %74 = load i32, i32* %73, align 4, !tbaa !30
  %inc29 = add i32 %74, 1
  store i32 %inc29, i32* %73, align 4, !tbaa !30
  br label %if.end30

if.end30:                                         ; preds = %if.then28, %if.end16
  %75 = bitcast i32* %num_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %77 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  ret void
}

; Function Attrs: nounwind
define internal void @noop_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %0 = load i8***, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  store i8** null, i8*** %0, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal void @fullsize_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %0 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  store i8** %0, i8*** %1, align 4, !tbaa !2
  ret void
}

declare i32 @jsimd_can_h2v1_fancy_upsample() #2

declare void @jsimd_h2v1_fancy_upsample(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***) #2

; Function Attrs: nounwind
define internal void @h2v1_fancy_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  %output_data = alloca i8**, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %invalue = alloca i32, align 4
  %colctr = alloca i32, align 4
  %inrow = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %0 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8***, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %2 = load i8**, i8*** %1, align 4, !tbaa !2
  store i8** %2, i8*** %output_data, align 4, !tbaa !2
  %3 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %invalue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store i32 0, i32* %inrow, align 4, !tbaa !30
  br label %for.cond

for.cond:                                         ; preds = %for.inc39, %entry
  %8 = load i32, i32* %inrow, align 4, !tbaa !30
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 62
  %10 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  %cmp = icmp slt i32 %8, %10
  br i1 %cmp, label %for.body, label %for.end40

for.body:                                         ; preds = %for.cond
  %11 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %12 = load i32, i32* %inrow, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds i8*, i8** %11, i32 %12
  %13 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %13, i8** %inptr, align 4, !tbaa !2
  %14 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %15 = load i32, i32* %inrow, align 4, !tbaa !30
  %arrayidx1 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %16, i8** %outptr, align 4, !tbaa !2
  %17 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %17, i32 1
  store i8* %incdec.ptr, i8** %inptr, align 4, !tbaa !2
  %18 = load i8, i8* %17, align 1, !tbaa !41
  %conv = zext i8 %18 to i32
  store i32 %conv, i32* %invalue, align 4, !tbaa !30
  %19 = load i32, i32* %invalue, align 4, !tbaa !30
  %conv2 = trunc i32 %19 to i8
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr3 = getelementptr inbounds i8, i8* %20, i32 1
  store i8* %incdec.ptr3, i8** %outptr, align 4, !tbaa !2
  store i8 %conv2, i8* %20, align 1, !tbaa !41
  %21 = load i32, i32* %invalue, align 4, !tbaa !30
  %mul = mul nsw i32 %21, 3
  %22 = load i8*, i8** %inptr, align 4, !tbaa !2
  %23 = load i8, i8* %22, align 1, !tbaa !41
  %conv4 = zext i8 %23 to i32
  %add = add nsw i32 %mul, %conv4
  %add5 = add nsw i32 %add, 2
  %shr = ashr i32 %add5, 2
  %conv6 = trunc i32 %shr to i8
  %24 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr7 = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr7, i8** %outptr, align 4, !tbaa !2
  store i8 %conv6, i8* %24, align 1, !tbaa !41
  %25 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %downsampled_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %25, i32 0, i32 10
  %26 = load i32, i32* %downsampled_width, align 4, !tbaa !40
  %sub = sub i32 %26, 2
  store i32 %sub, i32* %colctr, align 4, !tbaa !30
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %27 = load i32, i32* %colctr, align 4, !tbaa !30
  %cmp9 = icmp ugt i32 %27, 0
  br i1 %cmp9, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond8
  %28 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %28, i32 1
  store i8* %incdec.ptr12, i8** %inptr, align 4, !tbaa !2
  %29 = load i8, i8* %28, align 1, !tbaa !41
  %conv13 = zext i8 %29 to i32
  %mul14 = mul nsw i32 %conv13, 3
  store i32 %mul14, i32* %invalue, align 4, !tbaa !30
  %30 = load i32, i32* %invalue, align 4, !tbaa !30
  %31 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %31, i32 -2
  %32 = load i8, i8* %arrayidx15, align 1, !tbaa !41
  %conv16 = zext i8 %32 to i32
  %add17 = add nsw i32 %30, %conv16
  %add18 = add nsw i32 %add17, 1
  %shr19 = ashr i32 %add18, 2
  %conv20 = trunc i32 %shr19 to i8
  %33 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr21 = getelementptr inbounds i8, i8* %33, i32 1
  store i8* %incdec.ptr21, i8** %outptr, align 4, !tbaa !2
  store i8 %conv20, i8* %33, align 1, !tbaa !41
  %34 = load i32, i32* %invalue, align 4, !tbaa !30
  %35 = load i8*, i8** %inptr, align 4, !tbaa !2
  %36 = load i8, i8* %35, align 1, !tbaa !41
  %conv22 = zext i8 %36 to i32
  %add23 = add nsw i32 %34, %conv22
  %add24 = add nsw i32 %add23, 2
  %shr25 = ashr i32 %add24, 2
  %conv26 = trunc i32 %shr25 to i8
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr27 = getelementptr inbounds i8, i8* %37, i32 1
  store i8* %incdec.ptr27, i8** %outptr, align 4, !tbaa !2
  store i8 %conv26, i8* %37, align 1, !tbaa !41
  br label %for.inc

for.inc:                                          ; preds = %for.body11
  %38 = load i32, i32* %colctr, align 4, !tbaa !30
  %dec = add i32 %38, -1
  store i32 %dec, i32* %colctr, align 4, !tbaa !30
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %39 = load i8*, i8** %inptr, align 4, !tbaa !2
  %40 = load i8, i8* %39, align 1, !tbaa !41
  %conv28 = zext i8 %40 to i32
  store i32 %conv28, i32* %invalue, align 4, !tbaa !30
  %41 = load i32, i32* %invalue, align 4, !tbaa !30
  %mul29 = mul nsw i32 %41, 3
  %42 = load i8*, i8** %inptr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %42, i32 -1
  %43 = load i8, i8* %arrayidx30, align 1, !tbaa !41
  %conv31 = zext i8 %43 to i32
  %add32 = add nsw i32 %mul29, %conv31
  %add33 = add nsw i32 %add32, 1
  %shr34 = ashr i32 %add33, 2
  %conv35 = trunc i32 %shr34 to i8
  %44 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr36 = getelementptr inbounds i8, i8* %44, i32 1
  store i8* %incdec.ptr36, i8** %outptr, align 4, !tbaa !2
  store i8 %conv35, i8* %44, align 1, !tbaa !41
  %45 = load i32, i32* %invalue, align 4, !tbaa !30
  %conv37 = trunc i32 %45 to i8
  %46 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr38 = getelementptr inbounds i8, i8* %46, i32 1
  store i8* %incdec.ptr38, i8** %outptr, align 4, !tbaa !2
  store i8 %conv37, i8* %46, align 1, !tbaa !41
  br label %for.inc39

for.inc39:                                        ; preds = %for.end
  %47 = load i32, i32* %inrow, align 4, !tbaa !30
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %inrow, align 4, !tbaa !30
  br label %for.cond

for.end40:                                        ; preds = %for.cond
  %48 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  %49 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %50 = bitcast i32* %invalue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  ret void
}

declare i32 @jsimd_can_h2v1_upsample() #2

declare void @jsimd_h2v1_upsample(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***) #2

; Function Attrs: nounwind
define internal void @h2v1_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  %output_data = alloca i8**, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %invalue = alloca i8, align 1
  %outend = alloca i8*, align 4
  %inrow = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %0 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8***, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %2 = load i8**, i8*** %1, align 4, !tbaa !2
  store i8** %2, i8*** %output_data, align 4, !tbaa !2
  %3 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %invalue) #3
  %5 = bitcast i8** %outend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  store i32 0, i32* %inrow, align 4, !tbaa !30
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %inrow, align 4, !tbaa !30
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 62
  %9 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  %cmp = icmp slt i32 %7, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %11 = load i32, i32* %inrow, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %12, i8** %inptr, align 4, !tbaa !2
  %13 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %14 = load i32, i32* %inrow, align 4, !tbaa !30
  %arrayidx1 = getelementptr inbounds i8*, i8** %13, i32 %14
  %15 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %15, i8** %outptr, align 4, !tbaa !2
  %16 = load i8*, i8** %outptr, align 4, !tbaa !2
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 27
  %18 = load i32, i32* %output_width, align 8, !tbaa !43
  %add.ptr = getelementptr inbounds i8, i8* %16, i32 %18
  store i8* %add.ptr, i8** %outend, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body
  %19 = load i8*, i8** %outptr, align 4, !tbaa !2
  %20 = load i8*, i8** %outend, align 4, !tbaa !2
  %cmp2 = icmp ult i8* %19, %20
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %21 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %21, i32 1
  store i8* %incdec.ptr, i8** %inptr, align 4, !tbaa !2
  %22 = load i8, i8* %21, align 1, !tbaa !41
  store i8 %22, i8* %invalue, align 1, !tbaa !41
  %23 = load i8, i8* %invalue, align 1, !tbaa !41
  %24 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr3 = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr3, i8** %outptr, align 4, !tbaa !2
  store i8 %23, i8* %24, align 1, !tbaa !41
  %25 = load i8, i8* %invalue, align 1, !tbaa !41
  %26 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr4 = getelementptr inbounds i8, i8* %26, i32 1
  store i8* %incdec.ptr4, i8** %outptr, align 4, !tbaa !2
  store i8 %25, i8* %26, align 1, !tbaa !41
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %27 = load i32, i32* %inrow, align 4, !tbaa !30
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %inrow, align 4, !tbaa !30
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %28 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  %29 = bitcast i8** %outend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %invalue) #3
  %30 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %31 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  %32 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  ret void
}

; Function Attrs: nounwind
define internal void @h1v2_fancy_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  %output_data = alloca i8**, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %thiscolsum = alloca i32, align 4
  %colctr = alloca i32, align 4
  %inrow = alloca i32, align 4
  %outrow = alloca i32, align 4
  %v = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %0 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8***, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %2 = load i8**, i8*** %1, align 4, !tbaa !2
  store i8** %2, i8*** %output_data, align 4, !tbaa !2
  %3 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %thiscolsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  store i32 0, i32* %outrow, align 4, !tbaa !30
  store i32 0, i32* %inrow, align 4, !tbaa !30
  br label %while.cond

while.cond:                                       ; preds = %for.end18, %entry
  %11 = load i32, i32* %outrow, align 4, !tbaa !30
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 62
  %13 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  %cmp = icmp slt i32 %11, %13
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %v, align 4, !tbaa !30
  br label %for.cond

for.cond:                                         ; preds = %for.inc16, %while.body
  %14 = load i32, i32* %v, align 4, !tbaa !30
  %cmp1 = icmp slt i32 %14, 2
  br i1 %cmp1, label %for.body, label %for.end18

for.body:                                         ; preds = %for.cond
  %15 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %16 = load i32, i32* %inrow, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds i8*, i8** %15, i32 %16
  %17 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %17, i8** %inptr0, align 4, !tbaa !2
  %18 = load i32, i32* %v, align 4, !tbaa !30
  %cmp2 = icmp eq i32 %18, 0
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %19 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %20 = load i32, i32* %inrow, align 4, !tbaa !30
  %sub = sub nsw i32 %20, 1
  %arrayidx3 = getelementptr inbounds i8*, i8** %19, i32 %sub
  %21 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %21, i8** %inptr1, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %for.body
  %22 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %23 = load i32, i32* %inrow, align 4, !tbaa !30
  %add = add nsw i32 %23, 1
  %arrayidx4 = getelementptr inbounds i8*, i8** %22, i32 %add
  %24 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %24, i8** %inptr1, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %25 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %26 = load i32, i32* %outrow, align 4, !tbaa !30
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %outrow, align 4, !tbaa !30
  %arrayidx5 = getelementptr inbounds i8*, i8** %25, i32 %26
  %27 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %27, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %colctr, align 4, !tbaa !30
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %if.end
  %28 = load i32, i32* %colctr, align 4, !tbaa !30
  %29 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %downsampled_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %29, i32 0, i32 10
  %30 = load i32, i32* %downsampled_width, align 4, !tbaa !40
  %cmp7 = icmp ult i32 %28, %30
  br i1 %cmp7, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond6
  %31 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %31, i32 1
  store i8* %incdec.ptr, i8** %inptr0, align 4, !tbaa !2
  %32 = load i8, i8* %31, align 1, !tbaa !41
  %conv = zext i8 %32 to i32
  %mul = mul nsw i32 %conv, 3
  %33 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %33, i32 1
  store i8* %incdec.ptr9, i8** %inptr1, align 4, !tbaa !2
  %34 = load i8, i8* %33, align 1, !tbaa !41
  %conv10 = zext i8 %34 to i32
  %add11 = add nsw i32 %mul, %conv10
  store i32 %add11, i32* %thiscolsum, align 4, !tbaa !30
  %35 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  %add12 = add nsw i32 %35, 1
  %shr = ashr i32 %add12, 2
  %conv13 = trunc i32 %shr to i8
  %36 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr14 = getelementptr inbounds i8, i8* %36, i32 1
  store i8* %incdec.ptr14, i8** %outptr, align 4, !tbaa !2
  store i8 %conv13, i8* %36, align 1, !tbaa !41
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %37 = load i32, i32* %colctr, align 4, !tbaa !30
  %inc15 = add i32 %37, 1
  store i32 %inc15, i32* %colctr, align 4, !tbaa !30
  br label %for.cond6

for.end:                                          ; preds = %for.cond6
  br label %for.inc16

for.inc16:                                        ; preds = %for.end
  %38 = load i32, i32* %v, align 4, !tbaa !30
  %inc17 = add nsw i32 %38, 1
  store i32 %inc17, i32* %v, align 4, !tbaa !30
  br label %for.cond

for.end18:                                        ; preds = %for.cond
  %39 = load i32, i32* %inrow, align 4, !tbaa !30
  %inc19 = add nsw i32 %39, 1
  store i32 %inc19, i32* %inrow, align 4, !tbaa !30
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  %41 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  %42 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  %43 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  %44 = bitcast i32* %thiscolsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  ret void
}

declare i32 @jsimd_can_h2v2_fancy_upsample() #2

declare void @jsimd_h2v2_fancy_upsample(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***) #2

; Function Attrs: nounwind
define internal void @h2v2_fancy_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  %output_data = alloca i8**, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %thiscolsum = alloca i32, align 4
  %lastcolsum = alloca i32, align 4
  %nextcolsum = alloca i32, align 4
  %colctr = alloca i32, align 4
  %inrow = alloca i32, align 4
  %outrow = alloca i32, align 4
  %v = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %0 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8***, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %2 = load i8**, i8*** %1, align 4, !tbaa !2
  store i8** %2, i8*** %output_data, align 4, !tbaa !2
  %3 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %thiscolsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %lastcolsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %nextcolsum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  store i32 0, i32* %outrow, align 4, !tbaa !30
  store i32 0, i32* %inrow, align 4, !tbaa !30
  br label %while.cond

while.cond:                                       ; preds = %for.end61, %entry
  %13 = load i32, i32* %outrow, align 4, !tbaa !30
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 62
  %15 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  %cmp = icmp slt i32 %13, %15
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %v, align 4, !tbaa !30
  br label %for.cond

for.cond:                                         ; preds = %for.inc59, %while.body
  %16 = load i32, i32* %v, align 4, !tbaa !30
  %cmp1 = icmp slt i32 %16, 2
  br i1 %cmp1, label %for.body, label %for.end61

for.body:                                         ; preds = %for.cond
  %17 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %18 = load i32, i32* %inrow, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds i8*, i8** %17, i32 %18
  %19 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %19, i8** %inptr0, align 4, !tbaa !2
  %20 = load i32, i32* %v, align 4, !tbaa !30
  %cmp2 = icmp eq i32 %20, 0
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %21 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %22 = load i32, i32* %inrow, align 4, !tbaa !30
  %sub = sub nsw i32 %22, 1
  %arrayidx3 = getelementptr inbounds i8*, i8** %21, i32 %sub
  %23 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %23, i8** %inptr1, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %for.body
  %24 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %25 = load i32, i32* %inrow, align 4, !tbaa !30
  %add = add nsw i32 %25, 1
  %arrayidx4 = getelementptr inbounds i8*, i8** %24, i32 %add
  %26 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %26, i8** %inptr1, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %27 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %28 = load i32, i32* %outrow, align 4, !tbaa !30
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %outrow, align 4, !tbaa !30
  %arrayidx5 = getelementptr inbounds i8*, i8** %27, i32 %28
  %29 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %29, i8** %outptr, align 4, !tbaa !2
  %30 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %30, i32 1
  store i8* %incdec.ptr, i8** %inptr0, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !41
  %conv = zext i8 %31 to i32
  %mul = mul nsw i32 %conv, 3
  %32 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr6 = getelementptr inbounds i8, i8* %32, i32 1
  store i8* %incdec.ptr6, i8** %inptr1, align 4, !tbaa !2
  %33 = load i8, i8* %32, align 1, !tbaa !41
  %conv7 = zext i8 %33 to i32
  %add8 = add nsw i32 %mul, %conv7
  store i32 %add8, i32* %thiscolsum, align 4, !tbaa !30
  %34 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %34, i32 1
  store i8* %incdec.ptr9, i8** %inptr0, align 4, !tbaa !2
  %35 = load i8, i8* %34, align 1, !tbaa !41
  %conv10 = zext i8 %35 to i32
  %mul11 = mul nsw i32 %conv10, 3
  %36 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %36, i32 1
  store i8* %incdec.ptr12, i8** %inptr1, align 4, !tbaa !2
  %37 = load i8, i8* %36, align 1, !tbaa !41
  %conv13 = zext i8 %37 to i32
  %add14 = add nsw i32 %mul11, %conv13
  store i32 %add14, i32* %nextcolsum, align 4, !tbaa !30
  %38 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  %mul15 = mul nsw i32 %38, 4
  %add16 = add nsw i32 %mul15, 8
  %shr = ashr i32 %add16, 4
  %conv17 = trunc i32 %shr to i8
  %39 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr18 = getelementptr inbounds i8, i8* %39, i32 1
  store i8* %incdec.ptr18, i8** %outptr, align 4, !tbaa !2
  store i8 %conv17, i8* %39, align 1, !tbaa !41
  %40 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  %mul19 = mul nsw i32 %40, 3
  %41 = load i32, i32* %nextcolsum, align 4, !tbaa !30
  %add20 = add nsw i32 %mul19, %41
  %add21 = add nsw i32 %add20, 7
  %shr22 = ashr i32 %add21, 4
  %conv23 = trunc i32 %shr22 to i8
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr24 = getelementptr inbounds i8, i8* %42, i32 1
  store i8* %incdec.ptr24, i8** %outptr, align 4, !tbaa !2
  store i8 %conv23, i8* %42, align 1, !tbaa !41
  %43 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  store i32 %43, i32* %lastcolsum, align 4, !tbaa !30
  %44 = load i32, i32* %nextcolsum, align 4, !tbaa !30
  store i32 %44, i32* %thiscolsum, align 4, !tbaa !30
  %45 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %downsampled_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %45, i32 0, i32 10
  %46 = load i32, i32* %downsampled_width, align 4, !tbaa !40
  %sub25 = sub i32 %46, 2
  store i32 %sub25, i32* %colctr, align 4, !tbaa !30
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc, %if.end
  %47 = load i32, i32* %colctr, align 4, !tbaa !30
  %cmp27 = icmp ugt i32 %47, 0
  br i1 %cmp27, label %for.body29, label %for.end

for.body29:                                       ; preds = %for.cond26
  %48 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr30 = getelementptr inbounds i8, i8* %48, i32 1
  store i8* %incdec.ptr30, i8** %inptr0, align 4, !tbaa !2
  %49 = load i8, i8* %48, align 1, !tbaa !41
  %conv31 = zext i8 %49 to i32
  %mul32 = mul nsw i32 %conv31, 3
  %50 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr33 = getelementptr inbounds i8, i8* %50, i32 1
  store i8* %incdec.ptr33, i8** %inptr1, align 4, !tbaa !2
  %51 = load i8, i8* %50, align 1, !tbaa !41
  %conv34 = zext i8 %51 to i32
  %add35 = add nsw i32 %mul32, %conv34
  store i32 %add35, i32* %nextcolsum, align 4, !tbaa !30
  %52 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  %mul36 = mul nsw i32 %52, 3
  %53 = load i32, i32* %lastcolsum, align 4, !tbaa !30
  %add37 = add nsw i32 %mul36, %53
  %add38 = add nsw i32 %add37, 8
  %shr39 = ashr i32 %add38, 4
  %conv40 = trunc i32 %shr39 to i8
  %54 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr41 = getelementptr inbounds i8, i8* %54, i32 1
  store i8* %incdec.ptr41, i8** %outptr, align 4, !tbaa !2
  store i8 %conv40, i8* %54, align 1, !tbaa !41
  %55 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  %mul42 = mul nsw i32 %55, 3
  %56 = load i32, i32* %nextcolsum, align 4, !tbaa !30
  %add43 = add nsw i32 %mul42, %56
  %add44 = add nsw i32 %add43, 7
  %shr45 = ashr i32 %add44, 4
  %conv46 = trunc i32 %shr45 to i8
  %57 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr47 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr47, i8** %outptr, align 4, !tbaa !2
  store i8 %conv46, i8* %57, align 1, !tbaa !41
  %58 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  store i32 %58, i32* %lastcolsum, align 4, !tbaa !30
  %59 = load i32, i32* %nextcolsum, align 4, !tbaa !30
  store i32 %59, i32* %thiscolsum, align 4, !tbaa !30
  br label %for.inc

for.inc:                                          ; preds = %for.body29
  %60 = load i32, i32* %colctr, align 4, !tbaa !30
  %dec = add i32 %60, -1
  store i32 %dec, i32* %colctr, align 4, !tbaa !30
  br label %for.cond26

for.end:                                          ; preds = %for.cond26
  %61 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  %mul48 = mul nsw i32 %61, 3
  %62 = load i32, i32* %lastcolsum, align 4, !tbaa !30
  %add49 = add nsw i32 %mul48, %62
  %add50 = add nsw i32 %add49, 8
  %shr51 = ashr i32 %add50, 4
  %conv52 = trunc i32 %shr51 to i8
  %63 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr53 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr53, i8** %outptr, align 4, !tbaa !2
  store i8 %conv52, i8* %63, align 1, !tbaa !41
  %64 = load i32, i32* %thiscolsum, align 4, !tbaa !30
  %mul54 = mul nsw i32 %64, 4
  %add55 = add nsw i32 %mul54, 7
  %shr56 = ashr i32 %add55, 4
  %conv57 = trunc i32 %shr56 to i8
  %65 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr58 = getelementptr inbounds i8, i8* %65, i32 1
  store i8* %incdec.ptr58, i8** %outptr, align 4, !tbaa !2
  store i8 %conv57, i8* %65, align 1, !tbaa !41
  br label %for.inc59

for.inc59:                                        ; preds = %for.end
  %66 = load i32, i32* %v, align 4, !tbaa !30
  %inc60 = add nsw i32 %66, 1
  store i32 %inc60, i32* %v, align 4, !tbaa !30
  br label %for.cond

for.end61:                                        ; preds = %for.cond
  %67 = load i32, i32* %inrow, align 4, !tbaa !30
  %inc62 = add nsw i32 %67, 1
  store i32 %inc62, i32* %inrow, align 4, !tbaa !30
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %68 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #3
  %69 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #3
  %70 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  %71 = bitcast i32* %colctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #3
  %72 = bitcast i32* %nextcolsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast i32* %lastcolsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  %74 = bitcast i32* %thiscolsum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %75 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %77 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  ret void
}

declare i32 @jsimd_can_h2v2_upsample() #2

declare void @jsimd_h2v2_upsample(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***) #2

; Function Attrs: nounwind
define internal void @h2v2_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  %output_data = alloca i8**, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %invalue = alloca i8, align 1
  %outend = alloca i8*, align 4
  %inrow = alloca i32, align 4
  %outrow = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %0 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8***, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %2 = load i8**, i8*** %1, align 4, !tbaa !2
  store i8** %2, i8*** %output_data, align 4, !tbaa !2
  %3 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %invalue) #3
  %5 = bitcast i8** %outend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store i32 0, i32* %outrow, align 4, !tbaa !30
  store i32 0, i32* %inrow, align 4, !tbaa !30
  br label %while.cond

while.cond:                                       ; preds = %while.end, %entry
  %8 = load i32, i32* %outrow, align 4, !tbaa !30
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 62
  %10 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  %cmp = icmp slt i32 %8, %10
  br i1 %cmp, label %while.body, label %while.end9

while.body:                                       ; preds = %while.cond
  %11 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %12 = load i32, i32* %inrow, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds i8*, i8** %11, i32 %12
  %13 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %13, i8** %inptr, align 4, !tbaa !2
  %14 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %15 = load i32, i32* %outrow, align 4, !tbaa !30
  %arrayidx1 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %16, i8** %outptr, align 4, !tbaa !2
  %17 = load i8*, i8** %outptr, align 4, !tbaa !2
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 27
  %19 = load i32, i32* %output_width, align 8, !tbaa !43
  %add.ptr = getelementptr inbounds i8, i8* %17, i32 %19
  store i8* %add.ptr, i8** %outend, align 4, !tbaa !2
  br label %while.cond2

while.cond2:                                      ; preds = %while.body4, %while.body
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %21 = load i8*, i8** %outend, align 4, !tbaa !2
  %cmp3 = icmp ult i8* %20, %21
  br i1 %cmp3, label %while.body4, label %while.end

while.body4:                                      ; preds = %while.cond2
  %22 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %22, i32 1
  store i8* %incdec.ptr, i8** %inptr, align 4, !tbaa !2
  %23 = load i8, i8* %22, align 1, !tbaa !41
  store i8 %23, i8* %invalue, align 1, !tbaa !41
  %24 = load i8, i8* %invalue, align 1, !tbaa !41
  %25 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr5 = getelementptr inbounds i8, i8* %25, i32 1
  store i8* %incdec.ptr5, i8** %outptr, align 4, !tbaa !2
  store i8 %24, i8* %25, align 1, !tbaa !41
  %26 = load i8, i8* %invalue, align 1, !tbaa !41
  %27 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr6 = getelementptr inbounds i8, i8* %27, i32 1
  store i8* %incdec.ptr6, i8** %outptr, align 4, !tbaa !2
  store i8 %26, i8* %27, align 1, !tbaa !41
  br label %while.cond2

while.end:                                        ; preds = %while.cond2
  %28 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %29 = load i32, i32* %outrow, align 4, !tbaa !30
  %30 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %31 = load i32, i32* %outrow, align 4, !tbaa !30
  %add = add nsw i32 %31, 1
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 27
  %33 = load i32, i32* %output_width7, align 8, !tbaa !43
  call void @jcopy_sample_rows(i8** %28, i32 %29, i8** %30, i32 %add, i32 1, i32 %33)
  %34 = load i32, i32* %inrow, align 4, !tbaa !30
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %inrow, align 4, !tbaa !30
  %35 = load i32, i32* %outrow, align 4, !tbaa !30
  %add8 = add nsw i32 %35, 2
  store i32 %add8, i32* %outrow, align 4, !tbaa !30
  br label %while.cond

while.end9:                                       ; preds = %while.cond
  %36 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  %37 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #3
  %38 = bitcast i8** %outend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %invalue) #3
  %39 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  %40 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  %41 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  ret void
}

; Function Attrs: nounwind
define internal void @int_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %output_data = alloca i8**, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %invalue = alloca i8, align 1
  %h = alloca i32, align 4
  %outend = alloca i8*, align 4
  %h_expand = alloca i32, align 4
  %v_expand = alloca i32, align 4
  %inrow = alloca i32, align 4
  %outrow = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !17
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load i8***, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  %6 = load i8**, i8*** %5, align 4, !tbaa !2
  store i8** %6, i8*** %output_data, align 4, !tbaa !2
  %7 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %invalue) #3
  %9 = bitcast i32* %h to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i8** %outend to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %h_expand to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %v_expand to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %h_expand2 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %15, i32 0, i32 6
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 0, i32 1
  %17 = load i32, i32* %component_index, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds [10 x i8], [10 x i8]* %h_expand2, i32 0, i32 %17
  %18 = load i8, i8* %arrayidx, align 1, !tbaa !41
  %conv = zext i8 %18 to i32
  store i32 %conv, i32* %h_expand, align 4, !tbaa !30
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %v_expand3 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 7
  %20 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %component_index4 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %20, i32 0, i32 1
  %21 = load i32, i32* %component_index4, align 4, !tbaa !50
  %arrayidx5 = getelementptr inbounds [10 x i8], [10 x i8]* %v_expand3, i32 0, i32 %21
  %22 = load i8, i8* %arrayidx5, align 1, !tbaa !41
  %conv6 = zext i8 %22 to i32
  store i32 %conv6, i32* %v_expand, align 4, !tbaa !30
  store i32 0, i32* %outrow, align 4, !tbaa !30
  store i32 0, i32* %inrow, align 4, !tbaa !30
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %23 = load i32, i32* %outrow, align 4, !tbaa !30
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 62
  %25 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !38
  %cmp = icmp slt i32 %23, %25
  br i1 %cmp, label %while.body, label %while.end21

while.body:                                       ; preds = %while.cond
  %26 = load i8**, i8*** %input_data.addr, align 4, !tbaa !2
  %27 = load i32, i32* %inrow, align 4, !tbaa !30
  %arrayidx8 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %28, i8** %inptr, align 4, !tbaa !2
  %29 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %30 = load i32, i32* %outrow, align 4, !tbaa !30
  %arrayidx9 = getelementptr inbounds i8*, i8** %29, i32 %30
  %31 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %31, i8** %outptr, align 4, !tbaa !2
  %32 = load i8*, i8** %outptr, align 4, !tbaa !2
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 27
  %34 = load i32, i32* %output_width, align 8, !tbaa !43
  %add.ptr = getelementptr inbounds i8, i8* %32, i32 %34
  store i8* %add.ptr, i8** %outend, align 4, !tbaa !2
  br label %while.cond10

while.cond10:                                     ; preds = %for.end, %while.body
  %35 = load i8*, i8** %outptr, align 4, !tbaa !2
  %36 = load i8*, i8** %outend, align 4, !tbaa !2
  %cmp11 = icmp ult i8* %35, %36
  br i1 %cmp11, label %while.body13, label %while.end

while.body13:                                     ; preds = %while.cond10
  %37 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %37, i32 1
  store i8* %incdec.ptr, i8** %inptr, align 4, !tbaa !2
  %38 = load i8, i8* %37, align 1, !tbaa !41
  store i8 %38, i8* %invalue, align 1, !tbaa !41
  %39 = load i32, i32* %h_expand, align 4, !tbaa !30
  store i32 %39, i32* %h, align 4, !tbaa !30
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body13
  %40 = load i32, i32* %h, align 4, !tbaa !30
  %cmp14 = icmp sgt i32 %40, 0
  br i1 %cmp14, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %41 = load i8, i8* %invalue, align 1, !tbaa !41
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr16 = getelementptr inbounds i8, i8* %42, i32 1
  store i8* %incdec.ptr16, i8** %outptr, align 4, !tbaa !2
  store i8 %41, i8* %42, align 1, !tbaa !41
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %43 = load i32, i32* %h, align 4, !tbaa !30
  %dec = add nsw i32 %43, -1
  store i32 %dec, i32* %h, align 4, !tbaa !30
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond10

while.end:                                        ; preds = %while.cond10
  %44 = load i32, i32* %v_expand, align 4, !tbaa !30
  %cmp17 = icmp sgt i32 %44, 1
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %45 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %46 = load i32, i32* %outrow, align 4, !tbaa !30
  %47 = load i8**, i8*** %output_data, align 4, !tbaa !2
  %48 = load i32, i32* %outrow, align 4, !tbaa !30
  %add = add nsw i32 %48, 1
  %49 = load i32, i32* %v_expand, align 4, !tbaa !30
  %sub = sub nsw i32 %49, 1
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 27
  %51 = load i32, i32* %output_width19, align 8, !tbaa !43
  call void @jcopy_sample_rows(i8** %45, i32 %46, i8** %47, i32 %add, i32 %sub, i32 %51)
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end
  %52 = load i32, i32* %inrow, align 4, !tbaa !30
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %inrow, align 4, !tbaa !30
  %53 = load i32, i32* %v_expand, align 4, !tbaa !30
  %54 = load i32, i32* %outrow, align 4, !tbaa !30
  %add20 = add nsw i32 %54, %53
  store i32 %add20, i32* %outrow, align 4, !tbaa !30
  br label %while.cond

while.end21:                                      ; preds = %while.cond
  %55 = bitcast i32* %outrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %inrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i32* %v_expand to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %h_expand to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast i8** %outend to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  %60 = bitcast i32* %h to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %invalue) #3
  %61 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #3
  %62 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #3
  %63 = bitcast i8*** %output_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #3
  %64 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #3
  ret void
}

declare i32 @jround_up(i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @jcopy_sample_rows(i8**, i32, i8**, i32, i32, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 420}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !8, i64 100}
!12 = !{!"jpeg_decomp_master", !3, i64 0, !3, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !4, i64 20, !4, i64 60, !8, i64 100}
!13 = !{!7, !3, i64 4}
!14 = !{!15, !3, i64 0}
!15 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !16, i64 44, !16, i64 48}
!16 = !{!"long", !4, i64 0}
!17 = !{!7, !3, i64 452}
!18 = !{!19, !3, i64 0}
!19 = !{!"", !20, i64 0, !4, i64 12, !4, i64 52, !8, i64 92, !8, i64 96, !4, i64 100, !4, i64 140, !4, i64 150}
!20 = !{!"jpeg_upsampler", !3, i64 0, !3, i64 4, !8, i64 8}
!21 = !{!19, !3, i64 4}
!22 = !{!19, !8, i64 8}
!23 = !{!7, !8, i64 300}
!24 = !{!7, !3, i64 0}
!25 = !{!26, !8, i64 20}
!26 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !16, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!27 = !{!26, !3, i64 0}
!28 = !{!7, !8, i64 76}
!29 = !{!7, !8, i64 316}
!30 = !{!8, !8, i64 0}
!31 = !{!7, !3, i64 216}
!32 = !{!7, !8, i64 36}
!33 = !{!34, !8, i64 8}
!34 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!35 = !{!34, !8, i64 36}
!36 = !{!34, !8, i64 12}
!37 = !{!7, !8, i64 308}
!38 = !{!7, !8, i64 312}
!39 = !{!34, !8, i64 48}
!40 = !{!34, !8, i64 40}
!41 = !{!4, !4, i64 0}
!42 = !{!15, !3, i64 8}
!43 = !{!7, !8, i64 112}
!44 = !{!19, !8, i64 92}
!45 = !{!7, !8, i64 116}
!46 = !{!19, !8, i64 96}
!47 = !{!7, !3, i64 456}
!48 = !{!49, !3, i64 4}
!49 = !{!"jpeg_color_deconverter", !3, i64 0, !3, i64 4}
!50 = !{!34, !8, i64 4}
