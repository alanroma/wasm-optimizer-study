; ModuleID = 'jsimd_none.c'
source_filename = "jsimd_none.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, {}* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, {}* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.c_derived_tbl = type { [256 x i32], [256 x i8] }

; Function Attrs: nounwind
define hidden i32 @jsimd_can_rgb_ycc() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_rgb_gray() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_ycc_rgb() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_ycc_rgb565() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_c_can_null_convert() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_rgb_ycc_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !6
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_rgb_gray_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !6
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_ycc_rgb_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !6
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_ycc_rgb565_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !6
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_c_null_convert(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i8*** %output_buf, i32 %output_row, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8***, align 4
  %output_row.addr = alloca i32, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_row, i32* %output_row.addr, align 4, !tbaa !6
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v2_downsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v1_downsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v2_smooth_downsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v2_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v2_smooth_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v1_downsample(%struct.jpeg_compress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8** %output_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data.addr = alloca i8**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8** %output_data, i8*** %output_data.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v2_upsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v1_upsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_int_upsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_int_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v2_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v1_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v2_fancy_upsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v1_fancy_upsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v2_fancy_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v1_fancy_upsample(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i8** %input_data, i8*** %output_data_ptr) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %input_data.addr = alloca i8**, align 4
  %output_data_ptr.addr = alloca i8***, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i8** %input_data, i8*** %input_data.addr, align 4, !tbaa !2
  store i8*** %output_data_ptr, i8**** %output_data_ptr.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v2_merged_upsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_h2v1_merged_upsample() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v2_merged_upsample(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !6
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_h2v1_merged_upsample(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !6
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_convsamp() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_convsamp_float() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_convsamp(i8** %sample_data, i32 %start_col, i32* %workspace) #0 {
entry:
  %sample_data.addr = alloca i8**, align 4
  %start_col.addr = alloca i32, align 4
  %workspace.addr = alloca i32*, align 4
  store i8** %sample_data, i8*** %sample_data.addr, align 4, !tbaa !2
  store i32 %start_col, i32* %start_col.addr, align 4, !tbaa !6
  store i32* %workspace, i32** %workspace.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_convsamp_float(i8** %sample_data, i32 %start_col, float* %workspace) #0 {
entry:
  %sample_data.addr = alloca i8**, align 4
  %start_col.addr = alloca i32, align 4
  %workspace.addr = alloca float*, align 4
  store i8** %sample_data, i8*** %sample_data.addr, align 4, !tbaa !2
  store i32 %start_col, i32* %start_col.addr, align 4, !tbaa !6
  store float* %workspace, float** %workspace.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_fdct_islow() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_fdct_ifast() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_fdct_float() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_fdct_islow(i32* %data) #0 {
entry:
  %data.addr = alloca i32*, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_fdct_ifast(i32* %data) #0 {
entry:
  %data.addr = alloca i32*, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_fdct_float(float* %data) #0 {
entry:
  %data.addr = alloca float*, align 4
  store float* %data, float** %data.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_quantize() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_quantize_float() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_quantize(i16* %coef_block, i32* %divisors, i32* %workspace) #0 {
entry:
  %coef_block.addr = alloca i16*, align 4
  %divisors.addr = alloca i32*, align 4
  %workspace.addr = alloca i32*, align 4
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i32* %divisors, i32** %divisors.addr, align 4, !tbaa !2
  store i32* %workspace, i32** %workspace.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_quantize_float(i16* %coef_block, float* %divisors, float* %workspace) #0 {
entry:
  %coef_block.addr = alloca i16*, align 4
  %divisors.addr = alloca float*, align 4
  %workspace.addr = alloca float*, align 4
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store float* %divisors, float** %divisors.addr, align 4, !tbaa !2
  store float* %workspace, float** %workspace.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_idct_2x2() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_idct_4x4() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_idct_6x6() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_idct_12x12() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_idct_2x2(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_idct_4x4(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_idct_6x6(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_idct_12x12(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_idct_islow() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_idct_ifast() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_idct_float() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jsimd_idct_islow(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_idct_ifast(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden void @jsimd_idct_float(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jsimd_can_huff_encode_one_block() #0 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i8* @jsimd_huff_encode_one_block(i8* %state, i8* %buffer, i16* %block, i32 %last_dc_val, %struct.c_derived_tbl* %dctbl, %struct.c_derived_tbl* %actbl) #0 {
entry:
  %state.addr = alloca i8*, align 4
  %buffer.addr = alloca i8*, align 4
  %block.addr = alloca i16*, align 4
  %last_dc_val.addr = alloca i32, align 4
  %dctbl.addr = alloca %struct.c_derived_tbl*, align 4
  %actbl.addr = alloca %struct.c_derived_tbl*, align 4
  store i8* %state, i8** %state.addr, align 4, !tbaa !2
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i16* %block, i16** %block.addr, align 4, !tbaa !2
  store i32 %last_dc_val, i32* %last_dc_val.addr, align 4, !tbaa !6
  store %struct.c_derived_tbl* %dctbl, %struct.c_derived_tbl** %dctbl.addr, align 4, !tbaa !2
  store %struct.c_derived_tbl* %actbl, %struct.c_derived_tbl** %actbl.addr, align 4, !tbaa !2
  ret i8* null
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
