; ModuleID = 'jmemnobs.c'
source_filename = "jmemnobs.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, void (%struct.jpeg_common_struct*)*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { void (%struct.jpeg_common_struct*)*, i32, i32, i32, i32 }
%struct.backing_store_struct = type { void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*, i8*, i32, i32)*, void (%struct.jpeg_common_struct*, %struct.backing_store_struct*)*, %struct._IO_FILE*, [64 x i8] }
%struct._IO_FILE = type opaque

; Function Attrs: nounwind
define hidden i8* @jpeg_get_small(%struct.jpeg_common_struct* %cinfo, i32 %sizeofobject) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %sizeofobject.addr = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %sizeofobject, i32* %sizeofobject.addr, align 4, !tbaa !6
  %0 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !6
  %call = call i8* @malloc(i32 %0)
  ret i8* %call
}

declare i8* @malloc(i32) #1

; Function Attrs: nounwind
define hidden void @jpeg_free_small(%struct.jpeg_common_struct* %cinfo, i8* %object, i32 %sizeofobject) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %object.addr = alloca i8*, align 4
  %sizeofobject.addr = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %object, i8** %object.addr, align 4, !tbaa !2
  store i32 %sizeofobject, i32* %sizeofobject.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %object.addr, align 4, !tbaa !2
  call void @free(i8* %0)
  ret void
}

declare void @free(i8*) #1

; Function Attrs: nounwind
define hidden i8* @jpeg_get_large(%struct.jpeg_common_struct* %cinfo, i32 %sizeofobject) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %sizeofobject.addr = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %sizeofobject, i32* %sizeofobject.addr, align 4, !tbaa !6
  %0 = load i32, i32* %sizeofobject.addr, align 4, !tbaa !6
  %call = call i8* @malloc(i32 %0)
  ret i8* %call
}

; Function Attrs: nounwind
define hidden void @jpeg_free_large(%struct.jpeg_common_struct* %cinfo, i8* %object, i32 %sizeofobject) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %object.addr = alloca i8*, align 4
  %sizeofobject.addr = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %object, i8** %object.addr, align 4, !tbaa !2
  store i32 %sizeofobject, i32* %sizeofobject.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %object.addr, align 4, !tbaa !2
  call void @free(i8* %0)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jpeg_mem_available(%struct.jpeg_common_struct* %cinfo, i32 %min_bytes_needed, i32 %max_bytes_needed, i32 %already_allocated) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %min_bytes_needed.addr = alloca i32, align 4
  %max_bytes_needed.addr = alloca i32, align 4
  %already_allocated.addr = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %min_bytes_needed, i32* %min_bytes_needed.addr, align 4, !tbaa !6
  store i32 %max_bytes_needed, i32* %max_bytes_needed.addr, align 4, !tbaa !6
  store i32 %already_allocated, i32* %already_allocated.addr, align 4, !tbaa !6
  %0 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %0, i32 0, i32 1
  %1 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %max_memory_to_use = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %1, i32 0, i32 11
  %2 = load i32, i32* %max_memory_to_use, align 4, !tbaa !11
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.else6

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %3, i32 0, i32 1
  %4 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem1, align 4, !tbaa !8
  %max_memory_to_use2 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %4, i32 0, i32 11
  %5 = load i32, i32* %max_memory_to_use2, align 4, !tbaa !11
  %6 = load i32, i32* %already_allocated.addr, align 4, !tbaa !6
  %cmp = icmp ugt i32 %5, %6
  br i1 %cmp, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %7 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %mem4 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %7, i32 0, i32 1
  %8 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem4, align 4, !tbaa !8
  %max_memory_to_use5 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %8, i32 0, i32 11
  %9 = load i32, i32* %max_memory_to_use5, align 4, !tbaa !11
  %10 = load i32, i32* %already_allocated.addr, align 4, !tbaa !6
  %sub = sub i32 %9, %10
  store i32 %sub, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %if.then
  store i32 0, i32* %retval, align 4
  br label %return

if.else6:                                         ; preds = %entry
  %11 = load i32, i32* %max_bytes_needed.addr, align 4, !tbaa !6
  store i32 %11, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else6, %if.else, %if.then3
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: nounwind
define hidden void @jpeg_open_backing_store(%struct.jpeg_common_struct* %cinfo, %struct.backing_store_struct* %info, i32 %total_bytes_needed) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  %info.addr = alloca %struct.backing_store_struct*, align 4
  %total_bytes_needed.addr = alloca i32, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.backing_store_struct* %info, %struct.backing_store_struct** %info.addr, align 4, !tbaa !2
  store i32 %total_bytes_needed, i32* %total_bytes_needed.addr, align 4, !tbaa !6
  %0 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %0, i32 0, i32 0
  %1 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 4, !tbaa !13
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %1, i32 0, i32 5
  store i32 49, i32* %msg_code, align 4, !tbaa !14
  %2 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_common_struct, %struct.jpeg_common_struct* %2, i32 0, i32 0
  %3 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 4, !tbaa !13
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %3, i32 0, i32 0
  %4 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !16
  %5 = load %struct.jpeg_common_struct*, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  call void %4(%struct.jpeg_common_struct* %5)
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jpeg_mem_init(%struct.jpeg_common_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  ret i32 0
}

; Function Attrs: nounwind
define hidden void @jpeg_mem_term(%struct.jpeg_common_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_common_struct*, align 4
  store %struct.jpeg_common_struct* %cinfo, %struct.jpeg_common_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"jpeg_common_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !10, i64 16, !10, i64 20}
!10 = !{!"int", !4, i64 0}
!11 = !{!12, !7, i64 44}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !7, i64 44, !7, i64 48}
!13 = !{!9, !3, i64 0}
!14 = !{!15, !10, i64 20}
!15 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !10, i64 20, !4, i64 24, !10, i64 104, !7, i64 108, !3, i64 112, !10, i64 116, !3, i64 120, !10, i64 124, !10, i64 128}
!16 = !{!15, !3, i64 0}
