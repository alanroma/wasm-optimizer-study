; ModuleID = 'jidctint.c'
source_filename = "jidctint.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x {}*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }

; Function Attrs: nounwind
define hidden void @jpeg_idct_islow(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp3 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %z5 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [64 x i32], align 16
  %dcval = alloca i32, align 4
  %dcval182 = alloca i8, align 1
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %19, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = bitcast [64 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %21) #2
  %22 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %22, i16** %inptr, align 4, !tbaa !2
  %23 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %23, i32 0, i32 20
  %24 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %25 = bitcast i8* %24 to i32*
  store i32* %25, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 8, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %26 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %26, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %27 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %27, i32 8
  %28 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %28 to i32
  %cmp1 = icmp eq i32 %conv, 0
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %29 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i16, i16* %29, i32 16
  %30 = load i16, i16* %arrayidx3, align 2, !tbaa !14
  %conv4 = sext i16 %30 to i32
  %cmp5 = icmp eq i32 %conv4, 0
  br i1 %cmp5, label %land.lhs.true7, label %if.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %31 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i16, i16* %31, i32 24
  %32 = load i16, i16* %arrayidx8, align 2, !tbaa !14
  %conv9 = sext i16 %32 to i32
  %cmp10 = icmp eq i32 %conv9, 0
  br i1 %cmp10, label %land.lhs.true12, label %if.end

land.lhs.true12:                                  ; preds = %land.lhs.true7
  %33 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i16, i16* %33, i32 32
  %34 = load i16, i16* %arrayidx13, align 2, !tbaa !14
  %conv14 = sext i16 %34 to i32
  %cmp15 = icmp eq i32 %conv14, 0
  br i1 %cmp15, label %land.lhs.true17, label %if.end

land.lhs.true17:                                  ; preds = %land.lhs.true12
  %35 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i16, i16* %35, i32 40
  %36 = load i16, i16* %arrayidx18, align 2, !tbaa !14
  %conv19 = sext i16 %36 to i32
  %cmp20 = icmp eq i32 %conv19, 0
  br i1 %cmp20, label %land.lhs.true22, label %if.end

land.lhs.true22:                                  ; preds = %land.lhs.true17
  %37 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i16, i16* %37, i32 48
  %38 = load i16, i16* %arrayidx23, align 2, !tbaa !14
  %conv24 = sext i16 %38 to i32
  %cmp25 = icmp eq i32 %conv24, 0
  br i1 %cmp25, label %land.lhs.true27, label %if.end

land.lhs.true27:                                  ; preds = %land.lhs.true22
  %39 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i16, i16* %39, i32 56
  %40 = load i16, i16* %arrayidx28, align 2, !tbaa !14
  %conv29 = sext i16 %40 to i32
  %cmp30 = icmp eq i32 %conv29, 0
  br i1 %cmp30, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true27
  %41 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #2
  %42 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i16, i16* %42, i32 0
  %43 = load i16, i16* %arrayidx32, align 2, !tbaa !14
  %conv33 = sext i16 %43 to i32
  %44 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %44, i32 0
  %45 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %mul = mul nsw i32 %conv33, %45
  %shl = shl i32 %mul, 2
  store i32 %shl, i32* %dcval, align 4, !tbaa !6
  %46 = load i32, i32* %dcval, align 4, !tbaa !6
  %47 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %47, i32 0
  store i32 %46, i32* %arrayidx35, align 4, !tbaa !6
  %48 = load i32, i32* %dcval, align 4, !tbaa !6
  %49 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %49, i32 8
  store i32 %48, i32* %arrayidx36, align 4, !tbaa !6
  %50 = load i32, i32* %dcval, align 4, !tbaa !6
  %51 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %51, i32 16
  store i32 %50, i32* %arrayidx37, align 4, !tbaa !6
  %52 = load i32, i32* %dcval, align 4, !tbaa !6
  %53 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %53, i32 24
  store i32 %52, i32* %arrayidx38, align 4, !tbaa !6
  %54 = load i32, i32* %dcval, align 4, !tbaa !6
  %55 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %55, i32 32
  store i32 %54, i32* %arrayidx39, align 4, !tbaa !6
  %56 = load i32, i32* %dcval, align 4, !tbaa !6
  %57 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %57, i32 40
  store i32 %56, i32* %arrayidx40, align 4, !tbaa !6
  %58 = load i32, i32* %dcval, align 4, !tbaa !6
  %59 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %59, i32 48
  store i32 %58, i32* %arrayidx41, align 4, !tbaa !6
  %60 = load i32, i32* %dcval, align 4, !tbaa !6
  %61 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %61, i32 56
  store i32 %60, i32* %arrayidx42, align 4, !tbaa !6
  %62 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %62, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %63 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr43 = getelementptr inbounds i32, i32* %63, i32 1
  store i32* %incdec.ptr43, i32** %quantptr, align 4, !tbaa !2
  %64 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr44 = getelementptr inbounds i32, i32* %64, i32 1
  store i32* %incdec.ptr44, i32** %wsptr, align 4, !tbaa !2
  %65 = bitcast i32* %dcval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #2
  br label %for.inc

if.end:                                           ; preds = %land.lhs.true27, %land.lhs.true22, %land.lhs.true17, %land.lhs.true12, %land.lhs.true7, %land.lhs.true, %for.body
  %66 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i16, i16* %66, i32 16
  %67 = load i16, i16* %arrayidx45, align 2, !tbaa !14
  %conv46 = sext i16 %67 to i32
  %68 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %68, i32 16
  %69 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %mul48 = mul nsw i32 %conv46, %69
  store i32 %mul48, i32* %z2, align 4, !tbaa !15
  %70 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i16, i16* %70, i32 48
  %71 = load i16, i16* %arrayidx49, align 2, !tbaa !14
  %conv50 = sext i16 %71 to i32
  %72 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %72, i32 48
  %73 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %mul52 = mul nsw i32 %conv50, %73
  store i32 %mul52, i32* %z3, align 4, !tbaa !15
  %74 = load i32, i32* %z2, align 4, !tbaa !15
  %75 = load i32, i32* %z3, align 4, !tbaa !15
  %add = add nsw i32 %74, %75
  %mul53 = mul nsw i32 %add, 4433
  store i32 %mul53, i32* %z1, align 4, !tbaa !15
  %76 = load i32, i32* %z1, align 4, !tbaa !15
  %77 = load i32, i32* %z3, align 4, !tbaa !15
  %mul54 = mul nsw i32 %77, -15137
  %add55 = add nsw i32 %76, %mul54
  store i32 %add55, i32* %tmp2, align 4, !tbaa !15
  %78 = load i32, i32* %z1, align 4, !tbaa !15
  %79 = load i32, i32* %z2, align 4, !tbaa !15
  %mul56 = mul nsw i32 %79, 6270
  %add57 = add nsw i32 %78, %mul56
  store i32 %add57, i32* %tmp3, align 4, !tbaa !15
  %80 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i16, i16* %80, i32 0
  %81 = load i16, i16* %arrayidx58, align 2, !tbaa !14
  %conv59 = sext i16 %81 to i32
  %82 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i32, i32* %82, i32 0
  %83 = load i32, i32* %arrayidx60, align 4, !tbaa !6
  %mul61 = mul nsw i32 %conv59, %83
  store i32 %mul61, i32* %z2, align 4, !tbaa !15
  %84 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i16, i16* %84, i32 32
  %85 = load i16, i16* %arrayidx62, align 2, !tbaa !14
  %conv63 = sext i16 %85 to i32
  %86 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %86, i32 32
  %87 = load i32, i32* %arrayidx64, align 4, !tbaa !6
  %mul65 = mul nsw i32 %conv63, %87
  store i32 %mul65, i32* %z3, align 4, !tbaa !15
  %88 = load i32, i32* %z2, align 4, !tbaa !15
  %89 = load i32, i32* %z3, align 4, !tbaa !15
  %add66 = add nsw i32 %88, %89
  %shl67 = shl i32 %add66, 13
  store i32 %shl67, i32* %tmp0, align 4, !tbaa !15
  %90 = load i32, i32* %z2, align 4, !tbaa !15
  %91 = load i32, i32* %z3, align 4, !tbaa !15
  %sub = sub nsw i32 %90, %91
  %shl68 = shl i32 %sub, 13
  store i32 %shl68, i32* %tmp1, align 4, !tbaa !15
  %92 = load i32, i32* %tmp0, align 4, !tbaa !15
  %93 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add69 = add nsw i32 %92, %93
  store i32 %add69, i32* %tmp10, align 4, !tbaa !15
  %94 = load i32, i32* %tmp0, align 4, !tbaa !15
  %95 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub70 = sub nsw i32 %94, %95
  store i32 %sub70, i32* %tmp13, align 4, !tbaa !15
  %96 = load i32, i32* %tmp1, align 4, !tbaa !15
  %97 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add71 = add nsw i32 %96, %97
  store i32 %add71, i32* %tmp11, align 4, !tbaa !15
  %98 = load i32, i32* %tmp1, align 4, !tbaa !15
  %99 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub72 = sub nsw i32 %98, %99
  store i32 %sub72, i32* %tmp12, align 4, !tbaa !15
  %100 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i16, i16* %100, i32 56
  %101 = load i16, i16* %arrayidx73, align 2, !tbaa !14
  %conv74 = sext i16 %101 to i32
  %102 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds i32, i32* %102, i32 56
  %103 = load i32, i32* %arrayidx75, align 4, !tbaa !6
  %mul76 = mul nsw i32 %conv74, %103
  store i32 %mul76, i32* %tmp0, align 4, !tbaa !15
  %104 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i16, i16* %104, i32 40
  %105 = load i16, i16* %arrayidx77, align 2, !tbaa !14
  %conv78 = sext i16 %105 to i32
  %106 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %106, i32 40
  %107 = load i32, i32* %arrayidx79, align 4, !tbaa !6
  %mul80 = mul nsw i32 %conv78, %107
  store i32 %mul80, i32* %tmp1, align 4, !tbaa !15
  %108 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i16, i16* %108, i32 24
  %109 = load i16, i16* %arrayidx81, align 2, !tbaa !14
  %conv82 = sext i16 %109 to i32
  %110 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %110, i32 24
  %111 = load i32, i32* %arrayidx83, align 4, !tbaa !6
  %mul84 = mul nsw i32 %conv82, %111
  store i32 %mul84, i32* %tmp2, align 4, !tbaa !15
  %112 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i16, i16* %112, i32 8
  %113 = load i16, i16* %arrayidx85, align 2, !tbaa !14
  %conv86 = sext i16 %113 to i32
  %114 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx87 = getelementptr inbounds i32, i32* %114, i32 8
  %115 = load i32, i32* %arrayidx87, align 4, !tbaa !6
  %mul88 = mul nsw i32 %conv86, %115
  store i32 %mul88, i32* %tmp3, align 4, !tbaa !15
  %116 = load i32, i32* %tmp0, align 4, !tbaa !15
  %117 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add89 = add nsw i32 %116, %117
  store i32 %add89, i32* %z1, align 4, !tbaa !15
  %118 = load i32, i32* %tmp1, align 4, !tbaa !15
  %119 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add90 = add nsw i32 %118, %119
  store i32 %add90, i32* %z2, align 4, !tbaa !15
  %120 = load i32, i32* %tmp0, align 4, !tbaa !15
  %121 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add91 = add nsw i32 %120, %121
  store i32 %add91, i32* %z3, align 4, !tbaa !15
  %122 = load i32, i32* %tmp1, align 4, !tbaa !15
  %123 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add92 = add nsw i32 %122, %123
  store i32 %add92, i32* %z4, align 4, !tbaa !15
  %124 = load i32, i32* %z3, align 4, !tbaa !15
  %125 = load i32, i32* %z4, align 4, !tbaa !15
  %add93 = add nsw i32 %124, %125
  %mul94 = mul nsw i32 %add93, 9633
  store i32 %mul94, i32* %z5, align 4, !tbaa !15
  %126 = load i32, i32* %tmp0, align 4, !tbaa !15
  %mul95 = mul nsw i32 %126, 2446
  store i32 %mul95, i32* %tmp0, align 4, !tbaa !15
  %127 = load i32, i32* %tmp1, align 4, !tbaa !15
  %mul96 = mul nsw i32 %127, 16819
  store i32 %mul96, i32* %tmp1, align 4, !tbaa !15
  %128 = load i32, i32* %tmp2, align 4, !tbaa !15
  %mul97 = mul nsw i32 %128, 25172
  store i32 %mul97, i32* %tmp2, align 4, !tbaa !15
  %129 = load i32, i32* %tmp3, align 4, !tbaa !15
  %mul98 = mul nsw i32 %129, 12299
  store i32 %mul98, i32* %tmp3, align 4, !tbaa !15
  %130 = load i32, i32* %z1, align 4, !tbaa !15
  %mul99 = mul nsw i32 %130, -7373
  store i32 %mul99, i32* %z1, align 4, !tbaa !15
  %131 = load i32, i32* %z2, align 4, !tbaa !15
  %mul100 = mul nsw i32 %131, -20995
  store i32 %mul100, i32* %z2, align 4, !tbaa !15
  %132 = load i32, i32* %z3, align 4, !tbaa !15
  %mul101 = mul nsw i32 %132, -16069
  store i32 %mul101, i32* %z3, align 4, !tbaa !15
  %133 = load i32, i32* %z4, align 4, !tbaa !15
  %mul102 = mul nsw i32 %133, -3196
  store i32 %mul102, i32* %z4, align 4, !tbaa !15
  %134 = load i32, i32* %z5, align 4, !tbaa !15
  %135 = load i32, i32* %z3, align 4, !tbaa !15
  %add103 = add nsw i32 %135, %134
  store i32 %add103, i32* %z3, align 4, !tbaa !15
  %136 = load i32, i32* %z5, align 4, !tbaa !15
  %137 = load i32, i32* %z4, align 4, !tbaa !15
  %add104 = add nsw i32 %137, %136
  store i32 %add104, i32* %z4, align 4, !tbaa !15
  %138 = load i32, i32* %z1, align 4, !tbaa !15
  %139 = load i32, i32* %z3, align 4, !tbaa !15
  %add105 = add nsw i32 %138, %139
  %140 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add106 = add nsw i32 %140, %add105
  store i32 %add106, i32* %tmp0, align 4, !tbaa !15
  %141 = load i32, i32* %z2, align 4, !tbaa !15
  %142 = load i32, i32* %z4, align 4, !tbaa !15
  %add107 = add nsw i32 %141, %142
  %143 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add108 = add nsw i32 %143, %add107
  store i32 %add108, i32* %tmp1, align 4, !tbaa !15
  %144 = load i32, i32* %z2, align 4, !tbaa !15
  %145 = load i32, i32* %z3, align 4, !tbaa !15
  %add109 = add nsw i32 %144, %145
  %146 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add110 = add nsw i32 %146, %add109
  store i32 %add110, i32* %tmp2, align 4, !tbaa !15
  %147 = load i32, i32* %z1, align 4, !tbaa !15
  %148 = load i32, i32* %z4, align 4, !tbaa !15
  %add111 = add nsw i32 %147, %148
  %149 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add112 = add nsw i32 %149, %add111
  store i32 %add112, i32* %tmp3, align 4, !tbaa !15
  %150 = load i32, i32* %tmp10, align 4, !tbaa !15
  %151 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add113 = add nsw i32 %150, %151
  %add114 = add nsw i32 %add113, 1024
  %shr = ashr i32 %add114, 11
  %152 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %152, i32 0
  store i32 %shr, i32* %arrayidx115, align 4, !tbaa !6
  %153 = load i32, i32* %tmp10, align 4, !tbaa !15
  %154 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub116 = sub nsw i32 %153, %154
  %add117 = add nsw i32 %sub116, 1024
  %shr118 = ashr i32 %add117, 11
  %155 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i32, i32* %155, i32 56
  store i32 %shr118, i32* %arrayidx119, align 4, !tbaa !6
  %156 = load i32, i32* %tmp11, align 4, !tbaa !15
  %157 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add120 = add nsw i32 %156, %157
  %add121 = add nsw i32 %add120, 1024
  %shr122 = ashr i32 %add121, 11
  %158 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %158, i32 8
  store i32 %shr122, i32* %arrayidx123, align 4, !tbaa !6
  %159 = load i32, i32* %tmp11, align 4, !tbaa !15
  %160 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub124 = sub nsw i32 %159, %160
  %add125 = add nsw i32 %sub124, 1024
  %shr126 = ashr i32 %add125, 11
  %161 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %161, i32 48
  store i32 %shr126, i32* %arrayidx127, align 4, !tbaa !6
  %162 = load i32, i32* %tmp12, align 4, !tbaa !15
  %163 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add128 = add nsw i32 %162, %163
  %add129 = add nsw i32 %add128, 1024
  %shr130 = ashr i32 %add129, 11
  %164 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i32, i32* %164, i32 16
  store i32 %shr130, i32* %arrayidx131, align 4, !tbaa !6
  %165 = load i32, i32* %tmp12, align 4, !tbaa !15
  %166 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub132 = sub nsw i32 %165, %166
  %add133 = add nsw i32 %sub132, 1024
  %shr134 = ashr i32 %add133, 11
  %167 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i32, i32* %167, i32 40
  store i32 %shr134, i32* %arrayidx135, align 4, !tbaa !6
  %168 = load i32, i32* %tmp13, align 4, !tbaa !15
  %169 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add136 = add nsw i32 %168, %169
  %add137 = add nsw i32 %add136, 1024
  %shr138 = ashr i32 %add137, 11
  %170 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i32, i32* %170, i32 24
  store i32 %shr138, i32* %arrayidx139, align 4, !tbaa !6
  %171 = load i32, i32* %tmp13, align 4, !tbaa !15
  %172 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub140 = sub nsw i32 %171, %172
  %add141 = add nsw i32 %sub140, 1024
  %shr142 = ashr i32 %add141, 11
  %173 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds i32, i32* %173, i32 32
  store i32 %shr142, i32* %arrayidx143, align 4, !tbaa !6
  %174 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr144 = getelementptr inbounds i16, i16* %174, i32 1
  store i16* %incdec.ptr144, i16** %inptr, align 4, !tbaa !2
  %175 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr145 = getelementptr inbounds i32, i32* %175, i32 1
  store i32* %incdec.ptr145, i32** %quantptr, align 4, !tbaa !2
  %176 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr146 = getelementptr inbounds i32, i32* %176, i32 1
  store i32* %incdec.ptr146, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %if.end, %if.then
  %177 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec = add nsw i32 %177, -1
  store i32 %dec, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay147 = getelementptr inbounds [64 x i32], [64 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay147, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond148

for.cond148:                                      ; preds = %for.inc294, %for.end
  %178 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp149 = icmp slt i32 %178, 8
  br i1 %cmp149, label %for.body151, label %for.end295

for.body151:                                      ; preds = %for.cond148
  %179 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %180 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx152 = getelementptr inbounds i8*, i8** %179, i32 %180
  %181 = load i8*, i8** %arrayidx152, align 4, !tbaa !2
  %182 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr153 = getelementptr inbounds i8, i8* %181, i32 %182
  store i8* %add.ptr153, i8** %outptr, align 4, !tbaa !2
  %183 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds i32, i32* %183, i32 1
  %184 = load i32, i32* %arrayidx154, align 4, !tbaa !6
  %cmp155 = icmp eq i32 %184, 0
  br i1 %cmp155, label %land.lhs.true157, label %if.end196

land.lhs.true157:                                 ; preds = %for.body151
  %185 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx158 = getelementptr inbounds i32, i32* %185, i32 2
  %186 = load i32, i32* %arrayidx158, align 4, !tbaa !6
  %cmp159 = icmp eq i32 %186, 0
  br i1 %cmp159, label %land.lhs.true161, label %if.end196

land.lhs.true161:                                 ; preds = %land.lhs.true157
  %187 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx162 = getelementptr inbounds i32, i32* %187, i32 3
  %188 = load i32, i32* %arrayidx162, align 4, !tbaa !6
  %cmp163 = icmp eq i32 %188, 0
  br i1 %cmp163, label %land.lhs.true165, label %if.end196

land.lhs.true165:                                 ; preds = %land.lhs.true161
  %189 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i32, i32* %189, i32 4
  %190 = load i32, i32* %arrayidx166, align 4, !tbaa !6
  %cmp167 = icmp eq i32 %190, 0
  br i1 %cmp167, label %land.lhs.true169, label %if.end196

land.lhs.true169:                                 ; preds = %land.lhs.true165
  %191 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx170 = getelementptr inbounds i32, i32* %191, i32 5
  %192 = load i32, i32* %arrayidx170, align 4, !tbaa !6
  %cmp171 = icmp eq i32 %192, 0
  br i1 %cmp171, label %land.lhs.true173, label %if.end196

land.lhs.true173:                                 ; preds = %land.lhs.true169
  %193 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i32, i32* %193, i32 6
  %194 = load i32, i32* %arrayidx174, align 4, !tbaa !6
  %cmp175 = icmp eq i32 %194, 0
  br i1 %cmp175, label %land.lhs.true177, label %if.end196

land.lhs.true177:                                 ; preds = %land.lhs.true173
  %195 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx178 = getelementptr inbounds i32, i32* %195, i32 7
  %196 = load i32, i32* %arrayidx178, align 4, !tbaa !6
  %cmp179 = icmp eq i32 %196, 0
  br i1 %cmp179, label %if.then181, label %if.end196

if.then181:                                       ; preds = %land.lhs.true177
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %dcval182) #2
  %197 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %198 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx183 = getelementptr inbounds i32, i32* %198, i32 0
  %199 = load i32, i32* %arrayidx183, align 4, !tbaa !6
  %add184 = add nsw i32 %199, 16
  %shr185 = ashr i32 %add184, 5
  %and = and i32 %shr185, 1023
  %arrayidx186 = getelementptr inbounds i8, i8* %197, i32 %and
  %200 = load i8, i8* %arrayidx186, align 1, !tbaa !17
  store i8 %200, i8* %dcval182, align 1, !tbaa !17
  %201 = load i8, i8* %dcval182, align 1, !tbaa !17
  %202 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx187 = getelementptr inbounds i8, i8* %202, i32 0
  store i8 %201, i8* %arrayidx187, align 1, !tbaa !17
  %203 = load i8, i8* %dcval182, align 1, !tbaa !17
  %204 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx188 = getelementptr inbounds i8, i8* %204, i32 1
  store i8 %203, i8* %arrayidx188, align 1, !tbaa !17
  %205 = load i8, i8* %dcval182, align 1, !tbaa !17
  %206 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx189 = getelementptr inbounds i8, i8* %206, i32 2
  store i8 %205, i8* %arrayidx189, align 1, !tbaa !17
  %207 = load i8, i8* %dcval182, align 1, !tbaa !17
  %208 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx190 = getelementptr inbounds i8, i8* %208, i32 3
  store i8 %207, i8* %arrayidx190, align 1, !tbaa !17
  %209 = load i8, i8* %dcval182, align 1, !tbaa !17
  %210 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx191 = getelementptr inbounds i8, i8* %210, i32 4
  store i8 %209, i8* %arrayidx191, align 1, !tbaa !17
  %211 = load i8, i8* %dcval182, align 1, !tbaa !17
  %212 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx192 = getelementptr inbounds i8, i8* %212, i32 5
  store i8 %211, i8* %arrayidx192, align 1, !tbaa !17
  %213 = load i8, i8* %dcval182, align 1, !tbaa !17
  %214 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx193 = getelementptr inbounds i8, i8* %214, i32 6
  store i8 %213, i8* %arrayidx193, align 1, !tbaa !17
  %215 = load i8, i8* %dcval182, align 1, !tbaa !17
  %216 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds i8, i8* %216, i32 7
  store i8 %215, i8* %arrayidx194, align 1, !tbaa !17
  %217 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr195 = getelementptr inbounds i32, i32* %217, i32 8
  store i32* %add.ptr195, i32** %wsptr, align 4, !tbaa !2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %dcval182) #2
  br label %for.inc294

if.end196:                                        ; preds = %land.lhs.true177, %land.lhs.true173, %land.lhs.true169, %land.lhs.true165, %land.lhs.true161, %land.lhs.true157, %for.body151
  %218 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx197 = getelementptr inbounds i32, i32* %218, i32 2
  %219 = load i32, i32* %arrayidx197, align 4, !tbaa !6
  store i32 %219, i32* %z2, align 4, !tbaa !15
  %220 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx198 = getelementptr inbounds i32, i32* %220, i32 6
  %221 = load i32, i32* %arrayidx198, align 4, !tbaa !6
  store i32 %221, i32* %z3, align 4, !tbaa !15
  %222 = load i32, i32* %z2, align 4, !tbaa !15
  %223 = load i32, i32* %z3, align 4, !tbaa !15
  %add199 = add nsw i32 %222, %223
  %mul200 = mul nsw i32 %add199, 4433
  store i32 %mul200, i32* %z1, align 4, !tbaa !15
  %224 = load i32, i32* %z1, align 4, !tbaa !15
  %225 = load i32, i32* %z3, align 4, !tbaa !15
  %mul201 = mul nsw i32 %225, -15137
  %add202 = add nsw i32 %224, %mul201
  store i32 %add202, i32* %tmp2, align 4, !tbaa !15
  %226 = load i32, i32* %z1, align 4, !tbaa !15
  %227 = load i32, i32* %z2, align 4, !tbaa !15
  %mul203 = mul nsw i32 %227, 6270
  %add204 = add nsw i32 %226, %mul203
  store i32 %add204, i32* %tmp3, align 4, !tbaa !15
  %228 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx205 = getelementptr inbounds i32, i32* %228, i32 0
  %229 = load i32, i32* %arrayidx205, align 4, !tbaa !6
  %230 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx206 = getelementptr inbounds i32, i32* %230, i32 4
  %231 = load i32, i32* %arrayidx206, align 4, !tbaa !6
  %add207 = add nsw i32 %229, %231
  %shl208 = shl i32 %add207, 13
  store i32 %shl208, i32* %tmp0, align 4, !tbaa !15
  %232 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx209 = getelementptr inbounds i32, i32* %232, i32 0
  %233 = load i32, i32* %arrayidx209, align 4, !tbaa !6
  %234 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx210 = getelementptr inbounds i32, i32* %234, i32 4
  %235 = load i32, i32* %arrayidx210, align 4, !tbaa !6
  %sub211 = sub nsw i32 %233, %235
  %shl212 = shl i32 %sub211, 13
  store i32 %shl212, i32* %tmp1, align 4, !tbaa !15
  %236 = load i32, i32* %tmp0, align 4, !tbaa !15
  %237 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add213 = add nsw i32 %236, %237
  store i32 %add213, i32* %tmp10, align 4, !tbaa !15
  %238 = load i32, i32* %tmp0, align 4, !tbaa !15
  %239 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub214 = sub nsw i32 %238, %239
  store i32 %sub214, i32* %tmp13, align 4, !tbaa !15
  %240 = load i32, i32* %tmp1, align 4, !tbaa !15
  %241 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add215 = add nsw i32 %240, %241
  store i32 %add215, i32* %tmp11, align 4, !tbaa !15
  %242 = load i32, i32* %tmp1, align 4, !tbaa !15
  %243 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub216 = sub nsw i32 %242, %243
  store i32 %sub216, i32* %tmp12, align 4, !tbaa !15
  %244 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx217 = getelementptr inbounds i32, i32* %244, i32 7
  %245 = load i32, i32* %arrayidx217, align 4, !tbaa !6
  store i32 %245, i32* %tmp0, align 4, !tbaa !15
  %246 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx218 = getelementptr inbounds i32, i32* %246, i32 5
  %247 = load i32, i32* %arrayidx218, align 4, !tbaa !6
  store i32 %247, i32* %tmp1, align 4, !tbaa !15
  %248 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx219 = getelementptr inbounds i32, i32* %248, i32 3
  %249 = load i32, i32* %arrayidx219, align 4, !tbaa !6
  store i32 %249, i32* %tmp2, align 4, !tbaa !15
  %250 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx220 = getelementptr inbounds i32, i32* %250, i32 1
  %251 = load i32, i32* %arrayidx220, align 4, !tbaa !6
  store i32 %251, i32* %tmp3, align 4, !tbaa !15
  %252 = load i32, i32* %tmp0, align 4, !tbaa !15
  %253 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add221 = add nsw i32 %252, %253
  store i32 %add221, i32* %z1, align 4, !tbaa !15
  %254 = load i32, i32* %tmp1, align 4, !tbaa !15
  %255 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add222 = add nsw i32 %254, %255
  store i32 %add222, i32* %z2, align 4, !tbaa !15
  %256 = load i32, i32* %tmp0, align 4, !tbaa !15
  %257 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add223 = add nsw i32 %256, %257
  store i32 %add223, i32* %z3, align 4, !tbaa !15
  %258 = load i32, i32* %tmp1, align 4, !tbaa !15
  %259 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add224 = add nsw i32 %258, %259
  store i32 %add224, i32* %z4, align 4, !tbaa !15
  %260 = load i32, i32* %z3, align 4, !tbaa !15
  %261 = load i32, i32* %z4, align 4, !tbaa !15
  %add225 = add nsw i32 %260, %261
  %mul226 = mul nsw i32 %add225, 9633
  store i32 %mul226, i32* %z5, align 4, !tbaa !15
  %262 = load i32, i32* %tmp0, align 4, !tbaa !15
  %mul227 = mul nsw i32 %262, 2446
  store i32 %mul227, i32* %tmp0, align 4, !tbaa !15
  %263 = load i32, i32* %tmp1, align 4, !tbaa !15
  %mul228 = mul nsw i32 %263, 16819
  store i32 %mul228, i32* %tmp1, align 4, !tbaa !15
  %264 = load i32, i32* %tmp2, align 4, !tbaa !15
  %mul229 = mul nsw i32 %264, 25172
  store i32 %mul229, i32* %tmp2, align 4, !tbaa !15
  %265 = load i32, i32* %tmp3, align 4, !tbaa !15
  %mul230 = mul nsw i32 %265, 12299
  store i32 %mul230, i32* %tmp3, align 4, !tbaa !15
  %266 = load i32, i32* %z1, align 4, !tbaa !15
  %mul231 = mul nsw i32 %266, -7373
  store i32 %mul231, i32* %z1, align 4, !tbaa !15
  %267 = load i32, i32* %z2, align 4, !tbaa !15
  %mul232 = mul nsw i32 %267, -20995
  store i32 %mul232, i32* %z2, align 4, !tbaa !15
  %268 = load i32, i32* %z3, align 4, !tbaa !15
  %mul233 = mul nsw i32 %268, -16069
  store i32 %mul233, i32* %z3, align 4, !tbaa !15
  %269 = load i32, i32* %z4, align 4, !tbaa !15
  %mul234 = mul nsw i32 %269, -3196
  store i32 %mul234, i32* %z4, align 4, !tbaa !15
  %270 = load i32, i32* %z5, align 4, !tbaa !15
  %271 = load i32, i32* %z3, align 4, !tbaa !15
  %add235 = add nsw i32 %271, %270
  store i32 %add235, i32* %z3, align 4, !tbaa !15
  %272 = load i32, i32* %z5, align 4, !tbaa !15
  %273 = load i32, i32* %z4, align 4, !tbaa !15
  %add236 = add nsw i32 %273, %272
  store i32 %add236, i32* %z4, align 4, !tbaa !15
  %274 = load i32, i32* %z1, align 4, !tbaa !15
  %275 = load i32, i32* %z3, align 4, !tbaa !15
  %add237 = add nsw i32 %274, %275
  %276 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add238 = add nsw i32 %276, %add237
  store i32 %add238, i32* %tmp0, align 4, !tbaa !15
  %277 = load i32, i32* %z2, align 4, !tbaa !15
  %278 = load i32, i32* %z4, align 4, !tbaa !15
  %add239 = add nsw i32 %277, %278
  %279 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add240 = add nsw i32 %279, %add239
  store i32 %add240, i32* %tmp1, align 4, !tbaa !15
  %280 = load i32, i32* %z2, align 4, !tbaa !15
  %281 = load i32, i32* %z3, align 4, !tbaa !15
  %add241 = add nsw i32 %280, %281
  %282 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add242 = add nsw i32 %282, %add241
  store i32 %add242, i32* %tmp2, align 4, !tbaa !15
  %283 = load i32, i32* %z1, align 4, !tbaa !15
  %284 = load i32, i32* %z4, align 4, !tbaa !15
  %add243 = add nsw i32 %283, %284
  %285 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add244 = add nsw i32 %285, %add243
  store i32 %add244, i32* %tmp3, align 4, !tbaa !15
  %286 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %287 = load i32, i32* %tmp10, align 4, !tbaa !15
  %288 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add245 = add nsw i32 %287, %288
  %add246 = add nsw i32 %add245, 131072
  %shr247 = ashr i32 %add246, 18
  %and248 = and i32 %shr247, 1023
  %arrayidx249 = getelementptr inbounds i8, i8* %286, i32 %and248
  %289 = load i8, i8* %arrayidx249, align 1, !tbaa !17
  %290 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx250 = getelementptr inbounds i8, i8* %290, i32 0
  store i8 %289, i8* %arrayidx250, align 1, !tbaa !17
  %291 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %292 = load i32, i32* %tmp10, align 4, !tbaa !15
  %293 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub251 = sub nsw i32 %292, %293
  %add252 = add nsw i32 %sub251, 131072
  %shr253 = ashr i32 %add252, 18
  %and254 = and i32 %shr253, 1023
  %arrayidx255 = getelementptr inbounds i8, i8* %291, i32 %and254
  %294 = load i8, i8* %arrayidx255, align 1, !tbaa !17
  %295 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx256 = getelementptr inbounds i8, i8* %295, i32 7
  store i8 %294, i8* %arrayidx256, align 1, !tbaa !17
  %296 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %297 = load i32, i32* %tmp11, align 4, !tbaa !15
  %298 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add257 = add nsw i32 %297, %298
  %add258 = add nsw i32 %add257, 131072
  %shr259 = ashr i32 %add258, 18
  %and260 = and i32 %shr259, 1023
  %arrayidx261 = getelementptr inbounds i8, i8* %296, i32 %and260
  %299 = load i8, i8* %arrayidx261, align 1, !tbaa !17
  %300 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx262 = getelementptr inbounds i8, i8* %300, i32 1
  store i8 %299, i8* %arrayidx262, align 1, !tbaa !17
  %301 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %302 = load i32, i32* %tmp11, align 4, !tbaa !15
  %303 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub263 = sub nsw i32 %302, %303
  %add264 = add nsw i32 %sub263, 131072
  %shr265 = ashr i32 %add264, 18
  %and266 = and i32 %shr265, 1023
  %arrayidx267 = getelementptr inbounds i8, i8* %301, i32 %and266
  %304 = load i8, i8* %arrayidx267, align 1, !tbaa !17
  %305 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx268 = getelementptr inbounds i8, i8* %305, i32 6
  store i8 %304, i8* %arrayidx268, align 1, !tbaa !17
  %306 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %307 = load i32, i32* %tmp12, align 4, !tbaa !15
  %308 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add269 = add nsw i32 %307, %308
  %add270 = add nsw i32 %add269, 131072
  %shr271 = ashr i32 %add270, 18
  %and272 = and i32 %shr271, 1023
  %arrayidx273 = getelementptr inbounds i8, i8* %306, i32 %and272
  %309 = load i8, i8* %arrayidx273, align 1, !tbaa !17
  %310 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx274 = getelementptr inbounds i8, i8* %310, i32 2
  store i8 %309, i8* %arrayidx274, align 1, !tbaa !17
  %311 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %312 = load i32, i32* %tmp12, align 4, !tbaa !15
  %313 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub275 = sub nsw i32 %312, %313
  %add276 = add nsw i32 %sub275, 131072
  %shr277 = ashr i32 %add276, 18
  %and278 = and i32 %shr277, 1023
  %arrayidx279 = getelementptr inbounds i8, i8* %311, i32 %and278
  %314 = load i8, i8* %arrayidx279, align 1, !tbaa !17
  %315 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx280 = getelementptr inbounds i8, i8* %315, i32 5
  store i8 %314, i8* %arrayidx280, align 1, !tbaa !17
  %316 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %317 = load i32, i32* %tmp13, align 4, !tbaa !15
  %318 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add281 = add nsw i32 %317, %318
  %add282 = add nsw i32 %add281, 131072
  %shr283 = ashr i32 %add282, 18
  %and284 = and i32 %shr283, 1023
  %arrayidx285 = getelementptr inbounds i8, i8* %316, i32 %and284
  %319 = load i8, i8* %arrayidx285, align 1, !tbaa !17
  %320 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx286 = getelementptr inbounds i8, i8* %320, i32 3
  store i8 %319, i8* %arrayidx286, align 1, !tbaa !17
  %321 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %322 = load i32, i32* %tmp13, align 4, !tbaa !15
  %323 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub287 = sub nsw i32 %322, %323
  %add288 = add nsw i32 %sub287, 131072
  %shr289 = ashr i32 %add288, 18
  %and290 = and i32 %shr289, 1023
  %arrayidx291 = getelementptr inbounds i8, i8* %321, i32 %and290
  %324 = load i8, i8* %arrayidx291, align 1, !tbaa !17
  %325 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx292 = getelementptr inbounds i8, i8* %325, i32 4
  store i8 %324, i8* %arrayidx292, align 1, !tbaa !17
  %326 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr293 = getelementptr inbounds i32, i32* %326, i32 8
  store i32* %add.ptr293, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc294

for.inc294:                                       ; preds = %if.end196, %if.then181
  %327 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %327, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  br label %for.cond148

for.end295:                                       ; preds = %for.cond148
  %328 = bitcast [64 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %328) #2
  %329 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %329) #2
  %330 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %330) #2
  %331 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #2
  %332 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #2
  %333 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #2
  %334 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #2
  %335 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %335) #2
  %336 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %336) #2
  %337 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %337) #2
  %338 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %338) #2
  %339 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %339) #2
  %340 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %340) #2
  %341 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %341) #2
  %342 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %342) #2
  %343 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %343) #2
  %344 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %344) #2
  %345 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %345) #2
  %346 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %346) #2
  %347 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %347) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @jpeg_idct_7x7(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [49 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 65
  %16 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %16, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %17 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast [49 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 196, i8* %18) #2
  %19 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %19, i16** %inptr, align 4, !tbaa !2
  %20 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %20, i32 0, i32 20
  %21 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %22 = bitcast i8* %21 to i32*
  store i32* %22, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [49 x i32], [49 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %23 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %23, 7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %24, i32 0
  %25 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %25 to i32
  %26 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %26, i32 0
  %27 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %27
  store i32 %mul, i32* %tmp13, align 4, !tbaa !15
  %28 = load i32, i32* %tmp13, align 4, !tbaa !15
  %shl = shl i32 %28, 13
  store i32 %shl, i32* %tmp13, align 4, !tbaa !15
  %29 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add = add nsw i32 %29, 1024
  store i32 %add, i32* %tmp13, align 4, !tbaa !15
  %30 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %30, i32 16
  %31 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %31 to i32
  %32 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %32, i32 16
  %33 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %33
  store i32 %mul5, i32* %z1, align 4, !tbaa !15
  %34 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %34, i32 32
  %35 = load i16, i16* %arrayidx6, align 2, !tbaa !14
  %conv7 = sext i16 %35 to i32
  %36 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %36, i32 32
  %37 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %mul9 = mul nsw i32 %conv7, %37
  store i32 %mul9, i32* %z2, align 4, !tbaa !15
  %38 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %38, i32 48
  %39 = load i16, i16* %arrayidx10, align 2, !tbaa !14
  %conv11 = sext i16 %39 to i32
  %40 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %40, i32 48
  %41 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %mul13 = mul nsw i32 %conv11, %41
  store i32 %mul13, i32* %z3, align 4, !tbaa !15
  %42 = load i32, i32* %z2, align 4, !tbaa !15
  %43 = load i32, i32* %z3, align 4, !tbaa !15
  %sub = sub nsw i32 %42, %43
  %mul14 = mul nsw i32 %sub, 7223
  store i32 %mul14, i32* %tmp10, align 4, !tbaa !15
  %44 = load i32, i32* %z1, align 4, !tbaa !15
  %45 = load i32, i32* %z2, align 4, !tbaa !15
  %sub15 = sub nsw i32 %44, %45
  %mul16 = mul nsw i32 %sub15, 2578
  store i32 %mul16, i32* %tmp12, align 4, !tbaa !15
  %46 = load i32, i32* %tmp10, align 4, !tbaa !15
  %47 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add17 = add nsw i32 %46, %47
  %48 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add18 = add nsw i32 %add17, %48
  %49 = load i32, i32* %z2, align 4, !tbaa !15
  %mul19 = mul nsw i32 %49, 15083
  %sub20 = sub nsw i32 %add18, %mul19
  store i32 %sub20, i32* %tmp11, align 4, !tbaa !15
  %50 = load i32, i32* %z1, align 4, !tbaa !15
  %51 = load i32, i32* %z3, align 4, !tbaa !15
  %add21 = add nsw i32 %50, %51
  store i32 %add21, i32* %tmp0, align 4, !tbaa !15
  %52 = load i32, i32* %tmp0, align 4, !tbaa !15
  %53 = load i32, i32* %z2, align 4, !tbaa !15
  %sub22 = sub nsw i32 %53, %52
  store i32 %sub22, i32* %z2, align 4, !tbaa !15
  %54 = load i32, i32* %tmp0, align 4, !tbaa !15
  %mul23 = mul nsw i32 %54, 10438
  %55 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add24 = add nsw i32 %mul23, %55
  store i32 %add24, i32* %tmp0, align 4, !tbaa !15
  %56 = load i32, i32* %tmp0, align 4, !tbaa !15
  %57 = load i32, i32* %z3, align 4, !tbaa !15
  %mul25 = mul nsw i32 %57, 637
  %sub26 = sub nsw i32 %56, %mul25
  %58 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add27 = add nsw i32 %58, %sub26
  store i32 %add27, i32* %tmp10, align 4, !tbaa !15
  %59 = load i32, i32* %tmp0, align 4, !tbaa !15
  %60 = load i32, i32* %z1, align 4, !tbaa !15
  %mul28 = mul nsw i32 %60, 20239
  %sub29 = sub nsw i32 %59, %mul28
  %61 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add30 = add nsw i32 %61, %sub29
  store i32 %add30, i32* %tmp12, align 4, !tbaa !15
  %62 = load i32, i32* %z2, align 4, !tbaa !15
  %mul31 = mul nsw i32 %62, 11585
  %63 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add32 = add nsw i32 %63, %mul31
  store i32 %add32, i32* %tmp13, align 4, !tbaa !15
  %64 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i16, i16* %64, i32 8
  %65 = load i16, i16* %arrayidx33, align 2, !tbaa !14
  %conv34 = sext i16 %65 to i32
  %66 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %66, i32 8
  %67 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %mul36 = mul nsw i32 %conv34, %67
  store i32 %mul36, i32* %z1, align 4, !tbaa !15
  %68 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i16, i16* %68, i32 24
  %69 = load i16, i16* %arrayidx37, align 2, !tbaa !14
  %conv38 = sext i16 %69 to i32
  %70 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %70, i32 24
  %71 = load i32, i32* %arrayidx39, align 4, !tbaa !6
  %mul40 = mul nsw i32 %conv38, %71
  store i32 %mul40, i32* %z2, align 4, !tbaa !15
  %72 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i16, i16* %72, i32 40
  %73 = load i16, i16* %arrayidx41, align 2, !tbaa !14
  %conv42 = sext i16 %73 to i32
  %74 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %74, i32 40
  %75 = load i32, i32* %arrayidx43, align 4, !tbaa !6
  %mul44 = mul nsw i32 %conv42, %75
  store i32 %mul44, i32* %z3, align 4, !tbaa !15
  %76 = load i32, i32* %z1, align 4, !tbaa !15
  %77 = load i32, i32* %z2, align 4, !tbaa !15
  %add45 = add nsw i32 %76, %77
  %mul46 = mul nsw i32 %add45, 7663
  store i32 %mul46, i32* %tmp1, align 4, !tbaa !15
  %78 = load i32, i32* %z1, align 4, !tbaa !15
  %79 = load i32, i32* %z2, align 4, !tbaa !15
  %sub47 = sub nsw i32 %78, %79
  %mul48 = mul nsw i32 %sub47, 1395
  store i32 %mul48, i32* %tmp2, align 4, !tbaa !15
  %80 = load i32, i32* %tmp1, align 4, !tbaa !15
  %81 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub49 = sub nsw i32 %80, %81
  store i32 %sub49, i32* %tmp0, align 4, !tbaa !15
  %82 = load i32, i32* %tmp2, align 4, !tbaa !15
  %83 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add50 = add nsw i32 %83, %82
  store i32 %add50, i32* %tmp1, align 4, !tbaa !15
  %84 = load i32, i32* %z2, align 4, !tbaa !15
  %85 = load i32, i32* %z3, align 4, !tbaa !15
  %add51 = add nsw i32 %84, %85
  %mul52 = mul nsw i32 %add51, -11295
  store i32 %mul52, i32* %tmp2, align 4, !tbaa !15
  %86 = load i32, i32* %tmp2, align 4, !tbaa !15
  %87 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add53 = add nsw i32 %87, %86
  store i32 %add53, i32* %tmp1, align 4, !tbaa !15
  %88 = load i32, i32* %z1, align 4, !tbaa !15
  %89 = load i32, i32* %z3, align 4, !tbaa !15
  %add54 = add nsw i32 %88, %89
  %mul55 = mul nsw i32 %add54, 5027
  store i32 %mul55, i32* %z2, align 4, !tbaa !15
  %90 = load i32, i32* %z2, align 4, !tbaa !15
  %91 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add56 = add nsw i32 %91, %90
  store i32 %add56, i32* %tmp0, align 4, !tbaa !15
  %92 = load i32, i32* %z2, align 4, !tbaa !15
  %93 = load i32, i32* %z3, align 4, !tbaa !15
  %mul57 = mul nsw i32 %93, 15326
  %add58 = add nsw i32 %92, %mul57
  %94 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add59 = add nsw i32 %94, %add58
  store i32 %add59, i32* %tmp2, align 4, !tbaa !15
  %95 = load i32, i32* %tmp10, align 4, !tbaa !15
  %96 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add60 = add nsw i32 %95, %96
  %shr = ashr i32 %add60, 11
  %97 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %97, i32 0
  store i32 %shr, i32* %arrayidx61, align 4, !tbaa !6
  %98 = load i32, i32* %tmp10, align 4, !tbaa !15
  %99 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub62 = sub nsw i32 %98, %99
  %shr63 = ashr i32 %sub62, 11
  %100 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %100, i32 42
  store i32 %shr63, i32* %arrayidx64, align 4, !tbaa !6
  %101 = load i32, i32* %tmp11, align 4, !tbaa !15
  %102 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add65 = add nsw i32 %101, %102
  %shr66 = ashr i32 %add65, 11
  %103 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %103, i32 7
  store i32 %shr66, i32* %arrayidx67, align 4, !tbaa !6
  %104 = load i32, i32* %tmp11, align 4, !tbaa !15
  %105 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub68 = sub nsw i32 %104, %105
  %shr69 = ashr i32 %sub68, 11
  %106 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i32, i32* %106, i32 35
  store i32 %shr69, i32* %arrayidx70, align 4, !tbaa !6
  %107 = load i32, i32* %tmp12, align 4, !tbaa !15
  %108 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add71 = add nsw i32 %107, %108
  %shr72 = ashr i32 %add71, 11
  %109 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %109, i32 14
  store i32 %shr72, i32* %arrayidx73, align 4, !tbaa !6
  %110 = load i32, i32* %tmp12, align 4, !tbaa !15
  %111 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub74 = sub nsw i32 %110, %111
  %shr75 = ashr i32 %sub74, 11
  %112 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %112, i32 28
  store i32 %shr75, i32* %arrayidx76, align 4, !tbaa !6
  %113 = load i32, i32* %tmp13, align 4, !tbaa !15
  %shr77 = ashr i32 %113, 11
  %114 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %114, i32 21
  store i32 %shr77, i32* %arrayidx78, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %115 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %115, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %116 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %116, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %117 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr79 = getelementptr inbounds i32, i32* %117, i32 1
  store i32* %incdec.ptr79, i32** %quantptr, align 4, !tbaa !2
  %118 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr80 = getelementptr inbounds i32, i32* %118, i32 1
  store i32* %incdec.ptr80, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay81 = getelementptr inbounds [49 x i32], [49 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay81, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond82

for.cond82:                                       ; preds = %for.inc166, %for.end
  %119 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp83 = icmp slt i32 %119, 7
  br i1 %cmp83, label %for.body85, label %for.end168

for.body85:                                       ; preds = %for.cond82
  %120 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %121 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx86 = getelementptr inbounds i8*, i8** %120, i32 %121
  %122 = load i8*, i8** %arrayidx86, align 4, !tbaa !2
  %123 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr87 = getelementptr inbounds i8, i8* %122, i32 %123
  store i8* %add.ptr87, i8** %outptr, align 4, !tbaa !2
  %124 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i32, i32* %124, i32 0
  %125 = load i32, i32* %arrayidx88, align 4, !tbaa !6
  %add89 = add nsw i32 %125, 16
  store i32 %add89, i32* %tmp13, align 4, !tbaa !15
  %126 = load i32, i32* %tmp13, align 4, !tbaa !15
  %shl90 = shl i32 %126, 13
  store i32 %shl90, i32* %tmp13, align 4, !tbaa !15
  %127 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i32, i32* %127, i32 2
  %128 = load i32, i32* %arrayidx91, align 4, !tbaa !6
  store i32 %128, i32* %z1, align 4, !tbaa !15
  %129 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i32, i32* %129, i32 4
  %130 = load i32, i32* %arrayidx92, align 4, !tbaa !6
  store i32 %130, i32* %z2, align 4, !tbaa !15
  %131 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %131, i32 6
  %132 = load i32, i32* %arrayidx93, align 4, !tbaa !6
  store i32 %132, i32* %z3, align 4, !tbaa !15
  %133 = load i32, i32* %z2, align 4, !tbaa !15
  %134 = load i32, i32* %z3, align 4, !tbaa !15
  %sub94 = sub nsw i32 %133, %134
  %mul95 = mul nsw i32 %sub94, 7223
  store i32 %mul95, i32* %tmp10, align 4, !tbaa !15
  %135 = load i32, i32* %z1, align 4, !tbaa !15
  %136 = load i32, i32* %z2, align 4, !tbaa !15
  %sub96 = sub nsw i32 %135, %136
  %mul97 = mul nsw i32 %sub96, 2578
  store i32 %mul97, i32* %tmp12, align 4, !tbaa !15
  %137 = load i32, i32* %tmp10, align 4, !tbaa !15
  %138 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add98 = add nsw i32 %137, %138
  %139 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add99 = add nsw i32 %add98, %139
  %140 = load i32, i32* %z2, align 4, !tbaa !15
  %mul100 = mul nsw i32 %140, 15083
  %sub101 = sub nsw i32 %add99, %mul100
  store i32 %sub101, i32* %tmp11, align 4, !tbaa !15
  %141 = load i32, i32* %z1, align 4, !tbaa !15
  %142 = load i32, i32* %z3, align 4, !tbaa !15
  %add102 = add nsw i32 %141, %142
  store i32 %add102, i32* %tmp0, align 4, !tbaa !15
  %143 = load i32, i32* %tmp0, align 4, !tbaa !15
  %144 = load i32, i32* %z2, align 4, !tbaa !15
  %sub103 = sub nsw i32 %144, %143
  store i32 %sub103, i32* %z2, align 4, !tbaa !15
  %145 = load i32, i32* %tmp0, align 4, !tbaa !15
  %mul104 = mul nsw i32 %145, 10438
  %146 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add105 = add nsw i32 %mul104, %146
  store i32 %add105, i32* %tmp0, align 4, !tbaa !15
  %147 = load i32, i32* %tmp0, align 4, !tbaa !15
  %148 = load i32, i32* %z3, align 4, !tbaa !15
  %mul106 = mul nsw i32 %148, 637
  %sub107 = sub nsw i32 %147, %mul106
  %149 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add108 = add nsw i32 %149, %sub107
  store i32 %add108, i32* %tmp10, align 4, !tbaa !15
  %150 = load i32, i32* %tmp0, align 4, !tbaa !15
  %151 = load i32, i32* %z1, align 4, !tbaa !15
  %mul109 = mul nsw i32 %151, 20239
  %sub110 = sub nsw i32 %150, %mul109
  %152 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add111 = add nsw i32 %152, %sub110
  store i32 %add111, i32* %tmp12, align 4, !tbaa !15
  %153 = load i32, i32* %z2, align 4, !tbaa !15
  %mul112 = mul nsw i32 %153, 11585
  %154 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add113 = add nsw i32 %154, %mul112
  store i32 %add113, i32* %tmp13, align 4, !tbaa !15
  %155 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx114 = getelementptr inbounds i32, i32* %155, i32 1
  %156 = load i32, i32* %arrayidx114, align 4, !tbaa !6
  store i32 %156, i32* %z1, align 4, !tbaa !15
  %157 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %157, i32 3
  %158 = load i32, i32* %arrayidx115, align 4, !tbaa !6
  store i32 %158, i32* %z2, align 4, !tbaa !15
  %159 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds i32, i32* %159, i32 5
  %160 = load i32, i32* %arrayidx116, align 4, !tbaa !6
  store i32 %160, i32* %z3, align 4, !tbaa !15
  %161 = load i32, i32* %z1, align 4, !tbaa !15
  %162 = load i32, i32* %z2, align 4, !tbaa !15
  %add117 = add nsw i32 %161, %162
  %mul118 = mul nsw i32 %add117, 7663
  store i32 %mul118, i32* %tmp1, align 4, !tbaa !15
  %163 = load i32, i32* %z1, align 4, !tbaa !15
  %164 = load i32, i32* %z2, align 4, !tbaa !15
  %sub119 = sub nsw i32 %163, %164
  %mul120 = mul nsw i32 %sub119, 1395
  store i32 %mul120, i32* %tmp2, align 4, !tbaa !15
  %165 = load i32, i32* %tmp1, align 4, !tbaa !15
  %166 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub121 = sub nsw i32 %165, %166
  store i32 %sub121, i32* %tmp0, align 4, !tbaa !15
  %167 = load i32, i32* %tmp2, align 4, !tbaa !15
  %168 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add122 = add nsw i32 %168, %167
  store i32 %add122, i32* %tmp1, align 4, !tbaa !15
  %169 = load i32, i32* %z2, align 4, !tbaa !15
  %170 = load i32, i32* %z3, align 4, !tbaa !15
  %add123 = add nsw i32 %169, %170
  %mul124 = mul nsw i32 %add123, -11295
  store i32 %mul124, i32* %tmp2, align 4, !tbaa !15
  %171 = load i32, i32* %tmp2, align 4, !tbaa !15
  %172 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add125 = add nsw i32 %172, %171
  store i32 %add125, i32* %tmp1, align 4, !tbaa !15
  %173 = load i32, i32* %z1, align 4, !tbaa !15
  %174 = load i32, i32* %z3, align 4, !tbaa !15
  %add126 = add nsw i32 %173, %174
  %mul127 = mul nsw i32 %add126, 5027
  store i32 %mul127, i32* %z2, align 4, !tbaa !15
  %175 = load i32, i32* %z2, align 4, !tbaa !15
  %176 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add128 = add nsw i32 %176, %175
  store i32 %add128, i32* %tmp0, align 4, !tbaa !15
  %177 = load i32, i32* %z2, align 4, !tbaa !15
  %178 = load i32, i32* %z3, align 4, !tbaa !15
  %mul129 = mul nsw i32 %178, 15326
  %add130 = add nsw i32 %177, %mul129
  %179 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add131 = add nsw i32 %179, %add130
  store i32 %add131, i32* %tmp2, align 4, !tbaa !15
  %180 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %181 = load i32, i32* %tmp10, align 4, !tbaa !15
  %182 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add132 = add nsw i32 %181, %182
  %shr133 = ashr i32 %add132, 18
  %and = and i32 %shr133, 1023
  %arrayidx134 = getelementptr inbounds i8, i8* %180, i32 %and
  %183 = load i8, i8* %arrayidx134, align 1, !tbaa !17
  %184 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i8, i8* %184, i32 0
  store i8 %183, i8* %arrayidx135, align 1, !tbaa !17
  %185 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %186 = load i32, i32* %tmp10, align 4, !tbaa !15
  %187 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub136 = sub nsw i32 %186, %187
  %shr137 = ashr i32 %sub136, 18
  %and138 = and i32 %shr137, 1023
  %arrayidx139 = getelementptr inbounds i8, i8* %185, i32 %and138
  %188 = load i8, i8* %arrayidx139, align 1, !tbaa !17
  %189 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds i8, i8* %189, i32 6
  store i8 %188, i8* %arrayidx140, align 1, !tbaa !17
  %190 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %191 = load i32, i32* %tmp11, align 4, !tbaa !15
  %192 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add141 = add nsw i32 %191, %192
  %shr142 = ashr i32 %add141, 18
  %and143 = and i32 %shr142, 1023
  %arrayidx144 = getelementptr inbounds i8, i8* %190, i32 %and143
  %193 = load i8, i8* %arrayidx144, align 1, !tbaa !17
  %194 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds i8, i8* %194, i32 1
  store i8 %193, i8* %arrayidx145, align 1, !tbaa !17
  %195 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %196 = load i32, i32* %tmp11, align 4, !tbaa !15
  %197 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub146 = sub nsw i32 %196, %197
  %shr147 = ashr i32 %sub146, 18
  %and148 = and i32 %shr147, 1023
  %arrayidx149 = getelementptr inbounds i8, i8* %195, i32 %and148
  %198 = load i8, i8* %arrayidx149, align 1, !tbaa !17
  %199 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx150 = getelementptr inbounds i8, i8* %199, i32 5
  store i8 %198, i8* %arrayidx150, align 1, !tbaa !17
  %200 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %201 = load i32, i32* %tmp12, align 4, !tbaa !15
  %202 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add151 = add nsw i32 %201, %202
  %shr152 = ashr i32 %add151, 18
  %and153 = and i32 %shr152, 1023
  %arrayidx154 = getelementptr inbounds i8, i8* %200, i32 %and153
  %203 = load i8, i8* %arrayidx154, align 1, !tbaa !17
  %204 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds i8, i8* %204, i32 2
  store i8 %203, i8* %arrayidx155, align 1, !tbaa !17
  %205 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %206 = load i32, i32* %tmp12, align 4, !tbaa !15
  %207 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub156 = sub nsw i32 %206, %207
  %shr157 = ashr i32 %sub156, 18
  %and158 = and i32 %shr157, 1023
  %arrayidx159 = getelementptr inbounds i8, i8* %205, i32 %and158
  %208 = load i8, i8* %arrayidx159, align 1, !tbaa !17
  %209 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx160 = getelementptr inbounds i8, i8* %209, i32 4
  store i8 %208, i8* %arrayidx160, align 1, !tbaa !17
  %210 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %211 = load i32, i32* %tmp13, align 4, !tbaa !15
  %shr161 = ashr i32 %211, 18
  %and162 = and i32 %shr161, 1023
  %arrayidx163 = getelementptr inbounds i8, i8* %210, i32 %and162
  %212 = load i8, i8* %arrayidx163, align 1, !tbaa !17
  %213 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i8, i8* %213, i32 3
  store i8 %212, i8* %arrayidx164, align 1, !tbaa !17
  %214 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr165 = getelementptr inbounds i32, i32* %214, i32 7
  store i32* %add.ptr165, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc166

for.inc166:                                       ; preds = %for.body85
  %215 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc167 = add nsw i32 %215, 1
  store i32 %inc167, i32* %ctr, align 4, !tbaa !6
  br label %for.cond82

for.end168:                                       ; preds = %for.cond82
  %216 = bitcast [49 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 196, i8* %216) #2
  %217 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #2
  %218 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #2
  %219 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #2
  %220 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #2
  %221 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #2
  %222 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #2
  %223 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #2
  %224 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #2
  %225 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #2
  %226 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #2
  %227 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #2
  %228 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #2
  %229 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #2
  %230 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #2
  %231 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #2
  %232 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_6x6(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [36 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 65
  %15 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %16 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast [36 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 144, i8* %17) #2
  %18 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %18, i16** %inptr, align 4, !tbaa !2
  %19 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %19, i32 0, i32 20
  %20 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %21 = bitcast i8* %20 to i32*
  store i32* %21, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [36 x i32], [36 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %22 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %22, 6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %23, i32 0
  %24 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %24 to i32
  %25 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %25, i32 0
  %26 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %26
  store i32 %mul, i32* %tmp0, align 4, !tbaa !15
  %27 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl = shl i32 %27, 13
  store i32 %shl, i32* %tmp0, align 4, !tbaa !15
  %28 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add = add nsw i32 %28, 1024
  store i32 %add, i32* %tmp0, align 4, !tbaa !15
  %29 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %29, i32 32
  %30 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %30 to i32
  %31 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %31, i32 32
  %32 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %32
  store i32 %mul5, i32* %tmp2, align 4, !tbaa !15
  %33 = load i32, i32* %tmp2, align 4, !tbaa !15
  %mul6 = mul nsw i32 %33, 5793
  store i32 %mul6, i32* %tmp10, align 4, !tbaa !15
  %34 = load i32, i32* %tmp0, align 4, !tbaa !15
  %35 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add7 = add nsw i32 %34, %35
  store i32 %add7, i32* %tmp1, align 4, !tbaa !15
  %36 = load i32, i32* %tmp0, align 4, !tbaa !15
  %37 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub = sub nsw i32 %36, %37
  %38 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub8 = sub nsw i32 %sub, %38
  %shr = ashr i32 %sub8, 11
  store i32 %shr, i32* %tmp11, align 4, !tbaa !15
  %39 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i16, i16* %39, i32 16
  %40 = load i16, i16* %arrayidx9, align 2, !tbaa !14
  %conv10 = sext i16 %40 to i32
  %41 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %41, i32 16
  %42 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %mul12 = mul nsw i32 %conv10, %42
  store i32 %mul12, i32* %tmp10, align 4, !tbaa !15
  %43 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul13 = mul nsw i32 %43, 10033
  store i32 %mul13, i32* %tmp0, align 4, !tbaa !15
  %44 = load i32, i32* %tmp1, align 4, !tbaa !15
  %45 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add14 = add nsw i32 %44, %45
  store i32 %add14, i32* %tmp10, align 4, !tbaa !15
  %46 = load i32, i32* %tmp1, align 4, !tbaa !15
  %47 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub15 = sub nsw i32 %46, %47
  store i32 %sub15, i32* %tmp12, align 4, !tbaa !15
  %48 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i16, i16* %48, i32 8
  %49 = load i16, i16* %arrayidx16, align 2, !tbaa !14
  %conv17 = sext i16 %49 to i32
  %50 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i32, i32* %50, i32 8
  %51 = load i32, i32* %arrayidx18, align 4, !tbaa !6
  %mul19 = mul nsw i32 %conv17, %51
  store i32 %mul19, i32* %z1, align 4, !tbaa !15
  %52 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i16, i16* %52, i32 24
  %53 = load i16, i16* %arrayidx20, align 2, !tbaa !14
  %conv21 = sext i16 %53 to i32
  %54 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i32, i32* %54, i32 24
  %55 = load i32, i32* %arrayidx22, align 4, !tbaa !6
  %mul23 = mul nsw i32 %conv21, %55
  store i32 %mul23, i32* %z2, align 4, !tbaa !15
  %56 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i16, i16* %56, i32 40
  %57 = load i16, i16* %arrayidx24, align 2, !tbaa !14
  %conv25 = sext i16 %57 to i32
  %58 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i32, i32* %58, i32 40
  %59 = load i32, i32* %arrayidx26, align 4, !tbaa !6
  %mul27 = mul nsw i32 %conv25, %59
  store i32 %mul27, i32* %z3, align 4, !tbaa !15
  %60 = load i32, i32* %z1, align 4, !tbaa !15
  %61 = load i32, i32* %z3, align 4, !tbaa !15
  %add28 = add nsw i32 %60, %61
  %mul29 = mul nsw i32 %add28, 2998
  store i32 %mul29, i32* %tmp1, align 4, !tbaa !15
  %62 = load i32, i32* %tmp1, align 4, !tbaa !15
  %63 = load i32, i32* %z1, align 4, !tbaa !15
  %64 = load i32, i32* %z2, align 4, !tbaa !15
  %add30 = add nsw i32 %63, %64
  %shl31 = shl i32 %add30, 13
  %add32 = add nsw i32 %62, %shl31
  store i32 %add32, i32* %tmp0, align 4, !tbaa !15
  %65 = load i32, i32* %tmp1, align 4, !tbaa !15
  %66 = load i32, i32* %z3, align 4, !tbaa !15
  %67 = load i32, i32* %z2, align 4, !tbaa !15
  %sub33 = sub nsw i32 %66, %67
  %shl34 = shl i32 %sub33, 13
  %add35 = add nsw i32 %65, %shl34
  store i32 %add35, i32* %tmp2, align 4, !tbaa !15
  %68 = load i32, i32* %z1, align 4, !tbaa !15
  %69 = load i32, i32* %z2, align 4, !tbaa !15
  %sub36 = sub nsw i32 %68, %69
  %70 = load i32, i32* %z3, align 4, !tbaa !15
  %sub37 = sub nsw i32 %sub36, %70
  %shl38 = shl i32 %sub37, 2
  store i32 %shl38, i32* %tmp1, align 4, !tbaa !15
  %71 = load i32, i32* %tmp10, align 4, !tbaa !15
  %72 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add39 = add nsw i32 %71, %72
  %shr40 = ashr i32 %add39, 11
  %73 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %73, i32 0
  store i32 %shr40, i32* %arrayidx41, align 4, !tbaa !6
  %74 = load i32, i32* %tmp10, align 4, !tbaa !15
  %75 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub42 = sub nsw i32 %74, %75
  %shr43 = ashr i32 %sub42, 11
  %76 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %76, i32 30
  store i32 %shr43, i32* %arrayidx44, align 4, !tbaa !6
  %77 = load i32, i32* %tmp11, align 4, !tbaa !15
  %78 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add45 = add nsw i32 %77, %78
  %79 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i32, i32* %79, i32 6
  store i32 %add45, i32* %arrayidx46, align 4, !tbaa !6
  %80 = load i32, i32* %tmp11, align 4, !tbaa !15
  %81 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub47 = sub nsw i32 %80, %81
  %82 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %82, i32 24
  store i32 %sub47, i32* %arrayidx48, align 4, !tbaa !6
  %83 = load i32, i32* %tmp12, align 4, !tbaa !15
  %84 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add49 = add nsw i32 %83, %84
  %shr50 = ashr i32 %add49, 11
  %85 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %85, i32 12
  store i32 %shr50, i32* %arrayidx51, align 4, !tbaa !6
  %86 = load i32, i32* %tmp12, align 4, !tbaa !15
  %87 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub52 = sub nsw i32 %86, %87
  %shr53 = ashr i32 %sub52, 11
  %88 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds i32, i32* %88, i32 18
  store i32 %shr53, i32* %arrayidx54, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %89 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %89, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %90 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %90, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %91 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr55 = getelementptr inbounds i32, i32* %91, i32 1
  store i32* %incdec.ptr55, i32** %quantptr, align 4, !tbaa !2
  %92 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr56 = getelementptr inbounds i32, i32* %92, i32 1
  store i32* %incdec.ptr56, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay57 = getelementptr inbounds [36 x i32], [36 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay57, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc120, %for.end
  %93 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp59 = icmp slt i32 %93, 6
  br i1 %cmp59, label %for.body61, label %for.end122

for.body61:                                       ; preds = %for.cond58
  %94 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %95 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx62 = getelementptr inbounds i8*, i8** %94, i32 %95
  %96 = load i8*, i8** %arrayidx62, align 4, !tbaa !2
  %97 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr63 = getelementptr inbounds i8, i8* %96, i32 %97
  store i8* %add.ptr63, i8** %outptr, align 4, !tbaa !2
  %98 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i32, i32* %98, i32 0
  %99 = load i32, i32* %arrayidx64, align 4, !tbaa !6
  %add65 = add nsw i32 %99, 16
  store i32 %add65, i32* %tmp0, align 4, !tbaa !15
  %100 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl66 = shl i32 %100, 13
  store i32 %shl66, i32* %tmp0, align 4, !tbaa !15
  %101 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i32, i32* %101, i32 4
  %102 = load i32, i32* %arrayidx67, align 4, !tbaa !6
  store i32 %102, i32* %tmp2, align 4, !tbaa !15
  %103 = load i32, i32* %tmp2, align 4, !tbaa !15
  %mul68 = mul nsw i32 %103, 5793
  store i32 %mul68, i32* %tmp10, align 4, !tbaa !15
  %104 = load i32, i32* %tmp0, align 4, !tbaa !15
  %105 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add69 = add nsw i32 %104, %105
  store i32 %add69, i32* %tmp1, align 4, !tbaa !15
  %106 = load i32, i32* %tmp0, align 4, !tbaa !15
  %107 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub70 = sub nsw i32 %106, %107
  %108 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub71 = sub nsw i32 %sub70, %108
  store i32 %sub71, i32* %tmp11, align 4, !tbaa !15
  %109 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds i32, i32* %109, i32 2
  %110 = load i32, i32* %arrayidx72, align 4, !tbaa !6
  store i32 %110, i32* %tmp10, align 4, !tbaa !15
  %111 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul73 = mul nsw i32 %111, 10033
  store i32 %mul73, i32* %tmp0, align 4, !tbaa !15
  %112 = load i32, i32* %tmp1, align 4, !tbaa !15
  %113 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add74 = add nsw i32 %112, %113
  store i32 %add74, i32* %tmp10, align 4, !tbaa !15
  %114 = load i32, i32* %tmp1, align 4, !tbaa !15
  %115 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub75 = sub nsw i32 %114, %115
  store i32 %sub75, i32* %tmp12, align 4, !tbaa !15
  %116 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %116, i32 1
  %117 = load i32, i32* %arrayidx76, align 4, !tbaa !6
  store i32 %117, i32* %z1, align 4, !tbaa !15
  %118 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %118, i32 3
  %119 = load i32, i32* %arrayidx77, align 4, !tbaa !6
  store i32 %119, i32* %z2, align 4, !tbaa !15
  %120 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds i32, i32* %120, i32 5
  %121 = load i32, i32* %arrayidx78, align 4, !tbaa !6
  store i32 %121, i32* %z3, align 4, !tbaa !15
  %122 = load i32, i32* %z1, align 4, !tbaa !15
  %123 = load i32, i32* %z3, align 4, !tbaa !15
  %add79 = add nsw i32 %122, %123
  %mul80 = mul nsw i32 %add79, 2998
  store i32 %mul80, i32* %tmp1, align 4, !tbaa !15
  %124 = load i32, i32* %tmp1, align 4, !tbaa !15
  %125 = load i32, i32* %z1, align 4, !tbaa !15
  %126 = load i32, i32* %z2, align 4, !tbaa !15
  %add81 = add nsw i32 %125, %126
  %shl82 = shl i32 %add81, 13
  %add83 = add nsw i32 %124, %shl82
  store i32 %add83, i32* %tmp0, align 4, !tbaa !15
  %127 = load i32, i32* %tmp1, align 4, !tbaa !15
  %128 = load i32, i32* %z3, align 4, !tbaa !15
  %129 = load i32, i32* %z2, align 4, !tbaa !15
  %sub84 = sub nsw i32 %128, %129
  %shl85 = shl i32 %sub84, 13
  %add86 = add nsw i32 %127, %shl85
  store i32 %add86, i32* %tmp2, align 4, !tbaa !15
  %130 = load i32, i32* %z1, align 4, !tbaa !15
  %131 = load i32, i32* %z2, align 4, !tbaa !15
  %sub87 = sub nsw i32 %130, %131
  %132 = load i32, i32* %z3, align 4, !tbaa !15
  %sub88 = sub nsw i32 %sub87, %132
  %shl89 = shl i32 %sub88, 13
  store i32 %shl89, i32* %tmp1, align 4, !tbaa !15
  %133 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %134 = load i32, i32* %tmp10, align 4, !tbaa !15
  %135 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add90 = add nsw i32 %134, %135
  %shr91 = ashr i32 %add90, 18
  %and = and i32 %shr91, 1023
  %arrayidx92 = getelementptr inbounds i8, i8* %133, i32 %and
  %136 = load i8, i8* %arrayidx92, align 1, !tbaa !17
  %137 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i8, i8* %137, i32 0
  store i8 %136, i8* %arrayidx93, align 1, !tbaa !17
  %138 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %139 = load i32, i32* %tmp10, align 4, !tbaa !15
  %140 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub94 = sub nsw i32 %139, %140
  %shr95 = ashr i32 %sub94, 18
  %and96 = and i32 %shr95, 1023
  %arrayidx97 = getelementptr inbounds i8, i8* %138, i32 %and96
  %141 = load i8, i8* %arrayidx97, align 1, !tbaa !17
  %142 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i8, i8* %142, i32 5
  store i8 %141, i8* %arrayidx98, align 1, !tbaa !17
  %143 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %144 = load i32, i32* %tmp11, align 4, !tbaa !15
  %145 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add99 = add nsw i32 %144, %145
  %shr100 = ashr i32 %add99, 18
  %and101 = and i32 %shr100, 1023
  %arrayidx102 = getelementptr inbounds i8, i8* %143, i32 %and101
  %146 = load i8, i8* %arrayidx102, align 1, !tbaa !17
  %147 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i8, i8* %147, i32 1
  store i8 %146, i8* %arrayidx103, align 1, !tbaa !17
  %148 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %149 = load i32, i32* %tmp11, align 4, !tbaa !15
  %150 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub104 = sub nsw i32 %149, %150
  %shr105 = ashr i32 %sub104, 18
  %and106 = and i32 %shr105, 1023
  %arrayidx107 = getelementptr inbounds i8, i8* %148, i32 %and106
  %151 = load i8, i8* %arrayidx107, align 1, !tbaa !17
  %152 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i8, i8* %152, i32 4
  store i8 %151, i8* %arrayidx108, align 1, !tbaa !17
  %153 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %154 = load i32, i32* %tmp12, align 4, !tbaa !15
  %155 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add109 = add nsw i32 %154, %155
  %shr110 = ashr i32 %add109, 18
  %and111 = and i32 %shr110, 1023
  %arrayidx112 = getelementptr inbounds i8, i8* %153, i32 %and111
  %156 = load i8, i8* %arrayidx112, align 1, !tbaa !17
  %157 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds i8, i8* %157, i32 2
  store i8 %156, i8* %arrayidx113, align 1, !tbaa !17
  %158 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %159 = load i32, i32* %tmp12, align 4, !tbaa !15
  %160 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub114 = sub nsw i32 %159, %160
  %shr115 = ashr i32 %sub114, 18
  %and116 = and i32 %shr115, 1023
  %arrayidx117 = getelementptr inbounds i8, i8* %158, i32 %and116
  %161 = load i8, i8* %arrayidx117, align 1, !tbaa !17
  %162 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i8, i8* %162, i32 3
  store i8 %161, i8* %arrayidx118, align 1, !tbaa !17
  %163 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr119 = getelementptr inbounds i32, i32* %163, i32 6
  store i32* %add.ptr119, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc120

for.inc120:                                       ; preds = %for.body61
  %164 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc121 = add nsw i32 %164, 1
  store i32 %inc121, i32* %ctr, align 4, !tbaa !6
  br label %for.cond58

for.end122:                                       ; preds = %for.cond58
  %165 = bitcast [36 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 144, i8* %165) #2
  %166 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #2
  %167 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #2
  %168 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #2
  %169 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #2
  %170 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #2
  %171 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #2
  %172 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #2
  %173 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #2
  %174 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #2
  %175 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #2
  %176 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #2
  %177 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #2
  %178 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #2
  %179 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #2
  %180 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_5x5(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [25 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 65
  %14 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %15 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast [25 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 100, i8* %16) #2
  %17 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %17, i16** %inptr, align 4, !tbaa !2
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %18, i32 0, i32 20
  %19 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %20 = bitcast i8* %19 to i32*
  store i32* %20, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [25 x i32], [25 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %21 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %21, 5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %22, i32 0
  %23 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %23 to i32
  %24 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %24, i32 0
  %25 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %25
  store i32 %mul, i32* %tmp12, align 4, !tbaa !15
  %26 = load i32, i32* %tmp12, align 4, !tbaa !15
  %shl = shl i32 %26, 13
  store i32 %shl, i32* %tmp12, align 4, !tbaa !15
  %27 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add = add nsw i32 %27, 1024
  store i32 %add, i32* %tmp12, align 4, !tbaa !15
  %28 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %28, i32 16
  %29 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %29 to i32
  %30 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %30, i32 16
  %31 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %31
  store i32 %mul5, i32* %tmp0, align 4, !tbaa !15
  %32 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %32, i32 32
  %33 = load i16, i16* %arrayidx6, align 2, !tbaa !14
  %conv7 = sext i16 %33 to i32
  %34 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %34, i32 32
  %35 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %mul9 = mul nsw i32 %conv7, %35
  store i32 %mul9, i32* %tmp1, align 4, !tbaa !15
  %36 = load i32, i32* %tmp0, align 4, !tbaa !15
  %37 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add10 = add nsw i32 %36, %37
  %mul11 = mul nsw i32 %add10, 6476
  store i32 %mul11, i32* %z1, align 4, !tbaa !15
  %38 = load i32, i32* %tmp0, align 4, !tbaa !15
  %39 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub = sub nsw i32 %38, %39
  %mul12 = mul nsw i32 %sub, 2896
  store i32 %mul12, i32* %z2, align 4, !tbaa !15
  %40 = load i32, i32* %tmp12, align 4, !tbaa !15
  %41 = load i32, i32* %z2, align 4, !tbaa !15
  %add13 = add nsw i32 %40, %41
  store i32 %add13, i32* %z3, align 4, !tbaa !15
  %42 = load i32, i32* %z3, align 4, !tbaa !15
  %43 = load i32, i32* %z1, align 4, !tbaa !15
  %add14 = add nsw i32 %42, %43
  store i32 %add14, i32* %tmp10, align 4, !tbaa !15
  %44 = load i32, i32* %z3, align 4, !tbaa !15
  %45 = load i32, i32* %z1, align 4, !tbaa !15
  %sub15 = sub nsw i32 %44, %45
  store i32 %sub15, i32* %tmp11, align 4, !tbaa !15
  %46 = load i32, i32* %z2, align 4, !tbaa !15
  %shl16 = shl i32 %46, 2
  %47 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub17 = sub nsw i32 %47, %shl16
  store i32 %sub17, i32* %tmp12, align 4, !tbaa !15
  %48 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i16, i16* %48, i32 8
  %49 = load i16, i16* %arrayidx18, align 2, !tbaa !14
  %conv19 = sext i16 %49 to i32
  %50 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %50, i32 8
  %51 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %mul21 = mul nsw i32 %conv19, %51
  store i32 %mul21, i32* %z2, align 4, !tbaa !15
  %52 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i16, i16* %52, i32 24
  %53 = load i16, i16* %arrayidx22, align 2, !tbaa !14
  %conv23 = sext i16 %53 to i32
  %54 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i32, i32* %54, i32 24
  %55 = load i32, i32* %arrayidx24, align 4, !tbaa !6
  %mul25 = mul nsw i32 %conv23, %55
  store i32 %mul25, i32* %z3, align 4, !tbaa !15
  %56 = load i32, i32* %z2, align 4, !tbaa !15
  %57 = load i32, i32* %z3, align 4, !tbaa !15
  %add26 = add nsw i32 %56, %57
  %mul27 = mul nsw i32 %add26, 6810
  store i32 %mul27, i32* %z1, align 4, !tbaa !15
  %58 = load i32, i32* %z1, align 4, !tbaa !15
  %59 = load i32, i32* %z2, align 4, !tbaa !15
  %mul28 = mul nsw i32 %59, 4209
  %add29 = add nsw i32 %58, %mul28
  store i32 %add29, i32* %tmp0, align 4, !tbaa !15
  %60 = load i32, i32* %z1, align 4, !tbaa !15
  %61 = load i32, i32* %z3, align 4, !tbaa !15
  %mul30 = mul nsw i32 %61, 17828
  %sub31 = sub nsw i32 %60, %mul30
  store i32 %sub31, i32* %tmp1, align 4, !tbaa !15
  %62 = load i32, i32* %tmp10, align 4, !tbaa !15
  %63 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add32 = add nsw i32 %62, %63
  %shr = ashr i32 %add32, 11
  %64 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %64, i32 0
  store i32 %shr, i32* %arrayidx33, align 4, !tbaa !6
  %65 = load i32, i32* %tmp10, align 4, !tbaa !15
  %66 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub34 = sub nsw i32 %65, %66
  %shr35 = ashr i32 %sub34, 11
  %67 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %67, i32 20
  store i32 %shr35, i32* %arrayidx36, align 4, !tbaa !6
  %68 = load i32, i32* %tmp11, align 4, !tbaa !15
  %69 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add37 = add nsw i32 %68, %69
  %shr38 = ashr i32 %add37, 11
  %70 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %70, i32 5
  store i32 %shr38, i32* %arrayidx39, align 4, !tbaa !6
  %71 = load i32, i32* %tmp11, align 4, !tbaa !15
  %72 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub40 = sub nsw i32 %71, %72
  %shr41 = ashr i32 %sub40, 11
  %73 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %73, i32 15
  store i32 %shr41, i32* %arrayidx42, align 4, !tbaa !6
  %74 = load i32, i32* %tmp12, align 4, !tbaa !15
  %shr43 = ashr i32 %74, 11
  %75 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %75, i32 10
  store i32 %shr43, i32* %arrayidx44, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %76 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %76, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %77 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %77, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %78 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr45 = getelementptr inbounds i32, i32* %78, i32 1
  store i32* %incdec.ptr45, i32** %quantptr, align 4, !tbaa !2
  %79 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr46 = getelementptr inbounds i32, i32* %79, i32 1
  store i32* %incdec.ptr46, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay47 = getelementptr inbounds [25 x i32], [25 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay47, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc100, %for.end
  %80 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp49 = icmp slt i32 %80, 5
  br i1 %cmp49, label %for.body51, label %for.end102

for.body51:                                       ; preds = %for.cond48
  %81 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %82 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds i8*, i8** %81, i32 %82
  %83 = load i8*, i8** %arrayidx52, align 4, !tbaa !2
  %84 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr53 = getelementptr inbounds i8, i8* %83, i32 %84
  store i8* %add.ptr53, i8** %outptr, align 4, !tbaa !2
  %85 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds i32, i32* %85, i32 0
  %86 = load i32, i32* %arrayidx54, align 4, !tbaa !6
  %add55 = add nsw i32 %86, 16
  store i32 %add55, i32* %tmp12, align 4, !tbaa !15
  %87 = load i32, i32* %tmp12, align 4, !tbaa !15
  %shl56 = shl i32 %87, 13
  store i32 %shl56, i32* %tmp12, align 4, !tbaa !15
  %88 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i32, i32* %88, i32 2
  %89 = load i32, i32* %arrayidx57, align 4, !tbaa !6
  store i32 %89, i32* %tmp0, align 4, !tbaa !15
  %90 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i32, i32* %90, i32 4
  %91 = load i32, i32* %arrayidx58, align 4, !tbaa !6
  store i32 %91, i32* %tmp1, align 4, !tbaa !15
  %92 = load i32, i32* %tmp0, align 4, !tbaa !15
  %93 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add59 = add nsw i32 %92, %93
  %mul60 = mul nsw i32 %add59, 6476
  store i32 %mul60, i32* %z1, align 4, !tbaa !15
  %94 = load i32, i32* %tmp0, align 4, !tbaa !15
  %95 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub61 = sub nsw i32 %94, %95
  %mul62 = mul nsw i32 %sub61, 2896
  store i32 %mul62, i32* %z2, align 4, !tbaa !15
  %96 = load i32, i32* %tmp12, align 4, !tbaa !15
  %97 = load i32, i32* %z2, align 4, !tbaa !15
  %add63 = add nsw i32 %96, %97
  store i32 %add63, i32* %z3, align 4, !tbaa !15
  %98 = load i32, i32* %z3, align 4, !tbaa !15
  %99 = load i32, i32* %z1, align 4, !tbaa !15
  %add64 = add nsw i32 %98, %99
  store i32 %add64, i32* %tmp10, align 4, !tbaa !15
  %100 = load i32, i32* %z3, align 4, !tbaa !15
  %101 = load i32, i32* %z1, align 4, !tbaa !15
  %sub65 = sub nsw i32 %100, %101
  store i32 %sub65, i32* %tmp11, align 4, !tbaa !15
  %102 = load i32, i32* %z2, align 4, !tbaa !15
  %shl66 = shl i32 %102, 2
  %103 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub67 = sub nsw i32 %103, %shl66
  store i32 %sub67, i32* %tmp12, align 4, !tbaa !15
  %104 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %104, i32 1
  %105 = load i32, i32* %arrayidx68, align 4, !tbaa !6
  store i32 %105, i32* %z2, align 4, !tbaa !15
  %106 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i32, i32* %106, i32 3
  %107 = load i32, i32* %arrayidx69, align 4, !tbaa !6
  store i32 %107, i32* %z3, align 4, !tbaa !15
  %108 = load i32, i32* %z2, align 4, !tbaa !15
  %109 = load i32, i32* %z3, align 4, !tbaa !15
  %add70 = add nsw i32 %108, %109
  %mul71 = mul nsw i32 %add70, 6810
  store i32 %mul71, i32* %z1, align 4, !tbaa !15
  %110 = load i32, i32* %z1, align 4, !tbaa !15
  %111 = load i32, i32* %z2, align 4, !tbaa !15
  %mul72 = mul nsw i32 %111, 4209
  %add73 = add nsw i32 %110, %mul72
  store i32 %add73, i32* %tmp0, align 4, !tbaa !15
  %112 = load i32, i32* %z1, align 4, !tbaa !15
  %113 = load i32, i32* %z3, align 4, !tbaa !15
  %mul74 = mul nsw i32 %113, 17828
  %sub75 = sub nsw i32 %112, %mul74
  store i32 %sub75, i32* %tmp1, align 4, !tbaa !15
  %114 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %115 = load i32, i32* %tmp10, align 4, !tbaa !15
  %116 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add76 = add nsw i32 %115, %116
  %shr77 = ashr i32 %add76, 18
  %and = and i32 %shr77, 1023
  %arrayidx78 = getelementptr inbounds i8, i8* %114, i32 %and
  %117 = load i8, i8* %arrayidx78, align 1, !tbaa !17
  %118 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i8, i8* %118, i32 0
  store i8 %117, i8* %arrayidx79, align 1, !tbaa !17
  %119 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %120 = load i32, i32* %tmp10, align 4, !tbaa !15
  %121 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub80 = sub nsw i32 %120, %121
  %shr81 = ashr i32 %sub80, 18
  %and82 = and i32 %shr81, 1023
  %arrayidx83 = getelementptr inbounds i8, i8* %119, i32 %and82
  %122 = load i8, i8* %arrayidx83, align 1, !tbaa !17
  %123 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i8, i8* %123, i32 4
  store i8 %122, i8* %arrayidx84, align 1, !tbaa !17
  %124 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %125 = load i32, i32* %tmp11, align 4, !tbaa !15
  %126 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add85 = add nsw i32 %125, %126
  %shr86 = ashr i32 %add85, 18
  %and87 = and i32 %shr86, 1023
  %arrayidx88 = getelementptr inbounds i8, i8* %124, i32 %and87
  %127 = load i8, i8* %arrayidx88, align 1, !tbaa !17
  %128 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i8, i8* %128, i32 1
  store i8 %127, i8* %arrayidx89, align 1, !tbaa !17
  %129 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %130 = load i32, i32* %tmp11, align 4, !tbaa !15
  %131 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub90 = sub nsw i32 %130, %131
  %shr91 = ashr i32 %sub90, 18
  %and92 = and i32 %shr91, 1023
  %arrayidx93 = getelementptr inbounds i8, i8* %129, i32 %and92
  %132 = load i8, i8* %arrayidx93, align 1, !tbaa !17
  %133 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i8, i8* %133, i32 3
  store i8 %132, i8* %arrayidx94, align 1, !tbaa !17
  %134 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %135 = load i32, i32* %tmp12, align 4, !tbaa !15
  %shr95 = ashr i32 %135, 18
  %and96 = and i32 %shr95, 1023
  %arrayidx97 = getelementptr inbounds i8, i8* %134, i32 %and96
  %136 = load i8, i8* %arrayidx97, align 1, !tbaa !17
  %137 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i8, i8* %137, i32 2
  store i8 %136, i8* %arrayidx98, align 1, !tbaa !17
  %138 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr99 = getelementptr inbounds i32, i32* %138, i32 5
  store i32* %add.ptr99, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc100

for.inc100:                                       ; preds = %for.body51
  %139 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc101 = add nsw i32 %139, 1
  store i32 %inc101, i32* %ctr, align 4, !tbaa !6
  br label %for.cond48

for.end102:                                       ; preds = %for.cond48
  %140 = bitcast [25 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 100, i8* %140) #2
  %141 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #2
  %142 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #2
  %143 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #2
  %144 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #2
  %145 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #2
  %146 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #2
  %147 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #2
  %148 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #2
  %149 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #2
  %150 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #2
  %151 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #2
  %152 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #2
  %153 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #2
  %154 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_3x3(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [9 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 65
  %10 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %11 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast [9 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %12) #2
  %13 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %13, i16** %inptr, align 4, !tbaa !2
  %14 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %14, i32 0, i32 20
  %15 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %16 = bitcast i8* %15 to i32*
  store i32* %16, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [9 x i32], [9 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %17 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %17, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %18, i32 0
  %19 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %19 to i32
  %20 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %20, i32 0
  %21 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %21
  store i32 %mul, i32* %tmp0, align 4, !tbaa !15
  %22 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl = shl i32 %22, 13
  store i32 %shl, i32* %tmp0, align 4, !tbaa !15
  %23 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add = add nsw i32 %23, 1024
  store i32 %add, i32* %tmp0, align 4, !tbaa !15
  %24 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %24, i32 16
  %25 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %25 to i32
  %26 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %26, i32 16
  %27 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %27
  store i32 %mul5, i32* %tmp2, align 4, !tbaa !15
  %28 = load i32, i32* %tmp2, align 4, !tbaa !15
  %mul6 = mul nsw i32 %28, 5793
  store i32 %mul6, i32* %tmp12, align 4, !tbaa !15
  %29 = load i32, i32* %tmp0, align 4, !tbaa !15
  %30 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add7 = add nsw i32 %29, %30
  store i32 %add7, i32* %tmp10, align 4, !tbaa !15
  %31 = load i32, i32* %tmp0, align 4, !tbaa !15
  %32 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub = sub nsw i32 %31, %32
  %33 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub8 = sub nsw i32 %sub, %33
  store i32 %sub8, i32* %tmp2, align 4, !tbaa !15
  %34 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i16, i16* %34, i32 8
  %35 = load i16, i16* %arrayidx9, align 2, !tbaa !14
  %conv10 = sext i16 %35 to i32
  %36 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %36, i32 8
  %37 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %mul12 = mul nsw i32 %conv10, %37
  store i32 %mul12, i32* %tmp12, align 4, !tbaa !15
  %38 = load i32, i32* %tmp12, align 4, !tbaa !15
  %mul13 = mul nsw i32 %38, 10033
  store i32 %mul13, i32* %tmp0, align 4, !tbaa !15
  %39 = load i32, i32* %tmp10, align 4, !tbaa !15
  %40 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add14 = add nsw i32 %39, %40
  %shr = ashr i32 %add14, 11
  %41 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i32, i32* %41, i32 0
  store i32 %shr, i32* %arrayidx15, align 4, !tbaa !6
  %42 = load i32, i32* %tmp10, align 4, !tbaa !15
  %43 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub16 = sub nsw i32 %42, %43
  %shr17 = ashr i32 %sub16, 11
  %44 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i32, i32* %44, i32 6
  store i32 %shr17, i32* %arrayidx18, align 4, !tbaa !6
  %45 = load i32, i32* %tmp2, align 4, !tbaa !15
  %shr19 = ashr i32 %45, 11
  %46 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %46, i32 3
  store i32 %shr19, i32* %arrayidx20, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %47 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %48 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %48, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %49 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr21 = getelementptr inbounds i32, i32* %49, i32 1
  store i32* %incdec.ptr21, i32** %quantptr, align 4, !tbaa !2
  %50 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr22 = getelementptr inbounds i32, i32* %50, i32 1
  store i32* %incdec.ptr22, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay23 = getelementptr inbounds [9 x i32], [9 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay23, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc54, %for.end
  %51 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp25 = icmp slt i32 %51, 3
  br i1 %cmp25, label %for.body27, label %for.end56

for.body27:                                       ; preds = %for.cond24
  %52 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %53 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds i8*, i8** %52, i32 %53
  %54 = load i8*, i8** %arrayidx28, align 4, !tbaa !2
  %55 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr29 = getelementptr inbounds i8, i8* %54, i32 %55
  store i8* %add.ptr29, i8** %outptr, align 4, !tbaa !2
  %56 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %56, i32 0
  %57 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %add31 = add nsw i32 %57, 16
  store i32 %add31, i32* %tmp0, align 4, !tbaa !15
  %58 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl32 = shl i32 %58, 13
  store i32 %shl32, i32* %tmp0, align 4, !tbaa !15
  %59 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %59, i32 2
  %60 = load i32, i32* %arrayidx33, align 4, !tbaa !6
  store i32 %60, i32* %tmp2, align 4, !tbaa !15
  %61 = load i32, i32* %tmp2, align 4, !tbaa !15
  %mul34 = mul nsw i32 %61, 5793
  store i32 %mul34, i32* %tmp12, align 4, !tbaa !15
  %62 = load i32, i32* %tmp0, align 4, !tbaa !15
  %63 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add35 = add nsw i32 %62, %63
  store i32 %add35, i32* %tmp10, align 4, !tbaa !15
  %64 = load i32, i32* %tmp0, align 4, !tbaa !15
  %65 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub36 = sub nsw i32 %64, %65
  %66 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub37 = sub nsw i32 %sub36, %66
  store i32 %sub37, i32* %tmp2, align 4, !tbaa !15
  %67 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %67, i32 1
  %68 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  store i32 %68, i32* %tmp12, align 4, !tbaa !15
  %69 = load i32, i32* %tmp12, align 4, !tbaa !15
  %mul39 = mul nsw i32 %69, 10033
  store i32 %mul39, i32* %tmp0, align 4, !tbaa !15
  %70 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %71 = load i32, i32* %tmp10, align 4, !tbaa !15
  %72 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add40 = add nsw i32 %71, %72
  %shr41 = ashr i32 %add40, 18
  %and = and i32 %shr41, 1023
  %arrayidx42 = getelementptr inbounds i8, i8* %70, i32 %and
  %73 = load i8, i8* %arrayidx42, align 1, !tbaa !17
  %74 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i8, i8* %74, i32 0
  store i8 %73, i8* %arrayidx43, align 1, !tbaa !17
  %75 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %76 = load i32, i32* %tmp10, align 4, !tbaa !15
  %77 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub44 = sub nsw i32 %76, %77
  %shr45 = ashr i32 %sub44, 18
  %and46 = and i32 %shr45, 1023
  %arrayidx47 = getelementptr inbounds i8, i8* %75, i32 %and46
  %78 = load i8, i8* %arrayidx47, align 1, !tbaa !17
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i8, i8* %79, i32 2
  store i8 %78, i8* %arrayidx48, align 1, !tbaa !17
  %80 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %81 = load i32, i32* %tmp2, align 4, !tbaa !15
  %shr49 = ashr i32 %81, 18
  %and50 = and i32 %shr49, 1023
  %arrayidx51 = getelementptr inbounds i8, i8* %80, i32 %and50
  %82 = load i8, i8* %arrayidx51, align 1, !tbaa !17
  %83 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %83, i32 1
  store i8 %82, i8* %arrayidx52, align 1, !tbaa !17
  %84 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr53 = getelementptr inbounds i32, i32* %84, i32 3
  store i32* %add.ptr53, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc54

for.inc54:                                        ; preds = %for.body27
  %85 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc55 = add nsw i32 %85, 1
  store i32 %inc55, i32* %ctr, align 4, !tbaa !6
  br label %for.cond24

for.end56:                                        ; preds = %for.cond24
  %86 = bitcast [9 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %86) #2
  %87 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #2
  %88 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #2
  %89 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #2
  %90 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #2
  %91 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #2
  %92 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #2
  %93 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #2
  %94 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #2
  %95 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #2
  %96 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_9x9(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp3 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %tmp14 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [72 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %19, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = bitcast [72 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 288, i8* %21) #2
  %22 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %22, i16** %inptr, align 4, !tbaa !2
  %23 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %23, i32 0, i32 20
  %24 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %25 = bitcast i8* %24 to i32*
  store i32* %25, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [72 x i32], [72 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %26 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %26, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %27 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %27, i32 0
  %28 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %28 to i32
  %29 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %29, i32 0
  %30 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %30
  store i32 %mul, i32* %tmp0, align 4, !tbaa !15
  %31 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl = shl i32 %31, 13
  store i32 %shl, i32* %tmp0, align 4, !tbaa !15
  %32 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add = add nsw i32 %32, 1024
  store i32 %add, i32* %tmp0, align 4, !tbaa !15
  %33 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %33, i32 16
  %34 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %34 to i32
  %35 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %35, i32 16
  %36 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %36
  store i32 %mul5, i32* %z1, align 4, !tbaa !15
  %37 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %37, i32 32
  %38 = load i16, i16* %arrayidx6, align 2, !tbaa !14
  %conv7 = sext i16 %38 to i32
  %39 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %39, i32 32
  %40 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %mul9 = mul nsw i32 %conv7, %40
  store i32 %mul9, i32* %z2, align 4, !tbaa !15
  %41 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %41, i32 48
  %42 = load i16, i16* %arrayidx10, align 2, !tbaa !14
  %conv11 = sext i16 %42 to i32
  %43 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %43, i32 48
  %44 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %mul13 = mul nsw i32 %conv11, %44
  store i32 %mul13, i32* %z3, align 4, !tbaa !15
  %45 = load i32, i32* %z3, align 4, !tbaa !15
  %mul14 = mul nsw i32 %45, 5793
  store i32 %mul14, i32* %tmp3, align 4, !tbaa !15
  %46 = load i32, i32* %tmp0, align 4, !tbaa !15
  %47 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add15 = add nsw i32 %46, %47
  store i32 %add15, i32* %tmp1, align 4, !tbaa !15
  %48 = load i32, i32* %tmp0, align 4, !tbaa !15
  %49 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub = sub nsw i32 %48, %49
  %50 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub16 = sub nsw i32 %sub, %50
  store i32 %sub16, i32* %tmp2, align 4, !tbaa !15
  %51 = load i32, i32* %z1, align 4, !tbaa !15
  %52 = load i32, i32* %z2, align 4, !tbaa !15
  %sub17 = sub nsw i32 %51, %52
  %mul18 = mul nsw i32 %sub17, 5793
  store i32 %mul18, i32* %tmp0, align 4, !tbaa !15
  %53 = load i32, i32* %tmp2, align 4, !tbaa !15
  %54 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add19 = add nsw i32 %53, %54
  store i32 %add19, i32* %tmp11, align 4, !tbaa !15
  %55 = load i32, i32* %tmp2, align 4, !tbaa !15
  %56 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub20 = sub nsw i32 %55, %56
  %57 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub21 = sub nsw i32 %sub20, %57
  store i32 %sub21, i32* %tmp14, align 4, !tbaa !15
  %58 = load i32, i32* %z1, align 4, !tbaa !15
  %59 = load i32, i32* %z2, align 4, !tbaa !15
  %add22 = add nsw i32 %58, %59
  %mul23 = mul nsw i32 %add22, 10887
  store i32 %mul23, i32* %tmp0, align 4, !tbaa !15
  %60 = load i32, i32* %z1, align 4, !tbaa !15
  %mul24 = mul nsw i32 %60, 8875
  store i32 %mul24, i32* %tmp2, align 4, !tbaa !15
  %61 = load i32, i32* %z2, align 4, !tbaa !15
  %mul25 = mul nsw i32 %61, 2012
  store i32 %mul25, i32* %tmp3, align 4, !tbaa !15
  %62 = load i32, i32* %tmp1, align 4, !tbaa !15
  %63 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add26 = add nsw i32 %62, %63
  %64 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub27 = sub nsw i32 %add26, %64
  store i32 %sub27, i32* %tmp10, align 4, !tbaa !15
  %65 = load i32, i32* %tmp1, align 4, !tbaa !15
  %66 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub28 = sub nsw i32 %65, %66
  %67 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add29 = add nsw i32 %sub28, %67
  store i32 %add29, i32* %tmp12, align 4, !tbaa !15
  %68 = load i32, i32* %tmp1, align 4, !tbaa !15
  %69 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub30 = sub nsw i32 %68, %69
  %70 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add31 = add nsw i32 %sub30, %70
  store i32 %add31, i32* %tmp13, align 4, !tbaa !15
  %71 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i16, i16* %71, i32 8
  %72 = load i16, i16* %arrayidx32, align 2, !tbaa !14
  %conv33 = sext i16 %72 to i32
  %73 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %73, i32 8
  %74 = load i32, i32* %arrayidx34, align 4, !tbaa !6
  %mul35 = mul nsw i32 %conv33, %74
  store i32 %mul35, i32* %z1, align 4, !tbaa !15
  %75 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i16, i16* %75, i32 24
  %76 = load i16, i16* %arrayidx36, align 2, !tbaa !14
  %conv37 = sext i16 %76 to i32
  %77 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i32, i32* %77, i32 24
  %78 = load i32, i32* %arrayidx38, align 4, !tbaa !6
  %mul39 = mul nsw i32 %conv37, %78
  store i32 %mul39, i32* %z2, align 4, !tbaa !15
  %79 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i16, i16* %79, i32 40
  %80 = load i16, i16* %arrayidx40, align 2, !tbaa !14
  %conv41 = sext i16 %80 to i32
  %81 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i32, i32* %81, i32 40
  %82 = load i32, i32* %arrayidx42, align 4, !tbaa !6
  %mul43 = mul nsw i32 %conv41, %82
  store i32 %mul43, i32* %z3, align 4, !tbaa !15
  %83 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i16, i16* %83, i32 56
  %84 = load i16, i16* %arrayidx44, align 2, !tbaa !14
  %conv45 = sext i16 %84 to i32
  %85 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i32, i32* %85, i32 56
  %86 = load i32, i32* %arrayidx46, align 4, !tbaa !6
  %mul47 = mul nsw i32 %conv45, %86
  store i32 %mul47, i32* %z4, align 4, !tbaa !15
  %87 = load i32, i32* %z2, align 4, !tbaa !15
  %mul48 = mul nsw i32 %87, -10033
  store i32 %mul48, i32* %z2, align 4, !tbaa !15
  %88 = load i32, i32* %z1, align 4, !tbaa !15
  %89 = load i32, i32* %z3, align 4, !tbaa !15
  %add49 = add nsw i32 %88, %89
  %mul50 = mul nsw i32 %add49, 7447
  store i32 %mul50, i32* %tmp2, align 4, !tbaa !15
  %90 = load i32, i32* %z1, align 4, !tbaa !15
  %91 = load i32, i32* %z4, align 4, !tbaa !15
  %add51 = add nsw i32 %90, %91
  %mul52 = mul nsw i32 %add51, 3962
  store i32 %mul52, i32* %tmp3, align 4, !tbaa !15
  %92 = load i32, i32* %tmp2, align 4, !tbaa !15
  %93 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add53 = add nsw i32 %92, %93
  %94 = load i32, i32* %z2, align 4, !tbaa !15
  %sub54 = sub nsw i32 %add53, %94
  store i32 %sub54, i32* %tmp0, align 4, !tbaa !15
  %95 = load i32, i32* %z3, align 4, !tbaa !15
  %96 = load i32, i32* %z4, align 4, !tbaa !15
  %sub55 = sub nsw i32 %95, %96
  %mul56 = mul nsw i32 %sub55, 11409
  store i32 %mul56, i32* %tmp1, align 4, !tbaa !15
  %97 = load i32, i32* %z2, align 4, !tbaa !15
  %98 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub57 = sub nsw i32 %97, %98
  %99 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add58 = add nsw i32 %99, %sub57
  store i32 %add58, i32* %tmp2, align 4, !tbaa !15
  %100 = load i32, i32* %z2, align 4, !tbaa !15
  %101 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add59 = add nsw i32 %100, %101
  %102 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add60 = add nsw i32 %102, %add59
  store i32 %add60, i32* %tmp3, align 4, !tbaa !15
  %103 = load i32, i32* %z1, align 4, !tbaa !15
  %104 = load i32, i32* %z3, align 4, !tbaa !15
  %sub61 = sub nsw i32 %103, %104
  %105 = load i32, i32* %z4, align 4, !tbaa !15
  %sub62 = sub nsw i32 %sub61, %105
  %mul63 = mul nsw i32 %sub62, 10033
  store i32 %mul63, i32* %tmp1, align 4, !tbaa !15
  %106 = load i32, i32* %tmp10, align 4, !tbaa !15
  %107 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add64 = add nsw i32 %106, %107
  %shr = ashr i32 %add64, 11
  %108 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i32, i32* %108, i32 0
  store i32 %shr, i32* %arrayidx65, align 4, !tbaa !6
  %109 = load i32, i32* %tmp10, align 4, !tbaa !15
  %110 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub66 = sub nsw i32 %109, %110
  %shr67 = ashr i32 %sub66, 11
  %111 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i32, i32* %111, i32 64
  store i32 %shr67, i32* %arrayidx68, align 4, !tbaa !6
  %112 = load i32, i32* %tmp11, align 4, !tbaa !15
  %113 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add69 = add nsw i32 %112, %113
  %shr70 = ashr i32 %add69, 11
  %114 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %114, i32 8
  store i32 %shr70, i32* %arrayidx71, align 4, !tbaa !6
  %115 = load i32, i32* %tmp11, align 4, !tbaa !15
  %116 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub72 = sub nsw i32 %115, %116
  %shr73 = ashr i32 %sub72, 11
  %117 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i32, i32* %117, i32 56
  store i32 %shr73, i32* %arrayidx74, align 4, !tbaa !6
  %118 = load i32, i32* %tmp12, align 4, !tbaa !15
  %119 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add75 = add nsw i32 %118, %119
  %shr76 = ashr i32 %add75, 11
  %120 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds i32, i32* %120, i32 16
  store i32 %shr76, i32* %arrayidx77, align 4, !tbaa !6
  %121 = load i32, i32* %tmp12, align 4, !tbaa !15
  %122 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub78 = sub nsw i32 %121, %122
  %shr79 = ashr i32 %sub78, 11
  %123 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds i32, i32* %123, i32 48
  store i32 %shr79, i32* %arrayidx80, align 4, !tbaa !6
  %124 = load i32, i32* %tmp13, align 4, !tbaa !15
  %125 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add81 = add nsw i32 %124, %125
  %shr82 = ashr i32 %add81, 11
  %126 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %126, i32 24
  store i32 %shr82, i32* %arrayidx83, align 4, !tbaa !6
  %127 = load i32, i32* %tmp13, align 4, !tbaa !15
  %128 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub84 = sub nsw i32 %127, %128
  %shr85 = ashr i32 %sub84, 11
  %129 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i32, i32* %129, i32 40
  store i32 %shr85, i32* %arrayidx86, align 4, !tbaa !6
  %130 = load i32, i32* %tmp14, align 4, !tbaa !15
  %shr87 = ashr i32 %130, 11
  %131 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i32, i32* %131, i32 32
  store i32 %shr87, i32* %arrayidx88, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %132 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %132, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %133 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %133, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %134 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr89 = getelementptr inbounds i32, i32* %134, i32 1
  store i32* %incdec.ptr89, i32** %quantptr, align 4, !tbaa !2
  %135 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr90 = getelementptr inbounds i32, i32* %135, i32 1
  store i32* %incdec.ptr90, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay91 = getelementptr inbounds [72 x i32], [72 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay91, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond92

for.cond92:                                       ; preds = %for.inc187, %for.end
  %136 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp93 = icmp slt i32 %136, 9
  br i1 %cmp93, label %for.body95, label %for.end189

for.body95:                                       ; preds = %for.cond92
  %137 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %138 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx96 = getelementptr inbounds i8*, i8** %137, i32 %138
  %139 = load i8*, i8** %arrayidx96, align 4, !tbaa !2
  %140 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr97 = getelementptr inbounds i8, i8* %139, i32 %140
  store i8* %add.ptr97, i8** %outptr, align 4, !tbaa !2
  %141 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i32, i32* %141, i32 0
  %142 = load i32, i32* %arrayidx98, align 4, !tbaa !6
  %add99 = add nsw i32 %142, 16
  store i32 %add99, i32* %tmp0, align 4, !tbaa !15
  %143 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl100 = shl i32 %143, 13
  store i32 %shl100, i32* %tmp0, align 4, !tbaa !15
  %144 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i32, i32* %144, i32 2
  %145 = load i32, i32* %arrayidx101, align 4, !tbaa !6
  store i32 %145, i32* %z1, align 4, !tbaa !15
  %146 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %146, i32 4
  %147 = load i32, i32* %arrayidx102, align 4, !tbaa !6
  store i32 %147, i32* %z2, align 4, !tbaa !15
  %148 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %148, i32 6
  %149 = load i32, i32* %arrayidx103, align 4, !tbaa !6
  store i32 %149, i32* %z3, align 4, !tbaa !15
  %150 = load i32, i32* %z3, align 4, !tbaa !15
  %mul104 = mul nsw i32 %150, 5793
  store i32 %mul104, i32* %tmp3, align 4, !tbaa !15
  %151 = load i32, i32* %tmp0, align 4, !tbaa !15
  %152 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add105 = add nsw i32 %151, %152
  store i32 %add105, i32* %tmp1, align 4, !tbaa !15
  %153 = load i32, i32* %tmp0, align 4, !tbaa !15
  %154 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub106 = sub nsw i32 %153, %154
  %155 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub107 = sub nsw i32 %sub106, %155
  store i32 %sub107, i32* %tmp2, align 4, !tbaa !15
  %156 = load i32, i32* %z1, align 4, !tbaa !15
  %157 = load i32, i32* %z2, align 4, !tbaa !15
  %sub108 = sub nsw i32 %156, %157
  %mul109 = mul nsw i32 %sub108, 5793
  store i32 %mul109, i32* %tmp0, align 4, !tbaa !15
  %158 = load i32, i32* %tmp2, align 4, !tbaa !15
  %159 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add110 = add nsw i32 %158, %159
  store i32 %add110, i32* %tmp11, align 4, !tbaa !15
  %160 = load i32, i32* %tmp2, align 4, !tbaa !15
  %161 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub111 = sub nsw i32 %160, %161
  %162 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub112 = sub nsw i32 %sub111, %162
  store i32 %sub112, i32* %tmp14, align 4, !tbaa !15
  %163 = load i32, i32* %z1, align 4, !tbaa !15
  %164 = load i32, i32* %z2, align 4, !tbaa !15
  %add113 = add nsw i32 %163, %164
  %mul114 = mul nsw i32 %add113, 10887
  store i32 %mul114, i32* %tmp0, align 4, !tbaa !15
  %165 = load i32, i32* %z1, align 4, !tbaa !15
  %mul115 = mul nsw i32 %165, 8875
  store i32 %mul115, i32* %tmp2, align 4, !tbaa !15
  %166 = load i32, i32* %z2, align 4, !tbaa !15
  %mul116 = mul nsw i32 %166, 2012
  store i32 %mul116, i32* %tmp3, align 4, !tbaa !15
  %167 = load i32, i32* %tmp1, align 4, !tbaa !15
  %168 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add117 = add nsw i32 %167, %168
  %169 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub118 = sub nsw i32 %add117, %169
  store i32 %sub118, i32* %tmp10, align 4, !tbaa !15
  %170 = load i32, i32* %tmp1, align 4, !tbaa !15
  %171 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub119 = sub nsw i32 %170, %171
  %172 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add120 = add nsw i32 %sub119, %172
  store i32 %add120, i32* %tmp12, align 4, !tbaa !15
  %173 = load i32, i32* %tmp1, align 4, !tbaa !15
  %174 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub121 = sub nsw i32 %173, %174
  %175 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add122 = add nsw i32 %sub121, %175
  store i32 %add122, i32* %tmp13, align 4, !tbaa !15
  %176 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %176, i32 1
  %177 = load i32, i32* %arrayidx123, align 4, !tbaa !6
  store i32 %177, i32* %z1, align 4, !tbaa !15
  %178 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %178, i32 3
  %179 = load i32, i32* %arrayidx124, align 4, !tbaa !6
  store i32 %179, i32* %z2, align 4, !tbaa !15
  %180 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %180, i32 5
  %181 = load i32, i32* %arrayidx125, align 4, !tbaa !6
  store i32 %181, i32* %z3, align 4, !tbaa !15
  %182 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx126 = getelementptr inbounds i32, i32* %182, i32 7
  %183 = load i32, i32* %arrayidx126, align 4, !tbaa !6
  store i32 %183, i32* %z4, align 4, !tbaa !15
  %184 = load i32, i32* %z2, align 4, !tbaa !15
  %mul127 = mul nsw i32 %184, -10033
  store i32 %mul127, i32* %z2, align 4, !tbaa !15
  %185 = load i32, i32* %z1, align 4, !tbaa !15
  %186 = load i32, i32* %z3, align 4, !tbaa !15
  %add128 = add nsw i32 %185, %186
  %mul129 = mul nsw i32 %add128, 7447
  store i32 %mul129, i32* %tmp2, align 4, !tbaa !15
  %187 = load i32, i32* %z1, align 4, !tbaa !15
  %188 = load i32, i32* %z4, align 4, !tbaa !15
  %add130 = add nsw i32 %187, %188
  %mul131 = mul nsw i32 %add130, 3962
  store i32 %mul131, i32* %tmp3, align 4, !tbaa !15
  %189 = load i32, i32* %tmp2, align 4, !tbaa !15
  %190 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add132 = add nsw i32 %189, %190
  %191 = load i32, i32* %z2, align 4, !tbaa !15
  %sub133 = sub nsw i32 %add132, %191
  store i32 %sub133, i32* %tmp0, align 4, !tbaa !15
  %192 = load i32, i32* %z3, align 4, !tbaa !15
  %193 = load i32, i32* %z4, align 4, !tbaa !15
  %sub134 = sub nsw i32 %192, %193
  %mul135 = mul nsw i32 %sub134, 11409
  store i32 %mul135, i32* %tmp1, align 4, !tbaa !15
  %194 = load i32, i32* %z2, align 4, !tbaa !15
  %195 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub136 = sub nsw i32 %194, %195
  %196 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add137 = add nsw i32 %196, %sub136
  store i32 %add137, i32* %tmp2, align 4, !tbaa !15
  %197 = load i32, i32* %z2, align 4, !tbaa !15
  %198 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add138 = add nsw i32 %197, %198
  %199 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add139 = add nsw i32 %199, %add138
  store i32 %add139, i32* %tmp3, align 4, !tbaa !15
  %200 = load i32, i32* %z1, align 4, !tbaa !15
  %201 = load i32, i32* %z3, align 4, !tbaa !15
  %sub140 = sub nsw i32 %200, %201
  %202 = load i32, i32* %z4, align 4, !tbaa !15
  %sub141 = sub nsw i32 %sub140, %202
  %mul142 = mul nsw i32 %sub141, 10033
  store i32 %mul142, i32* %tmp1, align 4, !tbaa !15
  %203 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %204 = load i32, i32* %tmp10, align 4, !tbaa !15
  %205 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add143 = add nsw i32 %204, %205
  %shr144 = ashr i32 %add143, 18
  %and = and i32 %shr144, 1023
  %arrayidx145 = getelementptr inbounds i8, i8* %203, i32 %and
  %206 = load i8, i8* %arrayidx145, align 1, !tbaa !17
  %207 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i8, i8* %207, i32 0
  store i8 %206, i8* %arrayidx146, align 1, !tbaa !17
  %208 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %209 = load i32, i32* %tmp10, align 4, !tbaa !15
  %210 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub147 = sub nsw i32 %209, %210
  %shr148 = ashr i32 %sub147, 18
  %and149 = and i32 %shr148, 1023
  %arrayidx150 = getelementptr inbounds i8, i8* %208, i32 %and149
  %211 = load i8, i8* %arrayidx150, align 1, !tbaa !17
  %212 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i8, i8* %212, i32 8
  store i8 %211, i8* %arrayidx151, align 1, !tbaa !17
  %213 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %214 = load i32, i32* %tmp11, align 4, !tbaa !15
  %215 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add152 = add nsw i32 %214, %215
  %shr153 = ashr i32 %add152, 18
  %and154 = and i32 %shr153, 1023
  %arrayidx155 = getelementptr inbounds i8, i8* %213, i32 %and154
  %216 = load i8, i8* %arrayidx155, align 1, !tbaa !17
  %217 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds i8, i8* %217, i32 1
  store i8 %216, i8* %arrayidx156, align 1, !tbaa !17
  %218 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %219 = load i32, i32* %tmp11, align 4, !tbaa !15
  %220 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub157 = sub nsw i32 %219, %220
  %shr158 = ashr i32 %sub157, 18
  %and159 = and i32 %shr158, 1023
  %arrayidx160 = getelementptr inbounds i8, i8* %218, i32 %and159
  %221 = load i8, i8* %arrayidx160, align 1, !tbaa !17
  %222 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx161 = getelementptr inbounds i8, i8* %222, i32 7
  store i8 %221, i8* %arrayidx161, align 1, !tbaa !17
  %223 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %224 = load i32, i32* %tmp12, align 4, !tbaa !15
  %225 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add162 = add nsw i32 %224, %225
  %shr163 = ashr i32 %add162, 18
  %and164 = and i32 %shr163, 1023
  %arrayidx165 = getelementptr inbounds i8, i8* %223, i32 %and164
  %226 = load i8, i8* %arrayidx165, align 1, !tbaa !17
  %227 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i8, i8* %227, i32 2
  store i8 %226, i8* %arrayidx166, align 1, !tbaa !17
  %228 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %229 = load i32, i32* %tmp12, align 4, !tbaa !15
  %230 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub167 = sub nsw i32 %229, %230
  %shr168 = ashr i32 %sub167, 18
  %and169 = and i32 %shr168, 1023
  %arrayidx170 = getelementptr inbounds i8, i8* %228, i32 %and169
  %231 = load i8, i8* %arrayidx170, align 1, !tbaa !17
  %232 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx171 = getelementptr inbounds i8, i8* %232, i32 6
  store i8 %231, i8* %arrayidx171, align 1, !tbaa !17
  %233 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %234 = load i32, i32* %tmp13, align 4, !tbaa !15
  %235 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add172 = add nsw i32 %234, %235
  %shr173 = ashr i32 %add172, 18
  %and174 = and i32 %shr173, 1023
  %arrayidx175 = getelementptr inbounds i8, i8* %233, i32 %and174
  %236 = load i8, i8* %arrayidx175, align 1, !tbaa !17
  %237 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds i8, i8* %237, i32 3
  store i8 %236, i8* %arrayidx176, align 1, !tbaa !17
  %238 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %239 = load i32, i32* %tmp13, align 4, !tbaa !15
  %240 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub177 = sub nsw i32 %239, %240
  %shr178 = ashr i32 %sub177, 18
  %and179 = and i32 %shr178, 1023
  %arrayidx180 = getelementptr inbounds i8, i8* %238, i32 %and179
  %241 = load i8, i8* %arrayidx180, align 1, !tbaa !17
  %242 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx181 = getelementptr inbounds i8, i8* %242, i32 5
  store i8 %241, i8* %arrayidx181, align 1, !tbaa !17
  %243 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %244 = load i32, i32* %tmp14, align 4, !tbaa !15
  %shr182 = ashr i32 %244, 18
  %and183 = and i32 %shr182, 1023
  %arrayidx184 = getelementptr inbounds i8, i8* %243, i32 %and183
  %245 = load i8, i8* %arrayidx184, align 1, !tbaa !17
  %246 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx185 = getelementptr inbounds i8, i8* %246, i32 4
  store i8 %245, i8* %arrayidx185, align 1, !tbaa !17
  %247 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr186 = getelementptr inbounds i32, i32* %247, i32 8
  store i32* %add.ptr186, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc187

for.inc187:                                       ; preds = %for.body95
  %248 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc188 = add nsw i32 %248, 1
  store i32 %inc188, i32* %ctr, align 4, !tbaa !6
  br label %for.cond92

for.end189:                                       ; preds = %for.cond92
  %249 = bitcast [72 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 288, i8* %249) #2
  %250 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #2
  %251 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #2
  %252 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #2
  %253 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #2
  %254 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #2
  %255 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #2
  %256 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #2
  %257 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #2
  %258 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #2
  %259 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #2
  %260 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #2
  %261 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #2
  %262 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #2
  %263 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #2
  %264 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #2
  %265 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #2
  %266 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #2
  %267 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #2
  %268 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_10x10(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %tmp14 = alloca i32, align 4
  %tmp20 = alloca i32, align 4
  %tmp21 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %tmp23 = alloca i32, align 4
  %tmp24 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %z5 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [80 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 65
  %21 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %22 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #2
  %23 = bitcast [80 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 320, i8* %23) #2
  %24 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %24, i16** %inptr, align 4, !tbaa !2
  %25 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %25, i32 0, i32 20
  %26 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %27 = bitcast i8* %26 to i32*
  store i32* %27, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [80 x i32], [80 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %28 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %28, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %29 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %29, i32 0
  %30 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %30 to i32
  %31 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %31, i32 0
  %32 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %32
  store i32 %mul, i32* %z3, align 4, !tbaa !15
  %33 = load i32, i32* %z3, align 4, !tbaa !15
  %shl = shl i32 %33, 13
  store i32 %shl, i32* %z3, align 4, !tbaa !15
  %34 = load i32, i32* %z3, align 4, !tbaa !15
  %add = add nsw i32 %34, 1024
  store i32 %add, i32* %z3, align 4, !tbaa !15
  %35 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %35, i32 32
  %36 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %36 to i32
  %37 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %37, i32 32
  %38 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %38
  store i32 %mul5, i32* %z4, align 4, !tbaa !15
  %39 = load i32, i32* %z4, align 4, !tbaa !15
  %mul6 = mul nsw i32 %39, 9373
  store i32 %mul6, i32* %z1, align 4, !tbaa !15
  %40 = load i32, i32* %z4, align 4, !tbaa !15
  %mul7 = mul nsw i32 %40, 3580
  store i32 %mul7, i32* %z2, align 4, !tbaa !15
  %41 = load i32, i32* %z3, align 4, !tbaa !15
  %42 = load i32, i32* %z1, align 4, !tbaa !15
  %add8 = add nsw i32 %41, %42
  store i32 %add8, i32* %tmp10, align 4, !tbaa !15
  %43 = load i32, i32* %z3, align 4, !tbaa !15
  %44 = load i32, i32* %z2, align 4, !tbaa !15
  %sub = sub nsw i32 %43, %44
  store i32 %sub, i32* %tmp11, align 4, !tbaa !15
  %45 = load i32, i32* %z3, align 4, !tbaa !15
  %46 = load i32, i32* %z1, align 4, !tbaa !15
  %47 = load i32, i32* %z2, align 4, !tbaa !15
  %sub9 = sub nsw i32 %46, %47
  %shl10 = shl i32 %sub9, 1
  %sub11 = sub nsw i32 %45, %shl10
  %shr = ashr i32 %sub11, 11
  store i32 %shr, i32* %tmp22, align 4, !tbaa !15
  %48 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i16, i16* %48, i32 16
  %49 = load i16, i16* %arrayidx12, align 2, !tbaa !14
  %conv13 = sext i16 %49 to i32
  %50 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %50, i32 16
  %51 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  %mul15 = mul nsw i32 %conv13, %51
  store i32 %mul15, i32* %z2, align 4, !tbaa !15
  %52 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i16, i16* %52, i32 48
  %53 = load i16, i16* %arrayidx16, align 2, !tbaa !14
  %conv17 = sext i16 %53 to i32
  %54 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i32, i32* %54, i32 48
  %55 = load i32, i32* %arrayidx18, align 4, !tbaa !6
  %mul19 = mul nsw i32 %conv17, %55
  store i32 %mul19, i32* %z3, align 4, !tbaa !15
  %56 = load i32, i32* %z2, align 4, !tbaa !15
  %57 = load i32, i32* %z3, align 4, !tbaa !15
  %add20 = add nsw i32 %56, %57
  %mul21 = mul nsw i32 %add20, 6810
  store i32 %mul21, i32* %z1, align 4, !tbaa !15
  %58 = load i32, i32* %z1, align 4, !tbaa !15
  %59 = load i32, i32* %z2, align 4, !tbaa !15
  %mul22 = mul nsw i32 %59, 4209
  %add23 = add nsw i32 %58, %mul22
  store i32 %add23, i32* %tmp12, align 4, !tbaa !15
  %60 = load i32, i32* %z1, align 4, !tbaa !15
  %61 = load i32, i32* %z3, align 4, !tbaa !15
  %mul24 = mul nsw i32 %61, 17828
  %sub25 = sub nsw i32 %60, %mul24
  store i32 %sub25, i32* %tmp13, align 4, !tbaa !15
  %62 = load i32, i32* %tmp10, align 4, !tbaa !15
  %63 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add26 = add nsw i32 %62, %63
  store i32 %add26, i32* %tmp20, align 4, !tbaa !15
  %64 = load i32, i32* %tmp10, align 4, !tbaa !15
  %65 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub27 = sub nsw i32 %64, %65
  store i32 %sub27, i32* %tmp24, align 4, !tbaa !15
  %66 = load i32, i32* %tmp11, align 4, !tbaa !15
  %67 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add28 = add nsw i32 %66, %67
  store i32 %add28, i32* %tmp21, align 4, !tbaa !15
  %68 = load i32, i32* %tmp11, align 4, !tbaa !15
  %69 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub29 = sub nsw i32 %68, %69
  store i32 %sub29, i32* %tmp23, align 4, !tbaa !15
  %70 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i16, i16* %70, i32 8
  %71 = load i16, i16* %arrayidx30, align 2, !tbaa !14
  %conv31 = sext i16 %71 to i32
  %72 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i32, i32* %72, i32 8
  %73 = load i32, i32* %arrayidx32, align 4, !tbaa !6
  %mul33 = mul nsw i32 %conv31, %73
  store i32 %mul33, i32* %z1, align 4, !tbaa !15
  %74 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i16, i16* %74, i32 24
  %75 = load i16, i16* %arrayidx34, align 2, !tbaa !14
  %conv35 = sext i16 %75 to i32
  %76 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i32, i32* %76, i32 24
  %77 = load i32, i32* %arrayidx36, align 4, !tbaa !6
  %mul37 = mul nsw i32 %conv35, %77
  store i32 %mul37, i32* %z2, align 4, !tbaa !15
  %78 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i16, i16* %78, i32 40
  %79 = load i16, i16* %arrayidx38, align 2, !tbaa !14
  %conv39 = sext i16 %79 to i32
  %80 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %80, i32 40
  %81 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %mul41 = mul nsw i32 %conv39, %81
  store i32 %mul41, i32* %z3, align 4, !tbaa !15
  %82 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i16, i16* %82, i32 56
  %83 = load i16, i16* %arrayidx42, align 2, !tbaa !14
  %conv43 = sext i16 %83 to i32
  %84 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %84, i32 56
  %85 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %mul45 = mul nsw i32 %conv43, %85
  store i32 %mul45, i32* %z4, align 4, !tbaa !15
  %86 = load i32, i32* %z2, align 4, !tbaa !15
  %87 = load i32, i32* %z4, align 4, !tbaa !15
  %add46 = add nsw i32 %86, %87
  store i32 %add46, i32* %tmp11, align 4, !tbaa !15
  %88 = load i32, i32* %z2, align 4, !tbaa !15
  %89 = load i32, i32* %z4, align 4, !tbaa !15
  %sub47 = sub nsw i32 %88, %89
  store i32 %sub47, i32* %tmp13, align 4, !tbaa !15
  %90 = load i32, i32* %tmp13, align 4, !tbaa !15
  %mul48 = mul nsw i32 %90, 2531
  store i32 %mul48, i32* %tmp12, align 4, !tbaa !15
  %91 = load i32, i32* %z3, align 4, !tbaa !15
  %shl49 = shl i32 %91, 13
  store i32 %shl49, i32* %z5, align 4, !tbaa !15
  %92 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul50 = mul nsw i32 %92, 7791
  store i32 %mul50, i32* %z2, align 4, !tbaa !15
  %93 = load i32, i32* %z5, align 4, !tbaa !15
  %94 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add51 = add nsw i32 %93, %94
  store i32 %add51, i32* %z4, align 4, !tbaa !15
  %95 = load i32, i32* %z1, align 4, !tbaa !15
  %mul52 = mul nsw i32 %95, 11443
  %96 = load i32, i32* %z2, align 4, !tbaa !15
  %add53 = add nsw i32 %mul52, %96
  %97 = load i32, i32* %z4, align 4, !tbaa !15
  %add54 = add nsw i32 %add53, %97
  store i32 %add54, i32* %tmp10, align 4, !tbaa !15
  %98 = load i32, i32* %z1, align 4, !tbaa !15
  %mul55 = mul nsw i32 %98, 1812
  %99 = load i32, i32* %z2, align 4, !tbaa !15
  %sub56 = sub nsw i32 %mul55, %99
  %100 = load i32, i32* %z4, align 4, !tbaa !15
  %add57 = add nsw i32 %sub56, %100
  store i32 %add57, i32* %tmp14, align 4, !tbaa !15
  %101 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul58 = mul nsw i32 %101, 4815
  store i32 %mul58, i32* %z2, align 4, !tbaa !15
  %102 = load i32, i32* %z5, align 4, !tbaa !15
  %103 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub59 = sub nsw i32 %102, %103
  %104 = load i32, i32* %tmp13, align 4, !tbaa !15
  %shl60 = shl i32 %104, 12
  %sub61 = sub nsw i32 %sub59, %shl60
  store i32 %sub61, i32* %z4, align 4, !tbaa !15
  %105 = load i32, i32* %z1, align 4, !tbaa !15
  %106 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub62 = sub nsw i32 %105, %106
  %107 = load i32, i32* %z3, align 4, !tbaa !15
  %sub63 = sub nsw i32 %sub62, %107
  %shl64 = shl i32 %sub63, 2
  store i32 %shl64, i32* %tmp12, align 4, !tbaa !15
  %108 = load i32, i32* %z1, align 4, !tbaa !15
  %mul65 = mul nsw i32 %108, 10323
  %109 = load i32, i32* %z2, align 4, !tbaa !15
  %sub66 = sub nsw i32 %mul65, %109
  %110 = load i32, i32* %z4, align 4, !tbaa !15
  %sub67 = sub nsw i32 %sub66, %110
  store i32 %sub67, i32* %tmp11, align 4, !tbaa !15
  %111 = load i32, i32* %z1, align 4, !tbaa !15
  %mul68 = mul nsw i32 %111, 5260
  %112 = load i32, i32* %z2, align 4, !tbaa !15
  %sub69 = sub nsw i32 %mul68, %112
  %113 = load i32, i32* %z4, align 4, !tbaa !15
  %add70 = add nsw i32 %sub69, %113
  store i32 %add70, i32* %tmp13, align 4, !tbaa !15
  %114 = load i32, i32* %tmp20, align 4, !tbaa !15
  %115 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add71 = add nsw i32 %114, %115
  %shr72 = ashr i32 %add71, 11
  %116 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds i32, i32* %116, i32 0
  store i32 %shr72, i32* %arrayidx73, align 4, !tbaa !6
  %117 = load i32, i32* %tmp20, align 4, !tbaa !15
  %118 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub74 = sub nsw i32 %117, %118
  %shr75 = ashr i32 %sub74, 11
  %119 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %119, i32 72
  store i32 %shr75, i32* %arrayidx76, align 4, !tbaa !6
  %120 = load i32, i32* %tmp21, align 4, !tbaa !15
  %121 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add77 = add nsw i32 %120, %121
  %shr78 = ashr i32 %add77, 11
  %122 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %122, i32 8
  store i32 %shr78, i32* %arrayidx79, align 4, !tbaa !6
  %123 = load i32, i32* %tmp21, align 4, !tbaa !15
  %124 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub80 = sub nsw i32 %123, %124
  %shr81 = ashr i32 %sub80, 11
  %125 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %125, i32 64
  store i32 %shr81, i32* %arrayidx82, align 4, !tbaa !6
  %126 = load i32, i32* %tmp22, align 4, !tbaa !15
  %127 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add83 = add nsw i32 %126, %127
  %128 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i32, i32* %128, i32 16
  store i32 %add83, i32* %arrayidx84, align 4, !tbaa !6
  %129 = load i32, i32* %tmp22, align 4, !tbaa !15
  %130 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub85 = sub nsw i32 %129, %130
  %131 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i32, i32* %131, i32 56
  store i32 %sub85, i32* %arrayidx86, align 4, !tbaa !6
  %132 = load i32, i32* %tmp23, align 4, !tbaa !15
  %133 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add87 = add nsw i32 %132, %133
  %shr88 = ashr i32 %add87, 11
  %134 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i32, i32* %134, i32 24
  store i32 %shr88, i32* %arrayidx89, align 4, !tbaa !6
  %135 = load i32, i32* %tmp23, align 4, !tbaa !15
  %136 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub90 = sub nsw i32 %135, %136
  %shr91 = ashr i32 %sub90, 11
  %137 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i32, i32* %137, i32 48
  store i32 %shr91, i32* %arrayidx92, align 4, !tbaa !6
  %138 = load i32, i32* %tmp24, align 4, !tbaa !15
  %139 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add93 = add nsw i32 %138, %139
  %shr94 = ashr i32 %add93, 11
  %140 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i32, i32* %140, i32 32
  store i32 %shr94, i32* %arrayidx95, align 4, !tbaa !6
  %141 = load i32, i32* %tmp24, align 4, !tbaa !15
  %142 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub96 = sub nsw i32 %141, %142
  %shr97 = ashr i32 %sub96, 11
  %143 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i32, i32* %143, i32 40
  store i32 %shr97, i32* %arrayidx98, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %144 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %144, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %145 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %145, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %146 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr99 = getelementptr inbounds i32, i32* %146, i32 1
  store i32* %incdec.ptr99, i32** %quantptr, align 4, !tbaa !2
  %147 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr100 = getelementptr inbounds i32, i32* %147, i32 1
  store i32* %incdec.ptr100, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay101 = getelementptr inbounds [80 x i32], [80 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay101, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond102

for.cond102:                                      ; preds = %for.inc210, %for.end
  %148 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp103 = icmp slt i32 %148, 10
  br i1 %cmp103, label %for.body105, label %for.end212

for.body105:                                      ; preds = %for.cond102
  %149 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %150 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx106 = getelementptr inbounds i8*, i8** %149, i32 %150
  %151 = load i8*, i8** %arrayidx106, align 4, !tbaa !2
  %152 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr107 = getelementptr inbounds i8, i8* %151, i32 %152
  store i8* %add.ptr107, i8** %outptr, align 4, !tbaa !2
  %153 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %153, i32 0
  %154 = load i32, i32* %arrayidx108, align 4, !tbaa !6
  %add109 = add nsw i32 %154, 16
  store i32 %add109, i32* %z3, align 4, !tbaa !15
  %155 = load i32, i32* %z3, align 4, !tbaa !15
  %shl110 = shl i32 %155, 13
  store i32 %shl110, i32* %z3, align 4, !tbaa !15
  %156 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i32, i32* %156, i32 4
  %157 = load i32, i32* %arrayidx111, align 4, !tbaa !6
  store i32 %157, i32* %z4, align 4, !tbaa !15
  %158 = load i32, i32* %z4, align 4, !tbaa !15
  %mul112 = mul nsw i32 %158, 9373
  store i32 %mul112, i32* %z1, align 4, !tbaa !15
  %159 = load i32, i32* %z4, align 4, !tbaa !15
  %mul113 = mul nsw i32 %159, 3580
  store i32 %mul113, i32* %z2, align 4, !tbaa !15
  %160 = load i32, i32* %z3, align 4, !tbaa !15
  %161 = load i32, i32* %z1, align 4, !tbaa !15
  %add114 = add nsw i32 %160, %161
  store i32 %add114, i32* %tmp10, align 4, !tbaa !15
  %162 = load i32, i32* %z3, align 4, !tbaa !15
  %163 = load i32, i32* %z2, align 4, !tbaa !15
  %sub115 = sub nsw i32 %162, %163
  store i32 %sub115, i32* %tmp11, align 4, !tbaa !15
  %164 = load i32, i32* %z3, align 4, !tbaa !15
  %165 = load i32, i32* %z1, align 4, !tbaa !15
  %166 = load i32, i32* %z2, align 4, !tbaa !15
  %sub116 = sub nsw i32 %165, %166
  %shl117 = shl i32 %sub116, 1
  %sub118 = sub nsw i32 %164, %shl117
  store i32 %sub118, i32* %tmp22, align 4, !tbaa !15
  %167 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i32, i32* %167, i32 2
  %168 = load i32, i32* %arrayidx119, align 4, !tbaa !6
  store i32 %168, i32* %z2, align 4, !tbaa !15
  %169 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i32, i32* %169, i32 6
  %170 = load i32, i32* %arrayidx120, align 4, !tbaa !6
  store i32 %170, i32* %z3, align 4, !tbaa !15
  %171 = load i32, i32* %z2, align 4, !tbaa !15
  %172 = load i32, i32* %z3, align 4, !tbaa !15
  %add121 = add nsw i32 %171, %172
  %mul122 = mul nsw i32 %add121, 6810
  store i32 %mul122, i32* %z1, align 4, !tbaa !15
  %173 = load i32, i32* %z1, align 4, !tbaa !15
  %174 = load i32, i32* %z2, align 4, !tbaa !15
  %mul123 = mul nsw i32 %174, 4209
  %add124 = add nsw i32 %173, %mul123
  store i32 %add124, i32* %tmp12, align 4, !tbaa !15
  %175 = load i32, i32* %z1, align 4, !tbaa !15
  %176 = load i32, i32* %z3, align 4, !tbaa !15
  %mul125 = mul nsw i32 %176, 17828
  %sub126 = sub nsw i32 %175, %mul125
  store i32 %sub126, i32* %tmp13, align 4, !tbaa !15
  %177 = load i32, i32* %tmp10, align 4, !tbaa !15
  %178 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add127 = add nsw i32 %177, %178
  store i32 %add127, i32* %tmp20, align 4, !tbaa !15
  %179 = load i32, i32* %tmp10, align 4, !tbaa !15
  %180 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub128 = sub nsw i32 %179, %180
  store i32 %sub128, i32* %tmp24, align 4, !tbaa !15
  %181 = load i32, i32* %tmp11, align 4, !tbaa !15
  %182 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add129 = add nsw i32 %181, %182
  store i32 %add129, i32* %tmp21, align 4, !tbaa !15
  %183 = load i32, i32* %tmp11, align 4, !tbaa !15
  %184 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub130 = sub nsw i32 %183, %184
  store i32 %sub130, i32* %tmp23, align 4, !tbaa !15
  %185 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i32, i32* %185, i32 1
  %186 = load i32, i32* %arrayidx131, align 4, !tbaa !6
  store i32 %186, i32* %z1, align 4, !tbaa !15
  %187 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %187, i32 3
  %188 = load i32, i32* %arrayidx132, align 4, !tbaa !6
  store i32 %188, i32* %z2, align 4, !tbaa !15
  %189 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i32, i32* %189, i32 5
  %190 = load i32, i32* %arrayidx133, align 4, !tbaa !6
  store i32 %190, i32* %z3, align 4, !tbaa !15
  %191 = load i32, i32* %z3, align 4, !tbaa !15
  %shl134 = shl i32 %191, 13
  store i32 %shl134, i32* %z3, align 4, !tbaa !15
  %192 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i32, i32* %192, i32 7
  %193 = load i32, i32* %arrayidx135, align 4, !tbaa !6
  store i32 %193, i32* %z4, align 4, !tbaa !15
  %194 = load i32, i32* %z2, align 4, !tbaa !15
  %195 = load i32, i32* %z4, align 4, !tbaa !15
  %add136 = add nsw i32 %194, %195
  store i32 %add136, i32* %tmp11, align 4, !tbaa !15
  %196 = load i32, i32* %z2, align 4, !tbaa !15
  %197 = load i32, i32* %z4, align 4, !tbaa !15
  %sub137 = sub nsw i32 %196, %197
  store i32 %sub137, i32* %tmp13, align 4, !tbaa !15
  %198 = load i32, i32* %tmp13, align 4, !tbaa !15
  %mul138 = mul nsw i32 %198, 2531
  store i32 %mul138, i32* %tmp12, align 4, !tbaa !15
  %199 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul139 = mul nsw i32 %199, 7791
  store i32 %mul139, i32* %z2, align 4, !tbaa !15
  %200 = load i32, i32* %z3, align 4, !tbaa !15
  %201 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add140 = add nsw i32 %200, %201
  store i32 %add140, i32* %z4, align 4, !tbaa !15
  %202 = load i32, i32* %z1, align 4, !tbaa !15
  %mul141 = mul nsw i32 %202, 11443
  %203 = load i32, i32* %z2, align 4, !tbaa !15
  %add142 = add nsw i32 %mul141, %203
  %204 = load i32, i32* %z4, align 4, !tbaa !15
  %add143 = add nsw i32 %add142, %204
  store i32 %add143, i32* %tmp10, align 4, !tbaa !15
  %205 = load i32, i32* %z1, align 4, !tbaa !15
  %mul144 = mul nsw i32 %205, 1812
  %206 = load i32, i32* %z2, align 4, !tbaa !15
  %sub145 = sub nsw i32 %mul144, %206
  %207 = load i32, i32* %z4, align 4, !tbaa !15
  %add146 = add nsw i32 %sub145, %207
  store i32 %add146, i32* %tmp14, align 4, !tbaa !15
  %208 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul147 = mul nsw i32 %208, 4815
  store i32 %mul147, i32* %z2, align 4, !tbaa !15
  %209 = load i32, i32* %z3, align 4, !tbaa !15
  %210 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub148 = sub nsw i32 %209, %210
  %211 = load i32, i32* %tmp13, align 4, !tbaa !15
  %shl149 = shl i32 %211, 12
  %sub150 = sub nsw i32 %sub148, %shl149
  store i32 %sub150, i32* %z4, align 4, !tbaa !15
  %212 = load i32, i32* %z1, align 4, !tbaa !15
  %213 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub151 = sub nsw i32 %212, %213
  %shl152 = shl i32 %sub151, 13
  %214 = load i32, i32* %z3, align 4, !tbaa !15
  %sub153 = sub nsw i32 %shl152, %214
  store i32 %sub153, i32* %tmp12, align 4, !tbaa !15
  %215 = load i32, i32* %z1, align 4, !tbaa !15
  %mul154 = mul nsw i32 %215, 10323
  %216 = load i32, i32* %z2, align 4, !tbaa !15
  %sub155 = sub nsw i32 %mul154, %216
  %217 = load i32, i32* %z4, align 4, !tbaa !15
  %sub156 = sub nsw i32 %sub155, %217
  store i32 %sub156, i32* %tmp11, align 4, !tbaa !15
  %218 = load i32, i32* %z1, align 4, !tbaa !15
  %mul157 = mul nsw i32 %218, 5260
  %219 = load i32, i32* %z2, align 4, !tbaa !15
  %sub158 = sub nsw i32 %mul157, %219
  %220 = load i32, i32* %z4, align 4, !tbaa !15
  %add159 = add nsw i32 %sub158, %220
  store i32 %add159, i32* %tmp13, align 4, !tbaa !15
  %221 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %222 = load i32, i32* %tmp20, align 4, !tbaa !15
  %223 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add160 = add nsw i32 %222, %223
  %shr161 = ashr i32 %add160, 18
  %and = and i32 %shr161, 1023
  %arrayidx162 = getelementptr inbounds i8, i8* %221, i32 %and
  %224 = load i8, i8* %arrayidx162, align 1, !tbaa !17
  %225 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds i8, i8* %225, i32 0
  store i8 %224, i8* %arrayidx163, align 1, !tbaa !17
  %226 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %227 = load i32, i32* %tmp20, align 4, !tbaa !15
  %228 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub164 = sub nsw i32 %227, %228
  %shr165 = ashr i32 %sub164, 18
  %and166 = and i32 %shr165, 1023
  %arrayidx167 = getelementptr inbounds i8, i8* %226, i32 %and166
  %229 = load i8, i8* %arrayidx167, align 1, !tbaa !17
  %230 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx168 = getelementptr inbounds i8, i8* %230, i32 9
  store i8 %229, i8* %arrayidx168, align 1, !tbaa !17
  %231 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %232 = load i32, i32* %tmp21, align 4, !tbaa !15
  %233 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add169 = add nsw i32 %232, %233
  %shr170 = ashr i32 %add169, 18
  %and171 = and i32 %shr170, 1023
  %arrayidx172 = getelementptr inbounds i8, i8* %231, i32 %and171
  %234 = load i8, i8* %arrayidx172, align 1, !tbaa !17
  %235 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx173 = getelementptr inbounds i8, i8* %235, i32 1
  store i8 %234, i8* %arrayidx173, align 1, !tbaa !17
  %236 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %237 = load i32, i32* %tmp21, align 4, !tbaa !15
  %238 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub174 = sub nsw i32 %237, %238
  %shr175 = ashr i32 %sub174, 18
  %and176 = and i32 %shr175, 1023
  %arrayidx177 = getelementptr inbounds i8, i8* %236, i32 %and176
  %239 = load i8, i8* %arrayidx177, align 1, !tbaa !17
  %240 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx178 = getelementptr inbounds i8, i8* %240, i32 8
  store i8 %239, i8* %arrayidx178, align 1, !tbaa !17
  %241 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %242 = load i32, i32* %tmp22, align 4, !tbaa !15
  %243 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add179 = add nsw i32 %242, %243
  %shr180 = ashr i32 %add179, 18
  %and181 = and i32 %shr180, 1023
  %arrayidx182 = getelementptr inbounds i8, i8* %241, i32 %and181
  %244 = load i8, i8* %arrayidx182, align 1, !tbaa !17
  %245 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx183 = getelementptr inbounds i8, i8* %245, i32 2
  store i8 %244, i8* %arrayidx183, align 1, !tbaa !17
  %246 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %247 = load i32, i32* %tmp22, align 4, !tbaa !15
  %248 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub184 = sub nsw i32 %247, %248
  %shr185 = ashr i32 %sub184, 18
  %and186 = and i32 %shr185, 1023
  %arrayidx187 = getelementptr inbounds i8, i8* %246, i32 %and186
  %249 = load i8, i8* %arrayidx187, align 1, !tbaa !17
  %250 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx188 = getelementptr inbounds i8, i8* %250, i32 7
  store i8 %249, i8* %arrayidx188, align 1, !tbaa !17
  %251 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %252 = load i32, i32* %tmp23, align 4, !tbaa !15
  %253 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add189 = add nsw i32 %252, %253
  %shr190 = ashr i32 %add189, 18
  %and191 = and i32 %shr190, 1023
  %arrayidx192 = getelementptr inbounds i8, i8* %251, i32 %and191
  %254 = load i8, i8* %arrayidx192, align 1, !tbaa !17
  %255 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx193 = getelementptr inbounds i8, i8* %255, i32 3
  store i8 %254, i8* %arrayidx193, align 1, !tbaa !17
  %256 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %257 = load i32, i32* %tmp23, align 4, !tbaa !15
  %258 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub194 = sub nsw i32 %257, %258
  %shr195 = ashr i32 %sub194, 18
  %and196 = and i32 %shr195, 1023
  %arrayidx197 = getelementptr inbounds i8, i8* %256, i32 %and196
  %259 = load i8, i8* %arrayidx197, align 1, !tbaa !17
  %260 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx198 = getelementptr inbounds i8, i8* %260, i32 6
  store i8 %259, i8* %arrayidx198, align 1, !tbaa !17
  %261 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %262 = load i32, i32* %tmp24, align 4, !tbaa !15
  %263 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add199 = add nsw i32 %262, %263
  %shr200 = ashr i32 %add199, 18
  %and201 = and i32 %shr200, 1023
  %arrayidx202 = getelementptr inbounds i8, i8* %261, i32 %and201
  %264 = load i8, i8* %arrayidx202, align 1, !tbaa !17
  %265 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx203 = getelementptr inbounds i8, i8* %265, i32 4
  store i8 %264, i8* %arrayidx203, align 1, !tbaa !17
  %266 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %267 = load i32, i32* %tmp24, align 4, !tbaa !15
  %268 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub204 = sub nsw i32 %267, %268
  %shr205 = ashr i32 %sub204, 18
  %and206 = and i32 %shr205, 1023
  %arrayidx207 = getelementptr inbounds i8, i8* %266, i32 %and206
  %269 = load i8, i8* %arrayidx207, align 1, !tbaa !17
  %270 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx208 = getelementptr inbounds i8, i8* %270, i32 5
  store i8 %269, i8* %arrayidx208, align 1, !tbaa !17
  %271 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr209 = getelementptr inbounds i32, i32* %271, i32 8
  store i32* %add.ptr209, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc210

for.inc210:                                       ; preds = %for.body105
  %272 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc211 = add nsw i32 %272, 1
  store i32 %inc211, i32* %ctr, align 4, !tbaa !6
  br label %for.cond102

for.end212:                                       ; preds = %for.cond102
  %273 = bitcast [80 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 320, i8* %273) #2
  %274 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #2
  %275 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #2
  %276 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #2
  %277 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #2
  %278 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #2
  %279 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #2
  %280 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #2
  %281 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #2
  %282 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #2
  %283 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #2
  %284 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #2
  %285 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %285) #2
  %286 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %286) #2
  %287 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #2
  %288 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #2
  %289 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #2
  %290 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %290) #2
  %291 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #2
  %292 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #2
  %293 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #2
  %294 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_11x11(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %tmp14 = alloca i32, align 4
  %tmp20 = alloca i32, align 4
  %tmp21 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %tmp23 = alloca i32, align 4
  %tmp24 = alloca i32, align 4
  %tmp25 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [88 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 65
  %21 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %22 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #2
  %23 = bitcast [88 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 352, i8* %23) #2
  %24 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %24, i16** %inptr, align 4, !tbaa !2
  %25 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %25, i32 0, i32 20
  %26 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %27 = bitcast i8* %26 to i32*
  store i32* %27, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [88 x i32], [88 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %28 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %28, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %29 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %29, i32 0
  %30 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %30 to i32
  %31 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %31, i32 0
  %32 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %32
  store i32 %mul, i32* %tmp10, align 4, !tbaa !15
  %33 = load i32, i32* %tmp10, align 4, !tbaa !15
  %shl = shl i32 %33, 13
  store i32 %shl, i32* %tmp10, align 4, !tbaa !15
  %34 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add = add nsw i32 %34, 1024
  store i32 %add, i32* %tmp10, align 4, !tbaa !15
  %35 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %35, i32 16
  %36 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %36 to i32
  %37 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %37, i32 16
  %38 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %38
  store i32 %mul5, i32* %z1, align 4, !tbaa !15
  %39 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %39, i32 32
  %40 = load i16, i16* %arrayidx6, align 2, !tbaa !14
  %conv7 = sext i16 %40 to i32
  %41 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %41, i32 32
  %42 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %mul9 = mul nsw i32 %conv7, %42
  store i32 %mul9, i32* %z2, align 4, !tbaa !15
  %43 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %43, i32 48
  %44 = load i16, i16* %arrayidx10, align 2, !tbaa !14
  %conv11 = sext i16 %44 to i32
  %45 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %45, i32 48
  %46 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %mul13 = mul nsw i32 %conv11, %46
  store i32 %mul13, i32* %z3, align 4, !tbaa !15
  %47 = load i32, i32* %z2, align 4, !tbaa !15
  %48 = load i32, i32* %z3, align 4, !tbaa !15
  %sub = sub nsw i32 %47, %48
  %mul14 = mul nsw i32 %sub, 20862
  store i32 %mul14, i32* %tmp20, align 4, !tbaa !15
  %49 = load i32, i32* %z2, align 4, !tbaa !15
  %50 = load i32, i32* %z1, align 4, !tbaa !15
  %sub15 = sub nsw i32 %49, %50
  %mul16 = mul nsw i32 %sub15, 3529
  store i32 %mul16, i32* %tmp23, align 4, !tbaa !15
  %51 = load i32, i32* %z1, align 4, !tbaa !15
  %52 = load i32, i32* %z3, align 4, !tbaa !15
  %add17 = add nsw i32 %51, %52
  store i32 %add17, i32* %z4, align 4, !tbaa !15
  %53 = load i32, i32* %z4, align 4, !tbaa !15
  %mul18 = mul nsw i32 %53, -9467
  store i32 %mul18, i32* %tmp24, align 4, !tbaa !15
  %54 = load i32, i32* %z2, align 4, !tbaa !15
  %55 = load i32, i32* %z4, align 4, !tbaa !15
  %sub19 = sub nsw i32 %55, %54
  store i32 %sub19, i32* %z4, align 4, !tbaa !15
  %56 = load i32, i32* %tmp10, align 4, !tbaa !15
  %57 = load i32, i32* %z4, align 4, !tbaa !15
  %mul20 = mul nsw i32 %57, 11116
  %add21 = add nsw i32 %56, %mul20
  store i32 %add21, i32* %tmp25, align 4, !tbaa !15
  %58 = load i32, i32* %tmp20, align 4, !tbaa !15
  %59 = load i32, i32* %tmp23, align 4, !tbaa !15
  %add22 = add nsw i32 %58, %59
  %60 = load i32, i32* %tmp25, align 4, !tbaa !15
  %add23 = add nsw i32 %add22, %60
  %61 = load i32, i32* %z2, align 4, !tbaa !15
  %mul24 = mul nsw i32 %61, 14924
  %sub25 = sub nsw i32 %add23, %mul24
  store i32 %sub25, i32* %tmp21, align 4, !tbaa !15
  %62 = load i32, i32* %tmp25, align 4, !tbaa !15
  %63 = load i32, i32* %z3, align 4, !tbaa !15
  %mul26 = mul nsw i32 %63, 17333
  %add27 = add nsw i32 %62, %mul26
  %64 = load i32, i32* %tmp20, align 4, !tbaa !15
  %add28 = add nsw i32 %64, %add27
  store i32 %add28, i32* %tmp20, align 4, !tbaa !15
  %65 = load i32, i32* %tmp25, align 4, !tbaa !15
  %66 = load i32, i32* %z1, align 4, !tbaa !15
  %mul29 = mul nsw i32 %66, 12399
  %sub30 = sub nsw i32 %65, %mul29
  %67 = load i32, i32* %tmp23, align 4, !tbaa !15
  %add31 = add nsw i32 %67, %sub30
  store i32 %add31, i32* %tmp23, align 4, !tbaa !15
  %68 = load i32, i32* %tmp25, align 4, !tbaa !15
  %69 = load i32, i32* %tmp24, align 4, !tbaa !15
  %add32 = add nsw i32 %69, %68
  store i32 %add32, i32* %tmp24, align 4, !tbaa !15
  %70 = load i32, i32* %tmp24, align 4, !tbaa !15
  %71 = load i32, i32* %z3, align 4, !tbaa !15
  %mul33 = mul nsw i32 %71, 6461
  %sub34 = sub nsw i32 %70, %mul33
  store i32 %sub34, i32* %tmp22, align 4, !tbaa !15
  %72 = load i32, i32* %z2, align 4, !tbaa !15
  %mul35 = mul nsw i32 %72, 15929
  %73 = load i32, i32* %z1, align 4, !tbaa !15
  %mul36 = mul nsw i32 %73, 11395
  %sub37 = sub nsw i32 %mul35, %mul36
  %74 = load i32, i32* %tmp24, align 4, !tbaa !15
  %add38 = add nsw i32 %74, %sub37
  store i32 %add38, i32* %tmp24, align 4, !tbaa !15
  %75 = load i32, i32* %tmp10, align 4, !tbaa !15
  %76 = load i32, i32* %z4, align 4, !tbaa !15
  %mul39 = mul nsw i32 %76, 11585
  %sub40 = sub nsw i32 %75, %mul39
  store i32 %sub40, i32* %tmp25, align 4, !tbaa !15
  %77 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i16, i16* %77, i32 8
  %78 = load i16, i16* %arrayidx41, align 2, !tbaa !14
  %conv42 = sext i16 %78 to i32
  %79 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %79, i32 8
  %80 = load i32, i32* %arrayidx43, align 4, !tbaa !6
  %mul44 = mul nsw i32 %conv42, %80
  store i32 %mul44, i32* %z1, align 4, !tbaa !15
  %81 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i16, i16* %81, i32 24
  %82 = load i16, i16* %arrayidx45, align 2, !tbaa !14
  %conv46 = sext i16 %82 to i32
  %83 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %83, i32 24
  %84 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %mul48 = mul nsw i32 %conv46, %84
  store i32 %mul48, i32* %z2, align 4, !tbaa !15
  %85 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i16, i16* %85, i32 40
  %86 = load i16, i16* %arrayidx49, align 2, !tbaa !14
  %conv50 = sext i16 %86 to i32
  %87 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %87, i32 40
  %88 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %mul52 = mul nsw i32 %conv50, %88
  store i32 %mul52, i32* %z3, align 4, !tbaa !15
  %89 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i16, i16* %89, i32 56
  %90 = load i16, i16* %arrayidx53, align 2, !tbaa !14
  %conv54 = sext i16 %90 to i32
  %91 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %91, i32 56
  %92 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %mul56 = mul nsw i32 %conv54, %92
  store i32 %mul56, i32* %z4, align 4, !tbaa !15
  %93 = load i32, i32* %z1, align 4, !tbaa !15
  %94 = load i32, i32* %z2, align 4, !tbaa !15
  %add57 = add nsw i32 %93, %94
  store i32 %add57, i32* %tmp11, align 4, !tbaa !15
  %95 = load i32, i32* %tmp11, align 4, !tbaa !15
  %96 = load i32, i32* %z3, align 4, !tbaa !15
  %add58 = add nsw i32 %95, %96
  %97 = load i32, i32* %z4, align 4, !tbaa !15
  %add59 = add nsw i32 %add58, %97
  %mul60 = mul nsw i32 %add59, 3264
  store i32 %mul60, i32* %tmp14, align 4, !tbaa !15
  %98 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul61 = mul nsw i32 %98, 7274
  store i32 %mul61, i32* %tmp11, align 4, !tbaa !15
  %99 = load i32, i32* %z1, align 4, !tbaa !15
  %100 = load i32, i32* %z3, align 4, !tbaa !15
  %add62 = add nsw i32 %99, %100
  %mul63 = mul nsw i32 %add62, 5492
  store i32 %mul63, i32* %tmp12, align 4, !tbaa !15
  %101 = load i32, i32* %tmp14, align 4, !tbaa !15
  %102 = load i32, i32* %z1, align 4, !tbaa !15
  %103 = load i32, i32* %z4, align 4, !tbaa !15
  %add64 = add nsw i32 %102, %103
  %mul65 = mul nsw i32 %add64, 3000
  %add66 = add nsw i32 %101, %mul65
  store i32 %add66, i32* %tmp13, align 4, !tbaa !15
  %104 = load i32, i32* %tmp11, align 4, !tbaa !15
  %105 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add67 = add nsw i32 %104, %105
  %106 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add68 = add nsw i32 %add67, %106
  %107 = load i32, i32* %z1, align 4, !tbaa !15
  %mul69 = mul nsw i32 %107, 7562
  %sub70 = sub nsw i32 %add68, %mul69
  store i32 %sub70, i32* %tmp10, align 4, !tbaa !15
  %108 = load i32, i32* %tmp14, align 4, !tbaa !15
  %109 = load i32, i32* %z2, align 4, !tbaa !15
  %110 = load i32, i32* %z3, align 4, !tbaa !15
  %add71 = add nsw i32 %109, %110
  %mul72 = mul nsw i32 %add71, 9527
  %sub73 = sub nsw i32 %108, %mul72
  store i32 %sub73, i32* %z1, align 4, !tbaa !15
  %111 = load i32, i32* %z1, align 4, !tbaa !15
  %112 = load i32, i32* %z2, align 4, !tbaa !15
  %mul74 = mul nsw i32 %112, 16984
  %add75 = add nsw i32 %111, %mul74
  %113 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add76 = add nsw i32 %113, %add75
  store i32 %add76, i32* %tmp11, align 4, !tbaa !15
  %114 = load i32, i32* %z1, align 4, !tbaa !15
  %115 = load i32, i32* %z3, align 4, !tbaa !15
  %mul77 = mul nsw i32 %115, 9766
  %sub78 = sub nsw i32 %114, %mul77
  %116 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add79 = add nsw i32 %116, %sub78
  store i32 %add79, i32* %tmp12, align 4, !tbaa !15
  %117 = load i32, i32* %z2, align 4, !tbaa !15
  %118 = load i32, i32* %z4, align 4, !tbaa !15
  %add80 = add nsw i32 %117, %118
  %mul81 = mul nsw i32 %add80, -14731
  store i32 %mul81, i32* %z1, align 4, !tbaa !15
  %119 = load i32, i32* %z1, align 4, !tbaa !15
  %120 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add82 = add nsw i32 %120, %119
  store i32 %add82, i32* %tmp11, align 4, !tbaa !15
  %121 = load i32, i32* %z1, align 4, !tbaa !15
  %122 = load i32, i32* %z4, align 4, !tbaa !15
  %mul83 = mul nsw i32 %122, 17223
  %add84 = add nsw i32 %121, %mul83
  %123 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add85 = add nsw i32 %123, %add84
  store i32 %add85, i32* %tmp13, align 4, !tbaa !15
  %124 = load i32, i32* %z2, align 4, !tbaa !15
  %mul86 = mul nsw i32 %124, -12019
  %125 = load i32, i32* %z3, align 4, !tbaa !15
  %mul87 = mul nsw i32 %125, 8203
  %add88 = add nsw i32 %mul86, %mul87
  %126 = load i32, i32* %z4, align 4, !tbaa !15
  %mul89 = mul nsw i32 %126, 13802
  %sub90 = sub nsw i32 %add88, %mul89
  %127 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add91 = add nsw i32 %127, %sub90
  store i32 %add91, i32* %tmp14, align 4, !tbaa !15
  %128 = load i32, i32* %tmp20, align 4, !tbaa !15
  %129 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add92 = add nsw i32 %128, %129
  %shr = ashr i32 %add92, 11
  %130 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %130, i32 0
  store i32 %shr, i32* %arrayidx93, align 4, !tbaa !6
  %131 = load i32, i32* %tmp20, align 4, !tbaa !15
  %132 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub94 = sub nsw i32 %131, %132
  %shr95 = ashr i32 %sub94, 11
  %133 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i32, i32* %133, i32 80
  store i32 %shr95, i32* %arrayidx96, align 4, !tbaa !6
  %134 = load i32, i32* %tmp21, align 4, !tbaa !15
  %135 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add97 = add nsw i32 %134, %135
  %shr98 = ashr i32 %add97, 11
  %136 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %136, i32 8
  store i32 %shr98, i32* %arrayidx99, align 4, !tbaa !6
  %137 = load i32, i32* %tmp21, align 4, !tbaa !15
  %138 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub100 = sub nsw i32 %137, %138
  %shr101 = ashr i32 %sub100, 11
  %139 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %139, i32 72
  store i32 %shr101, i32* %arrayidx102, align 4, !tbaa !6
  %140 = load i32, i32* %tmp22, align 4, !tbaa !15
  %141 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add103 = add nsw i32 %140, %141
  %shr104 = ashr i32 %add103, 11
  %142 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i32, i32* %142, i32 16
  store i32 %shr104, i32* %arrayidx105, align 4, !tbaa !6
  %143 = load i32, i32* %tmp22, align 4, !tbaa !15
  %144 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub106 = sub nsw i32 %143, %144
  %shr107 = ashr i32 %sub106, 11
  %145 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %145, i32 64
  store i32 %shr107, i32* %arrayidx108, align 4, !tbaa !6
  %146 = load i32, i32* %tmp23, align 4, !tbaa !15
  %147 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add109 = add nsw i32 %146, %147
  %shr110 = ashr i32 %add109, 11
  %148 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i32, i32* %148, i32 24
  store i32 %shr110, i32* %arrayidx111, align 4, !tbaa !6
  %149 = load i32, i32* %tmp23, align 4, !tbaa !15
  %150 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub112 = sub nsw i32 %149, %150
  %shr113 = ashr i32 %sub112, 11
  %151 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx114 = getelementptr inbounds i32, i32* %151, i32 56
  store i32 %shr113, i32* %arrayidx114, align 4, !tbaa !6
  %152 = load i32, i32* %tmp24, align 4, !tbaa !15
  %153 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add115 = add nsw i32 %152, %153
  %shr116 = ashr i32 %add115, 11
  %154 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds i32, i32* %154, i32 32
  store i32 %shr116, i32* %arrayidx117, align 4, !tbaa !6
  %155 = load i32, i32* %tmp24, align 4, !tbaa !15
  %156 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub118 = sub nsw i32 %155, %156
  %shr119 = ashr i32 %sub118, 11
  %157 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx120 = getelementptr inbounds i32, i32* %157, i32 48
  store i32 %shr119, i32* %arrayidx120, align 4, !tbaa !6
  %158 = load i32, i32* %tmp25, align 4, !tbaa !15
  %shr121 = ashr i32 %158, 11
  %159 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %159, i32 40
  store i32 %shr121, i32* %arrayidx122, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %160 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %160, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %161 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %161, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %162 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr123 = getelementptr inbounds i32, i32* %162, i32 1
  store i32* %incdec.ptr123, i32** %quantptr, align 4, !tbaa !2
  %163 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr124 = getelementptr inbounds i32, i32* %163, i32 1
  store i32* %incdec.ptr124, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay125 = getelementptr inbounds [88 x i32], [88 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay125, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond126

for.cond126:                                      ; preds = %for.inc259, %for.end
  %164 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp127 = icmp slt i32 %164, 11
  br i1 %cmp127, label %for.body129, label %for.end261

for.body129:                                      ; preds = %for.cond126
  %165 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %166 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx130 = getelementptr inbounds i8*, i8** %165, i32 %166
  %167 = load i8*, i8** %arrayidx130, align 4, !tbaa !2
  %168 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr131 = getelementptr inbounds i8, i8* %167, i32 %168
  store i8* %add.ptr131, i8** %outptr, align 4, !tbaa !2
  %169 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %169, i32 0
  %170 = load i32, i32* %arrayidx132, align 4, !tbaa !6
  %add133 = add nsw i32 %170, 16
  store i32 %add133, i32* %tmp10, align 4, !tbaa !15
  %171 = load i32, i32* %tmp10, align 4, !tbaa !15
  %shl134 = shl i32 %171, 13
  store i32 %shl134, i32* %tmp10, align 4, !tbaa !15
  %172 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds i32, i32* %172, i32 2
  %173 = load i32, i32* %arrayidx135, align 4, !tbaa !6
  store i32 %173, i32* %z1, align 4, !tbaa !15
  %174 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %174, i32 4
  %175 = load i32, i32* %arrayidx136, align 4, !tbaa !6
  store i32 %175, i32* %z2, align 4, !tbaa !15
  %176 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds i32, i32* %176, i32 6
  %177 = load i32, i32* %arrayidx137, align 4, !tbaa !6
  store i32 %177, i32* %z3, align 4, !tbaa !15
  %178 = load i32, i32* %z2, align 4, !tbaa !15
  %179 = load i32, i32* %z3, align 4, !tbaa !15
  %sub138 = sub nsw i32 %178, %179
  %mul139 = mul nsw i32 %sub138, 20862
  store i32 %mul139, i32* %tmp20, align 4, !tbaa !15
  %180 = load i32, i32* %z2, align 4, !tbaa !15
  %181 = load i32, i32* %z1, align 4, !tbaa !15
  %sub140 = sub nsw i32 %180, %181
  %mul141 = mul nsw i32 %sub140, 3529
  store i32 %mul141, i32* %tmp23, align 4, !tbaa !15
  %182 = load i32, i32* %z1, align 4, !tbaa !15
  %183 = load i32, i32* %z3, align 4, !tbaa !15
  %add142 = add nsw i32 %182, %183
  store i32 %add142, i32* %z4, align 4, !tbaa !15
  %184 = load i32, i32* %z4, align 4, !tbaa !15
  %mul143 = mul nsw i32 %184, -9467
  store i32 %mul143, i32* %tmp24, align 4, !tbaa !15
  %185 = load i32, i32* %z2, align 4, !tbaa !15
  %186 = load i32, i32* %z4, align 4, !tbaa !15
  %sub144 = sub nsw i32 %186, %185
  store i32 %sub144, i32* %z4, align 4, !tbaa !15
  %187 = load i32, i32* %tmp10, align 4, !tbaa !15
  %188 = load i32, i32* %z4, align 4, !tbaa !15
  %mul145 = mul nsw i32 %188, 11116
  %add146 = add nsw i32 %187, %mul145
  store i32 %add146, i32* %tmp25, align 4, !tbaa !15
  %189 = load i32, i32* %tmp20, align 4, !tbaa !15
  %190 = load i32, i32* %tmp23, align 4, !tbaa !15
  %add147 = add nsw i32 %189, %190
  %191 = load i32, i32* %tmp25, align 4, !tbaa !15
  %add148 = add nsw i32 %add147, %191
  %192 = load i32, i32* %z2, align 4, !tbaa !15
  %mul149 = mul nsw i32 %192, 14924
  %sub150 = sub nsw i32 %add148, %mul149
  store i32 %sub150, i32* %tmp21, align 4, !tbaa !15
  %193 = load i32, i32* %tmp25, align 4, !tbaa !15
  %194 = load i32, i32* %z3, align 4, !tbaa !15
  %mul151 = mul nsw i32 %194, 17333
  %add152 = add nsw i32 %193, %mul151
  %195 = load i32, i32* %tmp20, align 4, !tbaa !15
  %add153 = add nsw i32 %195, %add152
  store i32 %add153, i32* %tmp20, align 4, !tbaa !15
  %196 = load i32, i32* %tmp25, align 4, !tbaa !15
  %197 = load i32, i32* %z1, align 4, !tbaa !15
  %mul154 = mul nsw i32 %197, 12399
  %sub155 = sub nsw i32 %196, %mul154
  %198 = load i32, i32* %tmp23, align 4, !tbaa !15
  %add156 = add nsw i32 %198, %sub155
  store i32 %add156, i32* %tmp23, align 4, !tbaa !15
  %199 = load i32, i32* %tmp25, align 4, !tbaa !15
  %200 = load i32, i32* %tmp24, align 4, !tbaa !15
  %add157 = add nsw i32 %200, %199
  store i32 %add157, i32* %tmp24, align 4, !tbaa !15
  %201 = load i32, i32* %tmp24, align 4, !tbaa !15
  %202 = load i32, i32* %z3, align 4, !tbaa !15
  %mul158 = mul nsw i32 %202, 6461
  %sub159 = sub nsw i32 %201, %mul158
  store i32 %sub159, i32* %tmp22, align 4, !tbaa !15
  %203 = load i32, i32* %z2, align 4, !tbaa !15
  %mul160 = mul nsw i32 %203, 15929
  %204 = load i32, i32* %z1, align 4, !tbaa !15
  %mul161 = mul nsw i32 %204, 11395
  %sub162 = sub nsw i32 %mul160, %mul161
  %205 = load i32, i32* %tmp24, align 4, !tbaa !15
  %add163 = add nsw i32 %205, %sub162
  store i32 %add163, i32* %tmp24, align 4, !tbaa !15
  %206 = load i32, i32* %tmp10, align 4, !tbaa !15
  %207 = load i32, i32* %z4, align 4, !tbaa !15
  %mul164 = mul nsw i32 %207, 11585
  %sub165 = sub nsw i32 %206, %mul164
  store i32 %sub165, i32* %tmp25, align 4, !tbaa !15
  %208 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx166 = getelementptr inbounds i32, i32* %208, i32 1
  %209 = load i32, i32* %arrayidx166, align 4, !tbaa !6
  store i32 %209, i32* %z1, align 4, !tbaa !15
  %210 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds i32, i32* %210, i32 3
  %211 = load i32, i32* %arrayidx167, align 4, !tbaa !6
  store i32 %211, i32* %z2, align 4, !tbaa !15
  %212 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx168 = getelementptr inbounds i32, i32* %212, i32 5
  %213 = load i32, i32* %arrayidx168, align 4, !tbaa !6
  store i32 %213, i32* %z3, align 4, !tbaa !15
  %214 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds i32, i32* %214, i32 7
  %215 = load i32, i32* %arrayidx169, align 4, !tbaa !6
  store i32 %215, i32* %z4, align 4, !tbaa !15
  %216 = load i32, i32* %z1, align 4, !tbaa !15
  %217 = load i32, i32* %z2, align 4, !tbaa !15
  %add170 = add nsw i32 %216, %217
  store i32 %add170, i32* %tmp11, align 4, !tbaa !15
  %218 = load i32, i32* %tmp11, align 4, !tbaa !15
  %219 = load i32, i32* %z3, align 4, !tbaa !15
  %add171 = add nsw i32 %218, %219
  %220 = load i32, i32* %z4, align 4, !tbaa !15
  %add172 = add nsw i32 %add171, %220
  %mul173 = mul nsw i32 %add172, 3264
  store i32 %mul173, i32* %tmp14, align 4, !tbaa !15
  %221 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul174 = mul nsw i32 %221, 7274
  store i32 %mul174, i32* %tmp11, align 4, !tbaa !15
  %222 = load i32, i32* %z1, align 4, !tbaa !15
  %223 = load i32, i32* %z3, align 4, !tbaa !15
  %add175 = add nsw i32 %222, %223
  %mul176 = mul nsw i32 %add175, 5492
  store i32 %mul176, i32* %tmp12, align 4, !tbaa !15
  %224 = load i32, i32* %tmp14, align 4, !tbaa !15
  %225 = load i32, i32* %z1, align 4, !tbaa !15
  %226 = load i32, i32* %z4, align 4, !tbaa !15
  %add177 = add nsw i32 %225, %226
  %mul178 = mul nsw i32 %add177, 3000
  %add179 = add nsw i32 %224, %mul178
  store i32 %add179, i32* %tmp13, align 4, !tbaa !15
  %227 = load i32, i32* %tmp11, align 4, !tbaa !15
  %228 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add180 = add nsw i32 %227, %228
  %229 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add181 = add nsw i32 %add180, %229
  %230 = load i32, i32* %z1, align 4, !tbaa !15
  %mul182 = mul nsw i32 %230, 7562
  %sub183 = sub nsw i32 %add181, %mul182
  store i32 %sub183, i32* %tmp10, align 4, !tbaa !15
  %231 = load i32, i32* %tmp14, align 4, !tbaa !15
  %232 = load i32, i32* %z2, align 4, !tbaa !15
  %233 = load i32, i32* %z3, align 4, !tbaa !15
  %add184 = add nsw i32 %232, %233
  %mul185 = mul nsw i32 %add184, 9527
  %sub186 = sub nsw i32 %231, %mul185
  store i32 %sub186, i32* %z1, align 4, !tbaa !15
  %234 = load i32, i32* %z1, align 4, !tbaa !15
  %235 = load i32, i32* %z2, align 4, !tbaa !15
  %mul187 = mul nsw i32 %235, 16984
  %add188 = add nsw i32 %234, %mul187
  %236 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add189 = add nsw i32 %236, %add188
  store i32 %add189, i32* %tmp11, align 4, !tbaa !15
  %237 = load i32, i32* %z1, align 4, !tbaa !15
  %238 = load i32, i32* %z3, align 4, !tbaa !15
  %mul190 = mul nsw i32 %238, 9766
  %sub191 = sub nsw i32 %237, %mul190
  %239 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add192 = add nsw i32 %239, %sub191
  store i32 %add192, i32* %tmp12, align 4, !tbaa !15
  %240 = load i32, i32* %z2, align 4, !tbaa !15
  %241 = load i32, i32* %z4, align 4, !tbaa !15
  %add193 = add nsw i32 %240, %241
  %mul194 = mul nsw i32 %add193, -14731
  store i32 %mul194, i32* %z1, align 4, !tbaa !15
  %242 = load i32, i32* %z1, align 4, !tbaa !15
  %243 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add195 = add nsw i32 %243, %242
  store i32 %add195, i32* %tmp11, align 4, !tbaa !15
  %244 = load i32, i32* %z1, align 4, !tbaa !15
  %245 = load i32, i32* %z4, align 4, !tbaa !15
  %mul196 = mul nsw i32 %245, 17223
  %add197 = add nsw i32 %244, %mul196
  %246 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add198 = add nsw i32 %246, %add197
  store i32 %add198, i32* %tmp13, align 4, !tbaa !15
  %247 = load i32, i32* %z2, align 4, !tbaa !15
  %mul199 = mul nsw i32 %247, -12019
  %248 = load i32, i32* %z3, align 4, !tbaa !15
  %mul200 = mul nsw i32 %248, 8203
  %add201 = add nsw i32 %mul199, %mul200
  %249 = load i32, i32* %z4, align 4, !tbaa !15
  %mul202 = mul nsw i32 %249, 13802
  %sub203 = sub nsw i32 %add201, %mul202
  %250 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add204 = add nsw i32 %250, %sub203
  store i32 %add204, i32* %tmp14, align 4, !tbaa !15
  %251 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %252 = load i32, i32* %tmp20, align 4, !tbaa !15
  %253 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add205 = add nsw i32 %252, %253
  %shr206 = ashr i32 %add205, 18
  %and = and i32 %shr206, 1023
  %arrayidx207 = getelementptr inbounds i8, i8* %251, i32 %and
  %254 = load i8, i8* %arrayidx207, align 1, !tbaa !17
  %255 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx208 = getelementptr inbounds i8, i8* %255, i32 0
  store i8 %254, i8* %arrayidx208, align 1, !tbaa !17
  %256 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %257 = load i32, i32* %tmp20, align 4, !tbaa !15
  %258 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub209 = sub nsw i32 %257, %258
  %shr210 = ashr i32 %sub209, 18
  %and211 = and i32 %shr210, 1023
  %arrayidx212 = getelementptr inbounds i8, i8* %256, i32 %and211
  %259 = load i8, i8* %arrayidx212, align 1, !tbaa !17
  %260 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx213 = getelementptr inbounds i8, i8* %260, i32 10
  store i8 %259, i8* %arrayidx213, align 1, !tbaa !17
  %261 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %262 = load i32, i32* %tmp21, align 4, !tbaa !15
  %263 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add214 = add nsw i32 %262, %263
  %shr215 = ashr i32 %add214, 18
  %and216 = and i32 %shr215, 1023
  %arrayidx217 = getelementptr inbounds i8, i8* %261, i32 %and216
  %264 = load i8, i8* %arrayidx217, align 1, !tbaa !17
  %265 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx218 = getelementptr inbounds i8, i8* %265, i32 1
  store i8 %264, i8* %arrayidx218, align 1, !tbaa !17
  %266 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %267 = load i32, i32* %tmp21, align 4, !tbaa !15
  %268 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub219 = sub nsw i32 %267, %268
  %shr220 = ashr i32 %sub219, 18
  %and221 = and i32 %shr220, 1023
  %arrayidx222 = getelementptr inbounds i8, i8* %266, i32 %and221
  %269 = load i8, i8* %arrayidx222, align 1, !tbaa !17
  %270 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx223 = getelementptr inbounds i8, i8* %270, i32 9
  store i8 %269, i8* %arrayidx223, align 1, !tbaa !17
  %271 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %272 = load i32, i32* %tmp22, align 4, !tbaa !15
  %273 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add224 = add nsw i32 %272, %273
  %shr225 = ashr i32 %add224, 18
  %and226 = and i32 %shr225, 1023
  %arrayidx227 = getelementptr inbounds i8, i8* %271, i32 %and226
  %274 = load i8, i8* %arrayidx227, align 1, !tbaa !17
  %275 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx228 = getelementptr inbounds i8, i8* %275, i32 2
  store i8 %274, i8* %arrayidx228, align 1, !tbaa !17
  %276 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %277 = load i32, i32* %tmp22, align 4, !tbaa !15
  %278 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub229 = sub nsw i32 %277, %278
  %shr230 = ashr i32 %sub229, 18
  %and231 = and i32 %shr230, 1023
  %arrayidx232 = getelementptr inbounds i8, i8* %276, i32 %and231
  %279 = load i8, i8* %arrayidx232, align 1, !tbaa !17
  %280 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx233 = getelementptr inbounds i8, i8* %280, i32 8
  store i8 %279, i8* %arrayidx233, align 1, !tbaa !17
  %281 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %282 = load i32, i32* %tmp23, align 4, !tbaa !15
  %283 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add234 = add nsw i32 %282, %283
  %shr235 = ashr i32 %add234, 18
  %and236 = and i32 %shr235, 1023
  %arrayidx237 = getelementptr inbounds i8, i8* %281, i32 %and236
  %284 = load i8, i8* %arrayidx237, align 1, !tbaa !17
  %285 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx238 = getelementptr inbounds i8, i8* %285, i32 3
  store i8 %284, i8* %arrayidx238, align 1, !tbaa !17
  %286 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %287 = load i32, i32* %tmp23, align 4, !tbaa !15
  %288 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub239 = sub nsw i32 %287, %288
  %shr240 = ashr i32 %sub239, 18
  %and241 = and i32 %shr240, 1023
  %arrayidx242 = getelementptr inbounds i8, i8* %286, i32 %and241
  %289 = load i8, i8* %arrayidx242, align 1, !tbaa !17
  %290 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx243 = getelementptr inbounds i8, i8* %290, i32 7
  store i8 %289, i8* %arrayidx243, align 1, !tbaa !17
  %291 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %292 = load i32, i32* %tmp24, align 4, !tbaa !15
  %293 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add244 = add nsw i32 %292, %293
  %shr245 = ashr i32 %add244, 18
  %and246 = and i32 %shr245, 1023
  %arrayidx247 = getelementptr inbounds i8, i8* %291, i32 %and246
  %294 = load i8, i8* %arrayidx247, align 1, !tbaa !17
  %295 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx248 = getelementptr inbounds i8, i8* %295, i32 4
  store i8 %294, i8* %arrayidx248, align 1, !tbaa !17
  %296 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %297 = load i32, i32* %tmp24, align 4, !tbaa !15
  %298 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub249 = sub nsw i32 %297, %298
  %shr250 = ashr i32 %sub249, 18
  %and251 = and i32 %shr250, 1023
  %arrayidx252 = getelementptr inbounds i8, i8* %296, i32 %and251
  %299 = load i8, i8* %arrayidx252, align 1, !tbaa !17
  %300 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx253 = getelementptr inbounds i8, i8* %300, i32 6
  store i8 %299, i8* %arrayidx253, align 1, !tbaa !17
  %301 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %302 = load i32, i32* %tmp25, align 4, !tbaa !15
  %shr254 = ashr i32 %302, 18
  %and255 = and i32 %shr254, 1023
  %arrayidx256 = getelementptr inbounds i8, i8* %301, i32 %and255
  %303 = load i8, i8* %arrayidx256, align 1, !tbaa !17
  %304 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx257 = getelementptr inbounds i8, i8* %304, i32 5
  store i8 %303, i8* %arrayidx257, align 1, !tbaa !17
  %305 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr258 = getelementptr inbounds i32, i32* %305, i32 8
  store i32* %add.ptr258, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc259

for.inc259:                                       ; preds = %for.body129
  %306 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc260 = add nsw i32 %306, 1
  store i32 %inc260, i32* %ctr, align 4, !tbaa !6
  br label %for.cond126

for.end261:                                       ; preds = %for.cond126
  %307 = bitcast [88 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 352, i8* %307) #2
  %308 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #2
  %309 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %309) #2
  %310 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %310) #2
  %311 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %311) #2
  %312 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %312) #2
  %313 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #2
  %314 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #2
  %315 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #2
  %316 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #2
  %317 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #2
  %318 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %318) #2
  %319 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #2
  %320 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #2
  %321 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %321) #2
  %322 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %322) #2
  %323 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %323) #2
  %324 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %324) #2
  %325 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #2
  %326 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #2
  %327 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %327) #2
  %328 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %328) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_12x12(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %tmp14 = alloca i32, align 4
  %tmp15 = alloca i32, align 4
  %tmp20 = alloca i32, align 4
  %tmp21 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %tmp23 = alloca i32, align 4
  %tmp24 = alloca i32, align 4
  %tmp25 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [96 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 65
  %22 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %23 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #2
  %24 = bitcast [96 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 384, i8* %24) #2
  %25 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %25, i16** %inptr, align 4, !tbaa !2
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %26, i32 0, i32 20
  %27 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %28 = bitcast i8* %27 to i32*
  store i32* %28, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [96 x i32], [96 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %29 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %29, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %30 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %30, i32 0
  %31 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %31 to i32
  %32 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %32, i32 0
  %33 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %33
  store i32 %mul, i32* %z3, align 4, !tbaa !15
  %34 = load i32, i32* %z3, align 4, !tbaa !15
  %shl = shl i32 %34, 13
  store i32 %shl, i32* %z3, align 4, !tbaa !15
  %35 = load i32, i32* %z3, align 4, !tbaa !15
  %add = add nsw i32 %35, 1024
  store i32 %add, i32* %z3, align 4, !tbaa !15
  %36 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %36, i32 32
  %37 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %37 to i32
  %38 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %38, i32 32
  %39 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %39
  store i32 %mul5, i32* %z4, align 4, !tbaa !15
  %40 = load i32, i32* %z4, align 4, !tbaa !15
  %mul6 = mul nsw i32 %40, 10033
  store i32 %mul6, i32* %z4, align 4, !tbaa !15
  %41 = load i32, i32* %z3, align 4, !tbaa !15
  %42 = load i32, i32* %z4, align 4, !tbaa !15
  %add7 = add nsw i32 %41, %42
  store i32 %add7, i32* %tmp10, align 4, !tbaa !15
  %43 = load i32, i32* %z3, align 4, !tbaa !15
  %44 = load i32, i32* %z4, align 4, !tbaa !15
  %sub = sub nsw i32 %43, %44
  store i32 %sub, i32* %tmp11, align 4, !tbaa !15
  %45 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i16, i16* %45, i32 16
  %46 = load i16, i16* %arrayidx8, align 2, !tbaa !14
  %conv9 = sext i16 %46 to i32
  %47 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %47, i32 16
  %48 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %mul11 = mul nsw i32 %conv9, %48
  store i32 %mul11, i32* %z1, align 4, !tbaa !15
  %49 = load i32, i32* %z1, align 4, !tbaa !15
  %mul12 = mul nsw i32 %49, 11190
  store i32 %mul12, i32* %z4, align 4, !tbaa !15
  %50 = load i32, i32* %z1, align 4, !tbaa !15
  %shl13 = shl i32 %50, 13
  store i32 %shl13, i32* %z1, align 4, !tbaa !15
  %51 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i16, i16* %51, i32 48
  %52 = load i16, i16* %arrayidx14, align 2, !tbaa !14
  %conv15 = sext i16 %52 to i32
  %53 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %53, i32 48
  %54 = load i32, i32* %arrayidx16, align 4, !tbaa !6
  %mul17 = mul nsw i32 %conv15, %54
  store i32 %mul17, i32* %z2, align 4, !tbaa !15
  %55 = load i32, i32* %z2, align 4, !tbaa !15
  %shl18 = shl i32 %55, 13
  store i32 %shl18, i32* %z2, align 4, !tbaa !15
  %56 = load i32, i32* %z1, align 4, !tbaa !15
  %57 = load i32, i32* %z2, align 4, !tbaa !15
  %sub19 = sub nsw i32 %56, %57
  store i32 %sub19, i32* %tmp12, align 4, !tbaa !15
  %58 = load i32, i32* %z3, align 4, !tbaa !15
  %59 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add20 = add nsw i32 %58, %59
  store i32 %add20, i32* %tmp21, align 4, !tbaa !15
  %60 = load i32, i32* %z3, align 4, !tbaa !15
  %61 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub21 = sub nsw i32 %60, %61
  store i32 %sub21, i32* %tmp24, align 4, !tbaa !15
  %62 = load i32, i32* %z4, align 4, !tbaa !15
  %63 = load i32, i32* %z2, align 4, !tbaa !15
  %add22 = add nsw i32 %62, %63
  store i32 %add22, i32* %tmp12, align 4, !tbaa !15
  %64 = load i32, i32* %tmp10, align 4, !tbaa !15
  %65 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add23 = add nsw i32 %64, %65
  store i32 %add23, i32* %tmp20, align 4, !tbaa !15
  %66 = load i32, i32* %tmp10, align 4, !tbaa !15
  %67 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub24 = sub nsw i32 %66, %67
  store i32 %sub24, i32* %tmp25, align 4, !tbaa !15
  %68 = load i32, i32* %z4, align 4, !tbaa !15
  %69 = load i32, i32* %z1, align 4, !tbaa !15
  %sub25 = sub nsw i32 %68, %69
  %70 = load i32, i32* %z2, align 4, !tbaa !15
  %sub26 = sub nsw i32 %sub25, %70
  store i32 %sub26, i32* %tmp12, align 4, !tbaa !15
  %71 = load i32, i32* %tmp11, align 4, !tbaa !15
  %72 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add27 = add nsw i32 %71, %72
  store i32 %add27, i32* %tmp22, align 4, !tbaa !15
  %73 = load i32, i32* %tmp11, align 4, !tbaa !15
  %74 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub28 = sub nsw i32 %73, %74
  store i32 %sub28, i32* %tmp23, align 4, !tbaa !15
  %75 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i16, i16* %75, i32 8
  %76 = load i16, i16* %arrayidx29, align 2, !tbaa !14
  %conv30 = sext i16 %76 to i32
  %77 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i32, i32* %77, i32 8
  %78 = load i32, i32* %arrayidx31, align 4, !tbaa !6
  %mul32 = mul nsw i32 %conv30, %78
  store i32 %mul32, i32* %z1, align 4, !tbaa !15
  %79 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i16, i16* %79, i32 24
  %80 = load i16, i16* %arrayidx33, align 2, !tbaa !14
  %conv34 = sext i16 %80 to i32
  %81 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %81, i32 24
  %82 = load i32, i32* %arrayidx35, align 4, !tbaa !6
  %mul36 = mul nsw i32 %conv34, %82
  store i32 %mul36, i32* %z2, align 4, !tbaa !15
  %83 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i16, i16* %83, i32 40
  %84 = load i16, i16* %arrayidx37, align 2, !tbaa !14
  %conv38 = sext i16 %84 to i32
  %85 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i32, i32* %85, i32 40
  %86 = load i32, i32* %arrayidx39, align 4, !tbaa !6
  %mul40 = mul nsw i32 %conv38, %86
  store i32 %mul40, i32* %z3, align 4, !tbaa !15
  %87 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i16, i16* %87, i32 56
  %88 = load i16, i16* %arrayidx41, align 2, !tbaa !14
  %conv42 = sext i16 %88 to i32
  %89 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %89, i32 56
  %90 = load i32, i32* %arrayidx43, align 4, !tbaa !6
  %mul44 = mul nsw i32 %conv42, %90
  store i32 %mul44, i32* %z4, align 4, !tbaa !15
  %91 = load i32, i32* %z2, align 4, !tbaa !15
  %mul45 = mul nsw i32 %91, 10703
  store i32 %mul45, i32* %tmp11, align 4, !tbaa !15
  %92 = load i32, i32* %z2, align 4, !tbaa !15
  %mul46 = mul nsw i32 %92, -4433
  store i32 %mul46, i32* %tmp14, align 4, !tbaa !15
  %93 = load i32, i32* %z1, align 4, !tbaa !15
  %94 = load i32, i32* %z3, align 4, !tbaa !15
  %add47 = add nsw i32 %93, %94
  store i32 %add47, i32* %tmp10, align 4, !tbaa !15
  %95 = load i32, i32* %tmp10, align 4, !tbaa !15
  %96 = load i32, i32* %z4, align 4, !tbaa !15
  %add48 = add nsw i32 %95, %96
  %mul49 = mul nsw i32 %add48, 7053
  store i32 %mul49, i32* %tmp15, align 4, !tbaa !15
  %97 = load i32, i32* %tmp15, align 4, !tbaa !15
  %98 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul50 = mul nsw i32 %98, 2139
  %add51 = add nsw i32 %97, %mul50
  store i32 %add51, i32* %tmp12, align 4, !tbaa !15
  %99 = load i32, i32* %tmp12, align 4, !tbaa !15
  %100 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add52 = add nsw i32 %99, %100
  %101 = load i32, i32* %z1, align 4, !tbaa !15
  %mul53 = mul nsw i32 %101, 2295
  %add54 = add nsw i32 %add52, %mul53
  store i32 %add54, i32* %tmp10, align 4, !tbaa !15
  %102 = load i32, i32* %z3, align 4, !tbaa !15
  %103 = load i32, i32* %z4, align 4, !tbaa !15
  %add55 = add nsw i32 %102, %103
  %mul56 = mul nsw i32 %add55, -8565
  store i32 %mul56, i32* %tmp13, align 4, !tbaa !15
  %104 = load i32, i32* %tmp13, align 4, !tbaa !15
  %105 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add57 = add nsw i32 %104, %105
  %106 = load i32, i32* %z3, align 4, !tbaa !15
  %mul58 = mul nsw i32 %106, 12112
  %sub59 = sub nsw i32 %add57, %mul58
  %107 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add60 = add nsw i32 %107, %sub59
  store i32 %add60, i32* %tmp12, align 4, !tbaa !15
  %108 = load i32, i32* %tmp15, align 4, !tbaa !15
  %109 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub61 = sub nsw i32 %108, %109
  %110 = load i32, i32* %z4, align 4, !tbaa !15
  %mul62 = mul nsw i32 %110, 12998
  %add63 = add nsw i32 %sub61, %mul62
  %111 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add64 = add nsw i32 %111, %add63
  store i32 %add64, i32* %tmp13, align 4, !tbaa !15
  %112 = load i32, i32* %tmp14, align 4, !tbaa !15
  %113 = load i32, i32* %z1, align 4, !tbaa !15
  %mul65 = mul nsw i32 %113, 5540
  %sub66 = sub nsw i32 %112, %mul65
  %114 = load i32, i32* %z4, align 4, !tbaa !15
  %mul67 = mul nsw i32 %114, 16244
  %sub68 = sub nsw i32 %sub66, %mul67
  %115 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add69 = add nsw i32 %115, %sub68
  store i32 %add69, i32* %tmp15, align 4, !tbaa !15
  %116 = load i32, i32* %z4, align 4, !tbaa !15
  %117 = load i32, i32* %z1, align 4, !tbaa !15
  %sub70 = sub nsw i32 %117, %116
  store i32 %sub70, i32* %z1, align 4, !tbaa !15
  %118 = load i32, i32* %z3, align 4, !tbaa !15
  %119 = load i32, i32* %z2, align 4, !tbaa !15
  %sub71 = sub nsw i32 %119, %118
  store i32 %sub71, i32* %z2, align 4, !tbaa !15
  %120 = load i32, i32* %z1, align 4, !tbaa !15
  %121 = load i32, i32* %z2, align 4, !tbaa !15
  %add72 = add nsw i32 %120, %121
  %mul73 = mul nsw i32 %add72, 4433
  store i32 %mul73, i32* %z3, align 4, !tbaa !15
  %122 = load i32, i32* %z3, align 4, !tbaa !15
  %123 = load i32, i32* %z1, align 4, !tbaa !15
  %mul74 = mul nsw i32 %123, 6270
  %add75 = add nsw i32 %122, %mul74
  store i32 %add75, i32* %tmp11, align 4, !tbaa !15
  %124 = load i32, i32* %z3, align 4, !tbaa !15
  %125 = load i32, i32* %z2, align 4, !tbaa !15
  %mul76 = mul nsw i32 %125, 15137
  %sub77 = sub nsw i32 %124, %mul76
  store i32 %sub77, i32* %tmp14, align 4, !tbaa !15
  %126 = load i32, i32* %tmp20, align 4, !tbaa !15
  %127 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add78 = add nsw i32 %126, %127
  %shr = ashr i32 %add78, 11
  %128 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i32, i32* %128, i32 0
  store i32 %shr, i32* %arrayidx79, align 4, !tbaa !6
  %129 = load i32, i32* %tmp20, align 4, !tbaa !15
  %130 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub80 = sub nsw i32 %129, %130
  %shr81 = ashr i32 %sub80, 11
  %131 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i32, i32* %131, i32 88
  store i32 %shr81, i32* %arrayidx82, align 4, !tbaa !6
  %132 = load i32, i32* %tmp21, align 4, !tbaa !15
  %133 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add83 = add nsw i32 %132, %133
  %shr84 = ashr i32 %add83, 11
  %134 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i32, i32* %134, i32 8
  store i32 %shr84, i32* %arrayidx85, align 4, !tbaa !6
  %135 = load i32, i32* %tmp21, align 4, !tbaa !15
  %136 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub86 = sub nsw i32 %135, %136
  %shr87 = ashr i32 %sub86, 11
  %137 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds i32, i32* %137, i32 80
  store i32 %shr87, i32* %arrayidx88, align 4, !tbaa !6
  %138 = load i32, i32* %tmp22, align 4, !tbaa !15
  %139 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add89 = add nsw i32 %138, %139
  %shr90 = ashr i32 %add89, 11
  %140 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds i32, i32* %140, i32 16
  store i32 %shr90, i32* %arrayidx91, align 4, !tbaa !6
  %141 = load i32, i32* %tmp22, align 4, !tbaa !15
  %142 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub92 = sub nsw i32 %141, %142
  %shr93 = ashr i32 %sub92, 11
  %143 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i32, i32* %143, i32 72
  store i32 %shr93, i32* %arrayidx94, align 4, !tbaa !6
  %144 = load i32, i32* %tmp23, align 4, !tbaa !15
  %145 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add95 = add nsw i32 %144, %145
  %shr96 = ashr i32 %add95, 11
  %146 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i32, i32* %146, i32 24
  store i32 %shr96, i32* %arrayidx97, align 4, !tbaa !6
  %147 = load i32, i32* %tmp23, align 4, !tbaa !15
  %148 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub98 = sub nsw i32 %147, %148
  %shr99 = ashr i32 %sub98, 11
  %149 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i32, i32* %149, i32 64
  store i32 %shr99, i32* %arrayidx100, align 4, !tbaa !6
  %150 = load i32, i32* %tmp24, align 4, !tbaa !15
  %151 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add101 = add nsw i32 %150, %151
  %shr102 = ashr i32 %add101, 11
  %152 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %152, i32 32
  store i32 %shr102, i32* %arrayidx103, align 4, !tbaa !6
  %153 = load i32, i32* %tmp24, align 4, !tbaa !15
  %154 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub104 = sub nsw i32 %153, %154
  %shr105 = ashr i32 %sub104, 11
  %155 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i32, i32* %155, i32 56
  store i32 %shr105, i32* %arrayidx106, align 4, !tbaa !6
  %156 = load i32, i32* %tmp25, align 4, !tbaa !15
  %157 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add107 = add nsw i32 %156, %157
  %shr108 = ashr i32 %add107, 11
  %158 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %158, i32 40
  store i32 %shr108, i32* %arrayidx109, align 4, !tbaa !6
  %159 = load i32, i32* %tmp25, align 4, !tbaa !15
  %160 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub110 = sub nsw i32 %159, %160
  %shr111 = ashr i32 %sub110, 11
  %161 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %161, i32 48
  store i32 %shr111, i32* %arrayidx112, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %162 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %162, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %163 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %163, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %164 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr113 = getelementptr inbounds i32, i32* %164, i32 1
  store i32* %incdec.ptr113, i32** %quantptr, align 4, !tbaa !2
  %165 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr114 = getelementptr inbounds i32, i32* %165, i32 1
  store i32* %incdec.ptr114, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay115 = getelementptr inbounds [96 x i32], [96 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay115, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond116

for.cond116:                                      ; preds = %for.inc241, %for.end
  %166 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp117 = icmp slt i32 %166, 12
  br i1 %cmp117, label %for.body119, label %for.end243

for.body119:                                      ; preds = %for.cond116
  %167 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %168 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx120 = getelementptr inbounds i8*, i8** %167, i32 %168
  %169 = load i8*, i8** %arrayidx120, align 4, !tbaa !2
  %170 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr121 = getelementptr inbounds i8, i8* %169, i32 %170
  store i8* %add.ptr121, i8** %outptr, align 4, !tbaa !2
  %171 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %171, i32 0
  %172 = load i32, i32* %arrayidx122, align 4, !tbaa !6
  %add123 = add nsw i32 %172, 16
  store i32 %add123, i32* %z3, align 4, !tbaa !15
  %173 = load i32, i32* %z3, align 4, !tbaa !15
  %shl124 = shl i32 %173, 13
  store i32 %shl124, i32* %z3, align 4, !tbaa !15
  %174 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %174, i32 4
  %175 = load i32, i32* %arrayidx125, align 4, !tbaa !6
  store i32 %175, i32* %z4, align 4, !tbaa !15
  %176 = load i32, i32* %z4, align 4, !tbaa !15
  %mul126 = mul nsw i32 %176, 10033
  store i32 %mul126, i32* %z4, align 4, !tbaa !15
  %177 = load i32, i32* %z3, align 4, !tbaa !15
  %178 = load i32, i32* %z4, align 4, !tbaa !15
  %add127 = add nsw i32 %177, %178
  store i32 %add127, i32* %tmp10, align 4, !tbaa !15
  %179 = load i32, i32* %z3, align 4, !tbaa !15
  %180 = load i32, i32* %z4, align 4, !tbaa !15
  %sub128 = sub nsw i32 %179, %180
  store i32 %sub128, i32* %tmp11, align 4, !tbaa !15
  %181 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds i32, i32* %181, i32 2
  %182 = load i32, i32* %arrayidx129, align 4, !tbaa !6
  store i32 %182, i32* %z1, align 4, !tbaa !15
  %183 = load i32, i32* %z1, align 4, !tbaa !15
  %mul130 = mul nsw i32 %183, 11190
  store i32 %mul130, i32* %z4, align 4, !tbaa !15
  %184 = load i32, i32* %z1, align 4, !tbaa !15
  %shl131 = shl i32 %184, 13
  store i32 %shl131, i32* %z1, align 4, !tbaa !15
  %185 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx132 = getelementptr inbounds i32, i32* %185, i32 6
  %186 = load i32, i32* %arrayidx132, align 4, !tbaa !6
  store i32 %186, i32* %z2, align 4, !tbaa !15
  %187 = load i32, i32* %z2, align 4, !tbaa !15
  %shl133 = shl i32 %187, 13
  store i32 %shl133, i32* %z2, align 4, !tbaa !15
  %188 = load i32, i32* %z1, align 4, !tbaa !15
  %189 = load i32, i32* %z2, align 4, !tbaa !15
  %sub134 = sub nsw i32 %188, %189
  store i32 %sub134, i32* %tmp12, align 4, !tbaa !15
  %190 = load i32, i32* %z3, align 4, !tbaa !15
  %191 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add135 = add nsw i32 %190, %191
  store i32 %add135, i32* %tmp21, align 4, !tbaa !15
  %192 = load i32, i32* %z3, align 4, !tbaa !15
  %193 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub136 = sub nsw i32 %192, %193
  store i32 %sub136, i32* %tmp24, align 4, !tbaa !15
  %194 = load i32, i32* %z4, align 4, !tbaa !15
  %195 = load i32, i32* %z2, align 4, !tbaa !15
  %add137 = add nsw i32 %194, %195
  store i32 %add137, i32* %tmp12, align 4, !tbaa !15
  %196 = load i32, i32* %tmp10, align 4, !tbaa !15
  %197 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add138 = add nsw i32 %196, %197
  store i32 %add138, i32* %tmp20, align 4, !tbaa !15
  %198 = load i32, i32* %tmp10, align 4, !tbaa !15
  %199 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub139 = sub nsw i32 %198, %199
  store i32 %sub139, i32* %tmp25, align 4, !tbaa !15
  %200 = load i32, i32* %z4, align 4, !tbaa !15
  %201 = load i32, i32* %z1, align 4, !tbaa !15
  %sub140 = sub nsw i32 %200, %201
  %202 = load i32, i32* %z2, align 4, !tbaa !15
  %sub141 = sub nsw i32 %sub140, %202
  store i32 %sub141, i32* %tmp12, align 4, !tbaa !15
  %203 = load i32, i32* %tmp11, align 4, !tbaa !15
  %204 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add142 = add nsw i32 %203, %204
  store i32 %add142, i32* %tmp22, align 4, !tbaa !15
  %205 = load i32, i32* %tmp11, align 4, !tbaa !15
  %206 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub143 = sub nsw i32 %205, %206
  store i32 %sub143, i32* %tmp23, align 4, !tbaa !15
  %207 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds i32, i32* %207, i32 1
  %208 = load i32, i32* %arrayidx144, align 4, !tbaa !6
  store i32 %208, i32* %z1, align 4, !tbaa !15
  %209 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds i32, i32* %209, i32 3
  %210 = load i32, i32* %arrayidx145, align 4, !tbaa !6
  store i32 %210, i32* %z2, align 4, !tbaa !15
  %211 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i32, i32* %211, i32 5
  %212 = load i32, i32* %arrayidx146, align 4, !tbaa !6
  store i32 %212, i32* %z3, align 4, !tbaa !15
  %213 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx147 = getelementptr inbounds i32, i32* %213, i32 7
  %214 = load i32, i32* %arrayidx147, align 4, !tbaa !6
  store i32 %214, i32* %z4, align 4, !tbaa !15
  %215 = load i32, i32* %z2, align 4, !tbaa !15
  %mul148 = mul nsw i32 %215, 10703
  store i32 %mul148, i32* %tmp11, align 4, !tbaa !15
  %216 = load i32, i32* %z2, align 4, !tbaa !15
  %mul149 = mul nsw i32 %216, -4433
  store i32 %mul149, i32* %tmp14, align 4, !tbaa !15
  %217 = load i32, i32* %z1, align 4, !tbaa !15
  %218 = load i32, i32* %z3, align 4, !tbaa !15
  %add150 = add nsw i32 %217, %218
  store i32 %add150, i32* %tmp10, align 4, !tbaa !15
  %219 = load i32, i32* %tmp10, align 4, !tbaa !15
  %220 = load i32, i32* %z4, align 4, !tbaa !15
  %add151 = add nsw i32 %219, %220
  %mul152 = mul nsw i32 %add151, 7053
  store i32 %mul152, i32* %tmp15, align 4, !tbaa !15
  %221 = load i32, i32* %tmp15, align 4, !tbaa !15
  %222 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul153 = mul nsw i32 %222, 2139
  %add154 = add nsw i32 %221, %mul153
  store i32 %add154, i32* %tmp12, align 4, !tbaa !15
  %223 = load i32, i32* %tmp12, align 4, !tbaa !15
  %224 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add155 = add nsw i32 %223, %224
  %225 = load i32, i32* %z1, align 4, !tbaa !15
  %mul156 = mul nsw i32 %225, 2295
  %add157 = add nsw i32 %add155, %mul156
  store i32 %add157, i32* %tmp10, align 4, !tbaa !15
  %226 = load i32, i32* %z3, align 4, !tbaa !15
  %227 = load i32, i32* %z4, align 4, !tbaa !15
  %add158 = add nsw i32 %226, %227
  %mul159 = mul nsw i32 %add158, -8565
  store i32 %mul159, i32* %tmp13, align 4, !tbaa !15
  %228 = load i32, i32* %tmp13, align 4, !tbaa !15
  %229 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add160 = add nsw i32 %228, %229
  %230 = load i32, i32* %z3, align 4, !tbaa !15
  %mul161 = mul nsw i32 %230, 12112
  %sub162 = sub nsw i32 %add160, %mul161
  %231 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add163 = add nsw i32 %231, %sub162
  store i32 %add163, i32* %tmp12, align 4, !tbaa !15
  %232 = load i32, i32* %tmp15, align 4, !tbaa !15
  %233 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub164 = sub nsw i32 %232, %233
  %234 = load i32, i32* %z4, align 4, !tbaa !15
  %mul165 = mul nsw i32 %234, 12998
  %add166 = add nsw i32 %sub164, %mul165
  %235 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add167 = add nsw i32 %235, %add166
  store i32 %add167, i32* %tmp13, align 4, !tbaa !15
  %236 = load i32, i32* %tmp14, align 4, !tbaa !15
  %237 = load i32, i32* %z1, align 4, !tbaa !15
  %mul168 = mul nsw i32 %237, 5540
  %sub169 = sub nsw i32 %236, %mul168
  %238 = load i32, i32* %z4, align 4, !tbaa !15
  %mul170 = mul nsw i32 %238, 16244
  %sub171 = sub nsw i32 %sub169, %mul170
  %239 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add172 = add nsw i32 %239, %sub171
  store i32 %add172, i32* %tmp15, align 4, !tbaa !15
  %240 = load i32, i32* %z4, align 4, !tbaa !15
  %241 = load i32, i32* %z1, align 4, !tbaa !15
  %sub173 = sub nsw i32 %241, %240
  store i32 %sub173, i32* %z1, align 4, !tbaa !15
  %242 = load i32, i32* %z3, align 4, !tbaa !15
  %243 = load i32, i32* %z2, align 4, !tbaa !15
  %sub174 = sub nsw i32 %243, %242
  store i32 %sub174, i32* %z2, align 4, !tbaa !15
  %244 = load i32, i32* %z1, align 4, !tbaa !15
  %245 = load i32, i32* %z2, align 4, !tbaa !15
  %add175 = add nsw i32 %244, %245
  %mul176 = mul nsw i32 %add175, 4433
  store i32 %mul176, i32* %z3, align 4, !tbaa !15
  %246 = load i32, i32* %z3, align 4, !tbaa !15
  %247 = load i32, i32* %z1, align 4, !tbaa !15
  %mul177 = mul nsw i32 %247, 6270
  %add178 = add nsw i32 %246, %mul177
  store i32 %add178, i32* %tmp11, align 4, !tbaa !15
  %248 = load i32, i32* %z3, align 4, !tbaa !15
  %249 = load i32, i32* %z2, align 4, !tbaa !15
  %mul179 = mul nsw i32 %249, 15137
  %sub180 = sub nsw i32 %248, %mul179
  store i32 %sub180, i32* %tmp14, align 4, !tbaa !15
  %250 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %251 = load i32, i32* %tmp20, align 4, !tbaa !15
  %252 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add181 = add nsw i32 %251, %252
  %shr182 = ashr i32 %add181, 18
  %and = and i32 %shr182, 1023
  %arrayidx183 = getelementptr inbounds i8, i8* %250, i32 %and
  %253 = load i8, i8* %arrayidx183, align 1, !tbaa !17
  %254 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx184 = getelementptr inbounds i8, i8* %254, i32 0
  store i8 %253, i8* %arrayidx184, align 1, !tbaa !17
  %255 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %256 = load i32, i32* %tmp20, align 4, !tbaa !15
  %257 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub185 = sub nsw i32 %256, %257
  %shr186 = ashr i32 %sub185, 18
  %and187 = and i32 %shr186, 1023
  %arrayidx188 = getelementptr inbounds i8, i8* %255, i32 %and187
  %258 = load i8, i8* %arrayidx188, align 1, !tbaa !17
  %259 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx189 = getelementptr inbounds i8, i8* %259, i32 11
  store i8 %258, i8* %arrayidx189, align 1, !tbaa !17
  %260 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %261 = load i32, i32* %tmp21, align 4, !tbaa !15
  %262 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add190 = add nsw i32 %261, %262
  %shr191 = ashr i32 %add190, 18
  %and192 = and i32 %shr191, 1023
  %arrayidx193 = getelementptr inbounds i8, i8* %260, i32 %and192
  %263 = load i8, i8* %arrayidx193, align 1, !tbaa !17
  %264 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx194 = getelementptr inbounds i8, i8* %264, i32 1
  store i8 %263, i8* %arrayidx194, align 1, !tbaa !17
  %265 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %266 = load i32, i32* %tmp21, align 4, !tbaa !15
  %267 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub195 = sub nsw i32 %266, %267
  %shr196 = ashr i32 %sub195, 18
  %and197 = and i32 %shr196, 1023
  %arrayidx198 = getelementptr inbounds i8, i8* %265, i32 %and197
  %268 = load i8, i8* %arrayidx198, align 1, !tbaa !17
  %269 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx199 = getelementptr inbounds i8, i8* %269, i32 10
  store i8 %268, i8* %arrayidx199, align 1, !tbaa !17
  %270 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %271 = load i32, i32* %tmp22, align 4, !tbaa !15
  %272 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add200 = add nsw i32 %271, %272
  %shr201 = ashr i32 %add200, 18
  %and202 = and i32 %shr201, 1023
  %arrayidx203 = getelementptr inbounds i8, i8* %270, i32 %and202
  %273 = load i8, i8* %arrayidx203, align 1, !tbaa !17
  %274 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx204 = getelementptr inbounds i8, i8* %274, i32 2
  store i8 %273, i8* %arrayidx204, align 1, !tbaa !17
  %275 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %276 = load i32, i32* %tmp22, align 4, !tbaa !15
  %277 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub205 = sub nsw i32 %276, %277
  %shr206 = ashr i32 %sub205, 18
  %and207 = and i32 %shr206, 1023
  %arrayidx208 = getelementptr inbounds i8, i8* %275, i32 %and207
  %278 = load i8, i8* %arrayidx208, align 1, !tbaa !17
  %279 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx209 = getelementptr inbounds i8, i8* %279, i32 9
  store i8 %278, i8* %arrayidx209, align 1, !tbaa !17
  %280 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %281 = load i32, i32* %tmp23, align 4, !tbaa !15
  %282 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add210 = add nsw i32 %281, %282
  %shr211 = ashr i32 %add210, 18
  %and212 = and i32 %shr211, 1023
  %arrayidx213 = getelementptr inbounds i8, i8* %280, i32 %and212
  %283 = load i8, i8* %arrayidx213, align 1, !tbaa !17
  %284 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx214 = getelementptr inbounds i8, i8* %284, i32 3
  store i8 %283, i8* %arrayidx214, align 1, !tbaa !17
  %285 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %286 = load i32, i32* %tmp23, align 4, !tbaa !15
  %287 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub215 = sub nsw i32 %286, %287
  %shr216 = ashr i32 %sub215, 18
  %and217 = and i32 %shr216, 1023
  %arrayidx218 = getelementptr inbounds i8, i8* %285, i32 %and217
  %288 = load i8, i8* %arrayidx218, align 1, !tbaa !17
  %289 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx219 = getelementptr inbounds i8, i8* %289, i32 8
  store i8 %288, i8* %arrayidx219, align 1, !tbaa !17
  %290 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %291 = load i32, i32* %tmp24, align 4, !tbaa !15
  %292 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add220 = add nsw i32 %291, %292
  %shr221 = ashr i32 %add220, 18
  %and222 = and i32 %shr221, 1023
  %arrayidx223 = getelementptr inbounds i8, i8* %290, i32 %and222
  %293 = load i8, i8* %arrayidx223, align 1, !tbaa !17
  %294 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds i8, i8* %294, i32 4
  store i8 %293, i8* %arrayidx224, align 1, !tbaa !17
  %295 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %296 = load i32, i32* %tmp24, align 4, !tbaa !15
  %297 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub225 = sub nsw i32 %296, %297
  %shr226 = ashr i32 %sub225, 18
  %and227 = and i32 %shr226, 1023
  %arrayidx228 = getelementptr inbounds i8, i8* %295, i32 %and227
  %298 = load i8, i8* %arrayidx228, align 1, !tbaa !17
  %299 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx229 = getelementptr inbounds i8, i8* %299, i32 7
  store i8 %298, i8* %arrayidx229, align 1, !tbaa !17
  %300 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %301 = load i32, i32* %tmp25, align 4, !tbaa !15
  %302 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add230 = add nsw i32 %301, %302
  %shr231 = ashr i32 %add230, 18
  %and232 = and i32 %shr231, 1023
  %arrayidx233 = getelementptr inbounds i8, i8* %300, i32 %and232
  %303 = load i8, i8* %arrayidx233, align 1, !tbaa !17
  %304 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds i8, i8* %304, i32 5
  store i8 %303, i8* %arrayidx234, align 1, !tbaa !17
  %305 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %306 = load i32, i32* %tmp25, align 4, !tbaa !15
  %307 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub235 = sub nsw i32 %306, %307
  %shr236 = ashr i32 %sub235, 18
  %and237 = and i32 %shr236, 1023
  %arrayidx238 = getelementptr inbounds i8, i8* %305, i32 %and237
  %308 = load i8, i8* %arrayidx238, align 1, !tbaa !17
  %309 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx239 = getelementptr inbounds i8, i8* %309, i32 6
  store i8 %308, i8* %arrayidx239, align 1, !tbaa !17
  %310 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr240 = getelementptr inbounds i32, i32* %310, i32 8
  store i32* %add.ptr240, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc241

for.inc241:                                       ; preds = %for.body119
  %311 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc242 = add nsw i32 %311, 1
  store i32 %inc242, i32* %ctr, align 4, !tbaa !6
  br label %for.cond116

for.end243:                                       ; preds = %for.cond116
  %312 = bitcast [96 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 384, i8* %312) #2
  %313 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #2
  %314 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #2
  %315 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #2
  %316 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %316) #2
  %317 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #2
  %318 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %318) #2
  %319 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #2
  %320 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #2
  %321 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %321) #2
  %322 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %322) #2
  %323 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %323) #2
  %324 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %324) #2
  %325 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %325) #2
  %326 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #2
  %327 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %327) #2
  %328 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %328) #2
  %329 = bitcast i32* %tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %329) #2
  %330 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %330) #2
  %331 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %331) #2
  %332 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %332) #2
  %333 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %333) #2
  %334 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_13x13(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %tmp14 = alloca i32, align 4
  %tmp15 = alloca i32, align 4
  %tmp20 = alloca i32, align 4
  %tmp21 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %tmp23 = alloca i32, align 4
  %tmp24 = alloca i32, align 4
  %tmp25 = alloca i32, align 4
  %tmp26 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [104 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 65
  %23 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %23, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %24 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #2
  %25 = bitcast [104 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 416, i8* %25) #2
  %26 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %26, i16** %inptr, align 4, !tbaa !2
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %27, i32 0, i32 20
  %28 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %29 = bitcast i8* %28 to i32*
  store i32* %29, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [104 x i32], [104 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %30 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %30, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %31, i32 0
  %32 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %32 to i32
  %33 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %33, i32 0
  %34 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %34
  store i32 %mul, i32* %z1, align 4, !tbaa !15
  %35 = load i32, i32* %z1, align 4, !tbaa !15
  %shl = shl i32 %35, 13
  store i32 %shl, i32* %z1, align 4, !tbaa !15
  %36 = load i32, i32* %z1, align 4, !tbaa !15
  %add = add nsw i32 %36, 1024
  store i32 %add, i32* %z1, align 4, !tbaa !15
  %37 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %37, i32 16
  %38 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %38 to i32
  %39 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %39, i32 16
  %40 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %40
  store i32 %mul5, i32* %z2, align 4, !tbaa !15
  %41 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %41, i32 32
  %42 = load i16, i16* %arrayidx6, align 2, !tbaa !14
  %conv7 = sext i16 %42 to i32
  %43 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %43, i32 32
  %44 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %mul9 = mul nsw i32 %conv7, %44
  store i32 %mul9, i32* %z3, align 4, !tbaa !15
  %45 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %45, i32 48
  %46 = load i16, i16* %arrayidx10, align 2, !tbaa !14
  %conv11 = sext i16 %46 to i32
  %47 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %47, i32 48
  %48 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %mul13 = mul nsw i32 %conv11, %48
  store i32 %mul13, i32* %z4, align 4, !tbaa !15
  %49 = load i32, i32* %z3, align 4, !tbaa !15
  %50 = load i32, i32* %z4, align 4, !tbaa !15
  %add14 = add nsw i32 %49, %50
  store i32 %add14, i32* %tmp10, align 4, !tbaa !15
  %51 = load i32, i32* %z3, align 4, !tbaa !15
  %52 = load i32, i32* %z4, align 4, !tbaa !15
  %sub = sub nsw i32 %51, %52
  store i32 %sub, i32* %tmp11, align 4, !tbaa !15
  %53 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul15 = mul nsw i32 %53, 9465
  store i32 %mul15, i32* %tmp12, align 4, !tbaa !15
  %54 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul16 = mul nsw i32 %54, 793
  %55 = load i32, i32* %z1, align 4, !tbaa !15
  %add17 = add nsw i32 %mul16, %55
  store i32 %add17, i32* %tmp13, align 4, !tbaa !15
  %56 = load i32, i32* %z2, align 4, !tbaa !15
  %mul18 = mul nsw i32 %56, 11249
  %57 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add19 = add nsw i32 %mul18, %57
  %58 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add20 = add nsw i32 %add19, %58
  store i32 %add20, i32* %tmp20, align 4, !tbaa !15
  %59 = load i32, i32* %z2, align 4, !tbaa !15
  %mul21 = mul nsw i32 %59, 4108
  %60 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub22 = sub nsw i32 %mul21, %60
  %61 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add23 = add nsw i32 %sub22, %61
  store i32 %add23, i32* %tmp22, align 4, !tbaa !15
  %62 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul24 = mul nsw i32 %62, 2592
  store i32 %mul24, i32* %tmp12, align 4, !tbaa !15
  %63 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul25 = mul nsw i32 %63, 3989
  %64 = load i32, i32* %z1, align 4, !tbaa !15
  %add26 = add nsw i32 %mul25, %64
  store i32 %add26, i32* %tmp13, align 4, !tbaa !15
  %65 = load i32, i32* %z2, align 4, !tbaa !15
  %mul27 = mul nsw i32 %65, 8672
  %66 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub28 = sub nsw i32 %mul27, %66
  %67 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add29 = add nsw i32 %sub28, %67
  store i32 %add29, i32* %tmp21, align 4, !tbaa !15
  %68 = load i32, i32* %z2, align 4, !tbaa !15
  %mul30 = mul nsw i32 %68, -10258
  %69 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add31 = add nsw i32 %mul30, %69
  %70 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add32 = add nsw i32 %add31, %70
  store i32 %add32, i32* %tmp25, align 4, !tbaa !15
  %71 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul33 = mul nsw i32 %71, 3570
  store i32 %mul33, i32* %tmp12, align 4, !tbaa !15
  %72 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul34 = mul nsw i32 %72, 7678
  %73 = load i32, i32* %z1, align 4, !tbaa !15
  %sub35 = sub nsw i32 %mul34, %73
  store i32 %sub35, i32* %tmp13, align 4, !tbaa !15
  %74 = load i32, i32* %z2, align 4, !tbaa !15
  %mul36 = mul nsw i32 %74, -1396
  %75 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub37 = sub nsw i32 %mul36, %75
  %76 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub38 = sub nsw i32 %sub37, %76
  store i32 %sub38, i32* %tmp23, align 4, !tbaa !15
  %77 = load i32, i32* %z2, align 4, !tbaa !15
  %mul39 = mul nsw i32 %77, -6581
  %78 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add40 = add nsw i32 %mul39, %78
  %79 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub41 = sub nsw i32 %add40, %79
  store i32 %sub41, i32* %tmp24, align 4, !tbaa !15
  %80 = load i32, i32* %tmp11, align 4, !tbaa !15
  %81 = load i32, i32* %z2, align 4, !tbaa !15
  %sub42 = sub nsw i32 %80, %81
  %mul43 = mul nsw i32 %sub42, 11585
  %82 = load i32, i32* %z1, align 4, !tbaa !15
  %add44 = add nsw i32 %mul43, %82
  store i32 %add44, i32* %tmp26, align 4, !tbaa !15
  %83 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i16, i16* %83, i32 8
  %84 = load i16, i16* %arrayidx45, align 2, !tbaa !14
  %conv46 = sext i16 %84 to i32
  %85 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %85, i32 8
  %86 = load i32, i32* %arrayidx47, align 4, !tbaa !6
  %mul48 = mul nsw i32 %conv46, %86
  store i32 %mul48, i32* %z1, align 4, !tbaa !15
  %87 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i16, i16* %87, i32 24
  %88 = load i16, i16* %arrayidx49, align 2, !tbaa !14
  %conv50 = sext i16 %88 to i32
  %89 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i32, i32* %89, i32 24
  %90 = load i32, i32* %arrayidx51, align 4, !tbaa !6
  %mul52 = mul nsw i32 %conv50, %90
  store i32 %mul52, i32* %z2, align 4, !tbaa !15
  %91 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i16, i16* %91, i32 40
  %92 = load i16, i16* %arrayidx53, align 2, !tbaa !14
  %conv54 = sext i16 %92 to i32
  %93 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i32, i32* %93, i32 40
  %94 = load i32, i32* %arrayidx55, align 4, !tbaa !6
  %mul56 = mul nsw i32 %conv54, %94
  store i32 %mul56, i32* %z3, align 4, !tbaa !15
  %95 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i16, i16* %95, i32 56
  %96 = load i16, i16* %arrayidx57, align 2, !tbaa !14
  %conv58 = sext i16 %96 to i32
  %97 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i32, i32* %97, i32 56
  %98 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %mul60 = mul nsw i32 %conv58, %98
  store i32 %mul60, i32* %z4, align 4, !tbaa !15
  %99 = load i32, i32* %z1, align 4, !tbaa !15
  %100 = load i32, i32* %z2, align 4, !tbaa !15
  %add61 = add nsw i32 %99, %100
  %mul62 = mul nsw i32 %add61, 10832
  store i32 %mul62, i32* %tmp11, align 4, !tbaa !15
  %101 = load i32, i32* %z1, align 4, !tbaa !15
  %102 = load i32, i32* %z3, align 4, !tbaa !15
  %add63 = add nsw i32 %101, %102
  %mul64 = mul nsw i32 %add63, 9534
  store i32 %mul64, i32* %tmp12, align 4, !tbaa !15
  %103 = load i32, i32* %z1, align 4, !tbaa !15
  %104 = load i32, i32* %z4, align 4, !tbaa !15
  %add65 = add nsw i32 %103, %104
  store i32 %add65, i32* %tmp15, align 4, !tbaa !15
  %105 = load i32, i32* %tmp15, align 4, !tbaa !15
  %mul66 = mul nsw i32 %105, 7682
  store i32 %mul66, i32* %tmp13, align 4, !tbaa !15
  %106 = load i32, i32* %tmp11, align 4, !tbaa !15
  %107 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add67 = add nsw i32 %106, %107
  %108 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add68 = add nsw i32 %add67, %108
  %109 = load i32, i32* %z1, align 4, !tbaa !15
  %mul69 = mul nsw i32 %109, 16549
  %sub70 = sub nsw i32 %add68, %mul69
  store i32 %sub70, i32* %tmp10, align 4, !tbaa !15
  %110 = load i32, i32* %z2, align 4, !tbaa !15
  %111 = load i32, i32* %z3, align 4, !tbaa !15
  %add71 = add nsw i32 %110, %111
  %mul72 = mul nsw i32 %add71, -2773
  store i32 %mul72, i32* %tmp14, align 4, !tbaa !15
  %112 = load i32, i32* %tmp14, align 4, !tbaa !15
  %113 = load i32, i32* %z2, align 4, !tbaa !15
  %mul73 = mul nsw i32 %113, 6859
  %add74 = add nsw i32 %112, %mul73
  %114 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add75 = add nsw i32 %114, %add74
  store i32 %add75, i32* %tmp11, align 4, !tbaa !15
  %115 = load i32, i32* %tmp14, align 4, !tbaa !15
  %116 = load i32, i32* %z3, align 4, !tbaa !15
  %mul76 = mul nsw i32 %116, 12879
  %sub77 = sub nsw i32 %115, %mul76
  %117 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add78 = add nsw i32 %117, %sub77
  store i32 %add78, i32* %tmp12, align 4, !tbaa !15
  %118 = load i32, i32* %z2, align 4, !tbaa !15
  %119 = load i32, i32* %z4, align 4, !tbaa !15
  %add79 = add nsw i32 %118, %119
  %mul80 = mul nsw i32 %add79, -9534
  store i32 %mul80, i32* %tmp14, align 4, !tbaa !15
  %120 = load i32, i32* %tmp14, align 4, !tbaa !15
  %121 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add81 = add nsw i32 %121, %120
  store i32 %add81, i32* %tmp11, align 4, !tbaa !15
  %122 = load i32, i32* %tmp14, align 4, !tbaa !15
  %123 = load i32, i32* %z4, align 4, !tbaa !15
  %mul82 = mul nsw i32 %123, 18068
  %add83 = add nsw i32 %122, %mul82
  %124 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add84 = add nsw i32 %124, %add83
  store i32 %add84, i32* %tmp13, align 4, !tbaa !15
  %125 = load i32, i32* %z3, align 4, !tbaa !15
  %126 = load i32, i32* %z4, align 4, !tbaa !15
  %add85 = add nsw i32 %125, %126
  %mul86 = mul nsw i32 %add85, -5384
  store i32 %mul86, i32* %tmp14, align 4, !tbaa !15
  %127 = load i32, i32* %tmp14, align 4, !tbaa !15
  %128 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add87 = add nsw i32 %128, %127
  store i32 %add87, i32* %tmp12, align 4, !tbaa !15
  %129 = load i32, i32* %tmp14, align 4, !tbaa !15
  %130 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add88 = add nsw i32 %130, %129
  store i32 %add88, i32* %tmp13, align 4, !tbaa !15
  %131 = load i32, i32* %tmp15, align 4, !tbaa !15
  %mul89 = mul nsw i32 %131, 2773
  store i32 %mul89, i32* %tmp15, align 4, !tbaa !15
  %132 = load i32, i32* %tmp15, align 4, !tbaa !15
  %133 = load i32, i32* %z1, align 4, !tbaa !15
  %mul90 = mul nsw i32 %133, 2611
  %add91 = add nsw i32 %132, %mul90
  %134 = load i32, i32* %z2, align 4, !tbaa !15
  %mul92 = mul nsw i32 %134, 3818
  %sub93 = sub nsw i32 %add91, %mul92
  store i32 %sub93, i32* %tmp14, align 4, !tbaa !15
  %135 = load i32, i32* %z3, align 4, !tbaa !15
  %136 = load i32, i32* %z2, align 4, !tbaa !15
  %sub94 = sub nsw i32 %135, %136
  %mul95 = mul nsw i32 %sub94, 7682
  store i32 %mul95, i32* %z1, align 4, !tbaa !15
  %137 = load i32, i32* %z1, align 4, !tbaa !15
  %138 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add96 = add nsw i32 %138, %137
  store i32 %add96, i32* %tmp14, align 4, !tbaa !15
  %139 = load i32, i32* %z1, align 4, !tbaa !15
  %140 = load i32, i32* %z3, align 4, !tbaa !15
  %mul97 = mul nsw i32 %140, 3150
  %add98 = add nsw i32 %139, %mul97
  %141 = load i32, i32* %z4, align 4, !tbaa !15
  %mul99 = mul nsw i32 %141, 14273
  %sub100 = sub nsw i32 %add98, %mul99
  %142 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add101 = add nsw i32 %142, %sub100
  store i32 %add101, i32* %tmp15, align 4, !tbaa !15
  %143 = load i32, i32* %tmp20, align 4, !tbaa !15
  %144 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add102 = add nsw i32 %143, %144
  %shr = ashr i32 %add102, 11
  %145 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds i32, i32* %145, i32 0
  store i32 %shr, i32* %arrayidx103, align 4, !tbaa !6
  %146 = load i32, i32* %tmp20, align 4, !tbaa !15
  %147 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub104 = sub nsw i32 %146, %147
  %shr105 = ashr i32 %sub104, 11
  %148 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx106 = getelementptr inbounds i32, i32* %148, i32 96
  store i32 %shr105, i32* %arrayidx106, align 4, !tbaa !6
  %149 = load i32, i32* %tmp21, align 4, !tbaa !15
  %150 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add107 = add nsw i32 %149, %150
  %shr108 = ashr i32 %add107, 11
  %151 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %151, i32 8
  store i32 %shr108, i32* %arrayidx109, align 4, !tbaa !6
  %152 = load i32, i32* %tmp21, align 4, !tbaa !15
  %153 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub110 = sub nsw i32 %152, %153
  %shr111 = ashr i32 %sub110, 11
  %154 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %154, i32 88
  store i32 %shr111, i32* %arrayidx112, align 4, !tbaa !6
  %155 = load i32, i32* %tmp22, align 4, !tbaa !15
  %156 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add113 = add nsw i32 %155, %156
  %shr114 = ashr i32 %add113, 11
  %157 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %157, i32 16
  store i32 %shr114, i32* %arrayidx115, align 4, !tbaa !6
  %158 = load i32, i32* %tmp22, align 4, !tbaa !15
  %159 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub116 = sub nsw i32 %158, %159
  %shr117 = ashr i32 %sub116, 11
  %160 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %160, i32 80
  store i32 %shr117, i32* %arrayidx118, align 4, !tbaa !6
  %161 = load i32, i32* %tmp23, align 4, !tbaa !15
  %162 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add119 = add nsw i32 %161, %162
  %shr120 = ashr i32 %add119, 11
  %163 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i32, i32* %163, i32 24
  store i32 %shr120, i32* %arrayidx121, align 4, !tbaa !6
  %164 = load i32, i32* %tmp23, align 4, !tbaa !15
  %165 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub122 = sub nsw i32 %164, %165
  %shr123 = ashr i32 %sub122, 11
  %166 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %166, i32 72
  store i32 %shr123, i32* %arrayidx124, align 4, !tbaa !6
  %167 = load i32, i32* %tmp24, align 4, !tbaa !15
  %168 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add125 = add nsw i32 %167, %168
  %shr126 = ashr i32 %add125, 11
  %169 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %169, i32 32
  store i32 %shr126, i32* %arrayidx127, align 4, !tbaa !6
  %170 = load i32, i32* %tmp24, align 4, !tbaa !15
  %171 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub128 = sub nsw i32 %170, %171
  %shr129 = ashr i32 %sub128, 11
  %172 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i32, i32* %172, i32 64
  store i32 %shr129, i32* %arrayidx130, align 4, !tbaa !6
  %173 = load i32, i32* %tmp25, align 4, !tbaa !15
  %174 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add131 = add nsw i32 %173, %174
  %shr132 = ashr i32 %add131, 11
  %175 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i32, i32* %175, i32 40
  store i32 %shr132, i32* %arrayidx133, align 4, !tbaa !6
  %176 = load i32, i32* %tmp25, align 4, !tbaa !15
  %177 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub134 = sub nsw i32 %176, %177
  %shr135 = ashr i32 %sub134, 11
  %178 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %178, i32 56
  store i32 %shr135, i32* %arrayidx136, align 4, !tbaa !6
  %179 = load i32, i32* %tmp26, align 4, !tbaa !15
  %shr137 = ashr i32 %179, 11
  %180 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx138 = getelementptr inbounds i32, i32* %180, i32 48
  store i32 %shr137, i32* %arrayidx138, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %181 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %181, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %182 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %182, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %183 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr139 = getelementptr inbounds i32, i32* %183, i32 1
  store i32* %incdec.ptr139, i32** %quantptr, align 4, !tbaa !2
  %184 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr140 = getelementptr inbounds i32, i32* %184, i32 1
  store i32* %incdec.ptr140, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay141 = getelementptr inbounds [104 x i32], [104 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay141, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond142

for.cond142:                                      ; preds = %for.inc295, %for.end
  %185 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp143 = icmp slt i32 %185, 13
  br i1 %cmp143, label %for.body145, label %for.end297

for.body145:                                      ; preds = %for.cond142
  %186 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %187 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx146 = getelementptr inbounds i8*, i8** %186, i32 %187
  %188 = load i8*, i8** %arrayidx146, align 4, !tbaa !2
  %189 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr147 = getelementptr inbounds i8, i8* %188, i32 %189
  store i8* %add.ptr147, i8** %outptr, align 4, !tbaa !2
  %190 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx148 = getelementptr inbounds i32, i32* %190, i32 0
  %191 = load i32, i32* %arrayidx148, align 4, !tbaa !6
  %add149 = add nsw i32 %191, 16
  store i32 %add149, i32* %z1, align 4, !tbaa !15
  %192 = load i32, i32* %z1, align 4, !tbaa !15
  %shl150 = shl i32 %192, 13
  store i32 %shl150, i32* %z1, align 4, !tbaa !15
  %193 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %193, i32 2
  %194 = load i32, i32* %arrayidx151, align 4, !tbaa !6
  store i32 %194, i32* %z2, align 4, !tbaa !15
  %195 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds i32, i32* %195, i32 4
  %196 = load i32, i32* %arrayidx152, align 4, !tbaa !6
  store i32 %196, i32* %z3, align 4, !tbaa !15
  %197 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %197, i32 6
  %198 = load i32, i32* %arrayidx153, align 4, !tbaa !6
  store i32 %198, i32* %z4, align 4, !tbaa !15
  %199 = load i32, i32* %z3, align 4, !tbaa !15
  %200 = load i32, i32* %z4, align 4, !tbaa !15
  %add154 = add nsw i32 %199, %200
  store i32 %add154, i32* %tmp10, align 4, !tbaa !15
  %201 = load i32, i32* %z3, align 4, !tbaa !15
  %202 = load i32, i32* %z4, align 4, !tbaa !15
  %sub155 = sub nsw i32 %201, %202
  store i32 %sub155, i32* %tmp11, align 4, !tbaa !15
  %203 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul156 = mul nsw i32 %203, 9465
  store i32 %mul156, i32* %tmp12, align 4, !tbaa !15
  %204 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul157 = mul nsw i32 %204, 793
  %205 = load i32, i32* %z1, align 4, !tbaa !15
  %add158 = add nsw i32 %mul157, %205
  store i32 %add158, i32* %tmp13, align 4, !tbaa !15
  %206 = load i32, i32* %z2, align 4, !tbaa !15
  %mul159 = mul nsw i32 %206, 11249
  %207 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add160 = add nsw i32 %mul159, %207
  %208 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add161 = add nsw i32 %add160, %208
  store i32 %add161, i32* %tmp20, align 4, !tbaa !15
  %209 = load i32, i32* %z2, align 4, !tbaa !15
  %mul162 = mul nsw i32 %209, 4108
  %210 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub163 = sub nsw i32 %mul162, %210
  %211 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add164 = add nsw i32 %sub163, %211
  store i32 %add164, i32* %tmp22, align 4, !tbaa !15
  %212 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul165 = mul nsw i32 %212, 2592
  store i32 %mul165, i32* %tmp12, align 4, !tbaa !15
  %213 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul166 = mul nsw i32 %213, 3989
  %214 = load i32, i32* %z1, align 4, !tbaa !15
  %add167 = add nsw i32 %mul166, %214
  store i32 %add167, i32* %tmp13, align 4, !tbaa !15
  %215 = load i32, i32* %z2, align 4, !tbaa !15
  %mul168 = mul nsw i32 %215, 8672
  %216 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub169 = sub nsw i32 %mul168, %216
  %217 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add170 = add nsw i32 %sub169, %217
  store i32 %add170, i32* %tmp21, align 4, !tbaa !15
  %218 = load i32, i32* %z2, align 4, !tbaa !15
  %mul171 = mul nsw i32 %218, -10258
  %219 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add172 = add nsw i32 %mul171, %219
  %220 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add173 = add nsw i32 %add172, %220
  store i32 %add173, i32* %tmp25, align 4, !tbaa !15
  %221 = load i32, i32* %tmp10, align 4, !tbaa !15
  %mul174 = mul nsw i32 %221, 3570
  store i32 %mul174, i32* %tmp12, align 4, !tbaa !15
  %222 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul175 = mul nsw i32 %222, 7678
  %223 = load i32, i32* %z1, align 4, !tbaa !15
  %sub176 = sub nsw i32 %mul175, %223
  store i32 %sub176, i32* %tmp13, align 4, !tbaa !15
  %224 = load i32, i32* %z2, align 4, !tbaa !15
  %mul177 = mul nsw i32 %224, -1396
  %225 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub178 = sub nsw i32 %mul177, %225
  %226 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub179 = sub nsw i32 %sub178, %226
  store i32 %sub179, i32* %tmp23, align 4, !tbaa !15
  %227 = load i32, i32* %z2, align 4, !tbaa !15
  %mul180 = mul nsw i32 %227, -6581
  %228 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add181 = add nsw i32 %mul180, %228
  %229 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub182 = sub nsw i32 %add181, %229
  store i32 %sub182, i32* %tmp24, align 4, !tbaa !15
  %230 = load i32, i32* %tmp11, align 4, !tbaa !15
  %231 = load i32, i32* %z2, align 4, !tbaa !15
  %sub183 = sub nsw i32 %230, %231
  %mul184 = mul nsw i32 %sub183, 11585
  %232 = load i32, i32* %z1, align 4, !tbaa !15
  %add185 = add nsw i32 %mul184, %232
  store i32 %add185, i32* %tmp26, align 4, !tbaa !15
  %233 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds i32, i32* %233, i32 1
  %234 = load i32, i32* %arrayidx186, align 4, !tbaa !6
  store i32 %234, i32* %z1, align 4, !tbaa !15
  %235 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx187 = getelementptr inbounds i32, i32* %235, i32 3
  %236 = load i32, i32* %arrayidx187, align 4, !tbaa !6
  store i32 %236, i32* %z2, align 4, !tbaa !15
  %237 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx188 = getelementptr inbounds i32, i32* %237, i32 5
  %238 = load i32, i32* %arrayidx188, align 4, !tbaa !6
  store i32 %238, i32* %z3, align 4, !tbaa !15
  %239 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx189 = getelementptr inbounds i32, i32* %239, i32 7
  %240 = load i32, i32* %arrayidx189, align 4, !tbaa !6
  store i32 %240, i32* %z4, align 4, !tbaa !15
  %241 = load i32, i32* %z1, align 4, !tbaa !15
  %242 = load i32, i32* %z2, align 4, !tbaa !15
  %add190 = add nsw i32 %241, %242
  %mul191 = mul nsw i32 %add190, 10832
  store i32 %mul191, i32* %tmp11, align 4, !tbaa !15
  %243 = load i32, i32* %z1, align 4, !tbaa !15
  %244 = load i32, i32* %z3, align 4, !tbaa !15
  %add192 = add nsw i32 %243, %244
  %mul193 = mul nsw i32 %add192, 9534
  store i32 %mul193, i32* %tmp12, align 4, !tbaa !15
  %245 = load i32, i32* %z1, align 4, !tbaa !15
  %246 = load i32, i32* %z4, align 4, !tbaa !15
  %add194 = add nsw i32 %245, %246
  store i32 %add194, i32* %tmp15, align 4, !tbaa !15
  %247 = load i32, i32* %tmp15, align 4, !tbaa !15
  %mul195 = mul nsw i32 %247, 7682
  store i32 %mul195, i32* %tmp13, align 4, !tbaa !15
  %248 = load i32, i32* %tmp11, align 4, !tbaa !15
  %249 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add196 = add nsw i32 %248, %249
  %250 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add197 = add nsw i32 %add196, %250
  %251 = load i32, i32* %z1, align 4, !tbaa !15
  %mul198 = mul nsw i32 %251, 16549
  %sub199 = sub nsw i32 %add197, %mul198
  store i32 %sub199, i32* %tmp10, align 4, !tbaa !15
  %252 = load i32, i32* %z2, align 4, !tbaa !15
  %253 = load i32, i32* %z3, align 4, !tbaa !15
  %add200 = add nsw i32 %252, %253
  %mul201 = mul nsw i32 %add200, -2773
  store i32 %mul201, i32* %tmp14, align 4, !tbaa !15
  %254 = load i32, i32* %tmp14, align 4, !tbaa !15
  %255 = load i32, i32* %z2, align 4, !tbaa !15
  %mul202 = mul nsw i32 %255, 6859
  %add203 = add nsw i32 %254, %mul202
  %256 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add204 = add nsw i32 %256, %add203
  store i32 %add204, i32* %tmp11, align 4, !tbaa !15
  %257 = load i32, i32* %tmp14, align 4, !tbaa !15
  %258 = load i32, i32* %z3, align 4, !tbaa !15
  %mul205 = mul nsw i32 %258, 12879
  %sub206 = sub nsw i32 %257, %mul205
  %259 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add207 = add nsw i32 %259, %sub206
  store i32 %add207, i32* %tmp12, align 4, !tbaa !15
  %260 = load i32, i32* %z2, align 4, !tbaa !15
  %261 = load i32, i32* %z4, align 4, !tbaa !15
  %add208 = add nsw i32 %260, %261
  %mul209 = mul nsw i32 %add208, -9534
  store i32 %mul209, i32* %tmp14, align 4, !tbaa !15
  %262 = load i32, i32* %tmp14, align 4, !tbaa !15
  %263 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add210 = add nsw i32 %263, %262
  store i32 %add210, i32* %tmp11, align 4, !tbaa !15
  %264 = load i32, i32* %tmp14, align 4, !tbaa !15
  %265 = load i32, i32* %z4, align 4, !tbaa !15
  %mul211 = mul nsw i32 %265, 18068
  %add212 = add nsw i32 %264, %mul211
  %266 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add213 = add nsw i32 %266, %add212
  store i32 %add213, i32* %tmp13, align 4, !tbaa !15
  %267 = load i32, i32* %z3, align 4, !tbaa !15
  %268 = load i32, i32* %z4, align 4, !tbaa !15
  %add214 = add nsw i32 %267, %268
  %mul215 = mul nsw i32 %add214, -5384
  store i32 %mul215, i32* %tmp14, align 4, !tbaa !15
  %269 = load i32, i32* %tmp14, align 4, !tbaa !15
  %270 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add216 = add nsw i32 %270, %269
  store i32 %add216, i32* %tmp12, align 4, !tbaa !15
  %271 = load i32, i32* %tmp14, align 4, !tbaa !15
  %272 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add217 = add nsw i32 %272, %271
  store i32 %add217, i32* %tmp13, align 4, !tbaa !15
  %273 = load i32, i32* %tmp15, align 4, !tbaa !15
  %mul218 = mul nsw i32 %273, 2773
  store i32 %mul218, i32* %tmp15, align 4, !tbaa !15
  %274 = load i32, i32* %tmp15, align 4, !tbaa !15
  %275 = load i32, i32* %z1, align 4, !tbaa !15
  %mul219 = mul nsw i32 %275, 2611
  %add220 = add nsw i32 %274, %mul219
  %276 = load i32, i32* %z2, align 4, !tbaa !15
  %mul221 = mul nsw i32 %276, 3818
  %sub222 = sub nsw i32 %add220, %mul221
  store i32 %sub222, i32* %tmp14, align 4, !tbaa !15
  %277 = load i32, i32* %z3, align 4, !tbaa !15
  %278 = load i32, i32* %z2, align 4, !tbaa !15
  %sub223 = sub nsw i32 %277, %278
  %mul224 = mul nsw i32 %sub223, 7682
  store i32 %mul224, i32* %z1, align 4, !tbaa !15
  %279 = load i32, i32* %z1, align 4, !tbaa !15
  %280 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add225 = add nsw i32 %280, %279
  store i32 %add225, i32* %tmp14, align 4, !tbaa !15
  %281 = load i32, i32* %z1, align 4, !tbaa !15
  %282 = load i32, i32* %z3, align 4, !tbaa !15
  %mul226 = mul nsw i32 %282, 3150
  %add227 = add nsw i32 %281, %mul226
  %283 = load i32, i32* %z4, align 4, !tbaa !15
  %mul228 = mul nsw i32 %283, 14273
  %sub229 = sub nsw i32 %add227, %mul228
  %284 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add230 = add nsw i32 %284, %sub229
  store i32 %add230, i32* %tmp15, align 4, !tbaa !15
  %285 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %286 = load i32, i32* %tmp20, align 4, !tbaa !15
  %287 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add231 = add nsw i32 %286, %287
  %shr232 = ashr i32 %add231, 18
  %and = and i32 %shr232, 1023
  %arrayidx233 = getelementptr inbounds i8, i8* %285, i32 %and
  %288 = load i8, i8* %arrayidx233, align 1, !tbaa !17
  %289 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds i8, i8* %289, i32 0
  store i8 %288, i8* %arrayidx234, align 1, !tbaa !17
  %290 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %291 = load i32, i32* %tmp20, align 4, !tbaa !15
  %292 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub235 = sub nsw i32 %291, %292
  %shr236 = ashr i32 %sub235, 18
  %and237 = and i32 %shr236, 1023
  %arrayidx238 = getelementptr inbounds i8, i8* %290, i32 %and237
  %293 = load i8, i8* %arrayidx238, align 1, !tbaa !17
  %294 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx239 = getelementptr inbounds i8, i8* %294, i32 12
  store i8 %293, i8* %arrayidx239, align 1, !tbaa !17
  %295 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %296 = load i32, i32* %tmp21, align 4, !tbaa !15
  %297 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add240 = add nsw i32 %296, %297
  %shr241 = ashr i32 %add240, 18
  %and242 = and i32 %shr241, 1023
  %arrayidx243 = getelementptr inbounds i8, i8* %295, i32 %and242
  %298 = load i8, i8* %arrayidx243, align 1, !tbaa !17
  %299 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx244 = getelementptr inbounds i8, i8* %299, i32 1
  store i8 %298, i8* %arrayidx244, align 1, !tbaa !17
  %300 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %301 = load i32, i32* %tmp21, align 4, !tbaa !15
  %302 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub245 = sub nsw i32 %301, %302
  %shr246 = ashr i32 %sub245, 18
  %and247 = and i32 %shr246, 1023
  %arrayidx248 = getelementptr inbounds i8, i8* %300, i32 %and247
  %303 = load i8, i8* %arrayidx248, align 1, !tbaa !17
  %304 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx249 = getelementptr inbounds i8, i8* %304, i32 11
  store i8 %303, i8* %arrayidx249, align 1, !tbaa !17
  %305 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %306 = load i32, i32* %tmp22, align 4, !tbaa !15
  %307 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add250 = add nsw i32 %306, %307
  %shr251 = ashr i32 %add250, 18
  %and252 = and i32 %shr251, 1023
  %arrayidx253 = getelementptr inbounds i8, i8* %305, i32 %and252
  %308 = load i8, i8* %arrayidx253, align 1, !tbaa !17
  %309 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx254 = getelementptr inbounds i8, i8* %309, i32 2
  store i8 %308, i8* %arrayidx254, align 1, !tbaa !17
  %310 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %311 = load i32, i32* %tmp22, align 4, !tbaa !15
  %312 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub255 = sub nsw i32 %311, %312
  %shr256 = ashr i32 %sub255, 18
  %and257 = and i32 %shr256, 1023
  %arrayidx258 = getelementptr inbounds i8, i8* %310, i32 %and257
  %313 = load i8, i8* %arrayidx258, align 1, !tbaa !17
  %314 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx259 = getelementptr inbounds i8, i8* %314, i32 10
  store i8 %313, i8* %arrayidx259, align 1, !tbaa !17
  %315 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %316 = load i32, i32* %tmp23, align 4, !tbaa !15
  %317 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add260 = add nsw i32 %316, %317
  %shr261 = ashr i32 %add260, 18
  %and262 = and i32 %shr261, 1023
  %arrayidx263 = getelementptr inbounds i8, i8* %315, i32 %and262
  %318 = load i8, i8* %arrayidx263, align 1, !tbaa !17
  %319 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx264 = getelementptr inbounds i8, i8* %319, i32 3
  store i8 %318, i8* %arrayidx264, align 1, !tbaa !17
  %320 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %321 = load i32, i32* %tmp23, align 4, !tbaa !15
  %322 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub265 = sub nsw i32 %321, %322
  %shr266 = ashr i32 %sub265, 18
  %and267 = and i32 %shr266, 1023
  %arrayidx268 = getelementptr inbounds i8, i8* %320, i32 %and267
  %323 = load i8, i8* %arrayidx268, align 1, !tbaa !17
  %324 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx269 = getelementptr inbounds i8, i8* %324, i32 9
  store i8 %323, i8* %arrayidx269, align 1, !tbaa !17
  %325 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %326 = load i32, i32* %tmp24, align 4, !tbaa !15
  %327 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add270 = add nsw i32 %326, %327
  %shr271 = ashr i32 %add270, 18
  %and272 = and i32 %shr271, 1023
  %arrayidx273 = getelementptr inbounds i8, i8* %325, i32 %and272
  %328 = load i8, i8* %arrayidx273, align 1, !tbaa !17
  %329 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx274 = getelementptr inbounds i8, i8* %329, i32 4
  store i8 %328, i8* %arrayidx274, align 1, !tbaa !17
  %330 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %331 = load i32, i32* %tmp24, align 4, !tbaa !15
  %332 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub275 = sub nsw i32 %331, %332
  %shr276 = ashr i32 %sub275, 18
  %and277 = and i32 %shr276, 1023
  %arrayidx278 = getelementptr inbounds i8, i8* %330, i32 %and277
  %333 = load i8, i8* %arrayidx278, align 1, !tbaa !17
  %334 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx279 = getelementptr inbounds i8, i8* %334, i32 8
  store i8 %333, i8* %arrayidx279, align 1, !tbaa !17
  %335 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %336 = load i32, i32* %tmp25, align 4, !tbaa !15
  %337 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add280 = add nsw i32 %336, %337
  %shr281 = ashr i32 %add280, 18
  %and282 = and i32 %shr281, 1023
  %arrayidx283 = getelementptr inbounds i8, i8* %335, i32 %and282
  %338 = load i8, i8* %arrayidx283, align 1, !tbaa !17
  %339 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx284 = getelementptr inbounds i8, i8* %339, i32 5
  store i8 %338, i8* %arrayidx284, align 1, !tbaa !17
  %340 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %341 = load i32, i32* %tmp25, align 4, !tbaa !15
  %342 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub285 = sub nsw i32 %341, %342
  %shr286 = ashr i32 %sub285, 18
  %and287 = and i32 %shr286, 1023
  %arrayidx288 = getelementptr inbounds i8, i8* %340, i32 %and287
  %343 = load i8, i8* %arrayidx288, align 1, !tbaa !17
  %344 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx289 = getelementptr inbounds i8, i8* %344, i32 7
  store i8 %343, i8* %arrayidx289, align 1, !tbaa !17
  %345 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %346 = load i32, i32* %tmp26, align 4, !tbaa !15
  %shr290 = ashr i32 %346, 18
  %and291 = and i32 %shr290, 1023
  %arrayidx292 = getelementptr inbounds i8, i8* %345, i32 %and291
  %347 = load i8, i8* %arrayidx292, align 1, !tbaa !17
  %348 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx293 = getelementptr inbounds i8, i8* %348, i32 6
  store i8 %347, i8* %arrayidx293, align 1, !tbaa !17
  %349 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr294 = getelementptr inbounds i32, i32* %349, i32 8
  store i32* %add.ptr294, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc295

for.inc295:                                       ; preds = %for.body145
  %350 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc296 = add nsw i32 %350, 1
  store i32 %inc296, i32* %ctr, align 4, !tbaa !6
  br label %for.cond142

for.end297:                                       ; preds = %for.cond142
  %351 = bitcast [104 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 416, i8* %351) #2
  %352 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %352) #2
  %353 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %353) #2
  %354 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %354) #2
  %355 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %355) #2
  %356 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %356) #2
  %357 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %357) #2
  %358 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %358) #2
  %359 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %359) #2
  %360 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %360) #2
  %361 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %361) #2
  %362 = bitcast i32* %tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %362) #2
  %363 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %363) #2
  %364 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %364) #2
  %365 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %365) #2
  %366 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %366) #2
  %367 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %367) #2
  %368 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %368) #2
  %369 = bitcast i32* %tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %369) #2
  %370 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %370) #2
  %371 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %371) #2
  %372 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %372) #2
  %373 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %373) #2
  %374 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %374) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_14x14(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %tmp14 = alloca i32, align 4
  %tmp15 = alloca i32, align 4
  %tmp16 = alloca i32, align 4
  %tmp20 = alloca i32, align 4
  %tmp21 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %tmp23 = alloca i32, align 4
  %tmp24 = alloca i32, align 4
  %tmp25 = alloca i32, align 4
  %tmp26 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [112 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  %22 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #2
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 65
  %24 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %24, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %25 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #2
  %26 = bitcast [112 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 448, i8* %26) #2
  %27 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %27, i16** %inptr, align 4, !tbaa !2
  %28 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %28, i32 0, i32 20
  %29 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %30 = bitcast i8* %29 to i32*
  store i32* %30, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [112 x i32], [112 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %31 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %31, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %32 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %32, i32 0
  %33 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %33 to i32
  %34 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %34, i32 0
  %35 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %35
  store i32 %mul, i32* %z1, align 4, !tbaa !15
  %36 = load i32, i32* %z1, align 4, !tbaa !15
  %shl = shl i32 %36, 13
  store i32 %shl, i32* %z1, align 4, !tbaa !15
  %37 = load i32, i32* %z1, align 4, !tbaa !15
  %add = add nsw i32 %37, 1024
  store i32 %add, i32* %z1, align 4, !tbaa !15
  %38 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %38, i32 32
  %39 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %39 to i32
  %40 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %40, i32 32
  %41 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %41
  store i32 %mul5, i32* %z4, align 4, !tbaa !15
  %42 = load i32, i32* %z4, align 4, !tbaa !15
  %mul6 = mul nsw i32 %42, 10438
  store i32 %mul6, i32* %z2, align 4, !tbaa !15
  %43 = load i32, i32* %z4, align 4, !tbaa !15
  %mul7 = mul nsw i32 %43, 2578
  store i32 %mul7, i32* %z3, align 4, !tbaa !15
  %44 = load i32, i32* %z4, align 4, !tbaa !15
  %mul8 = mul nsw i32 %44, 7223
  store i32 %mul8, i32* %z4, align 4, !tbaa !15
  %45 = load i32, i32* %z1, align 4, !tbaa !15
  %46 = load i32, i32* %z2, align 4, !tbaa !15
  %add9 = add nsw i32 %45, %46
  store i32 %add9, i32* %tmp10, align 4, !tbaa !15
  %47 = load i32, i32* %z1, align 4, !tbaa !15
  %48 = load i32, i32* %z3, align 4, !tbaa !15
  %add10 = add nsw i32 %47, %48
  store i32 %add10, i32* %tmp11, align 4, !tbaa !15
  %49 = load i32, i32* %z1, align 4, !tbaa !15
  %50 = load i32, i32* %z4, align 4, !tbaa !15
  %sub = sub nsw i32 %49, %50
  store i32 %sub, i32* %tmp12, align 4, !tbaa !15
  %51 = load i32, i32* %z1, align 4, !tbaa !15
  %52 = load i32, i32* %z2, align 4, !tbaa !15
  %53 = load i32, i32* %z3, align 4, !tbaa !15
  %add11 = add nsw i32 %52, %53
  %54 = load i32, i32* %z4, align 4, !tbaa !15
  %sub12 = sub nsw i32 %add11, %54
  %shl13 = shl i32 %sub12, 1
  %sub14 = sub nsw i32 %51, %shl13
  %shr = ashr i32 %sub14, 11
  store i32 %shr, i32* %tmp23, align 4, !tbaa !15
  %55 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i16, i16* %55, i32 16
  %56 = load i16, i16* %arrayidx15, align 2, !tbaa !14
  %conv16 = sext i16 %56 to i32
  %57 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %57, i32 16
  %58 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %mul18 = mul nsw i32 %conv16, %58
  store i32 %mul18, i32* %z1, align 4, !tbaa !15
  %59 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i16, i16* %59, i32 48
  %60 = load i16, i16* %arrayidx19, align 2, !tbaa !14
  %conv20 = sext i16 %60 to i32
  %61 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i32, i32* %61, i32 48
  %62 = load i32, i32* %arrayidx21, align 4, !tbaa !6
  %mul22 = mul nsw i32 %conv20, %62
  store i32 %mul22, i32* %z2, align 4, !tbaa !15
  %63 = load i32, i32* %z1, align 4, !tbaa !15
  %64 = load i32, i32* %z2, align 4, !tbaa !15
  %add23 = add nsw i32 %63, %64
  %mul24 = mul nsw i32 %add23, 9058
  store i32 %mul24, i32* %z3, align 4, !tbaa !15
  %65 = load i32, i32* %z3, align 4, !tbaa !15
  %66 = load i32, i32* %z1, align 4, !tbaa !15
  %mul25 = mul nsw i32 %66, 2237
  %add26 = add nsw i32 %65, %mul25
  store i32 %add26, i32* %tmp13, align 4, !tbaa !15
  %67 = load i32, i32* %z3, align 4, !tbaa !15
  %68 = load i32, i32* %z2, align 4, !tbaa !15
  %mul27 = mul nsw i32 %68, 14084
  %sub28 = sub nsw i32 %67, %mul27
  store i32 %sub28, i32* %tmp14, align 4, !tbaa !15
  %69 = load i32, i32* %z1, align 4, !tbaa !15
  %mul29 = mul nsw i32 %69, 5027
  %70 = load i32, i32* %z2, align 4, !tbaa !15
  %mul30 = mul nsw i32 %70, 11295
  %sub31 = sub nsw i32 %mul29, %mul30
  store i32 %sub31, i32* %tmp15, align 4, !tbaa !15
  %71 = load i32, i32* %tmp10, align 4, !tbaa !15
  %72 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add32 = add nsw i32 %71, %72
  store i32 %add32, i32* %tmp20, align 4, !tbaa !15
  %73 = load i32, i32* %tmp10, align 4, !tbaa !15
  %74 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub33 = sub nsw i32 %73, %74
  store i32 %sub33, i32* %tmp26, align 4, !tbaa !15
  %75 = load i32, i32* %tmp11, align 4, !tbaa !15
  %76 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add34 = add nsw i32 %75, %76
  store i32 %add34, i32* %tmp21, align 4, !tbaa !15
  %77 = load i32, i32* %tmp11, align 4, !tbaa !15
  %78 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub35 = sub nsw i32 %77, %78
  store i32 %sub35, i32* %tmp25, align 4, !tbaa !15
  %79 = load i32, i32* %tmp12, align 4, !tbaa !15
  %80 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add36 = add nsw i32 %79, %80
  store i32 %add36, i32* %tmp22, align 4, !tbaa !15
  %81 = load i32, i32* %tmp12, align 4, !tbaa !15
  %82 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub37 = sub nsw i32 %81, %82
  store i32 %sub37, i32* %tmp24, align 4, !tbaa !15
  %83 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i16, i16* %83, i32 8
  %84 = load i16, i16* %arrayidx38, align 2, !tbaa !14
  %conv39 = sext i16 %84 to i32
  %85 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %85, i32 8
  %86 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %mul41 = mul nsw i32 %conv39, %86
  store i32 %mul41, i32* %z1, align 4, !tbaa !15
  %87 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i16, i16* %87, i32 24
  %88 = load i16, i16* %arrayidx42, align 2, !tbaa !14
  %conv43 = sext i16 %88 to i32
  %89 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %89, i32 24
  %90 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %mul45 = mul nsw i32 %conv43, %90
  store i32 %mul45, i32* %z2, align 4, !tbaa !15
  %91 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i16, i16* %91, i32 40
  %92 = load i16, i16* %arrayidx46, align 2, !tbaa !14
  %conv47 = sext i16 %92 to i32
  %93 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %93, i32 40
  %94 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %mul49 = mul nsw i32 %conv47, %94
  store i32 %mul49, i32* %z3, align 4, !tbaa !15
  %95 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i16, i16* %95, i32 56
  %96 = load i16, i16* %arrayidx50, align 2, !tbaa !14
  %conv51 = sext i16 %96 to i32
  %97 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %97, i32 56
  %98 = load i32, i32* %arrayidx52, align 4, !tbaa !6
  %mul53 = mul nsw i32 %conv51, %98
  store i32 %mul53, i32* %z4, align 4, !tbaa !15
  %99 = load i32, i32* %z4, align 4, !tbaa !15
  %shl54 = shl i32 %99, 13
  store i32 %shl54, i32* %tmp13, align 4, !tbaa !15
  %100 = load i32, i32* %z1, align 4, !tbaa !15
  %101 = load i32, i32* %z3, align 4, !tbaa !15
  %add55 = add nsw i32 %100, %101
  store i32 %add55, i32* %tmp14, align 4, !tbaa !15
  %102 = load i32, i32* %z1, align 4, !tbaa !15
  %103 = load i32, i32* %z2, align 4, !tbaa !15
  %add56 = add nsw i32 %102, %103
  %mul57 = mul nsw i32 %add56, 10935
  store i32 %mul57, i32* %tmp11, align 4, !tbaa !15
  %104 = load i32, i32* %tmp14, align 4, !tbaa !15
  %mul58 = mul nsw i32 %104, 9810
  store i32 %mul58, i32* %tmp12, align 4, !tbaa !15
  %105 = load i32, i32* %tmp11, align 4, !tbaa !15
  %106 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add59 = add nsw i32 %105, %106
  %107 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add60 = add nsw i32 %add59, %107
  %108 = load i32, i32* %z1, align 4, !tbaa !15
  %mul61 = mul nsw i32 %108, 9232
  %sub62 = sub nsw i32 %add60, %mul61
  store i32 %sub62, i32* %tmp10, align 4, !tbaa !15
  %109 = load i32, i32* %tmp14, align 4, !tbaa !15
  %mul63 = mul nsw i32 %109, 6164
  store i32 %mul63, i32* %tmp14, align 4, !tbaa !15
  %110 = load i32, i32* %tmp14, align 4, !tbaa !15
  %111 = load i32, i32* %z1, align 4, !tbaa !15
  %mul64 = mul nsw i32 %111, 8693
  %sub65 = sub nsw i32 %110, %mul64
  store i32 %sub65, i32* %tmp16, align 4, !tbaa !15
  %112 = load i32, i32* %z2, align 4, !tbaa !15
  %113 = load i32, i32* %z1, align 4, !tbaa !15
  %sub66 = sub nsw i32 %113, %112
  store i32 %sub66, i32* %z1, align 4, !tbaa !15
  %114 = load i32, i32* %z1, align 4, !tbaa !15
  %mul67 = mul nsw i32 %114, 3826
  %115 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub68 = sub nsw i32 %mul67, %115
  store i32 %sub68, i32* %tmp15, align 4, !tbaa !15
  %116 = load i32, i32* %tmp15, align 4, !tbaa !15
  %117 = load i32, i32* %tmp16, align 4, !tbaa !15
  %add69 = add nsw i32 %117, %116
  store i32 %add69, i32* %tmp16, align 4, !tbaa !15
  %118 = load i32, i32* %z4, align 4, !tbaa !15
  %119 = load i32, i32* %z1, align 4, !tbaa !15
  %add70 = add nsw i32 %119, %118
  store i32 %add70, i32* %z1, align 4, !tbaa !15
  %120 = load i32, i32* %z2, align 4, !tbaa !15
  %121 = load i32, i32* %z3, align 4, !tbaa !15
  %add71 = add nsw i32 %120, %121
  %mul72 = mul nsw i32 %add71, -1297
  %122 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub73 = sub nsw i32 %mul72, %122
  store i32 %sub73, i32* %z4, align 4, !tbaa !15
  %123 = load i32, i32* %z4, align 4, !tbaa !15
  %124 = load i32, i32* %z2, align 4, !tbaa !15
  %mul74 = mul nsw i32 %124, 3474
  %sub75 = sub nsw i32 %123, %mul74
  %125 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add76 = add nsw i32 %125, %sub75
  store i32 %add76, i32* %tmp11, align 4, !tbaa !15
  %126 = load i32, i32* %z4, align 4, !tbaa !15
  %127 = load i32, i32* %z3, align 4, !tbaa !15
  %mul77 = mul nsw i32 %127, 19447
  %sub78 = sub nsw i32 %126, %mul77
  %128 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add79 = add nsw i32 %128, %sub78
  store i32 %add79, i32* %tmp12, align 4, !tbaa !15
  %129 = load i32, i32* %z3, align 4, !tbaa !15
  %130 = load i32, i32* %z2, align 4, !tbaa !15
  %sub80 = sub nsw i32 %129, %130
  %mul81 = mul nsw i32 %sub80, 11512
  store i32 %mul81, i32* %z4, align 4, !tbaa !15
  %131 = load i32, i32* %z4, align 4, !tbaa !15
  %132 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add82 = add nsw i32 %131, %132
  %133 = load i32, i32* %z3, align 4, !tbaa !15
  %mul83 = mul nsw i32 %133, 13850
  %sub84 = sub nsw i32 %add82, %mul83
  %134 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add85 = add nsw i32 %134, %sub84
  store i32 %add85, i32* %tmp14, align 4, !tbaa !15
  %135 = load i32, i32* %z4, align 4, !tbaa !15
  %136 = load i32, i32* %z2, align 4, !tbaa !15
  %mul86 = mul nsw i32 %136, 5529
  %add87 = add nsw i32 %135, %mul86
  %137 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add88 = add nsw i32 %137, %add87
  store i32 %add88, i32* %tmp15, align 4, !tbaa !15
  %138 = load i32, i32* %z1, align 4, !tbaa !15
  %139 = load i32, i32* %z3, align 4, !tbaa !15
  %sub89 = sub nsw i32 %138, %139
  %shl90 = shl i32 %sub89, 2
  store i32 %shl90, i32* %tmp13, align 4, !tbaa !15
  %140 = load i32, i32* %tmp20, align 4, !tbaa !15
  %141 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add91 = add nsw i32 %140, %141
  %shr92 = ashr i32 %add91, 11
  %142 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %142, i32 0
  store i32 %shr92, i32* %arrayidx93, align 4, !tbaa !6
  %143 = load i32, i32* %tmp20, align 4, !tbaa !15
  %144 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub94 = sub nsw i32 %143, %144
  %shr95 = ashr i32 %sub94, 11
  %145 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i32, i32* %145, i32 104
  store i32 %shr95, i32* %arrayidx96, align 4, !tbaa !6
  %146 = load i32, i32* %tmp21, align 4, !tbaa !15
  %147 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add97 = add nsw i32 %146, %147
  %shr98 = ashr i32 %add97, 11
  %148 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %148, i32 8
  store i32 %shr98, i32* %arrayidx99, align 4, !tbaa !6
  %149 = load i32, i32* %tmp21, align 4, !tbaa !15
  %150 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub100 = sub nsw i32 %149, %150
  %shr101 = ashr i32 %sub100, 11
  %151 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %151, i32 96
  store i32 %shr101, i32* %arrayidx102, align 4, !tbaa !6
  %152 = load i32, i32* %tmp22, align 4, !tbaa !15
  %153 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add103 = add nsw i32 %152, %153
  %shr104 = ashr i32 %add103, 11
  %154 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i32, i32* %154, i32 16
  store i32 %shr104, i32* %arrayidx105, align 4, !tbaa !6
  %155 = load i32, i32* %tmp22, align 4, !tbaa !15
  %156 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub106 = sub nsw i32 %155, %156
  %shr107 = ashr i32 %sub106, 11
  %157 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx108 = getelementptr inbounds i32, i32* %157, i32 88
  store i32 %shr107, i32* %arrayidx108, align 4, !tbaa !6
  %158 = load i32, i32* %tmp23, align 4, !tbaa !15
  %159 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add109 = add nsw i32 %158, %159
  %160 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds i32, i32* %160, i32 24
  store i32 %add109, i32* %arrayidx110, align 4, !tbaa !6
  %161 = load i32, i32* %tmp23, align 4, !tbaa !15
  %162 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub111 = sub nsw i32 %161, %162
  %163 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %163, i32 80
  store i32 %sub111, i32* %arrayidx112, align 4, !tbaa !6
  %164 = load i32, i32* %tmp24, align 4, !tbaa !15
  %165 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add113 = add nsw i32 %164, %165
  %shr114 = ashr i32 %add113, 11
  %166 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %166, i32 32
  store i32 %shr114, i32* %arrayidx115, align 4, !tbaa !6
  %167 = load i32, i32* %tmp24, align 4, !tbaa !15
  %168 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub116 = sub nsw i32 %167, %168
  %shr117 = ashr i32 %sub116, 11
  %169 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %169, i32 72
  store i32 %shr117, i32* %arrayidx118, align 4, !tbaa !6
  %170 = load i32, i32* %tmp25, align 4, !tbaa !15
  %171 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add119 = add nsw i32 %170, %171
  %shr120 = ashr i32 %add119, 11
  %172 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i32, i32* %172, i32 40
  store i32 %shr120, i32* %arrayidx121, align 4, !tbaa !6
  %173 = load i32, i32* %tmp25, align 4, !tbaa !15
  %174 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub122 = sub nsw i32 %173, %174
  %shr123 = ashr i32 %sub122, 11
  %175 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %175, i32 64
  store i32 %shr123, i32* %arrayidx124, align 4, !tbaa !6
  %176 = load i32, i32* %tmp26, align 4, !tbaa !15
  %177 = load i32, i32* %tmp16, align 4, !tbaa !15
  %add125 = add nsw i32 %176, %177
  %shr126 = ashr i32 %add125, 11
  %178 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %178, i32 48
  store i32 %shr126, i32* %arrayidx127, align 4, !tbaa !6
  %179 = load i32, i32* %tmp26, align 4, !tbaa !15
  %180 = load i32, i32* %tmp16, align 4, !tbaa !15
  %sub128 = sub nsw i32 %179, %180
  %shr129 = ashr i32 %sub128, 11
  %181 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i32, i32* %181, i32 56
  store i32 %shr129, i32* %arrayidx130, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %182 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %182, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %183 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %183, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %184 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr131 = getelementptr inbounds i32, i32* %184, i32 1
  store i32* %incdec.ptr131, i32** %quantptr, align 4, !tbaa !2
  %185 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr132 = getelementptr inbounds i32, i32* %185, i32 1
  store i32* %incdec.ptr132, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay133 = getelementptr inbounds [112 x i32], [112 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay133, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond134

for.cond134:                                      ; preds = %for.inc282, %for.end
  %186 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp135 = icmp slt i32 %186, 14
  br i1 %cmp135, label %for.body137, label %for.end284

for.body137:                                      ; preds = %for.cond134
  %187 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %188 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx138 = getelementptr inbounds i8*, i8** %187, i32 %188
  %189 = load i8*, i8** %arrayidx138, align 4, !tbaa !2
  %190 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr139 = getelementptr inbounds i8, i8* %189, i32 %190
  store i8* %add.ptr139, i8** %outptr, align 4, !tbaa !2
  %191 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds i32, i32* %191, i32 0
  %192 = load i32, i32* %arrayidx140, align 4, !tbaa !6
  %add141 = add nsw i32 %192, 16
  store i32 %add141, i32* %z1, align 4, !tbaa !15
  %193 = load i32, i32* %z1, align 4, !tbaa !15
  %shl142 = shl i32 %193, 13
  store i32 %shl142, i32* %z1, align 4, !tbaa !15
  %194 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds i32, i32* %194, i32 4
  %195 = load i32, i32* %arrayidx143, align 4, !tbaa !6
  store i32 %195, i32* %z4, align 4, !tbaa !15
  %196 = load i32, i32* %z4, align 4, !tbaa !15
  %mul144 = mul nsw i32 %196, 10438
  store i32 %mul144, i32* %z2, align 4, !tbaa !15
  %197 = load i32, i32* %z4, align 4, !tbaa !15
  %mul145 = mul nsw i32 %197, 2578
  store i32 %mul145, i32* %z3, align 4, !tbaa !15
  %198 = load i32, i32* %z4, align 4, !tbaa !15
  %mul146 = mul nsw i32 %198, 7223
  store i32 %mul146, i32* %z4, align 4, !tbaa !15
  %199 = load i32, i32* %z1, align 4, !tbaa !15
  %200 = load i32, i32* %z2, align 4, !tbaa !15
  %add147 = add nsw i32 %199, %200
  store i32 %add147, i32* %tmp10, align 4, !tbaa !15
  %201 = load i32, i32* %z1, align 4, !tbaa !15
  %202 = load i32, i32* %z3, align 4, !tbaa !15
  %add148 = add nsw i32 %201, %202
  store i32 %add148, i32* %tmp11, align 4, !tbaa !15
  %203 = load i32, i32* %z1, align 4, !tbaa !15
  %204 = load i32, i32* %z4, align 4, !tbaa !15
  %sub149 = sub nsw i32 %203, %204
  store i32 %sub149, i32* %tmp12, align 4, !tbaa !15
  %205 = load i32, i32* %z1, align 4, !tbaa !15
  %206 = load i32, i32* %z2, align 4, !tbaa !15
  %207 = load i32, i32* %z3, align 4, !tbaa !15
  %add150 = add nsw i32 %206, %207
  %208 = load i32, i32* %z4, align 4, !tbaa !15
  %sub151 = sub nsw i32 %add150, %208
  %shl152 = shl i32 %sub151, 1
  %sub153 = sub nsw i32 %205, %shl152
  store i32 %sub153, i32* %tmp23, align 4, !tbaa !15
  %209 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds i32, i32* %209, i32 2
  %210 = load i32, i32* %arrayidx154, align 4, !tbaa !6
  store i32 %210, i32* %z1, align 4, !tbaa !15
  %211 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx155 = getelementptr inbounds i32, i32* %211, i32 6
  %212 = load i32, i32* %arrayidx155, align 4, !tbaa !6
  store i32 %212, i32* %z2, align 4, !tbaa !15
  %213 = load i32, i32* %z1, align 4, !tbaa !15
  %214 = load i32, i32* %z2, align 4, !tbaa !15
  %add156 = add nsw i32 %213, %214
  %mul157 = mul nsw i32 %add156, 9058
  store i32 %mul157, i32* %z3, align 4, !tbaa !15
  %215 = load i32, i32* %z3, align 4, !tbaa !15
  %216 = load i32, i32* %z1, align 4, !tbaa !15
  %mul158 = mul nsw i32 %216, 2237
  %add159 = add nsw i32 %215, %mul158
  store i32 %add159, i32* %tmp13, align 4, !tbaa !15
  %217 = load i32, i32* %z3, align 4, !tbaa !15
  %218 = load i32, i32* %z2, align 4, !tbaa !15
  %mul160 = mul nsw i32 %218, 14084
  %sub161 = sub nsw i32 %217, %mul160
  store i32 %sub161, i32* %tmp14, align 4, !tbaa !15
  %219 = load i32, i32* %z1, align 4, !tbaa !15
  %mul162 = mul nsw i32 %219, 5027
  %220 = load i32, i32* %z2, align 4, !tbaa !15
  %mul163 = mul nsw i32 %220, 11295
  %sub164 = sub nsw i32 %mul162, %mul163
  store i32 %sub164, i32* %tmp15, align 4, !tbaa !15
  %221 = load i32, i32* %tmp10, align 4, !tbaa !15
  %222 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add165 = add nsw i32 %221, %222
  store i32 %add165, i32* %tmp20, align 4, !tbaa !15
  %223 = load i32, i32* %tmp10, align 4, !tbaa !15
  %224 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub166 = sub nsw i32 %223, %224
  store i32 %sub166, i32* %tmp26, align 4, !tbaa !15
  %225 = load i32, i32* %tmp11, align 4, !tbaa !15
  %226 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add167 = add nsw i32 %225, %226
  store i32 %add167, i32* %tmp21, align 4, !tbaa !15
  %227 = load i32, i32* %tmp11, align 4, !tbaa !15
  %228 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub168 = sub nsw i32 %227, %228
  store i32 %sub168, i32* %tmp25, align 4, !tbaa !15
  %229 = load i32, i32* %tmp12, align 4, !tbaa !15
  %230 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add169 = add nsw i32 %229, %230
  store i32 %add169, i32* %tmp22, align 4, !tbaa !15
  %231 = load i32, i32* %tmp12, align 4, !tbaa !15
  %232 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub170 = sub nsw i32 %231, %232
  store i32 %sub170, i32* %tmp24, align 4, !tbaa !15
  %233 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx171 = getelementptr inbounds i32, i32* %233, i32 1
  %234 = load i32, i32* %arrayidx171, align 4, !tbaa !6
  store i32 %234, i32* %z1, align 4, !tbaa !15
  %235 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx172 = getelementptr inbounds i32, i32* %235, i32 3
  %236 = load i32, i32* %arrayidx172, align 4, !tbaa !6
  store i32 %236, i32* %z2, align 4, !tbaa !15
  %237 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx173 = getelementptr inbounds i32, i32* %237, i32 5
  %238 = load i32, i32* %arrayidx173, align 4, !tbaa !6
  store i32 %238, i32* %z3, align 4, !tbaa !15
  %239 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i32, i32* %239, i32 7
  %240 = load i32, i32* %arrayidx174, align 4, !tbaa !6
  store i32 %240, i32* %z4, align 4, !tbaa !15
  %241 = load i32, i32* %z4, align 4, !tbaa !15
  %shl175 = shl i32 %241, 13
  store i32 %shl175, i32* %z4, align 4, !tbaa !15
  %242 = load i32, i32* %z1, align 4, !tbaa !15
  %243 = load i32, i32* %z3, align 4, !tbaa !15
  %add176 = add nsw i32 %242, %243
  store i32 %add176, i32* %tmp14, align 4, !tbaa !15
  %244 = load i32, i32* %z1, align 4, !tbaa !15
  %245 = load i32, i32* %z2, align 4, !tbaa !15
  %add177 = add nsw i32 %244, %245
  %mul178 = mul nsw i32 %add177, 10935
  store i32 %mul178, i32* %tmp11, align 4, !tbaa !15
  %246 = load i32, i32* %tmp14, align 4, !tbaa !15
  %mul179 = mul nsw i32 %246, 9810
  store i32 %mul179, i32* %tmp12, align 4, !tbaa !15
  %247 = load i32, i32* %tmp11, align 4, !tbaa !15
  %248 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add180 = add nsw i32 %247, %248
  %249 = load i32, i32* %z4, align 4, !tbaa !15
  %add181 = add nsw i32 %add180, %249
  %250 = load i32, i32* %z1, align 4, !tbaa !15
  %mul182 = mul nsw i32 %250, 9232
  %sub183 = sub nsw i32 %add181, %mul182
  store i32 %sub183, i32* %tmp10, align 4, !tbaa !15
  %251 = load i32, i32* %tmp14, align 4, !tbaa !15
  %mul184 = mul nsw i32 %251, 6164
  store i32 %mul184, i32* %tmp14, align 4, !tbaa !15
  %252 = load i32, i32* %tmp14, align 4, !tbaa !15
  %253 = load i32, i32* %z1, align 4, !tbaa !15
  %mul185 = mul nsw i32 %253, 8693
  %sub186 = sub nsw i32 %252, %mul185
  store i32 %sub186, i32* %tmp16, align 4, !tbaa !15
  %254 = load i32, i32* %z2, align 4, !tbaa !15
  %255 = load i32, i32* %z1, align 4, !tbaa !15
  %sub187 = sub nsw i32 %255, %254
  store i32 %sub187, i32* %z1, align 4, !tbaa !15
  %256 = load i32, i32* %z1, align 4, !tbaa !15
  %mul188 = mul nsw i32 %256, 3826
  %257 = load i32, i32* %z4, align 4, !tbaa !15
  %sub189 = sub nsw i32 %mul188, %257
  store i32 %sub189, i32* %tmp15, align 4, !tbaa !15
  %258 = load i32, i32* %tmp15, align 4, !tbaa !15
  %259 = load i32, i32* %tmp16, align 4, !tbaa !15
  %add190 = add nsw i32 %259, %258
  store i32 %add190, i32* %tmp16, align 4, !tbaa !15
  %260 = load i32, i32* %z2, align 4, !tbaa !15
  %261 = load i32, i32* %z3, align 4, !tbaa !15
  %add191 = add nsw i32 %260, %261
  %mul192 = mul nsw i32 %add191, -1297
  %262 = load i32, i32* %z4, align 4, !tbaa !15
  %sub193 = sub nsw i32 %mul192, %262
  store i32 %sub193, i32* %tmp13, align 4, !tbaa !15
  %263 = load i32, i32* %tmp13, align 4, !tbaa !15
  %264 = load i32, i32* %z2, align 4, !tbaa !15
  %mul194 = mul nsw i32 %264, 3474
  %sub195 = sub nsw i32 %263, %mul194
  %265 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add196 = add nsw i32 %265, %sub195
  store i32 %add196, i32* %tmp11, align 4, !tbaa !15
  %266 = load i32, i32* %tmp13, align 4, !tbaa !15
  %267 = load i32, i32* %z3, align 4, !tbaa !15
  %mul197 = mul nsw i32 %267, 19447
  %sub198 = sub nsw i32 %266, %mul197
  %268 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add199 = add nsw i32 %268, %sub198
  store i32 %add199, i32* %tmp12, align 4, !tbaa !15
  %269 = load i32, i32* %z3, align 4, !tbaa !15
  %270 = load i32, i32* %z2, align 4, !tbaa !15
  %sub200 = sub nsw i32 %269, %270
  %mul201 = mul nsw i32 %sub200, 11512
  store i32 %mul201, i32* %tmp13, align 4, !tbaa !15
  %271 = load i32, i32* %tmp13, align 4, !tbaa !15
  %272 = load i32, i32* %z4, align 4, !tbaa !15
  %add202 = add nsw i32 %271, %272
  %273 = load i32, i32* %z3, align 4, !tbaa !15
  %mul203 = mul nsw i32 %273, 13850
  %sub204 = sub nsw i32 %add202, %mul203
  %274 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add205 = add nsw i32 %274, %sub204
  store i32 %add205, i32* %tmp14, align 4, !tbaa !15
  %275 = load i32, i32* %tmp13, align 4, !tbaa !15
  %276 = load i32, i32* %z2, align 4, !tbaa !15
  %mul206 = mul nsw i32 %276, 5529
  %add207 = add nsw i32 %275, %mul206
  %277 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add208 = add nsw i32 %277, %add207
  store i32 %add208, i32* %tmp15, align 4, !tbaa !15
  %278 = load i32, i32* %z1, align 4, !tbaa !15
  %279 = load i32, i32* %z3, align 4, !tbaa !15
  %sub209 = sub nsw i32 %278, %279
  %shl210 = shl i32 %sub209, 13
  %280 = load i32, i32* %z4, align 4, !tbaa !15
  %add211 = add nsw i32 %shl210, %280
  store i32 %add211, i32* %tmp13, align 4, !tbaa !15
  %281 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %282 = load i32, i32* %tmp20, align 4, !tbaa !15
  %283 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add212 = add nsw i32 %282, %283
  %shr213 = ashr i32 %add212, 18
  %and = and i32 %shr213, 1023
  %arrayidx214 = getelementptr inbounds i8, i8* %281, i32 %and
  %284 = load i8, i8* %arrayidx214, align 1, !tbaa !17
  %285 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx215 = getelementptr inbounds i8, i8* %285, i32 0
  store i8 %284, i8* %arrayidx215, align 1, !tbaa !17
  %286 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %287 = load i32, i32* %tmp20, align 4, !tbaa !15
  %288 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub216 = sub nsw i32 %287, %288
  %shr217 = ashr i32 %sub216, 18
  %and218 = and i32 %shr217, 1023
  %arrayidx219 = getelementptr inbounds i8, i8* %286, i32 %and218
  %289 = load i8, i8* %arrayidx219, align 1, !tbaa !17
  %290 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx220 = getelementptr inbounds i8, i8* %290, i32 13
  store i8 %289, i8* %arrayidx220, align 1, !tbaa !17
  %291 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %292 = load i32, i32* %tmp21, align 4, !tbaa !15
  %293 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add221 = add nsw i32 %292, %293
  %shr222 = ashr i32 %add221, 18
  %and223 = and i32 %shr222, 1023
  %arrayidx224 = getelementptr inbounds i8, i8* %291, i32 %and223
  %294 = load i8, i8* %arrayidx224, align 1, !tbaa !17
  %295 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx225 = getelementptr inbounds i8, i8* %295, i32 1
  store i8 %294, i8* %arrayidx225, align 1, !tbaa !17
  %296 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %297 = load i32, i32* %tmp21, align 4, !tbaa !15
  %298 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub226 = sub nsw i32 %297, %298
  %shr227 = ashr i32 %sub226, 18
  %and228 = and i32 %shr227, 1023
  %arrayidx229 = getelementptr inbounds i8, i8* %296, i32 %and228
  %299 = load i8, i8* %arrayidx229, align 1, !tbaa !17
  %300 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx230 = getelementptr inbounds i8, i8* %300, i32 12
  store i8 %299, i8* %arrayidx230, align 1, !tbaa !17
  %301 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %302 = load i32, i32* %tmp22, align 4, !tbaa !15
  %303 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add231 = add nsw i32 %302, %303
  %shr232 = ashr i32 %add231, 18
  %and233 = and i32 %shr232, 1023
  %arrayidx234 = getelementptr inbounds i8, i8* %301, i32 %and233
  %304 = load i8, i8* %arrayidx234, align 1, !tbaa !17
  %305 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx235 = getelementptr inbounds i8, i8* %305, i32 2
  store i8 %304, i8* %arrayidx235, align 1, !tbaa !17
  %306 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %307 = load i32, i32* %tmp22, align 4, !tbaa !15
  %308 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub236 = sub nsw i32 %307, %308
  %shr237 = ashr i32 %sub236, 18
  %and238 = and i32 %shr237, 1023
  %arrayidx239 = getelementptr inbounds i8, i8* %306, i32 %and238
  %309 = load i8, i8* %arrayidx239, align 1, !tbaa !17
  %310 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx240 = getelementptr inbounds i8, i8* %310, i32 11
  store i8 %309, i8* %arrayidx240, align 1, !tbaa !17
  %311 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %312 = load i32, i32* %tmp23, align 4, !tbaa !15
  %313 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add241 = add nsw i32 %312, %313
  %shr242 = ashr i32 %add241, 18
  %and243 = and i32 %shr242, 1023
  %arrayidx244 = getelementptr inbounds i8, i8* %311, i32 %and243
  %314 = load i8, i8* %arrayidx244, align 1, !tbaa !17
  %315 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx245 = getelementptr inbounds i8, i8* %315, i32 3
  store i8 %314, i8* %arrayidx245, align 1, !tbaa !17
  %316 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %317 = load i32, i32* %tmp23, align 4, !tbaa !15
  %318 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub246 = sub nsw i32 %317, %318
  %shr247 = ashr i32 %sub246, 18
  %and248 = and i32 %shr247, 1023
  %arrayidx249 = getelementptr inbounds i8, i8* %316, i32 %and248
  %319 = load i8, i8* %arrayidx249, align 1, !tbaa !17
  %320 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx250 = getelementptr inbounds i8, i8* %320, i32 10
  store i8 %319, i8* %arrayidx250, align 1, !tbaa !17
  %321 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %322 = load i32, i32* %tmp24, align 4, !tbaa !15
  %323 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add251 = add nsw i32 %322, %323
  %shr252 = ashr i32 %add251, 18
  %and253 = and i32 %shr252, 1023
  %arrayidx254 = getelementptr inbounds i8, i8* %321, i32 %and253
  %324 = load i8, i8* %arrayidx254, align 1, !tbaa !17
  %325 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx255 = getelementptr inbounds i8, i8* %325, i32 4
  store i8 %324, i8* %arrayidx255, align 1, !tbaa !17
  %326 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %327 = load i32, i32* %tmp24, align 4, !tbaa !15
  %328 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub256 = sub nsw i32 %327, %328
  %shr257 = ashr i32 %sub256, 18
  %and258 = and i32 %shr257, 1023
  %arrayidx259 = getelementptr inbounds i8, i8* %326, i32 %and258
  %329 = load i8, i8* %arrayidx259, align 1, !tbaa !17
  %330 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx260 = getelementptr inbounds i8, i8* %330, i32 9
  store i8 %329, i8* %arrayidx260, align 1, !tbaa !17
  %331 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %332 = load i32, i32* %tmp25, align 4, !tbaa !15
  %333 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add261 = add nsw i32 %332, %333
  %shr262 = ashr i32 %add261, 18
  %and263 = and i32 %shr262, 1023
  %arrayidx264 = getelementptr inbounds i8, i8* %331, i32 %and263
  %334 = load i8, i8* %arrayidx264, align 1, !tbaa !17
  %335 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx265 = getelementptr inbounds i8, i8* %335, i32 5
  store i8 %334, i8* %arrayidx265, align 1, !tbaa !17
  %336 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %337 = load i32, i32* %tmp25, align 4, !tbaa !15
  %338 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub266 = sub nsw i32 %337, %338
  %shr267 = ashr i32 %sub266, 18
  %and268 = and i32 %shr267, 1023
  %arrayidx269 = getelementptr inbounds i8, i8* %336, i32 %and268
  %339 = load i8, i8* %arrayidx269, align 1, !tbaa !17
  %340 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx270 = getelementptr inbounds i8, i8* %340, i32 8
  store i8 %339, i8* %arrayidx270, align 1, !tbaa !17
  %341 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %342 = load i32, i32* %tmp26, align 4, !tbaa !15
  %343 = load i32, i32* %tmp16, align 4, !tbaa !15
  %add271 = add nsw i32 %342, %343
  %shr272 = ashr i32 %add271, 18
  %and273 = and i32 %shr272, 1023
  %arrayidx274 = getelementptr inbounds i8, i8* %341, i32 %and273
  %344 = load i8, i8* %arrayidx274, align 1, !tbaa !17
  %345 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx275 = getelementptr inbounds i8, i8* %345, i32 6
  store i8 %344, i8* %arrayidx275, align 1, !tbaa !17
  %346 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %347 = load i32, i32* %tmp26, align 4, !tbaa !15
  %348 = load i32, i32* %tmp16, align 4, !tbaa !15
  %sub276 = sub nsw i32 %347, %348
  %shr277 = ashr i32 %sub276, 18
  %and278 = and i32 %shr277, 1023
  %arrayidx279 = getelementptr inbounds i8, i8* %346, i32 %and278
  %349 = load i8, i8* %arrayidx279, align 1, !tbaa !17
  %350 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx280 = getelementptr inbounds i8, i8* %350, i32 7
  store i8 %349, i8* %arrayidx280, align 1, !tbaa !17
  %351 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr281 = getelementptr inbounds i32, i32* %351, i32 8
  store i32* %add.ptr281, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc282

for.inc282:                                       ; preds = %for.body137
  %352 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc283 = add nsw i32 %352, 1
  store i32 %inc283, i32* %ctr, align 4, !tbaa !6
  br label %for.cond134

for.end284:                                       ; preds = %for.cond134
  %353 = bitcast [112 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 448, i8* %353) #2
  %354 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %354) #2
  %355 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %355) #2
  %356 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %356) #2
  %357 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %357) #2
  %358 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %358) #2
  %359 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %359) #2
  %360 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %360) #2
  %361 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %361) #2
  %362 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %362) #2
  %363 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %363) #2
  %364 = bitcast i32* %tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %364) #2
  %365 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %365) #2
  %366 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %366) #2
  %367 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %367) #2
  %368 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %368) #2
  %369 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %369) #2
  %370 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %370) #2
  %371 = bitcast i32* %tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %371) #2
  %372 = bitcast i32* %tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %372) #2
  %373 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %373) #2
  %374 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %374) #2
  %375 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %375) #2
  %376 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %376) #2
  %377 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %377) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_15x15(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %tmp14 = alloca i32, align 4
  %tmp15 = alloca i32, align 4
  %tmp16 = alloca i32, align 4
  %tmp20 = alloca i32, align 4
  %tmp21 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %tmp23 = alloca i32, align 4
  %tmp24 = alloca i32, align 4
  %tmp25 = alloca i32, align 4
  %tmp26 = alloca i32, align 4
  %tmp27 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [120 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  %22 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #2
  %23 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #2
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 65
  %25 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %25, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %26 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #2
  %27 = bitcast [120 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 480, i8* %27) #2
  %28 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %28, i16** %inptr, align 4, !tbaa !2
  %29 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %29, i32 0, i32 20
  %30 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %31 = bitcast i8* %30 to i32*
  store i32* %31, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [120 x i32], [120 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %32 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %32, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %33 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %33, i32 0
  %34 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %34 to i32
  %35 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %35, i32 0
  %36 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %36
  store i32 %mul, i32* %z1, align 4, !tbaa !15
  %37 = load i32, i32* %z1, align 4, !tbaa !15
  %shl = shl i32 %37, 13
  store i32 %shl, i32* %z1, align 4, !tbaa !15
  %38 = load i32, i32* %z1, align 4, !tbaa !15
  %add = add nsw i32 %38, 1024
  store i32 %add, i32* %z1, align 4, !tbaa !15
  %39 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %39, i32 16
  %40 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %40 to i32
  %41 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %41, i32 16
  %42 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %42
  store i32 %mul5, i32* %z2, align 4, !tbaa !15
  %43 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %43, i32 32
  %44 = load i16, i16* %arrayidx6, align 2, !tbaa !14
  %conv7 = sext i16 %44 to i32
  %45 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %45, i32 32
  %46 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %mul9 = mul nsw i32 %conv7, %46
  store i32 %mul9, i32* %z3, align 4, !tbaa !15
  %47 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %47, i32 48
  %48 = load i16, i16* %arrayidx10, align 2, !tbaa !14
  %conv11 = sext i16 %48 to i32
  %49 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %49, i32 48
  %50 = load i32, i32* %arrayidx12, align 4, !tbaa !6
  %mul13 = mul nsw i32 %conv11, %50
  store i32 %mul13, i32* %z4, align 4, !tbaa !15
  %51 = load i32, i32* %z4, align 4, !tbaa !15
  %mul14 = mul nsw i32 %51, 3580
  store i32 %mul14, i32* %tmp10, align 4, !tbaa !15
  %52 = load i32, i32* %z4, align 4, !tbaa !15
  %mul15 = mul nsw i32 %52, 9373
  store i32 %mul15, i32* %tmp11, align 4, !tbaa !15
  %53 = load i32, i32* %z1, align 4, !tbaa !15
  %54 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub = sub nsw i32 %53, %54
  store i32 %sub, i32* %tmp12, align 4, !tbaa !15
  %55 = load i32, i32* %z1, align 4, !tbaa !15
  %56 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add16 = add nsw i32 %55, %56
  store i32 %add16, i32* %tmp13, align 4, !tbaa !15
  %57 = load i32, i32* %tmp11, align 4, !tbaa !15
  %58 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub17 = sub nsw i32 %57, %58
  %shl18 = shl i32 %sub17, 1
  %59 = load i32, i32* %z1, align 4, !tbaa !15
  %sub19 = sub nsw i32 %59, %shl18
  store i32 %sub19, i32* %z1, align 4, !tbaa !15
  %60 = load i32, i32* %z2, align 4, !tbaa !15
  %61 = load i32, i32* %z3, align 4, !tbaa !15
  %sub20 = sub nsw i32 %60, %61
  store i32 %sub20, i32* %z4, align 4, !tbaa !15
  %62 = load i32, i32* %z2, align 4, !tbaa !15
  %63 = load i32, i32* %z3, align 4, !tbaa !15
  %add21 = add nsw i32 %63, %62
  store i32 %add21, i32* %z3, align 4, !tbaa !15
  %64 = load i32, i32* %z3, align 4, !tbaa !15
  %mul22 = mul nsw i32 %64, 10958
  store i32 %mul22, i32* %tmp10, align 4, !tbaa !15
  %65 = load i32, i32* %z4, align 4, !tbaa !15
  %mul23 = mul nsw i32 %65, 374
  store i32 %mul23, i32* %tmp11, align 4, !tbaa !15
  %66 = load i32, i32* %z2, align 4, !tbaa !15
  %mul24 = mul nsw i32 %66, 11795
  store i32 %mul24, i32* %z2, align 4, !tbaa !15
  %67 = load i32, i32* %tmp13, align 4, !tbaa !15
  %68 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add25 = add nsw i32 %67, %68
  %69 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add26 = add nsw i32 %add25, %69
  store i32 %add26, i32* %tmp20, align 4, !tbaa !15
  %70 = load i32, i32* %tmp12, align 4, !tbaa !15
  %71 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub27 = sub nsw i32 %70, %71
  %72 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add28 = add nsw i32 %sub27, %72
  %73 = load i32, i32* %z2, align 4, !tbaa !15
  %add29 = add nsw i32 %add28, %73
  store i32 %add29, i32* %tmp23, align 4, !tbaa !15
  %74 = load i32, i32* %z3, align 4, !tbaa !15
  %mul30 = mul nsw i32 %74, 4482
  store i32 %mul30, i32* %tmp10, align 4, !tbaa !15
  %75 = load i32, i32* %z4, align 4, !tbaa !15
  %mul31 = mul nsw i32 %75, 3271
  store i32 %mul31, i32* %tmp11, align 4, !tbaa !15
  %76 = load i32, i32* %tmp13, align 4, !tbaa !15
  %77 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub32 = sub nsw i32 %76, %77
  %78 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub33 = sub nsw i32 %sub32, %78
  store i32 %sub33, i32* %tmp25, align 4, !tbaa !15
  %79 = load i32, i32* %tmp12, align 4, !tbaa !15
  %80 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add34 = add nsw i32 %79, %80
  %81 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub35 = sub nsw i32 %add34, %81
  %82 = load i32, i32* %z2, align 4, !tbaa !15
  %sub36 = sub nsw i32 %sub35, %82
  store i32 %sub36, i32* %tmp26, align 4, !tbaa !15
  %83 = load i32, i32* %z3, align 4, !tbaa !15
  %mul37 = mul nsw i32 %83, 6476
  store i32 %mul37, i32* %tmp10, align 4, !tbaa !15
  %84 = load i32, i32* %z4, align 4, !tbaa !15
  %mul38 = mul nsw i32 %84, 2896
  store i32 %mul38, i32* %tmp11, align 4, !tbaa !15
  %85 = load i32, i32* %tmp12, align 4, !tbaa !15
  %86 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add39 = add nsw i32 %85, %86
  %87 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add40 = add nsw i32 %add39, %87
  store i32 %add40, i32* %tmp21, align 4, !tbaa !15
  %88 = load i32, i32* %tmp13, align 4, !tbaa !15
  %89 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub41 = sub nsw i32 %88, %89
  %90 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add42 = add nsw i32 %sub41, %90
  store i32 %add42, i32* %tmp24, align 4, !tbaa !15
  %91 = load i32, i32* %tmp11, align 4, !tbaa !15
  %92 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add43 = add nsw i32 %92, %91
  store i32 %add43, i32* %tmp11, align 4, !tbaa !15
  %93 = load i32, i32* %z1, align 4, !tbaa !15
  %94 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add44 = add nsw i32 %93, %94
  store i32 %add44, i32* %tmp22, align 4, !tbaa !15
  %95 = load i32, i32* %z1, align 4, !tbaa !15
  %96 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub45 = sub nsw i32 %95, %96
  %97 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub46 = sub nsw i32 %sub45, %97
  store i32 %sub46, i32* %tmp27, align 4, !tbaa !15
  %98 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i16, i16* %98, i32 8
  %99 = load i16, i16* %arrayidx47, align 2, !tbaa !14
  %conv48 = sext i16 %99 to i32
  %100 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i32, i32* %100, i32 8
  %101 = load i32, i32* %arrayidx49, align 4, !tbaa !6
  %mul50 = mul nsw i32 %conv48, %101
  store i32 %mul50, i32* %z1, align 4, !tbaa !15
  %102 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i16, i16* %102, i32 24
  %103 = load i16, i16* %arrayidx51, align 2, !tbaa !14
  %conv52 = sext i16 %103 to i32
  %104 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i32, i32* %104, i32 24
  %105 = load i32, i32* %arrayidx53, align 4, !tbaa !6
  %mul54 = mul nsw i32 %conv52, %105
  store i32 %mul54, i32* %z2, align 4, !tbaa !15
  %106 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i16, i16* %106, i32 40
  %107 = load i16, i16* %arrayidx55, align 2, !tbaa !14
  %conv56 = sext i16 %107 to i32
  %108 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds i32, i32* %108, i32 40
  %109 = load i32, i32* %arrayidx57, align 4, !tbaa !6
  %mul58 = mul nsw i32 %conv56, %109
  store i32 %mul58, i32* %z4, align 4, !tbaa !15
  %110 = load i32, i32* %z4, align 4, !tbaa !15
  %mul59 = mul nsw i32 %110, 10033
  store i32 %mul59, i32* %z3, align 4, !tbaa !15
  %111 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds i16, i16* %111, i32 56
  %112 = load i16, i16* %arrayidx60, align 2, !tbaa !14
  %conv61 = sext i16 %112 to i32
  %113 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i32, i32* %113, i32 56
  %114 = load i32, i32* %arrayidx62, align 4, !tbaa !6
  %mul63 = mul nsw i32 %conv61, %114
  store i32 %mul63, i32* %z4, align 4, !tbaa !15
  %115 = load i32, i32* %z2, align 4, !tbaa !15
  %116 = load i32, i32* %z4, align 4, !tbaa !15
  %sub64 = sub nsw i32 %115, %116
  store i32 %sub64, i32* %tmp13, align 4, !tbaa !15
  %117 = load i32, i32* %z1, align 4, !tbaa !15
  %118 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add65 = add nsw i32 %117, %118
  %mul66 = mul nsw i32 %add65, 6810
  store i32 %mul66, i32* %tmp15, align 4, !tbaa !15
  %119 = load i32, i32* %tmp15, align 4, !tbaa !15
  %120 = load i32, i32* %z1, align 4, !tbaa !15
  %mul67 = mul nsw i32 %120, 4209
  %add68 = add nsw i32 %119, %mul67
  store i32 %add68, i32* %tmp11, align 4, !tbaa !15
  %121 = load i32, i32* %tmp15, align 4, !tbaa !15
  %122 = load i32, i32* %tmp13, align 4, !tbaa !15
  %mul69 = mul nsw i32 %122, 17828
  %sub70 = sub nsw i32 %121, %mul69
  store i32 %sub70, i32* %tmp14, align 4, !tbaa !15
  %123 = load i32, i32* %z2, align 4, !tbaa !15
  %mul71 = mul nsw i32 %123, -6810
  store i32 %mul71, i32* %tmp13, align 4, !tbaa !15
  %124 = load i32, i32* %z2, align 4, !tbaa !15
  %mul72 = mul nsw i32 %124, -11018
  store i32 %mul72, i32* %tmp15, align 4, !tbaa !15
  %125 = load i32, i32* %z1, align 4, !tbaa !15
  %126 = load i32, i32* %z4, align 4, !tbaa !15
  %sub73 = sub nsw i32 %125, %126
  store i32 %sub73, i32* %z2, align 4, !tbaa !15
  %127 = load i32, i32* %z3, align 4, !tbaa !15
  %128 = load i32, i32* %z2, align 4, !tbaa !15
  %mul74 = mul nsw i32 %128, 11522
  %add75 = add nsw i32 %127, %mul74
  store i32 %add75, i32* %tmp12, align 4, !tbaa !15
  %129 = load i32, i32* %tmp12, align 4, !tbaa !15
  %130 = load i32, i32* %z4, align 4, !tbaa !15
  %mul76 = mul nsw i32 %130, 20131
  %add77 = add nsw i32 %129, %mul76
  %131 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub78 = sub nsw i32 %add77, %131
  store i32 %sub78, i32* %tmp10, align 4, !tbaa !15
  %132 = load i32, i32* %tmp12, align 4, !tbaa !15
  %133 = load i32, i32* %z1, align 4, !tbaa !15
  %mul79 = mul nsw i32 %133, 9113
  %sub80 = sub nsw i32 %132, %mul79
  %134 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add81 = add nsw i32 %sub80, %134
  store i32 %add81, i32* %tmp16, align 4, !tbaa !15
  %135 = load i32, i32* %z2, align 4, !tbaa !15
  %mul82 = mul nsw i32 %135, 10033
  %136 = load i32, i32* %z3, align 4, !tbaa !15
  %sub83 = sub nsw i32 %mul82, %136
  store i32 %sub83, i32* %tmp12, align 4, !tbaa !15
  %137 = load i32, i32* %z1, align 4, !tbaa !15
  %138 = load i32, i32* %z4, align 4, !tbaa !15
  %add84 = add nsw i32 %137, %138
  %mul85 = mul nsw i32 %add84, 4712
  store i32 %mul85, i32* %z2, align 4, !tbaa !15
  %139 = load i32, i32* %z2, align 4, !tbaa !15
  %140 = load i32, i32* %z1, align 4, !tbaa !15
  %mul86 = mul nsw i32 %140, 3897
  %add87 = add nsw i32 %139, %mul86
  %141 = load i32, i32* %z3, align 4, !tbaa !15
  %sub88 = sub nsw i32 %add87, %141
  %142 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add89 = add nsw i32 %142, %sub88
  store i32 %add89, i32* %tmp13, align 4, !tbaa !15
  %143 = load i32, i32* %z2, align 4, !tbaa !15
  %144 = load i32, i32* %z4, align 4, !tbaa !15
  %mul90 = mul nsw i32 %144, 7121
  %sub91 = sub nsw i32 %143, %mul90
  %145 = load i32, i32* %z3, align 4, !tbaa !15
  %add92 = add nsw i32 %sub91, %145
  %146 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add93 = add nsw i32 %146, %add92
  store i32 %add93, i32* %tmp15, align 4, !tbaa !15
  %147 = load i32, i32* %tmp20, align 4, !tbaa !15
  %148 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add94 = add nsw i32 %147, %148
  %shr = ashr i32 %add94, 11
  %149 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i32, i32* %149, i32 0
  store i32 %shr, i32* %arrayidx95, align 4, !tbaa !6
  %150 = load i32, i32* %tmp20, align 4, !tbaa !15
  %151 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub96 = sub nsw i32 %150, %151
  %shr97 = ashr i32 %sub96, 11
  %152 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i32, i32* %152, i32 112
  store i32 %shr97, i32* %arrayidx98, align 4, !tbaa !6
  %153 = load i32, i32* %tmp21, align 4, !tbaa !15
  %154 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add99 = add nsw i32 %153, %154
  %shr100 = ashr i32 %add99, 11
  %155 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i32, i32* %155, i32 8
  store i32 %shr100, i32* %arrayidx101, align 4, !tbaa !6
  %156 = load i32, i32* %tmp21, align 4, !tbaa !15
  %157 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub102 = sub nsw i32 %156, %157
  %shr103 = ashr i32 %sub102, 11
  %158 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx104 = getelementptr inbounds i32, i32* %158, i32 104
  store i32 %shr103, i32* %arrayidx104, align 4, !tbaa !6
  %159 = load i32, i32* %tmp22, align 4, !tbaa !15
  %160 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add105 = add nsw i32 %159, %160
  %shr106 = ashr i32 %add105, 11
  %161 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx107 = getelementptr inbounds i32, i32* %161, i32 16
  store i32 %shr106, i32* %arrayidx107, align 4, !tbaa !6
  %162 = load i32, i32* %tmp22, align 4, !tbaa !15
  %163 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub108 = sub nsw i32 %162, %163
  %shr109 = ashr i32 %sub108, 11
  %164 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds i32, i32* %164, i32 96
  store i32 %shr109, i32* %arrayidx110, align 4, !tbaa !6
  %165 = load i32, i32* %tmp23, align 4, !tbaa !15
  %166 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add111 = add nsw i32 %165, %166
  %shr112 = ashr i32 %add111, 11
  %167 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds i32, i32* %167, i32 24
  store i32 %shr112, i32* %arrayidx113, align 4, !tbaa !6
  %168 = load i32, i32* %tmp23, align 4, !tbaa !15
  %169 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub114 = sub nsw i32 %168, %169
  %shr115 = ashr i32 %sub114, 11
  %170 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds i32, i32* %170, i32 88
  store i32 %shr115, i32* %arrayidx116, align 4, !tbaa !6
  %171 = load i32, i32* %tmp24, align 4, !tbaa !15
  %172 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add117 = add nsw i32 %171, %172
  %shr118 = ashr i32 %add117, 11
  %173 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds i32, i32* %173, i32 32
  store i32 %shr118, i32* %arrayidx119, align 4, !tbaa !6
  %174 = load i32, i32* %tmp24, align 4, !tbaa !15
  %175 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub120 = sub nsw i32 %174, %175
  %shr121 = ashr i32 %sub120, 11
  %176 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %176, i32 80
  store i32 %shr121, i32* %arrayidx122, align 4, !tbaa !6
  %177 = load i32, i32* %tmp25, align 4, !tbaa !15
  %178 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add123 = add nsw i32 %177, %178
  %shr124 = ashr i32 %add123, 11
  %179 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds i32, i32* %179, i32 40
  store i32 %shr124, i32* %arrayidx125, align 4, !tbaa !6
  %180 = load i32, i32* %tmp25, align 4, !tbaa !15
  %181 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub126 = sub nsw i32 %180, %181
  %shr127 = ashr i32 %sub126, 11
  %182 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx128 = getelementptr inbounds i32, i32* %182, i32 72
  store i32 %shr127, i32* %arrayidx128, align 4, !tbaa !6
  %183 = load i32, i32* %tmp26, align 4, !tbaa !15
  %184 = load i32, i32* %tmp16, align 4, !tbaa !15
  %add129 = add nsw i32 %183, %184
  %shr130 = ashr i32 %add129, 11
  %185 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx131 = getelementptr inbounds i32, i32* %185, i32 48
  store i32 %shr130, i32* %arrayidx131, align 4, !tbaa !6
  %186 = load i32, i32* %tmp26, align 4, !tbaa !15
  %187 = load i32, i32* %tmp16, align 4, !tbaa !15
  %sub132 = sub nsw i32 %186, %187
  %shr133 = ashr i32 %sub132, 11
  %188 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx134 = getelementptr inbounds i32, i32* %188, i32 64
  store i32 %shr133, i32* %arrayidx134, align 4, !tbaa !6
  %189 = load i32, i32* %tmp27, align 4, !tbaa !15
  %shr135 = ashr i32 %189, 11
  %190 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %190, i32 56
  store i32 %shr135, i32* %arrayidx136, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %191 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %191, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %192 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %192, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %193 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr137 = getelementptr inbounds i32, i32* %193, i32 1
  store i32* %incdec.ptr137, i32** %quantptr, align 4, !tbaa !2
  %194 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr138 = getelementptr inbounds i32, i32* %194, i32 1
  store i32* %incdec.ptr138, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay139 = getelementptr inbounds [120 x i32], [120 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay139, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond140

for.cond140:                                      ; preds = %for.inc295, %for.end
  %195 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp141 = icmp slt i32 %195, 15
  br i1 %cmp141, label %for.body143, label %for.end297

for.body143:                                      ; preds = %for.cond140
  %196 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %197 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx144 = getelementptr inbounds i8*, i8** %196, i32 %197
  %198 = load i8*, i8** %arrayidx144, align 4, !tbaa !2
  %199 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr145 = getelementptr inbounds i8, i8* %198, i32 %199
  store i8* %add.ptr145, i8** %outptr, align 4, !tbaa !2
  %200 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i32, i32* %200, i32 0
  %201 = load i32, i32* %arrayidx146, align 4, !tbaa !6
  %add147 = add nsw i32 %201, 16
  store i32 %add147, i32* %z1, align 4, !tbaa !15
  %202 = load i32, i32* %z1, align 4, !tbaa !15
  %shl148 = shl i32 %202, 13
  store i32 %shl148, i32* %z1, align 4, !tbaa !15
  %203 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx149 = getelementptr inbounds i32, i32* %203, i32 2
  %204 = load i32, i32* %arrayidx149, align 4, !tbaa !6
  store i32 %204, i32* %z2, align 4, !tbaa !15
  %205 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx150 = getelementptr inbounds i32, i32* %205, i32 4
  %206 = load i32, i32* %arrayidx150, align 4, !tbaa !6
  store i32 %206, i32* %z3, align 4, !tbaa !15
  %207 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %207, i32 6
  %208 = load i32, i32* %arrayidx151, align 4, !tbaa !6
  store i32 %208, i32* %z4, align 4, !tbaa !15
  %209 = load i32, i32* %z4, align 4, !tbaa !15
  %mul152 = mul nsw i32 %209, 3580
  store i32 %mul152, i32* %tmp10, align 4, !tbaa !15
  %210 = load i32, i32* %z4, align 4, !tbaa !15
  %mul153 = mul nsw i32 %210, 9373
  store i32 %mul153, i32* %tmp11, align 4, !tbaa !15
  %211 = load i32, i32* %z1, align 4, !tbaa !15
  %212 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub154 = sub nsw i32 %211, %212
  store i32 %sub154, i32* %tmp12, align 4, !tbaa !15
  %213 = load i32, i32* %z1, align 4, !tbaa !15
  %214 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add155 = add nsw i32 %213, %214
  store i32 %add155, i32* %tmp13, align 4, !tbaa !15
  %215 = load i32, i32* %tmp11, align 4, !tbaa !15
  %216 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub156 = sub nsw i32 %215, %216
  %shl157 = shl i32 %sub156, 1
  %217 = load i32, i32* %z1, align 4, !tbaa !15
  %sub158 = sub nsw i32 %217, %shl157
  store i32 %sub158, i32* %z1, align 4, !tbaa !15
  %218 = load i32, i32* %z2, align 4, !tbaa !15
  %219 = load i32, i32* %z3, align 4, !tbaa !15
  %sub159 = sub nsw i32 %218, %219
  store i32 %sub159, i32* %z4, align 4, !tbaa !15
  %220 = load i32, i32* %z2, align 4, !tbaa !15
  %221 = load i32, i32* %z3, align 4, !tbaa !15
  %add160 = add nsw i32 %221, %220
  store i32 %add160, i32* %z3, align 4, !tbaa !15
  %222 = load i32, i32* %z3, align 4, !tbaa !15
  %mul161 = mul nsw i32 %222, 10958
  store i32 %mul161, i32* %tmp10, align 4, !tbaa !15
  %223 = load i32, i32* %z4, align 4, !tbaa !15
  %mul162 = mul nsw i32 %223, 374
  store i32 %mul162, i32* %tmp11, align 4, !tbaa !15
  %224 = load i32, i32* %z2, align 4, !tbaa !15
  %mul163 = mul nsw i32 %224, 11795
  store i32 %mul163, i32* %z2, align 4, !tbaa !15
  %225 = load i32, i32* %tmp13, align 4, !tbaa !15
  %226 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add164 = add nsw i32 %225, %226
  %227 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add165 = add nsw i32 %add164, %227
  store i32 %add165, i32* %tmp20, align 4, !tbaa !15
  %228 = load i32, i32* %tmp12, align 4, !tbaa !15
  %229 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub166 = sub nsw i32 %228, %229
  %230 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add167 = add nsw i32 %sub166, %230
  %231 = load i32, i32* %z2, align 4, !tbaa !15
  %add168 = add nsw i32 %add167, %231
  store i32 %add168, i32* %tmp23, align 4, !tbaa !15
  %232 = load i32, i32* %z3, align 4, !tbaa !15
  %mul169 = mul nsw i32 %232, 4482
  store i32 %mul169, i32* %tmp10, align 4, !tbaa !15
  %233 = load i32, i32* %z4, align 4, !tbaa !15
  %mul170 = mul nsw i32 %233, 3271
  store i32 %mul170, i32* %tmp11, align 4, !tbaa !15
  %234 = load i32, i32* %tmp13, align 4, !tbaa !15
  %235 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub171 = sub nsw i32 %234, %235
  %236 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub172 = sub nsw i32 %sub171, %236
  store i32 %sub172, i32* %tmp25, align 4, !tbaa !15
  %237 = load i32, i32* %tmp12, align 4, !tbaa !15
  %238 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add173 = add nsw i32 %237, %238
  %239 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub174 = sub nsw i32 %add173, %239
  %240 = load i32, i32* %z2, align 4, !tbaa !15
  %sub175 = sub nsw i32 %sub174, %240
  store i32 %sub175, i32* %tmp26, align 4, !tbaa !15
  %241 = load i32, i32* %z3, align 4, !tbaa !15
  %mul176 = mul nsw i32 %241, 6476
  store i32 %mul176, i32* %tmp10, align 4, !tbaa !15
  %242 = load i32, i32* %z4, align 4, !tbaa !15
  %mul177 = mul nsw i32 %242, 2896
  store i32 %mul177, i32* %tmp11, align 4, !tbaa !15
  %243 = load i32, i32* %tmp12, align 4, !tbaa !15
  %244 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add178 = add nsw i32 %243, %244
  %245 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add179 = add nsw i32 %add178, %245
  store i32 %add179, i32* %tmp21, align 4, !tbaa !15
  %246 = load i32, i32* %tmp13, align 4, !tbaa !15
  %247 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub180 = sub nsw i32 %246, %247
  %248 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add181 = add nsw i32 %sub180, %248
  store i32 %add181, i32* %tmp24, align 4, !tbaa !15
  %249 = load i32, i32* %tmp11, align 4, !tbaa !15
  %250 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add182 = add nsw i32 %250, %249
  store i32 %add182, i32* %tmp11, align 4, !tbaa !15
  %251 = load i32, i32* %z1, align 4, !tbaa !15
  %252 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add183 = add nsw i32 %251, %252
  store i32 %add183, i32* %tmp22, align 4, !tbaa !15
  %253 = load i32, i32* %z1, align 4, !tbaa !15
  %254 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub184 = sub nsw i32 %253, %254
  %255 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub185 = sub nsw i32 %sub184, %255
  store i32 %sub185, i32* %tmp27, align 4, !tbaa !15
  %256 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx186 = getelementptr inbounds i32, i32* %256, i32 1
  %257 = load i32, i32* %arrayidx186, align 4, !tbaa !6
  store i32 %257, i32* %z1, align 4, !tbaa !15
  %258 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx187 = getelementptr inbounds i32, i32* %258, i32 3
  %259 = load i32, i32* %arrayidx187, align 4, !tbaa !6
  store i32 %259, i32* %z2, align 4, !tbaa !15
  %260 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx188 = getelementptr inbounds i32, i32* %260, i32 5
  %261 = load i32, i32* %arrayidx188, align 4, !tbaa !6
  store i32 %261, i32* %z4, align 4, !tbaa !15
  %262 = load i32, i32* %z4, align 4, !tbaa !15
  %mul189 = mul nsw i32 %262, 10033
  store i32 %mul189, i32* %z3, align 4, !tbaa !15
  %263 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx190 = getelementptr inbounds i32, i32* %263, i32 7
  %264 = load i32, i32* %arrayidx190, align 4, !tbaa !6
  store i32 %264, i32* %z4, align 4, !tbaa !15
  %265 = load i32, i32* %z2, align 4, !tbaa !15
  %266 = load i32, i32* %z4, align 4, !tbaa !15
  %sub191 = sub nsw i32 %265, %266
  store i32 %sub191, i32* %tmp13, align 4, !tbaa !15
  %267 = load i32, i32* %z1, align 4, !tbaa !15
  %268 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add192 = add nsw i32 %267, %268
  %mul193 = mul nsw i32 %add192, 6810
  store i32 %mul193, i32* %tmp15, align 4, !tbaa !15
  %269 = load i32, i32* %tmp15, align 4, !tbaa !15
  %270 = load i32, i32* %z1, align 4, !tbaa !15
  %mul194 = mul nsw i32 %270, 4209
  %add195 = add nsw i32 %269, %mul194
  store i32 %add195, i32* %tmp11, align 4, !tbaa !15
  %271 = load i32, i32* %tmp15, align 4, !tbaa !15
  %272 = load i32, i32* %tmp13, align 4, !tbaa !15
  %mul196 = mul nsw i32 %272, 17828
  %sub197 = sub nsw i32 %271, %mul196
  store i32 %sub197, i32* %tmp14, align 4, !tbaa !15
  %273 = load i32, i32* %z2, align 4, !tbaa !15
  %mul198 = mul nsw i32 %273, -6810
  store i32 %mul198, i32* %tmp13, align 4, !tbaa !15
  %274 = load i32, i32* %z2, align 4, !tbaa !15
  %mul199 = mul nsw i32 %274, -11018
  store i32 %mul199, i32* %tmp15, align 4, !tbaa !15
  %275 = load i32, i32* %z1, align 4, !tbaa !15
  %276 = load i32, i32* %z4, align 4, !tbaa !15
  %sub200 = sub nsw i32 %275, %276
  store i32 %sub200, i32* %z2, align 4, !tbaa !15
  %277 = load i32, i32* %z3, align 4, !tbaa !15
  %278 = load i32, i32* %z2, align 4, !tbaa !15
  %mul201 = mul nsw i32 %278, 11522
  %add202 = add nsw i32 %277, %mul201
  store i32 %add202, i32* %tmp12, align 4, !tbaa !15
  %279 = load i32, i32* %tmp12, align 4, !tbaa !15
  %280 = load i32, i32* %z4, align 4, !tbaa !15
  %mul203 = mul nsw i32 %280, 20131
  %add204 = add nsw i32 %279, %mul203
  %281 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub205 = sub nsw i32 %add204, %281
  store i32 %sub205, i32* %tmp10, align 4, !tbaa !15
  %282 = load i32, i32* %tmp12, align 4, !tbaa !15
  %283 = load i32, i32* %z1, align 4, !tbaa !15
  %mul206 = mul nsw i32 %283, 9113
  %sub207 = sub nsw i32 %282, %mul206
  %284 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add208 = add nsw i32 %sub207, %284
  store i32 %add208, i32* %tmp16, align 4, !tbaa !15
  %285 = load i32, i32* %z2, align 4, !tbaa !15
  %mul209 = mul nsw i32 %285, 10033
  %286 = load i32, i32* %z3, align 4, !tbaa !15
  %sub210 = sub nsw i32 %mul209, %286
  store i32 %sub210, i32* %tmp12, align 4, !tbaa !15
  %287 = load i32, i32* %z1, align 4, !tbaa !15
  %288 = load i32, i32* %z4, align 4, !tbaa !15
  %add211 = add nsw i32 %287, %288
  %mul212 = mul nsw i32 %add211, 4712
  store i32 %mul212, i32* %z2, align 4, !tbaa !15
  %289 = load i32, i32* %z2, align 4, !tbaa !15
  %290 = load i32, i32* %z1, align 4, !tbaa !15
  %mul213 = mul nsw i32 %290, 3897
  %add214 = add nsw i32 %289, %mul213
  %291 = load i32, i32* %z3, align 4, !tbaa !15
  %sub215 = sub nsw i32 %add214, %291
  %292 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add216 = add nsw i32 %292, %sub215
  store i32 %add216, i32* %tmp13, align 4, !tbaa !15
  %293 = load i32, i32* %z2, align 4, !tbaa !15
  %294 = load i32, i32* %z4, align 4, !tbaa !15
  %mul217 = mul nsw i32 %294, 7121
  %sub218 = sub nsw i32 %293, %mul217
  %295 = load i32, i32* %z3, align 4, !tbaa !15
  %add219 = add nsw i32 %sub218, %295
  %296 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add220 = add nsw i32 %296, %add219
  store i32 %add220, i32* %tmp15, align 4, !tbaa !15
  %297 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %298 = load i32, i32* %tmp20, align 4, !tbaa !15
  %299 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add221 = add nsw i32 %298, %299
  %shr222 = ashr i32 %add221, 18
  %and = and i32 %shr222, 1023
  %arrayidx223 = getelementptr inbounds i8, i8* %297, i32 %and
  %300 = load i8, i8* %arrayidx223, align 1, !tbaa !17
  %301 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds i8, i8* %301, i32 0
  store i8 %300, i8* %arrayidx224, align 1, !tbaa !17
  %302 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %303 = load i32, i32* %tmp20, align 4, !tbaa !15
  %304 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub225 = sub nsw i32 %303, %304
  %shr226 = ashr i32 %sub225, 18
  %and227 = and i32 %shr226, 1023
  %arrayidx228 = getelementptr inbounds i8, i8* %302, i32 %and227
  %305 = load i8, i8* %arrayidx228, align 1, !tbaa !17
  %306 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx229 = getelementptr inbounds i8, i8* %306, i32 14
  store i8 %305, i8* %arrayidx229, align 1, !tbaa !17
  %307 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %308 = load i32, i32* %tmp21, align 4, !tbaa !15
  %309 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add230 = add nsw i32 %308, %309
  %shr231 = ashr i32 %add230, 18
  %and232 = and i32 %shr231, 1023
  %arrayidx233 = getelementptr inbounds i8, i8* %307, i32 %and232
  %310 = load i8, i8* %arrayidx233, align 1, !tbaa !17
  %311 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx234 = getelementptr inbounds i8, i8* %311, i32 1
  store i8 %310, i8* %arrayidx234, align 1, !tbaa !17
  %312 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %313 = load i32, i32* %tmp21, align 4, !tbaa !15
  %314 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub235 = sub nsw i32 %313, %314
  %shr236 = ashr i32 %sub235, 18
  %and237 = and i32 %shr236, 1023
  %arrayidx238 = getelementptr inbounds i8, i8* %312, i32 %and237
  %315 = load i8, i8* %arrayidx238, align 1, !tbaa !17
  %316 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx239 = getelementptr inbounds i8, i8* %316, i32 13
  store i8 %315, i8* %arrayidx239, align 1, !tbaa !17
  %317 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %318 = load i32, i32* %tmp22, align 4, !tbaa !15
  %319 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add240 = add nsw i32 %318, %319
  %shr241 = ashr i32 %add240, 18
  %and242 = and i32 %shr241, 1023
  %arrayidx243 = getelementptr inbounds i8, i8* %317, i32 %and242
  %320 = load i8, i8* %arrayidx243, align 1, !tbaa !17
  %321 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx244 = getelementptr inbounds i8, i8* %321, i32 2
  store i8 %320, i8* %arrayidx244, align 1, !tbaa !17
  %322 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %323 = load i32, i32* %tmp22, align 4, !tbaa !15
  %324 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub245 = sub nsw i32 %323, %324
  %shr246 = ashr i32 %sub245, 18
  %and247 = and i32 %shr246, 1023
  %arrayidx248 = getelementptr inbounds i8, i8* %322, i32 %and247
  %325 = load i8, i8* %arrayidx248, align 1, !tbaa !17
  %326 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx249 = getelementptr inbounds i8, i8* %326, i32 12
  store i8 %325, i8* %arrayidx249, align 1, !tbaa !17
  %327 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %328 = load i32, i32* %tmp23, align 4, !tbaa !15
  %329 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add250 = add nsw i32 %328, %329
  %shr251 = ashr i32 %add250, 18
  %and252 = and i32 %shr251, 1023
  %arrayidx253 = getelementptr inbounds i8, i8* %327, i32 %and252
  %330 = load i8, i8* %arrayidx253, align 1, !tbaa !17
  %331 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx254 = getelementptr inbounds i8, i8* %331, i32 3
  store i8 %330, i8* %arrayidx254, align 1, !tbaa !17
  %332 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %333 = load i32, i32* %tmp23, align 4, !tbaa !15
  %334 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub255 = sub nsw i32 %333, %334
  %shr256 = ashr i32 %sub255, 18
  %and257 = and i32 %shr256, 1023
  %arrayidx258 = getelementptr inbounds i8, i8* %332, i32 %and257
  %335 = load i8, i8* %arrayidx258, align 1, !tbaa !17
  %336 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx259 = getelementptr inbounds i8, i8* %336, i32 11
  store i8 %335, i8* %arrayidx259, align 1, !tbaa !17
  %337 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %338 = load i32, i32* %tmp24, align 4, !tbaa !15
  %339 = load i32, i32* %tmp14, align 4, !tbaa !15
  %add260 = add nsw i32 %338, %339
  %shr261 = ashr i32 %add260, 18
  %and262 = and i32 %shr261, 1023
  %arrayidx263 = getelementptr inbounds i8, i8* %337, i32 %and262
  %340 = load i8, i8* %arrayidx263, align 1, !tbaa !17
  %341 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx264 = getelementptr inbounds i8, i8* %341, i32 4
  store i8 %340, i8* %arrayidx264, align 1, !tbaa !17
  %342 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %343 = load i32, i32* %tmp24, align 4, !tbaa !15
  %344 = load i32, i32* %tmp14, align 4, !tbaa !15
  %sub265 = sub nsw i32 %343, %344
  %shr266 = ashr i32 %sub265, 18
  %and267 = and i32 %shr266, 1023
  %arrayidx268 = getelementptr inbounds i8, i8* %342, i32 %and267
  %345 = load i8, i8* %arrayidx268, align 1, !tbaa !17
  %346 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx269 = getelementptr inbounds i8, i8* %346, i32 10
  store i8 %345, i8* %arrayidx269, align 1, !tbaa !17
  %347 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %348 = load i32, i32* %tmp25, align 4, !tbaa !15
  %349 = load i32, i32* %tmp15, align 4, !tbaa !15
  %add270 = add nsw i32 %348, %349
  %shr271 = ashr i32 %add270, 18
  %and272 = and i32 %shr271, 1023
  %arrayidx273 = getelementptr inbounds i8, i8* %347, i32 %and272
  %350 = load i8, i8* %arrayidx273, align 1, !tbaa !17
  %351 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx274 = getelementptr inbounds i8, i8* %351, i32 5
  store i8 %350, i8* %arrayidx274, align 1, !tbaa !17
  %352 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %353 = load i32, i32* %tmp25, align 4, !tbaa !15
  %354 = load i32, i32* %tmp15, align 4, !tbaa !15
  %sub275 = sub nsw i32 %353, %354
  %shr276 = ashr i32 %sub275, 18
  %and277 = and i32 %shr276, 1023
  %arrayidx278 = getelementptr inbounds i8, i8* %352, i32 %and277
  %355 = load i8, i8* %arrayidx278, align 1, !tbaa !17
  %356 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx279 = getelementptr inbounds i8, i8* %356, i32 9
  store i8 %355, i8* %arrayidx279, align 1, !tbaa !17
  %357 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %358 = load i32, i32* %tmp26, align 4, !tbaa !15
  %359 = load i32, i32* %tmp16, align 4, !tbaa !15
  %add280 = add nsw i32 %358, %359
  %shr281 = ashr i32 %add280, 18
  %and282 = and i32 %shr281, 1023
  %arrayidx283 = getelementptr inbounds i8, i8* %357, i32 %and282
  %360 = load i8, i8* %arrayidx283, align 1, !tbaa !17
  %361 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx284 = getelementptr inbounds i8, i8* %361, i32 6
  store i8 %360, i8* %arrayidx284, align 1, !tbaa !17
  %362 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %363 = load i32, i32* %tmp26, align 4, !tbaa !15
  %364 = load i32, i32* %tmp16, align 4, !tbaa !15
  %sub285 = sub nsw i32 %363, %364
  %shr286 = ashr i32 %sub285, 18
  %and287 = and i32 %shr286, 1023
  %arrayidx288 = getelementptr inbounds i8, i8* %362, i32 %and287
  %365 = load i8, i8* %arrayidx288, align 1, !tbaa !17
  %366 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx289 = getelementptr inbounds i8, i8* %366, i32 8
  store i8 %365, i8* %arrayidx289, align 1, !tbaa !17
  %367 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %368 = load i32, i32* %tmp27, align 4, !tbaa !15
  %shr290 = ashr i32 %368, 18
  %and291 = and i32 %shr290, 1023
  %arrayidx292 = getelementptr inbounds i8, i8* %367, i32 %and291
  %369 = load i8, i8* %arrayidx292, align 1, !tbaa !17
  %370 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx293 = getelementptr inbounds i8, i8* %370, i32 7
  store i8 %369, i8* %arrayidx293, align 1, !tbaa !17
  %371 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr294 = getelementptr inbounds i32, i32* %371, i32 8
  store i32* %add.ptr294, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc295

for.inc295:                                       ; preds = %for.body143
  %372 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc296 = add nsw i32 %372, 1
  store i32 %inc296, i32* %ctr, align 4, !tbaa !6
  br label %for.cond140

for.end297:                                       ; preds = %for.cond140
  %373 = bitcast [120 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 480, i8* %373) #2
  %374 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %374) #2
  %375 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %375) #2
  %376 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %376) #2
  %377 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %377) #2
  %378 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %378) #2
  %379 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %379) #2
  %380 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %380) #2
  %381 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %381) #2
  %382 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %382) #2
  %383 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %383) #2
  %384 = bitcast i32* %tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %384) #2
  %385 = bitcast i32* %tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %385) #2
  %386 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %386) #2
  %387 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #2
  %388 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %388) #2
  %389 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %389) #2
  %390 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %390) #2
  %391 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %391) #2
  %392 = bitcast i32* %tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %392) #2
  %393 = bitcast i32* %tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %393) #2
  %394 = bitcast i32* %tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %394) #2
  %395 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %395) #2
  %396 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %396) #2
  %397 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %397) #2
  %398 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %398) #2
  ret void
}

; Function Attrs: nounwind
define hidden void @jpeg_idct_16x16(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp3 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %tmp20 = alloca i32, align 4
  %tmp21 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %tmp23 = alloca i32, align 4
  %tmp24 = alloca i32, align 4
  %tmp25 = alloca i32, align 4
  %tmp26 = alloca i32, align 4
  %tmp27 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca i32*, align 4
  %wsptr = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [128 x i32], align 16
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32* %tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  %22 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #2
  %23 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #2
  %24 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #2
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 65
  %26 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  %add.ptr = getelementptr inbounds i8, i8* %26, i32 128
  store i8* %add.ptr, i8** %range_limit, align 4, !tbaa !2
  %27 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #2
  %28 = bitcast [128 x i32]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 512, i8* %28) #2
  %29 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %29, i16** %inptr, align 4, !tbaa !2
  %30 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %30, i32 0, i32 20
  %31 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %32 = bitcast i8* %31 to i32*
  store i32* %32, i32** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [128 x i32], [128 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %33 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp slt i32 %33, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %34 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %34, i32 0
  %35 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %35 to i32
  %36 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %36, i32 0
  %37 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %mul = mul nsw i32 %conv, %37
  store i32 %mul, i32* %tmp0, align 4, !tbaa !15
  %38 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl = shl i32 %38, 13
  store i32 %shl, i32* %tmp0, align 4, !tbaa !15
  %39 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add = add nsw i32 %39, 1024
  store i32 %add, i32* %tmp0, align 4, !tbaa !15
  %40 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i16, i16* %40, i32 32
  %41 = load i16, i16* %arrayidx2, align 2, !tbaa !14
  %conv3 = sext i16 %41 to i32
  %42 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %42, i32 32
  %43 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %mul5 = mul nsw i32 %conv3, %43
  store i32 %mul5, i32* %z1, align 4, !tbaa !15
  %44 = load i32, i32* %z1, align 4, !tbaa !15
  %mul6 = mul nsw i32 %44, 10703
  store i32 %mul6, i32* %tmp1, align 4, !tbaa !15
  %45 = load i32, i32* %z1, align 4, !tbaa !15
  %mul7 = mul nsw i32 %45, 4433
  store i32 %mul7, i32* %tmp2, align 4, !tbaa !15
  %46 = load i32, i32* %tmp0, align 4, !tbaa !15
  %47 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add8 = add nsw i32 %46, %47
  store i32 %add8, i32* %tmp10, align 4, !tbaa !15
  %48 = load i32, i32* %tmp0, align 4, !tbaa !15
  %49 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub = sub nsw i32 %48, %49
  store i32 %sub, i32* %tmp11, align 4, !tbaa !15
  %50 = load i32, i32* %tmp0, align 4, !tbaa !15
  %51 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add9 = add nsw i32 %50, %51
  store i32 %add9, i32* %tmp12, align 4, !tbaa !15
  %52 = load i32, i32* %tmp0, align 4, !tbaa !15
  %53 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub10 = sub nsw i32 %52, %53
  store i32 %sub10, i32* %tmp13, align 4, !tbaa !15
  %54 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i16, i16* %54, i32 16
  %55 = load i16, i16* %arrayidx11, align 2, !tbaa !14
  %conv12 = sext i16 %55 to i32
  %56 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %56, i32 16
  %57 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %mul14 = mul nsw i32 %conv12, %57
  store i32 %mul14, i32* %z1, align 4, !tbaa !15
  %58 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i16, i16* %58, i32 48
  %59 = load i16, i16* %arrayidx15, align 2, !tbaa !14
  %conv16 = sext i16 %59 to i32
  %60 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %60, i32 48
  %61 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %mul18 = mul nsw i32 %conv16, %61
  store i32 %mul18, i32* %z2, align 4, !tbaa !15
  %62 = load i32, i32* %z1, align 4, !tbaa !15
  %63 = load i32, i32* %z2, align 4, !tbaa !15
  %sub19 = sub nsw i32 %62, %63
  store i32 %sub19, i32* %z3, align 4, !tbaa !15
  %64 = load i32, i32* %z3, align 4, !tbaa !15
  %mul20 = mul nsw i32 %64, 2260
  store i32 %mul20, i32* %z4, align 4, !tbaa !15
  %65 = load i32, i32* %z3, align 4, !tbaa !15
  %mul21 = mul nsw i32 %65, 11363
  store i32 %mul21, i32* %z3, align 4, !tbaa !15
  %66 = load i32, i32* %z3, align 4, !tbaa !15
  %67 = load i32, i32* %z2, align 4, !tbaa !15
  %mul22 = mul nsw i32 %67, 20995
  %add23 = add nsw i32 %66, %mul22
  store i32 %add23, i32* %tmp0, align 4, !tbaa !15
  %68 = load i32, i32* %z4, align 4, !tbaa !15
  %69 = load i32, i32* %z1, align 4, !tbaa !15
  %mul24 = mul nsw i32 %69, 7373
  %add25 = add nsw i32 %68, %mul24
  store i32 %add25, i32* %tmp1, align 4, !tbaa !15
  %70 = load i32, i32* %z3, align 4, !tbaa !15
  %71 = load i32, i32* %z1, align 4, !tbaa !15
  %mul26 = mul nsw i32 %71, 4926
  %sub27 = sub nsw i32 %70, %mul26
  store i32 %sub27, i32* %tmp2, align 4, !tbaa !15
  %72 = load i32, i32* %z4, align 4, !tbaa !15
  %73 = load i32, i32* %z2, align 4, !tbaa !15
  %mul28 = mul nsw i32 %73, 4176
  %sub29 = sub nsw i32 %72, %mul28
  store i32 %sub29, i32* %tmp3, align 4, !tbaa !15
  %74 = load i32, i32* %tmp10, align 4, !tbaa !15
  %75 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add30 = add nsw i32 %74, %75
  store i32 %add30, i32* %tmp20, align 4, !tbaa !15
  %76 = load i32, i32* %tmp10, align 4, !tbaa !15
  %77 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub31 = sub nsw i32 %76, %77
  store i32 %sub31, i32* %tmp27, align 4, !tbaa !15
  %78 = load i32, i32* %tmp12, align 4, !tbaa !15
  %79 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add32 = add nsw i32 %78, %79
  store i32 %add32, i32* %tmp21, align 4, !tbaa !15
  %80 = load i32, i32* %tmp12, align 4, !tbaa !15
  %81 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub33 = sub nsw i32 %80, %81
  store i32 %sub33, i32* %tmp26, align 4, !tbaa !15
  %82 = load i32, i32* %tmp13, align 4, !tbaa !15
  %83 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add34 = add nsw i32 %82, %83
  store i32 %add34, i32* %tmp22, align 4, !tbaa !15
  %84 = load i32, i32* %tmp13, align 4, !tbaa !15
  %85 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub35 = sub nsw i32 %84, %85
  store i32 %sub35, i32* %tmp25, align 4, !tbaa !15
  %86 = load i32, i32* %tmp11, align 4, !tbaa !15
  %87 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add36 = add nsw i32 %86, %87
  store i32 %add36, i32* %tmp23, align 4, !tbaa !15
  %88 = load i32, i32* %tmp11, align 4, !tbaa !15
  %89 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub37 = sub nsw i32 %88, %89
  store i32 %sub37, i32* %tmp24, align 4, !tbaa !15
  %90 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i16, i16* %90, i32 8
  %91 = load i16, i16* %arrayidx38, align 2, !tbaa !14
  %conv39 = sext i16 %91 to i32
  %92 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %92, i32 8
  %93 = load i32, i32* %arrayidx40, align 4, !tbaa !6
  %mul41 = mul nsw i32 %conv39, %93
  store i32 %mul41, i32* %z1, align 4, !tbaa !15
  %94 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i16, i16* %94, i32 24
  %95 = load i16, i16* %arrayidx42, align 2, !tbaa !14
  %conv43 = sext i16 %95 to i32
  %96 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds i32, i32* %96, i32 24
  %97 = load i32, i32* %arrayidx44, align 4, !tbaa !6
  %mul45 = mul nsw i32 %conv43, %97
  store i32 %mul45, i32* %z2, align 4, !tbaa !15
  %98 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i16, i16* %98, i32 40
  %99 = load i16, i16* %arrayidx46, align 2, !tbaa !14
  %conv47 = sext i16 %99 to i32
  %100 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds i32, i32* %100, i32 40
  %101 = load i32, i32* %arrayidx48, align 4, !tbaa !6
  %mul49 = mul nsw i32 %conv47, %101
  store i32 %mul49, i32* %z3, align 4, !tbaa !15
  %102 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i16, i16* %102, i32 56
  %103 = load i16, i16* %arrayidx50, align 2, !tbaa !14
  %conv51 = sext i16 %103 to i32
  %104 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i32, i32* %104, i32 56
  %105 = load i32, i32* %arrayidx52, align 4, !tbaa !6
  %mul53 = mul nsw i32 %conv51, %105
  store i32 %mul53, i32* %z4, align 4, !tbaa !15
  %106 = load i32, i32* %z1, align 4, !tbaa !15
  %107 = load i32, i32* %z3, align 4, !tbaa !15
  %add54 = add nsw i32 %106, %107
  store i32 %add54, i32* %tmp11, align 4, !tbaa !15
  %108 = load i32, i32* %z1, align 4, !tbaa !15
  %109 = load i32, i32* %z2, align 4, !tbaa !15
  %add55 = add nsw i32 %108, %109
  %mul56 = mul nsw i32 %add55, 11086
  store i32 %mul56, i32* %tmp1, align 4, !tbaa !15
  %110 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul57 = mul nsw i32 %110, 10217
  store i32 %mul57, i32* %tmp2, align 4, !tbaa !15
  %111 = load i32, i32* %z1, align 4, !tbaa !15
  %112 = load i32, i32* %z4, align 4, !tbaa !15
  %add58 = add nsw i32 %111, %112
  %mul59 = mul nsw i32 %add58, 8956
  store i32 %mul59, i32* %tmp3, align 4, !tbaa !15
  %113 = load i32, i32* %z1, align 4, !tbaa !15
  %114 = load i32, i32* %z4, align 4, !tbaa !15
  %sub60 = sub nsw i32 %113, %114
  %mul61 = mul nsw i32 %sub60, 7350
  store i32 %mul61, i32* %tmp10, align 4, !tbaa !15
  %115 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul62 = mul nsw i32 %115, 5461
  store i32 %mul62, i32* %tmp11, align 4, !tbaa !15
  %116 = load i32, i32* %z1, align 4, !tbaa !15
  %117 = load i32, i32* %z2, align 4, !tbaa !15
  %sub63 = sub nsw i32 %116, %117
  %mul64 = mul nsw i32 %sub63, 3363
  store i32 %mul64, i32* %tmp12, align 4, !tbaa !15
  %118 = load i32, i32* %tmp1, align 4, !tbaa !15
  %119 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add65 = add nsw i32 %118, %119
  %120 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add66 = add nsw i32 %add65, %120
  %121 = load i32, i32* %z1, align 4, !tbaa !15
  %mul67 = mul nsw i32 %121, 18730
  %sub68 = sub nsw i32 %add66, %mul67
  store i32 %sub68, i32* %tmp0, align 4, !tbaa !15
  %122 = load i32, i32* %tmp10, align 4, !tbaa !15
  %123 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add69 = add nsw i32 %122, %123
  %124 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add70 = add nsw i32 %add69, %124
  %125 = load i32, i32* %z1, align 4, !tbaa !15
  %mul71 = mul nsw i32 %125, 15038
  %sub72 = sub nsw i32 %add70, %mul71
  store i32 %sub72, i32* %tmp13, align 4, !tbaa !15
  %126 = load i32, i32* %z2, align 4, !tbaa !15
  %127 = load i32, i32* %z3, align 4, !tbaa !15
  %add73 = add nsw i32 %126, %127
  %mul74 = mul nsw i32 %add73, 1136
  store i32 %mul74, i32* %z1, align 4, !tbaa !15
  %128 = load i32, i32* %z1, align 4, !tbaa !15
  %129 = load i32, i32* %z2, align 4, !tbaa !15
  %mul75 = mul nsw i32 %129, 589
  %add76 = add nsw i32 %128, %mul75
  %130 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add77 = add nsw i32 %130, %add76
  store i32 %add77, i32* %tmp1, align 4, !tbaa !15
  %131 = load i32, i32* %z1, align 4, !tbaa !15
  %132 = load i32, i32* %z3, align 4, !tbaa !15
  %mul78 = mul nsw i32 %132, 9222
  %sub79 = sub nsw i32 %131, %mul78
  %133 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add80 = add nsw i32 %133, %sub79
  store i32 %add80, i32* %tmp2, align 4, !tbaa !15
  %134 = load i32, i32* %z3, align 4, !tbaa !15
  %135 = load i32, i32* %z2, align 4, !tbaa !15
  %sub81 = sub nsw i32 %134, %135
  %mul82 = mul nsw i32 %sub81, 11529
  store i32 %mul82, i32* %z1, align 4, !tbaa !15
  %136 = load i32, i32* %z1, align 4, !tbaa !15
  %137 = load i32, i32* %z3, align 4, !tbaa !15
  %mul83 = mul nsw i32 %137, 6278
  %sub84 = sub nsw i32 %136, %mul83
  %138 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add85 = add nsw i32 %138, %sub84
  store i32 %add85, i32* %tmp11, align 4, !tbaa !15
  %139 = load i32, i32* %z1, align 4, !tbaa !15
  %140 = load i32, i32* %z2, align 4, !tbaa !15
  %mul86 = mul nsw i32 %140, 16154
  %add87 = add nsw i32 %139, %mul86
  %141 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add88 = add nsw i32 %141, %add87
  store i32 %add88, i32* %tmp12, align 4, !tbaa !15
  %142 = load i32, i32* %z4, align 4, !tbaa !15
  %143 = load i32, i32* %z2, align 4, !tbaa !15
  %add89 = add nsw i32 %143, %142
  store i32 %add89, i32* %z2, align 4, !tbaa !15
  %144 = load i32, i32* %z2, align 4, !tbaa !15
  %mul90 = mul nsw i32 %144, -5461
  store i32 %mul90, i32* %z1, align 4, !tbaa !15
  %145 = load i32, i32* %z1, align 4, !tbaa !15
  %146 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add91 = add nsw i32 %146, %145
  store i32 %add91, i32* %tmp1, align 4, !tbaa !15
  %147 = load i32, i32* %z1, align 4, !tbaa !15
  %148 = load i32, i32* %z4, align 4, !tbaa !15
  %mul92 = mul nsw i32 %148, 8728
  %add93 = add nsw i32 %147, %mul92
  %149 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add94 = add nsw i32 %149, %add93
  store i32 %add94, i32* %tmp3, align 4, !tbaa !15
  %150 = load i32, i32* %z2, align 4, !tbaa !15
  %mul95 = mul nsw i32 %150, -10217
  store i32 %mul95, i32* %z2, align 4, !tbaa !15
  %151 = load i32, i32* %z2, align 4, !tbaa !15
  %152 = load i32, i32* %z4, align 4, !tbaa !15
  %mul96 = mul nsw i32 %152, 25733
  %add97 = add nsw i32 %151, %mul96
  %153 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add98 = add nsw i32 %153, %add97
  store i32 %add98, i32* %tmp10, align 4, !tbaa !15
  %154 = load i32, i32* %z2, align 4, !tbaa !15
  %155 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add99 = add nsw i32 %155, %154
  store i32 %add99, i32* %tmp12, align 4, !tbaa !15
  %156 = load i32, i32* %z3, align 4, !tbaa !15
  %157 = load i32, i32* %z4, align 4, !tbaa !15
  %add100 = add nsw i32 %156, %157
  %mul101 = mul nsw i32 %add100, -11086
  store i32 %mul101, i32* %z2, align 4, !tbaa !15
  %158 = load i32, i32* %z2, align 4, !tbaa !15
  %159 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add102 = add nsw i32 %159, %158
  store i32 %add102, i32* %tmp2, align 4, !tbaa !15
  %160 = load i32, i32* %z2, align 4, !tbaa !15
  %161 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add103 = add nsw i32 %161, %160
  store i32 %add103, i32* %tmp3, align 4, !tbaa !15
  %162 = load i32, i32* %z4, align 4, !tbaa !15
  %163 = load i32, i32* %z3, align 4, !tbaa !15
  %sub104 = sub nsw i32 %162, %163
  %mul105 = mul nsw i32 %sub104, 3363
  store i32 %mul105, i32* %z2, align 4, !tbaa !15
  %164 = load i32, i32* %z2, align 4, !tbaa !15
  %165 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add106 = add nsw i32 %165, %164
  store i32 %add106, i32* %tmp10, align 4, !tbaa !15
  %166 = load i32, i32* %z2, align 4, !tbaa !15
  %167 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add107 = add nsw i32 %167, %166
  store i32 %add107, i32* %tmp11, align 4, !tbaa !15
  %168 = load i32, i32* %tmp20, align 4, !tbaa !15
  %169 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add108 = add nsw i32 %168, %169
  %shr = ashr i32 %add108, 11
  %170 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i32, i32* %170, i32 0
  store i32 %shr, i32* %arrayidx109, align 4, !tbaa !6
  %171 = load i32, i32* %tmp20, align 4, !tbaa !15
  %172 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub110 = sub nsw i32 %171, %172
  %shr111 = ashr i32 %sub110, 11
  %173 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds i32, i32* %173, i32 120
  store i32 %shr111, i32* %arrayidx112, align 4, !tbaa !6
  %174 = load i32, i32* %tmp21, align 4, !tbaa !15
  %175 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add113 = add nsw i32 %174, %175
  %shr114 = ashr i32 %add113, 11
  %176 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %176, i32 8
  store i32 %shr114, i32* %arrayidx115, align 4, !tbaa !6
  %177 = load i32, i32* %tmp21, align 4, !tbaa !15
  %178 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub116 = sub nsw i32 %177, %178
  %shr117 = ashr i32 %sub116, 11
  %179 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx118 = getelementptr inbounds i32, i32* %179, i32 112
  store i32 %shr117, i32* %arrayidx118, align 4, !tbaa !6
  %180 = load i32, i32* %tmp22, align 4, !tbaa !15
  %181 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add119 = add nsw i32 %180, %181
  %shr120 = ashr i32 %add119, 11
  %182 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds i32, i32* %182, i32 16
  store i32 %shr120, i32* %arrayidx121, align 4, !tbaa !6
  %183 = load i32, i32* %tmp22, align 4, !tbaa !15
  %184 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub122 = sub nsw i32 %183, %184
  %shr123 = ashr i32 %sub122, 11
  %185 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx124 = getelementptr inbounds i32, i32* %185, i32 104
  store i32 %shr123, i32* %arrayidx124, align 4, !tbaa !6
  %186 = load i32, i32* %tmp23, align 4, !tbaa !15
  %187 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add125 = add nsw i32 %186, %187
  %shr126 = ashr i32 %add125, 11
  %188 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %188, i32 24
  store i32 %shr126, i32* %arrayidx127, align 4, !tbaa !6
  %189 = load i32, i32* %tmp23, align 4, !tbaa !15
  %190 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub128 = sub nsw i32 %189, %190
  %shr129 = ashr i32 %sub128, 11
  %191 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx130 = getelementptr inbounds i32, i32* %191, i32 96
  store i32 %shr129, i32* %arrayidx130, align 4, !tbaa !6
  %192 = load i32, i32* %tmp24, align 4, !tbaa !15
  %193 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add131 = add nsw i32 %192, %193
  %shr132 = ashr i32 %add131, 11
  %194 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds i32, i32* %194, i32 32
  store i32 %shr132, i32* %arrayidx133, align 4, !tbaa !6
  %195 = load i32, i32* %tmp24, align 4, !tbaa !15
  %196 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub134 = sub nsw i32 %195, %196
  %shr135 = ashr i32 %sub134, 11
  %197 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx136 = getelementptr inbounds i32, i32* %197, i32 88
  store i32 %shr135, i32* %arrayidx136, align 4, !tbaa !6
  %198 = load i32, i32* %tmp25, align 4, !tbaa !15
  %199 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add137 = add nsw i32 %198, %199
  %shr138 = ashr i32 %add137, 11
  %200 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds i32, i32* %200, i32 40
  store i32 %shr138, i32* %arrayidx139, align 4, !tbaa !6
  %201 = load i32, i32* %tmp25, align 4, !tbaa !15
  %202 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub140 = sub nsw i32 %201, %202
  %shr141 = ashr i32 %sub140, 11
  %203 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx142 = getelementptr inbounds i32, i32* %203, i32 80
  store i32 %shr141, i32* %arrayidx142, align 4, !tbaa !6
  %204 = load i32, i32* %tmp26, align 4, !tbaa !15
  %205 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add143 = add nsw i32 %204, %205
  %shr144 = ashr i32 %add143, 11
  %206 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds i32, i32* %206, i32 48
  store i32 %shr144, i32* %arrayidx145, align 4, !tbaa !6
  %207 = load i32, i32* %tmp26, align 4, !tbaa !15
  %208 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub146 = sub nsw i32 %207, %208
  %shr147 = ashr i32 %sub146, 11
  %209 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx148 = getelementptr inbounds i32, i32* %209, i32 72
  store i32 %shr147, i32* %arrayidx148, align 4, !tbaa !6
  %210 = load i32, i32* %tmp27, align 4, !tbaa !15
  %211 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add149 = add nsw i32 %210, %211
  %shr150 = ashr i32 %add149, 11
  %212 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx151 = getelementptr inbounds i32, i32* %212, i32 56
  store i32 %shr150, i32* %arrayidx151, align 4, !tbaa !6
  %213 = load i32, i32* %tmp27, align 4, !tbaa !15
  %214 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub152 = sub nsw i32 %213, %214
  %shr153 = ashr i32 %sub152, 11
  %215 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds i32, i32* %215, i32 64
  store i32 %shr153, i32* %arrayidx154, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %216 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %216, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  %217 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %217, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %218 = load i32*, i32** %quantptr, align 4, !tbaa !2
  %incdec.ptr155 = getelementptr inbounds i32, i32* %218, i32 1
  store i32* %incdec.ptr155, i32** %quantptr, align 4, !tbaa !2
  %219 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %incdec.ptr156 = getelementptr inbounds i32, i32* %219, i32 1
  store i32* %incdec.ptr156, i32** %wsptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay157 = getelementptr inbounds [128 x i32], [128 x i32]* %workspace, i32 0, i32 0
  store i32* %arraydecay157, i32** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond158

for.cond158:                                      ; preds = %for.inc333, %for.end
  %220 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp159 = icmp slt i32 %220, 16
  br i1 %cmp159, label %for.body161, label %for.end335

for.body161:                                      ; preds = %for.cond158
  %221 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %222 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx162 = getelementptr inbounds i8*, i8** %221, i32 %222
  %223 = load i8*, i8** %arrayidx162, align 4, !tbaa !2
  %224 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr163 = getelementptr inbounds i8, i8* %223, i32 %224
  store i8* %add.ptr163, i8** %outptr, align 4, !tbaa !2
  %225 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx164 = getelementptr inbounds i32, i32* %225, i32 0
  %226 = load i32, i32* %arrayidx164, align 4, !tbaa !6
  %add165 = add nsw i32 %226, 16
  store i32 %add165, i32* %tmp0, align 4, !tbaa !15
  %227 = load i32, i32* %tmp0, align 4, !tbaa !15
  %shl166 = shl i32 %227, 13
  store i32 %shl166, i32* %tmp0, align 4, !tbaa !15
  %228 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds i32, i32* %228, i32 4
  %229 = load i32, i32* %arrayidx167, align 4, !tbaa !6
  store i32 %229, i32* %z1, align 4, !tbaa !15
  %230 = load i32, i32* %z1, align 4, !tbaa !15
  %mul168 = mul nsw i32 %230, 10703
  store i32 %mul168, i32* %tmp1, align 4, !tbaa !15
  %231 = load i32, i32* %z1, align 4, !tbaa !15
  %mul169 = mul nsw i32 %231, 4433
  store i32 %mul169, i32* %tmp2, align 4, !tbaa !15
  %232 = load i32, i32* %tmp0, align 4, !tbaa !15
  %233 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add170 = add nsw i32 %232, %233
  store i32 %add170, i32* %tmp10, align 4, !tbaa !15
  %234 = load i32, i32* %tmp0, align 4, !tbaa !15
  %235 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub171 = sub nsw i32 %234, %235
  store i32 %sub171, i32* %tmp11, align 4, !tbaa !15
  %236 = load i32, i32* %tmp0, align 4, !tbaa !15
  %237 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add172 = add nsw i32 %236, %237
  store i32 %add172, i32* %tmp12, align 4, !tbaa !15
  %238 = load i32, i32* %tmp0, align 4, !tbaa !15
  %239 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub173 = sub nsw i32 %238, %239
  store i32 %sub173, i32* %tmp13, align 4, !tbaa !15
  %240 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i32, i32* %240, i32 2
  %241 = load i32, i32* %arrayidx174, align 4, !tbaa !6
  store i32 %241, i32* %z1, align 4, !tbaa !15
  %242 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx175 = getelementptr inbounds i32, i32* %242, i32 6
  %243 = load i32, i32* %arrayidx175, align 4, !tbaa !6
  store i32 %243, i32* %z2, align 4, !tbaa !15
  %244 = load i32, i32* %z1, align 4, !tbaa !15
  %245 = load i32, i32* %z2, align 4, !tbaa !15
  %sub176 = sub nsw i32 %244, %245
  store i32 %sub176, i32* %z3, align 4, !tbaa !15
  %246 = load i32, i32* %z3, align 4, !tbaa !15
  %mul177 = mul nsw i32 %246, 2260
  store i32 %mul177, i32* %z4, align 4, !tbaa !15
  %247 = load i32, i32* %z3, align 4, !tbaa !15
  %mul178 = mul nsw i32 %247, 11363
  store i32 %mul178, i32* %z3, align 4, !tbaa !15
  %248 = load i32, i32* %z3, align 4, !tbaa !15
  %249 = load i32, i32* %z2, align 4, !tbaa !15
  %mul179 = mul nsw i32 %249, 20995
  %add180 = add nsw i32 %248, %mul179
  store i32 %add180, i32* %tmp0, align 4, !tbaa !15
  %250 = load i32, i32* %z4, align 4, !tbaa !15
  %251 = load i32, i32* %z1, align 4, !tbaa !15
  %mul181 = mul nsw i32 %251, 7373
  %add182 = add nsw i32 %250, %mul181
  store i32 %add182, i32* %tmp1, align 4, !tbaa !15
  %252 = load i32, i32* %z3, align 4, !tbaa !15
  %253 = load i32, i32* %z1, align 4, !tbaa !15
  %mul183 = mul nsw i32 %253, 4926
  %sub184 = sub nsw i32 %252, %mul183
  store i32 %sub184, i32* %tmp2, align 4, !tbaa !15
  %254 = load i32, i32* %z4, align 4, !tbaa !15
  %255 = load i32, i32* %z2, align 4, !tbaa !15
  %mul185 = mul nsw i32 %255, 4176
  %sub186 = sub nsw i32 %254, %mul185
  store i32 %sub186, i32* %tmp3, align 4, !tbaa !15
  %256 = load i32, i32* %tmp10, align 4, !tbaa !15
  %257 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add187 = add nsw i32 %256, %257
  store i32 %add187, i32* %tmp20, align 4, !tbaa !15
  %258 = load i32, i32* %tmp10, align 4, !tbaa !15
  %259 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub188 = sub nsw i32 %258, %259
  store i32 %sub188, i32* %tmp27, align 4, !tbaa !15
  %260 = load i32, i32* %tmp12, align 4, !tbaa !15
  %261 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add189 = add nsw i32 %260, %261
  store i32 %add189, i32* %tmp21, align 4, !tbaa !15
  %262 = load i32, i32* %tmp12, align 4, !tbaa !15
  %263 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub190 = sub nsw i32 %262, %263
  store i32 %sub190, i32* %tmp26, align 4, !tbaa !15
  %264 = load i32, i32* %tmp13, align 4, !tbaa !15
  %265 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add191 = add nsw i32 %264, %265
  store i32 %add191, i32* %tmp22, align 4, !tbaa !15
  %266 = load i32, i32* %tmp13, align 4, !tbaa !15
  %267 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub192 = sub nsw i32 %266, %267
  store i32 %sub192, i32* %tmp25, align 4, !tbaa !15
  %268 = load i32, i32* %tmp11, align 4, !tbaa !15
  %269 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add193 = add nsw i32 %268, %269
  store i32 %add193, i32* %tmp23, align 4, !tbaa !15
  %270 = load i32, i32* %tmp11, align 4, !tbaa !15
  %271 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub194 = sub nsw i32 %270, %271
  store i32 %sub194, i32* %tmp24, align 4, !tbaa !15
  %272 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx195 = getelementptr inbounds i32, i32* %272, i32 1
  %273 = load i32, i32* %arrayidx195, align 4, !tbaa !6
  store i32 %273, i32* %z1, align 4, !tbaa !15
  %274 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx196 = getelementptr inbounds i32, i32* %274, i32 3
  %275 = load i32, i32* %arrayidx196, align 4, !tbaa !6
  store i32 %275, i32* %z2, align 4, !tbaa !15
  %276 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx197 = getelementptr inbounds i32, i32* %276, i32 5
  %277 = load i32, i32* %arrayidx197, align 4, !tbaa !6
  store i32 %277, i32* %z3, align 4, !tbaa !15
  %278 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %arrayidx198 = getelementptr inbounds i32, i32* %278, i32 7
  %279 = load i32, i32* %arrayidx198, align 4, !tbaa !6
  store i32 %279, i32* %z4, align 4, !tbaa !15
  %280 = load i32, i32* %z1, align 4, !tbaa !15
  %281 = load i32, i32* %z3, align 4, !tbaa !15
  %add199 = add nsw i32 %280, %281
  store i32 %add199, i32* %tmp11, align 4, !tbaa !15
  %282 = load i32, i32* %z1, align 4, !tbaa !15
  %283 = load i32, i32* %z2, align 4, !tbaa !15
  %add200 = add nsw i32 %282, %283
  %mul201 = mul nsw i32 %add200, 11086
  store i32 %mul201, i32* %tmp1, align 4, !tbaa !15
  %284 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul202 = mul nsw i32 %284, 10217
  store i32 %mul202, i32* %tmp2, align 4, !tbaa !15
  %285 = load i32, i32* %z1, align 4, !tbaa !15
  %286 = load i32, i32* %z4, align 4, !tbaa !15
  %add203 = add nsw i32 %285, %286
  %mul204 = mul nsw i32 %add203, 8956
  store i32 %mul204, i32* %tmp3, align 4, !tbaa !15
  %287 = load i32, i32* %z1, align 4, !tbaa !15
  %288 = load i32, i32* %z4, align 4, !tbaa !15
  %sub205 = sub nsw i32 %287, %288
  %mul206 = mul nsw i32 %sub205, 7350
  store i32 %mul206, i32* %tmp10, align 4, !tbaa !15
  %289 = load i32, i32* %tmp11, align 4, !tbaa !15
  %mul207 = mul nsw i32 %289, 5461
  store i32 %mul207, i32* %tmp11, align 4, !tbaa !15
  %290 = load i32, i32* %z1, align 4, !tbaa !15
  %291 = load i32, i32* %z2, align 4, !tbaa !15
  %sub208 = sub nsw i32 %290, %291
  %mul209 = mul nsw i32 %sub208, 3363
  store i32 %mul209, i32* %tmp12, align 4, !tbaa !15
  %292 = load i32, i32* %tmp1, align 4, !tbaa !15
  %293 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add210 = add nsw i32 %292, %293
  %294 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add211 = add nsw i32 %add210, %294
  %295 = load i32, i32* %z1, align 4, !tbaa !15
  %mul212 = mul nsw i32 %295, 18730
  %sub213 = sub nsw i32 %add211, %mul212
  store i32 %sub213, i32* %tmp0, align 4, !tbaa !15
  %296 = load i32, i32* %tmp10, align 4, !tbaa !15
  %297 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add214 = add nsw i32 %296, %297
  %298 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add215 = add nsw i32 %add214, %298
  %299 = load i32, i32* %z1, align 4, !tbaa !15
  %mul216 = mul nsw i32 %299, 15038
  %sub217 = sub nsw i32 %add215, %mul216
  store i32 %sub217, i32* %tmp13, align 4, !tbaa !15
  %300 = load i32, i32* %z2, align 4, !tbaa !15
  %301 = load i32, i32* %z3, align 4, !tbaa !15
  %add218 = add nsw i32 %300, %301
  %mul219 = mul nsw i32 %add218, 1136
  store i32 %mul219, i32* %z1, align 4, !tbaa !15
  %302 = load i32, i32* %z1, align 4, !tbaa !15
  %303 = load i32, i32* %z2, align 4, !tbaa !15
  %mul220 = mul nsw i32 %303, 589
  %add221 = add nsw i32 %302, %mul220
  %304 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add222 = add nsw i32 %304, %add221
  store i32 %add222, i32* %tmp1, align 4, !tbaa !15
  %305 = load i32, i32* %z1, align 4, !tbaa !15
  %306 = load i32, i32* %z3, align 4, !tbaa !15
  %mul223 = mul nsw i32 %306, 9222
  %sub224 = sub nsw i32 %305, %mul223
  %307 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add225 = add nsw i32 %307, %sub224
  store i32 %add225, i32* %tmp2, align 4, !tbaa !15
  %308 = load i32, i32* %z3, align 4, !tbaa !15
  %309 = load i32, i32* %z2, align 4, !tbaa !15
  %sub226 = sub nsw i32 %308, %309
  %mul227 = mul nsw i32 %sub226, 11529
  store i32 %mul227, i32* %z1, align 4, !tbaa !15
  %310 = load i32, i32* %z1, align 4, !tbaa !15
  %311 = load i32, i32* %z3, align 4, !tbaa !15
  %mul228 = mul nsw i32 %311, 6278
  %sub229 = sub nsw i32 %310, %mul228
  %312 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add230 = add nsw i32 %312, %sub229
  store i32 %add230, i32* %tmp11, align 4, !tbaa !15
  %313 = load i32, i32* %z1, align 4, !tbaa !15
  %314 = load i32, i32* %z2, align 4, !tbaa !15
  %mul231 = mul nsw i32 %314, 16154
  %add232 = add nsw i32 %313, %mul231
  %315 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add233 = add nsw i32 %315, %add232
  store i32 %add233, i32* %tmp12, align 4, !tbaa !15
  %316 = load i32, i32* %z4, align 4, !tbaa !15
  %317 = load i32, i32* %z2, align 4, !tbaa !15
  %add234 = add nsw i32 %317, %316
  store i32 %add234, i32* %z2, align 4, !tbaa !15
  %318 = load i32, i32* %z2, align 4, !tbaa !15
  %mul235 = mul nsw i32 %318, -5461
  store i32 %mul235, i32* %z1, align 4, !tbaa !15
  %319 = load i32, i32* %z1, align 4, !tbaa !15
  %320 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add236 = add nsw i32 %320, %319
  store i32 %add236, i32* %tmp1, align 4, !tbaa !15
  %321 = load i32, i32* %z1, align 4, !tbaa !15
  %322 = load i32, i32* %z4, align 4, !tbaa !15
  %mul237 = mul nsw i32 %322, 8728
  %add238 = add nsw i32 %321, %mul237
  %323 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add239 = add nsw i32 %323, %add238
  store i32 %add239, i32* %tmp3, align 4, !tbaa !15
  %324 = load i32, i32* %z2, align 4, !tbaa !15
  %mul240 = mul nsw i32 %324, -10217
  store i32 %mul240, i32* %z2, align 4, !tbaa !15
  %325 = load i32, i32* %z2, align 4, !tbaa !15
  %326 = load i32, i32* %z4, align 4, !tbaa !15
  %mul241 = mul nsw i32 %326, 25733
  %add242 = add nsw i32 %325, %mul241
  %327 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add243 = add nsw i32 %327, %add242
  store i32 %add243, i32* %tmp10, align 4, !tbaa !15
  %328 = load i32, i32* %z2, align 4, !tbaa !15
  %329 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add244 = add nsw i32 %329, %328
  store i32 %add244, i32* %tmp12, align 4, !tbaa !15
  %330 = load i32, i32* %z3, align 4, !tbaa !15
  %331 = load i32, i32* %z4, align 4, !tbaa !15
  %add245 = add nsw i32 %330, %331
  %mul246 = mul nsw i32 %add245, -11086
  store i32 %mul246, i32* %z2, align 4, !tbaa !15
  %332 = load i32, i32* %z2, align 4, !tbaa !15
  %333 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add247 = add nsw i32 %333, %332
  store i32 %add247, i32* %tmp2, align 4, !tbaa !15
  %334 = load i32, i32* %z2, align 4, !tbaa !15
  %335 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add248 = add nsw i32 %335, %334
  store i32 %add248, i32* %tmp3, align 4, !tbaa !15
  %336 = load i32, i32* %z4, align 4, !tbaa !15
  %337 = load i32, i32* %z3, align 4, !tbaa !15
  %sub249 = sub nsw i32 %336, %337
  %mul250 = mul nsw i32 %sub249, 3363
  store i32 %mul250, i32* %z2, align 4, !tbaa !15
  %338 = load i32, i32* %z2, align 4, !tbaa !15
  %339 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add251 = add nsw i32 %339, %338
  store i32 %add251, i32* %tmp10, align 4, !tbaa !15
  %340 = load i32, i32* %z2, align 4, !tbaa !15
  %341 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add252 = add nsw i32 %341, %340
  store i32 %add252, i32* %tmp11, align 4, !tbaa !15
  %342 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %343 = load i32, i32* %tmp20, align 4, !tbaa !15
  %344 = load i32, i32* %tmp0, align 4, !tbaa !15
  %add253 = add nsw i32 %343, %344
  %shr254 = ashr i32 %add253, 18
  %and = and i32 %shr254, 1023
  %arrayidx255 = getelementptr inbounds i8, i8* %342, i32 %and
  %345 = load i8, i8* %arrayidx255, align 1, !tbaa !17
  %346 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx256 = getelementptr inbounds i8, i8* %346, i32 0
  store i8 %345, i8* %arrayidx256, align 1, !tbaa !17
  %347 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %348 = load i32, i32* %tmp20, align 4, !tbaa !15
  %349 = load i32, i32* %tmp0, align 4, !tbaa !15
  %sub257 = sub nsw i32 %348, %349
  %shr258 = ashr i32 %sub257, 18
  %and259 = and i32 %shr258, 1023
  %arrayidx260 = getelementptr inbounds i8, i8* %347, i32 %and259
  %350 = load i8, i8* %arrayidx260, align 1, !tbaa !17
  %351 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx261 = getelementptr inbounds i8, i8* %351, i32 15
  store i8 %350, i8* %arrayidx261, align 1, !tbaa !17
  %352 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %353 = load i32, i32* %tmp21, align 4, !tbaa !15
  %354 = load i32, i32* %tmp1, align 4, !tbaa !15
  %add262 = add nsw i32 %353, %354
  %shr263 = ashr i32 %add262, 18
  %and264 = and i32 %shr263, 1023
  %arrayidx265 = getelementptr inbounds i8, i8* %352, i32 %and264
  %355 = load i8, i8* %arrayidx265, align 1, !tbaa !17
  %356 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx266 = getelementptr inbounds i8, i8* %356, i32 1
  store i8 %355, i8* %arrayidx266, align 1, !tbaa !17
  %357 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %358 = load i32, i32* %tmp21, align 4, !tbaa !15
  %359 = load i32, i32* %tmp1, align 4, !tbaa !15
  %sub267 = sub nsw i32 %358, %359
  %shr268 = ashr i32 %sub267, 18
  %and269 = and i32 %shr268, 1023
  %arrayidx270 = getelementptr inbounds i8, i8* %357, i32 %and269
  %360 = load i8, i8* %arrayidx270, align 1, !tbaa !17
  %361 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx271 = getelementptr inbounds i8, i8* %361, i32 14
  store i8 %360, i8* %arrayidx271, align 1, !tbaa !17
  %362 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %363 = load i32, i32* %tmp22, align 4, !tbaa !15
  %364 = load i32, i32* %tmp2, align 4, !tbaa !15
  %add272 = add nsw i32 %363, %364
  %shr273 = ashr i32 %add272, 18
  %and274 = and i32 %shr273, 1023
  %arrayidx275 = getelementptr inbounds i8, i8* %362, i32 %and274
  %365 = load i8, i8* %arrayidx275, align 1, !tbaa !17
  %366 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx276 = getelementptr inbounds i8, i8* %366, i32 2
  store i8 %365, i8* %arrayidx276, align 1, !tbaa !17
  %367 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %368 = load i32, i32* %tmp22, align 4, !tbaa !15
  %369 = load i32, i32* %tmp2, align 4, !tbaa !15
  %sub277 = sub nsw i32 %368, %369
  %shr278 = ashr i32 %sub277, 18
  %and279 = and i32 %shr278, 1023
  %arrayidx280 = getelementptr inbounds i8, i8* %367, i32 %and279
  %370 = load i8, i8* %arrayidx280, align 1, !tbaa !17
  %371 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx281 = getelementptr inbounds i8, i8* %371, i32 13
  store i8 %370, i8* %arrayidx281, align 1, !tbaa !17
  %372 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %373 = load i32, i32* %tmp23, align 4, !tbaa !15
  %374 = load i32, i32* %tmp3, align 4, !tbaa !15
  %add282 = add nsw i32 %373, %374
  %shr283 = ashr i32 %add282, 18
  %and284 = and i32 %shr283, 1023
  %arrayidx285 = getelementptr inbounds i8, i8* %372, i32 %and284
  %375 = load i8, i8* %arrayidx285, align 1, !tbaa !17
  %376 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx286 = getelementptr inbounds i8, i8* %376, i32 3
  store i8 %375, i8* %arrayidx286, align 1, !tbaa !17
  %377 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %378 = load i32, i32* %tmp23, align 4, !tbaa !15
  %379 = load i32, i32* %tmp3, align 4, !tbaa !15
  %sub287 = sub nsw i32 %378, %379
  %shr288 = ashr i32 %sub287, 18
  %and289 = and i32 %shr288, 1023
  %arrayidx290 = getelementptr inbounds i8, i8* %377, i32 %and289
  %380 = load i8, i8* %arrayidx290, align 1, !tbaa !17
  %381 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx291 = getelementptr inbounds i8, i8* %381, i32 12
  store i8 %380, i8* %arrayidx291, align 1, !tbaa !17
  %382 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %383 = load i32, i32* %tmp24, align 4, !tbaa !15
  %384 = load i32, i32* %tmp10, align 4, !tbaa !15
  %add292 = add nsw i32 %383, %384
  %shr293 = ashr i32 %add292, 18
  %and294 = and i32 %shr293, 1023
  %arrayidx295 = getelementptr inbounds i8, i8* %382, i32 %and294
  %385 = load i8, i8* %arrayidx295, align 1, !tbaa !17
  %386 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx296 = getelementptr inbounds i8, i8* %386, i32 4
  store i8 %385, i8* %arrayidx296, align 1, !tbaa !17
  %387 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %388 = load i32, i32* %tmp24, align 4, !tbaa !15
  %389 = load i32, i32* %tmp10, align 4, !tbaa !15
  %sub297 = sub nsw i32 %388, %389
  %shr298 = ashr i32 %sub297, 18
  %and299 = and i32 %shr298, 1023
  %arrayidx300 = getelementptr inbounds i8, i8* %387, i32 %and299
  %390 = load i8, i8* %arrayidx300, align 1, !tbaa !17
  %391 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx301 = getelementptr inbounds i8, i8* %391, i32 11
  store i8 %390, i8* %arrayidx301, align 1, !tbaa !17
  %392 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %393 = load i32, i32* %tmp25, align 4, !tbaa !15
  %394 = load i32, i32* %tmp11, align 4, !tbaa !15
  %add302 = add nsw i32 %393, %394
  %shr303 = ashr i32 %add302, 18
  %and304 = and i32 %shr303, 1023
  %arrayidx305 = getelementptr inbounds i8, i8* %392, i32 %and304
  %395 = load i8, i8* %arrayidx305, align 1, !tbaa !17
  %396 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx306 = getelementptr inbounds i8, i8* %396, i32 5
  store i8 %395, i8* %arrayidx306, align 1, !tbaa !17
  %397 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %398 = load i32, i32* %tmp25, align 4, !tbaa !15
  %399 = load i32, i32* %tmp11, align 4, !tbaa !15
  %sub307 = sub nsw i32 %398, %399
  %shr308 = ashr i32 %sub307, 18
  %and309 = and i32 %shr308, 1023
  %arrayidx310 = getelementptr inbounds i8, i8* %397, i32 %and309
  %400 = load i8, i8* %arrayidx310, align 1, !tbaa !17
  %401 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx311 = getelementptr inbounds i8, i8* %401, i32 10
  store i8 %400, i8* %arrayidx311, align 1, !tbaa !17
  %402 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %403 = load i32, i32* %tmp26, align 4, !tbaa !15
  %404 = load i32, i32* %tmp12, align 4, !tbaa !15
  %add312 = add nsw i32 %403, %404
  %shr313 = ashr i32 %add312, 18
  %and314 = and i32 %shr313, 1023
  %arrayidx315 = getelementptr inbounds i8, i8* %402, i32 %and314
  %405 = load i8, i8* %arrayidx315, align 1, !tbaa !17
  %406 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx316 = getelementptr inbounds i8, i8* %406, i32 6
  store i8 %405, i8* %arrayidx316, align 1, !tbaa !17
  %407 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %408 = load i32, i32* %tmp26, align 4, !tbaa !15
  %409 = load i32, i32* %tmp12, align 4, !tbaa !15
  %sub317 = sub nsw i32 %408, %409
  %shr318 = ashr i32 %sub317, 18
  %and319 = and i32 %shr318, 1023
  %arrayidx320 = getelementptr inbounds i8, i8* %407, i32 %and319
  %410 = load i8, i8* %arrayidx320, align 1, !tbaa !17
  %411 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx321 = getelementptr inbounds i8, i8* %411, i32 9
  store i8 %410, i8* %arrayidx321, align 1, !tbaa !17
  %412 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %413 = load i32, i32* %tmp27, align 4, !tbaa !15
  %414 = load i32, i32* %tmp13, align 4, !tbaa !15
  %add322 = add nsw i32 %413, %414
  %shr323 = ashr i32 %add322, 18
  %and324 = and i32 %shr323, 1023
  %arrayidx325 = getelementptr inbounds i8, i8* %412, i32 %and324
  %415 = load i8, i8* %arrayidx325, align 1, !tbaa !17
  %416 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx326 = getelementptr inbounds i8, i8* %416, i32 7
  store i8 %415, i8* %arrayidx326, align 1, !tbaa !17
  %417 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %418 = load i32, i32* %tmp27, align 4, !tbaa !15
  %419 = load i32, i32* %tmp13, align 4, !tbaa !15
  %sub327 = sub nsw i32 %418, %419
  %shr328 = ashr i32 %sub327, 18
  %and329 = and i32 %shr328, 1023
  %arrayidx330 = getelementptr inbounds i8, i8* %417, i32 %and329
  %420 = load i8, i8* %arrayidx330, align 1, !tbaa !17
  %421 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx331 = getelementptr inbounds i8, i8* %421, i32 8
  store i8 %420, i8* %arrayidx331, align 1, !tbaa !17
  %422 = load i32*, i32** %wsptr, align 4, !tbaa !2
  %add.ptr332 = getelementptr inbounds i32, i32* %422, i32 8
  store i32* %add.ptr332, i32** %wsptr, align 4, !tbaa !2
  br label %for.inc333

for.inc333:                                       ; preds = %for.body161
  %423 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc334 = add nsw i32 %423, 1
  store i32 %inc334, i32* %ctr, align 4, !tbaa !6
  br label %for.cond158

for.end335:                                       ; preds = %for.cond158
  %424 = bitcast [128 x i32]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 512, i8* %424) #2
  %425 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %425) #2
  %426 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %426) #2
  %427 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %427) #2
  %428 = bitcast i32** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %428) #2
  %429 = bitcast i32** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %429) #2
  %430 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %430) #2
  %431 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %431) #2
  %432 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %432) #2
  %433 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %433) #2
  %434 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %434) #2
  %435 = bitcast i32* %tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %435) #2
  %436 = bitcast i32* %tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %436) #2
  %437 = bitcast i32* %tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %437) #2
  %438 = bitcast i32* %tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %438) #2
  %439 = bitcast i32* %tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %439) #2
  %440 = bitcast i32* %tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %440) #2
  %441 = bitcast i32* %tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %441) #2
  %442 = bitcast i32* %tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %442) #2
  %443 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %443) #2
  %444 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %444) #2
  %445 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %445) #2
  %446 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %446) #2
  %447 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %447) #2
  %448 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %448) #2
  %449 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %449) #2
  %450 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %450) #2
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 324}
!9 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !10, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !11, i64 288, !11, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 80}
!13 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!14 = !{!11, !11, i64 0}
!15 = !{!16, !16, i64 0}
!16 = !{!"long", !4, i64 0}
!17 = !{!4, !4, i64 0}
