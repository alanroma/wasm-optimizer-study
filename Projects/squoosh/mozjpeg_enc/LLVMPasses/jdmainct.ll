; ModuleID = 'jdmainct.c'
source_filename = "jdmainct.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { {}*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.my_main_controller = type { %struct.jpeg_d_main_controller, [10 x i8**], i32, i32, [2 x i8***], i32, i32, i32, i32 }

; Function Attrs: nounwind
define hidden void @jinit_d_main_controller(%struct.jpeg_decompress_struct* %cinfo, i32 %need_full_buffer) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %need_full_buffer.addr = alloca i32, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %ci = alloca i32, align 4
  %rgroup = alloca i32, align 4
  %ngroups = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %need_full_buffer, i32* %need_full_buffer.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %ngroups to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 1
  %6 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %6, i32 0, i32 0
  %7 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = bitcast %struct.jpeg_decompress_struct* %8 to %struct.jpeg_common_struct*
  %call = call i8* %7(%struct.jpeg_common_struct* %9, i32 1, i32 80)
  %10 = bitcast i8* %call to %struct.my_main_controller*
  store %struct.my_main_controller* %10, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %11 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %12 = bitcast %struct.my_main_controller* %11 to %struct.jpeg_d_main_controller*
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 78
  store %struct.jpeg_d_main_controller* %12, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !15
  %14 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %14, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_d_main_controller, %struct.jpeg_d_main_controller* %pub, i32 0, i32 0
  store void (%struct.jpeg_decompress_struct*, i32)* @start_pass_main, void (%struct.jpeg_decompress_struct*, i32)** %start_pass, align 4, !tbaa !16
  %15 = load i32, i32* %need_full_buffer.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %15, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !19
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 5
  store i32 4, i32* %msg_code, align 4, !tbaa !20
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !19
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 0
  %20 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !22
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %22 = bitcast %struct.jpeg_decompress_struct* %21 to %struct.jpeg_common_struct*
  call void %20(%struct.jpeg_common_struct* %22)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 85
  %24 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample, align 4, !tbaa !23
  %need_context_rows = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %24, i32 0, i32 2
  %25 = load i32, i32* %need_context_rows, align 4, !tbaa !24
  %tobool2 = icmp ne i32 %25, 0
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 63
  %27 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !26
  %cmp = icmp slt i32 %27, 2
  br i1 %cmp, label %if.then4, label %if.end9

if.then4:                                         ; preds = %if.then3
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 0
  %29 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !19
  %msg_code6 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %29, i32 0, i32 5
  store i32 47, i32* %msg_code6, align 4, !tbaa !20
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 0
  %31 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err7, align 8, !tbaa !19
  %error_exit8 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %31, i32 0, i32 0
  %32 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit8, align 4, !tbaa !22
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %34 = bitcast %struct.jpeg_decompress_struct* %33 to %struct.jpeg_common_struct*
  call void %32(%struct.jpeg_common_struct* %34)
  br label %if.end9

if.end9:                                          ; preds = %if.then4, %if.then3
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @alloc_funny_pointers(%struct.jpeg_decompress_struct* %35)
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 63
  %37 = load i32, i32* %min_DCT_scaled_size10, align 4, !tbaa !26
  %add = add nsw i32 %37, 2
  store i32 %add, i32* %ngroups, align 4, !tbaa !6
  br label %if.end12

if.else:                                          ; preds = %if.end
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 63
  %39 = load i32, i32* %min_DCT_scaled_size11, align 4, !tbaa !26
  store i32 %39, i32* %ngroups, align 4, !tbaa !6
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.end9
  store i32 0, i32* %ci, align 4, !tbaa !6
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 44
  %41 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !27
  store %struct.jpeg_component_info* %41, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end12
  %42 = load i32, i32* %ci, align 4, !tbaa !6
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 9
  %44 = load i32, i32* %num_components, align 4, !tbaa !28
  %cmp13 = icmp slt i32 %42, %44
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %45 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %45, i32 0, i32 3
  %46 = load i32, i32* %v_samp_factor, align 4, !tbaa !29
  %47 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %47, i32 0, i32 9
  %48 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !31
  %mul = mul nsw i32 %46, %48
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 63
  %50 = load i32, i32* %min_DCT_scaled_size14, align 4, !tbaa !26
  %div = sdiv i32 %mul, %50
  store i32 %div, i32* %rgroup, align 4, !tbaa !6
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 1
  %52 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem15, align 4, !tbaa !8
  %alloc_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %52, i32 0, i32 2
  %53 = load i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_sarray, align 4, !tbaa !32
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %55 = bitcast %struct.jpeg_decompress_struct* %54 to %struct.jpeg_common_struct*
  %56 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %56, i32 0, i32 7
  %57 = load i32, i32* %width_in_blocks, align 4, !tbaa !33
  %58 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size16 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %58, i32 0, i32 9
  %59 = load i32, i32* %DCT_scaled_size16, align 4, !tbaa !31
  %mul17 = mul i32 %57, %59
  %60 = load i32, i32* %rgroup, align 4, !tbaa !6
  %61 = load i32, i32* %ngroups, align 4, !tbaa !6
  %mul18 = mul nsw i32 %60, %61
  %call19 = call i8** %53(%struct.jpeg_common_struct* %55, i32 1, i32 %mul17, i32 %mul18)
  %62 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %62, i32 0, i32 1
  %63 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x i8**], [10 x i8**]* %buffer, i32 0, i32 %63
  store i8** %call19, i8*** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %64 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %65 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %65, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %66 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #2
  %67 = bitcast i32* %ngroups to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #2
  %68 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #2
  %69 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #2
  %70 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_main(%struct.jpeg_decompress_struct* %cinfo, i32 %pass_mode) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %pass_mode.addr = alloca i32, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pass_mode, i32* %pass_mode.addr, align 4, !tbaa !34
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = load i32, i32* %pass_mode.addr, align 4, !tbaa !34
  switch i32 %4, label %sw.default [
    i32 0, label %sw.bb
    i32 2, label %sw.bb3
  ]

sw.bb:                                            ; preds = %entry
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 85
  %6 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample, align 4, !tbaa !23
  %need_context_rows = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %6, i32 0, i32 2
  %7 = load i32, i32* %need_context_rows, align 4, !tbaa !24
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb
  %8 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %8, i32 0, i32 0
  %process_data = getelementptr inbounds %struct.jpeg_d_main_controller, %struct.jpeg_d_main_controller* %pub, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* @process_data_context_main, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)** %process_data, align 4, !tbaa !35
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @make_funny_pointers(%struct.jpeg_decompress_struct* %9)
  %10 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %whichptr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %10, i32 0, i32 5
  store i32 0, i32* %whichptr, align 4, !tbaa !36
  %11 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %context_state = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %11, i32 0, i32 6
  store i32 0, i32* %context_state, align 4, !tbaa !37
  %12 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %iMCU_row_ctr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %12, i32 0, i32 8
  store i32 0, i32* %iMCU_row_ctr, align 4, !tbaa !38
  br label %if.end

if.else:                                          ; preds = %sw.bb
  %13 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %pub1 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %13, i32 0, i32 0
  %process_data2 = getelementptr inbounds %struct.jpeg_d_main_controller, %struct.jpeg_d_main_controller* %pub1, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* @process_data_simple_main, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)** %process_data2, align 4, !tbaa !35
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %14 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %14, i32 0, i32 2
  store i32 0, i32* %buffer_full, align 4, !tbaa !39
  %15 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %15, i32 0, i32 3
  store i32 0, i32* %rowgroup_ctr, align 4, !tbaa !40
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  %16 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %pub4 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %16, i32 0, i32 0
  %process_data5 = getelementptr inbounds %struct.jpeg_d_main_controller, %struct.jpeg_d_main_controller* %pub4, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* @process_data_crank_post, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)** %process_data5, align 4, !tbaa !35
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !19
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 5
  store i32 4, i32* %msg_code, align 4, !tbaa !20
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !19
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 0
  %21 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !22
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = bitcast %struct.jpeg_decompress_struct* %22 to %struct.jpeg_common_struct*
  call void %21(%struct.jpeg_common_struct* %23)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb3, %if.end
  %24 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #2
  ret void
}

; Function Attrs: nounwind
define internal void @alloc_funny_pointers(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %ci = alloca i32, align 4
  %rgroup = alloca i32, align 4
  %M = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %xbuf = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %M to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 63
  %8 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !26
  store i32 %8, i32* %M, align 4, !tbaa !6
  %9 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i8*** %xbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 1
  %12 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %12, i32 0, i32 0
  %13 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = bitcast %struct.jpeg_decompress_struct* %14 to %struct.jpeg_common_struct*
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 9
  %17 = load i32, i32* %num_components, align 4, !tbaa !28
  %mul = mul nsw i32 %17, 2
  %mul1 = mul i32 %mul, 4
  %call = call i8* %13(%struct.jpeg_common_struct* %15, i32 1, i32 %mul1)
  %18 = bitcast i8* %call to i8***
  %19 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %19, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer, i32 0, i32 0
  store i8*** %18, i8**** %arrayidx, align 4, !tbaa !2
  %20 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer2 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %20, i32 0, i32 4
  %arrayidx3 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer2, i32 0, i32 0
  %21 = load i8***, i8**** %arrayidx3, align 4, !tbaa !2
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 9
  %23 = load i32, i32* %num_components4, align 4, !tbaa !28
  %add.ptr = getelementptr inbounds i8**, i8*** %21, i32 %23
  %24 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer5 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %24, i32 0, i32 4
  %arrayidx6 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer5, i32 0, i32 1
  store i8*** %add.ptr, i8**** %arrayidx6, align 4, !tbaa !2
  store i32 0, i32* %ci, align 4, !tbaa !6
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 44
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !27
  store %struct.jpeg_component_info* %26, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %27 = load i32, i32* %ci, align 4, !tbaa !6
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 9
  %29 = load i32, i32* %num_components7, align 4, !tbaa !28
  %cmp = icmp slt i32 %27, %29
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %30 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %30, i32 0, i32 3
  %31 = load i32, i32* %v_samp_factor, align 4, !tbaa !29
  %32 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %32, i32 0, i32 9
  %33 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !31
  %mul8 = mul nsw i32 %31, %33
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 63
  %35 = load i32, i32* %min_DCT_scaled_size9, align 4, !tbaa !26
  %div = sdiv i32 %mul8, %35
  store i32 %div, i32* %rgroup, align 4, !tbaa !6
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 1
  %37 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem10, align 4, !tbaa !8
  %alloc_small11 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %37, i32 0, i32 0
  %38 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small11, align 4, !tbaa !12
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = bitcast %struct.jpeg_decompress_struct* %39 to %struct.jpeg_common_struct*
  %41 = load i32, i32* %rgroup, align 4, !tbaa !6
  %42 = load i32, i32* %M, align 4, !tbaa !6
  %add = add nsw i32 %42, 4
  %mul12 = mul nsw i32 %41, %add
  %mul13 = mul nsw i32 2, %mul12
  %mul14 = mul i32 %mul13, 4
  %call15 = call i8* %38(%struct.jpeg_common_struct* %40, i32 1, i32 %mul14)
  %43 = bitcast i8* %call15 to i8**
  store i8** %43, i8*** %xbuf, align 4, !tbaa !2
  %44 = load i32, i32* %rgroup, align 4, !tbaa !6
  %45 = load i8**, i8*** %xbuf, align 4, !tbaa !2
  %add.ptr16 = getelementptr inbounds i8*, i8** %45, i32 %44
  store i8** %add.ptr16, i8*** %xbuf, align 4, !tbaa !2
  %46 = load i8**, i8*** %xbuf, align 4, !tbaa !2
  %47 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer17 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %47, i32 0, i32 4
  %arrayidx18 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer17, i32 0, i32 0
  %48 = load i8***, i8**** %arrayidx18, align 4, !tbaa !2
  %49 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds i8**, i8*** %48, i32 %49
  store i8** %46, i8*** %arrayidx19, align 4, !tbaa !2
  %50 = load i32, i32* %rgroup, align 4, !tbaa !6
  %51 = load i32, i32* %M, align 4, !tbaa !6
  %add20 = add nsw i32 %51, 4
  %mul21 = mul nsw i32 %50, %add20
  %52 = load i8**, i8*** %xbuf, align 4, !tbaa !2
  %add.ptr22 = getelementptr inbounds i8*, i8** %52, i32 %mul21
  store i8** %add.ptr22, i8*** %xbuf, align 4, !tbaa !2
  %53 = load i8**, i8*** %xbuf, align 4, !tbaa !2
  %54 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer23 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %54, i32 0, i32 4
  %arrayidx24 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer23, i32 0, i32 1
  %55 = load i8***, i8**** %arrayidx24, align 4, !tbaa !2
  %56 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds i8**, i8*** %55, i32 %56
  store i8** %53, i8*** %arrayidx25, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %57 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %57, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %58 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %58, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %59 = bitcast i8*** %xbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #2
  %60 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #2
  %61 = bitcast i32* %M to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #2
  %62 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #2
  %63 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #2
  %64 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @process_data_context_main(%struct.jpeg_decompress_struct* %cinfo, i8** %output_buf, i32* %out_row_ctr, i32 %out_rows_avail) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %output_buf.addr = alloca i8**, align 4
  %out_row_ctr.addr = alloca i32*, align 4
  %out_rows_avail.addr = alloca i32, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32* %out_row_ctr, i32** %out_row_ctr.addr, align 4, !tbaa !2
  store i32 %out_rows_avail, i32* %out_rows_avail.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %4, i32 0, i32 2
  %5 = load i32, i32* %buffer_full, align 4, !tbaa !39
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.end4, label %if.then

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 79
  %7 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef, align 4, !tbaa !41
  %decompress_data = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %7, i32 0, i32 3
  %8 = load i32 (%struct.jpeg_decompress_struct*, i8***)*, i32 (%struct.jpeg_decompress_struct*, i8***)** %decompress_data, align 4, !tbaa !42
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %10 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %10, i32 0, i32 4
  %11 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %whichptr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %11, i32 0, i32 5
  %12 = load i32, i32* %whichptr, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer, i32 0, i32 %12
  %13 = load i8***, i8**** %arrayidx, align 4, !tbaa !2
  %call = call i32 %8(%struct.jpeg_decompress_struct* %9, i8*** %13)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.end, label %if.then2

if.then2:                                         ; preds = %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %14 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full3 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %14, i32 0, i32 2
  store i32 1, i32* %buffer_full3, align 4, !tbaa !39
  %15 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %iMCU_row_ctr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %15, i32 0, i32 8
  %16 = load i32, i32* %iMCU_row_ctr, align 4, !tbaa !38
  %inc = add i32 %16, 1
  store i32 %inc, i32* %iMCU_row_ctr, align 4, !tbaa !38
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  %17 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %context_state = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %17, i32 0, i32 6
  %18 = load i32, i32* %context_state, align 4, !tbaa !37
  switch i32 %18, label %sw.epilog [
    i32 2, label %sw.bb
    i32 0, label %sw.bb16
    i32 1, label %sw.bb24
  ]

sw.bb:                                            ; preds = %if.end4
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %post = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 80
  %20 = load %struct.jpeg_d_post_controller*, %struct.jpeg_d_post_controller** %post, align 8, !tbaa !44
  %post_process_data = getelementptr inbounds %struct.jpeg_d_post_controller, %struct.jpeg_d_post_controller* %20, i32 0, i32 1
  %21 = load void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)** %post_process_data, align 4, !tbaa !45
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer5 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %23, i32 0, i32 4
  %24 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %whichptr6 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %24, i32 0, i32 5
  %25 = load i32, i32* %whichptr6, align 4, !tbaa !36
  %arrayidx7 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer5, i32 0, i32 %25
  %26 = load i8***, i8**** %arrayidx7, align 4, !tbaa !2
  %27 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %27, i32 0, i32 3
  %28 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroups_avail = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %28, i32 0, i32 7
  %29 = load i32, i32* %rowgroups_avail, align 4, !tbaa !47
  %30 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %31 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %32 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !6
  call void %21(%struct.jpeg_decompress_struct* %22, i8*** %26, i32* %rowgroup_ctr, i32 %29, i8** %30, i32* %31, i32 %32)
  %33 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr8 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %33, i32 0, i32 3
  %34 = load i32, i32* %rowgroup_ctr8, align 4, !tbaa !40
  %35 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroups_avail9 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %35, i32 0, i32 7
  %36 = load i32, i32* %rowgroups_avail9, align 4, !tbaa !47
  %cmp = icmp ult i32 %34, %36
  br i1 %cmp, label %if.then10, label %if.end11

if.then10:                                        ; preds = %sw.bb
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %sw.bb
  %37 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %context_state12 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %37, i32 0, i32 6
  store i32 0, i32* %context_state12, align 4, !tbaa !37
  %38 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %39 = load i32, i32* %38, align 4, !tbaa !6
  %40 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !6
  %cmp13 = icmp uge i32 %39, %40
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end11
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %if.end11
  br label %sw.bb16

sw.bb16:                                          ; preds = %if.end4, %if.end15
  %41 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr17 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %41, i32 0, i32 3
  store i32 0, i32* %rowgroup_ctr17, align 4, !tbaa !40
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 63
  %43 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !26
  %sub = sub nsw i32 %43, 1
  %44 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroups_avail18 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %44, i32 0, i32 7
  store i32 %sub, i32* %rowgroups_avail18, align 4, !tbaa !47
  %45 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %iMCU_row_ctr19 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %45, i32 0, i32 8
  %46 = load i32, i32* %iMCU_row_ctr19, align 4, !tbaa !38
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 64
  %48 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !48
  %cmp20 = icmp eq i32 %46, %48
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %sw.bb16
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @set_bottom_pointers(%struct.jpeg_decompress_struct* %49)
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %sw.bb16
  %50 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %context_state23 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %50, i32 0, i32 6
  store i32 1, i32* %context_state23, align 4, !tbaa !37
  br label %sw.bb24

sw.bb24:                                          ; preds = %if.end4, %if.end22
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %post25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 80
  %52 = load %struct.jpeg_d_post_controller*, %struct.jpeg_d_post_controller** %post25, align 8, !tbaa !44
  %post_process_data26 = getelementptr inbounds %struct.jpeg_d_post_controller, %struct.jpeg_d_post_controller* %52, i32 0, i32 1
  %53 = load void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)** %post_process_data26, align 4, !tbaa !45
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %55 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer27 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %55, i32 0, i32 4
  %56 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %whichptr28 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %56, i32 0, i32 5
  %57 = load i32, i32* %whichptr28, align 4, !tbaa !36
  %arrayidx29 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer27, i32 0, i32 %57
  %58 = load i8***, i8**** %arrayidx29, align 4, !tbaa !2
  %59 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr30 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %59, i32 0, i32 3
  %60 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroups_avail31 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %60, i32 0, i32 7
  %61 = load i32, i32* %rowgroups_avail31, align 4, !tbaa !47
  %62 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %63 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %64 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !6
  call void %53(%struct.jpeg_decompress_struct* %54, i8*** %58, i32* %rowgroup_ctr30, i32 %61, i8** %62, i32* %63, i32 %64)
  %65 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr32 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %65, i32 0, i32 3
  %66 = load i32, i32* %rowgroup_ctr32, align 4, !tbaa !40
  %67 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroups_avail33 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %67, i32 0, i32 7
  %68 = load i32, i32* %rowgroups_avail33, align 4, !tbaa !47
  %cmp34 = icmp ult i32 %66, %68
  br i1 %cmp34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %sw.bb24
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end36:                                         ; preds = %sw.bb24
  %69 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %iMCU_row_ctr37 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %69, i32 0, i32 8
  %70 = load i32, i32* %iMCU_row_ctr37, align 4, !tbaa !38
  %cmp38 = icmp eq i32 %70, 1
  br i1 %cmp38, label %if.then39, label %if.end40

if.then39:                                        ; preds = %if.end36
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @set_wraparound_pointers(%struct.jpeg_decompress_struct* %71)
  br label %if.end40

if.end40:                                         ; preds = %if.then39, %if.end36
  %72 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %whichptr41 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %72, i32 0, i32 5
  %73 = load i32, i32* %whichptr41, align 4, !tbaa !36
  %xor = xor i32 %73, 1
  store i32 %xor, i32* %whichptr41, align 4, !tbaa !36
  %74 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full42 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %74, i32 0, i32 2
  store i32 0, i32* %buffer_full42, align 4, !tbaa !39
  %75 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size43 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %75, i32 0, i32 63
  %76 = load i32, i32* %min_DCT_scaled_size43, align 4, !tbaa !26
  %add = add nsw i32 %76, 1
  %77 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr44 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %77, i32 0, i32 3
  store i32 %add, i32* %rowgroup_ctr44, align 4, !tbaa !40
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size45 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %78, i32 0, i32 63
  %79 = load i32, i32* %min_DCT_scaled_size45, align 4, !tbaa !26
  %add46 = add nsw i32 %79, 2
  %80 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroups_avail47 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %80, i32 0, i32 7
  store i32 %add46, i32* %rowgroups_avail47, align 4, !tbaa !47
  %81 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %context_state48 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %81, i32 0, i32 6
  store i32 2, i32* %context_state48, align 4, !tbaa !37
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end40, %if.end4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %sw.epilog, %if.then35, %if.then14, %if.then10, %if.then2
  %82 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #2
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @make_funny_pointers(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %ci = alloca i32, align 4
  %i = alloca i32, align 4
  %rgroup = alloca i32, align 4
  %M = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %buf = alloca i8**, align 4
  %xbuf0 = alloca i8**, align 4
  %xbuf1 = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %M to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 63
  %9 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !26
  store i32 %9, i32* %M, align 4, !tbaa !6
  %10 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i8*** %buf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i8*** %xbuf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i8*** %xbuf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  store i32 0, i32* %ci, align 4, !tbaa !6
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 44
  %15 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !27
  store %struct.jpeg_component_info* %15, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc43, %entry
  %16 = load i32, i32* %ci, align 4, !tbaa !6
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 9
  %18 = load i32, i32* %num_components, align 4, !tbaa !28
  %cmp = icmp slt i32 %16, %18
  br i1 %cmp, label %for.body, label %for.end45

for.body:                                         ; preds = %for.cond
  %19 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %19, i32 0, i32 3
  %20 = load i32, i32* %v_samp_factor, align 4, !tbaa !29
  %21 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %21, i32 0, i32 9
  %22 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !31
  %mul = mul nsw i32 %20, %22
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 63
  %24 = load i32, i32* %min_DCT_scaled_size1, align 4, !tbaa !26
  %div = sdiv i32 %mul, %24
  store i32 %div, i32* %rgroup, align 4, !tbaa !6
  %25 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %25, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer, i32 0, i32 0
  %26 = load i8***, i8**** %arrayidx, align 4, !tbaa !2
  %27 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8**, i8*** %26, i32 %27
  %28 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  store i8** %28, i8*** %xbuf0, align 4, !tbaa !2
  %29 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer3 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %29, i32 0, i32 4
  %arrayidx4 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer3, i32 0, i32 1
  %30 = load i8***, i8**** %arrayidx4, align 4, !tbaa !2
  %31 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8**, i8*** %30, i32 %31
  %32 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  store i8** %32, i8*** %xbuf1, align 4, !tbaa !2
  %33 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %33, i32 0, i32 1
  %34 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds [10 x i8**], [10 x i8**]* %buffer, i32 0, i32 %34
  %35 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  store i8** %35, i8*** %buf, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body
  %36 = load i32, i32* %i, align 4, !tbaa !6
  %37 = load i32, i32* %rgroup, align 4, !tbaa !6
  %38 = load i32, i32* %M, align 4, !tbaa !6
  %add = add nsw i32 %38, 2
  %mul8 = mul nsw i32 %37, %add
  %cmp9 = icmp slt i32 %36, %mul8
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond7
  %39 = load i8**, i8*** %buf, align 4, !tbaa !2
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx11 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  %42 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i8*, i8** %42, i32 %43
  store i8* %41, i8** %arrayidx12, align 4, !tbaa !2
  %44 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx13 = getelementptr inbounds i8*, i8** %44, i32 %45
  store i8* %41, i8** %arrayidx13, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc31, %for.end
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %48 = load i32, i32* %rgroup, align 4, !tbaa !6
  %mul15 = mul nsw i32 %48, 2
  %cmp16 = icmp slt i32 %47, %mul15
  br i1 %cmp16, label %for.body17, label %for.end33

for.body17:                                       ; preds = %for.cond14
  %49 = load i8**, i8*** %buf, align 4, !tbaa !2
  %50 = load i32, i32* %rgroup, align 4, !tbaa !6
  %51 = load i32, i32* %M, align 4, !tbaa !6
  %mul18 = mul nsw i32 %50, %51
  %52 = load i32, i32* %i, align 4, !tbaa !6
  %add19 = add nsw i32 %mul18, %52
  %arrayidx20 = getelementptr inbounds i8*, i8** %49, i32 %add19
  %53 = load i8*, i8** %arrayidx20, align 4, !tbaa !2
  %54 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %55 = load i32, i32* %rgroup, align 4, !tbaa !6
  %56 = load i32, i32* %M, align 4, !tbaa !6
  %sub = sub nsw i32 %56, 2
  %mul21 = mul nsw i32 %55, %sub
  %57 = load i32, i32* %i, align 4, !tbaa !6
  %add22 = add nsw i32 %mul21, %57
  %arrayidx23 = getelementptr inbounds i8*, i8** %54, i32 %add22
  store i8* %53, i8** %arrayidx23, align 4, !tbaa !2
  %58 = load i8**, i8*** %buf, align 4, !tbaa !2
  %59 = load i32, i32* %rgroup, align 4, !tbaa !6
  %60 = load i32, i32* %M, align 4, !tbaa !6
  %sub24 = sub nsw i32 %60, 2
  %mul25 = mul nsw i32 %59, %sub24
  %61 = load i32, i32* %i, align 4, !tbaa !6
  %add26 = add nsw i32 %mul25, %61
  %arrayidx27 = getelementptr inbounds i8*, i8** %58, i32 %add26
  %62 = load i8*, i8** %arrayidx27, align 4, !tbaa !2
  %63 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %64 = load i32, i32* %rgroup, align 4, !tbaa !6
  %65 = load i32, i32* %M, align 4, !tbaa !6
  %mul28 = mul nsw i32 %64, %65
  %66 = load i32, i32* %i, align 4, !tbaa !6
  %add29 = add nsw i32 %mul28, %66
  %arrayidx30 = getelementptr inbounds i8*, i8** %63, i32 %add29
  store i8* %62, i8** %arrayidx30, align 4, !tbaa !2
  br label %for.inc31

for.inc31:                                        ; preds = %for.body17
  %67 = load i32, i32* %i, align 4, !tbaa !6
  %inc32 = add nsw i32 %67, 1
  store i32 %inc32, i32* %i, align 4, !tbaa !6
  br label %for.cond14

for.end33:                                        ; preds = %for.cond14
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc40, %for.end33
  %68 = load i32, i32* %i, align 4, !tbaa !6
  %69 = load i32, i32* %rgroup, align 4, !tbaa !6
  %cmp35 = icmp slt i32 %68, %69
  br i1 %cmp35, label %for.body36, label %for.end42

for.body36:                                       ; preds = %for.cond34
  %70 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i8*, i8** %70, i32 0
  %71 = load i8*, i8** %arrayidx37, align 4, !tbaa !2
  %72 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %73 = load i32, i32* %i, align 4, !tbaa !6
  %74 = load i32, i32* %rgroup, align 4, !tbaa !6
  %sub38 = sub nsw i32 %73, %74
  %arrayidx39 = getelementptr inbounds i8*, i8** %72, i32 %sub38
  store i8* %71, i8** %arrayidx39, align 4, !tbaa !2
  br label %for.inc40

for.inc40:                                        ; preds = %for.body36
  %75 = load i32, i32* %i, align 4, !tbaa !6
  %inc41 = add nsw i32 %75, 1
  store i32 %inc41, i32* %i, align 4, !tbaa !6
  br label %for.cond34

for.end42:                                        ; preds = %for.cond34
  br label %for.inc43

for.inc43:                                        ; preds = %for.end42
  %76 = load i32, i32* %ci, align 4, !tbaa !6
  %inc44 = add nsw i32 %76, 1
  store i32 %inc44, i32* %ci, align 4, !tbaa !6
  %77 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %77, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end45:                                        ; preds = %for.cond
  %78 = bitcast i8*** %xbuf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #2
  %79 = bitcast i8*** %xbuf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #2
  %80 = bitcast i8*** %buf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #2
  %81 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #2
  %82 = bitcast i32* %M to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #2
  %83 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #2
  %84 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #2
  %85 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #2
  %86 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #2
  ret void
}

; Function Attrs: nounwind
define internal void @process_data_simple_main(%struct.jpeg_decompress_struct* %cinfo, i8** %output_buf, i32* %out_row_ctr, i32 %out_rows_avail) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %output_buf.addr = alloca i8**, align 4
  %out_row_ctr.addr = alloca i32*, align 4
  %out_rows_avail.addr = alloca i32, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %rowgroups_avail = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32* %out_row_ctr, i32** %out_row_ctr.addr, align 4, !tbaa !2
  store i32 %out_rows_avail, i32* %out_rows_avail.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = bitcast i32* %rowgroups_avail to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %5, i32 0, i32 2
  %6 = load i32, i32* %buffer_full, align 4, !tbaa !39
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.end4, label %if.then

if.then:                                          ; preds = %entry
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 79
  %8 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef, align 4, !tbaa !41
  %decompress_data = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %8, i32 0, i32 3
  %9 = load i32 (%struct.jpeg_decompress_struct*, i8***)*, i32 (%struct.jpeg_decompress_struct*, i8***)** %decompress_data, align 4, !tbaa !42
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %11, i32 0, i32 1
  %arraydecay = getelementptr inbounds [10 x i8**], [10 x i8**]* %buffer, i32 0, i32 0
  %call = call i32 %9(%struct.jpeg_decompress_struct* %10, i8*** %arraydecay)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.end, label %if.then2

if.then2:                                         ; preds = %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %12 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full3 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %12, i32 0, i32 2
  store i32 1, i32* %buffer_full3, align 4, !tbaa !39
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 63
  %14 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !26
  store i32 %14, i32* %rowgroups_avail, align 4, !tbaa !6
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %post = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 80
  %16 = load %struct.jpeg_d_post_controller*, %struct.jpeg_d_post_controller** %post, align 8, !tbaa !44
  %post_process_data = getelementptr inbounds %struct.jpeg_d_post_controller, %struct.jpeg_d_post_controller* %16, i32 0, i32 1
  %17 = load void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)** %post_process_data, align 4, !tbaa !45
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer5 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %19, i32 0, i32 1
  %arraydecay6 = getelementptr inbounds [10 x i8**], [10 x i8**]* %buffer5, i32 0, i32 0
  %20 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %20, i32 0, i32 3
  %21 = load i32, i32* %rowgroups_avail, align 4, !tbaa !6
  %22 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %23 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %24 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !6
  call void %17(%struct.jpeg_decompress_struct* %18, i8*** %arraydecay6, i32* %rowgroup_ctr, i32 %21, i8** %22, i32* %23, i32 %24)
  %25 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr7 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %25, i32 0, i32 3
  %26 = load i32, i32* %rowgroup_ctr7, align 4, !tbaa !40
  %27 = load i32, i32* %rowgroups_avail, align 4, !tbaa !6
  %cmp = icmp uge i32 %26, %27
  br i1 %cmp, label %if.then8, label %if.end11

if.then8:                                         ; preds = %if.end4
  %28 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full9 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %28, i32 0, i32 2
  store i32 0, i32* %buffer_full9, align 4, !tbaa !39
  %29 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr10 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %29, i32 0, i32 3
  store i32 0, i32* %rowgroup_ctr10, align 4, !tbaa !40
  br label %if.end11

if.end11:                                         ; preds = %if.then8, %if.end4
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end11, %if.then2
  %30 = bitcast i32* %rowgroups_avail to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #2
  %31 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #2
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @process_data_crank_post(%struct.jpeg_decompress_struct* %cinfo, i8** %output_buf, i32* %out_row_ctr, i32 %out_rows_avail) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %output_buf.addr = alloca i8**, align 4
  %out_row_ctr.addr = alloca i32*, align 4
  %out_rows_avail.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32* %out_row_ctr, i32** %out_row_ctr.addr, align 4, !tbaa !2
  store i32 %out_rows_avail, i32* %out_rows_avail.addr, align 4, !tbaa !6
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %post = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 80
  %1 = load %struct.jpeg_d_post_controller*, %struct.jpeg_d_post_controller** %post, align 8, !tbaa !44
  %post_process_data = getelementptr inbounds %struct.jpeg_d_post_controller, %struct.jpeg_d_post_controller* %1, i32 0, i32 1
  %2 = load void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)** %post_process_data, align 4, !tbaa !45
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %4 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %5 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %6 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !6
  call void %2(%struct.jpeg_decompress_struct* %3, i8*** null, i32* null, i32 0, i8** %4, i32* %5, i32 %6)
  ret void
}

; Function Attrs: nounwind
define internal void @set_bottom_pointers(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %ci = alloca i32, align 4
  %i = alloca i32, align 4
  %rgroup = alloca i32, align 4
  %iMCUheight = alloca i32, align 4
  %rows_left = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %xbuf = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %iMCUheight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %rows_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i8*** %xbuf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  store i32 0, i32* %ci, align 4, !tbaa !6
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 44
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !27
  store %struct.jpeg_component_info* %12, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %13 = load i32, i32* %ci, align 4, !tbaa !6
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 9
  %15 = load i32, i32* %num_components, align 4, !tbaa !28
  %cmp = icmp slt i32 %13, %15
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 0, i32 3
  %17 = load i32, i32* %v_samp_factor, align 4, !tbaa !29
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %18, i32 0, i32 9
  %19 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !31
  %mul = mul nsw i32 %17, %19
  store i32 %mul, i32* %iMCUheight, align 4, !tbaa !6
  %20 = load i32, i32* %iMCUheight, align 4, !tbaa !6
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 63
  %22 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !26
  %div = sdiv i32 %20, %22
  store i32 %div, i32* %rgroup, align 4, !tbaa !6
  %23 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %23, i32 0, i32 11
  %24 = load i32, i32* %downsampled_height, align 4, !tbaa !49
  %25 = load i32, i32* %iMCUheight, align 4, !tbaa !6
  %rem = urem i32 %24, %25
  store i32 %rem, i32* %rows_left, align 4, !tbaa !6
  %26 = load i32, i32* %rows_left, align 4, !tbaa !6
  %cmp1 = icmp eq i32 %26, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %27 = load i32, i32* %iMCUheight, align 4, !tbaa !6
  store i32 %27, i32* %rows_left, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %28 = load i32, i32* %ci, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %28, 0
  br i1 %cmp2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %29 = load i32, i32* %rows_left, align 4, !tbaa !6
  %sub = sub nsw i32 %29, 1
  %30 = load i32, i32* %rgroup, align 4, !tbaa !6
  %div4 = sdiv i32 %sub, %30
  %add = add nsw i32 %div4, 1
  %31 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroups_avail = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %31, i32 0, i32 7
  store i32 %add, i32* %rowgroups_avail, align 4, !tbaa !47
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  %32 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %32, i32 0, i32 4
  %33 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %whichptr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %33, i32 0, i32 5
  %34 = load i32, i32* %whichptr, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer, i32 0, i32 %34
  %35 = load i8***, i8**** %arrayidx, align 4, !tbaa !2
  %36 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds i8**, i8*** %35, i32 %36
  %37 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  store i8** %37, i8*** %xbuf, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %if.end5
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %39 = load i32, i32* %rgroup, align 4, !tbaa !6
  %mul8 = mul nsw i32 %39, 2
  %cmp9 = icmp slt i32 %38, %mul8
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond7
  %40 = load i8**, i8*** %xbuf, align 4, !tbaa !2
  %41 = load i32, i32* %rows_left, align 4, !tbaa !6
  %sub11 = sub nsw i32 %41, 1
  %arrayidx12 = getelementptr inbounds i8*, i8** %40, i32 %sub11
  %42 = load i8*, i8** %arrayidx12, align 4, !tbaa !2
  %43 = load i8**, i8*** %xbuf, align 4, !tbaa !2
  %44 = load i32, i32* %rows_left, align 4, !tbaa !6
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %add13 = add nsw i32 %44, %45
  %arrayidx14 = getelementptr inbounds i8*, i8** %43, i32 %add13
  store i8* %42, i8** %arrayidx14, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %46 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %47 = load i32, i32* %ci, align 4, !tbaa !6
  %inc16 = add nsw i32 %47, 1
  store i32 %inc16, i32* %ci, align 4, !tbaa !6
  %48 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %48, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  %49 = bitcast i8*** %xbuf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #2
  %50 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #2
  %51 = bitcast i32* %rows_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #2
  %52 = bitcast i32* %iMCUheight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #2
  %53 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #2
  %54 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #2
  %55 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #2
  %56 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #2
  ret void
}

; Function Attrs: nounwind
define internal void @set_wraparound_pointers(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %ci = alloca i32, align 4
  %i = alloca i32, align 4
  %rgroup = alloca i32, align 4
  %M = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %xbuf0 = alloca i8**, align 4
  %xbuf1 = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %M to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 63
  %9 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !26
  store i32 %9, i32* %M, align 4, !tbaa !6
  %10 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i8*** %xbuf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i8*** %xbuf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  store i32 0, i32* %ci, align 4, !tbaa !6
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 44
  %14 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !27
  store %struct.jpeg_component_info* %14, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc29, %entry
  %15 = load i32, i32* %ci, align 4, !tbaa !6
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 9
  %17 = load i32, i32* %num_components, align 4, !tbaa !28
  %cmp = icmp slt i32 %15, %17
  br i1 %cmp, label %for.body, label %for.end31

for.body:                                         ; preds = %for.cond
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %18, i32 0, i32 3
  %19 = load i32, i32* %v_samp_factor, align 4, !tbaa !29
  %20 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %20, i32 0, i32 9
  %21 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !31
  %mul = mul nsw i32 %19, %21
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 63
  %23 = load i32, i32* %min_DCT_scaled_size1, align 4, !tbaa !26
  %div = sdiv i32 %mul, %23
  store i32 %div, i32* %rgroup, align 4, !tbaa !6
  %24 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %24, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer, i32 0, i32 0
  %25 = load i8***, i8**** %arrayidx, align 4, !tbaa !2
  %26 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i8**, i8*** %25, i32 %26
  %27 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  store i8** %27, i8*** %xbuf0, align 4, !tbaa !2
  %28 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer3 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %28, i32 0, i32 4
  %arrayidx4 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer3, i32 0, i32 1
  %29 = load i8***, i8**** %arrayidx4, align 4, !tbaa !2
  %30 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 %30
  %31 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  store i8** %31, i8*** %xbuf1, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %33 = load i32, i32* %rgroup, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %32, %33
  br i1 %cmp7, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond6
  %34 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %35 = load i32, i32* %rgroup, align 4, !tbaa !6
  %36 = load i32, i32* %M, align 4, !tbaa !6
  %add = add nsw i32 %36, 1
  %mul9 = mul nsw i32 %35, %add
  %37 = load i32, i32* %i, align 4, !tbaa !6
  %add10 = add nsw i32 %mul9, %37
  %arrayidx11 = getelementptr inbounds i8*, i8** %34, i32 %add10
  %38 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  %39 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %40 = load i32, i32* %i, align 4, !tbaa !6
  %41 = load i32, i32* %rgroup, align 4, !tbaa !6
  %sub = sub nsw i32 %40, %41
  %arrayidx12 = getelementptr inbounds i8*, i8** %39, i32 %sub
  store i8* %38, i8** %arrayidx12, align 4, !tbaa !2
  %42 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %43 = load i32, i32* %rgroup, align 4, !tbaa !6
  %44 = load i32, i32* %M, align 4, !tbaa !6
  %add13 = add nsw i32 %44, 1
  %mul14 = mul nsw i32 %43, %add13
  %45 = load i32, i32* %i, align 4, !tbaa !6
  %add15 = add nsw i32 %mul14, %45
  %arrayidx16 = getelementptr inbounds i8*, i8** %42, i32 %add15
  %46 = load i8*, i8** %arrayidx16, align 4, !tbaa !2
  %47 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %49 = load i32, i32* %rgroup, align 4, !tbaa !6
  %sub17 = sub nsw i32 %48, %49
  %arrayidx18 = getelementptr inbounds i8*, i8** %47, i32 %sub17
  store i8* %46, i8** %arrayidx18, align 4, !tbaa !2
  %50 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds i8*, i8** %50, i32 %51
  %52 = load i8*, i8** %arrayidx19, align 4, !tbaa !2
  %53 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %54 = load i32, i32* %rgroup, align 4, !tbaa !6
  %55 = load i32, i32* %M, align 4, !tbaa !6
  %add20 = add nsw i32 %55, 2
  %mul21 = mul nsw i32 %54, %add20
  %56 = load i32, i32* %i, align 4, !tbaa !6
  %add22 = add nsw i32 %mul21, %56
  %arrayidx23 = getelementptr inbounds i8*, i8** %53, i32 %add22
  store i8* %52, i8** %arrayidx23, align 4, !tbaa !2
  %57 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds i8*, i8** %57, i32 %58
  %59 = load i8*, i8** %arrayidx24, align 4, !tbaa !2
  %60 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %61 = load i32, i32* %rgroup, align 4, !tbaa !6
  %62 = load i32, i32* %M, align 4, !tbaa !6
  %add25 = add nsw i32 %62, 2
  %mul26 = mul nsw i32 %61, %add25
  %63 = load i32, i32* %i, align 4, !tbaa !6
  %add27 = add nsw i32 %mul26, %63
  %arrayidx28 = getelementptr inbounds i8*, i8** %60, i32 %add27
  store i8* %59, i8** %arrayidx28, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %64 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond6

for.end:                                          ; preds = %for.cond6
  br label %for.inc29

for.inc29:                                        ; preds = %for.end
  %65 = load i32, i32* %ci, align 4, !tbaa !6
  %inc30 = add nsw i32 %65, 1
  store i32 %inc30, i32* %ci, align 4, !tbaa !6
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end31:                                        ; preds = %for.cond
  %67 = bitcast i8*** %xbuf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #2
  %68 = bitcast i8*** %xbuf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #2
  %69 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #2
  %70 = bitcast i32* %M to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #2
  %71 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #2
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #2
  %73 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #2
  %74 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #2
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !10, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !11, i64 288, !11, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 0}
!13 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!14 = !{!"long", !4, i64 0}
!15 = !{!9, !3, i64 424}
!16 = !{!17, !3, i64 0}
!17 = !{!"", !18, i64 0, !4, i64 8, !7, i64 48, !7, i64 52, !4, i64 56, !7, i64 64, !7, i64 68, !7, i64 72, !7, i64 76}
!18 = !{!"jpeg_d_main_controller", !3, i64 0, !3, i64 4}
!19 = !{!9, !3, i64 0}
!20 = !{!21, !7, i64 20}
!21 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !14, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!22 = !{!21, !3, i64 0}
!23 = !{!9, !3, i64 452}
!24 = !{!25, !7, i64 8}
!25 = !{!"jpeg_upsampler", !3, i64 0, !3, i64 4, !7, i64 8}
!26 = !{!9, !7, i64 316}
!27 = !{!9, !3, i64 216}
!28 = !{!9, !7, i64 36}
!29 = !{!30, !7, i64 12}
!30 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!31 = !{!30, !7, i64 36}
!32 = !{!13, !3, i64 8}
!33 = !{!30, !7, i64 28}
!34 = !{!4, !4, i64 0}
!35 = !{!17, !3, i64 4}
!36 = !{!17, !7, i64 64}
!37 = !{!17, !7, i64 68}
!38 = !{!17, !7, i64 76}
!39 = !{!17, !7, i64 48}
!40 = !{!17, !7, i64 52}
!41 = !{!9, !3, i64 428}
!42 = !{!43, !3, i64 12}
!43 = !{!"jpeg_d_coef_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!44 = !{!9, !3, i64 432}
!45 = !{!46, !3, i64 4}
!46 = !{!"jpeg_d_post_controller", !3, i64 0, !3, i64 4}
!47 = !{!17, !7, i64 72}
!48 = !{!9, !7, i64 320}
!49 = !{!30, !7, i64 44}
