; ModuleID = 'jdphuff.c'
source_filename = "jdphuff.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.phuff_entropy_decoder = type { %struct.jpeg_entropy_decoder, %struct.bitread_perm_state, %struct.savable_state, i32, [4 x %struct.d_derived_tbl*], %struct.d_derived_tbl* }
%struct.bitread_perm_state = type { i32, i32 }
%struct.savable_state = type { i32, [4 x i32] }
%struct.d_derived_tbl = type { [18 x i32], [18 x i32], %struct.JHUFF_TBL*, [256 x i32] }
%struct.bitread_working_state = type { i8*, i32, i32, i32, %struct.jpeg_decompress_struct* }

@jpeg_natural_order = external constant [0 x i32], align 4

; Function Attrs: nounwind
define hidden void @jinit_phuff_decoder(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %entropy = alloca %struct.phuff_entropy_decoder*, align 4
  %coef_bit_ptr = alloca i32*, align 4
  %ci = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32** %coef_bit_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 1
  %5 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %5, i32 0, i32 0
  %6 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = bitcast %struct.jpeg_decompress_struct* %7 to %struct.jpeg_common_struct*
  %call = call i8* %6(%struct.jpeg_common_struct* %8, i32 1, i32 64)
  %9 = bitcast i8* %call to %struct.phuff_entropy_decoder*
  store %struct.phuff_entropy_decoder* %9, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %10 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %11 = bitcast %struct.phuff_entropy_decoder* %10 to %struct.jpeg_entropy_decoder*
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 83
  store %struct.jpeg_entropy_decoder* %11, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !14
  %13 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %13, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 0
  %start_pass2 = bitcast {}** %start_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @start_pass_phuff_decoder, void (%struct.jpeg_decompress_struct*)** %start_pass2, align 4, !tbaa !15
  store i32 0, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %i, align 4, !tbaa !20
  %cmp = icmp slt i32 %14, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %derived_tbls = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %15, i32 0, i32 4
  %16 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %derived_tbls, i32 0, i32 %16
  store %struct.d_derived_tbl* null, %struct.d_derived_tbl** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 1
  %19 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem3, align 4, !tbaa !6
  %alloc_small4 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %19, i32 0, i32 0
  %20 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small4, align 4, !tbaa !11
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %22 = bitcast %struct.jpeg_decompress_struct* %21 to %struct.jpeg_common_struct*
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 9
  %24 = load i32, i32* %num_components, align 4, !tbaa !21
  %mul = mul nsw i32 %24, 64
  %mul5 = mul i32 %mul, 4
  %call6 = call i8* %20(%struct.jpeg_common_struct* %22, i32 1, i32 %mul5)
  %25 = bitcast i8* %call6 to [64 x i32]*
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef_bits = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 39
  store [64 x i32]* %25, [64 x i32]** %coef_bits, align 8, !tbaa !22
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef_bits7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 39
  %28 = load [64 x i32]*, [64 x i32]** %coef_bits7, align 8, !tbaa !22
  %arrayidx8 = getelementptr inbounds [64 x i32], [64 x i32]* %28, i32 0
  %arrayidx9 = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx8, i32 0, i32 0
  store i32* %arrayidx9, i32** %coef_bit_ptr, align 4, !tbaa !2
  store i32 0, i32* %ci, align 4, !tbaa !20
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc20, %for.end
  %29 = load i32, i32* %ci, align 4, !tbaa !20
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 9
  %31 = load i32, i32* %num_components11, align 4, !tbaa !21
  %cmp12 = icmp slt i32 %29, %31
  br i1 %cmp12, label %for.body13, label %for.end22

for.body13:                                       ; preds = %for.cond10
  store i32 0, i32* %i, align 4, !tbaa !20
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc17, %for.body13
  %32 = load i32, i32* %i, align 4, !tbaa !20
  %cmp15 = icmp slt i32 %32, 64
  br i1 %cmp15, label %for.body16, label %for.end19

for.body16:                                       ; preds = %for.cond14
  %33 = load i32*, i32** %coef_bit_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %33, i32 1
  store i32* %incdec.ptr, i32** %coef_bit_ptr, align 4, !tbaa !2
  store i32 -1, i32* %33, align 4, !tbaa !20
  br label %for.inc17

for.inc17:                                        ; preds = %for.body16
  %34 = load i32, i32* %i, align 4, !tbaa !20
  %inc18 = add nsw i32 %34, 1
  store i32 %inc18, i32* %i, align 4, !tbaa !20
  br label %for.cond14

for.end19:                                        ; preds = %for.cond14
  br label %for.inc20

for.inc20:                                        ; preds = %for.end19
  %35 = load i32, i32* %ci, align 4, !tbaa !20
  %inc21 = add nsw i32 %35, 1
  store i32 %inc21, i32* %ci, align 4, !tbaa !20
  br label %for.cond10

for.end22:                                        ; preds = %for.cond10
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  %37 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #3
  %38 = bitcast i32** %coef_bit_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #3
  %39 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_phuff_decoder(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %entropy = alloca %struct.phuff_entropy_decoder*, align 4
  %is_DC_band = alloca i32, align 4
  %bad = alloca i32, align 4
  %ci = alloca i32, align 4
  %coefi = alloca i32, align 4
  %tbl = alloca i32, align 4
  %pdtbl = alloca %struct.d_derived_tbl**, align 4
  %coef_bit_ptr = alloca i32*, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %cindex = alloca i32, align 4
  %expected = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.phuff_entropy_decoder*
  store %struct.phuff_entropy_decoder* %3, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %is_DC_band to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %bad to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %coefi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %tbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast %struct.d_derived_tbl*** %pdtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32** %coef_bit_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 72
  %13 = load i32, i32* %Ss, align 8, !tbaa !23
  %cmp = icmp eq i32 %13, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %is_DC_band, align 4, !tbaa !20
  store i32 0, i32* %bad, align 4, !tbaa !20
  %14 = load i32, i32* %is_DC_band, align 4, !tbaa !20
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 73
  %16 = load i32, i32* %Se, align 4, !tbaa !24
  %cmp2 = icmp ne i32 %16, 0
  br i1 %cmp2, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  store i32 1, i32* %bad, align 4, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  br label %if.end18

if.else:                                          ; preds = %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 72
  %18 = load i32, i32* %Ss5, align 8, !tbaa !23
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 73
  %20 = load i32, i32* %Se6, align 4, !tbaa !24
  %cmp7 = icmp sgt i32 %18, %20
  br i1 %cmp7, label %if.then12, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 73
  %22 = load i32, i32* %Se9, align 4, !tbaa !24
  %cmp10 = icmp sge i32 %22, 64
  br i1 %cmp10, label %if.then12, label %if.end13

if.then12:                                        ; preds = %lor.lhs.false, %if.else
  store i32 1, i32* %bad, align 4, !tbaa !20
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %lor.lhs.false
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 66
  %24 = load i32, i32* %comps_in_scan, align 8, !tbaa !25
  %cmp14 = icmp ne i32 %24, 1
  br i1 %cmp14, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end13
  store i32 1, i32* %bad, align 4, !tbaa !20
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %if.end13
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.end
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 74
  %26 = load i32, i32* %Ah, align 8, !tbaa !26
  %cmp19 = icmp ne i32 %26, 0
  br i1 %cmp19, label %if.then21, label %if.end27

if.then21:                                        ; preds = %if.end18
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 75
  %28 = load i32, i32* %Al, align 4, !tbaa !27
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 74
  %30 = load i32, i32* %Ah22, align 8, !tbaa !26
  %sub = sub nsw i32 %30, 1
  %cmp23 = icmp ne i32 %28, %sub
  br i1 %cmp23, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.then21
  store i32 1, i32* %bad, align 4, !tbaa !20
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %if.then21
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end18
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al28 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 75
  %32 = load i32, i32* %Al28, align 4, !tbaa !27
  %cmp29 = icmp sgt i32 %32, 13
  br i1 %cmp29, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end27
  store i32 1, i32* %bad, align 4, !tbaa !20
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.end27
  %33 = load i32, i32* %bad, align 4, !tbaa !20
  %tobool33 = icmp ne i32 %33, 0
  br i1 %tobool33, label %if.then34, label %if.end53

if.then34:                                        ; preds = %if.end32
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !28
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 5
  store i32 16, i32* %msg_code, align 4, !tbaa !29
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss35 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 72
  %37 = load i32, i32* %Ss35, align 8, !tbaa !23
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err36 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err36, align 8, !tbaa !28
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %37, i32* %arrayidx, align 4, !tbaa !31
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se37 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 73
  %41 = load i32, i32* %Se37, align 4, !tbaa !24
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 0
  %43 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err38, align 8, !tbaa !28
  %msg_parm39 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %43, i32 0, i32 6
  %i40 = bitcast %union.anon* %msg_parm39 to [8 x i32]*
  %arrayidx41 = getelementptr inbounds [8 x i32], [8 x i32]* %i40, i32 0, i32 1
  store i32 %41, i32* %arrayidx41, align 4, !tbaa !31
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah42 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 74
  %45 = load i32, i32* %Ah42, align 8, !tbaa !26
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err43 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 0
  %47 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err43, align 8, !tbaa !28
  %msg_parm44 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %47, i32 0, i32 6
  %i45 = bitcast %union.anon* %msg_parm44 to [8 x i32]*
  %arrayidx46 = getelementptr inbounds [8 x i32], [8 x i32]* %i45, i32 0, i32 2
  store i32 %45, i32* %arrayidx46, align 4, !tbaa !31
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al47 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 75
  %49 = load i32, i32* %Al47, align 4, !tbaa !27
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 0
  %51 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err48, align 8, !tbaa !28
  %msg_parm49 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %51, i32 0, i32 6
  %i50 = bitcast %union.anon* %msg_parm49 to [8 x i32]*
  %arrayidx51 = getelementptr inbounds [8 x i32], [8 x i32]* %i50, i32 0, i32 3
  store i32 %49, i32* %arrayidx51, align 4, !tbaa !31
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err52 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 0
  %53 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err52, align 8, !tbaa !28
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %53, i32 0, i32 0
  %54 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !32
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %56 = bitcast %struct.jpeg_decompress_struct* %55 to %struct.jpeg_common_struct*
  call void %54(%struct.jpeg_common_struct* %56)
  br label %if.end53

if.end53:                                         ; preds = %if.then34, %if.end32
  store i32 0, i32* %ci, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc106, %if.end53
  %57 = load i32, i32* %ci, align 4, !tbaa !20
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %58, i32 0, i32 66
  %59 = load i32, i32* %comps_in_scan54, align 8, !tbaa !25
  %cmp55 = icmp slt i32 %57, %59
  br i1 %cmp55, label %for.body, label %for.end108

for.body:                                         ; preds = %for.cond
  %60 = bitcast i32* %cindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #3
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 67
  %62 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx57 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %62
  %63 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx57, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %63, i32 0, i32 1
  %64 = load i32, i32* %component_index, align 4, !tbaa !33
  store i32 %64, i32* %cindex, align 4, !tbaa !20
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef_bits = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 39
  %66 = load [64 x i32]*, [64 x i32]** %coef_bits, align 8, !tbaa !22
  %67 = load i32, i32* %cindex, align 4, !tbaa !20
  %arrayidx58 = getelementptr inbounds [64 x i32], [64 x i32]* %66, i32 %67
  %arrayidx59 = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx58, i32 0, i32 0
  store i32* %arrayidx59, i32** %coef_bit_ptr, align 4, !tbaa !2
  %68 = load i32, i32* %is_DC_band, align 4, !tbaa !20
  %tobool60 = icmp ne i32 %68, 0
  br i1 %tobool60, label %if.end76, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body
  %69 = load i32*, i32** %coef_bit_ptr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %69, i32 0
  %70 = load i32, i32* %arrayidx61, align 4, !tbaa !20
  %cmp62 = icmp slt i32 %70, 0
  br i1 %cmp62, label %if.then64, label %if.end76

if.then64:                                        ; preds = %land.lhs.true
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err65 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 0
  %72 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err65, align 8, !tbaa !28
  %msg_code66 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %72, i32 0, i32 5
  store i32 115, i32* %msg_code66, align 4, !tbaa !29
  %73 = load i32, i32* %cindex, align 4, !tbaa !20
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %74, i32 0, i32 0
  %75 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err67, align 8, !tbaa !28
  %msg_parm68 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %75, i32 0, i32 6
  %i69 = bitcast %union.anon* %msg_parm68 to [8 x i32]*
  %arrayidx70 = getelementptr inbounds [8 x i32], [8 x i32]* %i69, i32 0, i32 0
  store i32 %73, i32* %arrayidx70, align 4, !tbaa !31
  %76 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %76, i32 0, i32 0
  %77 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err71, align 8, !tbaa !28
  %msg_parm72 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %77, i32 0, i32 6
  %i73 = bitcast %union.anon* %msg_parm72 to [8 x i32]*
  %arrayidx74 = getelementptr inbounds [8 x i32], [8 x i32]* %i73, i32 0, i32 1
  store i32 0, i32* %arrayidx74, align 4, !tbaa !31
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err75 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %78, i32 0, i32 0
  %79 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err75, align 8, !tbaa !28
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %79, i32 0, i32 1
  %80 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !35
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %82 = bitcast %struct.jpeg_decompress_struct* %81 to %struct.jpeg_common_struct*
  call void %80(%struct.jpeg_common_struct* %82, i32 -1)
  br label %if.end76

if.end76:                                         ; preds = %if.then64, %land.lhs.true, %for.body
  %83 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss77 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %83, i32 0, i32 72
  %84 = load i32, i32* %Ss77, align 8, !tbaa !23
  store i32 %84, i32* %coefi, align 4, !tbaa !20
  br label %for.cond78

for.cond78:                                       ; preds = %for.inc, %if.end76
  %85 = load i32, i32* %coefi, align 4, !tbaa !20
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se79 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 73
  %87 = load i32, i32* %Se79, align 4, !tbaa !24
  %cmp80 = icmp sle i32 %85, %87
  br i1 %cmp80, label %for.body82, label %for.end

for.body82:                                       ; preds = %for.cond78
  %88 = bitcast i32* %expected to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #3
  %89 = load i32*, i32** %coef_bit_ptr, align 4, !tbaa !2
  %90 = load i32, i32* %coefi, align 4, !tbaa !20
  %arrayidx83 = getelementptr inbounds i32, i32* %89, i32 %90
  %91 = load i32, i32* %arrayidx83, align 4, !tbaa !20
  %cmp84 = icmp slt i32 %91, 0
  br i1 %cmp84, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body82
  br label %cond.end

cond.false:                                       ; preds = %for.body82
  %92 = load i32*, i32** %coef_bit_ptr, align 4, !tbaa !2
  %93 = load i32, i32* %coefi, align 4, !tbaa !20
  %arrayidx86 = getelementptr inbounds i32, i32* %92, i32 %93
  %94 = load i32, i32* %arrayidx86, align 4, !tbaa !20
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %94, %cond.false ]
  store i32 %cond, i32* %expected, align 4, !tbaa !20
  %95 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah87 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %95, i32 0, i32 74
  %96 = load i32, i32* %Ah87, align 8, !tbaa !26
  %97 = load i32, i32* %expected, align 4, !tbaa !20
  %cmp88 = icmp ne i32 %96, %97
  br i1 %cmp88, label %if.then90, label %if.end103

if.then90:                                        ; preds = %cond.end
  %98 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err91 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %98, i32 0, i32 0
  %99 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err91, align 8, !tbaa !28
  %msg_code92 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %99, i32 0, i32 5
  store i32 115, i32* %msg_code92, align 4, !tbaa !29
  %100 = load i32, i32* %cindex, align 4, !tbaa !20
  %101 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err93 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %101, i32 0, i32 0
  %102 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err93, align 8, !tbaa !28
  %msg_parm94 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %102, i32 0, i32 6
  %i95 = bitcast %union.anon* %msg_parm94 to [8 x i32]*
  %arrayidx96 = getelementptr inbounds [8 x i32], [8 x i32]* %i95, i32 0, i32 0
  store i32 %100, i32* %arrayidx96, align 4, !tbaa !31
  %103 = load i32, i32* %coefi, align 4, !tbaa !20
  %104 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err97 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %104, i32 0, i32 0
  %105 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err97, align 8, !tbaa !28
  %msg_parm98 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %105, i32 0, i32 6
  %i99 = bitcast %union.anon* %msg_parm98 to [8 x i32]*
  %arrayidx100 = getelementptr inbounds [8 x i32], [8 x i32]* %i99, i32 0, i32 1
  store i32 %103, i32* %arrayidx100, align 4, !tbaa !31
  %106 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err101 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %106, i32 0, i32 0
  %107 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err101, align 8, !tbaa !28
  %emit_message102 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %107, i32 0, i32 1
  %108 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message102, align 4, !tbaa !35
  %109 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %110 = bitcast %struct.jpeg_decompress_struct* %109 to %struct.jpeg_common_struct*
  call void %108(%struct.jpeg_common_struct* %110, i32 -1)
  br label %if.end103

if.end103:                                        ; preds = %if.then90, %cond.end
  %111 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al104 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %111, i32 0, i32 75
  %112 = load i32, i32* %Al104, align 4, !tbaa !27
  %113 = load i32*, i32** %coef_bit_ptr, align 4, !tbaa !2
  %114 = load i32, i32* %coefi, align 4, !tbaa !20
  %arrayidx105 = getelementptr inbounds i32, i32* %113, i32 %114
  store i32 %112, i32* %arrayidx105, align 4, !tbaa !20
  %115 = bitcast i32* %expected to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #3
  br label %for.inc

for.inc:                                          ; preds = %if.end103
  %116 = load i32, i32* %coefi, align 4, !tbaa !20
  %inc = add nsw i32 %116, 1
  store i32 %inc, i32* %coefi, align 4, !tbaa !20
  br label %for.cond78

for.end:                                          ; preds = %for.cond78
  %117 = bitcast i32* %cindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  br label %for.inc106

for.inc106:                                       ; preds = %for.end
  %118 = load i32, i32* %ci, align 4, !tbaa !20
  %inc107 = add nsw i32 %118, 1
  store i32 %inc107, i32* %ci, align 4, !tbaa !20
  br label %for.cond

for.end108:                                       ; preds = %for.cond
  %119 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah109 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %119, i32 0, i32 74
  %120 = load i32, i32* %Ah109, align 8, !tbaa !26
  %cmp110 = icmp eq i32 %120, 0
  br i1 %cmp110, label %if.then112, label %if.else119

if.then112:                                       ; preds = %for.end108
  %121 = load i32, i32* %is_DC_band, align 4, !tbaa !20
  %tobool113 = icmp ne i32 %121, 0
  br i1 %tobool113, label %if.then114, label %if.else115

if.then114:                                       ; preds = %if.then112
  %122 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %122, i32 0, i32 0
  %decode_mcu = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)* @decode_mcu_DC_first, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)** %decode_mcu, align 4, !tbaa !36
  br label %if.end118

if.else115:                                       ; preds = %if.then112
  %123 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub116 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %123, i32 0, i32 0
  %decode_mcu117 = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub116, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)* @decode_mcu_AC_first, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)** %decode_mcu117, align 4, !tbaa !36
  br label %if.end118

if.end118:                                        ; preds = %if.else115, %if.then114
  br label %if.end128

if.else119:                                       ; preds = %for.end108
  %124 = load i32, i32* %is_DC_band, align 4, !tbaa !20
  %tobool120 = icmp ne i32 %124, 0
  br i1 %tobool120, label %if.then121, label %if.else124

if.then121:                                       ; preds = %if.else119
  %125 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub122 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %125, i32 0, i32 0
  %decode_mcu123 = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub122, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)* @decode_mcu_DC_refine, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)** %decode_mcu123, align 4, !tbaa !36
  br label %if.end127

if.else124:                                       ; preds = %if.else119
  %126 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub125 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %126, i32 0, i32 0
  %decode_mcu126 = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub125, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)* @decode_mcu_AC_refine, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)** %decode_mcu126, align 4, !tbaa !36
  br label %if.end127

if.end127:                                        ; preds = %if.else124, %if.then121
  br label %if.end128

if.end128:                                        ; preds = %if.end127, %if.end118
  store i32 0, i32* %ci, align 4, !tbaa !20
  br label %for.cond129

for.cond129:                                      ; preds = %for.inc151, %if.end128
  %127 = load i32, i32* %ci, align 4, !tbaa !20
  %128 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan130 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %128, i32 0, i32 66
  %129 = load i32, i32* %comps_in_scan130, align 8, !tbaa !25
  %cmp131 = icmp slt i32 %127, %129
  br i1 %cmp131, label %for.body133, label %for.end153

for.body133:                                      ; preds = %for.cond129
  %130 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info134 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %130, i32 0, i32 67
  %131 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx135 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info134, i32 0, i32 %131
  %132 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx135, align 4, !tbaa !2
  store %struct.jpeg_component_info* %132, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %133 = load i32, i32* %is_DC_band, align 4, !tbaa !20
  %tobool136 = icmp ne i32 %133, 0
  br i1 %tobool136, label %if.then137, label %if.else143

if.then137:                                       ; preds = %for.body133
  %134 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah138 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %134, i32 0, i32 74
  %135 = load i32, i32* %Ah138, align 8, !tbaa !26
  %cmp139 = icmp eq i32 %135, 0
  br i1 %cmp139, label %if.then141, label %if.end142

if.then141:                                       ; preds = %if.then137
  %136 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %136, i32 0, i32 5
  %137 = load i32, i32* %dc_tbl_no, align 4, !tbaa !37
  store i32 %137, i32* %tbl, align 4, !tbaa !20
  %138 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %derived_tbls = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %138, i32 0, i32 4
  %arraydecay = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %derived_tbls, i32 0, i32 0
  %139 = load i32, i32* %tbl, align 4, !tbaa !20
  %add.ptr = getelementptr inbounds %struct.d_derived_tbl*, %struct.d_derived_tbl** %arraydecay, i32 %139
  store %struct.d_derived_tbl** %add.ptr, %struct.d_derived_tbl*** %pdtbl, align 4, !tbaa !2
  %140 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %141 = load i32, i32* %tbl, align 4, !tbaa !20
  %142 = load %struct.d_derived_tbl**, %struct.d_derived_tbl*** %pdtbl, align 4, !tbaa !2
  call void @jpeg_make_d_derived_tbl(%struct.jpeg_decompress_struct* %140, i32 1, i32 %141, %struct.d_derived_tbl** %142)
  br label %if.end142

if.end142:                                        ; preds = %if.then141, %if.then137
  br label %if.end149

if.else143:                                       ; preds = %for.body133
  %143 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %143, i32 0, i32 6
  %144 = load i32, i32* %ac_tbl_no, align 4, !tbaa !38
  store i32 %144, i32* %tbl, align 4, !tbaa !20
  %145 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %derived_tbls144 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %145, i32 0, i32 4
  %arraydecay145 = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %derived_tbls144, i32 0, i32 0
  %146 = load i32, i32* %tbl, align 4, !tbaa !20
  %add.ptr146 = getelementptr inbounds %struct.d_derived_tbl*, %struct.d_derived_tbl** %arraydecay145, i32 %146
  store %struct.d_derived_tbl** %add.ptr146, %struct.d_derived_tbl*** %pdtbl, align 4, !tbaa !2
  %147 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %148 = load i32, i32* %tbl, align 4, !tbaa !20
  %149 = load %struct.d_derived_tbl**, %struct.d_derived_tbl*** %pdtbl, align 4, !tbaa !2
  call void @jpeg_make_d_derived_tbl(%struct.jpeg_decompress_struct* %147, i32 0, i32 %148, %struct.d_derived_tbl** %149)
  %150 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %derived_tbls147 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %150, i32 0, i32 4
  %151 = load i32, i32* %tbl, align 4, !tbaa !20
  %arrayidx148 = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %derived_tbls147, i32 0, i32 %151
  %152 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %arrayidx148, align 4, !tbaa !2
  %153 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_derived_tbl = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %153, i32 0, i32 5
  store %struct.d_derived_tbl* %152, %struct.d_derived_tbl** %ac_derived_tbl, align 4, !tbaa !39
  br label %if.end149

if.end149:                                        ; preds = %if.else143, %if.end142
  %154 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %154, i32 0, i32 2
  %last_dc_val = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved, i32 0, i32 1
  %155 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx150 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %155
  store i32 0, i32* %arrayidx150, align 4, !tbaa !20
  br label %for.inc151

for.inc151:                                       ; preds = %if.end149
  %156 = load i32, i32* %ci, align 4, !tbaa !20
  %inc152 = add nsw i32 %156, 1
  store i32 %inc152, i32* %ci, align 4, !tbaa !20
  br label %for.cond129

for.end153:                                       ; preds = %for.cond129
  %157 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %157, i32 0, i32 1
  %bits_left = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 1
  store i32 0, i32* %bits_left, align 4, !tbaa !40
  %158 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate154 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %158, i32 0, i32 1
  %get_buffer = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate154, i32 0, i32 0
  store i32 0, i32* %get_buffer, align 4, !tbaa !41
  %159 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub155 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %159, i32 0, i32 0
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub155, i32 0, i32 2
  store i32 0, i32* %insufficient_data, align 4, !tbaa !42
  %160 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved156 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %160, i32 0, i32 2
  %EOBRUN = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved156, i32 0, i32 0
  store i32 0, i32* %EOBRUN, align 4, !tbaa !43
  %161 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %161, i32 0, i32 50
  %162 = load i32, i32* %restart_interval, align 4, !tbaa !44
  %163 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %163, i32 0, i32 3
  store i32 %162, i32* %restarts_to_go, align 4, !tbaa !45
  %164 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #3
  %165 = bitcast i32** %coef_bit_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #3
  %166 = bitcast %struct.d_derived_tbl*** %pdtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #3
  %167 = bitcast i32* %tbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #3
  %168 = bitcast i32* %coefi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #3
  %169 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #3
  %170 = bitcast i32* %bad to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #3
  %171 = bitcast i32* %is_DC_band to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #3
  %172 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @decode_mcu_DC_first(%struct.jpeg_decompress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.phuff_entropy_decoder*, align 4
  %Al = alloca i32, align 4
  %s = alloca i32, align 4
  %r = alloca i32, align 4
  %blkn = alloca i32, align 4
  %ci = alloca i32, align 4
  %block = alloca [64 x i16]*, align 4
  %get_buffer = alloca i32, align 4
  %bits_left = alloca i32, align 4
  %br_state = alloca %struct.bitread_working_state, align 4
  %state = alloca %struct.savable_state, align 4
  %tbl = alloca %struct.d_derived_tbl*, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %nb = alloca i32, align 4
  %look = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.phuff_entropy_decoder*
  store %struct.phuff_entropy_decoder* %3, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 75
  %6 = load i32, i32* %Al2, align 4, !tbaa !27
  store i32 %6, i32* %Al, align 4, !tbaa !20
  %7 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %14) #3
  %15 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %15) #3
  %16 = bitcast %struct.d_derived_tbl** %tbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 50
  %19 = load i32, i32* %restart_interval, align 4, !tbaa !44
  %tobool = icmp ne i32 %19, 0
  br i1 %tobool, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %20 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %20, i32 0, i32 3
  %21 = load i32, i32* %restarts_to_go, align 4, !tbaa !45
  %cmp = icmp eq i32 %21, 0
  br i1 %cmp, label %if.then3, label %if.end6

if.then3:                                         ; preds = %if.then
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @process_restart(%struct.jpeg_decompress_struct* %22)
  %tobool4 = icmp ne i32 %call, 0
  br i1 %tobool4, label %if.end, label %if.then5

if.then5:                                         ; preds = %if.then3
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup89

if.end:                                           ; preds = %if.then3
  br label %if.end6

if.end6:                                          ; preds = %if.end, %if.then
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %entry
  %23 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %23, i32 0, i32 0
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 2
  %24 = load i32, i32* %insufficient_data, align 4, !tbaa !42
  %tobool8 = icmp ne i32 %24, 0
  br i1 %tobool8, label %if.end87, label %if.then9

if.then9:                                         ; preds = %if.end7
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cinfo10 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 4
  store %struct.jpeg_decompress_struct* %25, %struct.jpeg_decompress_struct** %cinfo10, align 4, !tbaa !46
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 6
  %27 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !48
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %27, i32 0, i32 0
  %28 = load i8*, i8** %next_input_byte, align 4, !tbaa !49
  %next_input_byte11 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  store i8* %28, i8** %next_input_byte11, align 4, !tbaa !51
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 6
  %30 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src12, align 8, !tbaa !48
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %30, i32 0, i32 1
  %31 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !52
  %bytes_in_buffer13 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  store i32 %31, i32* %bytes_in_buffer13, align 4, !tbaa !53
  %32 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %32, i32 0, i32 1
  %get_buffer14 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 0
  %33 = load i32, i32* %get_buffer14, align 4, !tbaa !41
  store i32 %33, i32* %get_buffer, align 4, !tbaa !54
  %34 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate15 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %34, i32 0, i32 1
  %bits_left16 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate15, i32 0, i32 1
  %35 = load i32, i32* %bits_left16, align 4, !tbaa !40
  store i32 %35, i32* %bits_left, align 4, !tbaa !20
  %36 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %36, i32 0, i32 2
  %37 = bitcast %struct.savable_state* %state to i8*
  %38 = bitcast %struct.savable_state* %saved to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 20, i1 false), !tbaa.struct !55
  store i32 0, i32* %blkn, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then9
  %39 = load i32, i32* %blkn, align 4, !tbaa !20
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 70
  %41 = load i32, i32* %blocks_in_MCU, align 4, !tbaa !56
  %cmp17 = icmp slt i32 %39, %41
  br i1 %cmp17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %42 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %43 = load i32, i32* %blkn, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %42, i32 %43
  %44 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  store [64 x i16]* %44, [64 x i16]** %block, align 4, !tbaa !2
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 71
  %46 = load i32, i32* %blkn, align 4, !tbaa !20
  %arrayidx18 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx18, align 4, !tbaa !20
  store i32 %47, i32* %ci, align 4, !tbaa !20
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 67
  %49 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx19 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %49
  %50 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx19, align 4, !tbaa !2
  store %struct.jpeg_component_info* %50, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %51 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %derived_tbls = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %51, i32 0, i32 4
  %52 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %52, i32 0, i32 5
  %53 = load i32, i32* %dc_tbl_no, align 4, !tbaa !37
  %arrayidx20 = getelementptr inbounds [4 x %struct.d_derived_tbl*], [4 x %struct.d_derived_tbl*]* %derived_tbls, i32 0, i32 %53
  %54 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %arrayidx20, align 4, !tbaa !2
  store %struct.d_derived_tbl* %54, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %55 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %look to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #3
  %57 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp21 = icmp slt i32 %57, 8
  br i1 %cmp21, label %if.then22, label %if.end32

if.then22:                                        ; preds = %for.body
  %58 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %59 = load i32, i32* %bits_left, align 4, !tbaa !20
  %call23 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %58, i32 %59, i32 0)
  %tobool24 = icmp ne i32 %call23, 0
  br i1 %tobool24, label %if.end26, label %if.then25

if.then25:                                        ; preds = %if.then22
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end26:                                         ; preds = %if.then22
  %get_buffer27 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %60 = load i32, i32* %get_buffer27, align 4, !tbaa !57
  store i32 %60, i32* %get_buffer, align 4, !tbaa !54
  %bits_left28 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %61 = load i32, i32* %bits_left28, align 4, !tbaa !58
  store i32 %61, i32* %bits_left, align 4, !tbaa !20
  %62 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp29 = icmp slt i32 %62, 8
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.end26
  store i32 1, i32* %nb, align 4, !tbaa !20
  br label %label1

if.end31:                                         ; preds = %if.end26
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %for.body
  %63 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %64 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub = sub nsw i32 %64, 8
  %shr = lshr i32 %63, %sub
  %and = and i32 %shr, 255
  store i32 %and, i32* %look, align 4, !tbaa !20
  %65 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %lookup = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %65, i32 0, i32 3
  %66 = load i32, i32* %look, align 4, !tbaa !20
  %arrayidx33 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup, i32 0, i32 %66
  %67 = load i32, i32* %arrayidx33, align 4, !tbaa !20
  %shr34 = ashr i32 %67, 8
  store i32 %shr34, i32* %nb, align 4, !tbaa !20
  %cmp35 = icmp sle i32 %shr34, 8
  br i1 %cmp35, label %if.then36, label %if.else

if.then36:                                        ; preds = %if.end32
  %68 = load i32, i32* %nb, align 4, !tbaa !20
  %69 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub37 = sub nsw i32 %69, %68
  store i32 %sub37, i32* %bits_left, align 4, !tbaa !20
  %70 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %lookup38 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %70, i32 0, i32 3
  %71 = load i32, i32* %look, align 4, !tbaa !20
  %arrayidx39 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup38, i32 0, i32 %71
  %72 = load i32, i32* %arrayidx39, align 4, !tbaa !20
  %and40 = and i32 %72, 255
  store i32 %and40, i32* %s, align 4, !tbaa !20
  br label %if.end47

if.else:                                          ; preds = %if.end32
  br label %label1

label1:                                           ; preds = %if.else, %if.then30
  %73 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %74 = load i32, i32* %bits_left, align 4, !tbaa !20
  %75 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %76 = load i32, i32* %nb, align 4, !tbaa !20
  %call41 = call i32 @jpeg_huff_decode(%struct.bitread_working_state* %br_state, i32 %73, i32 %74, %struct.d_derived_tbl* %75, i32 %76)
  store i32 %call41, i32* %s, align 4, !tbaa !20
  %cmp42 = icmp slt i32 %call41, 0
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %label1
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end44:                                         ; preds = %label1
  %get_buffer45 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %77 = load i32, i32* %get_buffer45, align 4, !tbaa !57
  store i32 %77, i32* %get_buffer, align 4, !tbaa !54
  %bits_left46 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %78 = load i32, i32* %bits_left46, align 4, !tbaa !58
  store i32 %78, i32* %bits_left, align 4, !tbaa !20
  br label %if.end47

if.end47:                                         ; preds = %if.end44, %if.then36
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end47, %if.then43, %if.then25
  %79 = bitcast i32* %look to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  %80 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup89 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %81 = load i32, i32* %s, align 4, !tbaa !20
  %tobool49 = icmp ne i32 %81, 0
  br i1 %tobool49, label %if.then50, label %if.end69

if.then50:                                        ; preds = %cleanup.cont
  %82 = load i32, i32* %bits_left, align 4, !tbaa !20
  %83 = load i32, i32* %s, align 4, !tbaa !20
  %cmp51 = icmp slt i32 %82, %83
  br i1 %cmp51, label %if.then52, label %if.end59

if.then52:                                        ; preds = %if.then50
  %84 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %85 = load i32, i32* %bits_left, align 4, !tbaa !20
  %86 = load i32, i32* %s, align 4, !tbaa !20
  %call53 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %84, i32 %85, i32 %86)
  %tobool54 = icmp ne i32 %call53, 0
  br i1 %tobool54, label %if.end56, label %if.then55

if.then55:                                        ; preds = %if.then52
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup89

if.end56:                                         ; preds = %if.then52
  %get_buffer57 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %87 = load i32, i32* %get_buffer57, align 4, !tbaa !57
  store i32 %87, i32* %get_buffer, align 4, !tbaa !54
  %bits_left58 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %88 = load i32, i32* %bits_left58, align 4, !tbaa !58
  store i32 %88, i32* %bits_left, align 4, !tbaa !20
  br label %if.end59

if.end59:                                         ; preds = %if.end56, %if.then50
  %89 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %90 = load i32, i32* %s, align 4, !tbaa !20
  %91 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub60 = sub nsw i32 %91, %90
  store i32 %sub60, i32* %bits_left, align 4, !tbaa !20
  %shr61 = lshr i32 %89, %sub60
  %92 = load i32, i32* %s, align 4, !tbaa !20
  %shl = shl i32 1, %92
  %sub62 = sub nsw i32 %shl, 1
  %and63 = and i32 %shr61, %sub62
  store i32 %and63, i32* %r, align 4, !tbaa !20
  %93 = load i32, i32* %r, align 4, !tbaa !20
  %94 = load i32, i32* %s, align 4, !tbaa !20
  %sub64 = sub nsw i32 %94, 1
  %shl65 = shl i32 1, %sub64
  %cmp66 = icmp slt i32 %93, %shl65
  br i1 %cmp66, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end59
  %95 = load i32, i32* %r, align 4, !tbaa !20
  %96 = load i32, i32* %s, align 4, !tbaa !20
  %shl67 = shl i32 -1, %96
  %add = add i32 %shl67, 1
  %add68 = add i32 %95, %add
  br label %cond.end

cond.false:                                       ; preds = %if.end59
  %97 = load i32, i32* %r, align 4, !tbaa !20
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add68, %cond.true ], [ %97, %cond.false ]
  store i32 %cond, i32* %s, align 4, !tbaa !20
  br label %if.end69

if.end69:                                         ; preds = %cond.end, %cleanup.cont
  %last_dc_val = getelementptr inbounds %struct.savable_state, %struct.savable_state* %state, i32 0, i32 1
  %98 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx70 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %98
  %99 = load i32, i32* %arrayidx70, align 4, !tbaa !20
  %100 = load i32, i32* %s, align 4, !tbaa !20
  %add71 = add nsw i32 %100, %99
  store i32 %add71, i32* %s, align 4, !tbaa !20
  %101 = load i32, i32* %s, align 4, !tbaa !20
  %last_dc_val72 = getelementptr inbounds %struct.savable_state, %struct.savable_state* %state, i32 0, i32 1
  %102 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx73 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val72, i32 0, i32 %102
  store i32 %101, i32* %arrayidx73, align 4, !tbaa !20
  %103 = load i32, i32* %s, align 4, !tbaa !20
  %104 = load i32, i32* %Al, align 4, !tbaa !20
  %shl74 = shl i32 %103, %104
  %conv = trunc i32 %shl74 to i16
  %105 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds [64 x i16], [64 x i16]* %105, i32 0, i32 0
  store i16 %conv, i16* %arrayidx75, align 2, !tbaa !59
  br label %for.inc

for.inc:                                          ; preds = %if.end69
  %106 = load i32, i32* %blkn, align 4, !tbaa !20
  %inc = add nsw i32 %106, 1
  store i32 %inc, i32* %blkn, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %next_input_byte76 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  %107 = load i8*, i8** %next_input_byte76, align 4, !tbaa !51
  %108 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src77 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %108, i32 0, i32 6
  %109 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src77, align 8, !tbaa !48
  %next_input_byte78 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %109, i32 0, i32 0
  store i8* %107, i8** %next_input_byte78, align 4, !tbaa !49
  %bytes_in_buffer79 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  %110 = load i32, i32* %bytes_in_buffer79, align 4, !tbaa !53
  %111 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src80 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %111, i32 0, i32 6
  %112 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src80, align 8, !tbaa !48
  %bytes_in_buffer81 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %112, i32 0, i32 1
  store i32 %110, i32* %bytes_in_buffer81, align 4, !tbaa !52
  %113 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %114 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate82 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %114, i32 0, i32 1
  %get_buffer83 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate82, i32 0, i32 0
  store i32 %113, i32* %get_buffer83, align 4, !tbaa !41
  %115 = load i32, i32* %bits_left, align 4, !tbaa !20
  %116 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate84 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %116, i32 0, i32 1
  %bits_left85 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate84, i32 0, i32 1
  store i32 %115, i32* %bits_left85, align 4, !tbaa !40
  %117 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved86 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %117, i32 0, i32 2
  %118 = bitcast %struct.savable_state* %saved86 to i8*
  %119 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %118, i8* align 4 %119, i32 20, i1 false), !tbaa.struct !55
  br label %if.end87

if.end87:                                         ; preds = %for.end, %if.end7
  %120 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go88 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %120, i32 0, i32 3
  %121 = load i32, i32* %restarts_to_go88, align 4, !tbaa !45
  %dec = add i32 %121, -1
  store i32 %dec, i32* %restarts_to_go88, align 4, !tbaa !45
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup89

cleanup89:                                        ; preds = %if.end87, %if.then55, %cleanup, %if.then5
  %122 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast %struct.d_derived_tbl** %tbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast %struct.savable_state* %state to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %124) #3
  %125 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %125) #3
  %126 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #3
  %129 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #3
  %130 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #3
  %131 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #3
  %132 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #3
  %133 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #3
  %134 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #3
  %135 = load i32, i32* %retval, align 4
  ret i32 %135
}

; Function Attrs: nounwind
define internal i32 @decode_mcu_AC_first(%struct.jpeg_decompress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.phuff_entropy_decoder*, align 4
  %Se = alloca i32, align 4
  %Al = alloca i32, align 4
  %s = alloca i32, align 4
  %k = alloca i32, align 4
  %r = alloca i32, align 4
  %EOBRUN = alloca i32, align 4
  %block = alloca [64 x i16]*, align 4
  %get_buffer = alloca i32, align 4
  %bits_left = alloca i32, align 4
  %br_state = alloca %struct.bitread_working_state, align 4
  %tbl = alloca %struct.d_derived_tbl*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %nb = alloca i32, align 4
  %look = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.phuff_entropy_decoder*
  store %struct.phuff_entropy_decoder* %3, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 73
  %6 = load i32, i32* %Se2, align 4, !tbaa !24
  store i32 %6, i32* %Se, align 4, !tbaa !20
  %7 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 75
  %9 = load i32, i32* %Al3, align 4, !tbaa !27
  store i32 %9, i32* %Al, align 4, !tbaa !20
  %10 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %EOBRUN to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %17) #3
  %18 = bitcast %struct.d_derived_tbl** %tbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 50
  %20 = load i32, i32* %restart_interval, align 4, !tbaa !44
  %tobool = icmp ne i32 %20, 0
  br i1 %tobool, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %21 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %21, i32 0, i32 3
  %22 = load i32, i32* %restarts_to_go, align 4, !tbaa !45
  %cmp = icmp eq i32 %22, 0
  br i1 %cmp, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.then
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @process_restart(%struct.jpeg_decompress_struct* %23)
  %tobool5 = icmp ne i32 %call, 0
  br i1 %tobool5, label %if.end, label %if.then6

if.then6:                                         ; preds = %if.then4
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup122

if.end:                                           ; preds = %if.then4
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %entry
  %24 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %24, i32 0, i32 0
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 2
  %25 = load i32, i32* %insufficient_data, align 4, !tbaa !42
  %tobool9 = icmp ne i32 %25, 0
  br i1 %tobool9, label %if.end119, label %if.then10

if.then10:                                        ; preds = %if.end8
  %26 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %26, i32 0, i32 2
  %EOBRUN11 = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved, i32 0, i32 0
  %27 = load i32, i32* %EOBRUN11, align 4, !tbaa !43
  store i32 %27, i32* %EOBRUN, align 4, !tbaa !20
  %28 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %cmp12 = icmp ugt i32 %28, 0
  br i1 %cmp12, label %if.then13, label %if.else

if.then13:                                        ; preds = %if.then10
  %29 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %dec = add i32 %29, -1
  store i32 %dec, i32* %EOBRUN, align 4, !tbaa !20
  br label %if.end116

if.else:                                          ; preds = %if.then10
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cinfo14 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 4
  store %struct.jpeg_decompress_struct* %30, %struct.jpeg_decompress_struct** %cinfo14, align 4, !tbaa !46
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 6
  %32 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !48
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %32, i32 0, i32 0
  %33 = load i8*, i8** %next_input_byte, align 4, !tbaa !49
  %next_input_byte15 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  store i8* %33, i8** %next_input_byte15, align 4, !tbaa !51
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 6
  %35 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src16, align 8, !tbaa !48
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %35, i32 0, i32 1
  %36 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !52
  %bytes_in_buffer17 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  store i32 %36, i32* %bytes_in_buffer17, align 4, !tbaa !53
  %37 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %37, i32 0, i32 1
  %get_buffer18 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 0
  %38 = load i32, i32* %get_buffer18, align 4, !tbaa !41
  store i32 %38, i32* %get_buffer, align 4, !tbaa !54
  %39 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate19 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %39, i32 0, i32 1
  %bits_left20 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate19, i32 0, i32 1
  %40 = load i32, i32* %bits_left20, align 4, !tbaa !40
  store i32 %40, i32* %bits_left, align 4, !tbaa !20
  %41 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %41, i32 0
  %42 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  store [64 x i16]* %42, [64 x i16]** %block, align 4, !tbaa !2
  %43 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_derived_tbl = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %43, i32 0, i32 5
  %44 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %ac_derived_tbl, align 4, !tbaa !39
  store %struct.d_derived_tbl* %44, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 72
  %46 = load i32, i32* %Ss, align 8, !tbaa !23
  store i32 %46, i32* %k, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %47 = load i32, i32* %k, align 4, !tbaa !20
  %48 = load i32, i32* %Se, align 4, !tbaa !20
  %cmp21 = icmp sle i32 %47, %48
  br i1 %cmp21, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %49 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #3
  %50 = bitcast i32* %look to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #3
  %51 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp22 = icmp slt i32 %51, 8
  br i1 %cmp22, label %if.then23, label %if.end33

if.then23:                                        ; preds = %for.body
  %52 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %53 = load i32, i32* %bits_left, align 4, !tbaa !20
  %call24 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %52, i32 %53, i32 0)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %if.then23
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %if.then23
  %get_buffer28 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %54 = load i32, i32* %get_buffer28, align 4, !tbaa !57
  store i32 %54, i32* %get_buffer, align 4, !tbaa !54
  %bits_left29 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %55 = load i32, i32* %bits_left29, align 4, !tbaa !58
  store i32 %55, i32* %bits_left, align 4, !tbaa !20
  %56 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp30 = icmp slt i32 %56, 8
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end27
  store i32 1, i32* %nb, align 4, !tbaa !20
  br label %label2

if.end32:                                         ; preds = %if.end27
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %for.body
  %57 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %58 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub = sub nsw i32 %58, 8
  %shr = lshr i32 %57, %sub
  %and = and i32 %shr, 255
  store i32 %and, i32* %look, align 4, !tbaa !20
  %59 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %lookup = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %59, i32 0, i32 3
  %60 = load i32, i32* %look, align 4, !tbaa !20
  %arrayidx34 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup, i32 0, i32 %60
  %61 = load i32, i32* %arrayidx34, align 4, !tbaa !20
  %shr35 = ashr i32 %61, 8
  store i32 %shr35, i32* %nb, align 4, !tbaa !20
  %cmp36 = icmp sle i32 %shr35, 8
  br i1 %cmp36, label %if.then37, label %if.else42

if.then37:                                        ; preds = %if.end33
  %62 = load i32, i32* %nb, align 4, !tbaa !20
  %63 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub38 = sub nsw i32 %63, %62
  store i32 %sub38, i32* %bits_left, align 4, !tbaa !20
  %64 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %lookup39 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %64, i32 0, i32 3
  %65 = load i32, i32* %look, align 4, !tbaa !20
  %arrayidx40 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup39, i32 0, i32 %65
  %66 = load i32, i32* %arrayidx40, align 4, !tbaa !20
  %and41 = and i32 %66, 255
  store i32 %and41, i32* %s, align 4, !tbaa !20
  br label %if.end49

if.else42:                                        ; preds = %if.end33
  br label %label2

label2:                                           ; preds = %if.else42, %if.then31
  %67 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %68 = load i32, i32* %bits_left, align 4, !tbaa !20
  %69 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %70 = load i32, i32* %nb, align 4, !tbaa !20
  %call43 = call i32 @jpeg_huff_decode(%struct.bitread_working_state* %br_state, i32 %67, i32 %68, %struct.d_derived_tbl* %69, i32 %70)
  store i32 %call43, i32* %s, align 4, !tbaa !20
  %cmp44 = icmp slt i32 %call43, 0
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %label2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %label2
  %get_buffer47 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %71 = load i32, i32* %get_buffer47, align 4, !tbaa !57
  store i32 %71, i32* %get_buffer, align 4, !tbaa !54
  %bits_left48 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %72 = load i32, i32* %bits_left48, align 4, !tbaa !58
  store i32 %72, i32* %bits_left, align 4, !tbaa !20
  br label %if.end49

if.end49:                                         ; preds = %if.end46, %if.then37
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end49, %if.then45, %if.then26
  %73 = bitcast i32* %look to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  %74 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup122 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %75 = load i32, i32* %s, align 4, !tbaa !20
  %shr51 = ashr i32 %75, 4
  store i32 %shr51, i32* %r, align 4, !tbaa !20
  %76 = load i32, i32* %s, align 4, !tbaa !20
  %and52 = and i32 %76, 15
  store i32 %and52, i32* %s, align 4, !tbaa !20
  %77 = load i32, i32* %s, align 4, !tbaa !20
  %tobool53 = icmp ne i32 %77, 0
  br i1 %tobool53, label %if.then54, label %if.else77

if.then54:                                        ; preds = %cleanup.cont
  %78 = load i32, i32* %r, align 4, !tbaa !20
  %79 = load i32, i32* %k, align 4, !tbaa !20
  %add = add nsw i32 %79, %78
  store i32 %add, i32* %k, align 4, !tbaa !20
  %80 = load i32, i32* %bits_left, align 4, !tbaa !20
  %81 = load i32, i32* %s, align 4, !tbaa !20
  %cmp55 = icmp slt i32 %80, %81
  br i1 %cmp55, label %if.then56, label %if.end63

if.then56:                                        ; preds = %if.then54
  %82 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %83 = load i32, i32* %bits_left, align 4, !tbaa !20
  %84 = load i32, i32* %s, align 4, !tbaa !20
  %call57 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %82, i32 %83, i32 %84)
  %tobool58 = icmp ne i32 %call57, 0
  br i1 %tobool58, label %if.end60, label %if.then59

if.then59:                                        ; preds = %if.then56
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup122

if.end60:                                         ; preds = %if.then56
  %get_buffer61 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %85 = load i32, i32* %get_buffer61, align 4, !tbaa !57
  store i32 %85, i32* %get_buffer, align 4, !tbaa !54
  %bits_left62 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %86 = load i32, i32* %bits_left62, align 4, !tbaa !58
  store i32 %86, i32* %bits_left, align 4, !tbaa !20
  br label %if.end63

if.end63:                                         ; preds = %if.end60, %if.then54
  %87 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %88 = load i32, i32* %s, align 4, !tbaa !20
  %89 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub64 = sub nsw i32 %89, %88
  store i32 %sub64, i32* %bits_left, align 4, !tbaa !20
  %shr65 = lshr i32 %87, %sub64
  %90 = load i32, i32* %s, align 4, !tbaa !20
  %shl = shl i32 1, %90
  %sub66 = sub nsw i32 %shl, 1
  %and67 = and i32 %shr65, %sub66
  store i32 %and67, i32* %r, align 4, !tbaa !20
  %91 = load i32, i32* %r, align 4, !tbaa !20
  %92 = load i32, i32* %s, align 4, !tbaa !20
  %sub68 = sub nsw i32 %92, 1
  %shl69 = shl i32 1, %sub68
  %cmp70 = icmp slt i32 %91, %shl69
  br i1 %cmp70, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end63
  %93 = load i32, i32* %r, align 4, !tbaa !20
  %94 = load i32, i32* %s, align 4, !tbaa !20
  %shl71 = shl i32 -1, %94
  %add72 = add i32 %shl71, 1
  %add73 = add i32 %93, %add72
  br label %cond.end

cond.false:                                       ; preds = %if.end63
  %95 = load i32, i32* %r, align 4, !tbaa !20
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add73, %cond.true ], [ %95, %cond.false ]
  store i32 %cond, i32* %s, align 4, !tbaa !20
  %96 = load i32, i32* %s, align 4, !tbaa !20
  %97 = load i32, i32* %Al, align 4, !tbaa !20
  %shl74 = shl i32 %96, %97
  %conv = trunc i32 %shl74 to i16
  %98 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %99 = load i32, i32* %k, align 4, !tbaa !20
  %arrayidx75 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %99
  %100 = load i32, i32* %arrayidx75, align 4, !tbaa !20
  %arrayidx76 = getelementptr inbounds [64 x i16], [64 x i16]* %98, i32 0, i32 %100
  store i16 %conv, i16* %arrayidx76, align 2, !tbaa !59
  br label %if.end105

if.else77:                                        ; preds = %cleanup.cont
  %101 = load i32, i32* %r, align 4, !tbaa !20
  %cmp78 = icmp eq i32 %101, 15
  br i1 %cmp78, label %if.then80, label %if.else82

if.then80:                                        ; preds = %if.else77
  %102 = load i32, i32* %k, align 4, !tbaa !20
  %add81 = add nsw i32 %102, 15
  store i32 %add81, i32* %k, align 4, !tbaa !20
  br label %if.end104

if.else82:                                        ; preds = %if.else77
  %103 = load i32, i32* %r, align 4, !tbaa !20
  %shl83 = shl i32 1, %103
  store i32 %shl83, i32* %EOBRUN, align 4, !tbaa !20
  %104 = load i32, i32* %r, align 4, !tbaa !20
  %tobool84 = icmp ne i32 %104, 0
  br i1 %tobool84, label %if.then85, label %if.end102

if.then85:                                        ; preds = %if.else82
  %105 = load i32, i32* %bits_left, align 4, !tbaa !20
  %106 = load i32, i32* %r, align 4, !tbaa !20
  %cmp86 = icmp slt i32 %105, %106
  br i1 %cmp86, label %if.then88, label %if.end95

if.then88:                                        ; preds = %if.then85
  %107 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %108 = load i32, i32* %bits_left, align 4, !tbaa !20
  %109 = load i32, i32* %r, align 4, !tbaa !20
  %call89 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %107, i32 %108, i32 %109)
  %tobool90 = icmp ne i32 %call89, 0
  br i1 %tobool90, label %if.end92, label %if.then91

if.then91:                                        ; preds = %if.then88
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup122

if.end92:                                         ; preds = %if.then88
  %get_buffer93 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %110 = load i32, i32* %get_buffer93, align 4, !tbaa !57
  store i32 %110, i32* %get_buffer, align 4, !tbaa !54
  %bits_left94 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %111 = load i32, i32* %bits_left94, align 4, !tbaa !58
  store i32 %111, i32* %bits_left, align 4, !tbaa !20
  br label %if.end95

if.end95:                                         ; preds = %if.end92, %if.then85
  %112 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %113 = load i32, i32* %r, align 4, !tbaa !20
  %114 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub96 = sub nsw i32 %114, %113
  store i32 %sub96, i32* %bits_left, align 4, !tbaa !20
  %shr97 = lshr i32 %112, %sub96
  %115 = load i32, i32* %r, align 4, !tbaa !20
  %shl98 = shl i32 1, %115
  %sub99 = sub nsw i32 %shl98, 1
  %and100 = and i32 %shr97, %sub99
  store i32 %and100, i32* %r, align 4, !tbaa !20
  %116 = load i32, i32* %r, align 4, !tbaa !20
  %117 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %add101 = add i32 %117, %116
  store i32 %add101, i32* %EOBRUN, align 4, !tbaa !20
  br label %if.end102

if.end102:                                        ; preds = %if.end95, %if.else82
  %118 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %dec103 = add i32 %118, -1
  store i32 %dec103, i32* %EOBRUN, align 4, !tbaa !20
  br label %for.end

if.end104:                                        ; preds = %if.then80
  br label %if.end105

if.end105:                                        ; preds = %if.end104, %cond.end
  br label %for.inc

for.inc:                                          ; preds = %if.end105
  %119 = load i32, i32* %k, align 4, !tbaa !20
  %inc = add nsw i32 %119, 1
  store i32 %inc, i32* %k, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %if.end102, %for.cond
  %next_input_byte106 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  %120 = load i8*, i8** %next_input_byte106, align 4, !tbaa !51
  %121 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src107 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %121, i32 0, i32 6
  %122 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src107, align 8, !tbaa !48
  %next_input_byte108 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %122, i32 0, i32 0
  store i8* %120, i8** %next_input_byte108, align 4, !tbaa !49
  %bytes_in_buffer109 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  %123 = load i32, i32* %bytes_in_buffer109, align 4, !tbaa !53
  %124 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src110 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %124, i32 0, i32 6
  %125 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src110, align 8, !tbaa !48
  %bytes_in_buffer111 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %125, i32 0, i32 1
  store i32 %123, i32* %bytes_in_buffer111, align 4, !tbaa !52
  %126 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %127 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate112 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %127, i32 0, i32 1
  %get_buffer113 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate112, i32 0, i32 0
  store i32 %126, i32* %get_buffer113, align 4, !tbaa !41
  %128 = load i32, i32* %bits_left, align 4, !tbaa !20
  %129 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate114 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %129, i32 0, i32 1
  %bits_left115 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate114, i32 0, i32 1
  store i32 %128, i32* %bits_left115, align 4, !tbaa !40
  br label %if.end116

if.end116:                                        ; preds = %for.end, %if.then13
  %130 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %131 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved117 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %131, i32 0, i32 2
  %EOBRUN118 = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved117, i32 0, i32 0
  store i32 %130, i32* %EOBRUN118, align 4, !tbaa !43
  br label %if.end119

if.end119:                                        ; preds = %if.end116, %if.end8
  %132 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go120 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %132, i32 0, i32 3
  %133 = load i32, i32* %restarts_to_go120, align 4, !tbaa !45
  %dec121 = add i32 %133, -1
  store i32 %dec121, i32* %restarts_to_go120, align 4, !tbaa !45
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup122

cleanup122:                                       ; preds = %if.end119, %if.then91, %if.then59, %cleanup, %if.then6
  %134 = bitcast %struct.d_derived_tbl** %tbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #3
  %135 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %135) #3
  %136 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #3
  %137 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #3
  %138 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #3
  %139 = bitcast i32* %EOBRUN to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #3
  %140 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #3
  %141 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #3
  %142 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #3
  %143 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #3
  %144 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #3
  %145 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #3
  %146 = load i32, i32* %retval, align 4
  ret i32 %146
}

; Function Attrs: nounwind
define internal i32 @decode_mcu_DC_refine(%struct.jpeg_decompress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.phuff_entropy_decoder*, align 4
  %p1 = alloca i32, align 4
  %blkn = alloca i32, align 4
  %block = alloca [64 x i16]*, align 4
  %get_buffer = alloca i32, align 4
  %bits_left = alloca i32, align 4
  %br_state = alloca %struct.bitread_working_state, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.phuff_entropy_decoder*
  store %struct.phuff_entropy_decoder* %3, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 75
  %6 = load i32, i32* %Al, align 4, !tbaa !27
  %shl = shl i32 1, %6
  store i32 %shl, i32* %p1, align 4, !tbaa !20
  %7 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %11) #3
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 50
  %13 = load i32, i32* %restart_interval, align 4, !tbaa !44
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %14 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %14, i32 0, i32 3
  %15 = load i32, i32* %restarts_to_go, align 4, !tbaa !45
  %cmp = icmp eq i32 %15, 0
  br i1 %cmp, label %if.then2, label %if.end5

if.then2:                                         ; preds = %if.then
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @process_restart(%struct.jpeg_decompress_struct* %16)
  %tobool3 = icmp ne i32 %call, 0
  br i1 %tobool3, label %if.end, label %if.then4

if.then4:                                         ; preds = %if.then2
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then2
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  br label %if.end6

if.end6:                                          ; preds = %if.end5, %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cinfo7 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 4
  store %struct.jpeg_decompress_struct* %17, %struct.jpeg_decompress_struct** %cinfo7, align 4, !tbaa !46
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 6
  %19 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !48
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %19, i32 0, i32 0
  %20 = load i8*, i8** %next_input_byte, align 4, !tbaa !49
  %next_input_byte8 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  store i8* %20, i8** %next_input_byte8, align 4, !tbaa !51
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 6
  %22 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src9, align 8, !tbaa !48
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %22, i32 0, i32 1
  %23 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !52
  %bytes_in_buffer10 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  store i32 %23, i32* %bytes_in_buffer10, align 4, !tbaa !53
  %24 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %24, i32 0, i32 1
  %get_buffer11 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 0
  %25 = load i32, i32* %get_buffer11, align 4, !tbaa !41
  store i32 %25, i32* %get_buffer, align 4, !tbaa !54
  %26 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate12 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %26, i32 0, i32 1
  %bits_left13 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate12, i32 0, i32 1
  %27 = load i32, i32* %bits_left13, align 4, !tbaa !40
  store i32 %27, i32* %bits_left, align 4, !tbaa !20
  store i32 0, i32* %blkn, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end6
  %28 = load i32, i32* %blkn, align 4, !tbaa !20
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 70
  %30 = load i32, i32* %blocks_in_MCU, align 4, !tbaa !56
  %cmp14 = icmp slt i32 %28, %30
  br i1 %cmp14, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %32 = load i32, i32* %blkn, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %31, i32 %32
  %33 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  store [64 x i16]* %33, [64 x i16]** %block, align 4, !tbaa !2
  %34 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp15 = icmp slt i32 %34, 1
  br i1 %cmp15, label %if.then16, label %if.end23

if.then16:                                        ; preds = %for.body
  %35 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %36 = load i32, i32* %bits_left, align 4, !tbaa !20
  %call17 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %35, i32 %36, i32 1)
  %tobool18 = icmp ne i32 %call17, 0
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %if.then16
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end20:                                         ; preds = %if.then16
  %get_buffer21 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %37 = load i32, i32* %get_buffer21, align 4, !tbaa !57
  store i32 %37, i32* %get_buffer, align 4, !tbaa !54
  %bits_left22 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %38 = load i32, i32* %bits_left22, align 4, !tbaa !58
  store i32 %38, i32* %bits_left, align 4, !tbaa !20
  br label %if.end23

if.end23:                                         ; preds = %if.end20, %for.body
  %39 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %40 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub = sub nsw i32 %40, 1
  store i32 %sub, i32* %bits_left, align 4, !tbaa !20
  %shr = lshr i32 %39, %sub
  %and = and i32 %shr, 1
  %tobool24 = icmp ne i32 %and, 0
  br i1 %tobool24, label %if.then25, label %if.end28

if.then25:                                        ; preds = %if.end23
  %41 = load i32, i32* %p1, align 4, !tbaa !20
  %42 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds [64 x i16], [64 x i16]* %42, i32 0, i32 0
  %43 = load i16, i16* %arrayidx26, align 2, !tbaa !59
  %conv = sext i16 %43 to i32
  %or = or i32 %conv, %41
  %conv27 = trunc i32 %or to i16
  store i16 %conv27, i16* %arrayidx26, align 2, !tbaa !59
  br label %if.end28

if.end28:                                         ; preds = %if.then25, %if.end23
  br label %for.inc

for.inc:                                          ; preds = %if.end28
  %44 = load i32, i32* %blkn, align 4, !tbaa !20
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %blkn, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %next_input_byte29 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  %45 = load i8*, i8** %next_input_byte29, align 4, !tbaa !51
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src30 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 6
  %47 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src30, align 8, !tbaa !48
  %next_input_byte31 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %47, i32 0, i32 0
  store i8* %45, i8** %next_input_byte31, align 4, !tbaa !49
  %bytes_in_buffer32 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  %48 = load i32, i32* %bytes_in_buffer32, align 4, !tbaa !53
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src33 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 6
  %50 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src33, align 8, !tbaa !48
  %bytes_in_buffer34 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %50, i32 0, i32 1
  store i32 %48, i32* %bytes_in_buffer34, align 4, !tbaa !52
  %51 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %52 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate35 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %52, i32 0, i32 1
  %get_buffer36 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate35, i32 0, i32 0
  store i32 %51, i32* %get_buffer36, align 4, !tbaa !41
  %53 = load i32, i32* %bits_left, align 4, !tbaa !20
  %54 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate37 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %54, i32 0, i32 1
  %bits_left38 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate37, i32 0, i32 1
  store i32 %53, i32* %bits_left38, align 4, !tbaa !40
  %55 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go39 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %55, i32 0, i32 3
  %56 = load i32, i32* %restarts_to_go39, align 4, !tbaa !45
  %dec = add i32 %56, -1
  store i32 %dec, i32* %restarts_to_go39, align 4, !tbaa !45
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then19, %if.then4
  %57 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %57) #3
  %58 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  %60 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  %61 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #3
  %62 = bitcast i32* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #3
  %63 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #3
  %64 = load i32, i32* %retval, align 4
  ret i32 %64
}

; Function Attrs: nounwind
define internal i32 @decode_mcu_AC_refine(%struct.jpeg_decompress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.phuff_entropy_decoder*, align 4
  %Se = alloca i32, align 4
  %p1 = alloca i32, align 4
  %m1 = alloca i32, align 4
  %s = alloca i32, align 4
  %k = alloca i32, align 4
  %r = alloca i32, align 4
  %EOBRUN = alloca i32, align 4
  %block = alloca [64 x i16]*, align 4
  %thiscoef = alloca i16*, align 4
  %get_buffer = alloca i32, align 4
  %bits_left = alloca i32, align 4
  %br_state = alloca %struct.bitread_working_state, align 4
  %tbl = alloca %struct.d_derived_tbl*, align 4
  %num_newnz = alloca i32, align 4
  %newnz_pos = alloca [64 x i32], align 16
  %cleanup.dest.slot = alloca i32, align 4
  %nb = alloca i32, align 4
  %look = alloca i32, align 4
  %pos = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.phuff_entropy_decoder*
  store %struct.phuff_entropy_decoder* %3, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 73
  %6 = load i32, i32* %Se2, align 4, !tbaa !24
  store i32 %6, i32* %Se, align 4, !tbaa !20
  %7 = bitcast i32* %p1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 75
  %9 = load i32, i32* %Al, align 4, !tbaa !27
  %shl = shl i32 1, %9
  store i32 %shl, i32* %p1, align 4, !tbaa !20
  %10 = bitcast i32* %m1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 75
  %12 = load i32, i32* %Al3, align 4, !tbaa !27
  %shl4 = shl i32 -1, %12
  store i32 %shl4, i32* %m1, align 4, !tbaa !20
  %13 = bitcast i32* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %EOBRUN to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i16** %thiscoef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %21) #3
  %22 = bitcast %struct.d_derived_tbl** %tbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = bitcast i32* %num_newnz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = bitcast [64 x i32]* %newnz_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %24) #3
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 50
  %26 = load i32, i32* %restart_interval, align 4, !tbaa !44
  %tobool = icmp ne i32 %26, 0
  br i1 %tobool, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %27 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %27, i32 0, i32 3
  %28 = load i32, i32* %restarts_to_go, align 4, !tbaa !45
  %cmp = icmp eq i32 %28, 0
  br i1 %cmp, label %if.then5, label %if.end8

if.then5:                                         ; preds = %if.then
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @process_restart(%struct.jpeg_decompress_struct* %29)
  %tobool6 = icmp ne i32 %call, 0
  br i1 %tobool6, label %if.end, label %if.then7

if.then7:                                         ; preds = %if.then5
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup228

if.end:                                           ; preds = %if.then5
  br label %if.end8

if.end8:                                          ; preds = %if.end, %if.then
  br label %if.end9

if.end9:                                          ; preds = %if.end8, %entry
  %30 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %30, i32 0, i32 0
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 2
  %31 = load i32, i32* %insufficient_data, align 4, !tbaa !42
  %tobool10 = icmp ne i32 %31, 0
  br i1 %tobool10, label %if.end220, label %if.then11

if.then11:                                        ; preds = %if.end9
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cinfo12 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 4
  store %struct.jpeg_decompress_struct* %32, %struct.jpeg_decompress_struct** %cinfo12, align 4, !tbaa !46
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 6
  %34 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !48
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %34, i32 0, i32 0
  %35 = load i8*, i8** %next_input_byte, align 4, !tbaa !49
  %next_input_byte13 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  store i8* %35, i8** %next_input_byte13, align 4, !tbaa !51
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 6
  %37 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src14, align 8, !tbaa !48
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %37, i32 0, i32 1
  %38 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !52
  %bytes_in_buffer15 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  store i32 %38, i32* %bytes_in_buffer15, align 4, !tbaa !53
  %39 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %39, i32 0, i32 1
  %get_buffer16 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 0
  %40 = load i32, i32* %get_buffer16, align 4, !tbaa !41
  store i32 %40, i32* %get_buffer, align 4, !tbaa !54
  %41 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate17 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %41, i32 0, i32 1
  %bits_left18 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate17, i32 0, i32 1
  %42 = load i32, i32* %bits_left18, align 4, !tbaa !40
  store i32 %42, i32* %bits_left, align 4, !tbaa !20
  %43 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %43, i32 0, i32 2
  %EOBRUN19 = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved, i32 0, i32 0
  %44 = load i32, i32* %EOBRUN19, align 4, !tbaa !43
  store i32 %44, i32* %EOBRUN, align 4, !tbaa !20
  %45 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %45, i32 0
  %46 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  store [64 x i16]* %46, [64 x i16]** %block, align 4, !tbaa !2
  %47 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %ac_derived_tbl = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %47, i32 0, i32 5
  %48 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %ac_derived_tbl, align 4, !tbaa !39
  store %struct.d_derived_tbl* %48, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  store i32 0, i32* %num_newnz, align 4, !tbaa !20
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 72
  %50 = load i32, i32* %Ss, align 8, !tbaa !23
  store i32 %50, i32* %k, align 4, !tbaa !20
  %51 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %cmp20 = icmp eq i32 %51, 0
  br i1 %cmp20, label %if.then21, label %if.end153

if.then21:                                        ; preds = %if.then11
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then21
  %52 = load i32, i32* %k, align 4, !tbaa !20
  %53 = load i32, i32* %Se, align 4, !tbaa !20
  %cmp22 = icmp sle i32 %52, %53
  br i1 %cmp22, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %54 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #3
  %55 = bitcast i32* %look to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #3
  %56 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp23 = icmp slt i32 %56, 8
  br i1 %cmp23, label %if.then24, label %if.end34

if.then24:                                        ; preds = %for.body
  %57 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %58 = load i32, i32* %bits_left, align 4, !tbaa !20
  %call25 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %57, i32 %58, i32 0)
  %tobool26 = icmp ne i32 %call25, 0
  br i1 %tobool26, label %if.end28, label %if.then27

if.then27:                                        ; preds = %if.then24
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end28:                                         ; preds = %if.then24
  %get_buffer29 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %59 = load i32, i32* %get_buffer29, align 4, !tbaa !57
  store i32 %59, i32* %get_buffer, align 4, !tbaa !54
  %bits_left30 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %60 = load i32, i32* %bits_left30, align 4, !tbaa !58
  store i32 %60, i32* %bits_left, align 4, !tbaa !20
  %61 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp31 = icmp slt i32 %61, 8
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.end28
  store i32 1, i32* %nb, align 4, !tbaa !20
  br label %label3

if.end33:                                         ; preds = %if.end28
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %for.body
  %62 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %63 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub = sub nsw i32 %63, 8
  %shr = lshr i32 %62, %sub
  %and = and i32 %shr, 255
  store i32 %and, i32* %look, align 4, !tbaa !20
  %64 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %lookup = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %64, i32 0, i32 3
  %65 = load i32, i32* %look, align 4, !tbaa !20
  %arrayidx35 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup, i32 0, i32 %65
  %66 = load i32, i32* %arrayidx35, align 4, !tbaa !20
  %shr36 = ashr i32 %66, 8
  store i32 %shr36, i32* %nb, align 4, !tbaa !20
  %cmp37 = icmp sle i32 %shr36, 8
  br i1 %cmp37, label %if.then38, label %if.else

if.then38:                                        ; preds = %if.end34
  %67 = load i32, i32* %nb, align 4, !tbaa !20
  %68 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub39 = sub nsw i32 %68, %67
  store i32 %sub39, i32* %bits_left, align 4, !tbaa !20
  %69 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %lookup40 = getelementptr inbounds %struct.d_derived_tbl, %struct.d_derived_tbl* %69, i32 0, i32 3
  %70 = load i32, i32* %look, align 4, !tbaa !20
  %arrayidx41 = getelementptr inbounds [256 x i32], [256 x i32]* %lookup40, i32 0, i32 %70
  %71 = load i32, i32* %arrayidx41, align 4, !tbaa !20
  %and42 = and i32 %71, 255
  store i32 %and42, i32* %s, align 4, !tbaa !20
  br label %if.end49

if.else:                                          ; preds = %if.end34
  br label %label3

label3:                                           ; preds = %if.else, %if.then32
  %72 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %73 = load i32, i32* %bits_left, align 4, !tbaa !20
  %74 = load %struct.d_derived_tbl*, %struct.d_derived_tbl** %tbl, align 4, !tbaa !2
  %75 = load i32, i32* %nb, align 4, !tbaa !20
  %call43 = call i32 @jpeg_huff_decode(%struct.bitread_working_state* %br_state, i32 %72, i32 %73, %struct.d_derived_tbl* %74, i32 %75)
  store i32 %call43, i32* %s, align 4, !tbaa !20
  %cmp44 = icmp slt i32 %call43, 0
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %label3
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end46:                                         ; preds = %label3
  %get_buffer47 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %76 = load i32, i32* %get_buffer47, align 4, !tbaa !57
  store i32 %76, i32* %get_buffer, align 4, !tbaa !54
  %bits_left48 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %77 = load i32, i32* %bits_left48, align 4, !tbaa !58
  store i32 %77, i32* %bits_left, align 4, !tbaa !20
  br label %if.end49

if.end49:                                         ; preds = %if.end46, %if.then38
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.then45, %if.then27, %if.end49
  %78 = bitcast i32* %look to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  %79 = bitcast i32* %nb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup228 [
    i32 0, label %cleanup.cont
    i32 5, label %undoit
  ]

cleanup.cont:                                     ; preds = %cleanup
  %80 = load i32, i32* %s, align 4, !tbaa !20
  %shr51 = ashr i32 %80, 4
  store i32 %shr51, i32* %r, align 4, !tbaa !20
  %81 = load i32, i32* %s, align 4, !tbaa !20
  %and52 = and i32 %81, 15
  store i32 %and52, i32* %s, align 4, !tbaa !20
  %82 = load i32, i32* %s, align 4, !tbaa !20
  %tobool53 = icmp ne i32 %82, 0
  br i1 %tobool53, label %if.then54, label %if.else75

if.then54:                                        ; preds = %cleanup.cont
  %83 = load i32, i32* %s, align 4, !tbaa !20
  %cmp55 = icmp ne i32 %83, 1
  br i1 %cmp55, label %if.then56, label %if.end58

if.then56:                                        ; preds = %if.then54
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %84, i32 0, i32 0
  %85 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !28
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %85, i32 0, i32 5
  store i32 118, i32* %msg_code, align 4, !tbaa !29
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err57 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 0
  %87 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err57, align 8, !tbaa !28
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %87, i32 0, i32 1
  %88 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !35
  %89 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %90 = bitcast %struct.jpeg_decompress_struct* %89 to %struct.jpeg_common_struct*
  call void %88(%struct.jpeg_common_struct* %90, i32 -1)
  br label %if.end58

if.end58:                                         ; preds = %if.then56, %if.then54
  %91 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp59 = icmp slt i32 %91, 1
  br i1 %cmp59, label %if.then60, label %if.end67

if.then60:                                        ; preds = %if.end58
  %92 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %93 = load i32, i32* %bits_left, align 4, !tbaa !20
  %call61 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %92, i32 %93, i32 1)
  %tobool62 = icmp ne i32 %call61, 0
  br i1 %tobool62, label %if.end64, label %if.then63

if.then63:                                        ; preds = %if.then60
  br label %undoit

if.end64:                                         ; preds = %if.then60
  %get_buffer65 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %94 = load i32, i32* %get_buffer65, align 4, !tbaa !57
  store i32 %94, i32* %get_buffer, align 4, !tbaa !54
  %bits_left66 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %95 = load i32, i32* %bits_left66, align 4, !tbaa !58
  store i32 %95, i32* %bits_left, align 4, !tbaa !20
  br label %if.end67

if.end67:                                         ; preds = %if.end64, %if.end58
  %96 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %97 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub68 = sub nsw i32 %97, 1
  store i32 %sub68, i32* %bits_left, align 4, !tbaa !20
  %shr69 = lshr i32 %96, %sub68
  %and70 = and i32 %shr69, 1
  %tobool71 = icmp ne i32 %and70, 0
  br i1 %tobool71, label %if.then72, label %if.else73

if.then72:                                        ; preds = %if.end67
  %98 = load i32, i32* %p1, align 4, !tbaa !20
  store i32 %98, i32* %s, align 4, !tbaa !20
  br label %if.end74

if.else73:                                        ; preds = %if.end67
  %99 = load i32, i32* %m1, align 4, !tbaa !20
  store i32 %99, i32* %s, align 4, !tbaa !20
  br label %if.end74

if.end74:                                         ; preds = %if.else73, %if.then72
  br label %if.end97

if.else75:                                        ; preds = %cleanup.cont
  %100 = load i32, i32* %r, align 4, !tbaa !20
  %cmp76 = icmp ne i32 %100, 15
  br i1 %cmp76, label %if.then77, label %if.end96

if.then77:                                        ; preds = %if.else75
  %101 = load i32, i32* %r, align 4, !tbaa !20
  %shl78 = shl i32 1, %101
  store i32 %shl78, i32* %EOBRUN, align 4, !tbaa !20
  %102 = load i32, i32* %r, align 4, !tbaa !20
  %tobool79 = icmp ne i32 %102, 0
  br i1 %tobool79, label %if.then80, label %if.end95

if.then80:                                        ; preds = %if.then77
  %103 = load i32, i32* %bits_left, align 4, !tbaa !20
  %104 = load i32, i32* %r, align 4, !tbaa !20
  %cmp81 = icmp slt i32 %103, %104
  br i1 %cmp81, label %if.then82, label %if.end89

if.then82:                                        ; preds = %if.then80
  %105 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %106 = load i32, i32* %bits_left, align 4, !tbaa !20
  %107 = load i32, i32* %r, align 4, !tbaa !20
  %call83 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %105, i32 %106, i32 %107)
  %tobool84 = icmp ne i32 %call83, 0
  br i1 %tobool84, label %if.end86, label %if.then85

if.then85:                                        ; preds = %if.then82
  br label %undoit

if.end86:                                         ; preds = %if.then82
  %get_buffer87 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %108 = load i32, i32* %get_buffer87, align 4, !tbaa !57
  store i32 %108, i32* %get_buffer, align 4, !tbaa !54
  %bits_left88 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %109 = load i32, i32* %bits_left88, align 4, !tbaa !58
  store i32 %109, i32* %bits_left, align 4, !tbaa !20
  br label %if.end89

if.end89:                                         ; preds = %if.end86, %if.then80
  %110 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %111 = load i32, i32* %r, align 4, !tbaa !20
  %112 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub90 = sub nsw i32 %112, %111
  store i32 %sub90, i32* %bits_left, align 4, !tbaa !20
  %shr91 = lshr i32 %110, %sub90
  %113 = load i32, i32* %r, align 4, !tbaa !20
  %shl92 = shl i32 1, %113
  %sub93 = sub nsw i32 %shl92, 1
  %and94 = and i32 %shr91, %sub93
  store i32 %and94, i32* %r, align 4, !tbaa !20
  %114 = load i32, i32* %r, align 4, !tbaa !20
  %115 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %add = add i32 %115, %114
  store i32 %add, i32* %EOBRUN, align 4, !tbaa !20
  br label %if.end95

if.end95:                                         ; preds = %if.end89, %if.then77
  br label %for.end

if.end96:                                         ; preds = %if.else75
  br label %if.end97

if.end97:                                         ; preds = %if.end96, %if.end74
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.end97
  %116 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [64 x i16], [64 x i16]* %116, i32 0, i32 0
  %117 = load i32, i32* %k, align 4, !tbaa !20
  %arrayidx98 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %117
  %118 = load i32, i32* %arrayidx98, align 4, !tbaa !20
  %add.ptr = getelementptr inbounds i16, i16* %arraydecay, i32 %118
  store i16* %add.ptr, i16** %thiscoef, align 4, !tbaa !2
  %119 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %120 = load i16, i16* %119, align 2, !tbaa !59
  %conv = sext i16 %120 to i32
  %cmp99 = icmp ne i32 %conv, 0
  br i1 %cmp99, label %if.then101, label %if.else136

if.then101:                                       ; preds = %do.body
  %121 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp102 = icmp slt i32 %121, 1
  br i1 %cmp102, label %if.then104, label %if.end111

if.then104:                                       ; preds = %if.then101
  %122 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %123 = load i32, i32* %bits_left, align 4, !tbaa !20
  %call105 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %122, i32 %123, i32 1)
  %tobool106 = icmp ne i32 %call105, 0
  br i1 %tobool106, label %if.end108, label %if.then107

if.then107:                                       ; preds = %if.then104
  br label %undoit

if.end108:                                        ; preds = %if.then104
  %get_buffer109 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %124 = load i32, i32* %get_buffer109, align 4, !tbaa !57
  store i32 %124, i32* %get_buffer, align 4, !tbaa !54
  %bits_left110 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %125 = load i32, i32* %bits_left110, align 4, !tbaa !58
  store i32 %125, i32* %bits_left, align 4, !tbaa !20
  br label %if.end111

if.end111:                                        ; preds = %if.end108, %if.then101
  %126 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %127 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub112 = sub nsw i32 %127, 1
  store i32 %sub112, i32* %bits_left, align 4, !tbaa !20
  %shr113 = lshr i32 %126, %sub112
  %and114 = and i32 %shr113, 1
  %tobool115 = icmp ne i32 %and114, 0
  br i1 %tobool115, label %if.then116, label %if.end135

if.then116:                                       ; preds = %if.end111
  %128 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %129 = load i16, i16* %128, align 2, !tbaa !59
  %conv117 = sext i16 %129 to i32
  %130 = load i32, i32* %p1, align 4, !tbaa !20
  %and118 = and i32 %conv117, %130
  %cmp119 = icmp eq i32 %and118, 0
  br i1 %cmp119, label %if.then121, label %if.end134

if.then121:                                       ; preds = %if.then116
  %131 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %132 = load i16, i16* %131, align 2, !tbaa !59
  %conv122 = sext i16 %132 to i32
  %cmp123 = icmp sge i32 %conv122, 0
  br i1 %cmp123, label %if.then125, label %if.else129

if.then125:                                       ; preds = %if.then121
  %133 = load i32, i32* %p1, align 4, !tbaa !20
  %134 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %135 = load i16, i16* %134, align 2, !tbaa !59
  %conv126 = sext i16 %135 to i32
  %add127 = add nsw i32 %conv126, %133
  %conv128 = trunc i32 %add127 to i16
  store i16 %conv128, i16* %134, align 2, !tbaa !59
  br label %if.end133

if.else129:                                       ; preds = %if.then121
  %136 = load i32, i32* %m1, align 4, !tbaa !20
  %137 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %138 = load i16, i16* %137, align 2, !tbaa !59
  %conv130 = sext i16 %138 to i32
  %add131 = add nsw i32 %conv130, %136
  %conv132 = trunc i32 %add131 to i16
  store i16 %conv132, i16* %137, align 2, !tbaa !59
  br label %if.end133

if.end133:                                        ; preds = %if.else129, %if.then125
  br label %if.end134

if.end134:                                        ; preds = %if.end133, %if.then116
  br label %if.end135

if.end135:                                        ; preds = %if.end134, %if.end111
  br label %if.end141

if.else136:                                       ; preds = %do.body
  %139 = load i32, i32* %r, align 4, !tbaa !20
  %dec = add nsw i32 %139, -1
  store i32 %dec, i32* %r, align 4, !tbaa !20
  %cmp137 = icmp slt i32 %dec, 0
  br i1 %cmp137, label %if.then139, label %if.end140

if.then139:                                       ; preds = %if.else136
  br label %do.end

if.end140:                                        ; preds = %if.else136
  br label %if.end141

if.end141:                                        ; preds = %if.end140, %if.end135
  %140 = load i32, i32* %k, align 4, !tbaa !20
  %inc = add nsw i32 %140, 1
  store i32 %inc, i32* %k, align 4, !tbaa !20
  br label %do.cond

do.cond:                                          ; preds = %if.end141
  %141 = load i32, i32* %k, align 4, !tbaa !20
  %142 = load i32, i32* %Se, align 4, !tbaa !20
  %cmp142 = icmp sle i32 %141, %142
  br i1 %cmp142, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %if.then139
  %143 = load i32, i32* %s, align 4, !tbaa !20
  %tobool144 = icmp ne i32 %143, 0
  br i1 %tobool144, label %if.then145, label %if.end151

if.then145:                                       ; preds = %do.end
  %144 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #3
  %145 = load i32, i32* %k, align 4, !tbaa !20
  %arrayidx146 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %145
  %146 = load i32, i32* %arrayidx146, align 4, !tbaa !20
  store i32 %146, i32* %pos, align 4, !tbaa !20
  %147 = load i32, i32* %s, align 4, !tbaa !20
  %conv147 = trunc i32 %147 to i16
  %148 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %149 = load i32, i32* %pos, align 4, !tbaa !20
  %arrayidx148 = getelementptr inbounds [64 x i16], [64 x i16]* %148, i32 0, i32 %149
  store i16 %conv147, i16* %arrayidx148, align 2, !tbaa !59
  %150 = load i32, i32* %pos, align 4, !tbaa !20
  %151 = load i32, i32* %num_newnz, align 4, !tbaa !20
  %inc149 = add nsw i32 %151, 1
  store i32 %inc149, i32* %num_newnz, align 4, !tbaa !20
  %arrayidx150 = getelementptr inbounds [64 x i32], [64 x i32]* %newnz_pos, i32 0, i32 %151
  store i32 %150, i32* %arrayidx150, align 4, !tbaa !20
  %152 = bitcast i32* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #3
  br label %if.end151

if.end151:                                        ; preds = %if.then145, %do.end
  br label %for.inc

for.inc:                                          ; preds = %if.end151
  %153 = load i32, i32* %k, align 4, !tbaa !20
  %inc152 = add nsw i32 %153, 1
  store i32 %inc152, i32* %k, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %if.end95, %for.cond
  br label %if.end153

if.end153:                                        ; preds = %for.end, %if.then11
  %154 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %cmp154 = icmp ugt i32 %154, 0
  br i1 %cmp154, label %if.then156, label %if.end207

if.then156:                                       ; preds = %if.end153
  br label %for.cond157

for.cond157:                                      ; preds = %for.inc203, %if.then156
  %155 = load i32, i32* %k, align 4, !tbaa !20
  %156 = load i32, i32* %Se, align 4, !tbaa !20
  %cmp158 = icmp sle i32 %155, %156
  br i1 %cmp158, label %for.body160, label %for.end205

for.body160:                                      ; preds = %for.cond157
  %157 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %arraydecay161 = getelementptr inbounds [64 x i16], [64 x i16]* %157, i32 0, i32 0
  %158 = load i32, i32* %k, align 4, !tbaa !20
  %arrayidx162 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %158
  %159 = load i32, i32* %arrayidx162, align 4, !tbaa !20
  %add.ptr163 = getelementptr inbounds i16, i16* %arraydecay161, i32 %159
  store i16* %add.ptr163, i16** %thiscoef, align 4, !tbaa !2
  %160 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %161 = load i16, i16* %160, align 2, !tbaa !59
  %conv164 = sext i16 %161 to i32
  %cmp165 = icmp ne i32 %conv164, 0
  br i1 %cmp165, label %if.then167, label %if.end202

if.then167:                                       ; preds = %for.body160
  %162 = load i32, i32* %bits_left, align 4, !tbaa !20
  %cmp168 = icmp slt i32 %162, 1
  br i1 %cmp168, label %if.then170, label %if.end177

if.then170:                                       ; preds = %if.then167
  %163 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %164 = load i32, i32* %bits_left, align 4, !tbaa !20
  %call171 = call i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state* %br_state, i32 %163, i32 %164, i32 1)
  %tobool172 = icmp ne i32 %call171, 0
  br i1 %tobool172, label %if.end174, label %if.then173

if.then173:                                       ; preds = %if.then170
  br label %undoit

if.end174:                                        ; preds = %if.then170
  %get_buffer175 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 2
  %165 = load i32, i32* %get_buffer175, align 4, !tbaa !57
  store i32 %165, i32* %get_buffer, align 4, !tbaa !54
  %bits_left176 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 3
  %166 = load i32, i32* %bits_left176, align 4, !tbaa !58
  store i32 %166, i32* %bits_left, align 4, !tbaa !20
  br label %if.end177

if.end177:                                        ; preds = %if.end174, %if.then167
  %167 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %168 = load i32, i32* %bits_left, align 4, !tbaa !20
  %sub178 = sub nsw i32 %168, 1
  store i32 %sub178, i32* %bits_left, align 4, !tbaa !20
  %shr179 = lshr i32 %167, %sub178
  %and180 = and i32 %shr179, 1
  %tobool181 = icmp ne i32 %and180, 0
  br i1 %tobool181, label %if.then182, label %if.end201

if.then182:                                       ; preds = %if.end177
  %169 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %170 = load i16, i16* %169, align 2, !tbaa !59
  %conv183 = sext i16 %170 to i32
  %171 = load i32, i32* %p1, align 4, !tbaa !20
  %and184 = and i32 %conv183, %171
  %cmp185 = icmp eq i32 %and184, 0
  br i1 %cmp185, label %if.then187, label %if.end200

if.then187:                                       ; preds = %if.then182
  %172 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %173 = load i16, i16* %172, align 2, !tbaa !59
  %conv188 = sext i16 %173 to i32
  %cmp189 = icmp sge i32 %conv188, 0
  br i1 %cmp189, label %if.then191, label %if.else195

if.then191:                                       ; preds = %if.then187
  %174 = load i32, i32* %p1, align 4, !tbaa !20
  %175 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %176 = load i16, i16* %175, align 2, !tbaa !59
  %conv192 = sext i16 %176 to i32
  %add193 = add nsw i32 %conv192, %174
  %conv194 = trunc i32 %add193 to i16
  store i16 %conv194, i16* %175, align 2, !tbaa !59
  br label %if.end199

if.else195:                                       ; preds = %if.then187
  %177 = load i32, i32* %m1, align 4, !tbaa !20
  %178 = load i16*, i16** %thiscoef, align 4, !tbaa !2
  %179 = load i16, i16* %178, align 2, !tbaa !59
  %conv196 = sext i16 %179 to i32
  %add197 = add nsw i32 %conv196, %177
  %conv198 = trunc i32 %add197 to i16
  store i16 %conv198, i16* %178, align 2, !tbaa !59
  br label %if.end199

if.end199:                                        ; preds = %if.else195, %if.then191
  br label %if.end200

if.end200:                                        ; preds = %if.end199, %if.then182
  br label %if.end201

if.end201:                                        ; preds = %if.end200, %if.end177
  br label %if.end202

if.end202:                                        ; preds = %if.end201, %for.body160
  br label %for.inc203

for.inc203:                                       ; preds = %if.end202
  %180 = load i32, i32* %k, align 4, !tbaa !20
  %inc204 = add nsw i32 %180, 1
  store i32 %inc204, i32* %k, align 4, !tbaa !20
  br label %for.cond157

for.end205:                                       ; preds = %for.cond157
  %181 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %dec206 = add i32 %181, -1
  store i32 %dec206, i32* %EOBRUN, align 4, !tbaa !20
  br label %if.end207

if.end207:                                        ; preds = %for.end205, %if.end153
  %next_input_byte208 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 0
  %182 = load i8*, i8** %next_input_byte208, align 4, !tbaa !51
  %183 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src209 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %183, i32 0, i32 6
  %184 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src209, align 8, !tbaa !48
  %next_input_byte210 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %184, i32 0, i32 0
  store i8* %182, i8** %next_input_byte210, align 4, !tbaa !49
  %bytes_in_buffer211 = getelementptr inbounds %struct.bitread_working_state, %struct.bitread_working_state* %br_state, i32 0, i32 1
  %185 = load i32, i32* %bytes_in_buffer211, align 4, !tbaa !53
  %186 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src212 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %186, i32 0, i32 6
  %187 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src212, align 8, !tbaa !48
  %bytes_in_buffer213 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %187, i32 0, i32 1
  store i32 %185, i32* %bytes_in_buffer213, align 4, !tbaa !52
  %188 = load i32, i32* %get_buffer, align 4, !tbaa !54
  %189 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate214 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %189, i32 0, i32 1
  %get_buffer215 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate214, i32 0, i32 0
  store i32 %188, i32* %get_buffer215, align 4, !tbaa !41
  %190 = load i32, i32* %bits_left, align 4, !tbaa !20
  %191 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate216 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %191, i32 0, i32 1
  %bits_left217 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate216, i32 0, i32 1
  store i32 %190, i32* %bits_left217, align 4, !tbaa !40
  %192 = load i32, i32* %EOBRUN, align 4, !tbaa !20
  %193 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved218 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %193, i32 0, i32 2
  %EOBRUN219 = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved218, i32 0, i32 0
  store i32 %192, i32* %EOBRUN219, align 4, !tbaa !43
  br label %if.end220

if.end220:                                        ; preds = %if.end207, %if.end9
  %194 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go221 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %194, i32 0, i32 3
  %195 = load i32, i32* %restarts_to_go221, align 4, !tbaa !45
  %dec222 = add i32 %195, -1
  store i32 %dec222, i32* %restarts_to_go221, align 4, !tbaa !45
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup228

undoit:                                           ; preds = %cleanup, %if.then173, %if.then107, %if.then85, %if.then63
  br label %while.cond

while.cond:                                       ; preds = %while.body, %undoit
  %196 = load i32, i32* %num_newnz, align 4, !tbaa !20
  %cmp223 = icmp sgt i32 %196, 0
  br i1 %cmp223, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %197 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %198 = load i32, i32* %num_newnz, align 4, !tbaa !20
  %dec225 = add nsw i32 %198, -1
  store i32 %dec225, i32* %num_newnz, align 4, !tbaa !20
  %arrayidx226 = getelementptr inbounds [64 x i32], [64 x i32]* %newnz_pos, i32 0, i32 %dec225
  %199 = load i32, i32* %arrayidx226, align 4, !tbaa !20
  %arrayidx227 = getelementptr inbounds [64 x i16], [64 x i16]* %197, i32 0, i32 %199
  store i16 0, i16* %arrayidx227, align 2, !tbaa !59
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup228

cleanup228:                                       ; preds = %while.end, %if.end220, %cleanup, %if.then7
  %200 = bitcast [64 x i32]* %newnz_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %200) #3
  %201 = bitcast i32* %num_newnz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #3
  %202 = bitcast %struct.d_derived_tbl** %tbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #3
  %203 = bitcast %struct.bitread_working_state* %br_state to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %203) #3
  %204 = bitcast i32* %bits_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #3
  %205 = bitcast i32* %get_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #3
  %206 = bitcast i16** %thiscoef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #3
  %207 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #3
  %208 = bitcast i32* %EOBRUN to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #3
  %209 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #3
  %210 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #3
  %211 = bitcast i32* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #3
  %212 = bitcast i32* %m1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #3
  %213 = bitcast i32* %p1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #3
  %214 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #3
  %215 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #3
  %216 = load i32, i32* %retval, align 4
  ret i32 %216
}

declare void @jpeg_make_d_derived_tbl(%struct.jpeg_decompress_struct*, i32, i32, %struct.d_derived_tbl**) #2

; Function Attrs: nounwind
define internal i32 @process_restart(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %entropy = alloca %struct.phuff_entropy_decoder*, align 4
  %ci = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 83
  %2 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_decoder* %2 to %struct.phuff_entropy_decoder*
  store %struct.phuff_entropy_decoder* %3, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %5, i32 0, i32 1
  %bits_left = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate, i32 0, i32 1
  %6 = load i32, i32* %bits_left, align 4, !tbaa !40
  %div = sdiv i32 %6, 8
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 82
  %8 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker, align 8, !tbaa !60
  %discarded_bytes = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %8, i32 0, i32 6
  %9 = load i32, i32* %discarded_bytes, align 4, !tbaa !61
  %add = add i32 %9, %div
  store i32 %add, i32* %discarded_bytes, align 4, !tbaa !61
  %10 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %bitstate2 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %10, i32 0, i32 1
  %bits_left3 = getelementptr inbounds %struct.bitread_perm_state, %struct.bitread_perm_state* %bitstate2, i32 0, i32 1
  store i32 0, i32* %bits_left3, align 4, !tbaa !40
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 82
  %12 = load %struct.jpeg_marker_reader*, %struct.jpeg_marker_reader** %marker4, align 8, !tbaa !60
  %read_restart_marker = getelementptr inbounds %struct.jpeg_marker_reader, %struct.jpeg_marker_reader* %12, i32 0, i32 2
  %13 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %read_restart_marker, align 4, !tbaa !63
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %13(%struct.jpeg_decompress_struct* %14)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %ci, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %15 = load i32, i32* %ci, align 4, !tbaa !20
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 66
  %17 = load i32, i32* %comps_in_scan, align 8, !tbaa !25
  %cmp = icmp slt i32 %15, %17
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %18, i32 0, i32 2
  %last_dc_val = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved, i32 0, i32 1
  %19 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %19
  store i32 0, i32* %arrayidx, align 4, !tbaa !20
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %ci, align 4, !tbaa !20
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %saved5 = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %21, i32 0, i32 2
  %EOBRUN = getelementptr inbounds %struct.savable_state, %struct.savable_state* %saved5, i32 0, i32 0
  store i32 0, i32* %EOBRUN, align 4, !tbaa !43
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 50
  %23 = load i32, i32* %restart_interval, align 4, !tbaa !44
  %24 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %24, i32 0, i32 3
  store i32 %23, i32* %restarts_to_go, align 4, !tbaa !45
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %unread_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 76
  %26 = load i32, i32* %unread_marker, align 8, !tbaa !64
  %cmp6 = icmp eq i32 %26, 0
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %for.end
  %27 = load %struct.phuff_entropy_decoder*, %struct.phuff_entropy_decoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.phuff_entropy_decoder, %struct.phuff_entropy_decoder* %27, i32 0, i32 0
  %insufficient_data = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %pub, i32 0, i32 2
  store i32 0, i32* %insufficient_data, align 4, !tbaa !42
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %for.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end8, %if.then
  %28 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  %29 = bitcast %struct.phuff_entropy_decoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  %30 = load i32, i32* %retval, align 4
  ret i32 %30
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare i32 @jpeg_fill_bit_buffer(%struct.bitread_working_state*, i32, i32, i32) #2

declare i32 @jpeg_huff_decode(%struct.bitread_working_state*, i32, i32, %struct.d_derived_tbl*, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 444}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !18, i64 12, !19, i64 20, !8, i64 40, !4, i64 44, !3, i64 60}
!17 = !{!"jpeg_entropy_decoder", !3, i64 0, !3, i64 4, !8, i64 8}
!18 = !{!"", !13, i64 0, !8, i64 4}
!19 = !{!"", !8, i64 0, !4, i64 4}
!20 = !{!8, !8, i64 0}
!21 = !{!7, !8, i64 36}
!22 = !{!7, !3, i64 160}
!23 = !{!7, !8, i64 400}
!24 = !{!7, !8, i64 404}
!25 = !{!7, !8, i64 328}
!26 = !{!7, !8, i64 408}
!27 = !{!7, !8, i64 412}
!28 = !{!7, !3, i64 0}
!29 = !{!30, !8, i64 20}
!30 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!31 = !{!4, !4, i64 0}
!32 = !{!30, !3, i64 0}
!33 = !{!34, !8, i64 4}
!34 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!35 = !{!30, !3, i64 4}
!36 = !{!16, !3, i64 4}
!37 = !{!34, !8, i64 20}
!38 = !{!34, !8, i64 24}
!39 = !{!16, !3, i64 60}
!40 = !{!16, !8, i64 16}
!41 = !{!16, !13, i64 12}
!42 = !{!16, !8, i64 8}
!43 = !{!16, !8, i64 20}
!44 = !{!7, !8, i64 276}
!45 = !{!16, !8, i64 40}
!46 = !{!47, !3, i64 16}
!47 = !{!"", !3, i64 0, !13, i64 4, !13, i64 8, !8, i64 12, !3, i64 16}
!48 = !{!7, !3, i64 24}
!49 = !{!50, !3, i64 0}
!50 = !{!"jpeg_source_mgr", !3, i64 0, !13, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!51 = !{!47, !3, i64 0}
!52 = !{!50, !13, i64 4}
!53 = !{!47, !13, i64 4}
!54 = !{!13, !13, i64 0}
!55 = !{i64 0, i64 4, !20, i64 4, i64 16, !31}
!56 = !{!7, !8, i64 356}
!57 = !{!47, !13, i64 8}
!58 = !{!47, !8, i64 12}
!59 = !{!10, !10, i64 0}
!60 = !{!7, !3, i64 440}
!61 = !{!62, !8, i64 24}
!62 = !{!"jpeg_marker_reader", !3, i64 0, !3, i64 4, !3, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24}
!63 = !{!62, !3, i64 8}
!64 = !{!7, !8, i64 416}
