; ModuleID = 'rdswitch.c'
source_filename = "rdswitch.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type opaque
%struct.jpeg_c_main_controller = type opaque
%struct.jpeg_c_prep_controller = type opaque
%struct.jpeg_c_coef_controller = type opaque
%struct.jpeg_marker_writer = type opaque
%struct.jpeg_color_converter = type opaque
%struct.jpeg_downsampler = type opaque
%struct.jpeg_forward_dct = type opaque
%struct.jpeg_entropy_encoder = type opaque
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }

@.str = private unnamed_addr constant [2 x i8] c"r\00", align 1
@stderr = external constant %struct._IO_FILE*, align 4
@.str.1 = private unnamed_addr constant [26 x i8] c"Can't open table file %s\0A\00", align 1
@.str.2 = private unnamed_addr constant [28 x i8] c"Too many tables in file %s\0A\00", align 1
@.str.3 = private unnamed_addr constant [31 x i8] c"Invalid table data in file %s\0A\00", align 1
@q_scale_factor = internal global [4 x i32] [i32 100, i32 100, i32 100, i32 100], align 16
@.str.4 = private unnamed_addr constant [29 x i8] c"Non-numeric data in file %s\0A\00", align 1
@.str.5 = private unnamed_addr constant [36 x i8] c"Can't open scan definition file %s\0A\00", align 1
@.str.6 = private unnamed_addr constant [35 x i8] c"Too many scans defined in file %s\0A\00", align 1
@.str.7 = private unnamed_addr constant [44 x i8] c"Too many components in one scan in file %s\0A\00", align 1
@.str.8 = private unnamed_addr constant [38 x i8] c"Invalid scan entry format in file %s\0A\00", align 1
@.str.9 = private unnamed_addr constant [5 x i8] c"%f%c\00", align 1
@.str.10 = private unnamed_addr constant [4 x i8] c"1x1\00", align 1
@.str.11 = private unnamed_addr constant [4 x i8] c"2x1\00", align 1
@.str.12 = private unnamed_addr constant [5 x i8] c"%d%c\00", align 1
@.str.13 = private unnamed_addr constant [45 x i8] c"JPEG quantization tables are numbered 0..%d\0A\00", align 1
@.str.14 = private unnamed_addr constant [9 x i8] c"%d%c%d%c\00", align 1
@.str.15 = private unnamed_addr constant [36 x i8] c"JPEG sampling factors must be 1..4\0A\00", align 1
@std_luminance_quant_tbl = internal constant [9 x [64 x i32]] [[64 x i32] [i32 16, i32 11, i32 10, i32 16, i32 24, i32 40, i32 51, i32 61, i32 12, i32 12, i32 14, i32 19, i32 26, i32 58, i32 60, i32 55, i32 14, i32 13, i32 16, i32 24, i32 40, i32 57, i32 69, i32 56, i32 14, i32 17, i32 22, i32 29, i32 51, i32 87, i32 80, i32 62, i32 18, i32 22, i32 37, i32 56, i32 68, i32 109, i32 103, i32 77, i32 24, i32 35, i32 55, i32 64, i32 81, i32 104, i32 113, i32 92, i32 49, i32 64, i32 78, i32 87, i32 103, i32 121, i32 120, i32 101, i32 72, i32 92, i32 95, i32 98, i32 112, i32 100, i32 103, i32 99], [64 x i32] [i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16], [64 x i32] [i32 12, i32 17, i32 20, i32 21, i32 30, i32 34, i32 56, i32 63, i32 18, i32 20, i32 20, i32 26, i32 28, i32 51, i32 61, i32 55, i32 19, i32 20, i32 21, i32 26, i32 33, i32 58, i32 69, i32 55, i32 26, i32 26, i32 26, i32 30, i32 46, i32 87, i32 86, i32 66, i32 31, i32 33, i32 36, i32 40, i32 46, i32 96, i32 100, i32 73, i32 40, i32 35, i32 46, i32 62, i32 81, i32 100, i32 111, i32 91, i32 46, i32 66, i32 76, i32 86, i32 102, i32 121, i32 120, i32 101, i32 68, i32 90, i32 90, i32 96, i32 113, i32 102, i32 105, i32 103], [64 x i32] [i32 16, i32 16, i32 16, i32 18, i32 25, i32 37, i32 56, i32 85, i32 16, i32 17, i32 20, i32 27, i32 34, i32 40, i32 53, i32 75, i32 16, i32 20, i32 24, i32 31, i32 43, i32 62, i32 91, i32 135, i32 18, i32 27, i32 31, i32 40, i32 53, i32 74, i32 106, i32 156, i32 25, i32 34, i32 43, i32 53, i32 69, i32 94, i32 131, i32 189, i32 37, i32 40, i32 62, i32 74, i32 94, i32 124, i32 169, i32 238, i32 56, i32 53, i32 91, i32 106, i32 131, i32 169, i32 226, i32 311, i32 85, i32 75, i32 135, i32 156, i32 189, i32 238, i32 311, i32 418], [64 x i32] [i32 9, i32 10, i32 12, i32 14, i32 27, i32 32, i32 51, i32 62, i32 11, i32 12, i32 14, i32 19, i32 27, i32 44, i32 59, i32 73, i32 12, i32 14, i32 18, i32 25, i32 42, i32 59, i32 79, i32 78, i32 17, i32 18, i32 25, i32 42, i32 61, i32 92, i32 87, i32 92, i32 23, i32 28, i32 42, i32 75, i32 79, i32 112, i32 112, i32 99, i32 40, i32 42, i32 59, i32 84, i32 88, i32 124, i32 132, i32 111, i32 42, i32 64, i32 78, i32 95, i32 105, i32 126, i32 125, i32 99, i32 70, i32 75, i32 100, i32 102, i32 116, i32 100, i32 107, i32 98], [64 x i32] [i32 10, i32 12, i32 14, i32 19, i32 26, i32 38, i32 57, i32 86, i32 12, i32 18, i32 21, i32 28, i32 35, i32 41, i32 54, i32 76, i32 14, i32 21, i32 25, i32 32, i32 44, i32 63, i32 92, i32 136, i32 19, i32 28, i32 32, i32 41, i32 54, i32 75, i32 107, i32 157, i32 26, i32 35, i32 44, i32 54, i32 70, i32 95, i32 132, i32 190, i32 38, i32 41, i32 63, i32 75, i32 95, i32 125, i32 170, i32 239, i32 57, i32 54, i32 92, i32 107, i32 132, i32 170, i32 227, i32 312, i32 86, i32 76, i32 136, i32 157, i32 190, i32 239, i32 312, i32 419], [64 x i32] [i32 7, i32 8, i32 10, i32 14, i32 23, i32 44, i32 95, i32 241, i32 8, i32 8, i32 11, i32 15, i32 25, i32 47, i32 102, i32 255, i32 10, i32 11, i32 13, i32 19, i32 31, i32 58, i32 127, i32 255, i32 14, i32 15, i32 19, i32 27, i32 44, i32 83, i32 181, i32 255, i32 23, i32 25, i32 31, i32 44, i32 72, i32 136, i32 255, i32 255, i32 44, i32 47, i32 58, i32 83, i32 136, i32 255, i32 255, i32 255, i32 95, i32 102, i32 127, i32 181, i32 255, i32 255, i32 255, i32 255, i32 241, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255], [64 x i32] [i32 15, i32 11, i32 11, i32 12, i32 15, i32 19, i32 25, i32 32, i32 11, i32 13, i32 10, i32 10, i32 12, i32 15, i32 19, i32 24, i32 11, i32 10, i32 14, i32 14, i32 16, i32 18, i32 22, i32 27, i32 12, i32 10, i32 14, i32 18, i32 21, i32 24, i32 28, i32 33, i32 15, i32 12, i32 16, i32 21, i32 26, i32 31, i32 36, i32 42, i32 19, i32 15, i32 18, i32 24, i32 31, i32 38, i32 45, i32 53, i32 25, i32 19, i32 22, i32 28, i32 36, i32 45, i32 55, i32 65, i32 32, i32 24, i32 27, i32 33, i32 42, i32 53, i32 65, i32 77], [64 x i32] [i32 14, i32 10, i32 11, i32 14, i32 19, i32 25, i32 34, i32 45, i32 10, i32 11, i32 11, i32 12, i32 15, i32 20, i32 26, i32 33, i32 11, i32 11, i32 15, i32 18, i32 21, i32 25, i32 31, i32 38, i32 14, i32 12, i32 18, i32 24, i32 28, i32 33, i32 39, i32 47, i32 19, i32 15, i32 21, i32 28, i32 36, i32 43, i32 51, i32 59, i32 25, i32 20, i32 25, i32 33, i32 43, i32 54, i32 64, i32 74, i32 34, i32 26, i32 31, i32 39, i32 51, i32 64, i32 77, i32 91, i32 45, i32 33, i32 38, i32 47, i32 59, i32 74, i32 91, i32 108]], align 16
@std_chrominance_quant_tbl = internal constant [9 x [64 x i32]] [[64 x i32] [i32 17, i32 18, i32 24, i32 47, i32 99, i32 99, i32 99, i32 99, i32 18, i32 21, i32 26, i32 66, i32 99, i32 99, i32 99, i32 99, i32 24, i32 26, i32 56, i32 99, i32 99, i32 99, i32 99, i32 99, i32 47, i32 66, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99], [64 x i32] [i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16, i32 16], [64 x i32] [i32 8, i32 12, i32 15, i32 15, i32 86, i32 96, i32 96, i32 98, i32 13, i32 13, i32 15, i32 26, i32 90, i32 96, i32 99, i32 98, i32 12, i32 15, i32 18, i32 96, i32 99, i32 99, i32 99, i32 99, i32 17, i32 16, i32 90, i32 96, i32 99, i32 99, i32 99, i32 99, i32 96, i32 96, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99], [64 x i32] [i32 16, i32 16, i32 16, i32 18, i32 25, i32 37, i32 56, i32 85, i32 16, i32 17, i32 20, i32 27, i32 34, i32 40, i32 53, i32 75, i32 16, i32 20, i32 24, i32 31, i32 43, i32 62, i32 91, i32 135, i32 18, i32 27, i32 31, i32 40, i32 53, i32 74, i32 106, i32 156, i32 25, i32 34, i32 43, i32 53, i32 69, i32 94, i32 131, i32 189, i32 37, i32 40, i32 62, i32 74, i32 94, i32 124, i32 169, i32 238, i32 56, i32 53, i32 91, i32 106, i32 131, i32 169, i32 226, i32 311, i32 85, i32 75, i32 135, i32 156, i32 189, i32 238, i32 311, i32 418], [64 x i32] [i32 9, i32 10, i32 17, i32 19, i32 62, i32 89, i32 91, i32 97, i32 12, i32 13, i32 18, i32 29, i32 84, i32 91, i32 88, i32 98, i32 14, i32 19, i32 29, i32 93, i32 95, i32 95, i32 98, i32 97, i32 20, i32 26, i32 84, i32 88, i32 95, i32 95, i32 98, i32 94, i32 26, i32 86, i32 91, i32 93, i32 97, i32 99, i32 98, i32 99, i32 99, i32 100, i32 98, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 99, i32 97, i32 97, i32 99, i32 99, i32 99, i32 99, i32 97, i32 99], [64 x i32] [i32 10, i32 12, i32 14, i32 19, i32 26, i32 38, i32 57, i32 86, i32 12, i32 18, i32 21, i32 28, i32 35, i32 41, i32 54, i32 76, i32 14, i32 21, i32 25, i32 32, i32 44, i32 63, i32 92, i32 136, i32 19, i32 28, i32 32, i32 41, i32 54, i32 75, i32 107, i32 157, i32 26, i32 35, i32 44, i32 54, i32 70, i32 95, i32 132, i32 190, i32 38, i32 41, i32 63, i32 75, i32 95, i32 125, i32 170, i32 239, i32 57, i32 54, i32 92, i32 107, i32 132, i32 170, i32 227, i32 312, i32 86, i32 76, i32 136, i32 157, i32 190, i32 239, i32 312, i32 419], [64 x i32] [i32 7, i32 8, i32 10, i32 14, i32 23, i32 44, i32 95, i32 241, i32 8, i32 8, i32 11, i32 15, i32 25, i32 47, i32 102, i32 255, i32 10, i32 11, i32 13, i32 19, i32 31, i32 58, i32 127, i32 255, i32 14, i32 15, i32 19, i32 27, i32 44, i32 83, i32 181, i32 255, i32 23, i32 25, i32 31, i32 44, i32 72, i32 136, i32 255, i32 255, i32 44, i32 47, i32 58, i32 83, i32 136, i32 255, i32 255, i32 255, i32 95, i32 102, i32 127, i32 181, i32 255, i32 255, i32 255, i32 255, i32 241, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255], [64 x i32] [i32 15, i32 11, i32 11, i32 12, i32 15, i32 19, i32 25, i32 32, i32 11, i32 13, i32 10, i32 10, i32 12, i32 15, i32 19, i32 24, i32 11, i32 10, i32 14, i32 14, i32 16, i32 18, i32 22, i32 27, i32 12, i32 10, i32 14, i32 18, i32 21, i32 24, i32 28, i32 33, i32 15, i32 12, i32 16, i32 21, i32 26, i32 31, i32 36, i32 42, i32 19, i32 15, i32 18, i32 24, i32 31, i32 38, i32 45, i32 53, i32 25, i32 19, i32 22, i32 28, i32 36, i32 45, i32 55, i32 65, i32 32, i32 24, i32 27, i32 33, i32 42, i32 53, i32 65, i32 77], [64 x i32] [i32 14, i32 10, i32 11, i32 14, i32 19, i32 25, i32 34, i32 45, i32 10, i32 11, i32 11, i32 12, i32 15, i32 20, i32 26, i32 33, i32 11, i32 11, i32 15, i32 18, i32 21, i32 25, i32 31, i32 38, i32 14, i32 12, i32 18, i32 24, i32 28, i32 33, i32 39, i32 47, i32 19, i32 15, i32 21, i32 28, i32 36, i32 43, i32 51, i32 59, i32 25, i32 20, i32 25, i32 33, i32 43, i32 54, i32 64, i32 74, i32 34, i32 26, i32 31, i32 39, i32 51, i32 64, i32 77, i32 91, i32 45, i32 33, i32 38, i32 47, i32 59, i32 74, i32 91, i32 108]], align 16

; Function Attrs: nounwind
define hidden i32 @read_quant_tables(%struct.jpeg_compress_struct* %cinfo, i8* %filename, i32 %force_baseline) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %filename.addr = alloca i8*, align 4
  %force_baseline.addr = alloca i32, align 4
  %fp = alloca %struct._IO_FILE*, align 4
  %tblno = alloca i32, align 4
  %i = alloca i32, align 4
  %termchar = alloca i32, align 4
  %val = alloca i32, align 4
  %table = alloca [64 x i32], align 16
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %filename, i8** %filename.addr, align 4, !tbaa !2
  store i32 %force_baseline, i32* %force_baseline.addr, align 4, !tbaa !6
  %0 = bitcast %struct._IO_FILE** %fp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %tblno to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %termchar to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast [64 x i32]* %table to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %5) #4
  %6 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @fopen(i8* %6, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  store %struct._IO_FILE* %call, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %cmp = icmp eq %struct._IO_FILE* %call, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %8 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0), i8* %8)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %tblno, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %for.end, %if.end
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call2 = call i32 @read_text_integer(%struct._IO_FILE* %9, i32* %val, i32* %termchar)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i32, i32* %tblno, align 4, !tbaa !6
  %cmp3 = icmp sge i32 %10, 4
  br i1 %cmp3, label %if.then4, label %if.end7

if.then4:                                         ; preds = %while.body
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %12 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %11, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.2, i32 0, i32 0), i8* %12)
  %13 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call6 = call i32 @fclose(%struct._IO_FILE* %13)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %while.body
  %14 = load i32, i32* %val, align 4, !tbaa !8
  %arrayidx = getelementptr inbounds [64 x i32], [64 x i32]* %table, i32 0, i32 0
  store i32 %14, i32* %arrayidx, align 16, !tbaa !6
  store i32 1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end7
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, 64
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call9 = call i32 @read_text_integer(%struct._IO_FILE* %16, i32* %val, i32* %termchar)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.end14, label %if.then11

if.then11:                                        ; preds = %for.body
  %17 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %18 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %17, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.3, i32 0, i32 0), i8* %18)
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call13 = call i32 @fclose(%struct._IO_FILE* %19)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end14:                                         ; preds = %for.body
  %20 = load i32, i32* %val, align 4, !tbaa !8
  %21 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [64 x i32], [64 x i32]* %table, i32 0, i32 %21
  store i32 %20, i32* %arrayidx15, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = load i32, i32* %tblno, align 4, !tbaa !6
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %table, i32 0, i32 0
  %25 = load i32, i32* %tblno, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [4 x i32], [4 x i32]* @q_scale_factor, i32 0, i32 %25
  %26 = load i32, i32* %arrayidx16, align 4, !tbaa !6
  %27 = load i32, i32* %force_baseline.addr, align 4, !tbaa !6
  call void @jpeg_add_quant_table(%struct.jpeg_compress_struct* %23, i32 %24, i32* %arraydecay, i32 %26, i32 %27)
  %28 = load i32, i32* %tblno, align 4, !tbaa !6
  %inc17 = add nsw i32 %28, 1
  store i32 %inc17, i32* %tblno, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %29 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp18 = icmp ne i32 %29, -1
  br i1 %cmp18, label %if.then19, label %if.end22

if.then19:                                        ; preds = %while.end
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %31 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call20 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %30, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.4, i32 0, i32 0), i8* %31)
  %32 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call21 = call i32 @fclose(%struct._IO_FILE* %32)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %while.end
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call23 = call i32 @fclose(%struct._IO_FILE* %33)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end22, %if.then19, %if.then11, %if.then4, %if.then
  %34 = bitcast [64 x i32]* %table to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %34) #4
  %35 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i32* %termchar to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = bitcast i32* %tblno to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  %39 = bitcast %struct._IO_FILE** %fp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #4
  %40 = load i32, i32* %retval, align 4
  ret i32 %40
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

declare %struct._IO_FILE* @fopen(i8*, i8*) #2

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: nounwind
define internal i32 @read_text_integer(%struct._IO_FILE* %file, i32* %result, i32* %termchar) #0 {
entry:
  %retval = alloca i32, align 4
  %file.addr = alloca %struct._IO_FILE*, align 4
  %result.addr = alloca i32*, align 4
  %termchar.addr = alloca i32*, align 4
  %ch = alloca i32, align 4
  %val = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct._IO_FILE* %file, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  store i32* %result, i32** %result.addr, align 4, !tbaa !2
  store i32* %termchar, i32** %termchar.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call = call i32 @text_getc(%struct._IO_FILE* %2)
  store i32 %call, i32* %ch, align 4, !tbaa !6
  %3 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp = icmp eq i32 %3, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %do.body
  %4 = load i32, i32* %ch, align 4, !tbaa !6
  %5 = load i32*, i32** %termchar.addr, align 4, !tbaa !2
  store i32 %4, i32* %5, align 4, !tbaa !6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %6 = load i32, i32* %ch, align 4, !tbaa !6
  %call1 = call i32 @isspace(i32 %6) #5
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %7 = load i32, i32* %ch, align 4, !tbaa !6
  %call2 = call i32 @isdigit(i32 %7) #5
  %tobool3 = icmp ne i32 %call2, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %do.end
  %8 = load i32, i32* %ch, align 4, !tbaa !6
  %9 = load i32*, i32** %termchar.addr, align 4, !tbaa !2
  store i32 %8, i32* %9, align 4, !tbaa !6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end5:                                          ; preds = %do.end
  %10 = load i32, i32* %ch, align 4, !tbaa !6
  %sub = sub nsw i32 %10, 48
  store i32 %sub, i32* %val, align 4, !tbaa !8
  br label %while.cond

while.cond:                                       ; preds = %if.end11, %if.end5
  %11 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call6 = call i32 @text_getc(%struct._IO_FILE* %11)
  store i32 %call6, i32* %ch, align 4, !tbaa !6
  %cmp7 = icmp ne i32 %call6, -1
  br i1 %cmp7, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = load i32, i32* %ch, align 4, !tbaa !6
  %call8 = call i32 @isdigit(i32 %12) #5
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %while.body
  br label %while.end

if.end11:                                         ; preds = %while.body
  %13 = load i32, i32* %val, align 4, !tbaa !8
  %mul = mul nsw i32 %13, 10
  store i32 %mul, i32* %val, align 4, !tbaa !8
  %14 = load i32, i32* %ch, align 4, !tbaa !6
  %sub12 = sub nsw i32 %14, 48
  %15 = load i32, i32* %val, align 4, !tbaa !8
  %add = add nsw i32 %15, %sub12
  store i32 %add, i32* %val, align 4, !tbaa !8
  br label %while.cond

while.end:                                        ; preds = %if.then10, %while.cond
  %16 = load i32, i32* %val, align 4, !tbaa !8
  %17 = load i32*, i32** %result.addr, align 4, !tbaa !2
  store i32 %16, i32* %17, align 4, !tbaa !8
  %18 = load i32, i32* %ch, align 4, !tbaa !6
  %19 = load i32*, i32** %termchar.addr, align 4, !tbaa !2
  store i32 %18, i32* %19, align 4, !tbaa !6
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then4, %if.then
  %20 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  %21 = bitcast i32* %ch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

declare i32 @fclose(%struct._IO_FILE*) #2

declare void @jpeg_add_quant_table(%struct.jpeg_compress_struct*, i32, i32*, i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden i32 @read_scan_script(%struct.jpeg_compress_struct* %cinfo, i8* %filename) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %filename.addr = alloca i8*, align 4
  %fp = alloca %struct._IO_FILE*, align 4
  %scanno = alloca i32, align 4
  %ncomps = alloca i32, align 4
  %termchar = alloca i32, align 4
  %val = alloca i32, align 4
  %scanptr = alloca %struct.jpeg_scan_info*, align 4
  %scans = alloca [100 x %struct.jpeg_scan_info], align 16
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %filename, i8** %filename.addr, align 4, !tbaa !2
  %0 = bitcast %struct._IO_FILE** %fp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %scanno to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %ncomps to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %termchar to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast [100 x %struct.jpeg_scan_info]* %scans to i8*
  call void @llvm.lifetime.start.p0i8(i64 3600, i8* %6) #4
  %7 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call = call %struct._IO_FILE* @fopen(i8* %7, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  store %struct._IO_FILE* %call, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %cmp = icmp eq %struct._IO_FILE* %call, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %9 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %8, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.5, i32 0, i32 0), i8* %9)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %arraydecay = getelementptr inbounds [100 x %struct.jpeg_scan_info], [100 x %struct.jpeg_scan_info]* %scans, i32 0, i32 0
  store %struct.jpeg_scan_info* %arraydecay, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  store i32 0, i32* %scanno, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %if.end55, %if.end
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call2 = call i32 @read_scan_integer(%struct._IO_FILE* %10, i32* %val, i32* %termchar)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %while.body, label %while.end57

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %scanno, align 4, !tbaa !6
  %cmp3 = icmp sge i32 %11, 100
  br i1 %cmp3, label %if.then4, label %if.end7

if.then4:                                         ; preds = %while.body
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %13 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call5 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %12, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.6, i32 0, i32 0), i8* %13)
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call6 = call i32 @fclose(%struct._IO_FILE* %14)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %while.body
  %15 = load i32, i32* %val, align 4, !tbaa !8
  %16 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %16, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %component_index, i32 0, i32 0
  store i32 %15, i32* %arrayidx, align 4, !tbaa !6
  store i32 1, i32* %ncomps, align 4, !tbaa !6
  br label %while.cond8

while.cond8:                                      ; preds = %if.end19, %if.end7
  %17 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp9 = icmp eq i32 %17, 32
  br i1 %cmp9, label %while.body10, label %while.end

while.body10:                                     ; preds = %while.cond8
  %18 = load i32, i32* %ncomps, align 4, !tbaa !6
  %cmp11 = icmp sge i32 %18, 4
  br i1 %cmp11, label %if.then12, label %if.end15

if.then12:                                        ; preds = %while.body10
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %20 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call13 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %19, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.7, i32 0, i32 0), i8* %20)
  %21 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call14 = call i32 @fclose(%struct._IO_FILE* %21)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %while.body10
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call16 = call i32 @read_scan_integer(%struct._IO_FILE* %22, i32* %val, i32* %termchar)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %if.end15
  br label %bogus

if.end19:                                         ; preds = %if.end15
  %23 = load i32, i32* %val, align 4, !tbaa !8
  %24 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %component_index20 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %24, i32 0, i32 1
  %25 = load i32, i32* %ncomps, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [4 x i32], [4 x i32]* %component_index20, i32 0, i32 %25
  store i32 %23, i32* %arrayidx21, align 4, !tbaa !6
  %26 = load i32, i32* %ncomps, align 4, !tbaa !6
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %ncomps, align 4, !tbaa !6
  br label %while.cond8

while.end:                                        ; preds = %while.cond8
  %27 = load i32, i32* %ncomps, align 4, !tbaa !6
  %28 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %28, i32 0, i32 0
  store i32 %27, i32* %comps_in_scan, align 4, !tbaa !10
  %29 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp22 = icmp eq i32 %29, 58
  br i1 %cmp22, label %if.then23, label %if.else

if.then23:                                        ; preds = %while.end
  %30 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call24 = call i32 @read_scan_integer(%struct._IO_FILE* %30, i32* %val, i32* %termchar)
  %tobool25 = icmp ne i32 %call24, 0
  br i1 %tobool25, label %lor.lhs.false, label %if.then27

lor.lhs.false:                                    ; preds = %if.then23
  %31 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp26 = icmp ne i32 %31, 32
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %lor.lhs.false, %if.then23
  br label %bogus

if.end28:                                         ; preds = %lor.lhs.false
  %32 = load i32, i32* %val, align 4, !tbaa !8
  %33 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %33, i32 0, i32 2
  store i32 %32, i32* %Ss, align 4, !tbaa !12
  %34 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call29 = call i32 @read_scan_integer(%struct._IO_FILE* %34, i32* %val, i32* %termchar)
  %tobool30 = icmp ne i32 %call29, 0
  br i1 %tobool30, label %lor.lhs.false31, label %if.then33

lor.lhs.false31:                                  ; preds = %if.end28
  %35 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp32 = icmp ne i32 %35, 32
  br i1 %cmp32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %lor.lhs.false31, %if.end28
  br label %bogus

if.end34:                                         ; preds = %lor.lhs.false31
  %36 = load i32, i32* %val, align 4, !tbaa !8
  %37 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Se = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %37, i32 0, i32 3
  store i32 %36, i32* %Se, align 4, !tbaa !13
  %38 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call35 = call i32 @read_scan_integer(%struct._IO_FILE* %38, i32* %val, i32* %termchar)
  %tobool36 = icmp ne i32 %call35, 0
  br i1 %tobool36, label %lor.lhs.false37, label %if.then39

lor.lhs.false37:                                  ; preds = %if.end34
  %39 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp38 = icmp ne i32 %39, 32
  br i1 %cmp38, label %if.then39, label %if.end40

if.then39:                                        ; preds = %lor.lhs.false37, %if.end34
  br label %bogus

if.end40:                                         ; preds = %lor.lhs.false37
  %40 = load i32, i32* %val, align 4, !tbaa !8
  %41 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %41, i32 0, i32 4
  store i32 %40, i32* %Ah, align 4, !tbaa !14
  %42 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call41 = call i32 @read_scan_integer(%struct._IO_FILE* %42, i32* %val, i32* %termchar)
  %tobool42 = icmp ne i32 %call41, 0
  br i1 %tobool42, label %if.end44, label %if.then43

if.then43:                                        ; preds = %if.end40
  br label %bogus

if.end44:                                         ; preds = %if.end40
  %43 = load i32, i32* %val, align 4, !tbaa !8
  %44 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Al = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %44, i32 0, i32 5
  store i32 %43, i32* %Al, align 4, !tbaa !15
  br label %if.end49

if.else:                                          ; preds = %while.end
  %45 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ss45 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %45, i32 0, i32 2
  store i32 0, i32* %Ss45, align 4, !tbaa !12
  %46 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Se46 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %46, i32 0, i32 3
  store i32 63, i32* %Se46, align 4, !tbaa !13
  %47 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Ah47 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %47, i32 0, i32 4
  store i32 0, i32* %Ah47, align 4, !tbaa !14
  %48 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %Al48 = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %48, i32 0, i32 5
  store i32 0, i32* %Al48, align 4, !tbaa !15
  br label %if.end49

if.end49:                                         ; preds = %if.else, %if.end44
  %49 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp50 = icmp ne i32 %49, 59
  br i1 %cmp50, label %land.lhs.true, label %if.end55

land.lhs.true:                                    ; preds = %if.end49
  %50 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp51 = icmp ne i32 %50, -1
  br i1 %cmp51, label %if.then52, label %if.end55

if.then52:                                        ; preds = %land.lhs.true
  br label %bogus

bogus:                                            ; preds = %if.then52, %if.then43, %if.then39, %if.then33, %if.then27, %if.then18
  %51 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %52 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call53 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %51, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.8, i32 0, i32 0), i8* %52)
  %53 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call54 = call i32 @fclose(%struct._IO_FILE* %53)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end55:                                         ; preds = %land.lhs.true, %if.end49
  %54 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_scan_info, %struct.jpeg_scan_info* %54, i32 1
  store %struct.jpeg_scan_info* %incdec.ptr, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %55 = load i32, i32* %scanno, align 4, !tbaa !6
  %inc56 = add nsw i32 %55, 1
  store i32 %inc56, i32* %scanno, align 4, !tbaa !6
  br label %while.cond

while.end57:                                      ; preds = %while.cond
  %56 = load i32, i32* %termchar, align 4, !tbaa !6
  %cmp58 = icmp ne i32 %56, -1
  br i1 %cmp58, label %if.then59, label %if.end62

if.then59:                                        ; preds = %while.end57
  %57 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %58 = load i8*, i8** %filename.addr, align 4, !tbaa !2
  %call60 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %57, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.4, i32 0, i32 0), i8* %58)
  %59 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call61 = call i32 @fclose(%struct._IO_FILE* %59)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end62:                                         ; preds = %while.end57
  %60 = load i32, i32* %scanno, align 4, !tbaa !6
  %cmp63 = icmp sgt i32 %60, 0
  br i1 %cmp63, label %if.then64, label %if.end68

if.then64:                                        ; preds = %if.end62
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 1
  %62 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !16
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %62, i32 0, i32 0
  %63 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !20
  %64 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %65 = bitcast %struct.jpeg_compress_struct* %64 to %struct.jpeg_common_struct*
  %66 = load i32, i32* %scanno, align 4, !tbaa !6
  %mul = mul i32 %66, 36
  %call65 = call i8* %63(%struct.jpeg_common_struct* %65, i32 1, i32 %mul)
  %67 = bitcast i8* %call65 to %struct.jpeg_scan_info*
  store %struct.jpeg_scan_info* %67, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %68 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %69 = bitcast %struct.jpeg_scan_info* %68 to i8*
  %arraydecay66 = getelementptr inbounds [100 x %struct.jpeg_scan_info], [100 x %struct.jpeg_scan_info]* %scans, i32 0, i32 0
  %70 = bitcast %struct.jpeg_scan_info* %arraydecay66 to i8*
  %71 = load i32, i32* %scanno, align 4, !tbaa !6
  %mul67 = mul i32 %71, 36
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %69, i8* align 16 %70, i32 %mul67, i1 false)
  %72 = load %struct.jpeg_scan_info*, %struct.jpeg_scan_info** %scanptr, align 4, !tbaa !2
  %73 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %73, i32 0, i32 23
  store %struct.jpeg_scan_info* %72, %struct.jpeg_scan_info** %scan_info, align 4, !tbaa !22
  %74 = load i32, i32* %scanno, align 4, !tbaa !6
  %75 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %75, i32 0, i32 22
  store i32 %74, i32* %num_scans, align 8, !tbaa !23
  %76 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_c_set_bool_param(%struct.jpeg_compress_struct* %76, i32 1745618462, i32 0)
  br label %if.end68

if.end68:                                         ; preds = %if.then64, %if.end62
  %77 = load %struct._IO_FILE*, %struct._IO_FILE** %fp, align 4, !tbaa !2
  %call69 = call i32 @fclose(%struct._IO_FILE* %77)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end68, %if.then59, %bogus, %if.then12, %if.then4, %if.then
  %78 = bitcast [100 x %struct.jpeg_scan_info]* %scans to i8*
  call void @llvm.lifetime.end.p0i8(i64 3600, i8* %78) #4
  %79 = bitcast %struct.jpeg_scan_info** %scanptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #4
  %80 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i32* %termchar to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i32* %ncomps to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  %83 = bitcast i32* %scanno to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast %struct._IO_FILE** %fp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = load i32, i32* %retval, align 4
  ret i32 %85
}

; Function Attrs: nounwind
define internal i32 @read_scan_integer(%struct._IO_FILE* %file, i32* %result, i32* %termchar) #0 {
entry:
  %retval = alloca i32, align 4
  %file.addr = alloca %struct._IO_FILE*, align 4
  %result.addr = alloca i32*, align 4
  %termchar.addr = alloca i32*, align 4
  %ch = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct._IO_FILE* %file, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  store i32* %result, i32** %result.addr, align 4, !tbaa !2
  store i32* %termchar, i32** %termchar.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %2 = load i32*, i32** %result.addr, align 4, !tbaa !2
  %3 = load i32*, i32** %termchar.addr, align 4, !tbaa !2
  %call = call i32 @read_text_integer(%struct._IO_FILE* %1, i32* %2, i32* %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %4 = load i32*, i32** %termchar.addr, align 4, !tbaa !2
  %5 = load i32, i32* %4, align 4, !tbaa !6
  store i32 %5, i32* %ch, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %6 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp = icmp ne i32 %6, -1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %7 = load i32, i32* %ch, align 4, !tbaa !6
  %call1 = call i32 @isspace(i32 %7) #5
  %tobool2 = icmp ne i32 %call1, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %8 = phi i1 [ false, %while.cond ], [ %tobool2, %land.rhs ]
  br i1 %8, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call3 = call i32 @text_getc(%struct._IO_FILE* %9)
  store i32 %call3, i32* %ch, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %land.end
  %10 = load i32, i32* %ch, align 4, !tbaa !6
  %call4 = call i32 @isdigit(i32 %10) #5
  %tobool5 = icmp ne i32 %call4, 0
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %while.end
  %11 = load i32, i32* %ch, align 4, !tbaa !6
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call7 = call i32 @ungetc(i32 %11, %struct._IO_FILE* %12)
  %cmp8 = icmp eq i32 %call7, -1
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.then6
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %if.then6
  store i32 32, i32* %ch, align 4, !tbaa !6
  br label %if.end17

if.else:                                          ; preds = %while.end
  %13 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp11 = icmp ne i32 %13, -1
  br i1 %cmp11, label %land.lhs.true, label %if.end16

land.lhs.true:                                    ; preds = %if.else
  %14 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp12 = icmp ne i32 %14, 59
  br i1 %cmp12, label %land.lhs.true13, label %if.end16

land.lhs.true13:                                  ; preds = %land.lhs.true
  %15 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp14 = icmp ne i32 %15, 58
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %land.lhs.true13
  store i32 32, i32* %ch, align 4, !tbaa !6
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %land.lhs.true13, %land.lhs.true, %if.else
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.end10
  %16 = load i32, i32* %ch, align 4, !tbaa !6
  %17 = load i32*, i32** %termchar.addr, align 4, !tbaa !2
  store i32 %16, i32* %17, align 4, !tbaa !6
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end17, %if.then9, %if.then
  %18 = bitcast i32* %ch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @jpeg_c_set_bool_param(%struct.jpeg_compress_struct*, i32, i32) #2

; Function Attrs: nounwind
define hidden i32 @set_quality_ratings(%struct.jpeg_compress_struct* %cinfo, i8* %arg, i32 %force_baseline) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %arg.addr = alloca i8*, align 4
  %force_baseline.addr = alloca i32, align 4
  %val = alloca float, align 4
  %tblno = alloca i32, align 4
  %ch = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %arg, i8** %arg.addr, align 4, !tbaa !2
  store i32 %force_baseline, i32* %force_baseline.addr, align 4, !tbaa !6
  %0 = bitcast float* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store float 7.500000e+01, float* %val, align 4, !tbaa !24
  %1 = bitcast i32* %tblno to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ch) #4
  store i32 0, i32* %tblno, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %tblno, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %4 = load i8, i8* %3, align 1, !tbaa !26
  %tobool = icmp ne i8 %4, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  store i8 44, i8* %ch, align 1, !tbaa !26
  %5 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %call = call i32 (i8*, i8*, ...) @sscanf(i8* %5, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0), float* %val, i8* %ch)
  %cmp1 = icmp slt i32 %call, 1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %6 = load i8, i8* %ch, align 1, !tbaa !26
  %conv = sext i8 %6 to i32
  %cmp3 = icmp ne i32 %conv, 44
  br i1 %cmp3, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end
  %7 = load float, float* %val, align 4, !tbaa !24
  %call7 = call float @jpeg_float_quality_scaling(float %7)
  %conv8 = fptosi float %call7 to i32
  %8 = load i32, i32* %tblno, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @q_scale_factor, i32 0, i32 %8
  store i32 %conv8, i32* %arrayidx, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end6
  %9 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %10 = load i8, i8* %9, align 1, !tbaa !26
  %conv9 = sext i8 %10 to i32
  %tobool10 = icmp ne i32 %conv9, 0
  br i1 %tobool10, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %11 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %11, i32 1
  store i8* %incdec.ptr, i8** %arg.addr, align 4, !tbaa !2
  %12 = load i8, i8* %11, align 1, !tbaa !26
  %conv11 = sext i8 %12 to i32
  %cmp12 = icmp ne i32 %conv11, 44
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %13 = phi i1 [ false, %while.cond ], [ %cmp12, %land.rhs ]
  br i1 %13, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  br label %while.cond

while.end:                                        ; preds = %land.end
  br label %if.end17

if.else:                                          ; preds = %for.body
  %14 = load float, float* %val, align 4, !tbaa !24
  %call14 = call float @jpeg_float_quality_scaling(float %14)
  %conv15 = fptosi float %call14 to i32
  %15 = load i32, i32* %tblno, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [4 x i32], [4 x i32]* @q_scale_factor, i32 0, i32 %15
  store i32 %conv15, i32* %arrayidx16, align 4, !tbaa !6
  br label %if.end17

if.end17:                                         ; preds = %if.else, %while.end
  br label %for.inc

for.inc:                                          ; preds = %if.end17
  %16 = load i32, i32* %tblno, align 4, !tbaa !6
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %tblno, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load i32, i32* %force_baseline.addr, align 4, !tbaa !6
  call void @jpeg_default_qtables(%struct.jpeg_compress_struct* %17, i32 %18)
  %19 = load float, float* %val, align 4, !tbaa !24
  %cmp18 = fcmp oge float %19, 9.000000e+01
  br i1 %cmp18, label %if.then20, label %if.else22

if.then20:                                        ; preds = %for.end
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call21 = call i32 @set_sample_factors(%struct.jpeg_compress_struct* %20, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.10, i32 0, i32 0))
  br label %if.end28

if.else22:                                        ; preds = %for.end
  %21 = load float, float* %val, align 4, !tbaa !24
  %cmp23 = fcmp oge float %21, 8.000000e+01
  br i1 %cmp23, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.else22
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call26 = call i32 @set_sample_factors(%struct.jpeg_compress_struct* %22, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.11, i32 0, i32 0))
  br label %if.end27

if.end27:                                         ; preds = %if.then25, %if.else22
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then20
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end28, %if.then5, %if.then2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ch) #4
  %23 = bitcast i32* %tblno to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast float* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

declare i32 @sscanf(i8*, i8*, ...) #2

declare float @jpeg_float_quality_scaling(float) #2

; Function Attrs: nounwind
define internal void @jpeg_default_qtables(%struct.jpeg_compress_struct* %cinfo, i32 %force_baseline) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %force_baseline.addr = alloca i32, align 4
  %quant_tbl_master_idx = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %force_baseline, i32* %force_baseline.addr, align 4, !tbaa !6
  %0 = bitcast i32* %quant_tbl_master_idx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %quant_tbl_master_idx, align 4, !tbaa !6
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @jpeg_c_int_param_supported(%struct.jpeg_compress_struct* %1, i32 1145645745)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %call1 = call i32 @jpeg_c_get_int_param(%struct.jpeg_compress_struct* %2, i32 1145645745)
  store i32 %call1, i32* %quant_tbl_master_idx, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %4 = load i32, i32* %quant_tbl_master_idx, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [9 x [64 x i32]], [9 x [64 x i32]]* @std_luminance_quant_tbl, i32 0, i32 %4
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx, i32 0, i32 0
  %5 = load i32, i32* getelementptr inbounds ([4 x i32], [4 x i32]* @q_scale_factor, i32 0, i32 0), align 16, !tbaa !6
  %6 = load i32, i32* %force_baseline.addr, align 4, !tbaa !6
  call void @jpeg_add_quant_table(%struct.jpeg_compress_struct* %3, i32 0, i32* %arraydecay, i32 %5, i32 %6)
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load i32, i32* %quant_tbl_master_idx, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds [9 x [64 x i32]], [9 x [64 x i32]]* @std_chrominance_quant_tbl, i32 0, i32 %8
  %arraydecay3 = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx2, i32 0, i32 0
  %9 = load i32, i32* getelementptr inbounds ([4 x i32], [4 x i32]* @q_scale_factor, i32 0, i32 1), align 4, !tbaa !6
  %10 = load i32, i32* %force_baseline.addr, align 4, !tbaa !6
  call void @jpeg_add_quant_table(%struct.jpeg_compress_struct* %7, i32 1, i32* %arraydecay3, i32 %9, i32 %10)
  %11 = bitcast i32* %quant_tbl_master_idx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #4
  ret void
}

; Function Attrs: nounwind
define hidden i32 @set_sample_factors(%struct.jpeg_compress_struct* %cinfo, i8* %arg) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %arg.addr = alloca i8*, align 4
  %ci = alloca i32, align 4
  %val1 = alloca i32, align 4
  %val2 = alloca i32, align 4
  %ch1 = alloca i8, align 1
  %ch2 = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %arg, i8** %arg.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %val1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %val2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ch1) #4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ch2) #4
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %ci, align 4, !tbaa !6
  %cmp = icmp slt i32 %3, 10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %5 = load i8, i8* %4, align 1, !tbaa !26
  %tobool = icmp ne i8 %5, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  store i8 44, i8* %ch2, align 1, !tbaa !26
  %6 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %call = call i32 (i8*, i8*, ...) @sscanf(i8* %6, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.14, i32 0, i32 0), i32* %val1, i8* %ch1, i32* %val2, i8* %ch2)
  %cmp1 = icmp slt i32 %call, 3
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %7 = load i8, i8* %ch1, align 1, !tbaa !26
  %conv = sext i8 %7 to i32
  %cmp3 = icmp ne i32 %conv, 120
  br i1 %cmp3, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %if.end
  %8 = load i8, i8* %ch1, align 1, !tbaa !26
  %conv5 = sext i8 %8 to i32
  %cmp6 = icmp ne i32 %conv5, 88
  br i1 %cmp6, label %if.then11, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %if.end
  %9 = load i8, i8* %ch2, align 1, !tbaa !26
  %conv8 = sext i8 %9 to i32
  %cmp9 = icmp ne i32 %conv8, 44
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %lor.lhs.false, %land.lhs.true
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %lor.lhs.false
  %10 = load i32, i32* %val1, align 4, !tbaa !6
  %cmp13 = icmp sle i32 %10, 0
  br i1 %cmp13, label %if.then24, label %lor.lhs.false15

lor.lhs.false15:                                  ; preds = %if.end12
  %11 = load i32, i32* %val1, align 4, !tbaa !6
  %cmp16 = icmp sgt i32 %11, 4
  br i1 %cmp16, label %if.then24, label %lor.lhs.false18

lor.lhs.false18:                                  ; preds = %lor.lhs.false15
  %12 = load i32, i32* %val2, align 4, !tbaa !6
  %cmp19 = icmp sle i32 %12, 0
  br i1 %cmp19, label %if.then24, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %lor.lhs.false18
  %13 = load i32, i32* %val2, align 4, !tbaa !6
  %cmp22 = icmp sgt i32 %13, 4
  br i1 %cmp22, label %if.then24, label %if.end26

if.then24:                                        ; preds = %lor.lhs.false21, %lor.lhs.false18, %lor.lhs.false15, %if.end12
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call25 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %14, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.15, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end26:                                         ; preds = %lor.lhs.false21
  %15 = load i32, i32* %val1, align 4, !tbaa !6
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 15
  %17 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !27
  %18 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %17, i32 %18
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx, i32 0, i32 2
  store i32 %15, i32* %h_samp_factor, align 4, !tbaa !28
  %19 = load i32, i32* %val2, align 4, !tbaa !6
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info27 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 15
  %21 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info27, align 4, !tbaa !27
  %22 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %21, i32 %22
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx28, i32 0, i32 3
  store i32 %19, i32* %v_samp_factor, align 4, !tbaa !30
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end26
  %23 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %24 = load i8, i8* %23, align 1, !tbaa !26
  %conv29 = sext i8 %24 to i32
  %tobool30 = icmp ne i32 %conv29, 0
  br i1 %tobool30, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %25 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %25, i32 1
  store i8* %incdec.ptr, i8** %arg.addr, align 4, !tbaa !2
  %26 = load i8, i8* %25, align 1, !tbaa !26
  %conv31 = sext i8 %26 to i32
  %cmp32 = icmp ne i32 %conv31, 44
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %27 = phi i1 [ false, %while.cond ], [ %cmp32, %land.rhs ]
  br i1 %27, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  br label %while.cond

while.end:                                        ; preds = %land.end
  br label %if.end40

if.else:                                          ; preds = %for.body
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info34 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 15
  %29 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info34, align 4, !tbaa !27
  %30 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %29, i32 %30
  %h_samp_factor36 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx35, i32 0, i32 2
  store i32 1, i32* %h_samp_factor36, align 4, !tbaa !28
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info37 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %31, i32 0, i32 15
  %32 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info37, align 4, !tbaa !27
  %33 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx38 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %32, i32 %33
  %v_samp_factor39 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx38, i32 0, i32 3
  store i32 1, i32* %v_samp_factor39, align 4, !tbaa !30
  br label %if.end40

if.end40:                                         ; preds = %if.else, %while.end
  br label %for.inc

for.inc:                                          ; preds = %if.end40
  %34 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then24, %if.then11, %if.then2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ch2) #4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ch1) #4
  %35 = bitcast i32* %val2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i32* %val1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #4
  %38 = load i32, i32* %retval, align 4
  ret i32 %38
}

; Function Attrs: nounwind
define hidden i32 @set_quant_slots(%struct.jpeg_compress_struct* %cinfo, i8* %arg) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %arg.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %ci = alloca i32, align 4
  %ch = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %arg, i8** %arg.addr, align 4, !tbaa !2
  %0 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %val, align 4, !tbaa !6
  %1 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ch) #4
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %ci, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, 10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %4 = load i8, i8* %3, align 1, !tbaa !26
  %tobool = icmp ne i8 %4, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  store i8 44, i8* %ch, align 1, !tbaa !26
  %5 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %call = call i32 (i8*, i8*, ...) @sscanf(i8* %5, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.12, i32 0, i32 0), i32* %val, i8* %ch)
  %cmp1 = icmp slt i32 %call, 1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  %6 = load i8, i8* %ch, align 1, !tbaa !26
  %conv = sext i8 %6 to i32
  %cmp3 = icmp ne i32 %conv, 44
  br i1 %cmp3, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end
  %7 = load i32, i32* %val, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %7, 0
  br i1 %cmp7, label %if.then11, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end6
  %8 = load i32, i32* %val, align 4, !tbaa !6
  %cmp9 = icmp sge i32 %8, 4
  br i1 %cmp9, label %if.then11, label %if.end13

if.then11:                                        ; preds = %lor.lhs.false, %if.end6
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4, !tbaa !2
  %call12 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %9, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.13, i32 0, i32 0), i32 3)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %lor.lhs.false
  %10 = load i32, i32* %val, align 4, !tbaa !6
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 15
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !27
  %13 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %12, i32 %13
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx, i32 0, i32 4
  store i32 %10, i32* %quant_tbl_no, align 4, !tbaa !31
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end13
  %14 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %15 = load i8, i8* %14, align 1, !tbaa !26
  %conv14 = sext i8 %15 to i32
  %tobool15 = icmp ne i32 %conv14, 0
  br i1 %tobool15, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %16 = load i8*, i8** %arg.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %16, i32 1
  store i8* %incdec.ptr, i8** %arg.addr, align 4, !tbaa !2
  %17 = load i8, i8* %16, align 1, !tbaa !26
  %conv16 = sext i8 %17 to i32
  %cmp17 = icmp ne i32 %conv16, 44
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %18 = phi i1 [ false, %while.cond ], [ %cmp17, %land.rhs ]
  br i1 %18, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  br label %while.cond

while.end:                                        ; preds = %land.end
  br label %if.end22

if.else:                                          ; preds = %for.body
  %19 = load i32, i32* %val, align 4, !tbaa !6
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info19 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 15
  %21 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info19, align 4, !tbaa !27
  %22 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx20 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %21, i32 %22
  %quant_tbl_no21 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx20, i32 0, i32 4
  store i32 %19, i32* %quant_tbl_no21, align 4, !tbaa !31
  br label %if.end22

if.end22:                                         ; preds = %if.else, %while.end
  br label %for.inc

for.inc:                                          ; preds = %if.end22
  %23 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then11, %if.then5, %if.then2
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ch) #4
  %24 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

; Function Attrs: nounwind
define internal i32 @text_getc(%struct._IO_FILE* %file) #0 {
entry:
  %file.addr = alloca %struct._IO_FILE*, align 4
  %ch = alloca i32, align 4
  store %struct._IO_FILE* %file, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call = call i32 @getc(%struct._IO_FILE* %1)
  store i32 %call, i32* %ch, align 4, !tbaa !6
  %2 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp = icmp eq i32 %2, 35
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %land.end, %if.then
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** %file.addr, align 4, !tbaa !2
  %call1 = call i32 @getc(%struct._IO_FILE* %3)
  store i32 %call1, i32* %ch, align 4, !tbaa !6
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %4 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp2 = icmp ne i32 %4, 10
  br i1 %cmp2, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %do.cond
  %5 = load i32, i32* %ch, align 4, !tbaa !6
  %cmp3 = icmp ne i32 %5, -1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %do.cond
  %6 = phi i1 [ false, %do.cond ], [ %cmp3, %land.rhs ]
  br i1 %6, label %do.body, label %do.end

do.end:                                           ; preds = %land.end
  br label %if.end

if.end:                                           ; preds = %do.end, %entry
  %7 = load i32, i32* %ch, align 4, !tbaa !6
  %8 = bitcast i32* %ch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  ret i32 %7
}

; Function Attrs: nounwind readonly
declare i32 @isspace(i32) #3

; Function Attrs: nounwind readonly
declare i32 @isdigit(i32) #3

declare i32 @getc(%struct._IO_FILE*) #2

declare i32 @ungetc(i32, %struct._IO_FILE*) #2

declare i32 @jpeg_c_int_param_supported(%struct.jpeg_compress_struct*, i32) #2

declare i32 @jpeg_c_get_int_param(%struct.jpeg_compress_struct*, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { nounwind readonly }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!11, !7, i64 0}
!11 = !{!"", !7, i64 0, !4, i64 4, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32}
!12 = !{!11, !7, i64 20}
!13 = !{!11, !7, i64 24}
!14 = !{!11, !7, i64 28}
!15 = !{!11, !7, i64 32}
!16 = !{!17, !3, i64 4}
!17 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !18, i64 48, !7, i64 56, !7, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !7, i64 168, !3, i64 172, !7, i64 176, !7, i64 180, !7, i64 184, !7, i64 188, !7, i64 192, !4, i64 196, !7, i64 200, !7, i64 204, !7, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !19, i64 216, !19, i64 218, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !7, i64 236, !7, i64 240, !7, i64 244, !4, i64 248, !7, i64 264, !7, i64 268, !7, i64 272, !4, i64 276, !7, i64 316, !7, i64 320, !7, i64 324, !7, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !7, i64 372}
!18 = !{!"double", !4, i64 0}
!19 = !{!"short", !4, i64 0}
!20 = !{!21, !3, i64 0}
!21 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !9, i64 44, !9, i64 48}
!22 = !{!17, !3, i64 172}
!23 = !{!17, !7, i64 168}
!24 = !{!25, !25, i64 0}
!25 = !{!"float", !4, i64 0}
!26 = !{!4, !4, i64 0}
!27 = !{!17, !3, i64 68}
!28 = !{!29, !7, i64 8}
!29 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!30 = !{!29, !7, i64 12}
!31 = !{!29, !7, i64 16}
