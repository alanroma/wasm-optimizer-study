; ModuleID = 'jcphuff.c'
source_filename = "jcphuff.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_compress_struct*)*, {}* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { {}*, {}*, {}*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { {}*, {}*, {}*, {}*, {}*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { {}*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { {}*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { {}*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, {}* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.phuff_entropy_encoder = type { %struct.jpeg_entropy_encoder, i32, i8*, i32, i32, i32, %struct.jpeg_compress_struct*, [4 x i32], i32, i32, i32, i8*, i32, i32, [4 x %struct.c_derived_tbl*], [4 x i32*] }
%struct.c_derived_tbl = type { [256 x i32], [256 x i8] }

@jpeg_natural_order = external constant [0 x i32], align 4

; Function Attrs: nounwind
define hidden void @jinit_phuff_encoder(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %entropy = alloca %struct.phuff_entropy_encoder*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 1
  %3 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %3, i32 0, i32 0
  %4 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = bitcast %struct.jpeg_compress_struct* %5 to %struct.jpeg_common_struct*
  %call = call i8* %4(%struct.jpeg_common_struct* %6, i32 1, i32 108)
  %7 = bitcast i8* %call to %struct.phuff_entropy_encoder*
  store %struct.phuff_entropy_encoder* %7, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %8 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %9 = bitcast %struct.phuff_entropy_encoder* %8 to %struct.jpeg_entropy_encoder*
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 62
  store %struct.jpeg_entropy_encoder* %9, %struct.jpeg_entropy_encoder** %entropy1, align 4, !tbaa !14
  %11 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %11, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %pub, i32 0, i32 0
  store void (%struct.jpeg_compress_struct*, i32)* @start_pass_phuff, void (%struct.jpeg_compress_struct*, i32)** %start_pass, align 4, !tbaa !15
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %12 = load i32, i32* %i, align 4, !tbaa !18
  %cmp = icmp slt i32 %12, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %derived_tbls = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %13, i32 0, i32 14
  %14 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [4 x %struct.c_derived_tbl*], [4 x %struct.c_derived_tbl*]* %derived_tbls, i32 0, i32 %14
  store %struct.c_derived_tbl* null, %struct.c_derived_tbl** %arrayidx, align 4, !tbaa !2
  %15 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %count_ptrs = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %15, i32 0, i32 15
  %16 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds [4 x i32*], [4 x i32*]* %count_ptrs, i32 0, i32 %16
  store i32* null, i32** %arrayidx2, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %bit_buffer = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %18, i32 0, i32 11
  store i8* null, i8** %bit_buffer, align 4, !tbaa !19
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #4
  %20 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_phuff(%struct.jpeg_compress_struct* %cinfo, i32 %gather_statistics) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %gather_statistics.addr = alloca i32, align 4
  %entropy = alloca %struct.phuff_entropy_encoder*, align 4
  %is_DC_band = alloca i32, align 4
  %ci = alloca i32, align 4
  %tbl = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %i71 = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %gather_statistics, i32* %gather_statistics.addr, align 4, !tbaa !18
  %0 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 62
  %2 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_encoder* %2 to %struct.phuff_entropy_encoder*
  store %struct.phuff_entropy_encoder* %3, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %is_DC_band to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %tbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %cinfo2 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %9, i32 0, i32 6
  store %struct.jpeg_compress_struct* %8, %struct.jpeg_compress_struct** %cinfo2, align 4, !tbaa !20
  %10 = load i32, i32* %gather_statistics.addr, align 4, !tbaa !18
  %11 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %gather_statistics3 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %11, i32 0, i32 1
  store i32 %10, i32* %gather_statistics3, align 4, !tbaa !21
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 50
  %13 = load i32, i32* %Ss, align 4, !tbaa !22
  %cmp = icmp eq i32 %13, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %is_DC_band, align 4, !tbaa !18
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 52
  %15 = load i32, i32* %Ah, align 4, !tbaa !23
  %cmp4 = icmp eq i32 %15, 0
  br i1 %cmp4, label %if.then, label %if.else9

if.then:                                          ; preds = %entry
  %16 = load i32, i32* %is_DC_band, align 4, !tbaa !18
  %tobool = icmp ne i32 %16, 0
  br i1 %tobool, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  %17 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %17, i32 0, i32 0
  %encode_mcu = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %pub, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, [64 x i16]**)* @encode_mcu_DC_first, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)** %encode_mcu, align 4, !tbaa !24
  br label %if.end

if.else:                                          ; preds = %if.then
  %18 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %pub7 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %18, i32 0, i32 0
  %encode_mcu8 = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %pub7, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, [64 x i16]**)* @encode_mcu_AC_first, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)** %encode_mcu8, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  br label %if.end23

if.else9:                                         ; preds = %entry
  %19 = load i32, i32* %is_DC_band, align 4, !tbaa !18
  %tobool10 = icmp ne i32 %19, 0
  br i1 %tobool10, label %if.then11, label %if.else14

if.then11:                                        ; preds = %if.else9
  %20 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %pub12 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %20, i32 0, i32 0
  %encode_mcu13 = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %pub12, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, [64 x i16]**)* @encode_mcu_DC_refine, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)** %encode_mcu13, align 4, !tbaa !24
  br label %if.end22

if.else14:                                        ; preds = %if.else9
  %21 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %pub15 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %21, i32 0, i32 0
  %encode_mcu16 = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %pub15, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, [64 x i16]**)* @encode_mcu_AC_refine, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)** %encode_mcu16, align 4, !tbaa !24
  %22 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %bit_buffer = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %22, i32 0, i32 11
  %23 = load i8*, i8** %bit_buffer, align 4, !tbaa !19
  %cmp17 = icmp eq i8* %23, null
  br i1 %cmp17, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.else14
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 1
  %25 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %25, i32 0, i32 0
  %26 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = bitcast %struct.jpeg_compress_struct* %27 to %struct.jpeg_common_struct*
  %call = call i8* %26(%struct.jpeg_common_struct* %28, i32 1, i32 1000)
  %29 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %bit_buffer20 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %29, i32 0, i32 11
  store i8* %call, i8** %bit_buffer20, align 4, !tbaa !19
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %if.else14
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %if.then11
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.end
  %30 = load i32, i32* %gather_statistics.addr, align 4, !tbaa !18
  %tobool24 = icmp ne i32 %30, 0
  br i1 %tobool24, label %if.then25, label %if.else28

if.then25:                                        ; preds = %if.end23
  %31 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %pub26 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %31, i32 0, i32 0
  %finish_pass = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %pub26, i32 0, i32 2
  %finish_pass27 = bitcast {}** %finish_pass to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @finish_pass_gather_phuff, void (%struct.jpeg_compress_struct*)** %finish_pass27, align 4, !tbaa !25
  br label %if.end32

if.else28:                                        ; preds = %if.end23
  %32 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %pub29 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %32, i32 0, i32 0
  %finish_pass30 = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %pub29, i32 0, i32 2
  %finish_pass31 = bitcast {}** %finish_pass30 to void (%struct.jpeg_compress_struct*)**
  store void (%struct.jpeg_compress_struct*)* @finish_pass_phuff, void (%struct.jpeg_compress_struct*)** %finish_pass31, align 4, !tbaa !25
  br label %if.end32

if.end32:                                         ; preds = %if.else28, %if.then25
  store i32 0, i32* %ci, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc90, %if.end32
  %33 = load i32, i32* %ci, align 4, !tbaa !18
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %34, i32 0, i32 44
  %35 = load i32, i32* %comps_in_scan, align 4, !tbaa !26
  %cmp33 = icmp slt i32 %33, %35
  br i1 %cmp33, label %for.body, label %for.end92

for.body:                                         ; preds = %for.cond
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 45
  %37 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %37
  %38 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %38, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %39 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %last_dc_val = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %39, i32 0, i32 7
  %40 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx35 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %40
  store i32 0, i32* %arrayidx35, align 4, !tbaa !18
  %41 = load i32, i32* %is_DC_band, align 4, !tbaa !18
  %tobool36 = icmp ne i32 %41, 0
  br i1 %tobool36, label %if.then37, label %if.else43

if.then37:                                        ; preds = %for.body
  %42 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %42, i32 0, i32 52
  %43 = load i32, i32* %Ah38, align 4, !tbaa !23
  %cmp39 = icmp ne i32 %43, 0
  br i1 %cmp39, label %if.then41, label %if.end42

if.then41:                                        ; preds = %if.then37
  br label %for.inc90

if.end42:                                         ; preds = %if.then37
  %44 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %44, i32 0, i32 5
  %45 = load i32, i32* %dc_tbl_no, align 4, !tbaa !27
  store i32 %45, i32* %tbl, align 4, !tbaa !18
  br label %if.end45

if.else43:                                        ; preds = %for.body
  %46 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %46, i32 0, i32 6
  %47 = load i32, i32* %ac_tbl_no, align 4, !tbaa !29
  store i32 %47, i32* %tbl, align 4, !tbaa !18
  %48 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %ac_tbl_no44 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %48, i32 0, i32 8
  store i32 %47, i32* %ac_tbl_no44, align 4, !tbaa !30
  br label %if.end45

if.end45:                                         ; preds = %if.else43, %if.end42
  %49 = load i32, i32* %gather_statistics.addr, align 4, !tbaa !18
  %tobool46 = icmp ne i32 %49, 0
  br i1 %tobool46, label %if.then47, label %if.else87

if.then47:                                        ; preds = %if.end45
  %50 = load i32, i32* %tbl, align 4, !tbaa !18
  %cmp48 = icmp slt i32 %50, 0
  br i1 %cmp48, label %if.then52, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then47
  %51 = load i32, i32* %tbl, align 4, !tbaa !18
  %cmp50 = icmp sge i32 %51, 4
  br i1 %cmp50, label %if.then52, label %if.end56

if.then52:                                        ; preds = %lor.lhs.false, %if.then47
  %52 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %52, i32 0, i32 0
  %53 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !31
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %53, i32 0, i32 5
  store i32 50, i32* %msg_code, align 4, !tbaa !32
  %54 = load i32, i32* %tbl, align 4, !tbaa !18
  %55 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err53 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %55, i32 0, i32 0
  %56 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err53, align 8, !tbaa !31
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %56, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx54 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %54, i32* %arrayidx54, align 4, !tbaa !34
  %57 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err55 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %57, i32 0, i32 0
  %58 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err55, align 8, !tbaa !31
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %58, i32 0, i32 0
  %59 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !35
  %60 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %61 = bitcast %struct.jpeg_compress_struct* %60 to %struct.jpeg_common_struct*
  call void %59(%struct.jpeg_common_struct* %61)
  br label %if.end56

if.end56:                                         ; preds = %if.then52, %lor.lhs.false
  %62 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %count_ptrs = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %62, i32 0, i32 15
  %63 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx57 = getelementptr inbounds [4 x i32*], [4 x i32*]* %count_ptrs, i32 0, i32 %63
  %64 = load i32*, i32** %arrayidx57, align 4, !tbaa !2
  %cmp58 = icmp eq i32* %64, null
  br i1 %cmp58, label %if.then60, label %if.end66

if.then60:                                        ; preds = %if.end56
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem61 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %65, i32 0, i32 1
  %66 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem61, align 4, !tbaa !6
  %alloc_small62 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %66, i32 0, i32 0
  %67 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small62, align 4, !tbaa !11
  %68 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %69 = bitcast %struct.jpeg_compress_struct* %68 to %struct.jpeg_common_struct*
  %call63 = call i8* %67(%struct.jpeg_common_struct* %69, i32 1, i32 1028)
  %70 = bitcast i8* %call63 to i32*
  %71 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %count_ptrs64 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %71, i32 0, i32 15
  %72 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx65 = getelementptr inbounds [4 x i32*], [4 x i32*]* %count_ptrs64, i32 0, i32 %72
  store i32* %70, i32** %arrayidx65, align 4, !tbaa !2
  br label %if.end66

if.end66:                                         ; preds = %if.then60, %if.end56
  %73 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %count_ptrs67 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %73, i32 0, i32 15
  %74 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx68 = getelementptr inbounds [4 x i32*], [4 x i32*]* %count_ptrs67, i32 0, i32 %74
  %75 = load i32*, i32** %arrayidx68, align 4, !tbaa !2
  %76 = bitcast i32* %75 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %76, i8 0, i32 1028, i1 false)
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %77, i32 0, i32 54
  %78 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !36
  %trellis_passes = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %78, i32 0, i32 11
  %79 = load i32, i32* %trellis_passes, align 4, !tbaa !37
  %tobool69 = icmp ne i32 %79, 0
  br i1 %tobool69, label %if.then70, label %if.end86

if.then70:                                        ; preds = %if.end66
  %80 = bitcast i32* %i71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #4
  %81 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #4
  store i32 0, i32* %i71, align 4, !tbaa !18
  br label %for.cond72

for.cond72:                                       ; preds = %for.inc83, %if.then70
  %82 = load i32, i32* %i71, align 4, !tbaa !18
  %cmp73 = icmp slt i32 %82, 16
  br i1 %cmp73, label %for.body75, label %for.end85

for.body75:                                       ; preds = %for.cond72
  store i32 0, i32* %j, align 4, !tbaa !18
  br label %for.cond76

for.cond76:                                       ; preds = %for.inc, %for.body75
  %83 = load i32, i32* %j, align 4, !tbaa !18
  %cmp77 = icmp slt i32 %83, 12
  br i1 %cmp77, label %for.body79, label %for.end

for.body79:                                       ; preds = %for.cond76
  %84 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %count_ptrs80 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %84, i32 0, i32 15
  %85 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx81 = getelementptr inbounds [4 x i32*], [4 x i32*]* %count_ptrs80, i32 0, i32 %85
  %86 = load i32*, i32** %arrayidx81, align 4, !tbaa !2
  %87 = load i32, i32* %i71, align 4, !tbaa !18
  %mul = mul nsw i32 16, %87
  %88 = load i32, i32* %j, align 4, !tbaa !18
  %add = add nsw i32 %mul, %88
  %arrayidx82 = getelementptr inbounds i32, i32* %86, i32 %add
  store i32 1, i32* %arrayidx82, align 4, !tbaa !40
  br label %for.inc

for.inc:                                          ; preds = %for.body79
  %89 = load i32, i32* %j, align 4, !tbaa !18
  %inc = add nsw i32 %89, 1
  store i32 %inc, i32* %j, align 4, !tbaa !18
  br label %for.cond76

for.end:                                          ; preds = %for.cond76
  br label %for.inc83

for.inc83:                                        ; preds = %for.end
  %90 = load i32, i32* %i71, align 4, !tbaa !18
  %inc84 = add nsw i32 %90, 1
  store i32 %inc84, i32* %i71, align 4, !tbaa !18
  br label %for.cond72

for.end85:                                        ; preds = %for.cond72
  %91 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i32* %i71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  br label %if.end86

if.end86:                                         ; preds = %for.end85, %if.end66
  br label %if.end89

if.else87:                                        ; preds = %if.end45
  %93 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %94 = load i32, i32* %is_DC_band, align 4, !tbaa !18
  %95 = load i32, i32* %tbl, align 4, !tbaa !18
  %96 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %derived_tbls = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %96, i32 0, i32 14
  %97 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx88 = getelementptr inbounds [4 x %struct.c_derived_tbl*], [4 x %struct.c_derived_tbl*]* %derived_tbls, i32 0, i32 %97
  call void @jpeg_make_c_derived_tbl(%struct.jpeg_compress_struct* %93, i32 %94, i32 %95, %struct.c_derived_tbl** %arrayidx88)
  br label %if.end89

if.end89:                                         ; preds = %if.else87, %if.end86
  br label %for.inc90

for.inc90:                                        ; preds = %if.end89, %if.then41
  %98 = load i32, i32* %ci, align 4, !tbaa !18
  %inc91 = add nsw i32 %98, 1
  store i32 %inc91, i32* %ci, align 4, !tbaa !18
  br label %for.cond

for.end92:                                        ; preds = %for.cond
  %99 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %EOBRUN = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %99, i32 0, i32 9
  store i32 0, i32* %EOBRUN, align 4, !tbaa !41
  %100 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %BE = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %100, i32 0, i32 10
  store i32 0, i32* %BE, align 4, !tbaa !42
  %101 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %put_buffer = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %101, i32 0, i32 4
  store i32 0, i32* %put_buffer, align 4, !tbaa !43
  %102 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %put_bits = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %102, i32 0, i32 5
  store i32 0, i32* %put_bits, align 4, !tbaa !44
  %103 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %103, i32 0, i32 30
  %104 = load i32, i32* %restart_interval, align 8, !tbaa !45
  %105 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %105, i32 0, i32 12
  store i32 %104, i32* %restarts_to_go, align 4, !tbaa !46
  %106 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %106, i32 0, i32 13
  store i32 0, i32* %next_restart_num, align 4, !tbaa !47
  %107 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  %108 = bitcast i32* %tbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #4
  %109 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %is_DC_band to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @encode_mcu_DC_first(%struct.jpeg_compress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.phuff_entropy_encoder*, align 4
  %temp = alloca i32, align 4
  %temp2 = alloca i32, align 4
  %nbits = alloca i32, align 4
  %blkn = alloca i32, align 4
  %ci = alloca i32, align 4
  %Al = alloca i32, align 4
  %block = alloca [64 x i16]*, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 62
  %2 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_encoder* %2 to %struct.phuff_entropy_encoder*
  store %struct.phuff_entropy_encoder* %3, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %temp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 53
  %11 = load i32, i32* %Al2, align 8, !tbaa !48
  store i32 %11, i32* %Al, align 4, !tbaa !18
  %12 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 6
  %15 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !49
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %15, i32 0, i32 0
  %16 = load i8*, i8** %next_output_byte, align 4, !tbaa !50
  %17 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte3 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %17, i32 0, i32 2
  store i8* %16, i8** %next_output_byte3, align 4, !tbaa !52
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 6
  %19 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest4, align 8, !tbaa !49
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %19, i32 0, i32 1
  %20 = load i32, i32* %free_in_buffer, align 4, !tbaa !53
  %21 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer5 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %21, i32 0, i32 3
  store i32 %20, i32* %free_in_buffer5, align 4, !tbaa !54
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 30
  %23 = load i32, i32* %restart_interval, align 8, !tbaa !45
  %tobool = icmp ne i32 %23, 0
  br i1 %tobool, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %24 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %24, i32 0, i32 12
  %25 = load i32, i32* %restarts_to_go, align 4, !tbaa !46
  %cmp = icmp eq i32 %25, 0
  br i1 %cmp, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %26 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %27 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %27, i32 0, i32 13
  %28 = load i32, i32* %next_restart_num, align 4, !tbaa !47
  call void @emit_restart(%struct.phuff_entropy_encoder* %26, i32 %28)
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  br label %if.end7

if.end7:                                          ; preds = %if.end, %entry
  store i32 0, i32* %blkn, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end7
  %29 = load i32, i32* %blkn, align 4, !tbaa !18
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 48
  %31 = load i32, i32* %blocks_in_MCU, align 8, !tbaa !55
  %cmp8 = icmp slt i32 %29, %31
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %32 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %33 = load i32, i32* %blkn, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %32, i32 %33
  %34 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  store [64 x i16]* %34, [64 x i16]** %block, align 4, !tbaa !2
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCU_membership = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %35, i32 0, i32 49
  %36 = load i32, i32* %blkn, align 4, !tbaa !18
  %arrayidx9 = getelementptr inbounds [10 x i32], [10 x i32]* %MCU_membership, i32 0, i32 %36
  %37 = load i32, i32* %arrayidx9, align 4, !tbaa !18
  store i32 %37, i32* %ci, align 4, !tbaa !18
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 45
  %39 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx10 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %39
  %40 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx10, align 4, !tbaa !2
  store %struct.jpeg_component_info* %40, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %41 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds [64 x i16], [64 x i16]* %41, i32 0, i32 0
  %42 = load i16, i16* %arrayidx11, align 2, !tbaa !56
  %conv = sext i16 %42 to i32
  %43 = load i32, i32* %Al, align 4, !tbaa !18
  %shr = ashr i32 %conv, %43
  store i32 %shr, i32* %temp2, align 4, !tbaa !18
  %44 = load i32, i32* %temp2, align 4, !tbaa !18
  %45 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %last_dc_val = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %45, i32 0, i32 7
  %46 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx12 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx12, align 4, !tbaa !18
  %sub = sub nsw i32 %44, %47
  store i32 %sub, i32* %temp, align 4, !tbaa !18
  %48 = load i32, i32* %temp2, align 4, !tbaa !18
  %49 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %last_dc_val13 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %49, i32 0, i32 7
  %50 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx14 = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val13, i32 0, i32 %50
  store i32 %48, i32* %arrayidx14, align 4, !tbaa !18
  %51 = load i32, i32* %temp, align 4, !tbaa !18
  store i32 %51, i32* %temp2, align 4, !tbaa !18
  %52 = load i32, i32* %temp, align 4, !tbaa !18
  %cmp15 = icmp slt i32 %52, 0
  br i1 %cmp15, label %if.then17, label %if.end19

if.then17:                                        ; preds = %for.body
  %53 = load i32, i32* %temp, align 4, !tbaa !18
  %sub18 = sub nsw i32 0, %53
  store i32 %sub18, i32* %temp, align 4, !tbaa !18
  %54 = load i32, i32* %temp2, align 4, !tbaa !18
  %dec = add nsw i32 %54, -1
  store i32 %dec, i32* %temp2, align 4, !tbaa !18
  br label %if.end19

if.end19:                                         ; preds = %if.then17, %for.body
  store i32 0, i32* %nbits, align 4, !tbaa !18
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end19
  %55 = load i32, i32* %temp, align 4, !tbaa !18
  %tobool20 = icmp ne i32 %55, 0
  br i1 %tobool20, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %56 = load i32, i32* %nbits, align 4, !tbaa !18
  %inc = add nsw i32 %56, 1
  store i32 %inc, i32* %nbits, align 4, !tbaa !18
  %57 = load i32, i32* %temp, align 4, !tbaa !18
  %shr21 = ashr i32 %57, 1
  store i32 %shr21, i32* %temp, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %58 = load i32, i32* %nbits, align 4, !tbaa !18
  %cmp22 = icmp sgt i32 %58, 11
  br i1 %cmp22, label %if.then24, label %if.end26

if.then24:                                        ; preds = %while.end
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 0
  %60 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !31
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %60, i32 0, i32 5
  store i32 6, i32* %msg_code, align 4, !tbaa !32
  %61 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err25 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %61, i32 0, i32 0
  %62 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err25, align 8, !tbaa !31
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %62, i32 0, i32 0
  %63 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !35
  %64 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %65 = bitcast %struct.jpeg_compress_struct* %64 to %struct.jpeg_common_struct*
  call void %63(%struct.jpeg_common_struct* %65)
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %while.end
  %66 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %67 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %67, i32 0, i32 5
  %68 = load i32, i32* %dc_tbl_no, align 4, !tbaa !27
  %69 = load i32, i32* %nbits, align 4, !tbaa !18
  call void @emit_symbol(%struct.phuff_entropy_encoder* %66, i32 %68, i32 %69)
  %70 = load i32, i32* %nbits, align 4, !tbaa !18
  %tobool27 = icmp ne i32 %70, 0
  br i1 %tobool27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.end26
  %71 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %72 = load i32, i32* %temp2, align 4, !tbaa !18
  %73 = load i32, i32* %nbits, align 4, !tbaa !18
  call void @emit_bits(%struct.phuff_entropy_encoder* %71, i32 %72, i32 %73)
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %if.end26
  br label %for.inc

for.inc:                                          ; preds = %if.end29
  %74 = load i32, i32* %blkn, align 4, !tbaa !18
  %inc30 = add nsw i32 %74, 1
  store i32 %inc30, i32* %blkn, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %75 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte31 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %75, i32 0, i32 2
  %76 = load i8*, i8** %next_output_byte31, align 4, !tbaa !52
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest32 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %77, i32 0, i32 6
  %78 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest32, align 8, !tbaa !49
  %next_output_byte33 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %78, i32 0, i32 0
  store i8* %76, i8** %next_output_byte33, align 4, !tbaa !50
  %79 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer34 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %79, i32 0, i32 3
  %80 = load i32, i32* %free_in_buffer34, align 4, !tbaa !54
  %81 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %81, i32 0, i32 6
  %82 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest35, align 8, !tbaa !49
  %free_in_buffer36 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %82, i32 0, i32 1
  store i32 %80, i32* %free_in_buffer36, align 4, !tbaa !53
  %83 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval37 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %83, i32 0, i32 30
  %84 = load i32, i32* %restart_interval37, align 8, !tbaa !45
  %tobool38 = icmp ne i32 %84, 0
  br i1 %tobool38, label %if.then39, label %if.end52

if.then39:                                        ; preds = %for.end
  %85 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go40 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %85, i32 0, i32 12
  %86 = load i32, i32* %restarts_to_go40, align 4, !tbaa !46
  %cmp41 = icmp eq i32 %86, 0
  br i1 %cmp41, label %if.then43, label %if.end49

if.then43:                                        ; preds = %if.then39
  %87 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval44 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %87, i32 0, i32 30
  %88 = load i32, i32* %restart_interval44, align 8, !tbaa !45
  %89 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go45 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %89, i32 0, i32 12
  store i32 %88, i32* %restarts_to_go45, align 4, !tbaa !46
  %90 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num46 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %90, i32 0, i32 13
  %91 = load i32, i32* %next_restart_num46, align 4, !tbaa !47
  %inc47 = add nsw i32 %91, 1
  store i32 %inc47, i32* %next_restart_num46, align 4, !tbaa !47
  %92 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num48 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %92, i32 0, i32 13
  %93 = load i32, i32* %next_restart_num48, align 4, !tbaa !47
  %and = and i32 %93, 7
  store i32 %and, i32* %next_restart_num48, align 4, !tbaa !47
  br label %if.end49

if.end49:                                         ; preds = %if.then43, %if.then39
  %94 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go50 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %94, i32 0, i32 12
  %95 = load i32, i32* %restarts_to_go50, align 4, !tbaa !46
  %dec51 = add i32 %95, -1
  store i32 %dec51, i32* %restarts_to_go50, align 4, !tbaa !46
  br label %if.end52

if.end52:                                         ; preds = %if.end49, %for.end
  %96 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  %98 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  %99 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #4
  %100 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #4
  %101 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #4
  %102 = bitcast i32* %temp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #4
  %103 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #4
  %104 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #4
  ret i32 1
}

; Function Attrs: nounwind
define internal i32 @encode_mcu_AC_first(%struct.jpeg_compress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.phuff_entropy_encoder*, align 4
  %temp = alloca i32, align 4
  %temp2 = alloca i32, align 4
  %nbits = alloca i32, align 4
  %r = alloca i32, align 4
  %k = alloca i32, align 4
  %Se = alloca i32, align 4
  %Al = alloca i32, align 4
  %block = alloca [64 x i16]*, align 4
  %deadzone = alloca i32, align 4
  %sign = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 62
  %2 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_encoder* %2 to %struct.phuff_entropy_encoder*
  store %struct.phuff_entropy_encoder* %3, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %temp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 51
  %11 = load i32, i32* %Se2, align 8, !tbaa !57
  store i32 %11, i32* %Se, align 4, !tbaa !18
  %12 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 53
  %14 = load i32, i32* %Al3, align 8, !tbaa !48
  store i32 %14, i32* %Al, align 4, !tbaa !18
  %15 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %deadzone to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load i32, i32* %Al, align 4, !tbaa !18
  %shl = shl i32 1, %17
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %deadzone, align 4, !tbaa !18
  %18 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 6
  %20 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !49
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %20, i32 0, i32 0
  %21 = load i8*, i8** %next_output_byte, align 4, !tbaa !50
  %22 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte4 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %22, i32 0, i32 2
  store i8* %21, i8** %next_output_byte4, align 4, !tbaa !52
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 6
  %24 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest5, align 8, !tbaa !49
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %24, i32 0, i32 1
  %25 = load i32, i32* %free_in_buffer, align 4, !tbaa !53
  %26 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer6 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %26, i32 0, i32 3
  store i32 %25, i32* %free_in_buffer6, align 4, !tbaa !54
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 30
  %28 = load i32, i32* %restart_interval, align 8, !tbaa !45
  %tobool = icmp ne i32 %28, 0
  br i1 %tobool, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %29 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %29, i32 0, i32 12
  %30 = load i32, i32* %restarts_to_go, align 4, !tbaa !46
  %cmp = icmp eq i32 %30, 0
  br i1 %cmp, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %31 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %32 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %32, i32 0, i32 13
  %33 = load i32, i32* %next_restart_num, align 4, !tbaa !47
  call void @emit_restart(%struct.phuff_entropy_encoder* %31, i32 %33)
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  br label %if.end8

if.end8:                                          ; preds = %if.end, %entry
  %34 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %34, i32 0
  %35 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  store [64 x i16]* %35, [64 x i16]** %block, align 4, !tbaa !2
  store i32 0, i32* %r, align 4, !tbaa !18
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 50
  %37 = load i32, i32* %Ss, align 4, !tbaa !22
  store i32 %37, i32* %k, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %38 = load i32, i32* %k, align 4, !tbaa !18
  %39 = load i32, i32* %Se, align 4, !tbaa !18
  %cmp9 = icmp sle i32 %38, %39
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %40 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %41 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx10 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx10, align 4, !tbaa !18
  %arrayidx11 = getelementptr inbounds [64 x i16], [64 x i16]* %40, i32 0, i32 %42
  %43 = load i16, i16* %arrayidx11, align 2, !tbaa !56
  %conv = sext i16 %43 to i32
  store i32 %conv, i32* %temp, align 4, !tbaa !18
  %44 = load i32, i32* %temp, align 4, !tbaa !18
  %45 = load i32, i32* %deadzone, align 4, !tbaa !18
  %add = add nsw i32 %44, %45
  %46 = load i32, i32* %deadzone, align 4, !tbaa !18
  %mul = mul nsw i32 2, %46
  %cmp12 = icmp ule i32 %add, %mul
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %for.body
  %47 = load i32, i32* %r, align 4, !tbaa !18
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %r, align 4, !tbaa !18
  br label %for.inc

if.end15:                                         ; preds = %for.body
  %48 = load i32, i32* %temp, align 4, !tbaa !18
  %shr = ashr i32 %48, 31
  store i32 %shr, i32* %sign, align 4, !tbaa !18
  %49 = load i32, i32* %sign, align 4, !tbaa !18
  %50 = load i32, i32* %temp, align 4, !tbaa !18
  %add16 = add nsw i32 %50, %49
  store i32 %add16, i32* %temp, align 4, !tbaa !18
  %51 = load i32, i32* %temp, align 4, !tbaa !18
  %52 = load i32, i32* %sign, align 4, !tbaa !18
  %xor = xor i32 %51, %52
  %53 = load i32, i32* %Al, align 4, !tbaa !18
  %shr17 = ashr i32 %xor, %53
  store i32 %shr17, i32* %temp, align 4, !tbaa !18
  %54 = load i32, i32* %temp, align 4, !tbaa !18
  %55 = load i32, i32* %sign, align 4, !tbaa !18
  %xor18 = xor i32 %54, %55
  store i32 %xor18, i32* %temp2, align 4, !tbaa !18
  %56 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %EOBRUN = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %56, i32 0, i32 9
  %57 = load i32, i32* %EOBRUN, align 4, !tbaa !41
  %cmp19 = icmp ugt i32 %57, 0
  br i1 %cmp19, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end15
  %58 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  call void @emit_eobrun(%struct.phuff_entropy_encoder* %58)
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %if.end15
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end22
  %59 = load i32, i32* %r, align 4, !tbaa !18
  %cmp23 = icmp sgt i32 %59, 15
  br i1 %cmp23, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %60 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %61 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %61, i32 0, i32 8
  %62 = load i32, i32* %ac_tbl_no, align 4, !tbaa !30
  call void @emit_symbol(%struct.phuff_entropy_encoder* %60, i32 %62, i32 240)
  %63 = load i32, i32* %r, align 4, !tbaa !18
  %sub25 = sub nsw i32 %63, 16
  store i32 %sub25, i32* %r, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 1, i32* %nbits, align 4, !tbaa !18
  br label %while.cond26

while.cond26:                                     ; preds = %while.body29, %while.end
  %64 = load i32, i32* %temp, align 4, !tbaa !18
  %shr27 = ashr i32 %64, 1
  store i32 %shr27, i32* %temp, align 4, !tbaa !18
  %tobool28 = icmp ne i32 %shr27, 0
  br i1 %tobool28, label %while.body29, label %while.end31

while.body29:                                     ; preds = %while.cond26
  %65 = load i32, i32* %nbits, align 4, !tbaa !18
  %inc30 = add nsw i32 %65, 1
  store i32 %inc30, i32* %nbits, align 4, !tbaa !18
  br label %while.cond26

while.end31:                                      ; preds = %while.cond26
  %66 = load i32, i32* %nbits, align 4, !tbaa !18
  %cmp32 = icmp sgt i32 %66, 10
  br i1 %cmp32, label %if.then34, label %if.end36

if.then34:                                        ; preds = %while.end31
  %67 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %67, i32 0, i32 0
  %68 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !31
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %68, i32 0, i32 5
  store i32 6, i32* %msg_code, align 4, !tbaa !32
  %69 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %69, i32 0, i32 0
  %70 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err35, align 8, !tbaa !31
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %70, i32 0, i32 0
  %71 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !35
  %72 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %73 = bitcast %struct.jpeg_compress_struct* %72 to %struct.jpeg_common_struct*
  call void %71(%struct.jpeg_common_struct* %73)
  br label %if.end36

if.end36:                                         ; preds = %if.then34, %while.end31
  %74 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %75 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %ac_tbl_no37 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %75, i32 0, i32 8
  %76 = load i32, i32* %ac_tbl_no37, align 4, !tbaa !30
  %77 = load i32, i32* %r, align 4, !tbaa !18
  %shl38 = shl i32 %77, 4
  %78 = load i32, i32* %nbits, align 4, !tbaa !18
  %add39 = add nsw i32 %shl38, %78
  call void @emit_symbol(%struct.phuff_entropy_encoder* %74, i32 %76, i32 %add39)
  %79 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %80 = load i32, i32* %temp2, align 4, !tbaa !18
  %81 = load i32, i32* %nbits, align 4, !tbaa !18
  call void @emit_bits(%struct.phuff_entropy_encoder* %79, i32 %80, i32 %81)
  store i32 0, i32* %r, align 4, !tbaa !18
  br label %for.inc

for.inc:                                          ; preds = %if.end36, %if.then14
  %82 = load i32, i32* %k, align 4, !tbaa !18
  %inc40 = add nsw i32 %82, 1
  store i32 %inc40, i32* %k, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %83 = load i32, i32* %r, align 4, !tbaa !18
  %cmp41 = icmp sgt i32 %83, 0
  br i1 %cmp41, label %if.then43, label %if.end51

if.then43:                                        ; preds = %for.end
  %84 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %EOBRUN44 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %84, i32 0, i32 9
  %85 = load i32, i32* %EOBRUN44, align 4, !tbaa !41
  %inc45 = add i32 %85, 1
  store i32 %inc45, i32* %EOBRUN44, align 4, !tbaa !41
  %86 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %EOBRUN46 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %86, i32 0, i32 9
  %87 = load i32, i32* %EOBRUN46, align 4, !tbaa !41
  %cmp47 = icmp eq i32 %87, 32767
  br i1 %cmp47, label %if.then49, label %if.end50

if.then49:                                        ; preds = %if.then43
  %88 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  call void @emit_eobrun(%struct.phuff_entropy_encoder* %88)
  br label %if.end50

if.end50:                                         ; preds = %if.then49, %if.then43
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %for.end
  %89 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte52 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %89, i32 0, i32 2
  %90 = load i8*, i8** %next_output_byte52, align 4, !tbaa !52
  %91 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest53 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %91, i32 0, i32 6
  %92 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest53, align 8, !tbaa !49
  %next_output_byte54 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %92, i32 0, i32 0
  store i8* %90, i8** %next_output_byte54, align 4, !tbaa !50
  %93 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer55 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %93, i32 0, i32 3
  %94 = load i32, i32* %free_in_buffer55, align 4, !tbaa !54
  %95 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest56 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %95, i32 0, i32 6
  %96 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest56, align 8, !tbaa !49
  %free_in_buffer57 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %96, i32 0, i32 1
  store i32 %94, i32* %free_in_buffer57, align 4, !tbaa !53
  %97 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval58 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %97, i32 0, i32 30
  %98 = load i32, i32* %restart_interval58, align 8, !tbaa !45
  %tobool59 = icmp ne i32 %98, 0
  br i1 %tobool59, label %if.then60, label %if.end72

if.then60:                                        ; preds = %if.end51
  %99 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go61 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %99, i32 0, i32 12
  %100 = load i32, i32* %restarts_to_go61, align 4, !tbaa !46
  %cmp62 = icmp eq i32 %100, 0
  br i1 %cmp62, label %if.then64, label %if.end70

if.then64:                                        ; preds = %if.then60
  %101 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval65 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %101, i32 0, i32 30
  %102 = load i32, i32* %restart_interval65, align 8, !tbaa !45
  %103 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go66 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %103, i32 0, i32 12
  store i32 %102, i32* %restarts_to_go66, align 4, !tbaa !46
  %104 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num67 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %104, i32 0, i32 13
  %105 = load i32, i32* %next_restart_num67, align 4, !tbaa !47
  %inc68 = add nsw i32 %105, 1
  store i32 %inc68, i32* %next_restart_num67, align 4, !tbaa !47
  %106 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num69 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %106, i32 0, i32 13
  %107 = load i32, i32* %next_restart_num69, align 4, !tbaa !47
  %and = and i32 %107, 7
  store i32 %and, i32* %next_restart_num69, align 4, !tbaa !47
  br label %if.end70

if.end70:                                         ; preds = %if.then64, %if.then60
  %108 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go71 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %108, i32 0, i32 12
  %109 = load i32, i32* %restarts_to_go71, align 4, !tbaa !46
  %dec = add i32 %109, -1
  store i32 %dec, i32* %restarts_to_go71, align 4, !tbaa !46
  br label %if.end72

if.end72:                                         ; preds = %if.end70, %if.end51
  %110 = bitcast i32* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32* %deadzone to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  %114 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #4
  %115 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #4
  %116 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #4
  %117 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #4
  %118 = bitcast i32* %temp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #4
  %119 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #4
  %120 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #4
  ret i32 1
}

; Function Attrs: nounwind
define internal i32 @encode_mcu_DC_refine(%struct.jpeg_compress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.phuff_entropy_encoder*, align 4
  %temp = alloca i32, align 4
  %blkn = alloca i32, align 4
  %Al = alloca i32, align 4
  %block = alloca [64 x i16]*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 62
  %2 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_encoder* %2 to %struct.phuff_entropy_encoder*
  store %struct.phuff_entropy_encoder* %3, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 53
  %8 = load i32, i32* %Al2, align 8, !tbaa !48
  store i32 %8, i32* %Al, align 4, !tbaa !18
  %9 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 6
  %11 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !49
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %11, i32 0, i32 0
  %12 = load i8*, i8** %next_output_byte, align 4, !tbaa !50
  %13 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte3 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %13, i32 0, i32 2
  store i8* %12, i8** %next_output_byte3, align 4, !tbaa !52
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 6
  %15 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest4, align 8, !tbaa !49
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %15, i32 0, i32 1
  %16 = load i32, i32* %free_in_buffer, align 4, !tbaa !53
  %17 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer5 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %17, i32 0, i32 3
  store i32 %16, i32* %free_in_buffer5, align 4, !tbaa !54
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 30
  %19 = load i32, i32* %restart_interval, align 8, !tbaa !45
  %tobool = icmp ne i32 %19, 0
  br i1 %tobool, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %20 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %20, i32 0, i32 12
  %21 = load i32, i32* %restarts_to_go, align 4, !tbaa !46
  %cmp = icmp eq i32 %21, 0
  br i1 %cmp, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %22 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %23 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %23, i32 0, i32 13
  %24 = load i32, i32* %next_restart_num, align 4, !tbaa !47
  call void @emit_restart(%struct.phuff_entropy_encoder* %22, i32 %24)
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  br label %if.end7

if.end7:                                          ; preds = %if.end, %entry
  store i32 0, i32* %blkn, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end7
  %25 = load i32, i32* %blkn, align 4, !tbaa !18
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 48
  %27 = load i32, i32* %blocks_in_MCU, align 8, !tbaa !55
  %cmp8 = icmp slt i32 %25, %27
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %28 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %29 = load i32, i32* %blkn, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %28, i32 %29
  %30 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  store [64 x i16]* %30, [64 x i16]** %block, align 4, !tbaa !2
  %31 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds [64 x i16], [64 x i16]* %31, i32 0, i32 0
  %32 = load i16, i16* %arrayidx9, align 2, !tbaa !56
  %conv = sext i16 %32 to i32
  store i32 %conv, i32* %temp, align 4, !tbaa !18
  %33 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %34 = load i32, i32* %temp, align 4, !tbaa !18
  %35 = load i32, i32* %Al, align 4, !tbaa !18
  %shr = ashr i32 %34, %35
  call void @emit_bits(%struct.phuff_entropy_encoder* %33, i32 %shr, i32 1)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %36 = load i32, i32* %blkn, align 4, !tbaa !18
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %blkn, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte10 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %37, i32 0, i32 2
  %38 = load i8*, i8** %next_output_byte10, align 4, !tbaa !52
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest11 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %39, i32 0, i32 6
  %40 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest11, align 8, !tbaa !49
  %next_output_byte12 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %40, i32 0, i32 0
  store i8* %38, i8** %next_output_byte12, align 4, !tbaa !50
  %41 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer13 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %41, i32 0, i32 3
  %42 = load i32, i32* %free_in_buffer13, align 4, !tbaa !54
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 6
  %44 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest14, align 8, !tbaa !49
  %free_in_buffer15 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %44, i32 0, i32 1
  store i32 %42, i32* %free_in_buffer15, align 4, !tbaa !53
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval16 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %45, i32 0, i32 30
  %46 = load i32, i32* %restart_interval16, align 8, !tbaa !45
  %tobool17 = icmp ne i32 %46, 0
  br i1 %tobool17, label %if.then18, label %if.end30

if.then18:                                        ; preds = %for.end
  %47 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go19 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %47, i32 0, i32 12
  %48 = load i32, i32* %restarts_to_go19, align 4, !tbaa !46
  %cmp20 = icmp eq i32 %48, 0
  br i1 %cmp20, label %if.then22, label %if.end28

if.then22:                                        ; preds = %if.then18
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval23 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %49, i32 0, i32 30
  %50 = load i32, i32* %restart_interval23, align 8, !tbaa !45
  %51 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go24 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %51, i32 0, i32 12
  store i32 %50, i32* %restarts_to_go24, align 4, !tbaa !46
  %52 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num25 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %52, i32 0, i32 13
  %53 = load i32, i32* %next_restart_num25, align 4, !tbaa !47
  %inc26 = add nsw i32 %53, 1
  store i32 %inc26, i32* %next_restart_num25, align 4, !tbaa !47
  %54 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num27 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %54, i32 0, i32 13
  %55 = load i32, i32* %next_restart_num27, align 4, !tbaa !47
  %and = and i32 %55, 7
  store i32 %and, i32* %next_restart_num27, align 4, !tbaa !47
  br label %if.end28

if.end28:                                         ; preds = %if.then22, %if.then18
  %56 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go29 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %56, i32 0, i32 12
  %57 = load i32, i32* %restarts_to_go29, align 4, !tbaa !46
  %dec = add i32 %57, -1
  store i32 %dec, i32* %restarts_to_go29, align 4, !tbaa !46
  br label %if.end30

if.end30:                                         ; preds = %if.end28, %for.end
  %58 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %61 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %62 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  ret i32 1
}

; Function Attrs: nounwind
define internal i32 @encode_mcu_AC_refine(%struct.jpeg_compress_struct* %cinfo, [64 x i16]** %MCU_data) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %MCU_data.addr = alloca [64 x i16]**, align 4
  %entropy = alloca %struct.phuff_entropy_encoder*, align 4
  %temp = alloca i32, align 4
  %r = alloca i32, align 4
  %k = alloca i32, align 4
  %EOB = alloca i32, align 4
  %BR_buffer = alloca i8*, align 4
  %BR = alloca i32, align 4
  %Se = alloca i32, align 4
  %Al = alloca i32, align 4
  %block = alloca [64 x i16]*, align 4
  %absvalues = alloca [64 x i32], align 16
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store [64 x i16]** %MCU_data, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 62
  %2 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_encoder* %2 to %struct.phuff_entropy_encoder*
  store %struct.phuff_entropy_encoder* %3, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %EOB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %BR_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %BR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Se2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 51
  %12 = load i32, i32* %Se2, align 8, !tbaa !57
  store i32 %12, i32* %Se, align 4, !tbaa !18
  %13 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Al3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 53
  %15 = load i32, i32* %Al3, align 8, !tbaa !48
  store i32 %15, i32* %Al, align 4, !tbaa !18
  %16 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast [64 x i32]* %absvalues to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %17) #4
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 6
  %19 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !49
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %19, i32 0, i32 0
  %20 = load i8*, i8** %next_output_byte, align 4, !tbaa !50
  %21 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte4 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %21, i32 0, i32 2
  store i8* %20, i8** %next_output_byte4, align 4, !tbaa !52
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 6
  %23 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest5, align 8, !tbaa !49
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %23, i32 0, i32 1
  %24 = load i32, i32* %free_in_buffer, align 4, !tbaa !53
  %25 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer6 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %25, i32 0, i32 3
  store i32 %24, i32* %free_in_buffer6, align 4, !tbaa !54
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 30
  %27 = load i32, i32* %restart_interval, align 8, !tbaa !45
  %tobool = icmp ne i32 %27, 0
  br i1 %tobool, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %28 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %28, i32 0, i32 12
  %29 = load i32, i32* %restarts_to_go, align 4, !tbaa !46
  %cmp = icmp eq i32 %29, 0
  br i1 %cmp, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %30 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %31 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %31, i32 0, i32 13
  %32 = load i32, i32* %next_restart_num, align 4, !tbaa !47
  call void @emit_restart(%struct.phuff_entropy_encoder* %30, i32 %32)
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  br label %if.end8

if.end8:                                          ; preds = %if.end, %entry
  %33 = load [64 x i16]**, [64 x i16]*** %MCU_data.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds [64 x i16]*, [64 x i16]** %33, i32 0
  %34 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  store [64 x i16]* %34, [64 x i16]** %block, align 4, !tbaa !2
  store i32 0, i32* %EOB, align 4, !tbaa !18
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %35, i32 0, i32 50
  %36 = load i32, i32* %Ss, align 4, !tbaa !22
  store i32 %36, i32* %k, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %37 = load i32, i32* %k, align 4, !tbaa !18
  %38 = load i32, i32* %Se, align 4, !tbaa !18
  %cmp9 = icmp sle i32 %37, %38
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %39 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %40 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx10 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %40
  %41 = load i32, i32* %arrayidx10, align 4, !tbaa !18
  %arrayidx11 = getelementptr inbounds [64 x i16], [64 x i16]* %39, i32 0, i32 %41
  %42 = load i16, i16* %arrayidx11, align 2, !tbaa !56
  %conv = sext i16 %42 to i32
  store i32 %conv, i32* %temp, align 4, !tbaa !18
  %43 = load i32, i32* %temp, align 4, !tbaa !18
  %cmp12 = icmp slt i32 %43, 0
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %for.body
  %44 = load i32, i32* %temp, align 4, !tbaa !18
  %sub = sub nsw i32 0, %44
  store i32 %sub, i32* %temp, align 4, !tbaa !18
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %for.body
  %45 = load i32, i32* %Al, align 4, !tbaa !18
  %46 = load i32, i32* %temp, align 4, !tbaa !18
  %shr = ashr i32 %46, %45
  store i32 %shr, i32* %temp, align 4, !tbaa !18
  %47 = load i32, i32* %temp, align 4, !tbaa !18
  %48 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx16 = getelementptr inbounds [64 x i32], [64 x i32]* %absvalues, i32 0, i32 %48
  store i32 %47, i32* %arrayidx16, align 4, !tbaa !18
  %49 = load i32, i32* %temp, align 4, !tbaa !18
  %cmp17 = icmp eq i32 %49, 1
  br i1 %cmp17, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end15
  %50 = load i32, i32* %k, align 4, !tbaa !18
  store i32 %50, i32* %EOB, align 4, !tbaa !18
  br label %if.end20

if.end20:                                         ; preds = %if.then19, %if.end15
  br label %for.inc

for.inc:                                          ; preds = %if.end20
  %51 = load i32, i32* %k, align 4, !tbaa !18
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %k, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %r, align 4, !tbaa !18
  store i32 0, i32* %BR, align 4, !tbaa !18
  %52 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %bit_buffer = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %52, i32 0, i32 11
  %53 = load i8*, i8** %bit_buffer, align 4, !tbaa !19
  %54 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %BE = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %54, i32 0, i32 10
  %55 = load i32, i32* %BE, align 4, !tbaa !42
  %add.ptr = getelementptr inbounds i8, i8* %53, i32 %55
  store i8* %add.ptr, i8** %BR_buffer, align 4, !tbaa !2
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss21 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 50
  %57 = load i32, i32* %Ss21, align 4, !tbaa !22
  store i32 %57, i32* %k, align 4, !tbaa !18
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc52, %for.end
  %58 = load i32, i32* %k, align 4, !tbaa !18
  %59 = load i32, i32* %Se, align 4, !tbaa !18
  %cmp23 = icmp sle i32 %58, %59
  br i1 %cmp23, label %for.body25, label %for.end54

for.body25:                                       ; preds = %for.cond22
  %60 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx26 = getelementptr inbounds [64 x i32], [64 x i32]* %absvalues, i32 0, i32 %60
  %61 = load i32, i32* %arrayidx26, align 4, !tbaa !18
  store i32 %61, i32* %temp, align 4, !tbaa !18
  %cmp27 = icmp eq i32 %61, 0
  br i1 %cmp27, label %if.then29, label %if.end31

if.then29:                                        ; preds = %for.body25
  %62 = load i32, i32* %r, align 4, !tbaa !18
  %inc30 = add nsw i32 %62, 1
  store i32 %inc30, i32* %r, align 4, !tbaa !18
  br label %for.inc52

if.end31:                                         ; preds = %for.body25
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end31
  %63 = load i32, i32* %r, align 4, !tbaa !18
  %cmp32 = icmp sgt i32 %63, 15
  br i1 %cmp32, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %64 = load i32, i32* %k, align 4, !tbaa !18
  %65 = load i32, i32* %EOB, align 4, !tbaa !18
  %cmp34 = icmp sle i32 %64, %65
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %66 = phi i1 [ false, %while.cond ], [ %cmp34, %land.rhs ]
  br i1 %66, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %67 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  call void @emit_eobrun(%struct.phuff_entropy_encoder* %67)
  %68 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %69 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %69, i32 0, i32 8
  %70 = load i32, i32* %ac_tbl_no, align 4, !tbaa !30
  call void @emit_symbol(%struct.phuff_entropy_encoder* %68, i32 %70, i32 240)
  %71 = load i32, i32* %r, align 4, !tbaa !18
  %sub36 = sub nsw i32 %71, 16
  store i32 %sub36, i32* %r, align 4, !tbaa !18
  %72 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %73 = load i8*, i8** %BR_buffer, align 4, !tbaa !2
  %74 = load i32, i32* %BR, align 4, !tbaa !18
  call void @emit_buffered_bits(%struct.phuff_entropy_encoder* %72, i8* %73, i32 %74)
  %75 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %bit_buffer37 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %75, i32 0, i32 11
  %76 = load i8*, i8** %bit_buffer37, align 4, !tbaa !19
  store i8* %76, i8** %BR_buffer, align 4, !tbaa !2
  store i32 0, i32* %BR, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %land.end
  %77 = load i32, i32* %temp, align 4, !tbaa !18
  %cmp38 = icmp sgt i32 %77, 1
  br i1 %cmp38, label %if.then40, label %if.end44

if.then40:                                        ; preds = %while.end
  %78 = load i32, i32* %temp, align 4, !tbaa !18
  %and = and i32 %78, 1
  %conv41 = trunc i32 %and to i8
  %79 = load i8*, i8** %BR_buffer, align 4, !tbaa !2
  %80 = load i32, i32* %BR, align 4, !tbaa !18
  %inc42 = add i32 %80, 1
  store i32 %inc42, i32* %BR, align 4, !tbaa !18
  %arrayidx43 = getelementptr inbounds i8, i8* %79, i32 %80
  store i8 %conv41, i8* %arrayidx43, align 1, !tbaa !34
  br label %for.inc52

if.end44:                                         ; preds = %while.end
  %81 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  call void @emit_eobrun(%struct.phuff_entropy_encoder* %81)
  %82 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %83 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %ac_tbl_no45 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %83, i32 0, i32 8
  %84 = load i32, i32* %ac_tbl_no45, align 4, !tbaa !30
  %85 = load i32, i32* %r, align 4, !tbaa !18
  %shl = shl i32 %85, 4
  %add = add nsw i32 %shl, 1
  call void @emit_symbol(%struct.phuff_entropy_encoder* %82, i32 %84, i32 %add)
  %86 = load [64 x i16]*, [64 x i16]** %block, align 4, !tbaa !2
  %87 = load i32, i32* %k, align 4, !tbaa !18
  %arrayidx46 = getelementptr inbounds [0 x i32], [0 x i32]* @jpeg_natural_order, i32 0, i32 %87
  %88 = load i32, i32* %arrayidx46, align 4, !tbaa !18
  %arrayidx47 = getelementptr inbounds [64 x i16], [64 x i16]* %86, i32 0, i32 %88
  %89 = load i16, i16* %arrayidx47, align 2, !tbaa !56
  %conv48 = sext i16 %89 to i32
  %cmp49 = icmp slt i32 %conv48, 0
  %90 = zext i1 %cmp49 to i64
  %cond = select i1 %cmp49, i32 0, i32 1
  store i32 %cond, i32* %temp, align 4, !tbaa !18
  %91 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %92 = load i32, i32* %temp, align 4, !tbaa !18
  call void @emit_bits(%struct.phuff_entropy_encoder* %91, i32 %92, i32 1)
  %93 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %94 = load i8*, i8** %BR_buffer, align 4, !tbaa !2
  %95 = load i32, i32* %BR, align 4, !tbaa !18
  call void @emit_buffered_bits(%struct.phuff_entropy_encoder* %93, i8* %94, i32 %95)
  %96 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %bit_buffer51 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %96, i32 0, i32 11
  %97 = load i8*, i8** %bit_buffer51, align 4, !tbaa !19
  store i8* %97, i8** %BR_buffer, align 4, !tbaa !2
  store i32 0, i32* %BR, align 4, !tbaa !18
  store i32 0, i32* %r, align 4, !tbaa !18
  br label %for.inc52

for.inc52:                                        ; preds = %if.end44, %if.then40, %if.then29
  %98 = load i32, i32* %k, align 4, !tbaa !18
  %inc53 = add nsw i32 %98, 1
  store i32 %inc53, i32* %k, align 4, !tbaa !18
  br label %for.cond22

for.end54:                                        ; preds = %for.cond22
  %99 = load i32, i32* %r, align 4, !tbaa !18
  %cmp55 = icmp sgt i32 %99, 0
  br i1 %cmp55, label %if.then59, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end54
  %100 = load i32, i32* %BR, align 4, !tbaa !18
  %cmp57 = icmp ugt i32 %100, 0
  br i1 %cmp57, label %if.then59, label %if.end72

if.then59:                                        ; preds = %lor.lhs.false, %for.end54
  %101 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %EOBRUN = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %101, i32 0, i32 9
  %102 = load i32, i32* %EOBRUN, align 4, !tbaa !41
  %inc60 = add i32 %102, 1
  store i32 %inc60, i32* %EOBRUN, align 4, !tbaa !41
  %103 = load i32, i32* %BR, align 4, !tbaa !18
  %104 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %BE61 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %104, i32 0, i32 10
  %105 = load i32, i32* %BE61, align 4, !tbaa !42
  %add62 = add i32 %105, %103
  store i32 %add62, i32* %BE61, align 4, !tbaa !42
  %106 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %EOBRUN63 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %106, i32 0, i32 9
  %107 = load i32, i32* %EOBRUN63, align 4, !tbaa !41
  %cmp64 = icmp eq i32 %107, 32767
  br i1 %cmp64, label %if.then70, label %lor.lhs.false66

lor.lhs.false66:                                  ; preds = %if.then59
  %108 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %BE67 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %108, i32 0, i32 10
  %109 = load i32, i32* %BE67, align 4, !tbaa !42
  %cmp68 = icmp ugt i32 %109, 937
  br i1 %cmp68, label %if.then70, label %if.end71

if.then70:                                        ; preds = %lor.lhs.false66, %if.then59
  %110 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  call void @emit_eobrun(%struct.phuff_entropy_encoder* %110)
  br label %if.end71

if.end71:                                         ; preds = %if.then70, %lor.lhs.false66
  br label %if.end72

if.end72:                                         ; preds = %if.end71, %lor.lhs.false
  %111 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte73 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %111, i32 0, i32 2
  %112 = load i8*, i8** %next_output_byte73, align 4, !tbaa !52
  %113 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest74 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %113, i32 0, i32 6
  %114 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest74, align 8, !tbaa !49
  %next_output_byte75 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %114, i32 0, i32 0
  store i8* %112, i8** %next_output_byte75, align 4, !tbaa !50
  %115 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer76 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %115, i32 0, i32 3
  %116 = load i32, i32* %free_in_buffer76, align 4, !tbaa !54
  %117 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest77 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %117, i32 0, i32 6
  %118 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest77, align 8, !tbaa !49
  %free_in_buffer78 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %118, i32 0, i32 1
  store i32 %116, i32* %free_in_buffer78, align 4, !tbaa !53
  %119 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval79 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %119, i32 0, i32 30
  %120 = load i32, i32* %restart_interval79, align 8, !tbaa !45
  %tobool80 = icmp ne i32 %120, 0
  br i1 %tobool80, label %if.then81, label %if.end94

if.then81:                                        ; preds = %if.end72
  %121 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go82 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %121, i32 0, i32 12
  %122 = load i32, i32* %restarts_to_go82, align 4, !tbaa !46
  %cmp83 = icmp eq i32 %122, 0
  br i1 %cmp83, label %if.then85, label %if.end92

if.then85:                                        ; preds = %if.then81
  %123 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %restart_interval86 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %123, i32 0, i32 30
  %124 = load i32, i32* %restart_interval86, align 8, !tbaa !45
  %125 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go87 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %125, i32 0, i32 12
  store i32 %124, i32* %restarts_to_go87, align 4, !tbaa !46
  %126 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num88 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %126, i32 0, i32 13
  %127 = load i32, i32* %next_restart_num88, align 4, !tbaa !47
  %inc89 = add nsw i32 %127, 1
  store i32 %inc89, i32* %next_restart_num88, align 4, !tbaa !47
  %128 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_restart_num90 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %128, i32 0, i32 13
  %129 = load i32, i32* %next_restart_num90, align 4, !tbaa !47
  %and91 = and i32 %129, 7
  store i32 %and91, i32* %next_restart_num90, align 4, !tbaa !47
  br label %if.end92

if.end92:                                         ; preds = %if.then85, %if.then81
  %130 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %restarts_to_go93 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %130, i32 0, i32 12
  %131 = load i32, i32* %restarts_to_go93, align 4, !tbaa !46
  %dec = add i32 %131, -1
  store i32 %dec, i32* %restarts_to_go93, align 4, !tbaa !46
  br label %if.end94

if.end94:                                         ; preds = %if.end92, %if.end72
  %132 = bitcast [64 x i32]* %absvalues to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %132) #4
  %133 = bitcast [64 x i16]** %block to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #4
  %134 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #4
  %135 = bitcast i32* %Se to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  %136 = bitcast i32* %BR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  %137 = bitcast i8** %BR_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #4
  %138 = bitcast i32* %EOB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  ret i32 1
}

; Function Attrs: nounwind
define internal void @finish_pass_gather_phuff(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %entropy = alloca %struct.phuff_entropy_encoder*, align 4
  %is_DC_band = alloca i32, align 4
  %ci = alloca i32, align 4
  %tbl = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %htblptr = alloca %struct.JHUFF_TBL**, align 4
  %did = alloca [4 x i32], align 16
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 62
  %2 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_encoder* %2 to %struct.phuff_entropy_encoder*
  store %struct.phuff_entropy_encoder* %3, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %4 = bitcast i32* %is_DC_band to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %tbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast %struct.JHUFF_TBL*** %htblptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast [4 x i32]* %did to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #4
  %10 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  call void @emit_eobrun(%struct.phuff_entropy_encoder* %10)
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 50
  %12 = load i32, i32* %Ss, align 4, !tbaa !22
  %cmp = icmp eq i32 %12, 0
  %conv = zext i1 %cmp to i32
  store i32 %conv, i32* %is_DC_band, align 4, !tbaa !18
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %did, i32 0, i32 0
  %13 = bitcast i32* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %13, i8 0, i32 16, i1 false)
  store i32 0, i32* %ci, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %ci, align 4, !tbaa !18
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 44
  %16 = load i32, i32* %comps_in_scan, align 4, !tbaa !26
  %cmp2 = icmp slt i32 %14, %16
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 45
  %18 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %18
  %19 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %19, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %20 = load i32, i32* %is_DC_band, align 4, !tbaa !18
  %tobool = icmp ne i32 %20, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ah = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 52
  %22 = load i32, i32* %Ah, align 4, !tbaa !23
  %cmp4 = icmp ne i32 %22, 0
  br i1 %cmp4, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  br label %for.inc

if.end:                                           ; preds = %if.then
  %23 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %23, i32 0, i32 5
  %24 = load i32, i32* %dc_tbl_no, align 4, !tbaa !27
  store i32 %24, i32* %tbl, align 4, !tbaa !18
  br label %if.end7

if.else:                                          ; preds = %for.body
  %25 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %25, i32 0, i32 6
  %26 = load i32, i32* %ac_tbl_no, align 4, !tbaa !29
  store i32 %26, i32* %tbl, align 4, !tbaa !18
  br label %if.end7

if.end7:                                          ; preds = %if.else, %if.end
  %27 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx8 = getelementptr inbounds [4 x i32], [4 x i32]* %did, i32 0, i32 %27
  %28 = load i32, i32* %arrayidx8, align 4, !tbaa !18
  %tobool9 = icmp ne i32 %28, 0
  br i1 %tobool9, label %if.end23, label %if.then10

if.then10:                                        ; preds = %if.end7
  %29 = load i32, i32* %is_DC_band, align 4, !tbaa !18
  %tobool11 = icmp ne i32 %29, 0
  br i1 %tobool11, label %if.then12, label %if.else14

if.then12:                                        ; preds = %if.then10
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 17
  %31 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx13 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %31
  store %struct.JHUFF_TBL** %arrayidx13, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  br label %if.end16

if.else14:                                        ; preds = %if.then10
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 18
  %33 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx15 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %33
  store %struct.JHUFF_TBL** %arrayidx15, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  br label %if.end16

if.end16:                                         ; preds = %if.else14, %if.then12
  %34 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  %35 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %34, align 4, !tbaa !2
  %cmp17 = icmp eq %struct.JHUFF_TBL* %35, null
  br i1 %cmp17, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end16
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %37 = bitcast %struct.jpeg_compress_struct* %36 to %struct.jpeg_common_struct*
  %call = call %struct.JHUFF_TBL* @jpeg_alloc_huff_table(%struct.jpeg_common_struct* %37)
  %38 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  store %struct.JHUFF_TBL* %call, %struct.JHUFF_TBL** %38, align 4, !tbaa !2
  br label %if.end20

if.end20:                                         ; preds = %if.then19, %if.end16
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = load %struct.JHUFF_TBL**, %struct.JHUFF_TBL*** %htblptr, align 4, !tbaa !2
  %41 = load %struct.JHUFF_TBL*, %struct.JHUFF_TBL** %40, align 4, !tbaa !2
  %42 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %count_ptrs = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %42, i32 0, i32 15
  %43 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx21 = getelementptr inbounds [4 x i32*], [4 x i32*]* %count_ptrs, i32 0, i32 %43
  %44 = load i32*, i32** %arrayidx21, align 4, !tbaa !2
  call void @jpeg_gen_optimal_table(%struct.jpeg_compress_struct* %39, %struct.JHUFF_TBL* %41, i32* %44)
  %45 = load i32, i32* %tbl, align 4, !tbaa !18
  %arrayidx22 = getelementptr inbounds [4 x i32], [4 x i32]* %did, i32 0, i32 %45
  store i32 1, i32* %arrayidx22, align 4, !tbaa !18
  br label %if.end23

if.end23:                                         ; preds = %if.end20, %if.end7
  br label %for.inc

for.inc:                                          ; preds = %if.end23, %if.then6
  %46 = load i32, i32* %ci, align 4, !tbaa !18
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %47 = bitcast [4 x i32]* %did to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #4
  %48 = bitcast %struct.JHUFF_TBL*** %htblptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #4
  %49 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %tbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %51 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #4
  %52 = bitcast i32* %is_DC_band to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #4
  %53 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  ret void
}

; Function Attrs: nounwind
define internal void @finish_pass_phuff(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %entropy = alloca %struct.phuff_entropy_encoder*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 62
  %2 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_entropy_encoder* %2 to %struct.phuff_entropy_encoder*
  store %struct.phuff_entropy_encoder* %3, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 6
  %5 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 8, !tbaa !49
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %5, i32 0, i32 0
  %6 = load i8*, i8** %next_output_byte, align 4, !tbaa !50
  %7 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte2 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %7, i32 0, i32 2
  store i8* %6, i8** %next_output_byte2, align 4, !tbaa !52
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 6
  %9 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest3, align 8, !tbaa !49
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %9, i32 0, i32 1
  %10 = load i32, i32* %free_in_buffer, align 4, !tbaa !53
  %11 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer4 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %11, i32 0, i32 3
  store i32 %10, i32* %free_in_buffer4, align 4, !tbaa !54
  %12 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  call void @emit_eobrun(%struct.phuff_entropy_encoder* %12)
  %13 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  call void @flush_bits(%struct.phuff_entropy_encoder* %13)
  %14 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %next_output_byte5 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %14, i32 0, i32 2
  %15 = load i8*, i8** %next_output_byte5, align 4, !tbaa !52
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 6
  %17 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest6, align 8, !tbaa !49
  %next_output_byte7 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %17, i32 0, i32 0
  store i8* %15, i8** %next_output_byte7, align 4, !tbaa !50
  %18 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy, align 4, !tbaa !2
  %free_in_buffer8 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %18, i32 0, i32 3
  %19 = load i32, i32* %free_in_buffer8, align 4, !tbaa !54
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 6
  %21 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest9, align 8, !tbaa !49
  %free_in_buffer10 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %21, i32 0, i32 1
  store i32 %19, i32* %free_in_buffer10, align 4, !tbaa !53
  %22 = bitcast %struct.phuff_entropy_encoder** %entropy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare void @jpeg_make_c_derived_tbl(%struct.jpeg_compress_struct*, i32, i32, %struct.c_derived_tbl**) #3

; Function Attrs: nounwind
define internal void @emit_restart(%struct.phuff_entropy_encoder* %entropy, i32 %restart_num) #0 {
entry:
  %entropy.addr = alloca %struct.phuff_entropy_encoder*, align 4
  %restart_num.addr = alloca i32, align 4
  %ci = alloca i32, align 4
  store %struct.phuff_entropy_encoder* %entropy, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  store i32 %restart_num, i32* %restart_num.addr, align 4, !tbaa !18
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  call void @emit_eobrun(%struct.phuff_entropy_encoder* %1)
  %2 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %gather_statistics = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %2, i32 0, i32 1
  %3 = load i32, i32* %gather_statistics, align 4, !tbaa !21
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.end10, label %if.then

if.then:                                          ; preds = %entry
  %4 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  call void @flush_bits(%struct.phuff_entropy_encoder* %4)
  %5 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %next_output_byte = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %5, i32 0, i32 2
  %6 = load i8*, i8** %next_output_byte, align 4, !tbaa !52
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %next_output_byte, align 4, !tbaa !52
  store i8 -1, i8* %6, align 1, !tbaa !34
  %7 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %free_in_buffer = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %7, i32 0, i32 3
  %8 = load i32, i32* %free_in_buffer, align 4, !tbaa !54
  %dec = add i32 %8, -1
  store i32 %dec, i32* %free_in_buffer, align 4, !tbaa !54
  %cmp = icmp eq i32 %dec, 0
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %9 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  call void @dump_buffer(%struct.phuff_entropy_encoder* %9)
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %10 = load i32, i32* %restart_num.addr, align 4, !tbaa !18
  %add = add nsw i32 208, %10
  %conv = trunc i32 %add to i8
  %11 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %next_output_byte2 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %11, i32 0, i32 2
  %12 = load i8*, i8** %next_output_byte2, align 4, !tbaa !52
  %incdec.ptr3 = getelementptr inbounds i8, i8* %12, i32 1
  store i8* %incdec.ptr3, i8** %next_output_byte2, align 4, !tbaa !52
  store i8 %conv, i8* %12, align 1, !tbaa !34
  %13 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %free_in_buffer4 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %13, i32 0, i32 3
  %14 = load i32, i32* %free_in_buffer4, align 4, !tbaa !54
  %dec5 = add i32 %14, -1
  store i32 %dec5, i32* %free_in_buffer4, align 4, !tbaa !54
  %cmp6 = icmp eq i32 %dec5, 0
  br i1 %cmp6, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  %15 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  call void @dump_buffer(%struct.phuff_entropy_encoder* %15)
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %if.end
  br label %if.end10

if.end10:                                         ; preds = %if.end9, %entry
  %16 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %16, i32 0, i32 6
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo, align 4, !tbaa !20
  %Ss = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 50
  %18 = load i32, i32* %Ss, align 4, !tbaa !22
  %cmp11 = icmp eq i32 %18, 0
  br i1 %cmp11, label %if.then13, label %if.else

if.then13:                                        ; preds = %if.end10
  store i32 0, i32* %ci, align 4, !tbaa !18
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then13
  %19 = load i32, i32* %ci, align 4, !tbaa !18
  %20 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo14 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %20, i32 0, i32 6
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo14, align 4, !tbaa !20
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 44
  %22 = load i32, i32* %comps_in_scan, align 4, !tbaa !26
  %cmp15 = icmp slt i32 %19, %22
  br i1 %cmp15, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %last_dc_val = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %23, i32 0, i32 7
  %24 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %last_dc_val, i32 0, i32 %24
  store i32 0, i32* %arrayidx, align 4, !tbaa !18
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %ci, align 4, !tbaa !18
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !18
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end17

if.else:                                          ; preds = %if.end10
  %26 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %EOBRUN = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %26, i32 0, i32 9
  store i32 0, i32* %EOBRUN, align 4, !tbaa !41
  %27 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %BE = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %27, i32 0, i32 10
  store i32 0, i32* %BE, align 4, !tbaa !42
  br label %if.end17

if.end17:                                         ; preds = %if.else, %for.end
  %28 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #4
  ret void
}

; Function Attrs: nounwind
define internal void @emit_symbol(%struct.phuff_entropy_encoder* %entropy, i32 %tbl_no, i32 %symbol) #0 {
entry:
  %entropy.addr = alloca %struct.phuff_entropy_encoder*, align 4
  %tbl_no.addr = alloca i32, align 4
  %symbol.addr = alloca i32, align 4
  %tbl = alloca %struct.c_derived_tbl*, align 4
  store %struct.phuff_entropy_encoder* %entropy, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  store i32 %tbl_no, i32* %tbl_no.addr, align 4, !tbaa !18
  store i32 %symbol, i32* %symbol.addr, align 4, !tbaa !18
  %0 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %gather_statistics = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %0, i32 0, i32 1
  %1 = load i32, i32* %gather_statistics, align 4, !tbaa !21
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %count_ptrs = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %2, i32 0, i32 15
  %3 = load i32, i32* %tbl_no.addr, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [4 x i32*], [4 x i32*]* %count_ptrs, i32 0, i32 %3
  %4 = load i32*, i32** %arrayidx, align 4, !tbaa !2
  %5 = load i32, i32* %symbol.addr, align 4, !tbaa !18
  %arrayidx1 = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx1, align 4, !tbaa !40
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %arrayidx1, align 4, !tbaa !40
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast %struct.c_derived_tbl** %tbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %derived_tbls = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %8, i32 0, i32 14
  %9 = load i32, i32* %tbl_no.addr, align 4, !tbaa !18
  %arrayidx2 = getelementptr inbounds [4 x %struct.c_derived_tbl*], [4 x %struct.c_derived_tbl*]* %derived_tbls, i32 0, i32 %9
  %10 = load %struct.c_derived_tbl*, %struct.c_derived_tbl** %arrayidx2, align 4, !tbaa !2
  store %struct.c_derived_tbl* %10, %struct.c_derived_tbl** %tbl, align 4, !tbaa !2
  %11 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %12 = load %struct.c_derived_tbl*, %struct.c_derived_tbl** %tbl, align 4, !tbaa !2
  %ehufco = getelementptr inbounds %struct.c_derived_tbl, %struct.c_derived_tbl* %12, i32 0, i32 0
  %13 = load i32, i32* %symbol.addr, align 4, !tbaa !18
  %arrayidx3 = getelementptr inbounds [256 x i32], [256 x i32]* %ehufco, i32 0, i32 %13
  %14 = load i32, i32* %arrayidx3, align 4, !tbaa !18
  %15 = load %struct.c_derived_tbl*, %struct.c_derived_tbl** %tbl, align 4, !tbaa !2
  %ehufsi = getelementptr inbounds %struct.c_derived_tbl, %struct.c_derived_tbl* %15, i32 0, i32 1
  %16 = load i32, i32* %symbol.addr, align 4, !tbaa !18
  %arrayidx4 = getelementptr inbounds [256 x i8], [256 x i8]* %ehufsi, i32 0, i32 %16
  %17 = load i8, i8* %arrayidx4, align 1, !tbaa !34
  %conv = sext i8 %17 to i32
  call void @emit_bits(%struct.phuff_entropy_encoder* %11, i32 %14, i32 %conv)
  %18 = bitcast %struct.c_derived_tbl** %tbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @emit_bits(%struct.phuff_entropy_encoder* %entropy, i32 %code, i32 %size) #0 {
entry:
  %entropy.addr = alloca %struct.phuff_entropy_encoder*, align 4
  %code.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  %put_buffer = alloca i32, align 4
  %put_bits = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %c = alloca i32, align 4
  store %struct.phuff_entropy_encoder* %entropy, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  store i32 %code, i32* %code.addr, align 4, !tbaa !18
  store i32 %size, i32* %size.addr, align 4, !tbaa !18
  %0 = bitcast i32* %put_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load i32, i32* %code.addr, align 4, !tbaa !18
  store i32 %1, i32* %put_buffer, align 4, !tbaa !40
  %2 = bitcast i32* %put_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %put_bits1 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %3, i32 0, i32 5
  %4 = load i32, i32* %put_bits1, align 4, !tbaa !44
  store i32 %4, i32* %put_bits, align 4, !tbaa !18
  %5 = load i32, i32* %size.addr, align 4, !tbaa !18
  %cmp = icmp eq i32 %5, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %6, i32 0, i32 6
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo, align 4, !tbaa !20
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !31
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 5
  store i32 40, i32* %msg_code, align 4, !tbaa !32
  %9 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo2 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %9, i32 0, i32 6
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo2, align 4, !tbaa !20
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !31
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !35
  %13 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo4 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %13, i32 0, i32 6
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo4, align 4, !tbaa !20
  %15 = bitcast %struct.jpeg_compress_struct* %14 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %15)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %16 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %gather_statistics = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %16, i32 0, i32 1
  %17 = load i32, i32* %gather_statistics, align 4, !tbaa !21
  %tobool = icmp ne i32 %17, 0
  br i1 %tobool, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end6:                                          ; preds = %if.end
  %18 = load i32, i32* %size.addr, align 4, !tbaa !18
  %shl = shl i32 1, %18
  %sub = sub i32 %shl, 1
  %19 = load i32, i32* %put_buffer, align 4, !tbaa !40
  %and = and i32 %19, %sub
  store i32 %and, i32* %put_buffer, align 4, !tbaa !40
  %20 = load i32, i32* %size.addr, align 4, !tbaa !18
  %21 = load i32, i32* %put_bits, align 4, !tbaa !18
  %add = add nsw i32 %21, %20
  store i32 %add, i32* %put_bits, align 4, !tbaa !18
  %22 = load i32, i32* %put_bits, align 4, !tbaa !18
  %sub7 = sub nsw i32 24, %22
  %23 = load i32, i32* %put_buffer, align 4, !tbaa !40
  %shl8 = shl i32 %23, %sub7
  store i32 %shl8, i32* %put_buffer, align 4, !tbaa !40
  %24 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %put_buffer9 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %24, i32 0, i32 4
  %25 = load i32, i32* %put_buffer9, align 4, !tbaa !43
  %26 = load i32, i32* %put_buffer, align 4, !tbaa !40
  %or = or i32 %26, %25
  store i32 %or, i32* %put_buffer, align 4, !tbaa !40
  br label %while.cond

while.cond:                                       ; preds = %if.end27, %if.end6
  %27 = load i32, i32* %put_bits, align 4, !tbaa !18
  %cmp10 = icmp sge i32 %27, 8
  br i1 %cmp10, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %28 = bitcast i32* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #4
  %29 = load i32, i32* %put_buffer, align 4, !tbaa !40
  %shr = lshr i32 %29, 16
  %and11 = and i32 %shr, 255
  store i32 %and11, i32* %c, align 4, !tbaa !18
  %30 = load i32, i32* %c, align 4, !tbaa !18
  %conv = trunc i32 %30 to i8
  %31 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %next_output_byte = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %31, i32 0, i32 2
  %32 = load i8*, i8** %next_output_byte, align 4, !tbaa !52
  %incdec.ptr = getelementptr inbounds i8, i8* %32, i32 1
  store i8* %incdec.ptr, i8** %next_output_byte, align 4, !tbaa !52
  store i8 %conv, i8* %32, align 1, !tbaa !34
  %33 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %free_in_buffer = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %33, i32 0, i32 3
  %34 = load i32, i32* %free_in_buffer, align 4, !tbaa !54
  %dec = add i32 %34, -1
  store i32 %dec, i32* %free_in_buffer, align 4, !tbaa !54
  %cmp12 = icmp eq i32 %dec, 0
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %while.body
  %35 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  call void @dump_buffer(%struct.phuff_entropy_encoder* %35)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %while.body
  %36 = load i32, i32* %c, align 4, !tbaa !18
  %cmp16 = icmp eq i32 %36, 255
  br i1 %cmp16, label %if.then18, label %if.end27

if.then18:                                        ; preds = %if.end15
  %37 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %next_output_byte19 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %37, i32 0, i32 2
  %38 = load i8*, i8** %next_output_byte19, align 4, !tbaa !52
  %incdec.ptr20 = getelementptr inbounds i8, i8* %38, i32 1
  store i8* %incdec.ptr20, i8** %next_output_byte19, align 4, !tbaa !52
  store i8 0, i8* %38, align 1, !tbaa !34
  %39 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %free_in_buffer21 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %39, i32 0, i32 3
  %40 = load i32, i32* %free_in_buffer21, align 4, !tbaa !54
  %dec22 = add i32 %40, -1
  store i32 %dec22, i32* %free_in_buffer21, align 4, !tbaa !54
  %cmp23 = icmp eq i32 %dec22, 0
  br i1 %cmp23, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.then18
  %41 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  call void @dump_buffer(%struct.phuff_entropy_encoder* %41)
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %if.then18
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end15
  %42 = load i32, i32* %put_buffer, align 4, !tbaa !40
  %shl28 = shl i32 %42, 8
  store i32 %shl28, i32* %put_buffer, align 4, !tbaa !40
  %43 = load i32, i32* %put_bits, align 4, !tbaa !18
  %sub29 = sub nsw i32 %43, 8
  store i32 %sub29, i32* %put_bits, align 4, !tbaa !18
  %44 = bitcast i32* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = load i32, i32* %put_buffer, align 4, !tbaa !40
  %46 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %put_buffer30 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %46, i32 0, i32 4
  store i32 %45, i32* %put_buffer30, align 4, !tbaa !43
  %47 = load i32, i32* %put_bits, align 4, !tbaa !18
  %48 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %put_bits31 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %48, i32 0, i32 5
  store i32 %47, i32* %put_bits31, align 4, !tbaa !44
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then5
  %49 = bitcast i32* %put_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #4
  %50 = bitcast i32* %put_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #4
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @emit_eobrun(%struct.phuff_entropy_encoder* %entropy) #0 {
entry:
  %entropy.addr = alloca %struct.phuff_entropy_encoder*, align 4
  %temp = alloca i32, align 4
  %nbits = alloca i32, align 4
  store %struct.phuff_entropy_encoder* %entropy, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %0 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %EOBRUN = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %2, i32 0, i32 9
  %3 = load i32, i32* %EOBRUN, align 4, !tbaa !41
  %cmp = icmp ugt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end13

if.then:                                          ; preds = %entry
  %4 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %EOBRUN1 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %4, i32 0, i32 9
  %5 = load i32, i32* %EOBRUN1, align 4, !tbaa !41
  store i32 %5, i32* %temp, align 4, !tbaa !18
  store i32 0, i32* %nbits, align 4, !tbaa !18
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %6 = load i32, i32* %temp, align 4, !tbaa !18
  %shr = ashr i32 %6, 1
  store i32 %shr, i32* %temp, align 4, !tbaa !18
  %tobool = icmp ne i32 %shr, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i32, i32* %nbits, align 4, !tbaa !18
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %nbits, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %8 = load i32, i32* %nbits, align 4, !tbaa !18
  %cmp2 = icmp sgt i32 %8, 14
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %while.end
  %9 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %9, i32 0, i32 6
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo, align 4, !tbaa !20
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !31
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 5
  store i32 40, i32* %msg_code, align 4, !tbaa !32
  %12 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo4 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %12, i32 0, i32 6
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo4, align 4, !tbaa !20
  %err5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !31
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 0
  %15 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !35
  %16 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo6 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %16, i32 0, i32 6
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo6, align 4, !tbaa !20
  %18 = bitcast %struct.jpeg_compress_struct* %17 to %struct.jpeg_common_struct*
  call void %15(%struct.jpeg_common_struct* %18)
  br label %if.end

if.end:                                           ; preds = %if.then3, %while.end
  %19 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %20 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %20, i32 0, i32 8
  %21 = load i32, i32* %ac_tbl_no, align 4, !tbaa !30
  %22 = load i32, i32* %nbits, align 4, !tbaa !18
  %shl = shl i32 %22, 4
  call void @emit_symbol(%struct.phuff_entropy_encoder* %19, i32 %21, i32 %shl)
  %23 = load i32, i32* %nbits, align 4, !tbaa !18
  %tobool7 = icmp ne i32 %23, 0
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end
  %24 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %25 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %EOBRUN9 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %25, i32 0, i32 9
  %26 = load i32, i32* %EOBRUN9, align 4, !tbaa !41
  %27 = load i32, i32* %nbits, align 4, !tbaa !18
  call void @emit_bits(%struct.phuff_entropy_encoder* %24, i32 %26, i32 %27)
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.end
  %28 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %EOBRUN11 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %28, i32 0, i32 9
  store i32 0, i32* %EOBRUN11, align 4, !tbaa !41
  %29 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %30 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %bit_buffer = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %30, i32 0, i32 11
  %31 = load i8*, i8** %bit_buffer, align 4, !tbaa !19
  %32 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %BE = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %32, i32 0, i32 10
  %33 = load i32, i32* %BE, align 4, !tbaa !42
  call void @emit_buffered_bits(%struct.phuff_entropy_encoder* %29, i8* %31, i32 %33)
  %34 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %BE12 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %34, i32 0, i32 10
  store i32 0, i32* %BE12, align 4, !tbaa !42
  br label %if.end13

if.end13:                                         ; preds = %if.end10, %entry
  %35 = bitcast i32* %nbits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #4
  ret void
}

; Function Attrs: nounwind
define internal void @flush_bits(%struct.phuff_entropy_encoder* %entropy) #0 {
entry:
  %entropy.addr = alloca %struct.phuff_entropy_encoder*, align 4
  store %struct.phuff_entropy_encoder* %entropy, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %0 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  call void @emit_bits(%struct.phuff_entropy_encoder* %0, i32 127, i32 7)
  %1 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %put_buffer = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %1, i32 0, i32 4
  store i32 0, i32* %put_buffer, align 4, !tbaa !43
  %2 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %put_bits = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %2, i32 0, i32 5
  store i32 0, i32* %put_bits, align 4, !tbaa !44
  ret void
}

; Function Attrs: nounwind
define internal void @dump_buffer(%struct.phuff_entropy_encoder* %entropy) #0 {
entry:
  %entropy.addr = alloca %struct.phuff_entropy_encoder*, align 4
  %dest = alloca %struct.jpeg_destination_mgr*, align 4
  store %struct.phuff_entropy_encoder* %entropy, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %0 = bitcast %struct.jpeg_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %1, i32 0, i32 6
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo, align 4, !tbaa !20
  %dest1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 6
  %3 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest1, align 8, !tbaa !49
  store %struct.jpeg_destination_mgr* %3, %struct.jpeg_destination_mgr** %dest, align 4, !tbaa !2
  %4 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 4, !tbaa !2
  %empty_output_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %4, i32 0, i32 3
  %5 = load i32 (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)** %empty_output_buffer, align 4, !tbaa !58
  %6 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo2 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %6, i32 0, i32 6
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo2, align 4, !tbaa !20
  %call = call i32 %5(%struct.jpeg_compress_struct* %7)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %8 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo3 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %8, i32 0, i32 6
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo3, align 4, !tbaa !20
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !31
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 5
  store i32 24, i32* %msg_code, align 4, !tbaa !32
  %11 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo4 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %11, i32 0, i32 6
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo4, align 4, !tbaa !20
  %err5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !31
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 0
  %14 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !35
  %15 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %cinfo6 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %15, i32 0, i32 6
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo6, align 4, !tbaa !20
  %17 = bitcast %struct.jpeg_compress_struct* %16 to %struct.jpeg_common_struct*
  call void %14(%struct.jpeg_common_struct* %17)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %18 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 4, !tbaa !2
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %18, i32 0, i32 0
  %19 = load i8*, i8** %next_output_byte, align 4, !tbaa !50
  %20 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %next_output_byte7 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %20, i32 0, i32 2
  store i8* %19, i8** %next_output_byte7, align 4, !tbaa !52
  %21 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest, align 4, !tbaa !2
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %21, i32 0, i32 1
  %22 = load i32, i32* %free_in_buffer, align 4, !tbaa !53
  %23 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %free_in_buffer8 = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %23, i32 0, i32 3
  store i32 %22, i32* %free_in_buffer8, align 4, !tbaa !54
  %24 = bitcast %struct.jpeg_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  ret void
}

; Function Attrs: nounwind
define internal void @emit_buffered_bits(%struct.phuff_entropy_encoder* %entropy, i8* %bufstart, i32 %nbits) #0 {
entry:
  %entropy.addr = alloca %struct.phuff_entropy_encoder*, align 4
  %bufstart.addr = alloca i8*, align 4
  %nbits.addr = alloca i32, align 4
  store %struct.phuff_entropy_encoder* %entropy, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  store i8* %bufstart, i8** %bufstart.addr, align 4, !tbaa !2
  store i32 %nbits, i32* %nbits.addr, align 4, !tbaa !18
  %0 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %gather_statistics = getelementptr inbounds %struct.phuff_entropy_encoder, %struct.phuff_entropy_encoder* %0, i32 0, i32 1
  %1 = load i32, i32* %gather_statistics, align 4, !tbaa !21
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %while.end

if.end:                                           ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %2 = load i32, i32* %nbits.addr, align 4, !tbaa !18
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load %struct.phuff_entropy_encoder*, %struct.phuff_entropy_encoder** %entropy.addr, align 4, !tbaa !2
  %4 = load i8*, i8** %bufstart.addr, align 4, !tbaa !2
  %5 = load i8, i8* %4, align 1, !tbaa !34
  %conv = sext i8 %5 to i32
  call void @emit_bits(%struct.phuff_entropy_encoder* %3, i32 %conv, i32 1)
  %6 = load i8*, i8** %bufstart.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %bufstart.addr, align 4, !tbaa !2
  %7 = load i32, i32* %nbits.addr, align 4, !tbaa !18
  %dec = add i32 %7, -1
  store i32 %dec, i32* %nbits.addr, align 4, !tbaa !18
  br label %while.cond

while.end:                                        ; preds = %if.then, %while.cond
  ret void
}

declare %struct.JHUFF_TBL* @jpeg_alloc_huff_table(%struct.jpeg_common_struct*) #3

declare void @jpeg_gen_optimal_table(%struct.jpeg_compress_struct*, %struct.JHUFF_TBL*, i32*) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !9, i64 48, !8, i64 56, !8, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !8, i64 168, !3, i64 172, !8, i64 176, !8, i64 180, !8, i64 184, !8, i64 188, !8, i64 192, !4, i64 196, !8, i64 200, !8, i64 204, !8, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !10, i64 216, !10, i64 218, !8, i64 220, !8, i64 224, !8, i64 228, !8, i64 232, !8, i64 236, !8, i64 240, !8, i64 244, !4, i64 248, !8, i64 264, !8, i64 268, !8, i64 272, !4, i64 276, !8, i64 316, !8, i64 320, !8, i64 324, !8, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !8, i64 372}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 364}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !8, i64 12, !3, i64 16, !13, i64 20, !13, i64 24, !8, i64 28, !3, i64 32, !4, i64 36, !8, i64 52, !8, i64 56, !8, i64 60, !3, i64 64, !8, i64 68, !8, i64 72, !4, i64 76, !4, i64 92}
!17 = !{!"jpeg_entropy_encoder", !3, i64 0, !3, i64 4, !3, i64 8}
!18 = !{!8, !8, i64 0}
!19 = !{!16, !3, i64 64}
!20 = !{!16, !3, i64 32}
!21 = !{!16, !8, i64 12}
!22 = !{!7, !8, i64 316}
!23 = !{!7, !8, i64 324}
!24 = !{!16, !3, i64 4}
!25 = !{!16, !3, i64 8}
!26 = !{!7, !8, i64 244}
!27 = !{!28, !8, i64 20}
!28 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!29 = !{!28, !8, i64 24}
!30 = !{!16, !8, i64 52}
!31 = !{!7, !3, i64 0}
!32 = !{!33, !8, i64 20}
!33 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!34 = !{!4, !4, i64 0}
!35 = !{!33, !3, i64 0}
!36 = !{!7, !3, i64 332}
!37 = !{!38, !8, i64 44}
!38 = !{!"jpeg_comp_master", !3, i64 0, !3, i64 4, !3, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !4, i64 56, !4, i64 2104, !8, i64 4152, !8, i64 4156, !8, i64 4160, !8, i64 4164, !8, i64 4168, !8, i64 4172, !8, i64 4176, !8, i64 4180, !8, i64 4184, !8, i64 4188, !8, i64 4192, !39, i64 4196, !39, i64 4200, !39, i64 4204}
!39 = !{!"float", !4, i64 0}
!40 = !{!13, !13, i64 0}
!41 = !{!16, !8, i64 56}
!42 = !{!16, !8, i64 60}
!43 = !{!16, !13, i64 24}
!44 = !{!16, !8, i64 28}
!45 = !{!7, !8, i64 200}
!46 = !{!16, !8, i64 68}
!47 = !{!16, !8, i64 72}
!48 = !{!7, !8, i64 328}
!49 = !{!7, !3, i64 24}
!50 = !{!51, !3, i64 0}
!51 = !{!"jpeg_destination_mgr", !3, i64 0, !13, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!52 = !{!16, !3, i64 16}
!53 = !{!51, !13, i64 4}
!54 = !{!16, !13, i64 20}
!55 = !{!7, !8, i64 272}
!56 = !{!10, !10, i64 0}
!57 = !{!7, !8, i64 320}
!58 = !{!51, !3, i64 12}
