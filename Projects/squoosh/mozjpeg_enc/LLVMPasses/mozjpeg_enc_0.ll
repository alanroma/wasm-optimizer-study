; ModuleID = 'mozjpeg_enc.cpp'
source_filename = "mozjpeg_enc.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.emscripten::val" = type { %"struct.emscripten::internal::_EM_VAL"* }
%"struct.emscripten::internal::_EM_VAL" = type opaque
%struct.EmscriptenBindingInitializer_my_module = type { i8 }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%struct.MozJpegOptions = type { i32, i8, i8, i8, i8, i32, i32, i32, i8, i8, i8, i32, i8, i32, i8, i32 }
%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon.1, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon.1 = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type opaque
%struct.jpeg_c_main_controller = type opaque
%struct.jpeg_c_prep_controller = type opaque
%struct.jpeg_c_coef_controller = type opaque
%struct.jpeg_marker_writer = type opaque
%struct.jpeg_color_converter = type opaque
%struct.jpeg_downsampler = type opaque
%struct.jpeg_forward_dct = type opaque
%struct.jpeg_entropy_encoder = type opaque
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%"struct.emscripten::memory_view" = type { i32, i8* }
%"class.emscripten::value_object" = type { i8 }
%"class.emscripten::internal::noncopyable" = type { i8 }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3" = type { i8 }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4" = type { i8 }
%struct.anon.5 = type { i32, [1 x i8] }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw" = type { [3 x i32] }
%"struct.emscripten::internal::WithPolicies<>::ArgTypeList" = type { i8 }
%"struct.emscripten::internal::WireTypePack" = type { %"struct.std::__2::array" }
%"struct.std::__2::array" = type { [1 x %"union.emscripten::internal::GenericWireType"] }
%"union.emscripten::internal::GenericWireType" = type { double }
%union.anon.2 = type { i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator" = type { i8 }

$_ZN10emscripten3val6globalEPKc = comdat any

$_ZN10emscripten3valD2Ev = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv = comdat any

$_ZNSt3__2plIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12basic_stringIT_T0_T1_EEPKS6_OS9_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEpLERKS5_ = comdat any

$_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_ = comdat any

$_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_ = comdat any

$_ZN10emscripten12value_objectI14MozJpegOptionsEC2EPKc = comdat any

$_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_ = comdat any

$_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_ = comdat any

$_ZN10emscripten12value_objectI14MozJpegOptionsED2Ev = comdat any

$_ZN10emscripten8functionIiJEJEEEvPKcPFT_DpT0_EDpT1_ = comdat any

$_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEii14MozJpegOptionsEJEEEvPKcPFT_DpT0_EDpT1_ = comdat any

$_ZN10emscripten3valC2EPNS_8internal7_EM_VALE = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNSt3__24moveIRNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EOS5_ = comdat any

$_ZNSt3__24moveIRNS_17__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES6_EEEEONS_16remove_referenceIT_E4typeEOSC_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendERKS5_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv = comdat any

$_ZN10emscripten11memory_viewIhEC2EmPKh = comdat any

$_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_ = comdat any

$_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_ = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv = comdat any

$_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv = comdat any

$_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_ = comdat any

$_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE = comdat any

$_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_ = comdat any

$_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv = comdat any

$_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv = comdat any

$_ZN10emscripten8internal11noncopyableC2Ev = comdat any

$_ZN10emscripten8internal15raw_constructorI14MozJpegOptionsJEEEPT_DpNS0_11BindingTypeIT0_vE8WireTypeE = comdat any

$_ZN10emscripten8internal14raw_destructorI14MozJpegOptionsEEvPT_ = comdat any

$_ZN10emscripten8internal6TypeIDI14MozJpegOptionsvE3getEv = comdat any

$_ZN10emscripten8internal12getSignatureIP14MozJpegOptionsJEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal12getSignatureIvJP14MozJpegOptionsEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11LightTypeIDI14MozJpegOptionsE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJP14MozJpegOptionsEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiEEEPKcv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJvP14MozJpegOptionsEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJviEEEPKcv = comdat any

$_ZN10emscripten8internal11noncopyableD2Ev = comdat any

$_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsiE7getWireIS2_EEiRKMS2_iRKT_ = comdat any

$_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsiE7setWireIS2_EEvRKMS2_iRT_i = comdat any

$_ZN10emscripten8internal6TypeIDIivE3getEv = comdat any

$_ZN10emscripten8internal12getSignatureIiJRKM14MozJpegOptionsiRKS2_EEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal10getContextIM14MozJpegOptionsiEEPT_RKS4_ = comdat any

$_ZN10emscripten8internal12getSignatureIvJRKM14MozJpegOptionsiRS2_iEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi = comdat any

$_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi = comdat any

$_ZN10emscripten8internal11LightTypeIDIiE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJiRKM14MozJpegOptionsiRKS2_EEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJvRKM14MozJpegOptionsiRS2_iEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcv = comdat any

$_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsbE7getWireIS2_EEbRKMS2_bRKT_ = comdat any

$_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsbE7setWireIS2_EEvRKMS2_bRT_b = comdat any

$_ZN10emscripten8internal6TypeIDIbvE3getEv = comdat any

$_ZN10emscripten8internal12getSignatureIbJRKM14MozJpegOptionsbRKS2_EEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal10getContextIM14MozJpegOptionsbEEPT_RKS4_ = comdat any

$_ZN10emscripten8internal12getSignatureIvJRKM14MozJpegOptionsbRS2_bEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11BindingTypeIbvE10toWireTypeEb = comdat any

$_ZN10emscripten8internal11BindingTypeIbvE12fromWireTypeEb = comdat any

$_ZN10emscripten8internal11LightTypeIDIbE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJbRKM14MozJpegOptionsbRKS2_EEEPKcv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJvRKM14MozJpegOptionsbRS2_bEEEPKcv = comdat any

$_ZN10emscripten8internal7InvokerIiJEE6invokeEPFivE = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getTypesEv = comdat any

$_ZN10emscripten8internal12getSignatureIiJPFivEEEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJiPFivEEEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiEEEPKcv = comdat any

$_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEii14MozJpegOptionsEE6invokeEPFS2_S9_iiSA_EPNS0_11BindingTypeIS9_vEUt_EiiPSA_ = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEE8getCountEv = comdat any

$_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEE8getTypesEv = comdat any

$_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcPFT_DpT0_E = comdat any

$_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_ = comdat any

$_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E = comdat any

$_ZN10emscripten8internal18GenericBindingTypeI14MozJpegOptionsE12fromWireTypeEPS2_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii14MozJpegOptionsEEEE3getEv = comdat any

$_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcv = comdat any

$_ZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcv = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types = comdat any

$_ZTSN10emscripten11memory_viewIhEE = comdat any

$_ZTIN10emscripten11memory_viewIhEE = comdat any

$_ZTS14MozJpegOptions = comdat any

$_ZTI14MozJpegOptions = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJviEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEvE5types = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiEEEPKcvE9signature = comdat any

$_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii14MozJpegOptionsEEEE3getEvE5types = comdat any

$_ZTSN10emscripten3valE = comdat any

$_ZTIN10emscripten3valE = comdat any

$_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTSNSt3__221__basic_string_commonILb1EEE = comdat any

$_ZTINSt3__221__basic_string_commonILb1EEE = comdat any

$_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcvE9signature = comdat any

@__const._Z7versionv.buffer = private unnamed_addr constant [6 x i8] c"3.3.1\00", align 1
@_ZL10Uint8Array = internal thread_local global %"class.emscripten::val" zeroinitializer, align 4
@.str = private unnamed_addr constant [11 x i8] c"Uint8Array\00", align 1
@__dso_handle = external hidden global i8
@.str.1 = private unnamed_addr constant [2 x i8] c",\00", align 1
@_ZL47EmscriptenBindingInitializer_my_module_instance = internal global %struct.EmscriptenBindingInitializer_my_module zeroinitializer, align 1
@.str.3 = private unnamed_addr constant [15 x i8] c"MozJpegOptions\00", align 1
@.str.4 = private unnamed_addr constant [8 x i8] c"quality\00", align 1
@.str.5 = private unnamed_addr constant [9 x i8] c"baseline\00", align 1
@.str.6 = private unnamed_addr constant [11 x i8] c"arithmetic\00", align 1
@.str.7 = private unnamed_addr constant [12 x i8] c"progressive\00", align 1
@.str.8 = private unnamed_addr constant [16 x i8] c"optimize_coding\00", align 1
@.str.9 = private unnamed_addr constant [10 x i8] c"smoothing\00", align 1
@.str.10 = private unnamed_addr constant [12 x i8] c"color_space\00", align 1
@.str.11 = private unnamed_addr constant [12 x i8] c"quant_table\00", align 1
@.str.12 = private unnamed_addr constant [18 x i8] c"trellis_multipass\00", align 1
@.str.13 = private unnamed_addr constant [17 x i8] c"trellis_opt_zero\00", align 1
@.str.14 = private unnamed_addr constant [18 x i8] c"trellis_opt_table\00", align 1
@.str.15 = private unnamed_addr constant [14 x i8] c"trellis_loops\00", align 1
@.str.16 = private unnamed_addr constant [17 x i8] c"chroma_subsample\00", align 1
@.str.17 = private unnamed_addr constant [15 x i8] c"auto_subsample\00", align 1
@.str.18 = private unnamed_addr constant [24 x i8] c"separate_chroma_quality\00", align 1
@.str.19 = private unnamed_addr constant [15 x i8] c"chroma_quality\00", align 1
@.str.20 = private unnamed_addr constant [8 x i8] c"version\00", align 1
@.str.21 = private unnamed_addr constant [7 x i8] c"encode\00", align 1
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types = linkonce_odr hidden constant [1 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten11memory_viewIhEE to i8*)], comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN10emscripten11memory_viewIhEE = linkonce_odr hidden constant [31 x i8] c"N10emscripten11memory_viewIhEE\00", comdat, align 1
@_ZTIN10emscripten11memory_viewIhEE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([31 x i8], [31 x i8]* @_ZTSN10emscripten11memory_viewIhEE, i32 0, i32 0) }, comdat, align 4
@_ZTS14MozJpegOptions = linkonce_odr hidden constant [17 x i8] c"14MozJpegOptions\00", comdat, align 1
@_ZTI14MozJpegOptions = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @_ZTS14MozJpegOptions, i32 0, i32 0) }, comdat, align 4
@_ZZN10emscripten8internal19getGenericSignatureIJiEEEPKcvE9signature = linkonce_odr hidden constant [2 x i8] c"i\00", comdat, align 1
@_ZZN10emscripten8internal19getGenericSignatureIJviEEEPKcvE9signature = linkonce_odr hidden constant [3 x i8] c"vi\00", comdat, align 1
@_ZTIi = external constant i8*
@_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature = linkonce_odr hidden constant [4 x i8] c"iii\00", comdat, align 1
@_ZZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcvE9signature = linkonce_odr hidden constant [5 x i8] c"viii\00", comdat, align 1
@_ZTIb = external constant i8*
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEvE5types = linkonce_odr hidden constant [1 x i8*] [i8* bitcast (i8** @_ZTIi to i8*)], comdat, align 4
@_ZZN10emscripten8internal19getGenericSignatureIJiiEEEPKcvE9signature = linkonce_odr hidden constant [3 x i8] c"ii\00", comdat, align 1
@_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii14MozJpegOptionsEEEE3getEvE5types = linkonce_odr hidden constant [5 x i8*] [i8* bitcast ({ i8*, i8* }* @_ZTIN10emscripten3valE to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i8*, i32 }* @_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast (i8** @_ZTIi to i8*), i8* bitcast ({ i8*, i8* }* @_ZTI14MozJpegOptions to i8*)], comdat, align 16
@_ZTSN10emscripten3valE = linkonce_odr hidden constant [19 x i8] c"N10emscripten3valE\00", comdat, align 1
@_ZTIN10emscripten3valE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN10emscripten3valE, i32 0, i32 0) }, comdat, align 4
@_ZTVN10__cxxabiv121__vmi_class_type_infoE = external global i8*
@_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant [63 x i8] c"NSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE\00", comdat, align 1
@_ZTSNSt3__221__basic_string_commonILb1EEE = linkonce_odr constant [38 x i8] c"NSt3__221__basic_string_commonILb1EEE\00", comdat, align 1
@_ZTINSt3__221__basic_string_commonILb1EEE = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([38 x i8], [38 x i8]* @_ZTSNSt3__221__basic_string_commonILb1EEE, i32 0, i32 0) }, comdat, align 4
@_ZTINSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant { i8*, i8*, i32, i32, i8*, i32 } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv121__vmi_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([63 x i8], [63 x i8]* @_ZTSNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, i32 0), i32 0, i32 1, i8* bitcast ({ i8*, i8* }* @_ZTINSt3__221__basic_string_commonILb1EEE to i8*), i32 0 }, comdat, align 4
@_ZZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcvE9signature = linkonce_odr hidden constant [7 x i8] c"iiiiii\00", comdat, align 1
@__tls_guard = internal thread_local global i8 0, align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_mozjpeg_enc.cpp, i8* null }]

@_ZN38EmscriptenBindingInitializer_my_moduleC1Ev = hidden unnamed_addr alias %struct.EmscriptenBindingInitializer_my_module* (%struct.EmscriptenBindingInitializer_my_module*), %struct.EmscriptenBindingInitializer_my_module* (%struct.EmscriptenBindingInitializer_my_module*)* @_ZN38EmscriptenBindingInitializer_my_moduleC2Ev
@_ZTHL10Uint8Array = internal alias void (), void ()* @__tls_init

; Function Attrs: noinline optnone
define hidden i32 @_Z7versionv() #0 {
entry:
  %buffer = alloca [6 x i8], align 1
  %version = alloca i32, align 4
  %last_index = alloca i32, align 4
  %i = alloca i32, align 4
  %0 = bitcast [6 x i8]* %buffer to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 getelementptr inbounds ([6 x i8], [6 x i8]* @__const._Z7versionv.buffer, i32 0, i32 0), i32 6, i1 false)
  store i32 0, i32* %version, align 4
  store i32 0, i32* %last_index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %arraydecay = getelementptr inbounds [6 x i8], [6 x i8]* %buffer, i32 0, i32 0
  %call = call i32 @strlen(i8* %arraydecay)
  %cmp = icmp ult i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [6 x i8], [6 x i8]* %buffer, i32 0, i32 %2
  %3 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %3 to i32
  %cmp1 = icmp eq i32 %conv, 46
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [6 x i8], [6 x i8]* %buffer, i32 0, i32 %4
  store i8 0, i8* %arrayidx2, align 1
  %5 = load i32, i32* %version, align 4
  %shl = shl i32 %5, 8
  %6 = load i32, i32* %last_index, align 4
  %arrayidx3 = getelementptr inbounds [6 x i8], [6 x i8]* %buffer, i32 0, i32 %6
  %call4 = call i32 @atoi(i8* %arrayidx3)
  %or = or i32 %shl, %call4
  store i32 %or, i32* %version, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [6 x i8], [6 x i8]* %buffer, i32 0, i32 %7
  store i8 46, i8* %arrayidx5, align 1
  %8 = load i32, i32* %i, align 4
  %add = add nsw i32 %8, 1
  store i32 %add, i32* %last_index, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load i32, i32* %version, align 4
  %shl6 = shl i32 %10, 8
  %11 = load i32, i32* %last_index, align 4
  %arrayidx7 = getelementptr inbounds [6 x i8], [6 x i8]* %buffer, i32 0, i32 %11
  %call8 = call i32 @atoi(i8* %arrayidx7)
  %or9 = or i32 %shl6, %call8
  store i32 %or9, i32* %version, align 4
  %12 = load i32, i32* %version, align 4
  ret i32 %12
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare i32 @strlen(i8*) #2

declare i32 @atoi(i8*) #2

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #3 {
entry:
  call void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* sret align 4 @_ZL10Uint8Array, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i32 0, i32 0))
  %0 = call i32 @__cxa_thread_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #5
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten3val6globalEPKc(%"class.emscripten::val"* noalias sret align 4 %agg.result, i8* %name) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %name.addr = alloca i8*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i8* %name, i8** %name.addr, align 4
  %1 = load i8*, i8** %name.addr, align 4
  %call = call %"struct.emscripten::internal::_EM_VAL"* @_emval_get_global(i8* %1)
  %call1 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call)
  ret void
}

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor(i8* %0) #3 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* @_ZL10Uint8Array) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"* %0)
  ret %"class.emscripten::val"* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_thread_atexit(void (i8*)*, i8*, i8*) #5

; Function Attrs: noinline optnone
define hidden void @_Z6encodeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEii14MozJpegOptions(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.std::__2::basic_string"* %image_in, i32 %image_width, i32 %image_height, %struct.MozJpegOptions* byval(%struct.MozJpegOptions) align 4 %opts) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %image_width.addr = alloca i32, align 4
  %image_height.addr = alloca i32, align 4
  %image_buffer = alloca i8*, align 4
  %cinfo = alloca %struct.jpeg_compress_struct, align 8
  %jerr = alloca %struct.jpeg_error_mgr, align 4
  %output = alloca i8*, align 4
  %size = alloca i32, align 4
  %quality_str = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp20 = alloca %"class.std::__2::basic_string", align 4
  %pqual = alloca i8*, align 4
  %row_stride = alloca i32, align 4
  %row_pointer = alloca i8*, align 4
  %nrvo = alloca i1, align 1
  %ref.tmp50 = alloca %"struct.emscripten::memory_view", align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i32 %image_width, i32* %image_width.addr, align 4
  store i32 %image_height, i32* %image_height.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %image_in) #5
  store i8* %call, i8** %image_buffer, align 4
  %call1 = call %struct.jpeg_error_mgr* @jpeg_std_error(%struct.jpeg_error_mgr* %jerr)
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 0
  store %struct.jpeg_error_mgr* %call1, %struct.jpeg_error_mgr** %err, align 8
  call void @jpeg_CreateCompress(%struct.jpeg_compress_struct* %cinfo, i32 62, i32 376)
  store i8* null, i8** %output, align 4
  store i32 0, i32* %size, align 4
  call void @jpeg_mem_dest(%struct.jpeg_compress_struct* %cinfo, i8** %output, i32* %size)
  %1 = load i32, i32* %image_width.addr, align 4
  %image_width2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 7
  store i32 %1, i32* %image_width2, align 4
  %2 = load i32, i32* %image_height.addr, align 4
  %image_height3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 8
  store i32 %2, i32* %image_height3, align 8
  %input_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 9
  store i32 4, i32* %input_components, align 4
  %in_color_space = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 10
  store i32 12, i32* %in_color_space, align 8
  call void @jpeg_set_defaults(%struct.jpeg_compress_struct* %cinfo)
  %color_space = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 6
  %3 = load i32, i32* %color_space, align 4
  call void @jpeg_set_colorspace(%struct.jpeg_compress_struct* %cinfo, i32 %3)
  %quant_table = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 7
  %4 = load i32, i32* %quant_table, align 4
  %cmp = icmp ne i32 %4, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %quant_table4 = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 7
  %5 = load i32, i32* %quant_table4, align 4
  call void @jpeg_c_set_int_param(%struct.jpeg_compress_struct* %cinfo, i32 1145645745, i32 %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %optimize_coding = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 4
  %6 = load i8, i8* %optimize_coding, align 1
  %tobool = trunc i8 %6 to i1
  %conv = zext i1 %tobool to i32
  %optimize_coding5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 26
  store i32 %conv, i32* %optimize_coding5, align 8
  %arithmetic = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 2
  %7 = load i8, i8* %arithmetic, align 1
  %tobool6 = trunc i8 %7 to i1
  br i1 %tobool6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end
  %arith_code = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 25
  store i32 1, i32* %arith_code, align 4
  %optimize_coding8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 26
  store i32 0, i32* %optimize_coding8, align 8
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %if.end
  %smoothing = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 5
  %8 = load i32, i32* %smoothing, align 4
  %smoothing_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 28
  store i32 %8, i32* %smoothing_factor, align 8
  %trellis_multipass = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 8
  %9 = load i8, i8* %trellis_multipass, align 4
  %tobool10 = trunc i8 %9 to i1
  %conv11 = zext i1 %tobool10 to i32
  call void @jpeg_c_set_bool_param(%struct.jpeg_compress_struct* %cinfo, i32 -41675723, i32 %conv11)
  %trellis_opt_zero = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 9
  %10 = load i8, i8* %trellis_opt_zero, align 1
  %tobool12 = trunc i8 %10 to i1
  %conv13 = zext i1 %tobool12 to i32
  call void @jpeg_c_set_bool_param(%struct.jpeg_compress_struct* %cinfo, i32 -671664256, i32 %conv13)
  %trellis_opt_table = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 10
  %11 = load i8, i8* %trellis_opt_table, align 2
  %tobool14 = trunc i8 %11 to i1
  %conv15 = zext i1 %tobool14 to i32
  call void @jpeg_c_set_bool_param(%struct.jpeg_compress_struct* %cinfo, i32 -517283223, i32 %conv15)
  %trellis_loops = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 11
  %12 = load i32, i32* %trellis_loops, align 4
  call void @jpeg_c_set_int_param(%struct.jpeg_compress_struct* %cinfo, i32 -1237401799, i32 %12)
  %quality = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 0
  %13 = load i32, i32* %quality, align 4
  call void @_ZNSt3__29to_stringEi(%"class.std::__2::basic_string"* sret align 4 %quality_str, i32 %13)
  %separate_chroma_quality = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 14
  %14 = load i8, i8* %separate_chroma_quality, align 4
  %tobool16 = trunc i8 %14 to i1
  br i1 %tobool16, label %land.lhs.true, label %if.end24

land.lhs.true:                                    ; preds = %if.end9
  %color_space17 = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 6
  %15 = load i32, i32* %color_space17, align 4
  %cmp18 = icmp eq i32 %15, 3
  br i1 %cmp18, label %if.then19, label %if.end24

if.then19:                                        ; preds = %land.lhs.true
  %chroma_quality = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 15
  %16 = load i32, i32* %chroma_quality, align 4
  call void @_ZNSt3__29to_stringEi(%"class.std::__2::basic_string"* sret align 4 %ref.tmp20, i32 %16)
  call void @_ZNSt3__2plIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12basic_stringIT_T0_T1_EEPKS6_OS9_(%"class.std::__2::basic_string"* sret align 4 %ref.tmp, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0), %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp20)
  %call21 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEpLERKS5_(%"class.std::__2::basic_string"* %quality_str, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %call22 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #5
  %call23 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp20) #5
  br label %if.end24

if.end24:                                         ; preds = %if.then19, %land.lhs.true, %if.end9
  %call25 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %quality_str) #5
  store i8* %call25, i8** %pqual, align 4
  %17 = load i8*, i8** %pqual, align 4
  %baseline = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 1
  %18 = load i8, i8* %baseline, align 4
  %tobool26 = trunc i8 %18 to i1
  %conv27 = zext i1 %tobool26 to i32
  %call28 = call i32 @set_quality_ratings(%struct.jpeg_compress_struct* %cinfo, i8* %17, i32 %conv27)
  %auto_subsample = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 12
  %19 = load i8, i8* %auto_subsample, align 4
  %tobool29 = trunc i8 %19 to i1
  br i1 %tobool29, label %if.end37, label %land.lhs.true30

land.lhs.true30:                                  ; preds = %if.end24
  %color_space31 = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 6
  %20 = load i32, i32* %color_space31, align 4
  %cmp32 = icmp eq i32 %20, 3
  br i1 %cmp32, label %if.then33, label %if.end37

if.then33:                                        ; preds = %land.lhs.true30
  %chroma_subsample = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 13
  %21 = load i32, i32* %chroma_subsample, align 4
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 15
  %22 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4
  %arrayidx = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %22, i32 0
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx, i32 0, i32 2
  store i32 %21, i32* %h_samp_factor, align 4
  %chroma_subsample34 = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 13
  %23 = load i32, i32* %chroma_subsample34, align 4
  %comp_info35 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 15
  %24 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info35, align 4
  %arrayidx36 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %24, i32 0
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx36, i32 0, i32 3
  store i32 %23, i32* %v_samp_factor, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.then33, %land.lhs.true30, %if.end24
  %baseline38 = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 1
  %25 = load i8, i8* %baseline38, align 4
  %tobool39 = trunc i8 %25 to i1
  br i1 %tobool39, label %if.else, label %land.lhs.true40

land.lhs.true40:                                  ; preds = %if.end37
  %progressive = getelementptr inbounds %struct.MozJpegOptions, %struct.MozJpegOptions* %opts, i32 0, i32 3
  %26 = load i8, i8* %progressive, align 2
  %tobool41 = trunc i8 %26 to i1
  br i1 %tobool41, label %if.then42, label %if.else

if.then42:                                        ; preds = %land.lhs.true40
  call void @jpeg_simple_progression(%struct.jpeg_compress_struct* %cinfo)
  br label %if.end43

if.else:                                          ; preds = %land.lhs.true40, %if.end37
  %num_scans = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 22
  store i32 0, i32* %num_scans, align 8
  %scan_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 23
  store %struct.jpeg_scan_info* null, %struct.jpeg_scan_info** %scan_info, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.else, %if.then42
  call void @jpeg_start_compress(%struct.jpeg_compress_struct* %cinfo, i32 1)
  %27 = load i32, i32* %image_width.addr, align 4
  %mul = mul nsw i32 %27, 4
  store i32 %mul, i32* %row_stride, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end43
  %next_scanline = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 39
  %28 = load i32, i32* %next_scanline, align 8
  %image_height44 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 8
  %29 = load i32, i32* %image_height44, align 8
  %cmp45 = icmp ult i32 %28, %29
  br i1 %cmp45, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %30 = load i8*, i8** %image_buffer, align 4
  %next_scanline46 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %cinfo, i32 0, i32 39
  %31 = load i32, i32* %next_scanline46, align 8
  %32 = load i32, i32* %row_stride, align 4
  %mul47 = mul i32 %31, %32
  %arrayidx48 = getelementptr inbounds i8, i8* %30, i32 %mul47
  store i8* %arrayidx48, i8** %row_pointer, align 4
  %call49 = call i32 @jpeg_write_scanlines(%struct.jpeg_compress_struct* %cinfo, i8** %row_pointer, i32 1)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  call void @jpeg_finish_compress(%struct.jpeg_compress_struct* %cinfo)
  store i1 false, i1* %nrvo, align 1
  %33 = call %"class.emscripten::val"* @_ZTWL10Uint8Array()
  %34 = load i32, i32* %size, align 4
  %35 = load i8*, i8** %output, align 4
  call void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp50, i32 %34, i8* %35)
  call void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* sret align 4 %agg.result, %"class.emscripten::val"* %33, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp50)
  call void @jpeg_destroy_compress(%struct.jpeg_compress_struct* %cinfo)
  %36 = load i8*, i8** %output, align 4
  call void @free(i8* %36)
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %while.end
  %call51 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %agg.result) #5
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %while.end
  %call52 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %quality_str) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #5
  ret i8* %call
}

declare %struct.jpeg_error_mgr* @jpeg_std_error(%struct.jpeg_error_mgr*) #2

declare void @jpeg_CreateCompress(%struct.jpeg_compress_struct*, i32, i32) #2

declare void @jpeg_mem_dest(%struct.jpeg_compress_struct*, i8**, i32*) #2

declare void @jpeg_set_defaults(%struct.jpeg_compress_struct*) #2

declare void @jpeg_set_colorspace(%struct.jpeg_compress_struct*, i32) #2

declare void @jpeg_c_set_int_param(%struct.jpeg_compress_struct*, i32, i32) #2

declare void @jpeg_c_set_bool_param(%struct.jpeg_compress_struct*, i32, i32) #2

declare void @_ZNSt3__29to_stringEi(%"class.std::__2::basic_string"* sret align 4, i32) #2

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNSt3__2plIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12basic_stringIT_T0_T1_EEPKS6_OS9_(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, i8* %__lhs, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__rhs) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %__lhs.addr = alloca i8*, align 4
  %__rhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i8* %__lhs, i8** %__lhs.addr, align 4
  store %"class.std::__2::basic_string"* %__rhs, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %2 = load i8*, i8** %__lhs.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6insertEmPKc(%"class.std::__2::basic_string"* %1, i32 0, i8* %2)
  %call1 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__24moveIRNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call) #5
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EOS5_(%"class.std::__2::basic_string"* %agg.result, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call1) #5
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEpLERKS5_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendERKS5_(%"class.std::__2::basic_string"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  ret %"class.std::__2::basic_string"* %call
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #6

declare i32 @set_quality_ratings(%struct.jpeg_compress_struct*, i8*, i32) #2

declare void @jpeg_simple_progression(%struct.jpeg_compress_struct*) #2

declare void @jpeg_start_compress(%struct.jpeg_compress_struct*, i32) #2

declare i32 @jpeg_write_scanlines(%struct.jpeg_compress_struct*, i8**, i32) #2

declare void @jpeg_finish_compress(%struct.jpeg_compress_struct*) #2

; Function Attrs: noinline
define internal %"class.emscripten::val"* @_ZTWL10Uint8Array() #7 {
  call void @_ZTHL10Uint8Array()
  ret %"class.emscripten::val"* @_ZL10Uint8Array
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val4new_IJNS_11memory_viewIhEEEEES0_DpOT_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #5
  call void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_(%"class.emscripten::val"* sret align 4 %agg.result, %"class.emscripten::val"* %this1, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* @_emval_new, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten17typed_memory_viewIhEENS_11memory_viewIT_EEmPKS2_(%"struct.emscripten::memory_view"* noalias sret align 4 %agg.result, i32 %size, i8* %data) #0 comdat {
entry:
  %size.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %1 = load i8*, i8** %data.addr, align 4
  %call = call %"struct.emscripten::memory_view"* @_ZN10emscripten11memory_viewIhEC2EmPKh(%"struct.emscripten::memory_view"* %agg.result, i32 %0, i8* %1)
  ret void
}

declare void @jpeg_destroy_compress(%struct.jpeg_compress_struct*) #2

declare void @free(i8*) #2

; Function Attrs: noinline
define internal void @__cxx_global_var_init.2() #3 {
entry:
  %call = call %struct.EmscriptenBindingInitializer_my_module* @_ZN38EmscriptenBindingInitializer_my_moduleC1Ev(%struct.EmscriptenBindingInitializer_my_module* @_ZL47EmscriptenBindingInitializer_my_module_instance)
  ret void
}

; Function Attrs: noinline optnone
define hidden %struct.EmscriptenBindingInitializer_my_module* @_ZN38EmscriptenBindingInitializer_my_moduleC2Ev(%struct.EmscriptenBindingInitializer_my_module* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.EmscriptenBindingInitializer_my_module*, align 4
  %ref.tmp = alloca %"class.emscripten::value_object", align 1
  store %struct.EmscriptenBindingInitializer_my_module* %this, %struct.EmscriptenBindingInitializer_my_module** %this.addr, align 4
  %this1 = load %struct.EmscriptenBindingInitializer_my_module*, %struct.EmscriptenBindingInitializer_my_module** %this.addr, align 4
  %call = call %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsEC2EPKc(%"class.emscripten::value_object"* %ref.tmp, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.3, i32 0, i32 0))
  %call2 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %ref.tmp, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.4, i32 0, i32 0), i32 0)
  %call3 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call2, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.5, i32 0, i32 0), i32 4)
  %call4 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call3, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.6, i32 0, i32 0), i32 5)
  %call5 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call4, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.7, i32 0, i32 0), i32 6)
  %call6 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call5, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.8, i32 0, i32 0), i32 7)
  %call7 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call6, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.9, i32 0, i32 0), i32 8)
  %call8 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call7, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.10, i32 0, i32 0), i32 12)
  %call9 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call8, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.11, i32 0, i32 0), i32 16)
  %call10 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call9, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.12, i32 0, i32 0), i32 20)
  %call11 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call10, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.13, i32 0, i32 0), i32 21)
  %call12 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call11, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.14, i32 0, i32 0), i32 22)
  %call13 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call12, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.15, i32 0, i32 0), i32 24)
  %call14 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call13, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.16, i32 0, i32 0), i32 32)
  %call15 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call14, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.17, i32 0, i32 0), i32 28)
  %call16 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call15, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.18, i32 0, i32 0), i32 36)
  %call17 = call nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %call16, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.19, i32 0, i32 0), i32 40)
  %call18 = call %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsED2Ev(%"class.emscripten::value_object"* %ref.tmp) #5
  call void @_ZN10emscripten8functionIiJEJEEEvPKcPFT_DpT0_EDpT1_(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.20, i32 0, i32 0), i32 ()* @_Z7versionv)
  call void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEii14MozJpegOptionsEJEEEvPKcPFT_DpT0_EDpT1_(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.21, i32 0, i32 0), void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)* @_Z6encodeNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEii14MozJpegOptions)
  ret %struct.EmscriptenBindingInitializer_my_module* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsEC2EPKc(%"class.emscripten::value_object"* returned %this, i8* %name) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.emscripten::value_object"*, align 4
  %name.addr = alloca i8*, align 4
  %ctor = alloca %struct.MozJpegOptions* ()*, align 4
  %dtor = alloca void (%struct.MozJpegOptions*)*, align 4
  store %"class.emscripten::value_object"* %this, %"class.emscripten::value_object"** %this.addr, align 4
  store i8* %name, i8** %name.addr, align 4
  %this1 = load %"class.emscripten::value_object"*, %"class.emscripten::value_object"** %this.addr, align 4
  %0 = bitcast %"class.emscripten::value_object"* %this1 to %"class.emscripten::internal::noncopyable"*
  %call = call %"class.emscripten::internal::noncopyable"* @_ZN10emscripten8internal11noncopyableC2Ev(%"class.emscripten::internal::noncopyable"* %0)
  store %struct.MozJpegOptions* ()* @_ZN10emscripten8internal15raw_constructorI14MozJpegOptionsJEEEPT_DpNS0_11BindingTypeIT0_vE8WireTypeE, %struct.MozJpegOptions* ()** %ctor, align 4
  store void (%struct.MozJpegOptions*)* @_ZN10emscripten8internal14raw_destructorI14MozJpegOptionsEEvPT_, void (%struct.MozJpegOptions*)** %dtor, align 4
  %call2 = call i8* @_ZN10emscripten8internal6TypeIDI14MozJpegOptionsvE3getEv()
  %1 = load i8*, i8** %name.addr, align 4
  %2 = load %struct.MozJpegOptions* ()*, %struct.MozJpegOptions* ()** %ctor, align 4
  %call3 = call i8* @_ZN10emscripten8internal12getSignatureIP14MozJpegOptionsJEEEPKcPFT_DpT0_E(%struct.MozJpegOptions* ()* %2)
  %3 = load %struct.MozJpegOptions* ()*, %struct.MozJpegOptions* ()** %ctor, align 4
  %4 = bitcast %struct.MozJpegOptions* ()* %3 to i8*
  %5 = load void (%struct.MozJpegOptions*)*, void (%struct.MozJpegOptions*)** %dtor, align 4
  %call4 = call i8* @_ZN10emscripten8internal12getSignatureIvJP14MozJpegOptionsEEEPKcPFT_DpT0_E(void (%struct.MozJpegOptions*)* %5)
  %6 = load void (%struct.MozJpegOptions*)*, void (%struct.MozJpegOptions*)** %dtor, align 4
  %7 = bitcast void (%struct.MozJpegOptions*)* %6 to i8*
  call void @_embind_register_value_object(i8* %call2, i8* %1, i8* %call3, i8* %4, i8* %call4, i8* %7)
  ret %"class.emscripten::value_object"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_iEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %this, i8* %fieldName, i32 %field) #0 comdat {
entry:
  %this.addr = alloca %"class.emscripten::value_object"*, align 4
  %fieldName.addr = alloca i8*, align 4
  %field.addr = alloca i32, align 4
  %getter = alloca i32 (i32*, %struct.MozJpegOptions*)*, align 4
  %setter = alloca void (i32*, %struct.MozJpegOptions*, i32)*, align 4
  store %"class.emscripten::value_object"* %this, %"class.emscripten::value_object"** %this.addr, align 4
  store i8* %fieldName, i8** %fieldName.addr, align 4
  store i32 %field, i32* %field.addr, align 4
  %this1 = load %"class.emscripten::value_object"*, %"class.emscripten::value_object"** %this.addr, align 4
  store i32 (i32*, %struct.MozJpegOptions*)* @_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsiE7getWireIS2_EEiRKMS2_iRKT_, i32 (i32*, %struct.MozJpegOptions*)** %getter, align 4
  store void (i32*, %struct.MozJpegOptions*, i32)* @_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsiE7setWireIS2_EEvRKMS2_iRT_i, void (i32*, %struct.MozJpegOptions*, i32)** %setter, align 4
  %call = call i8* @_ZN10emscripten8internal6TypeIDI14MozJpegOptionsvE3getEv()
  %0 = load i8*, i8** %fieldName.addr, align 4
  %call2 = call i8* @_ZN10emscripten8internal6TypeIDIivE3getEv()
  %1 = load i32 (i32*, %struct.MozJpegOptions*)*, i32 (i32*, %struct.MozJpegOptions*)** %getter, align 4
  %call3 = call i8* @_ZN10emscripten8internal12getSignatureIiJRKM14MozJpegOptionsiRKS2_EEEPKcPFT_DpT0_E(i32 (i32*, %struct.MozJpegOptions*)* %1)
  %2 = load i32 (i32*, %struct.MozJpegOptions*)*, i32 (i32*, %struct.MozJpegOptions*)** %getter, align 4
  %3 = bitcast i32 (i32*, %struct.MozJpegOptions*)* %2 to i8*
  %call4 = call i32* @_ZN10emscripten8internal10getContextIM14MozJpegOptionsiEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %field.addr)
  %4 = bitcast i32* %call4 to i8*
  %call5 = call i8* @_ZN10emscripten8internal6TypeIDIivE3getEv()
  %5 = load void (i32*, %struct.MozJpegOptions*, i32)*, void (i32*, %struct.MozJpegOptions*, i32)** %setter, align 4
  %call6 = call i8* @_ZN10emscripten8internal12getSignatureIvJRKM14MozJpegOptionsiRS2_iEEEPKcPFT_DpT0_E(void (i32*, %struct.MozJpegOptions*, i32)* %5)
  %6 = load void (i32*, %struct.MozJpegOptions*, i32)*, void (i32*, %struct.MozJpegOptions*, i32)** %setter, align 4
  %7 = bitcast void (i32*, %struct.MozJpegOptions*, i32)* %6 to i8*
  %call7 = call i32* @_ZN10emscripten8internal10getContextIM14MozJpegOptionsiEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %field.addr)
  %8 = bitcast i32* %call7 to i8*
  call void @_embind_register_value_object_field(i8* %call, i8* %0, i8* %call2, i8* %call3, i8* %3, i8* %4, i8* %call5, i8* %call6, i8* %7, i8* %8)
  ret %"class.emscripten::value_object"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsE5fieldIS1_bEERS2_PKcMT_T0_(%"class.emscripten::value_object"* %this, i8* %fieldName, i32 %field) #0 comdat {
entry:
  %this.addr = alloca %"class.emscripten::value_object"*, align 4
  %fieldName.addr = alloca i8*, align 4
  %field.addr = alloca i32, align 4
  %getter = alloca i1 (i32*, %struct.MozJpegOptions*)*, align 4
  %setter = alloca void (i32*, %struct.MozJpegOptions*, i1)*, align 4
  store %"class.emscripten::value_object"* %this, %"class.emscripten::value_object"** %this.addr, align 4
  store i8* %fieldName, i8** %fieldName.addr, align 4
  store i32 %field, i32* %field.addr, align 4
  %this1 = load %"class.emscripten::value_object"*, %"class.emscripten::value_object"** %this.addr, align 4
  store i1 (i32*, %struct.MozJpegOptions*)* @_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsbE7getWireIS2_EEbRKMS2_bRKT_, i1 (i32*, %struct.MozJpegOptions*)** %getter, align 4
  store void (i32*, %struct.MozJpegOptions*, i1)* @_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsbE7setWireIS2_EEvRKMS2_bRT_b, void (i32*, %struct.MozJpegOptions*, i1)** %setter, align 4
  %call = call i8* @_ZN10emscripten8internal6TypeIDI14MozJpegOptionsvE3getEv()
  %0 = load i8*, i8** %fieldName.addr, align 4
  %call2 = call i8* @_ZN10emscripten8internal6TypeIDIbvE3getEv()
  %1 = load i1 (i32*, %struct.MozJpegOptions*)*, i1 (i32*, %struct.MozJpegOptions*)** %getter, align 4
  %call3 = call i8* @_ZN10emscripten8internal12getSignatureIbJRKM14MozJpegOptionsbRKS2_EEEPKcPFT_DpT0_E(i1 (i32*, %struct.MozJpegOptions*)* %1)
  %2 = load i1 (i32*, %struct.MozJpegOptions*)*, i1 (i32*, %struct.MozJpegOptions*)** %getter, align 4
  %3 = bitcast i1 (i32*, %struct.MozJpegOptions*)* %2 to i8*
  %call4 = call i32* @_ZN10emscripten8internal10getContextIM14MozJpegOptionsbEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %field.addr)
  %4 = bitcast i32* %call4 to i8*
  %call5 = call i8* @_ZN10emscripten8internal6TypeIDIbvE3getEv()
  %5 = load void (i32*, %struct.MozJpegOptions*, i1)*, void (i32*, %struct.MozJpegOptions*, i1)** %setter, align 4
  %call6 = call i8* @_ZN10emscripten8internal12getSignatureIvJRKM14MozJpegOptionsbRS2_bEEEPKcPFT_DpT0_E(void (i32*, %struct.MozJpegOptions*, i1)* %5)
  %6 = load void (i32*, %struct.MozJpegOptions*, i1)*, void (i32*, %struct.MozJpegOptions*, i1)** %setter, align 4
  %7 = bitcast void (i32*, %struct.MozJpegOptions*, i1)* %6 to i8*
  %call7 = call i32* @_ZN10emscripten8internal10getContextIM14MozJpegOptionsbEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %field.addr)
  %8 = bitcast i32* %call7 to i8*
  call void @_embind_register_value_object_field(i8* %call, i8* %0, i8* %call2, i8* %call3, i8* %3, i8* %4, i8* %call5, i8* %call6, i8* %7, i8* %8)
  ret %"class.emscripten::value_object"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::value_object"* @_ZN10emscripten12value_objectI14MozJpegOptionsED2Ev(%"class.emscripten::value_object"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"class.emscripten::value_object"*, align 4
  store %"class.emscripten::value_object"* %this, %"class.emscripten::value_object"** %this.addr, align 4
  %this1 = load %"class.emscripten::value_object"*, %"class.emscripten::value_object"** %this.addr, align 4
  %call = call i8* @_ZN10emscripten8internal6TypeIDI14MozJpegOptionsvE3getEv()
  call void @_embind_finalize_value_object(i8* %call)
  %0 = bitcast %"class.emscripten::value_object"* %this1 to %"class.emscripten::internal::noncopyable"*
  %call2 = call %"class.emscripten::internal::noncopyable"* @_ZN10emscripten8internal11noncopyableD2Ev(%"class.emscripten::internal::noncopyable"* %0) #5
  ret %"class.emscripten::value_object"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8functionIiJEJEEEvPKcPFT_DpT0_EDpT1_(i8* %name, i32 ()* %fn) #0 comdat {
entry:
  %name.addr = alloca i8*, align 4
  %fn.addr = alloca i32 ()*, align 4
  %args = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3", align 1
  %invoker = alloca i32 (i32 ()*)*, align 4
  store i8* %name, i8** %name.addr, align 4
  store i32 ()* %fn, i32 ()** %fn.addr, align 4
  store i32 (i32 ()*)* @_ZN10emscripten8internal7InvokerIiJEE6invokeEPFivE, i32 (i32 ()*)** %invoker, align 4
  %0 = load i8*, i8** %name.addr, align 4
  %call = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"* %args)
  %call1 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"* %args)
  %1 = load i32 (i32 ()*)*, i32 (i32 ()*)** %invoker, align 4
  %call2 = call i8* @_ZN10emscripten8internal12getSignatureIiJPFivEEEEPKcPFT_DpT0_E(i32 (i32 ()*)* %1)
  %2 = load i32 (i32 ()*)*, i32 (i32 ()*)** %invoker, align 4
  %3 = bitcast i32 (i32 ()*)* %2 to i8*
  %4 = load i32 ()*, i32 ()** %fn.addr, align 4
  %5 = bitcast i32 ()* %4 to i8*
  call void @_embind_register_function(i8* %0, i32 %call, i8** %call1, i8* %call2, i8* %3, i8* %5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8functionINS_3valEJNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEii14MozJpegOptionsEJEEEvPKcPFT_DpT0_EDpT1_(i8* %name, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)* %fn) #0 comdat {
entry:
  %name.addr = alloca i8*, align 4
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, align 4
  %args = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4", align 1
  %invoker = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)*, align 4
  store i8* %name, i8** %name.addr, align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)** %fn.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEii14MozJpegOptionsEE6invokeEPFS2_S9_iiSA_EPNS0_11BindingTypeIS9_vEUt_EiiPSA_, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)** %invoker, align 4
  %0 = load i8*, i8** %name.addr, align 4
  %call = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"* %args)
  %call1 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"* %args)
  %1 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)** %invoker, align 4
  %call2 = call i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)* %1)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)*, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)** %invoker, align 4
  %3 = bitcast %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)* %2 to i8*
  %4 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)** %fn.addr, align 4
  %5 = bitcast void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)* %4 to i8*
  call void @_embind_register_function(i8* %0, i32 %call, i8** %call1, i8* %call2, i8* %3, i8* %5)
  ret void
}

declare %"struct.emscripten::internal::_EM_VAL"* @_emval_get_global(i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* returned %this, %"struct.emscripten::internal::_EM_VAL"* %handle) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %handle.addr = alloca %"struct.emscripten::internal::_EM_VAL"*, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %handle, %"struct.emscripten::internal::_EM_VAL"** %handle.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %handle2 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %0 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* %0, %"struct.emscripten::internal::_EM_VAL"** %handle2, align 4
  ret %"class.emscripten::val"* %this1
}

declare void @_emval_decref(%"struct.emscripten::internal::_EM_VAL"*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #5
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #5
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #4 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #5
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #5
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #5
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #5
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #5
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #5
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #5
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #5
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #4 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #5
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #4 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__24moveIRNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__t) #4 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %__t, %"class.std::__2::basic_string"** %__t.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__t.addr, align 4
  ret %"class.std::__2::basic_string"* %0
}

declare nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6insertEmPKc(%"class.std::__2::basic_string"*, i32, i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EOS5_(%"class.std::__2::basic_string"* returned %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  %__r_2 = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::__compressed_pair"* @_ZNSt3__24moveIRNS_17__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES6_EEEEONS_16remove_referenceIT_E4typeEOSC_(%"class.std::__2::__compressed_pair"* nonnull align 4 dereferenceable(12) %__r_2) #5
  %2 = bitcast %"class.std::__2::__compressed_pair"* %__r_ to i8*
  %3 = bitcast %"class.std::__2::__compressed_pair"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 12, i1 false)
  %4 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %4) #5
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::__compressed_pair"* @_ZNSt3__24moveIRNS_17__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES6_EEEEONS_16remove_referenceIT_E4typeEOSC_(%"class.std::__2::__compressed_pair"* nonnull align 4 dereferenceable(12) %__t) #4 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %__t, %"class.std::__2::__compressed_pair"** %__t.addr, align 4
  %0 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %__t.addr, align 4
  ret %"class.std::__2::__compressed_pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__a = alloca [3 x i32]*, align 4
  %__i = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #5
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__r = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"*
  %__words = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"* %__r, i32 0, i32 0
  store [3 x i32]* %__words, [3 x i32]** %__a, align 4
  store i32 0, i32* %__i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i, align 4
  %cmp = icmp ult i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load [3 x i32]*, [3 x i32]** %__a, align 4
  %3 = load i32, i32* %__i, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %2, i32 0, i32 %3
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %__i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %__i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #5
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendERKS5_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %0) #5
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %1) #5
  %call3 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEPKcm(%"class.std::__2::basic_string"* %this1, i8* %call, i32 %call2)
  ret %"class.std::__2::basic_string"* %call3
}

declare nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #5
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this1) #5
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this1) #5
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #5
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__size_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this) #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #5
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.emscripten::memory_view"* @_ZN10emscripten11memory_viewIhEC2EmPKh(%"struct.emscripten::memory_view"* returned %this, i32 %size, i8* %data) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %size.addr = alloca i32, align 4
  %data.addr = alloca i8*, align 4
  store %"struct.emscripten::memory_view"* %this, %"struct.emscripten::memory_view"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %this1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %this.addr, align 4
  %size2 = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  store i32 %0, i32* %size2, align 4
  %data3 = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %this1, i32 0, i32 1
  %1 = load i8*, i8** %data.addr, align 4
  store i8* %1, i8** %data3, align 4
  ret %"struct.emscripten::memory_view"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10emscripten3val12internalCallIPFPNS_8internal7_EM_VALES4_jPKPKvS6_EJNS_11memory_viewIhEEEEES0_T_DpOT0_(%"class.emscripten::val"* noalias sret align 4 %agg.result, %"class.emscripten::val"* %this, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.emscripten::val"*, align 4
  %impl.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %argList = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList", align 1
  %argv = alloca %"struct.emscripten::internal::WireTypePack", align 8
  %0 = bitcast %"class.emscripten::val"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.emscripten::val"* %this, %"class.emscripten::val"** %this.addr, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)* %impl, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"class.emscripten::val"*, %"class.emscripten::val"** %this.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #5
  %call2 = call %"struct.emscripten::internal::WireTypePack"* @_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_(%"struct.emscripten::internal::WireTypePack"* %argv, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  %2 = load %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)*, %"struct.emscripten::internal::_EM_VAL"* (%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*)** %impl.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %this1, i32 0, i32 0
  %3 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  %call3 = call i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %argList)
  %call4 = call i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %argList)
  %call5 = call i8* @_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv(%"struct.emscripten::internal::WireTypePack"* %argv)
  %call6 = call %"struct.emscripten::internal::_EM_VAL"* %2(%"struct.emscripten::internal::_EM_VAL"* %3, i32 %call3, i8** %call4, i8* %call5)
  %call7 = call %"class.emscripten::val"* @_ZN10emscripten3valC2EPNS_8internal7_EM_VALE(%"class.emscripten::val"* %agg.result, %"struct.emscripten::internal::_EM_VAL"* %call6)
  ret void
}

declare %"struct.emscripten::internal::_EM_VAL"* @_emval_new(%"struct.emscripten::internal::_EM_VAL"*, i32, i8**, i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %__t) #4 comdat {
entry:
  %__t.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"struct.emscripten::memory_view"* %__t, %"struct.emscripten::memory_view"** %__t.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %__t.addr, align 4
  ret %"struct.emscripten::memory_view"* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::WireTypePack"* @_ZN10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEC2EOS3_(%"struct.emscripten::internal::WireTypePack"* returned %this, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %args) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack"*, align 4
  %args.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %cursor = alloca %"union.emscripten::internal::GenericWireType"*, align 4
  store %"struct.emscripten::internal::WireTypePack"* %this, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  store %"struct.emscripten::memory_view"* %args, %"struct.emscripten::memory_view"** %args.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack"*, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %elements2 = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %elements2) #5
  store %"union.emscripten::internal::GenericWireType"* %call, %"union.emscripten::internal::GenericWireType"** %cursor, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %args.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %0) #5
  call void @_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call3)
  ret %"struct.emscripten::internal::WireTypePack"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_11memory_viewIhEEEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv()
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK10emscripten8internal12WireTypePackIJNS_11memory_viewIhEEEEcvPKvEv(%"struct.emscripten::internal::WireTypePack"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WireTypePack"*, align 4
  store %"struct.emscripten::internal::WireTypePack"* %this, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WireTypePack"*, %"struct.emscripten::internal::WireTypePack"** %this.addr, align 4
  %elements = getelementptr inbounds %"struct.emscripten::internal::WireTypePack", %"struct.emscripten::internal::WireTypePack"* %this1, i32 0, i32 0
  %call = call %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %elements) #5
  %0 = bitcast %"union.emscripten::internal::GenericWireType"* %call to i8*
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1 x %"union.emscripten::internal::GenericWireType"], [1 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: alwaysinline
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesINS_11memory_viewIhEEJEEEvRPNS0_15GenericWireTypeEOT_DpOT0_(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %first) #8 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %first.addr = alloca %"struct.emscripten::memory_view"*, align 4
  %ref.tmp = alloca %"struct.emscripten::memory_view", align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::memory_view"* %first, %"struct.emscripten::memory_view"** %first.addr, align 4
  %0 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %1 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %first.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %"struct.emscripten::memory_view"* @_ZNSt3__27forwardIN10emscripten11memory_viewIhEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %1) #5
  call void @_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_(%"struct.emscripten::memory_view"* sret align 4 %ref.tmp, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %call)
  call void @_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  call void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal20writeGenericWireTypeIhEEvRPNS0_15GenericWireTypeERKNS_11memory_viewIT_EE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %cursor, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %wt) #4 comdat {
entry:
  %cursor.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  %wt.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"union.emscripten::internal::GenericWireType"** %cursor, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  store %"struct.emscripten::memory_view"* %wt, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %size = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %0, i32 0, i32 0
  %1 = load i32, i32* %size, align 4
  %2 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %3 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %2, align 4
  %w = bitcast %"union.emscripten::internal::GenericWireType"* %3 to [2 x %union.anon.2]*
  %arrayidx = getelementptr inbounds [2 x %union.anon.2], [2 x %union.anon.2]* %w, i32 0, i32 0
  %u = bitcast %union.anon.2* %arrayidx to i32*
  store i32 %1, i32* %u, align 8
  %4 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %wt.addr, align 4
  %data = getelementptr inbounds %"struct.emscripten::memory_view", %"struct.emscripten::memory_view"* %4, i32 0, i32 1
  %5 = load i8*, i8** %data, align 4
  %6 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %7 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %6, align 4
  %w1 = bitcast %"union.emscripten::internal::GenericWireType"* %7 to [2 x %union.anon.2]*
  %arrayidx2 = getelementptr inbounds [2 x %union.anon.2], [2 x %union.anon.2]* %w1, i32 0, i32 1
  %p = bitcast %union.anon.2* %arrayidx2 to i8**
  store i8* %5, i8** %p, align 4
  %8 = load %"union.emscripten::internal::GenericWireType"**, %"union.emscripten::internal::GenericWireType"*** %cursor.addr, align 4
  %9 = load %"union.emscripten::internal::GenericWireType"*, %"union.emscripten::internal::GenericWireType"** %8, align 4
  %incdec.ptr = getelementptr inbounds %"union.emscripten::internal::GenericWireType", %"union.emscripten::internal::GenericWireType"* %9, i32 1
  store %"union.emscripten::internal::GenericWireType"* %incdec.ptr, %"union.emscripten::internal::GenericWireType"** %8, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal11BindingTypeINS_11memory_viewIhEEvE10toWireTypeERKS3_(%"struct.emscripten::memory_view"* noalias sret align 4 %agg.result, %"struct.emscripten::memory_view"* nonnull align 4 dereferenceable(8) %mv) #4 comdat {
entry:
  %mv.addr = alloca %"struct.emscripten::memory_view"*, align 4
  store %"struct.emscripten::memory_view"* %mv, %"struct.emscripten::memory_view"** %mv.addr, align 4
  %0 = load %"struct.emscripten::memory_view"*, %"struct.emscripten::memory_view"** %mv.addr, align 4
  %1 = bitcast %"struct.emscripten::memory_view"* %agg.result to i8*
  %2 = bitcast %"struct.emscripten::memory_view"* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal21writeGenericWireTypesERPNS0_15GenericWireTypeE(%"union.emscripten::internal::GenericWireType"** nonnull align 4 dereferenceable(4) %0) #4 comdat {
entry:
  %.addr = alloca %"union.emscripten::internal::GenericWireType"**, align 4
  store %"union.emscripten::internal::GenericWireType"** %0, %"union.emscripten::internal::GenericWireType"*** %.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEv() #4 comdat {
entry:
  ret i8** getelementptr inbounds ([1 x i8*], [1 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_11memory_viewIhEEEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"union.emscripten::internal::GenericWireType"* @_ZNKSt3__25arrayIN10emscripten8internal15GenericWireTypeELm1EE4dataEv(%"struct.std::__2::array"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [1 x %"union.emscripten::internal::GenericWireType"], [1 x %"union.emscripten::internal::GenericWireType"]* %__elems_, i32 0, i32 0
  ret %"union.emscripten::internal::GenericWireType"* %arraydecay
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::internal::noncopyable"* @_ZN10emscripten8internal11noncopyableC2Ev(%"class.emscripten::internal::noncopyable"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"class.emscripten::internal::noncopyable"*, align 4
  store %"class.emscripten::internal::noncopyable"* %this, %"class.emscripten::internal::noncopyable"** %this.addr, align 4
  %this1 = load %"class.emscripten::internal::noncopyable"*, %"class.emscripten::internal::noncopyable"** %this.addr, align 4
  ret %"class.emscripten::internal::noncopyable"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.MozJpegOptions* @_ZN10emscripten8internal15raw_constructorI14MozJpegOptionsJEEEPT_DpNS0_11BindingTypeIT0_vE8WireTypeE() #0 comdat {
entry:
  %call = call noalias nonnull i8* @_Znwm(i32 44) #13
  %0 = bitcast i8* %call to %struct.MozJpegOptions*
  %1 = bitcast %struct.MozJpegOptions* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %1, i8 0, i32 44, i1 false)
  ret %struct.MozJpegOptions* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10emscripten8internal14raw_destructorI14MozJpegOptionsEEvPT_(%struct.MozJpegOptions* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca %struct.MozJpegOptions*, align 4
  store %struct.MozJpegOptions* %ptr, %struct.MozJpegOptions** %ptr.addr, align 4
  %0 = load %struct.MozJpegOptions*, %struct.MozJpegOptions** %ptr.addr, align 4
  %isnull = icmp eq %struct.MozJpegOptions* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %struct.MozJpegOptions* %0 to i8*
  call void @_ZdlPv(i8* %1) #14
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

declare void @_embind_register_value_object(i8*, i8*, i8*, i8*, i8*, i8*) #2

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal6TypeIDI14MozJpegOptionsvE3getEv() #0 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal11LightTypeIDI14MozJpegOptionsE3getEv()
  ret i8* %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIP14MozJpegOptionsJEEEPKcPFT_DpT0_E(%struct.MozJpegOptions* ()* %0) #8 comdat {
entry:
  %.addr = alloca %struct.MozJpegOptions* ()*, align 4
  store %struct.MozJpegOptions* ()* %0, %struct.MozJpegOptions* ()** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJP14MozJpegOptionsEEEPKcv()
  ret i8* %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIvJP14MozJpegOptionsEEEPKcPFT_DpT0_E(void (%struct.MozJpegOptions*)* %0) #8 comdat {
entry:
  %.addr = alloca void (%struct.MozJpegOptions*)*, align 4
  store void (%struct.MozJpegOptions*)* %0, void (%struct.MozJpegOptions*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJvP14MozJpegOptionsEEEPKcv()
  ret i8* %call
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #9

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #10

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #11

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal11LightTypeIDI14MozJpegOptionsE3getEv() #4 comdat {
entry:
  ret i8* bitcast ({ i8*, i8* }* @_ZTI14MozJpegOptions to i8*)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJP14MozJpegOptionsEEEPKcv() #8 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiEEEPKcv() #4 comdat {
entry:
  ret i8* getelementptr inbounds ([2 x i8], [2 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJvP14MozJpegOptionsEEEPKcv() #8 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJviEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJviEEEPKcv() #4 comdat {
entry:
  ret i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJviEEEPKcvE9signature, i32 0, i32 0)
}

declare void @_embind_finalize_value_object(i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.emscripten::internal::noncopyable"* @_ZN10emscripten8internal11noncopyableD2Ev(%"class.emscripten::internal::noncopyable"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"class.emscripten::internal::noncopyable"*, align 4
  store %"class.emscripten::internal::noncopyable"* %this, %"class.emscripten::internal::noncopyable"** %this.addr, align 4
  %this1 = load %"class.emscripten::internal::noncopyable"*, %"class.emscripten::internal::noncopyable"** %this.addr, align 4
  ret %"class.emscripten::internal::noncopyable"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsiE7getWireIS2_EEiRKMS2_iRKT_(i32* nonnull align 4 dereferenceable(4) %field, %struct.MozJpegOptions* nonnull align 4 dereferenceable(44) %ptr) #0 comdat {
entry:
  %field.addr = alloca i32*, align 4
  %ptr.addr = alloca %struct.MozJpegOptions*, align 4
  store i32* %field, i32** %field.addr, align 4
  store %struct.MozJpegOptions* %ptr, %struct.MozJpegOptions** %ptr.addr, align 4
  %0 = load %struct.MozJpegOptions*, %struct.MozJpegOptions** %ptr.addr, align 4
  %1 = load i32*, i32** %field.addr, align 4
  %2 = load i32, i32* %1, align 4
  %3 = bitcast %struct.MozJpegOptions* %0 to i8*
  %memptr.offset = getelementptr inbounds i8, i8* %3, i32 %2
  %4 = bitcast i8* %memptr.offset to i32*
  %call = call i32 @_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi(i32* nonnull align 4 dereferenceable(4) %4)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsiE7setWireIS2_EEvRKMS2_iRT_i(i32* nonnull align 4 dereferenceable(4) %field, %struct.MozJpegOptions* nonnull align 4 dereferenceable(44) %ptr, i32 %value) #0 comdat {
entry:
  %field.addr = alloca i32*, align 4
  %ptr.addr = alloca %struct.MozJpegOptions*, align 4
  %value.addr = alloca i32, align 4
  store i32* %field, i32** %field.addr, align 4
  store %struct.MozJpegOptions* %ptr, %struct.MozJpegOptions** %ptr.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %0 = load i32, i32* %value.addr, align 4
  %call = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %0)
  %1 = load %struct.MozJpegOptions*, %struct.MozJpegOptions** %ptr.addr, align 4
  %2 = load i32*, i32** %field.addr, align 4
  %3 = load i32, i32* %2, align 4
  %4 = bitcast %struct.MozJpegOptions* %1 to i8*
  %memptr.offset = getelementptr inbounds i8, i8* %4, i32 %3
  %5 = bitcast i8* %memptr.offset to i32*
  store i32 %call, i32* %5, align 4
  ret void
}

declare void @_embind_register_value_object_field(i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*) #2

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal6TypeIDIivE3getEv() #0 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal11LightTypeIDIiE3getEv()
  ret i8* %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIiJRKM14MozJpegOptionsiRKS2_EEEPKcPFT_DpT0_E(i32 (i32*, %struct.MozJpegOptions*)* %0) #8 comdat {
entry:
  %.addr = alloca i32 (i32*, %struct.MozJpegOptions*)*, align 4
  store i32 (i32*, %struct.MozJpegOptions*)* %0, i32 (i32*, %struct.MozJpegOptions*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJiRKM14MozJpegOptionsiRKS2_EEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN10emscripten8internal10getContextIM14MozJpegOptionsiEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %t) #0 comdat {
entry:
  %t.addr = alloca i32*, align 4
  store i32* %t, i32** %t.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 4) #13
  %0 = bitcast i8* %call to i32*
  %1 = load i32*, i32** %t.addr, align 4
  %2 = load i32, i32* %1, align 4
  store i32 %2, i32* %0, align 4
  ret i32* %0
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIvJRKM14MozJpegOptionsiRS2_iEEEPKcPFT_DpT0_E(void (i32*, %struct.MozJpegOptions*, i32)* %0) #8 comdat {
entry:
  %.addr = alloca void (i32*, %struct.MozJpegOptions*, i32)*, align 4
  store void (i32*, %struct.MozJpegOptions*, i32)* %0, void (i32*, %struct.MozJpegOptions*, i32)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJvRKM14MozJpegOptionsiRS2_iEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi(i32* nonnull align 4 dereferenceable(4) %v) #4 comdat {
entry:
  %v.addr = alloca i32*, align 4
  store i32* %v, i32** %v.addr, align 4
  %0 = load i32*, i32** %v.addr, align 4
  %1 = load i32, i32* %0, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %v) #4 comdat {
entry:
  %v.addr = alloca i32, align 4
  store i32 %v, i32* %v.addr, align 4
  %0 = load i32, i32* %v.addr, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal11LightTypeIDIiE3getEv() #4 comdat {
entry:
  ret i8* bitcast (i8** @_ZTIi to i8*)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJiRKM14MozJpegOptionsiRKS2_EEEPKcv() #8 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv() #4 comdat {
entry:
  ret i8* getelementptr inbounds ([4 x i8], [4 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJvRKM14MozJpegOptionsiRS2_iEEEPKcv() #8 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcv() #4 comdat {
entry:
  ret i8* getelementptr inbounds ([5 x i8], [5 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsbE7getWireIS2_EEbRKMS2_bRKT_(i32* nonnull align 4 dereferenceable(4) %field, %struct.MozJpegOptions* nonnull align 4 dereferenceable(44) %ptr) #0 comdat {
entry:
  %field.addr = alloca i32*, align 4
  %ptr.addr = alloca %struct.MozJpegOptions*, align 4
  store i32* %field, i32** %field.addr, align 4
  store %struct.MozJpegOptions* %ptr, %struct.MozJpegOptions** %ptr.addr, align 4
  %0 = load %struct.MozJpegOptions*, %struct.MozJpegOptions** %ptr.addr, align 4
  %1 = load i32*, i32** %field.addr, align 4
  %2 = load i32, i32* %1, align 4
  %3 = bitcast %struct.MozJpegOptions* %0 to i8*
  %memptr.offset = getelementptr inbounds i8, i8* %3, i32 %2
  %4 = load i8, i8* %memptr.offset, align 1
  %tobool = trunc i8 %4 to i1
  %call = call zeroext i1 @_ZN10emscripten8internal11BindingTypeIbvE10toWireTypeEb(i1 zeroext %tobool)
  ret i1 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8internal12MemberAccessI14MozJpegOptionsbE7setWireIS2_EEvRKMS2_bRT_b(i32* nonnull align 4 dereferenceable(4) %field, %struct.MozJpegOptions* nonnull align 4 dereferenceable(44) %ptr, i1 zeroext %value) #0 comdat {
entry:
  %field.addr = alloca i32*, align 4
  %ptr.addr = alloca %struct.MozJpegOptions*, align 4
  %value.addr = alloca i8, align 1
  store i32* %field, i32** %field.addr, align 4
  store %struct.MozJpegOptions* %ptr, %struct.MozJpegOptions** %ptr.addr, align 4
  %frombool = zext i1 %value to i8
  store i8 %frombool, i8* %value.addr, align 1
  %0 = load i8, i8* %value.addr, align 1
  %tobool = trunc i8 %0 to i1
  %call = call zeroext i1 @_ZN10emscripten8internal11BindingTypeIbvE12fromWireTypeEb(i1 zeroext %tobool)
  %1 = load %struct.MozJpegOptions*, %struct.MozJpegOptions** %ptr.addr, align 4
  %2 = load i32*, i32** %field.addr, align 4
  %3 = load i32, i32* %2, align 4
  %4 = bitcast %struct.MozJpegOptions* %1 to i8*
  %memptr.offset = getelementptr inbounds i8, i8* %4, i32 %3
  %frombool1 = zext i1 %call to i8
  store i8 %frombool1, i8* %memptr.offset, align 1
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal6TypeIDIbvE3getEv() #0 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal11LightTypeIDIbE3getEv()
  ret i8* %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIbJRKM14MozJpegOptionsbRKS2_EEEPKcPFT_DpT0_E(i1 (i32*, %struct.MozJpegOptions*)* %0) #8 comdat {
entry:
  %.addr = alloca i1 (i32*, %struct.MozJpegOptions*)*, align 4
  store i1 (i32*, %struct.MozJpegOptions*)* %0, i1 (i32*, %struct.MozJpegOptions*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJbRKM14MozJpegOptionsbRKS2_EEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN10emscripten8internal10getContextIM14MozJpegOptionsbEEPT_RKS4_(i32* nonnull align 4 dereferenceable(4) %t) #0 comdat {
entry:
  %t.addr = alloca i32*, align 4
  store i32* %t, i32** %t.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 4) #13
  %0 = bitcast i8* %call to i32*
  %1 = load i32*, i32** %t.addr, align 4
  %2 = load i32, i32* %1, align 4
  store i32 %2, i32* %0, align 4
  ret i32* %0
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIvJRKM14MozJpegOptionsbRS2_bEEEPKcPFT_DpT0_E(void (i32*, %struct.MozJpegOptions*, i1)* %0) #8 comdat {
entry:
  %.addr = alloca void (i32*, %struct.MozJpegOptions*, i1)*, align 4
  store void (i32*, %struct.MozJpegOptions*, i1)* %0, void (i32*, %struct.MozJpegOptions*, i1)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJvRKM14MozJpegOptionsbRS2_bEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN10emscripten8internal11BindingTypeIbvE10toWireTypeEb(i1 zeroext %b) #4 comdat {
entry:
  %b.addr = alloca i8, align 1
  %frombool = zext i1 %b to i8
  store i8 %frombool, i8* %b.addr, align 1
  %0 = load i8, i8* %b.addr, align 1
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN10emscripten8internal11BindingTypeIbvE12fromWireTypeEb(i1 zeroext %wt) #4 comdat {
entry:
  %wt.addr = alloca i8, align 1
  %frombool = zext i1 %wt to i8
  store i8 %frombool, i8* %wt.addr, align 1
  %0 = load i8, i8* %wt.addr, align 1
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal11LightTypeIDIbE3getEv() #4 comdat {
entry:
  ret i8* bitcast (i8** @_ZTIb to i8*)
}

; Function Attrs: alwaysinline nounwind
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJbRKM14MozJpegOptionsbRKS2_EEEPKcv() #12 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: alwaysinline nounwind
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJvRKM14MozJpegOptionsbRS2_bEEEPKcv() #12 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJviiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN10emscripten8internal7InvokerIiJEE6invokeEPFivE(i32 ()* %fn) #0 comdat {
entry:
  %fn.addr = alloca i32 ()*, align 4
  %ref.tmp = alloca i32, align 4
  store i32 ()* %fn, i32 ()** %fn.addr, align 4
  %0 = load i32 ()*, i32 ()** %fn.addr, align 4
  %call = call i32 %0()
  store i32 %call, i32* %ref.tmp, align 4
  %call1 = call i32 @_ZN10emscripten8internal11BindingTypeIivE10toWireTypeERKi(i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret i32 %call1
}

declare void @_embind_register_function(i8*, i32, i8**, i8*, i8*, i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJiEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.3"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEv()
  ret i8** %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIiJPFivEEEEPKcPFT_DpT0_E(i32 (i32 ()*)* %0) #8 comdat {
entry:
  %.addr = alloca i32 (i32 ()*)*, align 4
  store i32 (i32 ()*)* %0, i32 (i32 ()*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJiPFivEEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEv() #4 comdat {
entry:
  ret i8** getelementptr inbounds ([1 x i8*], [1 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJiEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJiPFivEEEEPKcv() #8 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiEEEPKcv() #4 comdat {
entry:
  ret i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal7InvokerINS_3valEJNSt3__212basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEii14MozJpegOptionsEE6invokeEPFS2_S9_iiSA_EPNS0_11BindingTypeIS9_vEUt_EiiPSA_(void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)* %fn, %struct.anon.5* %args, i32 %args1, i32 %args3, %struct.MozJpegOptions* %args5) #0 comdat {
entry:
  %fn.addr = alloca void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, align 4
  %args.addr = alloca %struct.anon.5*, align 4
  %args.addr2 = alloca i32, align 4
  %args.addr4 = alloca i32, align 4
  %args.addr6 = alloca %struct.MozJpegOptions*, align 4
  %ref.tmp = alloca %"class.emscripten::val", align 4
  %agg.tmp = alloca %"class.std::__2::basic_string", align 4
  %agg.tmp8 = alloca %struct.MozJpegOptions, align 4
  store void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)* %fn, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)** %fn.addr, align 4
  store %struct.anon.5* %args, %struct.anon.5** %args.addr, align 4
  store i32 %args1, i32* %args.addr2, align 4
  store i32 %args3, i32* %args.addr4, align 4
  store %struct.MozJpegOptions* %args5, %struct.MozJpegOptions** %args.addr6, align 4
  %0 = load void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)** %fn.addr, align 4
  %1 = load %struct.anon.5*, %struct.anon.5** %args.addr, align 4
  call void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* sret align 4 %agg.tmp, %struct.anon.5* %1)
  %2 = load i32, i32* %args.addr2, align 4
  %call = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %2)
  %3 = load i32, i32* %args.addr4, align 4
  %call7 = call i32 @_ZN10emscripten8internal11BindingTypeIivE12fromWireTypeEi(i32 %3)
  %4 = load %struct.MozJpegOptions*, %struct.MozJpegOptions** %args.addr6, align 4
  %call9 = call nonnull align 4 dereferenceable(44) %struct.MozJpegOptions* @_ZN10emscripten8internal18GenericBindingTypeI14MozJpegOptionsE12fromWireTypeEPS2_(%struct.MozJpegOptions* %4)
  %5 = bitcast %struct.MozJpegOptions* %agg.tmp8 to i8*
  %6 = bitcast %struct.MozJpegOptions* %call9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 44, i1 false)
  call void %0(%"class.emscripten::val"* sret align 4 %ref.tmp, %"class.std::__2::basic_string"* %agg.tmp, i32 %call, i32 %call7, %struct.MozJpegOptions* byval(%struct.MozJpegOptions) align 4 %agg.tmp8)
  %call10 = call %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call11 = call %"class.emscripten::val"* @_ZN10emscripten3valD2Ev(%"class.emscripten::val"* %ref.tmp) #5
  %call12 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %agg.tmp) #5
  ret %"struct.emscripten::internal::_EM_VAL"* %call10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEE8getCountEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"* %this) #4 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"** %this.addr, align 4
  ret i32 5
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8** @_ZNK10emscripten8internal12WithPoliciesIJEE11ArgTypeListIJNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEE8getTypesEv(%"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"*, align 4
  store %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"* %this, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"** %this.addr, align 4
  %this1 = load %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"*, %"struct.emscripten::internal::WithPolicies<>::ArgTypeList.4"** %this.addr, align 4
  %call = call i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii14MozJpegOptionsEEEE3getEv()
  ret i8** %call
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal12getSignatureIPNS0_7_EM_VALEJPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcPFT_DpT0_E(%"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)* %0) #8 comdat {
entry:
  %.addr = alloca %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)*, align 4
  store %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)* %0, %"struct.emscripten::internal::_EM_VAL"* (void (%"class.emscripten::val"*, %"class.std::__2::basic_string"*, i32, i32, %struct.MozJpegOptions*)*, %struct.anon.5*, i32, i32, %struct.MozJpegOptions*)** %.addr, align 4
  %call = call i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.emscripten::internal::_EM_VAL"* @_ZN10emscripten8internal11BindingTypeINS_3valEvE10toWireTypeERKS2_(%"class.emscripten::val"* nonnull align 4 dereferenceable(4) %v) #0 comdat {
entry:
  %v.addr = alloca %"class.emscripten::val"*, align 4
  store %"class.emscripten::val"* %v, %"class.emscripten::val"** %v.addr, align 4
  %0 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %0, i32 0, i32 0
  %1 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle, align 4
  call void @_emval_incref(%"struct.emscripten::internal::_EM_VAL"* %1)
  %2 = load %"class.emscripten::val"*, %"class.emscripten::val"** %v.addr, align 4
  %handle1 = getelementptr inbounds %"class.emscripten::val", %"class.emscripten::val"* %2, i32 0, i32 0
  %3 = load %"struct.emscripten::internal::_EM_VAL"*, %"struct.emscripten::internal::_EM_VAL"** %handle1, align 4
  ret %"struct.emscripten::internal::_EM_VAL"* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN10emscripten8internal11BindingTypeINSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEEvE12fromWireTypeEPNS9_Ut_E(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, %struct.anon.5* %v) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %v.addr = alloca %struct.anon.5*, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.anon.5* %v, %struct.anon.5** %v.addr, align 4
  %1 = load %struct.anon.5*, %struct.anon.5** %v.addr, align 4
  %data = getelementptr inbounds %struct.anon.5, %struct.anon.5* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [1 x i8], [1 x i8]* %data, i32 0, i32 0
  %2 = load %struct.anon.5*, %struct.anon.5** %v.addr, align 4
  %length = getelementptr inbounds %struct.anon.5, %struct.anon.5* %2, i32 0, i32 0
  %3 = load i32, i32* %length, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm(%"class.std::__2::basic_string"* %agg.result, i8* %arraydecay, i32 %3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(44) %struct.MozJpegOptions* @_ZN10emscripten8internal18GenericBindingTypeI14MozJpegOptionsE12fromWireTypeEPS2_(%struct.MozJpegOptions* %p) #4 comdat {
entry:
  %p.addr = alloca %struct.MozJpegOptions*, align 4
  store %struct.MozJpegOptions* %p, %struct.MozJpegOptions** %p.addr, align 4
  %0 = load %struct.MozJpegOptions*, %struct.MozJpegOptions** %p.addr, align 4
  ret %struct.MozJpegOptions* %0
}

declare void @_emval_incref(%"struct.emscripten::internal::_EM_VAL"*) #2

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2EPKcm(%"class.std::__2::basic_string"* returned %this, i8* %__s, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %2)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #5
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #5
  %call5 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #4 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* returned %this) unnamed_addr #4 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #4 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* %1) #5
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8** @_ZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii14MozJpegOptionsEEEE3getEv() #4 comdat {
entry:
  ret i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZZN10emscripten8internal14ArgArrayGetterINS0_8TypeListIJNS_3valENSt3__212basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEEii14MozJpegOptionsEEEE3getEvE5types, i32 0, i32 0)
}

; Function Attrs: alwaysinline
define linkonce_odr hidden i8* @_ZN10emscripten8internal20getSpecificSignatureIJPNS0_7_EM_VALEPFNS_3valENSt3__212basic_stringIcNS5_11char_traitsIcEENS5_9allocatorIcEEEEii14MozJpegOptionsEPNS0_11BindingTypeISB_vEUt_EiiPSC_EEEPKcv() #8 comdat {
entry:
  %call = call i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcv()
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcv() #4 comdat {
entry:
  ret i8* getelementptr inbounds ([7 x i8], [7 x i8]* @_ZZN10emscripten8internal19getGenericSignatureIJiiiiiiEEEPKcvE9signature, i32 0, i32 0)
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_mozjpeg_enc.cpp() #3 {
entry:
  call void @__cxx_global_var_init.2()
  ret void
}

; Function Attrs: noinline
define internal void @__tls_init() #3 {
entry:
  %0 = load i8, i8* @__tls_guard, align 1
  %guard.uninitialized = icmp eq i8 %0, 0
  br i1 %guard.uninitialized, label %init, label %exit, !prof !2

init:                                             ; preds = %entry
  store i8 1, i8* @__tls_guard, align 1
  call void @__cxx_global_var_init()
  br label %exit

exit:                                             ; preds = %init, %entry
  ret void
}

attributes #0 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { alwaysinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { argmemonly nounwind willreturn writeonly }
attributes #11 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #12 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #13 = { builtin allocsize(0) }
attributes #14 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1023}
