; ModuleID = 'jdtrans.c'
source_filename = "jdtrans.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jvirt_barray_control = type opaque
%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }

; Function Attrs: nounwind
define hidden %struct.jvirt_barray_control** @jpeg_read_coefficients(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca %struct.jvirt_barray_control**, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %retcode = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp eq i32 %1, 202
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @transdecode_master_selection(%struct.jpeg_decompress_struct* %2)
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 5
  store i32 209, i32* %global_state1, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 5
  %5 = load i32, i32* %global_state2, align 4, !tbaa !6
  %cmp3 = icmp eq i32 %5, 209
  br i1 %cmp3, label %if.then4, label %if.end30

if.then4:                                         ; preds = %if.end
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %if.then4
  %6 = bitcast i32* %retcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 2
  %8 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !11
  %cmp5 = icmp ne %struct.jpeg_progress_mgr* %8, null
  br i1 %cmp5, label %if.then6, label %if.end9

if.then6:                                         ; preds = %for.cond
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 2
  %10 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress7, align 8, !tbaa !11
  %progress_monitor = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %10, i32 0, i32 0
  %progress_monitor8 = bitcast {}** %progress_monitor to void (%struct.jpeg_common_struct*)**
  %11 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %progress_monitor8, align 4, !tbaa !12
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = bitcast %struct.jpeg_decompress_struct* %12 to %struct.jpeg_common_struct*
  call void %11(%struct.jpeg_common_struct* %13)
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %for.cond
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 81
  %15 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !15
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %15, i32 0, i32 0
  %16 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !16
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %16(%struct.jpeg_decompress_struct* %17)
  store i32 %call, i32* %retcode, align 4, !tbaa !18
  %18 = load i32, i32* %retcode, align 4, !tbaa !18
  %cmp10 = icmp eq i32 %18, 0
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  store %struct.jvirt_barray_control** null, %struct.jvirt_barray_control*** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end9
  %19 = load i32, i32* %retcode, align 4, !tbaa !18
  %cmp13 = icmp eq i32 %19, 2
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end12
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end15:                                         ; preds = %if.end12
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 2
  %21 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress16, align 8, !tbaa !11
  %cmp17 = icmp ne %struct.jpeg_progress_mgr* %21, null
  br i1 %cmp17, label %land.lhs.true, label %if.end28

land.lhs.true:                                    ; preds = %if.end15
  %22 = load i32, i32* %retcode, align 4, !tbaa !18
  %cmp18 = icmp eq i32 %22, 3
  br i1 %cmp18, label %if.then20, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %23 = load i32, i32* %retcode, align 4, !tbaa !18
  %cmp19 = icmp eq i32 %23, 1
  br i1 %cmp19, label %if.then20, label %if.end28

if.then20:                                        ; preds = %lor.lhs.false, %land.lhs.true
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 2
  %25 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress21, align 8, !tbaa !11
  %pass_counter = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %25, i32 0, i32 1
  %26 = load i32, i32* %pass_counter, align 4, !tbaa !19
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %pass_counter, align 4, !tbaa !19
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 2
  %28 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress22, align 8, !tbaa !11
  %pass_limit = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %28, i32 0, i32 2
  %29 = load i32, i32* %pass_limit, align 4, !tbaa !20
  %cmp23 = icmp sge i32 %inc, %29
  br i1 %cmp23, label %if.then24, label %if.end27

if.then24:                                        ; preds = %if.then20
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 64
  %31 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !21
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 2
  %33 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress25, align 8, !tbaa !11
  %pass_limit26 = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %33, i32 0, i32 2
  %34 = load i32, i32* %pass_limit26, align 4, !tbaa !20
  %add = add nsw i32 %34, %31
  store i32 %add, i32* %pass_limit26, align 4, !tbaa !20
  br label %if.end27

if.end27:                                         ; preds = %if.then24, %if.then20
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %lor.lhs.false, %if.end15
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end28, %if.then14, %if.then11
  %35 = bitcast i32* %retcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond

for.end:                                          ; preds = %cleanup
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 5
  store i32 210, i32* %global_state29, align 4, !tbaa !6
  br label %if.end30

if.end30:                                         ; preds = %for.end, %if.end
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 5
  %38 = load i32, i32* %global_state31, align 4, !tbaa !6
  %cmp32 = icmp eq i32 %38, 210
  br i1 %cmp32, label %land.lhs.true36, label %lor.lhs.false33

lor.lhs.false33:                                  ; preds = %if.end30
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state34 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 5
  %40 = load i32, i32* %global_state34, align 4, !tbaa !6
  %cmp35 = icmp eq i32 %40, 207
  br i1 %cmp35, label %land.lhs.true36, label %if.end38

land.lhs.true36:                                  ; preds = %lor.lhs.false33, %if.end30
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 15
  %42 = load i32, i32* %buffered_image, align 8, !tbaa !22
  %tobool = icmp ne i32 %42, 0
  br i1 %tobool, label %if.then37, label %if.end38

if.then37:                                        ; preds = %land.lhs.true36
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 79
  %44 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef, align 4, !tbaa !23
  %coef_arrays = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %44, i32 0, i32 4
  %45 = load %struct.jvirt_barray_control**, %struct.jvirt_barray_control*** %coef_arrays, align 4, !tbaa !24
  store %struct.jvirt_barray_control** %45, %struct.jvirt_barray_control*** %retval, align 4
  br label %return

if.end38:                                         ; preds = %land.lhs.true36, %lor.lhs.false33
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 0
  %47 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %47, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !27
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state39 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 5
  %49 = load i32, i32* %global_state39, align 4, !tbaa !6
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 0
  %51 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err40, align 8, !tbaa !26
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %51, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %49, i32* %arrayidx, align 4, !tbaa !29
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err41 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 0
  %53 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err41, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %53, i32 0, i32 0
  %54 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %56 = bitcast %struct.jpeg_decompress_struct* %55 to %struct.jpeg_common_struct*
  call void %54(%struct.jpeg_common_struct* %56)
  store %struct.jvirt_barray_control** null, %struct.jvirt_barray_control*** %retval, align 4
  br label %return

return:                                           ; preds = %if.end38, %if.then37, %cleanup
  %57 = load %struct.jvirt_barray_control**, %struct.jvirt_barray_control*** %retval, align 4
  ret %struct.jvirt_barray_control** %57

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define internal void @transdecode_master_selection(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %nscans = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 15
  store i32 1, i32* %buffered_image, align 8, !tbaa !22
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 46
  %2 = load i32, i32* %arith_code, align 8, !tbaa !31
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 5
  store i32 1, i32* %msg_code, align 4, !tbaa !27
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 0
  %7 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = bitcast %struct.jpeg_decompress_struct* %8 to %struct.jpeg_common_struct*
  call void %7(%struct.jpeg_common_struct* %9)
  br label %if.end5

if.else:                                          ; preds = %entry
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 45
  %11 = load i32, i32* %progressive_mode, align 4, !tbaa !32
  %tobool2 = icmp ne i32 %11, 0
  br i1 %tobool2, label %if.then3, label %if.else4

if.then3:                                         ; preds = %if.else
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_phuff_decoder(%struct.jpeg_decompress_struct* %12)
  br label %if.end

if.else4:                                         ; preds = %if.else
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_huff_decoder(%struct.jpeg_decompress_struct* %13)
  br label %if.end

if.end:                                           ; preds = %if.else4, %if.then3
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_d_coef_controller(%struct.jpeg_decompress_struct* %14, i32 1)
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 1
  %16 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !33
  %realize_virt_arrays = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %16, i32 0, i32 6
  %realize_virt_arrays6 = bitcast {}** %realize_virt_arrays to void (%struct.jpeg_common_struct*)**
  %17 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %realize_virt_arrays6, align 4, !tbaa !34
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = bitcast %struct.jpeg_decompress_struct* %18 to %struct.jpeg_common_struct*
  call void %17(%struct.jpeg_common_struct* %19)
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 81
  %21 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !15
  %start_input_pass = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %21, i32 0, i32 2
  %22 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %start_input_pass, align 4, !tbaa !36
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %22(%struct.jpeg_decompress_struct* %23)
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 2
  %25 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !11
  %cmp = icmp ne %struct.jpeg_progress_mgr* %25, null
  br i1 %cmp, label %if.then7, label %if.end24

if.then7:                                         ; preds = %if.end5
  %26 = bitcast i32* %nscans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 45
  %28 = load i32, i32* %progressive_mode8, align 4, !tbaa !32
  %tobool9 = icmp ne i32 %28, 0
  br i1 %tobool9, label %if.then10, label %if.else11

if.then10:                                        ; preds = %if.then7
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 9
  %30 = load i32, i32* %num_components, align 4, !tbaa !37
  %mul = mul nsw i32 3, %30
  %add = add nsw i32 2, %mul
  store i32 %add, i32* %nscans, align 4, !tbaa !18
  br label %if.end18

if.else11:                                        ; preds = %if.then7
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 81
  %32 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl12, align 4, !tbaa !15
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %32, i32 0, i32 4
  %33 = load i32, i32* %has_multiple_scans, align 4, !tbaa !38
  %tobool13 = icmp ne i32 %33, 0
  br i1 %tobool13, label %if.then14, label %if.else16

if.then14:                                        ; preds = %if.else11
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 9
  %35 = load i32, i32* %num_components15, align 4, !tbaa !37
  store i32 %35, i32* %nscans, align 4, !tbaa !18
  br label %if.end17

if.else16:                                        ; preds = %if.else11
  store i32 1, i32* %nscans, align 4, !tbaa !18
  br label %if.end17

if.end17:                                         ; preds = %if.else16, %if.then14
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.then10
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 2
  %37 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress19, align 8, !tbaa !11
  %pass_counter = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %37, i32 0, i32 1
  store i32 0, i32* %pass_counter, align 4, !tbaa !19
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 64
  %39 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !21
  %40 = load i32, i32* %nscans, align 4, !tbaa !18
  %mul20 = mul nsw i32 %39, %40
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 2
  %42 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress21, align 8, !tbaa !11
  %pass_limit = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %42, i32 0, i32 2
  store i32 %mul20, i32* %pass_limit, align 4, !tbaa !20
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 2
  %44 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress22, align 8, !tbaa !11
  %completed_passes = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %44, i32 0, i32 3
  store i32 0, i32* %completed_passes, align 4, !tbaa !39
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress23 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 2
  %46 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress23, align 8, !tbaa !11
  %total_passes = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %46, i32 0, i32 4
  store i32 1, i32* %total_passes, align 4, !tbaa !40
  %47 = bitcast i32* %nscans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  br label %if.end24

if.end24:                                         ; preds = %if.end18, %if.end5
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @jinit_phuff_decoder(%struct.jpeg_decompress_struct*) #2

declare void @jinit_huff_decoder(%struct.jpeg_decompress_struct*) #2

declare void @jinit_d_coef_controller(%struct.jpeg_decompress_struct*, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 20}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!7, !3, i64 8}
!12 = !{!13, !3, i64 0}
!13 = !{!"jpeg_progress_mgr", !3, i64 0, !14, i64 4, !14, i64 8, !8, i64 12, !8, i64 16}
!14 = !{!"long", !4, i64 0}
!15 = !{!7, !3, i64 436}
!16 = !{!17, !3, i64 0}
!17 = !{!"jpeg_input_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20}
!18 = !{!8, !8, i64 0}
!19 = !{!13, !14, i64 4}
!20 = !{!13, !14, i64 8}
!21 = !{!7, !8, i64 320}
!22 = !{!7, !8, i64 64}
!23 = !{!7, !3, i64 428}
!24 = !{!25, !3, i64 16}
!25 = !{!"jpeg_d_coef_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!26 = !{!7, !3, i64 0}
!27 = !{!28, !8, i64 20}
!28 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !14, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!29 = !{!4, !4, i64 0}
!30 = !{!28, !3, i64 0}
!31 = !{!7, !8, i64 224}
!32 = !{!7, !8, i64 220}
!33 = !{!7, !3, i64 4}
!34 = !{!35, !3, i64 24}
!35 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!36 = !{!17, !3, i64 8}
!37 = !{!7, !8, i64 36}
!38 = !{!17, !8, i64 16}
!39 = !{!13, !8, i64 12}
!40 = !{!13, !8, i64 16}
