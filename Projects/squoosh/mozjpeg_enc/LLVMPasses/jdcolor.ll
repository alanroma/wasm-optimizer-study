; ModuleID = 'jdcolor.c'
source_filename = "jdcolor.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.my_color_deconverter = type { %struct.jpeg_color_deconverter, i32*, i32*, i32*, i32*, i32* }

@rgb_pixelsize = internal constant [17 x i32] [i32 -1, i32 -1, i32 3, i32 -1, i32 -1, i32 -1, i32 3, i32 4, i32 3, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 -1], align 16
@rgb_red = internal constant [17 x i32] [i32 -1, i32 -1, i32 0, i32 -1, i32 -1, i32 -1, i32 0, i32 0, i32 2, i32 2, i32 3, i32 1, i32 0, i32 2, i32 3, i32 1, i32 -1], align 16
@rgb_green = internal constant [17 x i32] [i32 -1, i32 -1, i32 1, i32 -1, i32 -1, i32 -1, i32 1, i32 1, i32 1, i32 1, i32 2, i32 2, i32 1, i32 1, i32 2, i32 2, i32 -1], align 16
@rgb_blue = internal constant [17 x i32] [i32 -1, i32 -1, i32 2, i32 -1, i32 -1, i32 -1, i32 2, i32 2, i32 0, i32 0, i32 1, i32 3, i32 2, i32 0, i32 1, i32 3, i32 -1], align 16
@dither_matrix = internal constant [4 x i32] [i32 524810, i32 201592326, i32 51052809, i32 252120325], align 16

; Function Attrs: nounwind
define hidden void @jinit_color_deconverter(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %ci = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 1
  %3 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %3, i32 0, i32 0
  %4 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = bitcast %struct.jpeg_decompress_struct* %5 to %struct.jpeg_common_struct*
  %call = call i8* %4(%struct.jpeg_common_struct* %6, i32 1, i32 28)
  %7 = bitcast i8* %call to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %7, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %8 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %9 = bitcast %struct.my_color_deconverter* %8 to %struct.jpeg_color_deconverter*
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 86
  store %struct.jpeg_color_deconverter* %9, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %11 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %11, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub, i32 0, i32 0
  %start_pass2 = bitcast {}** %start_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @start_pass_dcolor, void (%struct.jpeg_decompress_struct*)** %start_pass2, align 4, !tbaa !15
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 10
  %13 = load i32, i32* %jpeg_color_space, align 8, !tbaa !18
  switch i32 %13, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb4
    i32 3, label %sw.bb4
    i32 4, label %sw.bb13
    i32 5, label %sw.bb13
  ]

sw.bb:                                            ; preds = %entry
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 9
  %15 = load i32, i32* %num_components, align 4, !tbaa !19
  %cmp = icmp ne i32 %15, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %sw.bb
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !20
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 5
  store i32 10, i32* %msg_code, align 4, !tbaa !21
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !20
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 0
  %20 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !23
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %22 = bitcast %struct.jpeg_decompress_struct* %21 to %struct.jpeg_common_struct*
  call void %20(%struct.jpeg_common_struct* %22)
  br label %if.end

if.end:                                           ; preds = %if.then, %sw.bb
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 9
  %24 = load i32, i32* %num_components5, align 4, !tbaa !19
  %cmp6 = icmp ne i32 %24, 3
  br i1 %cmp6, label %if.then7, label %if.end12

if.then7:                                         ; preds = %sw.bb4
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 0
  %26 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !20
  %msg_code9 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %26, i32 0, i32 5
  store i32 10, i32* %msg_code9, align 4, !tbaa !21
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !20
  %error_exit11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 0
  %29 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit11, align 4, !tbaa !23
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_decompress_struct* %30 to %struct.jpeg_common_struct*
  call void %29(%struct.jpeg_common_struct* %31)
  br label %if.end12

if.end12:                                         ; preds = %if.then7, %sw.bb4
  br label %sw.epilog

sw.bb13:                                          ; preds = %entry, %entry
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 9
  %33 = load i32, i32* %num_components14, align 4, !tbaa !19
  %cmp15 = icmp ne i32 %33, 4
  br i1 %cmp15, label %if.then16, label %if.end21

if.then16:                                        ; preds = %sw.bb13
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err17, align 8, !tbaa !20
  %msg_code18 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 5
  store i32 10, i32* %msg_code18, align 4, !tbaa !21
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err19, align 8, !tbaa !20
  %error_exit20 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 0
  %38 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit20, align 4, !tbaa !23
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = bitcast %struct.jpeg_decompress_struct* %39 to %struct.jpeg_common_struct*
  call void %38(%struct.jpeg_common_struct* %40)
  br label %if.end21

if.end21:                                         ; preds = %if.then16, %sw.bb13
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 9
  %42 = load i32, i32* %num_components22, align 4, !tbaa !19
  %cmp23 = icmp slt i32 %42, 1
  br i1 %cmp23, label %if.then24, label %if.end29

if.then24:                                        ; preds = %sw.default
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 0
  %44 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err25, align 8, !tbaa !20
  %msg_code26 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %44, i32 0, i32 5
  store i32 10, i32* %msg_code26, align 4, !tbaa !21
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 0
  %46 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err27, align 8, !tbaa !20
  %error_exit28 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %46, i32 0, i32 0
  %47 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit28, align 4, !tbaa !23
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %49 = bitcast %struct.jpeg_decompress_struct* %48 to %struct.jpeg_common_struct*
  call void %47(%struct.jpeg_common_struct* %49)
  br label %if.end29

if.end29:                                         ; preds = %if.then24, %sw.default
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end29, %if.end21, %if.end12, %if.end
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 11
  %51 = load i32, i32* %out_color_space, align 4, !tbaa !24
  switch i32 %51, label %sw.default188 [
    i32 1, label %sw.bb30
    i32 2, label %sw.bb51
    i32 6, label %sw.bb51
    i32 7, label %sw.bb51
    i32 8, label %sw.bb51
    i32 9, label %sw.bb51
    i32 10, label %sw.bb51
    i32 11, label %sw.bb51
    i32 12, label %sw.bb51
    i32 13, label %sw.bb51
    i32 14, label %sw.bb51
    i32 15, label %sw.bb51
    i32 16, label %sw.bb105
    i32 4, label %sw.bb168
  ]

sw.bb30:                                          ; preds = %sw.epilog
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 29
  store i32 1, i32* %out_color_components, align 8, !tbaa !25
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 10
  %54 = load i32, i32* %jpeg_color_space31, align 8, !tbaa !18
  %cmp32 = icmp eq i32 %54, 1
  br i1 %cmp32, label %if.then35, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %sw.bb30
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space33 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 10
  %56 = load i32, i32* %jpeg_color_space33, align 8, !tbaa !18
  %cmp34 = icmp eq i32 %56, 3
  br i1 %cmp34, label %if.then35, label %if.else

if.then35:                                        ; preds = %lor.lhs.false, %sw.bb30
  %57 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub36 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %57, i32 0, i32 0
  %color_convert = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub36, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @grayscale_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert, align 4, !tbaa !26
  store i32 1, i32* %ci, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then35
  %58 = load i32, i32* %ci, align 4, !tbaa !27
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components37 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 9
  %60 = load i32, i32* %num_components37, align 4, !tbaa !19
  %cmp38 = icmp slt i32 %58, %60
  br i1 %cmp38, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 44
  %62 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !28
  %63 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %62, i32 %63
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx, i32 0, i32 12
  store i32 0, i32* %component_needed, align 4, !tbaa !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %64 = load i32, i32* %ci, align 4, !tbaa !27
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end50

if.else:                                          ; preds = %lor.lhs.false
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space39 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 10
  %66 = load i32, i32* %jpeg_color_space39, align 8, !tbaa !18
  %cmp40 = icmp eq i32 %66, 2
  br i1 %cmp40, label %if.then41, label %if.else44

if.then41:                                        ; preds = %if.else
  %67 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub42 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %67, i32 0, i32 0
  %color_convert43 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub42, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @rgb_gray_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert43, align 4, !tbaa !26
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @build_rgb_y_table(%struct.jpeg_decompress_struct* %68)
  br label %if.end49

if.else44:                                        ; preds = %if.else
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err45 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %69, i32 0, i32 0
  %70 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err45, align 8, !tbaa !20
  %msg_code46 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %70, i32 0, i32 5
  store i32 27, i32* %msg_code46, align 4, !tbaa !21
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err47 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 0
  %72 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err47, align 8, !tbaa !20
  %error_exit48 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %72, i32 0, i32 0
  %73 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit48, align 4, !tbaa !23
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %75 = bitcast %struct.jpeg_decompress_struct* %74 to %struct.jpeg_common_struct*
  call void %73(%struct.jpeg_common_struct* %75)
  br label %if.end49

if.end49:                                         ; preds = %if.else44, %if.then41
  br label %if.end50

if.end50:                                         ; preds = %if.end49, %for.end
  br label %sw.epilog203

sw.bb51:                                          ; preds = %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog, %sw.epilog
  %76 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space52 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %76, i32 0, i32 11
  %77 = load i32, i32* %out_color_space52, align 4, !tbaa !24
  %arrayidx53 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_pixelsize, i32 0, i32 %77
  %78 = load i32, i32* %arrayidx53, align 4, !tbaa !27
  %79 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %79, i32 0, i32 29
  store i32 %78, i32* %out_color_components54, align 8, !tbaa !25
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space55 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %80, i32 0, i32 10
  %81 = load i32, i32* %jpeg_color_space55, align 8, !tbaa !18
  %cmp56 = icmp eq i32 %81, 3
  br i1 %cmp56, label %if.then57, label %if.else66

if.then57:                                        ; preds = %sw.bb51
  %call58 = call i32 @jsimd_can_ycc_rgb()
  %tobool = icmp ne i32 %call58, 0
  br i1 %tobool, label %if.then59, label %if.else62

if.then59:                                        ; preds = %if.then57
  %82 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub60 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %82, i32 0, i32 0
  %color_convert61 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub60, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @jsimd_ycc_rgb_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert61, align 4, !tbaa !26
  br label %if.end65

if.else62:                                        ; preds = %if.then57
  %83 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub63 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %83, i32 0, i32 0
  %color_convert64 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub63, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @ycc_rgb_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert64, align 4, !tbaa !26
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @build_ycc_rgb_table(%struct.jpeg_decompress_struct* %84)
  br label %if.end65

if.end65:                                         ; preds = %if.else62, %if.then59
  br label %if.end104

if.else66:                                        ; preds = %sw.bb51
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 10
  %86 = load i32, i32* %jpeg_color_space67, align 8, !tbaa !18
  %cmp68 = icmp eq i32 %86, 1
  br i1 %cmp68, label %if.then69, label %if.else72

if.then69:                                        ; preds = %if.else66
  %87 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub70 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %87, i32 0, i32 0
  %color_convert71 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub70, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @gray_rgb_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert71, align 4, !tbaa !26
  br label %if.end103

if.else72:                                        ; preds = %if.else66
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space73 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %88, i32 0, i32 10
  %89 = load i32, i32* %jpeg_color_space73, align 8, !tbaa !18
  %cmp74 = icmp eq i32 %89, 2
  br i1 %cmp74, label %if.then75, label %if.else97

if.then75:                                        ; preds = %if.else72
  %90 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space76 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %90, i32 0, i32 11
  %91 = load i32, i32* %out_color_space76, align 4, !tbaa !24
  %arrayidx77 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %91
  %92 = load i32, i32* %arrayidx77, align 4, !tbaa !27
  %cmp78 = icmp eq i32 %92, 0
  br i1 %cmp78, label %land.lhs.true, label %if.else93

land.lhs.true:                                    ; preds = %if.then75
  %93 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space79 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %93, i32 0, i32 11
  %94 = load i32, i32* %out_color_space79, align 4, !tbaa !24
  %arrayidx80 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %94
  %95 = load i32, i32* %arrayidx80, align 4, !tbaa !27
  %cmp81 = icmp eq i32 %95, 1
  br i1 %cmp81, label %land.lhs.true82, label %if.else93

land.lhs.true82:                                  ; preds = %land.lhs.true
  %96 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space83 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %96, i32 0, i32 11
  %97 = load i32, i32* %out_color_space83, align 4, !tbaa !24
  %arrayidx84 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %97
  %98 = load i32, i32* %arrayidx84, align 4, !tbaa !27
  %cmp85 = icmp eq i32 %98, 2
  br i1 %cmp85, label %land.lhs.true86, label %if.else93

land.lhs.true86:                                  ; preds = %land.lhs.true82
  %99 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space87 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %99, i32 0, i32 11
  %100 = load i32, i32* %out_color_space87, align 4, !tbaa !24
  %arrayidx88 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_pixelsize, i32 0, i32 %100
  %101 = load i32, i32* %arrayidx88, align 4, !tbaa !27
  %cmp89 = icmp eq i32 %101, 3
  br i1 %cmp89, label %if.then90, label %if.else93

if.then90:                                        ; preds = %land.lhs.true86
  %102 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub91 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %102, i32 0, i32 0
  %color_convert92 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub91, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @null_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert92, align 4, !tbaa !26
  br label %if.end96

if.else93:                                        ; preds = %land.lhs.true86, %land.lhs.true82, %land.lhs.true, %if.then75
  %103 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub94 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %103, i32 0, i32 0
  %color_convert95 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub94, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @rgb_rgb_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert95, align 4, !tbaa !26
  br label %if.end96

if.end96:                                         ; preds = %if.else93, %if.then90
  br label %if.end102

if.else97:                                        ; preds = %if.else72
  %104 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err98 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %104, i32 0, i32 0
  %105 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err98, align 8, !tbaa !20
  %msg_code99 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %105, i32 0, i32 5
  store i32 27, i32* %msg_code99, align 4, !tbaa !21
  %106 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err100 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %106, i32 0, i32 0
  %107 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err100, align 8, !tbaa !20
  %error_exit101 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %107, i32 0, i32 0
  %108 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit101, align 4, !tbaa !23
  %109 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %110 = bitcast %struct.jpeg_decompress_struct* %109 to %struct.jpeg_common_struct*
  call void %108(%struct.jpeg_common_struct* %110)
  br label %if.end102

if.end102:                                        ; preds = %if.else97, %if.end96
  br label %if.end103

if.end103:                                        ; preds = %if.end102, %if.then69
  br label %if.end104

if.end104:                                        ; preds = %if.end103, %if.end65
  br label %sw.epilog203

sw.bb105:                                         ; preds = %sw.epilog
  %111 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components106 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %111, i32 0, i32 29
  store i32 3, i32* %out_color_components106, align 8, !tbaa !25
  %112 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %112, i32 0, i32 21
  %113 = load i32, i32* %dither_mode, align 8, !tbaa !31
  %cmp107 = icmp eq i32 %113, 0
  br i1 %cmp107, label %if.then108, label %if.else141

if.then108:                                       ; preds = %sw.bb105
  %114 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space109 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %114, i32 0, i32 10
  %115 = load i32, i32* %jpeg_color_space109, align 8, !tbaa !18
  %cmp110 = icmp eq i32 %115, 3
  br i1 %cmp110, label %if.then111, label %if.else121

if.then111:                                       ; preds = %if.then108
  %call112 = call i32 @jsimd_can_ycc_rgb565()
  %tobool113 = icmp ne i32 %call112, 0
  br i1 %tobool113, label %if.then114, label %if.else117

if.then114:                                       ; preds = %if.then111
  %116 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub115 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %116, i32 0, i32 0
  %color_convert116 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub115, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @jsimd_ycc_rgb565_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert116, align 4, !tbaa !26
  br label %if.end120

if.else117:                                       ; preds = %if.then111
  %117 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub118 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %117, i32 0, i32 0
  %color_convert119 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub118, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @ycc_rgb565_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert119, align 4, !tbaa !26
  %118 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @build_ycc_rgb_table(%struct.jpeg_decompress_struct* %118)
  br label %if.end120

if.end120:                                        ; preds = %if.else117, %if.then114
  br label %if.end140

if.else121:                                       ; preds = %if.then108
  %119 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space122 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %119, i32 0, i32 10
  %120 = load i32, i32* %jpeg_color_space122, align 8, !tbaa !18
  %cmp123 = icmp eq i32 %120, 1
  br i1 %cmp123, label %if.then124, label %if.else127

if.then124:                                       ; preds = %if.else121
  %121 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub125 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %121, i32 0, i32 0
  %color_convert126 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub125, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @gray_rgb565_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert126, align 4, !tbaa !26
  br label %if.end139

if.else127:                                       ; preds = %if.else121
  %122 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space128 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %122, i32 0, i32 10
  %123 = load i32, i32* %jpeg_color_space128, align 8, !tbaa !18
  %cmp129 = icmp eq i32 %123, 2
  br i1 %cmp129, label %if.then130, label %if.else133

if.then130:                                       ; preds = %if.else127
  %124 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub131 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %124, i32 0, i32 0
  %color_convert132 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub131, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @rgb_rgb565_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert132, align 4, !tbaa !26
  br label %if.end138

if.else133:                                       ; preds = %if.else127
  %125 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err134 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %125, i32 0, i32 0
  %126 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err134, align 8, !tbaa !20
  %msg_code135 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %126, i32 0, i32 5
  store i32 27, i32* %msg_code135, align 4, !tbaa !21
  %127 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err136 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %127, i32 0, i32 0
  %128 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err136, align 8, !tbaa !20
  %error_exit137 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %128, i32 0, i32 0
  %129 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit137, align 4, !tbaa !23
  %130 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %131 = bitcast %struct.jpeg_decompress_struct* %130 to %struct.jpeg_common_struct*
  call void %129(%struct.jpeg_common_struct* %131)
  br label %if.end138

if.end138:                                        ; preds = %if.else133, %if.then130
  br label %if.end139

if.end139:                                        ; preds = %if.end138, %if.then124
  br label %if.end140

if.end140:                                        ; preds = %if.end139, %if.end120
  br label %if.end167

if.else141:                                       ; preds = %sw.bb105
  %132 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space142 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %132, i32 0, i32 10
  %133 = load i32, i32* %jpeg_color_space142, align 8, !tbaa !18
  %cmp143 = icmp eq i32 %133, 3
  br i1 %cmp143, label %if.then144, label %if.else147

if.then144:                                       ; preds = %if.else141
  %134 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub145 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %134, i32 0, i32 0
  %color_convert146 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub145, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @ycc_rgb565D_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert146, align 4, !tbaa !26
  %135 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @build_ycc_rgb_table(%struct.jpeg_decompress_struct* %135)
  br label %if.end166

if.else147:                                       ; preds = %if.else141
  %136 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space148 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %136, i32 0, i32 10
  %137 = load i32, i32* %jpeg_color_space148, align 8, !tbaa !18
  %cmp149 = icmp eq i32 %137, 1
  br i1 %cmp149, label %if.then150, label %if.else153

if.then150:                                       ; preds = %if.else147
  %138 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub151 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %138, i32 0, i32 0
  %color_convert152 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub151, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @gray_rgb565D_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert152, align 4, !tbaa !26
  br label %if.end165

if.else153:                                       ; preds = %if.else147
  %139 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space154 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %139, i32 0, i32 10
  %140 = load i32, i32* %jpeg_color_space154, align 8, !tbaa !18
  %cmp155 = icmp eq i32 %140, 2
  br i1 %cmp155, label %if.then156, label %if.else159

if.then156:                                       ; preds = %if.else153
  %141 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub157 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %141, i32 0, i32 0
  %color_convert158 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub157, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @rgb_rgb565D_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert158, align 4, !tbaa !26
  br label %if.end164

if.else159:                                       ; preds = %if.else153
  %142 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err160 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %142, i32 0, i32 0
  %143 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err160, align 8, !tbaa !20
  %msg_code161 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %143, i32 0, i32 5
  store i32 27, i32* %msg_code161, align 4, !tbaa !21
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err162 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 0
  %145 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err162, align 8, !tbaa !20
  %error_exit163 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %145, i32 0, i32 0
  %146 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit163, align 4, !tbaa !23
  %147 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %148 = bitcast %struct.jpeg_decompress_struct* %147 to %struct.jpeg_common_struct*
  call void %146(%struct.jpeg_common_struct* %148)
  br label %if.end164

if.end164:                                        ; preds = %if.else159, %if.then156
  br label %if.end165

if.end165:                                        ; preds = %if.end164, %if.then150
  br label %if.end166

if.end166:                                        ; preds = %if.end165, %if.then144
  br label %if.end167

if.end167:                                        ; preds = %if.end166, %if.end140
  br label %sw.epilog203

sw.bb168:                                         ; preds = %sw.epilog
  %149 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components169 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %149, i32 0, i32 29
  store i32 4, i32* %out_color_components169, align 8, !tbaa !25
  %150 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space170 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %150, i32 0, i32 10
  %151 = load i32, i32* %jpeg_color_space170, align 8, !tbaa !18
  %cmp171 = icmp eq i32 %151, 5
  br i1 %cmp171, label %if.then172, label %if.else175

if.then172:                                       ; preds = %sw.bb168
  %152 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub173 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %152, i32 0, i32 0
  %color_convert174 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub173, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @ycck_cmyk_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert174, align 4, !tbaa !26
  %153 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @build_ycc_rgb_table(%struct.jpeg_decompress_struct* %153)
  br label %if.end187

if.else175:                                       ; preds = %sw.bb168
  %154 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space176 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %154, i32 0, i32 10
  %155 = load i32, i32* %jpeg_color_space176, align 8, !tbaa !18
  %cmp177 = icmp eq i32 %155, 4
  br i1 %cmp177, label %if.then178, label %if.else181

if.then178:                                       ; preds = %if.else175
  %156 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub179 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %156, i32 0, i32 0
  %color_convert180 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub179, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @null_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert180, align 4, !tbaa !26
  br label %if.end186

if.else181:                                       ; preds = %if.else175
  %157 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err182 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %157, i32 0, i32 0
  %158 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err182, align 8, !tbaa !20
  %msg_code183 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %158, i32 0, i32 5
  store i32 27, i32* %msg_code183, align 4, !tbaa !21
  %159 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err184 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %159, i32 0, i32 0
  %160 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err184, align 8, !tbaa !20
  %error_exit185 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %160, i32 0, i32 0
  %161 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit185, align 4, !tbaa !23
  %162 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %163 = bitcast %struct.jpeg_decompress_struct* %162 to %struct.jpeg_common_struct*
  call void %161(%struct.jpeg_common_struct* %163)
  br label %if.end186

if.end186:                                        ; preds = %if.else181, %if.then178
  br label %if.end187

if.end187:                                        ; preds = %if.end186, %if.then172
  br label %sw.epilog203

sw.default188:                                    ; preds = %sw.epilog
  %164 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space189 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %164, i32 0, i32 11
  %165 = load i32, i32* %out_color_space189, align 4, !tbaa !24
  %166 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space190 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %166, i32 0, i32 10
  %167 = load i32, i32* %jpeg_color_space190, align 8, !tbaa !18
  %cmp191 = icmp eq i32 %165, %167
  br i1 %cmp191, label %if.then192, label %if.else197

if.then192:                                       ; preds = %sw.default188
  %168 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components193 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %168, i32 0, i32 9
  %169 = load i32, i32* %num_components193, align 4, !tbaa !19
  %170 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components194 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %170, i32 0, i32 29
  store i32 %169, i32* %out_color_components194, align 8, !tbaa !25
  %171 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %pub195 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %171, i32 0, i32 0
  %color_convert196 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %pub195, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @null_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert196, align 4, !tbaa !26
  br label %if.end202

if.else197:                                       ; preds = %sw.default188
  %172 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err198 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %172, i32 0, i32 0
  %173 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err198, align 8, !tbaa !20
  %msg_code199 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %173, i32 0, i32 5
  store i32 27, i32* %msg_code199, align 4, !tbaa !21
  %174 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err200 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %174, i32 0, i32 0
  %175 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err200, align 8, !tbaa !20
  %error_exit201 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %175, i32 0, i32 0
  %176 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit201, align 4, !tbaa !23
  %177 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %178 = bitcast %struct.jpeg_decompress_struct* %177 to %struct.jpeg_common_struct*
  call void %176(%struct.jpeg_common_struct* %178)
  br label %if.end202

if.end202:                                        ; preds = %if.else197, %if.then192
  br label %sw.epilog203

sw.epilog203:                                     ; preds = %if.end202, %if.end187, %if.end167, %if.end104, %if.end50
  %179 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %179, i32 0, i32 20
  %180 = load i32, i32* %quantize_colors, align 4, !tbaa !32
  %tobool204 = icmp ne i32 %180, 0
  br i1 %tobool204, label %if.then205, label %if.else206

if.then205:                                       ; preds = %sw.epilog203
  %181 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %181, i32 0, i32 30
  store i32 1, i32* %output_components, align 4, !tbaa !33
  br label %if.end209

if.else206:                                       ; preds = %sw.epilog203
  %182 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components207 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %182, i32 0, i32 29
  %183 = load i32, i32* %out_color_components207, align 8, !tbaa !25
  %184 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_components208 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %184, i32 0, i32 30
  store i32 %183, i32* %output_components208, align 4, !tbaa !33
  br label %if.end209

if.end209:                                        ; preds = %if.else206, %if.then205
  %185 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #4
  %186 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_dcolor(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal void @grayscale_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %0, i32 0
  %1 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %2 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 27
  %6 = load i32, i32* %output_width, align 8, !tbaa !34
  call void @jcopy_sample_rows(i8** %1, i32 %2, i8** %3, i32 0, i32 %4, i32 %6)
  ret void
}

; Function Attrs: nounwind
define internal void @rgb_gray_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %ctab = alloca i32*, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %rgb_y_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %8, i32 0, i32 5
  %9 = load i32*, i32** %rgb_y_tab, align 4, !tbaa !35
  store i32* %9, i32** %ctab, align 4, !tbaa !2
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 27
  %17 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %17, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %18 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %19 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %19, i32 0
  %20 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %21 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %20, i32 %21
  %22 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %22, i8** %inptr0, align 4, !tbaa !2
  %23 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %23, i32 1
  %24 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %25 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %24, i32 %25
  %26 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %26, i8** %inptr1, align 4, !tbaa !2
  %27 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %27, i32 2
  %28 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %29 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %28, i32 %29
  %30 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %30, i8** %inptr2, align 4, !tbaa !2
  %31 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %31, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %32 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %32, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %33 = load i8*, i8** %32, align 4, !tbaa !2
  store i8* %33, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %34 = load i32, i32* %col, align 4, !tbaa !27
  %35 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp7 = icmp ult i32 %34, %35
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %37 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %36, i32 %37
  %38 = load i8, i8* %arrayidx8, align 1, !tbaa !36
  %conv = zext i8 %38 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !27
  %39 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %40 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %39, i32 %40
  %41 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %conv10 = zext i8 %41 to i32
  store i32 %conv10, i32* %g, align 4, !tbaa !27
  %42 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %43 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %42, i32 %43
  %44 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %44 to i32
  store i32 %conv12, i32* %b, align 4, !tbaa !27
  %45 = load i32*, i32** %ctab, align 4, !tbaa !2
  %46 = load i32, i32* %r, align 4, !tbaa !27
  %add = add nsw i32 %46, 0
  %arrayidx13 = getelementptr inbounds i32, i32* %45, i32 %add
  %47 = load i32, i32* %arrayidx13, align 4, !tbaa !37
  %48 = load i32*, i32** %ctab, align 4, !tbaa !2
  %49 = load i32, i32* %g, align 4, !tbaa !27
  %add14 = add nsw i32 %49, 256
  %arrayidx15 = getelementptr inbounds i32, i32* %48, i32 %add14
  %50 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %add16 = add nsw i32 %47, %50
  %51 = load i32*, i32** %ctab, align 4, !tbaa !2
  %52 = load i32, i32* %b, align 4, !tbaa !27
  %add17 = add nsw i32 %52, 512
  %arrayidx18 = getelementptr inbounds i32, i32* %51, i32 %add17
  %53 = load i32, i32* %arrayidx18, align 4, !tbaa !37
  %add19 = add nsw i32 %add16, %53
  %shr = ashr i32 %add19, 16
  %conv20 = trunc i32 %shr to i8
  %54 = load i8*, i8** %outptr, align 4, !tbaa !2
  %55 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx21 = getelementptr inbounds i8, i8* %54, i32 %55
  store i8 %conv20, i8* %arrayidx21, align 1, !tbaa !36
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %56 = load i32, i32* %col, align 4, !tbaa !27
  %inc22 = add i32 %56, 1
  store i32 %inc22, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %57 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  %60 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %61 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %62 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  %63 = bitcast i32** %ctab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  %64 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #4
  %65 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  %66 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #4
  %67 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #4
  ret void
}

; Function Attrs: nounwind
define internal void @build_rgb_y_table(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %rgb_y_tab = alloca i32*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32** %rgb_y_tab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 1
  %7 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %7, i32 0, i32 0
  %8 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %10 = bitcast %struct.jpeg_decompress_struct* %9 to %struct.jpeg_common_struct*
  %call = call i8* %8(%struct.jpeg_common_struct* %10, i32 1, i32 3072)
  %11 = bitcast i8* %call to i32*
  store i32* %11, i32** %rgb_y_tab, align 4, !tbaa !2
  %12 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %rgb_y_tab2 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %12, i32 0, i32 5
  store i32* %11, i32** %rgb_y_tab2, align 4, !tbaa !35
  store i32 0, i32* %i, align 4, !tbaa !37
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i32, i32* %i, align 4, !tbaa !37
  %cmp = icmp sle i32 %13, 255
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i32, i32* %i, align 4, !tbaa !37
  %mul = mul nsw i32 19595, %14
  %15 = load i32*, i32** %rgb_y_tab, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !37
  %add = add nsw i32 %16, 0
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 %add
  store i32 %mul, i32* %arrayidx, align 4, !tbaa !37
  %17 = load i32, i32* %i, align 4, !tbaa !37
  %mul3 = mul nsw i32 38470, %17
  %18 = load i32*, i32** %rgb_y_tab, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !37
  %add4 = add nsw i32 %19, 256
  %arrayidx5 = getelementptr inbounds i32, i32* %18, i32 %add4
  store i32 %mul3, i32* %arrayidx5, align 4, !tbaa !37
  %20 = load i32, i32* %i, align 4, !tbaa !37
  %mul6 = mul nsw i32 7471, %20
  %add7 = add nsw i32 %mul6, 32768
  %21 = load i32*, i32** %rgb_y_tab, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !37
  %add8 = add nsw i32 %22, 512
  %arrayidx9 = getelementptr inbounds i32, i32* %21, i32 %add8
  store i32 %add7, i32* %arrayidx9, align 4, !tbaa !37
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %i, align 4, !tbaa !37
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4, !tbaa !37
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i32** %rgb_y_tab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  ret void
}

declare i32 @jsimd_can_ycc_rgb() #2

declare void @jsimd_ycc_rgb_convert(%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32) #2

; Function Attrs: nounwind
define internal void @ycc_rgb_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 11
  %1 = load i32, i32* %out_color_space, align 4, !tbaa !24
  switch i32 %1, label %sw.default [
    i32 6, label %sw.bb
    i32 7, label %sw.bb1
    i32 12, label %sw.bb1
    i32 8, label %sw.bb2
    i32 9, label %sw.bb3
    i32 13, label %sw.bb3
    i32 10, label %sw.bb4
    i32 14, label %sw.bb4
    i32 11, label %sw.bb5
    i32 15, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %5 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_extrgb_convert_internal(%struct.jpeg_decompress_struct* %2, i8*** %3, i32 %4, i8** %5, i32 %6)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %10 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %11 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_extrgbx_convert_internal(%struct.jpeg_decompress_struct* %7, i8*** %8, i32 %9, i8** %10, i32 %11)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %14 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %15 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_extbgr_convert_internal(%struct.jpeg_decompress_struct* %12, i8*** %13, i32 %14, i8** %15, i32 %16)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %20 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %21 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_extbgrx_convert_internal(%struct.jpeg_decompress_struct* %17, i8*** %18, i32 %19, i8** %20, i32 %21)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %24 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %25 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %26 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_extxbgr_convert_internal(%struct.jpeg_decompress_struct* %22, i8*** %23, i32 %24, i8** %25, i32 %26)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %29 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %30 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %31 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_extxrgb_convert_internal(%struct.jpeg_decompress_struct* %27, i8*** %28, i32 %29, i8** %30, i32 %31)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %34 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %35 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %36 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_rgb_convert_internal(%struct.jpeg_decompress_struct* %32, i8*** %33, i32 %34, i8** %35, i32 %36)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @build_ycc_rgb_table(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 1
  %7 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %7, i32 0, i32 0
  %8 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %10 = bitcast %struct.jpeg_decompress_struct* %9 to %struct.jpeg_common_struct*
  %call = call i8* %8(%struct.jpeg_common_struct* %10, i32 1, i32 1024)
  %11 = bitcast i8* %call to i32*
  %12 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %12, i32 0, i32 1
  store i32* %11, i32** %Cr_r_tab, align 4, !tbaa !38
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 1
  %14 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem2, align 4, !tbaa !6
  %alloc_small3 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %14, i32 0, i32 0
  %15 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small3, align 4, !tbaa !11
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = bitcast %struct.jpeg_decompress_struct* %16 to %struct.jpeg_common_struct*
  %call4 = call i8* %15(%struct.jpeg_common_struct* %17, i32 1, i32 1024)
  %18 = bitcast i8* %call4 to i32*
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 2
  store i32* %18, i32** %Cb_b_tab, align 4, !tbaa !39
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 1
  %21 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem5, align 4, !tbaa !6
  %alloc_small6 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %21, i32 0, i32 0
  %22 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small6, align 4, !tbaa !11
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_decompress_struct* %23 to %struct.jpeg_common_struct*
  %call7 = call i8* %22(%struct.jpeg_common_struct* %24, i32 1, i32 1024)
  %25 = bitcast i8* %call7 to i32*
  %26 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %26, i32 0, i32 3
  store i32* %25, i32** %Cr_g_tab, align 4, !tbaa !40
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 1
  %28 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem8, align 4, !tbaa !6
  %alloc_small9 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %28, i32 0, i32 0
  %29 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small9, align 4, !tbaa !11
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_decompress_struct* %30 to %struct.jpeg_common_struct*
  %call10 = call i8* %29(%struct.jpeg_common_struct* %31, i32 1, i32 1024)
  %32 = bitcast i8* %call10 to i32*
  %33 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %33, i32 0, i32 4
  store i32* %32, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32 0, i32* %i, align 4, !tbaa !27
  store i32 -128, i32* %x, align 4, !tbaa !37
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %34 = load i32, i32* %i, align 4, !tbaa !27
  %cmp = icmp sle i32 %34, 255
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %35 = load i32, i32* %x, align 4, !tbaa !37
  %mul = mul nsw i32 91881, %35
  %add = add nsw i32 %mul, 32768
  %shr = ashr i32 %add, 16
  %36 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab11 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %36, i32 0, i32 1
  %37 = load i32*, i32** %Cr_r_tab11, align 4, !tbaa !38
  %38 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds i32, i32* %37, i32 %38
  store i32 %shr, i32* %arrayidx, align 4, !tbaa !27
  %39 = load i32, i32* %x, align 4, !tbaa !37
  %mul12 = mul nsw i32 116130, %39
  %add13 = add nsw i32 %mul12, 32768
  %shr14 = ashr i32 %add13, 16
  %40 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab15 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %40, i32 0, i32 2
  %41 = load i32*, i32** %Cb_b_tab15, align 4, !tbaa !39
  %42 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %41, i32 %42
  store i32 %shr14, i32* %arrayidx16, align 4, !tbaa !27
  %43 = load i32, i32* %x, align 4, !tbaa !37
  %mul17 = mul nsw i32 -46802, %43
  %44 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab18 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %44, i32 0, i32 3
  %45 = load i32*, i32** %Cr_g_tab18, align 4, !tbaa !40
  %46 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx19 = getelementptr inbounds i32, i32* %45, i32 %46
  store i32 %mul17, i32* %arrayidx19, align 4, !tbaa !37
  %47 = load i32, i32* %x, align 4, !tbaa !37
  %mul20 = mul nsw i32 -22554, %47
  %add21 = add nsw i32 %mul20, 32768
  %48 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab22 = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %48, i32 0, i32 4
  %49 = load i32*, i32** %Cb_g_tab22, align 4, !tbaa !41
  %50 = load i32, i32* %i, align 4, !tbaa !27
  %arrayidx23 = getelementptr inbounds i32, i32* %49, i32 %50
  store i32 %add21, i32* %arrayidx23, align 4, !tbaa !37
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %51 = load i32, i32* %i, align 4, !tbaa !27
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %i, align 4, !tbaa !27
  %52 = load i32, i32* %x, align 4, !tbaa !37
  %inc24 = add nsw i32 %52, 1
  store i32 %inc24, i32* %x, align 4, !tbaa !37
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %53 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  ret void
}

; Function Attrs: nounwind
define internal void @gray_rgb_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 11
  %1 = load i32, i32* %out_color_space, align 4, !tbaa !24
  switch i32 %1, label %sw.default [
    i32 6, label %sw.bb
    i32 7, label %sw.bb1
    i32 12, label %sw.bb1
    i32 8, label %sw.bb2
    i32 9, label %sw.bb3
    i32 13, label %sw.bb3
    i32 10, label %sw.bb4
    i32 14, label %sw.bb4
    i32 11, label %sw.bb5
    i32 15, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %5 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_extrgb_convert_internal(%struct.jpeg_decompress_struct* %2, i8*** %3, i32 %4, i8** %5, i32 %6)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %10 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %11 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_extrgbx_convert_internal(%struct.jpeg_decompress_struct* %7, i8*** %8, i32 %9, i8** %10, i32 %11)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %14 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %15 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_extbgr_convert_internal(%struct.jpeg_decompress_struct* %12, i8*** %13, i32 %14, i8** %15, i32 %16)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %20 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %21 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_extbgrx_convert_internal(%struct.jpeg_decompress_struct* %17, i8*** %18, i32 %19, i8** %20, i32 %21)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %24 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %25 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %26 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_extxbgr_convert_internal(%struct.jpeg_decompress_struct* %22, i8*** %23, i32 %24, i8** %25, i32 %26)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %29 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %30 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %31 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_extxrgb_convert_internal(%struct.jpeg_decompress_struct* %27, i8*** %28, i32 %29, i8** %30, i32 %31)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %34 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %35 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %36 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_rgb_convert_internal(%struct.jpeg_decompress_struct* %32, i8*** %33, i32 %34, i8** %35, i32 %36)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @null_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %inptr3 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_components = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %ci = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i8** %inptr3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %num_components to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 9
  %9 = load i32, i32* %num_components1, align 4, !tbaa !19
  store i32 %9, i32* %num_components, align 4, !tbaa !27
  %10 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 27
  %12 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %12, i32* %num_cols, align 4, !tbaa !27
  %13 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load i32, i32* %num_components, align 4, !tbaa !27
  %cmp = icmp eq i32 %14, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %for.end, %if.then
  %15 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp2 = icmp sge i32 %dec, 0
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %16 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %16, i32 0
  %17 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %18 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %17, i32 %18
  %19 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %19, i8** %inptr0, align 4, !tbaa !2
  %20 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %20, i32 1
  %21 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %22 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %21, i32 %22
  %23 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %23, i8** %inptr1, align 4, !tbaa !2
  %24 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %24, i32 2
  %25 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %26 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8*, i8** %25, i32 %26
  %27 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %27, i8** %inptr2, align 4, !tbaa !2
  %28 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %28, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %29 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %29, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %30 = load i8*, i8** %29, align 4, !tbaa !2
  store i8* %30, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %31 = load i32, i32* %col, align 4, !tbaa !27
  %32 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp8 = icmp ult i32 %31, %32
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %33 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %34 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %33, i32 %34
  %35 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %36 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr10 = getelementptr inbounds i8, i8* %36, i32 1
  store i8* %incdec.ptr10, i8** %outptr, align 4, !tbaa !2
  store i8 %35, i8* %36, align 1, !tbaa !36
  %37 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %38 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %37, i32 %38
  %39 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %40 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %40, i32 1
  store i8* %incdec.ptr12, i8** %outptr, align 4, !tbaa !2
  store i8 %39, i8* %40, align 1, !tbaa !36
  %41 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %42 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i8, i8* %41, i32 %42
  %43 = load i8, i8* %arrayidx13, align 1, !tbaa !36
  %44 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr14 = getelementptr inbounds i8, i8* %44, i32 1
  store i8* %incdec.ptr14, i8** %outptr, align 4, !tbaa !2
  store i8 %43, i8* %44, align 1, !tbaa !36
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %45 = load i32, i32* %col, align 4, !tbaa !27
  %inc15 = add i32 %45, 1
  store i32 %inc15, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end71

if.else:                                          ; preds = %entry
  %46 = load i32, i32* %num_components, align 4, !tbaa !27
  %cmp16 = icmp eq i32 %46, 4
  br i1 %cmp16, label %if.then17, label %if.else47

if.then17:                                        ; preds = %if.else
  br label %while.cond18

while.cond18:                                     ; preds = %for.end45, %if.then17
  %47 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec19 = add nsw i32 %47, -1
  store i32 %dec19, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp20 = icmp sge i32 %dec19, 0
  br i1 %cmp20, label %while.body21, label %while.end46

while.body21:                                     ; preds = %while.cond18
  %48 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8**, i8*** %48, i32 0
  %49 = load i8**, i8*** %arrayidx22, align 4, !tbaa !2
  %50 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx23 = getelementptr inbounds i8*, i8** %49, i32 %50
  %51 = load i8*, i8** %arrayidx23, align 4, !tbaa !2
  store i8* %51, i8** %inptr0, align 4, !tbaa !2
  %52 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8**, i8*** %52, i32 1
  %53 = load i8**, i8*** %arrayidx24, align 4, !tbaa !2
  %54 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx25 = getelementptr inbounds i8*, i8** %53, i32 %54
  %55 = load i8*, i8** %arrayidx25, align 4, !tbaa !2
  store i8* %55, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8**, i8*** %56, i32 2
  %57 = load i8**, i8*** %arrayidx26, align 4, !tbaa !2
  %58 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx27 = getelementptr inbounds i8*, i8** %57, i32 %58
  %59 = load i8*, i8** %arrayidx27, align 4, !tbaa !2
  store i8* %59, i8** %inptr2, align 4, !tbaa !2
  %60 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i8**, i8*** %60, i32 3
  %61 = load i8**, i8*** %arrayidx28, align 4, !tbaa !2
  %62 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx29 = getelementptr inbounds i8*, i8** %61, i32 %62
  %63 = load i8*, i8** %arrayidx29, align 4, !tbaa !2
  store i8* %63, i8** %inptr3, align 4, !tbaa !2
  %64 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc30 = add i32 %64, 1
  store i32 %inc30, i32* %input_row.addr, align 4, !tbaa !27
  %65 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr31 = getelementptr inbounds i8*, i8** %65, i32 1
  store i8** %incdec.ptr31, i8*** %output_buf.addr, align 4, !tbaa !2
  %66 = load i8*, i8** %65, align 4, !tbaa !2
  store i8* %66, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc43, %while.body21
  %67 = load i32, i32* %col, align 4, !tbaa !27
  %68 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp33 = icmp ult i32 %67, %68
  br i1 %cmp33, label %for.body34, label %for.end45

for.body34:                                       ; preds = %for.cond32
  %69 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %70 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx35 = getelementptr inbounds i8, i8* %69, i32 %70
  %71 = load i8, i8* %arrayidx35, align 1, !tbaa !36
  %72 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr36 = getelementptr inbounds i8, i8* %72, i32 1
  store i8* %incdec.ptr36, i8** %outptr, align 4, !tbaa !2
  store i8 %71, i8* %72, align 1, !tbaa !36
  %73 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %74 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx37 = getelementptr inbounds i8, i8* %73, i32 %74
  %75 = load i8, i8* %arrayidx37, align 1, !tbaa !36
  %76 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr38 = getelementptr inbounds i8, i8* %76, i32 1
  store i8* %incdec.ptr38, i8** %outptr, align 4, !tbaa !2
  store i8 %75, i8* %76, align 1, !tbaa !36
  %77 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %78 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx39 = getelementptr inbounds i8, i8* %77, i32 %78
  %79 = load i8, i8* %arrayidx39, align 1, !tbaa !36
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr40 = getelementptr inbounds i8, i8* %80, i32 1
  store i8* %incdec.ptr40, i8** %outptr, align 4, !tbaa !2
  store i8 %79, i8* %80, align 1, !tbaa !36
  %81 = load i8*, i8** %inptr3, align 4, !tbaa !2
  %82 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx41 = getelementptr inbounds i8, i8* %81, i32 %82
  %83 = load i8, i8* %arrayidx41, align 1, !tbaa !36
  %84 = load i8*, i8** %outptr, align 4, !tbaa !2
  %incdec.ptr42 = getelementptr inbounds i8, i8* %84, i32 1
  store i8* %incdec.ptr42, i8** %outptr, align 4, !tbaa !2
  store i8 %83, i8* %84, align 1, !tbaa !36
  br label %for.inc43

for.inc43:                                        ; preds = %for.body34
  %85 = load i32, i32* %col, align 4, !tbaa !27
  %inc44 = add i32 %85, 1
  store i32 %inc44, i32* %col, align 4, !tbaa !27
  br label %for.cond32

for.end45:                                        ; preds = %for.cond32
  br label %while.cond18

while.end46:                                      ; preds = %while.cond18
  br label %if.end

if.else47:                                        ; preds = %if.else
  br label %while.cond48

while.cond48:                                     ; preds = %for.end67, %if.else47
  %86 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec49 = add nsw i32 %86, -1
  store i32 %dec49, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp50 = icmp sge i32 %dec49, 0
  br i1 %cmp50, label %while.body51, label %while.end70

while.body51:                                     ; preds = %while.cond48
  store i32 0, i32* %ci, align 4, !tbaa !27
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc65, %while.body51
  %87 = load i32, i32* %ci, align 4, !tbaa !27
  %88 = load i32, i32* %num_components, align 4, !tbaa !27
  %cmp53 = icmp slt i32 %87, %88
  br i1 %cmp53, label %for.body54, label %for.end67

for.body54:                                       ; preds = %for.cond52
  %89 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %90 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx55 = getelementptr inbounds i8**, i8*** %89, i32 %90
  %91 = load i8**, i8*** %arrayidx55, align 4, !tbaa !2
  %92 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx56 = getelementptr inbounds i8*, i8** %91, i32 %92
  %93 = load i8*, i8** %arrayidx56, align 4, !tbaa !2
  store i8* %93, i8** %inptr, align 4, !tbaa !2
  %94 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %95 = load i8*, i8** %94, align 4, !tbaa !2
  store i8* %95, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc62, %for.body54
  %96 = load i32, i32* %col, align 4, !tbaa !27
  %97 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp58 = icmp ult i32 %96, %97
  br i1 %cmp58, label %for.body59, label %for.end64

for.body59:                                       ; preds = %for.cond57
  %98 = load i8*, i8** %inptr, align 4, !tbaa !2
  %99 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx60 = getelementptr inbounds i8, i8* %98, i32 %99
  %100 = load i8, i8* %arrayidx60, align 1, !tbaa !36
  %101 = load i8*, i8** %outptr, align 4, !tbaa !2
  %102 = load i32, i32* %ci, align 4, !tbaa !27
  %arrayidx61 = getelementptr inbounds i8, i8* %101, i32 %102
  store i8 %100, i8* %arrayidx61, align 1, !tbaa !36
  %103 = load i32, i32* %num_components, align 4, !tbaa !27
  %104 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %104, i32 %103
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc62

for.inc62:                                        ; preds = %for.body59
  %105 = load i32, i32* %col, align 4, !tbaa !27
  %inc63 = add i32 %105, 1
  store i32 %inc63, i32* %col, align 4, !tbaa !27
  br label %for.cond57

for.end64:                                        ; preds = %for.cond57
  br label %for.inc65

for.inc65:                                        ; preds = %for.end64
  %106 = load i32, i32* %ci, align 4, !tbaa !27
  %inc66 = add nsw i32 %106, 1
  store i32 %inc66, i32* %ci, align 4, !tbaa !27
  br label %for.cond52

for.end67:                                        ; preds = %for.cond52
  %107 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr68 = getelementptr inbounds i8*, i8** %107, i32 1
  store i8** %incdec.ptr68, i8*** %output_buf.addr, align 4, !tbaa !2
  %108 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc69 = add i32 %108, 1
  store i32 %inc69, i32* %input_row.addr, align 4, !tbaa !27
  br label %while.cond48

while.end70:                                      ; preds = %while.cond48
  br label %if.end

if.end:                                           ; preds = %while.end70, %while.end46
  br label %if.end71

if.end71:                                         ; preds = %if.end, %while.end
  %109 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #4
  %110 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #4
  %111 = bitcast i32* %num_components to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  %114 = bitcast i8** %inptr3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #4
  %115 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #4
  %116 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #4
  %117 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #4
  %118 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #4
  ret void
}

; Function Attrs: nounwind
define internal void @rgb_rgb_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 11
  %1 = load i32, i32* %out_color_space, align 4, !tbaa !24
  switch i32 %1, label %sw.default [
    i32 6, label %sw.bb
    i32 7, label %sw.bb1
    i32 12, label %sw.bb1
    i32 8, label %sw.bb2
    i32 9, label %sw.bb3
    i32 13, label %sw.bb3
    i32 10, label %sw.bb4
    i32 14, label %sw.bb4
    i32 11, label %sw.bb5
    i32 15, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %5 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_extrgb_convert_internal(%struct.jpeg_decompress_struct* %2, i8*** %3, i32 %4, i8** %5, i32 %6)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %10 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %11 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_extrgbx_convert_internal(%struct.jpeg_decompress_struct* %7, i8*** %8, i32 %9, i8** %10, i32 %11)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %14 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %15 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %16 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_extbgr_convert_internal(%struct.jpeg_decompress_struct* %12, i8*** %13, i32 %14, i8** %15, i32 %16)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %20 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %21 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_extbgrx_convert_internal(%struct.jpeg_decompress_struct* %17, i8*** %18, i32 %19, i8** %20, i32 %21)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %24 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %25 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %26 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_extxbgr_convert_internal(%struct.jpeg_decompress_struct* %22, i8*** %23, i32 %24, i8** %25, i32 %26)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %29 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %30 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %31 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_extxrgb_convert_internal(%struct.jpeg_decompress_struct* %27, i8*** %28, i32 %29, i8** %30, i32 %31)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %34 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %35 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %36 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_rgb_convert_internal(%struct.jpeg_decompress_struct* %32, i8*** %33, i32 %34, i8** %35, i32 %36)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

declare i32 @jsimd_can_ycc_rgb565() #2

declare void @jsimd_ycc_rgb565_convert(%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32) #2

; Function Attrs: nounwind
define internal void @ycc_rgb565_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_rgb565_convert_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3, i32 %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %8 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_rgb565_convert_le(%struct.jpeg_decompress_struct* %5, i8*** %6, i32 %7, i8** %8, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @gray_rgb565_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_rgb565_convert_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3, i32 %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %8 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_rgb565_convert_le(%struct.jpeg_decompress_struct* %5, i8*** %6, i32 %7, i8** %8, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @rgb_rgb565_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_rgb565_convert_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3, i32 %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %8 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_rgb565_convert_le(%struct.jpeg_decompress_struct* %5, i8*** %6, i32 %7, i8** %8, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @ycc_rgb565D_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_rgb565D_convert_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3, i32 %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %8 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @ycc_rgb565D_convert_le(%struct.jpeg_decompress_struct* %5, i8*** %6, i32 %7, i8** %8, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @gray_rgb565D_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_rgb565D_convert_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3, i32 %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %8 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @gray_rgb565D_convert_le(%struct.jpeg_decompress_struct* %5, i8*** %6, i32 %7, i8** %8, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @rgb_rgb565D_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_rgb565D_convert_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3, i32 %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %7 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %8 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %9 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  call void @rgb_rgb565D_convert_le(%struct.jpeg_decompress_struct* %5, i8*** %6, i32 %7, i8** %8, i32 %9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @ycck_cmyk_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %inptr3 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 27
  %15 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %15, i32* %num_cols, align 4, !tbaa !27
  %16 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 65
  %18 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %18, i8** %range_limit, align 4, !tbaa !2
  %19 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %20, i32 0, i32 1
  %21 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %21, i32** %Crrtab, align 4, !tbaa !2
  %22 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #4
  %23 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %23, i32 0, i32 2
  %24 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %24, i32** %Cbbtab, align 4, !tbaa !2
  %25 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  %26 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %26, i32 0, i32 3
  %27 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %27, i32** %Crgtab, align 4, !tbaa !2
  %28 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #4
  %29 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %29, i32 0, i32 4
  %30 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %30, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %31 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %31, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %32 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %32, i32 0
  %33 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %33, i32 %34
  %35 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %35, i8** %inptr0, align 4, !tbaa !2
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %36, i32 1
  %37 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %38 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %37, i32 %38
  %39 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %39, i8** %inptr1, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %40, i32 2
  %41 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %42 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %43, i8** %inptr2, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8**, i8*** %44, i32 3
  %45 = load i8**, i8*** %arrayidx7, align 4, !tbaa !2
  %46 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %47, i8** %inptr3, align 4, !tbaa !2
  %48 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %48, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %49 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %49, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %50 = load i8*, i8** %49, align 4, !tbaa !2
  store i8* %50, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %51 = load i32, i32* %col, align 4, !tbaa !27
  %52 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp9 = icmp ult i32 %51, %52
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %53 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %54 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx10 = getelementptr inbounds i8, i8* %53, i32 %54
  %55 = load i8, i8* %arrayidx10, align 1, !tbaa !36
  %conv = zext i8 %55 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %56 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %57 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %56, i32 %57
  %58 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %58 to i32
  store i32 %conv12, i32* %cb, align 4, !tbaa !27
  %59 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %60 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i8, i8* %59, i32 %60
  %61 = load i8, i8* %arrayidx13, align 1, !tbaa !36
  %conv14 = zext i8 %61 to i32
  store i32 %conv14, i32* %cr, align 4, !tbaa !27
  %62 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %63 = load i32, i32* %y, align 4, !tbaa !27
  %64 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %65 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx15 = getelementptr inbounds i32, i32* %64, i32 %65
  %66 = load i32, i32* %arrayidx15, align 4, !tbaa !27
  %add = add nsw i32 %63, %66
  %sub = sub nsw i32 255, %add
  %arrayidx16 = getelementptr inbounds i8, i8* %62, i32 %sub
  %67 = load i8, i8* %arrayidx16, align 1, !tbaa !36
  %68 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i8, i8* %68, i32 0
  store i8 %67, i8* %arrayidx17, align 1, !tbaa !36
  %69 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %70 = load i32, i32* %y, align 4, !tbaa !27
  %71 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %72 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx18 = getelementptr inbounds i32, i32* %71, i32 %72
  %73 = load i32, i32* %arrayidx18, align 4, !tbaa !37
  %74 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %75 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx19 = getelementptr inbounds i32, i32* %74, i32 %75
  %76 = load i32, i32* %arrayidx19, align 4, !tbaa !37
  %add20 = add nsw i32 %73, %76
  %shr = ashr i32 %add20, 16
  %add21 = add nsw i32 %70, %shr
  %sub22 = sub nsw i32 255, %add21
  %arrayidx23 = getelementptr inbounds i8, i8* %69, i32 %sub22
  %77 = load i8, i8* %arrayidx23, align 1, !tbaa !36
  %78 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %78, i32 1
  store i8 %77, i8* %arrayidx24, align 1, !tbaa !36
  %79 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %80 = load i32, i32* %y, align 4, !tbaa !27
  %81 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %82 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx25 = getelementptr inbounds i32, i32* %81, i32 %82
  %83 = load i32, i32* %arrayidx25, align 4, !tbaa !27
  %add26 = add nsw i32 %80, %83
  %sub27 = sub nsw i32 255, %add26
  %arrayidx28 = getelementptr inbounds i8, i8* %79, i32 %sub27
  %84 = load i8, i8* %arrayidx28, align 1, !tbaa !36
  %85 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i8, i8* %85, i32 2
  store i8 %84, i8* %arrayidx29, align 1, !tbaa !36
  %86 = load i8*, i8** %inptr3, align 4, !tbaa !2
  %87 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx30 = getelementptr inbounds i8, i8* %86, i32 %87
  %88 = load i8, i8* %arrayidx30, align 1, !tbaa !36
  %89 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %89, i32 3
  store i8 %88, i8* %arrayidx31, align 1, !tbaa !36
  %90 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %90, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %91 = load i32, i32* %col, align 4, !tbaa !27
  %inc32 = add i32 %91, 1
  store i32 %inc32, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %92 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  %98 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  %99 = bitcast i8** %inptr3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #4
  %100 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #4
  %101 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #4
  %102 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #4
  %103 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #4
  %104 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #4
  %105 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #4
  %106 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  %107 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @jcopy_sample_rows(i8**, i32, i8**, i32, i32, i32) #2

; Function Attrs: alwaysinline nounwind
define internal void @ycc_extrgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %31, i32 0
  %32 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %33 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %32, i32 %33
  %34 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %34, i8** %inptr0, align 4, !tbaa !2
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %35, i32 1
  %36 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %38, i8** %inptr1, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %39, i32 2
  %40 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %42, i8** %inptr2, align 4, !tbaa !2
  %43 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %43, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %44 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %44, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %44, align 4, !tbaa !2
  store i8* %45, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %46 = load i32, i32* %col, align 4, !tbaa !27
  %47 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp7 = icmp ult i32 %46, %47
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %49 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %48, i32 %49
  %50 = load i8, i8* %arrayidx8, align 1, !tbaa !36
  %conv = zext i8 %50 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %52 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %conv10 = zext i8 %53 to i32
  store i32 %conv10, i32* %cb, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %55 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %56 to i32
  store i32 %conv12, i32* %cr, align 4, !tbaa !27
  %57 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %58 = load i32, i32* %y, align 4, !tbaa !27
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx13, align 4, !tbaa !27
  %add = add nsw i32 %58, %61
  %arrayidx14 = getelementptr inbounds i8, i8* %57, i32 %add
  %62 = load i8, i8* %arrayidx14, align 1, !tbaa !36
  %63 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %63, i32 0
  store i8 %62, i8* %arrayidx15, align 1, !tbaa !36
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx17, align 4, !tbaa !37
  %add18 = add nsw i32 %68, %71
  %shr = ashr i32 %add18, 16
  %add19 = add nsw i32 %65, %shr
  %arrayidx20 = getelementptr inbounds i8, i8* %64, i32 %add19
  %72 = load i8, i8* %arrayidx20, align 1, !tbaa !36
  %73 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 1
  store i8 %72, i8* %arrayidx21, align 1, !tbaa !36
  %74 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %75 = load i32, i32* %y, align 4, !tbaa !27
  %76 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %77 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx22 = getelementptr inbounds i32, i32* %76, i32 %77
  %78 = load i32, i32* %arrayidx22, align 4, !tbaa !27
  %add23 = add nsw i32 %75, %78
  %arrayidx24 = getelementptr inbounds i8, i8* %74, i32 %add23
  %79 = load i8, i8* %arrayidx24, align 1, !tbaa !36
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %80, i32 2
  store i8 %79, i8* %arrayidx25, align 1, !tbaa !36
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %81, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %82 = load i32, i32* %col, align 4, !tbaa !27
  %inc26 = add i32 %82, 1
  store i32 %inc26, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %83 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_extrgbx_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %31, i32 0
  %32 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %33 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %32, i32 %33
  %34 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %34, i8** %inptr0, align 4, !tbaa !2
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %35, i32 1
  %36 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %38, i8** %inptr1, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %39, i32 2
  %40 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %42, i8** %inptr2, align 4, !tbaa !2
  %43 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %43, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %44 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %44, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %44, align 4, !tbaa !2
  store i8* %45, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %46 = load i32, i32* %col, align 4, !tbaa !27
  %47 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp7 = icmp ult i32 %46, %47
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %49 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %48, i32 %49
  %50 = load i8, i8* %arrayidx8, align 1, !tbaa !36
  %conv = zext i8 %50 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %52 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %conv10 = zext i8 %53 to i32
  store i32 %conv10, i32* %cb, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %55 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %56 to i32
  store i32 %conv12, i32* %cr, align 4, !tbaa !27
  %57 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %58 = load i32, i32* %y, align 4, !tbaa !27
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx13, align 4, !tbaa !27
  %add = add nsw i32 %58, %61
  %arrayidx14 = getelementptr inbounds i8, i8* %57, i32 %add
  %62 = load i8, i8* %arrayidx14, align 1, !tbaa !36
  %63 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %63, i32 0
  store i8 %62, i8* %arrayidx15, align 1, !tbaa !36
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx17, align 4, !tbaa !37
  %add18 = add nsw i32 %68, %71
  %shr = ashr i32 %add18, 16
  %add19 = add nsw i32 %65, %shr
  %arrayidx20 = getelementptr inbounds i8, i8* %64, i32 %add19
  %72 = load i8, i8* %arrayidx20, align 1, !tbaa !36
  %73 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 1
  store i8 %72, i8* %arrayidx21, align 1, !tbaa !36
  %74 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %75 = load i32, i32* %y, align 4, !tbaa !27
  %76 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %77 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx22 = getelementptr inbounds i32, i32* %76, i32 %77
  %78 = load i32, i32* %arrayidx22, align 4, !tbaa !27
  %add23 = add nsw i32 %75, %78
  %arrayidx24 = getelementptr inbounds i8, i8* %74, i32 %add23
  %79 = load i8, i8* %arrayidx24, align 1, !tbaa !36
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %80, i32 2
  store i8 %79, i8* %arrayidx25, align 1, !tbaa !36
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %81, i32 3
  store i8 -1, i8* %arrayidx26, align 1, !tbaa !36
  %82 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %82, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %83 = load i32, i32* %col, align 4, !tbaa !27
  %inc27 = add i32 %83, 1
  store i32 %inc27, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %84 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  %98 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_extbgr_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %31, i32 0
  %32 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %33 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %32, i32 %33
  %34 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %34, i8** %inptr0, align 4, !tbaa !2
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %35, i32 1
  %36 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %38, i8** %inptr1, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %39, i32 2
  %40 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %42, i8** %inptr2, align 4, !tbaa !2
  %43 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %43, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %44 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %44, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %44, align 4, !tbaa !2
  store i8* %45, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %46 = load i32, i32* %col, align 4, !tbaa !27
  %47 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp7 = icmp ult i32 %46, %47
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %49 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %48, i32 %49
  %50 = load i8, i8* %arrayidx8, align 1, !tbaa !36
  %conv = zext i8 %50 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %52 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %conv10 = zext i8 %53 to i32
  store i32 %conv10, i32* %cb, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %55 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %56 to i32
  store i32 %conv12, i32* %cr, align 4, !tbaa !27
  %57 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %58 = load i32, i32* %y, align 4, !tbaa !27
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx13, align 4, !tbaa !27
  %add = add nsw i32 %58, %61
  %arrayidx14 = getelementptr inbounds i8, i8* %57, i32 %add
  %62 = load i8, i8* %arrayidx14, align 1, !tbaa !36
  %63 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %63, i32 2
  store i8 %62, i8* %arrayidx15, align 1, !tbaa !36
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx17, align 4, !tbaa !37
  %add18 = add nsw i32 %68, %71
  %shr = ashr i32 %add18, 16
  %add19 = add nsw i32 %65, %shr
  %arrayidx20 = getelementptr inbounds i8, i8* %64, i32 %add19
  %72 = load i8, i8* %arrayidx20, align 1, !tbaa !36
  %73 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 1
  store i8 %72, i8* %arrayidx21, align 1, !tbaa !36
  %74 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %75 = load i32, i32* %y, align 4, !tbaa !27
  %76 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %77 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx22 = getelementptr inbounds i32, i32* %76, i32 %77
  %78 = load i32, i32* %arrayidx22, align 4, !tbaa !27
  %add23 = add nsw i32 %75, %78
  %arrayidx24 = getelementptr inbounds i8, i8* %74, i32 %add23
  %79 = load i8, i8* %arrayidx24, align 1, !tbaa !36
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %80, i32 0
  store i8 %79, i8* %arrayidx25, align 1, !tbaa !36
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %81, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %82 = load i32, i32* %col, align 4, !tbaa !27
  %inc26 = add i32 %82, 1
  store i32 %inc26, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %83 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_extbgrx_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %31, i32 0
  %32 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %33 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %32, i32 %33
  %34 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %34, i8** %inptr0, align 4, !tbaa !2
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %35, i32 1
  %36 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %38, i8** %inptr1, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %39, i32 2
  %40 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %42, i8** %inptr2, align 4, !tbaa !2
  %43 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %43, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %44 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %44, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %44, align 4, !tbaa !2
  store i8* %45, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %46 = load i32, i32* %col, align 4, !tbaa !27
  %47 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp7 = icmp ult i32 %46, %47
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %49 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %48, i32 %49
  %50 = load i8, i8* %arrayidx8, align 1, !tbaa !36
  %conv = zext i8 %50 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %52 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %conv10 = zext i8 %53 to i32
  store i32 %conv10, i32* %cb, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %55 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %56 to i32
  store i32 %conv12, i32* %cr, align 4, !tbaa !27
  %57 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %58 = load i32, i32* %y, align 4, !tbaa !27
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx13, align 4, !tbaa !27
  %add = add nsw i32 %58, %61
  %arrayidx14 = getelementptr inbounds i8, i8* %57, i32 %add
  %62 = load i8, i8* %arrayidx14, align 1, !tbaa !36
  %63 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %63, i32 2
  store i8 %62, i8* %arrayidx15, align 1, !tbaa !36
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx17, align 4, !tbaa !37
  %add18 = add nsw i32 %68, %71
  %shr = ashr i32 %add18, 16
  %add19 = add nsw i32 %65, %shr
  %arrayidx20 = getelementptr inbounds i8, i8* %64, i32 %add19
  %72 = load i8, i8* %arrayidx20, align 1, !tbaa !36
  %73 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 1
  store i8 %72, i8* %arrayidx21, align 1, !tbaa !36
  %74 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %75 = load i32, i32* %y, align 4, !tbaa !27
  %76 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %77 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx22 = getelementptr inbounds i32, i32* %76, i32 %77
  %78 = load i32, i32* %arrayidx22, align 4, !tbaa !27
  %add23 = add nsw i32 %75, %78
  %arrayidx24 = getelementptr inbounds i8, i8* %74, i32 %add23
  %79 = load i8, i8* %arrayidx24, align 1, !tbaa !36
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %80, i32 0
  store i8 %79, i8* %arrayidx25, align 1, !tbaa !36
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %81, i32 3
  store i8 -1, i8* %arrayidx26, align 1, !tbaa !36
  %82 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %82, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %83 = load i32, i32* %col, align 4, !tbaa !27
  %inc27 = add i32 %83, 1
  store i32 %inc27, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %84 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  %98 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_extxbgr_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %31, i32 0
  %32 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %33 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %32, i32 %33
  %34 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %34, i8** %inptr0, align 4, !tbaa !2
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %35, i32 1
  %36 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %38, i8** %inptr1, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %39, i32 2
  %40 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %42, i8** %inptr2, align 4, !tbaa !2
  %43 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %43, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %44 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %44, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %44, align 4, !tbaa !2
  store i8* %45, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %46 = load i32, i32* %col, align 4, !tbaa !27
  %47 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp7 = icmp ult i32 %46, %47
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %49 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %48, i32 %49
  %50 = load i8, i8* %arrayidx8, align 1, !tbaa !36
  %conv = zext i8 %50 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %52 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %conv10 = zext i8 %53 to i32
  store i32 %conv10, i32* %cb, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %55 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %56 to i32
  store i32 %conv12, i32* %cr, align 4, !tbaa !27
  %57 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %58 = load i32, i32* %y, align 4, !tbaa !27
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx13, align 4, !tbaa !27
  %add = add nsw i32 %58, %61
  %arrayidx14 = getelementptr inbounds i8, i8* %57, i32 %add
  %62 = load i8, i8* %arrayidx14, align 1, !tbaa !36
  %63 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %63, i32 3
  store i8 %62, i8* %arrayidx15, align 1, !tbaa !36
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx17, align 4, !tbaa !37
  %add18 = add nsw i32 %68, %71
  %shr = ashr i32 %add18, 16
  %add19 = add nsw i32 %65, %shr
  %arrayidx20 = getelementptr inbounds i8, i8* %64, i32 %add19
  %72 = load i8, i8* %arrayidx20, align 1, !tbaa !36
  %73 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 2
  store i8 %72, i8* %arrayidx21, align 1, !tbaa !36
  %74 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %75 = load i32, i32* %y, align 4, !tbaa !27
  %76 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %77 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx22 = getelementptr inbounds i32, i32* %76, i32 %77
  %78 = load i32, i32* %arrayidx22, align 4, !tbaa !27
  %add23 = add nsw i32 %75, %78
  %arrayidx24 = getelementptr inbounds i8, i8* %74, i32 %add23
  %79 = load i8, i8* %arrayidx24, align 1, !tbaa !36
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %80, i32 1
  store i8 %79, i8* %arrayidx25, align 1, !tbaa !36
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %81, i32 0
  store i8 -1, i8* %arrayidx26, align 1, !tbaa !36
  %82 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %82, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %83 = load i32, i32* %col, align 4, !tbaa !27
  %inc27 = add i32 %83, 1
  store i32 %inc27, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %84 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  %98 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_extxrgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %31, i32 0
  %32 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %33 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %32, i32 %33
  %34 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %34, i8** %inptr0, align 4, !tbaa !2
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %35, i32 1
  %36 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %38, i8** %inptr1, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %39, i32 2
  %40 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %42, i8** %inptr2, align 4, !tbaa !2
  %43 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %43, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %44 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %44, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %44, align 4, !tbaa !2
  store i8* %45, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %46 = load i32, i32* %col, align 4, !tbaa !27
  %47 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp7 = icmp ult i32 %46, %47
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %49 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %48, i32 %49
  %50 = load i8, i8* %arrayidx8, align 1, !tbaa !36
  %conv = zext i8 %50 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %52 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %conv10 = zext i8 %53 to i32
  store i32 %conv10, i32* %cb, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %55 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %56 to i32
  store i32 %conv12, i32* %cr, align 4, !tbaa !27
  %57 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %58 = load i32, i32* %y, align 4, !tbaa !27
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx13, align 4, !tbaa !27
  %add = add nsw i32 %58, %61
  %arrayidx14 = getelementptr inbounds i8, i8* %57, i32 %add
  %62 = load i8, i8* %arrayidx14, align 1, !tbaa !36
  %63 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %63, i32 1
  store i8 %62, i8* %arrayidx15, align 1, !tbaa !36
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx17, align 4, !tbaa !37
  %add18 = add nsw i32 %68, %71
  %shr = ashr i32 %add18, 16
  %add19 = add nsw i32 %65, %shr
  %arrayidx20 = getelementptr inbounds i8, i8* %64, i32 %add19
  %72 = load i8, i8* %arrayidx20, align 1, !tbaa !36
  %73 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 2
  store i8 %72, i8* %arrayidx21, align 1, !tbaa !36
  %74 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %75 = load i32, i32* %y, align 4, !tbaa !27
  %76 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %77 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx22 = getelementptr inbounds i32, i32* %76, i32 %77
  %78 = load i32, i32* %arrayidx22, align 4, !tbaa !27
  %add23 = add nsw i32 %75, %78
  %arrayidx24 = getelementptr inbounds i8, i8* %74, i32 %add23
  %79 = load i8, i8* %arrayidx24, align 1, !tbaa !36
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %80, i32 3
  store i8 %79, i8* %arrayidx25, align 1, !tbaa !36
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %81, i32 0
  store i8 -1, i8* %arrayidx26, align 1, !tbaa !36
  %82 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %82, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %83 = load i32, i32* %col, align 4, !tbaa !27
  %inc27 = add i32 %83, 1
  store i32 %inc27, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %84 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  %98 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_rgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %31, i32 0
  %32 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %33 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %32, i32 %33
  %34 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %34, i8** %inptr0, align 4, !tbaa !2
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %35, i32 1
  %36 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %38, i8** %inptr1, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %39, i32 2
  %40 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %42, i8** %inptr2, align 4, !tbaa !2
  %43 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %43, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %44 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %44, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %45 = load i8*, i8** %44, align 4, !tbaa !2
  store i8* %45, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %46 = load i32, i32* %col, align 4, !tbaa !27
  %47 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp7 = icmp ult i32 %46, %47
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %49 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx8 = getelementptr inbounds i8, i8* %48, i32 %49
  %50 = load i8, i8* %arrayidx8, align 1, !tbaa !36
  %conv = zext i8 %50 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %52 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %51, i32 %52
  %53 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %conv10 = zext i8 %53 to i32
  store i32 %conv10, i32* %cb, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %55 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %54, i32 %55
  %56 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %conv12 = zext i8 %56 to i32
  store i32 %conv12, i32* %cr, align 4, !tbaa !27
  %57 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %58 = load i32, i32* %y, align 4, !tbaa !27
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx13 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx13, align 4, !tbaa !27
  %add = add nsw i32 %58, %61
  %arrayidx14 = getelementptr inbounds i8, i8* %57, i32 %add
  %62 = load i8, i8* %arrayidx14, align 1, !tbaa !36
  %63 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8, i8* %63, i32 0
  store i8 %62, i8* %arrayidx15, align 1, !tbaa !36
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx17 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx17, align 4, !tbaa !37
  %add18 = add nsw i32 %68, %71
  %shr = ashr i32 %add18, 16
  %add19 = add nsw i32 %65, %shr
  %arrayidx20 = getelementptr inbounds i8, i8* %64, i32 %add19
  %72 = load i8, i8* %arrayidx20, align 1, !tbaa !36
  %73 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 1
  store i8 %72, i8* %arrayidx21, align 1, !tbaa !36
  %74 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %75 = load i32, i32* %y, align 4, !tbaa !27
  %76 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %77 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx22 = getelementptr inbounds i32, i32* %76, i32 %77
  %78 = load i32, i32* %arrayidx22, align 4, !tbaa !27
  %add23 = add nsw i32 %75, %78
  %arrayidx24 = getelementptr inbounds i8, i8* %74, i32 %add23
  %79 = load i8, i8* %arrayidx24, align 1, !tbaa !36
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %80, i32 2
  store i8 %79, i8* %arrayidx25, align 1, !tbaa !36
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %81, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %82 = load i32, i32* %col, align 4, !tbaa !27
  %inc26 = add i32 %82, 1
  store i32 %inc26, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %83 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  %97 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_extrgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %7, i32 0
  %8 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %9, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %8, i32 %9
  %10 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %11, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %11, align 4, !tbaa !2
  store i8* %12, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %13 = load i32, i32* %col, align 4, !tbaa !27
  %14 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp2 = icmp ult i32 %13, %14
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i8*, i8** %inptr, align 4, !tbaa !2
  %16 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !36
  %18 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %18, i32 2
  store i8 %17, i8* %arrayidx4, align 1, !tbaa !36
  %19 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %19, i32 1
  store i8 %17, i8* %arrayidx5, align 1, !tbaa !36
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %20, i32 0
  store i8 %17, i8* %arrayidx6, align 1, !tbaa !36
  %21 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %col, align 4, !tbaa !27
  %inc7 = add i32 %22, 1
  store i32 %inc7, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_extrgbx_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %7, i32 0
  %8 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %9, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %8, i32 %9
  %10 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %11, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %11, align 4, !tbaa !2
  store i8* %12, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %13 = load i32, i32* %col, align 4, !tbaa !27
  %14 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp2 = icmp ult i32 %13, %14
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i8*, i8** %inptr, align 4, !tbaa !2
  %16 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !36
  %18 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %18, i32 2
  store i8 %17, i8* %arrayidx4, align 1, !tbaa !36
  %19 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %19, i32 1
  store i8 %17, i8* %arrayidx5, align 1, !tbaa !36
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %20, i32 0
  store i8 %17, i8* %arrayidx6, align 1, !tbaa !36
  %21 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %21, i32 3
  store i8 -1, i8* %arrayidx7, align 1, !tbaa !36
  %22 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %col, align 4, !tbaa !27
  %inc8 = add i32 %23, 1
  store i32 %inc8, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %24 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_extbgr_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %7, i32 0
  %8 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %9, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %8, i32 %9
  %10 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %11, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %11, align 4, !tbaa !2
  store i8* %12, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %13 = load i32, i32* %col, align 4, !tbaa !27
  %14 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp2 = icmp ult i32 %13, %14
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i8*, i8** %inptr, align 4, !tbaa !2
  %16 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !36
  %18 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %18, i32 0
  store i8 %17, i8* %arrayidx4, align 1, !tbaa !36
  %19 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %19, i32 1
  store i8 %17, i8* %arrayidx5, align 1, !tbaa !36
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %20, i32 2
  store i8 %17, i8* %arrayidx6, align 1, !tbaa !36
  %21 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %col, align 4, !tbaa !27
  %inc7 = add i32 %22, 1
  store i32 %inc7, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_extbgrx_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %7, i32 0
  %8 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %9, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %8, i32 %9
  %10 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %11, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %11, align 4, !tbaa !2
  store i8* %12, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %13 = load i32, i32* %col, align 4, !tbaa !27
  %14 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp2 = icmp ult i32 %13, %14
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i8*, i8** %inptr, align 4, !tbaa !2
  %16 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !36
  %18 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %18, i32 0
  store i8 %17, i8* %arrayidx4, align 1, !tbaa !36
  %19 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %19, i32 1
  store i8 %17, i8* %arrayidx5, align 1, !tbaa !36
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %20, i32 2
  store i8 %17, i8* %arrayidx6, align 1, !tbaa !36
  %21 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %21, i32 3
  store i8 -1, i8* %arrayidx7, align 1, !tbaa !36
  %22 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %col, align 4, !tbaa !27
  %inc8 = add i32 %23, 1
  store i32 %inc8, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %24 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_extxbgr_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %7, i32 0
  %8 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %9, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %8, i32 %9
  %10 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %11, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %11, align 4, !tbaa !2
  store i8* %12, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %13 = load i32, i32* %col, align 4, !tbaa !27
  %14 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp2 = icmp ult i32 %13, %14
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i8*, i8** %inptr, align 4, !tbaa !2
  %16 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !36
  %18 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %18, i32 1
  store i8 %17, i8* %arrayidx4, align 1, !tbaa !36
  %19 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %19, i32 2
  store i8 %17, i8* %arrayidx5, align 1, !tbaa !36
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %20, i32 3
  store i8 %17, i8* %arrayidx6, align 1, !tbaa !36
  %21 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %21, i32 0
  store i8 -1, i8* %arrayidx7, align 1, !tbaa !36
  %22 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %col, align 4, !tbaa !27
  %inc8 = add i32 %23, 1
  store i32 %inc8, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %24 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_extxrgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %7, i32 0
  %8 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %9, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %8, i32 %9
  %10 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %11, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %11, align 4, !tbaa !2
  store i8* %12, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %13 = load i32, i32* %col, align 4, !tbaa !27
  %14 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp2 = icmp ult i32 %13, %14
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i8*, i8** %inptr, align 4, !tbaa !2
  %16 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !36
  %18 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %18, i32 3
  store i8 %17, i8* %arrayidx4, align 1, !tbaa !36
  %19 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %19, i32 2
  store i8 %17, i8* %arrayidx5, align 1, !tbaa !36
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %20, i32 1
  store i8 %17, i8* %arrayidx6, align 1, !tbaa !36
  %21 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %21, i32 0
  store i8 -1, i8* %arrayidx7, align 1, !tbaa !36
  %22 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %22, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %23 = load i32, i32* %col, align 4, !tbaa !27
  %inc8 = add i32 %23, 1
  store i32 %inc8, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %24 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  %27 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_rgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %7, i32 0
  %8 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %9 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %9, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %8, i32 %9
  %10 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %10, i8** %inptr, align 4, !tbaa !2
  %11 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %11, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %12 = load i8*, i8** %11, align 4, !tbaa !2
  store i8* %12, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %13 = load i32, i32* %col, align 4, !tbaa !27
  %14 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp2 = icmp ult i32 %13, %14
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i8*, i8** %inptr, align 4, !tbaa !2
  %16 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8, i8* %15, i32 %16
  %17 = load i8, i8* %arrayidx3, align 1, !tbaa !36
  %18 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %18, i32 2
  store i8 %17, i8* %arrayidx4, align 1, !tbaa !36
  %19 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %19, i32 1
  store i8 %17, i8* %arrayidx5, align 1, !tbaa !36
  %20 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %20, i32 0
  store i8 %17, i8* %arrayidx6, align 1, !tbaa !36
  %21 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %col, align 4, !tbaa !27
  %inc7 = add i32 %22, 1
  store i32 %inc7, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #4
  %24 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #4
  %25 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_extrgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr0, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %13, i32 1
  %14 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %16, i8** %inptr1, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %17, i32 2
  %18 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %20, i8** %inptr2, align 4, !tbaa !2
  %21 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %21, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %22 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %22, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %23 = load i8*, i8** %22, align 4, !tbaa !2
  store i8* %23, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !27
  %25 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 %27
  %28 = load i8, i8* %arrayidx7, align 1, !tbaa !36
  %29 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %29, i32 0
  store i8 %28, i8* %arrayidx8, align 1, !tbaa !36
  %30 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %31 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %30, i32 %31
  %32 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %33 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %33, i32 1
  store i8 %32, i8* %arrayidx10, align 1, !tbaa !36
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %35 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %37, i32 2
  store i8 %36, i8* %arrayidx12, align 1, !tbaa !36
  %38 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !27
  %inc13 = add i32 %39, 1
  store i32 %inc13, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_extrgbx_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr0, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %13, i32 1
  %14 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %16, i8** %inptr1, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %17, i32 2
  %18 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %20, i8** %inptr2, align 4, !tbaa !2
  %21 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %21, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %22 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %22, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %23 = load i8*, i8** %22, align 4, !tbaa !2
  store i8* %23, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !27
  %25 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 %27
  %28 = load i8, i8* %arrayidx7, align 1, !tbaa !36
  %29 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %29, i32 0
  store i8 %28, i8* %arrayidx8, align 1, !tbaa !36
  %30 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %31 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %30, i32 %31
  %32 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %33 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %33, i32 1
  store i8 %32, i8* %arrayidx10, align 1, !tbaa !36
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %35 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %37, i32 2
  store i8 %36, i8* %arrayidx12, align 1, !tbaa !36
  %38 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i8, i8* %38, i32 3
  store i8 -1, i8* %arrayidx13, align 1, !tbaa !36
  %39 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %39, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %col, align 4, !tbaa !27
  %inc14 = add i32 %40, 1
  store i32 %inc14, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %41 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_extbgr_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr0, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %13, i32 1
  %14 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %16, i8** %inptr1, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %17, i32 2
  %18 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %20, i8** %inptr2, align 4, !tbaa !2
  %21 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %21, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %22 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %22, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %23 = load i8*, i8** %22, align 4, !tbaa !2
  store i8* %23, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !27
  %25 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 %27
  %28 = load i8, i8* %arrayidx7, align 1, !tbaa !36
  %29 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %29, i32 2
  store i8 %28, i8* %arrayidx8, align 1, !tbaa !36
  %30 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %31 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %30, i32 %31
  %32 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %33 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %33, i32 1
  store i8 %32, i8* %arrayidx10, align 1, !tbaa !36
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %35 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %37, i32 0
  store i8 %36, i8* %arrayidx12, align 1, !tbaa !36
  %38 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !27
  %inc13 = add i32 %39, 1
  store i32 %inc13, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_extbgrx_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr0, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %13, i32 1
  %14 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %16, i8** %inptr1, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %17, i32 2
  %18 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %20, i8** %inptr2, align 4, !tbaa !2
  %21 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %21, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %22 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %22, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %23 = load i8*, i8** %22, align 4, !tbaa !2
  store i8* %23, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !27
  %25 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 %27
  %28 = load i8, i8* %arrayidx7, align 1, !tbaa !36
  %29 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %29, i32 2
  store i8 %28, i8* %arrayidx8, align 1, !tbaa !36
  %30 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %31 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %30, i32 %31
  %32 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %33 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %33, i32 1
  store i8 %32, i8* %arrayidx10, align 1, !tbaa !36
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %35 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %37, i32 0
  store i8 %36, i8* %arrayidx12, align 1, !tbaa !36
  %38 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i8, i8* %38, i32 3
  store i8 -1, i8* %arrayidx13, align 1, !tbaa !36
  %39 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %39, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %col, align 4, !tbaa !27
  %inc14 = add i32 %40, 1
  store i32 %inc14, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %41 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_extxbgr_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr0, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %13, i32 1
  %14 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %16, i8** %inptr1, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %17, i32 2
  %18 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %20, i8** %inptr2, align 4, !tbaa !2
  %21 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %21, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %22 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %22, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %23 = load i8*, i8** %22, align 4, !tbaa !2
  store i8* %23, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !27
  %25 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 %27
  %28 = load i8, i8* %arrayidx7, align 1, !tbaa !36
  %29 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %29, i32 3
  store i8 %28, i8* %arrayidx8, align 1, !tbaa !36
  %30 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %31 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %30, i32 %31
  %32 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %33 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %33, i32 2
  store i8 %32, i8* %arrayidx10, align 1, !tbaa !36
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %35 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %37, i32 1
  store i8 %36, i8* %arrayidx12, align 1, !tbaa !36
  %38 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i8, i8* %38, i32 0
  store i8 -1, i8* %arrayidx13, align 1, !tbaa !36
  %39 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %39, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %col, align 4, !tbaa !27
  %inc14 = add i32 %40, 1
  store i32 %inc14, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %41 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_extxrgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr0, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %13, i32 1
  %14 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %16, i8** %inptr1, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %17, i32 2
  %18 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %20, i8** %inptr2, align 4, !tbaa !2
  %21 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %21, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %22 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %22, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %23 = load i8*, i8** %22, align 4, !tbaa !2
  store i8* %23, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !27
  %25 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 %27
  %28 = load i8, i8* %arrayidx7, align 1, !tbaa !36
  %29 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %29, i32 1
  store i8 %28, i8* %arrayidx8, align 1, !tbaa !36
  %30 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %31 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %30, i32 %31
  %32 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %33 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %33, i32 2
  store i8 %32, i8* %arrayidx10, align 1, !tbaa !36
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %35 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %37, i32 3
  store i8 %36, i8* %arrayidx12, align 1, !tbaa !36
  %38 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i8, i8* %38, i32 0
  store i8 -1, i8* %arrayidx13, align 1, !tbaa !36
  %39 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %39, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %col, align 4, !tbaa !27
  %inc14 = add i32 %40, 1
  store i32 %inc14, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %41 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  %46 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_rgb_convert_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr0, align 4, !tbaa !2
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %13, i32 1
  %14 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %16, i8** %inptr1, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %17, i32 2
  %18 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %20, i8** %inptr2, align 4, !tbaa !2
  %21 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %21, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %22 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %22, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %23 = load i8*, i8** %22, align 4, !tbaa !2
  store i8* %23, i8** %outptr, align 4, !tbaa !2
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %24 = load i32, i32* %col, align 4, !tbaa !27
  %25 = load i32, i32* %num_cols, align 4, !tbaa !27
  %cmp6 = icmp ult i32 %24, %25
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8, i8* %26, i32 %27
  %28 = load i8, i8* %arrayidx7, align 1, !tbaa !36
  %29 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8, i8* %29, i32 0
  store i8 %28, i8* %arrayidx8, align 1, !tbaa !36
  %30 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %31 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx9 = getelementptr inbounds i8, i8* %30, i32 %31
  %32 = load i8, i8* %arrayidx9, align 1, !tbaa !36
  %33 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8, i8* %33, i32 1
  store i8 %32, i8* %arrayidx10, align 1, !tbaa !36
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %35 = load i32, i32* %col, align 4, !tbaa !27
  %arrayidx11 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx11, align 1, !tbaa !36
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i8, i8* %37, i32 2
  store i8 %36, i8* %arrayidx12, align 1, !tbaa !36
  %38 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %38, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %39 = load i32, i32* %col, align 4, !tbaa !27
  %inc13 = add i32 %39, 1
  store i32 %inc13, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %40 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  %44 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #4
  %45 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal i32 @is_big_endian() #3 {
entry:
  %retval = alloca i32, align 4
  %test_value = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %0 = bitcast i32* %test_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 1, i32* %test_value, align 4, !tbaa !27
  %1 = bitcast i32* %test_value to i8*
  %2 = load i8, i8* %1, align 4, !tbaa !36
  %conv = sext i8 %2 to i32
  %cmp = icmp ne i32 %conv, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %3 = bitcast i32* %test_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_rgb565_convert_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %rgb = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end132, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %35, i32 0
  %36 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %38, i8** %inptr0, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %39, i32 1
  %40 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %42, i8** %inptr1, align 4, !tbaa !2
  %43 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %43, i32 2
  %44 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %45 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %44, i32 %45
  %46 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %46, i8** %inptr2, align 4, !tbaa !2
  %47 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %47, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %48, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %49 = load i8*, i8** %48, align 4, !tbaa !2
  store i8* %49, i8** %outptr, align 4, !tbaa !2
  %50 = load i8*, i8** %outptr, align 4, !tbaa !2
  %51 = ptrtoint i8* %50 to i32
  %and = and i32 %51, 3
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %52 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr7 = getelementptr inbounds i8, i8* %52, i32 1
  store i8* %incdec.ptr7, i8** %inptr0, align 4, !tbaa !2
  %53 = load i8, i8* %52, align 1, !tbaa !36
  %conv = zext i8 %53 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %54, i32 1
  store i8* %incdec.ptr8, i8** %inptr1, align 4, !tbaa !2
  %55 = load i8, i8* %54, align 1, !tbaa !36
  %conv9 = zext i8 %55 to i32
  store i32 %conv9, i32* %cb, align 4, !tbaa !27
  %56 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr10 = getelementptr inbounds i8, i8* %56, i32 1
  store i8* %incdec.ptr10, i8** %inptr2, align 4, !tbaa !2
  %57 = load i8, i8* %56, align 1, !tbaa !36
  %conv11 = zext i8 %57 to i32
  store i32 %conv11, i32* %cr, align 4, !tbaa !27
  %58 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %59 = load i32, i32* %y, align 4, !tbaa !27
  %60 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %61 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx12 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx12, align 4, !tbaa !27
  %add = add nsw i32 %59, %62
  %arrayidx13 = getelementptr inbounds i8, i8* %58, i32 %add
  %63 = load i8, i8* %arrayidx13, align 1, !tbaa !36
  %conv14 = zext i8 %63 to i32
  store i32 %conv14, i32* %r, align 4, !tbaa !27
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx15 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %68, %71
  %shr = ashr i32 %add17, 16
  %add18 = add nsw i32 %65, %shr
  %arrayidx19 = getelementptr inbounds i8, i8* %64, i32 %add18
  %72 = load i8, i8* %arrayidx19, align 1, !tbaa !36
  %conv20 = zext i8 %72 to i32
  store i32 %conv20, i32* %g, align 4, !tbaa !27
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !27
  %75 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %76 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx21 = getelementptr inbounds i32, i32* %75, i32 %76
  %77 = load i32, i32* %arrayidx21, align 4, !tbaa !27
  %add22 = add nsw i32 %74, %77
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %78 = load i8, i8* %arrayidx23, align 1, !tbaa !36
  %conv24 = zext i8 %78 to i32
  store i32 %conv24, i32* %b, align 4, !tbaa !27
  %79 = load i32, i32* %r, align 4, !tbaa !27
  %and25 = and i32 %79, 248
  %80 = load i32, i32* %g, align 4, !tbaa !27
  %shr26 = lshr i32 %80, 5
  %or = or i32 %and25, %shr26
  %81 = load i32, i32* %g, align 4, !tbaa !27
  %shl = shl i32 %81, 11
  %and27 = and i32 %shl, 57344
  %or28 = or i32 %or, %and27
  %82 = load i32, i32* %b, align 4, !tbaa !27
  %shl29 = shl i32 %82, 5
  %and30 = and i32 %shl29, 7936
  %or31 = or i32 %or28, %and30
  store i32 %or31, i32* %rgb, align 4, !tbaa !37
  %83 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv32 = trunc i32 %83 to i16
  %84 = load i8*, i8** %outptr, align 4, !tbaa !2
  %85 = bitcast i8* %84 to i16*
  store i16 %conv32, i16* %85, align 2, !tbaa !43
  %86 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %86, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %87 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec33 = add i32 %87, -1
  store i32 %dec33, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %88 = load i32, i32* %col, align 4, !tbaa !27
  %89 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr34 = lshr i32 %89, 1
  %cmp35 = icmp ult i32 %88, %shr34
  br i1 %cmp35, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %90 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr37 = getelementptr inbounds i8, i8* %90, i32 1
  store i8* %incdec.ptr37, i8** %inptr0, align 4, !tbaa !2
  %91 = load i8, i8* %90, align 1, !tbaa !36
  %conv38 = zext i8 %91 to i32
  store i32 %conv38, i32* %y, align 4, !tbaa !27
  %92 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr39 = getelementptr inbounds i8, i8* %92, i32 1
  store i8* %incdec.ptr39, i8** %inptr1, align 4, !tbaa !2
  %93 = load i8, i8* %92, align 1, !tbaa !36
  %conv40 = zext i8 %93 to i32
  store i32 %conv40, i32* %cb, align 4, !tbaa !27
  %94 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr41 = getelementptr inbounds i8, i8* %94, i32 1
  store i8* %incdec.ptr41, i8** %inptr2, align 4, !tbaa !2
  %95 = load i8, i8* %94, align 1, !tbaa !36
  %conv42 = zext i8 %95 to i32
  store i32 %conv42, i32* %cr, align 4, !tbaa !27
  %96 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %97 = load i32, i32* %y, align 4, !tbaa !27
  %98 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %99 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx43 = getelementptr inbounds i32, i32* %98, i32 %99
  %100 = load i32, i32* %arrayidx43, align 4, !tbaa !27
  %add44 = add nsw i32 %97, %100
  %arrayidx45 = getelementptr inbounds i8, i8* %96, i32 %add44
  %101 = load i8, i8* %arrayidx45, align 1, !tbaa !36
  %conv46 = zext i8 %101 to i32
  store i32 %conv46, i32* %r, align 4, !tbaa !27
  %102 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %103 = load i32, i32* %y, align 4, !tbaa !27
  %104 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %105 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx47 = getelementptr inbounds i32, i32* %104, i32 %105
  %106 = load i32, i32* %arrayidx47, align 4, !tbaa !37
  %107 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %108 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx48 = getelementptr inbounds i32, i32* %107, i32 %108
  %109 = load i32, i32* %arrayidx48, align 4, !tbaa !37
  %add49 = add nsw i32 %106, %109
  %shr50 = ashr i32 %add49, 16
  %add51 = add nsw i32 %103, %shr50
  %arrayidx52 = getelementptr inbounds i8, i8* %102, i32 %add51
  %110 = load i8, i8* %arrayidx52, align 1, !tbaa !36
  %conv53 = zext i8 %110 to i32
  store i32 %conv53, i32* %g, align 4, !tbaa !27
  %111 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %112 = load i32, i32* %y, align 4, !tbaa !27
  %113 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %114 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx54 = getelementptr inbounds i32, i32* %113, i32 %114
  %115 = load i32, i32* %arrayidx54, align 4, !tbaa !27
  %add55 = add nsw i32 %112, %115
  %arrayidx56 = getelementptr inbounds i8, i8* %111, i32 %add55
  %116 = load i8, i8* %arrayidx56, align 1, !tbaa !36
  %conv57 = zext i8 %116 to i32
  store i32 %conv57, i32* %b, align 4, !tbaa !27
  %117 = load i32, i32* %r, align 4, !tbaa !27
  %and58 = and i32 %117, 248
  %118 = load i32, i32* %g, align 4, !tbaa !27
  %shr59 = lshr i32 %118, 5
  %or60 = or i32 %and58, %shr59
  %119 = load i32, i32* %g, align 4, !tbaa !27
  %shl61 = shl i32 %119, 11
  %and62 = and i32 %shl61, 57344
  %or63 = or i32 %or60, %and62
  %120 = load i32, i32* %b, align 4, !tbaa !27
  %shl64 = shl i32 %120, 5
  %and65 = and i32 %shl64, 7936
  %or66 = or i32 %or63, %and65
  store i32 %or66, i32* %rgb, align 4, !tbaa !37
  %121 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr67 = getelementptr inbounds i8, i8* %121, i32 1
  store i8* %incdec.ptr67, i8** %inptr0, align 4, !tbaa !2
  %122 = load i8, i8* %121, align 1, !tbaa !36
  %conv68 = zext i8 %122 to i32
  store i32 %conv68, i32* %y, align 4, !tbaa !27
  %123 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr69 = getelementptr inbounds i8, i8* %123, i32 1
  store i8* %incdec.ptr69, i8** %inptr1, align 4, !tbaa !2
  %124 = load i8, i8* %123, align 1, !tbaa !36
  %conv70 = zext i8 %124 to i32
  store i32 %conv70, i32* %cb, align 4, !tbaa !27
  %125 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr71 = getelementptr inbounds i8, i8* %125, i32 1
  store i8* %incdec.ptr71, i8** %inptr2, align 4, !tbaa !2
  %126 = load i8, i8* %125, align 1, !tbaa !36
  %conv72 = zext i8 %126 to i32
  store i32 %conv72, i32* %cr, align 4, !tbaa !27
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !27
  %129 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %130 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx73 = getelementptr inbounds i32, i32* %129, i32 %130
  %131 = load i32, i32* %arrayidx73, align 4, !tbaa !27
  %add74 = add nsw i32 %128, %131
  %arrayidx75 = getelementptr inbounds i8, i8* %127, i32 %add74
  %132 = load i8, i8* %arrayidx75, align 1, !tbaa !36
  %conv76 = zext i8 %132 to i32
  store i32 %conv76, i32* %r, align 4, !tbaa !27
  %133 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %134 = load i32, i32* %y, align 4, !tbaa !27
  %135 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %136 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx77 = getelementptr inbounds i32, i32* %135, i32 %136
  %137 = load i32, i32* %arrayidx77, align 4, !tbaa !37
  %138 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %139 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx78 = getelementptr inbounds i32, i32* %138, i32 %139
  %140 = load i32, i32* %arrayidx78, align 4, !tbaa !37
  %add79 = add nsw i32 %137, %140
  %shr80 = ashr i32 %add79, 16
  %add81 = add nsw i32 %134, %shr80
  %arrayidx82 = getelementptr inbounds i8, i8* %133, i32 %add81
  %141 = load i8, i8* %arrayidx82, align 1, !tbaa !36
  %conv83 = zext i8 %141 to i32
  store i32 %conv83, i32* %g, align 4, !tbaa !27
  %142 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %143 = load i32, i32* %y, align 4, !tbaa !27
  %144 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %145 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx84 = getelementptr inbounds i32, i32* %144, i32 %145
  %146 = load i32, i32* %arrayidx84, align 4, !tbaa !27
  %add85 = add nsw i32 %143, %146
  %arrayidx86 = getelementptr inbounds i8, i8* %142, i32 %add85
  %147 = load i8, i8* %arrayidx86, align 1, !tbaa !36
  %conv87 = zext i8 %147 to i32
  store i32 %conv87, i32* %b, align 4, !tbaa !27
  %148 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl88 = shl i32 %148, 16
  %149 = load i32, i32* %r, align 4, !tbaa !27
  %and89 = and i32 %149, 248
  %150 = load i32, i32* %g, align 4, !tbaa !27
  %shr90 = lshr i32 %150, 5
  %or91 = or i32 %and89, %shr90
  %151 = load i32, i32* %g, align 4, !tbaa !27
  %shl92 = shl i32 %151, 11
  %and93 = and i32 %shl92, 57344
  %or94 = or i32 %or91, %and93
  %152 = load i32, i32* %b, align 4, !tbaa !27
  %shl95 = shl i32 %152, 5
  %and96 = and i32 %shl95, 7936
  %or97 = or i32 %or94, %and96
  %or98 = or i32 %shl88, %or97
  store i32 %or98, i32* %rgb, align 4, !tbaa !37
  %153 = load i32, i32* %rgb, align 4, !tbaa !37
  %154 = load i8*, i8** %outptr, align 4, !tbaa !2
  %155 = bitcast i8* %154 to i32*
  store i32 %153, i32* %155, align 4, !tbaa !27
  %156 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr99 = getelementptr inbounds i8, i8* %156, i32 4
  store i8* %add.ptr99, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %157 = load i32, i32* %col, align 4, !tbaa !27
  %inc100 = add i32 %157, 1
  store i32 %inc100, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %158 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and101 = and i32 %158, 1
  %tobool102 = icmp ne i32 %and101, 0
  br i1 %tobool102, label %if.then103, label %if.end132

if.then103:                                       ; preds = %for.end
  %159 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %160 = load i8, i8* %159, align 1, !tbaa !36
  %conv104 = zext i8 %160 to i32
  store i32 %conv104, i32* %y, align 4, !tbaa !27
  %161 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %162 = load i8, i8* %161, align 1, !tbaa !36
  %conv105 = zext i8 %162 to i32
  store i32 %conv105, i32* %cb, align 4, !tbaa !27
  %163 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %164 = load i8, i8* %163, align 1, !tbaa !36
  %conv106 = zext i8 %164 to i32
  store i32 %conv106, i32* %cr, align 4, !tbaa !27
  %165 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %166 = load i32, i32* %y, align 4, !tbaa !27
  %167 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %168 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx107 = getelementptr inbounds i32, i32* %167, i32 %168
  %169 = load i32, i32* %arrayidx107, align 4, !tbaa !27
  %add108 = add nsw i32 %166, %169
  %arrayidx109 = getelementptr inbounds i8, i8* %165, i32 %add108
  %170 = load i8, i8* %arrayidx109, align 1, !tbaa !36
  %conv110 = zext i8 %170 to i32
  store i32 %conv110, i32* %r, align 4, !tbaa !27
  %171 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %172 = load i32, i32* %y, align 4, !tbaa !27
  %173 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %174 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx111 = getelementptr inbounds i32, i32* %173, i32 %174
  %175 = load i32, i32* %arrayidx111, align 4, !tbaa !37
  %176 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %177 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx112 = getelementptr inbounds i32, i32* %176, i32 %177
  %178 = load i32, i32* %arrayidx112, align 4, !tbaa !37
  %add113 = add nsw i32 %175, %178
  %shr114 = ashr i32 %add113, 16
  %add115 = add nsw i32 %172, %shr114
  %arrayidx116 = getelementptr inbounds i8, i8* %171, i32 %add115
  %179 = load i8, i8* %arrayidx116, align 1, !tbaa !36
  %conv117 = zext i8 %179 to i32
  store i32 %conv117, i32* %g, align 4, !tbaa !27
  %180 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %181 = load i32, i32* %y, align 4, !tbaa !27
  %182 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %183 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx118 = getelementptr inbounds i32, i32* %182, i32 %183
  %184 = load i32, i32* %arrayidx118, align 4, !tbaa !27
  %add119 = add nsw i32 %181, %184
  %arrayidx120 = getelementptr inbounds i8, i8* %180, i32 %add119
  %185 = load i8, i8* %arrayidx120, align 1, !tbaa !36
  %conv121 = zext i8 %185 to i32
  store i32 %conv121, i32* %b, align 4, !tbaa !27
  %186 = load i32, i32* %r, align 4, !tbaa !27
  %and122 = and i32 %186, 248
  %187 = load i32, i32* %g, align 4, !tbaa !27
  %shr123 = lshr i32 %187, 5
  %or124 = or i32 %and122, %shr123
  %188 = load i32, i32* %g, align 4, !tbaa !27
  %shl125 = shl i32 %188, 11
  %and126 = and i32 %shl125, 57344
  %or127 = or i32 %or124, %and126
  %189 = load i32, i32* %b, align 4, !tbaa !27
  %shl128 = shl i32 %189, 5
  %and129 = and i32 %shl128, 7936
  %or130 = or i32 %or127, %and129
  store i32 %or130, i32* %rgb, align 4, !tbaa !37
  %190 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv131 = trunc i32 %190 to i16
  %191 = load i8*, i8** %outptr, align 4, !tbaa !2
  %192 = bitcast i8* %191 to i16*
  store i16 %conv131, i16* %192, align 2, !tbaa !43
  br label %if.end132

if.end132:                                        ; preds = %if.then103, %for.end
  %193 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #4
  %194 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #4
  %195 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #4
  %196 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %197 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #4
  %198 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  %199 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #4
  %200 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #4
  %201 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #4
  %202 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_rgb565_convert_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %rgb = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end124, %entry
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %31 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %35, i32 0
  %36 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %37 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %38, i8** %inptr0, align 4, !tbaa !2
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %39, i32 1
  %40 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %42, i8** %inptr1, align 4, !tbaa !2
  %43 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %43, i32 2
  %44 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %45 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %44, i32 %45
  %46 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %46, i8** %inptr2, align 4, !tbaa !2
  %47 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %47, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %48, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %49 = load i8*, i8** %48, align 4, !tbaa !2
  store i8* %49, i8** %outptr, align 4, !tbaa !2
  %50 = load i8*, i8** %outptr, align 4, !tbaa !2
  %51 = ptrtoint i8* %50 to i32
  %and = and i32 %51, 3
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %52 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr7 = getelementptr inbounds i8, i8* %52, i32 1
  store i8* %incdec.ptr7, i8** %inptr0, align 4, !tbaa !2
  %53 = load i8, i8* %52, align 1, !tbaa !36
  %conv = zext i8 %53 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %54 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %54, i32 1
  store i8* %incdec.ptr8, i8** %inptr1, align 4, !tbaa !2
  %55 = load i8, i8* %54, align 1, !tbaa !36
  %conv9 = zext i8 %55 to i32
  store i32 %conv9, i32* %cb, align 4, !tbaa !27
  %56 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr10 = getelementptr inbounds i8, i8* %56, i32 1
  store i8* %incdec.ptr10, i8** %inptr2, align 4, !tbaa !2
  %57 = load i8, i8* %56, align 1, !tbaa !36
  %conv11 = zext i8 %57 to i32
  store i32 %conv11, i32* %cr, align 4, !tbaa !27
  %58 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %59 = load i32, i32* %y, align 4, !tbaa !27
  %60 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %61 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx12 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx12, align 4, !tbaa !27
  %add = add nsw i32 %59, %62
  %arrayidx13 = getelementptr inbounds i8, i8* %58, i32 %add
  %63 = load i8, i8* %arrayidx13, align 1, !tbaa !36
  %conv14 = zext i8 %63 to i32
  store i32 %conv14, i32* %r, align 4, !tbaa !27
  %64 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %65 = load i32, i32* %y, align 4, !tbaa !27
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx15 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx16 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %68, %71
  %shr = ashr i32 %add17, 16
  %add18 = add nsw i32 %65, %shr
  %arrayidx19 = getelementptr inbounds i8, i8* %64, i32 %add18
  %72 = load i8, i8* %arrayidx19, align 1, !tbaa !36
  %conv20 = zext i8 %72 to i32
  store i32 %conv20, i32* %g, align 4, !tbaa !27
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !27
  %75 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %76 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx21 = getelementptr inbounds i32, i32* %75, i32 %76
  %77 = load i32, i32* %arrayidx21, align 4, !tbaa !27
  %add22 = add nsw i32 %74, %77
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %78 = load i8, i8* %arrayidx23, align 1, !tbaa !36
  %conv24 = zext i8 %78 to i32
  store i32 %conv24, i32* %b, align 4, !tbaa !27
  %79 = load i32, i32* %r, align 4, !tbaa !27
  %shl = shl i32 %79, 8
  %and25 = and i32 %shl, 63488
  %80 = load i32, i32* %g, align 4, !tbaa !27
  %shl26 = shl i32 %80, 3
  %and27 = and i32 %shl26, 2016
  %or = or i32 %and25, %and27
  %81 = load i32, i32* %b, align 4, !tbaa !27
  %shr28 = lshr i32 %81, 3
  %or29 = or i32 %or, %shr28
  store i32 %or29, i32* %rgb, align 4, !tbaa !37
  %82 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv30 = trunc i32 %82 to i16
  %83 = load i8*, i8** %outptr, align 4, !tbaa !2
  %84 = bitcast i8* %83 to i16*
  store i16 %conv30, i16* %84, align 2, !tbaa !43
  %85 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %85, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %86 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec31 = add i32 %86, -1
  store i32 %dec31, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %87 = load i32, i32* %col, align 4, !tbaa !27
  %88 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr32 = lshr i32 %88, 1
  %cmp33 = icmp ult i32 %87, %shr32
  br i1 %cmp33, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %89 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr35 = getelementptr inbounds i8, i8* %89, i32 1
  store i8* %incdec.ptr35, i8** %inptr0, align 4, !tbaa !2
  %90 = load i8, i8* %89, align 1, !tbaa !36
  %conv36 = zext i8 %90 to i32
  store i32 %conv36, i32* %y, align 4, !tbaa !27
  %91 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr37 = getelementptr inbounds i8, i8* %91, i32 1
  store i8* %incdec.ptr37, i8** %inptr1, align 4, !tbaa !2
  %92 = load i8, i8* %91, align 1, !tbaa !36
  %conv38 = zext i8 %92 to i32
  store i32 %conv38, i32* %cb, align 4, !tbaa !27
  %93 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr39 = getelementptr inbounds i8, i8* %93, i32 1
  store i8* %incdec.ptr39, i8** %inptr2, align 4, !tbaa !2
  %94 = load i8, i8* %93, align 1, !tbaa !36
  %conv40 = zext i8 %94 to i32
  store i32 %conv40, i32* %cr, align 4, !tbaa !27
  %95 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %96 = load i32, i32* %y, align 4, !tbaa !27
  %97 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %98 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx41 = getelementptr inbounds i32, i32* %97, i32 %98
  %99 = load i32, i32* %arrayidx41, align 4, !tbaa !27
  %add42 = add nsw i32 %96, %99
  %arrayidx43 = getelementptr inbounds i8, i8* %95, i32 %add42
  %100 = load i8, i8* %arrayidx43, align 1, !tbaa !36
  %conv44 = zext i8 %100 to i32
  store i32 %conv44, i32* %r, align 4, !tbaa !27
  %101 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %102 = load i32, i32* %y, align 4, !tbaa !27
  %103 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %104 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx45 = getelementptr inbounds i32, i32* %103, i32 %104
  %105 = load i32, i32* %arrayidx45, align 4, !tbaa !37
  %106 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %107 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx46 = getelementptr inbounds i32, i32* %106, i32 %107
  %108 = load i32, i32* %arrayidx46, align 4, !tbaa !37
  %add47 = add nsw i32 %105, %108
  %shr48 = ashr i32 %add47, 16
  %add49 = add nsw i32 %102, %shr48
  %arrayidx50 = getelementptr inbounds i8, i8* %101, i32 %add49
  %109 = load i8, i8* %arrayidx50, align 1, !tbaa !36
  %conv51 = zext i8 %109 to i32
  store i32 %conv51, i32* %g, align 4, !tbaa !27
  %110 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %111 = load i32, i32* %y, align 4, !tbaa !27
  %112 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %113 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx52 = getelementptr inbounds i32, i32* %112, i32 %113
  %114 = load i32, i32* %arrayidx52, align 4, !tbaa !27
  %add53 = add nsw i32 %111, %114
  %arrayidx54 = getelementptr inbounds i8, i8* %110, i32 %add53
  %115 = load i8, i8* %arrayidx54, align 1, !tbaa !36
  %conv55 = zext i8 %115 to i32
  store i32 %conv55, i32* %b, align 4, !tbaa !27
  %116 = load i32, i32* %r, align 4, !tbaa !27
  %shl56 = shl i32 %116, 8
  %and57 = and i32 %shl56, 63488
  %117 = load i32, i32* %g, align 4, !tbaa !27
  %shl58 = shl i32 %117, 3
  %and59 = and i32 %shl58, 2016
  %or60 = or i32 %and57, %and59
  %118 = load i32, i32* %b, align 4, !tbaa !27
  %shr61 = lshr i32 %118, 3
  %or62 = or i32 %or60, %shr61
  store i32 %or62, i32* %rgb, align 4, !tbaa !37
  %119 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr63 = getelementptr inbounds i8, i8* %119, i32 1
  store i8* %incdec.ptr63, i8** %inptr0, align 4, !tbaa !2
  %120 = load i8, i8* %119, align 1, !tbaa !36
  %conv64 = zext i8 %120 to i32
  store i32 %conv64, i32* %y, align 4, !tbaa !27
  %121 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr65 = getelementptr inbounds i8, i8* %121, i32 1
  store i8* %incdec.ptr65, i8** %inptr1, align 4, !tbaa !2
  %122 = load i8, i8* %121, align 1, !tbaa !36
  %conv66 = zext i8 %122 to i32
  store i32 %conv66, i32* %cb, align 4, !tbaa !27
  %123 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr67 = getelementptr inbounds i8, i8* %123, i32 1
  store i8* %incdec.ptr67, i8** %inptr2, align 4, !tbaa !2
  %124 = load i8, i8* %123, align 1, !tbaa !36
  %conv68 = zext i8 %124 to i32
  store i32 %conv68, i32* %cr, align 4, !tbaa !27
  %125 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %126 = load i32, i32* %y, align 4, !tbaa !27
  %127 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %128 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx69 = getelementptr inbounds i32, i32* %127, i32 %128
  %129 = load i32, i32* %arrayidx69, align 4, !tbaa !27
  %add70 = add nsw i32 %126, %129
  %arrayidx71 = getelementptr inbounds i8, i8* %125, i32 %add70
  %130 = load i8, i8* %arrayidx71, align 1, !tbaa !36
  %conv72 = zext i8 %130 to i32
  store i32 %conv72, i32* %r, align 4, !tbaa !27
  %131 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %132 = load i32, i32* %y, align 4, !tbaa !27
  %133 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %134 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx73 = getelementptr inbounds i32, i32* %133, i32 %134
  %135 = load i32, i32* %arrayidx73, align 4, !tbaa !37
  %136 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %137 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx74 = getelementptr inbounds i32, i32* %136, i32 %137
  %138 = load i32, i32* %arrayidx74, align 4, !tbaa !37
  %add75 = add nsw i32 %135, %138
  %shr76 = ashr i32 %add75, 16
  %add77 = add nsw i32 %132, %shr76
  %arrayidx78 = getelementptr inbounds i8, i8* %131, i32 %add77
  %139 = load i8, i8* %arrayidx78, align 1, !tbaa !36
  %conv79 = zext i8 %139 to i32
  store i32 %conv79, i32* %g, align 4, !tbaa !27
  %140 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !27
  %142 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %143 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx80 = getelementptr inbounds i32, i32* %142, i32 %143
  %144 = load i32, i32* %arrayidx80, align 4, !tbaa !27
  %add81 = add nsw i32 %141, %144
  %arrayidx82 = getelementptr inbounds i8, i8* %140, i32 %add81
  %145 = load i8, i8* %arrayidx82, align 1, !tbaa !36
  %conv83 = zext i8 %145 to i32
  store i32 %conv83, i32* %b, align 4, !tbaa !27
  %146 = load i32, i32* %r, align 4, !tbaa !27
  %shl84 = shl i32 %146, 8
  %and85 = and i32 %shl84, 63488
  %147 = load i32, i32* %g, align 4, !tbaa !27
  %shl86 = shl i32 %147, 3
  %and87 = and i32 %shl86, 2016
  %or88 = or i32 %and85, %and87
  %148 = load i32, i32* %b, align 4, !tbaa !27
  %shr89 = lshr i32 %148, 3
  %or90 = or i32 %or88, %shr89
  %shl91 = shl i32 %or90, 16
  %149 = load i32, i32* %rgb, align 4, !tbaa !37
  %or92 = or i32 %shl91, %149
  store i32 %or92, i32* %rgb, align 4, !tbaa !37
  %150 = load i32, i32* %rgb, align 4, !tbaa !37
  %151 = load i8*, i8** %outptr, align 4, !tbaa !2
  %152 = bitcast i8* %151 to i32*
  store i32 %150, i32* %152, align 4, !tbaa !27
  %153 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr93 = getelementptr inbounds i8, i8* %153, i32 4
  store i8* %add.ptr93, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %154 = load i32, i32* %col, align 4, !tbaa !27
  %inc94 = add i32 %154, 1
  store i32 %inc94, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %155 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and95 = and i32 %155, 1
  %tobool96 = icmp ne i32 %and95, 0
  br i1 %tobool96, label %if.then97, label %if.end124

if.then97:                                        ; preds = %for.end
  %156 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %157 = load i8, i8* %156, align 1, !tbaa !36
  %conv98 = zext i8 %157 to i32
  store i32 %conv98, i32* %y, align 4, !tbaa !27
  %158 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %159 = load i8, i8* %158, align 1, !tbaa !36
  %conv99 = zext i8 %159 to i32
  store i32 %conv99, i32* %cb, align 4, !tbaa !27
  %160 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %161 = load i8, i8* %160, align 1, !tbaa !36
  %conv100 = zext i8 %161 to i32
  store i32 %conv100, i32* %cr, align 4, !tbaa !27
  %162 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %163 = load i32, i32* %y, align 4, !tbaa !27
  %164 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %165 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx101 = getelementptr inbounds i32, i32* %164, i32 %165
  %166 = load i32, i32* %arrayidx101, align 4, !tbaa !27
  %add102 = add nsw i32 %163, %166
  %arrayidx103 = getelementptr inbounds i8, i8* %162, i32 %add102
  %167 = load i8, i8* %arrayidx103, align 1, !tbaa !36
  %conv104 = zext i8 %167 to i32
  store i32 %conv104, i32* %r, align 4, !tbaa !27
  %168 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %169 = load i32, i32* %y, align 4, !tbaa !27
  %170 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %171 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx105 = getelementptr inbounds i32, i32* %170, i32 %171
  %172 = load i32, i32* %arrayidx105, align 4, !tbaa !37
  %173 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %174 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx106 = getelementptr inbounds i32, i32* %173, i32 %174
  %175 = load i32, i32* %arrayidx106, align 4, !tbaa !37
  %add107 = add nsw i32 %172, %175
  %shr108 = ashr i32 %add107, 16
  %add109 = add nsw i32 %169, %shr108
  %arrayidx110 = getelementptr inbounds i8, i8* %168, i32 %add109
  %176 = load i8, i8* %arrayidx110, align 1, !tbaa !36
  %conv111 = zext i8 %176 to i32
  store i32 %conv111, i32* %g, align 4, !tbaa !27
  %177 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %178 = load i32, i32* %y, align 4, !tbaa !27
  %179 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %180 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx112 = getelementptr inbounds i32, i32* %179, i32 %180
  %181 = load i32, i32* %arrayidx112, align 4, !tbaa !27
  %add113 = add nsw i32 %178, %181
  %arrayidx114 = getelementptr inbounds i8, i8* %177, i32 %add113
  %182 = load i8, i8* %arrayidx114, align 1, !tbaa !36
  %conv115 = zext i8 %182 to i32
  store i32 %conv115, i32* %b, align 4, !tbaa !27
  %183 = load i32, i32* %r, align 4, !tbaa !27
  %shl116 = shl i32 %183, 8
  %and117 = and i32 %shl116, 63488
  %184 = load i32, i32* %g, align 4, !tbaa !27
  %shl118 = shl i32 %184, 3
  %and119 = and i32 %shl118, 2016
  %or120 = or i32 %and117, %and119
  %185 = load i32, i32* %b, align 4, !tbaa !27
  %shr121 = lshr i32 %185, 3
  %or122 = or i32 %or120, %shr121
  store i32 %or122, i32* %rgb, align 4, !tbaa !37
  %186 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv123 = trunc i32 %186 to i16
  %187 = load i8*, i8** %outptr, align 4, !tbaa !2
  %188 = bitcast i8* %187 to i16*
  store i16 %conv123, i16* %188, align 2, !tbaa !43
  br label %if.end124

if.end124:                                        ; preds = %if.then97, %for.end
  %189 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #4
  %190 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #4
  %191 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #4
  %192 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %193 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #4
  %194 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #4
  %195 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #4
  %196 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  %197 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #4
  %198 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  %199 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #4
  %200 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #4
  %201 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #4
  %202 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_rgb565_convert_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %rgb = alloca i32, align 4
  %g = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %if.end54, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %11, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr, align 4, !tbaa !2
  %13 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %13, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %14 = load i8*, i8** %13, align 4, !tbaa !2
  store i8* %14, i8** %outptr, align 4, !tbaa !2
  %15 = load i8*, i8** %outptr, align 4, !tbaa !2
  %16 = ptrtoint i8* %15 to i32
  %and = and i32 %16, 3
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %17 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr2 = getelementptr inbounds i8, i8* %17, i32 1
  store i8* %incdec.ptr2, i8** %inptr, align 4, !tbaa !2
  %18 = load i8, i8* %17, align 1, !tbaa !36
  %conv = zext i8 %18 to i32
  store i32 %conv, i32* %g, align 4, !tbaa !27
  %19 = load i32, i32* %g, align 4, !tbaa !27
  %and3 = and i32 %19, 248
  %20 = load i32, i32* %g, align 4, !tbaa !27
  %shr = lshr i32 %20, 5
  %or = or i32 %and3, %shr
  %21 = load i32, i32* %g, align 4, !tbaa !27
  %shl = shl i32 %21, 11
  %and4 = and i32 %shl, 57344
  %or5 = or i32 %or, %and4
  %22 = load i32, i32* %g, align 4, !tbaa !27
  %shl6 = shl i32 %22, 5
  %and7 = and i32 %shl6, 7936
  %or8 = or i32 %or5, %and7
  store i32 %or8, i32* %rgb, align 4, !tbaa !37
  %23 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv9 = trunc i32 %23 to i16
  %24 = load i8*, i8** %outptr, align 4, !tbaa !2
  %25 = bitcast i8* %24 to i16*
  store i16 %conv9, i16* %25, align 2, !tbaa !43
  %26 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %26, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %27 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec10 = add i32 %27, -1
  store i32 %dec10, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %28 = load i32, i32* %col, align 4, !tbaa !27
  %29 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr11 = lshr i32 %29, 1
  %cmp12 = icmp ult i32 %28, %shr11
  br i1 %cmp12, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %30 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr14 = getelementptr inbounds i8, i8* %30, i32 1
  store i8* %incdec.ptr14, i8** %inptr, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !36
  %conv15 = zext i8 %31 to i32
  store i32 %conv15, i32* %g, align 4, !tbaa !27
  %32 = load i32, i32* %g, align 4, !tbaa !27
  %and16 = and i32 %32, 248
  %33 = load i32, i32* %g, align 4, !tbaa !27
  %shr17 = lshr i32 %33, 5
  %or18 = or i32 %and16, %shr17
  %34 = load i32, i32* %g, align 4, !tbaa !27
  %shl19 = shl i32 %34, 11
  %and20 = and i32 %shl19, 57344
  %or21 = or i32 %or18, %and20
  %35 = load i32, i32* %g, align 4, !tbaa !27
  %shl22 = shl i32 %35, 5
  %and23 = and i32 %shl22, 7936
  %or24 = or i32 %or21, %and23
  store i32 %or24, i32* %rgb, align 4, !tbaa !37
  %36 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr25 = getelementptr inbounds i8, i8* %36, i32 1
  store i8* %incdec.ptr25, i8** %inptr, align 4, !tbaa !2
  %37 = load i8, i8* %36, align 1, !tbaa !36
  %conv26 = zext i8 %37 to i32
  store i32 %conv26, i32* %g, align 4, !tbaa !27
  %38 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl27 = shl i32 %38, 16
  %39 = load i32, i32* %g, align 4, !tbaa !27
  %and28 = and i32 %39, 248
  %40 = load i32, i32* %g, align 4, !tbaa !27
  %shr29 = lshr i32 %40, 5
  %or30 = or i32 %and28, %shr29
  %41 = load i32, i32* %g, align 4, !tbaa !27
  %shl31 = shl i32 %41, 11
  %and32 = and i32 %shl31, 57344
  %or33 = or i32 %or30, %and32
  %42 = load i32, i32* %g, align 4, !tbaa !27
  %shl34 = shl i32 %42, 5
  %and35 = and i32 %shl34, 7936
  %or36 = or i32 %or33, %and35
  %or37 = or i32 %shl27, %or36
  store i32 %or37, i32* %rgb, align 4, !tbaa !37
  %43 = load i32, i32* %rgb, align 4, !tbaa !37
  %44 = load i8*, i8** %outptr, align 4, !tbaa !2
  %45 = bitcast i8* %44 to i32*
  store i32 %43, i32* %45, align 4, !tbaa !27
  %46 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr38 = getelementptr inbounds i8, i8* %46, i32 4
  store i8* %add.ptr38, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %47 = load i32, i32* %col, align 4, !tbaa !27
  %inc39 = add i32 %47, 1
  store i32 %inc39, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %48 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and40 = and i32 %48, 1
  %tobool41 = icmp ne i32 %and40, 0
  br i1 %tobool41, label %if.then42, label %if.end54

if.then42:                                        ; preds = %for.end
  %49 = load i8*, i8** %inptr, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !36
  %conv43 = zext i8 %50 to i32
  store i32 %conv43, i32* %g, align 4, !tbaa !27
  %51 = load i32, i32* %g, align 4, !tbaa !27
  %and44 = and i32 %51, 248
  %52 = load i32, i32* %g, align 4, !tbaa !27
  %shr45 = lshr i32 %52, 5
  %or46 = or i32 %and44, %shr45
  %53 = load i32, i32* %g, align 4, !tbaa !27
  %shl47 = shl i32 %53, 11
  %and48 = and i32 %shl47, 57344
  %or49 = or i32 %or46, %and48
  %54 = load i32, i32* %g, align 4, !tbaa !27
  %shl50 = shl i32 %54, 5
  %and51 = and i32 %shl50, 7936
  %or52 = or i32 %or49, %and51
  store i32 %or52, i32* %rgb, align 4, !tbaa !37
  %55 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv53 = trunc i32 %55 to i16
  %56 = load i8*, i8** %outptr, align 4, !tbaa !2
  %57 = bitcast i8* %56 to i16*
  store i16 %conv53, i16* %57, align 2, !tbaa !43
  br label %if.end54

if.end54:                                         ; preds = %if.then42, %for.end
  %58 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %60 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #4
  %61 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %62 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #4
  %63 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_rgb565_convert_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %rgb = alloca i32, align 4
  %g = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 27
  %5 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %5, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %if.end46, %entry
  %6 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %9, i32 0
  %10 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %11 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %11, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %10, i32 %11
  %12 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %12, i8** %inptr, align 4, !tbaa !2
  %13 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %13, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %14 = load i8*, i8** %13, align 4, !tbaa !2
  store i8* %14, i8** %outptr, align 4, !tbaa !2
  %15 = load i8*, i8** %outptr, align 4, !tbaa !2
  %16 = ptrtoint i8* %15 to i32
  %and = and i32 %16, 3
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %17 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr2 = getelementptr inbounds i8, i8* %17, i32 1
  store i8* %incdec.ptr2, i8** %inptr, align 4, !tbaa !2
  %18 = load i8, i8* %17, align 1, !tbaa !36
  %conv = zext i8 %18 to i32
  store i32 %conv, i32* %g, align 4, !tbaa !27
  %19 = load i32, i32* %g, align 4, !tbaa !27
  %shl = shl i32 %19, 8
  %and3 = and i32 %shl, 63488
  %20 = load i32, i32* %g, align 4, !tbaa !27
  %shl4 = shl i32 %20, 3
  %and5 = and i32 %shl4, 2016
  %or = or i32 %and3, %and5
  %21 = load i32, i32* %g, align 4, !tbaa !27
  %shr = lshr i32 %21, 3
  %or6 = or i32 %or, %shr
  store i32 %or6, i32* %rgb, align 4, !tbaa !37
  %22 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv7 = trunc i32 %22 to i16
  %23 = load i8*, i8** %outptr, align 4, !tbaa !2
  %24 = bitcast i8* %23 to i16*
  store i16 %conv7, i16* %24, align 2, !tbaa !43
  %25 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %25, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %26 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec8 = add i32 %26, -1
  store i32 %dec8, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %27 = load i32, i32* %col, align 4, !tbaa !27
  %28 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr9 = lshr i32 %28, 1
  %cmp10 = icmp ult i32 %27, %shr9
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %29 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %29, i32 1
  store i8* %incdec.ptr12, i8** %inptr, align 4, !tbaa !2
  %30 = load i8, i8* %29, align 1, !tbaa !36
  %conv13 = zext i8 %30 to i32
  store i32 %conv13, i32* %g, align 4, !tbaa !27
  %31 = load i32, i32* %g, align 4, !tbaa !27
  %shl14 = shl i32 %31, 8
  %and15 = and i32 %shl14, 63488
  %32 = load i32, i32* %g, align 4, !tbaa !27
  %shl16 = shl i32 %32, 3
  %and17 = and i32 %shl16, 2016
  %or18 = or i32 %and15, %and17
  %33 = load i32, i32* %g, align 4, !tbaa !27
  %shr19 = lshr i32 %33, 3
  %or20 = or i32 %or18, %shr19
  store i32 %or20, i32* %rgb, align 4, !tbaa !37
  %34 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr21 = getelementptr inbounds i8, i8* %34, i32 1
  store i8* %incdec.ptr21, i8** %inptr, align 4, !tbaa !2
  %35 = load i8, i8* %34, align 1, !tbaa !36
  %conv22 = zext i8 %35 to i32
  store i32 %conv22, i32* %g, align 4, !tbaa !27
  %36 = load i32, i32* %g, align 4, !tbaa !27
  %shl23 = shl i32 %36, 8
  %and24 = and i32 %shl23, 63488
  %37 = load i32, i32* %g, align 4, !tbaa !27
  %shl25 = shl i32 %37, 3
  %and26 = and i32 %shl25, 2016
  %or27 = or i32 %and24, %and26
  %38 = load i32, i32* %g, align 4, !tbaa !27
  %shr28 = lshr i32 %38, 3
  %or29 = or i32 %or27, %shr28
  %shl30 = shl i32 %or29, 16
  %39 = load i32, i32* %rgb, align 4, !tbaa !37
  %or31 = or i32 %shl30, %39
  store i32 %or31, i32* %rgb, align 4, !tbaa !37
  %40 = load i32, i32* %rgb, align 4, !tbaa !37
  %41 = load i8*, i8** %outptr, align 4, !tbaa !2
  %42 = bitcast i8* %41 to i32*
  store i32 %40, i32* %42, align 4, !tbaa !27
  %43 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr32 = getelementptr inbounds i8, i8* %43, i32 4
  store i8* %add.ptr32, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %col, align 4, !tbaa !27
  %inc33 = add i32 %44, 1
  store i32 %inc33, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %45 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and34 = and i32 %45, 1
  %tobool35 = icmp ne i32 %and34, 0
  br i1 %tobool35, label %if.then36, label %if.end46

if.then36:                                        ; preds = %for.end
  %46 = load i8*, i8** %inptr, align 4, !tbaa !2
  %47 = load i8, i8* %46, align 1, !tbaa !36
  %conv37 = zext i8 %47 to i32
  store i32 %conv37, i32* %g, align 4, !tbaa !27
  %48 = load i32, i32* %g, align 4, !tbaa !27
  %shl38 = shl i32 %48, 8
  %and39 = and i32 %shl38, 63488
  %49 = load i32, i32* %g, align 4, !tbaa !27
  %shl40 = shl i32 %49, 3
  %and41 = and i32 %shl40, 2016
  %or42 = or i32 %and39, %and41
  %50 = load i32, i32* %g, align 4, !tbaa !27
  %shr43 = lshr i32 %50, 3
  %or44 = or i32 %or42, %shr43
  store i32 %or44, i32* %rgb, align 4, !tbaa !37
  %51 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv45 = trunc i32 %51 to i16
  %52 = load i8*, i8** %outptr, align 4, !tbaa !2
  %53 = bitcast i8* %52 to i16*
  store i16 %conv45, i16* %53, align 2, !tbaa !43
  br label %if.end46

if.end46:                                         ; preds = %if.then36, %for.end
  %54 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %56 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #4
  %57 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #4
  %58 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #4
  %59 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_rgb565_convert_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %rgb = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %if.end72, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %13, i32 0
  %14 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %16, i8** %inptr0, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %17, i32 1
  %18 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %20, i8** %inptr1, align 4, !tbaa !2
  %21 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %21, i32 2
  %22 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %23 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %24, i8** %inptr2, align 4, !tbaa !2
  %25 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %25, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %26 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %26, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %27 = load i8*, i8** %26, align 4, !tbaa !2
  store i8* %27, i8** %outptr, align 4, !tbaa !2
  %28 = load i8*, i8** %outptr, align 4, !tbaa !2
  %29 = ptrtoint i8* %28 to i32
  %and = and i32 %29, 3
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %30 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr6 = getelementptr inbounds i8, i8* %30, i32 1
  store i8* %incdec.ptr6, i8** %inptr0, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !36
  %conv = zext i8 %31 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !27
  %32 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr7 = getelementptr inbounds i8, i8* %32, i32 1
  store i8* %incdec.ptr7, i8** %inptr1, align 4, !tbaa !2
  %33 = load i8, i8* %32, align 1, !tbaa !36
  %conv8 = zext i8 %33 to i32
  store i32 %conv8, i32* %g, align 4, !tbaa !27
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %34, i32 1
  store i8* %incdec.ptr9, i8** %inptr2, align 4, !tbaa !2
  %35 = load i8, i8* %34, align 1, !tbaa !36
  %conv10 = zext i8 %35 to i32
  store i32 %conv10, i32* %b, align 4, !tbaa !27
  %36 = load i32, i32* %r, align 4, !tbaa !27
  %and11 = and i32 %36, 248
  %37 = load i32, i32* %g, align 4, !tbaa !27
  %shr = lshr i32 %37, 5
  %or = or i32 %and11, %shr
  %38 = load i32, i32* %g, align 4, !tbaa !27
  %shl = shl i32 %38, 11
  %and12 = and i32 %shl, 57344
  %or13 = or i32 %or, %and12
  %39 = load i32, i32* %b, align 4, !tbaa !27
  %shl14 = shl i32 %39, 5
  %and15 = and i32 %shl14, 7936
  %or16 = or i32 %or13, %and15
  store i32 %or16, i32* %rgb, align 4, !tbaa !37
  %40 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv17 = trunc i32 %40 to i16
  %41 = load i8*, i8** %outptr, align 4, !tbaa !2
  %42 = bitcast i8* %41 to i16*
  store i16 %conv17, i16* %42, align 2, !tbaa !43
  %43 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %43, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %44 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec18 = add i32 %44, -1
  store i32 %dec18, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %45 = load i32, i32* %col, align 4, !tbaa !27
  %46 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr19 = lshr i32 %46, 1
  %cmp20 = icmp ult i32 %45, %shr19
  br i1 %cmp20, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %47 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr22 = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr22, i8** %inptr0, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !36
  %conv23 = zext i8 %48 to i32
  store i32 %conv23, i32* %r, align 4, !tbaa !27
  %49 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr24 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr24, i8** %inptr1, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !36
  %conv25 = zext i8 %50 to i32
  store i32 %conv25, i32* %g, align 4, !tbaa !27
  %51 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr26 = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr26, i8** %inptr2, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !36
  %conv27 = zext i8 %52 to i32
  store i32 %conv27, i32* %b, align 4, !tbaa !27
  %53 = load i32, i32* %r, align 4, !tbaa !27
  %and28 = and i32 %53, 248
  %54 = load i32, i32* %g, align 4, !tbaa !27
  %shr29 = lshr i32 %54, 5
  %or30 = or i32 %and28, %shr29
  %55 = load i32, i32* %g, align 4, !tbaa !27
  %shl31 = shl i32 %55, 11
  %and32 = and i32 %shl31, 57344
  %or33 = or i32 %or30, %and32
  %56 = load i32, i32* %b, align 4, !tbaa !27
  %shl34 = shl i32 %56, 5
  %and35 = and i32 %shl34, 7936
  %or36 = or i32 %or33, %and35
  store i32 %or36, i32* %rgb, align 4, !tbaa !37
  %57 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr37 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr37, i8** %inptr0, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !36
  %conv38 = zext i8 %58 to i32
  store i32 %conv38, i32* %r, align 4, !tbaa !27
  %59 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr39 = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr39, i8** %inptr1, align 4, !tbaa !2
  %60 = load i8, i8* %59, align 1, !tbaa !36
  %conv40 = zext i8 %60 to i32
  store i32 %conv40, i32* %g, align 4, !tbaa !27
  %61 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr41 = getelementptr inbounds i8, i8* %61, i32 1
  store i8* %incdec.ptr41, i8** %inptr2, align 4, !tbaa !2
  %62 = load i8, i8* %61, align 1, !tbaa !36
  %conv42 = zext i8 %62 to i32
  store i32 %conv42, i32* %b, align 4, !tbaa !27
  %63 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl43 = shl i32 %63, 16
  %64 = load i32, i32* %r, align 4, !tbaa !27
  %and44 = and i32 %64, 248
  %65 = load i32, i32* %g, align 4, !tbaa !27
  %shr45 = lshr i32 %65, 5
  %or46 = or i32 %and44, %shr45
  %66 = load i32, i32* %g, align 4, !tbaa !27
  %shl47 = shl i32 %66, 11
  %and48 = and i32 %shl47, 57344
  %or49 = or i32 %or46, %and48
  %67 = load i32, i32* %b, align 4, !tbaa !27
  %shl50 = shl i32 %67, 5
  %and51 = and i32 %shl50, 7936
  %or52 = or i32 %or49, %and51
  %or53 = or i32 %shl43, %or52
  store i32 %or53, i32* %rgb, align 4, !tbaa !37
  %68 = load i32, i32* %rgb, align 4, !tbaa !37
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %70 = bitcast i8* %69 to i32*
  store i32 %68, i32* %70, align 4, !tbaa !27
  %71 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr54 = getelementptr inbounds i8, i8* %71, i32 4
  store i8* %add.ptr54, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %72 = load i32, i32* %col, align 4, !tbaa !27
  %inc55 = add i32 %72, 1
  store i32 %inc55, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %73 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and56 = and i32 %73, 1
  %tobool57 = icmp ne i32 %and56, 0
  br i1 %tobool57, label %if.then58, label %if.end72

if.then58:                                        ; preds = %for.end
  %74 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %75 = load i8, i8* %74, align 1, !tbaa !36
  %conv59 = zext i8 %75 to i32
  store i32 %conv59, i32* %r, align 4, !tbaa !27
  %76 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %77 = load i8, i8* %76, align 1, !tbaa !36
  %conv60 = zext i8 %77 to i32
  store i32 %conv60, i32* %g, align 4, !tbaa !27
  %78 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %79 = load i8, i8* %78, align 1, !tbaa !36
  %conv61 = zext i8 %79 to i32
  store i32 %conv61, i32* %b, align 4, !tbaa !27
  %80 = load i32, i32* %r, align 4, !tbaa !27
  %and62 = and i32 %80, 248
  %81 = load i32, i32* %g, align 4, !tbaa !27
  %shr63 = lshr i32 %81, 5
  %or64 = or i32 %and62, %shr63
  %82 = load i32, i32* %g, align 4, !tbaa !27
  %shl65 = shl i32 %82, 11
  %and66 = and i32 %shl65, 57344
  %or67 = or i32 %or64, %and66
  %83 = load i32, i32* %b, align 4, !tbaa !27
  %shl68 = shl i32 %83, 5
  %and69 = and i32 %shl68, 7936
  %or70 = or i32 %or67, %and69
  store i32 %or70, i32* %rgb, align 4, !tbaa !37
  %84 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv71 = trunc i32 %84 to i16
  %85 = load i8*, i8** %outptr, align 4, !tbaa !2
  %86 = bitcast i8* %85 to i16*
  store i16 %conv71, i16* %86, align 2, !tbaa !43
  br label %if.end72

if.end72:                                         ; preds = %if.then58, %for.end
  %87 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %91 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  %93 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  %95 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #4
  %96 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_rgb565_convert_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %rgb = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %7, i32* %num_cols, align 4, !tbaa !27
  br label %while.cond

while.cond:                                       ; preds = %if.end64, %entry
  %8 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %13, i32 0
  %14 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %15 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx1 = getelementptr inbounds i8*, i8** %14, i32 %15
  %16 = load i8*, i8** %arrayidx1, align 4, !tbaa !2
  store i8* %16, i8** %inptr0, align 4, !tbaa !2
  %17 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %17, i32 1
  %18 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %19 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %18, i32 %19
  %20 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %20, i8** %inptr1, align 4, !tbaa !2
  %21 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %21, i32 2
  %22 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %23 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %22, i32 %23
  %24 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %24, i8** %inptr2, align 4, !tbaa !2
  %25 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %25, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %26 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %26, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %27 = load i8*, i8** %26, align 4, !tbaa !2
  store i8* %27, i8** %outptr, align 4, !tbaa !2
  %28 = load i8*, i8** %outptr, align 4, !tbaa !2
  %29 = ptrtoint i8* %28 to i32
  %and = and i32 %29, 3
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %30 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr6 = getelementptr inbounds i8, i8* %30, i32 1
  store i8* %incdec.ptr6, i8** %inptr0, align 4, !tbaa !2
  %31 = load i8, i8* %30, align 1, !tbaa !36
  %conv = zext i8 %31 to i32
  store i32 %conv, i32* %r, align 4, !tbaa !27
  %32 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr7 = getelementptr inbounds i8, i8* %32, i32 1
  store i8* %incdec.ptr7, i8** %inptr1, align 4, !tbaa !2
  %33 = load i8, i8* %32, align 1, !tbaa !36
  %conv8 = zext i8 %33 to i32
  store i32 %conv8, i32* %g, align 4, !tbaa !27
  %34 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %34, i32 1
  store i8* %incdec.ptr9, i8** %inptr2, align 4, !tbaa !2
  %35 = load i8, i8* %34, align 1, !tbaa !36
  %conv10 = zext i8 %35 to i32
  store i32 %conv10, i32* %b, align 4, !tbaa !27
  %36 = load i32, i32* %r, align 4, !tbaa !27
  %shl = shl i32 %36, 8
  %and11 = and i32 %shl, 63488
  %37 = load i32, i32* %g, align 4, !tbaa !27
  %shl12 = shl i32 %37, 3
  %and13 = and i32 %shl12, 2016
  %or = or i32 %and11, %and13
  %38 = load i32, i32* %b, align 4, !tbaa !27
  %shr = lshr i32 %38, 3
  %or14 = or i32 %or, %shr
  store i32 %or14, i32* %rgb, align 4, !tbaa !37
  %39 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv15 = trunc i32 %39 to i16
  %40 = load i8*, i8** %outptr, align 4, !tbaa !2
  %41 = bitcast i8* %40 to i16*
  store i16 %conv15, i16* %41, align 2, !tbaa !43
  %42 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %42, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %43 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec16 = add i32 %43, -1
  store i32 %dec16, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %44 = load i32, i32* %col, align 4, !tbaa !27
  %45 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr17 = lshr i32 %45, 1
  %cmp18 = icmp ult i32 %44, %shr17
  br i1 %cmp18, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %46 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %46, i32 1
  store i8* %incdec.ptr20, i8** %inptr0, align 4, !tbaa !2
  %47 = load i8, i8* %46, align 1, !tbaa !36
  %conv21 = zext i8 %47 to i32
  store i32 %conv21, i32* %r, align 4, !tbaa !27
  %48 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr22 = getelementptr inbounds i8, i8* %48, i32 1
  store i8* %incdec.ptr22, i8** %inptr1, align 4, !tbaa !2
  %49 = load i8, i8* %48, align 1, !tbaa !36
  %conv23 = zext i8 %49 to i32
  store i32 %conv23, i32* %g, align 4, !tbaa !27
  %50 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr24 = getelementptr inbounds i8, i8* %50, i32 1
  store i8* %incdec.ptr24, i8** %inptr2, align 4, !tbaa !2
  %51 = load i8, i8* %50, align 1, !tbaa !36
  %conv25 = zext i8 %51 to i32
  store i32 %conv25, i32* %b, align 4, !tbaa !27
  %52 = load i32, i32* %r, align 4, !tbaa !27
  %shl26 = shl i32 %52, 8
  %and27 = and i32 %shl26, 63488
  %53 = load i32, i32* %g, align 4, !tbaa !27
  %shl28 = shl i32 %53, 3
  %and29 = and i32 %shl28, 2016
  %or30 = or i32 %and27, %and29
  %54 = load i32, i32* %b, align 4, !tbaa !27
  %shr31 = lshr i32 %54, 3
  %or32 = or i32 %or30, %shr31
  store i32 %or32, i32* %rgb, align 4, !tbaa !37
  %55 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr33 = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr33, i8** %inptr0, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !36
  %conv34 = zext i8 %56 to i32
  store i32 %conv34, i32* %r, align 4, !tbaa !27
  %57 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr35 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr35, i8** %inptr1, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !36
  %conv36 = zext i8 %58 to i32
  store i32 %conv36, i32* %g, align 4, !tbaa !27
  %59 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr37 = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr37, i8** %inptr2, align 4, !tbaa !2
  %60 = load i8, i8* %59, align 1, !tbaa !36
  %conv38 = zext i8 %60 to i32
  store i32 %conv38, i32* %b, align 4, !tbaa !27
  %61 = load i32, i32* %r, align 4, !tbaa !27
  %shl39 = shl i32 %61, 8
  %and40 = and i32 %shl39, 63488
  %62 = load i32, i32* %g, align 4, !tbaa !27
  %shl41 = shl i32 %62, 3
  %and42 = and i32 %shl41, 2016
  %or43 = or i32 %and40, %and42
  %63 = load i32, i32* %b, align 4, !tbaa !27
  %shr44 = lshr i32 %63, 3
  %or45 = or i32 %or43, %shr44
  %shl46 = shl i32 %or45, 16
  %64 = load i32, i32* %rgb, align 4, !tbaa !37
  %or47 = or i32 %shl46, %64
  store i32 %or47, i32* %rgb, align 4, !tbaa !37
  %65 = load i32, i32* %rgb, align 4, !tbaa !37
  %66 = load i8*, i8** %outptr, align 4, !tbaa !2
  %67 = bitcast i8* %66 to i32*
  store i32 %65, i32* %67, align 4, !tbaa !27
  %68 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr48 = getelementptr inbounds i8, i8* %68, i32 4
  store i8* %add.ptr48, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %69 = load i32, i32* %col, align 4, !tbaa !27
  %inc49 = add i32 %69, 1
  store i32 %inc49, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %70 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and50 = and i32 %70, 1
  %tobool51 = icmp ne i32 %and50, 0
  br i1 %tobool51, label %if.then52, label %if.end64

if.then52:                                        ; preds = %for.end
  %71 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !36
  %conv53 = zext i8 %72 to i32
  store i32 %conv53, i32* %r, align 4, !tbaa !27
  %73 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %74 = load i8, i8* %73, align 1, !tbaa !36
  %conv54 = zext i8 %74 to i32
  store i32 %conv54, i32* %g, align 4, !tbaa !27
  %75 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %76 = load i8, i8* %75, align 1, !tbaa !36
  %conv55 = zext i8 %76 to i32
  store i32 %conv55, i32* %b, align 4, !tbaa !27
  %77 = load i32, i32* %r, align 4, !tbaa !27
  %shl56 = shl i32 %77, 8
  %and57 = and i32 %shl56, 63488
  %78 = load i32, i32* %g, align 4, !tbaa !27
  %shl58 = shl i32 %78, 3
  %and59 = and i32 %shl58, 2016
  %or60 = or i32 %and57, %and59
  %79 = load i32, i32* %b, align 4, !tbaa !27
  %shr61 = lshr i32 %79, 3
  %or62 = or i32 %or60, %shr61
  store i32 %or62, i32* %rgb, align 4, !tbaa !37
  %80 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv63 = trunc i32 %80 to i16
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %82 = bitcast i8* %81 to i16*
  store i16 %conv63, i16* %82, align 2, !tbaa !43
  br label %if.end64

if.end64:                                         ; preds = %if.then52, %for.end
  %83 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %87 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_rgb565D_convert_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %d0 = alloca i32, align 4
  %rgb = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 34
  %32 = load i32, i32* %output_scanline, align 4, !tbaa !44
  %and = and i32 %32, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %33 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %33, i32* %d0, align 4, !tbaa !37
  br label %while.cond

while.cond:                                       ; preds = %if.end172, %entry
  %34 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %34, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %35 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  %38 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #4
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %39, i32 0
  %40 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %42, i8** %inptr0, align 4, !tbaa !2
  %43 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %43, i32 1
  %44 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %45 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %44, i32 %45
  %46 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %46, i8** %inptr1, align 4, !tbaa !2
  %47 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %47, i32 2
  %48 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %49 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8*, i8** %48, i32 %49
  %50 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %50, i8** %inptr2, align 4, !tbaa !2
  %51 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %51, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %52 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %52, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %53 = load i8*, i8** %52, align 4, !tbaa !2
  store i8* %53, i8** %outptr, align 4, !tbaa !2
  %54 = load i8*, i8** %outptr, align 4, !tbaa !2
  %55 = ptrtoint i8* %54 to i32
  %and8 = and i32 %55, 3
  %tobool = icmp ne i32 %and8, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %56 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %56, i32 1
  store i8* %incdec.ptr9, i8** %inptr0, align 4, !tbaa !2
  %57 = load i8, i8* %56, align 1, !tbaa !36
  %conv = zext i8 %57 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %58 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr10 = getelementptr inbounds i8, i8* %58, i32 1
  store i8* %incdec.ptr10, i8** %inptr1, align 4, !tbaa !2
  %59 = load i8, i8* %58, align 1, !tbaa !36
  %conv11 = zext i8 %59 to i32
  store i32 %conv11, i32* %cb, align 4, !tbaa !27
  %60 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %60, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %61 = load i8, i8* %60, align 1, !tbaa !36
  %conv13 = zext i8 %61 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !27
  %62 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %63 = load i32, i32* %y, align 4, !tbaa !27
  %64 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %65 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx14 = getelementptr inbounds i32, i32* %64, i32 %65
  %66 = load i32, i32* %arrayidx14, align 4, !tbaa !27
  %add = add nsw i32 %63, %66
  %67 = load i32, i32* %d0, align 4, !tbaa !37
  %and15 = and i32 %67, 255
  %add16 = add nsw i32 %add, %and15
  %arrayidx17 = getelementptr inbounds i8, i8* %62, i32 %add16
  %68 = load i8, i8* %arrayidx17, align 1, !tbaa !36
  %conv18 = zext i8 %68 to i32
  store i32 %conv18, i32* %r, align 4, !tbaa !27
  %69 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %70 = load i32, i32* %y, align 4, !tbaa !27
  %71 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %72 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx19 = getelementptr inbounds i32, i32* %71, i32 %72
  %73 = load i32, i32* %arrayidx19, align 4, !tbaa !37
  %74 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %75 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx20 = getelementptr inbounds i32, i32* %74, i32 %75
  %76 = load i32, i32* %arrayidx20, align 4, !tbaa !37
  %add21 = add nsw i32 %73, %76
  %shr = ashr i32 %add21, 16
  %add22 = add nsw i32 %70, %shr
  %77 = load i32, i32* %d0, align 4, !tbaa !37
  %and23 = and i32 %77, 255
  %shr24 = ashr i32 %and23, 1
  %add25 = add nsw i32 %add22, %shr24
  %arrayidx26 = getelementptr inbounds i8, i8* %69, i32 %add25
  %78 = load i8, i8* %arrayidx26, align 1, !tbaa !36
  %conv27 = zext i8 %78 to i32
  store i32 %conv27, i32* %g, align 4, !tbaa !27
  %79 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %80 = load i32, i32* %y, align 4, !tbaa !27
  %81 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %82 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx28 = getelementptr inbounds i32, i32* %81, i32 %82
  %83 = load i32, i32* %arrayidx28, align 4, !tbaa !27
  %add29 = add nsw i32 %80, %83
  %84 = load i32, i32* %d0, align 4, !tbaa !37
  %and30 = and i32 %84, 255
  %add31 = add nsw i32 %add29, %and30
  %arrayidx32 = getelementptr inbounds i8, i8* %79, i32 %add31
  %85 = load i8, i8* %arrayidx32, align 1, !tbaa !36
  %conv33 = zext i8 %85 to i32
  store i32 %conv33, i32* %b, align 4, !tbaa !27
  %86 = load i32, i32* %r, align 4, !tbaa !27
  %and34 = and i32 %86, 248
  %87 = load i32, i32* %g, align 4, !tbaa !27
  %shr35 = lshr i32 %87, 5
  %or = or i32 %and34, %shr35
  %88 = load i32, i32* %g, align 4, !tbaa !27
  %shl = shl i32 %88, 11
  %and36 = and i32 %shl, 57344
  %or37 = or i32 %or, %and36
  %89 = load i32, i32* %b, align 4, !tbaa !27
  %shl38 = shl i32 %89, 5
  %and39 = and i32 %shl38, 7936
  %or40 = or i32 %or37, %and39
  store i32 %or40, i32* %rgb, align 4, !tbaa !37
  %90 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv41 = trunc i32 %90 to i16
  %91 = load i8*, i8** %outptr, align 4, !tbaa !2
  %92 = bitcast i8* %91 to i16*
  store i16 %conv41, i16* %92, align 2, !tbaa !43
  %93 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %93, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %94 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec42 = add i32 %94, -1
  store i32 %dec42, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %95 = load i32, i32* %col, align 4, !tbaa !27
  %96 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr43 = lshr i32 %96, 1
  %cmp44 = icmp ult i32 %95, %shr43
  br i1 %cmp44, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %97 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr46 = getelementptr inbounds i8, i8* %97, i32 1
  store i8* %incdec.ptr46, i8** %inptr0, align 4, !tbaa !2
  %98 = load i8, i8* %97, align 1, !tbaa !36
  %conv47 = zext i8 %98 to i32
  store i32 %conv47, i32* %y, align 4, !tbaa !27
  %99 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr48 = getelementptr inbounds i8, i8* %99, i32 1
  store i8* %incdec.ptr48, i8** %inptr1, align 4, !tbaa !2
  %100 = load i8, i8* %99, align 1, !tbaa !36
  %conv49 = zext i8 %100 to i32
  store i32 %conv49, i32* %cb, align 4, !tbaa !27
  %101 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr50 = getelementptr inbounds i8, i8* %101, i32 1
  store i8* %incdec.ptr50, i8** %inptr2, align 4, !tbaa !2
  %102 = load i8, i8* %101, align 1, !tbaa !36
  %conv51 = zext i8 %102 to i32
  store i32 %conv51, i32* %cr, align 4, !tbaa !27
  %103 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %104 = load i32, i32* %y, align 4, !tbaa !27
  %105 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %106 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx52 = getelementptr inbounds i32, i32* %105, i32 %106
  %107 = load i32, i32* %arrayidx52, align 4, !tbaa !27
  %add53 = add nsw i32 %104, %107
  %108 = load i32, i32* %d0, align 4, !tbaa !37
  %and54 = and i32 %108, 255
  %add55 = add nsw i32 %add53, %and54
  %arrayidx56 = getelementptr inbounds i8, i8* %103, i32 %add55
  %109 = load i8, i8* %arrayidx56, align 1, !tbaa !36
  %conv57 = zext i8 %109 to i32
  store i32 %conv57, i32* %r, align 4, !tbaa !27
  %110 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %111 = load i32, i32* %y, align 4, !tbaa !27
  %112 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %113 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx58 = getelementptr inbounds i32, i32* %112, i32 %113
  %114 = load i32, i32* %arrayidx58, align 4, !tbaa !37
  %115 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %116 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx59 = getelementptr inbounds i32, i32* %115, i32 %116
  %117 = load i32, i32* %arrayidx59, align 4, !tbaa !37
  %add60 = add nsw i32 %114, %117
  %shr61 = ashr i32 %add60, 16
  %add62 = add nsw i32 %111, %shr61
  %118 = load i32, i32* %d0, align 4, !tbaa !37
  %and63 = and i32 %118, 255
  %shr64 = ashr i32 %and63, 1
  %add65 = add nsw i32 %add62, %shr64
  %arrayidx66 = getelementptr inbounds i8, i8* %110, i32 %add65
  %119 = load i8, i8* %arrayidx66, align 1, !tbaa !36
  %conv67 = zext i8 %119 to i32
  store i32 %conv67, i32* %g, align 4, !tbaa !27
  %120 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %121 = load i32, i32* %y, align 4, !tbaa !27
  %122 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %123 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx68 = getelementptr inbounds i32, i32* %122, i32 %123
  %124 = load i32, i32* %arrayidx68, align 4, !tbaa !27
  %add69 = add nsw i32 %121, %124
  %125 = load i32, i32* %d0, align 4, !tbaa !37
  %and70 = and i32 %125, 255
  %add71 = add nsw i32 %add69, %and70
  %arrayidx72 = getelementptr inbounds i8, i8* %120, i32 %add71
  %126 = load i8, i8* %arrayidx72, align 1, !tbaa !36
  %conv73 = zext i8 %126 to i32
  store i32 %conv73, i32* %b, align 4, !tbaa !27
  %127 = load i32, i32* %d0, align 4, !tbaa !37
  %and74 = and i32 %127, 255
  %shl75 = shl i32 %and74, 24
  %128 = load i32, i32* %d0, align 4, !tbaa !37
  %shr76 = ashr i32 %128, 8
  %and77 = and i32 %shr76, 16777215
  %or78 = or i32 %shl75, %and77
  store i32 %or78, i32* %d0, align 4, !tbaa !37
  %129 = load i32, i32* %r, align 4, !tbaa !27
  %and79 = and i32 %129, 248
  %130 = load i32, i32* %g, align 4, !tbaa !27
  %shr80 = lshr i32 %130, 5
  %or81 = or i32 %and79, %shr80
  %131 = load i32, i32* %g, align 4, !tbaa !27
  %shl82 = shl i32 %131, 11
  %and83 = and i32 %shl82, 57344
  %or84 = or i32 %or81, %and83
  %132 = load i32, i32* %b, align 4, !tbaa !27
  %shl85 = shl i32 %132, 5
  %and86 = and i32 %shl85, 7936
  %or87 = or i32 %or84, %and86
  store i32 %or87, i32* %rgb, align 4, !tbaa !37
  %133 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr88 = getelementptr inbounds i8, i8* %133, i32 1
  store i8* %incdec.ptr88, i8** %inptr0, align 4, !tbaa !2
  %134 = load i8, i8* %133, align 1, !tbaa !36
  %conv89 = zext i8 %134 to i32
  store i32 %conv89, i32* %y, align 4, !tbaa !27
  %135 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr90 = getelementptr inbounds i8, i8* %135, i32 1
  store i8* %incdec.ptr90, i8** %inptr1, align 4, !tbaa !2
  %136 = load i8, i8* %135, align 1, !tbaa !36
  %conv91 = zext i8 %136 to i32
  store i32 %conv91, i32* %cb, align 4, !tbaa !27
  %137 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr92 = getelementptr inbounds i8, i8* %137, i32 1
  store i8* %incdec.ptr92, i8** %inptr2, align 4, !tbaa !2
  %138 = load i8, i8* %137, align 1, !tbaa !36
  %conv93 = zext i8 %138 to i32
  store i32 %conv93, i32* %cr, align 4, !tbaa !27
  %139 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %140 = load i32, i32* %y, align 4, !tbaa !27
  %141 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %142 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx94 = getelementptr inbounds i32, i32* %141, i32 %142
  %143 = load i32, i32* %arrayidx94, align 4, !tbaa !27
  %add95 = add nsw i32 %140, %143
  %144 = load i32, i32* %d0, align 4, !tbaa !37
  %and96 = and i32 %144, 255
  %add97 = add nsw i32 %add95, %and96
  %arrayidx98 = getelementptr inbounds i8, i8* %139, i32 %add97
  %145 = load i8, i8* %arrayidx98, align 1, !tbaa !36
  %conv99 = zext i8 %145 to i32
  store i32 %conv99, i32* %r, align 4, !tbaa !27
  %146 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %147 = load i32, i32* %y, align 4, !tbaa !27
  %148 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %149 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx100 = getelementptr inbounds i32, i32* %148, i32 %149
  %150 = load i32, i32* %arrayidx100, align 4, !tbaa !37
  %151 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %152 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx101 = getelementptr inbounds i32, i32* %151, i32 %152
  %153 = load i32, i32* %arrayidx101, align 4, !tbaa !37
  %add102 = add nsw i32 %150, %153
  %shr103 = ashr i32 %add102, 16
  %add104 = add nsw i32 %147, %shr103
  %154 = load i32, i32* %d0, align 4, !tbaa !37
  %and105 = and i32 %154, 255
  %shr106 = ashr i32 %and105, 1
  %add107 = add nsw i32 %add104, %shr106
  %arrayidx108 = getelementptr inbounds i8, i8* %146, i32 %add107
  %155 = load i8, i8* %arrayidx108, align 1, !tbaa !36
  %conv109 = zext i8 %155 to i32
  store i32 %conv109, i32* %g, align 4, !tbaa !27
  %156 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %157 = load i32, i32* %y, align 4, !tbaa !27
  %158 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %159 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx110 = getelementptr inbounds i32, i32* %158, i32 %159
  %160 = load i32, i32* %arrayidx110, align 4, !tbaa !27
  %add111 = add nsw i32 %157, %160
  %161 = load i32, i32* %d0, align 4, !tbaa !37
  %and112 = and i32 %161, 255
  %add113 = add nsw i32 %add111, %and112
  %arrayidx114 = getelementptr inbounds i8, i8* %156, i32 %add113
  %162 = load i8, i8* %arrayidx114, align 1, !tbaa !36
  %conv115 = zext i8 %162 to i32
  store i32 %conv115, i32* %b, align 4, !tbaa !27
  %163 = load i32, i32* %d0, align 4, !tbaa !37
  %and116 = and i32 %163, 255
  %shl117 = shl i32 %and116, 24
  %164 = load i32, i32* %d0, align 4, !tbaa !37
  %shr118 = ashr i32 %164, 8
  %and119 = and i32 %shr118, 16777215
  %or120 = or i32 %shl117, %and119
  store i32 %or120, i32* %d0, align 4, !tbaa !37
  %165 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl121 = shl i32 %165, 16
  %166 = load i32, i32* %r, align 4, !tbaa !27
  %and122 = and i32 %166, 248
  %167 = load i32, i32* %g, align 4, !tbaa !27
  %shr123 = lshr i32 %167, 5
  %or124 = or i32 %and122, %shr123
  %168 = load i32, i32* %g, align 4, !tbaa !27
  %shl125 = shl i32 %168, 11
  %and126 = and i32 %shl125, 57344
  %or127 = or i32 %or124, %and126
  %169 = load i32, i32* %b, align 4, !tbaa !27
  %shl128 = shl i32 %169, 5
  %and129 = and i32 %shl128, 7936
  %or130 = or i32 %or127, %and129
  %or131 = or i32 %shl121, %or130
  store i32 %or131, i32* %rgb, align 4, !tbaa !37
  %170 = load i32, i32* %rgb, align 4, !tbaa !37
  %171 = load i8*, i8** %outptr, align 4, !tbaa !2
  %172 = bitcast i8* %171 to i32*
  store i32 %170, i32* %172, align 4, !tbaa !27
  %173 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr132 = getelementptr inbounds i8, i8* %173, i32 4
  store i8* %add.ptr132, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %174 = load i32, i32* %col, align 4, !tbaa !27
  %inc133 = add i32 %174, 1
  store i32 %inc133, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %175 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and134 = and i32 %175, 1
  %tobool135 = icmp ne i32 %and134, 0
  br i1 %tobool135, label %if.then136, label %if.end172

if.then136:                                       ; preds = %for.end
  %176 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %177 = load i8, i8* %176, align 1, !tbaa !36
  %conv137 = zext i8 %177 to i32
  store i32 %conv137, i32* %y, align 4, !tbaa !27
  %178 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %179 = load i8, i8* %178, align 1, !tbaa !36
  %conv138 = zext i8 %179 to i32
  store i32 %conv138, i32* %cb, align 4, !tbaa !27
  %180 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %181 = load i8, i8* %180, align 1, !tbaa !36
  %conv139 = zext i8 %181 to i32
  store i32 %conv139, i32* %cr, align 4, !tbaa !27
  %182 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %183 = load i32, i32* %y, align 4, !tbaa !27
  %184 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %185 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx140 = getelementptr inbounds i32, i32* %184, i32 %185
  %186 = load i32, i32* %arrayidx140, align 4, !tbaa !27
  %add141 = add nsw i32 %183, %186
  %187 = load i32, i32* %d0, align 4, !tbaa !37
  %and142 = and i32 %187, 255
  %add143 = add nsw i32 %add141, %and142
  %arrayidx144 = getelementptr inbounds i8, i8* %182, i32 %add143
  %188 = load i8, i8* %arrayidx144, align 1, !tbaa !36
  %conv145 = zext i8 %188 to i32
  store i32 %conv145, i32* %r, align 4, !tbaa !27
  %189 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %190 = load i32, i32* %y, align 4, !tbaa !27
  %191 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %192 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx146 = getelementptr inbounds i32, i32* %191, i32 %192
  %193 = load i32, i32* %arrayidx146, align 4, !tbaa !37
  %194 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %195 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx147 = getelementptr inbounds i32, i32* %194, i32 %195
  %196 = load i32, i32* %arrayidx147, align 4, !tbaa !37
  %add148 = add nsw i32 %193, %196
  %shr149 = ashr i32 %add148, 16
  %add150 = add nsw i32 %190, %shr149
  %197 = load i32, i32* %d0, align 4, !tbaa !37
  %and151 = and i32 %197, 255
  %shr152 = ashr i32 %and151, 1
  %add153 = add nsw i32 %add150, %shr152
  %arrayidx154 = getelementptr inbounds i8, i8* %189, i32 %add153
  %198 = load i8, i8* %arrayidx154, align 1, !tbaa !36
  %conv155 = zext i8 %198 to i32
  store i32 %conv155, i32* %g, align 4, !tbaa !27
  %199 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %200 = load i32, i32* %y, align 4, !tbaa !27
  %201 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %202 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx156 = getelementptr inbounds i32, i32* %201, i32 %202
  %203 = load i32, i32* %arrayidx156, align 4, !tbaa !27
  %add157 = add nsw i32 %200, %203
  %204 = load i32, i32* %d0, align 4, !tbaa !37
  %and158 = and i32 %204, 255
  %add159 = add nsw i32 %add157, %and158
  %arrayidx160 = getelementptr inbounds i8, i8* %199, i32 %add159
  %205 = load i8, i8* %arrayidx160, align 1, !tbaa !36
  %conv161 = zext i8 %205 to i32
  store i32 %conv161, i32* %b, align 4, !tbaa !27
  %206 = load i32, i32* %r, align 4, !tbaa !27
  %and162 = and i32 %206, 248
  %207 = load i32, i32* %g, align 4, !tbaa !27
  %shr163 = lshr i32 %207, 5
  %or164 = or i32 %and162, %shr163
  %208 = load i32, i32* %g, align 4, !tbaa !27
  %shl165 = shl i32 %208, 11
  %and166 = and i32 %shl165, 57344
  %or167 = or i32 %or164, %and166
  %209 = load i32, i32* %b, align 4, !tbaa !27
  %shl168 = shl i32 %209, 5
  %and169 = and i32 %shl168, 7936
  %or170 = or i32 %or167, %and169
  store i32 %or170, i32* %rgb, align 4, !tbaa !37
  %210 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv171 = trunc i32 %210 to i16
  %211 = load i8*, i8** %outptr, align 4, !tbaa !2
  %212 = bitcast i8* %211 to i16*
  store i16 %conv171, i16* %212, align 2, !tbaa !43
  br label %if.end172

if.end172:                                        ; preds = %if.then136, %for.end
  %213 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  %215 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #4
  %216 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %217 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #4
  %218 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  %219 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #4
  %220 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  %221 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #4
  %222 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #4
  %223 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #4
  %224 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #4
  %225 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #4
  %226 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #4
  %227 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #4
  %228 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #4
  %229 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #4
  %230 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #4
  %231 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #4
  %232 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @ycc_rgb565D_convert_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cconvert = alloca %struct.my_color_deconverter*, align 4
  %y = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %num_cols = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %d0 = alloca i32, align 4
  %rgb = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 86
  %2 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_color_deconverter* %2 to %struct.my_color_deconverter*
  store %struct.my_color_deconverter* %3, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %14, i32* %num_cols, align 4, !tbaa !27
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %19, i32 0, i32 1
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !38
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %22, i32 0, i32 2
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !39
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %25, i32 0, i32 3
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !40
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_color_deconverter*, %struct.my_color_deconverter** %cconvert, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_color_deconverter, %struct.my_color_deconverter* %28, i32 0, i32 4
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !41
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 34
  %32 = load i32, i32* %output_scanline, align 4, !tbaa !44
  %and = and i32 %32, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %33 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %33, i32* %d0, align 4, !tbaa !37
  br label %while.cond

while.cond:                                       ; preds = %if.end164, %entry
  %34 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %34, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %35 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  %38 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #4
  %39 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %39, i32 0
  %40 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %41 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx3 = getelementptr inbounds i8*, i8** %40, i32 %41
  %42 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %42, i8** %inptr0, align 4, !tbaa !2
  %43 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %43, i32 1
  %44 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %45 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx5 = getelementptr inbounds i8*, i8** %44, i32 %45
  %46 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %46, i8** %inptr1, align 4, !tbaa !2
  %47 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %47, i32 2
  %48 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %49 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx7 = getelementptr inbounds i8*, i8** %48, i32 %49
  %50 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %50, i8** %inptr2, align 4, !tbaa !2
  %51 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %51, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %52 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %52, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %53 = load i8*, i8** %52, align 4, !tbaa !2
  store i8* %53, i8** %outptr, align 4, !tbaa !2
  %54 = load i8*, i8** %outptr, align 4, !tbaa !2
  %55 = ptrtoint i8* %54 to i32
  %and8 = and i32 %55, 3
  %tobool = icmp ne i32 %and8, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %56 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %56, i32 1
  store i8* %incdec.ptr9, i8** %inptr0, align 4, !tbaa !2
  %57 = load i8, i8* %56, align 1, !tbaa !36
  %conv = zext i8 %57 to i32
  store i32 %conv, i32* %y, align 4, !tbaa !27
  %58 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr10 = getelementptr inbounds i8, i8* %58, i32 1
  store i8* %incdec.ptr10, i8** %inptr1, align 4, !tbaa !2
  %59 = load i8, i8* %58, align 1, !tbaa !36
  %conv11 = zext i8 %59 to i32
  store i32 %conv11, i32* %cb, align 4, !tbaa !27
  %60 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %60, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %61 = load i8, i8* %60, align 1, !tbaa !36
  %conv13 = zext i8 %61 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !27
  %62 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %63 = load i32, i32* %y, align 4, !tbaa !27
  %64 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %65 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx14 = getelementptr inbounds i32, i32* %64, i32 %65
  %66 = load i32, i32* %arrayidx14, align 4, !tbaa !27
  %add = add nsw i32 %63, %66
  %67 = load i32, i32* %d0, align 4, !tbaa !37
  %and15 = and i32 %67, 255
  %add16 = add nsw i32 %add, %and15
  %arrayidx17 = getelementptr inbounds i8, i8* %62, i32 %add16
  %68 = load i8, i8* %arrayidx17, align 1, !tbaa !36
  %conv18 = zext i8 %68 to i32
  store i32 %conv18, i32* %r, align 4, !tbaa !27
  %69 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %70 = load i32, i32* %y, align 4, !tbaa !27
  %71 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %72 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx19 = getelementptr inbounds i32, i32* %71, i32 %72
  %73 = load i32, i32* %arrayidx19, align 4, !tbaa !37
  %74 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %75 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx20 = getelementptr inbounds i32, i32* %74, i32 %75
  %76 = load i32, i32* %arrayidx20, align 4, !tbaa !37
  %add21 = add nsw i32 %73, %76
  %shr = ashr i32 %add21, 16
  %add22 = add nsw i32 %70, %shr
  %77 = load i32, i32* %d0, align 4, !tbaa !37
  %and23 = and i32 %77, 255
  %shr24 = ashr i32 %and23, 1
  %add25 = add nsw i32 %add22, %shr24
  %arrayidx26 = getelementptr inbounds i8, i8* %69, i32 %add25
  %78 = load i8, i8* %arrayidx26, align 1, !tbaa !36
  %conv27 = zext i8 %78 to i32
  store i32 %conv27, i32* %g, align 4, !tbaa !27
  %79 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %80 = load i32, i32* %y, align 4, !tbaa !27
  %81 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %82 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx28 = getelementptr inbounds i32, i32* %81, i32 %82
  %83 = load i32, i32* %arrayidx28, align 4, !tbaa !27
  %add29 = add nsw i32 %80, %83
  %84 = load i32, i32* %d0, align 4, !tbaa !37
  %and30 = and i32 %84, 255
  %add31 = add nsw i32 %add29, %and30
  %arrayidx32 = getelementptr inbounds i8, i8* %79, i32 %add31
  %85 = load i8, i8* %arrayidx32, align 1, !tbaa !36
  %conv33 = zext i8 %85 to i32
  store i32 %conv33, i32* %b, align 4, !tbaa !27
  %86 = load i32, i32* %r, align 4, !tbaa !27
  %shl = shl i32 %86, 8
  %and34 = and i32 %shl, 63488
  %87 = load i32, i32* %g, align 4, !tbaa !27
  %shl35 = shl i32 %87, 3
  %and36 = and i32 %shl35, 2016
  %or = or i32 %and34, %and36
  %88 = load i32, i32* %b, align 4, !tbaa !27
  %shr37 = lshr i32 %88, 3
  %or38 = or i32 %or, %shr37
  store i32 %or38, i32* %rgb, align 4, !tbaa !37
  %89 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv39 = trunc i32 %89 to i16
  %90 = load i8*, i8** %outptr, align 4, !tbaa !2
  %91 = bitcast i8* %90 to i16*
  store i16 %conv39, i16* %91, align 2, !tbaa !43
  %92 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %92, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %93 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec40 = add i32 %93, -1
  store i32 %dec40, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %94 = load i32, i32* %col, align 4, !tbaa !27
  %95 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr41 = lshr i32 %95, 1
  %cmp42 = icmp ult i32 %94, %shr41
  br i1 %cmp42, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %96 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr44 = getelementptr inbounds i8, i8* %96, i32 1
  store i8* %incdec.ptr44, i8** %inptr0, align 4, !tbaa !2
  %97 = load i8, i8* %96, align 1, !tbaa !36
  %conv45 = zext i8 %97 to i32
  store i32 %conv45, i32* %y, align 4, !tbaa !27
  %98 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr46 = getelementptr inbounds i8, i8* %98, i32 1
  store i8* %incdec.ptr46, i8** %inptr1, align 4, !tbaa !2
  %99 = load i8, i8* %98, align 1, !tbaa !36
  %conv47 = zext i8 %99 to i32
  store i32 %conv47, i32* %cb, align 4, !tbaa !27
  %100 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr48 = getelementptr inbounds i8, i8* %100, i32 1
  store i8* %incdec.ptr48, i8** %inptr2, align 4, !tbaa !2
  %101 = load i8, i8* %100, align 1, !tbaa !36
  %conv49 = zext i8 %101 to i32
  store i32 %conv49, i32* %cr, align 4, !tbaa !27
  %102 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %103 = load i32, i32* %y, align 4, !tbaa !27
  %104 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %105 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx50 = getelementptr inbounds i32, i32* %104, i32 %105
  %106 = load i32, i32* %arrayidx50, align 4, !tbaa !27
  %add51 = add nsw i32 %103, %106
  %107 = load i32, i32* %d0, align 4, !tbaa !37
  %and52 = and i32 %107, 255
  %add53 = add nsw i32 %add51, %and52
  %arrayidx54 = getelementptr inbounds i8, i8* %102, i32 %add53
  %108 = load i8, i8* %arrayidx54, align 1, !tbaa !36
  %conv55 = zext i8 %108 to i32
  store i32 %conv55, i32* %r, align 4, !tbaa !27
  %109 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %110 = load i32, i32* %y, align 4, !tbaa !27
  %111 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %112 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx56 = getelementptr inbounds i32, i32* %111, i32 %112
  %113 = load i32, i32* %arrayidx56, align 4, !tbaa !37
  %114 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %115 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx57 = getelementptr inbounds i32, i32* %114, i32 %115
  %116 = load i32, i32* %arrayidx57, align 4, !tbaa !37
  %add58 = add nsw i32 %113, %116
  %shr59 = ashr i32 %add58, 16
  %add60 = add nsw i32 %110, %shr59
  %117 = load i32, i32* %d0, align 4, !tbaa !37
  %and61 = and i32 %117, 255
  %shr62 = ashr i32 %and61, 1
  %add63 = add nsw i32 %add60, %shr62
  %arrayidx64 = getelementptr inbounds i8, i8* %109, i32 %add63
  %118 = load i8, i8* %arrayidx64, align 1, !tbaa !36
  %conv65 = zext i8 %118 to i32
  store i32 %conv65, i32* %g, align 4, !tbaa !27
  %119 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %120 = load i32, i32* %y, align 4, !tbaa !27
  %121 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %122 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx66 = getelementptr inbounds i32, i32* %121, i32 %122
  %123 = load i32, i32* %arrayidx66, align 4, !tbaa !27
  %add67 = add nsw i32 %120, %123
  %124 = load i32, i32* %d0, align 4, !tbaa !37
  %and68 = and i32 %124, 255
  %add69 = add nsw i32 %add67, %and68
  %arrayidx70 = getelementptr inbounds i8, i8* %119, i32 %add69
  %125 = load i8, i8* %arrayidx70, align 1, !tbaa !36
  %conv71 = zext i8 %125 to i32
  store i32 %conv71, i32* %b, align 4, !tbaa !27
  %126 = load i32, i32* %d0, align 4, !tbaa !37
  %and72 = and i32 %126, 255
  %shl73 = shl i32 %and72, 24
  %127 = load i32, i32* %d0, align 4, !tbaa !37
  %shr74 = ashr i32 %127, 8
  %and75 = and i32 %shr74, 16777215
  %or76 = or i32 %shl73, %and75
  store i32 %or76, i32* %d0, align 4, !tbaa !37
  %128 = load i32, i32* %r, align 4, !tbaa !27
  %shl77 = shl i32 %128, 8
  %and78 = and i32 %shl77, 63488
  %129 = load i32, i32* %g, align 4, !tbaa !27
  %shl79 = shl i32 %129, 3
  %and80 = and i32 %shl79, 2016
  %or81 = or i32 %and78, %and80
  %130 = load i32, i32* %b, align 4, !tbaa !27
  %shr82 = lshr i32 %130, 3
  %or83 = or i32 %or81, %shr82
  store i32 %or83, i32* %rgb, align 4, !tbaa !37
  %131 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr84 = getelementptr inbounds i8, i8* %131, i32 1
  store i8* %incdec.ptr84, i8** %inptr0, align 4, !tbaa !2
  %132 = load i8, i8* %131, align 1, !tbaa !36
  %conv85 = zext i8 %132 to i32
  store i32 %conv85, i32* %y, align 4, !tbaa !27
  %133 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr86 = getelementptr inbounds i8, i8* %133, i32 1
  store i8* %incdec.ptr86, i8** %inptr1, align 4, !tbaa !2
  %134 = load i8, i8* %133, align 1, !tbaa !36
  %conv87 = zext i8 %134 to i32
  store i32 %conv87, i32* %cb, align 4, !tbaa !27
  %135 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr88 = getelementptr inbounds i8, i8* %135, i32 1
  store i8* %incdec.ptr88, i8** %inptr2, align 4, !tbaa !2
  %136 = load i8, i8* %135, align 1, !tbaa !36
  %conv89 = zext i8 %136 to i32
  store i32 %conv89, i32* %cr, align 4, !tbaa !27
  %137 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %138 = load i32, i32* %y, align 4, !tbaa !27
  %139 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %140 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx90 = getelementptr inbounds i32, i32* %139, i32 %140
  %141 = load i32, i32* %arrayidx90, align 4, !tbaa !27
  %add91 = add nsw i32 %138, %141
  %142 = load i32, i32* %d0, align 4, !tbaa !37
  %and92 = and i32 %142, 255
  %add93 = add nsw i32 %add91, %and92
  %arrayidx94 = getelementptr inbounds i8, i8* %137, i32 %add93
  %143 = load i8, i8* %arrayidx94, align 1, !tbaa !36
  %conv95 = zext i8 %143 to i32
  store i32 %conv95, i32* %r, align 4, !tbaa !27
  %144 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %145 = load i32, i32* %y, align 4, !tbaa !27
  %146 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %147 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx96 = getelementptr inbounds i32, i32* %146, i32 %147
  %148 = load i32, i32* %arrayidx96, align 4, !tbaa !37
  %149 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %150 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx97 = getelementptr inbounds i32, i32* %149, i32 %150
  %151 = load i32, i32* %arrayidx97, align 4, !tbaa !37
  %add98 = add nsw i32 %148, %151
  %shr99 = ashr i32 %add98, 16
  %add100 = add nsw i32 %145, %shr99
  %152 = load i32, i32* %d0, align 4, !tbaa !37
  %and101 = and i32 %152, 255
  %shr102 = ashr i32 %and101, 1
  %add103 = add nsw i32 %add100, %shr102
  %arrayidx104 = getelementptr inbounds i8, i8* %144, i32 %add103
  %153 = load i8, i8* %arrayidx104, align 1, !tbaa !36
  %conv105 = zext i8 %153 to i32
  store i32 %conv105, i32* %g, align 4, !tbaa !27
  %154 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %155 = load i32, i32* %y, align 4, !tbaa !27
  %156 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %157 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx106 = getelementptr inbounds i32, i32* %156, i32 %157
  %158 = load i32, i32* %arrayidx106, align 4, !tbaa !27
  %add107 = add nsw i32 %155, %158
  %159 = load i32, i32* %d0, align 4, !tbaa !37
  %and108 = and i32 %159, 255
  %add109 = add nsw i32 %add107, %and108
  %arrayidx110 = getelementptr inbounds i8, i8* %154, i32 %add109
  %160 = load i8, i8* %arrayidx110, align 1, !tbaa !36
  %conv111 = zext i8 %160 to i32
  store i32 %conv111, i32* %b, align 4, !tbaa !27
  %161 = load i32, i32* %d0, align 4, !tbaa !37
  %and112 = and i32 %161, 255
  %shl113 = shl i32 %and112, 24
  %162 = load i32, i32* %d0, align 4, !tbaa !37
  %shr114 = ashr i32 %162, 8
  %and115 = and i32 %shr114, 16777215
  %or116 = or i32 %shl113, %and115
  store i32 %or116, i32* %d0, align 4, !tbaa !37
  %163 = load i32, i32* %r, align 4, !tbaa !27
  %shl117 = shl i32 %163, 8
  %and118 = and i32 %shl117, 63488
  %164 = load i32, i32* %g, align 4, !tbaa !27
  %shl119 = shl i32 %164, 3
  %and120 = and i32 %shl119, 2016
  %or121 = or i32 %and118, %and120
  %165 = load i32, i32* %b, align 4, !tbaa !27
  %shr122 = lshr i32 %165, 3
  %or123 = or i32 %or121, %shr122
  %shl124 = shl i32 %or123, 16
  %166 = load i32, i32* %rgb, align 4, !tbaa !37
  %or125 = or i32 %shl124, %166
  store i32 %or125, i32* %rgb, align 4, !tbaa !37
  %167 = load i32, i32* %rgb, align 4, !tbaa !37
  %168 = load i8*, i8** %outptr, align 4, !tbaa !2
  %169 = bitcast i8* %168 to i32*
  store i32 %167, i32* %169, align 4, !tbaa !27
  %170 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr126 = getelementptr inbounds i8, i8* %170, i32 4
  store i8* %add.ptr126, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %171 = load i32, i32* %col, align 4, !tbaa !27
  %inc127 = add i32 %171, 1
  store i32 %inc127, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %172 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and128 = and i32 %172, 1
  %tobool129 = icmp ne i32 %and128, 0
  br i1 %tobool129, label %if.then130, label %if.end164

if.then130:                                       ; preds = %for.end
  %173 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %174 = load i8, i8* %173, align 1, !tbaa !36
  %conv131 = zext i8 %174 to i32
  store i32 %conv131, i32* %y, align 4, !tbaa !27
  %175 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %176 = load i8, i8* %175, align 1, !tbaa !36
  %conv132 = zext i8 %176 to i32
  store i32 %conv132, i32* %cb, align 4, !tbaa !27
  %177 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %178 = load i8, i8* %177, align 1, !tbaa !36
  %conv133 = zext i8 %178 to i32
  store i32 %conv133, i32* %cr, align 4, !tbaa !27
  %179 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %180 = load i32, i32* %y, align 4, !tbaa !27
  %181 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %182 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx134 = getelementptr inbounds i32, i32* %181, i32 %182
  %183 = load i32, i32* %arrayidx134, align 4, !tbaa !27
  %add135 = add nsw i32 %180, %183
  %184 = load i32, i32* %d0, align 4, !tbaa !37
  %and136 = and i32 %184, 255
  %add137 = add nsw i32 %add135, %and136
  %arrayidx138 = getelementptr inbounds i8, i8* %179, i32 %add137
  %185 = load i8, i8* %arrayidx138, align 1, !tbaa !36
  %conv139 = zext i8 %185 to i32
  store i32 %conv139, i32* %r, align 4, !tbaa !27
  %186 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %187 = load i32, i32* %y, align 4, !tbaa !27
  %188 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %189 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx140 = getelementptr inbounds i32, i32* %188, i32 %189
  %190 = load i32, i32* %arrayidx140, align 4, !tbaa !37
  %191 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %192 = load i32, i32* %cr, align 4, !tbaa !27
  %arrayidx141 = getelementptr inbounds i32, i32* %191, i32 %192
  %193 = load i32, i32* %arrayidx141, align 4, !tbaa !37
  %add142 = add nsw i32 %190, %193
  %shr143 = ashr i32 %add142, 16
  %add144 = add nsw i32 %187, %shr143
  %194 = load i32, i32* %d0, align 4, !tbaa !37
  %and145 = and i32 %194, 255
  %shr146 = ashr i32 %and145, 1
  %add147 = add nsw i32 %add144, %shr146
  %arrayidx148 = getelementptr inbounds i8, i8* %186, i32 %add147
  %195 = load i8, i8* %arrayidx148, align 1, !tbaa !36
  %conv149 = zext i8 %195 to i32
  store i32 %conv149, i32* %g, align 4, !tbaa !27
  %196 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %197 = load i32, i32* %y, align 4, !tbaa !27
  %198 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %199 = load i32, i32* %cb, align 4, !tbaa !27
  %arrayidx150 = getelementptr inbounds i32, i32* %198, i32 %199
  %200 = load i32, i32* %arrayidx150, align 4, !tbaa !27
  %add151 = add nsw i32 %197, %200
  %201 = load i32, i32* %d0, align 4, !tbaa !37
  %and152 = and i32 %201, 255
  %add153 = add nsw i32 %add151, %and152
  %arrayidx154 = getelementptr inbounds i8, i8* %196, i32 %add153
  %202 = load i8, i8* %arrayidx154, align 1, !tbaa !36
  %conv155 = zext i8 %202 to i32
  store i32 %conv155, i32* %b, align 4, !tbaa !27
  %203 = load i32, i32* %r, align 4, !tbaa !27
  %shl156 = shl i32 %203, 8
  %and157 = and i32 %shl156, 63488
  %204 = load i32, i32* %g, align 4, !tbaa !27
  %shl158 = shl i32 %204, 3
  %and159 = and i32 %shl158, 2016
  %or160 = or i32 %and157, %and159
  %205 = load i32, i32* %b, align 4, !tbaa !27
  %shr161 = lshr i32 %205, 3
  %or162 = or i32 %or160, %shr161
  store i32 %or162, i32* %rgb, align 4, !tbaa !37
  %206 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv163 = trunc i32 %206 to i16
  %207 = load i8*, i8** %outptr, align 4, !tbaa !2
  %208 = bitcast i8* %207 to i16*
  store i16 %conv163, i16* %208, align 2, !tbaa !43
  br label %if.end164

if.end164:                                        ; preds = %if.then130, %for.end
  %209 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %213 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  %215 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #4
  %216 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #4
  %217 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #4
  %218 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  %219 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #4
  %220 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  %221 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #4
  %222 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #4
  %223 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #4
  %224 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #4
  %225 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #4
  %226 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #4
  %227 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #4
  %228 = bitcast %struct.my_color_deconverter** %cconvert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_rgb565D_convert_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %num_cols = alloca i32, align 4
  %d0 = alloca i32, align 4
  %rgb = alloca i32, align 4
  %g = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 65
  %5 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %5, i8** %range_limit, align 4, !tbaa !2
  %6 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 27
  %8 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %8, i32* %num_cols, align 4, !tbaa !27
  %9 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 34
  %11 = load i32, i32* %output_scanline, align 4, !tbaa !44
  %and = and i32 %11, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %12 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %12, i32* %d0, align 4, !tbaa !37
  br label %while.cond

while.cond:                                       ; preds = %if.end81, %entry
  %13 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %13, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8**, i8*** %16, i32 0
  %17 = load i8**, i8*** %arrayidx1, align 4, !tbaa !2
  %18 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %18, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %17, i32 %18
  %19 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %19, i8** %inptr, align 4, !tbaa !2
  %20 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %20, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %21 = load i8*, i8** %20, align 4, !tbaa !2
  store i8* %21, i8** %outptr, align 4, !tbaa !2
  %22 = load i8*, i8** %outptr, align 4, !tbaa !2
  %23 = ptrtoint i8* %22 to i32
  %and3 = and i32 %23, 3
  %tobool = icmp ne i32 %and3, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %24 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr4 = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr4, i8** %inptr, align 4, !tbaa !2
  %25 = load i8, i8* %24, align 1, !tbaa !36
  %conv = zext i8 %25 to i32
  store i32 %conv, i32* %g, align 4, !tbaa !27
  %26 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %27 = load i32, i32* %g, align 4, !tbaa !27
  %28 = load i32, i32* %d0, align 4, !tbaa !37
  %and5 = and i32 %28, 255
  %add = add i32 %27, %and5
  %arrayidx6 = getelementptr inbounds i8, i8* %26, i32 %add
  %29 = load i8, i8* %arrayidx6, align 1, !tbaa !36
  %conv7 = zext i8 %29 to i32
  store i32 %conv7, i32* %g, align 4, !tbaa !27
  %30 = load i32, i32* %g, align 4, !tbaa !27
  %and8 = and i32 %30, 248
  %31 = load i32, i32* %g, align 4, !tbaa !27
  %shr = lshr i32 %31, 5
  %or = or i32 %and8, %shr
  %32 = load i32, i32* %g, align 4, !tbaa !27
  %shl = shl i32 %32, 11
  %and9 = and i32 %shl, 57344
  %or10 = or i32 %or, %and9
  %33 = load i32, i32* %g, align 4, !tbaa !27
  %shl11 = shl i32 %33, 5
  %and12 = and i32 %shl11, 7936
  %or13 = or i32 %or10, %and12
  store i32 %or13, i32* %rgb, align 4, !tbaa !37
  %34 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv14 = trunc i32 %34 to i16
  %35 = load i8*, i8** %outptr, align 4, !tbaa !2
  %36 = bitcast i8* %35 to i16*
  store i16 %conv14, i16* %36, align 2, !tbaa !43
  %37 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %37, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %38 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec15 = add i32 %38, -1
  store i32 %dec15, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %39 = load i32, i32* %col, align 4, !tbaa !27
  %40 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr16 = lshr i32 %40, 1
  %cmp17 = icmp ult i32 %39, %shr16
  br i1 %cmp17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %41 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr19 = getelementptr inbounds i8, i8* %41, i32 1
  store i8* %incdec.ptr19, i8** %inptr, align 4, !tbaa !2
  %42 = load i8, i8* %41, align 1, !tbaa !36
  %conv20 = zext i8 %42 to i32
  store i32 %conv20, i32* %g, align 4, !tbaa !27
  %43 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %44 = load i32, i32* %g, align 4, !tbaa !27
  %45 = load i32, i32* %d0, align 4, !tbaa !37
  %and21 = and i32 %45, 255
  %add22 = add i32 %44, %and21
  %arrayidx23 = getelementptr inbounds i8, i8* %43, i32 %add22
  %46 = load i8, i8* %arrayidx23, align 1, !tbaa !36
  %conv24 = zext i8 %46 to i32
  store i32 %conv24, i32* %g, align 4, !tbaa !27
  %47 = load i32, i32* %g, align 4, !tbaa !27
  %and25 = and i32 %47, 248
  %48 = load i32, i32* %g, align 4, !tbaa !27
  %shr26 = lshr i32 %48, 5
  %or27 = or i32 %and25, %shr26
  %49 = load i32, i32* %g, align 4, !tbaa !27
  %shl28 = shl i32 %49, 11
  %and29 = and i32 %shl28, 57344
  %or30 = or i32 %or27, %and29
  %50 = load i32, i32* %g, align 4, !tbaa !27
  %shl31 = shl i32 %50, 5
  %and32 = and i32 %shl31, 7936
  %or33 = or i32 %or30, %and32
  store i32 %or33, i32* %rgb, align 4, !tbaa !37
  %51 = load i32, i32* %d0, align 4, !tbaa !37
  %and34 = and i32 %51, 255
  %shl35 = shl i32 %and34, 24
  %52 = load i32, i32* %d0, align 4, !tbaa !37
  %shr36 = ashr i32 %52, 8
  %and37 = and i32 %shr36, 16777215
  %or38 = or i32 %shl35, %and37
  store i32 %or38, i32* %d0, align 4, !tbaa !37
  %53 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr39 = getelementptr inbounds i8, i8* %53, i32 1
  store i8* %incdec.ptr39, i8** %inptr, align 4, !tbaa !2
  %54 = load i8, i8* %53, align 1, !tbaa !36
  %conv40 = zext i8 %54 to i32
  store i32 %conv40, i32* %g, align 4, !tbaa !27
  %55 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %56 = load i32, i32* %g, align 4, !tbaa !27
  %57 = load i32, i32* %d0, align 4, !tbaa !37
  %and41 = and i32 %57, 255
  %add42 = add i32 %56, %and41
  %arrayidx43 = getelementptr inbounds i8, i8* %55, i32 %add42
  %58 = load i8, i8* %arrayidx43, align 1, !tbaa !36
  %conv44 = zext i8 %58 to i32
  store i32 %conv44, i32* %g, align 4, !tbaa !27
  %59 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl45 = shl i32 %59, 16
  %60 = load i32, i32* %g, align 4, !tbaa !27
  %and46 = and i32 %60, 248
  %61 = load i32, i32* %g, align 4, !tbaa !27
  %shr47 = lshr i32 %61, 5
  %or48 = or i32 %and46, %shr47
  %62 = load i32, i32* %g, align 4, !tbaa !27
  %shl49 = shl i32 %62, 11
  %and50 = and i32 %shl49, 57344
  %or51 = or i32 %or48, %and50
  %63 = load i32, i32* %g, align 4, !tbaa !27
  %shl52 = shl i32 %63, 5
  %and53 = and i32 %shl52, 7936
  %or54 = or i32 %or51, %and53
  %or55 = or i32 %shl45, %or54
  store i32 %or55, i32* %rgb, align 4, !tbaa !37
  %64 = load i32, i32* %d0, align 4, !tbaa !37
  %and56 = and i32 %64, 255
  %shl57 = shl i32 %and56, 24
  %65 = load i32, i32* %d0, align 4, !tbaa !37
  %shr58 = ashr i32 %65, 8
  %and59 = and i32 %shr58, 16777215
  %or60 = or i32 %shl57, %and59
  store i32 %or60, i32* %d0, align 4, !tbaa !37
  %66 = load i32, i32* %rgb, align 4, !tbaa !37
  %67 = load i8*, i8** %outptr, align 4, !tbaa !2
  %68 = bitcast i8* %67 to i32*
  store i32 %66, i32* %68, align 4, !tbaa !27
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr61 = getelementptr inbounds i8, i8* %69, i32 4
  store i8* %add.ptr61, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %70 = load i32, i32* %col, align 4, !tbaa !27
  %inc62 = add i32 %70, 1
  store i32 %inc62, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %71 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and63 = and i32 %71, 1
  %tobool64 = icmp ne i32 %and63, 0
  br i1 %tobool64, label %if.then65, label %if.end81

if.then65:                                        ; preds = %for.end
  %72 = load i8*, i8** %inptr, align 4, !tbaa !2
  %73 = load i8, i8* %72, align 1, !tbaa !36
  %conv66 = zext i8 %73 to i32
  store i32 %conv66, i32* %g, align 4, !tbaa !27
  %74 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %75 = load i32, i32* %g, align 4, !tbaa !27
  %76 = load i32, i32* %d0, align 4, !tbaa !37
  %and67 = and i32 %76, 255
  %add68 = add i32 %75, %and67
  %arrayidx69 = getelementptr inbounds i8, i8* %74, i32 %add68
  %77 = load i8, i8* %arrayidx69, align 1, !tbaa !36
  %conv70 = zext i8 %77 to i32
  store i32 %conv70, i32* %g, align 4, !tbaa !27
  %78 = load i32, i32* %g, align 4, !tbaa !27
  %and71 = and i32 %78, 248
  %79 = load i32, i32* %g, align 4, !tbaa !27
  %shr72 = lshr i32 %79, 5
  %or73 = or i32 %and71, %shr72
  %80 = load i32, i32* %g, align 4, !tbaa !27
  %shl74 = shl i32 %80, 11
  %and75 = and i32 %shl74, 57344
  %or76 = or i32 %or73, %and75
  %81 = load i32, i32* %g, align 4, !tbaa !27
  %shl77 = shl i32 %81, 5
  %and78 = and i32 %shl77, 7936
  %or79 = or i32 %or76, %and78
  store i32 %or79, i32* %rgb, align 4, !tbaa !37
  %82 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv80 = trunc i32 %82 to i16
  %83 = load i8*, i8** %outptr, align 4, !tbaa !2
  %84 = bitcast i8* %83 to i16*
  store i16 %conv80, i16* %84, align 2, !tbaa !43
  br label %if.end81

if.end81:                                         ; preds = %if.then65, %for.end
  %85 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %87 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  %89 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #4
  %90 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #4
  %91 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #4
  %92 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @gray_rgb565D_convert_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %inptr = alloca i8*, align 4
  %outptr = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %num_cols = alloca i32, align 4
  %d0 = alloca i32, align 4
  %rgb = alloca i32, align 4
  %g = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 65
  %5 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %5, i8** %range_limit, align 4, !tbaa !2
  %6 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 27
  %8 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %8, i32* %num_cols, align 4, !tbaa !27
  %9 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 34
  %11 = load i32, i32* %output_scanline, align 4, !tbaa !44
  %and = and i32 %11, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %12 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %12, i32* %d0, align 4, !tbaa !37
  br label %while.cond

while.cond:                                       ; preds = %if.end73, %entry
  %13 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %13, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8**, i8*** %16, i32 0
  %17 = load i8**, i8*** %arrayidx1, align 4, !tbaa !2
  %18 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %18, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %17, i32 %18
  %19 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %19, i8** %inptr, align 4, !tbaa !2
  %20 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %20, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %21 = load i8*, i8** %20, align 4, !tbaa !2
  store i8* %21, i8** %outptr, align 4, !tbaa !2
  %22 = load i8*, i8** %outptr, align 4, !tbaa !2
  %23 = ptrtoint i8* %22 to i32
  %and3 = and i32 %23, 3
  %tobool = icmp ne i32 %and3, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %24 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr4 = getelementptr inbounds i8, i8* %24, i32 1
  store i8* %incdec.ptr4, i8** %inptr, align 4, !tbaa !2
  %25 = load i8, i8* %24, align 1, !tbaa !36
  %conv = zext i8 %25 to i32
  store i32 %conv, i32* %g, align 4, !tbaa !27
  %26 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %27 = load i32, i32* %g, align 4, !tbaa !27
  %28 = load i32, i32* %d0, align 4, !tbaa !37
  %and5 = and i32 %28, 255
  %add = add i32 %27, %and5
  %arrayidx6 = getelementptr inbounds i8, i8* %26, i32 %add
  %29 = load i8, i8* %arrayidx6, align 1, !tbaa !36
  %conv7 = zext i8 %29 to i32
  store i32 %conv7, i32* %g, align 4, !tbaa !27
  %30 = load i32, i32* %g, align 4, !tbaa !27
  %shl = shl i32 %30, 8
  %and8 = and i32 %shl, 63488
  %31 = load i32, i32* %g, align 4, !tbaa !27
  %shl9 = shl i32 %31, 3
  %and10 = and i32 %shl9, 2016
  %or = or i32 %and8, %and10
  %32 = load i32, i32* %g, align 4, !tbaa !27
  %shr = lshr i32 %32, 3
  %or11 = or i32 %or, %shr
  store i32 %or11, i32* %rgb, align 4, !tbaa !37
  %33 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv12 = trunc i32 %33 to i16
  %34 = load i8*, i8** %outptr, align 4, !tbaa !2
  %35 = bitcast i8* %34 to i16*
  store i16 %conv12, i16* %35, align 2, !tbaa !43
  %36 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %36, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %37 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec13 = add i32 %37, -1
  store i32 %dec13, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %38 = load i32, i32* %col, align 4, !tbaa !27
  %39 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr14 = lshr i32 %39, 1
  %cmp15 = icmp ult i32 %38, %shr14
  br i1 %cmp15, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %40 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr17 = getelementptr inbounds i8, i8* %40, i32 1
  store i8* %incdec.ptr17, i8** %inptr, align 4, !tbaa !2
  %41 = load i8, i8* %40, align 1, !tbaa !36
  %conv18 = zext i8 %41 to i32
  store i32 %conv18, i32* %g, align 4, !tbaa !27
  %42 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %43 = load i32, i32* %g, align 4, !tbaa !27
  %44 = load i32, i32* %d0, align 4, !tbaa !37
  %and19 = and i32 %44, 255
  %add20 = add i32 %43, %and19
  %arrayidx21 = getelementptr inbounds i8, i8* %42, i32 %add20
  %45 = load i8, i8* %arrayidx21, align 1, !tbaa !36
  %conv22 = zext i8 %45 to i32
  store i32 %conv22, i32* %g, align 4, !tbaa !27
  %46 = load i32, i32* %g, align 4, !tbaa !27
  %shl23 = shl i32 %46, 8
  %and24 = and i32 %shl23, 63488
  %47 = load i32, i32* %g, align 4, !tbaa !27
  %shl25 = shl i32 %47, 3
  %and26 = and i32 %shl25, 2016
  %or27 = or i32 %and24, %and26
  %48 = load i32, i32* %g, align 4, !tbaa !27
  %shr28 = lshr i32 %48, 3
  %or29 = or i32 %or27, %shr28
  store i32 %or29, i32* %rgb, align 4, !tbaa !37
  %49 = load i32, i32* %d0, align 4, !tbaa !37
  %and30 = and i32 %49, 255
  %shl31 = shl i32 %and30, 24
  %50 = load i32, i32* %d0, align 4, !tbaa !37
  %shr32 = ashr i32 %50, 8
  %and33 = and i32 %shr32, 16777215
  %or34 = or i32 %shl31, %and33
  store i32 %or34, i32* %d0, align 4, !tbaa !37
  %51 = load i8*, i8** %inptr, align 4, !tbaa !2
  %incdec.ptr35 = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr35, i8** %inptr, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !36
  %conv36 = zext i8 %52 to i32
  store i32 %conv36, i32* %g, align 4, !tbaa !27
  %53 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %54 = load i32, i32* %g, align 4, !tbaa !27
  %55 = load i32, i32* %d0, align 4, !tbaa !37
  %and37 = and i32 %55, 255
  %add38 = add i32 %54, %and37
  %arrayidx39 = getelementptr inbounds i8, i8* %53, i32 %add38
  %56 = load i8, i8* %arrayidx39, align 1, !tbaa !36
  %conv40 = zext i8 %56 to i32
  store i32 %conv40, i32* %g, align 4, !tbaa !27
  %57 = load i32, i32* %g, align 4, !tbaa !27
  %shl41 = shl i32 %57, 8
  %and42 = and i32 %shl41, 63488
  %58 = load i32, i32* %g, align 4, !tbaa !27
  %shl43 = shl i32 %58, 3
  %and44 = and i32 %shl43, 2016
  %or45 = or i32 %and42, %and44
  %59 = load i32, i32* %g, align 4, !tbaa !27
  %shr46 = lshr i32 %59, 3
  %or47 = or i32 %or45, %shr46
  %shl48 = shl i32 %or47, 16
  %60 = load i32, i32* %rgb, align 4, !tbaa !37
  %or49 = or i32 %shl48, %60
  store i32 %or49, i32* %rgb, align 4, !tbaa !37
  %61 = load i32, i32* %d0, align 4, !tbaa !37
  %and50 = and i32 %61, 255
  %shl51 = shl i32 %and50, 24
  %62 = load i32, i32* %d0, align 4, !tbaa !37
  %shr52 = ashr i32 %62, 8
  %and53 = and i32 %shr52, 16777215
  %or54 = or i32 %shl51, %and53
  store i32 %or54, i32* %d0, align 4, !tbaa !37
  %63 = load i32, i32* %rgb, align 4, !tbaa !37
  %64 = load i8*, i8** %outptr, align 4, !tbaa !2
  %65 = bitcast i8* %64 to i32*
  store i32 %63, i32* %65, align 4, !tbaa !27
  %66 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr55 = getelementptr inbounds i8, i8* %66, i32 4
  store i8* %add.ptr55, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %67 = load i32, i32* %col, align 4, !tbaa !27
  %inc56 = add i32 %67, 1
  store i32 %inc56, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %68 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and57 = and i32 %68, 1
  %tobool58 = icmp ne i32 %and57, 0
  br i1 %tobool58, label %if.then59, label %if.end73

if.then59:                                        ; preds = %for.end
  %69 = load i8*, i8** %inptr, align 4, !tbaa !2
  %70 = load i8, i8* %69, align 1, !tbaa !36
  %conv60 = zext i8 %70 to i32
  store i32 %conv60, i32* %g, align 4, !tbaa !27
  %71 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %72 = load i32, i32* %g, align 4, !tbaa !27
  %73 = load i32, i32* %d0, align 4, !tbaa !37
  %and61 = and i32 %73, 255
  %add62 = add i32 %72, %and61
  %arrayidx63 = getelementptr inbounds i8, i8* %71, i32 %add62
  %74 = load i8, i8* %arrayidx63, align 1, !tbaa !36
  %conv64 = zext i8 %74 to i32
  store i32 %conv64, i32* %g, align 4, !tbaa !27
  %75 = load i32, i32* %g, align 4, !tbaa !27
  %shl65 = shl i32 %75, 8
  %and66 = and i32 %shl65, 63488
  %76 = load i32, i32* %g, align 4, !tbaa !27
  %shl67 = shl i32 %76, 3
  %and68 = and i32 %shl67, 2016
  %or69 = or i32 %and66, %and68
  %77 = load i32, i32* %g, align 4, !tbaa !27
  %shr70 = lshr i32 %77, 3
  %or71 = or i32 %or69, %shr70
  store i32 %or71, i32* %rgb, align 4, !tbaa !37
  %78 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv72 = trunc i32 %78 to i16
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %80 = bitcast i8* %79 to i16*
  store i16 %conv72, i16* %80, align 2, !tbaa !43
  br label %if.end73

if.end73:                                         ; preds = %if.then59, %for.end
  %81 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #4
  %82 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %83 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #4
  %85 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #4
  %86 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #4
  %87 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #4
  %88 = bitcast i8** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_rgb565D_convert_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %num_cols = alloca i32, align 4
  %d0 = alloca i32, align 4
  %rgb = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 65
  %7 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %7, i8** %range_limit, align 4, !tbaa !2
  %8 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 27
  %10 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %10, i32* %num_cols, align 4, !tbaa !27
  %11 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 34
  %13 = load i32, i32* %output_scanline, align 4, !tbaa !44
  %and = and i32 %13, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %14 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %14, i32* %d0, align 4, !tbaa !37
  br label %while.cond

while.cond:                                       ; preds = %if.end135, %entry
  %15 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %16 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8**, i8*** %20, i32 0
  %21 = load i8**, i8*** %arrayidx1, align 4, !tbaa !2
  %22 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %21, i32 %22
  %23 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %23, i8** %inptr0, align 4, !tbaa !2
  %24 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %24, i32 1
  %25 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %26 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %25, i32 %26
  %27 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %27, i8** %inptr1, align 4, !tbaa !2
  %28 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %28, i32 2
  %29 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %30 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %29, i32 %30
  %31 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %31, i8** %inptr2, align 4, !tbaa !2
  %32 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %32, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %33 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %33, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %34 = load i8*, i8** %33, align 4, !tbaa !2
  store i8* %34, i8** %outptr, align 4, !tbaa !2
  %35 = load i8*, i8** %outptr, align 4, !tbaa !2
  %36 = ptrtoint i8* %35 to i32
  %and7 = and i32 %36, 3
  %tobool = icmp ne i32 %and7, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %37 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %38 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %38, i32 1
  store i8* %incdec.ptr8, i8** %inptr0, align 4, !tbaa !2
  %39 = load i8, i8* %38, align 1, !tbaa !36
  %conv = zext i8 %39 to i32
  %40 = load i32, i32* %d0, align 4, !tbaa !37
  %and9 = and i32 %40, 255
  %add = add nsw i32 %conv, %and9
  %arrayidx10 = getelementptr inbounds i8, i8* %37, i32 %add
  %41 = load i8, i8* %arrayidx10, align 1, !tbaa !36
  %conv11 = zext i8 %41 to i32
  store i32 %conv11, i32* %r, align 4, !tbaa !27
  %42 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %43 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %43, i32 1
  store i8* %incdec.ptr12, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8, i8* %43, align 1, !tbaa !36
  %conv13 = zext i8 %44 to i32
  %45 = load i32, i32* %d0, align 4, !tbaa !37
  %and14 = and i32 %45, 255
  %shr = ashr i32 %and14, 1
  %add15 = add nsw i32 %conv13, %shr
  %arrayidx16 = getelementptr inbounds i8, i8* %42, i32 %add15
  %46 = load i8, i8* %arrayidx16, align 1, !tbaa !36
  %conv17 = zext i8 %46 to i32
  store i32 %conv17, i32* %g, align 4, !tbaa !27
  %47 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %48 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr18 = getelementptr inbounds i8, i8* %48, i32 1
  store i8* %incdec.ptr18, i8** %inptr2, align 4, !tbaa !2
  %49 = load i8, i8* %48, align 1, !tbaa !36
  %conv19 = zext i8 %49 to i32
  %50 = load i32, i32* %d0, align 4, !tbaa !37
  %and20 = and i32 %50, 255
  %add21 = add nsw i32 %conv19, %and20
  %arrayidx22 = getelementptr inbounds i8, i8* %47, i32 %add21
  %51 = load i8, i8* %arrayidx22, align 1, !tbaa !36
  %conv23 = zext i8 %51 to i32
  store i32 %conv23, i32* %b, align 4, !tbaa !27
  %52 = load i32, i32* %r, align 4, !tbaa !27
  %and24 = and i32 %52, 248
  %53 = load i32, i32* %g, align 4, !tbaa !27
  %shr25 = lshr i32 %53, 5
  %or = or i32 %and24, %shr25
  %54 = load i32, i32* %g, align 4, !tbaa !27
  %shl = shl i32 %54, 11
  %and26 = and i32 %shl, 57344
  %or27 = or i32 %or, %and26
  %55 = load i32, i32* %b, align 4, !tbaa !27
  %shl28 = shl i32 %55, 5
  %and29 = and i32 %shl28, 7936
  %or30 = or i32 %or27, %and29
  store i32 %or30, i32* %rgb, align 4, !tbaa !37
  %56 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv31 = trunc i32 %56 to i16
  %57 = load i8*, i8** %outptr, align 4, !tbaa !2
  %58 = bitcast i8* %57 to i16*
  store i16 %conv31, i16* %58, align 2, !tbaa !43
  %59 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %59, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %60 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec32 = add i32 %60, -1
  store i32 %dec32, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %61 = load i32, i32* %col, align 4, !tbaa !27
  %62 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr33 = lshr i32 %62, 1
  %cmp34 = icmp ult i32 %61, %shr33
  br i1 %cmp34, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %63 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %64 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr36 = getelementptr inbounds i8, i8* %64, i32 1
  store i8* %incdec.ptr36, i8** %inptr0, align 4, !tbaa !2
  %65 = load i8, i8* %64, align 1, !tbaa !36
  %conv37 = zext i8 %65 to i32
  %66 = load i32, i32* %d0, align 4, !tbaa !37
  %and38 = and i32 %66, 255
  %add39 = add nsw i32 %conv37, %and38
  %arrayidx40 = getelementptr inbounds i8, i8* %63, i32 %add39
  %67 = load i8, i8* %arrayidx40, align 1, !tbaa !36
  %conv41 = zext i8 %67 to i32
  store i32 %conv41, i32* %r, align 4, !tbaa !27
  %68 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %69 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr42 = getelementptr inbounds i8, i8* %69, i32 1
  store i8* %incdec.ptr42, i8** %inptr1, align 4, !tbaa !2
  %70 = load i8, i8* %69, align 1, !tbaa !36
  %conv43 = zext i8 %70 to i32
  %71 = load i32, i32* %d0, align 4, !tbaa !37
  %and44 = and i32 %71, 255
  %shr45 = ashr i32 %and44, 1
  %add46 = add nsw i32 %conv43, %shr45
  %arrayidx47 = getelementptr inbounds i8, i8* %68, i32 %add46
  %72 = load i8, i8* %arrayidx47, align 1, !tbaa !36
  %conv48 = zext i8 %72 to i32
  store i32 %conv48, i32* %g, align 4, !tbaa !27
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr49 = getelementptr inbounds i8, i8* %74, i32 1
  store i8* %incdec.ptr49, i8** %inptr2, align 4, !tbaa !2
  %75 = load i8, i8* %74, align 1, !tbaa !36
  %conv50 = zext i8 %75 to i32
  %76 = load i32, i32* %d0, align 4, !tbaa !37
  %and51 = and i32 %76, 255
  %add52 = add nsw i32 %conv50, %and51
  %arrayidx53 = getelementptr inbounds i8, i8* %73, i32 %add52
  %77 = load i8, i8* %arrayidx53, align 1, !tbaa !36
  %conv54 = zext i8 %77 to i32
  store i32 %conv54, i32* %b, align 4, !tbaa !27
  %78 = load i32, i32* %d0, align 4, !tbaa !37
  %and55 = and i32 %78, 255
  %shl56 = shl i32 %and55, 24
  %79 = load i32, i32* %d0, align 4, !tbaa !37
  %shr57 = ashr i32 %79, 8
  %and58 = and i32 %shr57, 16777215
  %or59 = or i32 %shl56, %and58
  store i32 %or59, i32* %d0, align 4, !tbaa !37
  %80 = load i32, i32* %r, align 4, !tbaa !27
  %and60 = and i32 %80, 248
  %81 = load i32, i32* %g, align 4, !tbaa !27
  %shr61 = lshr i32 %81, 5
  %or62 = or i32 %and60, %shr61
  %82 = load i32, i32* %g, align 4, !tbaa !27
  %shl63 = shl i32 %82, 11
  %and64 = and i32 %shl63, 57344
  %or65 = or i32 %or62, %and64
  %83 = load i32, i32* %b, align 4, !tbaa !27
  %shl66 = shl i32 %83, 5
  %and67 = and i32 %shl66, 7936
  %or68 = or i32 %or65, %and67
  store i32 %or68, i32* %rgb, align 4, !tbaa !37
  %84 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %85 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr69 = getelementptr inbounds i8, i8* %85, i32 1
  store i8* %incdec.ptr69, i8** %inptr0, align 4, !tbaa !2
  %86 = load i8, i8* %85, align 1, !tbaa !36
  %conv70 = zext i8 %86 to i32
  %87 = load i32, i32* %d0, align 4, !tbaa !37
  %and71 = and i32 %87, 255
  %add72 = add nsw i32 %conv70, %and71
  %arrayidx73 = getelementptr inbounds i8, i8* %84, i32 %add72
  %88 = load i8, i8* %arrayidx73, align 1, !tbaa !36
  %conv74 = zext i8 %88 to i32
  store i32 %conv74, i32* %r, align 4, !tbaa !27
  %89 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %90 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr75 = getelementptr inbounds i8, i8* %90, i32 1
  store i8* %incdec.ptr75, i8** %inptr1, align 4, !tbaa !2
  %91 = load i8, i8* %90, align 1, !tbaa !36
  %conv76 = zext i8 %91 to i32
  %92 = load i32, i32* %d0, align 4, !tbaa !37
  %and77 = and i32 %92, 255
  %shr78 = ashr i32 %and77, 1
  %add79 = add nsw i32 %conv76, %shr78
  %arrayidx80 = getelementptr inbounds i8, i8* %89, i32 %add79
  %93 = load i8, i8* %arrayidx80, align 1, !tbaa !36
  %conv81 = zext i8 %93 to i32
  store i32 %conv81, i32* %g, align 4, !tbaa !27
  %94 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %95 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr82 = getelementptr inbounds i8, i8* %95, i32 1
  store i8* %incdec.ptr82, i8** %inptr2, align 4, !tbaa !2
  %96 = load i8, i8* %95, align 1, !tbaa !36
  %conv83 = zext i8 %96 to i32
  %97 = load i32, i32* %d0, align 4, !tbaa !37
  %and84 = and i32 %97, 255
  %add85 = add nsw i32 %conv83, %and84
  %arrayidx86 = getelementptr inbounds i8, i8* %94, i32 %add85
  %98 = load i8, i8* %arrayidx86, align 1, !tbaa !36
  %conv87 = zext i8 %98 to i32
  store i32 %conv87, i32* %b, align 4, !tbaa !27
  %99 = load i32, i32* %d0, align 4, !tbaa !37
  %and88 = and i32 %99, 255
  %shl89 = shl i32 %and88, 24
  %100 = load i32, i32* %d0, align 4, !tbaa !37
  %shr90 = ashr i32 %100, 8
  %and91 = and i32 %shr90, 16777215
  %or92 = or i32 %shl89, %and91
  store i32 %or92, i32* %d0, align 4, !tbaa !37
  %101 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl93 = shl i32 %101, 16
  %102 = load i32, i32* %r, align 4, !tbaa !27
  %and94 = and i32 %102, 248
  %103 = load i32, i32* %g, align 4, !tbaa !27
  %shr95 = lshr i32 %103, 5
  %or96 = or i32 %and94, %shr95
  %104 = load i32, i32* %g, align 4, !tbaa !27
  %shl97 = shl i32 %104, 11
  %and98 = and i32 %shl97, 57344
  %or99 = or i32 %or96, %and98
  %105 = load i32, i32* %b, align 4, !tbaa !27
  %shl100 = shl i32 %105, 5
  %and101 = and i32 %shl100, 7936
  %or102 = or i32 %or99, %and101
  %or103 = or i32 %shl93, %or102
  store i32 %or103, i32* %rgb, align 4, !tbaa !37
  %106 = load i32, i32* %rgb, align 4, !tbaa !37
  %107 = load i8*, i8** %outptr, align 4, !tbaa !2
  %108 = bitcast i8* %107 to i32*
  store i32 %106, i32* %108, align 4, !tbaa !27
  %109 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr104 = getelementptr inbounds i8, i8* %109, i32 4
  store i8* %add.ptr104, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %110 = load i32, i32* %col, align 4, !tbaa !27
  %inc105 = add i32 %110, 1
  store i32 %inc105, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %111 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and106 = and i32 %111, 1
  %tobool107 = icmp ne i32 %and106, 0
  br i1 %tobool107, label %if.then108, label %if.end135

if.then108:                                       ; preds = %for.end
  %112 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %113 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %114 = load i8, i8* %113, align 1, !tbaa !36
  %conv109 = zext i8 %114 to i32
  %115 = load i32, i32* %d0, align 4, !tbaa !37
  %and110 = and i32 %115, 255
  %add111 = add nsw i32 %conv109, %and110
  %arrayidx112 = getelementptr inbounds i8, i8* %112, i32 %add111
  %116 = load i8, i8* %arrayidx112, align 1, !tbaa !36
  %conv113 = zext i8 %116 to i32
  store i32 %conv113, i32* %r, align 4, !tbaa !27
  %117 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %118 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %119 = load i8, i8* %118, align 1, !tbaa !36
  %conv114 = zext i8 %119 to i32
  %120 = load i32, i32* %d0, align 4, !tbaa !37
  %and115 = and i32 %120, 255
  %shr116 = ashr i32 %and115, 1
  %add117 = add nsw i32 %conv114, %shr116
  %arrayidx118 = getelementptr inbounds i8, i8* %117, i32 %add117
  %121 = load i8, i8* %arrayidx118, align 1, !tbaa !36
  %conv119 = zext i8 %121 to i32
  store i32 %conv119, i32* %g, align 4, !tbaa !27
  %122 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %123 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %124 = load i8, i8* %123, align 1, !tbaa !36
  %conv120 = zext i8 %124 to i32
  %125 = load i32, i32* %d0, align 4, !tbaa !37
  %and121 = and i32 %125, 255
  %add122 = add nsw i32 %conv120, %and121
  %arrayidx123 = getelementptr inbounds i8, i8* %122, i32 %add122
  %126 = load i8, i8* %arrayidx123, align 1, !tbaa !36
  %conv124 = zext i8 %126 to i32
  store i32 %conv124, i32* %b, align 4, !tbaa !27
  %127 = load i32, i32* %r, align 4, !tbaa !27
  %and125 = and i32 %127, 248
  %128 = load i32, i32* %g, align 4, !tbaa !27
  %shr126 = lshr i32 %128, 5
  %or127 = or i32 %and125, %shr126
  %129 = load i32, i32* %g, align 4, !tbaa !27
  %shl128 = shl i32 %129, 11
  %and129 = and i32 %shl128, 57344
  %or130 = or i32 %or127, %and129
  %130 = load i32, i32* %b, align 4, !tbaa !27
  %shl131 = shl i32 %130, 5
  %and132 = and i32 %shl131, 7936
  %or133 = or i32 %or130, %and132
  store i32 %or133, i32* %rgb, align 4, !tbaa !37
  %131 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv134 = trunc i32 %131 to i16
  %132 = load i8*, i8** %outptr, align 4, !tbaa !2
  %133 = bitcast i8* %132 to i16*
  store i16 %conv134, i16* %133, align 2, !tbaa !43
  br label %if.end135

if.end135:                                        ; preds = %if.then108, %for.end
  %134 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #4
  %135 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  %136 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  %137 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %138 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  %143 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %144 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @rgb_rgb565D_convert_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %num_cols = alloca i32, align 4
  %d0 = alloca i32, align 4
  %rgb = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !27
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !27
  %0 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #4
  %4 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 65
  %7 = load i8*, i8** %sample_range_limit, align 4, !tbaa !42
  store i8* %7, i8** %range_limit, align 4, !tbaa !2
  %8 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 27
  %10 = load i32, i32* %output_width, align 8, !tbaa !34
  store i32 %10, i32* %num_cols, align 4, !tbaa !27
  %11 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 34
  %13 = load i32, i32* %output_scanline, align 4, !tbaa !44
  %and = and i32 %13, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %14 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %14, i32* %d0, align 4, !tbaa !37
  br label %while.cond

while.cond:                                       ; preds = %if.end127, %entry
  %15 = load i32, i32* %num_rows.addr, align 4, !tbaa !27
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %num_rows.addr, align 4, !tbaa !27
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %16 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #4
  %20 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8**, i8*** %20, i32 0
  %21 = load i8**, i8*** %arrayidx1, align 4, !tbaa !2
  %22 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx2 = getelementptr inbounds i8*, i8** %21, i32 %22
  %23 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %23, i8** %inptr0, align 4, !tbaa !2
  %24 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %24, i32 1
  %25 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %26 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx4 = getelementptr inbounds i8*, i8** %25, i32 %26
  %27 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %27, i8** %inptr1, align 4, !tbaa !2
  %28 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %28, i32 2
  %29 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %30 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %arrayidx6 = getelementptr inbounds i8*, i8** %29, i32 %30
  %31 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %31, i8** %inptr2, align 4, !tbaa !2
  %32 = load i32, i32* %input_row.addr, align 4, !tbaa !27
  %inc = add i32 %32, 1
  store i32 %inc, i32* %input_row.addr, align 4, !tbaa !27
  %33 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8*, i8** %33, i32 1
  store i8** %incdec.ptr, i8*** %output_buf.addr, align 4, !tbaa !2
  %34 = load i8*, i8** %33, align 4, !tbaa !2
  store i8* %34, i8** %outptr, align 4, !tbaa !2
  %35 = load i8*, i8** %outptr, align 4, !tbaa !2
  %36 = ptrtoint i8* %35 to i32
  %and7 = and i32 %36, 3
  %tobool = icmp ne i32 %and7, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %37 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %38 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %38, i32 1
  store i8* %incdec.ptr8, i8** %inptr0, align 4, !tbaa !2
  %39 = load i8, i8* %38, align 1, !tbaa !36
  %conv = zext i8 %39 to i32
  %40 = load i32, i32* %d0, align 4, !tbaa !37
  %and9 = and i32 %40, 255
  %add = add nsw i32 %conv, %and9
  %arrayidx10 = getelementptr inbounds i8, i8* %37, i32 %add
  %41 = load i8, i8* %arrayidx10, align 1, !tbaa !36
  %conv11 = zext i8 %41 to i32
  store i32 %conv11, i32* %r, align 4, !tbaa !27
  %42 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %43 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %43, i32 1
  store i8* %incdec.ptr12, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8, i8* %43, align 1, !tbaa !36
  %conv13 = zext i8 %44 to i32
  %45 = load i32, i32* %d0, align 4, !tbaa !37
  %and14 = and i32 %45, 255
  %shr = ashr i32 %and14, 1
  %add15 = add nsw i32 %conv13, %shr
  %arrayidx16 = getelementptr inbounds i8, i8* %42, i32 %add15
  %46 = load i8, i8* %arrayidx16, align 1, !tbaa !36
  %conv17 = zext i8 %46 to i32
  store i32 %conv17, i32* %g, align 4, !tbaa !27
  %47 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %48 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr18 = getelementptr inbounds i8, i8* %48, i32 1
  store i8* %incdec.ptr18, i8** %inptr2, align 4, !tbaa !2
  %49 = load i8, i8* %48, align 1, !tbaa !36
  %conv19 = zext i8 %49 to i32
  %50 = load i32, i32* %d0, align 4, !tbaa !37
  %and20 = and i32 %50, 255
  %add21 = add nsw i32 %conv19, %and20
  %arrayidx22 = getelementptr inbounds i8, i8* %47, i32 %add21
  %51 = load i8, i8* %arrayidx22, align 1, !tbaa !36
  %conv23 = zext i8 %51 to i32
  store i32 %conv23, i32* %b, align 4, !tbaa !27
  %52 = load i32, i32* %r, align 4, !tbaa !27
  %shl = shl i32 %52, 8
  %and24 = and i32 %shl, 63488
  %53 = load i32, i32* %g, align 4, !tbaa !27
  %shl25 = shl i32 %53, 3
  %and26 = and i32 %shl25, 2016
  %or = or i32 %and24, %and26
  %54 = load i32, i32* %b, align 4, !tbaa !27
  %shr27 = lshr i32 %54, 3
  %or28 = or i32 %or, %shr27
  store i32 %or28, i32* %rgb, align 4, !tbaa !37
  %55 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv29 = trunc i32 %55 to i16
  %56 = load i8*, i8** %outptr, align 4, !tbaa !2
  %57 = bitcast i8* %56 to i16*
  store i16 %conv29, i16* %57, align 2, !tbaa !43
  %58 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %58, i32 2
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %59 = load i32, i32* %num_cols, align 4, !tbaa !27
  %dec30 = add i32 %59, -1
  store i32 %dec30, i32* %num_cols, align 4, !tbaa !27
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  store i32 0, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %60 = load i32, i32* %col, align 4, !tbaa !27
  %61 = load i32, i32* %num_cols, align 4, !tbaa !27
  %shr31 = lshr i32 %61, 1
  %cmp32 = icmp ult i32 %60, %shr31
  br i1 %cmp32, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %62 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %63 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr34 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr34, i8** %inptr0, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !36
  %conv35 = zext i8 %64 to i32
  %65 = load i32, i32* %d0, align 4, !tbaa !37
  %and36 = and i32 %65, 255
  %add37 = add nsw i32 %conv35, %and36
  %arrayidx38 = getelementptr inbounds i8, i8* %62, i32 %add37
  %66 = load i8, i8* %arrayidx38, align 1, !tbaa !36
  %conv39 = zext i8 %66 to i32
  store i32 %conv39, i32* %r, align 4, !tbaa !27
  %67 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %68 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr40 = getelementptr inbounds i8, i8* %68, i32 1
  store i8* %incdec.ptr40, i8** %inptr1, align 4, !tbaa !2
  %69 = load i8, i8* %68, align 1, !tbaa !36
  %conv41 = zext i8 %69 to i32
  %70 = load i32, i32* %d0, align 4, !tbaa !37
  %and42 = and i32 %70, 255
  %shr43 = ashr i32 %and42, 1
  %add44 = add nsw i32 %conv41, %shr43
  %arrayidx45 = getelementptr inbounds i8, i8* %67, i32 %add44
  %71 = load i8, i8* %arrayidx45, align 1, !tbaa !36
  %conv46 = zext i8 %71 to i32
  store i32 %conv46, i32* %g, align 4, !tbaa !27
  %72 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %73 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr47 = getelementptr inbounds i8, i8* %73, i32 1
  store i8* %incdec.ptr47, i8** %inptr2, align 4, !tbaa !2
  %74 = load i8, i8* %73, align 1, !tbaa !36
  %conv48 = zext i8 %74 to i32
  %75 = load i32, i32* %d0, align 4, !tbaa !37
  %and49 = and i32 %75, 255
  %add50 = add nsw i32 %conv48, %and49
  %arrayidx51 = getelementptr inbounds i8, i8* %72, i32 %add50
  %76 = load i8, i8* %arrayidx51, align 1, !tbaa !36
  %conv52 = zext i8 %76 to i32
  store i32 %conv52, i32* %b, align 4, !tbaa !27
  %77 = load i32, i32* %d0, align 4, !tbaa !37
  %and53 = and i32 %77, 255
  %shl54 = shl i32 %and53, 24
  %78 = load i32, i32* %d0, align 4, !tbaa !37
  %shr55 = ashr i32 %78, 8
  %and56 = and i32 %shr55, 16777215
  %or57 = or i32 %shl54, %and56
  store i32 %or57, i32* %d0, align 4, !tbaa !37
  %79 = load i32, i32* %r, align 4, !tbaa !27
  %shl58 = shl i32 %79, 8
  %and59 = and i32 %shl58, 63488
  %80 = load i32, i32* %g, align 4, !tbaa !27
  %shl60 = shl i32 %80, 3
  %and61 = and i32 %shl60, 2016
  %or62 = or i32 %and59, %and61
  %81 = load i32, i32* %b, align 4, !tbaa !27
  %shr63 = lshr i32 %81, 3
  %or64 = or i32 %or62, %shr63
  store i32 %or64, i32* %rgb, align 4, !tbaa !37
  %82 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %83 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr65 = getelementptr inbounds i8, i8* %83, i32 1
  store i8* %incdec.ptr65, i8** %inptr0, align 4, !tbaa !2
  %84 = load i8, i8* %83, align 1, !tbaa !36
  %conv66 = zext i8 %84 to i32
  %85 = load i32, i32* %d0, align 4, !tbaa !37
  %and67 = and i32 %85, 255
  %add68 = add nsw i32 %conv66, %and67
  %arrayidx69 = getelementptr inbounds i8, i8* %82, i32 %add68
  %86 = load i8, i8* %arrayidx69, align 1, !tbaa !36
  %conv70 = zext i8 %86 to i32
  store i32 %conv70, i32* %r, align 4, !tbaa !27
  %87 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %88 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr71 = getelementptr inbounds i8, i8* %88, i32 1
  store i8* %incdec.ptr71, i8** %inptr1, align 4, !tbaa !2
  %89 = load i8, i8* %88, align 1, !tbaa !36
  %conv72 = zext i8 %89 to i32
  %90 = load i32, i32* %d0, align 4, !tbaa !37
  %and73 = and i32 %90, 255
  %shr74 = ashr i32 %and73, 1
  %add75 = add nsw i32 %conv72, %shr74
  %arrayidx76 = getelementptr inbounds i8, i8* %87, i32 %add75
  %91 = load i8, i8* %arrayidx76, align 1, !tbaa !36
  %conv77 = zext i8 %91 to i32
  store i32 %conv77, i32* %g, align 4, !tbaa !27
  %92 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %93 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr78 = getelementptr inbounds i8, i8* %93, i32 1
  store i8* %incdec.ptr78, i8** %inptr2, align 4, !tbaa !2
  %94 = load i8, i8* %93, align 1, !tbaa !36
  %conv79 = zext i8 %94 to i32
  %95 = load i32, i32* %d0, align 4, !tbaa !37
  %and80 = and i32 %95, 255
  %add81 = add nsw i32 %conv79, %and80
  %arrayidx82 = getelementptr inbounds i8, i8* %92, i32 %add81
  %96 = load i8, i8* %arrayidx82, align 1, !tbaa !36
  %conv83 = zext i8 %96 to i32
  store i32 %conv83, i32* %b, align 4, !tbaa !27
  %97 = load i32, i32* %d0, align 4, !tbaa !37
  %and84 = and i32 %97, 255
  %shl85 = shl i32 %and84, 24
  %98 = load i32, i32* %d0, align 4, !tbaa !37
  %shr86 = ashr i32 %98, 8
  %and87 = and i32 %shr86, 16777215
  %or88 = or i32 %shl85, %and87
  store i32 %or88, i32* %d0, align 4, !tbaa !37
  %99 = load i32, i32* %r, align 4, !tbaa !27
  %shl89 = shl i32 %99, 8
  %and90 = and i32 %shl89, 63488
  %100 = load i32, i32* %g, align 4, !tbaa !27
  %shl91 = shl i32 %100, 3
  %and92 = and i32 %shl91, 2016
  %or93 = or i32 %and90, %and92
  %101 = load i32, i32* %b, align 4, !tbaa !27
  %shr94 = lshr i32 %101, 3
  %or95 = or i32 %or93, %shr94
  %shl96 = shl i32 %or95, 16
  %102 = load i32, i32* %rgb, align 4, !tbaa !37
  %or97 = or i32 %shl96, %102
  store i32 %or97, i32* %rgb, align 4, !tbaa !37
  %103 = load i32, i32* %rgb, align 4, !tbaa !37
  %104 = load i8*, i8** %outptr, align 4, !tbaa !2
  %105 = bitcast i8* %104 to i32*
  store i32 %103, i32* %105, align 4, !tbaa !27
  %106 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr98 = getelementptr inbounds i8, i8* %106, i32 4
  store i8* %add.ptr98, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %107 = load i32, i32* %col, align 4, !tbaa !27
  %inc99 = add i32 %107, 1
  store i32 %inc99, i32* %col, align 4, !tbaa !27
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %108 = load i32, i32* %num_cols, align 4, !tbaa !27
  %and100 = and i32 %108, 1
  %tobool101 = icmp ne i32 %and100, 0
  br i1 %tobool101, label %if.then102, label %if.end127

if.then102:                                       ; preds = %for.end
  %109 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %110 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %111 = load i8, i8* %110, align 1, !tbaa !36
  %conv103 = zext i8 %111 to i32
  %112 = load i32, i32* %d0, align 4, !tbaa !37
  %and104 = and i32 %112, 255
  %add105 = add nsw i32 %conv103, %and104
  %arrayidx106 = getelementptr inbounds i8, i8* %109, i32 %add105
  %113 = load i8, i8* %arrayidx106, align 1, !tbaa !36
  %conv107 = zext i8 %113 to i32
  store i32 %conv107, i32* %r, align 4, !tbaa !27
  %114 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %115 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %116 = load i8, i8* %115, align 1, !tbaa !36
  %conv108 = zext i8 %116 to i32
  %117 = load i32, i32* %d0, align 4, !tbaa !37
  %and109 = and i32 %117, 255
  %shr110 = ashr i32 %and109, 1
  %add111 = add nsw i32 %conv108, %shr110
  %arrayidx112 = getelementptr inbounds i8, i8* %114, i32 %add111
  %118 = load i8, i8* %arrayidx112, align 1, !tbaa !36
  %conv113 = zext i8 %118 to i32
  store i32 %conv113, i32* %g, align 4, !tbaa !27
  %119 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %120 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %121 = load i8, i8* %120, align 1, !tbaa !36
  %conv114 = zext i8 %121 to i32
  %122 = load i32, i32* %d0, align 4, !tbaa !37
  %and115 = and i32 %122, 255
  %add116 = add nsw i32 %conv114, %and115
  %arrayidx117 = getelementptr inbounds i8, i8* %119, i32 %add116
  %123 = load i8, i8* %arrayidx117, align 1, !tbaa !36
  %conv118 = zext i8 %123 to i32
  store i32 %conv118, i32* %b, align 4, !tbaa !27
  %124 = load i32, i32* %r, align 4, !tbaa !27
  %shl119 = shl i32 %124, 8
  %and120 = and i32 %shl119, 63488
  %125 = load i32, i32* %g, align 4, !tbaa !27
  %shl121 = shl i32 %125, 3
  %and122 = and i32 %shl121, 2016
  %or123 = or i32 %and120, %and122
  %126 = load i32, i32* %b, align 4, !tbaa !27
  %shr124 = lshr i32 %126, 3
  %or125 = or i32 %or123, %shr124
  store i32 %or125, i32* %rgb, align 4, !tbaa !37
  %127 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv126 = trunc i32 %127 to i16
  %128 = load i8*, i8** %outptr, align 4, !tbaa !2
  %129 = bitcast i8* %128 to i16*
  store i16 %conv126, i16* %129, align 2, !tbaa !43
  br label %if.end127

if.end127:                                        ; preds = %if.then102, %for.end
  %130 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #4
  %131 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #4
  %132 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #4
  %133 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %134 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #4
  %135 = bitcast i32* %num_cols to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  %136 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  %137 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #4
  %138 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 456}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!17 = !{!"jpeg_color_deconverter", !3, i64 0, !3, i64 4}
!18 = !{!7, !4, i64 40}
!19 = !{!7, !8, i64 36}
!20 = !{!7, !3, i64 0}
!21 = !{!22, !8, i64 20}
!22 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!23 = !{!22, !3, i64 0}
!24 = !{!7, !4, i64 44}
!25 = !{!7, !8, i64 120}
!26 = !{!16, !3, i64 4}
!27 = !{!8, !8, i64 0}
!28 = !{!7, !3, i64 216}
!29 = !{!30, !8, i64 48}
!30 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!31 = !{!7, !4, i64 88}
!32 = !{!7, !8, i64 84}
!33 = !{!7, !8, i64 124}
!34 = !{!7, !8, i64 112}
!35 = !{!16, !3, i64 24}
!36 = !{!4, !4, i64 0}
!37 = !{!13, !13, i64 0}
!38 = !{!16, !3, i64 8}
!39 = !{!16, !3, i64 12}
!40 = !{!16, !3, i64 16}
!41 = !{!16, !3, i64 20}
!42 = !{!7, !3, i64 324}
!43 = !{!10, !10, i64 0}
!44 = !{!7, !8, i64 140}
