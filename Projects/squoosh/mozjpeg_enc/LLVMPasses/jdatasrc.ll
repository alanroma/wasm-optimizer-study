; ModuleID = 'jdatasrc.c'
source_filename = "jdatasrc.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type opaque
%struct.jpeg_d_main_controller = type opaque
%struct.jpeg_d_coef_controller = type opaque
%struct.jpeg_d_post_controller = type opaque
%struct.jpeg_input_controller = type opaque
%struct.jpeg_marker_reader = type opaque
%struct.jpeg_entropy_decoder = type opaque
%struct.jpeg_inverse_dct = type opaque
%struct.jpeg_upsampler = type opaque
%struct.jpeg_color_deconverter = type opaque
%struct.jpeg_color_quantizer = type opaque
%struct._IO_FILE = type opaque
%struct.my_source_mgr = type { %struct.jpeg_source_mgr, %struct._IO_FILE*, i8*, i32 }

@fill_mem_input_buffer.mybuffer = internal constant [4 x i8] c"\FF\D9\00\00", align 1

; Function Attrs: nounwind
define hidden void @jpeg_stdio_src(%struct.jpeg_decompress_struct* %cinfo, %struct._IO_FILE* %infile) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %infile.addr = alloca %struct._IO_FILE*, align 4
  %src = alloca %struct.my_source_mgr*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %infile, %struct._IO_FILE** %infile.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_source_mgr** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src1, align 8, !tbaa !6
  %cmp = icmp eq %struct.jpeg_source_mgr* %2, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 1
  %4 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !11
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %4, i32 0, i32 0
  %5 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_decompress_struct* %6 to %struct.jpeg_common_struct*
  %call = call i8* %5(%struct.jpeg_common_struct* %7, i32 0, i32 40)
  %8 = bitcast i8* %call to %struct.jpeg_source_mgr*
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 6
  store %struct.jpeg_source_mgr* %8, %struct.jpeg_source_mgr** %src2, align 8, !tbaa !6
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 6
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src3, align 8, !tbaa !6
  %12 = bitcast %struct.jpeg_source_mgr* %11 to %struct.my_source_mgr*
  store %struct.my_source_mgr* %12, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 1
  %14 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem4, align 4, !tbaa !11
  %alloc_small5 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %14, i32 0, i32 0
  %15 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small5, align 4, !tbaa !12
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = bitcast %struct.jpeg_decompress_struct* %16 to %struct.jpeg_common_struct*
  %call6 = call i8* %15(%struct.jpeg_common_struct* %17, i32 0, i32 4096)
  %18 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %18, i32 0, i32 2
  store i8* %call6, i8** %buffer, align 4, !tbaa !15
  br label %if.end11

if.else:                                          ; preds = %entry
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 6
  %20 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src7, align 8, !tbaa !6
  %init_source = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %20, i32 0, i32 2
  %21 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %init_source, align 4, !tbaa !18
  %cmp8 = icmp ne void (%struct.jpeg_decompress_struct*)* %21, @init_source
  br i1 %cmp8, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.else
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 0
  %23 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !19
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %23, i32 0, i32 5
  store i32 23, i32* %msg_code, align 4, !tbaa !20
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 0
  %25 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !19
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %25, i32 0, i32 0
  %26 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !22
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = bitcast %struct.jpeg_decompress_struct* %27 to %struct.jpeg_common_struct*
  call void %26(%struct.jpeg_common_struct* %28)
  br label %if.end

if.end:                                           ; preds = %if.then9, %if.else
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 6
  %30 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src12, align 8, !tbaa !6
  %31 = bitcast %struct.jpeg_source_mgr* %30 to %struct.my_source_mgr*
  store %struct.my_source_mgr* %31, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %32 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %32, i32 0, i32 0
  %init_source13 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub, i32 0, i32 2
  store void (%struct.jpeg_decompress_struct*)* @init_source, void (%struct.jpeg_decompress_struct*)** %init_source13, align 4, !tbaa !23
  %33 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub14 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %33, i32 0, i32 0
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub14, i32 0, i32 3
  store i32 (%struct.jpeg_decompress_struct*)* @fill_input_buffer, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !24
  %34 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub15 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %34, i32 0, i32 0
  %skip_input_data = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub15, i32 0, i32 4
  store void (%struct.jpeg_decompress_struct*, i32)* @skip_input_data, void (%struct.jpeg_decompress_struct*, i32)** %skip_input_data, align 4, !tbaa !25
  %35 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub16 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %35, i32 0, i32 0
  %resync_to_restart = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub16, i32 0, i32 5
  store i32 (%struct.jpeg_decompress_struct*, i32)* @jpeg_resync_to_restart, i32 (%struct.jpeg_decompress_struct*, i32)** %resync_to_restart, align 4, !tbaa !26
  %36 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub17 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %36, i32 0, i32 0
  %term_source = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub17, i32 0, i32 6
  store void (%struct.jpeg_decompress_struct*)* @term_source, void (%struct.jpeg_decompress_struct*)** %term_source, align 4, !tbaa !27
  %37 = load %struct._IO_FILE*, %struct._IO_FILE** %infile.addr, align 4, !tbaa !2
  %38 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %infile18 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %38, i32 0, i32 1
  store %struct._IO_FILE* %37, %struct._IO_FILE** %infile18, align 4, !tbaa !28
  %39 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub19 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %39, i32 0, i32 0
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub19, i32 0, i32 1
  store i32 0, i32* %bytes_in_buffer, align 4, !tbaa !29
  %40 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub20 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %40, i32 0, i32 0
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub20, i32 0, i32 0
  store i8* null, i8** %next_input_byte, align 4, !tbaa !30
  %41 = bitcast %struct.my_source_mgr** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @init_source(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %src = alloca %struct.my_source_mgr*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_source_mgr** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src1, align 8, !tbaa !6
  %3 = bitcast %struct.jpeg_source_mgr* %2 to %struct.my_source_mgr*
  store %struct.my_source_mgr* %3, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %4 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %start_of_file = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %4, i32 0, i32 3
  store i32 1, i32* %start_of_file, align 4, !tbaa !31
  %5 = bitcast %struct.my_source_mgr** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @fill_input_buffer(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %src = alloca %struct.my_source_mgr*, align 4
  %nbytes = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_source_mgr** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src1, align 8, !tbaa !6
  %3 = bitcast %struct.jpeg_source_mgr* %2 to %struct.my_source_mgr*
  store %struct.my_source_mgr* %3, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %4 = bitcast i32* %nbytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %5, i32 0, i32 2
  %6 = load i8*, i8** %buffer, align 4, !tbaa !15
  %7 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %infile = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %7, i32 0, i32 1
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** %infile, align 4, !tbaa !28
  %call = call i32 @fread(i8* %6, i32 1, i32 4096, %struct._IO_FILE* %8)
  store i32 %call, i32* %nbytes, align 4, !tbaa !32
  %9 = load i32, i32* %nbytes, align 4, !tbaa !32
  %cmp = icmp ule i32 %9, 0
  br i1 %cmp, label %if.then, label %if.end10

if.then:                                          ; preds = %entry
  %10 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %start_of_file = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %10, i32 0, i32 3
  %11 = load i32, i32* %start_of_file, align 4, !tbaa !31
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !19
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 5
  store i32 42, i32* %msg_code, align 4, !tbaa !20
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !19
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 0
  %16 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !22
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = bitcast %struct.jpeg_decompress_struct* %17 to %struct.jpeg_common_struct*
  call void %16(%struct.jpeg_common_struct* %18)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !19
  %msg_code5 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 5
  store i32 120, i32* %msg_code5, align 4, !tbaa !20
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !19
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 1
  %23 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !33
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = bitcast %struct.jpeg_decompress_struct* %24 to %struct.jpeg_common_struct*
  call void %23(%struct.jpeg_common_struct* %25, i32 -1)
  %26 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %buffer7 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %26, i32 0, i32 2
  %27 = load i8*, i8** %buffer7, align 4, !tbaa !15
  %arrayidx = getelementptr inbounds i8, i8* %27, i32 0
  store i8 -1, i8* %arrayidx, align 1, !tbaa !34
  %28 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %buffer8 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %28, i32 0, i32 2
  %29 = load i8*, i8** %buffer8, align 4, !tbaa !15
  %arrayidx9 = getelementptr inbounds i8, i8* %29, i32 1
  store i8 -39, i8* %arrayidx9, align 1, !tbaa !34
  store i32 2, i32* %nbytes, align 4, !tbaa !32
  br label %if.end10

if.end10:                                         ; preds = %if.end, %entry
  %30 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %buffer11 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %30, i32 0, i32 2
  %31 = load i8*, i8** %buffer11, align 4, !tbaa !15
  %32 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %32, i32 0, i32 0
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub, i32 0, i32 0
  store i8* %31, i8** %next_input_byte, align 4, !tbaa !30
  %33 = load i32, i32* %nbytes, align 4, !tbaa !32
  %34 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %pub12 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %34, i32 0, i32 0
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %pub12, i32 0, i32 1
  store i32 %33, i32* %bytes_in_buffer, align 4, !tbaa !29
  %35 = load %struct.my_source_mgr*, %struct.my_source_mgr** %src, align 4, !tbaa !2
  %start_of_file13 = getelementptr inbounds %struct.my_source_mgr, %struct.my_source_mgr* %35, i32 0, i32 3
  store i32 0, i32* %start_of_file13, align 4, !tbaa !31
  %36 = bitcast i32* %nbytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #3
  %37 = bitcast %struct.my_source_mgr** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #3
  ret i32 1
}

; Function Attrs: nounwind
define internal void @skip_input_data(%struct.jpeg_decompress_struct* %cinfo, i32 %num_bytes) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %num_bytes.addr = alloca i32, align 4
  %src = alloca %struct.jpeg_source_mgr*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %num_bytes, i32* %num_bytes.addr, align 4, !tbaa !32
  %0 = bitcast %struct.jpeg_source_mgr** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src1, align 8, !tbaa !6
  store %struct.jpeg_source_mgr* %2, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %3 = load i32, i32* %num_bytes.addr, align 4, !tbaa !32
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %4 = load i32, i32* %num_bytes.addr, align 4, !tbaa !32
  %5 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %5, i32 0, i32 1
  %6 = load i32, i32* %bytes_in_buffer, align 4, !tbaa !35
  %cmp2 = icmp sgt i32 %4, %6
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %bytes_in_buffer3 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %7, i32 0, i32 1
  %8 = load i32, i32* %bytes_in_buffer3, align 4, !tbaa !35
  %9 = load i32, i32* %num_bytes.addr, align 4, !tbaa !32
  %sub = sub nsw i32 %9, %8
  store i32 %sub, i32* %num_bytes.addr, align 4, !tbaa !32
  %10 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %10, i32 0, i32 3
  %11 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !36
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %11(%struct.jpeg_decompress_struct* %12)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %13 = load i32, i32* %num_bytes.addr, align 4, !tbaa !32
  %14 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %14, i32 0, i32 0
  %15 = load i8*, i8** %next_input_byte, align 4, !tbaa !37
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 %13
  store i8* %add.ptr, i8** %next_input_byte, align 4, !tbaa !37
  %16 = load i32, i32* %num_bytes.addr, align 4, !tbaa !32
  %17 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %bytes_in_buffer4 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %17, i32 0, i32 1
  %18 = load i32, i32* %bytes_in_buffer4, align 4, !tbaa !35
  %sub5 = sub i32 %18, %16
  store i32 %sub5, i32* %bytes_in_buffer4, align 4, !tbaa !35
  br label %if.end

if.end:                                           ; preds = %while.end, %entry
  %19 = bitcast %struct.jpeg_source_mgr** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  ret void
}

declare i32 @jpeg_resync_to_restart(%struct.jpeg_decompress_struct*, i32) #2

; Function Attrs: nounwind
define internal void @term_source(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @jpeg_mem_src(%struct.jpeg_decompress_struct* %cinfo, i8* %inbuffer, i32 %insize) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %inbuffer.addr = alloca i8*, align 4
  %insize.addr = alloca i32, align 4
  %src = alloca %struct.jpeg_source_mgr*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8* %inbuffer, i8** %inbuffer.addr, align 4, !tbaa !2
  store i32 %insize, i32* %insize.addr, align 4, !tbaa !32
  %0 = bitcast %struct.jpeg_source_mgr** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8*, i8** %inbuffer.addr, align 4, !tbaa !2
  %cmp = icmp eq i8* %1, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32, i32* %insize.addr, align 4, !tbaa !32
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !19
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 5
  store i32 42, i32* %msg_code, align 4, !tbaa !20
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !19
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 0
  %7 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !22
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = bitcast %struct.jpeg_decompress_struct* %8 to %struct.jpeg_common_struct*
  call void %7(%struct.jpeg_common_struct* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 6
  %11 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src3, align 8, !tbaa !6
  %cmp4 = icmp eq %struct.jpeg_source_mgr* %11, null
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 1
  %13 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !11
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %13, i32 0, i32 0
  %14 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_decompress_struct* %15 to %struct.jpeg_common_struct*
  %call = call i8* %14(%struct.jpeg_common_struct* %16, i32 0, i32 28)
  %17 = bitcast i8* %call to %struct.jpeg_source_mgr*
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 6
  store %struct.jpeg_source_mgr* %17, %struct.jpeg_source_mgr** %src6, align 8, !tbaa !6
  br label %if.end15

if.else:                                          ; preds = %if.end
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 6
  %20 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src7, align 8, !tbaa !6
  %init_source = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %20, i32 0, i32 2
  %21 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %init_source, align 4, !tbaa !18
  %cmp8 = icmp ne void (%struct.jpeg_decompress_struct*)* %21, @init_mem_source
  br i1 %cmp8, label %if.then9, label %if.end14

if.then9:                                         ; preds = %if.else
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 0
  %23 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !19
  %msg_code11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %23, i32 0, i32 5
  store i32 23, i32* %msg_code11, align 4, !tbaa !20
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 0
  %25 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !19
  %error_exit13 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %25, i32 0, i32 0
  %26 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit13, align 4, !tbaa !22
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %28 = bitcast %struct.jpeg_decompress_struct* %27 to %struct.jpeg_common_struct*
  call void %26(%struct.jpeg_common_struct* %28)
  br label %if.end14

if.end14:                                         ; preds = %if.then9, %if.else
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.then5
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 6
  %30 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src16, align 8, !tbaa !6
  store %struct.jpeg_source_mgr* %30, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %31 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %init_source17 = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %31, i32 0, i32 2
  store void (%struct.jpeg_decompress_struct*)* @init_mem_source, void (%struct.jpeg_decompress_struct*)** %init_source17, align 4, !tbaa !18
  %32 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %fill_input_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %32, i32 0, i32 3
  store i32 (%struct.jpeg_decompress_struct*)* @fill_mem_input_buffer, i32 (%struct.jpeg_decompress_struct*)** %fill_input_buffer, align 4, !tbaa !36
  %33 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %skip_input_data = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %33, i32 0, i32 4
  store void (%struct.jpeg_decompress_struct*, i32)* @skip_input_data, void (%struct.jpeg_decompress_struct*, i32)** %skip_input_data, align 4, !tbaa !38
  %34 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %resync_to_restart = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %34, i32 0, i32 5
  store i32 (%struct.jpeg_decompress_struct*, i32)* @jpeg_resync_to_restart, i32 (%struct.jpeg_decompress_struct*, i32)** %resync_to_restart, align 4, !tbaa !39
  %35 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %term_source = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %35, i32 0, i32 6
  store void (%struct.jpeg_decompress_struct*)* @term_source, void (%struct.jpeg_decompress_struct*)** %term_source, align 4, !tbaa !40
  %36 = load i32, i32* %insize.addr, align 4, !tbaa !32
  %37 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %37, i32 0, i32 1
  store i32 %36, i32* %bytes_in_buffer, align 4, !tbaa !35
  %38 = load i8*, i8** %inbuffer.addr, align 4, !tbaa !2
  %39 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 4, !tbaa !2
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %39, i32 0, i32 0
  store i8* %38, i8** %next_input_byte, align 4, !tbaa !37
  %40 = bitcast %struct.jpeg_source_mgr** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #3
  ret void
}

; Function Attrs: nounwind
define internal void @init_mem_source(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal i32 @fill_mem_input_buffer(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 0
  %1 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !19
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %1, i32 0, i32 5
  store i32 120, i32* %msg_code, align 4, !tbaa !20
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 0
  %3 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !19
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %3, i32 0, i32 1
  %4 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !33
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = bitcast %struct.jpeg_decompress_struct* %5 to %struct.jpeg_common_struct*
  call void %4(%struct.jpeg_common_struct* %6, i32 -1)
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 6
  %8 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !6
  %next_input_byte = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %8, i32 0, i32 0
  store i8* getelementptr inbounds ([4 x i8], [4 x i8]* @fill_mem_input_buffer.mybuffer, i32 0, i32 0), i8** %next_input_byte, align 4, !tbaa !37
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 6
  %10 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src2, align 8, !tbaa !6
  %bytes_in_buffer = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %10, i32 0, i32 1
  store i32 2, i32* %bytes_in_buffer, align 4, !tbaa !35
  ret i32 1
}

declare i32 @fread(i8*, i32, i32, %struct._IO_FILE*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 24}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!7, !3, i64 4}
!12 = !{!13, !3, i64 0}
!13 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!14 = !{!"long", !4, i64 0}
!15 = !{!16, !3, i64 32}
!16 = !{!"", !17, i64 0, !3, i64 28, !3, i64 32, !8, i64 36}
!17 = !{!"jpeg_source_mgr", !3, i64 0, !14, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!18 = !{!17, !3, i64 8}
!19 = !{!7, !3, i64 0}
!20 = !{!21, !8, i64 20}
!21 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !14, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!22 = !{!21, !3, i64 0}
!23 = !{!16, !3, i64 8}
!24 = !{!16, !3, i64 12}
!25 = !{!16, !3, i64 16}
!26 = !{!16, !3, i64 20}
!27 = !{!16, !3, i64 24}
!28 = !{!16, !3, i64 28}
!29 = !{!16, !14, i64 4}
!30 = !{!16, !3, i64 0}
!31 = !{!16, !8, i64 36}
!32 = !{!14, !14, i64 0}
!33 = !{!21, !3, i64 4}
!34 = !{!4, !4, i64 0}
!35 = !{!17, !14, i64 4}
!36 = !{!17, !3, i64 12}
!37 = !{!17, !3, i64 0}
!38 = !{!17, !3, i64 16}
!39 = !{!17, !3, i64 20}
!40 = !{!17, !3, i64 24}
