; ModuleID = 'jcext.c'
source_filename = "jcext.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, void (%struct.jpeg_compress_struct*, i32)* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }

; Function Attrs: nounwind
define hidden i32 @jpeg_c_bool_param_supported(%struct.jpeg_compress_struct* %cinfo, i32 %param) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.epilog [
    i32 1745618462, label %sw.bb
    i32 -988667853, label %sw.bb
    i32 865946636, label %sw.bb
    i32 -671664256, label %sw.bb
    i32 865973855, label %sw.bb
    i32 -41675723, label %sw.bb
    i32 -517283223, label %sw.bb
    i32 1061927929, label %sw.bb
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry
  store i32 1, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: nounwind
define hidden void @jpeg_c_set_bool_param(%struct.jpeg_compress_struct* %cinfo, i32 %param, i32 %value) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  %value.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  store i32 %value, i32* %value.addr, align 4, !tbaa !7
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 1745618462, label %sw.bb
    i32 -988667853, label %sw.bb1
    i32 865946636, label %sw.bb3
    i32 -671664256, label %sw.bb5
    i32 865973855, label %sw.bb7
    i32 -41675723, label %sw.bb9
    i32 -517283223, label %sw.bb11
    i32 1061927929, label %sw.bb13
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %value.addr, align 4, !tbaa !7
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 54
  %3 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !9
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %3, i32 0, i32 5
  store i32 %1, i32* %optimize_scans, align 4, !tbaa !13
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  %4 = load i32, i32* %value.addr, align 4, !tbaa !7
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 54
  %6 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !9
  %trellis_quant = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %6, i32 0, i32 6
  store i32 %4, i32* %trellis_quant, align 8, !tbaa !16
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  %7 = load i32, i32* %value.addr, align 4, !tbaa !7
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 54
  %9 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master4, align 4, !tbaa !9
  %trellis_quant_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %9, i32 0, i32 7
  store i32 %7, i32* %trellis_quant_dc, align 4, !tbaa !17
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry
  %10 = load i32, i32* %value.addr, align 4, !tbaa !7
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 54
  %12 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master6, align 4, !tbaa !9
  %trellis_eob_opt = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %12, i32 0, i32 8
  store i32 %10, i32* %trellis_eob_opt, align 8, !tbaa !18
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry
  %13 = load i32, i32* %value.addr, align 4, !tbaa !7
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 54
  %15 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master8, align 4, !tbaa !9
  %use_lambda_weight_tbl = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %15, i32 0, i32 9
  store i32 %13, i32* %use_lambda_weight_tbl, align 4, !tbaa !19
  br label %sw.epilog

sw.bb9:                                           ; preds = %entry
  %16 = load i32, i32* %value.addr, align 4, !tbaa !7
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 54
  %18 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master10, align 4, !tbaa !9
  %use_scans_in_trellis = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %18, i32 0, i32 10
  store i32 %16, i32* %use_scans_in_trellis, align 8, !tbaa !20
  br label %sw.epilog

sw.bb11:                                          ; preds = %entry
  %19 = load i32, i32* %value.addr, align 4, !tbaa !7
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 54
  %21 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master12, align 4, !tbaa !9
  %trellis_q_opt = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %21, i32 0, i32 12
  store i32 %19, i32* %trellis_q_opt, align 8, !tbaa !21
  br label %sw.epilog

sw.bb13:                                          ; preds = %entry
  %22 = load i32, i32* %value.addr, align 4, !tbaa !7
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 54
  %24 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master14, align 4, !tbaa !9
  %overshoot_deringing = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %24, i32 0, i32 13
  store i32 %22, i32* %overshoot_deringing, align 4, !tbaa !22
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 0
  %26 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !23
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %26, i32 0, i32 5
  store i32 125, i32* %msg_code, align 4, !tbaa !24
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 8, !tbaa !23
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 0
  %29 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !27
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_compress_struct* %30 to %struct.jpeg_common_struct*
  call void %29(%struct.jpeg_common_struct* %31)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb13, %sw.bb11, %sw.bb9, %sw.bb7, %sw.bb5, %sw.bb3, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jpeg_c_get_bool_param(%struct.jpeg_compress_struct* %cinfo, i32 %param) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 1745618462, label %sw.bb
    i32 -988667853, label %sw.bb1
    i32 865946636, label %sw.bb3
    i32 -671664256, label %sw.bb5
    i32 865973855, label %sw.bb7
    i32 -41675723, label %sw.bb9
    i32 -517283223, label %sw.bb11
    i32 1061927929, label %sw.bb13
  ]

sw.bb:                                            ; preds = %entry
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !9
  %optimize_scans = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %2, i32 0, i32 5
  %3 = load i32, i32* %optimize_scans, align 4, !tbaa !13
  store i32 %3, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 54
  %5 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !9
  %trellis_quant = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %5, i32 0, i32 6
  %6 = load i32, i32* %trellis_quant, align 8, !tbaa !16
  store i32 %6, i32* %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 54
  %8 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master4, align 4, !tbaa !9
  %trellis_quant_dc = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %8, i32 0, i32 7
  %9 = load i32, i32* %trellis_quant_dc, align 4, !tbaa !17
  store i32 %9, i32* %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 54
  %11 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master6, align 4, !tbaa !9
  %trellis_eob_opt = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %11, i32 0, i32 8
  %12 = load i32, i32* %trellis_eob_opt, align 8, !tbaa !18
  store i32 %12, i32* %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 54
  %14 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master8, align 4, !tbaa !9
  %use_lambda_weight_tbl = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %14, i32 0, i32 9
  %15 = load i32, i32* %use_lambda_weight_tbl, align 4, !tbaa !19
  store i32 %15, i32* %retval, align 4
  br label %return

sw.bb9:                                           ; preds = %entry
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 54
  %17 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master10, align 4, !tbaa !9
  %use_scans_in_trellis = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %17, i32 0, i32 10
  %18 = load i32, i32* %use_scans_in_trellis, align 8, !tbaa !20
  store i32 %18, i32* %retval, align 4
  br label %return

sw.bb11:                                          ; preds = %entry
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 54
  %20 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master12, align 4, !tbaa !9
  %trellis_q_opt = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %20, i32 0, i32 12
  %21 = load i32, i32* %trellis_q_opt, align 8, !tbaa !21
  store i32 %21, i32* %retval, align 4
  br label %return

sw.bb13:                                          ; preds = %entry
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master14 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 54
  %23 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master14, align 4, !tbaa !9
  %overshoot_deringing = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %23, i32 0, i32 13
  %24 = load i32, i32* %overshoot_deringing, align 4, !tbaa !22
  store i32 %24, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 0
  %26 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !23
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %26, i32 0, i32 5
  store i32 125, i32* %msg_code, align 4, !tbaa !24
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 8, !tbaa !23
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 0
  %29 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !27
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_compress_struct* %30 to %struct.jpeg_common_struct*
  call void %29(%struct.jpeg_common_struct* %31)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb13, %sw.bb11, %sw.bb9, %sw.bb7, %sw.bb5, %sw.bb3, %sw.bb1, %sw.bb
  %32 = load i32, i32* %retval, align 4
  ret i32 %32
}

; Function Attrs: nounwind
define hidden i32 @jpeg_c_float_param_supported(%struct.jpeg_compress_struct* %cinfo, i32 %param) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.epilog [
    i32 1533126041, label %sw.bb
    i32 -1178882557, label %sw.bb
    i32 326587475, label %sw.bb
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry
  store i32 1, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: nounwind
define hidden void @jpeg_c_set_float_param(%struct.jpeg_compress_struct* %cinfo, i32 %param, float %value) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  store float %value, float* %value.addr, align 4, !tbaa !28
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 1533126041, label %sw.bb
    i32 -1178882557, label %sw.bb1
    i32 326587475, label %sw.bb3
  ]

sw.bb:                                            ; preds = %entry
  %1 = load float, float* %value.addr, align 4, !tbaa !28
  %2 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %2, i32 0, i32 54
  %3 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !9
  %lambda_log_scale1 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %3, i32 0, i32 27
  store float %1, float* %lambda_log_scale1, align 4, !tbaa !29
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  %4 = load float, float* %value.addr, align 4, !tbaa !28
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 54
  %6 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !9
  %lambda_log_scale2 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %6, i32 0, i32 28
  store float %4, float* %lambda_log_scale2, align 8, !tbaa !30
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  %7 = load float, float* %value.addr, align 4, !tbaa !28
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 54
  %9 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master4, align 4, !tbaa !9
  %trellis_delta_dc_weight = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %9, i32 0, i32 29
  store float %7, float* %trellis_delta_dc_weight, align 4, !tbaa !31
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !23
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 5
  store i32 125, i32* %msg_code, align 4, !tbaa !24
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !23
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 0
  %14 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !27
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_compress_struct* %15 to %struct.jpeg_common_struct*
  call void %14(%struct.jpeg_common_struct* %16)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb3, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define hidden float @jpeg_c_get_float_param(%struct.jpeg_compress_struct* %cinfo, i32 %param) #0 {
entry:
  %retval = alloca float, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 1533126041, label %sw.bb
    i32 -1178882557, label %sw.bb1
    i32 326587475, label %sw.bb3
  ]

sw.bb:                                            ; preds = %entry
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !9
  %lambda_log_scale1 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %2, i32 0, i32 27
  %3 = load float, float* %lambda_log_scale1, align 4, !tbaa !29
  store float %3, float* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 54
  %5 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !9
  %lambda_log_scale2 = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %5, i32 0, i32 28
  %6 = load float, float* %lambda_log_scale2, align 8, !tbaa !30
  store float %6, float* %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 54
  %8 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master4, align 4, !tbaa !9
  %trellis_delta_dc_weight = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %8, i32 0, i32 29
  %9 = load float, float* %trellis_delta_dc_weight, align 4, !tbaa !31
  store float %9, float* %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !23
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 5
  store i32 125, i32* %msg_code, align 4, !tbaa !24
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !23
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 0
  %14 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !27
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_compress_struct* %15 to %struct.jpeg_common_struct*
  call void %14(%struct.jpeg_common_struct* %16)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store float -1.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb3, %sw.bb1, %sw.bb
  %17 = load float, float* %retval, align 4
  ret float %17
}

; Function Attrs: nounwind
define hidden i32 @jpeg_c_int_param_supported(%struct.jpeg_compress_struct* %cinfo, i32 %param) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.epilog [
    i32 -376338907, label %sw.bb
    i32 1873801511, label %sw.bb
    i32 -1237401799, label %sw.bb
    i32 1145645745, label %sw.bb
    i32 199732540, label %sw.bb
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry, %entry, %entry
  store i32 1, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: nounwind
define hidden void @jpeg_c_set_int_param(%struct.jpeg_compress_struct* %cinfo, i32 %param, i32 %value) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  %value.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  store i32 %value, i32* %value.addr, align 4, !tbaa !7
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default12 [
    i32 -376338907, label %sw.bb
    i32 1873801511, label %sw.bb3
    i32 -1237401799, label %sw.bb5
    i32 1145645745, label %sw.bb7
    i32 199732540, label %sw.bb10
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %value.addr, align 4, !tbaa !7
  switch i32 %1, label %sw.default [
    i32 1560820397, label %sw.bb1
    i32 720002228, label %sw.bb1
  ]

sw.bb1:                                           ; preds = %sw.bb, %sw.bb
  %2 = load i32, i32* %value.addr, align 4, !tbaa !7
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 54
  %4 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !9
  %compress_profile = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %4, i32 0, i32 16
  store i32 %2, i32* %compress_profile, align 8, !tbaa !32
  br label %sw.epilog

sw.default:                                       ; preds = %sw.bb
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !23
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 5
  store i32 126, i32* %msg_code, align 4, !tbaa !24
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !23
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 0
  %9 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !27
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = bitcast %struct.jpeg_compress_struct* %10 to %struct.jpeg_common_struct*
  call void %9(%struct.jpeg_common_struct* %11)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb1
  br label %sw.epilog17

sw.bb3:                                           ; preds = %entry
  %12 = load i32, i32* %value.addr, align 4, !tbaa !7
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 54
  %14 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master4, align 4, !tbaa !9
  %trellis_freq_split = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %14, i32 0, i32 19
  store i32 %12, i32* %trellis_freq_split, align 4, !tbaa !33
  br label %sw.epilog17

sw.bb5:                                           ; preds = %entry
  %15 = load i32, i32* %value.addr, align 4, !tbaa !7
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 54
  %17 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master6, align 4, !tbaa !9
  %trellis_num_loops = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %17, i32 0, i32 20
  store i32 %15, i32* %trellis_num_loops, align 8, !tbaa !34
  br label %sw.epilog17

sw.bb7:                                           ; preds = %entry
  %18 = load i32, i32* %value.addr, align 4, !tbaa !7
  %cmp = icmp sge i32 %18, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %sw.bb7
  %19 = load i32, i32* %value.addr, align 4, !tbaa !7
  %cmp8 = icmp sle i32 %19, 8
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %20 = load i32, i32* %value.addr, align 4, !tbaa !7
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 54
  %22 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master9, align 4, !tbaa !9
  %quant_tbl_master_idx = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %22, i32 0, i32 18
  store i32 %20, i32* %quant_tbl_master_idx, align 8, !tbaa !35
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %sw.bb7
  br label %sw.epilog17

sw.bb10:                                          ; preds = %entry
  %23 = load i32, i32* %value.addr, align 4, !tbaa !7
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master11 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 54
  %25 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master11, align 4, !tbaa !9
  %dc_scan_opt_mode = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %25, i32 0, i32 17
  store i32 %23, i32* %dc_scan_opt_mode, align 4, !tbaa !36
  br label %sw.epilog17

sw.default12:                                     ; preds = %entry
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 0
  %27 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 8, !tbaa !23
  %msg_code14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %27, i32 0, i32 5
  store i32 125, i32* %msg_code14, align 4, !tbaa !24
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %28, i32 0, i32 0
  %29 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 8, !tbaa !23
  %error_exit16 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %29, i32 0, i32 0
  %30 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit16, align 4, !tbaa !27
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %32 = bitcast %struct.jpeg_compress_struct* %31 to %struct.jpeg_common_struct*
  call void %30(%struct.jpeg_common_struct* %32)
  br label %sw.epilog17

sw.epilog17:                                      ; preds = %sw.default12, %sw.bb10, %if.end, %sw.bb5, %sw.bb3, %sw.epilog
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jpeg_c_get_int_param(%struct.jpeg_compress_struct* %cinfo, i32 %param) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %param.addr = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %param, i32* %param.addr, align 4, !tbaa !6
  %0 = load i32, i32* %param.addr, align 4, !tbaa !6
  switch i32 %0, label %sw.default [
    i32 -376338907, label %sw.bb
    i32 1873801511, label %sw.bb1
    i32 -1237401799, label %sw.bb3
    i32 1145645745, label %sw.bb5
    i32 199732540, label %sw.bb7
  ]

sw.bb:                                            ; preds = %entry
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 54
  %2 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !9
  %compress_profile = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %2, i32 0, i32 16
  %3 = load i32, i32* %compress_profile, align 8, !tbaa !32
  store i32 %3, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 54
  %5 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master2, align 4, !tbaa !9
  %trellis_freq_split = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %5, i32 0, i32 19
  %6 = load i32, i32* %trellis_freq_split, align 4, !tbaa !33
  store i32 %6, i32* %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 54
  %8 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master4, align 4, !tbaa !9
  %trellis_num_loops = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %8, i32 0, i32 20
  %9 = load i32, i32* %trellis_num_loops, align 8, !tbaa !34
  store i32 %9, i32* %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 54
  %11 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master6, align 4, !tbaa !9
  %quant_tbl_master_idx = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %11, i32 0, i32 18
  %12 = load i32, i32* %quant_tbl_master_idx, align 8, !tbaa !35
  store i32 %12, i32* %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 54
  %14 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master8, align 4, !tbaa !9
  %dc_scan_opt_mode = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %14, i32 0, i32 17
  %15 = load i32, i32* %dc_scan_opt_mode, align 4, !tbaa !36
  store i32 %15, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !23
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 5
  store i32 125, i32* %msg_code, align 4, !tbaa !24
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !23
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 0
  %20 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !27
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %22 = bitcast %struct.jpeg_compress_struct* %21 to %struct.jpeg_common_struct*
  call void %20(%struct.jpeg_common_struct* %22)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb7, %sw.bb5, %sw.bb3, %sw.bb1, %sw.bb
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!4, !4, i64 0}
!7 = !{!8, !8, i64 0}
!8 = !{!"int", !4, i64 0}
!9 = !{!10, !3, i64 332}
!10 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !11, i64 48, !8, i64 56, !8, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !8, i64 168, !3, i64 172, !8, i64 176, !8, i64 180, !8, i64 184, !8, i64 188, !8, i64 192, !4, i64 196, !8, i64 200, !8, i64 204, !8, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !12, i64 216, !12, i64 218, !8, i64 220, !8, i64 224, !8, i64 228, !8, i64 232, !8, i64 236, !8, i64 240, !8, i64 244, !4, i64 248, !8, i64 264, !8, i64 268, !8, i64 272, !4, i64 276, !8, i64 316, !8, i64 320, !8, i64 324, !8, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !8, i64 372}
!11 = !{!"double", !4, i64 0}
!12 = !{!"short", !4, i64 0}
!13 = !{!14, !8, i64 20}
!14 = !{!"jpeg_comp_master", !3, i64 0, !3, i64 4, !3, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !4, i64 56, !4, i64 2104, !8, i64 4152, !8, i64 4156, !8, i64 4160, !8, i64 4164, !8, i64 4168, !8, i64 4172, !8, i64 4176, !8, i64 4180, !8, i64 4184, !8, i64 4188, !8, i64 4192, !15, i64 4196, !15, i64 4200, !15, i64 4204}
!15 = !{!"float", !4, i64 0}
!16 = !{!14, !8, i64 24}
!17 = !{!14, !8, i64 28}
!18 = !{!14, !8, i64 32}
!19 = !{!14, !8, i64 36}
!20 = !{!14, !8, i64 40}
!21 = !{!14, !8, i64 48}
!22 = !{!14, !8, i64 52}
!23 = !{!10, !3, i64 0}
!24 = !{!25, !8, i64 20}
!25 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !26, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!26 = !{!"long", !4, i64 0}
!27 = !{!25, !3, i64 0}
!28 = !{!15, !15, i64 0}
!29 = !{!14, !15, i64 4196}
!30 = !{!14, !15, i64 4200}
!31 = !{!14, !15, i64 4204}
!32 = !{!14, !8, i64 4152}
!33 = !{!14, !8, i64 4164}
!34 = !{!14, !8, i64 4168}
!35 = !{!14, !8, i64 4160}
!36 = !{!14, !8, i64 4156}
