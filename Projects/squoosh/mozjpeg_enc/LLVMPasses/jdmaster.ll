; ModuleID = 'jdmaster.c'
source_filename = "jdmaster.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.my_decomp_master = type { %struct.jpeg_decomp_master, i32, i32, %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer* }

@rgb_pixelsize = internal constant [17 x i32] [i32 -1, i32 -1, i32 3, i32 -1, i32 -1, i32 -1, i32 3, i32 4, i32 3, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 -1], align 16

; Function Attrs: nounwind
define hidden void @jpeg_calc_output_dimensions(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %ssize = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 5
  %3 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp ne i32 %3, 202
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 0
  %5 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %5, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !12
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 5
  %7 = load i32, i32* %global_state1, align 4, !tbaa !6
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %7, i32* %arrayidx, align 4, !tbaa !15
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !16
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_decompress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_core_output_dimensions(%struct.jpeg_decompress_struct* %15)
  store i32 0, i32* %ci, align 4, !tbaa !17
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 44
  %17 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !18
  store %struct.jpeg_component_info* %17, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %18 = load i32, i32* %ci, align 4, !tbaa !17
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 9
  %20 = load i32, i32* %num_components, align 4, !tbaa !19
  %cmp4 = icmp slt i32 %18, %20
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %21 = bitcast i32* %ssize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 63
  %23 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !20
  store i32 %23, i32* %ssize, align 4, !tbaa !17
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body
  %24 = load i32, i32* %ssize, align 4, !tbaa !17
  %cmp5 = icmp slt i32 %24, 8
  br i1 %cmp5, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %while.cond
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 61
  %26 = load i32, i32* %max_h_samp_factor, align 4, !tbaa !21
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 63
  %28 = load i32, i32* %min_DCT_scaled_size6, align 4, !tbaa !20
  %mul = mul nsw i32 %26, %28
  %29 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %29, i32 0, i32 2
  %30 = load i32, i32* %h_samp_factor, align 4, !tbaa !22
  %31 = load i32, i32* %ssize, align 4, !tbaa !17
  %mul7 = mul nsw i32 %30, %31
  %mul8 = mul nsw i32 %mul7, 2
  %rem = srem i32 %mul, %mul8
  %cmp9 = icmp eq i32 %rem, 0
  br i1 %cmp9, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 62
  %33 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !24
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 63
  %35 = load i32, i32* %min_DCT_scaled_size10, align 4, !tbaa !20
  %mul11 = mul nsw i32 %33, %35
  %36 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %36, i32 0, i32 3
  %37 = load i32, i32* %v_samp_factor, align 4, !tbaa !25
  %38 = load i32, i32* %ssize, align 4, !tbaa !17
  %mul12 = mul nsw i32 %37, %38
  %mul13 = mul nsw i32 %mul12, 2
  %rem14 = srem i32 %mul11, %mul13
  %cmp15 = icmp eq i32 %rem14, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %while.cond
  %39 = phi i1 [ false, %land.lhs.true ], [ false, %while.cond ], [ %cmp15, %land.rhs ]
  br i1 %39, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %40 = load i32, i32* %ssize, align 4, !tbaa !17
  %mul16 = mul nsw i32 %40, 2
  store i32 %mul16, i32* %ssize, align 4, !tbaa !17
  br label %while.cond

while.end:                                        ; preds = %land.end
  %41 = load i32, i32* %ssize, align 4, !tbaa !17
  %42 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %42, i32 0, i32 9
  store i32 %41, i32* %DCT_scaled_size, align 4, !tbaa !26
  %43 = bitcast i32* %ssize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #4
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %44 = load i32, i32* %ci, align 4, !tbaa !17
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !17
  %45 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %45, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %ci, align 4, !tbaa !17
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 44
  %47 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info17, align 8, !tbaa !18
  store %struct.jpeg_component_info* %47, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc35, %for.end
  %48 = load i32, i32* %ci, align 4, !tbaa !17
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 9
  %50 = load i32, i32* %num_components19, align 4, !tbaa !19
  %cmp20 = icmp slt i32 %48, %50
  br i1 %cmp20, label %for.body21, label %for.end38

for.body21:                                       ; preds = %for.cond18
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 7
  %52 = load i32, i32* %image_width, align 4, !tbaa !27
  %53 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor22 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %53, i32 0, i32 2
  %54 = load i32, i32* %h_samp_factor22, align 4, !tbaa !22
  %55 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size23 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %55, i32 0, i32 9
  %56 = load i32, i32* %DCT_scaled_size23, align 4, !tbaa !26
  %mul24 = mul nsw i32 %54, %56
  %mul25 = mul nsw i32 %52, %mul24
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor26 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 61
  %58 = load i32, i32* %max_h_samp_factor26, align 4, !tbaa !21
  %mul27 = mul nsw i32 %58, 8
  %call = call i32 @jdiv_round_up(i32 %mul25, i32 %mul27)
  %59 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %59, i32 0, i32 10
  store i32 %call, i32* %downsampled_width, align 4, !tbaa !28
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %60, i32 0, i32 8
  %61 = load i32, i32* %image_height, align 8, !tbaa !29
  %62 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor28 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %62, i32 0, i32 3
  %63 = load i32, i32* %v_samp_factor28, align 4, !tbaa !25
  %64 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size29 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %64, i32 0, i32 9
  %65 = load i32, i32* %DCT_scaled_size29, align 4, !tbaa !26
  %mul30 = mul nsw i32 %63, %65
  %mul31 = mul nsw i32 %61, %mul30
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor32 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %66, i32 0, i32 62
  %67 = load i32, i32* %max_v_samp_factor32, align 8, !tbaa !24
  %mul33 = mul nsw i32 %67, 8
  %call34 = call i32 @jdiv_round_up(i32 %mul31, i32 %mul33)
  %68 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %68, i32 0, i32 11
  store i32 %call34, i32* %downsampled_height, align 4, !tbaa !30
  br label %for.inc35

for.inc35:                                        ; preds = %for.body21
  %69 = load i32, i32* %ci, align 4, !tbaa !17
  %inc36 = add nsw i32 %69, 1
  store i32 %inc36, i32* %ci, align 4, !tbaa !17
  %70 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr37 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %70, i32 1
  store %struct.jpeg_component_info* %incdec.ptr37, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond18

for.end38:                                        ; preds = %for.cond18
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 11
  %72 = load i32, i32* %out_color_space, align 4, !tbaa !31
  switch i32 %72, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb39
    i32 6, label %sw.bb39
    i32 7, label %sw.bb39
    i32 8, label %sw.bb39
    i32 9, label %sw.bb39
    i32 10, label %sw.bb39
    i32 11, label %sw.bb39
    i32 12, label %sw.bb39
    i32 13, label %sw.bb39
    i32 14, label %sw.bb39
    i32 15, label %sw.bb39
    i32 3, label %sw.bb43
    i32 16, label %sw.bb43
    i32 4, label %sw.bb45
    i32 5, label %sw.bb45
  ]

sw.bb:                                            ; preds = %for.end38
  %73 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %73, i32 0, i32 29
  store i32 1, i32* %out_color_components, align 8, !tbaa !32
  br label %sw.epilog

sw.bb39:                                          ; preds = %for.end38, %for.end38, %for.end38, %for.end38, %for.end38, %for.end38, %for.end38, %for.end38, %for.end38, %for.end38, %for.end38
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %74, i32 0, i32 11
  %75 = load i32, i32* %out_color_space40, align 4, !tbaa !31
  %arrayidx41 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_pixelsize, i32 0, i32 %75
  %76 = load i32, i32* %arrayidx41, align 4, !tbaa !17
  %77 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components42 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %77, i32 0, i32 29
  store i32 %76, i32* %out_color_components42, align 8, !tbaa !32
  br label %sw.epilog

sw.bb43:                                          ; preds = %for.end38, %for.end38
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components44 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %78, i32 0, i32 29
  store i32 3, i32* %out_color_components44, align 8, !tbaa !32
  br label %sw.epilog

sw.bb45:                                          ; preds = %for.end38, %for.end38
  %79 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %79, i32 0, i32 29
  store i32 4, i32* %out_color_components46, align 8, !tbaa !32
  br label %sw.epilog

sw.default:                                       ; preds = %for.end38
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components47 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %80, i32 0, i32 9
  %81 = load i32, i32* %num_components47, align 4, !tbaa !19
  %82 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %82, i32 0, i32 29
  store i32 %81, i32* %out_color_components48, align 8, !tbaa !32
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb45, %sw.bb43, %sw.bb39, %sw.bb
  %83 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %83, i32 0, i32 20
  %84 = load i32, i32* %quantize_colors, align 4, !tbaa !33
  %tobool = icmp ne i32 %84, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.epilog
  br label %cond.end

cond.false:                                       ; preds = %sw.epilog
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components49 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 29
  %86 = load i32, i32* %out_color_components49, align 8, !tbaa !32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %86, %cond.false ]
  %87 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %87, i32 0, i32 30
  store i32 %cond, i32* %output_components, align 4, !tbaa !34
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call50 = call i32 @use_merged_upsample(%struct.jpeg_decompress_struct* %88)
  %tobool51 = icmp ne i32 %call50, 0
  br i1 %tobool51, label %if.then52, label %if.else

if.then52:                                        ; preds = %cond.end
  %89 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor53 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %89, i32 0, i32 62
  %90 = load i32, i32* %max_v_samp_factor53, align 8, !tbaa !24
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %rec_outbuf_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %91, i32 0, i32 31
  store i32 %90, i32* %rec_outbuf_height, align 8, !tbaa !35
  br label %if.end55

if.else:                                          ; preds = %cond.end
  %92 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %rec_outbuf_height54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %92, i32 0, i32 31
  store i32 1, i32* %rec_outbuf_height54, align 8, !tbaa !35
  br label %if.end55

if.end55:                                         ; preds = %if.else, %if.then52
  %93 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #4
  %94 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @jpeg_core_output_dimensions(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 12
  %3 = load i32, i32* %scale_num, align 8, !tbaa !36
  %mul = mul i32 %3, 8
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 13
  %5 = load i32, i32* %scale_denom, align 4, !tbaa !37
  %cmp = icmp ule i32 %mul, %5
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 7
  %7 = load i32, i32* %image_width, align 4, !tbaa !27
  %call = call i32 @jdiv_round_up(i32 %7, i32 8)
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 27
  store i32 %call, i32* %output_width, align 8, !tbaa !38
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 8
  %10 = load i32, i32* %image_height, align 8, !tbaa !29
  %call1 = call i32 @jdiv_round_up(i32 %10, i32 8)
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 28
  store i32 %call1, i32* %output_height, align 4, !tbaa !39
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 63
  store i32 1, i32* %min_DCT_scaled_size, align 4, !tbaa !20
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 63
  store i32 1, i32* %min_DCT_scaled_size2, align 4, !tbaa !20
  br label %if.end264

if.else:                                          ; preds = %entry
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 12
  %15 = load i32, i32* %scale_num3, align 8, !tbaa !36
  %mul4 = mul i32 %15, 8
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 13
  %17 = load i32, i32* %scale_denom5, align 4, !tbaa !37
  %mul6 = mul i32 %17, 2
  %cmp7 = icmp ule i32 %mul4, %mul6
  br i1 %cmp7, label %if.then8, label %if.else19

if.then8:                                         ; preds = %if.else
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 7
  %19 = load i32, i32* %image_width9, align 4, !tbaa !27
  %mul10 = mul nsw i32 %19, 2
  %call11 = call i32 @jdiv_round_up(i32 %mul10, i32 8)
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 27
  store i32 %call11, i32* %output_width12, align 8, !tbaa !38
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 8
  %22 = load i32, i32* %image_height13, align 8, !tbaa !29
  %mul14 = mul nsw i32 %22, 2
  %call15 = call i32 @jdiv_round_up(i32 %mul14, i32 8)
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 28
  store i32 %call15, i32* %output_height16, align 4, !tbaa !39
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 63
  store i32 2, i32* %min_DCT_scaled_size17, align 4, !tbaa !20
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 63
  store i32 2, i32* %min_DCT_scaled_size18, align 4, !tbaa !20
  br label %if.end263

if.else19:                                        ; preds = %if.else
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 12
  %27 = load i32, i32* %scale_num20, align 8, !tbaa !36
  %mul21 = mul i32 %27, 8
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 13
  %29 = load i32, i32* %scale_denom22, align 4, !tbaa !37
  %mul23 = mul i32 %29, 3
  %cmp24 = icmp ule i32 %mul21, %mul23
  br i1 %cmp24, label %if.then25, label %if.else36

if.then25:                                        ; preds = %if.else19
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width26 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 7
  %31 = load i32, i32* %image_width26, align 4, !tbaa !27
  %mul27 = mul nsw i32 %31, 3
  %call28 = call i32 @jdiv_round_up(i32 %mul27, i32 8)
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 27
  store i32 %call28, i32* %output_width29, align 8, !tbaa !38
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height30 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 8
  %34 = load i32, i32* %image_height30, align 8, !tbaa !29
  %mul31 = mul nsw i32 %34, 3
  %call32 = call i32 @jdiv_round_up(i32 %mul31, i32 8)
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height33 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 28
  store i32 %call32, i32* %output_height33, align 4, !tbaa !39
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size34 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 63
  store i32 3, i32* %min_DCT_scaled_size34, align 4, !tbaa !20
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size35 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 63
  store i32 3, i32* %min_DCT_scaled_size35, align 4, !tbaa !20
  br label %if.end262

if.else36:                                        ; preds = %if.else19
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num37 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 12
  %39 = load i32, i32* %scale_num37, align 8, !tbaa !36
  %mul38 = mul i32 %39, 8
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom39 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 13
  %41 = load i32, i32* %scale_denom39, align 4, !tbaa !37
  %mul40 = mul i32 %41, 4
  %cmp41 = icmp ule i32 %mul38, %mul40
  br i1 %cmp41, label %if.then42, label %if.else53

if.then42:                                        ; preds = %if.else36
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width43 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 7
  %43 = load i32, i32* %image_width43, align 4, !tbaa !27
  %mul44 = mul nsw i32 %43, 4
  %call45 = call i32 @jdiv_round_up(i32 %mul44, i32 8)
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 27
  store i32 %call45, i32* %output_width46, align 8, !tbaa !38
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height47 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 8
  %46 = load i32, i32* %image_height47, align 8, !tbaa !29
  %mul48 = mul nsw i32 %46, 4
  %call49 = call i32 @jdiv_round_up(i32 %mul48, i32 8)
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height50 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 28
  store i32 %call49, i32* %output_height50, align 4, !tbaa !39
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size51 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 63
  store i32 4, i32* %min_DCT_scaled_size51, align 4, !tbaa !20
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size52 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 63
  store i32 4, i32* %min_DCT_scaled_size52, align 4, !tbaa !20
  br label %if.end261

if.else53:                                        ; preds = %if.else36
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 12
  %51 = load i32, i32* %scale_num54, align 8, !tbaa !36
  %mul55 = mul i32 %51, 8
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom56 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 13
  %53 = load i32, i32* %scale_denom56, align 4, !tbaa !37
  %mul57 = mul i32 %53, 5
  %cmp58 = icmp ule i32 %mul55, %mul57
  br i1 %cmp58, label %if.then59, label %if.else70

if.then59:                                        ; preds = %if.else53
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width60 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 7
  %55 = load i32, i32* %image_width60, align 4, !tbaa !27
  %mul61 = mul nsw i32 %55, 5
  %call62 = call i32 @jdiv_round_up(i32 %mul61, i32 8)
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width63 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 27
  store i32 %call62, i32* %output_width63, align 8, !tbaa !38
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height64 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 8
  %58 = load i32, i32* %image_height64, align 8, !tbaa !29
  %mul65 = mul nsw i32 %58, 5
  %call66 = call i32 @jdiv_round_up(i32 %mul65, i32 8)
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 28
  store i32 %call66, i32* %output_height67, align 4, !tbaa !39
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size68 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %60, i32 0, i32 63
  store i32 5, i32* %min_DCT_scaled_size68, align 4, !tbaa !20
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size69 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 63
  store i32 5, i32* %min_DCT_scaled_size69, align 4, !tbaa !20
  br label %if.end260

if.else70:                                        ; preds = %if.else53
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %62, i32 0, i32 12
  %63 = load i32, i32* %scale_num71, align 8, !tbaa !36
  %mul72 = mul i32 %63, 8
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom73 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 13
  %65 = load i32, i32* %scale_denom73, align 4, !tbaa !37
  %mul74 = mul i32 %65, 6
  %cmp75 = icmp ule i32 %mul72, %mul74
  br i1 %cmp75, label %if.then76, label %if.else87

if.then76:                                        ; preds = %if.else70
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width77 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %66, i32 0, i32 7
  %67 = load i32, i32* %image_width77, align 4, !tbaa !27
  %mul78 = mul nsw i32 %67, 6
  %call79 = call i32 @jdiv_round_up(i32 %mul78, i32 8)
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width80 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %68, i32 0, i32 27
  store i32 %call79, i32* %output_width80, align 8, !tbaa !38
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height81 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %69, i32 0, i32 8
  %70 = load i32, i32* %image_height81, align 8, !tbaa !29
  %mul82 = mul nsw i32 %70, 6
  %call83 = call i32 @jdiv_round_up(i32 %mul82, i32 8)
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height84 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 28
  store i32 %call83, i32* %output_height84, align 4, !tbaa !39
  %72 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size85 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %72, i32 0, i32 63
  store i32 6, i32* %min_DCT_scaled_size85, align 4, !tbaa !20
  %73 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size86 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %73, i32 0, i32 63
  store i32 6, i32* %min_DCT_scaled_size86, align 4, !tbaa !20
  br label %if.end259

if.else87:                                        ; preds = %if.else70
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num88 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %74, i32 0, i32 12
  %75 = load i32, i32* %scale_num88, align 8, !tbaa !36
  %mul89 = mul i32 %75, 8
  %76 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom90 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %76, i32 0, i32 13
  %77 = load i32, i32* %scale_denom90, align 4, !tbaa !37
  %mul91 = mul i32 %77, 7
  %cmp92 = icmp ule i32 %mul89, %mul91
  br i1 %cmp92, label %if.then93, label %if.else104

if.then93:                                        ; preds = %if.else87
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width94 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %78, i32 0, i32 7
  %79 = load i32, i32* %image_width94, align 4, !tbaa !27
  %mul95 = mul nsw i32 %79, 7
  %call96 = call i32 @jdiv_round_up(i32 %mul95, i32 8)
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width97 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %80, i32 0, i32 27
  store i32 %call96, i32* %output_width97, align 8, !tbaa !38
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height98 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 8
  %82 = load i32, i32* %image_height98, align 8, !tbaa !29
  %mul99 = mul nsw i32 %82, 7
  %call100 = call i32 @jdiv_round_up(i32 %mul99, i32 8)
  %83 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height101 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %83, i32 0, i32 28
  store i32 %call100, i32* %output_height101, align 4, !tbaa !39
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size102 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %84, i32 0, i32 63
  store i32 7, i32* %min_DCT_scaled_size102, align 4, !tbaa !20
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size103 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 63
  store i32 7, i32* %min_DCT_scaled_size103, align 4, !tbaa !20
  br label %if.end258

if.else104:                                       ; preds = %if.else87
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num105 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 12
  %87 = load i32, i32* %scale_num105, align 8, !tbaa !36
  %mul106 = mul i32 %87, 8
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom107 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %88, i32 0, i32 13
  %89 = load i32, i32* %scale_denom107, align 4, !tbaa !37
  %mul108 = mul i32 %89, 8
  %cmp109 = icmp ule i32 %mul106, %mul108
  br i1 %cmp109, label %if.then110, label %if.else121

if.then110:                                       ; preds = %if.else104
  %90 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width111 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %90, i32 0, i32 7
  %91 = load i32, i32* %image_width111, align 4, !tbaa !27
  %mul112 = mul nsw i32 %91, 8
  %call113 = call i32 @jdiv_round_up(i32 %mul112, i32 8)
  %92 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width114 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %92, i32 0, i32 27
  store i32 %call113, i32* %output_width114, align 8, !tbaa !38
  %93 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height115 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %93, i32 0, i32 8
  %94 = load i32, i32* %image_height115, align 8, !tbaa !29
  %mul116 = mul nsw i32 %94, 8
  %call117 = call i32 @jdiv_round_up(i32 %mul116, i32 8)
  %95 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height118 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %95, i32 0, i32 28
  store i32 %call117, i32* %output_height118, align 4, !tbaa !39
  %96 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size119 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %96, i32 0, i32 63
  store i32 8, i32* %min_DCT_scaled_size119, align 4, !tbaa !20
  %97 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size120 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %97, i32 0, i32 63
  store i32 8, i32* %min_DCT_scaled_size120, align 4, !tbaa !20
  br label %if.end257

if.else121:                                       ; preds = %if.else104
  %98 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num122 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %98, i32 0, i32 12
  %99 = load i32, i32* %scale_num122, align 8, !tbaa !36
  %mul123 = mul i32 %99, 8
  %100 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom124 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %100, i32 0, i32 13
  %101 = load i32, i32* %scale_denom124, align 4, !tbaa !37
  %mul125 = mul i32 %101, 9
  %cmp126 = icmp ule i32 %mul123, %mul125
  br i1 %cmp126, label %if.then127, label %if.else138

if.then127:                                       ; preds = %if.else121
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width128 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 7
  %103 = load i32, i32* %image_width128, align 4, !tbaa !27
  %mul129 = mul nsw i32 %103, 9
  %call130 = call i32 @jdiv_round_up(i32 %mul129, i32 8)
  %104 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width131 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %104, i32 0, i32 27
  store i32 %call130, i32* %output_width131, align 8, !tbaa !38
  %105 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height132 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %105, i32 0, i32 8
  %106 = load i32, i32* %image_height132, align 8, !tbaa !29
  %mul133 = mul nsw i32 %106, 9
  %call134 = call i32 @jdiv_round_up(i32 %mul133, i32 8)
  %107 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height135 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %107, i32 0, i32 28
  store i32 %call134, i32* %output_height135, align 4, !tbaa !39
  %108 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size136 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %108, i32 0, i32 63
  store i32 9, i32* %min_DCT_scaled_size136, align 4, !tbaa !20
  %109 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size137 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %109, i32 0, i32 63
  store i32 9, i32* %min_DCT_scaled_size137, align 4, !tbaa !20
  br label %if.end256

if.else138:                                       ; preds = %if.else121
  %110 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num139 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %110, i32 0, i32 12
  %111 = load i32, i32* %scale_num139, align 8, !tbaa !36
  %mul140 = mul i32 %111, 8
  %112 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom141 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %112, i32 0, i32 13
  %113 = load i32, i32* %scale_denom141, align 4, !tbaa !37
  %mul142 = mul i32 %113, 10
  %cmp143 = icmp ule i32 %mul140, %mul142
  br i1 %cmp143, label %if.then144, label %if.else155

if.then144:                                       ; preds = %if.else138
  %114 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width145 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %114, i32 0, i32 7
  %115 = load i32, i32* %image_width145, align 4, !tbaa !27
  %mul146 = mul nsw i32 %115, 10
  %call147 = call i32 @jdiv_round_up(i32 %mul146, i32 8)
  %116 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width148 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %116, i32 0, i32 27
  store i32 %call147, i32* %output_width148, align 8, !tbaa !38
  %117 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height149 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %117, i32 0, i32 8
  %118 = load i32, i32* %image_height149, align 8, !tbaa !29
  %mul150 = mul nsw i32 %118, 10
  %call151 = call i32 @jdiv_round_up(i32 %mul150, i32 8)
  %119 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height152 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %119, i32 0, i32 28
  store i32 %call151, i32* %output_height152, align 4, !tbaa !39
  %120 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size153 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %120, i32 0, i32 63
  store i32 10, i32* %min_DCT_scaled_size153, align 4, !tbaa !20
  %121 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size154 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %121, i32 0, i32 63
  store i32 10, i32* %min_DCT_scaled_size154, align 4, !tbaa !20
  br label %if.end255

if.else155:                                       ; preds = %if.else138
  %122 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num156 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %122, i32 0, i32 12
  %123 = load i32, i32* %scale_num156, align 8, !tbaa !36
  %mul157 = mul i32 %123, 8
  %124 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom158 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %124, i32 0, i32 13
  %125 = load i32, i32* %scale_denom158, align 4, !tbaa !37
  %mul159 = mul i32 %125, 11
  %cmp160 = icmp ule i32 %mul157, %mul159
  br i1 %cmp160, label %if.then161, label %if.else172

if.then161:                                       ; preds = %if.else155
  %126 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width162 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %126, i32 0, i32 7
  %127 = load i32, i32* %image_width162, align 4, !tbaa !27
  %mul163 = mul nsw i32 %127, 11
  %call164 = call i32 @jdiv_round_up(i32 %mul163, i32 8)
  %128 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width165 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %128, i32 0, i32 27
  store i32 %call164, i32* %output_width165, align 8, !tbaa !38
  %129 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height166 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %129, i32 0, i32 8
  %130 = load i32, i32* %image_height166, align 8, !tbaa !29
  %mul167 = mul nsw i32 %130, 11
  %call168 = call i32 @jdiv_round_up(i32 %mul167, i32 8)
  %131 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height169 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %131, i32 0, i32 28
  store i32 %call168, i32* %output_height169, align 4, !tbaa !39
  %132 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size170 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %132, i32 0, i32 63
  store i32 11, i32* %min_DCT_scaled_size170, align 4, !tbaa !20
  %133 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size171 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %133, i32 0, i32 63
  store i32 11, i32* %min_DCT_scaled_size171, align 4, !tbaa !20
  br label %if.end254

if.else172:                                       ; preds = %if.else155
  %134 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num173 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %134, i32 0, i32 12
  %135 = load i32, i32* %scale_num173, align 8, !tbaa !36
  %mul174 = mul i32 %135, 8
  %136 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom175 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %136, i32 0, i32 13
  %137 = load i32, i32* %scale_denom175, align 4, !tbaa !37
  %mul176 = mul i32 %137, 12
  %cmp177 = icmp ule i32 %mul174, %mul176
  br i1 %cmp177, label %if.then178, label %if.else189

if.then178:                                       ; preds = %if.else172
  %138 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width179 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %138, i32 0, i32 7
  %139 = load i32, i32* %image_width179, align 4, !tbaa !27
  %mul180 = mul nsw i32 %139, 12
  %call181 = call i32 @jdiv_round_up(i32 %mul180, i32 8)
  %140 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width182 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %140, i32 0, i32 27
  store i32 %call181, i32* %output_width182, align 8, !tbaa !38
  %141 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height183 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %141, i32 0, i32 8
  %142 = load i32, i32* %image_height183, align 8, !tbaa !29
  %mul184 = mul nsw i32 %142, 12
  %call185 = call i32 @jdiv_round_up(i32 %mul184, i32 8)
  %143 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height186 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %143, i32 0, i32 28
  store i32 %call185, i32* %output_height186, align 4, !tbaa !39
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size187 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 63
  store i32 12, i32* %min_DCT_scaled_size187, align 4, !tbaa !20
  %145 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size188 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %145, i32 0, i32 63
  store i32 12, i32* %min_DCT_scaled_size188, align 4, !tbaa !20
  br label %if.end253

if.else189:                                       ; preds = %if.else172
  %146 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num190 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %146, i32 0, i32 12
  %147 = load i32, i32* %scale_num190, align 8, !tbaa !36
  %mul191 = mul i32 %147, 8
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom192 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 13
  %149 = load i32, i32* %scale_denom192, align 4, !tbaa !37
  %mul193 = mul i32 %149, 13
  %cmp194 = icmp ule i32 %mul191, %mul193
  br i1 %cmp194, label %if.then195, label %if.else206

if.then195:                                       ; preds = %if.else189
  %150 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width196 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %150, i32 0, i32 7
  %151 = load i32, i32* %image_width196, align 4, !tbaa !27
  %mul197 = mul nsw i32 %151, 13
  %call198 = call i32 @jdiv_round_up(i32 %mul197, i32 8)
  %152 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width199 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %152, i32 0, i32 27
  store i32 %call198, i32* %output_width199, align 8, !tbaa !38
  %153 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height200 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %153, i32 0, i32 8
  %154 = load i32, i32* %image_height200, align 8, !tbaa !29
  %mul201 = mul nsw i32 %154, 13
  %call202 = call i32 @jdiv_round_up(i32 %mul201, i32 8)
  %155 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height203 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %155, i32 0, i32 28
  store i32 %call202, i32* %output_height203, align 4, !tbaa !39
  %156 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size204 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %156, i32 0, i32 63
  store i32 13, i32* %min_DCT_scaled_size204, align 4, !tbaa !20
  %157 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size205 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %157, i32 0, i32 63
  store i32 13, i32* %min_DCT_scaled_size205, align 4, !tbaa !20
  br label %if.end252

if.else206:                                       ; preds = %if.else189
  %158 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num207 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %158, i32 0, i32 12
  %159 = load i32, i32* %scale_num207, align 8, !tbaa !36
  %mul208 = mul i32 %159, 8
  %160 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom209 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %160, i32 0, i32 13
  %161 = load i32, i32* %scale_denom209, align 4, !tbaa !37
  %mul210 = mul i32 %161, 14
  %cmp211 = icmp ule i32 %mul208, %mul210
  br i1 %cmp211, label %if.then212, label %if.else223

if.then212:                                       ; preds = %if.else206
  %162 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width213 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %162, i32 0, i32 7
  %163 = load i32, i32* %image_width213, align 4, !tbaa !27
  %mul214 = mul nsw i32 %163, 14
  %call215 = call i32 @jdiv_round_up(i32 %mul214, i32 8)
  %164 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width216 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %164, i32 0, i32 27
  store i32 %call215, i32* %output_width216, align 8, !tbaa !38
  %165 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height217 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %165, i32 0, i32 8
  %166 = load i32, i32* %image_height217, align 8, !tbaa !29
  %mul218 = mul nsw i32 %166, 14
  %call219 = call i32 @jdiv_round_up(i32 %mul218, i32 8)
  %167 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height220 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %167, i32 0, i32 28
  store i32 %call219, i32* %output_height220, align 4, !tbaa !39
  %168 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size221 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %168, i32 0, i32 63
  store i32 14, i32* %min_DCT_scaled_size221, align 4, !tbaa !20
  %169 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size222 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %169, i32 0, i32 63
  store i32 14, i32* %min_DCT_scaled_size222, align 4, !tbaa !20
  br label %if.end251

if.else223:                                       ; preds = %if.else206
  %170 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num224 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %170, i32 0, i32 12
  %171 = load i32, i32* %scale_num224, align 8, !tbaa !36
  %mul225 = mul i32 %171, 8
  %172 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom226 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %172, i32 0, i32 13
  %173 = load i32, i32* %scale_denom226, align 4, !tbaa !37
  %mul227 = mul i32 %173, 15
  %cmp228 = icmp ule i32 %mul225, %mul227
  br i1 %cmp228, label %if.then229, label %if.else240

if.then229:                                       ; preds = %if.else223
  %174 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width230 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %174, i32 0, i32 7
  %175 = load i32, i32* %image_width230, align 4, !tbaa !27
  %mul231 = mul nsw i32 %175, 15
  %call232 = call i32 @jdiv_round_up(i32 %mul231, i32 8)
  %176 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width233 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %176, i32 0, i32 27
  store i32 %call232, i32* %output_width233, align 8, !tbaa !38
  %177 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height234 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %177, i32 0, i32 8
  %178 = load i32, i32* %image_height234, align 8, !tbaa !29
  %mul235 = mul nsw i32 %178, 15
  %call236 = call i32 @jdiv_round_up(i32 %mul235, i32 8)
  %179 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height237 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %179, i32 0, i32 28
  store i32 %call236, i32* %output_height237, align 4, !tbaa !39
  %180 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size238 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %180, i32 0, i32 63
  store i32 15, i32* %min_DCT_scaled_size238, align 4, !tbaa !20
  %181 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size239 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %181, i32 0, i32 63
  store i32 15, i32* %min_DCT_scaled_size239, align 4, !tbaa !20
  br label %if.end

if.else240:                                       ; preds = %if.else223
  %182 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width241 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %182, i32 0, i32 7
  %183 = load i32, i32* %image_width241, align 4, !tbaa !27
  %mul242 = mul nsw i32 %183, 16
  %call243 = call i32 @jdiv_round_up(i32 %mul242, i32 8)
  %184 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width244 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %184, i32 0, i32 27
  store i32 %call243, i32* %output_width244, align 8, !tbaa !38
  %185 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height245 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %185, i32 0, i32 8
  %186 = load i32, i32* %image_height245, align 8, !tbaa !29
  %mul246 = mul nsw i32 %186, 16
  %call247 = call i32 @jdiv_round_up(i32 %mul246, i32 8)
  %187 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height248 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %187, i32 0, i32 28
  store i32 %call247, i32* %output_height248, align 4, !tbaa !39
  %188 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size249 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %188, i32 0, i32 63
  store i32 16, i32* %min_DCT_scaled_size249, align 4, !tbaa !20
  %189 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size250 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %189, i32 0, i32 63
  store i32 16, i32* %min_DCT_scaled_size250, align 4, !tbaa !20
  br label %if.end

if.end:                                           ; preds = %if.else240, %if.then229
  br label %if.end251

if.end251:                                        ; preds = %if.end, %if.then212
  br label %if.end252

if.end252:                                        ; preds = %if.end251, %if.then195
  br label %if.end253

if.end253:                                        ; preds = %if.end252, %if.then178
  br label %if.end254

if.end254:                                        ; preds = %if.end253, %if.then161
  br label %if.end255

if.end255:                                        ; preds = %if.end254, %if.then144
  br label %if.end256

if.end256:                                        ; preds = %if.end255, %if.then127
  br label %if.end257

if.end257:                                        ; preds = %if.end256, %if.then110
  br label %if.end258

if.end258:                                        ; preds = %if.end257, %if.then93
  br label %if.end259

if.end259:                                        ; preds = %if.end258, %if.then76
  br label %if.end260

if.end260:                                        ; preds = %if.end259, %if.then59
  br label %if.end261

if.end261:                                        ; preds = %if.end260, %if.then42
  br label %if.end262

if.end262:                                        ; preds = %if.end261, %if.then25
  br label %if.end263

if.end263:                                        ; preds = %if.end262, %if.then8
  br label %if.end264

if.end264:                                        ; preds = %if.end263, %if.then
  store i32 0, i32* %ci, align 4, !tbaa !17
  %190 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %190, i32 0, i32 44
  %191 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !18
  store %struct.jpeg_component_info* %191, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end264
  %192 = load i32, i32* %ci, align 4, !tbaa !17
  %193 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %193, i32 0, i32 9
  %194 = load i32, i32* %num_components, align 4, !tbaa !19
  %cmp265 = icmp slt i32 %192, %194
  br i1 %cmp265, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %195 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size266 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %195, i32 0, i32 63
  %196 = load i32, i32* %min_DCT_scaled_size266, align 4, !tbaa !20
  %197 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %197, i32 0, i32 9
  store i32 %196, i32* %DCT_scaled_size, align 4, !tbaa !26
  %198 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size267 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %198, i32 0, i32 63
  %199 = load i32, i32* %min_DCT_scaled_size267, align 4, !tbaa !20
  %200 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size268 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %200, i32 0, i32 9
  store i32 %199, i32* %DCT_scaled_size268, align 4, !tbaa !26
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %201 = load i32, i32* %ci, align 4, !tbaa !17
  %inc = add nsw i32 %201, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !17
  %202 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %202, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %203 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare i32 @jdiv_round_up(i32, i32) #2

; Function Attrs: nounwind
define internal i32 @use_merged_upsample(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %do_fancy_upsampling = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 18
  %1 = load i32, i32* %do_fancy_upsampling, align 4, !tbaa !40
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %CCIR601_sampling = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 59
  %3 = load i32, i32* %CCIR601_sampling, align 4, !tbaa !41
  %tobool1 = icmp ne i32 %3, 0
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 10
  %5 = load i32, i32* %jpeg_color_space, align 8, !tbaa !42
  %cmp = icmp ne i32 %5, 3
  br i1 %cmp, label %if.then38, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %if.end
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 9
  %7 = load i32, i32* %num_components, align 4, !tbaa !19
  %cmp3 = icmp ne i32 %7, 3
  br i1 %cmp3, label %if.then38, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 11
  %9 = load i32, i32* %out_color_space, align 4, !tbaa !31
  %cmp5 = icmp ne i32 %9, 2
  br i1 %cmp5, label %land.lhs.true, label %if.end39

land.lhs.true:                                    ; preds = %lor.lhs.false4
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 11
  %11 = load i32, i32* %out_color_space6, align 4, !tbaa !31
  %cmp7 = icmp ne i32 %11, 16
  br i1 %cmp7, label %land.lhs.true8, label %if.end39

land.lhs.true8:                                   ; preds = %land.lhs.true
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 11
  %13 = load i32, i32* %out_color_space9, align 4, !tbaa !31
  %cmp10 = icmp ne i32 %13, 6
  br i1 %cmp10, label %land.lhs.true11, label %if.end39

land.lhs.true11:                                  ; preds = %land.lhs.true8
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 11
  %15 = load i32, i32* %out_color_space12, align 4, !tbaa !31
  %cmp13 = icmp ne i32 %15, 7
  br i1 %cmp13, label %land.lhs.true14, label %if.end39

land.lhs.true14:                                  ; preds = %land.lhs.true11
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 11
  %17 = load i32, i32* %out_color_space15, align 4, !tbaa !31
  %cmp16 = icmp ne i32 %17, 8
  br i1 %cmp16, label %land.lhs.true17, label %if.end39

land.lhs.true17:                                  ; preds = %land.lhs.true14
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 11
  %19 = load i32, i32* %out_color_space18, align 4, !tbaa !31
  %cmp19 = icmp ne i32 %19, 9
  br i1 %cmp19, label %land.lhs.true20, label %if.end39

land.lhs.true20:                                  ; preds = %land.lhs.true17
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 11
  %21 = load i32, i32* %out_color_space21, align 4, !tbaa !31
  %cmp22 = icmp ne i32 %21, 10
  br i1 %cmp22, label %land.lhs.true23, label %if.end39

land.lhs.true23:                                  ; preds = %land.lhs.true20
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space24 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 11
  %23 = load i32, i32* %out_color_space24, align 4, !tbaa !31
  %cmp25 = icmp ne i32 %23, 11
  br i1 %cmp25, label %land.lhs.true26, label %if.end39

land.lhs.true26:                                  ; preds = %land.lhs.true23
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 11
  %25 = load i32, i32* %out_color_space27, align 4, !tbaa !31
  %cmp28 = icmp ne i32 %25, 12
  br i1 %cmp28, label %land.lhs.true29, label %if.end39

land.lhs.true29:                                  ; preds = %land.lhs.true26
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space30 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 11
  %27 = load i32, i32* %out_color_space30, align 4, !tbaa !31
  %cmp31 = icmp ne i32 %27, 13
  br i1 %cmp31, label %land.lhs.true32, label %if.end39

land.lhs.true32:                                  ; preds = %land.lhs.true29
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space33 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 11
  %29 = load i32, i32* %out_color_space33, align 4, !tbaa !31
  %cmp34 = icmp ne i32 %29, 14
  br i1 %cmp34, label %land.lhs.true35, label %if.end39

land.lhs.true35:                                  ; preds = %land.lhs.true32
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space36 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 11
  %31 = load i32, i32* %out_color_space36, align 4, !tbaa !31
  %cmp37 = icmp ne i32 %31, 15
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %land.lhs.true35, %lor.lhs.false2, %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end39:                                         ; preds = %land.lhs.true35, %land.lhs.true32, %land.lhs.true29, %land.lhs.true26, %land.lhs.true23, %land.lhs.true20, %land.lhs.true17, %land.lhs.true14, %land.lhs.true11, %land.lhs.true8, %land.lhs.true, %lor.lhs.false4
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 11
  %33 = load i32, i32* %out_color_space40, align 4, !tbaa !31
  %cmp41 = icmp eq i32 %33, 16
  br i1 %cmp41, label %land.lhs.true42, label %lor.lhs.false44

land.lhs.true42:                                  ; preds = %if.end39
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 29
  %35 = load i32, i32* %out_color_components, align 8, !tbaa !32
  %cmp43 = icmp ne i32 %35, 3
  br i1 %cmp43, label %if.then51, label %lor.lhs.false44

lor.lhs.false44:                                  ; preds = %land.lhs.true42, %if.end39
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space45 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 11
  %37 = load i32, i32* %out_color_space45, align 4, !tbaa !31
  %cmp46 = icmp ne i32 %37, 16
  br i1 %cmp46, label %land.lhs.true47, label %if.end52

land.lhs.true47:                                  ; preds = %lor.lhs.false44
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 29
  %39 = load i32, i32* %out_color_components48, align 8, !tbaa !32
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space49 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 11
  %41 = load i32, i32* %out_color_space49, align 4, !tbaa !31
  %arrayidx = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_pixelsize, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx, align 4, !tbaa !17
  %cmp50 = icmp ne i32 %39, %42
  br i1 %cmp50, label %if.then51, label %if.end52

if.then51:                                        ; preds = %land.lhs.true47, %land.lhs.true42
  store i32 0, i32* %retval, align 4
  br label %return

if.end52:                                         ; preds = %land.lhs.true47, %lor.lhs.false44
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 44
  %44 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !18
  %arrayidx53 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %44, i32 0
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx53, i32 0, i32 2
  %45 = load i32, i32* %h_samp_factor, align 4, !tbaa !22
  %cmp54 = icmp ne i32 %45, 2
  br i1 %cmp54, label %if.then79, label %lor.lhs.false55

lor.lhs.false55:                                  ; preds = %if.end52
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info56 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 44
  %47 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info56, align 8, !tbaa !18
  %arrayidx57 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %47, i32 1
  %h_samp_factor58 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx57, i32 0, i32 2
  %48 = load i32, i32* %h_samp_factor58, align 4, !tbaa !22
  %cmp59 = icmp ne i32 %48, 1
  br i1 %cmp59, label %if.then79, label %lor.lhs.false60

lor.lhs.false60:                                  ; preds = %lor.lhs.false55
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info61 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 44
  %50 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info61, align 8, !tbaa !18
  %arrayidx62 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %50, i32 2
  %h_samp_factor63 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx62, i32 0, i32 2
  %51 = load i32, i32* %h_samp_factor63, align 4, !tbaa !22
  %cmp64 = icmp ne i32 %51, 1
  br i1 %cmp64, label %if.then79, label %lor.lhs.false65

lor.lhs.false65:                                  ; preds = %lor.lhs.false60
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info66 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 44
  %53 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info66, align 8, !tbaa !18
  %arrayidx67 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %53, i32 0
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx67, i32 0, i32 3
  %54 = load i32, i32* %v_samp_factor, align 4, !tbaa !25
  %cmp68 = icmp sgt i32 %54, 2
  br i1 %cmp68, label %if.then79, label %lor.lhs.false69

lor.lhs.false69:                                  ; preds = %lor.lhs.false65
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info70 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 44
  %56 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info70, align 8, !tbaa !18
  %arrayidx71 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %56, i32 1
  %v_samp_factor72 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx71, i32 0, i32 3
  %57 = load i32, i32* %v_samp_factor72, align 4, !tbaa !25
  %cmp73 = icmp ne i32 %57, 1
  br i1 %cmp73, label %if.then79, label %lor.lhs.false74

lor.lhs.false74:                                  ; preds = %lor.lhs.false69
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info75 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %58, i32 0, i32 44
  %59 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info75, align 8, !tbaa !18
  %arrayidx76 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %59, i32 2
  %v_samp_factor77 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx76, i32 0, i32 3
  %60 = load i32, i32* %v_samp_factor77, align 4, !tbaa !25
  %cmp78 = icmp ne i32 %60, 1
  br i1 %cmp78, label %if.then79, label %if.end80

if.then79:                                        ; preds = %lor.lhs.false74, %lor.lhs.false69, %lor.lhs.false65, %lor.lhs.false60, %lor.lhs.false55, %if.end52
  store i32 0, i32* %retval, align 4
  br label %return

if.end80:                                         ; preds = %lor.lhs.false74
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info81 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 44
  %62 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info81, align 8, !tbaa !18
  %arrayidx82 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %62, i32 0
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx82, i32 0, i32 9
  %63 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !26
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 63
  %65 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !20
  %cmp83 = icmp ne i32 %63, %65
  br i1 %cmp83, label %if.then96, label %lor.lhs.false84

lor.lhs.false84:                                  ; preds = %if.end80
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info85 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %66, i32 0, i32 44
  %67 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info85, align 8, !tbaa !18
  %arrayidx86 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %67, i32 1
  %DCT_scaled_size87 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx86, i32 0, i32 9
  %68 = load i32, i32* %DCT_scaled_size87, align 4, !tbaa !26
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size88 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %69, i32 0, i32 63
  %70 = load i32, i32* %min_DCT_scaled_size88, align 4, !tbaa !20
  %cmp89 = icmp ne i32 %68, %70
  br i1 %cmp89, label %if.then96, label %lor.lhs.false90

lor.lhs.false90:                                  ; preds = %lor.lhs.false84
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info91 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 44
  %72 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info91, align 8, !tbaa !18
  %arrayidx92 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %72, i32 2
  %DCT_scaled_size93 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx92, i32 0, i32 9
  %73 = load i32, i32* %DCT_scaled_size93, align 4, !tbaa !26
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size94 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %74, i32 0, i32 63
  %75 = load i32, i32* %min_DCT_scaled_size94, align 4, !tbaa !20
  %cmp95 = icmp ne i32 %73, %75
  br i1 %cmp95, label %if.then96, label %if.end97

if.then96:                                        ; preds = %lor.lhs.false90, %lor.lhs.false84, %if.end80
  store i32 0, i32* %retval, align 4
  br label %return

if.end97:                                         ; preds = %lor.lhs.false90
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end97, %if.then96, %if.then79, %if.then51, %if.then38, %if.then
  %76 = load i32, i32* %retval, align 4
  ret i32 %76
}

; Function Attrs: nounwind
define hidden void @jpeg_new_colormap(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %master = alloca %struct.my_decomp_master*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 77
  %2 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master1, align 4, !tbaa !43
  %3 = bitcast %struct.jpeg_decomp_master* %2 to %struct.my_decomp_master*
  store %struct.my_decomp_master* %3, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 5
  %5 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp ne i32 %5, 207
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 0
  %7 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %7, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !12
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 5
  %9 = load i32, i32* %global_state2, align 4, !tbaa !6
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !11
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %9, i32* %arrayidx, align 4, !tbaa !15
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 0
  %14 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !16
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_decompress_struct* %15 to %struct.jpeg_common_struct*
  call void %14(%struct.jpeg_common_struct* %16)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 20
  %18 = load i32, i32* %quantize_colors, align 4, !tbaa !33
  %tobool = icmp ne i32 %18, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.end
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_external_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 25
  %20 = load i32, i32* %enable_external_quant, align 8, !tbaa !44
  %tobool5 = icmp ne i32 %20, 0
  br i1 %tobool5, label %land.lhs.true6, label %if.else

land.lhs.true6:                                   ; preds = %land.lhs.true
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 33
  %22 = load i8**, i8*** %colormap, align 8, !tbaa !45
  %cmp7 = icmp ne i8** %22, null
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %land.lhs.true6
  %23 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %quantizer_2pass = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %23, i32 0, i32 4
  %24 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %quantizer_2pass, align 4, !tbaa !46
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 87
  store %struct.jpeg_color_quantizer* %24, %struct.jpeg_color_quantizer** %cquantize, align 4, !tbaa !49
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 87
  %27 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize9, align 4, !tbaa !49
  %new_color_map = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %27, i32 0, i32 3
  %new_color_map10 = bitcast {}** %new_color_map to void (%struct.jpeg_decompress_struct*)**
  %28 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %new_color_map10, align 4, !tbaa !50
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %28(%struct.jpeg_decompress_struct* %29)
  %30 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %30, i32 0, i32 0
  %is_dummy_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub, i32 0, i32 2
  store i32 0, i32* %is_dummy_pass, align 4, !tbaa !52
  br label %if.end15

if.else:                                          ; preds = %land.lhs.true6, %land.lhs.true, %if.end
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 0
  %32 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err11, align 8, !tbaa !11
  %msg_code12 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %32, i32 0, i32 5
  store i32 46, i32* %msg_code12, align 4, !tbaa !12
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 0
  %34 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 8, !tbaa !11
  %error_exit14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %34, i32 0, i32 0
  %35 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit14, align 4, !tbaa !16
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %37 = bitcast %struct.jpeg_decompress_struct* %36 to %struct.jpeg_common_struct*
  call void %35(%struct.jpeg_common_struct* %37)
  br label %if.end15

if.end15:                                         ; preds = %if.else, %if.then8
  %38 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #4
  ret void
}

; Function Attrs: nounwind
define hidden void @jinit_master_decompress(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %master = alloca %struct.my_decomp_master*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 77
  %2 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master1, align 4, !tbaa !43
  %3 = bitcast %struct.jpeg_decomp_master* %2 to %struct.my_decomp_master*
  store %struct.my_decomp_master* %3, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %4 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %4, i32 0, i32 0
  %prepare_for_output_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub, i32 0, i32 0
  %prepare_for_output_pass2 = bitcast {}** %prepare_for_output_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @prepare_for_output_pass, void (%struct.jpeg_decompress_struct*)** %prepare_for_output_pass2, align 4, !tbaa !53
  %5 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %5, i32 0, i32 0
  %finish_output_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub3, i32 0, i32 1
  %finish_output_pass4 = bitcast {}** %finish_output_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @finish_output_pass, void (%struct.jpeg_decompress_struct*)** %finish_output_pass4, align 4, !tbaa !54
  %6 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %6, i32 0, i32 0
  %is_dummy_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub5, i32 0, i32 2
  store i32 0, i32* %is_dummy_pass, align 4, !tbaa !52
  %7 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub6 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %7, i32 0, i32 0
  %jinit_upsampler_no_alloc = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub6, i32 0, i32 7
  store i32 0, i32* %jinit_upsampler_no_alloc, align 4, !tbaa !55
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @master_selection(%struct.jpeg_decompress_struct* %8)
  %9 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #4
  ret void
}

; Function Attrs: nounwind
define internal void @prepare_for_output_pass(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %master = alloca %struct.my_decomp_master*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 77
  %2 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master1, align 4, !tbaa !43
  %3 = bitcast %struct.jpeg_decomp_master* %2 to %struct.my_decomp_master*
  store %struct.my_decomp_master* %3, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %4 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %4, i32 0, i32 0
  %is_dummy_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub, i32 0, i32 2
  %5 = load i32, i32* %is_dummy_pass, align 4, !tbaa !52
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %6, i32 0, i32 0
  %is_dummy_pass3 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub2, i32 0, i32 2
  store i32 0, i32* %is_dummy_pass3, align 4, !tbaa !52
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 87
  %8 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize, align 4, !tbaa !49
  %start_pass = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %8, i32 0, i32 0
  %9 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %start_pass, align 4, !tbaa !56
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %9(%struct.jpeg_decompress_struct* %10, i32 0)
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %post = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 80
  %12 = load %struct.jpeg_d_post_controller*, %struct.jpeg_d_post_controller** %post, align 8, !tbaa !57
  %start_pass4 = getelementptr inbounds %struct.jpeg_d_post_controller, %struct.jpeg_d_post_controller* %12, i32 0, i32 0
  %13 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %start_pass4, align 4, !tbaa !58
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %13(%struct.jpeg_decompress_struct* %14, i32 2)
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 78
  %16 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !60
  %start_pass5 = getelementptr inbounds %struct.jpeg_d_main_controller, %struct.jpeg_d_main_controller* %16, i32 0, i32 0
  %17 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %start_pass5, align 4, !tbaa !61
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %17(%struct.jpeg_decompress_struct* %18, i32 2)
  br label %if.end51

if.else:                                          ; preds = %entry
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 20
  %20 = load i32, i32* %quantize_colors, align 4, !tbaa !33
  %tobool6 = icmp ne i32 %20, 0
  br i1 %tobool6, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %if.else
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 33
  %22 = load i8**, i8*** %colormap, align 8, !tbaa !45
  %cmp = icmp eq i8** %22, null
  br i1 %cmp, label %if.then7, label %if.end22

if.then7:                                         ; preds = %land.lhs.true
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %two_pass_quantize = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 22
  %24 = load i32, i32* %two_pass_quantize, align 4, !tbaa !63
  %tobool8 = icmp ne i32 %24, 0
  br i1 %tobool8, label %land.lhs.true9, label %if.else15

land.lhs.true9:                                   ; preds = %if.then7
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 26
  %26 = load i32, i32* %enable_2pass_quant, align 4, !tbaa !64
  %tobool10 = icmp ne i32 %26, 0
  br i1 %tobool10, label %if.then11, label %if.else15

if.then11:                                        ; preds = %land.lhs.true9
  %27 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %quantizer_2pass = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %27, i32 0, i32 4
  %28 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %quantizer_2pass, align 4, !tbaa !46
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 87
  store %struct.jpeg_color_quantizer* %28, %struct.jpeg_color_quantizer** %cquantize12, align 4, !tbaa !49
  %30 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub13 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %30, i32 0, i32 0
  %is_dummy_pass14 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub13, i32 0, i32 2
  store i32 1, i32* %is_dummy_pass14, align 4, !tbaa !52
  br label %if.end21

if.else15:                                        ; preds = %land.lhs.true9, %if.then7
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_1pass_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 24
  %32 = load i32, i32* %enable_1pass_quant, align 4, !tbaa !65
  %tobool16 = icmp ne i32 %32, 0
  br i1 %tobool16, label %if.then17, label %if.else19

if.then17:                                        ; preds = %if.else15
  %33 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %quantizer_1pass = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %33, i32 0, i32 3
  %34 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %quantizer_1pass, align 4, !tbaa !66
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 87
  store %struct.jpeg_color_quantizer* %34, %struct.jpeg_color_quantizer** %cquantize18, align 4, !tbaa !49
  br label %if.end

if.else19:                                        ; preds = %if.else15
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 0
  %37 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %37, i32 0, i32 5
  store i32 46, i32* %msg_code, align 4, !tbaa !12
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err20, align 8, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 0
  %40 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !16
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %42 = bitcast %struct.jpeg_decompress_struct* %41 to %struct.jpeg_common_struct*
  call void %40(%struct.jpeg_common_struct* %42)
  br label %if.end

if.end:                                           ; preds = %if.else19, %if.then17
  br label %if.end21

if.end21:                                         ; preds = %if.end, %if.then11
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %land.lhs.true, %if.else
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %idct = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 84
  %44 = load %struct.jpeg_inverse_dct*, %struct.jpeg_inverse_dct** %idct, align 8, !tbaa !67
  %start_pass23 = getelementptr inbounds %struct.jpeg_inverse_dct, %struct.jpeg_inverse_dct* %44, i32 0, i32 0
  %start_pass24 = bitcast {}** %start_pass23 to void (%struct.jpeg_decompress_struct*)**
  %45 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %start_pass24, align 4, !tbaa !68
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %45(%struct.jpeg_decompress_struct* %46)
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 79
  %48 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef, align 4, !tbaa !70
  %start_output_pass = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %48, i32 0, i32 2
  %start_output_pass25 = bitcast {}** %start_output_pass to void (%struct.jpeg_decompress_struct*)**
  %49 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %start_output_pass25, align 4, !tbaa !71
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %49(%struct.jpeg_decompress_struct* %50)
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %raw_data_out = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 16
  %52 = load i32, i32* %raw_data_out, align 4, !tbaa !73
  %tobool26 = icmp ne i32 %52, 0
  br i1 %tobool26, label %if.end50, label %if.then27

if.then27:                                        ; preds = %if.end22
  %53 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %using_merged_upsample = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %53, i32 0, i32 2
  %54 = load i32, i32* %using_merged_upsample, align 4, !tbaa !74
  %tobool28 = icmp ne i32 %54, 0
  br i1 %tobool28, label %if.end32, label %if.then29

if.then29:                                        ; preds = %if.then27
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 86
  %56 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert, align 8, !tbaa !75
  %start_pass30 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %56, i32 0, i32 0
  %start_pass31 = bitcast {}** %start_pass30 to void (%struct.jpeg_decompress_struct*)**
  %57 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %start_pass31, align 4, !tbaa !76
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %57(%struct.jpeg_decompress_struct* %58)
  br label %if.end32

if.end32:                                         ; preds = %if.then29, %if.then27
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 85
  %60 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample, align 4, !tbaa !78
  %start_pass33 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %60, i32 0, i32 0
  %start_pass34 = bitcast {}** %start_pass33 to void (%struct.jpeg_decompress_struct*)**
  %61 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %start_pass34, align 4, !tbaa !79
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %61(%struct.jpeg_decompress_struct* %62)
  %63 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors35 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %63, i32 0, i32 20
  %64 = load i32, i32* %quantize_colors35, align 4, !tbaa !33
  %tobool36 = icmp ne i32 %64, 0
  br i1 %tobool36, label %if.then37, label %if.end42

if.then37:                                        ; preds = %if.end32
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 87
  %66 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize38, align 4, !tbaa !49
  %start_pass39 = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %66, i32 0, i32 0
  %67 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %start_pass39, align 4, !tbaa !56
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %69 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub40 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %69, i32 0, i32 0
  %is_dummy_pass41 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub40, i32 0, i32 2
  %70 = load i32, i32* %is_dummy_pass41, align 4, !tbaa !52
  call void %67(%struct.jpeg_decompress_struct* %68, i32 %70)
  br label %if.end42

if.end42:                                         ; preds = %if.then37, %if.end32
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %post43 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 80
  %72 = load %struct.jpeg_d_post_controller*, %struct.jpeg_d_post_controller** %post43, align 8, !tbaa !57
  %start_pass44 = getelementptr inbounds %struct.jpeg_d_post_controller, %struct.jpeg_d_post_controller* %72, i32 0, i32 0
  %73 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %start_pass44, align 4, !tbaa !58
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %75 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub45 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %75, i32 0, i32 0
  %is_dummy_pass46 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub45, i32 0, i32 2
  %76 = load i32, i32* %is_dummy_pass46, align 4, !tbaa !52
  %tobool47 = icmp ne i32 %76, 0
  %77 = zext i1 %tobool47 to i64
  %cond = select i1 %tobool47, i32 3, i32 0
  call void %73(%struct.jpeg_decompress_struct* %74, i32 %cond)
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %78, i32 0, i32 78
  %79 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main48, align 8, !tbaa !60
  %start_pass49 = getelementptr inbounds %struct.jpeg_d_main_controller, %struct.jpeg_d_main_controller* %79, i32 0, i32 0
  %80 = load void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i32)** %start_pass49, align 4, !tbaa !61
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %80(%struct.jpeg_decompress_struct* %81, i32 0)
  br label %if.end50

if.end50:                                         ; preds = %if.end42, %if.end22
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.then
  %82 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %82, i32 0, i32 2
  %83 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !81
  %cmp52 = icmp ne %struct.jpeg_progress_mgr* %83, null
  br i1 %cmp52, label %if.then53, label %if.end72

if.then53:                                        ; preds = %if.end51
  %84 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pass_number = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %84, i32 0, i32 1
  %85 = load i32, i32* %pass_number, align 4, !tbaa !82
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 2
  %87 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress54, align 8, !tbaa !81
  %completed_passes = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %87, i32 0, i32 3
  store i32 %85, i32* %completed_passes, align 4, !tbaa !83
  %88 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pass_number55 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %88, i32 0, i32 1
  %89 = load i32, i32* %pass_number55, align 4, !tbaa !82
  %90 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pub56 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %90, i32 0, i32 0
  %is_dummy_pass57 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %pub56, i32 0, i32 2
  %91 = load i32, i32* %is_dummy_pass57, align 4, !tbaa !52
  %tobool58 = icmp ne i32 %91, 0
  %92 = zext i1 %tobool58 to i64
  %cond59 = select i1 %tobool58, i32 2, i32 1
  %add = add nsw i32 %89, %cond59
  %93 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress60 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %93, i32 0, i32 2
  %94 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress60, align 8, !tbaa !81
  %total_passes = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %94, i32 0, i32 4
  store i32 %add, i32* %total_passes, align 4, !tbaa !85
  %95 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %95, i32 0, i32 15
  %96 = load i32, i32* %buffered_image, align 8, !tbaa !86
  %tobool61 = icmp ne i32 %96, 0
  br i1 %tobool61, label %land.lhs.true62, label %if.end71

land.lhs.true62:                                  ; preds = %if.then53
  %97 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %97, i32 0, i32 81
  %98 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !87
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %98, i32 0, i32 5
  %99 = load i32, i32* %eoi_reached, align 4, !tbaa !88
  %tobool63 = icmp ne i32 %99, 0
  br i1 %tobool63, label %if.end71, label %if.then64

if.then64:                                        ; preds = %land.lhs.true62
  %100 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant65 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %100, i32 0, i32 26
  %101 = load i32, i32* %enable_2pass_quant65, align 4, !tbaa !64
  %tobool66 = icmp ne i32 %101, 0
  %102 = zext i1 %tobool66 to i64
  %cond67 = select i1 %tobool66, i32 2, i32 1
  %103 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress68 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %103, i32 0, i32 2
  %104 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress68, align 8, !tbaa !81
  %total_passes69 = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %104, i32 0, i32 4
  %105 = load i32, i32* %total_passes69, align 4, !tbaa !85
  %add70 = add nsw i32 %105, %cond67
  store i32 %add70, i32* %total_passes69, align 4, !tbaa !85
  br label %if.end71

if.end71:                                         ; preds = %if.then64, %land.lhs.true62, %if.then53
  br label %if.end72

if.end72:                                         ; preds = %if.end71, %if.end51
  %106 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #4
  ret void
}

; Function Attrs: nounwind
define internal void @finish_output_pass(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %master = alloca %struct.my_decomp_master*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 77
  %2 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master1, align 4, !tbaa !43
  %3 = bitcast %struct.jpeg_decomp_master* %2 to %struct.my_decomp_master*
  store %struct.my_decomp_master* %3, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 20
  %5 = load i32, i32* %quantize_colors, align 4, !tbaa !33
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 87
  %7 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize, align 4, !tbaa !49
  %finish_pass = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %7, i32 0, i32 2
  %finish_pass2 = bitcast {}** %finish_pass to void (%struct.jpeg_decompress_struct*)**
  %8 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %finish_pass2, align 4, !tbaa !90
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %8(%struct.jpeg_decompress_struct* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pass_number = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %10, i32 0, i32 1
  %11 = load i32, i32* %pass_number, align 4, !tbaa !82
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %pass_number, align 4, !tbaa !82
  %12 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #4
  ret void
}

; Function Attrs: nounwind
define internal void @master_selection(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %master = alloca %struct.my_decomp_master*, align 4
  %use_c_buffer = alloca i32, align 4
  %samplesperrow = alloca i32, align 4
  %jd_samplesperrow = alloca i32, align 4
  %nscans = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 77
  %2 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master1, align 4, !tbaa !43
  %3 = bitcast %struct.jpeg_decomp_master* %2 to %struct.my_decomp_master*
  store %struct.my_decomp_master* %3, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %4 = bitcast i32* %use_c_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %samplesperrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %jd_samplesperrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jpeg_calc_output_dimensions(%struct.jpeg_decompress_struct* %7)
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @prepare_range_limit_table(%struct.jpeg_decompress_struct* %8)
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 27
  %10 = load i32, i32* %output_width, align 8, !tbaa !38
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 29
  %12 = load i32, i32* %out_color_components, align 8, !tbaa !32
  %mul = mul nsw i32 %10, %12
  store i32 %mul, i32* %samplesperrow, align 4, !tbaa !91
  %13 = load i32, i32* %samplesperrow, align 4, !tbaa !91
  store i32 %13, i32* %jd_samplesperrow, align 4, !tbaa !17
  %14 = load i32, i32* %jd_samplesperrow, align 4, !tbaa !17
  %15 = load i32, i32* %samplesperrow, align 4, !tbaa !91
  %cmp = icmp ne i32 %14, %15
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !11
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 5
  store i32 70, i32* %msg_code, align 4, !tbaa !12
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !11
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 0
  %20 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !16
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %22 = bitcast %struct.jpeg_decompress_struct* %21 to %struct.jpeg_common_struct*
  call void %20(%struct.jpeg_common_struct* %22)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %23 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pass_number = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %23, i32 0, i32 1
  store i32 0, i32* %pass_number, align 4, !tbaa !82
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @use_merged_upsample(%struct.jpeg_decompress_struct* %24)
  %25 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %using_merged_upsample = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %25, i32 0, i32 2
  store i32 %call, i32* %using_merged_upsample, align 4, !tbaa !74
  %26 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %quantizer_1pass = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %26, i32 0, i32 3
  store %struct.jpeg_color_quantizer* null, %struct.jpeg_color_quantizer** %quantizer_1pass, align 4, !tbaa !66
  %27 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %quantizer_2pass = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %27, i32 0, i32 4
  store %struct.jpeg_color_quantizer* null, %struct.jpeg_color_quantizer** %quantizer_2pass, align 4, !tbaa !46
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 20
  %29 = load i32, i32* %quantize_colors, align 4, !tbaa !33
  %tobool = icmp ne i32 %29, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then4

lor.lhs.false:                                    ; preds = %if.end
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 15
  %31 = load i32, i32* %buffered_image, align 8, !tbaa !86
  %tobool3 = icmp ne i32 %31, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %lor.lhs.false, %if.end
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_1pass_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 24
  store i32 0, i32* %enable_1pass_quant, align 4, !tbaa !65
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_external_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 25
  store i32 0, i32* %enable_external_quant, align 8, !tbaa !44
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 26
  store i32 0, i32* %enable_2pass_quant, align 4, !tbaa !64
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %lor.lhs.false
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 20
  %36 = load i32, i32* %quantize_colors6, align 4, !tbaa !33
  %tobool7 = icmp ne i32 %36, 0
  br i1 %tobool7, label %if.then8, label %if.end49

if.then8:                                         ; preds = %if.end5
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %raw_data_out = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 16
  %38 = load i32, i32* %raw_data_out, align 4, !tbaa !73
  %tobool9 = icmp ne i32 %38, 0
  br i1 %tobool9, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.then8
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 0
  %40 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err11, align 8, !tbaa !11
  %msg_code12 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %40, i32 0, i32 5
  store i32 47, i32* %msg_code12, align 4, !tbaa !12
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 0
  %42 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 8, !tbaa !11
  %error_exit14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %42, i32 0, i32 0
  %43 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit14, align 4, !tbaa !16
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %45 = bitcast %struct.jpeg_decompress_struct* %44 to %struct.jpeg_common_struct*
  call void %43(%struct.jpeg_common_struct* %45)
  br label %if.end15

if.end15:                                         ; preds = %if.then10, %if.then8
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 29
  %47 = load i32, i32* %out_color_components16, align 8, !tbaa !32
  %cmp17 = icmp ne i32 %47, 3
  br i1 %cmp17, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.end15
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_1pass_quant19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 24
  store i32 1, i32* %enable_1pass_quant19, align 4, !tbaa !65
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_external_quant20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 25
  store i32 0, i32* %enable_external_quant20, align 8, !tbaa !44
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 26
  store i32 0, i32* %enable_2pass_quant21, align 4, !tbaa !64
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 33
  store i8** null, i8*** %colormap, align 8, !tbaa !45
  br label %if.end34

if.else:                                          ; preds = %if.end15
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 33
  %53 = load i8**, i8*** %colormap22, align 8, !tbaa !45
  %cmp23 = icmp ne i8** %53, null
  br i1 %cmp23, label %if.then24, label %if.else26

if.then24:                                        ; preds = %if.else
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_external_quant25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 25
  store i32 1, i32* %enable_external_quant25, align 8, !tbaa !44
  br label %if.end33

if.else26:                                        ; preds = %if.else
  %55 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %two_pass_quantize = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %55, i32 0, i32 22
  %56 = load i32, i32* %two_pass_quantize, align 4, !tbaa !63
  %tobool27 = icmp ne i32 %56, 0
  br i1 %tobool27, label %if.then28, label %if.else30

if.then28:                                        ; preds = %if.else26
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 26
  store i32 1, i32* %enable_2pass_quant29, align 4, !tbaa !64
  br label %if.end32

if.else30:                                        ; preds = %if.else26
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_1pass_quant31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %58, i32 0, i32 24
  store i32 1, i32* %enable_1pass_quant31, align 4, !tbaa !65
  br label %if.end32

if.end32:                                         ; preds = %if.else30, %if.then28
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.then24
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.then18
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_1pass_quant35 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 24
  %60 = load i32, i32* %enable_1pass_quant35, align 4, !tbaa !65
  %tobool36 = icmp ne i32 %60, 0
  br i1 %tobool36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.end34
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_1pass_quantizer(%struct.jpeg_decompress_struct* %61)
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %62, i32 0, i32 87
  %63 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize, align 4, !tbaa !49
  %64 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %quantizer_1pass38 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %64, i32 0, i32 3
  store %struct.jpeg_color_quantizer* %63, %struct.jpeg_color_quantizer** %quantizer_1pass38, align 4, !tbaa !66
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %if.end34
  %65 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %65, i32 0, i32 26
  %66 = load i32, i32* %enable_2pass_quant40, align 4, !tbaa !64
  %tobool41 = icmp ne i32 %66, 0
  br i1 %tobool41, label %if.then45, label %lor.lhs.false42

lor.lhs.false42:                                  ; preds = %if.end39
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_external_quant43 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %67, i32 0, i32 25
  %68 = load i32, i32* %enable_external_quant43, align 8, !tbaa !44
  %tobool44 = icmp ne i32 %68, 0
  br i1 %tobool44, label %if.then45, label %if.end48

if.then45:                                        ; preds = %lor.lhs.false42, %if.end39
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_2pass_quantizer(%struct.jpeg_decompress_struct* %69)
  %70 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize46 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %70, i32 0, i32 87
  %71 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize46, align 4, !tbaa !49
  %72 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %quantizer_2pass47 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %72, i32 0, i32 4
  store %struct.jpeg_color_quantizer* %71, %struct.jpeg_color_quantizer** %quantizer_2pass47, align 4, !tbaa !46
  br label %if.end48

if.end48:                                         ; preds = %if.then45, %lor.lhs.false42
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.end5
  %73 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %raw_data_out50 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %73, i32 0, i32 16
  %74 = load i32, i32* %raw_data_out50, align 4, !tbaa !73
  %tobool51 = icmp ne i32 %74, 0
  br i1 %tobool51, label %if.end59, label %if.then52

if.then52:                                        ; preds = %if.end49
  %75 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %using_merged_upsample53 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %75, i32 0, i32 2
  %76 = load i32, i32* %using_merged_upsample53, align 4, !tbaa !74
  %tobool54 = icmp ne i32 %76, 0
  br i1 %tobool54, label %if.then55, label %if.else56

if.then55:                                        ; preds = %if.then52
  %77 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_merged_upsampler(%struct.jpeg_decompress_struct* %77)
  br label %if.end57

if.else56:                                        ; preds = %if.then52
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_color_deconverter(%struct.jpeg_decompress_struct* %78)
  %79 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_upsampler(%struct.jpeg_decompress_struct* %79)
  br label %if.end57

if.end57:                                         ; preds = %if.else56, %if.then55
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %81 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant58 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %81, i32 0, i32 26
  %82 = load i32, i32* %enable_2pass_quant58, align 4, !tbaa !64
  call void @jinit_d_post_controller(%struct.jpeg_decompress_struct* %80, i32 %82)
  br label %if.end59

if.end59:                                         ; preds = %if.end57, %if.end49
  %83 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_inverse_dct(%struct.jpeg_decompress_struct* %83)
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %arith_code = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %84, i32 0, i32 46
  %85 = load i32, i32* %arith_code, align 8, !tbaa !92
  %tobool60 = icmp ne i32 %85, 0
  br i1 %tobool60, label %if.then61, label %if.else66

if.then61:                                        ; preds = %if.end59
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err62 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 0
  %87 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err62, align 8, !tbaa !11
  %msg_code63 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %87, i32 0, i32 5
  store i32 1, i32* %msg_code63, align 4, !tbaa !12
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err64 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %88, i32 0, i32 0
  %89 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err64, align 8, !tbaa !11
  %error_exit65 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %89, i32 0, i32 0
  %90 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit65, align 4, !tbaa !16
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %92 = bitcast %struct.jpeg_decompress_struct* %91 to %struct.jpeg_common_struct*
  call void %90(%struct.jpeg_common_struct* %92)
  br label %if.end71

if.else66:                                        ; preds = %if.end59
  %93 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %93, i32 0, i32 45
  %94 = load i32, i32* %progressive_mode, align 4, !tbaa !93
  %tobool67 = icmp ne i32 %94, 0
  br i1 %tobool67, label %if.then68, label %if.else69

if.then68:                                        ; preds = %if.else66
  %95 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_phuff_decoder(%struct.jpeg_decompress_struct* %95)
  br label %if.end70

if.else69:                                        ; preds = %if.else66
  %96 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_huff_decoder(%struct.jpeg_decompress_struct* %96)
  br label %if.end70

if.end70:                                         ; preds = %if.else69, %if.then68
  br label %if.end71

if.end71:                                         ; preds = %if.end70, %if.then61
  %97 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %97, i32 0, i32 81
  %98 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !87
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %98, i32 0, i32 4
  %99 = load i32, i32* %has_multiple_scans, align 4, !tbaa !94
  %tobool72 = icmp ne i32 %99, 0
  br i1 %tobool72, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end71
  %100 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image73 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %100, i32 0, i32 15
  %101 = load i32, i32* %buffered_image73, align 8, !tbaa !86
  %tobool74 = icmp ne i32 %101, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.end71
  %102 = phi i1 [ true, %if.end71 ], [ %tobool74, %lor.rhs ]
  %lor.ext = zext i1 %102 to i32
  store i32 %lor.ext, i32* %use_c_buffer, align 4, !tbaa !17
  %103 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %104 = load i32, i32* %use_c_buffer, align 4, !tbaa !17
  call void @jinit_d_coef_controller(%struct.jpeg_decompress_struct* %103, i32 %104)
  %105 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %raw_data_out75 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %105, i32 0, i32 16
  %106 = load i32, i32* %raw_data_out75, align 4, !tbaa !73
  %tobool76 = icmp ne i32 %106, 0
  br i1 %tobool76, label %if.end78, label %if.then77

if.then77:                                        ; preds = %lor.end
  %107 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_d_main_controller(%struct.jpeg_decompress_struct* %107, i32 0)
  br label %if.end78

if.end78:                                         ; preds = %if.then77, %lor.end
  %108 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %108, i32 0, i32 1
  %109 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !95
  %realize_virt_arrays = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %109, i32 0, i32 6
  %realize_virt_arrays79 = bitcast {}** %realize_virt_arrays to void (%struct.jpeg_common_struct*)**
  %110 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %realize_virt_arrays79, align 4, !tbaa !96
  %111 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %112 = bitcast %struct.jpeg_decompress_struct* %111 to %struct.jpeg_common_struct*
  call void %110(%struct.jpeg_common_struct* %112)
  %113 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl80 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %113, i32 0, i32 81
  %114 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl80, align 4, !tbaa !87
  %start_input_pass = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %114, i32 0, i32 2
  %start_input_pass81 = bitcast {}** %start_input_pass to void (%struct.jpeg_decompress_struct*)**
  %115 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %start_input_pass81, align 4, !tbaa !98
  %116 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %115(%struct.jpeg_decompress_struct* %116)
  %117 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master82 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %117, i32 0, i32 77
  %118 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master82, align 4, !tbaa !43
  %first_iMCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %118, i32 0, i32 3
  store i32 0, i32* %first_iMCU_col, align 4, !tbaa !99
  %119 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %119, i32 0, i32 68
  %120 = load i32, i32* %MCUs_per_row, align 4, !tbaa !100
  %sub = sub i32 %120, 1
  %121 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master83 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %121, i32 0, i32 77
  %122 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master83, align 4, !tbaa !43
  %last_iMCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %122, i32 0, i32 4
  store i32 %sub, i32* %last_iMCU_col, align 4, !tbaa !101
  %123 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %123, i32 0, i32 2
  %124 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !81
  %cmp84 = icmp ne %struct.jpeg_progress_mgr* %124, null
  br i1 %cmp84, label %land.lhs.true, label %if.end107

land.lhs.true:                                    ; preds = %if.end78
  %125 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image85 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %125, i32 0, i32 15
  %126 = load i32, i32* %buffered_image85, align 8, !tbaa !86
  %tobool86 = icmp ne i32 %126, 0
  br i1 %tobool86, label %if.end107, label %land.lhs.true87

land.lhs.true87:                                  ; preds = %land.lhs.true
  %127 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl88 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %127, i32 0, i32 81
  %128 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl88, align 4, !tbaa !87
  %has_multiple_scans89 = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %128, i32 0, i32 4
  %129 = load i32, i32* %has_multiple_scans89, align 4, !tbaa !94
  %tobool90 = icmp ne i32 %129, 0
  br i1 %tobool90, label %if.then91, label %if.end107

if.then91:                                        ; preds = %land.lhs.true87
  %130 = bitcast i32* %nscans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #4
  %131 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode92 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %131, i32 0, i32 45
  %132 = load i32, i32* %progressive_mode92, align 4, !tbaa !93
  %tobool93 = icmp ne i32 %132, 0
  br i1 %tobool93, label %if.then94, label %if.else96

if.then94:                                        ; preds = %if.then91
  %133 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %133, i32 0, i32 9
  %134 = load i32, i32* %num_components, align 4, !tbaa !19
  %mul95 = mul nsw i32 3, %134
  %add = add nsw i32 2, %mul95
  store i32 %add, i32* %nscans, align 4, !tbaa !17
  br label %if.end98

if.else96:                                        ; preds = %if.then91
  %135 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components97 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %135, i32 0, i32 9
  %136 = load i32, i32* %num_components97, align 4, !tbaa !19
  store i32 %136, i32* %nscans, align 4, !tbaa !17
  br label %if.end98

if.end98:                                         ; preds = %if.else96, %if.then94
  %137 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress99 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %137, i32 0, i32 2
  %138 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress99, align 8, !tbaa !81
  %pass_counter = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %138, i32 0, i32 1
  store i32 0, i32* %pass_counter, align 4, !tbaa !102
  %139 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %139, i32 0, i32 64
  %140 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !103
  %141 = load i32, i32* %nscans, align 4, !tbaa !17
  %mul100 = mul nsw i32 %140, %141
  %142 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress101 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %142, i32 0, i32 2
  %143 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress101, align 8, !tbaa !81
  %pass_limit = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %143, i32 0, i32 2
  store i32 %mul100, i32* %pass_limit, align 4, !tbaa !104
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress102 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 2
  %145 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress102, align 8, !tbaa !81
  %completed_passes = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %145, i32 0, i32 3
  store i32 0, i32* %completed_passes, align 4, !tbaa !83
  %146 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant103 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %146, i32 0, i32 26
  %147 = load i32, i32* %enable_2pass_quant103, align 4, !tbaa !64
  %tobool104 = icmp ne i32 %147, 0
  %148 = zext i1 %tobool104 to i64
  %cond = select i1 %tobool104, i32 3, i32 2
  %149 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress105 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %149, i32 0, i32 2
  %150 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress105, align 8, !tbaa !81
  %total_passes = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %150, i32 0, i32 4
  store i32 %cond, i32* %total_passes, align 4, !tbaa !85
  %151 = load %struct.my_decomp_master*, %struct.my_decomp_master** %master, align 4, !tbaa !2
  %pass_number106 = getelementptr inbounds %struct.my_decomp_master, %struct.my_decomp_master* %151, i32 0, i32 1
  %152 = load i32, i32* %pass_number106, align 4, !tbaa !82
  %inc = add nsw i32 %152, 1
  store i32 %inc, i32* %pass_number106, align 4, !tbaa !82
  %153 = bitcast i32* %nscans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  br label %if.end107

if.end107:                                        ; preds = %if.end98, %land.lhs.true87, %land.lhs.true, %if.end78
  %154 = bitcast i32* %jd_samplesperrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  %155 = bitcast i32* %samplesperrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #4
  %156 = bitcast i32* %use_c_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #4
  %157 = bitcast %struct.my_decomp_master** %master to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  ret void
}

; Function Attrs: nounwind
define internal void @prepare_range_limit_table(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %table = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i8** %table to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 1
  %3 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !95
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %3, i32 0, i32 0
  %4 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !105
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = bitcast %struct.jpeg_decompress_struct* %5 to %struct.jpeg_common_struct*
  %call = call i8* %4(%struct.jpeg_common_struct* %6, i32 1, i32 1408)
  store i8* %call, i8** %table, align 4, !tbaa !2
  %7 = load i8*, i8** %table, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 256
  store i8* %add.ptr, i8** %table, align 4, !tbaa !2
  %8 = load i8*, i8** %table, align 4, !tbaa !2
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 65
  store i8* %8, i8** %sample_range_limit, align 4, !tbaa !106
  %10 = load i8*, i8** %table, align 4, !tbaa !2
  %add.ptr1 = getelementptr inbounds i8, i8* %10, i32 -256
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr1, i8 0, i32 256, i1 false)
  store i32 0, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !17
  %cmp = icmp sle i32 %11, 255
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load i32, i32* %i, align 4, !tbaa !17
  %conv = trunc i32 %12 to i8
  %13 = load i8*, i8** %table, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds i8, i8* %13, i32 %14
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !15
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = load i8*, i8** %table, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds i8, i8* %16, i32 128
  store i8* %add.ptr2, i8** %table, align 4, !tbaa !2
  store i32 128, i32* %i, align 4, !tbaa !17
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc8, %for.end
  %17 = load i32, i32* %i, align 4, !tbaa !17
  %cmp4 = icmp slt i32 %17, 512
  br i1 %cmp4, label %for.body6, label %for.end10

for.body6:                                        ; preds = %for.cond3
  %18 = load i8*, i8** %table, align 4, !tbaa !2
  %19 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx7 = getelementptr inbounds i8, i8* %18, i32 %19
  store i8 -1, i8* %arrayidx7, align 1, !tbaa !15
  br label %for.inc8

for.inc8:                                         ; preds = %for.body6
  %20 = load i32, i32* %i, align 4, !tbaa !17
  %inc9 = add nsw i32 %20, 1
  store i32 %inc9, i32* %i, align 4, !tbaa !17
  br label %for.cond3

for.end10:                                        ; preds = %for.cond3
  %21 = load i8*, i8** %table, align 4, !tbaa !2
  %add.ptr11 = getelementptr inbounds i8, i8* %21, i32 512
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr11, i8 0, i32 384, i1 false)
  %22 = load i8*, i8** %table, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i8, i8* %22, i32 896
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 65
  %24 = load i8*, i8** %sample_range_limit13, align 4, !tbaa !106
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr12, i8* align 1 %24, i32 128, i1 false)
  %25 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #4
  %26 = bitcast i8** %table to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #4
  ret void
}

declare void @jinit_1pass_quantizer(%struct.jpeg_decompress_struct*) #2

declare void @jinit_2pass_quantizer(%struct.jpeg_decompress_struct*) #2

declare void @jinit_merged_upsampler(%struct.jpeg_decompress_struct*) #2

declare void @jinit_color_deconverter(%struct.jpeg_decompress_struct*) #2

declare void @jinit_upsampler(%struct.jpeg_decompress_struct*) #2

declare void @jinit_d_post_controller(%struct.jpeg_decompress_struct*, i32) #2

declare void @jinit_inverse_dct(%struct.jpeg_decompress_struct*) #2

declare void @jinit_phuff_decoder(%struct.jpeg_decompress_struct*) #2

declare void @jinit_huff_decoder(%struct.jpeg_decompress_struct*) #2

declare void @jinit_d_coef_controller(%struct.jpeg_decompress_struct*, i32) #2

declare void @jinit_d_main_controller(%struct.jpeg_decompress_struct*, i32) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 20}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!7, !3, i64 0}
!12 = !{!13, !8, i64 20}
!13 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !14, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!14 = !{!"long", !4, i64 0}
!15 = !{!4, !4, i64 0}
!16 = !{!13, !3, i64 0}
!17 = !{!8, !8, i64 0}
!18 = !{!7, !3, i64 216}
!19 = !{!7, !8, i64 36}
!20 = !{!7, !8, i64 316}
!21 = !{!7, !8, i64 308}
!22 = !{!23, !8, i64 8}
!23 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!24 = !{!7, !8, i64 312}
!25 = !{!23, !8, i64 12}
!26 = !{!23, !8, i64 36}
!27 = !{!7, !8, i64 28}
!28 = !{!23, !8, i64 40}
!29 = !{!7, !8, i64 32}
!30 = !{!23, !8, i64 44}
!31 = !{!7, !4, i64 44}
!32 = !{!7, !8, i64 120}
!33 = !{!7, !8, i64 84}
!34 = !{!7, !8, i64 124}
!35 = !{!7, !8, i64 128}
!36 = !{!7, !8, i64 48}
!37 = !{!7, !8, i64 52}
!38 = !{!7, !8, i64 112}
!39 = !{!7, !8, i64 116}
!40 = !{!7, !8, i64 76}
!41 = !{!7, !8, i64 300}
!42 = !{!7, !4, i64 40}
!43 = !{!7, !3, i64 420}
!44 = !{!7, !8, i64 104}
!45 = !{!7, !3, i64 136}
!46 = !{!47, !3, i64 116}
!47 = !{!"", !48, i64 0, !8, i64 104, !8, i64 108, !3, i64 112, !3, i64 116}
!48 = !{!"jpeg_decomp_master", !3, i64 0, !3, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !4, i64 20, !4, i64 60, !8, i64 100}
!49 = !{!7, !3, i64 460}
!50 = !{!51, !3, i64 12}
!51 = !{!"jpeg_color_quantizer", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!52 = !{!47, !8, i64 8}
!53 = !{!47, !3, i64 0}
!54 = !{!47, !3, i64 4}
!55 = !{!47, !8, i64 100}
!56 = !{!51, !3, i64 0}
!57 = !{!7, !3, i64 432}
!58 = !{!59, !3, i64 0}
!59 = !{!"jpeg_d_post_controller", !3, i64 0, !3, i64 4}
!60 = !{!7, !3, i64 424}
!61 = !{!62, !3, i64 0}
!62 = !{!"jpeg_d_main_controller", !3, i64 0, !3, i64 4}
!63 = !{!7, !8, i64 92}
!64 = !{!7, !8, i64 108}
!65 = !{!7, !8, i64 100}
!66 = !{!47, !3, i64 112}
!67 = !{!7, !3, i64 448}
!68 = !{!69, !3, i64 0}
!69 = !{!"jpeg_inverse_dct", !3, i64 0, !4, i64 4}
!70 = !{!7, !3, i64 428}
!71 = !{!72, !3, i64 8}
!72 = !{!"jpeg_d_coef_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!73 = !{!7, !8, i64 68}
!74 = !{!47, !8, i64 108}
!75 = !{!7, !3, i64 456}
!76 = !{!77, !3, i64 0}
!77 = !{!"jpeg_color_deconverter", !3, i64 0, !3, i64 4}
!78 = !{!7, !3, i64 452}
!79 = !{!80, !3, i64 0}
!80 = !{!"jpeg_upsampler", !3, i64 0, !3, i64 4, !8, i64 8}
!81 = !{!7, !3, i64 8}
!82 = !{!47, !8, i64 104}
!83 = !{!84, !8, i64 12}
!84 = !{!"jpeg_progress_mgr", !3, i64 0, !14, i64 4, !14, i64 8, !8, i64 12, !8, i64 16}
!85 = !{!84, !8, i64 16}
!86 = !{!7, !8, i64 64}
!87 = !{!7, !3, i64 436}
!88 = !{!89, !8, i64 20}
!89 = !{!"jpeg_input_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20}
!90 = !{!51, !3, i64 8}
!91 = !{!14, !14, i64 0}
!92 = !{!7, !8, i64 224}
!93 = !{!7, !8, i64 220}
!94 = !{!89, !8, i64 16}
!95 = !{!7, !3, i64 4}
!96 = !{!97, !3, i64 24}
!97 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!98 = !{!89, !3, i64 8}
!99 = !{!48, !8, i64 12}
!100 = !{!7, !8, i64 348}
!101 = !{!48, !8, i64 16}
!102 = !{!84, !14, i64 4}
!103 = !{!7, !8, i64 320}
!104 = !{!84, !14, i64 8}
!105 = !{!97, !3, i64 0}
!106 = !{!7, !3, i64 324}
