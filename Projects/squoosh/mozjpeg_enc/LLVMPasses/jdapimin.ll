; ModuleID = 'jdapimin.c'
source_filename = "jdapimin.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }

; Function Attrs: nounwind
define hidden void @jpeg_CreateDecompress(%struct.jpeg_decompress_struct* %cinfo, i32 %version, i32 %structsize) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %version.addr = alloca i32, align 4
  %structsize.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %err23 = alloca %struct.jpeg_error_mgr*, align 4
  %client_data = alloca i8*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %version, i32* %version.addr, align 4, !tbaa !6
  store i32 %structsize, i32* %structsize.addr, align 4, !tbaa !8
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 1
  store %struct.jpeg_memory_mgr* null, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !10
  %2 = load i32, i32* %version.addr, align 4, !tbaa !6
  %cmp = icmp ne i32 %2, 62
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 5
  store i32 12, i32* %msg_code, align 4, !tbaa !15
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 6
  %i2 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i2, i32 0, i32 0
  store i32 62, i32* %arrayidx, align 4, !tbaa !17
  %7 = load i32, i32* %version.addr, align 4, !tbaa !6
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !14
  %msg_parm4 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 6
  %i5 = bitcast %union.anon* %msg_parm4 to [8 x i32]*
  %arrayidx6 = getelementptr inbounds [8 x i32], [8 x i32]* %i5, i32 0, i32 1
  store i32 %7, i32* %arrayidx6, align 4, !tbaa !17
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err7, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_decompress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load i32, i32* %structsize.addr, align 4, !tbaa !8
  %cmp8 = icmp ne i32 %15, 464
  br i1 %cmp8, label %if.then9, label %if.end22

if.then9:                                         ; preds = %if.end
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !14
  %msg_code11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 5
  store i32 21, i32* %msg_code11, align 4, !tbaa !15
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !14
  %msg_parm13 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 6
  %i14 = bitcast %union.anon* %msg_parm13 to [8 x i32]*
  %arrayidx15 = getelementptr inbounds [8 x i32], [8 x i32]* %i14, i32 0, i32 0
  store i32 464, i32* %arrayidx15, align 4, !tbaa !17
  %20 = load i32, i32* %structsize.addr, align 4, !tbaa !8
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err16, align 8, !tbaa !14
  %msg_parm17 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 6
  %i18 = bitcast %union.anon* %msg_parm17 to [8 x i32]*
  %arrayidx19 = getelementptr inbounds [8 x i32], [8 x i32]* %i18, i32 0, i32 1
  store i32 %20, i32* %arrayidx19, align 4, !tbaa !17
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err20 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err20, align 8, !tbaa !14
  %error_exit21 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 0
  %25 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit21, align 4, !tbaa !18
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = bitcast %struct.jpeg_decompress_struct* %26 to %struct.jpeg_common_struct*
  call void %25(%struct.jpeg_common_struct* %27)
  br label %if.end22

if.end22:                                         ; preds = %if.then9, %if.end
  %28 = bitcast %struct.jpeg_error_mgr** %err23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #4
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err24 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 0
  %30 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err24, align 8, !tbaa !14
  store %struct.jpeg_error_mgr* %30, %struct.jpeg_error_mgr** %err23, align 4, !tbaa !2
  %31 = bitcast i8** %client_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %client_data25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 3
  %33 = load i8*, i8** %client_data25, align 4, !tbaa !19
  store i8* %33, i8** %client_data, align 4, !tbaa !2
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %35 = bitcast %struct.jpeg_decompress_struct* %34 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %35, i8 0, i32 464, i1 false)
  %36 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err23, align 4, !tbaa !2
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err26 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 0
  store %struct.jpeg_error_mgr* %36, %struct.jpeg_error_mgr** %err26, align 8, !tbaa !14
  %38 = load i8*, i8** %client_data, align 4, !tbaa !2
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %client_data27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 3
  store i8* %38, i8** %client_data27, align 4, !tbaa !19
  %40 = bitcast i8** %client_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #4
  %41 = bitcast %struct.jpeg_error_mgr** %err23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #4
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %is_decompressor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 4
  store i32 1, i32* %is_decompressor, align 8, !tbaa !20
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %44 = bitcast %struct.jpeg_decompress_struct* %43 to %struct.jpeg_common_struct*
  call void @jinit_memory_mgr(%struct.jpeg_common_struct* %44)
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 2
  store %struct.jpeg_progress_mgr* null, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !21
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 6
  store %struct.jpeg_source_mgr* null, %struct.jpeg_source_mgr** %src, align 8, !tbaa !22
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end22
  %47 = load i32, i32* %i, align 4, !tbaa !6
  %cmp28 = icmp slt i32 %47, 4
  br i1 %cmp28, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 40
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %49
  store %struct.JQUANT_TBL* null, %struct.JQUANT_TBL** %arrayidx29, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %50 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %50, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc35, %for.end
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %cmp31 = icmp slt i32 %51, 4
  br i1 %cmp31, label %for.body32, label %for.end37

for.body32:                                       ; preds = %for.cond30
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dc_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 41
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %dc_huff_tbl_ptrs, i32 0, i32 %53
  store %struct.JHUFF_TBL* null, %struct.JHUFF_TBL** %arrayidx33, align 4, !tbaa !2
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %ac_huff_tbl_ptrs = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 42
  %55 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*]* %ac_huff_tbl_ptrs, i32 0, i32 %55
  store %struct.JHUFF_TBL* null, %struct.JHUFF_TBL** %arrayidx34, align 4, !tbaa !2
  br label %for.inc35

for.inc35:                                        ; preds = %for.body32
  %56 = load i32, i32* %i, align 4, !tbaa !6
  %inc36 = add nsw i32 %56, 1
  store i32 %inc36, i32* %i, align 4, !tbaa !6
  br label %for.cond30

for.end37:                                        ; preds = %for.cond30
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %marker_list = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 60
  store %struct.jpeg_marker_struct* null, %struct.jpeg_marker_struct** %marker_list, align 8, !tbaa !23
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_marker_reader(%struct.jpeg_decompress_struct* %58)
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_input_controller(%struct.jpeg_decompress_struct* %59)
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %60, i32 0, i32 5
  store i32 200, i32* %global_state, align 4, !tbaa !24
  %61 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %61, i32 0, i32 1
  %62 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem38, align 4, !tbaa !10
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %62, i32 0, i32 0
  %63 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !25
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %65 = bitcast %struct.jpeg_decompress_struct* %64 to %struct.jpeg_common_struct*
  %call = call i8* %63(%struct.jpeg_common_struct* %65, i32 0, i32 120)
  %66 = bitcast i8* %call to %struct.jpeg_decomp_master*
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %67, i32 0, i32 77
  store %struct.jpeg_decomp_master* %66, %struct.jpeg_decomp_master** %master, align 4, !tbaa !27
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master39 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %68, i32 0, i32 77
  %69 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master39, align 4, !tbaa !27
  %70 = bitcast %struct.jpeg_decomp_master* %69 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %70, i8 0, i32 120, i1 false)
  %71 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @jinit_memory_mgr(%struct.jpeg_common_struct*) #3

declare void @jinit_marker_reader(%struct.jpeg_decompress_struct*) #3

declare void @jinit_input_controller(%struct.jpeg_decompress_struct*) #3

; Function Attrs: nounwind
define hidden void @jpeg_destroy_decompress(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = bitcast %struct.jpeg_decompress_struct* %0 to %struct.jpeg_common_struct*
  call void @jpeg_destroy(%struct.jpeg_common_struct* %1)
  ret void
}

declare void @jpeg_destroy(%struct.jpeg_common_struct*) #3

; Function Attrs: nounwind
define hidden void @jpeg_abort_decompress(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = bitcast %struct.jpeg_decompress_struct* %0 to %struct.jpeg_common_struct*
  call void @jpeg_abort(%struct.jpeg_common_struct* %1)
  ret void
}

declare void @jpeg_abort(%struct.jpeg_common_struct*) #3

; Function Attrs: nounwind
define hidden i32 @jpeg_read_header(%struct.jpeg_decompress_struct* %cinfo, i32 %require_image) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %require_image.addr = alloca i32, align 4
  %retcode = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %require_image, i32* %require_image.addr, align 4, !tbaa !6
  %0 = bitcast i32* %retcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 5
  %2 = load i32, i32* %global_state, align 4, !tbaa !24
  %cmp = icmp ne i32 %2, 200
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 5
  %4 = load i32, i32* %global_state1, align 4, !tbaa !24
  %cmp2 = icmp ne i32 %4, 201
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !15
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 5
  %8 = load i32, i32* %global_state3, align 4, !tbaa !24
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %8, i32* %arrayidx, align 4, !tbaa !17
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 0
  %13 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = bitcast %struct.jpeg_decompress_struct* %14 to %struct.jpeg_common_struct*
  call void %13(%struct.jpeg_common_struct* %15)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @jpeg_consume_input(%struct.jpeg_decompress_struct* %16)
  store i32 %call, i32* %retcode, align 4, !tbaa !6
  %17 = load i32, i32* %retcode, align 4, !tbaa !6
  switch i32 %17, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb6
    i32 0, label %sw.epilog
  ]

sw.bb:                                            ; preds = %if.end
  store i32 1, i32* %retcode, align 4, !tbaa !6
  br label %sw.epilog

sw.bb6:                                           ; preds = %if.end
  %18 = load i32, i32* %require_image.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %18, 0
  br i1 %tobool, label %if.then7, label %if.end12

if.then7:                                         ; preds = %sw.bb6
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !14
  %msg_code9 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 5
  store i32 51, i32* %msg_code9, align 4, !tbaa !15
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !14
  %error_exit11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 0
  %23 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit11, align 4, !tbaa !18
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = bitcast %struct.jpeg_decompress_struct* %24 to %struct.jpeg_common_struct*
  call void %23(%struct.jpeg_common_struct* %25)
  br label %if.end12

if.end12:                                         ; preds = %if.then7, %sw.bb6
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = bitcast %struct.jpeg_decompress_struct* %26 to %struct.jpeg_common_struct*
  call void @jpeg_abort(%struct.jpeg_common_struct* %27)
  store i32 2, i32* %retcode, align 4, !tbaa !6
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end, %if.end, %if.end12, %sw.bb
  %28 = load i32, i32* %retcode, align 4, !tbaa !6
  %29 = bitcast i32* %retcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #4
  ret i32 %28
}

; Function Attrs: nounwind
define hidden i32 @jpeg_consume_input(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %retcode = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %retcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 0, i32* %retcode, align 4, !tbaa !6
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 5
  %2 = load i32, i32* %global_state, align 4, !tbaa !24
  switch i32 %2, label %sw.default [
    i32 200, label %sw.bb
    i32 201, label %sw.bb2
    i32 202, label %sw.bb5
    i32 203, label %sw.bb6
    i32 204, label %sw.bb6
    i32 205, label %sw.bb6
    i32 206, label %sw.bb6
    i32 207, label %sw.bb6
    i32 208, label %sw.bb6
    i32 210, label %sw.bb6
  ]

sw.bb:                                            ; preds = %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 81
  %4 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !28
  %reset_input_controller = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %4, i32 0, i32 1
  %5 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %reset_input_controller, align 4, !tbaa !29
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %5(%struct.jpeg_decompress_struct* %6)
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 6
  %8 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !22
  %init_source = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %8, i32 0, i32 2
  %9 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %init_source, align 4, !tbaa !31
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %9(%struct.jpeg_decompress_struct* %10)
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 5
  store i32 201, i32* %global_state1, align 4, !tbaa !24
  br label %sw.bb2

sw.bb2:                                           ; preds = %entry, %sw.bb
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 81
  %13 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl3, align 4, !tbaa !28
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %13, i32 0, i32 0
  %14 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !33
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %14(%struct.jpeg_decompress_struct* %15)
  store i32 %call, i32* %retcode, align 4, !tbaa !6
  %16 = load i32, i32* %retcode, align 4, !tbaa !6
  %cmp = icmp eq i32 %16, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %sw.bb2
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @default_decompress_parms(%struct.jpeg_decompress_struct* %17)
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 5
  store i32 202, i32* %global_state4, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.then, %sw.bb2
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry
  store i32 1, i32* %retcode, align 4, !tbaa !6
  br label %sw.epilog

sw.bb6:                                           ; preds = %entry, %entry, %entry, %entry, %entry, %entry, %entry
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 81
  %20 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl7, align 4, !tbaa !28
  %consume_input8 = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %20, i32 0, i32 0
  %21 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_input8, align 4, !tbaa !33
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call9 = call i32 %21(%struct.jpeg_decompress_struct* %22)
  store i32 %call9, i32* %retcode, align 4, !tbaa !6
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !15
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 5
  %26 = load i32, i32* %global_state10, align 4, !tbaa !24
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err11, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %26, i32* %arrayidx, align 4, !tbaa !17
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 0
  %30 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %30, i32 0, i32 0
  %31 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = bitcast %struct.jpeg_decompress_struct* %32 to %struct.jpeg_common_struct*
  call void %31(%struct.jpeg_common_struct* %33)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb6, %sw.bb5, %if.end
  %34 = load i32, i32* %retcode, align 4, !tbaa !6
  %35 = bitcast i32* %retcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #4
  ret i32 %34
}

; Function Attrs: nounwind
define internal void @default_decompress_parms(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cid0 = alloca i32, align 4
  %cid1 = alloca i32, align 4
  %cid2 = alloca i32, align 4
  %_mp = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 9
  %1 = load i32, i32* %num_components, align 4, !tbaa !34
  switch i32 %1, label %sw.default84 [
    i32 1, label %sw.bb
    i32 3, label %sw.bb1
    i32 4, label %sw.bb57
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 10
  store i32 1, i32* %jpeg_color_space, align 8, !tbaa !35
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 11
  store i32 1, i32* %out_color_space, align 4, !tbaa !36
  br label %sw.epilog87

sw.bb1:                                           ; preds = %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %saw_JFIF_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 51
  %5 = load i32, i32* %saw_JFIF_marker, align 8, !tbaa !37
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb1
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 10
  store i32 3, i32* %jpeg_color_space2, align 8, !tbaa !35
  br label %if.end55

if.else:                                          ; preds = %sw.bb1
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %saw_Adobe_marker = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 57
  %8 = load i32, i32* %saw_Adobe_marker, align 4, !tbaa !38
  %tobool3 = icmp ne i32 %8, 0
  br i1 %tobool3, label %if.then4, label %if.else14

if.then4:                                         ; preds = %if.else
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Adobe_transform = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 58
  %10 = load i8, i8* %Adobe_transform, align 8, !tbaa !39
  %conv = zext i8 %10 to i32
  switch i32 %conv, label %sw.default [
    i32 0, label %sw.bb5
    i32 1, label %sw.bb7
  ]

sw.bb5:                                           ; preds = %if.then4
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 10
  store i32 2, i32* %jpeg_color_space6, align 8, !tbaa !35
  br label %sw.epilog

sw.bb7:                                           ; preds = %if.then4
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 10
  store i32 3, i32* %jpeg_color_space8, align 8, !tbaa !35
  br label %sw.epilog

sw.default:                                       ; preds = %if.then4
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 5
  store i32 114, i32* %msg_code, align 4, !tbaa !15
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Adobe_transform9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 58
  %16 = load i8, i8* %Adobe_transform9, align 8, !tbaa !39
  %conv10 = zext i8 %16 to i32
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err11, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %conv10, i32* %arrayidx, align 4, !tbaa !17
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !14
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 1
  %21 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !40
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = bitcast %struct.jpeg_decompress_struct* %22 to %struct.jpeg_common_struct*
  call void %21(%struct.jpeg_common_struct* %23, i32 -1)
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 10
  store i32 3, i32* %jpeg_color_space13, align 8, !tbaa !35
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb7, %sw.bb5
  br label %if.end54

if.else14:                                        ; preds = %if.else
  %25 = bitcast i32* %cid0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #4
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 44
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !41
  %arrayidx15 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %27, i32 0
  %component_id = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx15, i32 0, i32 0
  %28 = load i32, i32* %component_id, align 4, !tbaa !42
  store i32 %28, i32* %cid0, align 4, !tbaa !6
  %29 = bitcast i32* %cid1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 44
  %31 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info16, align 8, !tbaa !41
  %arrayidx17 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %31, i32 1
  %component_id18 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx17, i32 0, i32 0
  %32 = load i32, i32* %component_id18, align 4, !tbaa !42
  store i32 %32, i32* %cid1, align 4, !tbaa !6
  %33 = bitcast i32* %cid2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 44
  %35 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info19, align 8, !tbaa !41
  %arrayidx20 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %35, i32 2
  %component_id21 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %arrayidx20, i32 0, i32 0
  %36 = load i32, i32* %component_id21, align 4, !tbaa !42
  store i32 %36, i32* %cid2, align 4, !tbaa !6
  %37 = load i32, i32* %cid0, align 4, !tbaa !6
  %cmp = icmp eq i32 %37, 1
  br i1 %cmp, label %land.lhs.true, label %if.else30

land.lhs.true:                                    ; preds = %if.else14
  %38 = load i32, i32* %cid1, align 4, !tbaa !6
  %cmp23 = icmp eq i32 %38, 2
  br i1 %cmp23, label %land.lhs.true25, label %if.else30

land.lhs.true25:                                  ; preds = %land.lhs.true
  %39 = load i32, i32* %cid2, align 4, !tbaa !6
  %cmp26 = icmp eq i32 %39, 3
  br i1 %cmp26, label %if.then28, label %if.else30

if.then28:                                        ; preds = %land.lhs.true25
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 10
  store i32 3, i32* %jpeg_color_space29, align 8, !tbaa !35
  br label %if.end53

if.else30:                                        ; preds = %land.lhs.true25, %land.lhs.true, %if.else14
  %41 = load i32, i32* %cid0, align 4, !tbaa !6
  %cmp31 = icmp eq i32 %41, 82
  br i1 %cmp31, label %land.lhs.true33, label %if.else41

land.lhs.true33:                                  ; preds = %if.else30
  %42 = load i32, i32* %cid1, align 4, !tbaa !6
  %cmp34 = icmp eq i32 %42, 71
  br i1 %cmp34, label %land.lhs.true36, label %if.else41

land.lhs.true36:                                  ; preds = %land.lhs.true33
  %43 = load i32, i32* %cid2, align 4, !tbaa !6
  %cmp37 = icmp eq i32 %43, 66
  br i1 %cmp37, label %if.then39, label %if.else41

if.then39:                                        ; preds = %land.lhs.true36
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 10
  store i32 2, i32* %jpeg_color_space40, align 8, !tbaa !35
  br label %if.end

if.else41:                                        ; preds = %land.lhs.true36, %land.lhs.true33, %if.else30
  br label %do.body

do.body:                                          ; preds = %if.else41
  %45 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #4
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err42 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 0
  %47 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err42, align 8, !tbaa !14
  %msg_parm43 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %47, i32 0, i32 6
  %i44 = bitcast %union.anon* %msg_parm43 to [8 x i32]*
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %i44, i32 0, i32 0
  store i32* %arraydecay, i32** %_mp, align 4, !tbaa !2
  %48 = load i32, i32* %cid0, align 4, !tbaa !6
  %49 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx45 = getelementptr inbounds i32, i32* %49, i32 0
  store i32 %48, i32* %arrayidx45, align 4, !tbaa !6
  %50 = load i32, i32* %cid1, align 4, !tbaa !6
  %51 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i32, i32* %51, i32 1
  store i32 %50, i32* %arrayidx46, align 4, !tbaa !6
  %52 = load i32, i32* %cid2, align 4, !tbaa !6
  %53 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i32, i32* %53, i32 2
  store i32 %52, i32* %arrayidx47, align 4, !tbaa !6
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 0
  %55 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err48, align 8, !tbaa !14
  %msg_code49 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %55, i32 0, i32 5
  store i32 111, i32* %msg_code49, align 4, !tbaa !15
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err50 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 0
  %57 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err50, align 8, !tbaa !14
  %emit_message51 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %57, i32 0, i32 1
  %58 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message51, align 4, !tbaa !40
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %60 = bitcast %struct.jpeg_decompress_struct* %59 to %struct.jpeg_common_struct*
  call void %58(%struct.jpeg_common_struct* %60, i32 1)
  %61 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %do.cond
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space52 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %62, i32 0, i32 10
  store i32 3, i32* %jpeg_color_space52, align 8, !tbaa !35
  br label %if.end

if.end:                                           ; preds = %do.end, %if.then39
  br label %if.end53

if.end53:                                         ; preds = %if.end, %if.then28
  %63 = bitcast i32* %cid2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  %64 = bitcast i32* %cid1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #4
  %65 = bitcast i32* %cid0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #4
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %sw.epilog
  br label %if.end55

if.end55:                                         ; preds = %if.end54, %if.then
  %66 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space56 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %66, i32 0, i32 11
  store i32 2, i32* %out_color_space56, align 4, !tbaa !36
  br label %sw.epilog87

sw.bb57:                                          ; preds = %entry
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %saw_Adobe_marker58 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %67, i32 0, i32 57
  %68 = load i32, i32* %saw_Adobe_marker58, align 4, !tbaa !38
  %tobool59 = icmp ne i32 %68, 0
  br i1 %tobool59, label %if.then60, label %if.else80

if.then60:                                        ; preds = %sw.bb57
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Adobe_transform61 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %69, i32 0, i32 58
  %70 = load i8, i8* %Adobe_transform61, align 8, !tbaa !39
  %conv62 = zext i8 %70 to i32
  switch i32 %conv62, label %sw.default67 [
    i32 0, label %sw.bb63
    i32 2, label %sw.bb65
  ]

sw.bb63:                                          ; preds = %if.then60
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space64 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 10
  store i32 4, i32* %jpeg_color_space64, align 8, !tbaa !35
  br label %sw.epilog79

sw.bb65:                                          ; preds = %if.then60
  %72 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space66 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %72, i32 0, i32 10
  store i32 5, i32* %jpeg_color_space66, align 8, !tbaa !35
  br label %sw.epilog79

sw.default67:                                     ; preds = %if.then60
  %73 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err68 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %73, i32 0, i32 0
  %74 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err68, align 8, !tbaa !14
  %msg_code69 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %74, i32 0, i32 5
  store i32 114, i32* %msg_code69, align 4, !tbaa !15
  %75 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Adobe_transform70 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %75, i32 0, i32 58
  %76 = load i8, i8* %Adobe_transform70, align 8, !tbaa !39
  %conv71 = zext i8 %76 to i32
  %77 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err72 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %77, i32 0, i32 0
  %78 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err72, align 8, !tbaa !14
  %msg_parm73 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %78, i32 0, i32 6
  %i74 = bitcast %union.anon* %msg_parm73 to [8 x i32]*
  %arrayidx75 = getelementptr inbounds [8 x i32], [8 x i32]* %i74, i32 0, i32 0
  store i32 %conv71, i32* %arrayidx75, align 4, !tbaa !17
  %79 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err76 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %79, i32 0, i32 0
  %80 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err76, align 8, !tbaa !14
  %emit_message77 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %80, i32 0, i32 1
  %81 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message77, align 4, !tbaa !40
  %82 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %83 = bitcast %struct.jpeg_decompress_struct* %82 to %struct.jpeg_common_struct*
  call void %81(%struct.jpeg_common_struct* %83, i32 -1)
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space78 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %84, i32 0, i32 10
  store i32 5, i32* %jpeg_color_space78, align 8, !tbaa !35
  br label %sw.epilog79

sw.epilog79:                                      ; preds = %sw.default67, %sw.bb65, %sw.bb63
  br label %if.end82

if.else80:                                        ; preds = %sw.bb57
  %85 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space81 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %85, i32 0, i32 10
  store i32 4, i32* %jpeg_color_space81, align 8, !tbaa !35
  br label %if.end82

if.end82:                                         ; preds = %if.else80, %sw.epilog79
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space83 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 11
  store i32 4, i32* %out_color_space83, align 4, !tbaa !36
  br label %sw.epilog87

sw.default84:                                     ; preds = %entry
  %87 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %jpeg_color_space85 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %87, i32 0, i32 10
  store i32 0, i32* %jpeg_color_space85, align 8, !tbaa !35
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space86 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %88, i32 0, i32 11
  store i32 0, i32* %out_color_space86, align 4, !tbaa !36
  br label %sw.epilog87

sw.epilog87:                                      ; preds = %sw.default84, %if.end82, %if.end55, %sw.bb
  %89 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_num = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %89, i32 0, i32 12
  store i32 1, i32* %scale_num, align 8, !tbaa !44
  %90 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %scale_denom = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %90, i32 0, i32 13
  store i32 1, i32* %scale_denom, align 4, !tbaa !45
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_gamma = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %91, i32 0, i32 14
  store double 1.000000e+00, double* %output_gamma, align 8, !tbaa !46
  %92 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %92, i32 0, i32 15
  store i32 0, i32* %buffered_image, align 8, !tbaa !47
  %93 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %raw_data_out = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %93, i32 0, i32 16
  store i32 0, i32* %raw_data_out, align 4, !tbaa !48
  %94 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dct_method = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %94, i32 0, i32 17
  store i32 0, i32* %dct_method, align 8, !tbaa !49
  %95 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %do_fancy_upsampling = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %95, i32 0, i32 18
  store i32 1, i32* %do_fancy_upsampling, align 4, !tbaa !50
  %96 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %do_block_smoothing = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %96, i32 0, i32 19
  store i32 1, i32* %do_block_smoothing, align 8, !tbaa !51
  %97 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %quantize_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %97, i32 0, i32 20
  store i32 0, i32* %quantize_colors, align 4, !tbaa !52
  %98 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %98, i32 0, i32 21
  store i32 2, i32* %dither_mode, align 8, !tbaa !53
  %99 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %two_pass_quantize = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %99, i32 0, i32 22
  store i32 1, i32* %two_pass_quantize, align 4, !tbaa !54
  %100 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %desired_number_of_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %100, i32 0, i32 23
  store i32 256, i32* %desired_number_of_colors, align 8, !tbaa !55
  %101 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %101, i32 0, i32 33
  store i8** null, i8*** %colormap, align 8, !tbaa !56
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_1pass_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 24
  store i32 0, i32* %enable_1pass_quant, align 4, !tbaa !57
  %103 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_external_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %103, i32 0, i32 25
  store i32 0, i32* %enable_external_quant, align 8, !tbaa !58
  %104 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %enable_2pass_quant = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %104, i32 0, i32 26
  store i32 0, i32* %enable_2pass_quant, align 4, !tbaa !59
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jpeg_input_complete(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !24
  %cmp = icmp slt i32 %1, 200
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 5
  %3 = load i32, i32* %global_state1, align 4, !tbaa !24
  %cmp2 = icmp sgt i32 %3, 210
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 0
  %5 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %5, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !15
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 5
  %7 = load i32, i32* %global_state3, align 4, !tbaa !24
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %7, i32* %arrayidx, align 4, !tbaa !17
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_decompress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 81
  %16 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !28
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %16, i32 0, i32 5
  %17 = load i32, i32* %eoi_reached, align 4, !tbaa !60
  ret i32 %17
}

; Function Attrs: nounwind
define hidden i32 @jpeg_has_multiple_scans(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !24
  %cmp = icmp slt i32 %1, 202
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 5
  %3 = load i32, i32* %global_state1, align 4, !tbaa !24
  %cmp2 = icmp sgt i32 %3, 210
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 0
  %5 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %5, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !15
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 5
  %7 = load i32, i32* %global_state3, align 4, !tbaa !24
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %7, i32* %arrayidx, align 4, !tbaa !17
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_decompress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 81
  %16 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !28
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %16, i32 0, i32 4
  %17 = load i32, i32* %has_multiple_scans, align 4, !tbaa !61
  ret i32 %17
}

; Function Attrs: nounwind
define hidden i32 @jpeg_finish_decompress(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !24
  %cmp = icmp eq i32 %1, 205
  br i1 %cmp, label %land.lhs.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 5
  %3 = load i32, i32* %global_state1, align 4, !tbaa !24
  %cmp2 = icmp eq i32 %3, 206
  br i1 %cmp2, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %lor.lhs.false, %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 15
  %5 = load i32, i32* %buffered_image, align 8, !tbaa !47
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 34
  %7 = load i32, i32* %output_scanline, align 4, !tbaa !62
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 28
  %9 = load i32, i32* %output_height, align 4, !tbaa !63
  %cmp3 = icmp ult i32 %7, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !14
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 5
  store i32 67, i32* %msg_code, align 4, !tbaa !15
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !14
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 0
  %14 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !18
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_decompress_struct* %15 to %struct.jpeg_common_struct*
  call void %14(%struct.jpeg_common_struct* %16)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 77
  %18 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !27
  %finish_output_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %18, i32 0, i32 1
  %19 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %finish_output_pass, align 4, !tbaa !64
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %19(%struct.jpeg_decompress_struct* %20)
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 5
  store i32 210, i32* %global_state6, align 4, !tbaa !24
  br label %if.end23

if.else:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 5
  %23 = load i32, i32* %global_state7, align 4, !tbaa !24
  %cmp8 = icmp eq i32 %23, 207
  br i1 %cmp8, label %if.then9, label %if.else11

if.then9:                                         ; preds = %if.else
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 5
  store i32 210, i32* %global_state10, align 4, !tbaa !24
  br label %if.end22

if.else11:                                        ; preds = %if.else
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 5
  %26 = load i32, i32* %global_state12, align 4, !tbaa !24
  %cmp13 = icmp ne i32 %26, 210
  br i1 %cmp13, label %if.then14, label %if.end21

if.then14:                                        ; preds = %if.else11
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 8, !tbaa !14
  %msg_code16 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 5
  store i32 20, i32* %msg_code16, align 4, !tbaa !15
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 5
  %30 = load i32, i32* %global_state17, align 4, !tbaa !24
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 0
  %32 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err18, align 8, !tbaa !14
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %32, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %30, i32* %arrayidx, align 4, !tbaa !17
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 0
  %34 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err19, align 8, !tbaa !14
  %error_exit20 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %34, i32 0, i32 0
  %35 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit20, align 4, !tbaa !18
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %37 = bitcast %struct.jpeg_decompress_struct* %36 to %struct.jpeg_common_struct*
  call void %35(%struct.jpeg_common_struct* %37)
  br label %if.end21

if.end21:                                         ; preds = %if.then14, %if.else11
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %if.then9
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.end
  br label %while.cond

while.cond:                                       ; preds = %if.end28, %if.end23
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 81
  %39 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !28
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %39, i32 0, i32 5
  %40 = load i32, i32* %eoi_reached, align 4, !tbaa !60
  %tobool24 = icmp ne i32 %40, 0
  %lnot = xor i1 %tobool24, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 81
  %42 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl25, align 4, !tbaa !28
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %42, i32 0, i32 0
  %43 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !33
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %43(%struct.jpeg_decompress_struct* %44)
  %cmp26 = icmp eq i32 %call, 0
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %while.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %src = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 6
  %46 = load %struct.jpeg_source_mgr*, %struct.jpeg_source_mgr** %src, align 8, !tbaa !22
  %term_source = getelementptr inbounds %struct.jpeg_source_mgr, %struct.jpeg_source_mgr* %46, i32 0, i32 6
  %47 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %term_source, align 4, !tbaa !66
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %47(%struct.jpeg_decompress_struct* %48)
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %50 = bitcast %struct.jpeg_decompress_struct* %49 to %struct.jpeg_common_struct*
  call void @jpeg_abort(%struct.jpeg_common_struct* %50)
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then27
  %51 = load i32, i32* %retval, align 4
  ret i32 %51
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!11, !3, i64 4}
!11 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !12, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !13, i64 288, !13, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!12 = !{!"double", !4, i64 0}
!13 = !{!"short", !4, i64 0}
!14 = !{!11, !3, i64 0}
!15 = !{!16, !7, i64 20}
!16 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !9, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!17 = !{!4, !4, i64 0}
!18 = !{!16, !3, i64 0}
!19 = !{!11, !3, i64 12}
!20 = !{!11, !7, i64 16}
!21 = !{!11, !3, i64 8}
!22 = !{!11, !3, i64 24}
!23 = !{!11, !3, i64 304}
!24 = !{!11, !7, i64 20}
!25 = !{!26, !3, i64 0}
!26 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !9, i64 44, !9, i64 48}
!27 = !{!11, !3, i64 420}
!28 = !{!11, !3, i64 436}
!29 = !{!30, !3, i64 4}
!30 = !{!"jpeg_input_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20}
!31 = !{!32, !3, i64 8}
!32 = !{!"jpeg_source_mgr", !3, i64 0, !9, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24}
!33 = !{!30, !3, i64 0}
!34 = !{!11, !7, i64 36}
!35 = !{!11, !4, i64 40}
!36 = !{!11, !4, i64 44}
!37 = !{!11, !7, i64 280}
!38 = !{!11, !7, i64 292}
!39 = !{!11, !4, i64 296}
!40 = !{!16, !3, i64 4}
!41 = !{!11, !3, i64 216}
!42 = !{!43, !7, i64 0}
!43 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!44 = !{!11, !7, i64 48}
!45 = !{!11, !7, i64 52}
!46 = !{!11, !12, i64 56}
!47 = !{!11, !7, i64 64}
!48 = !{!11, !7, i64 68}
!49 = !{!11, !4, i64 72}
!50 = !{!11, !7, i64 76}
!51 = !{!11, !7, i64 80}
!52 = !{!11, !7, i64 84}
!53 = !{!11, !4, i64 88}
!54 = !{!11, !7, i64 92}
!55 = !{!11, !7, i64 96}
!56 = !{!11, !3, i64 136}
!57 = !{!11, !7, i64 100}
!58 = !{!11, !7, i64 104}
!59 = !{!11, !7, i64 108}
!60 = !{!30, !7, i64 20}
!61 = !{!30, !7, i64 16}
!62 = !{!11, !7, i64 140}
!63 = !{!11, !7, i64 116}
!64 = !{!65, !3, i64 4}
!65 = !{!"jpeg_decomp_master", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 60, !7, i64 100}
!66 = !{!32, !3, i64 24}
