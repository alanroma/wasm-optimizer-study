; ModuleID = 'jdapistd.c'
source_filename = "jdapistd.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, {}*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, {}*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { {}*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, {}*, {}*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.my_main_controller = type { %struct.jpeg_d_main_controller, [10 x i8**], i32, i32, [2 x i8***], i32, i32, i32, i32 }
%struct.my_coef_controller = type { %struct.jpeg_d_coef_controller, i32, i32, i32, [10 x [64 x i16]*], i16*, [10 x %struct.jvirt_barray_control*], i32* }
%struct.my_upsampler = type { %struct.jpeg_upsampler, [10 x i8**], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i8**, i8***)*], i32, i32, [10 x i32], [10 x i8], [10 x i8] }

; Function Attrs: nounwind
define hidden i32 @jpeg_start_decompress(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %retcode = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp eq i32 %1, 202
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_master_decompress(%struct.jpeg_decompress_struct* %2)
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 15
  %4 = load i32, i32* %buffered_image, align 8, !tbaa !11
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 5
  store i32 207, i32* %global_state2, align 4, !tbaa !6
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 5
  store i32 203, i32* %global_state3, align 4, !tbaa !6
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 5
  %8 = load i32, i32* %global_state5, align 4, !tbaa !6
  %cmp6 = icmp eq i32 %8, 203
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end4
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 81
  %10 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !12
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %10, i32 0, i32 4
  %11 = load i32, i32* %has_multiple_scans, align 4, !tbaa !13
  %tobool8 = icmp ne i32 %11, 0
  br i1 %tobool8, label %if.then9, label %if.end36

if.then9:                                         ; preds = %if.then7
  br label %for.cond

for.cond:                                         ; preds = %cleanup.cont, %if.then9
  %12 = bitcast i32* %retcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 2
  %14 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !15
  %cmp10 = icmp ne %struct.jpeg_progress_mgr* %14, null
  br i1 %cmp10, label %if.then11, label %if.end14

if.then11:                                        ; preds = %for.cond
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 2
  %16 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress12, align 8, !tbaa !15
  %progress_monitor = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %16, i32 0, i32 0
  %progress_monitor13 = bitcast {}** %progress_monitor to void (%struct.jpeg_common_struct*)**
  %17 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %progress_monitor13, align 4, !tbaa !16
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = bitcast %struct.jpeg_decompress_struct* %18 to %struct.jpeg_common_struct*
  call void %17(%struct.jpeg_common_struct* %19)
  br label %if.end14

if.end14:                                         ; preds = %if.then11, %for.cond
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 81
  %21 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl15, align 4, !tbaa !12
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %21, i32 0, i32 0
  %consume_input16 = bitcast {}** %consume_input to i32 (%struct.jpeg_decompress_struct*)**
  %22 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_input16, align 4, !tbaa !19
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %22(%struct.jpeg_decompress_struct* %23)
  store i32 %call, i32* %retcode, align 4, !tbaa !20
  %24 = load i32, i32* %retcode, align 4, !tbaa !20
  %cmp17 = icmp eq i32 %24, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end14
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.end14
  %25 = load i32, i32* %retcode, align 4, !tbaa !20
  %cmp20 = icmp eq i32 %25, 2
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end19
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %if.end19
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress23 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 2
  %27 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress23, align 8, !tbaa !15
  %cmp24 = icmp ne %struct.jpeg_progress_mgr* %27, null
  br i1 %cmp24, label %land.lhs.true, label %if.end35

land.lhs.true:                                    ; preds = %if.end22
  %28 = load i32, i32* %retcode, align 4, !tbaa !20
  %cmp25 = icmp eq i32 %28, 3
  br i1 %cmp25, label %if.then27, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %29 = load i32, i32* %retcode, align 4, !tbaa !20
  %cmp26 = icmp eq i32 %29, 1
  br i1 %cmp26, label %if.then27, label %if.end35

if.then27:                                        ; preds = %lor.lhs.false, %land.lhs.true
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress28 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 2
  %31 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress28, align 8, !tbaa !15
  %pass_counter = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %31, i32 0, i32 1
  %32 = load i32, i32* %pass_counter, align 4, !tbaa !21
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %pass_counter, align 4, !tbaa !21
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress29 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 2
  %34 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress29, align 8, !tbaa !15
  %pass_limit = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %34, i32 0, i32 2
  %35 = load i32, i32* %pass_limit, align 4, !tbaa !22
  %cmp30 = icmp sge i32 %inc, %35
  br i1 %cmp30, label %if.then31, label %if.end34

if.then31:                                        ; preds = %if.then27
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 64
  %37 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !23
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress32 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 2
  %39 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress32, align 8, !tbaa !15
  %pass_limit33 = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %39, i32 0, i32 2
  %40 = load i32, i32* %pass_limit33, align 4, !tbaa !22
  %add = add nsw i32 %40, %37
  store i32 %add, i32* %pass_limit33, align 4, !tbaa !22
  br label %if.end34

if.end34:                                         ; preds = %if.then31, %if.then27
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %lor.lhs.false, %if.end22
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end35, %if.then21, %if.then18
  %41 = bitcast i32* %retcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
    i32 2, label %for.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.cond

for.end:                                          ; preds = %cleanup
  br label %if.end36

if.end36:                                         ; preds = %for.end, %if.then7
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 35
  %43 = load i32, i32* %input_scan_number, align 8, !tbaa !24
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 37
  store i32 %43, i32* %output_scan_number, align 8, !tbaa !25
  br label %if.end44

if.else:                                          ; preds = %if.end4
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state37 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 5
  %46 = load i32, i32* %global_state37, align 4, !tbaa !6
  %cmp38 = icmp ne i32 %46, 204
  br i1 %cmp38, label %if.then39, label %if.end43

if.then39:                                        ; preds = %if.else
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 0
  %48 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %48, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !27
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 5
  %50 = load i32, i32* %global_state40, align 4, !tbaa !6
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err41 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 0
  %52 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err41, align 8, !tbaa !26
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %52, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %50, i32* %arrayidx, align 4, !tbaa !29
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err42 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 0
  %54 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err42, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %54, i32 0, i32 0
  %55 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %57 = bitcast %struct.jpeg_decompress_struct* %56 to %struct.jpeg_common_struct*
  call void %55(%struct.jpeg_common_struct* %57)
  br label %if.end43

if.end43:                                         ; preds = %if.then39, %if.else
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %if.end36
  %58 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call45 = call i32 @output_pass_setup(%struct.jpeg_decompress_struct* %58)
  store i32 %call45, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end44, %cleanup, %if.then1
  %59 = load i32, i32* %retval, align 4
  ret i32 %59

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare void @jinit_master_decompress(%struct.jpeg_decompress_struct*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define internal i32 @output_pass_setup(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %last_scanline = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp ne i32 %1, 204
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 77
  %3 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !31
  %prepare_for_output_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %3, i32 0, i32 0
  %4 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %prepare_for_output_pass, align 4, !tbaa !32
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %4(%struct.jpeg_decompress_struct* %5)
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 34
  store i32 0, i32* %output_scanline, align 4, !tbaa !34
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 5
  store i32 204, i32* %global_state1, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.end, %if.end
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 77
  %9 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master2, align 4, !tbaa !31
  %is_dummy_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %9, i32 0, i32 2
  %10 = load i32, i32* %is_dummy_pass, align 4, !tbaa !35
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %while.body, label %while.end26

while.body:                                       ; preds = %while.cond
  br label %while.cond3

while.cond3:                                      ; preds = %cleanup.cont, %while.body
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 34
  %12 = load i32, i32* %output_scanline4, align 4, !tbaa !34
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 28
  %14 = load i32, i32* %output_height, align 4, !tbaa !36
  %cmp5 = icmp ult i32 %12, %14
  br i1 %cmp5, label %while.body6, label %while.end

while.body6:                                      ; preds = %while.cond3
  %15 = bitcast i32* %last_scanline to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 2
  %17 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !15
  %cmp7 = icmp ne %struct.jpeg_progress_mgr* %17, null
  br i1 %cmp7, label %if.then8, label %if.end15

if.then8:                                         ; preds = %while.body6
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 34
  %19 = load i32, i32* %output_scanline9, align 4, !tbaa !34
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 2
  %21 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress10, align 8, !tbaa !15
  %pass_counter = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %21, i32 0, i32 1
  store i32 %19, i32* %pass_counter, align 4, !tbaa !21
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 28
  %23 = load i32, i32* %output_height11, align 4, !tbaa !36
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 2
  %25 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress12, align 8, !tbaa !15
  %pass_limit = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %25, i32 0, i32 2
  store i32 %23, i32* %pass_limit, align 4, !tbaa !22
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 2
  %27 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress13, align 8, !tbaa !15
  %progress_monitor = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %27, i32 0, i32 0
  %progress_monitor14 = bitcast {}** %progress_monitor to void (%struct.jpeg_common_struct*)**
  %28 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %progress_monitor14, align 4, !tbaa !16
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %30 = bitcast %struct.jpeg_decompress_struct* %29 to %struct.jpeg_common_struct*
  call void %28(%struct.jpeg_common_struct* %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then8, %while.body6
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 34
  %32 = load i32, i32* %output_scanline16, align 4, !tbaa !34
  store i32 %32, i32* %last_scanline, align 4, !tbaa !20
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 78
  %34 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !37
  %process_data = getelementptr inbounds %struct.jpeg_d_main_controller, %struct.jpeg_d_main_controller* %34, i32 0, i32 1
  %35 = load void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)** %process_data, align 4, !tbaa !38
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 34
  call void %35(%struct.jpeg_decompress_struct* %36, i8** null, i32* %output_scanline17, i32 0)
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 34
  %39 = load i32, i32* %output_scanline18, align 4, !tbaa !34
  %40 = load i32, i32* %last_scanline, align 4, !tbaa !20
  %cmp19 = icmp eq i32 %39, %40
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end15
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end21:                                         ; preds = %if.end15
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end21, %if.then20
  %41 = bitcast i32* %last_scanline to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond3

while.end:                                        ; preds = %while.cond3
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 77
  %43 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master22, align 4, !tbaa !31
  %finish_output_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %43, i32 0, i32 1
  %44 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %finish_output_pass, align 4, !tbaa !40
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %44(%struct.jpeg_decompress_struct* %45)
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master23 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 77
  %47 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master23, align 4, !tbaa !31
  %prepare_for_output_pass24 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %47, i32 0, i32 0
  %48 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %prepare_for_output_pass24, align 4, !tbaa !32
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %48(%struct.jpeg_decompress_struct* %49)
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 34
  store i32 0, i32* %output_scanline25, align 4, !tbaa !34
  br label %while.cond

while.end26:                                      ; preds = %while.cond
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %raw_data_out = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 16
  %52 = load i32, i32* %raw_data_out, align 4, !tbaa !41
  %tobool27 = icmp ne i32 %52, 0
  %53 = zext i1 %tobool27 to i64
  %cond = select i1 %tobool27, i32 206, i32 205
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state28 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 5
  store i32 %cond, i32* %global_state28, align 4, !tbaa !6
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end26, %cleanup
  %55 = load i32, i32* %retval, align 4
  ret i32 %55

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define hidden void @jpeg_crop_scanline(%struct.jpeg_decompress_struct* %cinfo, i32* %xoffset, i32* %width) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %xoffset.addr = alloca i32*, align 4
  %width.addr = alloca i32*, align 4
  %ci = alloca i32, align 4
  %align = alloca i32, align 4
  %orig_downsampled_width = alloca i32, align 4
  %input_xoffset = alloca i32, align 4
  %reinit_upsampler = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32* %xoffset, i32** %xoffset.addr, align 4, !tbaa !2
  store i32* %width, i32** %width.addr, align 4, !tbaa !2
  %0 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %align to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %orig_downsampled_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %input_xoffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %reinit_upsampler to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  store i32 0, i32* %reinit_upsampler, align 4, !tbaa !20
  %5 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 5
  %7 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp ne i32 %7, 205
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 34
  %9 = load i32, i32* %output_scanline, align 4, !tbaa !34
  %cmp1 = icmp ne i32 %9, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !27
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 5
  %13 = load i32, i32* %global_state2, align 4, !tbaa !6
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !26
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %13, i32* %arrayidx, align 4, !tbaa !29
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 0
  %18 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %20 = bitcast %struct.jpeg_decompress_struct* %19 to %struct.jpeg_common_struct*
  call void %18(%struct.jpeg_common_struct* %20)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %21 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  %tobool = icmp ne i32* %21, null
  br i1 %tobool, label %lor.lhs.false5, label %if.then7

lor.lhs.false5:                                   ; preds = %if.end
  %22 = load i32*, i32** %width.addr, align 4, !tbaa !2
  %tobool6 = icmp ne i32* %22, null
  br i1 %tobool6, label %if.end12, label %if.then7

if.then7:                                         ; preds = %lor.lhs.false5, %if.end
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !26
  %msg_code9 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 5
  store i32 124, i32* %msg_code9, align 4, !tbaa !27
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 0
  %26 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !26
  %error_exit11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %26, i32 0, i32 0
  %27 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit11, align 4, !tbaa !30
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %29 = bitcast %struct.jpeg_decompress_struct* %28 to %struct.jpeg_common_struct*
  call void %27(%struct.jpeg_common_struct* %29)
  br label %if.end12

if.end12:                                         ; preds = %if.then7, %lor.lhs.false5
  %30 = load i32*, i32** %width.addr, align 4, !tbaa !2
  %31 = load i32, i32* %30, align 4, !tbaa !20
  %cmp13 = icmp eq i32 %31, 0
  br i1 %cmp13, label %if.then16, label %lor.lhs.false14

lor.lhs.false14:                                  ; preds = %if.end12
  %32 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  %33 = load i32, i32* %32, align 4, !tbaa !20
  %34 = load i32*, i32** %width.addr, align 4, !tbaa !2
  %35 = load i32, i32* %34, align 4, !tbaa !20
  %add = add i32 %33, %35
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 27
  %37 = load i32, i32* %output_width, align 8, !tbaa !42
  %cmp15 = icmp ugt i32 %add, %37
  br i1 %cmp15, label %if.then16, label %if.end21

if.then16:                                        ; preds = %lor.lhs.false14, %if.end12
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 0
  %39 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err17, align 8, !tbaa !26
  %msg_code18 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %39, i32 0, i32 5
  store i32 70, i32* %msg_code18, align 4, !tbaa !27
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 0
  %41 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err19, align 8, !tbaa !26
  %error_exit20 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %41, i32 0, i32 0
  %42 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit20, align 4, !tbaa !30
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %44 = bitcast %struct.jpeg_decompress_struct* %43 to %struct.jpeg_common_struct*
  call void %42(%struct.jpeg_common_struct* %44)
  br label %if.end21

if.end21:                                         ; preds = %if.then16, %lor.lhs.false14
  %45 = load i32*, i32** %width.addr, align 4, !tbaa !2
  %46 = load i32, i32* %45, align 4, !tbaa !20
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width22 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 27
  %48 = load i32, i32* %output_width22, align 8, !tbaa !42
  %cmp23 = icmp eq i32 %46, %48
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end21
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end25:                                         ; preds = %if.end21
  %49 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %49, i32 0, i32 63
  %50 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !43
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 61
  %52 = load i32, i32* %max_h_samp_factor, align 4, !tbaa !44
  %mul = mul nsw i32 %50, %52
  store i32 %mul, i32* %align, align 4, !tbaa !20
  %53 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  %54 = load i32, i32* %53, align 4, !tbaa !20
  store i32 %54, i32* %input_xoffset, align 4, !tbaa !20
  %55 = load i32, i32* %input_xoffset, align 4, !tbaa !20
  %56 = load i32, i32* %align, align 4, !tbaa !20
  %div = udiv i32 %55, %56
  %57 = load i32, i32* %align, align 4, !tbaa !20
  %mul26 = mul i32 %div, %57
  %58 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  store i32 %mul26, i32* %58, align 4, !tbaa !20
  %59 = load i32*, i32** %width.addr, align 4, !tbaa !2
  %60 = load i32, i32* %59, align 4, !tbaa !20
  %61 = load i32, i32* %input_xoffset, align 4, !tbaa !20
  %add27 = add i32 %60, %61
  %62 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  %63 = load i32, i32* %62, align 4, !tbaa !20
  %sub = sub i32 %add27, %63
  %64 = load i32*, i32** %width.addr, align 4, !tbaa !2
  store i32 %sub, i32* %64, align 4, !tbaa !20
  %65 = load i32*, i32** %width.addr, align 4, !tbaa !2
  %66 = load i32, i32* %65, align 4, !tbaa !20
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width28 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %67, i32 0, i32 27
  store i32 %66, i32* %output_width28, align 8, !tbaa !42
  %68 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  %69 = load i32, i32* %68, align 4, !tbaa !20
  %70 = load i32, i32* %align, align 4, !tbaa !20
  %div29 = udiv i32 %69, %70
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 77
  %72 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !31
  %first_iMCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %72, i32 0, i32 3
  store i32 %div29, i32* %first_iMCU_col, align 4, !tbaa !45
  %73 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  %74 = load i32, i32* %73, align 4, !tbaa !20
  %75 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width30 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %75, i32 0, i32 27
  %76 = load i32, i32* %output_width30, align 8, !tbaa !42
  %add31 = add i32 %74, %76
  %77 = load i32, i32* %align, align 4, !tbaa !20
  %call = call i32 @jdiv_round_up(i32 %add31, i32 %77)
  %sub32 = sub i32 %call, 1
  %78 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master33 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %78, i32 0, i32 77
  %79 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master33, align 4, !tbaa !31
  %last_iMCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %79, i32 0, i32 4
  store i32 %sub32, i32* %last_iMCU_col, align 4, !tbaa !46
  store i32 0, i32* %ci, align 4, !tbaa !20
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %80, i32 0, i32 44
  %81 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !47
  store %struct.jpeg_component_info* %81, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end25
  %82 = load i32, i32* %ci, align 4, !tbaa !20
  %83 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %83, i32 0, i32 9
  %84 = load i32, i32* %num_components, align 4, !tbaa !48
  %cmp34 = icmp slt i32 %82, %84
  br i1 %cmp34, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %85 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %85, i32 0, i32 10
  %86 = load i32, i32* %downsampled_width, align 4, !tbaa !49
  store i32 %86, i32* %orig_downsampled_width, align 4, !tbaa !20
  %87 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width35 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %87, i32 0, i32 27
  %88 = load i32, i32* %output_width35, align 8, !tbaa !42
  %89 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %89, i32 0, i32 2
  %90 = load i32, i32* %h_samp_factor, align 4, !tbaa !51
  %mul36 = mul i32 %88, %90
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor37 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %91, i32 0, i32 61
  %92 = load i32, i32* %max_h_samp_factor37, align 4, !tbaa !44
  %call38 = call i32 @jdiv_round_up(i32 %mul36, i32 %92)
  %93 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_width39 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %93, i32 0, i32 10
  store i32 %call38, i32* %downsampled_width39, align 4, !tbaa !49
  %94 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %downsampled_width40 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %94, i32 0, i32 10
  %95 = load i32, i32* %downsampled_width40, align 4, !tbaa !49
  %cmp41 = icmp ult i32 %95, 2
  br i1 %cmp41, label %land.lhs.true, label %if.end44

land.lhs.true:                                    ; preds = %for.body
  %96 = load i32, i32* %orig_downsampled_width, align 4, !tbaa !20
  %cmp42 = icmp sge i32 %96, 2
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %land.lhs.true
  store i32 1, i32* %reinit_upsampler, align 4, !tbaa !20
  br label %if.end44

if.end44:                                         ; preds = %if.then43, %land.lhs.true, %for.body
  %97 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  %98 = load i32, i32* %97, align 4, !tbaa !20
  %99 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor45 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %99, i32 0, i32 2
  %100 = load i32, i32* %h_samp_factor45, align 4, !tbaa !51
  %mul46 = mul i32 %98, %100
  %101 = load i32, i32* %align, align 4, !tbaa !20
  %div47 = udiv i32 %mul46, %101
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 77
  %103 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master48, align 4, !tbaa !31
  %first_MCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %103, i32 0, i32 5
  %104 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx49 = getelementptr inbounds [10 x i32], [10 x i32]* %first_MCU_col, i32 0, i32 %104
  store i32 %div47, i32* %arrayidx49, align 4, !tbaa !20
  %105 = load i32*, i32** %xoffset.addr, align 4, !tbaa !2
  %106 = load i32, i32* %105, align 4, !tbaa !20
  %107 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width50 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %107, i32 0, i32 27
  %108 = load i32, i32* %output_width50, align 8, !tbaa !42
  %add51 = add i32 %106, %108
  %109 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor52 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %109, i32 0, i32 2
  %110 = load i32, i32* %h_samp_factor52, align 4, !tbaa !51
  %mul53 = mul i32 %add51, %110
  %111 = load i32, i32* %align, align 4, !tbaa !20
  %call54 = call i32 @jdiv_round_up(i32 %mul53, i32 %111)
  %sub55 = sub i32 %call54, 1
  %112 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master56 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %112, i32 0, i32 77
  %113 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master56, align 4, !tbaa !31
  %last_MCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %113, i32 0, i32 6
  %114 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx57 = getelementptr inbounds [10 x i32], [10 x i32]* %last_MCU_col, i32 0, i32 %114
  store i32 %sub55, i32* %arrayidx57, align 4, !tbaa !20
  br label %for.inc

for.inc:                                          ; preds = %if.end44
  %115 = load i32, i32* %ci, align 4, !tbaa !20
  %inc = add nsw i32 %115, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !20
  %116 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %116, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %117 = load i32, i32* %reinit_upsampler, align 4, !tbaa !20
  %tobool58 = icmp ne i32 %117, 0
  br i1 %tobool58, label %if.then59, label %if.end63

if.then59:                                        ; preds = %for.end
  %118 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master60 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %118, i32 0, i32 77
  %119 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master60, align 4, !tbaa !31
  %jinit_upsampler_no_alloc = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %119, i32 0, i32 7
  store i32 1, i32* %jinit_upsampler_no_alloc, align 4, !tbaa !52
  %120 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @jinit_upsampler(%struct.jpeg_decompress_struct* %120)
  %121 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master61 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %121, i32 0, i32 77
  %122 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master61, align 4, !tbaa !31
  %jinit_upsampler_no_alloc62 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %122, i32 0, i32 7
  store i32 0, i32* %jinit_upsampler_no_alloc62, align 4, !tbaa !52
  br label %if.end63

if.end63:                                         ; preds = %if.then59, %for.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end63, %if.then24
  %123 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast i32* %reinit_upsampler to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast i32* %input_xoffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  %126 = bitcast i32* %orig_downsampled_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast i32* %align to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare i32 @jdiv_round_up(i32, i32) #1

declare void @jinit_upsampler(%struct.jpeg_decompress_struct*) #1

; Function Attrs: nounwind
define hidden i32 @jpeg_read_scanlines(%struct.jpeg_decompress_struct* %cinfo, i8** %scanlines, i32 %max_lines) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %scanlines.addr = alloca i8**, align 4
  %max_lines.addr = alloca i32, align 4
  %row_ctr = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %scanlines, i8*** %scanlines.addr, align 4, !tbaa !2
  store i32 %max_lines, i32* %max_lines.addr, align 4, !tbaa !20
  %0 = bitcast i32* %row_ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 5
  %2 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp ne i32 %2, 205
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !27
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 5
  %6 = load i32, i32* %global_state1, align 4, !tbaa !6
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !26
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %6, i32* %arrayidx, align 4, !tbaa !29
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 0
  %11 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = bitcast %struct.jpeg_decompress_struct* %12 to %struct.jpeg_common_struct*
  call void %11(%struct.jpeg_common_struct* %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 34
  %15 = load i32, i32* %output_scanline, align 4, !tbaa !34
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 28
  %17 = load i32, i32* %output_height, align 4, !tbaa !36
  %cmp4 = icmp uge i32 %15, %17
  br i1 %cmp4, label %if.then5, label %if.end9

if.then5:                                         ; preds = %if.end
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !26
  %msg_code7 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 5
  store i32 123, i32* %msg_code7, align 4, !tbaa !27
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 0
  %21 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !26
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %21, i32 0, i32 1
  %22 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !53
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_decompress_struct* %23 to %struct.jpeg_common_struct*
  call void %22(%struct.jpeg_common_struct* %24, i32 -1)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 2
  %26 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !15
  %cmp10 = icmp ne %struct.jpeg_progress_mgr* %26, null
  br i1 %cmp10, label %if.then11, label %if.end18

if.then11:                                        ; preds = %if.end9
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 34
  %28 = load i32, i32* %output_scanline12, align 4, !tbaa !34
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 2
  %30 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress13, align 8, !tbaa !15
  %pass_counter = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %30, i32 0, i32 1
  store i32 %28, i32* %pass_counter, align 4, !tbaa !21
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 28
  %32 = load i32, i32* %output_height14, align 4, !tbaa !36
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 2
  %34 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress15, align 8, !tbaa !15
  %pass_limit = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %34, i32 0, i32 2
  store i32 %32, i32* %pass_limit, align 4, !tbaa !22
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 2
  %36 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress16, align 8, !tbaa !15
  %progress_monitor = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %36, i32 0, i32 0
  %progress_monitor17 = bitcast {}** %progress_monitor to void (%struct.jpeg_common_struct*)**
  %37 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %progress_monitor17, align 4, !tbaa !16
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %39 = bitcast %struct.jpeg_decompress_struct* %38 to %struct.jpeg_common_struct*
  call void %37(%struct.jpeg_common_struct* %39)
  br label %if.end18

if.end18:                                         ; preds = %if.then11, %if.end9
  store i32 0, i32* %row_ctr, align 4, !tbaa !20
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 78
  %41 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !37
  %process_data = getelementptr inbounds %struct.jpeg_d_main_controller, %struct.jpeg_d_main_controller* %41, i32 0, i32 1
  %42 = load void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)** %process_data, align 4, !tbaa !38
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %44 = load i8**, i8*** %scanlines.addr, align 4, !tbaa !2
  %45 = load i32, i32* %max_lines.addr, align 4, !tbaa !20
  call void %42(%struct.jpeg_decompress_struct* %43, i8** %44, i32* %row_ctr, i32 %45)
  %46 = load i32, i32* %row_ctr, align 4, !tbaa !20
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline19 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 34
  %48 = load i32, i32* %output_scanline19, align 4, !tbaa !34
  %add = add i32 %48, %46
  store i32 %add, i32* %output_scanline19, align 4, !tbaa !34
  %49 = load i32, i32* %row_ctr, align 4, !tbaa !20
  store i32 %49, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end18, %if.then5
  %50 = bitcast i32* %row_ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = load i32, i32* %retval, align 4
  ret i32 %51
}

; Function Attrs: nounwind
define hidden i32 @jpeg_skip_scanlines(%struct.jpeg_decompress_struct* %cinfo, i32 %num_lines) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %num_lines.addr = alloca i32, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %lines_per_iMCU_row = alloca i32, align 4
  %lines_left_in_iMCU_row = alloca i32, align 4
  %lines_after_iMCU_row = alloca i32, align 4
  %lines_to_skip = alloca i32, align 4
  %lines_to_read = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %num_lines, i32* %num_lines.addr, align 4, !tbaa !20
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !37
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 79
  %6 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !54
  %7 = bitcast %struct.jpeg_d_coef_controller* %6 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %7, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %8 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 85
  %10 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample2, align 4, !tbaa !55
  %11 = bitcast %struct.jpeg_upsampler* %10 to %struct.my_upsampler*
  store %struct.my_upsampler* %11, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %lines_per_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %lines_left_in_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %lines_after_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %lines_to_skip to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %lines_to_read to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 5
  %21 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp ne i32 %21, 205
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 0
  %23 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %23, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !27
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 5
  %25 = load i32, i32* %global_state3, align 4, !tbaa !6
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 0
  %27 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !26
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %27, i32 0, i32 6
  %i5 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i5, i32 0, i32 0
  store i32 %25, i32* %arrayidx, align 4, !tbaa !29
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 0
  %29 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %29, i32 0, i32 0
  %30 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %32 = bitcast %struct.jpeg_decompress_struct* %31 to %struct.jpeg_common_struct*
  call void %30(%struct.jpeg_common_struct* %32)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 34
  %34 = load i32, i32* %output_scanline, align 4, !tbaa !34
  %35 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  %add = add i32 %34, %35
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 28
  %37 = load i32, i32* %output_height, align 4, !tbaa !36
  %cmp7 = icmp uge i32 %add, %37
  br i1 %cmp7, label %if.then8, label %if.end13

if.then8:                                         ; preds = %if.end
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 28
  %39 = load i32, i32* %output_height9, align 4, !tbaa !36
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 34
  store i32 %39, i32* %output_scanline10, align 4, !tbaa !34
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 28
  %42 = load i32, i32* %output_height11, align 4, !tbaa !36
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 34
  %44 = load i32, i32* %output_scanline12, align 4, !tbaa !34
  %sub = sub i32 %42, %44
  store i32 %sub, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.end
  %45 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  %cmp14 = icmp eq i32 %45, 0
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end13
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %if.end13
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 63
  %47 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !43
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 62
  %49 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !56
  %mul = mul nsw i32 %47, %49
  store i32 %mul, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %50 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %51, i32 0, i32 34
  %52 = load i32, i32* %output_scanline17, align 4, !tbaa !34
  %53 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %rem = urem i32 %52, %53
  %sub18 = sub i32 %50, %rem
  %54 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %rem19 = urem i32 %sub18, %54
  store i32 %rem19, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %55 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  %56 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %sub20 = sub i32 %55, %56
  store i32 %sub20, i32* %lines_after_iMCU_row, align 4, !tbaa !20
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %57, i32 0, i32 85
  %58 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample21, align 4, !tbaa !55
  %need_context_rows = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %58, i32 0, i32 2
  %59 = load i32, i32* %need_context_rows, align 4, !tbaa !57
  %tobool = icmp ne i32 %59, 0
  br i1 %tobool, label %if.then22, label %if.else57

if.then22:                                        ; preds = %if.end16
  %60 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  %61 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %add23 = add i32 %61, 1
  %cmp24 = icmp ult i32 %60, %add23
  br i1 %cmp24, label %if.then30, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then22
  %62 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %cmp25 = icmp ule i32 %62, 1
  br i1 %cmp25, label %land.lhs.true, label %if.end31

land.lhs.true:                                    ; preds = %lor.lhs.false
  %63 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %63, i32 0, i32 2
  %64 = load i32, i32* %buffer_full, align 4, !tbaa !59
  %tobool26 = icmp ne i32 %64, 0
  br i1 %tobool26, label %land.lhs.true27, label %if.end31

land.lhs.true27:                                  ; preds = %land.lhs.true
  %65 = load i32, i32* %lines_after_iMCU_row, align 4, !tbaa !20
  %66 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %add28 = add i32 %66, 1
  %cmp29 = icmp ult i32 %65, %add28
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %land.lhs.true27, %if.then22
  %67 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %68 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  call void @read_and_discard_scanlines(%struct.jpeg_decompress_struct* %67, i32 %68)
  %69 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  store i32 %69, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %land.lhs.true27, %land.lhs.true, %lor.lhs.false
  %70 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %cmp32 = icmp ule i32 %70, 1
  br i1 %cmp32, label %land.lhs.true33, label %if.else

land.lhs.true33:                                  ; preds = %if.end31
  %71 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full34 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %71, i32 0, i32 2
  %72 = load i32, i32* %buffer_full34, align 4, !tbaa !59
  %tobool35 = icmp ne i32 %72, 0
  br i1 %tobool35, label %if.then36, label %if.else

if.then36:                                        ; preds = %land.lhs.true33
  %73 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %74 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %add37 = add i32 %73, %74
  %75 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %75, i32 0, i32 34
  %76 = load i32, i32* %output_scanline38, align 4, !tbaa !34
  %add39 = add i32 %76, %add37
  store i32 %add39, i32* %output_scanline38, align 4, !tbaa !34
  %77 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %78 = load i32, i32* %lines_after_iMCU_row, align 4, !tbaa !20
  %sub40 = sub i32 %78, %77
  store i32 %sub40, i32* %lines_after_iMCU_row, align 4, !tbaa !20
  br label %if.end43

if.else:                                          ; preds = %land.lhs.true33, %if.end31
  %79 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline41 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %80, i32 0, i32 34
  %81 = load i32, i32* %output_scanline41, align 4, !tbaa !34
  %add42 = add i32 %81, %79
  store i32 %add42, i32* %output_scanline41, align 4, !tbaa !34
  br label %if.end43

if.end43:                                         ; preds = %if.else, %if.then36
  %82 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %iMCU_row_ctr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %82, i32 0, i32 8
  %83 = load i32, i32* %iMCU_row_ctr, align 4, !tbaa !61
  %cmp44 = icmp eq i32 %83, 0
  br i1 %cmp44, label %if.then50, label %lor.lhs.false45

lor.lhs.false45:                                  ; preds = %if.end43
  %84 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %iMCU_row_ctr46 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %84, i32 0, i32 8
  %85 = load i32, i32* %iMCU_row_ctr46, align 4, !tbaa !61
  %cmp47 = icmp eq i32 %85, 1
  br i1 %cmp47, label %land.lhs.true48, label %if.end51

land.lhs.true48:                                  ; preds = %lor.lhs.false45
  %86 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %cmp49 = icmp ugt i32 %86, 2
  br i1 %cmp49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %land.lhs.true48, %if.end43
  %87 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @set_wraparound_pointers(%struct.jpeg_decompress_struct* %87)
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %land.lhs.true48, %lor.lhs.false45
  %88 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full52 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %88, i32 0, i32 2
  store i32 0, i32* %buffer_full52, align 4, !tbaa !59
  %89 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %89, i32 0, i32 3
  store i32 0, i32* %rowgroup_ctr, align 4, !tbaa !62
  %90 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %context_state = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %90, i32 0, i32 6
  store i32 0, i32* %context_state, align 4, !tbaa !63
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor53 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %91, i32 0, i32 62
  %92 = load i32, i32* %max_v_samp_factor53, align 8, !tbaa !56
  %93 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %93, i32 0, i32 3
  store i32 %92, i32* %next_row_out, align 4, !tbaa !64
  %94 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height54 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %94, i32 0, i32 28
  %95 = load i32, i32* %output_height54, align 4, !tbaa !36
  %96 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline55 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %96, i32 0, i32 34
  %97 = load i32, i32* %output_scanline55, align 4, !tbaa !34
  %sub56 = sub i32 %95, %97
  %98 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %98, i32 0, i32 4
  store i32 %sub56, i32* %rows_to_go, align 4, !tbaa !66
  br label %if.end72

if.else57:                                        ; preds = %if.end16
  %99 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  %100 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %cmp58 = icmp ult i32 %99, %100
  br i1 %cmp58, label %if.then59, label %if.else60

if.then59:                                        ; preds = %if.else57
  %101 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %102 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  call void @increment_simple_rowgroup_ctr(%struct.jpeg_decompress_struct* %101, i32 %102)
  %103 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  store i32 %103, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else60:                                        ; preds = %if.else57
  %104 = load i32, i32* %lines_left_in_iMCU_row, align 4, !tbaa !20
  %105 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline61 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %105, i32 0, i32 34
  %106 = load i32, i32* %output_scanline61, align 4, !tbaa !34
  %add62 = add i32 %106, %104
  store i32 %add62, i32* %output_scanline61, align 4, !tbaa !34
  %107 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %buffer_full63 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %107, i32 0, i32 2
  store i32 0, i32* %buffer_full63, align 4, !tbaa !59
  %108 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr64 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %108, i32 0, i32 3
  store i32 0, i32* %rowgroup_ctr64, align 4, !tbaa !62
  %109 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor65 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %109, i32 0, i32 62
  %110 = load i32, i32* %max_v_samp_factor65, align 8, !tbaa !56
  %111 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %next_row_out66 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %111, i32 0, i32 3
  store i32 %110, i32* %next_row_out66, align 4, !tbaa !64
  %112 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %112, i32 0, i32 28
  %113 = load i32, i32* %output_height67, align 4, !tbaa !36
  %114 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline68 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %114, i32 0, i32 34
  %115 = load i32, i32* %output_scanline68, align 4, !tbaa !34
  %sub69 = sub i32 %113, %115
  %116 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go70 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %116, i32 0, i32 4
  store i32 %sub69, i32* %rows_to_go70, align 4, !tbaa !66
  br label %if.end71

if.end71:                                         ; preds = %if.else60
  br label %if.end72

if.end72:                                         ; preds = %if.end71, %if.end51
  %117 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample73 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %117, i32 0, i32 85
  %118 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample73, align 4, !tbaa !55
  %need_context_rows74 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %118, i32 0, i32 2
  %119 = load i32, i32* %need_context_rows74, align 4, !tbaa !57
  %tobool75 = icmp ne i32 %119, 0
  br i1 %tobool75, label %if.then76, label %if.else79

if.then76:                                        ; preds = %if.end72
  %120 = load i32, i32* %lines_after_iMCU_row, align 4, !tbaa !20
  %sub77 = sub i32 %120, 1
  %121 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %div = udiv i32 %sub77, %121
  %122 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %mul78 = mul i32 %div, %122
  store i32 %mul78, i32* %lines_to_skip, align 4, !tbaa !20
  br label %if.end82

if.else79:                                        ; preds = %if.end72
  %123 = load i32, i32* %lines_after_iMCU_row, align 4, !tbaa !20
  %124 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %div80 = udiv i32 %123, %124
  %125 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %mul81 = mul i32 %div80, %125
  store i32 %mul81, i32* %lines_to_skip, align 4, !tbaa !20
  br label %if.end82

if.end82:                                         ; preds = %if.else79, %if.then76
  %126 = load i32, i32* %lines_after_iMCU_row, align 4, !tbaa !20
  %127 = load i32, i32* %lines_to_skip, align 4, !tbaa !20
  %sub83 = sub i32 %126, %127
  store i32 %sub83, i32* %lines_to_read, align 4, !tbaa !20
  %128 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %128, i32 0, i32 81
  %129 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !12
  %has_multiple_scans = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %129, i32 0, i32 4
  %130 = load i32, i32* %has_multiple_scans, align 4, !tbaa !13
  %tobool84 = icmp ne i32 %130, 0
  br i1 %tobool84, label %if.then85, label %if.end108

if.then85:                                        ; preds = %if.end82
  %131 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample86 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %131, i32 0, i32 85
  %132 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample86, align 4, !tbaa !55
  %need_context_rows87 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %132, i32 0, i32 2
  %133 = load i32, i32* %need_context_rows87, align 4, !tbaa !57
  %tobool88 = icmp ne i32 %133, 0
  br i1 %tobool88, label %if.then89, label %if.else97

if.then89:                                        ; preds = %if.then85
  %134 = load i32, i32* %lines_to_skip, align 4, !tbaa !20
  %135 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline90 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %135, i32 0, i32 34
  %136 = load i32, i32* %output_scanline90, align 4, !tbaa !34
  %add91 = add i32 %136, %134
  store i32 %add91, i32* %output_scanline90, align 4, !tbaa !34
  %137 = load i32, i32* %lines_to_skip, align 4, !tbaa !20
  %138 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %div92 = udiv i32 %137, %138
  %139 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %139, i32 0, i32 38
  %140 = load i32, i32* %output_iMCU_row, align 4, !tbaa !67
  %add93 = add i32 %140, %div92
  store i32 %add93, i32* %output_iMCU_row, align 4, !tbaa !67
  %141 = load i32, i32* %lines_after_iMCU_row, align 4, !tbaa !20
  %142 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %div94 = udiv i32 %141, %142
  %143 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %iMCU_row_ctr95 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %143, i32 0, i32 8
  %144 = load i32, i32* %iMCU_row_ctr95, align 4, !tbaa !61
  %add96 = add i32 %144, %div94
  store i32 %add96, i32* %iMCU_row_ctr95, align 4, !tbaa !61
  %145 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %146 = load i32, i32* %lines_to_read, align 4, !tbaa !20
  call void @read_and_discard_scanlines(%struct.jpeg_decompress_struct* %145, i32 %146)
  br label %if.end103

if.else97:                                        ; preds = %if.then85
  %147 = load i32, i32* %lines_to_skip, align 4, !tbaa !20
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline98 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 34
  %149 = load i32, i32* %output_scanline98, align 4, !tbaa !34
  %add99 = add i32 %149, %147
  store i32 %add99, i32* %output_scanline98, align 4, !tbaa !34
  %150 = load i32, i32* %lines_to_skip, align 4, !tbaa !20
  %151 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %div100 = udiv i32 %150, %151
  %152 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row101 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %152, i32 0, i32 38
  %153 = load i32, i32* %output_iMCU_row101, align 4, !tbaa !67
  %add102 = add i32 %153, %div100
  store i32 %add102, i32* %output_iMCU_row101, align 4, !tbaa !67
  %154 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %155 = load i32, i32* %lines_to_read, align 4, !tbaa !20
  call void @increment_simple_rowgroup_ctr(%struct.jpeg_decompress_struct* %154, i32 %155)
  br label %if.end103

if.end103:                                        ; preds = %if.else97, %if.then89
  %156 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height104 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %156, i32 0, i32 28
  %157 = load i32, i32* %output_height104, align 4, !tbaa !36
  %158 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline105 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %158, i32 0, i32 34
  %159 = load i32, i32* %output_scanline105, align 4, !tbaa !34
  %sub106 = sub i32 %157, %159
  %160 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go107 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %160, i32 0, i32 4
  store i32 %sub106, i32* %rows_to_go107, align 4, !tbaa !66
  %161 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  store i32 %161, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end108:                                        ; preds = %if.end82
  store i32 0, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc128, %if.end108
  %162 = load i32, i32* %i, align 4, !tbaa !20
  %163 = load i32, i32* %lines_to_skip, align 4, !tbaa !20
  %cmp109 = icmp ult i32 %162, %163
  br i1 %cmp109, label %for.body, label %for.end130

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %y, align 4, !tbaa !20
  br label %for.cond110

for.cond110:                                      ; preds = %for.inc116, %for.body
  %164 = load i32, i32* %y, align 4, !tbaa !20
  %165 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %165, i32 0, i32 3
  %166 = load i32, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !68
  %cmp111 = icmp slt i32 %164, %166
  br i1 %cmp111, label %for.body112, label %for.end118

for.body112:                                      ; preds = %for.cond110
  store i32 0, i32* %x, align 4, !tbaa !20
  br label %for.cond113

for.cond113:                                      ; preds = %for.inc, %for.body112
  %167 = load i32, i32* %x, align 4, !tbaa !20
  %168 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %168, i32 0, i32 68
  %169 = load i32, i32* %MCUs_per_row, align 4, !tbaa !71
  %cmp114 = icmp ult i32 %167, %169
  br i1 %cmp114, label %for.body115, label %for.end

for.body115:                                      ; preds = %for.cond113
  %170 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %170, i32 0, i32 83
  %171 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy, align 4, !tbaa !72
  %decode_mcu = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %171, i32 0, i32 1
  %172 = load i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)** %decode_mcu, align 4, !tbaa !73
  %173 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %172(%struct.jpeg_decompress_struct* %173, [64 x i16]** null)
  br label %for.inc

for.inc:                                          ; preds = %for.body115
  %174 = load i32, i32* %x, align 4, !tbaa !20
  %inc = add i32 %174, 1
  store i32 %inc, i32* %x, align 4, !tbaa !20
  br label %for.cond113

for.end:                                          ; preds = %for.cond113
  br label %for.inc116

for.inc116:                                       ; preds = %for.end
  %175 = load i32, i32* %y, align 4, !tbaa !20
  %inc117 = add nsw i32 %175, 1
  store i32 %inc117, i32* %y, align 4, !tbaa !20
  br label %for.cond110

for.end118:                                       ; preds = %for.cond110
  %176 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %176, i32 0, i32 36
  %177 = load i32, i32* %input_iMCU_row, align 4, !tbaa !75
  %inc119 = add i32 %177, 1
  store i32 %inc119, i32* %input_iMCU_row, align 4, !tbaa !75
  %178 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row120 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %178, i32 0, i32 38
  %179 = load i32, i32* %output_iMCU_row120, align 4, !tbaa !67
  %inc121 = add i32 %179, 1
  store i32 %inc121, i32* %output_iMCU_row120, align 4, !tbaa !67
  %180 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row122 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %180, i32 0, i32 36
  %181 = load i32, i32* %input_iMCU_row122, align 4, !tbaa !75
  %182 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %182, i32 0, i32 64
  %183 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !23
  %cmp123 = icmp ult i32 %181, %183
  br i1 %cmp123, label %if.then124, label %if.else125

if.then124:                                       ; preds = %for.end118
  %184 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_decompress_struct* %184)
  br label %if.end127

if.else125:                                       ; preds = %for.end118
  %185 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl126 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %185, i32 0, i32 81
  %186 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl126, align 4, !tbaa !12
  %finish_input_pass = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %186, i32 0, i32 3
  %187 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %finish_input_pass, align 4, !tbaa !76
  %188 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %187(%struct.jpeg_decompress_struct* %188)
  br label %if.end127

if.end127:                                        ; preds = %if.else125, %if.then124
  br label %for.inc128

for.inc128:                                       ; preds = %if.end127
  %189 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %190 = load i32, i32* %i, align 4, !tbaa !20
  %add129 = add i32 %190, %189
  store i32 %add129, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end130:                                       ; preds = %for.cond
  %191 = load i32, i32* %lines_to_skip, align 4, !tbaa !20
  %192 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline131 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %192, i32 0, i32 34
  %193 = load i32, i32* %output_scanline131, align 4, !tbaa !34
  %add132 = add i32 %193, %191
  store i32 %add132, i32* %output_scanline131, align 4, !tbaa !34
  %194 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample133 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %194, i32 0, i32 85
  %195 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample133, align 4, !tbaa !55
  %need_context_rows134 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %195, i32 0, i32 2
  %196 = load i32, i32* %need_context_rows134, align 4, !tbaa !57
  %tobool135 = icmp ne i32 %196, 0
  br i1 %tobool135, label %if.then136, label %if.else140

if.then136:                                       ; preds = %for.end130
  %197 = load i32, i32* %lines_to_skip, align 4, !tbaa !20
  %198 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %div137 = udiv i32 %197, %198
  %199 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %iMCU_row_ctr138 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %199, i32 0, i32 8
  %200 = load i32, i32* %iMCU_row_ctr138, align 4, !tbaa !61
  %add139 = add i32 %200, %div137
  store i32 %add139, i32* %iMCU_row_ctr138, align 4, !tbaa !61
  %201 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %202 = load i32, i32* %lines_to_read, align 4, !tbaa !20
  call void @read_and_discard_scanlines(%struct.jpeg_decompress_struct* %201, i32 %202)
  br label %if.end141

if.else140:                                       ; preds = %for.end130
  %203 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %204 = load i32, i32* %lines_to_read, align 4, !tbaa !20
  call void @increment_simple_rowgroup_ctr(%struct.jpeg_decompress_struct* %203, i32 %204)
  br label %if.end141

if.end141:                                        ; preds = %if.else140, %if.then136
  %205 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height142 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %205, i32 0, i32 28
  %206 = load i32, i32* %output_height142, align 4, !tbaa !36
  %207 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline143 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %207, i32 0, i32 34
  %208 = load i32, i32* %output_scanline143, align 4, !tbaa !34
  %sub144 = sub i32 %206, %208
  %209 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go145 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %209, i32 0, i32 4
  store i32 %sub144, i32* %rows_to_go145, align 4, !tbaa !66
  %210 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  store i32 %210, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end141, %if.end103, %if.then59, %if.then30, %if.then15, %if.then8
  %211 = bitcast i32* %lines_to_read to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #3
  %212 = bitcast i32* %lines_to_skip to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #3
  %213 = bitcast i32* %lines_after_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #3
  %214 = bitcast i32* %lines_left_in_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #3
  %215 = bitcast i32* %lines_per_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #3
  %216 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #3
  %217 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #3
  %218 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #3
  %219 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #3
  %220 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #3
  %221 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #3
  %222 = load i32, i32* %retval, align 4
  ret i32 %222
}

; Function Attrs: nounwind
define internal void @read_and_discard_scanlines(%struct.jpeg_decompress_struct* %cinfo, i32 %num_lines) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %num_lines.addr = alloca i32, align 4
  %n = alloca i32, align 4
  %color_convert = alloca void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %num_lines, i32* %num_lines.addr, align 4, !tbaa !20
  %0 = bitcast i32* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 86
  %3 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert, align 8, !tbaa !77
  %color_convert1 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %3, i32 0, i32 1
  %4 = load void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert1, align 4, !tbaa !78
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* %4, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert, align 4, !tbaa !2
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 86
  %6 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert2, align 8, !tbaa !77
  %color_convert3 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %6, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* @noop_convert, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert3, align 4, !tbaa !78
  store i32 0, i32* %n, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %n, align 4, !tbaa !20
  %8 = load i32, i32* %num_lines.addr, align 4, !tbaa !20
  %cmp = icmp ult i32 %7, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @jpeg_read_scanlines(%struct.jpeg_decompress_struct* %9, i8** null, i32 1)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %n, align 4, !tbaa !20
  %inc = add i32 %10, 1
  store i32 %inc, i32* %n, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert, align 4, !tbaa !2
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 86
  %13 = load %struct.jpeg_color_deconverter*, %struct.jpeg_color_deconverter** %cconvert4, align 8, !tbaa !77
  %color_convert5 = getelementptr inbounds %struct.jpeg_color_deconverter, %struct.jpeg_color_deconverter* %13, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* %11, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert5, align 4, !tbaa !78
  %14 = bitcast void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)** %color_convert to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #3
  ret void
}

; Function Attrs: nounwind
define internal void @set_wraparound_pointers(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  %ci = alloca i32, align 4
  %i = alloca i32, align 4
  %rgroup = alloca i32, align 4
  %M = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %xbuf0 = alloca i8**, align 4
  %xbuf1 = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 78
  %2 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !37
  %3 = bitcast %struct.jpeg_d_main_controller* %2 to %struct.my_main_controller*
  store %struct.my_main_controller* %3, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %M to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 63
  %9 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !43
  store i32 %9, i32* %M, align 4, !tbaa !20
  %10 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i8*** %xbuf0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i8*** %xbuf1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  store i32 0, i32* %ci, align 4, !tbaa !20
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 44
  %14 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !47
  store %struct.jpeg_component_info* %14, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc29, %entry
  %15 = load i32, i32* %ci, align 4, !tbaa !20
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 9
  %17 = load i32, i32* %num_components, align 4, !tbaa !48
  %cmp = icmp slt i32 %15, %17
  br i1 %cmp, label %for.body, label %for.end31

for.body:                                         ; preds = %for.cond
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %18, i32 0, i32 3
  %19 = load i32, i32* %v_samp_factor, align 4, !tbaa !80
  %20 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %20, i32 0, i32 9
  %21 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !81
  %mul = mul nsw i32 %19, %21
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 63
  %23 = load i32, i32* %min_DCT_scaled_size1, align 4, !tbaa !43
  %div = sdiv i32 %mul, %23
  store i32 %div, i32* %rgroup, align 4, !tbaa !20
  %24 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %24, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer, i32 0, i32 0
  %25 = load i8***, i8**** %arrayidx, align 4, !tbaa !2
  %26 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx2 = getelementptr inbounds i8**, i8*** %25, i32 %26
  %27 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  store i8** %27, i8*** %xbuf0, align 4, !tbaa !2
  %28 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %xbuffer3 = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %28, i32 0, i32 4
  %arrayidx4 = getelementptr inbounds [2 x i8***], [2 x i8***]* %xbuffer3, i32 0, i32 1
  %29 = load i8***, i8**** %arrayidx4, align 4, !tbaa !2
  %30 = load i32, i32* %ci, align 4, !tbaa !20
  %arrayidx5 = getelementptr inbounds i8**, i8*** %29, i32 %30
  %31 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  store i8** %31, i8*** %xbuf1, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !20
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %32 = load i32, i32* %i, align 4, !tbaa !20
  %33 = load i32, i32* %rgroup, align 4, !tbaa !20
  %cmp7 = icmp slt i32 %32, %33
  br i1 %cmp7, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond6
  %34 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %35 = load i32, i32* %rgroup, align 4, !tbaa !20
  %36 = load i32, i32* %M, align 4, !tbaa !20
  %add = add nsw i32 %36, 1
  %mul9 = mul nsw i32 %35, %add
  %37 = load i32, i32* %i, align 4, !tbaa !20
  %add10 = add nsw i32 %mul9, %37
  %arrayidx11 = getelementptr inbounds i8*, i8** %34, i32 %add10
  %38 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  %39 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %40 = load i32, i32* %i, align 4, !tbaa !20
  %41 = load i32, i32* %rgroup, align 4, !tbaa !20
  %sub = sub nsw i32 %40, %41
  %arrayidx12 = getelementptr inbounds i8*, i8** %39, i32 %sub
  store i8* %38, i8** %arrayidx12, align 4, !tbaa !2
  %42 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %43 = load i32, i32* %rgroup, align 4, !tbaa !20
  %44 = load i32, i32* %M, align 4, !tbaa !20
  %add13 = add nsw i32 %44, 1
  %mul14 = mul nsw i32 %43, %add13
  %45 = load i32, i32* %i, align 4, !tbaa !20
  %add15 = add nsw i32 %mul14, %45
  %arrayidx16 = getelementptr inbounds i8*, i8** %42, i32 %add15
  %46 = load i8*, i8** %arrayidx16, align 4, !tbaa !2
  %47 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %48 = load i32, i32* %i, align 4, !tbaa !20
  %49 = load i32, i32* %rgroup, align 4, !tbaa !20
  %sub17 = sub nsw i32 %48, %49
  %arrayidx18 = getelementptr inbounds i8*, i8** %47, i32 %sub17
  store i8* %46, i8** %arrayidx18, align 4, !tbaa !2
  %50 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx19 = getelementptr inbounds i8*, i8** %50, i32 %51
  %52 = load i8*, i8** %arrayidx19, align 4, !tbaa !2
  %53 = load i8**, i8*** %xbuf0, align 4, !tbaa !2
  %54 = load i32, i32* %rgroup, align 4, !tbaa !20
  %55 = load i32, i32* %M, align 4, !tbaa !20
  %add20 = add nsw i32 %55, 2
  %mul21 = mul nsw i32 %54, %add20
  %56 = load i32, i32* %i, align 4, !tbaa !20
  %add22 = add nsw i32 %mul21, %56
  %arrayidx23 = getelementptr inbounds i8*, i8** %53, i32 %add22
  store i8* %52, i8** %arrayidx23, align 4, !tbaa !2
  %57 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %58 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx24 = getelementptr inbounds i8*, i8** %57, i32 %58
  %59 = load i8*, i8** %arrayidx24, align 4, !tbaa !2
  %60 = load i8**, i8*** %xbuf1, align 4, !tbaa !2
  %61 = load i32, i32* %rgroup, align 4, !tbaa !20
  %62 = load i32, i32* %M, align 4, !tbaa !20
  %add25 = add nsw i32 %62, 2
  %mul26 = mul nsw i32 %61, %add25
  %63 = load i32, i32* %i, align 4, !tbaa !20
  %add27 = add nsw i32 %mul26, %63
  %arrayidx28 = getelementptr inbounds i8*, i8** %60, i32 %add27
  store i8* %59, i8** %arrayidx28, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %64 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond6

for.end:                                          ; preds = %for.cond6
  br label %for.inc29

for.inc29:                                        ; preds = %for.end
  %65 = load i32, i32* %ci, align 4, !tbaa !20
  %inc30 = add nsw i32 %65, 1
  store i32 %inc30, i32* %ci, align 4, !tbaa !20
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end31:                                        ; preds = %for.cond
  %67 = bitcast i8*** %xbuf1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #3
  %68 = bitcast i8*** %xbuf0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #3
  %69 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #3
  %70 = bitcast i32* %M to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  %71 = bitcast i32* %rgroup to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #3
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  %74 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  ret void
}

; Function Attrs: nounwind
define internal void @increment_simple_rowgroup_ctr(%struct.jpeg_decompress_struct* %cinfo, i32 %rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %rows.addr = alloca i32, align 4
  %rows_left = alloca i32, align 4
  %main_ptr = alloca %struct.my_main_controller*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %rows, i32* %rows.addr, align 4, !tbaa !20
  %0 = bitcast i32* %rows_left to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %main = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 78
  %3 = load %struct.jpeg_d_main_controller*, %struct.jpeg_d_main_controller** %main, align 8, !tbaa !37
  %4 = bitcast %struct.jpeg_d_main_controller* %3 to %struct.my_main_controller*
  store %struct.my_main_controller* %4, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %5 = load i32, i32* %rows.addr, align 4, !tbaa !20
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 62
  %7 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !56
  %div = udiv i32 %5, %7
  %8 = load %struct.my_main_controller*, %struct.my_main_controller** %main_ptr, align 4, !tbaa !2
  %rowgroup_ctr = getelementptr inbounds %struct.my_main_controller, %struct.my_main_controller* %8, i32 0, i32 3
  %9 = load i32, i32* %rowgroup_ctr, align 4, !tbaa !62
  %add = add i32 %9, %div
  store i32 %add, i32* %rowgroup_ctr, align 4, !tbaa !62
  %10 = load i32, i32* %rows.addr, align 4, !tbaa !20
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 62
  %12 = load i32, i32* %max_v_samp_factor1, align 8, !tbaa !56
  %rem = urem i32 %10, %12
  store i32 %rem, i32* %rows_left, align 4, !tbaa !20
  %13 = load i32, i32* %rows.addr, align 4, !tbaa !20
  %14 = load i32, i32* %rows_left, align 4, !tbaa !20
  %sub = sub i32 %13, %14
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 34
  %16 = load i32, i32* %output_scanline, align 4, !tbaa !34
  %add2 = add i32 %16, %sub
  store i32 %add2, i32* %output_scanline, align 4, !tbaa !34
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = load i32, i32* %rows_left, align 4, !tbaa !20
  call void @read_and_discard_scanlines(%struct.jpeg_decompress_struct* %17, i32 %18)
  %19 = bitcast %struct.my_main_controller** %main_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %rows_left to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  ret void
}

; Function Attrs: nounwind
define internal void @start_iMCU_row(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 79
  %2 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !54
  %3 = bitcast %struct.jpeg_d_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 66
  %5 = load i32, i32* %comps_in_scan, align 8, !tbaa !82
  %cmp = icmp sgt i32 %5, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %6, i32 0, i32 3
  store i32 1, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !68
  br label %if.end9

if.else:                                          ; preds = %entry
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 36
  %8 = load i32, i32* %input_iMCU_row, align 4, !tbaa !75
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 64
  %10 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !23
  %sub = sub i32 %10, 1
  %cmp2 = icmp ult i32 %8, %sub
  br i1 %cmp2, label %if.then3, label %if.else5

if.then3:                                         ; preds = %if.else
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 67
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 0
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %12, i32 0, i32 3
  %13 = load i32, i32* %v_samp_factor, align 4, !tbaa !80
  %14 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row4 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %14, i32 0, i32 3
  store i32 %13, i32* %MCU_rows_per_iMCU_row4, align 4, !tbaa !68
  br label %if.end

if.else5:                                         ; preds = %if.else
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 67
  %arrayidx7 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info6, i32 0, i32 0
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx7, align 4, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 0, i32 18
  %17 = load i32, i32* %last_row_height, align 4, !tbaa !83
  %18 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row8 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %18, i32 0, i32 3
  store i32 %17, i32* %MCU_rows_per_iMCU_row8, align 4, !tbaa !68
  br label %if.end

if.end:                                           ; preds = %if.else5, %if.then3
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %19 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %19, i32 0, i32 1
  store i32 0, i32* %MCU_ctr, align 4, !tbaa !84
  %20 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %20, i32 0, i32 2
  store i32 0, i32* %MCU_vert_offset, align 4, !tbaa !85
  %21 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  ret void
}

; Function Attrs: nounwind
define hidden i32 @jpeg_read_raw_data(%struct.jpeg_decompress_struct* %cinfo, i8*** %data, i32 %max_lines) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %data.addr = alloca i8***, align 4
  %max_lines.addr = alloca i32, align 4
  %lines_per_iMCU_row = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %data, i8**** %data.addr, align 4, !tbaa !2
  store i32 %max_lines, i32* %max_lines.addr, align 4, !tbaa !20
  %0 = bitcast i32* %lines_per_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 5
  %2 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp ne i32 %2, 206
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !27
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 5
  %6 = load i32, i32* %global_state1, align 4, !tbaa !6
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !26
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %6, i32* %arrayidx, align 4, !tbaa !29
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 0
  %11 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %13 = bitcast %struct.jpeg_decompress_struct* %12 to %struct.jpeg_common_struct*
  call void %11(%struct.jpeg_common_struct* %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 34
  %15 = load i32, i32* %output_scanline, align 4, !tbaa !34
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 28
  %17 = load i32, i32* %output_height, align 4, !tbaa !36
  %cmp4 = icmp uge i32 %15, %17
  br i1 %cmp4, label %if.then5, label %if.end9

if.then5:                                         ; preds = %if.end
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 0
  %19 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !26
  %msg_code7 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %19, i32 0, i32 5
  store i32 123, i32* %msg_code7, align 4, !tbaa !27
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 0
  %21 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !26
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %21, i32 0, i32 1
  %22 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !53
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_decompress_struct* %23 to %struct.jpeg_common_struct*
  call void %22(%struct.jpeg_common_struct* %24, i32 -1)
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end9:                                          ; preds = %if.end
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 2
  %26 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress, align 8, !tbaa !15
  %cmp10 = icmp ne %struct.jpeg_progress_mgr* %26, null
  br i1 %cmp10, label %if.then11, label %if.end18

if.then11:                                        ; preds = %if.end9
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 34
  %28 = load i32, i32* %output_scanline12, align 4, !tbaa !34
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 2
  %30 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress13, align 8, !tbaa !15
  %pass_counter = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %30, i32 0, i32 1
  store i32 %28, i32* %pass_counter, align 4, !tbaa !21
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 28
  %32 = load i32, i32* %output_height14, align 4, !tbaa !36
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 2
  %34 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress15, align 8, !tbaa !15
  %pass_limit = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %34, i32 0, i32 2
  store i32 %32, i32* %pass_limit, align 4, !tbaa !22
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progress16 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 2
  %36 = load %struct.jpeg_progress_mgr*, %struct.jpeg_progress_mgr** %progress16, align 8, !tbaa !15
  %progress_monitor = getelementptr inbounds %struct.jpeg_progress_mgr, %struct.jpeg_progress_mgr* %36, i32 0, i32 0
  %progress_monitor17 = bitcast {}** %progress_monitor to void (%struct.jpeg_common_struct*)**
  %37 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %progress_monitor17, align 4, !tbaa !16
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %39 = bitcast %struct.jpeg_decompress_struct* %38 to %struct.jpeg_common_struct*
  call void %37(%struct.jpeg_common_struct* %39)
  br label %if.end18

if.end18:                                         ; preds = %if.then11, %if.end9
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 62
  %41 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !56
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %min_DCT_scaled_size = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 63
  %43 = load i32, i32* %min_DCT_scaled_size, align 4, !tbaa !43
  %mul = mul nsw i32 %41, %43
  store i32 %mul, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %44 = load i32, i32* %max_lines.addr, align 4, !tbaa !20
  %45 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %cmp19 = icmp ult i32 %44, %45
  br i1 %cmp19, label %if.then20, label %if.end25

if.then20:                                        ; preds = %if.end18
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 0
  %47 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err21, align 8, !tbaa !26
  %msg_code22 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %47, i32 0, i32 5
  store i32 23, i32* %msg_code22, align 4, !tbaa !27
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err23 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 0
  %49 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err23, align 8, !tbaa !26
  %error_exit24 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %49, i32 0, i32 0
  %50 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit24, align 4, !tbaa !30
  %51 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %52 = bitcast %struct.jpeg_decompress_struct* %51 to %struct.jpeg_common_struct*
  call void %50(%struct.jpeg_common_struct* %52)
  br label %if.end25

if.end25:                                         ; preds = %if.then20, %if.end18
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %53, i32 0, i32 79
  %54 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef, align 4, !tbaa !54
  %decompress_data = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %54, i32 0, i32 3
  %55 = load i32 (%struct.jpeg_decompress_struct*, i8***)*, i32 (%struct.jpeg_decompress_struct*, i8***)** %decompress_data, align 4, !tbaa !86
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %57 = load i8***, i8**** %data.addr, align 4, !tbaa !2
  %call = call i32 %55(%struct.jpeg_decompress_struct* %56, i8*** %57)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end27, label %if.then26

if.then26:                                        ; preds = %if.end25
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %if.end25
  %58 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline28 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 34
  %60 = load i32, i32* %output_scanline28, align 4, !tbaa !34
  %add = add i32 %60, %58
  store i32 %add, i32* %output_scanline28, align 4, !tbaa !34
  %61 = load i32, i32* %lines_per_iMCU_row, align 4, !tbaa !20
  store i32 %61, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end27, %if.then26, %if.then5
  %62 = bitcast i32* %lines_per_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #3
  %63 = load i32, i32* %retval, align 4
  ret i32 %63
}

; Function Attrs: nounwind
define hidden i32 @jpeg_start_output(%struct.jpeg_decompress_struct* %cinfo, i32 %scan_number) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %scan_number.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %scan_number, i32* %scan_number.addr, align 4, !tbaa !20
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp ne i32 %1, 207
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 5
  %3 = load i32, i32* %global_state1, align 4, !tbaa !6
  %cmp2 = icmp ne i32 %3, 204
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 0
  %5 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %5, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !27
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 5
  %7 = load i32, i32* %global_state3, align 4, !tbaa !6
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !26
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %7, i32* %arrayidx, align 4, !tbaa !29
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err5, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_decompress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %15 = load i32, i32* %scan_number.addr, align 4, !tbaa !20
  %cmp6 = icmp sle i32 %15, 0
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  store i32 1, i32* %scan_number.addr, align 4, !tbaa !20
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %if.end
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 81
  %17 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !12
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %17, i32 0, i32 5
  %18 = load i32, i32* %eoi_reached, align 4, !tbaa !87
  %tobool = icmp ne i32 %18, 0
  br i1 %tobool, label %land.lhs.true9, label %if.end13

land.lhs.true9:                                   ; preds = %if.end8
  %19 = load i32, i32* %scan_number.addr, align 4, !tbaa !20
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 35
  %21 = load i32, i32* %input_scan_number, align 8, !tbaa !24
  %cmp10 = icmp sgt i32 %19, %21
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %land.lhs.true9
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 35
  %23 = load i32, i32* %input_scan_number12, align 8, !tbaa !24
  store i32 %23, i32* %scan_number.addr, align 4, !tbaa !20
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %land.lhs.true9, %if.end8
  %24 = load i32, i32* %scan_number.addr, align 4, !tbaa !20
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 37
  store i32 %24, i32* %output_scan_number, align 8, !tbaa !25
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @output_pass_setup(%struct.jpeg_decompress_struct* %26)
  ret i32 %call
}

; Function Attrs: nounwind
define hidden i32 @jpeg_finish_output(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 5
  %1 = load i32, i32* %global_state, align 4, !tbaa !6
  %cmp = icmp eq i32 %1, 205
  br i1 %cmp, label %land.lhs.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 5
  %3 = load i32, i32* %global_state1, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %3, 206
  br i1 %cmp2, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %lor.lhs.false, %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %buffered_image = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 15
  %5 = load i32, i32* %buffered_image, align 8, !tbaa !11
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 77
  %7 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !31
  %finish_output_pass = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %7, i32 0, i32 1
  %8 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %finish_output_pass, align 4, !tbaa !40
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %8(%struct.jpeg_decompress_struct* %9)
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 5
  store i32 208, i32* %global_state3, align 4, !tbaa !6
  br label %if.end10

if.else:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 5
  %12 = load i32, i32* %global_state4, align 4, !tbaa !6
  %cmp5 = icmp ne i32 %12, 208
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !26
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 5
  store i32 20, i32* %msg_code, align 4, !tbaa !27
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 5
  %16 = load i32, i32* %global_state7, align 4, !tbaa !6
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !26
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 %16, i32* %arrayidx, align 4, !tbaa !29
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !26
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 0
  %21 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !30
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = bitcast %struct.jpeg_decompress_struct* %22 to %struct.jpeg_common_struct*
  call void %21(%struct.jpeg_common_struct* %23)
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.else
  br label %if.end10

if.end10:                                         ; preds = %if.end, %if.then
  br label %while.cond

while.cond:                                       ; preds = %if.end17, %if.end10
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 35
  %25 = load i32, i32* %input_scan_number, align 8, !tbaa !24
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 37
  %27 = load i32, i32* %output_scan_number, align 8, !tbaa !25
  %cmp11 = icmp sle i32 %25, %27
  br i1 %cmp11, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 81
  %29 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !12
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %29, i32 0, i32 5
  %30 = load i32, i32* %eoi_reached, align 4, !tbaa !87
  %tobool12 = icmp ne i32 %30, 0
  %lnot = xor i1 %tobool12, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %31 = phi i1 [ false, %while.cond ], [ %lnot, %land.rhs ]
  br i1 %31, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 81
  %33 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl13, align 4, !tbaa !12
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %33, i32 0, i32 0
  %consume_input14 = bitcast {}** %consume_input to i32 (%struct.jpeg_decompress_struct*)**
  %34 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_input14, align 4, !tbaa !19
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %34(%struct.jpeg_decompress_struct* %35)
  %cmp15 = icmp eq i32 %call, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %while.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %land.end
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %global_state18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 5
  store i32 207, i32* %global_state18, align 4, !tbaa !6
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then16
  %37 = load i32, i32* %retval, align 4
  ret i32 %37
}

; Function Attrs: nounwind
define internal void @noop_convert(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %input_row, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %input_row.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %input_row, i32* %input_row.addr, align 4, !tbaa !20
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !20
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 20}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!7, !8, i64 64}
!12 = !{!7, !3, i64 436}
!13 = !{!14, !8, i64 16}
!14 = !{!"jpeg_input_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20}
!15 = !{!7, !3, i64 8}
!16 = !{!17, !3, i64 0}
!17 = !{!"jpeg_progress_mgr", !3, i64 0, !18, i64 4, !18, i64 8, !8, i64 12, !8, i64 16}
!18 = !{!"long", !4, i64 0}
!19 = !{!14, !3, i64 0}
!20 = !{!8, !8, i64 0}
!21 = !{!17, !18, i64 4}
!22 = !{!17, !18, i64 8}
!23 = !{!7, !8, i64 320}
!24 = !{!7, !8, i64 144}
!25 = !{!7, !8, i64 152}
!26 = !{!7, !3, i64 0}
!27 = !{!28, !8, i64 20}
!28 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !18, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!29 = !{!4, !4, i64 0}
!30 = !{!28, !3, i64 0}
!31 = !{!7, !3, i64 420}
!32 = !{!33, !3, i64 0}
!33 = !{!"jpeg_decomp_master", !3, i64 0, !3, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !4, i64 20, !4, i64 60, !8, i64 100}
!34 = !{!7, !8, i64 140}
!35 = !{!33, !8, i64 8}
!36 = !{!7, !8, i64 116}
!37 = !{!7, !3, i64 424}
!38 = !{!39, !3, i64 4}
!39 = !{!"jpeg_d_main_controller", !3, i64 0, !3, i64 4}
!40 = !{!33, !3, i64 4}
!41 = !{!7, !8, i64 68}
!42 = !{!7, !8, i64 112}
!43 = !{!7, !8, i64 316}
!44 = !{!7, !8, i64 308}
!45 = !{!33, !8, i64 12}
!46 = !{!33, !8, i64 16}
!47 = !{!7, !3, i64 216}
!48 = !{!7, !8, i64 36}
!49 = !{!50, !8, i64 40}
!50 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!51 = !{!50, !8, i64 8}
!52 = !{!33, !8, i64 100}
!53 = !{!28, !3, i64 4}
!54 = !{!7, !3, i64 428}
!55 = !{!7, !3, i64 452}
!56 = !{!7, !8, i64 312}
!57 = !{!58, !8, i64 8}
!58 = !{!"jpeg_upsampler", !3, i64 0, !3, i64 4, !8, i64 8}
!59 = !{!60, !8, i64 48}
!60 = !{!"", !39, i64 0, !4, i64 8, !8, i64 48, !8, i64 52, !4, i64 56, !8, i64 64, !8, i64 68, !8, i64 72, !8, i64 76}
!61 = !{!60, !8, i64 76}
!62 = !{!60, !8, i64 52}
!63 = !{!60, !8, i64 68}
!64 = !{!65, !8, i64 92}
!65 = !{!"", !58, i64 0, !4, i64 12, !4, i64 52, !8, i64 92, !8, i64 96, !4, i64 100, !4, i64 140, !4, i64 150}
!66 = !{!65, !8, i64 96}
!67 = !{!7, !8, i64 156}
!68 = !{!69, !8, i64 28}
!69 = !{!"", !70, i64 0, !8, i64 20, !8, i64 24, !8, i64 28, !4, i64 32, !3, i64 72, !4, i64 76, !3, i64 116}
!70 = !{!"jpeg_d_coef_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!71 = !{!7, !8, i64 348}
!72 = !{!7, !3, i64 444}
!73 = !{!74, !3, i64 4}
!74 = !{!"jpeg_entropy_decoder", !3, i64 0, !3, i64 4, !8, i64 8}
!75 = !{!7, !8, i64 148}
!76 = !{!14, !3, i64 12}
!77 = !{!7, !3, i64 456}
!78 = !{!79, !3, i64 4}
!79 = !{!"jpeg_color_deconverter", !3, i64 0, !3, i64 4}
!80 = !{!50, !8, i64 12}
!81 = !{!50, !8, i64 36}
!82 = !{!7, !8, i64 328}
!83 = !{!50, !8, i64 72}
!84 = !{!69, !8, i64 20}
!85 = !{!69, !8, i64 24}
!86 = !{!70, !3, i64 12}
!87 = !{!14, !8, i64 20}
