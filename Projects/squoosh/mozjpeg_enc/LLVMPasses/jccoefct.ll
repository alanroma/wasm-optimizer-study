; ModuleID = 'jccoefct.c'
source_filename = "jccoefct.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, {}* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { {}*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.my_coef_controller = type { %struct.jpeg_c_coef_controller, i32, i32, i32, i32, [10 x [64 x i16]*], [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*] }
%struct.c_derived_tbl = type { [256 x i32], [256 x i8] }

; Function Attrs: nounwind
define hidden void @jinit_c_coef_controller(%struct.jpeg_compress_struct* %cinfo, i32 %need_full_buffer) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %need_full_buffer.addr = alloca i32, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %buffer = alloca [64 x i16]*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %need_full_buffer, i32* %need_full_buffer.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %2, i32 0, i32 0
  %3 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_compress_struct* %4 to %struct.jpeg_common_struct*
  %call = call i8* %3(%struct.jpeg_common_struct* %5, i32 1, i32 144)
  %6 = bitcast i8* %call to %struct.my_coef_controller*
  store %struct.my_coef_controller* %6, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %7 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %8 = bitcast %struct.my_coef_controller* %7 to %struct.jpeg_c_coef_controller*
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 57
  store %struct.jpeg_c_coef_controller* %8, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !15
  %10 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %10, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %pub, i32 0, i32 0
  store void (%struct.jpeg_compress_struct*, i32)* @start_pass_coef, void (%struct.jpeg_compress_struct*, i32)** %start_pass, align 4, !tbaa !16
  %11 = load i32, i32* %need_full_buffer.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %12 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  store i32 0, i32* %ci, align 4, !tbaa !6
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 15
  %15 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !19
  store %struct.jpeg_component_info* %15, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %16 = load i32, i32* %ci, align 4, !tbaa !6
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 13
  %18 = load i32, i32* %num_components, align 4, !tbaa !20
  %cmp = icmp slt i32 %16, %18
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 1
  %20 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem2, align 4, !tbaa !8
  %request_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %20, i32 0, i32 5
  %21 = load %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)** %request_virt_barray, align 4, !tbaa !21
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = bitcast %struct.jpeg_compress_struct* %22 to %struct.jpeg_common_struct*
  %24 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %24, i32 0, i32 7
  %25 = load i32, i32* %width_in_blocks, align 4, !tbaa !22
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %26, i32 0, i32 2
  %27 = load i32, i32* %h_samp_factor, align 4, !tbaa !24
  %call3 = call i32 @jround_up(i32 %25, i32 %27)
  %28 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %28, i32 0, i32 8
  %29 = load i32, i32* %height_in_blocks, align 4, !tbaa !25
  %30 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %30, i32 0, i32 3
  %31 = load i32, i32* %v_samp_factor, align 4, !tbaa !26
  %call4 = call i32 @jround_up(i32 %29, i32 %31)
  %32 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor5 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %32, i32 0, i32 3
  %33 = load i32, i32* %v_samp_factor5, align 4, !tbaa !26
  %call6 = call %struct.jvirt_barray_control* %21(%struct.jpeg_common_struct* %23, i32 1, i32 0, i32 %call3, i32 %call4, i32 %33)
  %34 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %34, i32 0, i32 6
  %35 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 %35
  store %struct.jvirt_barray_control* %call6, %struct.jvirt_barray_control** %arrayidx, align 4, !tbaa !2
  %36 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %36, i32 0, i32 1
  %37 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem7, align 4, !tbaa !8
  %request_virt_barray8 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %37, i32 0, i32 5
  %38 = load %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)** %request_virt_barray8, align 4, !tbaa !21
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = bitcast %struct.jpeg_compress_struct* %39 to %struct.jpeg_common_struct*
  %41 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks9 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %41, i32 0, i32 7
  %42 = load i32, i32* %width_in_blocks9, align 4, !tbaa !22
  %43 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor10 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %43, i32 0, i32 2
  %44 = load i32, i32* %h_samp_factor10, align 4, !tbaa !24
  %call11 = call i32 @jround_up(i32 %42, i32 %44)
  %45 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks12 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %45, i32 0, i32 8
  %46 = load i32, i32* %height_in_blocks12, align 4, !tbaa !25
  %47 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor13 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %47, i32 0, i32 3
  %48 = load i32, i32* %v_samp_factor13, align 4, !tbaa !26
  %call14 = call i32 @jround_up(i32 %46, i32 %48)
  %49 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor15 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %49, i32 0, i32 3
  %50 = load i32, i32* %v_samp_factor15, align 4, !tbaa !26
  %call16 = call %struct.jvirt_barray_control* %38(%struct.jpeg_common_struct* %40, i32 1, i32 0, i32 %call11, i32 %call14, i32 %50)
  %51 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image_uq = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %51, i32 0, i32 7
  %52 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image_uq, i32 0, i32 %52
  store %struct.jvirt_barray_control* %call16, %struct.jvirt_barray_control** %arrayidx17, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %53 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %53, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %54 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %54, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %55 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  br label %if.end

if.else:                                          ; preds = %entry
  %57 = bitcast [64 x i16]** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #3
  %59 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem18 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %59, i32 0, i32 1
  %60 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem18, align 4, !tbaa !8
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %60, i32 0, i32 1
  %61 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !27
  %62 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %63 = bitcast %struct.jpeg_compress_struct* %62 to %struct.jpeg_common_struct*
  %call19 = call i8* %61(%struct.jpeg_common_struct* %63, i32 1, i32 1280)
  %64 = bitcast i8* %call19 to [64 x i16]*
  store [64 x i16]* %64, [64 x i16]** %buffer, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc24, %if.else
  %65 = load i32, i32* %i, align 4, !tbaa !6
  %cmp21 = icmp slt i32 %65, 10
  br i1 %cmp21, label %for.body22, label %for.end26

for.body22:                                       ; preds = %for.cond20
  %66 = load [64 x i16]*, [64 x i16]** %buffer, align 4, !tbaa !2
  %67 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %66, i32 %67
  %68 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %68, i32 0, i32 5
  %69 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx23 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %69
  store [64 x i16]* %add.ptr, [64 x i16]** %arrayidx23, align 4, !tbaa !2
  br label %for.inc24

for.inc24:                                        ; preds = %for.body22
  %70 = load i32, i32* %i, align 4, !tbaa !6
  %inc25 = add nsw i32 %70, 1
  store i32 %inc25, i32* %i, align 4, !tbaa !6
  br label %for.cond20

for.end26:                                        ; preds = %for.cond20
  %71 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image27 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %71, i32 0, i32 6
  %arrayidx28 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image27, i32 0, i32 0
  store %struct.jvirt_barray_control* null, %struct.jvirt_barray_control** %arrayidx28, align 4, !tbaa !2
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast [64 x i16]** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  br label %if.end

if.end:                                           ; preds = %for.end26, %for.end
  %74 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_coef(%struct.jpeg_compress_struct* %cinfo, i32 %pass_mode) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %pass_mode.addr = alloca i32, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pass_mode, i32* %pass_mode.addr, align 4, !tbaa !28
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %4, i32 0, i32 1
  store i32 0, i32* %iMCU_row_num, align 4, !tbaa !29
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_compress_struct* %5)
  %6 = load i32, i32* %pass_mode.addr, align 4, !tbaa !28
  switch i32 %6, label %sw.default [
    i32 0, label %sw.bb
    i32 3, label %sw.bb3
    i32 2, label %sw.bb15
    i32 4, label %sw.bb27
  ]

sw.bb:                                            ; preds = %entry
  %7 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %7, i32 0, i32 6
  %arrayidx = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 0
  %8 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx, align 4, !tbaa !2
  %cmp = icmp ne %struct.jvirt_barray_control* %8, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %sw.bb
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 0
  %10 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !30
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %10, i32 0, i32 5
  store i32 4, i32* %msg_code, align 4, !tbaa !31
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 0
  %12 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !30
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %12, i32 0, i32 0
  %13 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !33
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = bitcast %struct.jpeg_compress_struct* %14 to %struct.jpeg_common_struct*
  call void %13(%struct.jpeg_common_struct* %15)
  br label %if.end

if.end:                                           ; preds = %if.then, %sw.bb
  %16 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %16, i32 0, i32 0
  %compress_data = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %pub, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, i8***)* @compress_data, i32 (%struct.jpeg_compress_struct*, i8***)** %compress_data, align 4, !tbaa !34
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  %17 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image4 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %17, i32 0, i32 6
  %arrayidx5 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image4, i32 0, i32 0
  %18 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx5, align 4, !tbaa !2
  %cmp6 = icmp eq %struct.jvirt_barray_control* %18, null
  br i1 %cmp6, label %if.then7, label %if.end12

if.then7:                                         ; preds = %sw.bb3
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err8, align 8, !tbaa !30
  %msg_code9 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 5
  store i32 4, i32* %msg_code9, align 4, !tbaa !31
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !30
  %error_exit11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 0
  %23 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit11, align 4, !tbaa !33
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = bitcast %struct.jpeg_compress_struct* %24 to %struct.jpeg_common_struct*
  call void %23(%struct.jpeg_common_struct* %25)
  br label %if.end12

if.end12:                                         ; preds = %if.then7, %sw.bb3
  %26 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub13 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %26, i32 0, i32 0
  %compress_data14 = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %pub13, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, i8***)* @compress_first_pass, i32 (%struct.jpeg_compress_struct*, i8***)** %compress_data14, align 4, !tbaa !34
  br label %sw.epilog

sw.bb15:                                          ; preds = %entry
  %27 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image16 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %27, i32 0, i32 6
  %arrayidx17 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image16, i32 0, i32 0
  %28 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx17, align 4, !tbaa !2
  %cmp18 = icmp eq %struct.jvirt_barray_control* %28, null
  br i1 %cmp18, label %if.then19, label %if.end24

if.then19:                                        ; preds = %sw.bb15
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err20 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 0
  %30 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err20, align 8, !tbaa !30
  %msg_code21 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %30, i32 0, i32 5
  store i32 4, i32* %msg_code21, align 4, !tbaa !31
  %31 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err22 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %31, i32 0, i32 0
  %32 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err22, align 8, !tbaa !30
  %error_exit23 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %32, i32 0, i32 0
  %33 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit23, align 4, !tbaa !33
  %34 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %35 = bitcast %struct.jpeg_compress_struct* %34 to %struct.jpeg_common_struct*
  call void %33(%struct.jpeg_common_struct* %35)
  br label %if.end24

if.end24:                                         ; preds = %if.then19, %sw.bb15
  %36 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub25 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %36, i32 0, i32 0
  %compress_data26 = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %pub25, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, i8***)* @compress_output, i32 (%struct.jpeg_compress_struct*, i8***)** %compress_data26, align 4, !tbaa !34
  br label %sw.epilog

sw.bb27:                                          ; preds = %entry
  %37 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image28 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %37, i32 0, i32 6
  %arrayidx29 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image28, i32 0, i32 0
  %38 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx29, align 4, !tbaa !2
  %cmp30 = icmp eq %struct.jvirt_barray_control* %38, null
  br i1 %cmp30, label %if.then31, label %if.end36

if.then31:                                        ; preds = %sw.bb27
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err32 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %39, i32 0, i32 0
  %40 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err32, align 8, !tbaa !30
  %msg_code33 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %40, i32 0, i32 5
  store i32 4, i32* %msg_code33, align 4, !tbaa !31
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err34 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 0
  %42 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err34, align 8, !tbaa !30
  %error_exit35 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %42, i32 0, i32 0
  %43 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit35, align 4, !tbaa !33
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %45 = bitcast %struct.jpeg_compress_struct* %44 to %struct.jpeg_common_struct*
  call void %43(%struct.jpeg_common_struct* %45)
  br label %if.end36

if.end36:                                         ; preds = %if.then31, %sw.bb27
  %46 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub37 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %46, i32 0, i32 0
  %compress_data38 = getelementptr inbounds %struct.jpeg_c_coef_controller, %struct.jpeg_c_coef_controller* %pub37, i32 0, i32 1
  store i32 (%struct.jpeg_compress_struct*, i8***)* @compress_trellis_pass, i32 (%struct.jpeg_compress_struct*, i8***)** %compress_data38, align 4, !tbaa !34
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err39 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %47, i32 0, i32 0
  %48 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err39, align 8, !tbaa !30
  %msg_code40 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %48, i32 0, i32 5
  store i32 4, i32* %msg_code40, align 4, !tbaa !31
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err41 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %49, i32 0, i32 0
  %50 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err41, align 8, !tbaa !30
  %error_exit42 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %50, i32 0, i32 0
  %51 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit42, align 4, !tbaa !33
  %52 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %53 = bitcast %struct.jpeg_compress_struct* %52 to %struct.jpeg_common_struct*
  call void %51(%struct.jpeg_common_struct* %53)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end36, %if.end24, %if.end12, %if.end
  %54 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  ret void
}

declare i32 @jround_up(i32, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_iMCU_row(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 44
  %5 = load i32, i32* %comps_in_scan, align 4, !tbaa !35
  %cmp = icmp sgt i32 %5, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %6, i32 0, i32 4
  store i32 1, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !36
  br label %if.end9

if.else:                                          ; preds = %entry
  %7 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %7, i32 0, i32 1
  %8 = load i32, i32* %iMCU_row_num, align 4, !tbaa !29
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 43
  %10 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !37
  %sub = sub i32 %10, 1
  %cmp2 = icmp ult i32 %8, %sub
  br i1 %cmp2, label %if.then3, label %if.else5

if.then3:                                         ; preds = %if.else
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 45
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 0
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 8, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %12, i32 0, i32 3
  %13 = load i32, i32* %v_samp_factor, align 4, !tbaa !26
  %14 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row4 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %14, i32 0, i32 4
  store i32 %13, i32* %MCU_rows_per_iMCU_row4, align 4, !tbaa !36
  br label %if.end

if.else5:                                         ; preds = %if.else
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 45
  %arrayidx7 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info6, i32 0, i32 0
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx7, align 8, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 0, i32 18
  %17 = load i32, i32* %last_row_height, align 4, !tbaa !38
  %18 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row8 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %18, i32 0, i32 4
  store i32 %17, i32* %MCU_rows_per_iMCU_row8, align 4, !tbaa !36
  br label %if.end

if.end:                                           ; preds = %if.else5, %if.then3
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %19 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %19, i32 0, i32 2
  store i32 0, i32* %mcu_ctr, align 4, !tbaa !39
  %20 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %20, i32 0, i32 3
  store i32 0, i32* %MCU_vert_offset, align 4, !tbaa !40
  %21 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @compress_data(%struct.jpeg_compress_struct* %cinfo, i8*** %input_buf) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %MCU_col_num = alloca i32, align 4
  %last_MCU_col = alloca i32, align 4
  %last_iMCU_row = alloca i32, align 4
  %blkn = alloca i32, align 4
  %bi = alloca i32, align 4
  %ci = alloca i32, align 4
  %yindex = alloca i32, align 4
  %yoffset = alloca i32, align 4
  %blockcnt = alloca i32, align 4
  %ypos = alloca i32, align 4
  %xpos = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %last_MCU_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 46
  %7 = load i32, i32* %MCUs_per_row, align 8, !tbaa !41
  %sub = sub i32 %7, 1
  store i32 %sub, i32* %last_MCU_col, align 4, !tbaa !6
  %8 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 43
  %10 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !37
  %sub2 = sub i32 %10, 1
  store i32 %sub2, i32* %last_iMCU_row, align 4, !tbaa !6
  %11 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %blockcnt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i32* %ypos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %xpos to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %20, i32 0, i32 3
  %21 = load i32, i32* %MCU_vert_offset, align 4, !tbaa !40
  store i32 %21, i32* %yoffset, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc82, %entry
  %22 = load i32, i32* %yoffset, align 4, !tbaa !6
  %23 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %23, i32 0, i32 4
  %24 = load i32, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !36
  %cmp = icmp slt i32 %22, %24
  br i1 %cmp, label %for.body, label %for.end84

for.body:                                         ; preds = %for.cond
  %25 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %25, i32 0, i32 2
  %26 = load i32, i32* %mcu_ctr, align 4, !tbaa !39
  store i32 %26, i32* %MCU_col_num, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc78, %for.body
  %27 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %28 = load i32, i32* %last_MCU_col, align 4, !tbaa !6
  %cmp4 = icmp ule i32 %27, %28
  br i1 %cmp4, label %for.body5, label %for.end80

for.body5:                                        ; preds = %for.cond3
  store i32 0, i32* %blkn, align 4, !tbaa !6
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc70, %for.body5
  %29 = load i32, i32* %ci, align 4, !tbaa !6
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 44
  %31 = load i32, i32* %comps_in_scan, align 4, !tbaa !35
  %cmp7 = icmp slt i32 %29, %31
  br i1 %cmp7, label %for.body8, label %for.end72

for.body8:                                        ; preds = %for.cond6
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 45
  %33 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %33
  %34 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %34, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %35 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %36 = load i32, i32* %last_MCU_col, align 4, !tbaa !6
  %cmp9 = icmp ult i32 %35, %36
  br i1 %cmp9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body8
  %37 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %37, i32 0, i32 13
  %38 = load i32, i32* %MCU_width, align 4, !tbaa !42
  br label %cond.end

cond.false:                                       ; preds = %for.body8
  %39 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_col_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %39, i32 0, i32 17
  %40 = load i32, i32* %last_col_width, align 4, !tbaa !43
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %38, %cond.true ], [ %40, %cond.false ]
  store i32 %cond, i32* %blockcnt, align 4, !tbaa !6
  %41 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %42 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_sample_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %42, i32 0, i32 16
  %43 = load i32, i32* %MCU_sample_width, align 4, !tbaa !44
  %mul = mul i32 %41, %43
  store i32 %mul, i32* %xpos, align 4, !tbaa !6
  %44 = load i32, i32* %yoffset, align 4, !tbaa !6
  %mul10 = mul nsw i32 %44, 8
  store i32 %mul10, i32* %ypos, align 4, !tbaa !6
  store i32 0, i32* %yindex, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc67, %cond.end
  %45 = load i32, i32* %yindex, align 4, !tbaa !6
  %46 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %46, i32 0, i32 14
  %47 = load i32, i32* %MCU_height, align 4, !tbaa !45
  %cmp12 = icmp slt i32 %45, %47
  br i1 %cmp12, label %for.body13, label %for.end69

for.body13:                                       ; preds = %for.cond11
  %48 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %48, i32 0, i32 1
  %49 = load i32, i32* %iMCU_row_num, align 4, !tbaa !29
  %50 = load i32, i32* %last_iMCU_row, align 4, !tbaa !6
  %cmp14 = icmp ult i32 %49, %50
  br i1 %cmp14, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body13
  %51 = load i32, i32* %yoffset, align 4, !tbaa !6
  %52 = load i32, i32* %yindex, align 4, !tbaa !6
  %add = add nsw i32 %51, %52
  %53 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %53, i32 0, i32 18
  %54 = load i32, i32* %last_row_height, align 4, !tbaa !38
  %cmp15 = icmp slt i32 %add, %54
  br i1 %cmp15, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %for.body13
  %55 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %fdct = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %55, i32 0, i32 61
  %56 = load %struct.jpeg_forward_dct*, %struct.jpeg_forward_dct** %fdct, align 8, !tbaa !46
  %forward_DCT = getelementptr inbounds %struct.jpeg_forward_dct, %struct.jpeg_forward_dct* %56, i32 0, i32 1
  %57 = load void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)** %forward_DCT, align 4, !tbaa !47
  %58 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %59 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %60 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %61 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %61, i32 0, i32 1
  %62 = load i32, i32* %component_index, align 4, !tbaa !49
  %arrayidx16 = getelementptr inbounds i8**, i8*** %60, i32 %62
  %63 = load i8**, i8*** %arrayidx16, align 4, !tbaa !2
  %64 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %64, i32 0, i32 5
  %65 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %65
  %66 = load [64 x i16]*, [64 x i16]** %arrayidx17, align 4, !tbaa !2
  %67 = load i32, i32* %ypos, align 4, !tbaa !6
  %68 = load i32, i32* %xpos, align 4, !tbaa !6
  %69 = load i32, i32* %blockcnt, align 4, !tbaa !6
  call void %57(%struct.jpeg_compress_struct* %58, %struct.jpeg_component_info* %59, i8** %63, [64 x i16]* %66, i32 %67, i32 %68, i32 %69, [64 x i16]* null)
  %70 = load i32, i32* %blockcnt, align 4, !tbaa !6
  %71 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width18 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %71, i32 0, i32 13
  %72 = load i32, i32* %MCU_width18, align 4, !tbaa !42
  %cmp19 = icmp slt i32 %70, %72
  br i1 %cmp19, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then
  %73 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer21 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %73, i32 0, i32 5
  %74 = load i32, i32* %blkn, align 4, !tbaa !6
  %75 = load i32, i32* %blockcnt, align 4, !tbaa !6
  %add22 = add nsw i32 %74, %75
  %arrayidx23 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer21, i32 0, i32 %add22
  %76 = load [64 x i16]*, [64 x i16]** %arrayidx23, align 4, !tbaa !2
  %77 = bitcast [64 x i16]* %76 to i8*
  %78 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width24 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %78, i32 0, i32 13
  %79 = load i32, i32* %MCU_width24, align 4, !tbaa !42
  %80 = load i32, i32* %blockcnt, align 4, !tbaa !6
  %sub25 = sub nsw i32 %79, %80
  %mul26 = mul i32 %sub25, 128
  call void @jzero_far(i8* %77, i32 %mul26)
  %81 = load i32, i32* %blockcnt, align 4, !tbaa !6
  store i32 %81, i32* %bi, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc, %if.then20
  %82 = load i32, i32* %bi, align 4, !tbaa !6
  %83 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width28 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %83, i32 0, i32 13
  %84 = load i32, i32* %MCU_width28, align 4, !tbaa !42
  %cmp29 = icmp slt i32 %82, %84
  br i1 %cmp29, label %for.body30, label %for.end

for.body30:                                       ; preds = %for.cond27
  %85 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer31 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %85, i32 0, i32 5
  %86 = load i32, i32* %blkn, align 4, !tbaa !6
  %87 = load i32, i32* %bi, align 4, !tbaa !6
  %add32 = add nsw i32 %86, %87
  %sub33 = sub nsw i32 %add32, 1
  %arrayidx34 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer31, i32 0, i32 %sub33
  %88 = load [64 x i16]*, [64 x i16]** %arrayidx34, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds [64 x i16], [64 x i16]* %88, i32 0
  %arrayidx36 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx35, i32 0, i32 0
  %89 = load i16, i16* %arrayidx36, align 2, !tbaa !50
  %90 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer37 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %90, i32 0, i32 5
  %91 = load i32, i32* %blkn, align 4, !tbaa !6
  %92 = load i32, i32* %bi, align 4, !tbaa !6
  %add38 = add nsw i32 %91, %92
  %arrayidx39 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer37, i32 0, i32 %add38
  %93 = load [64 x i16]*, [64 x i16]** %arrayidx39, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds [64 x i16], [64 x i16]* %93, i32 0
  %arrayidx41 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx40, i32 0, i32 0
  store i16 %89, i16* %arrayidx41, align 2, !tbaa !50
  br label %for.inc

for.inc:                                          ; preds = %for.body30
  %94 = load i32, i32* %bi, align 4, !tbaa !6
  %inc = add nsw i32 %94, 1
  store i32 %inc, i32* %bi, align 4, !tbaa !6
  br label %for.cond27

for.end:                                          ; preds = %for.cond27
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  br label %if.end63

if.else:                                          ; preds = %lor.lhs.false
  %95 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer42 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %95, i32 0, i32 5
  %96 = load i32, i32* %blkn, align 4, !tbaa !6
  %arrayidx43 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer42, i32 0, i32 %96
  %97 = load [64 x i16]*, [64 x i16]** %arrayidx43, align 4, !tbaa !2
  %98 = bitcast [64 x i16]* %97 to i8*
  %99 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width44 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %99, i32 0, i32 13
  %100 = load i32, i32* %MCU_width44, align 4, !tbaa !42
  %mul45 = mul i32 %100, 128
  call void @jzero_far(i8* %98, i32 %mul45)
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc60, %if.else
  %101 = load i32, i32* %bi, align 4, !tbaa !6
  %102 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width47 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %102, i32 0, i32 13
  %103 = load i32, i32* %MCU_width47, align 4, !tbaa !42
  %cmp48 = icmp slt i32 %101, %103
  br i1 %cmp48, label %for.body49, label %for.end62

for.body49:                                       ; preds = %for.cond46
  %104 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer50 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %104, i32 0, i32 5
  %105 = load i32, i32* %blkn, align 4, !tbaa !6
  %sub51 = sub nsw i32 %105, 1
  %arrayidx52 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer50, i32 0, i32 %sub51
  %106 = load [64 x i16]*, [64 x i16]** %arrayidx52, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds [64 x i16], [64 x i16]* %106, i32 0
  %arrayidx54 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx53, i32 0, i32 0
  %107 = load i16, i16* %arrayidx54, align 2, !tbaa !50
  %108 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer55 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %108, i32 0, i32 5
  %109 = load i32, i32* %blkn, align 4, !tbaa !6
  %110 = load i32, i32* %bi, align 4, !tbaa !6
  %add56 = add nsw i32 %109, %110
  %arrayidx57 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer55, i32 0, i32 %add56
  %111 = load [64 x i16]*, [64 x i16]** %arrayidx57, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds [64 x i16], [64 x i16]* %111, i32 0
  %arrayidx59 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx58, i32 0, i32 0
  store i16 %107, i16* %arrayidx59, align 2, !tbaa !50
  br label %for.inc60

for.inc60:                                        ; preds = %for.body49
  %112 = load i32, i32* %bi, align 4, !tbaa !6
  %inc61 = add nsw i32 %112, 1
  store i32 %inc61, i32* %bi, align 4, !tbaa !6
  br label %for.cond46

for.end62:                                        ; preds = %for.cond46
  br label %if.end63

if.end63:                                         ; preds = %for.end62, %if.end
  %113 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width64 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %113, i32 0, i32 13
  %114 = load i32, i32* %MCU_width64, align 4, !tbaa !42
  %115 = load i32, i32* %blkn, align 4, !tbaa !6
  %add65 = add nsw i32 %115, %114
  store i32 %add65, i32* %blkn, align 4, !tbaa !6
  %116 = load i32, i32* %ypos, align 4, !tbaa !6
  %add66 = add i32 %116, 8
  store i32 %add66, i32* %ypos, align 4, !tbaa !6
  br label %for.inc67

for.inc67:                                        ; preds = %if.end63
  %117 = load i32, i32* %yindex, align 4, !tbaa !6
  %inc68 = add nsw i32 %117, 1
  store i32 %inc68, i32* %yindex, align 4, !tbaa !6
  br label %for.cond11

for.end69:                                        ; preds = %for.cond11
  br label %for.inc70

for.inc70:                                        ; preds = %for.end69
  %118 = load i32, i32* %ci, align 4, !tbaa !6
  %inc71 = add nsw i32 %118, 1
  store i32 %inc71, i32* %ci, align 4, !tbaa !6
  br label %for.cond6

for.end72:                                        ; preds = %for.cond6
  %119 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %119, i32 0, i32 62
  %120 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy, align 4, !tbaa !51
  %encode_mcu = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %120, i32 0, i32 1
  %121 = load i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)** %encode_mcu, align 4, !tbaa !52
  %122 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %123 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer73 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %123, i32 0, i32 5
  %arraydecay = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer73, i32 0, i32 0
  %call = call i32 %121(%struct.jpeg_compress_struct* %122, [64 x i16]** %arraydecay)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end77, label %if.then74

if.then74:                                        ; preds = %for.end72
  %124 = load i32, i32* %yoffset, align 4, !tbaa !6
  %125 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset75 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %125, i32 0, i32 3
  store i32 %124, i32* %MCU_vert_offset75, align 4, !tbaa !40
  %126 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %127 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr76 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %127, i32 0, i32 2
  store i32 %126, i32* %mcu_ctr76, align 4, !tbaa !39
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end77:                                         ; preds = %for.end72
  br label %for.inc78

for.inc78:                                        ; preds = %if.end77
  %128 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %inc79 = add i32 %128, 1
  store i32 %inc79, i32* %MCU_col_num, align 4, !tbaa !6
  br label %for.cond3

for.end80:                                        ; preds = %for.cond3
  %129 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr81 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %129, i32 0, i32 2
  store i32 0, i32* %mcu_ctr81, align 4, !tbaa !39
  br label %for.inc82

for.inc82:                                        ; preds = %for.end80
  %130 = load i32, i32* %yoffset, align 4, !tbaa !6
  %inc83 = add nsw i32 %130, 1
  store i32 %inc83, i32* %yoffset, align 4, !tbaa !6
  br label %for.cond

for.end84:                                        ; preds = %for.cond
  %131 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num85 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %131, i32 0, i32 1
  %132 = load i32, i32* %iMCU_row_num85, align 4, !tbaa !29
  %inc86 = add i32 %132, 1
  store i32 %inc86, i32* %iMCU_row_num85, align 4, !tbaa !29
  %133 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_compress_struct* %133)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end84, %if.then74
  %134 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #3
  %135 = bitcast i32* %xpos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #3
  %136 = bitcast i32* %ypos to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #3
  %137 = bitcast i32* %blockcnt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #3
  %138 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #3
  %139 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #3
  %140 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #3
  %141 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #3
  %142 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #3
  %143 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #3
  %144 = bitcast i32* %last_MCU_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #3
  %145 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #3
  %146 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #3
  %147 = load i32, i32* %retval, align 4
  ret i32 %147
}

; Function Attrs: nounwind
define internal i32 @compress_first_pass(%struct.jpeg_compress_struct* %cinfo, i8*** %input_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %last_iMCU_row = alloca i32, align 4
  %blocks_across = alloca i32, align 4
  %MCUs_across = alloca i32, align 4
  %MCUindex = alloca i32, align 4
  %bi = alloca i32, align 4
  %ci = alloca i32, align 4
  %h_samp_factor = alloca i32, align 4
  %block_row = alloca i32, align 4
  %block_rows = alloca i32, align 4
  %ndummy = alloca i32, align 4
  %lastDC = alloca i16, align 2
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %buffer = alloca [64 x i16]**, align 4
  %thisblockrow = alloca [64 x i16]*, align 4
  %lastblockrow = alloca [64 x i16]*, align 4
  %buffer_dst = alloca [64 x i16]**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 43
  %6 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !37
  %sub = sub i32 %6, 1
  store i32 %sub, i32* %last_iMCU_row, align 4, !tbaa !6
  %7 = bitcast i32* %blocks_across to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %MCUs_across to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %MCUindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %h_samp_factor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %block_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %ndummy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i16* %lastDC to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #3
  %17 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast [64 x i16]*** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast [64 x i16]** %thisblockrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast [64 x i16]** %lastblockrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast [64 x i16]*** %buffer_dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  store i32 0, i32* %ci, align 4, !tbaa !6
  %22 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %22, i32 0, i32 15
  %23 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !19
  store %struct.jpeg_component_info* %23, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc80, %entry
  %24 = load i32, i32* %ci, align 4, !tbaa !6
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 13
  %26 = load i32, i32* %num_components, align 4, !tbaa !20
  %cmp = icmp slt i32 %24, %26
  br i1 %cmp, label %for.body, label %for.end82

for.body:                                         ; preds = %for.cond
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 1
  %28 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %access_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %28, i32 0, i32 8
  %29 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray, align 4, !tbaa !54
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_compress_struct* %30 to %struct.jpeg_common_struct*
  %32 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %32, i32 0, i32 6
  %33 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 %33
  %34 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx, align 4, !tbaa !2
  %35 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %35, i32 0, i32 1
  %36 = load i32, i32* %iMCU_row_num, align 4, !tbaa !29
  %37 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %37, i32 0, i32 3
  %38 = load i32, i32* %v_samp_factor, align 4, !tbaa !26
  %mul = mul i32 %36, %38
  %39 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor2 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %39, i32 0, i32 3
  %40 = load i32, i32* %v_samp_factor2, align 4, !tbaa !26
  %call = call [64 x i16]** %29(%struct.jpeg_common_struct* %31, %struct.jvirt_barray_control* %34, i32 %mul, i32 %40, i32 1)
  store [64 x i16]** %call, [64 x i16]*** %buffer, align 4, !tbaa !2
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %41, i32 0, i32 1
  %42 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem3, align 4, !tbaa !8
  %access_virt_barray4 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %42, i32 0, i32 8
  %43 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray4, align 4, !tbaa !54
  %44 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %45 = bitcast %struct.jpeg_compress_struct* %44 to %struct.jpeg_common_struct*
  %46 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image_uq = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %46, i32 0, i32 7
  %47 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image_uq, i32 0, i32 %47
  %48 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx5, align 4, !tbaa !2
  %49 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num6 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %49, i32 0, i32 1
  %50 = load i32, i32* %iMCU_row_num6, align 4, !tbaa !29
  %51 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor7 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %51, i32 0, i32 3
  %52 = load i32, i32* %v_samp_factor7, align 4, !tbaa !26
  %mul8 = mul i32 %50, %52
  %53 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor9 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %53, i32 0, i32 3
  %54 = load i32, i32* %v_samp_factor9, align 4, !tbaa !26
  %call10 = call [64 x i16]** %43(%struct.jpeg_common_struct* %45, %struct.jvirt_barray_control* %48, i32 %mul8, i32 %54, i32 1)
  store [64 x i16]** %call10, [64 x i16]*** %buffer_dst, align 4, !tbaa !2
  %55 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num11 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %55, i32 0, i32 1
  %56 = load i32, i32* %iMCU_row_num11, align 4, !tbaa !29
  %57 = load i32, i32* %last_iMCU_row, align 4, !tbaa !6
  %cmp12 = icmp ult i32 %56, %57
  br i1 %cmp12, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %58 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor13 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %58, i32 0, i32 3
  %59 = load i32, i32* %v_samp_factor13, align 4, !tbaa !26
  store i32 %59, i32* %block_rows, align 4, !tbaa !6
  br label %if.end18

if.else:                                          ; preds = %for.body
  %60 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %60, i32 0, i32 8
  %61 = load i32, i32* %height_in_blocks, align 4, !tbaa !25
  %62 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor14 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %62, i32 0, i32 3
  %63 = load i32, i32* %v_samp_factor14, align 4, !tbaa !26
  %rem = urem i32 %61, %63
  store i32 %rem, i32* %block_rows, align 4, !tbaa !6
  %64 = load i32, i32* %block_rows, align 4, !tbaa !6
  %cmp15 = icmp eq i32 %64, 0
  br i1 %cmp15, label %if.then16, label %if.end

if.then16:                                        ; preds = %if.else
  %65 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor17 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %65, i32 0, i32 3
  %66 = load i32, i32* %v_samp_factor17, align 4, !tbaa !26
  store i32 %66, i32* %block_rows, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then16, %if.else
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then
  %67 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %67, i32 0, i32 7
  %68 = load i32, i32* %width_in_blocks, align 4, !tbaa !22
  store i32 %68, i32* %blocks_across, align 4, !tbaa !6
  %69 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor19 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %69, i32 0, i32 2
  %70 = load i32, i32* %h_samp_factor19, align 4, !tbaa !24
  store i32 %70, i32* %h_samp_factor, align 4, !tbaa !6
  %71 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %72 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %rem20 = urem i32 %71, %72
  store i32 %rem20, i32* %ndummy, align 4, !tbaa !6
  %73 = load i32, i32* %ndummy, align 4, !tbaa !6
  %cmp21 = icmp sgt i32 %73, 0
  br i1 %cmp21, label %if.then22, label %if.end24

if.then22:                                        ; preds = %if.end18
  %74 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %75 = load i32, i32* %ndummy, align 4, !tbaa !6
  %sub23 = sub nsw i32 %74, %75
  store i32 %sub23, i32* %ndummy, align 4, !tbaa !6
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.end18
  store i32 0, i32* %block_row, align 4, !tbaa !6
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc43, %if.end24
  %76 = load i32, i32* %block_row, align 4, !tbaa !6
  %77 = load i32, i32* %block_rows, align 4, !tbaa !6
  %cmp26 = icmp slt i32 %76, %77
  br i1 %cmp26, label %for.body27, label %for.end45

for.body27:                                       ; preds = %for.cond25
  %78 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %79 = load i32, i32* %block_row, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds [64 x i16]*, [64 x i16]** %78, i32 %79
  %80 = load [64 x i16]*, [64 x i16]** %arrayidx28, align 4, !tbaa !2
  store [64 x i16]* %80, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %81 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %fdct = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %81, i32 0, i32 61
  %82 = load %struct.jpeg_forward_dct*, %struct.jpeg_forward_dct** %fdct, align 8, !tbaa !46
  %forward_DCT = getelementptr inbounds %struct.jpeg_forward_dct, %struct.jpeg_forward_dct* %82, i32 0, i32 1
  %83 = load void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)** %forward_DCT, align 4, !tbaa !47
  %84 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %85 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %86 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %87 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds i8**, i8*** %86, i32 %87
  %88 = load i8**, i8*** %arrayidx29, align 4, !tbaa !2
  %89 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %90 = load i32, i32* %block_row, align 4, !tbaa !6
  %mul30 = mul nsw i32 %90, 8
  %91 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %92 = load [64 x i16]**, [64 x i16]*** %buffer_dst, align 4, !tbaa !2
  %93 = load i32, i32* %block_row, align 4, !tbaa !6
  %arrayidx31 = getelementptr inbounds [64 x i16]*, [64 x i16]** %92, i32 %93
  %94 = load [64 x i16]*, [64 x i16]** %arrayidx31, align 4, !tbaa !2
  call void %83(%struct.jpeg_compress_struct* %84, %struct.jpeg_component_info* %85, i8** %88, [64 x i16]* %89, i32 %mul30, i32 0, i32 %91, [64 x i16]* %94)
  %95 = load i32, i32* %ndummy, align 4, !tbaa !6
  %cmp32 = icmp sgt i32 %95, 0
  br i1 %cmp32, label %if.then33, label %if.end42

if.then33:                                        ; preds = %for.body27
  %96 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %97 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %97, i32 %96
  store [64 x i16]* %add.ptr, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %98 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %99 = bitcast [64 x i16]* %98 to i8*
  %100 = load i32, i32* %ndummy, align 4, !tbaa !6
  %mul34 = mul i32 %100, 128
  call void @jzero_far(i8* %99, i32 %mul34)
  %101 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds [64 x i16], [64 x i16]* %101, i32 -1
  %arrayidx36 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx35, i32 0, i32 0
  %102 = load i16, i16* %arrayidx36, align 2, !tbaa !50
  store i16 %102, i16* %lastDC, align 2, !tbaa !50
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc, %if.then33
  %103 = load i32, i32* %bi, align 4, !tbaa !6
  %104 = load i32, i32* %ndummy, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %103, %104
  br i1 %cmp38, label %for.body39, label %for.end

for.body39:                                       ; preds = %for.cond37
  %105 = load i16, i16* %lastDC, align 2, !tbaa !50
  %106 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %107 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx40 = getelementptr inbounds [64 x i16], [64 x i16]* %106, i32 %107
  %arrayidx41 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx40, i32 0, i32 0
  store i16 %105, i16* %arrayidx41, align 2, !tbaa !50
  br label %for.inc

for.inc:                                          ; preds = %for.body39
  %108 = load i32, i32* %bi, align 4, !tbaa !6
  %inc = add nsw i32 %108, 1
  store i32 %inc, i32* %bi, align 4, !tbaa !6
  br label %for.cond37

for.end:                                          ; preds = %for.cond37
  br label %if.end42

if.end42:                                         ; preds = %for.end, %for.body27
  br label %for.inc43

for.inc43:                                        ; preds = %if.end42
  %109 = load i32, i32* %block_row, align 4, !tbaa !6
  %inc44 = add nsw i32 %109, 1
  store i32 %inc44, i32* %block_row, align 4, !tbaa !6
  br label %for.cond25

for.end45:                                        ; preds = %for.cond25
  %110 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num46 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %110, i32 0, i32 1
  %111 = load i32, i32* %iMCU_row_num46, align 4, !tbaa !29
  %112 = load i32, i32* %last_iMCU_row, align 4, !tbaa !6
  %cmp47 = icmp eq i32 %111, %112
  br i1 %cmp47, label %if.then48, label %if.end79

if.then48:                                        ; preds = %for.end45
  %113 = load i32, i32* %ndummy, align 4, !tbaa !6
  %114 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %add = add i32 %114, %113
  store i32 %add, i32* %blocks_across, align 4, !tbaa !6
  %115 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %116 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %div = udiv i32 %115, %116
  store i32 %div, i32* %MCUs_across, align 4, !tbaa !6
  %117 = load i32, i32* %block_rows, align 4, !tbaa !6
  store i32 %117, i32* %block_row, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc76, %if.then48
  %118 = load i32, i32* %block_row, align 4, !tbaa !6
  %119 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor50 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %119, i32 0, i32 3
  %120 = load i32, i32* %v_samp_factor50, align 4, !tbaa !26
  %cmp51 = icmp slt i32 %118, %120
  br i1 %cmp51, label %for.body52, label %for.end78

for.body52:                                       ; preds = %for.cond49
  %121 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %122 = load i32, i32* %block_row, align 4, !tbaa !6
  %arrayidx53 = getelementptr inbounds [64 x i16]*, [64 x i16]** %121, i32 %122
  %123 = load [64 x i16]*, [64 x i16]** %arrayidx53, align 4, !tbaa !2
  store [64 x i16]* %123, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %124 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %125 = load i32, i32* %block_row, align 4, !tbaa !6
  %sub54 = sub nsw i32 %125, 1
  %arrayidx55 = getelementptr inbounds [64 x i16]*, [64 x i16]** %124, i32 %sub54
  %126 = load [64 x i16]*, [64 x i16]** %arrayidx55, align 4, !tbaa !2
  store [64 x i16]* %126, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  %127 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %128 = bitcast [64 x i16]* %127 to i8*
  %129 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %mul56 = mul i32 %129, 128
  call void @jzero_far(i8* %128, i32 %mul56)
  store i32 0, i32* %MCUindex, align 4, !tbaa !6
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc73, %for.body52
  %130 = load i32, i32* %MCUindex, align 4, !tbaa !6
  %131 = load i32, i32* %MCUs_across, align 4, !tbaa !6
  %cmp58 = icmp ult i32 %130, %131
  br i1 %cmp58, label %for.body59, label %for.end75

for.body59:                                       ; preds = %for.cond57
  %132 = load [64 x i16]*, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  %133 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %sub60 = sub nsw i32 %133, 1
  %arrayidx61 = getelementptr inbounds [64 x i16], [64 x i16]* %132, i32 %sub60
  %arrayidx62 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx61, i32 0, i32 0
  %134 = load i16, i16* %arrayidx62, align 2, !tbaa !50
  store i16 %134, i16* %lastDC, align 2, !tbaa !50
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond63

for.cond63:                                       ; preds = %for.inc68, %for.body59
  %135 = load i32, i32* %bi, align 4, !tbaa !6
  %136 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %cmp64 = icmp slt i32 %135, %136
  br i1 %cmp64, label %for.body65, label %for.end70

for.body65:                                       ; preds = %for.cond63
  %137 = load i16, i16* %lastDC, align 2, !tbaa !50
  %138 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %139 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx66 = getelementptr inbounds [64 x i16], [64 x i16]* %138, i32 %139
  %arrayidx67 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx66, i32 0, i32 0
  store i16 %137, i16* %arrayidx67, align 2, !tbaa !50
  br label %for.inc68

for.inc68:                                        ; preds = %for.body65
  %140 = load i32, i32* %bi, align 4, !tbaa !6
  %inc69 = add nsw i32 %140, 1
  store i32 %inc69, i32* %bi, align 4, !tbaa !6
  br label %for.cond63

for.end70:                                        ; preds = %for.cond63
  %141 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %142 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %add.ptr71 = getelementptr inbounds [64 x i16], [64 x i16]* %142, i32 %141
  store [64 x i16]* %add.ptr71, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %143 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %144 = load [64 x i16]*, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  %add.ptr72 = getelementptr inbounds [64 x i16], [64 x i16]* %144, i32 %143
  store [64 x i16]* %add.ptr72, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  br label %for.inc73

for.inc73:                                        ; preds = %for.end70
  %145 = load i32, i32* %MCUindex, align 4, !tbaa !6
  %inc74 = add i32 %145, 1
  store i32 %inc74, i32* %MCUindex, align 4, !tbaa !6
  br label %for.cond57

for.end75:                                        ; preds = %for.cond57
  br label %for.inc76

for.inc76:                                        ; preds = %for.end75
  %146 = load i32, i32* %block_row, align 4, !tbaa !6
  %inc77 = add nsw i32 %146, 1
  store i32 %inc77, i32* %block_row, align 4, !tbaa !6
  br label %for.cond49

for.end78:                                        ; preds = %for.cond49
  br label %if.end79

if.end79:                                         ; preds = %for.end78, %for.end45
  br label %for.inc80

for.inc80:                                        ; preds = %if.end79
  %147 = load i32, i32* %ci, align 4, !tbaa !6
  %inc81 = add nsw i32 %147, 1
  store i32 %inc81, i32* %ci, align 4, !tbaa !6
  %148 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %148, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end82:                                        ; preds = %for.cond
  %149 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %150 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %call83 = call i32 @compress_output(%struct.jpeg_compress_struct* %149, i8*** %150)
  %151 = bitcast [64 x i16]*** %buffer_dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #3
  %152 = bitcast [64 x i16]** %lastblockrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #3
  %153 = bitcast [64 x i16]** %thisblockrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #3
  %154 = bitcast [64 x i16]*** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #3
  %155 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #3
  %156 = bitcast i16* %lastDC to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %156) #3
  %157 = bitcast i32* %ndummy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #3
  %158 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #3
  %159 = bitcast i32* %block_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #3
  %160 = bitcast i32* %h_samp_factor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #3
  %161 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #3
  %162 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #3
  %163 = bitcast i32* %MCUindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #3
  %164 = bitcast i32* %MCUs_across to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #3
  %165 = bitcast i32* %blocks_across to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #3
  %166 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #3
  %167 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #3
  ret i32 %call83
}

; Function Attrs: nounwind
define internal i32 @compress_output(%struct.jpeg_compress_struct* %cinfo, i8*** %input_buf) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %MCU_col_num = alloca i32, align 4
  %blkn = alloca i32, align 4
  %ci = alloca i32, align 4
  %xindex = alloca i32, align 4
  %yindex = alloca i32, align 4
  %yoffset = alloca i32, align 4
  %start_col = alloca i32, align 4
  %buffer = alloca [4 x [64 x i16]**], align 16
  %buffer_ptr = alloca [64 x i16]*, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %xindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %start_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast [4 x [64 x i16]**]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #3
  %12 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %ci, align 4, !tbaa !6
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 44
  %16 = load i32, i32* %comps_in_scan, align 4, !tbaa !35
  %cmp = icmp slt i32 %14, %16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 45
  %18 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %18
  %19 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %19, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 1
  %21 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %access_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %21, i32 0, i32 8
  %22 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray, align 4, !tbaa !54
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_compress_struct* %23 to %struct.jpeg_common_struct*
  %25 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %25, i32 0, i32 6
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %26, i32 0, i32 1
  %27 = load i32, i32* %component_index, align 4, !tbaa !49
  %arrayidx2 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 %27
  %28 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx2, align 4, !tbaa !2
  %29 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %29, i32 0, i32 1
  %30 = load i32, i32* %iMCU_row_num, align 4, !tbaa !29
  %31 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %31, i32 0, i32 3
  %32 = load i32, i32* %v_samp_factor, align 4, !tbaa !26
  %mul = mul i32 %30, %32
  %33 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor3 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %33, i32 0, i32 3
  %34 = load i32, i32* %v_samp_factor3, align 4, !tbaa !26
  %call = call [64 x i16]** %22(%struct.jpeg_common_struct* %24, %struct.jvirt_barray_control* %28, i32 %mul, i32 %34, i32 0)
  %35 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [4 x [64 x i16]**], [4 x [64 x i16]**]* %buffer, i32 0, i32 %35
  store [64 x i16]** %call, [64 x i16]*** %arrayidx4, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %36 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %37, i32 0, i32 3
  %38 = load i32, i32* %MCU_vert_offset, align 4, !tbaa !40
  store i32 %38, i32* %yoffset, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc46, %for.end
  %39 = load i32, i32* %yoffset, align 4, !tbaa !6
  %40 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %40, i32 0, i32 4
  %41 = load i32, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !36
  %cmp6 = icmp slt i32 %39, %41
  br i1 %cmp6, label %for.body7, label %for.end48

for.body7:                                        ; preds = %for.cond5
  %42 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %42, i32 0, i32 2
  %43 = load i32, i32* %mcu_ctr, align 4, !tbaa !39
  store i32 %43, i32* %MCU_col_num, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc42, %for.body7
  %44 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %45 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %45, i32 0, i32 46
  %46 = load i32, i32* %MCUs_per_row, align 8, !tbaa !41
  %cmp9 = icmp ult i32 %44, %46
  br i1 %cmp9, label %for.body10, label %for.end44

for.body10:                                       ; preds = %for.cond8
  store i32 0, i32* %blkn, align 4, !tbaa !6
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc35, %for.body10
  %47 = load i32, i32* %ci, align 4, !tbaa !6
  %48 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %48, i32 0, i32 44
  %49 = load i32, i32* %comps_in_scan12, align 4, !tbaa !35
  %cmp13 = icmp slt i32 %47, %49
  br i1 %cmp13, label %for.body14, label %for.end37

for.body14:                                       ; preds = %for.cond11
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info15 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %50, i32 0, i32 45
  %51 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info15, i32 0, i32 %51
  %52 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx16, align 4, !tbaa !2
  store %struct.jpeg_component_info* %52, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %53 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %54 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %54, i32 0, i32 13
  %55 = load i32, i32* %MCU_width, align 4, !tbaa !42
  %mul17 = mul i32 %53, %55
  store i32 %mul17, i32* %start_col, align 4, !tbaa !6
  store i32 0, i32* %yindex, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc32, %for.body14
  %56 = load i32, i32* %yindex, align 4, !tbaa !6
  %57 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %57, i32 0, i32 14
  %58 = load i32, i32* %MCU_height, align 4, !tbaa !45
  %cmp19 = icmp slt i32 %56, %58
  br i1 %cmp19, label %for.body20, label %for.end34

for.body20:                                       ; preds = %for.cond18
  %59 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [4 x [64 x i16]**], [4 x [64 x i16]**]* %buffer, i32 0, i32 %59
  %60 = load [64 x i16]**, [64 x i16]*** %arrayidx21, align 4, !tbaa !2
  %61 = load i32, i32* %yindex, align 4, !tbaa !6
  %62 = load i32, i32* %yoffset, align 4, !tbaa !6
  %add = add nsw i32 %61, %62
  %arrayidx22 = getelementptr inbounds [64 x i16]*, [64 x i16]** %60, i32 %add
  %63 = load [64 x i16]*, [64 x i16]** %arrayidx22, align 4, !tbaa !2
  %64 = load i32, i32* %start_col, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %63, i32 %64
  store [64 x i16]* %add.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  store i32 0, i32* %xindex, align 4, !tbaa !6
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc29, %for.body20
  %65 = load i32, i32* %xindex, align 4, !tbaa !6
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width24 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 0, i32 13
  %67 = load i32, i32* %MCU_width24, align 4, !tbaa !42
  %cmp25 = icmp slt i32 %65, %67
  br i1 %cmp25, label %for.body26, label %for.end31

for.body26:                                       ; preds = %for.cond23
  %68 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %68, i32 1
  store [64 x i16]* %incdec.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %69 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %69, i32 0, i32 5
  %70 = load i32, i32* %blkn, align 4, !tbaa !6
  %inc27 = add nsw i32 %70, 1
  store i32 %inc27, i32* %blkn, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %70
  store [64 x i16]* %68, [64 x i16]** %arrayidx28, align 4, !tbaa !2
  br label %for.inc29

for.inc29:                                        ; preds = %for.body26
  %71 = load i32, i32* %xindex, align 4, !tbaa !6
  %inc30 = add nsw i32 %71, 1
  store i32 %inc30, i32* %xindex, align 4, !tbaa !6
  br label %for.cond23

for.end31:                                        ; preds = %for.cond23
  br label %for.inc32

for.inc32:                                        ; preds = %for.end31
  %72 = load i32, i32* %yindex, align 4, !tbaa !6
  %inc33 = add nsw i32 %72, 1
  store i32 %inc33, i32* %yindex, align 4, !tbaa !6
  br label %for.cond18

for.end34:                                        ; preds = %for.cond18
  br label %for.inc35

for.inc35:                                        ; preds = %for.end34
  %73 = load i32, i32* %ci, align 4, !tbaa !6
  %inc36 = add nsw i32 %73, 1
  store i32 %inc36, i32* %ci, align 4, !tbaa !6
  br label %for.cond11

for.end37:                                        ; preds = %for.cond11
  %74 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %74, i32 0, i32 62
  %75 = load %struct.jpeg_entropy_encoder*, %struct.jpeg_entropy_encoder** %entropy, align 4, !tbaa !51
  %encode_mcu = getelementptr inbounds %struct.jpeg_entropy_encoder, %struct.jpeg_entropy_encoder* %75, i32 0, i32 1
  %76 = load i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)** %encode_mcu, align 4, !tbaa !52
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %78 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer38 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %78, i32 0, i32 5
  %arraydecay = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer38, i32 0, i32 0
  %call39 = call i32 %76(%struct.jpeg_compress_struct* %77, [64 x i16]** %arraydecay)
  %tobool = icmp ne i32 %call39, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.end37
  %79 = load i32, i32* %yoffset, align 4, !tbaa !6
  %80 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset40 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %80, i32 0, i32 3
  store i32 %79, i32* %MCU_vert_offset40, align 4, !tbaa !40
  %81 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %82 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr41 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %82, i32 0, i32 2
  store i32 %81, i32* %mcu_ctr41, align 4, !tbaa !39
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.end37
  br label %for.inc42

for.inc42:                                        ; preds = %if.end
  %83 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %inc43 = add i32 %83, 1
  store i32 %inc43, i32* %MCU_col_num, align 4, !tbaa !6
  br label %for.cond8

for.end44:                                        ; preds = %for.cond8
  %84 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %mcu_ctr45 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %84, i32 0, i32 2
  store i32 0, i32* %mcu_ctr45, align 4, !tbaa !39
  br label %for.inc46

for.inc46:                                        ; preds = %for.end44
  %85 = load i32, i32* %yoffset, align 4, !tbaa !6
  %inc47 = add nsw i32 %85, 1
  store i32 %inc47, i32* %yoffset, align 4, !tbaa !6
  br label %for.cond5

for.end48:                                        ; preds = %for.cond5
  %86 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num49 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %86, i32 0, i32 1
  %87 = load i32, i32* %iMCU_row_num49, align 4, !tbaa !29
  %inc50 = add i32 %87, 1
  store i32 %inc50, i32* %iMCU_row_num49, align 4, !tbaa !29
  %88 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_compress_struct* %88)
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end48, %if.then
  %89 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #3
  %90 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #3
  %91 = bitcast [4 x [64 x i16]**]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %91) #3
  %92 = bitcast i32* %start_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #3
  %93 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #3
  %94 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #3
  %95 = bitcast i32* %xindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  %96 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #3
  %97 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #3
  %98 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #3
  %99 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #3
  %100 = load i32, i32* %retval, align 4
  ret i32 %100
}

; Function Attrs: nounwind
define internal i32 @compress_trellis_pass(%struct.jpeg_compress_struct* %cinfo, i8*** %input_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %last_iMCU_row = alloca i32, align 4
  %blocks_across = alloca i32, align 4
  %MCUs_across = alloca i32, align 4
  %MCUindex = alloca i32, align 4
  %bi = alloca i32, align 4
  %ci = alloca i32, align 4
  %h_samp_factor = alloca i32, align 4
  %block_row = alloca i32, align 4
  %block_rows = alloca i32, align 4
  %ndummy = alloca i32, align 4
  %lastDC = alloca i16, align 2
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %buffer = alloca [64 x i16]**, align 4
  %thisblockrow = alloca [64 x i16]*, align 4
  %lastblockrow = alloca [64 x i16]*, align 4
  %buffer_dst = alloca [64 x i16]**, align 4
  %dctbl_data = alloca %struct.c_derived_tbl, align 4
  %dctbl = alloca %struct.c_derived_tbl*, align 4
  %actbl_data = alloca %struct.c_derived_tbl, align 4
  %actbl = alloca %struct.c_derived_tbl*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 57
  %2 = load %struct.jpeg_c_coef_controller*, %struct.jpeg_c_coef_controller** %coef1, align 8, !tbaa !15
  %3 = bitcast %struct.jpeg_c_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 43
  %6 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !37
  %sub = sub i32 %6, 1
  store i32 %sub, i32* %last_iMCU_row, align 4, !tbaa !6
  %7 = bitcast i32* %blocks_across to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %MCUs_across to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %MCUindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %h_samp_factor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %block_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %ndummy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i16* %lastDC to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %16) #3
  %17 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast [64 x i16]*** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast [64 x i16]** %thisblockrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast [64 x i16]** %lastblockrow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast [64 x i16]*** %buffer_dst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc92, %entry
  %22 = load i32, i32* %ci, align 4, !tbaa !6
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 44
  %24 = load i32, i32* %comps_in_scan, align 4, !tbaa !35
  %cmp = icmp slt i32 %22, %24
  br i1 %cmp, label %for.body, label %for.end94

for.body:                                         ; preds = %for.cond
  %25 = bitcast %struct.c_derived_tbl* %dctbl_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 1280, i8* %25) #3
  %26 = bitcast %struct.c_derived_tbl** %dctbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  store %struct.c_derived_tbl* %dctbl_data, %struct.c_derived_tbl** %dctbl, align 4, !tbaa !2
  %27 = bitcast %struct.c_derived_tbl* %actbl_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 1280, i8* %27) #3
  %28 = bitcast %struct.c_derived_tbl** %actbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  store %struct.c_derived_tbl* %actbl_data, %struct.c_derived_tbl** %actbl, align 4, !tbaa !2
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %29, i32 0, i32 45
  %30 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %30
  %31 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %31, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %33 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dc_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %33, i32 0, i32 5
  %34 = load i32, i32* %dc_tbl_no, align 4, !tbaa !55
  call void @jpeg_make_c_derived_tbl(%struct.jpeg_compress_struct* %32, i32 1, i32 %34, %struct.c_derived_tbl** %dctbl)
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %36 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %ac_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %36, i32 0, i32 6
  %37 = load i32, i32* %ac_tbl_no, align 4, !tbaa !56
  call void @jpeg_make_c_derived_tbl(%struct.jpeg_compress_struct* %35, i32 0, i32 %37, %struct.c_derived_tbl** %actbl)
  %38 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %38, i32 0, i32 1
  %39 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %access_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %39, i32 0, i32 8
  %40 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray, align 4, !tbaa !54
  %41 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %42 = bitcast %struct.jpeg_compress_struct* %41 to %struct.jpeg_common_struct*
  %43 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %43, i32 0, i32 6
  %44 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %44, i32 0, i32 1
  %45 = load i32, i32* %component_index, align 4, !tbaa !49
  %arrayidx2 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 %45
  %46 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx2, align 4, !tbaa !2
  %47 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %47, i32 0, i32 1
  %48 = load i32, i32* %iMCU_row_num, align 4, !tbaa !29
  %49 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %49, i32 0, i32 3
  %50 = load i32, i32* %v_samp_factor, align 4, !tbaa !26
  %mul = mul i32 %48, %50
  %51 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor3 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %51, i32 0, i32 3
  %52 = load i32, i32* %v_samp_factor3, align 4, !tbaa !26
  %call = call [64 x i16]** %40(%struct.jpeg_common_struct* %42, %struct.jvirt_barray_control* %46, i32 %mul, i32 %52, i32 1)
  store [64 x i16]** %call, [64 x i16]*** %buffer, align 4, !tbaa !2
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %53, i32 0, i32 1
  %54 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem4, align 4, !tbaa !8
  %access_virt_barray5 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %54, i32 0, i32 8
  %55 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray5, align 4, !tbaa !54
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %57 = bitcast %struct.jpeg_compress_struct* %56 to %struct.jpeg_common_struct*
  %58 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image_uq = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %58, i32 0, i32 7
  %59 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index6 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %59, i32 0, i32 1
  %60 = load i32, i32* %component_index6, align 4, !tbaa !49
  %arrayidx7 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image_uq, i32 0, i32 %60
  %61 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx7, align 4, !tbaa !2
  %62 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num8 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %62, i32 0, i32 1
  %63 = load i32, i32* %iMCU_row_num8, align 4, !tbaa !29
  %64 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor9 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %64, i32 0, i32 3
  %65 = load i32, i32* %v_samp_factor9, align 4, !tbaa !26
  %mul10 = mul i32 %63, %65
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor11 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 0, i32 3
  %67 = load i32, i32* %v_samp_factor11, align 4, !tbaa !26
  %call12 = call [64 x i16]** %55(%struct.jpeg_common_struct* %57, %struct.jvirt_barray_control* %61, i32 %mul10, i32 %67, i32 1)
  store [64 x i16]** %call12, [64 x i16]*** %buffer_dst, align 4, !tbaa !2
  %68 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num13 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %68, i32 0, i32 1
  %69 = load i32, i32* %iMCU_row_num13, align 4, !tbaa !29
  %70 = load i32, i32* %last_iMCU_row, align 4, !tbaa !6
  %cmp14 = icmp ult i32 %69, %70
  br i1 %cmp14, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %71 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor15 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %71, i32 0, i32 3
  %72 = load i32, i32* %v_samp_factor15, align 4, !tbaa !26
  store i32 %72, i32* %block_rows, align 4, !tbaa !6
  br label %if.end20

if.else:                                          ; preds = %for.body
  %73 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %73, i32 0, i32 8
  %74 = load i32, i32* %height_in_blocks, align 4, !tbaa !25
  %75 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor16 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %75, i32 0, i32 3
  %76 = load i32, i32* %v_samp_factor16, align 4, !tbaa !26
  %rem = urem i32 %74, %76
  store i32 %rem, i32* %block_rows, align 4, !tbaa !6
  %77 = load i32, i32* %block_rows, align 4, !tbaa !6
  %cmp17 = icmp eq i32 %77, 0
  br i1 %cmp17, label %if.then18, label %if.end

if.then18:                                        ; preds = %if.else
  %78 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor19 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %78, i32 0, i32 3
  %79 = load i32, i32* %v_samp_factor19, align 4, !tbaa !26
  store i32 %79, i32* %block_rows, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then18, %if.else
  br label %if.end20

if.end20:                                         ; preds = %if.end, %if.then
  %80 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %80, i32 0, i32 7
  %81 = load i32, i32* %width_in_blocks, align 4, !tbaa !22
  store i32 %81, i32* %blocks_across, align 4, !tbaa !6
  %82 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor21 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %82, i32 0, i32 2
  %83 = load i32, i32* %h_samp_factor21, align 4, !tbaa !24
  store i32 %83, i32* %h_samp_factor, align 4, !tbaa !6
  %84 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %85 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %rem22 = urem i32 %84, %85
  store i32 %rem22, i32* %ndummy, align 4, !tbaa !6
  %86 = load i32, i32* %ndummy, align 4, !tbaa !6
  %cmp23 = icmp sgt i32 %86, 0
  br i1 %cmp23, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.end20
  %87 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %88 = load i32, i32* %ndummy, align 4, !tbaa !6
  %sub25 = sub nsw i32 %87, %88
  store i32 %sub25, i32* %ndummy, align 4, !tbaa !6
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %if.end20
  store i16 0, i16* %lastDC, align 2, !tbaa !50
  store i32 0, i32* %block_row, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc55, %if.end26
  %89 = load i32, i32* %block_row, align 4, !tbaa !6
  %90 = load i32, i32* %block_rows, align 4, !tbaa !6
  %cmp28 = icmp slt i32 %89, %90
  br i1 %cmp28, label %for.body29, label %for.end57

for.body29:                                       ; preds = %for.cond27
  %91 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %92 = load i32, i32* %block_row, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds [64 x i16]*, [64 x i16]** %91, i32 %92
  %93 = load [64 x i16]*, [64 x i16]** %arrayidx30, align 4, !tbaa !2
  store [64 x i16]* %93, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %94 = load i32, i32* %block_row, align 4, !tbaa !6
  %cmp31 = icmp sgt i32 %94, 0
  br i1 %cmp31, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body29
  %95 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %96 = load i32, i32* %block_row, align 4, !tbaa !6
  %sub32 = sub nsw i32 %96, 1
  %arrayidx33 = getelementptr inbounds [64 x i16]*, [64 x i16]** %95, i32 %sub32
  %97 = load [64 x i16]*, [64 x i16]** %arrayidx33, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %for.body29
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi [64 x i16]* [ %97, %cond.true ], [ null, %cond.false ]
  store [64 x i16]* %cond, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  %98 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %99 = load %struct.c_derived_tbl*, %struct.c_derived_tbl** %dctbl, align 4, !tbaa !2
  %100 = load %struct.c_derived_tbl*, %struct.c_derived_tbl** %actbl, align 4, !tbaa !2
  %101 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %102 = load [64 x i16]**, [64 x i16]*** %buffer_dst, align 4, !tbaa !2
  %103 = load i32, i32* %block_row, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds [64 x i16]*, [64 x i16]** %102, i32 %103
  %104 = load [64 x i16]*, [64 x i16]** %arrayidx34, align 4, !tbaa !2
  %105 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %106 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %quant_tbl_ptrs = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %106, i32 0, i32 16
  %107 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %107, i32 0, i32 4
  %108 = load i32, i32* %quant_tbl_no, align 4, !tbaa !57
  %arrayidx35 = getelementptr inbounds [4 x %struct.JQUANT_TBL*], [4 x %struct.JQUANT_TBL*]* %quant_tbl_ptrs, i32 0, i32 %108
  %109 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %arrayidx35, align 4, !tbaa !2
  %110 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %110, i32 0, i32 54
  %111 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master, align 4, !tbaa !58
  %norm_src = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %111, i32 0, i32 14
  %112 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no36 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %112, i32 0, i32 4
  %113 = load i32, i32* %quant_tbl_no36, align 4, !tbaa !57
  %arrayidx37 = getelementptr inbounds [4 x [64 x double]], [4 x [64 x double]]* %norm_src, i32 0, i32 %113
  %arraydecay = getelementptr inbounds [64 x double], [64 x double]* %arrayidx37, i32 0, i32 0
  %114 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %master38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %114, i32 0, i32 54
  %115 = load %struct.jpeg_comp_master*, %struct.jpeg_comp_master** %master38, align 4, !tbaa !58
  %norm_coef = getelementptr inbounds %struct.jpeg_comp_master, %struct.jpeg_comp_master* %115, i32 0, i32 15
  %116 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_tbl_no39 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %116, i32 0, i32 4
  %117 = load i32, i32* %quant_tbl_no39, align 4, !tbaa !57
  %arrayidx40 = getelementptr inbounds [4 x [64 x double]], [4 x [64 x double]]* %norm_coef, i32 0, i32 %117
  %arraydecay41 = getelementptr inbounds [64 x double], [64 x double]* %arrayidx40, i32 0, i32 0
  %118 = load [64 x i16]*, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  %119 = load [64 x i16]**, [64 x i16]*** %buffer_dst, align 4, !tbaa !2
  %120 = load i32, i32* %block_row, align 4, !tbaa !6
  %sub42 = sub nsw i32 %120, 1
  %arrayidx43 = getelementptr inbounds [64 x i16]*, [64 x i16]** %119, i32 %sub42
  %121 = load [64 x i16]*, [64 x i16]** %arrayidx43, align 4, !tbaa !2
  call void @quantize_trellis(%struct.jpeg_compress_struct* %98, %struct.c_derived_tbl* %99, %struct.c_derived_tbl* %100, [64 x i16]* %101, [64 x i16]* %104, i32 %105, %struct.JQUANT_TBL* %109, double* %arraydecay, double* %arraydecay41, i16* %lastDC, [64 x i16]* %118, [64 x i16]* %121)
  %122 = load i32, i32* %ndummy, align 4, !tbaa !6
  %cmp44 = icmp sgt i32 %122, 0
  br i1 %cmp44, label %if.then45, label %if.end54

if.then45:                                        ; preds = %cond.end
  %123 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %124 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %124, i32 %123
  store [64 x i16]* %add.ptr, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %125 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %126 = bitcast [64 x i16]* %125 to i8*
  %127 = load i32, i32* %ndummy, align 4, !tbaa !6
  %mul46 = mul i32 %127, 128
  call void @jzero_far(i8* %126, i32 %mul46)
  %128 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds [64 x i16], [64 x i16]* %128, i32 -1
  %arrayidx48 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx47, i32 0, i32 0
  %129 = load i16, i16* %arrayidx48, align 2, !tbaa !50
  store i16 %129, i16* %lastDC, align 2, !tbaa !50
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc, %if.then45
  %130 = load i32, i32* %bi, align 4, !tbaa !6
  %131 = load i32, i32* %ndummy, align 4, !tbaa !6
  %cmp50 = icmp slt i32 %130, %131
  br i1 %cmp50, label %for.body51, label %for.end

for.body51:                                       ; preds = %for.cond49
  %132 = load i16, i16* %lastDC, align 2, !tbaa !50
  %133 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %134 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx52 = getelementptr inbounds [64 x i16], [64 x i16]* %133, i32 %134
  %arrayidx53 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx52, i32 0, i32 0
  store i16 %132, i16* %arrayidx53, align 2, !tbaa !50
  br label %for.inc

for.inc:                                          ; preds = %for.body51
  %135 = load i32, i32* %bi, align 4, !tbaa !6
  %inc = add nsw i32 %135, 1
  store i32 %inc, i32* %bi, align 4, !tbaa !6
  br label %for.cond49

for.end:                                          ; preds = %for.cond49
  br label %if.end54

if.end54:                                         ; preds = %for.end, %cond.end
  br label %for.inc55

for.inc55:                                        ; preds = %if.end54
  %136 = load i32, i32* %block_row, align 4, !tbaa !6
  %inc56 = add nsw i32 %136, 1
  store i32 %inc56, i32* %block_row, align 4, !tbaa !6
  br label %for.cond27

for.end57:                                        ; preds = %for.cond27
  %137 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %iMCU_row_num58 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %137, i32 0, i32 1
  %138 = load i32, i32* %iMCU_row_num58, align 4, !tbaa !29
  %139 = load i32, i32* %last_iMCU_row, align 4, !tbaa !6
  %cmp59 = icmp eq i32 %138, %139
  br i1 %cmp59, label %if.then60, label %if.end91

if.then60:                                        ; preds = %for.end57
  %140 = load i32, i32* %ndummy, align 4, !tbaa !6
  %141 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %add = add i32 %141, %140
  store i32 %add, i32* %blocks_across, align 4, !tbaa !6
  %142 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %143 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %div = udiv i32 %142, %143
  store i32 %div, i32* %MCUs_across, align 4, !tbaa !6
  %144 = load i32, i32* %block_rows, align 4, !tbaa !6
  store i32 %144, i32* %block_row, align 4, !tbaa !6
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc88, %if.then60
  %145 = load i32, i32* %block_row, align 4, !tbaa !6
  %146 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor62 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %146, i32 0, i32 3
  %147 = load i32, i32* %v_samp_factor62, align 4, !tbaa !26
  %cmp63 = icmp slt i32 %145, %147
  br i1 %cmp63, label %for.body64, label %for.end90

for.body64:                                       ; preds = %for.cond61
  %148 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %149 = load i32, i32* %block_row, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds [64 x i16]*, [64 x i16]** %148, i32 %149
  %150 = load [64 x i16]*, [64 x i16]** %arrayidx65, align 4, !tbaa !2
  store [64 x i16]* %150, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %151 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %152 = load i32, i32* %block_row, align 4, !tbaa !6
  %sub66 = sub nsw i32 %152, 1
  %arrayidx67 = getelementptr inbounds [64 x i16]*, [64 x i16]** %151, i32 %sub66
  %153 = load [64 x i16]*, [64 x i16]** %arrayidx67, align 4, !tbaa !2
  store [64 x i16]* %153, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  %154 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %155 = bitcast [64 x i16]* %154 to i8*
  %156 = load i32, i32* %blocks_across, align 4, !tbaa !6
  %mul68 = mul i32 %156, 128
  call void @jzero_far(i8* %155, i32 %mul68)
  store i32 0, i32* %MCUindex, align 4, !tbaa !6
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc85, %for.body64
  %157 = load i32, i32* %MCUindex, align 4, !tbaa !6
  %158 = load i32, i32* %MCUs_across, align 4, !tbaa !6
  %cmp70 = icmp ult i32 %157, %158
  br i1 %cmp70, label %for.body71, label %for.end87

for.body71:                                       ; preds = %for.cond69
  %159 = load [64 x i16]*, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  %160 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %sub72 = sub nsw i32 %160, 1
  %arrayidx73 = getelementptr inbounds [64 x i16], [64 x i16]* %159, i32 %sub72
  %arrayidx74 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx73, i32 0, i32 0
  %161 = load i16, i16* %arrayidx74, align 2, !tbaa !50
  store i16 %161, i16* %lastDC, align 2, !tbaa !50
  store i32 0, i32* %bi, align 4, !tbaa !6
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc80, %for.body71
  %162 = load i32, i32* %bi, align 4, !tbaa !6
  %163 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %cmp76 = icmp slt i32 %162, %163
  br i1 %cmp76, label %for.body77, label %for.end82

for.body77:                                       ; preds = %for.cond75
  %164 = load i16, i16* %lastDC, align 2, !tbaa !50
  %165 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %166 = load i32, i32* %bi, align 4, !tbaa !6
  %arrayidx78 = getelementptr inbounds [64 x i16], [64 x i16]* %165, i32 %166
  %arrayidx79 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx78, i32 0, i32 0
  store i16 %164, i16* %arrayidx79, align 2, !tbaa !50
  br label %for.inc80

for.inc80:                                        ; preds = %for.body77
  %167 = load i32, i32* %bi, align 4, !tbaa !6
  %inc81 = add nsw i32 %167, 1
  store i32 %inc81, i32* %bi, align 4, !tbaa !6
  br label %for.cond75

for.end82:                                        ; preds = %for.cond75
  %168 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %169 = load [64 x i16]*, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %add.ptr83 = getelementptr inbounds [64 x i16], [64 x i16]* %169, i32 %168
  store [64 x i16]* %add.ptr83, [64 x i16]** %thisblockrow, align 4, !tbaa !2
  %170 = load i32, i32* %h_samp_factor, align 4, !tbaa !6
  %171 = load [64 x i16]*, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  %add.ptr84 = getelementptr inbounds [64 x i16], [64 x i16]* %171, i32 %170
  store [64 x i16]* %add.ptr84, [64 x i16]** %lastblockrow, align 4, !tbaa !2
  br label %for.inc85

for.inc85:                                        ; preds = %for.end82
  %172 = load i32, i32* %MCUindex, align 4, !tbaa !6
  %inc86 = add i32 %172, 1
  store i32 %inc86, i32* %MCUindex, align 4, !tbaa !6
  br label %for.cond69

for.end87:                                        ; preds = %for.cond69
  br label %for.inc88

for.inc88:                                        ; preds = %for.end87
  %173 = load i32, i32* %block_row, align 4, !tbaa !6
  %inc89 = add nsw i32 %173, 1
  store i32 %inc89, i32* %block_row, align 4, !tbaa !6
  br label %for.cond61

for.end90:                                        ; preds = %for.cond61
  br label %if.end91

if.end91:                                         ; preds = %for.end90, %for.end57
  %174 = bitcast %struct.c_derived_tbl** %actbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #3
  %175 = bitcast %struct.c_derived_tbl* %actbl_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 1280, i8* %175) #3
  %176 = bitcast %struct.c_derived_tbl** %dctbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #3
  %177 = bitcast %struct.c_derived_tbl* %dctbl_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 1280, i8* %177) #3
  br label %for.inc92

for.inc92:                                        ; preds = %if.end91
  %178 = load i32, i32* %ci, align 4, !tbaa !6
  %inc93 = add nsw i32 %178, 1
  store i32 %inc93, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end94:                                        ; preds = %for.cond
  %179 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %180 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %call95 = call i32 @compress_output(%struct.jpeg_compress_struct* %179, i8*** %180)
  %181 = bitcast [64 x i16]*** %buffer_dst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #3
  %182 = bitcast [64 x i16]** %lastblockrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #3
  %183 = bitcast [64 x i16]** %thisblockrow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #3
  %184 = bitcast [64 x i16]*** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #3
  %185 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #3
  %186 = bitcast i16* %lastDC to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %186) #3
  %187 = bitcast i32* %ndummy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #3
  %188 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #3
  %189 = bitcast i32* %block_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #3
  %190 = bitcast i32* %h_samp_factor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #3
  %191 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #3
  %192 = bitcast i32* %bi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #3
  %193 = bitcast i32* %MCUindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #3
  %194 = bitcast i32* %MCUs_across to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #3
  %195 = bitcast i32* %blocks_across to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #3
  %196 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #3
  %197 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #3
  ret i32 %call95
}

declare void @jzero_far(i8*, i32) #2

declare void @jpeg_make_c_derived_tbl(%struct.jpeg_compress_struct*, i32, i32, %struct.c_derived_tbl**) #2

declare void @quantize_trellis(%struct.jpeg_compress_struct*, %struct.c_derived_tbl*, %struct.c_derived_tbl*, [64 x i16]*, [64 x i16]*, i32, %struct.JQUANT_TBL*, double*, double*, i16*, [64 x i16]*, [64 x i16]*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !10, i64 48, !7, i64 56, !7, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !7, i64 168, !3, i64 172, !7, i64 176, !7, i64 180, !7, i64 184, !7, i64 188, !7, i64 192, !4, i64 196, !7, i64 200, !7, i64 204, !7, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !11, i64 216, !11, i64 218, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !7, i64 236, !7, i64 240, !7, i64 244, !4, i64 248, !7, i64 264, !7, i64 268, !7, i64 272, !4, i64 276, !7, i64 316, !7, i64 320, !7, i64 324, !7, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !7, i64 372}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 0}
!13 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!14 = !{!"long", !4, i64 0}
!15 = !{!9, !3, i64 344}
!16 = !{!17, !3, i64 0}
!17 = !{!"", !18, i64 0, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !4, i64 24, !4, i64 64, !4, i64 104}
!18 = !{!"jpeg_c_coef_controller", !3, i64 0, !3, i64 4}
!19 = !{!9, !3, i64 68}
!20 = !{!9, !7, i64 60}
!21 = !{!13, !3, i64 20}
!22 = !{!23, !7, i64 28}
!23 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!24 = !{!23, !7, i64 8}
!25 = !{!23, !7, i64 32}
!26 = !{!23, !7, i64 12}
!27 = !{!13, !3, i64 4}
!28 = !{!4, !4, i64 0}
!29 = !{!17, !7, i64 8}
!30 = !{!9, !3, i64 0}
!31 = !{!32, !7, i64 20}
!32 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !14, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!33 = !{!32, !3, i64 0}
!34 = !{!17, !3, i64 4}
!35 = !{!9, !7, i64 244}
!36 = !{!17, !7, i64 20}
!37 = !{!9, !7, i64 240}
!38 = !{!23, !7, i64 72}
!39 = !{!17, !7, i64 12}
!40 = !{!17, !7, i64 16}
!41 = !{!9, !7, i64 264}
!42 = !{!23, !7, i64 52}
!43 = !{!23, !7, i64 68}
!44 = !{!23, !7, i64 64}
!45 = !{!23, !7, i64 56}
!46 = !{!9, !3, i64 360}
!47 = !{!48, !3, i64 4}
!48 = !{!"jpeg_forward_dct", !3, i64 0, !3, i64 4}
!49 = !{!23, !7, i64 4}
!50 = !{!11, !11, i64 0}
!51 = !{!9, !3, i64 364}
!52 = !{!53, !3, i64 4}
!53 = !{!"jpeg_entropy_encoder", !3, i64 0, !3, i64 4, !3, i64 8}
!54 = !{!13, !3, i64 32}
!55 = !{!23, !7, i64 20}
!56 = !{!23, !7, i64 24}
!57 = !{!23, !7, i64 16}
!58 = !{!9, !3, i64 332}
