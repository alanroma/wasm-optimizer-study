; ModuleID = 'jdatadst.c'
source_filename = "jdatadst.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type opaque
%struct.jpeg_c_main_controller = type opaque
%struct.jpeg_c_prep_controller = type opaque
%struct.jpeg_c_coef_controller = type opaque
%struct.jpeg_marker_writer = type opaque
%struct.jpeg_color_converter = type opaque
%struct.jpeg_downsampler = type opaque
%struct.jpeg_forward_dct = type opaque
%struct.jpeg_entropy_encoder = type opaque
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct._IO_FILE = type opaque
%struct.my_destination_mgr = type { %struct.jpeg_destination_mgr, %struct._IO_FILE*, i8* }
%struct.my_mem_destination_mgr = type { %struct.jpeg_destination_mgr, i8**, i32*, i8*, i8*, i32 }

; Function Attrs: nounwind
define hidden void @jpeg_stdio_dest(%struct.jpeg_compress_struct* %cinfo, %struct._IO_FILE* %outfile) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %outfile.addr = alloca %struct._IO_FILE*, align 4
  %dest = alloca %struct.my_destination_mgr*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct._IO_FILE* %outfile, %struct._IO_FILE** %outfile.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest1, align 8, !tbaa !6
  %cmp = icmp eq %struct.jpeg_destination_mgr* %2, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 1
  %4 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !11
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %4, i32 0, i32 0
  %5 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_compress_struct* %6 to %struct.jpeg_common_struct*
  %call = call i8* %5(%struct.jpeg_common_struct* %7, i32 0, i32 28)
  %8 = bitcast i8* %call to %struct.jpeg_destination_mgr*
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %9, i32 0, i32 6
  store %struct.jpeg_destination_mgr* %8, %struct.jpeg_destination_mgr** %dest2, align 8, !tbaa !6
  br label %if.end7

if.else:                                          ; preds = %entry
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 6
  %11 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest3, align 8, !tbaa !6
  %init_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %11, i32 0, i32 2
  %12 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %init_destination, align 4, !tbaa !15
  %cmp4 = icmp ne void (%struct.jpeg_compress_struct*)* %12, @init_destination
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %13, i32 0, i32 0
  %14 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !17
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %14, i32 0, i32 5
  store i32 23, i32* %msg_code, align 4, !tbaa !18
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %15, i32 0, i32 0
  %16 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err6, align 8, !tbaa !17
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %16, i32 0, i32 0
  %17 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !20
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = bitcast %struct.jpeg_compress_struct* %18 to %struct.jpeg_common_struct*
  call void %17(%struct.jpeg_common_struct* %19)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 6
  %21 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest8, align 8, !tbaa !6
  %22 = bitcast %struct.jpeg_destination_mgr* %21 to %struct.my_destination_mgr*
  store %struct.my_destination_mgr* %22, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %23 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %23, i32 0, i32 0
  %init_destination9 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub, i32 0, i32 2
  store void (%struct.jpeg_compress_struct*)* @init_destination, void (%struct.jpeg_compress_struct*)** %init_destination9, align 4, !tbaa !21
  %24 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %pub10 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %24, i32 0, i32 0
  %empty_output_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub10, i32 0, i32 3
  store i32 (%struct.jpeg_compress_struct*)* @empty_output_buffer, i32 (%struct.jpeg_compress_struct*)** %empty_output_buffer, align 4, !tbaa !23
  %25 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %pub11 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %25, i32 0, i32 0
  %term_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub11, i32 0, i32 4
  store void (%struct.jpeg_compress_struct*)* @term_destination, void (%struct.jpeg_compress_struct*)** %term_destination, align 4, !tbaa !24
  %26 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile.addr, align 4, !tbaa !2
  %27 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %outfile12 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %27, i32 0, i32 1
  store %struct._IO_FILE* %26, %struct._IO_FILE** %outfile12, align 4, !tbaa !25
  %28 = bitcast %struct.my_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @init_destination(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %dest = alloca %struct.my_destination_mgr*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest1, align 8, !tbaa !6
  %3 = bitcast %struct.jpeg_destination_mgr* %2 to %struct.my_destination_mgr*
  store %struct.my_destination_mgr* %3, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 1
  %5 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !11
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %5, i32 0, i32 0
  %6 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %8 = bitcast %struct.jpeg_compress_struct* %7 to %struct.jpeg_common_struct*
  %call = call i8* %6(%struct.jpeg_common_struct* %8, i32 1, i32 4096)
  %9 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %9, i32 0, i32 2
  store i8* %call, i8** %buffer, align 4, !tbaa !26
  %10 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %buffer2 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %10, i32 0, i32 2
  %11 = load i8*, i8** %buffer2, align 4, !tbaa !26
  %12 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %12, i32 0, i32 0
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub, i32 0, i32 0
  store i8* %11, i8** %next_output_byte, align 4, !tbaa !27
  %13 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %13, i32 0, i32 0
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub3, i32 0, i32 1
  store i32 4096, i32* %free_in_buffer, align 4, !tbaa !28
  %14 = bitcast %struct.my_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @empty_output_buffer(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %dest = alloca %struct.my_destination_mgr*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest1, align 8, !tbaa !6
  %3 = bitcast %struct.jpeg_destination_mgr* %2 to %struct.my_destination_mgr*
  store %struct.my_destination_mgr* %3, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %4 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %4, i32 0, i32 2
  %5 = load i8*, i8** %buffer, align 4, !tbaa !26
  %6 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %outfile = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %6, i32 0, i32 1
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 4, !tbaa !25
  %call = call i32 @fwrite(i8* %5, i32 1, i32 4096, %struct._IO_FILE* %7)
  %cmp = icmp ne i32 %call, 4096
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %8, i32 0, i32 0
  %9 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !17
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %9, i32 0, i32 5
  store i32 37, i32* %msg_code, align 4, !tbaa !18
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !17
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 0
  %12 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !20
  %13 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %14 = bitcast %struct.jpeg_compress_struct* %13 to %struct.jpeg_common_struct*
  call void %12(%struct.jpeg_common_struct* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %buffer3 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %15, i32 0, i32 2
  %16 = load i8*, i8** %buffer3, align 4, !tbaa !26
  %17 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %17, i32 0, i32 0
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub, i32 0, i32 0
  store i8* %16, i8** %next_output_byte, align 4, !tbaa !27
  %18 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %pub4 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %18, i32 0, i32 0
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub4, i32 0, i32 1
  store i32 4096, i32* %free_in_buffer, align 4, !tbaa !28
  %19 = bitcast %struct.my_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #3
  ret i32 1
}

; Function Attrs: nounwind
define internal void @term_destination(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %dest = alloca %struct.my_destination_mgr*, align 4
  %datacount = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest1, align 8, !tbaa !6
  %3 = bitcast %struct.jpeg_destination_mgr* %2 to %struct.my_destination_mgr*
  store %struct.my_destination_mgr* %3, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %4 = bitcast i32* %datacount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %5, i32 0, i32 0
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub, i32 0, i32 1
  %6 = load i32, i32* %free_in_buffer, align 4, !tbaa !28
  %sub = sub i32 4096, %6
  store i32 %sub, i32* %datacount, align 4, !tbaa !29
  %7 = load i32, i32* %datacount, align 4, !tbaa !29
  %cmp = icmp ugt i32 %7, 0
  br i1 %cmp, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %8 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %8, i32 0, i32 2
  %9 = load i8*, i8** %buffer, align 4, !tbaa !26
  %10 = load i32, i32* %datacount, align 4, !tbaa !29
  %11 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %outfile = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %11, i32 0, i32 1
  %12 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 4, !tbaa !25
  %call = call i32 @fwrite(i8* %9, i32 1, i32 %10, %struct._IO_FILE* %12)
  %13 = load i32, i32* %datacount, align 4, !tbaa !29
  %cmp2 = icmp ne i32 %call, %13
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !17
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 5
  store i32 37, i32* %msg_code, align 4, !tbaa !18
  %16 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %16, i32 0, i32 0
  %17 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err4, align 8, !tbaa !17
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %17, i32 0, i32 0
  %18 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !20
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %20 = bitcast %struct.jpeg_compress_struct* %19 to %struct.jpeg_common_struct*
  call void %18(%struct.jpeg_common_struct* %20)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  %21 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %outfile6 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %21, i32 0, i32 1
  %22 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile6, align 4, !tbaa !25
  %call7 = call i32 @fflush(%struct._IO_FILE* %22)
  %23 = load %struct.my_destination_mgr*, %struct.my_destination_mgr** %dest, align 4, !tbaa !2
  %outfile8 = getelementptr inbounds %struct.my_destination_mgr, %struct.my_destination_mgr* %23, i32 0, i32 1
  %24 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile8, align 4, !tbaa !25
  %call9 = call i32 @ferror(%struct._IO_FILE* %24)
  %tobool = icmp ne i32 %call9, 0
  br i1 %tobool, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.end5
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 0
  %26 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err11, align 8, !tbaa !17
  %msg_code12 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %26, i32 0, i32 5
  store i32 37, i32* %msg_code12, align 4, !tbaa !18
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 0
  %28 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 8, !tbaa !17
  %error_exit14 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %28, i32 0, i32 0
  %29 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit14, align 4, !tbaa !20
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_compress_struct* %30 to %struct.jpeg_common_struct*
  call void %29(%struct.jpeg_common_struct* %31)
  br label %if.end15

if.end15:                                         ; preds = %if.then10, %if.end5
  %32 = bitcast i32* %datacount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  %33 = bitcast %struct.my_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define hidden void @jpeg_mem_dest_internal(%struct.jpeg_compress_struct* %cinfo, i8** %outbuffer, i32* %outsize, i32 %pool_id) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %outbuffer.addr = alloca i8**, align 4
  %outsize.addr = alloca i32*, align 4
  %pool_id.addr = alloca i32, align 4
  %dest = alloca %struct.my_mem_destination_mgr*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %outbuffer, i8*** %outbuffer.addr, align 4, !tbaa !2
  store i32* %outsize, i32** %outsize.addr, align 4, !tbaa !2
  store i32 %pool_id, i32* %pool_id.addr, align 4, !tbaa !30
  %0 = bitcast %struct.my_mem_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i8**, i8*** %outbuffer.addr, align 4, !tbaa !2
  %cmp = icmp eq i8** %1, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32*, i32** %outsize.addr, align 4, !tbaa !2
  %cmp1 = icmp eq i32* %2, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 0
  %4 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !17
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %4, i32 0, i32 5
  store i32 23, i32* %msg_code, align 4, !tbaa !18
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !17
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 0
  %7 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !20
  %8 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = bitcast %struct.jpeg_compress_struct* %8 to %struct.jpeg_common_struct*
  call void %7(%struct.jpeg_common_struct* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 6
  %11 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest3, align 8, !tbaa !6
  %cmp4 = icmp eq %struct.jpeg_destination_mgr* %11, null
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 1
  %13 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !11
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %13, i32 0, i32 0
  %14 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_compress_struct* %15 to %struct.jpeg_common_struct*
  %17 = load i32, i32* %pool_id.addr, align 4, !tbaa !30
  %call = call i8* %14(%struct.jpeg_common_struct* %16, i32 %17, i32 40)
  %18 = bitcast i8* %call to %struct.jpeg_destination_mgr*
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest6 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 6
  store %struct.jpeg_destination_mgr* %18, %struct.jpeg_destination_mgr** %dest6, align 8, !tbaa !6
  br label %if.end15

if.else:                                          ; preds = %if.end
  %20 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest7 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %20, i32 0, i32 6
  %21 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest7, align 8, !tbaa !6
  %init_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %21, i32 0, i32 2
  %22 = load void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)** %init_destination, align 4, !tbaa !15
  %cmp8 = icmp ne void (%struct.jpeg_compress_struct*)* %22, @init_mem_destination
  br i1 %cmp8, label %if.then9, label %if.end14

if.then9:                                         ; preds = %if.else
  %23 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %23, i32 0, i32 0
  %24 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err10, align 8, !tbaa !17
  %msg_code11 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %24, i32 0, i32 5
  store i32 23, i32* %msg_code11, align 4, !tbaa !18
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %25, i32 0, i32 0
  %26 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !17
  %error_exit13 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %26, i32 0, i32 0
  %27 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit13, align 4, !tbaa !20
  %28 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %29 = bitcast %struct.jpeg_compress_struct* %28 to %struct.jpeg_common_struct*
  call void %27(%struct.jpeg_common_struct* %29)
  br label %if.end14

if.end14:                                         ; preds = %if.then9, %if.else
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.then5
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest16 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 6
  %31 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest16, align 8, !tbaa !6
  %32 = bitcast %struct.jpeg_destination_mgr* %31 to %struct.my_mem_destination_mgr*
  store %struct.my_mem_destination_mgr* %32, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %33 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %33, i32 0, i32 0
  %init_destination17 = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub, i32 0, i32 2
  store void (%struct.jpeg_compress_struct*)* @init_mem_destination, void (%struct.jpeg_compress_struct*)** %init_destination17, align 4, !tbaa !31
  %34 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %pub18 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %34, i32 0, i32 0
  %empty_output_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub18, i32 0, i32 3
  store i32 (%struct.jpeg_compress_struct*)* @empty_mem_output_buffer, i32 (%struct.jpeg_compress_struct*)** %empty_output_buffer, align 4, !tbaa !33
  %35 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %pub19 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %35, i32 0, i32 0
  %term_destination = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub19, i32 0, i32 4
  store void (%struct.jpeg_compress_struct*)* @term_mem_destination, void (%struct.jpeg_compress_struct*)** %term_destination, align 4, !tbaa !34
  %36 = load i8**, i8*** %outbuffer.addr, align 4, !tbaa !2
  %37 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %outbuffer20 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %37, i32 0, i32 1
  store i8** %36, i8*** %outbuffer20, align 4, !tbaa !35
  %38 = load i32*, i32** %outsize.addr, align 4, !tbaa !2
  %39 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %outsize21 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %39, i32 0, i32 2
  store i32* %38, i32** %outsize21, align 4, !tbaa !36
  %40 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %newbuffer = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %40, i32 0, i32 3
  store i8* null, i8** %newbuffer, align 4, !tbaa !37
  %41 = load i8**, i8*** %outbuffer.addr, align 4, !tbaa !2
  %42 = load i8*, i8** %41, align 4, !tbaa !2
  %cmp22 = icmp eq i8* %42, null
  br i1 %cmp22, label %if.then25, label %lor.lhs.false23

lor.lhs.false23:                                  ; preds = %if.end15
  %43 = load i32*, i32** %outsize.addr, align 4, !tbaa !2
  %44 = load i32, i32* %43, align 4, !tbaa !29
  %cmp24 = icmp eq i32 %44, 0
  br i1 %cmp24, label %if.then25, label %if.end37

if.then25:                                        ; preds = %lor.lhs.false23, %if.end15
  %call26 = call i8* @malloc(i32 4096)
  %45 = load i8**, i8*** %outbuffer.addr, align 4, !tbaa !2
  store i8* %call26, i8** %45, align 4, !tbaa !2
  %46 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %newbuffer27 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %46, i32 0, i32 3
  store i8* %call26, i8** %newbuffer27, align 4, !tbaa !37
  %47 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %newbuffer28 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %47, i32 0, i32 3
  %48 = load i8*, i8** %newbuffer28, align 4, !tbaa !37
  %cmp29 = icmp eq i8* %48, null
  br i1 %cmp29, label %if.then30, label %if.end36

if.then30:                                        ; preds = %if.then25
  %49 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err31 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %49, i32 0, i32 0
  %50 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err31, align 8, !tbaa !17
  %msg_code32 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %50, i32 0, i32 5
  store i32 54, i32* %msg_code32, align 4, !tbaa !18
  %51 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err33 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %51, i32 0, i32 0
  %52 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err33, align 8, !tbaa !17
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %52, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 10, i32* %arrayidx, align 4, !tbaa !38
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err34 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %53, i32 0, i32 0
  %54 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err34, align 8, !tbaa !17
  %error_exit35 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %54, i32 0, i32 0
  %55 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit35, align 4, !tbaa !20
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %57 = bitcast %struct.jpeg_compress_struct* %56 to %struct.jpeg_common_struct*
  call void %55(%struct.jpeg_common_struct* %57)
  br label %if.end36

if.end36:                                         ; preds = %if.then30, %if.then25
  %58 = load i32*, i32** %outsize.addr, align 4, !tbaa !2
  store i32 4096, i32* %58, align 4, !tbaa !29
  br label %if.end37

if.end37:                                         ; preds = %if.end36, %lor.lhs.false23
  %59 = load i8**, i8*** %outbuffer.addr, align 4, !tbaa !2
  %60 = load i8*, i8** %59, align 4, !tbaa !2
  %61 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %61, i32 0, i32 4
  store i8* %60, i8** %buffer, align 4, !tbaa !39
  %62 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %pub38 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %62, i32 0, i32 0
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub38, i32 0, i32 0
  store i8* %60, i8** %next_output_byte, align 4, !tbaa !40
  %63 = load i32*, i32** %outsize.addr, align 4, !tbaa !2
  %64 = load i32, i32* %63, align 4, !tbaa !29
  %65 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %bufsize = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %65, i32 0, i32 5
  store i32 %64, i32* %bufsize, align 4, !tbaa !41
  %66 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %pub39 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %66, i32 0, i32 0
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub39, i32 0, i32 1
  store i32 %64, i32* %free_in_buffer, align 4, !tbaa !42
  %67 = bitcast %struct.my_mem_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #3
  ret void
}

; Function Attrs: nounwind
define internal void @init_mem_destination(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal i32 @empty_mem_output_buffer(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %nextsize = alloca i32, align 4
  %nextbuffer = alloca i8*, align 4
  %dest = alloca %struct.my_mem_destination_mgr*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast i32* %nextsize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i8** %nextbuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast %struct.my_mem_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %3, i32 0, i32 6
  %4 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest1, align 8, !tbaa !6
  %5 = bitcast %struct.jpeg_destination_mgr* %4 to %struct.my_mem_destination_mgr*
  store %struct.my_mem_destination_mgr* %5, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %6 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %bufsize = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %6, i32 0, i32 5
  %7 = load i32, i32* %bufsize, align 4, !tbaa !41
  %mul = mul i32 %7, 2
  store i32 %mul, i32* %nextsize, align 4, !tbaa !29
  %8 = load i32, i32* %nextsize, align 4, !tbaa !29
  %call = call i8* @malloc(i32 %8)
  store i8* %call, i8** %nextbuffer, align 4, !tbaa !2
  %9 = load i8*, i8** %nextbuffer, align 4, !tbaa !2
  %cmp = icmp eq i8* %9, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %10, i32 0, i32 0
  %11 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !17
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %11, i32 0, i32 5
  store i32 54, i32* %msg_code, align 4, !tbaa !18
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 0
  %13 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !17
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %13, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 10, i32* %arrayidx, align 4, !tbaa !38
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err3 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %14, i32 0, i32 0
  %15 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err3, align 8, !tbaa !17
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %15, i32 0, i32 0
  %16 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !20
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %18 = bitcast %struct.jpeg_compress_struct* %17 to %struct.jpeg_common_struct*
  call void %16(%struct.jpeg_common_struct* %18)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %19 = load i8*, i8** %nextbuffer, align 4, !tbaa !2
  %20 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %20, i32 0, i32 4
  %21 = load i8*, i8** %buffer, align 4, !tbaa !39
  %22 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %bufsize4 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %22, i32 0, i32 5
  %23 = load i32, i32* %bufsize4, align 4, !tbaa !41
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %19, i8* align 1 %21, i32 %23, i1 false)
  %24 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %newbuffer = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %24, i32 0, i32 3
  %25 = load i8*, i8** %newbuffer, align 4, !tbaa !37
  %cmp5 = icmp ne i8* %25, null
  br i1 %cmp5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  %26 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %newbuffer7 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %26, i32 0, i32 3
  %27 = load i8*, i8** %newbuffer7, align 4, !tbaa !37
  call void @free(i8* %27)
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.end
  %28 = load i8*, i8** %nextbuffer, align 4, !tbaa !2
  %29 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %newbuffer9 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %29, i32 0, i32 3
  store i8* %28, i8** %newbuffer9, align 4, !tbaa !37
  %30 = load i8*, i8** %nextbuffer, align 4, !tbaa !2
  %31 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %bufsize10 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %31, i32 0, i32 5
  %32 = load i32, i32* %bufsize10, align 4, !tbaa !41
  %add.ptr = getelementptr inbounds i8, i8* %30, i32 %32
  %33 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %33, i32 0, i32 0
  %next_output_byte = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub, i32 0, i32 0
  store i8* %add.ptr, i8** %next_output_byte, align 4, !tbaa !40
  %34 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %bufsize11 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %34, i32 0, i32 5
  %35 = load i32, i32* %bufsize11, align 4, !tbaa !41
  %36 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %pub12 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %36, i32 0, i32 0
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub12, i32 0, i32 1
  store i32 %35, i32* %free_in_buffer, align 4, !tbaa !42
  %37 = load i8*, i8** %nextbuffer, align 4, !tbaa !2
  %38 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %buffer13 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %38, i32 0, i32 4
  store i8* %37, i8** %buffer13, align 4, !tbaa !39
  %39 = load i32, i32* %nextsize, align 4, !tbaa !29
  %40 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %bufsize14 = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %40, i32 0, i32 5
  store i32 %39, i32* %bufsize14, align 4, !tbaa !41
  %41 = bitcast %struct.my_mem_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #3
  %42 = bitcast i8** %nextbuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  %43 = bitcast i32* %nextsize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  ret i32 1
}

; Function Attrs: nounwind
define internal void @term_mem_destination(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %dest = alloca %struct.my_mem_destination_mgr*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_mem_destination_mgr** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %dest1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 6
  %2 = load %struct.jpeg_destination_mgr*, %struct.jpeg_destination_mgr** %dest1, align 8, !tbaa !6
  %3 = bitcast %struct.jpeg_destination_mgr* %2 to %struct.my_mem_destination_mgr*
  store %struct.my_mem_destination_mgr* %3, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %4 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %buffer = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %4, i32 0, i32 4
  %5 = load i8*, i8** %buffer, align 4, !tbaa !39
  %6 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %outbuffer = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %6, i32 0, i32 1
  %7 = load i8**, i8*** %outbuffer, align 4, !tbaa !35
  store i8* %5, i8** %7, align 4, !tbaa !2
  %8 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %bufsize = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %8, i32 0, i32 5
  %9 = load i32, i32* %bufsize, align 4, !tbaa !41
  %10 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %10, i32 0, i32 0
  %free_in_buffer = getelementptr inbounds %struct.jpeg_destination_mgr, %struct.jpeg_destination_mgr* %pub, i32 0, i32 1
  %11 = load i32, i32* %free_in_buffer, align 4, !tbaa !42
  %sub = sub i32 %9, %11
  %12 = load %struct.my_mem_destination_mgr*, %struct.my_mem_destination_mgr** %dest, align 4, !tbaa !2
  %outsize = getelementptr inbounds %struct.my_mem_destination_mgr, %struct.my_mem_destination_mgr* %12, i32 0, i32 2
  %13 = load i32*, i32** %outsize, align 4, !tbaa !36
  store i32 %sub, i32* %13, align 4, !tbaa !29
  %14 = bitcast %struct.my_mem_destination_mgr** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #3
  ret void
}

declare i8* @malloc(i32) #2

; Function Attrs: nounwind
define hidden void @jpeg_mem_dest(%struct.jpeg_compress_struct* %cinfo, i8** %outbuffer, i32* %outsize) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %outbuffer.addr = alloca i8**, align 4
  %outsize.addr = alloca i32*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %outbuffer, i8*** %outbuffer.addr, align 4, !tbaa !2
  store i32* %outsize, i32** %outsize.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8**, i8*** %outbuffer.addr, align 4, !tbaa !2
  %2 = load i32*, i32** %outsize.addr, align 4, !tbaa !2
  call void @jpeg_mem_dest_internal(%struct.jpeg_compress_struct* %0, i8** %1, i32* %2, i32 0)
  ret void
}

declare i32 @fwrite(i8*, i32, i32, %struct._IO_FILE*) #2

declare i32 @fflush(%struct._IO_FILE*) #2

declare i32 @ferror(%struct._IO_FILE*) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare void @free(i8*) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 24}
!7 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !9, i64 48, !8, i64 56, !8, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !8, i64 168, !3, i64 172, !8, i64 176, !8, i64 180, !8, i64 184, !8, i64 188, !8, i64 192, !4, i64 196, !8, i64 200, !8, i64 204, !8, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !10, i64 216, !10, i64 218, !8, i64 220, !8, i64 224, !8, i64 228, !8, i64 232, !8, i64 236, !8, i64 240, !8, i64 244, !4, i64 248, !8, i64 264, !8, i64 268, !8, i64 272, !4, i64 276, !8, i64 316, !8, i64 320, !8, i64 324, !8, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !8, i64 372}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!7, !3, i64 4}
!12 = !{!13, !3, i64 0}
!13 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!14 = !{!"long", !4, i64 0}
!15 = !{!16, !3, i64 8}
!16 = !{!"jpeg_destination_mgr", !3, i64 0, !14, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!17 = !{!7, !3, i64 0}
!18 = !{!19, !8, i64 20}
!19 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !14, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!20 = !{!19, !3, i64 0}
!21 = !{!22, !3, i64 8}
!22 = !{!"", !16, i64 0, !3, i64 20, !3, i64 24}
!23 = !{!22, !3, i64 12}
!24 = !{!22, !3, i64 16}
!25 = !{!22, !3, i64 20}
!26 = !{!22, !3, i64 24}
!27 = !{!22, !3, i64 0}
!28 = !{!22, !14, i64 4}
!29 = !{!14, !14, i64 0}
!30 = !{!8, !8, i64 0}
!31 = !{!32, !3, i64 8}
!32 = !{!"", !16, i64 0, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !14, i64 36}
!33 = !{!32, !3, i64 12}
!34 = !{!32, !3, i64 16}
!35 = !{!32, !3, i64 20}
!36 = !{!32, !3, i64 24}
!37 = !{!32, !3, i64 28}
!38 = !{!4, !4, i64 0}
!39 = !{!32, !3, i64 32}
!40 = !{!32, !3, i64 0}
!41 = !{!32, !14, i64 36}
!42 = !{!32, !14, i64 4}
