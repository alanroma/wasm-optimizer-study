; ModuleID = 'jidctflt.c'
source_filename = "jidctflt.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x {}*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }

; Function Attrs: nounwind
define hidden void @jpeg_idct_float(%struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_component_info* %compptr, i16* %coef_block, i8** %output_buf, i32 %output_col) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %compptr.addr = alloca %struct.jpeg_component_info*, align 4
  %coef_block.addr = alloca i16*, align 4
  %output_buf.addr = alloca i8**, align 4
  %output_col.addr = alloca i32, align 4
  %tmp0 = alloca float, align 4
  %tmp1 = alloca float, align 4
  %tmp2 = alloca float, align 4
  %tmp3 = alloca float, align 4
  %tmp4 = alloca float, align 4
  %tmp5 = alloca float, align 4
  %tmp6 = alloca float, align 4
  %tmp7 = alloca float, align 4
  %tmp10 = alloca float, align 4
  %tmp11 = alloca float, align 4
  %tmp12 = alloca float, align 4
  %tmp13 = alloca float, align 4
  %z5 = alloca float, align 4
  %z10 = alloca float, align 4
  %z11 = alloca float, align 4
  %z12 = alloca float, align 4
  %z13 = alloca float, align 4
  %inptr = alloca i16*, align 4
  %quantptr = alloca float*, align 4
  %wsptr = alloca float*, align 4
  %outptr = alloca i8*, align 4
  %range_limit = alloca i8*, align 4
  %ctr = alloca i32, align 4
  %workspace = alloca [64 x float], align 16
  %dcval = alloca float, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store %struct.jpeg_component_info* %compptr, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  store i16* %coef_block, i16** %coef_block.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %output_col, i32* %output_col.addr, align 4, !tbaa !6
  %0 = bitcast float* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast float* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast float* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast float* %tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast float* %tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast float* %tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast float* %tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast float* %tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast float* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast float* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast float* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast float* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast float* %z5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast float* %z10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast float* %z11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast float* %z12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast float* %z13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast float** %quantptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = bitcast float** %wsptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #2
  %20 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #2
  %21 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #2
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 65
  %23 = load i8*, i8** %sample_range_limit, align 4, !tbaa !8
  store i8* %23, i8** %range_limit, align 4, !tbaa !2
  %24 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #2
  %25 = bitcast [64 x float]* %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 256, i8* %25) #2
  %26 = load i16*, i16** %coef_block.addr, align 4, !tbaa !2
  store i16* %26, i16** %inptr, align 4, !tbaa !2
  %27 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr.addr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %27, i32 0, i32 20
  %28 = load i8*, i8** %dct_table, align 4, !tbaa !12
  %29 = bitcast i8* %28 to float*
  store float* %29, float** %quantptr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [64 x float], [64 x float]* %workspace, i32 0, i32 0
  store float* %arraydecay, float** %wsptr, align 4, !tbaa !2
  store i32 8, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %30 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp sgt i32 %30, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %31, i32 8
  %32 = load i16, i16* %arrayidx, align 2, !tbaa !14
  %conv = sext i16 %32 to i32
  %cmp1 = icmp eq i32 %conv, 0
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %33 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i16, i16* %33, i32 16
  %34 = load i16, i16* %arrayidx3, align 2, !tbaa !14
  %conv4 = sext i16 %34 to i32
  %cmp5 = icmp eq i32 %conv4, 0
  br i1 %cmp5, label %land.lhs.true7, label %if.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %35 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i16, i16* %35, i32 24
  %36 = load i16, i16* %arrayidx8, align 2, !tbaa !14
  %conv9 = sext i16 %36 to i32
  %cmp10 = icmp eq i32 %conv9, 0
  br i1 %cmp10, label %land.lhs.true12, label %if.end

land.lhs.true12:                                  ; preds = %land.lhs.true7
  %37 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i16, i16* %37, i32 32
  %38 = load i16, i16* %arrayidx13, align 2, !tbaa !14
  %conv14 = sext i16 %38 to i32
  %cmp15 = icmp eq i32 %conv14, 0
  br i1 %cmp15, label %land.lhs.true17, label %if.end

land.lhs.true17:                                  ; preds = %land.lhs.true12
  %39 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds i16, i16* %39, i32 40
  %40 = load i16, i16* %arrayidx18, align 2, !tbaa !14
  %conv19 = sext i16 %40 to i32
  %cmp20 = icmp eq i32 %conv19, 0
  br i1 %cmp20, label %land.lhs.true22, label %if.end

land.lhs.true22:                                  ; preds = %land.lhs.true17
  %41 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds i16, i16* %41, i32 48
  %42 = load i16, i16* %arrayidx23, align 2, !tbaa !14
  %conv24 = sext i16 %42 to i32
  %cmp25 = icmp eq i32 %conv24, 0
  br i1 %cmp25, label %land.lhs.true27, label %if.end

land.lhs.true27:                                  ; preds = %land.lhs.true22
  %43 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i16, i16* %43, i32 56
  %44 = load i16, i16* %arrayidx28, align 2, !tbaa !14
  %conv29 = sext i16 %44 to i32
  %cmp30 = icmp eq i32 %conv29, 0
  br i1 %cmp30, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true27
  %45 = bitcast float* %dcval to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #2
  %46 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds i16, i16* %46, i32 0
  %47 = load i16, i16* %arrayidx32, align 2, !tbaa !14
  %conv33 = sitofp i16 %47 to float
  %48 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds float, float* %48, i32 0
  %49 = load float, float* %arrayidx34, align 4, !tbaa !15
  %mul = fmul float %49, 1.250000e-01
  %mul35 = fmul float %conv33, %mul
  store float %mul35, float* %dcval, align 4, !tbaa !15
  %50 = load float, float* %dcval, align 4, !tbaa !15
  %51 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds float, float* %51, i32 0
  store float %50, float* %arrayidx36, align 4, !tbaa !15
  %52 = load float, float* %dcval, align 4, !tbaa !15
  %53 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds float, float* %53, i32 8
  store float %52, float* %arrayidx37, align 4, !tbaa !15
  %54 = load float, float* %dcval, align 4, !tbaa !15
  %55 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds float, float* %55, i32 16
  store float %54, float* %arrayidx38, align 4, !tbaa !15
  %56 = load float, float* %dcval, align 4, !tbaa !15
  %57 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds float, float* %57, i32 24
  store float %56, float* %arrayidx39, align 4, !tbaa !15
  %58 = load float, float* %dcval, align 4, !tbaa !15
  %59 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds float, float* %59, i32 32
  store float %58, float* %arrayidx40, align 4, !tbaa !15
  %60 = load float, float* %dcval, align 4, !tbaa !15
  %61 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds float, float* %61, i32 40
  store float %60, float* %arrayidx41, align 4, !tbaa !15
  %62 = load float, float* %dcval, align 4, !tbaa !15
  %63 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds float, float* %63, i32 48
  store float %62, float* %arrayidx42, align 4, !tbaa !15
  %64 = load float, float* %dcval, align 4, !tbaa !15
  %65 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds float, float* %65, i32 56
  store float %64, float* %arrayidx43, align 4, !tbaa !15
  %66 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i16, i16* %66, i32 1
  store i16* %incdec.ptr, i16** %inptr, align 4, !tbaa !2
  %67 = load float*, float** %quantptr, align 4, !tbaa !2
  %incdec.ptr44 = getelementptr inbounds float, float* %67, i32 1
  store float* %incdec.ptr44, float** %quantptr, align 4, !tbaa !2
  %68 = load float*, float** %wsptr, align 4, !tbaa !2
  %incdec.ptr45 = getelementptr inbounds float, float* %68, i32 1
  store float* %incdec.ptr45, float** %wsptr, align 4, !tbaa !2
  %69 = bitcast float* %dcval to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #2
  br label %for.inc

if.end:                                           ; preds = %land.lhs.true27, %land.lhs.true22, %land.lhs.true17, %land.lhs.true12, %land.lhs.true7, %land.lhs.true, %for.body
  %70 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds i16, i16* %70, i32 0
  %71 = load i16, i16* %arrayidx46, align 2, !tbaa !14
  %conv47 = sitofp i16 %71 to float
  %72 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds float, float* %72, i32 0
  %73 = load float, float* %arrayidx48, align 4, !tbaa !15
  %mul49 = fmul float %73, 1.250000e-01
  %mul50 = fmul float %conv47, %mul49
  store float %mul50, float* %tmp0, align 4, !tbaa !15
  %74 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds i16, i16* %74, i32 16
  %75 = load i16, i16* %arrayidx51, align 2, !tbaa !14
  %conv52 = sitofp i16 %75 to float
  %76 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds float, float* %76, i32 16
  %77 = load float, float* %arrayidx53, align 4, !tbaa !15
  %mul54 = fmul float %77, 1.250000e-01
  %mul55 = fmul float %conv52, %mul54
  store float %mul55, float* %tmp1, align 4, !tbaa !15
  %78 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i16, i16* %78, i32 32
  %79 = load i16, i16* %arrayidx56, align 2, !tbaa !14
  %conv57 = sitofp i16 %79 to float
  %80 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds float, float* %80, i32 32
  %81 = load float, float* %arrayidx58, align 4, !tbaa !15
  %mul59 = fmul float %81, 1.250000e-01
  %mul60 = fmul float %conv57, %mul59
  store float %mul60, float* %tmp2, align 4, !tbaa !15
  %82 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i16, i16* %82, i32 48
  %83 = load i16, i16* %arrayidx61, align 2, !tbaa !14
  %conv62 = sitofp i16 %83 to float
  %84 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx63 = getelementptr inbounds float, float* %84, i32 48
  %85 = load float, float* %arrayidx63, align 4, !tbaa !15
  %mul64 = fmul float %85, 1.250000e-01
  %mul65 = fmul float %conv62, %mul64
  store float %mul65, float* %tmp3, align 4, !tbaa !15
  %86 = load float, float* %tmp0, align 4, !tbaa !15
  %87 = load float, float* %tmp2, align 4, !tbaa !15
  %add = fadd float %86, %87
  store float %add, float* %tmp10, align 4, !tbaa !15
  %88 = load float, float* %tmp0, align 4, !tbaa !15
  %89 = load float, float* %tmp2, align 4, !tbaa !15
  %sub = fsub float %88, %89
  store float %sub, float* %tmp11, align 4, !tbaa !15
  %90 = load float, float* %tmp1, align 4, !tbaa !15
  %91 = load float, float* %tmp3, align 4, !tbaa !15
  %add66 = fadd float %90, %91
  store float %add66, float* %tmp13, align 4, !tbaa !15
  %92 = load float, float* %tmp1, align 4, !tbaa !15
  %93 = load float, float* %tmp3, align 4, !tbaa !15
  %sub67 = fsub float %92, %93
  %mul68 = fmul float %sub67, 0x3FF6A09E60000000
  %94 = load float, float* %tmp13, align 4, !tbaa !15
  %sub69 = fsub float %mul68, %94
  store float %sub69, float* %tmp12, align 4, !tbaa !15
  %95 = load float, float* %tmp10, align 4, !tbaa !15
  %96 = load float, float* %tmp13, align 4, !tbaa !15
  %add70 = fadd float %95, %96
  store float %add70, float* %tmp0, align 4, !tbaa !15
  %97 = load float, float* %tmp10, align 4, !tbaa !15
  %98 = load float, float* %tmp13, align 4, !tbaa !15
  %sub71 = fsub float %97, %98
  store float %sub71, float* %tmp3, align 4, !tbaa !15
  %99 = load float, float* %tmp11, align 4, !tbaa !15
  %100 = load float, float* %tmp12, align 4, !tbaa !15
  %add72 = fadd float %99, %100
  store float %add72, float* %tmp1, align 4, !tbaa !15
  %101 = load float, float* %tmp11, align 4, !tbaa !15
  %102 = load float, float* %tmp12, align 4, !tbaa !15
  %sub73 = fsub float %101, %102
  store float %sub73, float* %tmp2, align 4, !tbaa !15
  %103 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx74 = getelementptr inbounds i16, i16* %103, i32 8
  %104 = load i16, i16* %arrayidx74, align 2, !tbaa !14
  %conv75 = sitofp i16 %104 to float
  %105 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds float, float* %105, i32 8
  %106 = load float, float* %arrayidx76, align 4, !tbaa !15
  %mul77 = fmul float %106, 1.250000e-01
  %mul78 = fmul float %conv75, %mul77
  store float %mul78, float* %tmp4, align 4, !tbaa !15
  %107 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i16, i16* %107, i32 24
  %108 = load i16, i16* %arrayidx79, align 2, !tbaa !14
  %conv80 = sitofp i16 %108 to float
  %109 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds float, float* %109, i32 24
  %110 = load float, float* %arrayidx81, align 4, !tbaa !15
  %mul82 = fmul float %110, 1.250000e-01
  %mul83 = fmul float %conv80, %mul82
  store float %mul83, float* %tmp5, align 4, !tbaa !15
  %111 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i16, i16* %111, i32 40
  %112 = load i16, i16* %arrayidx84, align 2, !tbaa !14
  %conv85 = sitofp i16 %112 to float
  %113 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds float, float* %113, i32 40
  %114 = load float, float* %arrayidx86, align 4, !tbaa !15
  %mul87 = fmul float %114, 1.250000e-01
  %mul88 = fmul float %conv85, %mul87
  store float %mul88, float* %tmp6, align 4, !tbaa !15
  %115 = load i16*, i16** %inptr, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i16, i16* %115, i32 56
  %116 = load i16, i16* %arrayidx89, align 2, !tbaa !14
  %conv90 = sitofp i16 %116 to float
  %117 = load float*, float** %quantptr, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds float, float* %117, i32 56
  %118 = load float, float* %arrayidx91, align 4, !tbaa !15
  %mul92 = fmul float %118, 1.250000e-01
  %mul93 = fmul float %conv90, %mul92
  store float %mul93, float* %tmp7, align 4, !tbaa !15
  %119 = load float, float* %tmp6, align 4, !tbaa !15
  %120 = load float, float* %tmp5, align 4, !tbaa !15
  %add94 = fadd float %119, %120
  store float %add94, float* %z13, align 4, !tbaa !15
  %121 = load float, float* %tmp6, align 4, !tbaa !15
  %122 = load float, float* %tmp5, align 4, !tbaa !15
  %sub95 = fsub float %121, %122
  store float %sub95, float* %z10, align 4, !tbaa !15
  %123 = load float, float* %tmp4, align 4, !tbaa !15
  %124 = load float, float* %tmp7, align 4, !tbaa !15
  %add96 = fadd float %123, %124
  store float %add96, float* %z11, align 4, !tbaa !15
  %125 = load float, float* %tmp4, align 4, !tbaa !15
  %126 = load float, float* %tmp7, align 4, !tbaa !15
  %sub97 = fsub float %125, %126
  store float %sub97, float* %z12, align 4, !tbaa !15
  %127 = load float, float* %z11, align 4, !tbaa !15
  %128 = load float, float* %z13, align 4, !tbaa !15
  %add98 = fadd float %127, %128
  store float %add98, float* %tmp7, align 4, !tbaa !15
  %129 = load float, float* %z11, align 4, !tbaa !15
  %130 = load float, float* %z13, align 4, !tbaa !15
  %sub99 = fsub float %129, %130
  %mul100 = fmul float %sub99, 0x3FF6A09E60000000
  store float %mul100, float* %tmp11, align 4, !tbaa !15
  %131 = load float, float* %z10, align 4, !tbaa !15
  %132 = load float, float* %z12, align 4, !tbaa !15
  %add101 = fadd float %131, %132
  %mul102 = fmul float %add101, 0x3FFD906BC0000000
  store float %mul102, float* %z5, align 4, !tbaa !15
  %133 = load float, float* %z5, align 4, !tbaa !15
  %134 = load float, float* %z12, align 4, !tbaa !15
  %mul103 = fmul float %134, 0x3FF1517A80000000
  %sub104 = fsub float %133, %mul103
  store float %sub104, float* %tmp10, align 4, !tbaa !15
  %135 = load float, float* %z5, align 4, !tbaa !15
  %136 = load float, float* %z10, align 4, !tbaa !15
  %mul105 = fmul float %136, 0x4004E7AEA0000000
  %sub106 = fsub float %135, %mul105
  store float %sub106, float* %tmp12, align 4, !tbaa !15
  %137 = load float, float* %tmp12, align 4, !tbaa !15
  %138 = load float, float* %tmp7, align 4, !tbaa !15
  %sub107 = fsub float %137, %138
  store float %sub107, float* %tmp6, align 4, !tbaa !15
  %139 = load float, float* %tmp11, align 4, !tbaa !15
  %140 = load float, float* %tmp6, align 4, !tbaa !15
  %sub108 = fsub float %139, %140
  store float %sub108, float* %tmp5, align 4, !tbaa !15
  %141 = load float, float* %tmp10, align 4, !tbaa !15
  %142 = load float, float* %tmp5, align 4, !tbaa !15
  %sub109 = fsub float %141, %142
  store float %sub109, float* %tmp4, align 4, !tbaa !15
  %143 = load float, float* %tmp0, align 4, !tbaa !15
  %144 = load float, float* %tmp7, align 4, !tbaa !15
  %add110 = fadd float %143, %144
  %145 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds float, float* %145, i32 0
  store float %add110, float* %arrayidx111, align 4, !tbaa !15
  %146 = load float, float* %tmp0, align 4, !tbaa !15
  %147 = load float, float* %tmp7, align 4, !tbaa !15
  %sub112 = fsub float %146, %147
  %148 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds float, float* %148, i32 56
  store float %sub112, float* %arrayidx113, align 4, !tbaa !15
  %149 = load float, float* %tmp1, align 4, !tbaa !15
  %150 = load float, float* %tmp6, align 4, !tbaa !15
  %add114 = fadd float %149, %150
  %151 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds float, float* %151, i32 8
  store float %add114, float* %arrayidx115, align 4, !tbaa !15
  %152 = load float, float* %tmp1, align 4, !tbaa !15
  %153 = load float, float* %tmp6, align 4, !tbaa !15
  %sub116 = fsub float %152, %153
  %154 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx117 = getelementptr inbounds float, float* %154, i32 48
  store float %sub116, float* %arrayidx117, align 4, !tbaa !15
  %155 = load float, float* %tmp2, align 4, !tbaa !15
  %156 = load float, float* %tmp5, align 4, !tbaa !15
  %add118 = fadd float %155, %156
  %157 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds float, float* %157, i32 16
  store float %add118, float* %arrayidx119, align 4, !tbaa !15
  %158 = load float, float* %tmp2, align 4, !tbaa !15
  %159 = load float, float* %tmp5, align 4, !tbaa !15
  %sub120 = fsub float %158, %159
  %160 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx121 = getelementptr inbounds float, float* %160, i32 40
  store float %sub120, float* %arrayidx121, align 4, !tbaa !15
  %161 = load float, float* %tmp3, align 4, !tbaa !15
  %162 = load float, float* %tmp4, align 4, !tbaa !15
  %add122 = fadd float %161, %162
  %163 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds float, float* %163, i32 24
  store float %add122, float* %arrayidx123, align 4, !tbaa !15
  %164 = load float, float* %tmp3, align 4, !tbaa !15
  %165 = load float, float* %tmp4, align 4, !tbaa !15
  %sub124 = fsub float %164, %165
  %166 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx125 = getelementptr inbounds float, float* %166, i32 32
  store float %sub124, float* %arrayidx125, align 4, !tbaa !15
  %167 = load i16*, i16** %inptr, align 4, !tbaa !2
  %incdec.ptr126 = getelementptr inbounds i16, i16* %167, i32 1
  store i16* %incdec.ptr126, i16** %inptr, align 4, !tbaa !2
  %168 = load float*, float** %quantptr, align 4, !tbaa !2
  %incdec.ptr127 = getelementptr inbounds float, float* %168, i32 1
  store float* %incdec.ptr127, float** %quantptr, align 4, !tbaa !2
  %169 = load float*, float** %wsptr, align 4, !tbaa !2
  %incdec.ptr128 = getelementptr inbounds float, float* %169, i32 1
  store float* %incdec.ptr128, float** %wsptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %if.end, %if.then
  %170 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec = add nsw i32 %170, -1
  store i32 %dec, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay129 = getelementptr inbounds [64 x float], [64 x float]* %workspace, i32 0, i32 0
  store float* %arraydecay129, float** %wsptr, align 4, !tbaa !2
  store i32 0, i32* %ctr, align 4, !tbaa !6
  br label %for.cond130

for.cond130:                                      ; preds = %for.inc217, %for.end
  %171 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp131 = icmp slt i32 %171, 8
  br i1 %cmp131, label %for.body133, label %for.end218

for.body133:                                      ; preds = %for.cond130
  %172 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %173 = load i32, i32* %ctr, align 4, !tbaa !6
  %arrayidx134 = getelementptr inbounds i8*, i8** %172, i32 %173
  %174 = load i8*, i8** %arrayidx134, align 4, !tbaa !2
  %175 = load i32, i32* %output_col.addr, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8, i8* %174, i32 %175
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %176 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds float, float* %176, i32 0
  %177 = load float, float* %arrayidx135, align 4, !tbaa !15
  %add136 = fadd float %177, 1.285000e+02
  store float %add136, float* %z5, align 4, !tbaa !15
  %178 = load float, float* %z5, align 4, !tbaa !15
  %179 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds float, float* %179, i32 4
  %180 = load float, float* %arrayidx137, align 4, !tbaa !15
  %add138 = fadd float %178, %180
  store float %add138, float* %tmp10, align 4, !tbaa !15
  %181 = load float, float* %z5, align 4, !tbaa !15
  %182 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds float, float* %182, i32 4
  %183 = load float, float* %arrayidx139, align 4, !tbaa !15
  %sub140 = fsub float %181, %183
  store float %sub140, float* %tmp11, align 4, !tbaa !15
  %184 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx141 = getelementptr inbounds float, float* %184, i32 2
  %185 = load float, float* %arrayidx141, align 4, !tbaa !15
  %186 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx142 = getelementptr inbounds float, float* %186, i32 6
  %187 = load float, float* %arrayidx142, align 4, !tbaa !15
  %add143 = fadd float %185, %187
  store float %add143, float* %tmp13, align 4, !tbaa !15
  %188 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds float, float* %188, i32 2
  %189 = load float, float* %arrayidx144, align 4, !tbaa !15
  %190 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx145 = getelementptr inbounds float, float* %190, i32 6
  %191 = load float, float* %arrayidx145, align 4, !tbaa !15
  %sub146 = fsub float %189, %191
  %mul147 = fmul float %sub146, 0x3FF6A09E60000000
  %192 = load float, float* %tmp13, align 4, !tbaa !15
  %sub148 = fsub float %mul147, %192
  store float %sub148, float* %tmp12, align 4, !tbaa !15
  %193 = load float, float* %tmp10, align 4, !tbaa !15
  %194 = load float, float* %tmp13, align 4, !tbaa !15
  %add149 = fadd float %193, %194
  store float %add149, float* %tmp0, align 4, !tbaa !15
  %195 = load float, float* %tmp10, align 4, !tbaa !15
  %196 = load float, float* %tmp13, align 4, !tbaa !15
  %sub150 = fsub float %195, %196
  store float %sub150, float* %tmp3, align 4, !tbaa !15
  %197 = load float, float* %tmp11, align 4, !tbaa !15
  %198 = load float, float* %tmp12, align 4, !tbaa !15
  %add151 = fadd float %197, %198
  store float %add151, float* %tmp1, align 4, !tbaa !15
  %199 = load float, float* %tmp11, align 4, !tbaa !15
  %200 = load float, float* %tmp12, align 4, !tbaa !15
  %sub152 = fsub float %199, %200
  store float %sub152, float* %tmp2, align 4, !tbaa !15
  %201 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds float, float* %201, i32 5
  %202 = load float, float* %arrayidx153, align 4, !tbaa !15
  %203 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx154 = getelementptr inbounds float, float* %203, i32 3
  %204 = load float, float* %arrayidx154, align 4, !tbaa !15
  %add155 = fadd float %202, %204
  store float %add155, float* %z13, align 4, !tbaa !15
  %205 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds float, float* %205, i32 5
  %206 = load float, float* %arrayidx156, align 4, !tbaa !15
  %207 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx157 = getelementptr inbounds float, float* %207, i32 3
  %208 = load float, float* %arrayidx157, align 4, !tbaa !15
  %sub158 = fsub float %206, %208
  store float %sub158, float* %z10, align 4, !tbaa !15
  %209 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx159 = getelementptr inbounds float, float* %209, i32 1
  %210 = load float, float* %arrayidx159, align 4, !tbaa !15
  %211 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx160 = getelementptr inbounds float, float* %211, i32 7
  %212 = load float, float* %arrayidx160, align 4, !tbaa !15
  %add161 = fadd float %210, %212
  store float %add161, float* %z11, align 4, !tbaa !15
  %213 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx162 = getelementptr inbounds float, float* %213, i32 1
  %214 = load float, float* %arrayidx162, align 4, !tbaa !15
  %215 = load float*, float** %wsptr, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds float, float* %215, i32 7
  %216 = load float, float* %arrayidx163, align 4, !tbaa !15
  %sub164 = fsub float %214, %216
  store float %sub164, float* %z12, align 4, !tbaa !15
  %217 = load float, float* %z11, align 4, !tbaa !15
  %218 = load float, float* %z13, align 4, !tbaa !15
  %add165 = fadd float %217, %218
  store float %add165, float* %tmp7, align 4, !tbaa !15
  %219 = load float, float* %z11, align 4, !tbaa !15
  %220 = load float, float* %z13, align 4, !tbaa !15
  %sub166 = fsub float %219, %220
  %mul167 = fmul float %sub166, 0x3FF6A09E60000000
  store float %mul167, float* %tmp11, align 4, !tbaa !15
  %221 = load float, float* %z10, align 4, !tbaa !15
  %222 = load float, float* %z12, align 4, !tbaa !15
  %add168 = fadd float %221, %222
  %mul169 = fmul float %add168, 0x3FFD906BC0000000
  store float %mul169, float* %z5, align 4, !tbaa !15
  %223 = load float, float* %z5, align 4, !tbaa !15
  %224 = load float, float* %z12, align 4, !tbaa !15
  %mul170 = fmul float %224, 0x3FF1517A80000000
  %sub171 = fsub float %223, %mul170
  store float %sub171, float* %tmp10, align 4, !tbaa !15
  %225 = load float, float* %z5, align 4, !tbaa !15
  %226 = load float, float* %z10, align 4, !tbaa !15
  %mul172 = fmul float %226, 0x4004E7AEA0000000
  %sub173 = fsub float %225, %mul172
  store float %sub173, float* %tmp12, align 4, !tbaa !15
  %227 = load float, float* %tmp12, align 4, !tbaa !15
  %228 = load float, float* %tmp7, align 4, !tbaa !15
  %sub174 = fsub float %227, %228
  store float %sub174, float* %tmp6, align 4, !tbaa !15
  %229 = load float, float* %tmp11, align 4, !tbaa !15
  %230 = load float, float* %tmp6, align 4, !tbaa !15
  %sub175 = fsub float %229, %230
  store float %sub175, float* %tmp5, align 4, !tbaa !15
  %231 = load float, float* %tmp10, align 4, !tbaa !15
  %232 = load float, float* %tmp5, align 4, !tbaa !15
  %sub176 = fsub float %231, %232
  store float %sub176, float* %tmp4, align 4, !tbaa !15
  %233 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %234 = load float, float* %tmp0, align 4, !tbaa !15
  %235 = load float, float* %tmp7, align 4, !tbaa !15
  %add177 = fadd float %234, %235
  %conv178 = fptosi float %add177 to i32
  %and = and i32 %conv178, 1023
  %arrayidx179 = getelementptr inbounds i8, i8* %233, i32 %and
  %236 = load i8, i8* %arrayidx179, align 1, !tbaa !17
  %237 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx180 = getelementptr inbounds i8, i8* %237, i32 0
  store i8 %236, i8* %arrayidx180, align 1, !tbaa !17
  %238 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %239 = load float, float* %tmp0, align 4, !tbaa !15
  %240 = load float, float* %tmp7, align 4, !tbaa !15
  %sub181 = fsub float %239, %240
  %conv182 = fptosi float %sub181 to i32
  %and183 = and i32 %conv182, 1023
  %arrayidx184 = getelementptr inbounds i8, i8* %238, i32 %and183
  %241 = load i8, i8* %arrayidx184, align 1, !tbaa !17
  %242 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx185 = getelementptr inbounds i8, i8* %242, i32 7
  store i8 %241, i8* %arrayidx185, align 1, !tbaa !17
  %243 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %244 = load float, float* %tmp1, align 4, !tbaa !15
  %245 = load float, float* %tmp6, align 4, !tbaa !15
  %add186 = fadd float %244, %245
  %conv187 = fptosi float %add186 to i32
  %and188 = and i32 %conv187, 1023
  %arrayidx189 = getelementptr inbounds i8, i8* %243, i32 %and188
  %246 = load i8, i8* %arrayidx189, align 1, !tbaa !17
  %247 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx190 = getelementptr inbounds i8, i8* %247, i32 1
  store i8 %246, i8* %arrayidx190, align 1, !tbaa !17
  %248 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %249 = load float, float* %tmp1, align 4, !tbaa !15
  %250 = load float, float* %tmp6, align 4, !tbaa !15
  %sub191 = fsub float %249, %250
  %conv192 = fptosi float %sub191 to i32
  %and193 = and i32 %conv192, 1023
  %arrayidx194 = getelementptr inbounds i8, i8* %248, i32 %and193
  %251 = load i8, i8* %arrayidx194, align 1, !tbaa !17
  %252 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx195 = getelementptr inbounds i8, i8* %252, i32 6
  store i8 %251, i8* %arrayidx195, align 1, !tbaa !17
  %253 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %254 = load float, float* %tmp2, align 4, !tbaa !15
  %255 = load float, float* %tmp5, align 4, !tbaa !15
  %add196 = fadd float %254, %255
  %conv197 = fptosi float %add196 to i32
  %and198 = and i32 %conv197, 1023
  %arrayidx199 = getelementptr inbounds i8, i8* %253, i32 %and198
  %256 = load i8, i8* %arrayidx199, align 1, !tbaa !17
  %257 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx200 = getelementptr inbounds i8, i8* %257, i32 2
  store i8 %256, i8* %arrayidx200, align 1, !tbaa !17
  %258 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %259 = load float, float* %tmp2, align 4, !tbaa !15
  %260 = load float, float* %tmp5, align 4, !tbaa !15
  %sub201 = fsub float %259, %260
  %conv202 = fptosi float %sub201 to i32
  %and203 = and i32 %conv202, 1023
  %arrayidx204 = getelementptr inbounds i8, i8* %258, i32 %and203
  %261 = load i8, i8* %arrayidx204, align 1, !tbaa !17
  %262 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx205 = getelementptr inbounds i8, i8* %262, i32 5
  store i8 %261, i8* %arrayidx205, align 1, !tbaa !17
  %263 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %264 = load float, float* %tmp3, align 4, !tbaa !15
  %265 = load float, float* %tmp4, align 4, !tbaa !15
  %add206 = fadd float %264, %265
  %conv207 = fptosi float %add206 to i32
  %and208 = and i32 %conv207, 1023
  %arrayidx209 = getelementptr inbounds i8, i8* %263, i32 %and208
  %266 = load i8, i8* %arrayidx209, align 1, !tbaa !17
  %267 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx210 = getelementptr inbounds i8, i8* %267, i32 3
  store i8 %266, i8* %arrayidx210, align 1, !tbaa !17
  %268 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %269 = load float, float* %tmp3, align 4, !tbaa !15
  %270 = load float, float* %tmp4, align 4, !tbaa !15
  %sub211 = fsub float %269, %270
  %conv212 = fptosi float %sub211 to i32
  %and213 = and i32 %conv212, 1023
  %arrayidx214 = getelementptr inbounds i8, i8* %268, i32 %and213
  %271 = load i8, i8* %arrayidx214, align 1, !tbaa !17
  %272 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx215 = getelementptr inbounds i8, i8* %272, i32 4
  store i8 %271, i8* %arrayidx215, align 1, !tbaa !17
  %273 = load float*, float** %wsptr, align 4, !tbaa !2
  %add.ptr216 = getelementptr inbounds float, float* %273, i32 8
  store float* %add.ptr216, float** %wsptr, align 4, !tbaa !2
  br label %for.inc217

for.inc217:                                       ; preds = %for.body133
  %274 = load i32, i32* %ctr, align 4, !tbaa !6
  %inc = add nsw i32 %274, 1
  store i32 %inc, i32* %ctr, align 4, !tbaa !6
  br label %for.cond130

for.end218:                                       ; preds = %for.cond130
  %275 = bitcast [64 x float]* %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 256, i8* %275) #2
  %276 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #2
  %277 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #2
  %278 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #2
  %279 = bitcast float** %wsptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #2
  %280 = bitcast float** %quantptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #2
  %281 = bitcast i16** %inptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #2
  %282 = bitcast float* %z13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #2
  %283 = bitcast float* %z12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %283) #2
  %284 = bitcast float* %z11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %284) #2
  %285 = bitcast float* %z10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %285) #2
  %286 = bitcast float* %z5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %286) #2
  %287 = bitcast float* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %287) #2
  %288 = bitcast float* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %288) #2
  %289 = bitcast float* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %289) #2
  %290 = bitcast float* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %290) #2
  %291 = bitcast float* %tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #2
  %292 = bitcast float* %tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %292) #2
  %293 = bitcast float* %tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %293) #2
  %294 = bitcast float* %tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %294) #2
  %295 = bitcast float* %tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %295) #2
  %296 = bitcast float* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %296) #2
  %297 = bitcast float* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %297) #2
  %298 = bitcast float* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 324}
!9 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !10, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !11, i64 288, !11, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 80}
!13 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!14 = !{!11, !11, i64 0}
!15 = !{!16, !16, i64 0}
!16 = !{!"float", !4, i64 0}
!17 = !{!4, !4, i64 0}
