; ModuleID = 'jdmerge.c'
source_filename = "jdmerge.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.my_upsampler = type { %struct.jpeg_upsampler, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)*, i32*, i32*, i32*, i32*, i8*, i32, i32, i32 }

@dither_matrix = internal constant [4 x i32] [i32 524810, i32 201592326, i32 51052809, i32 252120325], align 16

; Function Attrs: nounwind
define hidden void @jinit_merged_upsampler(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %2, i32 0, i32 0
  %3 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_decompress_struct* %4 to %struct.jpeg_common_struct*
  %call = call i8* %3(%struct.jpeg_common_struct* %5, i32 1, i32 48)
  %6 = bitcast i8* %call to %struct.my_upsampler*
  store %struct.my_upsampler* %6, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %7 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %8 = bitcast %struct.my_upsampler* %7 to %struct.jpeg_upsampler*
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 85
  store %struct.jpeg_upsampler* %8, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %10 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %10, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub, i32 0, i32 0
  %start_pass2 = bitcast {}** %start_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @start_pass_merged_upsample, void (%struct.jpeg_decompress_struct*)** %start_pass2, align 4, !tbaa !15
  %11 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub3 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %11, i32 0, i32 0
  %need_context_rows = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub3, i32 0, i32 2
  store i32 0, i32* %need_context_rows, align 4, !tbaa !18
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 27
  %13 = load i32, i32* %output_width, align 8, !tbaa !19
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 29
  %15 = load i32, i32* %out_color_components, align 8, !tbaa !20
  %mul = mul i32 %13, %15
  %16 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %out_row_width = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %16, i32 0, i32 8
  store i32 %mul, i32* %out_row_width, align 4, !tbaa !21
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 62
  %18 = load i32, i32* %max_v_samp_factor, align 8, !tbaa !22
  %cmp = icmp eq i32 %18, 2
  br i1 %cmp, label %if.then, label %if.else22

if.then:                                          ; preds = %entry
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub4 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 0
  %upsample5 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub4, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* @merged_2v_upsample, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)** %upsample5, align 4, !tbaa !23
  %call6 = call i32 @jsimd_can_h2v2_merged_upsample()
  %tobool = icmp ne i32 %call6, 0
  br i1 %tobool, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then
  %20 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %20, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)* @jsimd_h2v2_merged_upsample, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod, align 4, !tbaa !24
  br label %if.end

if.else:                                          ; preds = %if.then
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod8 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)* @h2v2_merged_upsample, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod8, align 4, !tbaa !24
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then7
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 11
  %23 = load i32, i32* %out_color_space, align 4, !tbaa !25
  %cmp9 = icmp eq i32 %23, 16
  br i1 %cmp9, label %if.then10, label %if.end17

if.then10:                                        ; preds = %if.end
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 21
  %25 = load i32, i32* %dither_mode, align 8, !tbaa !26
  %cmp11 = icmp ne i32 %25, 0
  br i1 %cmp11, label %if.then12, label %if.else14

if.then12:                                        ; preds = %if.then10
  %26 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod13 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %26, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)* @h2v2_merged_upsample_565D, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod13, align 4, !tbaa !24
  br label %if.end16

if.else14:                                        ; preds = %if.then10
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod15 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)* @h2v2_merged_upsample_565, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod15, align 4, !tbaa !24
  br label %if.end16

if.end16:                                         ; preds = %if.else14, %if.then12
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.end
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 1
  %29 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem18, align 4, !tbaa !6
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %29, i32 0, i32 1
  %30 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !27
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %32 = bitcast %struct.jpeg_decompress_struct* %31 to %struct.jpeg_common_struct*
  %33 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %out_row_width19 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %33, i32 0, i32 8
  %34 = load i32, i32* %out_row_width19, align 4, !tbaa !21
  %mul20 = mul i32 %34, 1
  %call21 = call i8* %30(%struct.jpeg_common_struct* %32, i32 1, i32 %mul20)
  %35 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_row = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %35, i32 0, i32 6
  store i8* %call21, i8** %spare_row, align 4, !tbaa !28
  br label %if.end44

if.else22:                                        ; preds = %entry
  %36 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %pub23 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %36, i32 0, i32 0
  %upsample24 = getelementptr inbounds %struct.jpeg_upsampler, %struct.jpeg_upsampler* %pub23, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* @merged_1v_upsample, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)** %upsample24, align 4, !tbaa !23
  %call25 = call i32 @jsimd_can_h2v1_merged_upsample()
  %tobool26 = icmp ne i32 %call25, 0
  br i1 %tobool26, label %if.then27, label %if.else29

if.then27:                                        ; preds = %if.else22
  %37 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod28 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %37, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)* @jsimd_h2v1_merged_upsample, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod28, align 4, !tbaa !24
  br label %if.end31

if.else29:                                        ; preds = %if.else22
  %38 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod30 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %38, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)* @h2v1_merged_upsample, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod30, align 4, !tbaa !24
  br label %if.end31

if.end31:                                         ; preds = %if.else29, %if.then27
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space32 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 11
  %40 = load i32, i32* %out_color_space32, align 4, !tbaa !25
  %cmp33 = icmp eq i32 %40, 16
  br i1 %cmp33, label %if.then34, label %if.end42

if.then34:                                        ; preds = %if.end31
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode35 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %41, i32 0, i32 21
  %42 = load i32, i32* %dither_mode35, align 8, !tbaa !26
  %cmp36 = icmp ne i32 %42, 0
  br i1 %cmp36, label %if.then37, label %if.else39

if.then37:                                        ; preds = %if.then34
  %43 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod38 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %43, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)* @h2v1_merged_upsample_565D, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod38, align 4, !tbaa !24
  br label %if.end41

if.else39:                                        ; preds = %if.then34
  %44 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod40 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %44, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)* @h2v1_merged_upsample_565, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod40, align 4, !tbaa !24
  br label %if.end41

if.end41:                                         ; preds = %if.else39, %if.then37
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.end31
  %45 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_row43 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %45, i32 0, i32 6
  store i8* null, i8** %spare_row43, align 4, !tbaa !28
  br label %if.end44

if.end44:                                         ; preds = %if.end42, %if.end17
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @build_ycc_rgb_table(%struct.jpeg_decompress_struct* %46)
  %47 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_merged_upsample(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_full = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %4, i32 0, i32 7
  store i32 0, i32* %spare_full, align 4, !tbaa !29
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_height = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 28
  %6 = load i32, i32* %output_height, align 4, !tbaa !30
  %7 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %7, i32 0, i32 9
  store i32 %6, i32* %rows_to_go, align 4, !tbaa !31
  %8 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #4
  ret void
}

; Function Attrs: nounwind
define internal void @merged_2v_upsample(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32* %in_row_group_ctr, i32 %in_row_groups_avail, i8** %output_buf, i32* %out_row_ctr, i32 %out_rows_avail) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32*, align 4
  %in_row_groups_avail.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %out_row_ctr.addr = alloca i32*, align 4
  %out_rows_avail.addr = alloca i32, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %work_ptrs = alloca [2 x i8*], align 4
  %num_rows = alloca i32, align 4
  %size = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32* %in_row_group_ctr, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  store i32 %in_row_groups_avail, i32* %in_row_groups_avail.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32* %out_row_ctr, i32** %out_row_ctr.addr, align 4, !tbaa !2
  store i32 %out_rows_avail, i32* %out_rows_avail.addr, align 4, !tbaa !32
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast [2 x i8*]* %work_ptrs to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #4
  %5 = bitcast i32* %num_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_full = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %6, i32 0, i32 7
  %7 = load i32, i32* %spare_full, align 4, !tbaa !29
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %8 = bitcast i32* %size to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %out_row_width = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %9, i32 0, i32 8
  %10 = load i32, i32* %out_row_width, align 4, !tbaa !21
  store i32 %10, i32* %size, align 4, !tbaa !32
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 11
  %12 = load i32, i32* %out_color_space, align 4, !tbaa !25
  %cmp = icmp eq i32 %12, 16
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 27
  %14 = load i32, i32* %output_width, align 8, !tbaa !19
  %mul = mul i32 %14, 2
  store i32 %mul, i32* %size, align 4, !tbaa !32
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %15 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_row = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %15, i32 0, i32 6
  %16 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %17 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %18 = load i32, i32* %17, align 4, !tbaa !32
  %add.ptr = getelementptr inbounds i8*, i8** %16, i32 %18
  %19 = load i32, i32* %size, align 4, !tbaa !32
  call void @jcopy_sample_rows(i8** %spare_row, i32 0, i8** %add.ptr, i32 0, i32 1, i32 %19)
  store i32 1, i32* %num_rows, align 4, !tbaa !32
  %20 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_full3 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %20, i32 0, i32 7
  store i32 0, i32* %spare_full3, align 4, !tbaa !29
  %21 = bitcast i32* %size to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #4
  br label %if.end21

if.else:                                          ; preds = %entry
  store i32 2, i32* %num_rows, align 4, !tbaa !32
  %22 = load i32, i32* %num_rows, align 4, !tbaa !32
  %23 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %23, i32 0, i32 9
  %24 = load i32, i32* %rows_to_go, align 4, !tbaa !31
  %cmp4 = icmp ugt i32 %22, %24
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.else
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go6 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 9
  %26 = load i32, i32* %rows_to_go6, align 4, !tbaa !31
  store i32 %26, i32* %num_rows, align 4, !tbaa !32
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.else
  %27 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %28 = load i32, i32* %27, align 4, !tbaa !32
  %29 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !32
  %sub = sub i32 %29, %28
  store i32 %sub, i32* %out_rows_avail.addr, align 4, !tbaa !32
  %30 = load i32, i32* %num_rows, align 4, !tbaa !32
  %31 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !32
  %cmp8 = icmp ugt i32 %30, %31
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end7
  %32 = load i32, i32* %out_rows_avail.addr, align 4, !tbaa !32
  store i32 %32, i32* %num_rows, align 4, !tbaa !32
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %if.end7
  %33 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %34 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %35 = load i32, i32* %34, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds i8*, i8** %33, i32 %35
  %36 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds [2 x i8*], [2 x i8*]* %work_ptrs, i32 0, i32 0
  store i8* %36, i8** %arrayidx11, align 4, !tbaa !2
  %37 = load i32, i32* %num_rows, align 4, !tbaa !32
  %cmp12 = icmp ugt i32 %37, 1
  br i1 %cmp12, label %if.then13, label %if.else16

if.then13:                                        ; preds = %if.end10
  %38 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %39 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %40 = load i32, i32* %39, align 4, !tbaa !32
  %add = add i32 %40, 1
  %arrayidx14 = getelementptr inbounds i8*, i8** %38, i32 %add
  %41 = load i8*, i8** %arrayidx14, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds [2 x i8*], [2 x i8*]* %work_ptrs, i32 0, i32 1
  store i8* %41, i8** %arrayidx15, align 4, !tbaa !2
  br label %if.end20

if.else16:                                        ; preds = %if.end10
  %42 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_row17 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %42, i32 0, i32 6
  %43 = load i8*, i8** %spare_row17, align 4, !tbaa !28
  %arrayidx18 = getelementptr inbounds [2 x i8*], [2 x i8*]* %work_ptrs, i32 0, i32 1
  store i8* %43, i8** %arrayidx18, align 4, !tbaa !2
  %44 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_full19 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %44, i32 0, i32 7
  store i32 1, i32* %spare_full19, align 4, !tbaa !29
  br label %if.end20

if.end20:                                         ; preds = %if.else16, %if.then13
  %45 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %45, i32 0, i32 1
  %46 = load void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod, align 4, !tbaa !24
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %48 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %49 = load i32*, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  %50 = load i32, i32* %49, align 4, !tbaa !32
  %arraydecay = getelementptr inbounds [2 x i8*], [2 x i8*]* %work_ptrs, i32 0, i32 0
  call void %46(%struct.jpeg_decompress_struct* %47, i8*** %48, i32 %50, i8** %arraydecay)
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.end
  %51 = load i32, i32* %num_rows, align 4, !tbaa !32
  %52 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %53 = load i32, i32* %52, align 4, !tbaa !32
  %add22 = add i32 %53, %51
  store i32 %add22, i32* %52, align 4, !tbaa !32
  %54 = load i32, i32* %num_rows, align 4, !tbaa !32
  %55 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %rows_to_go23 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %55, i32 0, i32 9
  %56 = load i32, i32* %rows_to_go23, align 4, !tbaa !31
  %sub24 = sub i32 %56, %54
  store i32 %sub24, i32* %rows_to_go23, align 4, !tbaa !31
  %57 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %spare_full25 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %57, i32 0, i32 7
  %58 = load i32, i32* %spare_full25, align 4, !tbaa !29
  %tobool26 = icmp ne i32 %58, 0
  br i1 %tobool26, label %if.end28, label %if.then27

if.then27:                                        ; preds = %if.end21
  %59 = load i32*, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  %60 = load i32, i32* %59, align 4, !tbaa !32
  %inc = add i32 %60, 1
  store i32 %inc, i32* %59, align 4, !tbaa !32
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.end21
  %61 = bitcast i32* %num_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #4
  %62 = bitcast [2 x i8*]* %work_ptrs to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %62) #4
  %63 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #4
  ret void
}

declare i32 @jsimd_can_h2v2_merged_upsample() #2

declare void @jsimd_h2v2_merged_upsample(%struct.jpeg_decompress_struct*, i8***, i32, i8**) #2

; Function Attrs: nounwind
define internal void @h2v2_merged_upsample(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 11
  %1 = load i32, i32* %out_color_space, align 4, !tbaa !25
  switch i32 %1, label %sw.default [
    i32 6, label %sw.bb
    i32 7, label %sw.bb1
    i32 12, label %sw.bb1
    i32 8, label %sw.bb2
    i32 9, label %sw.bb3
    i32 13, label %sw.bb3
    i32 10, label %sw.bb4
    i32 14, label %sw.bb4
    i32 11, label %sw.bb5
    i32 15, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %5 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extrgb_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %2, i8*** %3, i32 %4, i8** %5)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %8 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %9 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extrgbx_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %6, i8*** %7, i32 %8, i8** %9)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %12 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %13 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extbgr_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %10, i8*** %11, i32 %12, i8** %13)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %16 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %17 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extbgrx_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %14, i8*** %15, i32 %16, i8** %17)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %20 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %21 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extxbgr_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %18, i8*** %19, i32 %20, i8** %21)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %24 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %25 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extxrgb_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %22, i8*** %23, i32 %24, i8** %25)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %28 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %29 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %26, i8*** %27, i32 %28, i8** %29)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @h2v2_merged_upsample_565D(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v2_merged_upsample_565D_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %7 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v2_merged_upsample_565D_le(%struct.jpeg_decompress_struct* %4, i8*** %5, i32 %6, i8** %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @h2v2_merged_upsample_565(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v2_merged_upsample_565_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %7 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v2_merged_upsample_565_le(%struct.jpeg_decompress_struct* %4, i8*** %5, i32 %6, i8** %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @merged_1v_upsample(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32* %in_row_group_ctr, i32 %in_row_groups_avail, i8** %output_buf, i32* %out_row_ctr, i32 %out_rows_avail) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32*, align 4
  %in_row_groups_avail.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %out_row_ctr.addr = alloca i32*, align 4
  %out_rows_avail.addr = alloca i32, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32* %in_row_group_ctr, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  store i32 %in_row_groups_avail, i32* %in_row_groups_avail.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32* %out_row_ctr, i32** %out_row_ctr.addr, align 4, !tbaa !2
  store i32 %out_rows_avail, i32* %out_rows_avail.addr, align 4, !tbaa !32
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %upmethod = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %4, i32 0, i32 1
  %5 = load void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**)** %upmethod, align 4, !tbaa !24
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %8 = load i32*, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  %9 = load i32, i32* %8, align 4, !tbaa !32
  %10 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %11 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %12 = load i32, i32* %11, align 4, !tbaa !32
  %add.ptr = getelementptr inbounds i8*, i8** %10, i32 %12
  call void %5(%struct.jpeg_decompress_struct* %6, i8*** %7, i32 %9, i8** %add.ptr)
  %13 = load i32*, i32** %out_row_ctr.addr, align 4, !tbaa !2
  %14 = load i32, i32* %13, align 4, !tbaa !32
  %inc = add i32 %14, 1
  store i32 %inc, i32* %13, align 4, !tbaa !32
  %15 = load i32*, i32** %in_row_group_ctr.addr, align 4, !tbaa !2
  %16 = load i32, i32* %15, align 4, !tbaa !32
  %inc2 = add i32 %16, 1
  store i32 %inc2, i32* %15, align 4, !tbaa !32
  %17 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #4
  ret void
}

declare i32 @jsimd_can_h2v1_merged_upsample() #2

declare void @jsimd_h2v1_merged_upsample(%struct.jpeg_decompress_struct*, i8***, i32, i8**) #2

; Function Attrs: nounwind
define internal void @h2v1_merged_upsample(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 11
  %1 = load i32, i32* %out_color_space, align 4, !tbaa !25
  switch i32 %1, label %sw.default [
    i32 6, label %sw.bb
    i32 7, label %sw.bb1
    i32 12, label %sw.bb1
    i32 8, label %sw.bb2
    i32 9, label %sw.bb3
    i32 13, label %sw.bb3
    i32 10, label %sw.bb4
    i32 14, label %sw.bb4
    i32 11, label %sw.bb5
    i32 15, label %sw.bb5
  ]

sw.bb:                                            ; preds = %entry
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %3 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %4 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %5 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extrgb_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %2, i8*** %3, i32 %4, i8** %5)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %8 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %9 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extrgbx_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %6, i8*** %7, i32 %8, i8** %9)
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %12 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %13 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extbgr_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %10, i8*** %11, i32 %12, i8** %13)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %16 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %17 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extbgrx_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %14, i8*** %15, i32 %16, i8** %17)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry, %entry
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %19 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %20 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %21 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extxbgr_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %18, i8*** %19, i32 %20, i8** %21)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %23 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %24 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %25 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @extxrgb_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %22, i8*** %23, i32 %24, i8** %25)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %27 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %28 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %29 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %26, i8*** %27, i32 %28, i8** %29)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  ret void
}

; Function Attrs: nounwind
define internal void @h2v1_merged_upsample_565D(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v1_merged_upsample_565D_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %7 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v1_merged_upsample_565D_le(%struct.jpeg_decompress_struct* %4, i8*** %5, i32 %6, i8** %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @h2v1_merged_upsample_565(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %call = call i32 @is_big_endian()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %1 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %2 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %3 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v1_merged_upsample_565_be(%struct.jpeg_decompress_struct* %0, i8*** %1, i32 %2, i8** %3)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %6 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %7 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  call void @h2v1_merged_upsample_565_le(%struct.jpeg_decompress_struct* %4, i8*** %5, i32 %6, i8** %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
define internal void @build_ycc_rgb_table(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 1
  %7 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %7, i32 0, i32 0
  %8 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %10 = bitcast %struct.jpeg_decompress_struct* %9 to %struct.jpeg_common_struct*
  %call = call i8* %8(%struct.jpeg_common_struct* %10, i32 1, i32 1024)
  %11 = bitcast i8* %call to i32*
  %12 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %12, i32 0, i32 2
  store i32* %11, i32** %Cr_r_tab, align 4, !tbaa !33
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 1
  %14 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem2, align 4, !tbaa !6
  %alloc_small3 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %14, i32 0, i32 0
  %15 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small3, align 4, !tbaa !11
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %17 = bitcast %struct.jpeg_decompress_struct* %16 to %struct.jpeg_common_struct*
  %call4 = call i8* %15(%struct.jpeg_common_struct* %17, i32 1, i32 1024)
  %18 = bitcast i8* %call4 to i32*
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 3
  store i32* %18, i32** %Cb_b_tab, align 4, !tbaa !34
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 1
  %21 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem5, align 4, !tbaa !6
  %alloc_small6 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %21, i32 0, i32 0
  %22 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small6, align 4, !tbaa !11
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_decompress_struct* %23 to %struct.jpeg_common_struct*
  %call7 = call i8* %22(%struct.jpeg_common_struct* %24, i32 1, i32 1024)
  %25 = bitcast i8* %call7 to i32*
  %26 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %26, i32 0, i32 4
  store i32* %25, i32** %Cr_g_tab, align 4, !tbaa !35
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem8 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 1
  %28 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem8, align 4, !tbaa !6
  %alloc_small9 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %28, i32 0, i32 0
  %29 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small9, align 4, !tbaa !11
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_decompress_struct* %30 to %struct.jpeg_common_struct*
  %call10 = call i8* %29(%struct.jpeg_common_struct* %31, i32 1, i32 1024)
  %32 = bitcast i8* %call10 to i32*
  %33 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %33, i32 0, i32 5
  store i32* %32, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32 0, i32* %i, align 4, !tbaa !32
  store i32 -128, i32* %x, align 4, !tbaa !37
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %34 = load i32, i32* %i, align 4, !tbaa !32
  %cmp = icmp sle i32 %34, 255
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %35 = load i32, i32* %x, align 4, !tbaa !37
  %mul = mul nsw i32 91881, %35
  %add = add nsw i32 %mul, 32768
  %shr = ashr i32 %add, 16
  %36 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab11 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %36, i32 0, i32 2
  %37 = load i32*, i32** %Cr_r_tab11, align 4, !tbaa !33
  %38 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds i32, i32* %37, i32 %38
  store i32 %shr, i32* %arrayidx, align 4, !tbaa !32
  %39 = load i32, i32* %x, align 4, !tbaa !37
  %mul12 = mul nsw i32 116130, %39
  %add13 = add nsw i32 %mul12, 32768
  %shr14 = ashr i32 %add13, 16
  %40 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab15 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %40, i32 0, i32 3
  %41 = load i32*, i32** %Cb_b_tab15, align 4, !tbaa !34
  %42 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %41, i32 %42
  store i32 %shr14, i32* %arrayidx16, align 4, !tbaa !32
  %43 = load i32, i32* %x, align 4, !tbaa !37
  %mul17 = mul nsw i32 -46802, %43
  %44 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab18 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %44, i32 0, i32 4
  %45 = load i32*, i32** %Cr_g_tab18, align 4, !tbaa !35
  %46 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %45, i32 %46
  store i32 %mul17, i32* %arrayidx19, align 4, !tbaa !37
  %47 = load i32, i32* %x, align 4, !tbaa !37
  %mul20 = mul nsw i32 -22554, %47
  %add21 = add nsw i32 %mul20, 32768
  %48 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab22 = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %48, i32 0, i32 5
  %49 = load i32*, i32** %Cb_g_tab22, align 4, !tbaa !36
  %50 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx23 = getelementptr inbounds i32, i32* %49, i32 %50
  store i32 %add21, i32* %arrayidx23, align 4, !tbaa !37
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %51 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  %52 = load i32, i32* %x, align 4, !tbaa !37
  %inc24 = add nsw i32 %52, 1
  store i32 %inc24, i32* %x, align 4, !tbaa !37
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %53 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #4
  %54 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #4
  %55 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @jcopy_sample_rows(i8**, i32, i8**, i32, i32, i32) #2

; Function Attrs: alwaysinline nounwind
define internal void @extrgb_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %32, i32 0
  %33 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %34, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %33, i32 %mul
  %35 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %35, i8** %inptr00, align 4, !tbaa !2
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %38, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %37, i32 %add
  %39 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %39, i8** %inptr01, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %40, i32 1
  %41 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %44, i32 2
  %45 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %47, i8** %inptr2, align 4, !tbaa !2
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %48, i32 0
  %49 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %49, i8** %outptr0, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %50, i32 1
  %51 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %51, i8** %outptr1, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv13 = zext i8 %58 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %64, %67
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv21 = zext i8 %72 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %74, %75
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %76 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %77 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 0
  store i8 %76, i8* %arrayidx24, align 1, !tbaa !39
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %79, %80
  %arrayidx26 = getelementptr inbounds i8, i8* %78, i32 %add25
  %81 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %82 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8 %81, i8* %arrayidx27, align 1, !tbaa !39
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 2
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %88, i32 3
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %89 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr31 = getelementptr inbounds i8, i8* %89, i32 1
  store i8* %incdec.ptr31, i8** %inptr00, align 4, !tbaa !2
  %90 = load i8, i8* %89, align 1, !tbaa !39
  %conv32 = zext i8 %90 to i32
  store i32 %conv32, i32* %y, align 4, !tbaa !32
  %91 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %92 = load i32, i32* %y, align 4, !tbaa !32
  %93 = load i32, i32* %cred, align 4, !tbaa !32
  %add33 = add nsw i32 %92, %93
  %arrayidx34 = getelementptr inbounds i8, i8* %91, i32 %add33
  %94 = load i8, i8* %arrayidx34, align 1, !tbaa !39
  %95 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i8, i8* %95, i32 0
  store i8 %94, i8* %arrayidx35, align 1, !tbaa !39
  %96 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %97 = load i32, i32* %y, align 4, !tbaa !32
  %98 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add36 = add nsw i32 %97, %98
  %arrayidx37 = getelementptr inbounds i8, i8* %96, i32 %add36
  %99 = load i8, i8* %arrayidx37, align 1, !tbaa !39
  %100 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i8, i8* %100, i32 1
  store i8 %99, i8* %arrayidx38, align 1, !tbaa !39
  %101 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %102 = load i32, i32* %y, align 4, !tbaa !32
  %103 = load i32, i32* %cblue, align 4, !tbaa !32
  %add39 = add nsw i32 %102, %103
  %arrayidx40 = getelementptr inbounds i8, i8* %101, i32 %add39
  %104 = load i8, i8* %arrayidx40, align 1, !tbaa !39
  %105 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i8, i8* %105, i32 2
  store i8 %104, i8* %arrayidx41, align 1, !tbaa !39
  %106 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr42 = getelementptr inbounds i8, i8* %106, i32 3
  store i8* %add.ptr42, i8** %outptr0, align 4, !tbaa !2
  %107 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr43 = getelementptr inbounds i8, i8* %107, i32 1
  store i8* %incdec.ptr43, i8** %inptr01, align 4, !tbaa !2
  %108 = load i8, i8* %107, align 1, !tbaa !39
  %conv44 = zext i8 %108 to i32
  store i32 %conv44, i32* %y, align 4, !tbaa !32
  %109 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %110 = load i32, i32* %y, align 4, !tbaa !32
  %111 = load i32, i32* %cred, align 4, !tbaa !32
  %add45 = add nsw i32 %110, %111
  %arrayidx46 = getelementptr inbounds i8, i8* %109, i32 %add45
  %112 = load i8, i8* %arrayidx46, align 1, !tbaa !39
  %113 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i8, i8* %113, i32 0
  store i8 %112, i8* %arrayidx47, align 1, !tbaa !39
  %114 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %115 = load i32, i32* %y, align 4, !tbaa !32
  %116 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add48 = add nsw i32 %115, %116
  %arrayidx49 = getelementptr inbounds i8, i8* %114, i32 %add48
  %117 = load i8, i8* %arrayidx49, align 1, !tbaa !39
  %118 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i8, i8* %118, i32 1
  store i8 %117, i8* %arrayidx50, align 1, !tbaa !39
  %119 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %120 = load i32, i32* %y, align 4, !tbaa !32
  %121 = load i32, i32* %cblue, align 4, !tbaa !32
  %add51 = add nsw i32 %120, %121
  %arrayidx52 = getelementptr inbounds i8, i8* %119, i32 %add51
  %122 = load i8, i8* %arrayidx52, align 1, !tbaa !39
  %123 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i8, i8* %123, i32 2
  store i8 %122, i8* %arrayidx53, align 1, !tbaa !39
  %124 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr54 = getelementptr inbounds i8, i8* %124, i32 3
  store i8* %add.ptr54, i8** %outptr1, align 4, !tbaa !2
  %125 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr55 = getelementptr inbounds i8, i8* %125, i32 1
  store i8* %incdec.ptr55, i8** %inptr01, align 4, !tbaa !2
  %126 = load i8, i8* %125, align 1, !tbaa !39
  %conv56 = zext i8 %126 to i32
  store i32 %conv56, i32* %y, align 4, !tbaa !32
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !32
  %129 = load i32, i32* %cred, align 4, !tbaa !32
  %add57 = add nsw i32 %128, %129
  %arrayidx58 = getelementptr inbounds i8, i8* %127, i32 %add57
  %130 = load i8, i8* %arrayidx58, align 1, !tbaa !39
  %131 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i8, i8* %131, i32 0
  store i8 %130, i8* %arrayidx59, align 1, !tbaa !39
  %132 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %133 = load i32, i32* %y, align 4, !tbaa !32
  %134 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add60 = add nsw i32 %133, %134
  %arrayidx61 = getelementptr inbounds i8, i8* %132, i32 %add60
  %135 = load i8, i8* %arrayidx61, align 1, !tbaa !39
  %136 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i8, i8* %136, i32 1
  store i8 %135, i8* %arrayidx62, align 1, !tbaa !39
  %137 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %138 = load i32, i32* %y, align 4, !tbaa !32
  %139 = load i32, i32* %cblue, align 4, !tbaa !32
  %add63 = add nsw i32 %138, %139
  %arrayidx64 = getelementptr inbounds i8, i8* %137, i32 %add63
  %140 = load i8, i8* %arrayidx64, align 1, !tbaa !39
  %141 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %141, i32 2
  store i8 %140, i8* %arrayidx65, align 1, !tbaa !39
  %142 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr66 = getelementptr inbounds i8, i8* %142, i32 3
  store i8* %add.ptr66, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %143 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %143, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 27
  %145 = load i32, i32* %output_width67, align 8, !tbaa !19
  %and = and i32 %145, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %146 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %147 = load i8, i8* %146, align 1, !tbaa !39
  %conv68 = zext i8 %147 to i32
  store i32 %conv68, i32* %cb, align 4, !tbaa !32
  %148 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %149 = load i8, i8* %148, align 1, !tbaa !39
  %conv69 = zext i8 %149 to i32
  store i32 %conv69, i32* %cr, align 4, !tbaa !32
  %150 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %151 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx70 = getelementptr inbounds i32, i32* %150, i32 %151
  %152 = load i32, i32* %arrayidx70, align 4, !tbaa !32
  store i32 %152, i32* %cred, align 4, !tbaa !32
  %153 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %154 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx71 = getelementptr inbounds i32, i32* %153, i32 %154
  %155 = load i32, i32* %arrayidx71, align 4, !tbaa !37
  %156 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %157 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx72 = getelementptr inbounds i32, i32* %156, i32 %157
  %158 = load i32, i32* %arrayidx72, align 4, !tbaa !37
  %add73 = add nsw i32 %155, %158
  %shr74 = ashr i32 %add73, 16
  store i32 %shr74, i32* %cgreen, align 4, !tbaa !32
  %159 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %160 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx75 = getelementptr inbounds i32, i32* %159, i32 %160
  %161 = load i32, i32* %arrayidx75, align 4, !tbaa !32
  store i32 %161, i32* %cblue, align 4, !tbaa !32
  %162 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %163 = load i8, i8* %162, align 1, !tbaa !39
  %conv76 = zext i8 %163 to i32
  store i32 %conv76, i32* %y, align 4, !tbaa !32
  %164 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %165 = load i32, i32* %y, align 4, !tbaa !32
  %166 = load i32, i32* %cred, align 4, !tbaa !32
  %add77 = add nsw i32 %165, %166
  %arrayidx78 = getelementptr inbounds i8, i8* %164, i32 %add77
  %167 = load i8, i8* %arrayidx78, align 1, !tbaa !39
  %168 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i8, i8* %168, i32 0
  store i8 %167, i8* %arrayidx79, align 1, !tbaa !39
  %169 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %170 = load i32, i32* %y, align 4, !tbaa !32
  %171 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add80 = add nsw i32 %170, %171
  %arrayidx81 = getelementptr inbounds i8, i8* %169, i32 %add80
  %172 = load i8, i8* %arrayidx81, align 1, !tbaa !39
  %173 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i8, i8* %173, i32 1
  store i8 %172, i8* %arrayidx82, align 1, !tbaa !39
  %174 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %175 = load i32, i32* %y, align 4, !tbaa !32
  %176 = load i32, i32* %cblue, align 4, !tbaa !32
  %add83 = add nsw i32 %175, %176
  %arrayidx84 = getelementptr inbounds i8, i8* %174, i32 %add83
  %177 = load i8, i8* %arrayidx84, align 1, !tbaa !39
  %178 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i8, i8* %178, i32 2
  store i8 %177, i8* %arrayidx85, align 1, !tbaa !39
  %179 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %180 = load i8, i8* %179, align 1, !tbaa !39
  %conv86 = zext i8 %180 to i32
  store i32 %conv86, i32* %y, align 4, !tbaa !32
  %181 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %182 = load i32, i32* %y, align 4, !tbaa !32
  %183 = load i32, i32* %cred, align 4, !tbaa !32
  %add87 = add nsw i32 %182, %183
  %arrayidx88 = getelementptr inbounds i8, i8* %181, i32 %add87
  %184 = load i8, i8* %arrayidx88, align 1, !tbaa !39
  %185 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i8, i8* %185, i32 0
  store i8 %184, i8* %arrayidx89, align 1, !tbaa !39
  %186 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %187 = load i32, i32* %y, align 4, !tbaa !32
  %188 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add90 = add nsw i32 %187, %188
  %arrayidx91 = getelementptr inbounds i8, i8* %186, i32 %add90
  %189 = load i8, i8* %arrayidx91, align 1, !tbaa !39
  %190 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i8, i8* %190, i32 1
  store i8 %189, i8* %arrayidx92, align 1, !tbaa !39
  %191 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %192 = load i32, i32* %y, align 4, !tbaa !32
  %193 = load i32, i32* %cblue, align 4, !tbaa !32
  %add93 = add nsw i32 %192, %193
  %arrayidx94 = getelementptr inbounds i8, i8* %191, i32 %add93
  %194 = load i8, i8* %arrayidx94, align 1, !tbaa !39
  %195 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i8, i8* %195, i32 2
  store i8 %194, i8* %arrayidx95, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %196 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  %197 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #4
  %198 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  %199 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #4
  %200 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #4
  %201 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #4
  %202 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  %213 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extrgbx_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %32, i32 0
  %33 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %34, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %33, i32 %mul
  %35 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %35, i8** %inptr00, align 4, !tbaa !2
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %38, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %37, i32 %add
  %39 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %39, i8** %inptr01, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %40, i32 1
  %41 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %44, i32 2
  %45 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %47, i8** %inptr2, align 4, !tbaa !2
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %48, i32 0
  %49 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %49, i8** %outptr0, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %50, i32 1
  %51 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %51, i8** %outptr1, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv13 = zext i8 %58 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %64, %67
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv21 = zext i8 %72 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %74, %75
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %76 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %77 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 0
  store i8 %76, i8* %arrayidx24, align 1, !tbaa !39
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %79, %80
  %arrayidx26 = getelementptr inbounds i8, i8* %78, i32 %add25
  %81 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %82 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8 %81, i8* %arrayidx27, align 1, !tbaa !39
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 2
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %88, i32 3
  store i8 -1, i8* %arrayidx31, align 1, !tbaa !39
  %89 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %89, i32 4
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %90 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds i8, i8* %90, i32 1
  store i8* %incdec.ptr32, i8** %inptr00, align 4, !tbaa !2
  %91 = load i8, i8* %90, align 1, !tbaa !39
  %conv33 = zext i8 %91 to i32
  store i32 %conv33, i32* %y, align 4, !tbaa !32
  %92 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %93 = load i32, i32* %y, align 4, !tbaa !32
  %94 = load i32, i32* %cred, align 4, !tbaa !32
  %add34 = add nsw i32 %93, %94
  %arrayidx35 = getelementptr inbounds i8, i8* %92, i32 %add34
  %95 = load i8, i8* %arrayidx35, align 1, !tbaa !39
  %96 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %96, i32 0
  store i8 %95, i8* %arrayidx36, align 1, !tbaa !39
  %97 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %98 = load i32, i32* %y, align 4, !tbaa !32
  %99 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add37 = add nsw i32 %98, %99
  %arrayidx38 = getelementptr inbounds i8, i8* %97, i32 %add37
  %100 = load i8, i8* %arrayidx38, align 1, !tbaa !39
  %101 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i8, i8* %101, i32 1
  store i8 %100, i8* %arrayidx39, align 1, !tbaa !39
  %102 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %103 = load i32, i32* %y, align 4, !tbaa !32
  %104 = load i32, i32* %cblue, align 4, !tbaa !32
  %add40 = add nsw i32 %103, %104
  %arrayidx41 = getelementptr inbounds i8, i8* %102, i32 %add40
  %105 = load i8, i8* %arrayidx41, align 1, !tbaa !39
  %106 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i8, i8* %106, i32 2
  store i8 %105, i8* %arrayidx42, align 1, !tbaa !39
  %107 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i8, i8* %107, i32 3
  store i8 -1, i8* %arrayidx43, align 1, !tbaa !39
  %108 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr44 = getelementptr inbounds i8, i8* %108, i32 4
  store i8* %add.ptr44, i8** %outptr0, align 4, !tbaa !2
  %109 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr45 = getelementptr inbounds i8, i8* %109, i32 1
  store i8* %incdec.ptr45, i8** %inptr01, align 4, !tbaa !2
  %110 = load i8, i8* %109, align 1, !tbaa !39
  %conv46 = zext i8 %110 to i32
  store i32 %conv46, i32* %y, align 4, !tbaa !32
  %111 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %112 = load i32, i32* %y, align 4, !tbaa !32
  %113 = load i32, i32* %cred, align 4, !tbaa !32
  %add47 = add nsw i32 %112, %113
  %arrayidx48 = getelementptr inbounds i8, i8* %111, i32 %add47
  %114 = load i8, i8* %arrayidx48, align 1, !tbaa !39
  %115 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i8, i8* %115, i32 0
  store i8 %114, i8* %arrayidx49, align 1, !tbaa !39
  %116 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %117 = load i32, i32* %y, align 4, !tbaa !32
  %118 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add50 = add nsw i32 %117, %118
  %arrayidx51 = getelementptr inbounds i8, i8* %116, i32 %add50
  %119 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %120 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %120, i32 1
  store i8 %119, i8* %arrayidx52, align 1, !tbaa !39
  %121 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %122 = load i32, i32* %y, align 4, !tbaa !32
  %123 = load i32, i32* %cblue, align 4, !tbaa !32
  %add53 = add nsw i32 %122, %123
  %arrayidx54 = getelementptr inbounds i8, i8* %121, i32 %add53
  %124 = load i8, i8* %arrayidx54, align 1, !tbaa !39
  %125 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %125, i32 2
  store i8 %124, i8* %arrayidx55, align 1, !tbaa !39
  %126 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i8, i8* %126, i32 3
  store i8 -1, i8* %arrayidx56, align 1, !tbaa !39
  %127 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr57 = getelementptr inbounds i8, i8* %127, i32 4
  store i8* %add.ptr57, i8** %outptr1, align 4, !tbaa !2
  %128 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr58 = getelementptr inbounds i8, i8* %128, i32 1
  store i8* %incdec.ptr58, i8** %inptr01, align 4, !tbaa !2
  %129 = load i8, i8* %128, align 1, !tbaa !39
  %conv59 = zext i8 %129 to i32
  store i32 %conv59, i32* %y, align 4, !tbaa !32
  %130 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %131 = load i32, i32* %y, align 4, !tbaa !32
  %132 = load i32, i32* %cred, align 4, !tbaa !32
  %add60 = add nsw i32 %131, %132
  %arrayidx61 = getelementptr inbounds i8, i8* %130, i32 %add60
  %133 = load i8, i8* %arrayidx61, align 1, !tbaa !39
  %134 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i8, i8* %134, i32 0
  store i8 %133, i8* %arrayidx62, align 1, !tbaa !39
  %135 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %136 = load i32, i32* %y, align 4, !tbaa !32
  %137 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add63 = add nsw i32 %136, %137
  %arrayidx64 = getelementptr inbounds i8, i8* %135, i32 %add63
  %138 = load i8, i8* %arrayidx64, align 1, !tbaa !39
  %139 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %139, i32 1
  store i8 %138, i8* %arrayidx65, align 1, !tbaa !39
  %140 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !32
  %142 = load i32, i32* %cblue, align 4, !tbaa !32
  %add66 = add nsw i32 %141, %142
  %arrayidx67 = getelementptr inbounds i8, i8* %140, i32 %add66
  %143 = load i8, i8* %arrayidx67, align 1, !tbaa !39
  %144 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i8, i8* %144, i32 2
  store i8 %143, i8* %arrayidx68, align 1, !tbaa !39
  %145 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i8, i8* %145, i32 3
  store i8 -1, i8* %arrayidx69, align 1, !tbaa !39
  %146 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr70 = getelementptr inbounds i8, i8* %146, i32 4
  store i8* %add.ptr70, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %147 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %147, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 27
  %149 = load i32, i32* %output_width71, align 8, !tbaa !19
  %and = and i32 %149, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %150 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %151 = load i8, i8* %150, align 1, !tbaa !39
  %conv72 = zext i8 %151 to i32
  store i32 %conv72, i32* %cb, align 4, !tbaa !32
  %152 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %153 = load i8, i8* %152, align 1, !tbaa !39
  %conv73 = zext i8 %153 to i32
  store i32 %conv73, i32* %cr, align 4, !tbaa !32
  %154 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %155 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx74 = getelementptr inbounds i32, i32* %154, i32 %155
  %156 = load i32, i32* %arrayidx74, align 4, !tbaa !32
  store i32 %156, i32* %cred, align 4, !tbaa !32
  %157 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %158 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx75 = getelementptr inbounds i32, i32* %157, i32 %158
  %159 = load i32, i32* %arrayidx75, align 4, !tbaa !37
  %160 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %161 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx76 = getelementptr inbounds i32, i32* %160, i32 %161
  %162 = load i32, i32* %arrayidx76, align 4, !tbaa !37
  %add77 = add nsw i32 %159, %162
  %shr78 = ashr i32 %add77, 16
  store i32 %shr78, i32* %cgreen, align 4, !tbaa !32
  %163 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %164 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx79 = getelementptr inbounds i32, i32* %163, i32 %164
  %165 = load i32, i32* %arrayidx79, align 4, !tbaa !32
  store i32 %165, i32* %cblue, align 4, !tbaa !32
  %166 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %167 = load i8, i8* %166, align 1, !tbaa !39
  %conv80 = zext i8 %167 to i32
  store i32 %conv80, i32* %y, align 4, !tbaa !32
  %168 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %169 = load i32, i32* %y, align 4, !tbaa !32
  %170 = load i32, i32* %cred, align 4, !tbaa !32
  %add81 = add nsw i32 %169, %170
  %arrayidx82 = getelementptr inbounds i8, i8* %168, i32 %add81
  %171 = load i8, i8* %arrayidx82, align 1, !tbaa !39
  %172 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i8, i8* %172, i32 0
  store i8 %171, i8* %arrayidx83, align 1, !tbaa !39
  %173 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %174 = load i32, i32* %y, align 4, !tbaa !32
  %175 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add84 = add nsw i32 %174, %175
  %arrayidx85 = getelementptr inbounds i8, i8* %173, i32 %add84
  %176 = load i8, i8* %arrayidx85, align 1, !tbaa !39
  %177 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i8, i8* %177, i32 1
  store i8 %176, i8* %arrayidx86, align 1, !tbaa !39
  %178 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %179 = load i32, i32* %y, align 4, !tbaa !32
  %180 = load i32, i32* %cblue, align 4, !tbaa !32
  %add87 = add nsw i32 %179, %180
  %arrayidx88 = getelementptr inbounds i8, i8* %178, i32 %add87
  %181 = load i8, i8* %arrayidx88, align 1, !tbaa !39
  %182 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i8, i8* %182, i32 2
  store i8 %181, i8* %arrayidx89, align 1, !tbaa !39
  %183 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i8, i8* %183, i32 3
  store i8 -1, i8* %arrayidx90, align 1, !tbaa !39
  %184 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %185 = load i8, i8* %184, align 1, !tbaa !39
  %conv91 = zext i8 %185 to i32
  store i32 %conv91, i32* %y, align 4, !tbaa !32
  %186 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %187 = load i32, i32* %y, align 4, !tbaa !32
  %188 = load i32, i32* %cred, align 4, !tbaa !32
  %add92 = add nsw i32 %187, %188
  %arrayidx93 = getelementptr inbounds i8, i8* %186, i32 %add92
  %189 = load i8, i8* %arrayidx93, align 1, !tbaa !39
  %190 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i8, i8* %190, i32 0
  store i8 %189, i8* %arrayidx94, align 1, !tbaa !39
  %191 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %192 = load i32, i32* %y, align 4, !tbaa !32
  %193 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add95 = add nsw i32 %192, %193
  %arrayidx96 = getelementptr inbounds i8, i8* %191, i32 %add95
  %194 = load i8, i8* %arrayidx96, align 1, !tbaa !39
  %195 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i8, i8* %195, i32 1
  store i8 %194, i8* %arrayidx97, align 1, !tbaa !39
  %196 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %197 = load i32, i32* %y, align 4, !tbaa !32
  %198 = load i32, i32* %cblue, align 4, !tbaa !32
  %add98 = add nsw i32 %197, %198
  %arrayidx99 = getelementptr inbounds i8, i8* %196, i32 %add98
  %199 = load i8, i8* %arrayidx99, align 1, !tbaa !39
  %200 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i8, i8* %200, i32 2
  store i8 %199, i8* %arrayidx100, align 1, !tbaa !39
  %201 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i8, i8* %201, i32 3
  store i8 -1, i8* %arrayidx101, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %202 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  %213 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  %215 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #4
  %216 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #4
  %217 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #4
  %218 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  %219 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #4
  %220 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgr_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %32, i32 0
  %33 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %34, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %33, i32 %mul
  %35 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %35, i8** %inptr00, align 4, !tbaa !2
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %38, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %37, i32 %add
  %39 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %39, i8** %inptr01, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %40, i32 1
  %41 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %44, i32 2
  %45 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %47, i8** %inptr2, align 4, !tbaa !2
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %48, i32 0
  %49 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %49, i8** %outptr0, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %50, i32 1
  %51 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %51, i8** %outptr1, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv13 = zext i8 %58 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %64, %67
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv21 = zext i8 %72 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %74, %75
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %76 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %77 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 2
  store i8 %76, i8* %arrayidx24, align 1, !tbaa !39
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %79, %80
  %arrayidx26 = getelementptr inbounds i8, i8* %78, i32 %add25
  %81 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %82 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8 %81, i8* %arrayidx27, align 1, !tbaa !39
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 0
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %88, i32 3
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %89 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr31 = getelementptr inbounds i8, i8* %89, i32 1
  store i8* %incdec.ptr31, i8** %inptr00, align 4, !tbaa !2
  %90 = load i8, i8* %89, align 1, !tbaa !39
  %conv32 = zext i8 %90 to i32
  store i32 %conv32, i32* %y, align 4, !tbaa !32
  %91 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %92 = load i32, i32* %y, align 4, !tbaa !32
  %93 = load i32, i32* %cred, align 4, !tbaa !32
  %add33 = add nsw i32 %92, %93
  %arrayidx34 = getelementptr inbounds i8, i8* %91, i32 %add33
  %94 = load i8, i8* %arrayidx34, align 1, !tbaa !39
  %95 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i8, i8* %95, i32 2
  store i8 %94, i8* %arrayidx35, align 1, !tbaa !39
  %96 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %97 = load i32, i32* %y, align 4, !tbaa !32
  %98 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add36 = add nsw i32 %97, %98
  %arrayidx37 = getelementptr inbounds i8, i8* %96, i32 %add36
  %99 = load i8, i8* %arrayidx37, align 1, !tbaa !39
  %100 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i8, i8* %100, i32 1
  store i8 %99, i8* %arrayidx38, align 1, !tbaa !39
  %101 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %102 = load i32, i32* %y, align 4, !tbaa !32
  %103 = load i32, i32* %cblue, align 4, !tbaa !32
  %add39 = add nsw i32 %102, %103
  %arrayidx40 = getelementptr inbounds i8, i8* %101, i32 %add39
  %104 = load i8, i8* %arrayidx40, align 1, !tbaa !39
  %105 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i8, i8* %105, i32 0
  store i8 %104, i8* %arrayidx41, align 1, !tbaa !39
  %106 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr42 = getelementptr inbounds i8, i8* %106, i32 3
  store i8* %add.ptr42, i8** %outptr0, align 4, !tbaa !2
  %107 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr43 = getelementptr inbounds i8, i8* %107, i32 1
  store i8* %incdec.ptr43, i8** %inptr01, align 4, !tbaa !2
  %108 = load i8, i8* %107, align 1, !tbaa !39
  %conv44 = zext i8 %108 to i32
  store i32 %conv44, i32* %y, align 4, !tbaa !32
  %109 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %110 = load i32, i32* %y, align 4, !tbaa !32
  %111 = load i32, i32* %cred, align 4, !tbaa !32
  %add45 = add nsw i32 %110, %111
  %arrayidx46 = getelementptr inbounds i8, i8* %109, i32 %add45
  %112 = load i8, i8* %arrayidx46, align 1, !tbaa !39
  %113 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i8, i8* %113, i32 2
  store i8 %112, i8* %arrayidx47, align 1, !tbaa !39
  %114 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %115 = load i32, i32* %y, align 4, !tbaa !32
  %116 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add48 = add nsw i32 %115, %116
  %arrayidx49 = getelementptr inbounds i8, i8* %114, i32 %add48
  %117 = load i8, i8* %arrayidx49, align 1, !tbaa !39
  %118 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i8, i8* %118, i32 1
  store i8 %117, i8* %arrayidx50, align 1, !tbaa !39
  %119 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %120 = load i32, i32* %y, align 4, !tbaa !32
  %121 = load i32, i32* %cblue, align 4, !tbaa !32
  %add51 = add nsw i32 %120, %121
  %arrayidx52 = getelementptr inbounds i8, i8* %119, i32 %add51
  %122 = load i8, i8* %arrayidx52, align 1, !tbaa !39
  %123 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i8, i8* %123, i32 0
  store i8 %122, i8* %arrayidx53, align 1, !tbaa !39
  %124 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr54 = getelementptr inbounds i8, i8* %124, i32 3
  store i8* %add.ptr54, i8** %outptr1, align 4, !tbaa !2
  %125 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr55 = getelementptr inbounds i8, i8* %125, i32 1
  store i8* %incdec.ptr55, i8** %inptr01, align 4, !tbaa !2
  %126 = load i8, i8* %125, align 1, !tbaa !39
  %conv56 = zext i8 %126 to i32
  store i32 %conv56, i32* %y, align 4, !tbaa !32
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !32
  %129 = load i32, i32* %cred, align 4, !tbaa !32
  %add57 = add nsw i32 %128, %129
  %arrayidx58 = getelementptr inbounds i8, i8* %127, i32 %add57
  %130 = load i8, i8* %arrayidx58, align 1, !tbaa !39
  %131 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i8, i8* %131, i32 2
  store i8 %130, i8* %arrayidx59, align 1, !tbaa !39
  %132 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %133 = load i32, i32* %y, align 4, !tbaa !32
  %134 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add60 = add nsw i32 %133, %134
  %arrayidx61 = getelementptr inbounds i8, i8* %132, i32 %add60
  %135 = load i8, i8* %arrayidx61, align 1, !tbaa !39
  %136 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i8, i8* %136, i32 1
  store i8 %135, i8* %arrayidx62, align 1, !tbaa !39
  %137 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %138 = load i32, i32* %y, align 4, !tbaa !32
  %139 = load i32, i32* %cblue, align 4, !tbaa !32
  %add63 = add nsw i32 %138, %139
  %arrayidx64 = getelementptr inbounds i8, i8* %137, i32 %add63
  %140 = load i8, i8* %arrayidx64, align 1, !tbaa !39
  %141 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %141, i32 0
  store i8 %140, i8* %arrayidx65, align 1, !tbaa !39
  %142 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr66 = getelementptr inbounds i8, i8* %142, i32 3
  store i8* %add.ptr66, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %143 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %143, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 27
  %145 = load i32, i32* %output_width67, align 8, !tbaa !19
  %and = and i32 %145, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %146 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %147 = load i8, i8* %146, align 1, !tbaa !39
  %conv68 = zext i8 %147 to i32
  store i32 %conv68, i32* %cb, align 4, !tbaa !32
  %148 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %149 = load i8, i8* %148, align 1, !tbaa !39
  %conv69 = zext i8 %149 to i32
  store i32 %conv69, i32* %cr, align 4, !tbaa !32
  %150 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %151 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx70 = getelementptr inbounds i32, i32* %150, i32 %151
  %152 = load i32, i32* %arrayidx70, align 4, !tbaa !32
  store i32 %152, i32* %cred, align 4, !tbaa !32
  %153 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %154 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx71 = getelementptr inbounds i32, i32* %153, i32 %154
  %155 = load i32, i32* %arrayidx71, align 4, !tbaa !37
  %156 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %157 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx72 = getelementptr inbounds i32, i32* %156, i32 %157
  %158 = load i32, i32* %arrayidx72, align 4, !tbaa !37
  %add73 = add nsw i32 %155, %158
  %shr74 = ashr i32 %add73, 16
  store i32 %shr74, i32* %cgreen, align 4, !tbaa !32
  %159 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %160 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx75 = getelementptr inbounds i32, i32* %159, i32 %160
  %161 = load i32, i32* %arrayidx75, align 4, !tbaa !32
  store i32 %161, i32* %cblue, align 4, !tbaa !32
  %162 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %163 = load i8, i8* %162, align 1, !tbaa !39
  %conv76 = zext i8 %163 to i32
  store i32 %conv76, i32* %y, align 4, !tbaa !32
  %164 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %165 = load i32, i32* %y, align 4, !tbaa !32
  %166 = load i32, i32* %cred, align 4, !tbaa !32
  %add77 = add nsw i32 %165, %166
  %arrayidx78 = getelementptr inbounds i8, i8* %164, i32 %add77
  %167 = load i8, i8* %arrayidx78, align 1, !tbaa !39
  %168 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i8, i8* %168, i32 2
  store i8 %167, i8* %arrayidx79, align 1, !tbaa !39
  %169 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %170 = load i32, i32* %y, align 4, !tbaa !32
  %171 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add80 = add nsw i32 %170, %171
  %arrayidx81 = getelementptr inbounds i8, i8* %169, i32 %add80
  %172 = load i8, i8* %arrayidx81, align 1, !tbaa !39
  %173 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i8, i8* %173, i32 1
  store i8 %172, i8* %arrayidx82, align 1, !tbaa !39
  %174 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %175 = load i32, i32* %y, align 4, !tbaa !32
  %176 = load i32, i32* %cblue, align 4, !tbaa !32
  %add83 = add nsw i32 %175, %176
  %arrayidx84 = getelementptr inbounds i8, i8* %174, i32 %add83
  %177 = load i8, i8* %arrayidx84, align 1, !tbaa !39
  %178 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i8, i8* %178, i32 0
  store i8 %177, i8* %arrayidx85, align 1, !tbaa !39
  %179 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %180 = load i8, i8* %179, align 1, !tbaa !39
  %conv86 = zext i8 %180 to i32
  store i32 %conv86, i32* %y, align 4, !tbaa !32
  %181 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %182 = load i32, i32* %y, align 4, !tbaa !32
  %183 = load i32, i32* %cred, align 4, !tbaa !32
  %add87 = add nsw i32 %182, %183
  %arrayidx88 = getelementptr inbounds i8, i8* %181, i32 %add87
  %184 = load i8, i8* %arrayidx88, align 1, !tbaa !39
  %185 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i8, i8* %185, i32 2
  store i8 %184, i8* %arrayidx89, align 1, !tbaa !39
  %186 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %187 = load i32, i32* %y, align 4, !tbaa !32
  %188 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add90 = add nsw i32 %187, %188
  %arrayidx91 = getelementptr inbounds i8, i8* %186, i32 %add90
  %189 = load i8, i8* %arrayidx91, align 1, !tbaa !39
  %190 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i8, i8* %190, i32 1
  store i8 %189, i8* %arrayidx92, align 1, !tbaa !39
  %191 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %192 = load i32, i32* %y, align 4, !tbaa !32
  %193 = load i32, i32* %cblue, align 4, !tbaa !32
  %add93 = add nsw i32 %192, %193
  %arrayidx94 = getelementptr inbounds i8, i8* %191, i32 %add93
  %194 = load i8, i8* %arrayidx94, align 1, !tbaa !39
  %195 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i8, i8* %195, i32 0
  store i8 %194, i8* %arrayidx95, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %196 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  %197 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #4
  %198 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  %199 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #4
  %200 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #4
  %201 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #4
  %202 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  %213 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgrx_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %32, i32 0
  %33 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %34, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %33, i32 %mul
  %35 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %35, i8** %inptr00, align 4, !tbaa !2
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %38, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %37, i32 %add
  %39 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %39, i8** %inptr01, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %40, i32 1
  %41 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %44, i32 2
  %45 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %47, i8** %inptr2, align 4, !tbaa !2
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %48, i32 0
  %49 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %49, i8** %outptr0, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %50, i32 1
  %51 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %51, i8** %outptr1, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv13 = zext i8 %58 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %64, %67
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv21 = zext i8 %72 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %74, %75
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %76 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %77 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 2
  store i8 %76, i8* %arrayidx24, align 1, !tbaa !39
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %79, %80
  %arrayidx26 = getelementptr inbounds i8, i8* %78, i32 %add25
  %81 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %82 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8 %81, i8* %arrayidx27, align 1, !tbaa !39
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 0
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %88, i32 3
  store i8 -1, i8* %arrayidx31, align 1, !tbaa !39
  %89 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %89, i32 4
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %90 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds i8, i8* %90, i32 1
  store i8* %incdec.ptr32, i8** %inptr00, align 4, !tbaa !2
  %91 = load i8, i8* %90, align 1, !tbaa !39
  %conv33 = zext i8 %91 to i32
  store i32 %conv33, i32* %y, align 4, !tbaa !32
  %92 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %93 = load i32, i32* %y, align 4, !tbaa !32
  %94 = load i32, i32* %cred, align 4, !tbaa !32
  %add34 = add nsw i32 %93, %94
  %arrayidx35 = getelementptr inbounds i8, i8* %92, i32 %add34
  %95 = load i8, i8* %arrayidx35, align 1, !tbaa !39
  %96 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %96, i32 2
  store i8 %95, i8* %arrayidx36, align 1, !tbaa !39
  %97 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %98 = load i32, i32* %y, align 4, !tbaa !32
  %99 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add37 = add nsw i32 %98, %99
  %arrayidx38 = getelementptr inbounds i8, i8* %97, i32 %add37
  %100 = load i8, i8* %arrayidx38, align 1, !tbaa !39
  %101 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i8, i8* %101, i32 1
  store i8 %100, i8* %arrayidx39, align 1, !tbaa !39
  %102 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %103 = load i32, i32* %y, align 4, !tbaa !32
  %104 = load i32, i32* %cblue, align 4, !tbaa !32
  %add40 = add nsw i32 %103, %104
  %arrayidx41 = getelementptr inbounds i8, i8* %102, i32 %add40
  %105 = load i8, i8* %arrayidx41, align 1, !tbaa !39
  %106 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i8, i8* %106, i32 0
  store i8 %105, i8* %arrayidx42, align 1, !tbaa !39
  %107 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i8, i8* %107, i32 3
  store i8 -1, i8* %arrayidx43, align 1, !tbaa !39
  %108 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr44 = getelementptr inbounds i8, i8* %108, i32 4
  store i8* %add.ptr44, i8** %outptr0, align 4, !tbaa !2
  %109 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr45 = getelementptr inbounds i8, i8* %109, i32 1
  store i8* %incdec.ptr45, i8** %inptr01, align 4, !tbaa !2
  %110 = load i8, i8* %109, align 1, !tbaa !39
  %conv46 = zext i8 %110 to i32
  store i32 %conv46, i32* %y, align 4, !tbaa !32
  %111 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %112 = load i32, i32* %y, align 4, !tbaa !32
  %113 = load i32, i32* %cred, align 4, !tbaa !32
  %add47 = add nsw i32 %112, %113
  %arrayidx48 = getelementptr inbounds i8, i8* %111, i32 %add47
  %114 = load i8, i8* %arrayidx48, align 1, !tbaa !39
  %115 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i8, i8* %115, i32 2
  store i8 %114, i8* %arrayidx49, align 1, !tbaa !39
  %116 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %117 = load i32, i32* %y, align 4, !tbaa !32
  %118 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add50 = add nsw i32 %117, %118
  %arrayidx51 = getelementptr inbounds i8, i8* %116, i32 %add50
  %119 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %120 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %120, i32 1
  store i8 %119, i8* %arrayidx52, align 1, !tbaa !39
  %121 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %122 = load i32, i32* %y, align 4, !tbaa !32
  %123 = load i32, i32* %cblue, align 4, !tbaa !32
  %add53 = add nsw i32 %122, %123
  %arrayidx54 = getelementptr inbounds i8, i8* %121, i32 %add53
  %124 = load i8, i8* %arrayidx54, align 1, !tbaa !39
  %125 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %125, i32 0
  store i8 %124, i8* %arrayidx55, align 1, !tbaa !39
  %126 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i8, i8* %126, i32 3
  store i8 -1, i8* %arrayidx56, align 1, !tbaa !39
  %127 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr57 = getelementptr inbounds i8, i8* %127, i32 4
  store i8* %add.ptr57, i8** %outptr1, align 4, !tbaa !2
  %128 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr58 = getelementptr inbounds i8, i8* %128, i32 1
  store i8* %incdec.ptr58, i8** %inptr01, align 4, !tbaa !2
  %129 = load i8, i8* %128, align 1, !tbaa !39
  %conv59 = zext i8 %129 to i32
  store i32 %conv59, i32* %y, align 4, !tbaa !32
  %130 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %131 = load i32, i32* %y, align 4, !tbaa !32
  %132 = load i32, i32* %cred, align 4, !tbaa !32
  %add60 = add nsw i32 %131, %132
  %arrayidx61 = getelementptr inbounds i8, i8* %130, i32 %add60
  %133 = load i8, i8* %arrayidx61, align 1, !tbaa !39
  %134 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i8, i8* %134, i32 2
  store i8 %133, i8* %arrayidx62, align 1, !tbaa !39
  %135 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %136 = load i32, i32* %y, align 4, !tbaa !32
  %137 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add63 = add nsw i32 %136, %137
  %arrayidx64 = getelementptr inbounds i8, i8* %135, i32 %add63
  %138 = load i8, i8* %arrayidx64, align 1, !tbaa !39
  %139 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %139, i32 1
  store i8 %138, i8* %arrayidx65, align 1, !tbaa !39
  %140 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !32
  %142 = load i32, i32* %cblue, align 4, !tbaa !32
  %add66 = add nsw i32 %141, %142
  %arrayidx67 = getelementptr inbounds i8, i8* %140, i32 %add66
  %143 = load i8, i8* %arrayidx67, align 1, !tbaa !39
  %144 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i8, i8* %144, i32 0
  store i8 %143, i8* %arrayidx68, align 1, !tbaa !39
  %145 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i8, i8* %145, i32 3
  store i8 -1, i8* %arrayidx69, align 1, !tbaa !39
  %146 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr70 = getelementptr inbounds i8, i8* %146, i32 4
  store i8* %add.ptr70, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %147 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %147, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 27
  %149 = load i32, i32* %output_width71, align 8, !tbaa !19
  %and = and i32 %149, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %150 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %151 = load i8, i8* %150, align 1, !tbaa !39
  %conv72 = zext i8 %151 to i32
  store i32 %conv72, i32* %cb, align 4, !tbaa !32
  %152 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %153 = load i8, i8* %152, align 1, !tbaa !39
  %conv73 = zext i8 %153 to i32
  store i32 %conv73, i32* %cr, align 4, !tbaa !32
  %154 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %155 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx74 = getelementptr inbounds i32, i32* %154, i32 %155
  %156 = load i32, i32* %arrayidx74, align 4, !tbaa !32
  store i32 %156, i32* %cred, align 4, !tbaa !32
  %157 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %158 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx75 = getelementptr inbounds i32, i32* %157, i32 %158
  %159 = load i32, i32* %arrayidx75, align 4, !tbaa !37
  %160 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %161 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx76 = getelementptr inbounds i32, i32* %160, i32 %161
  %162 = load i32, i32* %arrayidx76, align 4, !tbaa !37
  %add77 = add nsw i32 %159, %162
  %shr78 = ashr i32 %add77, 16
  store i32 %shr78, i32* %cgreen, align 4, !tbaa !32
  %163 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %164 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx79 = getelementptr inbounds i32, i32* %163, i32 %164
  %165 = load i32, i32* %arrayidx79, align 4, !tbaa !32
  store i32 %165, i32* %cblue, align 4, !tbaa !32
  %166 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %167 = load i8, i8* %166, align 1, !tbaa !39
  %conv80 = zext i8 %167 to i32
  store i32 %conv80, i32* %y, align 4, !tbaa !32
  %168 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %169 = load i32, i32* %y, align 4, !tbaa !32
  %170 = load i32, i32* %cred, align 4, !tbaa !32
  %add81 = add nsw i32 %169, %170
  %arrayidx82 = getelementptr inbounds i8, i8* %168, i32 %add81
  %171 = load i8, i8* %arrayidx82, align 1, !tbaa !39
  %172 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i8, i8* %172, i32 2
  store i8 %171, i8* %arrayidx83, align 1, !tbaa !39
  %173 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %174 = load i32, i32* %y, align 4, !tbaa !32
  %175 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add84 = add nsw i32 %174, %175
  %arrayidx85 = getelementptr inbounds i8, i8* %173, i32 %add84
  %176 = load i8, i8* %arrayidx85, align 1, !tbaa !39
  %177 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i8, i8* %177, i32 1
  store i8 %176, i8* %arrayidx86, align 1, !tbaa !39
  %178 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %179 = load i32, i32* %y, align 4, !tbaa !32
  %180 = load i32, i32* %cblue, align 4, !tbaa !32
  %add87 = add nsw i32 %179, %180
  %arrayidx88 = getelementptr inbounds i8, i8* %178, i32 %add87
  %181 = load i8, i8* %arrayidx88, align 1, !tbaa !39
  %182 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i8, i8* %182, i32 0
  store i8 %181, i8* %arrayidx89, align 1, !tbaa !39
  %183 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i8, i8* %183, i32 3
  store i8 -1, i8* %arrayidx90, align 1, !tbaa !39
  %184 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %185 = load i8, i8* %184, align 1, !tbaa !39
  %conv91 = zext i8 %185 to i32
  store i32 %conv91, i32* %y, align 4, !tbaa !32
  %186 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %187 = load i32, i32* %y, align 4, !tbaa !32
  %188 = load i32, i32* %cred, align 4, !tbaa !32
  %add92 = add nsw i32 %187, %188
  %arrayidx93 = getelementptr inbounds i8, i8* %186, i32 %add92
  %189 = load i8, i8* %arrayidx93, align 1, !tbaa !39
  %190 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i8, i8* %190, i32 2
  store i8 %189, i8* %arrayidx94, align 1, !tbaa !39
  %191 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %192 = load i32, i32* %y, align 4, !tbaa !32
  %193 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add95 = add nsw i32 %192, %193
  %arrayidx96 = getelementptr inbounds i8, i8* %191, i32 %add95
  %194 = load i8, i8* %arrayidx96, align 1, !tbaa !39
  %195 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i8, i8* %195, i32 1
  store i8 %194, i8* %arrayidx97, align 1, !tbaa !39
  %196 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %197 = load i32, i32* %y, align 4, !tbaa !32
  %198 = load i32, i32* %cblue, align 4, !tbaa !32
  %add98 = add nsw i32 %197, %198
  %arrayidx99 = getelementptr inbounds i8, i8* %196, i32 %add98
  %199 = load i8, i8* %arrayidx99, align 1, !tbaa !39
  %200 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i8, i8* %200, i32 0
  store i8 %199, i8* %arrayidx100, align 1, !tbaa !39
  %201 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i8, i8* %201, i32 3
  store i8 -1, i8* %arrayidx101, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %202 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  %213 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  %215 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #4
  %216 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #4
  %217 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #4
  %218 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  %219 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #4
  %220 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxbgr_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %32, i32 0
  %33 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %34, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %33, i32 %mul
  %35 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %35, i8** %inptr00, align 4, !tbaa !2
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %38, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %37, i32 %add
  %39 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %39, i8** %inptr01, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %40, i32 1
  %41 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %44, i32 2
  %45 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %47, i8** %inptr2, align 4, !tbaa !2
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %48, i32 0
  %49 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %49, i8** %outptr0, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %50, i32 1
  %51 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %51, i8** %outptr1, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv13 = zext i8 %58 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %64, %67
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv21 = zext i8 %72 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %74, %75
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %76 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %77 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 3
  store i8 %76, i8* %arrayidx24, align 1, !tbaa !39
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %79, %80
  %arrayidx26 = getelementptr inbounds i8, i8* %78, i32 %add25
  %81 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %82 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i8, i8* %82, i32 2
  store i8 %81, i8* %arrayidx27, align 1, !tbaa !39
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 1
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %88, i32 0
  store i8 -1, i8* %arrayidx31, align 1, !tbaa !39
  %89 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %89, i32 4
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %90 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds i8, i8* %90, i32 1
  store i8* %incdec.ptr32, i8** %inptr00, align 4, !tbaa !2
  %91 = load i8, i8* %90, align 1, !tbaa !39
  %conv33 = zext i8 %91 to i32
  store i32 %conv33, i32* %y, align 4, !tbaa !32
  %92 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %93 = load i32, i32* %y, align 4, !tbaa !32
  %94 = load i32, i32* %cred, align 4, !tbaa !32
  %add34 = add nsw i32 %93, %94
  %arrayidx35 = getelementptr inbounds i8, i8* %92, i32 %add34
  %95 = load i8, i8* %arrayidx35, align 1, !tbaa !39
  %96 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %96, i32 3
  store i8 %95, i8* %arrayidx36, align 1, !tbaa !39
  %97 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %98 = load i32, i32* %y, align 4, !tbaa !32
  %99 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add37 = add nsw i32 %98, %99
  %arrayidx38 = getelementptr inbounds i8, i8* %97, i32 %add37
  %100 = load i8, i8* %arrayidx38, align 1, !tbaa !39
  %101 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i8, i8* %101, i32 2
  store i8 %100, i8* %arrayidx39, align 1, !tbaa !39
  %102 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %103 = load i32, i32* %y, align 4, !tbaa !32
  %104 = load i32, i32* %cblue, align 4, !tbaa !32
  %add40 = add nsw i32 %103, %104
  %arrayidx41 = getelementptr inbounds i8, i8* %102, i32 %add40
  %105 = load i8, i8* %arrayidx41, align 1, !tbaa !39
  %106 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i8, i8* %106, i32 1
  store i8 %105, i8* %arrayidx42, align 1, !tbaa !39
  %107 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i8, i8* %107, i32 0
  store i8 -1, i8* %arrayidx43, align 1, !tbaa !39
  %108 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr44 = getelementptr inbounds i8, i8* %108, i32 4
  store i8* %add.ptr44, i8** %outptr0, align 4, !tbaa !2
  %109 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr45 = getelementptr inbounds i8, i8* %109, i32 1
  store i8* %incdec.ptr45, i8** %inptr01, align 4, !tbaa !2
  %110 = load i8, i8* %109, align 1, !tbaa !39
  %conv46 = zext i8 %110 to i32
  store i32 %conv46, i32* %y, align 4, !tbaa !32
  %111 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %112 = load i32, i32* %y, align 4, !tbaa !32
  %113 = load i32, i32* %cred, align 4, !tbaa !32
  %add47 = add nsw i32 %112, %113
  %arrayidx48 = getelementptr inbounds i8, i8* %111, i32 %add47
  %114 = load i8, i8* %arrayidx48, align 1, !tbaa !39
  %115 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i8, i8* %115, i32 3
  store i8 %114, i8* %arrayidx49, align 1, !tbaa !39
  %116 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %117 = load i32, i32* %y, align 4, !tbaa !32
  %118 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add50 = add nsw i32 %117, %118
  %arrayidx51 = getelementptr inbounds i8, i8* %116, i32 %add50
  %119 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %120 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %120, i32 2
  store i8 %119, i8* %arrayidx52, align 1, !tbaa !39
  %121 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %122 = load i32, i32* %y, align 4, !tbaa !32
  %123 = load i32, i32* %cblue, align 4, !tbaa !32
  %add53 = add nsw i32 %122, %123
  %arrayidx54 = getelementptr inbounds i8, i8* %121, i32 %add53
  %124 = load i8, i8* %arrayidx54, align 1, !tbaa !39
  %125 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %125, i32 1
  store i8 %124, i8* %arrayidx55, align 1, !tbaa !39
  %126 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i8, i8* %126, i32 0
  store i8 -1, i8* %arrayidx56, align 1, !tbaa !39
  %127 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr57 = getelementptr inbounds i8, i8* %127, i32 4
  store i8* %add.ptr57, i8** %outptr1, align 4, !tbaa !2
  %128 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr58 = getelementptr inbounds i8, i8* %128, i32 1
  store i8* %incdec.ptr58, i8** %inptr01, align 4, !tbaa !2
  %129 = load i8, i8* %128, align 1, !tbaa !39
  %conv59 = zext i8 %129 to i32
  store i32 %conv59, i32* %y, align 4, !tbaa !32
  %130 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %131 = load i32, i32* %y, align 4, !tbaa !32
  %132 = load i32, i32* %cred, align 4, !tbaa !32
  %add60 = add nsw i32 %131, %132
  %arrayidx61 = getelementptr inbounds i8, i8* %130, i32 %add60
  %133 = load i8, i8* %arrayidx61, align 1, !tbaa !39
  %134 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i8, i8* %134, i32 3
  store i8 %133, i8* %arrayidx62, align 1, !tbaa !39
  %135 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %136 = load i32, i32* %y, align 4, !tbaa !32
  %137 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add63 = add nsw i32 %136, %137
  %arrayidx64 = getelementptr inbounds i8, i8* %135, i32 %add63
  %138 = load i8, i8* %arrayidx64, align 1, !tbaa !39
  %139 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %139, i32 2
  store i8 %138, i8* %arrayidx65, align 1, !tbaa !39
  %140 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !32
  %142 = load i32, i32* %cblue, align 4, !tbaa !32
  %add66 = add nsw i32 %141, %142
  %arrayidx67 = getelementptr inbounds i8, i8* %140, i32 %add66
  %143 = load i8, i8* %arrayidx67, align 1, !tbaa !39
  %144 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i8, i8* %144, i32 1
  store i8 %143, i8* %arrayidx68, align 1, !tbaa !39
  %145 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i8, i8* %145, i32 0
  store i8 -1, i8* %arrayidx69, align 1, !tbaa !39
  %146 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr70 = getelementptr inbounds i8, i8* %146, i32 4
  store i8* %add.ptr70, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %147 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %147, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 27
  %149 = load i32, i32* %output_width71, align 8, !tbaa !19
  %and = and i32 %149, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %150 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %151 = load i8, i8* %150, align 1, !tbaa !39
  %conv72 = zext i8 %151 to i32
  store i32 %conv72, i32* %cb, align 4, !tbaa !32
  %152 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %153 = load i8, i8* %152, align 1, !tbaa !39
  %conv73 = zext i8 %153 to i32
  store i32 %conv73, i32* %cr, align 4, !tbaa !32
  %154 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %155 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx74 = getelementptr inbounds i32, i32* %154, i32 %155
  %156 = load i32, i32* %arrayidx74, align 4, !tbaa !32
  store i32 %156, i32* %cred, align 4, !tbaa !32
  %157 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %158 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx75 = getelementptr inbounds i32, i32* %157, i32 %158
  %159 = load i32, i32* %arrayidx75, align 4, !tbaa !37
  %160 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %161 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx76 = getelementptr inbounds i32, i32* %160, i32 %161
  %162 = load i32, i32* %arrayidx76, align 4, !tbaa !37
  %add77 = add nsw i32 %159, %162
  %shr78 = ashr i32 %add77, 16
  store i32 %shr78, i32* %cgreen, align 4, !tbaa !32
  %163 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %164 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx79 = getelementptr inbounds i32, i32* %163, i32 %164
  %165 = load i32, i32* %arrayidx79, align 4, !tbaa !32
  store i32 %165, i32* %cblue, align 4, !tbaa !32
  %166 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %167 = load i8, i8* %166, align 1, !tbaa !39
  %conv80 = zext i8 %167 to i32
  store i32 %conv80, i32* %y, align 4, !tbaa !32
  %168 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %169 = load i32, i32* %y, align 4, !tbaa !32
  %170 = load i32, i32* %cred, align 4, !tbaa !32
  %add81 = add nsw i32 %169, %170
  %arrayidx82 = getelementptr inbounds i8, i8* %168, i32 %add81
  %171 = load i8, i8* %arrayidx82, align 1, !tbaa !39
  %172 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i8, i8* %172, i32 3
  store i8 %171, i8* %arrayidx83, align 1, !tbaa !39
  %173 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %174 = load i32, i32* %y, align 4, !tbaa !32
  %175 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add84 = add nsw i32 %174, %175
  %arrayidx85 = getelementptr inbounds i8, i8* %173, i32 %add84
  %176 = load i8, i8* %arrayidx85, align 1, !tbaa !39
  %177 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i8, i8* %177, i32 2
  store i8 %176, i8* %arrayidx86, align 1, !tbaa !39
  %178 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %179 = load i32, i32* %y, align 4, !tbaa !32
  %180 = load i32, i32* %cblue, align 4, !tbaa !32
  %add87 = add nsw i32 %179, %180
  %arrayidx88 = getelementptr inbounds i8, i8* %178, i32 %add87
  %181 = load i8, i8* %arrayidx88, align 1, !tbaa !39
  %182 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i8, i8* %182, i32 1
  store i8 %181, i8* %arrayidx89, align 1, !tbaa !39
  %183 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i8, i8* %183, i32 0
  store i8 -1, i8* %arrayidx90, align 1, !tbaa !39
  %184 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %185 = load i8, i8* %184, align 1, !tbaa !39
  %conv91 = zext i8 %185 to i32
  store i32 %conv91, i32* %y, align 4, !tbaa !32
  %186 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %187 = load i32, i32* %y, align 4, !tbaa !32
  %188 = load i32, i32* %cred, align 4, !tbaa !32
  %add92 = add nsw i32 %187, %188
  %arrayidx93 = getelementptr inbounds i8, i8* %186, i32 %add92
  %189 = load i8, i8* %arrayidx93, align 1, !tbaa !39
  %190 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i8, i8* %190, i32 3
  store i8 %189, i8* %arrayidx94, align 1, !tbaa !39
  %191 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %192 = load i32, i32* %y, align 4, !tbaa !32
  %193 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add95 = add nsw i32 %192, %193
  %arrayidx96 = getelementptr inbounds i8, i8* %191, i32 %add95
  %194 = load i8, i8* %arrayidx96, align 1, !tbaa !39
  %195 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i8, i8* %195, i32 2
  store i8 %194, i8* %arrayidx97, align 1, !tbaa !39
  %196 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %197 = load i32, i32* %y, align 4, !tbaa !32
  %198 = load i32, i32* %cblue, align 4, !tbaa !32
  %add98 = add nsw i32 %197, %198
  %arrayidx99 = getelementptr inbounds i8, i8* %196, i32 %add98
  %199 = load i8, i8* %arrayidx99, align 1, !tbaa !39
  %200 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i8, i8* %200, i32 1
  store i8 %199, i8* %arrayidx100, align 1, !tbaa !39
  %201 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i8, i8* %201, i32 0
  store i8 -1, i8* %arrayidx101, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %202 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  %213 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  %215 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #4
  %216 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #4
  %217 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #4
  %218 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  %219 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #4
  %220 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxrgb_h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %32, i32 0
  %33 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %34, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %33, i32 %mul
  %35 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %35, i8** %inptr00, align 4, !tbaa !2
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %38, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %37, i32 %add
  %39 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %39, i8** %inptr01, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %40, i32 1
  %41 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %44, i32 2
  %45 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %47, i8** %inptr2, align 4, !tbaa !2
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %48, i32 0
  %49 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %49, i8** %outptr0, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %50, i32 1
  %51 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %51, i8** %outptr1, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv13 = zext i8 %58 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %64, %67
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv21 = zext i8 %72 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %74, %75
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %76 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %77 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 1
  store i8 %76, i8* %arrayidx24, align 1, !tbaa !39
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %79, %80
  %arrayidx26 = getelementptr inbounds i8, i8* %78, i32 %add25
  %81 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %82 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i8, i8* %82, i32 2
  store i8 %81, i8* %arrayidx27, align 1, !tbaa !39
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 3
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %88, i32 0
  store i8 -1, i8* %arrayidx31, align 1, !tbaa !39
  %89 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %89, i32 4
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %90 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds i8, i8* %90, i32 1
  store i8* %incdec.ptr32, i8** %inptr00, align 4, !tbaa !2
  %91 = load i8, i8* %90, align 1, !tbaa !39
  %conv33 = zext i8 %91 to i32
  store i32 %conv33, i32* %y, align 4, !tbaa !32
  %92 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %93 = load i32, i32* %y, align 4, !tbaa !32
  %94 = load i32, i32* %cred, align 4, !tbaa !32
  %add34 = add nsw i32 %93, %94
  %arrayidx35 = getelementptr inbounds i8, i8* %92, i32 %add34
  %95 = load i8, i8* %arrayidx35, align 1, !tbaa !39
  %96 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %96, i32 1
  store i8 %95, i8* %arrayidx36, align 1, !tbaa !39
  %97 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %98 = load i32, i32* %y, align 4, !tbaa !32
  %99 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add37 = add nsw i32 %98, %99
  %arrayidx38 = getelementptr inbounds i8, i8* %97, i32 %add37
  %100 = load i8, i8* %arrayidx38, align 1, !tbaa !39
  %101 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx39 = getelementptr inbounds i8, i8* %101, i32 2
  store i8 %100, i8* %arrayidx39, align 1, !tbaa !39
  %102 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %103 = load i32, i32* %y, align 4, !tbaa !32
  %104 = load i32, i32* %cblue, align 4, !tbaa !32
  %add40 = add nsw i32 %103, %104
  %arrayidx41 = getelementptr inbounds i8, i8* %102, i32 %add40
  %105 = load i8, i8* %arrayidx41, align 1, !tbaa !39
  %106 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i8, i8* %106, i32 3
  store i8 %105, i8* %arrayidx42, align 1, !tbaa !39
  %107 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i8, i8* %107, i32 0
  store i8 -1, i8* %arrayidx43, align 1, !tbaa !39
  %108 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr44 = getelementptr inbounds i8, i8* %108, i32 4
  store i8* %add.ptr44, i8** %outptr0, align 4, !tbaa !2
  %109 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr45 = getelementptr inbounds i8, i8* %109, i32 1
  store i8* %incdec.ptr45, i8** %inptr01, align 4, !tbaa !2
  %110 = load i8, i8* %109, align 1, !tbaa !39
  %conv46 = zext i8 %110 to i32
  store i32 %conv46, i32* %y, align 4, !tbaa !32
  %111 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %112 = load i32, i32* %y, align 4, !tbaa !32
  %113 = load i32, i32* %cred, align 4, !tbaa !32
  %add47 = add nsw i32 %112, %113
  %arrayidx48 = getelementptr inbounds i8, i8* %111, i32 %add47
  %114 = load i8, i8* %arrayidx48, align 1, !tbaa !39
  %115 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx49 = getelementptr inbounds i8, i8* %115, i32 1
  store i8 %114, i8* %arrayidx49, align 1, !tbaa !39
  %116 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %117 = load i32, i32* %y, align 4, !tbaa !32
  %118 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add50 = add nsw i32 %117, %118
  %arrayidx51 = getelementptr inbounds i8, i8* %116, i32 %add50
  %119 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %120 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %120, i32 2
  store i8 %119, i8* %arrayidx52, align 1, !tbaa !39
  %121 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %122 = load i32, i32* %y, align 4, !tbaa !32
  %123 = load i32, i32* %cblue, align 4, !tbaa !32
  %add53 = add nsw i32 %122, %123
  %arrayidx54 = getelementptr inbounds i8, i8* %121, i32 %add53
  %124 = load i8, i8* %arrayidx54, align 1, !tbaa !39
  %125 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %125, i32 3
  store i8 %124, i8* %arrayidx55, align 1, !tbaa !39
  %126 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i8, i8* %126, i32 0
  store i8 -1, i8* %arrayidx56, align 1, !tbaa !39
  %127 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr57 = getelementptr inbounds i8, i8* %127, i32 4
  store i8* %add.ptr57, i8** %outptr1, align 4, !tbaa !2
  %128 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr58 = getelementptr inbounds i8, i8* %128, i32 1
  store i8* %incdec.ptr58, i8** %inptr01, align 4, !tbaa !2
  %129 = load i8, i8* %128, align 1, !tbaa !39
  %conv59 = zext i8 %129 to i32
  store i32 %conv59, i32* %y, align 4, !tbaa !32
  %130 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %131 = load i32, i32* %y, align 4, !tbaa !32
  %132 = load i32, i32* %cred, align 4, !tbaa !32
  %add60 = add nsw i32 %131, %132
  %arrayidx61 = getelementptr inbounds i8, i8* %130, i32 %add60
  %133 = load i8, i8* %arrayidx61, align 1, !tbaa !39
  %134 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i8, i8* %134, i32 1
  store i8 %133, i8* %arrayidx62, align 1, !tbaa !39
  %135 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %136 = load i32, i32* %y, align 4, !tbaa !32
  %137 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add63 = add nsw i32 %136, %137
  %arrayidx64 = getelementptr inbounds i8, i8* %135, i32 %add63
  %138 = load i8, i8* %arrayidx64, align 1, !tbaa !39
  %139 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %139, i32 2
  store i8 %138, i8* %arrayidx65, align 1, !tbaa !39
  %140 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !32
  %142 = load i32, i32* %cblue, align 4, !tbaa !32
  %add66 = add nsw i32 %141, %142
  %arrayidx67 = getelementptr inbounds i8, i8* %140, i32 %add66
  %143 = load i8, i8* %arrayidx67, align 1, !tbaa !39
  %144 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx68 = getelementptr inbounds i8, i8* %144, i32 3
  store i8 %143, i8* %arrayidx68, align 1, !tbaa !39
  %145 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds i8, i8* %145, i32 0
  store i8 -1, i8* %arrayidx69, align 1, !tbaa !39
  %146 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr70 = getelementptr inbounds i8, i8* %146, i32 4
  store i8* %add.ptr70, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %147 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %147, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %148 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width71 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %148, i32 0, i32 27
  %149 = load i32, i32* %output_width71, align 8, !tbaa !19
  %and = and i32 %149, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %150 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %151 = load i8, i8* %150, align 1, !tbaa !39
  %conv72 = zext i8 %151 to i32
  store i32 %conv72, i32* %cb, align 4, !tbaa !32
  %152 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %153 = load i8, i8* %152, align 1, !tbaa !39
  %conv73 = zext i8 %153 to i32
  store i32 %conv73, i32* %cr, align 4, !tbaa !32
  %154 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %155 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx74 = getelementptr inbounds i32, i32* %154, i32 %155
  %156 = load i32, i32* %arrayidx74, align 4, !tbaa !32
  store i32 %156, i32* %cred, align 4, !tbaa !32
  %157 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %158 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx75 = getelementptr inbounds i32, i32* %157, i32 %158
  %159 = load i32, i32* %arrayidx75, align 4, !tbaa !37
  %160 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %161 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx76 = getelementptr inbounds i32, i32* %160, i32 %161
  %162 = load i32, i32* %arrayidx76, align 4, !tbaa !37
  %add77 = add nsw i32 %159, %162
  %shr78 = ashr i32 %add77, 16
  store i32 %shr78, i32* %cgreen, align 4, !tbaa !32
  %163 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %164 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx79 = getelementptr inbounds i32, i32* %163, i32 %164
  %165 = load i32, i32* %arrayidx79, align 4, !tbaa !32
  store i32 %165, i32* %cblue, align 4, !tbaa !32
  %166 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %167 = load i8, i8* %166, align 1, !tbaa !39
  %conv80 = zext i8 %167 to i32
  store i32 %conv80, i32* %y, align 4, !tbaa !32
  %168 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %169 = load i32, i32* %y, align 4, !tbaa !32
  %170 = load i32, i32* %cred, align 4, !tbaa !32
  %add81 = add nsw i32 %169, %170
  %arrayidx82 = getelementptr inbounds i8, i8* %168, i32 %add81
  %171 = load i8, i8* %arrayidx82, align 1, !tbaa !39
  %172 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i8, i8* %172, i32 1
  store i8 %171, i8* %arrayidx83, align 1, !tbaa !39
  %173 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %174 = load i32, i32* %y, align 4, !tbaa !32
  %175 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add84 = add nsw i32 %174, %175
  %arrayidx85 = getelementptr inbounds i8, i8* %173, i32 %add84
  %176 = load i8, i8* %arrayidx85, align 1, !tbaa !39
  %177 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i8, i8* %177, i32 2
  store i8 %176, i8* %arrayidx86, align 1, !tbaa !39
  %178 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %179 = load i32, i32* %y, align 4, !tbaa !32
  %180 = load i32, i32* %cblue, align 4, !tbaa !32
  %add87 = add nsw i32 %179, %180
  %arrayidx88 = getelementptr inbounds i8, i8* %178, i32 %add87
  %181 = load i8, i8* %arrayidx88, align 1, !tbaa !39
  %182 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i8, i8* %182, i32 3
  store i8 %181, i8* %arrayidx89, align 1, !tbaa !39
  %183 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i8, i8* %183, i32 0
  store i8 -1, i8* %arrayidx90, align 1, !tbaa !39
  %184 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %185 = load i8, i8* %184, align 1, !tbaa !39
  %conv91 = zext i8 %185 to i32
  store i32 %conv91, i32* %y, align 4, !tbaa !32
  %186 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %187 = load i32, i32* %y, align 4, !tbaa !32
  %188 = load i32, i32* %cred, align 4, !tbaa !32
  %add92 = add nsw i32 %187, %188
  %arrayidx93 = getelementptr inbounds i8, i8* %186, i32 %add92
  %189 = load i8, i8* %arrayidx93, align 1, !tbaa !39
  %190 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i8, i8* %190, i32 1
  store i8 %189, i8* %arrayidx94, align 1, !tbaa !39
  %191 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %192 = load i32, i32* %y, align 4, !tbaa !32
  %193 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add95 = add nsw i32 %192, %193
  %arrayidx96 = getelementptr inbounds i8, i8* %191, i32 %add95
  %194 = load i8, i8* %arrayidx96, align 1, !tbaa !39
  %195 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds i8, i8* %195, i32 2
  store i8 %194, i8* %arrayidx97, align 1, !tbaa !39
  %196 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %197 = load i32, i32* %y, align 4, !tbaa !32
  %198 = load i32, i32* %cblue, align 4, !tbaa !32
  %add98 = add nsw i32 %197, %198
  %arrayidx99 = getelementptr inbounds i8, i8* %196, i32 %add98
  %199 = load i8, i8* %arrayidx99, align 1, !tbaa !39
  %200 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx100 = getelementptr inbounds i8, i8* %200, i32 3
  store i8 %199, i8* %arrayidx100, align 1, !tbaa !39
  %201 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i8, i8* %201, i32 0
  store i8 -1, i8* %arrayidx101, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %202 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  %213 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  %215 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #4
  %216 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #4
  %217 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #4
  %218 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  %219 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #4
  %220 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v2_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %32, i32 0
  %33 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %34, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %33, i32 %mul
  %35 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %35, i8** %inptr00, align 4, !tbaa !2
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %38, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %37, i32 %add
  %39 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %39, i8** %inptr01, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %40, i32 1
  %41 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %inptr1, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %44, i32 2
  %45 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %47, i8** %inptr2, align 4, !tbaa !2
  %48 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %48, i32 0
  %49 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %49, i8** %outptr0, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %50, i32 1
  %51 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %51, i8** %outptr1, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv13 = zext i8 %58 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %64, %67
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv21 = zext i8 %72 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %74, %75
  %arrayidx23 = getelementptr inbounds i8, i8* %73, i32 %add22
  %76 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %77 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 0
  store i8 %76, i8* %arrayidx24, align 1, !tbaa !39
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %79, %80
  %arrayidx26 = getelementptr inbounds i8, i8* %78, i32 %add25
  %81 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %82 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8 %81, i8* %arrayidx27, align 1, !tbaa !39
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 2
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %88, i32 3
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %89 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr31 = getelementptr inbounds i8, i8* %89, i32 1
  store i8* %incdec.ptr31, i8** %inptr00, align 4, !tbaa !2
  %90 = load i8, i8* %89, align 1, !tbaa !39
  %conv32 = zext i8 %90 to i32
  store i32 %conv32, i32* %y, align 4, !tbaa !32
  %91 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %92 = load i32, i32* %y, align 4, !tbaa !32
  %93 = load i32, i32* %cred, align 4, !tbaa !32
  %add33 = add nsw i32 %92, %93
  %arrayidx34 = getelementptr inbounds i8, i8* %91, i32 %add33
  %94 = load i8, i8* %arrayidx34, align 1, !tbaa !39
  %95 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i8, i8* %95, i32 0
  store i8 %94, i8* %arrayidx35, align 1, !tbaa !39
  %96 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %97 = load i32, i32* %y, align 4, !tbaa !32
  %98 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add36 = add nsw i32 %97, %98
  %arrayidx37 = getelementptr inbounds i8, i8* %96, i32 %add36
  %99 = load i8, i8* %arrayidx37, align 1, !tbaa !39
  %100 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i8, i8* %100, i32 1
  store i8 %99, i8* %arrayidx38, align 1, !tbaa !39
  %101 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %102 = load i32, i32* %y, align 4, !tbaa !32
  %103 = load i32, i32* %cblue, align 4, !tbaa !32
  %add39 = add nsw i32 %102, %103
  %arrayidx40 = getelementptr inbounds i8, i8* %101, i32 %add39
  %104 = load i8, i8* %arrayidx40, align 1, !tbaa !39
  %105 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i8, i8* %105, i32 2
  store i8 %104, i8* %arrayidx41, align 1, !tbaa !39
  %106 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr42 = getelementptr inbounds i8, i8* %106, i32 3
  store i8* %add.ptr42, i8** %outptr0, align 4, !tbaa !2
  %107 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr43 = getelementptr inbounds i8, i8* %107, i32 1
  store i8* %incdec.ptr43, i8** %inptr01, align 4, !tbaa !2
  %108 = load i8, i8* %107, align 1, !tbaa !39
  %conv44 = zext i8 %108 to i32
  store i32 %conv44, i32* %y, align 4, !tbaa !32
  %109 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %110 = load i32, i32* %y, align 4, !tbaa !32
  %111 = load i32, i32* %cred, align 4, !tbaa !32
  %add45 = add nsw i32 %110, %111
  %arrayidx46 = getelementptr inbounds i8, i8* %109, i32 %add45
  %112 = load i8, i8* %arrayidx46, align 1, !tbaa !39
  %113 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds i8, i8* %113, i32 0
  store i8 %112, i8* %arrayidx47, align 1, !tbaa !39
  %114 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %115 = load i32, i32* %y, align 4, !tbaa !32
  %116 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add48 = add nsw i32 %115, %116
  %arrayidx49 = getelementptr inbounds i8, i8* %114, i32 %add48
  %117 = load i8, i8* %arrayidx49, align 1, !tbaa !39
  %118 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i8, i8* %118, i32 1
  store i8 %117, i8* %arrayidx50, align 1, !tbaa !39
  %119 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %120 = load i32, i32* %y, align 4, !tbaa !32
  %121 = load i32, i32* %cblue, align 4, !tbaa !32
  %add51 = add nsw i32 %120, %121
  %arrayidx52 = getelementptr inbounds i8, i8* %119, i32 %add51
  %122 = load i8, i8* %arrayidx52, align 1, !tbaa !39
  %123 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i8, i8* %123, i32 2
  store i8 %122, i8* %arrayidx53, align 1, !tbaa !39
  %124 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr54 = getelementptr inbounds i8, i8* %124, i32 3
  store i8* %add.ptr54, i8** %outptr1, align 4, !tbaa !2
  %125 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr55 = getelementptr inbounds i8, i8* %125, i32 1
  store i8* %incdec.ptr55, i8** %inptr01, align 4, !tbaa !2
  %126 = load i8, i8* %125, align 1, !tbaa !39
  %conv56 = zext i8 %126 to i32
  store i32 %conv56, i32* %y, align 4, !tbaa !32
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !32
  %129 = load i32, i32* %cred, align 4, !tbaa !32
  %add57 = add nsw i32 %128, %129
  %arrayidx58 = getelementptr inbounds i8, i8* %127, i32 %add57
  %130 = load i8, i8* %arrayidx58, align 1, !tbaa !39
  %131 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i8, i8* %131, i32 0
  store i8 %130, i8* %arrayidx59, align 1, !tbaa !39
  %132 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %133 = load i32, i32* %y, align 4, !tbaa !32
  %134 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add60 = add nsw i32 %133, %134
  %arrayidx61 = getelementptr inbounds i8, i8* %132, i32 %add60
  %135 = load i8, i8* %arrayidx61, align 1, !tbaa !39
  %136 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds i8, i8* %136, i32 1
  store i8 %135, i8* %arrayidx62, align 1, !tbaa !39
  %137 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %138 = load i32, i32* %y, align 4, !tbaa !32
  %139 = load i32, i32* %cblue, align 4, !tbaa !32
  %add63 = add nsw i32 %138, %139
  %arrayidx64 = getelementptr inbounds i8, i8* %137, i32 %add63
  %140 = load i8, i8* %arrayidx64, align 1, !tbaa !39
  %141 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx65 = getelementptr inbounds i8, i8* %141, i32 2
  store i8 %140, i8* %arrayidx65, align 1, !tbaa !39
  %142 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr66 = getelementptr inbounds i8, i8* %142, i32 3
  store i8* %add.ptr66, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %143 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %143, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %144 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %144, i32 0, i32 27
  %145 = load i32, i32* %output_width67, align 8, !tbaa !19
  %and = and i32 %145, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %146 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %147 = load i8, i8* %146, align 1, !tbaa !39
  %conv68 = zext i8 %147 to i32
  store i32 %conv68, i32* %cb, align 4, !tbaa !32
  %148 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %149 = load i8, i8* %148, align 1, !tbaa !39
  %conv69 = zext i8 %149 to i32
  store i32 %conv69, i32* %cr, align 4, !tbaa !32
  %150 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %151 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx70 = getelementptr inbounds i32, i32* %150, i32 %151
  %152 = load i32, i32* %arrayidx70, align 4, !tbaa !32
  store i32 %152, i32* %cred, align 4, !tbaa !32
  %153 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %154 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx71 = getelementptr inbounds i32, i32* %153, i32 %154
  %155 = load i32, i32* %arrayidx71, align 4, !tbaa !37
  %156 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %157 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx72 = getelementptr inbounds i32, i32* %156, i32 %157
  %158 = load i32, i32* %arrayidx72, align 4, !tbaa !37
  %add73 = add nsw i32 %155, %158
  %shr74 = ashr i32 %add73, 16
  store i32 %shr74, i32* %cgreen, align 4, !tbaa !32
  %159 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %160 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx75 = getelementptr inbounds i32, i32* %159, i32 %160
  %161 = load i32, i32* %arrayidx75, align 4, !tbaa !32
  store i32 %161, i32* %cblue, align 4, !tbaa !32
  %162 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %163 = load i8, i8* %162, align 1, !tbaa !39
  %conv76 = zext i8 %163 to i32
  store i32 %conv76, i32* %y, align 4, !tbaa !32
  %164 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %165 = load i32, i32* %y, align 4, !tbaa !32
  %166 = load i32, i32* %cred, align 4, !tbaa !32
  %add77 = add nsw i32 %165, %166
  %arrayidx78 = getelementptr inbounds i8, i8* %164, i32 %add77
  %167 = load i8, i8* %arrayidx78, align 1, !tbaa !39
  %168 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds i8, i8* %168, i32 0
  store i8 %167, i8* %arrayidx79, align 1, !tbaa !39
  %169 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %170 = load i32, i32* %y, align 4, !tbaa !32
  %171 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add80 = add nsw i32 %170, %171
  %arrayidx81 = getelementptr inbounds i8, i8* %169, i32 %add80
  %172 = load i8, i8* %arrayidx81, align 1, !tbaa !39
  %173 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i8, i8* %173, i32 1
  store i8 %172, i8* %arrayidx82, align 1, !tbaa !39
  %174 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %175 = load i32, i32* %y, align 4, !tbaa !32
  %176 = load i32, i32* %cblue, align 4, !tbaa !32
  %add83 = add nsw i32 %175, %176
  %arrayidx84 = getelementptr inbounds i8, i8* %174, i32 %add83
  %177 = load i8, i8* %arrayidx84, align 1, !tbaa !39
  %178 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %arrayidx85 = getelementptr inbounds i8, i8* %178, i32 2
  store i8 %177, i8* %arrayidx85, align 1, !tbaa !39
  %179 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %180 = load i8, i8* %179, align 1, !tbaa !39
  %conv86 = zext i8 %180 to i32
  store i32 %conv86, i32* %y, align 4, !tbaa !32
  %181 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %182 = load i32, i32* %y, align 4, !tbaa !32
  %183 = load i32, i32* %cred, align 4, !tbaa !32
  %add87 = add nsw i32 %182, %183
  %arrayidx88 = getelementptr inbounds i8, i8* %181, i32 %add87
  %184 = load i8, i8* %arrayidx88, align 1, !tbaa !39
  %185 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i8, i8* %185, i32 0
  store i8 %184, i8* %arrayidx89, align 1, !tbaa !39
  %186 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %187 = load i32, i32* %y, align 4, !tbaa !32
  %188 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add90 = add nsw i32 %187, %188
  %arrayidx91 = getelementptr inbounds i8, i8* %186, i32 %add90
  %189 = load i8, i8* %arrayidx91, align 1, !tbaa !39
  %190 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i8, i8* %190, i32 1
  store i8 %189, i8* %arrayidx92, align 1, !tbaa !39
  %191 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %192 = load i32, i32* %y, align 4, !tbaa !32
  %193 = load i32, i32* %cblue, align 4, !tbaa !32
  %add93 = add nsw i32 %192, %193
  %arrayidx94 = getelementptr inbounds i8, i8* %191, i32 %add93
  %194 = load i8, i8* %arrayidx94, align 1, !tbaa !39
  %195 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i8, i8* %195, i32 2
  store i8 %194, i8* %arrayidx95, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %196 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #4
  %197 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #4
  %198 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #4
  %199 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #4
  %200 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #4
  %201 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #4
  %202 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #4
  %203 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #4
  %204 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #4
  %205 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #4
  %206 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #4
  %207 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #4
  %208 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #4
  %209 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #4
  %210 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #4
  %211 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #4
  %212 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #4
  %213 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #4
  %214 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal i32 @is_big_endian() #3 {
entry:
  %retval = alloca i32, align 4
  %test_value = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %0 = bitcast i32* %test_value to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  store i32 1, i32* %test_value, align 4, !tbaa !32
  %1 = bitcast i32* %test_value to i8*
  %2 = load i8, i8* %1, align 4, !tbaa !39
  %conv = sext i8 %2 to i32
  %cmp = icmp ne i32 %conv, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %3 = bitcast i32* %test_value to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #4
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v2_merged_upsample_565D_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %d0 = alloca i32, align 4
  %d1 = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %rgb = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 34
  %34 = load i32, i32* %output_scanline, align 4, !tbaa !40
  %and = and i32 %34, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %35 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %35, i32* %d0, align 4, !tbaa !37
  %36 = bitcast i32* %d1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 34
  %38 = load i32, i32* %output_scanline2, align 4, !tbaa !40
  %add = add i32 %38, 1
  %and3 = and i32 %add, 3
  %arrayidx4 = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and3
  %39 = load i32, i32* %arrayidx4, align 4, !tbaa !37
  store i32 %39, i32* %d1, align 4, !tbaa !37
  %40 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %44, i32 0
  %45 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %46, 2
  %arrayidx6 = getelementptr inbounds i8*, i8** %45, i32 %mul
  %47 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %47, i8** %inptr00, align 4, !tbaa !2
  %48 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8**, i8*** %48, i32 0
  %49 = load i8**, i8*** %arrayidx7, align 4, !tbaa !2
  %50 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul8 = mul i32 %50, 2
  %add9 = add i32 %mul8, 1
  %arrayidx10 = getelementptr inbounds i8*, i8** %49, i32 %add9
  %51 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %51, i8** %inptr01, align 4, !tbaa !2
  %52 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8**, i8*** %52, i32 1
  %53 = load i8**, i8*** %arrayidx11, align 4, !tbaa !2
  %54 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i8*, i8** %53, i32 %54
  %55 = load i8*, i8** %arrayidx12, align 4, !tbaa !2
  store i8* %55, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i8**, i8*** %56, i32 2
  %57 = load i8**, i8*** %arrayidx13, align 4, !tbaa !2
  %58 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i8*, i8** %57, i32 %58
  %59 = load i8*, i8** %arrayidx14, align 4, !tbaa !2
  store i8* %59, i8** %inptr2, align 4, !tbaa !2
  %60 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8*, i8** %60, i32 0
  %61 = load i8*, i8** %arrayidx15, align 4, !tbaa !2
  store i8* %61, i8** %outptr0, align 4, !tbaa !2
  %62 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i8*, i8** %62, i32 1
  %63 = load i8*, i8** %arrayidx16, align 4, !tbaa !2
  store i8* %63, i8** %outptr1, align 4, !tbaa !2
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 27
  %65 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %65, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %66 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %66, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %67 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %67, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %68 = load i8, i8* %67, align 1, !tbaa !39
  %conv = zext i8 %68 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %69 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr17 = getelementptr inbounds i8, i8* %69, i32 1
  store i8* %incdec.ptr17, i8** %inptr2, align 4, !tbaa !2
  %70 = load i8, i8* %69, align 1, !tbaa !39
  %conv18 = zext i8 %70 to i32
  store i32 %conv18, i32* %cr, align 4, !tbaa !32
  %71 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %72 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %71, i32 %72
  %73 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %73, i32* %cred, align 4, !tbaa !32
  %74 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %75 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx20 = getelementptr inbounds i32, i32* %74, i32 %75
  %76 = load i32, i32* %arrayidx20, align 4, !tbaa !37
  %77 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %78 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx21 = getelementptr inbounds i32, i32* %77, i32 %78
  %79 = load i32, i32* %arrayidx21, align 4, !tbaa !37
  %add22 = add nsw i32 %76, %79
  %shr23 = ashr i32 %add22, 16
  store i32 %shr23, i32* %cgreen, align 4, !tbaa !32
  %80 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %81 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx24 = getelementptr inbounds i32, i32* %80, i32 %81
  %82 = load i32, i32* %arrayidx24, align 4, !tbaa !32
  store i32 %82, i32* %cblue, align 4, !tbaa !32
  %83 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr25 = getelementptr inbounds i8, i8* %83, i32 1
  store i8* %incdec.ptr25, i8** %inptr00, align 4, !tbaa !2
  %84 = load i8, i8* %83, align 1, !tbaa !39
  %conv26 = zext i8 %84 to i32
  store i32 %conv26, i32* %y, align 4, !tbaa !32
  %85 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %86 = load i32, i32* %y, align 4, !tbaa !32
  %87 = load i32, i32* %cred, align 4, !tbaa !32
  %add27 = add nsw i32 %86, %87
  %88 = load i32, i32* %d0, align 4, !tbaa !37
  %and28 = and i32 %88, 255
  %add29 = add nsw i32 %add27, %and28
  %arrayidx30 = getelementptr inbounds i8, i8* %85, i32 %add29
  %89 = load i8, i8* %arrayidx30, align 1, !tbaa !39
  %conv31 = zext i8 %89 to i32
  store i32 %conv31, i32* %r, align 4, !tbaa !32
  %90 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %91 = load i32, i32* %y, align 4, !tbaa !32
  %92 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add32 = add nsw i32 %91, %92
  %93 = load i32, i32* %d0, align 4, !tbaa !37
  %and33 = and i32 %93, 255
  %shr34 = ashr i32 %and33, 1
  %add35 = add nsw i32 %add32, %shr34
  %arrayidx36 = getelementptr inbounds i8, i8* %90, i32 %add35
  %94 = load i8, i8* %arrayidx36, align 1, !tbaa !39
  %conv37 = zext i8 %94 to i32
  store i32 %conv37, i32* %g, align 4, !tbaa !32
  %95 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %96 = load i32, i32* %y, align 4, !tbaa !32
  %97 = load i32, i32* %cblue, align 4, !tbaa !32
  %add38 = add nsw i32 %96, %97
  %98 = load i32, i32* %d0, align 4, !tbaa !37
  %and39 = and i32 %98, 255
  %add40 = add nsw i32 %add38, %and39
  %arrayidx41 = getelementptr inbounds i8, i8* %95, i32 %add40
  %99 = load i8, i8* %arrayidx41, align 1, !tbaa !39
  %conv42 = zext i8 %99 to i32
  store i32 %conv42, i32* %b, align 4, !tbaa !32
  %100 = load i32, i32* %d0, align 4, !tbaa !37
  %and43 = and i32 %100, 255
  %shl = shl i32 %and43, 24
  %101 = load i32, i32* %d0, align 4, !tbaa !37
  %shr44 = ashr i32 %101, 8
  %and45 = and i32 %shr44, 16777215
  %or = or i32 %shl, %and45
  store i32 %or, i32* %d0, align 4, !tbaa !37
  %102 = load i32, i32* %r, align 4, !tbaa !32
  %and46 = and i32 %102, 248
  %103 = load i32, i32* %g, align 4, !tbaa !32
  %shr47 = lshr i32 %103, 5
  %or48 = or i32 %and46, %shr47
  %104 = load i32, i32* %g, align 4, !tbaa !32
  %shl49 = shl i32 %104, 11
  %and50 = and i32 %shl49, 57344
  %or51 = or i32 %or48, %and50
  %105 = load i32, i32* %b, align 4, !tbaa !32
  %shl52 = shl i32 %105, 5
  %and53 = and i32 %shl52, 7936
  %or54 = or i32 %or51, %and53
  store i32 %or54, i32* %rgb, align 4, !tbaa !37
  %106 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr55 = getelementptr inbounds i8, i8* %106, i32 1
  store i8* %incdec.ptr55, i8** %inptr00, align 4, !tbaa !2
  %107 = load i8, i8* %106, align 1, !tbaa !39
  %conv56 = zext i8 %107 to i32
  store i32 %conv56, i32* %y, align 4, !tbaa !32
  %108 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %109 = load i32, i32* %y, align 4, !tbaa !32
  %110 = load i32, i32* %cred, align 4, !tbaa !32
  %add57 = add nsw i32 %109, %110
  %111 = load i32, i32* %d1, align 4, !tbaa !37
  %and58 = and i32 %111, 255
  %add59 = add nsw i32 %add57, %and58
  %arrayidx60 = getelementptr inbounds i8, i8* %108, i32 %add59
  %112 = load i8, i8* %arrayidx60, align 1, !tbaa !39
  %conv61 = zext i8 %112 to i32
  store i32 %conv61, i32* %r, align 4, !tbaa !32
  %113 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %114 = load i32, i32* %y, align 4, !tbaa !32
  %115 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add62 = add nsw i32 %114, %115
  %116 = load i32, i32* %d1, align 4, !tbaa !37
  %and63 = and i32 %116, 255
  %shr64 = ashr i32 %and63, 1
  %add65 = add nsw i32 %add62, %shr64
  %arrayidx66 = getelementptr inbounds i8, i8* %113, i32 %add65
  %117 = load i8, i8* %arrayidx66, align 1, !tbaa !39
  %conv67 = zext i8 %117 to i32
  store i32 %conv67, i32* %g, align 4, !tbaa !32
  %118 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %119 = load i32, i32* %y, align 4, !tbaa !32
  %120 = load i32, i32* %cblue, align 4, !tbaa !32
  %add68 = add nsw i32 %119, %120
  %121 = load i32, i32* %d1, align 4, !tbaa !37
  %and69 = and i32 %121, 255
  %add70 = add nsw i32 %add68, %and69
  %arrayidx71 = getelementptr inbounds i8, i8* %118, i32 %add70
  %122 = load i8, i8* %arrayidx71, align 1, !tbaa !39
  %conv72 = zext i8 %122 to i32
  store i32 %conv72, i32* %b, align 4, !tbaa !32
  %123 = load i32, i32* %d1, align 4, !tbaa !37
  %and73 = and i32 %123, 255
  %shl74 = shl i32 %and73, 24
  %124 = load i32, i32* %d1, align 4, !tbaa !37
  %shr75 = ashr i32 %124, 8
  %and76 = and i32 %shr75, 16777215
  %or77 = or i32 %shl74, %and76
  store i32 %or77, i32* %d1, align 4, !tbaa !37
  %125 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl78 = shl i32 %125, 16
  %126 = load i32, i32* %r, align 4, !tbaa !32
  %and79 = and i32 %126, 248
  %127 = load i32, i32* %g, align 4, !tbaa !32
  %shr80 = lshr i32 %127, 5
  %or81 = or i32 %and79, %shr80
  %128 = load i32, i32* %g, align 4, !tbaa !32
  %shl82 = shl i32 %128, 11
  %and83 = and i32 %shl82, 57344
  %or84 = or i32 %or81, %and83
  %129 = load i32, i32* %b, align 4, !tbaa !32
  %shl85 = shl i32 %129, 5
  %and86 = and i32 %shl85, 7936
  %or87 = or i32 %or84, %and86
  %or88 = or i32 %shl78, %or87
  store i32 %or88, i32* %rgb, align 4, !tbaa !37
  %130 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv89 = trunc i32 %130 to i16
  %131 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %132 = bitcast i8* %131 to i16*
  %arrayidx90 = getelementptr inbounds i16, i16* %132, i32 1
  store i16 %conv89, i16* %arrayidx90, align 2, !tbaa !41
  %133 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr91 = ashr i32 %133, 16
  %conv92 = trunc i32 %shr91 to i16
  %134 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %135 = bitcast i8* %134 to i16*
  %arrayidx93 = getelementptr inbounds i16, i16* %135, i32 0
  store i16 %conv92, i16* %arrayidx93, align 2, !tbaa !41
  %136 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %136, i32 4
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %137 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr94 = getelementptr inbounds i8, i8* %137, i32 1
  store i8* %incdec.ptr94, i8** %inptr01, align 4, !tbaa !2
  %138 = load i8, i8* %137, align 1, !tbaa !39
  %conv95 = zext i8 %138 to i32
  store i32 %conv95, i32* %y, align 4, !tbaa !32
  %139 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %140 = load i32, i32* %y, align 4, !tbaa !32
  %141 = load i32, i32* %cred, align 4, !tbaa !32
  %add96 = add nsw i32 %140, %141
  %142 = load i32, i32* %d0, align 4, !tbaa !37
  %and97 = and i32 %142, 255
  %add98 = add nsw i32 %add96, %and97
  %arrayidx99 = getelementptr inbounds i8, i8* %139, i32 %add98
  %143 = load i8, i8* %arrayidx99, align 1, !tbaa !39
  %conv100 = zext i8 %143 to i32
  store i32 %conv100, i32* %r, align 4, !tbaa !32
  %144 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %145 = load i32, i32* %y, align 4, !tbaa !32
  %146 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add101 = add nsw i32 %145, %146
  %147 = load i32, i32* %d0, align 4, !tbaa !37
  %and102 = and i32 %147, 255
  %shr103 = ashr i32 %and102, 1
  %add104 = add nsw i32 %add101, %shr103
  %arrayidx105 = getelementptr inbounds i8, i8* %144, i32 %add104
  %148 = load i8, i8* %arrayidx105, align 1, !tbaa !39
  %conv106 = zext i8 %148 to i32
  store i32 %conv106, i32* %g, align 4, !tbaa !32
  %149 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %150 = load i32, i32* %y, align 4, !tbaa !32
  %151 = load i32, i32* %cblue, align 4, !tbaa !32
  %add107 = add nsw i32 %150, %151
  %152 = load i32, i32* %d0, align 4, !tbaa !37
  %and108 = and i32 %152, 255
  %add109 = add nsw i32 %add107, %and108
  %arrayidx110 = getelementptr inbounds i8, i8* %149, i32 %add109
  %153 = load i8, i8* %arrayidx110, align 1, !tbaa !39
  %conv111 = zext i8 %153 to i32
  store i32 %conv111, i32* %b, align 4, !tbaa !32
  %154 = load i32, i32* %d0, align 4, !tbaa !37
  %and112 = and i32 %154, 255
  %shl113 = shl i32 %and112, 24
  %155 = load i32, i32* %d0, align 4, !tbaa !37
  %shr114 = ashr i32 %155, 8
  %and115 = and i32 %shr114, 16777215
  %or116 = or i32 %shl113, %and115
  store i32 %or116, i32* %d0, align 4, !tbaa !37
  %156 = load i32, i32* %r, align 4, !tbaa !32
  %and117 = and i32 %156, 248
  %157 = load i32, i32* %g, align 4, !tbaa !32
  %shr118 = lshr i32 %157, 5
  %or119 = or i32 %and117, %shr118
  %158 = load i32, i32* %g, align 4, !tbaa !32
  %shl120 = shl i32 %158, 11
  %and121 = and i32 %shl120, 57344
  %or122 = or i32 %or119, %and121
  %159 = load i32, i32* %b, align 4, !tbaa !32
  %shl123 = shl i32 %159, 5
  %and124 = and i32 %shl123, 7936
  %or125 = or i32 %or122, %and124
  store i32 %or125, i32* %rgb, align 4, !tbaa !37
  %160 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr126 = getelementptr inbounds i8, i8* %160, i32 1
  store i8* %incdec.ptr126, i8** %inptr01, align 4, !tbaa !2
  %161 = load i8, i8* %160, align 1, !tbaa !39
  %conv127 = zext i8 %161 to i32
  store i32 %conv127, i32* %y, align 4, !tbaa !32
  %162 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %163 = load i32, i32* %y, align 4, !tbaa !32
  %164 = load i32, i32* %cred, align 4, !tbaa !32
  %add128 = add nsw i32 %163, %164
  %165 = load i32, i32* %d1, align 4, !tbaa !37
  %and129 = and i32 %165, 255
  %add130 = add nsw i32 %add128, %and129
  %arrayidx131 = getelementptr inbounds i8, i8* %162, i32 %add130
  %166 = load i8, i8* %arrayidx131, align 1, !tbaa !39
  %conv132 = zext i8 %166 to i32
  store i32 %conv132, i32* %r, align 4, !tbaa !32
  %167 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %168 = load i32, i32* %y, align 4, !tbaa !32
  %169 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add133 = add nsw i32 %168, %169
  %170 = load i32, i32* %d1, align 4, !tbaa !37
  %and134 = and i32 %170, 255
  %shr135 = ashr i32 %and134, 1
  %add136 = add nsw i32 %add133, %shr135
  %arrayidx137 = getelementptr inbounds i8, i8* %167, i32 %add136
  %171 = load i8, i8* %arrayidx137, align 1, !tbaa !39
  %conv138 = zext i8 %171 to i32
  store i32 %conv138, i32* %g, align 4, !tbaa !32
  %172 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %173 = load i32, i32* %y, align 4, !tbaa !32
  %174 = load i32, i32* %cblue, align 4, !tbaa !32
  %add139 = add nsw i32 %173, %174
  %175 = load i32, i32* %d1, align 4, !tbaa !37
  %and140 = and i32 %175, 255
  %add141 = add nsw i32 %add139, %and140
  %arrayidx142 = getelementptr inbounds i8, i8* %172, i32 %add141
  %176 = load i8, i8* %arrayidx142, align 1, !tbaa !39
  %conv143 = zext i8 %176 to i32
  store i32 %conv143, i32* %b, align 4, !tbaa !32
  %177 = load i32, i32* %d1, align 4, !tbaa !37
  %and144 = and i32 %177, 255
  %shl145 = shl i32 %and144, 24
  %178 = load i32, i32* %d1, align 4, !tbaa !37
  %shr146 = ashr i32 %178, 8
  %and147 = and i32 %shr146, 16777215
  %or148 = or i32 %shl145, %and147
  store i32 %or148, i32* %d1, align 4, !tbaa !37
  %179 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl149 = shl i32 %179, 16
  %180 = load i32, i32* %r, align 4, !tbaa !32
  %and150 = and i32 %180, 248
  %181 = load i32, i32* %g, align 4, !tbaa !32
  %shr151 = lshr i32 %181, 5
  %or152 = or i32 %and150, %shr151
  %182 = load i32, i32* %g, align 4, !tbaa !32
  %shl153 = shl i32 %182, 11
  %and154 = and i32 %shl153, 57344
  %or155 = or i32 %or152, %and154
  %183 = load i32, i32* %b, align 4, !tbaa !32
  %shl156 = shl i32 %183, 5
  %and157 = and i32 %shl156, 7936
  %or158 = or i32 %or155, %and157
  %or159 = or i32 %shl149, %or158
  store i32 %or159, i32* %rgb, align 4, !tbaa !37
  %184 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv160 = trunc i32 %184 to i16
  %185 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %186 = bitcast i8* %185 to i16*
  %arrayidx161 = getelementptr inbounds i16, i16* %186, i32 1
  store i16 %conv160, i16* %arrayidx161, align 2, !tbaa !41
  %187 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr162 = ashr i32 %187, 16
  %conv163 = trunc i32 %shr162 to i16
  %188 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %189 = bitcast i8* %188 to i16*
  %arrayidx164 = getelementptr inbounds i16, i16* %189, i32 0
  store i16 %conv163, i16* %arrayidx164, align 2, !tbaa !41
  %190 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr165 = getelementptr inbounds i8, i8* %190, i32 4
  store i8* %add.ptr165, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %191 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %191, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %192 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width166 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %192, i32 0, i32 27
  %193 = load i32, i32* %output_width166, align 8, !tbaa !19
  %and167 = and i32 %193, 1
  %tobool = icmp ne i32 %and167, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %194 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %195 = load i8, i8* %194, align 1, !tbaa !39
  %conv168 = zext i8 %195 to i32
  store i32 %conv168, i32* %cb, align 4, !tbaa !32
  %196 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %197 = load i8, i8* %196, align 1, !tbaa !39
  %conv169 = zext i8 %197 to i32
  store i32 %conv169, i32* %cr, align 4, !tbaa !32
  %198 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %199 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx170 = getelementptr inbounds i32, i32* %198, i32 %199
  %200 = load i32, i32* %arrayidx170, align 4, !tbaa !32
  store i32 %200, i32* %cred, align 4, !tbaa !32
  %201 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %202 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx171 = getelementptr inbounds i32, i32* %201, i32 %202
  %203 = load i32, i32* %arrayidx171, align 4, !tbaa !37
  %204 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %205 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx172 = getelementptr inbounds i32, i32* %204, i32 %205
  %206 = load i32, i32* %arrayidx172, align 4, !tbaa !37
  %add173 = add nsw i32 %203, %206
  %shr174 = ashr i32 %add173, 16
  store i32 %shr174, i32* %cgreen, align 4, !tbaa !32
  %207 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %208 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx175 = getelementptr inbounds i32, i32* %207, i32 %208
  %209 = load i32, i32* %arrayidx175, align 4, !tbaa !32
  store i32 %209, i32* %cblue, align 4, !tbaa !32
  %210 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %211 = load i8, i8* %210, align 1, !tbaa !39
  %conv176 = zext i8 %211 to i32
  store i32 %conv176, i32* %y, align 4, !tbaa !32
  %212 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %213 = load i32, i32* %y, align 4, !tbaa !32
  %214 = load i32, i32* %cred, align 4, !tbaa !32
  %add177 = add nsw i32 %213, %214
  %215 = load i32, i32* %d0, align 4, !tbaa !37
  %and178 = and i32 %215, 255
  %add179 = add nsw i32 %add177, %and178
  %arrayidx180 = getelementptr inbounds i8, i8* %212, i32 %add179
  %216 = load i8, i8* %arrayidx180, align 1, !tbaa !39
  %conv181 = zext i8 %216 to i32
  store i32 %conv181, i32* %r, align 4, !tbaa !32
  %217 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %218 = load i32, i32* %y, align 4, !tbaa !32
  %219 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add182 = add nsw i32 %218, %219
  %220 = load i32, i32* %d0, align 4, !tbaa !37
  %and183 = and i32 %220, 255
  %shr184 = ashr i32 %and183, 1
  %add185 = add nsw i32 %add182, %shr184
  %arrayidx186 = getelementptr inbounds i8, i8* %217, i32 %add185
  %221 = load i8, i8* %arrayidx186, align 1, !tbaa !39
  %conv187 = zext i8 %221 to i32
  store i32 %conv187, i32* %g, align 4, !tbaa !32
  %222 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %223 = load i32, i32* %y, align 4, !tbaa !32
  %224 = load i32, i32* %cblue, align 4, !tbaa !32
  %add188 = add nsw i32 %223, %224
  %225 = load i32, i32* %d0, align 4, !tbaa !37
  %and189 = and i32 %225, 255
  %add190 = add nsw i32 %add188, %and189
  %arrayidx191 = getelementptr inbounds i8, i8* %222, i32 %add190
  %226 = load i8, i8* %arrayidx191, align 1, !tbaa !39
  %conv192 = zext i8 %226 to i32
  store i32 %conv192, i32* %b, align 4, !tbaa !32
  %227 = load i32, i32* %r, align 4, !tbaa !32
  %and193 = and i32 %227, 248
  %228 = load i32, i32* %g, align 4, !tbaa !32
  %shr194 = lshr i32 %228, 5
  %or195 = or i32 %and193, %shr194
  %229 = load i32, i32* %g, align 4, !tbaa !32
  %shl196 = shl i32 %229, 11
  %and197 = and i32 %shl196, 57344
  %or198 = or i32 %or195, %and197
  %230 = load i32, i32* %b, align 4, !tbaa !32
  %shl199 = shl i32 %230, 5
  %and200 = and i32 %shl199, 7936
  %or201 = or i32 %or198, %and200
  store i32 %or201, i32* %rgb, align 4, !tbaa !37
  %231 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv202 = trunc i32 %231 to i16
  %232 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %233 = bitcast i8* %232 to i16*
  store i16 %conv202, i16* %233, align 2, !tbaa !41
  %234 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %235 = load i8, i8* %234, align 1, !tbaa !39
  %conv203 = zext i8 %235 to i32
  store i32 %conv203, i32* %y, align 4, !tbaa !32
  %236 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %237 = load i32, i32* %y, align 4, !tbaa !32
  %238 = load i32, i32* %cred, align 4, !tbaa !32
  %add204 = add nsw i32 %237, %238
  %239 = load i32, i32* %d1, align 4, !tbaa !37
  %and205 = and i32 %239, 255
  %add206 = add nsw i32 %add204, %and205
  %arrayidx207 = getelementptr inbounds i8, i8* %236, i32 %add206
  %240 = load i8, i8* %arrayidx207, align 1, !tbaa !39
  %conv208 = zext i8 %240 to i32
  store i32 %conv208, i32* %r, align 4, !tbaa !32
  %241 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %242 = load i32, i32* %y, align 4, !tbaa !32
  %243 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add209 = add nsw i32 %242, %243
  %244 = load i32, i32* %d1, align 4, !tbaa !37
  %and210 = and i32 %244, 255
  %shr211 = ashr i32 %and210, 1
  %add212 = add nsw i32 %add209, %shr211
  %arrayidx213 = getelementptr inbounds i8, i8* %241, i32 %add212
  %245 = load i8, i8* %arrayidx213, align 1, !tbaa !39
  %conv214 = zext i8 %245 to i32
  store i32 %conv214, i32* %g, align 4, !tbaa !32
  %246 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %247 = load i32, i32* %y, align 4, !tbaa !32
  %248 = load i32, i32* %cblue, align 4, !tbaa !32
  %add215 = add nsw i32 %247, %248
  %249 = load i32, i32* %d1, align 4, !tbaa !37
  %and216 = and i32 %249, 255
  %add217 = add nsw i32 %add215, %and216
  %arrayidx218 = getelementptr inbounds i8, i8* %246, i32 %add217
  %250 = load i8, i8* %arrayidx218, align 1, !tbaa !39
  %conv219 = zext i8 %250 to i32
  store i32 %conv219, i32* %b, align 4, !tbaa !32
  %251 = load i32, i32* %r, align 4, !tbaa !32
  %and220 = and i32 %251, 248
  %252 = load i32, i32* %g, align 4, !tbaa !32
  %shr221 = lshr i32 %252, 5
  %or222 = or i32 %and220, %shr221
  %253 = load i32, i32* %g, align 4, !tbaa !32
  %shl223 = shl i32 %253, 11
  %and224 = and i32 %shl223, 57344
  %or225 = or i32 %or222, %and224
  %254 = load i32, i32* %b, align 4, !tbaa !32
  %shl226 = shl i32 %254, 5
  %and227 = and i32 %shl226, 7936
  %or228 = or i32 %or225, %and227
  store i32 %or228, i32* %rgb, align 4, !tbaa !37
  %255 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv229 = trunc i32 %255 to i16
  %256 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %257 = bitcast i8* %256 to i16*
  store i16 %conv229, i16* %257, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %258 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #4
  %259 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #4
  %260 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #4
  %261 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #4
  %262 = bitcast i32* %d1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #4
  %263 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #4
  %264 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #4
  %265 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #4
  %266 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #4
  %267 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #4
  %268 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #4
  %269 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #4
  %270 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %270) #4
  %271 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %271) #4
  %272 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #4
  %273 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #4
  %274 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #4
  %275 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #4
  %276 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #4
  %277 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %277) #4
  %278 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #4
  %279 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #4
  %280 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #4
  %281 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %281) #4
  %282 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v2_merged_upsample_565D_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %d0 = alloca i32, align 4
  %d1 = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %rgb = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %33, i32 0, i32 34
  %34 = load i32, i32* %output_scanline, align 4, !tbaa !40
  %and = and i32 %34, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %35 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %35, i32* %d0, align 4, !tbaa !37
  %36 = bitcast i32* %d1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 34
  %38 = load i32, i32* %output_scanline2, align 4, !tbaa !40
  %add = add i32 %38, 1
  %and3 = and i32 %add, 3
  %arrayidx4 = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and3
  %39 = load i32, i32* %arrayidx4, align 4, !tbaa !37
  store i32 %39, i32* %d1, align 4, !tbaa !37
  %40 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #4
  %41 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #4
  %42 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #4
  %43 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #4
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %44, i32 0
  %45 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %46, 2
  %arrayidx6 = getelementptr inbounds i8*, i8** %45, i32 %mul
  %47 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %47, i8** %inptr00, align 4, !tbaa !2
  %48 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8**, i8*** %48, i32 0
  %49 = load i8**, i8*** %arrayidx7, align 4, !tbaa !2
  %50 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul8 = mul i32 %50, 2
  %add9 = add i32 %mul8, 1
  %arrayidx10 = getelementptr inbounds i8*, i8** %49, i32 %add9
  %51 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %51, i8** %inptr01, align 4, !tbaa !2
  %52 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8**, i8*** %52, i32 1
  %53 = load i8**, i8*** %arrayidx11, align 4, !tbaa !2
  %54 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i8*, i8** %53, i32 %54
  %55 = load i8*, i8** %arrayidx12, align 4, !tbaa !2
  store i8* %55, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i8**, i8*** %56, i32 2
  %57 = load i8**, i8*** %arrayidx13, align 4, !tbaa !2
  %58 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i8*, i8** %57, i32 %58
  %59 = load i8*, i8** %arrayidx14, align 4, !tbaa !2
  store i8* %59, i8** %inptr2, align 4, !tbaa !2
  %60 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx15 = getelementptr inbounds i8*, i8** %60, i32 0
  %61 = load i8*, i8** %arrayidx15, align 4, !tbaa !2
  store i8* %61, i8** %outptr0, align 4, !tbaa !2
  %62 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i8*, i8** %62, i32 1
  %63 = load i8*, i8** %arrayidx16, align 4, !tbaa !2
  store i8* %63, i8** %outptr1, align 4, !tbaa !2
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 27
  %65 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %65, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %66 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %66, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %67 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %67, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %68 = load i8, i8* %67, align 1, !tbaa !39
  %conv = zext i8 %68 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %69 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr17 = getelementptr inbounds i8, i8* %69, i32 1
  store i8* %incdec.ptr17, i8** %inptr2, align 4, !tbaa !2
  %70 = load i8, i8* %69, align 1, !tbaa !39
  %conv18 = zext i8 %70 to i32
  store i32 %conv18, i32* %cr, align 4, !tbaa !32
  %71 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %72 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %71, i32 %72
  %73 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %73, i32* %cred, align 4, !tbaa !32
  %74 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %75 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx20 = getelementptr inbounds i32, i32* %74, i32 %75
  %76 = load i32, i32* %arrayidx20, align 4, !tbaa !37
  %77 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %78 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx21 = getelementptr inbounds i32, i32* %77, i32 %78
  %79 = load i32, i32* %arrayidx21, align 4, !tbaa !37
  %add22 = add nsw i32 %76, %79
  %shr23 = ashr i32 %add22, 16
  store i32 %shr23, i32* %cgreen, align 4, !tbaa !32
  %80 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %81 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx24 = getelementptr inbounds i32, i32* %80, i32 %81
  %82 = load i32, i32* %arrayidx24, align 4, !tbaa !32
  store i32 %82, i32* %cblue, align 4, !tbaa !32
  %83 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr25 = getelementptr inbounds i8, i8* %83, i32 1
  store i8* %incdec.ptr25, i8** %inptr00, align 4, !tbaa !2
  %84 = load i8, i8* %83, align 1, !tbaa !39
  %conv26 = zext i8 %84 to i32
  store i32 %conv26, i32* %y, align 4, !tbaa !32
  %85 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %86 = load i32, i32* %y, align 4, !tbaa !32
  %87 = load i32, i32* %cred, align 4, !tbaa !32
  %add27 = add nsw i32 %86, %87
  %88 = load i32, i32* %d0, align 4, !tbaa !37
  %and28 = and i32 %88, 255
  %add29 = add nsw i32 %add27, %and28
  %arrayidx30 = getelementptr inbounds i8, i8* %85, i32 %add29
  %89 = load i8, i8* %arrayidx30, align 1, !tbaa !39
  %conv31 = zext i8 %89 to i32
  store i32 %conv31, i32* %r, align 4, !tbaa !32
  %90 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %91 = load i32, i32* %y, align 4, !tbaa !32
  %92 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add32 = add nsw i32 %91, %92
  %93 = load i32, i32* %d0, align 4, !tbaa !37
  %and33 = and i32 %93, 255
  %shr34 = ashr i32 %and33, 1
  %add35 = add nsw i32 %add32, %shr34
  %arrayidx36 = getelementptr inbounds i8, i8* %90, i32 %add35
  %94 = load i8, i8* %arrayidx36, align 1, !tbaa !39
  %conv37 = zext i8 %94 to i32
  store i32 %conv37, i32* %g, align 4, !tbaa !32
  %95 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %96 = load i32, i32* %y, align 4, !tbaa !32
  %97 = load i32, i32* %cblue, align 4, !tbaa !32
  %add38 = add nsw i32 %96, %97
  %98 = load i32, i32* %d0, align 4, !tbaa !37
  %and39 = and i32 %98, 255
  %add40 = add nsw i32 %add38, %and39
  %arrayidx41 = getelementptr inbounds i8, i8* %95, i32 %add40
  %99 = load i8, i8* %arrayidx41, align 1, !tbaa !39
  %conv42 = zext i8 %99 to i32
  store i32 %conv42, i32* %b, align 4, !tbaa !32
  %100 = load i32, i32* %d0, align 4, !tbaa !37
  %and43 = and i32 %100, 255
  %shl = shl i32 %and43, 24
  %101 = load i32, i32* %d0, align 4, !tbaa !37
  %shr44 = ashr i32 %101, 8
  %and45 = and i32 %shr44, 16777215
  %or = or i32 %shl, %and45
  store i32 %or, i32* %d0, align 4, !tbaa !37
  %102 = load i32, i32* %r, align 4, !tbaa !32
  %shl46 = shl i32 %102, 8
  %and47 = and i32 %shl46, 63488
  %103 = load i32, i32* %g, align 4, !tbaa !32
  %shl48 = shl i32 %103, 3
  %and49 = and i32 %shl48, 2016
  %or50 = or i32 %and47, %and49
  %104 = load i32, i32* %b, align 4, !tbaa !32
  %shr51 = lshr i32 %104, 3
  %or52 = or i32 %or50, %shr51
  store i32 %or52, i32* %rgb, align 4, !tbaa !37
  %105 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr53 = getelementptr inbounds i8, i8* %105, i32 1
  store i8* %incdec.ptr53, i8** %inptr00, align 4, !tbaa !2
  %106 = load i8, i8* %105, align 1, !tbaa !39
  %conv54 = zext i8 %106 to i32
  store i32 %conv54, i32* %y, align 4, !tbaa !32
  %107 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %108 = load i32, i32* %y, align 4, !tbaa !32
  %109 = load i32, i32* %cred, align 4, !tbaa !32
  %add55 = add nsw i32 %108, %109
  %110 = load i32, i32* %d1, align 4, !tbaa !37
  %and56 = and i32 %110, 255
  %add57 = add nsw i32 %add55, %and56
  %arrayidx58 = getelementptr inbounds i8, i8* %107, i32 %add57
  %111 = load i8, i8* %arrayidx58, align 1, !tbaa !39
  %conv59 = zext i8 %111 to i32
  store i32 %conv59, i32* %r, align 4, !tbaa !32
  %112 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %113 = load i32, i32* %y, align 4, !tbaa !32
  %114 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add60 = add nsw i32 %113, %114
  %115 = load i32, i32* %d1, align 4, !tbaa !37
  %and61 = and i32 %115, 255
  %shr62 = ashr i32 %and61, 1
  %add63 = add nsw i32 %add60, %shr62
  %arrayidx64 = getelementptr inbounds i8, i8* %112, i32 %add63
  %116 = load i8, i8* %arrayidx64, align 1, !tbaa !39
  %conv65 = zext i8 %116 to i32
  store i32 %conv65, i32* %g, align 4, !tbaa !32
  %117 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %118 = load i32, i32* %y, align 4, !tbaa !32
  %119 = load i32, i32* %cblue, align 4, !tbaa !32
  %add66 = add nsw i32 %118, %119
  %120 = load i32, i32* %d1, align 4, !tbaa !37
  %and67 = and i32 %120, 255
  %add68 = add nsw i32 %add66, %and67
  %arrayidx69 = getelementptr inbounds i8, i8* %117, i32 %add68
  %121 = load i8, i8* %arrayidx69, align 1, !tbaa !39
  %conv70 = zext i8 %121 to i32
  store i32 %conv70, i32* %b, align 4, !tbaa !32
  %122 = load i32, i32* %d1, align 4, !tbaa !37
  %and71 = and i32 %122, 255
  %shl72 = shl i32 %and71, 24
  %123 = load i32, i32* %d1, align 4, !tbaa !37
  %shr73 = ashr i32 %123, 8
  %and74 = and i32 %shr73, 16777215
  %or75 = or i32 %shl72, %and74
  store i32 %or75, i32* %d1, align 4, !tbaa !37
  %124 = load i32, i32* %r, align 4, !tbaa !32
  %shl76 = shl i32 %124, 8
  %and77 = and i32 %shl76, 63488
  %125 = load i32, i32* %g, align 4, !tbaa !32
  %shl78 = shl i32 %125, 3
  %and79 = and i32 %shl78, 2016
  %or80 = or i32 %and77, %and79
  %126 = load i32, i32* %b, align 4, !tbaa !32
  %shr81 = lshr i32 %126, 3
  %or82 = or i32 %or80, %shr81
  %shl83 = shl i32 %or82, 16
  %127 = load i32, i32* %rgb, align 4, !tbaa !37
  %or84 = or i32 %shl83, %127
  store i32 %or84, i32* %rgb, align 4, !tbaa !37
  %128 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv85 = trunc i32 %128 to i16
  %129 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %130 = bitcast i8* %129 to i16*
  %arrayidx86 = getelementptr inbounds i16, i16* %130, i32 0
  store i16 %conv85, i16* %arrayidx86, align 2, !tbaa !41
  %131 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr87 = ashr i32 %131, 16
  %conv88 = trunc i32 %shr87 to i16
  %132 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %133 = bitcast i8* %132 to i16*
  %arrayidx89 = getelementptr inbounds i16, i16* %133, i32 1
  store i16 %conv88, i16* %arrayidx89, align 2, !tbaa !41
  %134 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %134, i32 4
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %135 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr90 = getelementptr inbounds i8, i8* %135, i32 1
  store i8* %incdec.ptr90, i8** %inptr01, align 4, !tbaa !2
  %136 = load i8, i8* %135, align 1, !tbaa !39
  %conv91 = zext i8 %136 to i32
  store i32 %conv91, i32* %y, align 4, !tbaa !32
  %137 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %138 = load i32, i32* %y, align 4, !tbaa !32
  %139 = load i32, i32* %cred, align 4, !tbaa !32
  %add92 = add nsw i32 %138, %139
  %140 = load i32, i32* %d0, align 4, !tbaa !37
  %and93 = and i32 %140, 255
  %add94 = add nsw i32 %add92, %and93
  %arrayidx95 = getelementptr inbounds i8, i8* %137, i32 %add94
  %141 = load i8, i8* %arrayidx95, align 1, !tbaa !39
  %conv96 = zext i8 %141 to i32
  store i32 %conv96, i32* %r, align 4, !tbaa !32
  %142 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %143 = load i32, i32* %y, align 4, !tbaa !32
  %144 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add97 = add nsw i32 %143, %144
  %145 = load i32, i32* %d0, align 4, !tbaa !37
  %and98 = and i32 %145, 255
  %shr99 = ashr i32 %and98, 1
  %add100 = add nsw i32 %add97, %shr99
  %arrayidx101 = getelementptr inbounds i8, i8* %142, i32 %add100
  %146 = load i8, i8* %arrayidx101, align 1, !tbaa !39
  %conv102 = zext i8 %146 to i32
  store i32 %conv102, i32* %g, align 4, !tbaa !32
  %147 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %148 = load i32, i32* %y, align 4, !tbaa !32
  %149 = load i32, i32* %cblue, align 4, !tbaa !32
  %add103 = add nsw i32 %148, %149
  %150 = load i32, i32* %d0, align 4, !tbaa !37
  %and104 = and i32 %150, 255
  %add105 = add nsw i32 %add103, %and104
  %arrayidx106 = getelementptr inbounds i8, i8* %147, i32 %add105
  %151 = load i8, i8* %arrayidx106, align 1, !tbaa !39
  %conv107 = zext i8 %151 to i32
  store i32 %conv107, i32* %b, align 4, !tbaa !32
  %152 = load i32, i32* %d0, align 4, !tbaa !37
  %and108 = and i32 %152, 255
  %shl109 = shl i32 %and108, 24
  %153 = load i32, i32* %d0, align 4, !tbaa !37
  %shr110 = ashr i32 %153, 8
  %and111 = and i32 %shr110, 16777215
  %or112 = or i32 %shl109, %and111
  store i32 %or112, i32* %d0, align 4, !tbaa !37
  %154 = load i32, i32* %r, align 4, !tbaa !32
  %shl113 = shl i32 %154, 8
  %and114 = and i32 %shl113, 63488
  %155 = load i32, i32* %g, align 4, !tbaa !32
  %shl115 = shl i32 %155, 3
  %and116 = and i32 %shl115, 2016
  %or117 = or i32 %and114, %and116
  %156 = load i32, i32* %b, align 4, !tbaa !32
  %shr118 = lshr i32 %156, 3
  %or119 = or i32 %or117, %shr118
  store i32 %or119, i32* %rgb, align 4, !tbaa !37
  %157 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr120 = getelementptr inbounds i8, i8* %157, i32 1
  store i8* %incdec.ptr120, i8** %inptr01, align 4, !tbaa !2
  %158 = load i8, i8* %157, align 1, !tbaa !39
  %conv121 = zext i8 %158 to i32
  store i32 %conv121, i32* %y, align 4, !tbaa !32
  %159 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %160 = load i32, i32* %y, align 4, !tbaa !32
  %161 = load i32, i32* %cred, align 4, !tbaa !32
  %add122 = add nsw i32 %160, %161
  %162 = load i32, i32* %d1, align 4, !tbaa !37
  %and123 = and i32 %162, 255
  %add124 = add nsw i32 %add122, %and123
  %arrayidx125 = getelementptr inbounds i8, i8* %159, i32 %add124
  %163 = load i8, i8* %arrayidx125, align 1, !tbaa !39
  %conv126 = zext i8 %163 to i32
  store i32 %conv126, i32* %r, align 4, !tbaa !32
  %164 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %165 = load i32, i32* %y, align 4, !tbaa !32
  %166 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add127 = add nsw i32 %165, %166
  %167 = load i32, i32* %d1, align 4, !tbaa !37
  %and128 = and i32 %167, 255
  %shr129 = ashr i32 %and128, 1
  %add130 = add nsw i32 %add127, %shr129
  %arrayidx131 = getelementptr inbounds i8, i8* %164, i32 %add130
  %168 = load i8, i8* %arrayidx131, align 1, !tbaa !39
  %conv132 = zext i8 %168 to i32
  store i32 %conv132, i32* %g, align 4, !tbaa !32
  %169 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %170 = load i32, i32* %y, align 4, !tbaa !32
  %171 = load i32, i32* %cblue, align 4, !tbaa !32
  %add133 = add nsw i32 %170, %171
  %172 = load i32, i32* %d1, align 4, !tbaa !37
  %and134 = and i32 %172, 255
  %add135 = add nsw i32 %add133, %and134
  %arrayidx136 = getelementptr inbounds i8, i8* %169, i32 %add135
  %173 = load i8, i8* %arrayidx136, align 1, !tbaa !39
  %conv137 = zext i8 %173 to i32
  store i32 %conv137, i32* %b, align 4, !tbaa !32
  %174 = load i32, i32* %d1, align 4, !tbaa !37
  %and138 = and i32 %174, 255
  %shl139 = shl i32 %and138, 24
  %175 = load i32, i32* %d1, align 4, !tbaa !37
  %shr140 = ashr i32 %175, 8
  %and141 = and i32 %shr140, 16777215
  %or142 = or i32 %shl139, %and141
  store i32 %or142, i32* %d1, align 4, !tbaa !37
  %176 = load i32, i32* %r, align 4, !tbaa !32
  %shl143 = shl i32 %176, 8
  %and144 = and i32 %shl143, 63488
  %177 = load i32, i32* %g, align 4, !tbaa !32
  %shl145 = shl i32 %177, 3
  %and146 = and i32 %shl145, 2016
  %or147 = or i32 %and144, %and146
  %178 = load i32, i32* %b, align 4, !tbaa !32
  %shr148 = lshr i32 %178, 3
  %or149 = or i32 %or147, %shr148
  %shl150 = shl i32 %or149, 16
  %179 = load i32, i32* %rgb, align 4, !tbaa !37
  %or151 = or i32 %shl150, %179
  store i32 %or151, i32* %rgb, align 4, !tbaa !37
  %180 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv152 = trunc i32 %180 to i16
  %181 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %182 = bitcast i8* %181 to i16*
  %arrayidx153 = getelementptr inbounds i16, i16* %182, i32 0
  store i16 %conv152, i16* %arrayidx153, align 2, !tbaa !41
  %183 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr154 = ashr i32 %183, 16
  %conv155 = trunc i32 %shr154 to i16
  %184 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %185 = bitcast i8* %184 to i16*
  %arrayidx156 = getelementptr inbounds i16, i16* %185, i32 1
  store i16 %conv155, i16* %arrayidx156, align 2, !tbaa !41
  %186 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr157 = getelementptr inbounds i8, i8* %186, i32 4
  store i8* %add.ptr157, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %187 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %187, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %188 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width158 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %188, i32 0, i32 27
  %189 = load i32, i32* %output_width158, align 8, !tbaa !19
  %and159 = and i32 %189, 1
  %tobool = icmp ne i32 %and159, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %190 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %191 = load i8, i8* %190, align 1, !tbaa !39
  %conv160 = zext i8 %191 to i32
  store i32 %conv160, i32* %cb, align 4, !tbaa !32
  %192 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %193 = load i8, i8* %192, align 1, !tbaa !39
  %conv161 = zext i8 %193 to i32
  store i32 %conv161, i32* %cr, align 4, !tbaa !32
  %194 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %195 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx162 = getelementptr inbounds i32, i32* %194, i32 %195
  %196 = load i32, i32* %arrayidx162, align 4, !tbaa !32
  store i32 %196, i32* %cred, align 4, !tbaa !32
  %197 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %198 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx163 = getelementptr inbounds i32, i32* %197, i32 %198
  %199 = load i32, i32* %arrayidx163, align 4, !tbaa !37
  %200 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %201 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx164 = getelementptr inbounds i32, i32* %200, i32 %201
  %202 = load i32, i32* %arrayidx164, align 4, !tbaa !37
  %add165 = add nsw i32 %199, %202
  %shr166 = ashr i32 %add165, 16
  store i32 %shr166, i32* %cgreen, align 4, !tbaa !32
  %203 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %204 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx167 = getelementptr inbounds i32, i32* %203, i32 %204
  %205 = load i32, i32* %arrayidx167, align 4, !tbaa !32
  store i32 %205, i32* %cblue, align 4, !tbaa !32
  %206 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %207 = load i8, i8* %206, align 1, !tbaa !39
  %conv168 = zext i8 %207 to i32
  store i32 %conv168, i32* %y, align 4, !tbaa !32
  %208 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %209 = load i32, i32* %y, align 4, !tbaa !32
  %210 = load i32, i32* %cred, align 4, !tbaa !32
  %add169 = add nsw i32 %209, %210
  %211 = load i32, i32* %d0, align 4, !tbaa !37
  %and170 = and i32 %211, 255
  %add171 = add nsw i32 %add169, %and170
  %arrayidx172 = getelementptr inbounds i8, i8* %208, i32 %add171
  %212 = load i8, i8* %arrayidx172, align 1, !tbaa !39
  %conv173 = zext i8 %212 to i32
  store i32 %conv173, i32* %r, align 4, !tbaa !32
  %213 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %214 = load i32, i32* %y, align 4, !tbaa !32
  %215 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add174 = add nsw i32 %214, %215
  %216 = load i32, i32* %d0, align 4, !tbaa !37
  %and175 = and i32 %216, 255
  %shr176 = ashr i32 %and175, 1
  %add177 = add nsw i32 %add174, %shr176
  %arrayidx178 = getelementptr inbounds i8, i8* %213, i32 %add177
  %217 = load i8, i8* %arrayidx178, align 1, !tbaa !39
  %conv179 = zext i8 %217 to i32
  store i32 %conv179, i32* %g, align 4, !tbaa !32
  %218 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %219 = load i32, i32* %y, align 4, !tbaa !32
  %220 = load i32, i32* %cblue, align 4, !tbaa !32
  %add180 = add nsw i32 %219, %220
  %221 = load i32, i32* %d0, align 4, !tbaa !37
  %and181 = and i32 %221, 255
  %add182 = add nsw i32 %add180, %and181
  %arrayidx183 = getelementptr inbounds i8, i8* %218, i32 %add182
  %222 = load i8, i8* %arrayidx183, align 1, !tbaa !39
  %conv184 = zext i8 %222 to i32
  store i32 %conv184, i32* %b, align 4, !tbaa !32
  %223 = load i32, i32* %r, align 4, !tbaa !32
  %shl185 = shl i32 %223, 8
  %and186 = and i32 %shl185, 63488
  %224 = load i32, i32* %g, align 4, !tbaa !32
  %shl187 = shl i32 %224, 3
  %and188 = and i32 %shl187, 2016
  %or189 = or i32 %and186, %and188
  %225 = load i32, i32* %b, align 4, !tbaa !32
  %shr190 = lshr i32 %225, 3
  %or191 = or i32 %or189, %shr190
  store i32 %or191, i32* %rgb, align 4, !tbaa !37
  %226 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv192 = trunc i32 %226 to i16
  %227 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %228 = bitcast i8* %227 to i16*
  store i16 %conv192, i16* %228, align 2, !tbaa !41
  %229 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %230 = load i8, i8* %229, align 1, !tbaa !39
  %conv193 = zext i8 %230 to i32
  store i32 %conv193, i32* %y, align 4, !tbaa !32
  %231 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %232 = load i32, i32* %y, align 4, !tbaa !32
  %233 = load i32, i32* %cred, align 4, !tbaa !32
  %add194 = add nsw i32 %232, %233
  %234 = load i32, i32* %d1, align 4, !tbaa !37
  %and195 = and i32 %234, 255
  %add196 = add nsw i32 %add194, %and195
  %arrayidx197 = getelementptr inbounds i8, i8* %231, i32 %add196
  %235 = load i8, i8* %arrayidx197, align 1, !tbaa !39
  %conv198 = zext i8 %235 to i32
  store i32 %conv198, i32* %r, align 4, !tbaa !32
  %236 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %237 = load i32, i32* %y, align 4, !tbaa !32
  %238 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add199 = add nsw i32 %237, %238
  %239 = load i32, i32* %d1, align 4, !tbaa !37
  %and200 = and i32 %239, 255
  %shr201 = ashr i32 %and200, 1
  %add202 = add nsw i32 %add199, %shr201
  %arrayidx203 = getelementptr inbounds i8, i8* %236, i32 %add202
  %240 = load i8, i8* %arrayidx203, align 1, !tbaa !39
  %conv204 = zext i8 %240 to i32
  store i32 %conv204, i32* %g, align 4, !tbaa !32
  %241 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %242 = load i32, i32* %y, align 4, !tbaa !32
  %243 = load i32, i32* %cblue, align 4, !tbaa !32
  %add205 = add nsw i32 %242, %243
  %244 = load i32, i32* %d1, align 4, !tbaa !37
  %and206 = and i32 %244, 255
  %add207 = add nsw i32 %add205, %and206
  %arrayidx208 = getelementptr inbounds i8, i8* %241, i32 %add207
  %245 = load i8, i8* %arrayidx208, align 1, !tbaa !39
  %conv209 = zext i8 %245 to i32
  store i32 %conv209, i32* %b, align 4, !tbaa !32
  %246 = load i32, i32* %r, align 4, !tbaa !32
  %shl210 = shl i32 %246, 8
  %and211 = and i32 %shl210, 63488
  %247 = load i32, i32* %g, align 4, !tbaa !32
  %shl212 = shl i32 %247, 3
  %and213 = and i32 %shl212, 2016
  %or214 = or i32 %and211, %and213
  %248 = load i32, i32* %b, align 4, !tbaa !32
  %shr215 = lshr i32 %248, 3
  %or216 = or i32 %or214, %shr215
  store i32 %or216, i32* %rgb, align 4, !tbaa !37
  %249 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv217 = trunc i32 %249 to i16
  %250 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %251 = bitcast i8* %250 to i16*
  store i16 %conv217, i16* %251, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %252 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #4
  %253 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #4
  %254 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #4
  %255 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #4
  %256 = bitcast i32* %d1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #4
  %257 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #4
  %258 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #4
  %259 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #4
  %260 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #4
  %261 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #4
  %262 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #4
  %263 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %263) #4
  %264 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #4
  %265 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %265) #4
  %266 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %266) #4
  %267 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %267) #4
  %268 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #4
  %269 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #4
  %270 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %270) #4
  %271 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %271) #4
  %272 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %272) #4
  %273 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %273) #4
  %274 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %274) #4
  %275 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %275) #4
  %276 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %276) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v2_merged_upsample_565_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %rgb = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %38, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %37, i32 %mul
  %39 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %39, i8** %inptr00, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %40, i32 0
  %41 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %42, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %41, i32 %add
  %43 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %43, i8** %inptr01, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %44, i32 1
  %45 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %47, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %48, i32 2
  %49 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %50 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %49, i32 %50
  %51 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %51, i8** %inptr2, align 4, !tbaa !2
  %52 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %52, i32 0
  %53 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %53, i8** %outptr0, align 4, !tbaa !2
  %54 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %54, i32 1
  %55 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %55, i8** %outptr1, align 4, !tbaa !2
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 27
  %57 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %57, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %58 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %58, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %59 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %60 = load i8, i8* %59, align 1, !tbaa !39
  %conv = zext i8 %60 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %61 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %61, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %62 = load i8, i8* %61, align 1, !tbaa !39
  %conv13 = zext i8 %62 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %63 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %64 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %63, i32 %64
  %65 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %65, i32* %cred, align 4, !tbaa !32
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %68, %71
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %72 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %73 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %72, i32 %73
  %74 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %74, i32* %cblue, align 4, !tbaa !32
  %75 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %75, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %76 = load i8, i8* %75, align 1, !tbaa !39
  %conv21 = zext i8 %76 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %77 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %78 = load i32, i32* %y, align 4, !tbaa !32
  %79 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %78, %79
  %arrayidx23 = getelementptr inbounds i8, i8* %77, i32 %add22
  %80 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %conv24 = zext i8 %80 to i32
  store i32 %conv24, i32* %r, align 4, !tbaa !32
  %81 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %82 = load i32, i32* %y, align 4, !tbaa !32
  %83 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %82, %83
  %arrayidx26 = getelementptr inbounds i8, i8* %81, i32 %add25
  %84 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %conv27 = zext i8 %84 to i32
  store i32 %conv27, i32* %g, align 4, !tbaa !32
  %85 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %86 = load i32, i32* %y, align 4, !tbaa !32
  %87 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %86, %87
  %arrayidx29 = getelementptr inbounds i8, i8* %85, i32 %add28
  %88 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %conv30 = zext i8 %88 to i32
  store i32 %conv30, i32* %b, align 4, !tbaa !32
  %89 = load i32, i32* %r, align 4, !tbaa !32
  %and = and i32 %89, 248
  %90 = load i32, i32* %g, align 4, !tbaa !32
  %shr31 = lshr i32 %90, 5
  %or = or i32 %and, %shr31
  %91 = load i32, i32* %g, align 4, !tbaa !32
  %shl = shl i32 %91, 11
  %and32 = and i32 %shl, 57344
  %or33 = or i32 %or, %and32
  %92 = load i32, i32* %b, align 4, !tbaa !32
  %shl34 = shl i32 %92, 5
  %and35 = and i32 %shl34, 7936
  %or36 = or i32 %or33, %and35
  store i32 %or36, i32* %rgb, align 4, !tbaa !37
  %93 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr37 = getelementptr inbounds i8, i8* %93, i32 1
  store i8* %incdec.ptr37, i8** %inptr00, align 4, !tbaa !2
  %94 = load i8, i8* %93, align 1, !tbaa !39
  %conv38 = zext i8 %94 to i32
  store i32 %conv38, i32* %y, align 4, !tbaa !32
  %95 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %96 = load i32, i32* %y, align 4, !tbaa !32
  %97 = load i32, i32* %cred, align 4, !tbaa !32
  %add39 = add nsw i32 %96, %97
  %arrayidx40 = getelementptr inbounds i8, i8* %95, i32 %add39
  %98 = load i8, i8* %arrayidx40, align 1, !tbaa !39
  %conv41 = zext i8 %98 to i32
  store i32 %conv41, i32* %r, align 4, !tbaa !32
  %99 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %100 = load i32, i32* %y, align 4, !tbaa !32
  %101 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add42 = add nsw i32 %100, %101
  %arrayidx43 = getelementptr inbounds i8, i8* %99, i32 %add42
  %102 = load i8, i8* %arrayidx43, align 1, !tbaa !39
  %conv44 = zext i8 %102 to i32
  store i32 %conv44, i32* %g, align 4, !tbaa !32
  %103 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %104 = load i32, i32* %y, align 4, !tbaa !32
  %105 = load i32, i32* %cblue, align 4, !tbaa !32
  %add45 = add nsw i32 %104, %105
  %arrayidx46 = getelementptr inbounds i8, i8* %103, i32 %add45
  %106 = load i8, i8* %arrayidx46, align 1, !tbaa !39
  %conv47 = zext i8 %106 to i32
  store i32 %conv47, i32* %b, align 4, !tbaa !32
  %107 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl48 = shl i32 %107, 16
  %108 = load i32, i32* %r, align 4, !tbaa !32
  %and49 = and i32 %108, 248
  %109 = load i32, i32* %g, align 4, !tbaa !32
  %shr50 = lshr i32 %109, 5
  %or51 = or i32 %and49, %shr50
  %110 = load i32, i32* %g, align 4, !tbaa !32
  %shl52 = shl i32 %110, 11
  %and53 = and i32 %shl52, 57344
  %or54 = or i32 %or51, %and53
  %111 = load i32, i32* %b, align 4, !tbaa !32
  %shl55 = shl i32 %111, 5
  %and56 = and i32 %shl55, 7936
  %or57 = or i32 %or54, %and56
  %or58 = or i32 %shl48, %or57
  store i32 %or58, i32* %rgb, align 4, !tbaa !37
  %112 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv59 = trunc i32 %112 to i16
  %113 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %114 = bitcast i8* %113 to i16*
  %arrayidx60 = getelementptr inbounds i16, i16* %114, i32 1
  store i16 %conv59, i16* %arrayidx60, align 2, !tbaa !41
  %115 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr61 = ashr i32 %115, 16
  %conv62 = trunc i32 %shr61 to i16
  %116 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %117 = bitcast i8* %116 to i16*
  %arrayidx63 = getelementptr inbounds i16, i16* %117, i32 0
  store i16 %conv62, i16* %arrayidx63, align 2, !tbaa !41
  %118 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %118, i32 4
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %119 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr64 = getelementptr inbounds i8, i8* %119, i32 1
  store i8* %incdec.ptr64, i8** %inptr01, align 4, !tbaa !2
  %120 = load i8, i8* %119, align 1, !tbaa !39
  %conv65 = zext i8 %120 to i32
  store i32 %conv65, i32* %y, align 4, !tbaa !32
  %121 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %122 = load i32, i32* %y, align 4, !tbaa !32
  %123 = load i32, i32* %cred, align 4, !tbaa !32
  %add66 = add nsw i32 %122, %123
  %arrayidx67 = getelementptr inbounds i8, i8* %121, i32 %add66
  %124 = load i8, i8* %arrayidx67, align 1, !tbaa !39
  %conv68 = zext i8 %124 to i32
  store i32 %conv68, i32* %r, align 4, !tbaa !32
  %125 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %126 = load i32, i32* %y, align 4, !tbaa !32
  %127 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add69 = add nsw i32 %126, %127
  %arrayidx70 = getelementptr inbounds i8, i8* %125, i32 %add69
  %128 = load i8, i8* %arrayidx70, align 1, !tbaa !39
  %conv71 = zext i8 %128 to i32
  store i32 %conv71, i32* %g, align 4, !tbaa !32
  %129 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %130 = load i32, i32* %y, align 4, !tbaa !32
  %131 = load i32, i32* %cblue, align 4, !tbaa !32
  %add72 = add nsw i32 %130, %131
  %arrayidx73 = getelementptr inbounds i8, i8* %129, i32 %add72
  %132 = load i8, i8* %arrayidx73, align 1, !tbaa !39
  %conv74 = zext i8 %132 to i32
  store i32 %conv74, i32* %b, align 4, !tbaa !32
  %133 = load i32, i32* %r, align 4, !tbaa !32
  %and75 = and i32 %133, 248
  %134 = load i32, i32* %g, align 4, !tbaa !32
  %shr76 = lshr i32 %134, 5
  %or77 = or i32 %and75, %shr76
  %135 = load i32, i32* %g, align 4, !tbaa !32
  %shl78 = shl i32 %135, 11
  %and79 = and i32 %shl78, 57344
  %or80 = or i32 %or77, %and79
  %136 = load i32, i32* %b, align 4, !tbaa !32
  %shl81 = shl i32 %136, 5
  %and82 = and i32 %shl81, 7936
  %or83 = or i32 %or80, %and82
  store i32 %or83, i32* %rgb, align 4, !tbaa !37
  %137 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr84 = getelementptr inbounds i8, i8* %137, i32 1
  store i8* %incdec.ptr84, i8** %inptr01, align 4, !tbaa !2
  %138 = load i8, i8* %137, align 1, !tbaa !39
  %conv85 = zext i8 %138 to i32
  store i32 %conv85, i32* %y, align 4, !tbaa !32
  %139 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %140 = load i32, i32* %y, align 4, !tbaa !32
  %141 = load i32, i32* %cred, align 4, !tbaa !32
  %add86 = add nsw i32 %140, %141
  %arrayidx87 = getelementptr inbounds i8, i8* %139, i32 %add86
  %142 = load i8, i8* %arrayidx87, align 1, !tbaa !39
  %conv88 = zext i8 %142 to i32
  store i32 %conv88, i32* %r, align 4, !tbaa !32
  %143 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %144 = load i32, i32* %y, align 4, !tbaa !32
  %145 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add89 = add nsw i32 %144, %145
  %arrayidx90 = getelementptr inbounds i8, i8* %143, i32 %add89
  %146 = load i8, i8* %arrayidx90, align 1, !tbaa !39
  %conv91 = zext i8 %146 to i32
  store i32 %conv91, i32* %g, align 4, !tbaa !32
  %147 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %148 = load i32, i32* %y, align 4, !tbaa !32
  %149 = load i32, i32* %cblue, align 4, !tbaa !32
  %add92 = add nsw i32 %148, %149
  %arrayidx93 = getelementptr inbounds i8, i8* %147, i32 %add92
  %150 = load i8, i8* %arrayidx93, align 1, !tbaa !39
  %conv94 = zext i8 %150 to i32
  store i32 %conv94, i32* %b, align 4, !tbaa !32
  %151 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl95 = shl i32 %151, 16
  %152 = load i32, i32* %r, align 4, !tbaa !32
  %and96 = and i32 %152, 248
  %153 = load i32, i32* %g, align 4, !tbaa !32
  %shr97 = lshr i32 %153, 5
  %or98 = or i32 %and96, %shr97
  %154 = load i32, i32* %g, align 4, !tbaa !32
  %shl99 = shl i32 %154, 11
  %and100 = and i32 %shl99, 57344
  %or101 = or i32 %or98, %and100
  %155 = load i32, i32* %b, align 4, !tbaa !32
  %shl102 = shl i32 %155, 5
  %and103 = and i32 %shl102, 7936
  %or104 = or i32 %or101, %and103
  %or105 = or i32 %shl95, %or104
  store i32 %or105, i32* %rgb, align 4, !tbaa !37
  %156 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv106 = trunc i32 %156 to i16
  %157 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %158 = bitcast i8* %157 to i16*
  %arrayidx107 = getelementptr inbounds i16, i16* %158, i32 1
  store i16 %conv106, i16* %arrayidx107, align 2, !tbaa !41
  %159 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr108 = ashr i32 %159, 16
  %conv109 = trunc i32 %shr108 to i16
  %160 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %161 = bitcast i8* %160 to i16*
  %arrayidx110 = getelementptr inbounds i16, i16* %161, i32 0
  store i16 %conv109, i16* %arrayidx110, align 2, !tbaa !41
  %162 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr111 = getelementptr inbounds i8, i8* %162, i32 4
  store i8* %add.ptr111, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %163 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %163, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %164 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width112 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %164, i32 0, i32 27
  %165 = load i32, i32* %output_width112, align 8, !tbaa !19
  %and113 = and i32 %165, 1
  %tobool = icmp ne i32 %and113, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %166 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %167 = load i8, i8* %166, align 1, !tbaa !39
  %conv114 = zext i8 %167 to i32
  store i32 %conv114, i32* %cb, align 4, !tbaa !32
  %168 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %169 = load i8, i8* %168, align 1, !tbaa !39
  %conv115 = zext i8 %169 to i32
  store i32 %conv115, i32* %cr, align 4, !tbaa !32
  %170 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %171 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx116 = getelementptr inbounds i32, i32* %170, i32 %171
  %172 = load i32, i32* %arrayidx116, align 4, !tbaa !32
  store i32 %172, i32* %cred, align 4, !tbaa !32
  %173 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %174 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx117 = getelementptr inbounds i32, i32* %173, i32 %174
  %175 = load i32, i32* %arrayidx117, align 4, !tbaa !37
  %176 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %177 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx118 = getelementptr inbounds i32, i32* %176, i32 %177
  %178 = load i32, i32* %arrayidx118, align 4, !tbaa !37
  %add119 = add nsw i32 %175, %178
  %shr120 = ashr i32 %add119, 16
  store i32 %shr120, i32* %cgreen, align 4, !tbaa !32
  %179 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %180 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx121 = getelementptr inbounds i32, i32* %179, i32 %180
  %181 = load i32, i32* %arrayidx121, align 4, !tbaa !32
  store i32 %181, i32* %cblue, align 4, !tbaa !32
  %182 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %183 = load i8, i8* %182, align 1, !tbaa !39
  %conv122 = zext i8 %183 to i32
  store i32 %conv122, i32* %y, align 4, !tbaa !32
  %184 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %185 = load i32, i32* %y, align 4, !tbaa !32
  %186 = load i32, i32* %cred, align 4, !tbaa !32
  %add123 = add nsw i32 %185, %186
  %arrayidx124 = getelementptr inbounds i8, i8* %184, i32 %add123
  %187 = load i8, i8* %arrayidx124, align 1, !tbaa !39
  %conv125 = zext i8 %187 to i32
  store i32 %conv125, i32* %r, align 4, !tbaa !32
  %188 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %189 = load i32, i32* %y, align 4, !tbaa !32
  %190 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add126 = add nsw i32 %189, %190
  %arrayidx127 = getelementptr inbounds i8, i8* %188, i32 %add126
  %191 = load i8, i8* %arrayidx127, align 1, !tbaa !39
  %conv128 = zext i8 %191 to i32
  store i32 %conv128, i32* %g, align 4, !tbaa !32
  %192 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %193 = load i32, i32* %y, align 4, !tbaa !32
  %194 = load i32, i32* %cblue, align 4, !tbaa !32
  %add129 = add nsw i32 %193, %194
  %arrayidx130 = getelementptr inbounds i8, i8* %192, i32 %add129
  %195 = load i8, i8* %arrayidx130, align 1, !tbaa !39
  %conv131 = zext i8 %195 to i32
  store i32 %conv131, i32* %b, align 4, !tbaa !32
  %196 = load i32, i32* %r, align 4, !tbaa !32
  %and132 = and i32 %196, 248
  %197 = load i32, i32* %g, align 4, !tbaa !32
  %shr133 = lshr i32 %197, 5
  %or134 = or i32 %and132, %shr133
  %198 = load i32, i32* %g, align 4, !tbaa !32
  %shl135 = shl i32 %198, 11
  %and136 = and i32 %shl135, 57344
  %or137 = or i32 %or134, %and136
  %199 = load i32, i32* %b, align 4, !tbaa !32
  %shl138 = shl i32 %199, 5
  %and139 = and i32 %shl138, 7936
  %or140 = or i32 %or137, %and139
  store i32 %or140, i32* %rgb, align 4, !tbaa !37
  %200 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv141 = trunc i32 %200 to i16
  %201 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %202 = bitcast i8* %201 to i16*
  store i16 %conv141, i16* %202, align 2, !tbaa !41
  %203 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %204 = load i8, i8* %203, align 1, !tbaa !39
  %conv142 = zext i8 %204 to i32
  store i32 %conv142, i32* %y, align 4, !tbaa !32
  %205 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %206 = load i32, i32* %y, align 4, !tbaa !32
  %207 = load i32, i32* %cred, align 4, !tbaa !32
  %add143 = add nsw i32 %206, %207
  %arrayidx144 = getelementptr inbounds i8, i8* %205, i32 %add143
  %208 = load i8, i8* %arrayidx144, align 1, !tbaa !39
  %conv145 = zext i8 %208 to i32
  store i32 %conv145, i32* %r, align 4, !tbaa !32
  %209 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %210 = load i32, i32* %y, align 4, !tbaa !32
  %211 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add146 = add nsw i32 %210, %211
  %arrayidx147 = getelementptr inbounds i8, i8* %209, i32 %add146
  %212 = load i8, i8* %arrayidx147, align 1, !tbaa !39
  %conv148 = zext i8 %212 to i32
  store i32 %conv148, i32* %g, align 4, !tbaa !32
  %213 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %214 = load i32, i32* %y, align 4, !tbaa !32
  %215 = load i32, i32* %cblue, align 4, !tbaa !32
  %add149 = add nsw i32 %214, %215
  %arrayidx150 = getelementptr inbounds i8, i8* %213, i32 %add149
  %216 = load i8, i8* %arrayidx150, align 1, !tbaa !39
  %conv151 = zext i8 %216 to i32
  store i32 %conv151, i32* %b, align 4, !tbaa !32
  %217 = load i32, i32* %r, align 4, !tbaa !32
  %and152 = and i32 %217, 248
  %218 = load i32, i32* %g, align 4, !tbaa !32
  %shr153 = lshr i32 %218, 5
  %or154 = or i32 %and152, %shr153
  %219 = load i32, i32* %g, align 4, !tbaa !32
  %shl155 = shl i32 %219, 11
  %and156 = and i32 %shl155, 57344
  %or157 = or i32 %or154, %and156
  %220 = load i32, i32* %b, align 4, !tbaa !32
  %shl158 = shl i32 %220, 5
  %and159 = and i32 %shl158, 7936
  %or160 = or i32 %or157, %and159
  store i32 %or160, i32* %rgb, align 4, !tbaa !37
  %221 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv161 = trunc i32 %221 to i16
  %222 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %223 = bitcast i8* %222 to i16*
  store i16 %conv161, i16* %223, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %224 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #4
  %225 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #4
  %226 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #4
  %227 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #4
  %228 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #4
  %229 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #4
  %230 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #4
  %231 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #4
  %232 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #4
  %233 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #4
  %234 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #4
  %235 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #4
  %236 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #4
  %237 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #4
  %238 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #4
  %239 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #4
  %240 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #4
  %241 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #4
  %242 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #4
  %243 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #4
  %244 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #4
  %245 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #4
  %246 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v2_merged_upsample_565_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr0 = alloca i8*, align 4
  %outptr1 = alloca i8*, align 4
  %inptr00 = alloca i8*, align 4
  %inptr01 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %rgb = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #4
  %17 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #4
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 65
  %19 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %19, i8** %range_limit, align 4, !tbaa !2
  %20 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #4
  %21 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %21, i32 0, i32 2
  %22 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %22, i32** %Crrtab, align 4, !tbaa !2
  %23 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #4
  %24 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %24, i32 0, i32 3
  %25 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %25, i32** %Cbbtab, align 4, !tbaa !2
  %26 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #4
  %27 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %27, i32 0, i32 4
  %28 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %28, i32** %Crgtab, align 4, !tbaa !2
  %29 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #4
  %30 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %30, i32 0, i32 5
  %31 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %31, i32** %Cbgtab, align 4, !tbaa !2
  %32 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %36, i32 0
  %37 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %38 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul = mul i32 %38, 2
  %arrayidx2 = getelementptr inbounds i8*, i8** %37, i32 %mul
  %39 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %39, i8** %inptr00, align 4, !tbaa !2
  %40 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %40, i32 0
  %41 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %42 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %mul4 = mul i32 %42, 2
  %add = add i32 %mul4, 1
  %arrayidx5 = getelementptr inbounds i8*, i8** %41, i32 %add
  %43 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %43, i8** %inptr01, align 4, !tbaa !2
  %44 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %44, i32 1
  %45 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %46 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %45, i32 %46
  %47 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %47, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8**, i8*** %48, i32 2
  %49 = load i8**, i8*** %arrayidx8, align 4, !tbaa !2
  %50 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds i8*, i8** %49, i32 %50
  %51 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %51, i8** %inptr2, align 4, !tbaa !2
  %52 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i8*, i8** %52, i32 0
  %53 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %53, i8** %outptr0, align 4, !tbaa !2
  %54 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i8*, i8** %54, i32 1
  %55 = load i8*, i8** %arrayidx11, align 4, !tbaa !2
  store i8* %55, i8** %outptr1, align 4, !tbaa !2
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 27
  %57 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %57, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %58 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %58, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %59 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %59, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %60 = load i8, i8* %59, align 1, !tbaa !39
  %conv = zext i8 %60 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %61 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr12 = getelementptr inbounds i8, i8* %61, i32 1
  store i8* %incdec.ptr12, i8** %inptr2, align 4, !tbaa !2
  %62 = load i8, i8* %61, align 1, !tbaa !39
  %conv13 = zext i8 %62 to i32
  store i32 %conv13, i32* %cr, align 4, !tbaa !32
  %63 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %64 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %63, i32 %64
  %65 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %65, i32* %cred, align 4, !tbaa !32
  %66 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %67 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx15, align 4, !tbaa !37
  %69 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %70 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx16 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx16, align 4, !tbaa !37
  %add17 = add nsw i32 %68, %71
  %shr18 = ashr i32 %add17, 16
  store i32 %shr18, i32* %cgreen, align 4, !tbaa !32
  %72 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %73 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx19 = getelementptr inbounds i32, i32* %72, i32 %73
  %74 = load i32, i32* %arrayidx19, align 4, !tbaa !32
  store i32 %74, i32* %cblue, align 4, !tbaa !32
  %75 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr20 = getelementptr inbounds i8, i8* %75, i32 1
  store i8* %incdec.ptr20, i8** %inptr00, align 4, !tbaa !2
  %76 = load i8, i8* %75, align 1, !tbaa !39
  %conv21 = zext i8 %76 to i32
  store i32 %conv21, i32* %y, align 4, !tbaa !32
  %77 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %78 = load i32, i32* %y, align 4, !tbaa !32
  %79 = load i32, i32* %cred, align 4, !tbaa !32
  %add22 = add nsw i32 %78, %79
  %arrayidx23 = getelementptr inbounds i8, i8* %77, i32 %add22
  %80 = load i8, i8* %arrayidx23, align 1, !tbaa !39
  %conv24 = zext i8 %80 to i32
  store i32 %conv24, i32* %r, align 4, !tbaa !32
  %81 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %82 = load i32, i32* %y, align 4, !tbaa !32
  %83 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add25 = add nsw i32 %82, %83
  %arrayidx26 = getelementptr inbounds i8, i8* %81, i32 %add25
  %84 = load i8, i8* %arrayidx26, align 1, !tbaa !39
  %conv27 = zext i8 %84 to i32
  store i32 %conv27, i32* %g, align 4, !tbaa !32
  %85 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %86 = load i32, i32* %y, align 4, !tbaa !32
  %87 = load i32, i32* %cblue, align 4, !tbaa !32
  %add28 = add nsw i32 %86, %87
  %arrayidx29 = getelementptr inbounds i8, i8* %85, i32 %add28
  %88 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %conv30 = zext i8 %88 to i32
  store i32 %conv30, i32* %b, align 4, !tbaa !32
  %89 = load i32, i32* %r, align 4, !tbaa !32
  %shl = shl i32 %89, 8
  %and = and i32 %shl, 63488
  %90 = load i32, i32* %g, align 4, !tbaa !32
  %shl31 = shl i32 %90, 3
  %and32 = and i32 %shl31, 2016
  %or = or i32 %and, %and32
  %91 = load i32, i32* %b, align 4, !tbaa !32
  %shr33 = lshr i32 %91, 3
  %or34 = or i32 %or, %shr33
  store i32 %or34, i32* %rgb, align 4, !tbaa !37
  %92 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %incdec.ptr35 = getelementptr inbounds i8, i8* %92, i32 1
  store i8* %incdec.ptr35, i8** %inptr00, align 4, !tbaa !2
  %93 = load i8, i8* %92, align 1, !tbaa !39
  %conv36 = zext i8 %93 to i32
  store i32 %conv36, i32* %y, align 4, !tbaa !32
  %94 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %95 = load i32, i32* %y, align 4, !tbaa !32
  %96 = load i32, i32* %cred, align 4, !tbaa !32
  %add37 = add nsw i32 %95, %96
  %arrayidx38 = getelementptr inbounds i8, i8* %94, i32 %add37
  %97 = load i8, i8* %arrayidx38, align 1, !tbaa !39
  %conv39 = zext i8 %97 to i32
  store i32 %conv39, i32* %r, align 4, !tbaa !32
  %98 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %99 = load i32, i32* %y, align 4, !tbaa !32
  %100 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add40 = add nsw i32 %99, %100
  %arrayidx41 = getelementptr inbounds i8, i8* %98, i32 %add40
  %101 = load i8, i8* %arrayidx41, align 1, !tbaa !39
  %conv42 = zext i8 %101 to i32
  store i32 %conv42, i32* %g, align 4, !tbaa !32
  %102 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %103 = load i32, i32* %y, align 4, !tbaa !32
  %104 = load i32, i32* %cblue, align 4, !tbaa !32
  %add43 = add nsw i32 %103, %104
  %arrayidx44 = getelementptr inbounds i8, i8* %102, i32 %add43
  %105 = load i8, i8* %arrayidx44, align 1, !tbaa !39
  %conv45 = zext i8 %105 to i32
  store i32 %conv45, i32* %b, align 4, !tbaa !32
  %106 = load i32, i32* %r, align 4, !tbaa !32
  %shl46 = shl i32 %106, 8
  %and47 = and i32 %shl46, 63488
  %107 = load i32, i32* %g, align 4, !tbaa !32
  %shl48 = shl i32 %107, 3
  %and49 = and i32 %shl48, 2016
  %or50 = or i32 %and47, %and49
  %108 = load i32, i32* %b, align 4, !tbaa !32
  %shr51 = lshr i32 %108, 3
  %or52 = or i32 %or50, %shr51
  %shl53 = shl i32 %or52, 16
  %109 = load i32, i32* %rgb, align 4, !tbaa !37
  %or54 = or i32 %shl53, %109
  store i32 %or54, i32* %rgb, align 4, !tbaa !37
  %110 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv55 = trunc i32 %110 to i16
  %111 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %112 = bitcast i8* %111 to i16*
  %arrayidx56 = getelementptr inbounds i16, i16* %112, i32 0
  store i16 %conv55, i16* %arrayidx56, align 2, !tbaa !41
  %113 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr57 = ashr i32 %113, 16
  %conv58 = trunc i32 %shr57 to i16
  %114 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %115 = bitcast i8* %114 to i16*
  %arrayidx59 = getelementptr inbounds i16, i16* %115, i32 1
  store i16 %conv58, i16* %arrayidx59, align 2, !tbaa !41
  %116 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %116, i32 4
  store i8* %add.ptr, i8** %outptr0, align 4, !tbaa !2
  %117 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr60 = getelementptr inbounds i8, i8* %117, i32 1
  store i8* %incdec.ptr60, i8** %inptr01, align 4, !tbaa !2
  %118 = load i8, i8* %117, align 1, !tbaa !39
  %conv61 = zext i8 %118 to i32
  store i32 %conv61, i32* %y, align 4, !tbaa !32
  %119 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %120 = load i32, i32* %y, align 4, !tbaa !32
  %121 = load i32, i32* %cred, align 4, !tbaa !32
  %add62 = add nsw i32 %120, %121
  %arrayidx63 = getelementptr inbounds i8, i8* %119, i32 %add62
  %122 = load i8, i8* %arrayidx63, align 1, !tbaa !39
  %conv64 = zext i8 %122 to i32
  store i32 %conv64, i32* %r, align 4, !tbaa !32
  %123 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %124 = load i32, i32* %y, align 4, !tbaa !32
  %125 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add65 = add nsw i32 %124, %125
  %arrayidx66 = getelementptr inbounds i8, i8* %123, i32 %add65
  %126 = load i8, i8* %arrayidx66, align 1, !tbaa !39
  %conv67 = zext i8 %126 to i32
  store i32 %conv67, i32* %g, align 4, !tbaa !32
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !32
  %129 = load i32, i32* %cblue, align 4, !tbaa !32
  %add68 = add nsw i32 %128, %129
  %arrayidx69 = getelementptr inbounds i8, i8* %127, i32 %add68
  %130 = load i8, i8* %arrayidx69, align 1, !tbaa !39
  %conv70 = zext i8 %130 to i32
  store i32 %conv70, i32* %b, align 4, !tbaa !32
  %131 = load i32, i32* %r, align 4, !tbaa !32
  %shl71 = shl i32 %131, 8
  %and72 = and i32 %shl71, 63488
  %132 = load i32, i32* %g, align 4, !tbaa !32
  %shl73 = shl i32 %132, 3
  %and74 = and i32 %shl73, 2016
  %or75 = or i32 %and72, %and74
  %133 = load i32, i32* %b, align 4, !tbaa !32
  %shr76 = lshr i32 %133, 3
  %or77 = or i32 %or75, %shr76
  store i32 %or77, i32* %rgb, align 4, !tbaa !37
  %134 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %incdec.ptr78 = getelementptr inbounds i8, i8* %134, i32 1
  store i8* %incdec.ptr78, i8** %inptr01, align 4, !tbaa !2
  %135 = load i8, i8* %134, align 1, !tbaa !39
  %conv79 = zext i8 %135 to i32
  store i32 %conv79, i32* %y, align 4, !tbaa !32
  %136 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %137 = load i32, i32* %y, align 4, !tbaa !32
  %138 = load i32, i32* %cred, align 4, !tbaa !32
  %add80 = add nsw i32 %137, %138
  %arrayidx81 = getelementptr inbounds i8, i8* %136, i32 %add80
  %139 = load i8, i8* %arrayidx81, align 1, !tbaa !39
  %conv82 = zext i8 %139 to i32
  store i32 %conv82, i32* %r, align 4, !tbaa !32
  %140 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !32
  %142 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add83 = add nsw i32 %141, %142
  %arrayidx84 = getelementptr inbounds i8, i8* %140, i32 %add83
  %143 = load i8, i8* %arrayidx84, align 1, !tbaa !39
  %conv85 = zext i8 %143 to i32
  store i32 %conv85, i32* %g, align 4, !tbaa !32
  %144 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %145 = load i32, i32* %y, align 4, !tbaa !32
  %146 = load i32, i32* %cblue, align 4, !tbaa !32
  %add86 = add nsw i32 %145, %146
  %arrayidx87 = getelementptr inbounds i8, i8* %144, i32 %add86
  %147 = load i8, i8* %arrayidx87, align 1, !tbaa !39
  %conv88 = zext i8 %147 to i32
  store i32 %conv88, i32* %b, align 4, !tbaa !32
  %148 = load i32, i32* %r, align 4, !tbaa !32
  %shl89 = shl i32 %148, 8
  %and90 = and i32 %shl89, 63488
  %149 = load i32, i32* %g, align 4, !tbaa !32
  %shl91 = shl i32 %149, 3
  %and92 = and i32 %shl91, 2016
  %or93 = or i32 %and90, %and92
  %150 = load i32, i32* %b, align 4, !tbaa !32
  %shr94 = lshr i32 %150, 3
  %or95 = or i32 %or93, %shr94
  %shl96 = shl i32 %or95, 16
  %151 = load i32, i32* %rgb, align 4, !tbaa !37
  %or97 = or i32 %shl96, %151
  store i32 %or97, i32* %rgb, align 4, !tbaa !37
  %152 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv98 = trunc i32 %152 to i16
  %153 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %154 = bitcast i8* %153 to i16*
  %arrayidx99 = getelementptr inbounds i16, i16* %154, i32 0
  store i16 %conv98, i16* %arrayidx99, align 2, !tbaa !41
  %155 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr100 = ashr i32 %155, 16
  %conv101 = trunc i32 %shr100 to i16
  %156 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %157 = bitcast i8* %156 to i16*
  %arrayidx102 = getelementptr inbounds i16, i16* %157, i32 1
  store i16 %conv101, i16* %arrayidx102, align 2, !tbaa !41
  %158 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %add.ptr103 = getelementptr inbounds i8, i8* %158, i32 4
  store i8* %add.ptr103, i8** %outptr1, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %159 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %159, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %160 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width104 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %160, i32 0, i32 27
  %161 = load i32, i32* %output_width104, align 8, !tbaa !19
  %and105 = and i32 %161, 1
  %tobool = icmp ne i32 %and105, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %162 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %163 = load i8, i8* %162, align 1, !tbaa !39
  %conv106 = zext i8 %163 to i32
  store i32 %conv106, i32* %cb, align 4, !tbaa !32
  %164 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %165 = load i8, i8* %164, align 1, !tbaa !39
  %conv107 = zext i8 %165 to i32
  store i32 %conv107, i32* %cr, align 4, !tbaa !32
  %166 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %167 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx108 = getelementptr inbounds i32, i32* %166, i32 %167
  %168 = load i32, i32* %arrayidx108, align 4, !tbaa !32
  store i32 %168, i32* %cred, align 4, !tbaa !32
  %169 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %170 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx109 = getelementptr inbounds i32, i32* %169, i32 %170
  %171 = load i32, i32* %arrayidx109, align 4, !tbaa !37
  %172 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %173 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx110 = getelementptr inbounds i32, i32* %172, i32 %173
  %174 = load i32, i32* %arrayidx110, align 4, !tbaa !37
  %add111 = add nsw i32 %171, %174
  %shr112 = ashr i32 %add111, 16
  store i32 %shr112, i32* %cgreen, align 4, !tbaa !32
  %175 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %176 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx113 = getelementptr inbounds i32, i32* %175, i32 %176
  %177 = load i32, i32* %arrayidx113, align 4, !tbaa !32
  store i32 %177, i32* %cblue, align 4, !tbaa !32
  %178 = load i8*, i8** %inptr00, align 4, !tbaa !2
  %179 = load i8, i8* %178, align 1, !tbaa !39
  %conv114 = zext i8 %179 to i32
  store i32 %conv114, i32* %y, align 4, !tbaa !32
  %180 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %181 = load i32, i32* %y, align 4, !tbaa !32
  %182 = load i32, i32* %cred, align 4, !tbaa !32
  %add115 = add nsw i32 %181, %182
  %arrayidx116 = getelementptr inbounds i8, i8* %180, i32 %add115
  %183 = load i8, i8* %arrayidx116, align 1, !tbaa !39
  %conv117 = zext i8 %183 to i32
  store i32 %conv117, i32* %r, align 4, !tbaa !32
  %184 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %185 = load i32, i32* %y, align 4, !tbaa !32
  %186 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add118 = add nsw i32 %185, %186
  %arrayidx119 = getelementptr inbounds i8, i8* %184, i32 %add118
  %187 = load i8, i8* %arrayidx119, align 1, !tbaa !39
  %conv120 = zext i8 %187 to i32
  store i32 %conv120, i32* %g, align 4, !tbaa !32
  %188 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %189 = load i32, i32* %y, align 4, !tbaa !32
  %190 = load i32, i32* %cblue, align 4, !tbaa !32
  %add121 = add nsw i32 %189, %190
  %arrayidx122 = getelementptr inbounds i8, i8* %188, i32 %add121
  %191 = load i8, i8* %arrayidx122, align 1, !tbaa !39
  %conv123 = zext i8 %191 to i32
  store i32 %conv123, i32* %b, align 4, !tbaa !32
  %192 = load i32, i32* %r, align 4, !tbaa !32
  %shl124 = shl i32 %192, 8
  %and125 = and i32 %shl124, 63488
  %193 = load i32, i32* %g, align 4, !tbaa !32
  %shl126 = shl i32 %193, 3
  %and127 = and i32 %shl126, 2016
  %or128 = or i32 %and125, %and127
  %194 = load i32, i32* %b, align 4, !tbaa !32
  %shr129 = lshr i32 %194, 3
  %or130 = or i32 %or128, %shr129
  store i32 %or130, i32* %rgb, align 4, !tbaa !37
  %195 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv131 = trunc i32 %195 to i16
  %196 = load i8*, i8** %outptr0, align 4, !tbaa !2
  %197 = bitcast i8* %196 to i16*
  store i16 %conv131, i16* %197, align 2, !tbaa !41
  %198 = load i8*, i8** %inptr01, align 4, !tbaa !2
  %199 = load i8, i8* %198, align 1, !tbaa !39
  %conv132 = zext i8 %199 to i32
  store i32 %conv132, i32* %y, align 4, !tbaa !32
  %200 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %201 = load i32, i32* %y, align 4, !tbaa !32
  %202 = load i32, i32* %cred, align 4, !tbaa !32
  %add133 = add nsw i32 %201, %202
  %arrayidx134 = getelementptr inbounds i8, i8* %200, i32 %add133
  %203 = load i8, i8* %arrayidx134, align 1, !tbaa !39
  %conv135 = zext i8 %203 to i32
  store i32 %conv135, i32* %r, align 4, !tbaa !32
  %204 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %205 = load i32, i32* %y, align 4, !tbaa !32
  %206 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add136 = add nsw i32 %205, %206
  %arrayidx137 = getelementptr inbounds i8, i8* %204, i32 %add136
  %207 = load i8, i8* %arrayidx137, align 1, !tbaa !39
  %conv138 = zext i8 %207 to i32
  store i32 %conv138, i32* %g, align 4, !tbaa !32
  %208 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %209 = load i32, i32* %y, align 4, !tbaa !32
  %210 = load i32, i32* %cblue, align 4, !tbaa !32
  %add139 = add nsw i32 %209, %210
  %arrayidx140 = getelementptr inbounds i8, i8* %208, i32 %add139
  %211 = load i8, i8* %arrayidx140, align 1, !tbaa !39
  %conv141 = zext i8 %211 to i32
  store i32 %conv141, i32* %b, align 4, !tbaa !32
  %212 = load i32, i32* %r, align 4, !tbaa !32
  %shl142 = shl i32 %212, 8
  %and143 = and i32 %shl142, 63488
  %213 = load i32, i32* %g, align 4, !tbaa !32
  %shl144 = shl i32 %213, 3
  %and145 = and i32 %shl144, 2016
  %or146 = or i32 %and143, %and145
  %214 = load i32, i32* %b, align 4, !tbaa !32
  %shr147 = lshr i32 %214, 3
  %or148 = or i32 %or146, %shr147
  store i32 %or148, i32* %rgb, align 4, !tbaa !37
  %215 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv149 = trunc i32 %215 to i16
  %216 = load i8*, i8** %outptr1, align 4, !tbaa !2
  %217 = bitcast i8* %216 to i16*
  store i16 %conv149, i16* %217, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %218 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #4
  %219 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #4
  %220 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #4
  %221 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #4
  %222 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #4
  %223 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #4
  %224 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #4
  %225 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #4
  %226 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #4
  %227 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #4
  %228 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #4
  %229 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #4
  %230 = bitcast i8** %inptr01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #4
  %231 = bitcast i8** %inptr00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #4
  %232 = bitcast i8** %outptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #4
  %233 = bitcast i8** %outptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #4
  %234 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #4
  %235 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #4
  %236 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #4
  %237 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #4
  %238 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #4
  %239 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #4
  %240 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extrgb_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %30, i32 0
  %31 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %32 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %33, i8** %inptr0, align 4, !tbaa !2
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %34, i32 1
  %35 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %37, i8** %inptr1, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %38, i32 2
  %39 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %41, i8** %inptr2, align 4, !tbaa !2
  %42 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %42, i32 0
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %outptr, align 4, !tbaa !2
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 27
  %45 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %45, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %46 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %46, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %47 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !39
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %49 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !39
  %conv9 = zext i8 %50 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %51 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %52 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %53, i32* %cred, align 4, !tbaa !32
  %54 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %55 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %54, i32 %55
  %56 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %57 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %58 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %57, i32 %58
  %59 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %56, %59
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %60 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %61 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %62, i32* %cblue, align 4, !tbaa !32
  %63 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !39
  %conv16 = zext i8 %64 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %65 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %66 = load i32, i32* %y, align 4, !tbaa !32
  %67 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %66, %67
  %arrayidx18 = getelementptr inbounds i8, i8* %65, i32 %add17
  %68 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %69, i32 0
  store i8 %68, i8* %arrayidx19, align 1, !tbaa !39
  %70 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !32
  %72 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %71, %72
  %arrayidx21 = getelementptr inbounds i8, i8* %70, i32 %add20
  %73 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %74 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8, i8* %74, i32 1
  store i8 %73, i8* %arrayidx22, align 1, !tbaa !39
  %75 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %76 = load i32, i32* %y, align 4, !tbaa !32
  %77 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %76, %77
  %arrayidx24 = getelementptr inbounds i8, i8* %75, i32 %add23
  %78 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %79, i32 2
  store i8 %78, i8* %arrayidx25, align 1, !tbaa !39
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %80, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %81 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr26 = getelementptr inbounds i8, i8* %81, i32 1
  store i8* %incdec.ptr26, i8** %inptr0, align 4, !tbaa !2
  %82 = load i8, i8* %81, align 1, !tbaa !39
  %conv27 = zext i8 %82 to i32
  store i32 %conv27, i32* %y, align 4, !tbaa !32
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cred, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 0
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %89 = load i32, i32* %y, align 4, !tbaa !32
  %90 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add31 = add nsw i32 %89, %90
  %arrayidx32 = getelementptr inbounds i8, i8* %88, i32 %add31
  %91 = load i8, i8* %arrayidx32, align 1, !tbaa !39
  %92 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i8, i8* %92, i32 1
  store i8 %91, i8* %arrayidx33, align 1, !tbaa !39
  %93 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %94 = load i32, i32* %y, align 4, !tbaa !32
  %95 = load i32, i32* %cblue, align 4, !tbaa !32
  %add34 = add nsw i32 %94, %95
  %arrayidx35 = getelementptr inbounds i8, i8* %93, i32 %add34
  %96 = load i8, i8* %arrayidx35, align 1, !tbaa !39
  %97 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %97, i32 2
  store i8 %96, i8* %arrayidx36, align 1, !tbaa !39
  %98 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr37 = getelementptr inbounds i8, i8* %98, i32 3
  store i8* %add.ptr37, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %99 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %99, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %100 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %100, i32 0, i32 27
  %101 = load i32, i32* %output_width38, align 8, !tbaa !19
  %and = and i32 %101, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %102 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %103 = load i8, i8* %102, align 1, !tbaa !39
  %conv39 = zext i8 %103 to i32
  store i32 %conv39, i32* %cb, align 4, !tbaa !32
  %104 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %105 = load i8, i8* %104, align 1, !tbaa !39
  %conv40 = zext i8 %105 to i32
  store i32 %conv40, i32* %cr, align 4, !tbaa !32
  %106 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %107 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx41 = getelementptr inbounds i32, i32* %106, i32 %107
  %108 = load i32, i32* %arrayidx41, align 4, !tbaa !32
  store i32 %108, i32* %cred, align 4, !tbaa !32
  %109 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %110 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx42 = getelementptr inbounds i32, i32* %109, i32 %110
  %111 = load i32, i32* %arrayidx42, align 4, !tbaa !37
  %112 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %113 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx43 = getelementptr inbounds i32, i32* %112, i32 %113
  %114 = load i32, i32* %arrayidx43, align 4, !tbaa !37
  %add44 = add nsw i32 %111, %114
  %shr45 = ashr i32 %add44, 16
  store i32 %shr45, i32* %cgreen, align 4, !tbaa !32
  %115 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %116 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx46 = getelementptr inbounds i32, i32* %115, i32 %116
  %117 = load i32, i32* %arrayidx46, align 4, !tbaa !32
  store i32 %117, i32* %cblue, align 4, !tbaa !32
  %118 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %119 = load i8, i8* %118, align 1, !tbaa !39
  %conv47 = zext i8 %119 to i32
  store i32 %conv47, i32* %y, align 4, !tbaa !32
  %120 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %121 = load i32, i32* %y, align 4, !tbaa !32
  %122 = load i32, i32* %cred, align 4, !tbaa !32
  %add48 = add nsw i32 %121, %122
  %arrayidx49 = getelementptr inbounds i8, i8* %120, i32 %add48
  %123 = load i8, i8* %arrayidx49, align 1, !tbaa !39
  %124 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i8, i8* %124, i32 0
  store i8 %123, i8* %arrayidx50, align 1, !tbaa !39
  %125 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %126 = load i32, i32* %y, align 4, !tbaa !32
  %127 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add51 = add nsw i32 %126, %127
  %arrayidx52 = getelementptr inbounds i8, i8* %125, i32 %add51
  %128 = load i8, i8* %arrayidx52, align 1, !tbaa !39
  %129 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i8, i8* %129, i32 1
  store i8 %128, i8* %arrayidx53, align 1, !tbaa !39
  %130 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %131 = load i32, i32* %y, align 4, !tbaa !32
  %132 = load i32, i32* %cblue, align 4, !tbaa !32
  %add54 = add nsw i32 %131, %132
  %arrayidx55 = getelementptr inbounds i8, i8* %130, i32 %add54
  %133 = load i8, i8* %arrayidx55, align 1, !tbaa !39
  %134 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i8, i8* %134, i32 2
  store i8 %133, i8* %arrayidx56, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %135 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  %136 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  %137 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #4
  %138 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  %143 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %144 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  %149 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extrgbx_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %30, i32 0
  %31 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %32 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %33, i8** %inptr0, align 4, !tbaa !2
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %34, i32 1
  %35 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %37, i8** %inptr1, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %38, i32 2
  %39 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %41, i8** %inptr2, align 4, !tbaa !2
  %42 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %42, i32 0
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %outptr, align 4, !tbaa !2
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 27
  %45 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %45, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %46 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %46, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %47 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !39
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %49 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !39
  %conv9 = zext i8 %50 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %51 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %52 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %53, i32* %cred, align 4, !tbaa !32
  %54 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %55 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %54, i32 %55
  %56 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %57 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %58 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %57, i32 %58
  %59 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %56, %59
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %60 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %61 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %62, i32* %cblue, align 4, !tbaa !32
  %63 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !39
  %conv16 = zext i8 %64 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %65 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %66 = load i32, i32* %y, align 4, !tbaa !32
  %67 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %66, %67
  %arrayidx18 = getelementptr inbounds i8, i8* %65, i32 %add17
  %68 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %69, i32 0
  store i8 %68, i8* %arrayidx19, align 1, !tbaa !39
  %70 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !32
  %72 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %71, %72
  %arrayidx21 = getelementptr inbounds i8, i8* %70, i32 %add20
  %73 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %74 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8, i8* %74, i32 1
  store i8 %73, i8* %arrayidx22, align 1, !tbaa !39
  %75 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %76 = load i32, i32* %y, align 4, !tbaa !32
  %77 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %76, %77
  %arrayidx24 = getelementptr inbounds i8, i8* %75, i32 %add23
  %78 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %79, i32 2
  store i8 %78, i8* %arrayidx25, align 1, !tbaa !39
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %80, i32 3
  store i8 -1, i8* %arrayidx26, align 1, !tbaa !39
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %81, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %82 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8* %incdec.ptr27, i8** %inptr0, align 4, !tbaa !2
  %83 = load i8, i8* %82, align 1, !tbaa !39
  %conv28 = zext i8 %83 to i32
  store i32 %conv28, i32* %y, align 4, !tbaa !32
  %84 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %85 = load i32, i32* %y, align 4, !tbaa !32
  %86 = load i32, i32* %cred, align 4, !tbaa !32
  %add29 = add nsw i32 %85, %86
  %arrayidx30 = getelementptr inbounds i8, i8* %84, i32 %add29
  %87 = load i8, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %88, i32 0
  store i8 %87, i8* %arrayidx31, align 1, !tbaa !39
  %89 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %90 = load i32, i32* %y, align 4, !tbaa !32
  %91 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add32 = add nsw i32 %90, %91
  %arrayidx33 = getelementptr inbounds i8, i8* %89, i32 %add32
  %92 = load i8, i8* %arrayidx33, align 1, !tbaa !39
  %93 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i8, i8* %93, i32 1
  store i8 %92, i8* %arrayidx34, align 1, !tbaa !39
  %94 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %95 = load i32, i32* %y, align 4, !tbaa !32
  %96 = load i32, i32* %cblue, align 4, !tbaa !32
  %add35 = add nsw i32 %95, %96
  %arrayidx36 = getelementptr inbounds i8, i8* %94, i32 %add35
  %97 = load i8, i8* %arrayidx36, align 1, !tbaa !39
  %98 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i8, i8* %98, i32 2
  store i8 %97, i8* %arrayidx37, align 1, !tbaa !39
  %99 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i8, i8* %99, i32 3
  store i8 -1, i8* %arrayidx38, align 1, !tbaa !39
  %100 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr39 = getelementptr inbounds i8, i8* %100, i32 4
  store i8* %add.ptr39, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %101 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %101, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 27
  %103 = load i32, i32* %output_width40, align 8, !tbaa !19
  %and = and i32 %103, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %104 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %105 = load i8, i8* %104, align 1, !tbaa !39
  %conv41 = zext i8 %105 to i32
  store i32 %conv41, i32* %cb, align 4, !tbaa !32
  %106 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %107 = load i8, i8* %106, align 1, !tbaa !39
  %conv42 = zext i8 %107 to i32
  store i32 %conv42, i32* %cr, align 4, !tbaa !32
  %108 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %109 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx43 = getelementptr inbounds i32, i32* %108, i32 %109
  %110 = load i32, i32* %arrayidx43, align 4, !tbaa !32
  store i32 %110, i32* %cred, align 4, !tbaa !32
  %111 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %112 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx44 = getelementptr inbounds i32, i32* %111, i32 %112
  %113 = load i32, i32* %arrayidx44, align 4, !tbaa !37
  %114 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %115 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx45 = getelementptr inbounds i32, i32* %114, i32 %115
  %116 = load i32, i32* %arrayidx45, align 4, !tbaa !37
  %add46 = add nsw i32 %113, %116
  %shr47 = ashr i32 %add46, 16
  store i32 %shr47, i32* %cgreen, align 4, !tbaa !32
  %117 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %118 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx48 = getelementptr inbounds i32, i32* %117, i32 %118
  %119 = load i32, i32* %arrayidx48, align 4, !tbaa !32
  store i32 %119, i32* %cblue, align 4, !tbaa !32
  %120 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %121 = load i8, i8* %120, align 1, !tbaa !39
  %conv49 = zext i8 %121 to i32
  store i32 %conv49, i32* %y, align 4, !tbaa !32
  %122 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %123 = load i32, i32* %y, align 4, !tbaa !32
  %124 = load i32, i32* %cred, align 4, !tbaa !32
  %add50 = add nsw i32 %123, %124
  %arrayidx51 = getelementptr inbounds i8, i8* %122, i32 %add50
  %125 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %126 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %126, i32 0
  store i8 %125, i8* %arrayidx52, align 1, !tbaa !39
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !32
  %129 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add53 = add nsw i32 %128, %129
  %arrayidx54 = getelementptr inbounds i8, i8* %127, i32 %add53
  %130 = load i8, i8* %arrayidx54, align 1, !tbaa !39
  %131 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %131, i32 1
  store i8 %130, i8* %arrayidx55, align 1, !tbaa !39
  %132 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %133 = load i32, i32* %y, align 4, !tbaa !32
  %134 = load i32, i32* %cblue, align 4, !tbaa !32
  %add56 = add nsw i32 %133, %134
  %arrayidx57 = getelementptr inbounds i8, i8* %132, i32 %add56
  %135 = load i8, i8* %arrayidx57, align 1, !tbaa !39
  %136 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i8, i8* %136, i32 2
  store i8 %135, i8* %arrayidx58, align 1, !tbaa !39
  %137 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i8, i8* %137, i32 3
  store i8 -1, i8* %arrayidx59, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %138 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  %143 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %144 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  %149 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  %152 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %153 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  %154 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgr_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %30, i32 0
  %31 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %32 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %33, i8** %inptr0, align 4, !tbaa !2
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %34, i32 1
  %35 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %37, i8** %inptr1, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %38, i32 2
  %39 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %41, i8** %inptr2, align 4, !tbaa !2
  %42 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %42, i32 0
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %outptr, align 4, !tbaa !2
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 27
  %45 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %45, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %46 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %46, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %47 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !39
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %49 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !39
  %conv9 = zext i8 %50 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %51 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %52 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %53, i32* %cred, align 4, !tbaa !32
  %54 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %55 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %54, i32 %55
  %56 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %57 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %58 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %57, i32 %58
  %59 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %56, %59
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %60 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %61 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %62, i32* %cblue, align 4, !tbaa !32
  %63 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !39
  %conv16 = zext i8 %64 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %65 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %66 = load i32, i32* %y, align 4, !tbaa !32
  %67 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %66, %67
  %arrayidx18 = getelementptr inbounds i8, i8* %65, i32 %add17
  %68 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %69, i32 2
  store i8 %68, i8* %arrayidx19, align 1, !tbaa !39
  %70 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !32
  %72 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %71, %72
  %arrayidx21 = getelementptr inbounds i8, i8* %70, i32 %add20
  %73 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %74 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8, i8* %74, i32 1
  store i8 %73, i8* %arrayidx22, align 1, !tbaa !39
  %75 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %76 = load i32, i32* %y, align 4, !tbaa !32
  %77 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %76, %77
  %arrayidx24 = getelementptr inbounds i8, i8* %75, i32 %add23
  %78 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %79, i32 0
  store i8 %78, i8* %arrayidx25, align 1, !tbaa !39
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %80, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %81 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr26 = getelementptr inbounds i8, i8* %81, i32 1
  store i8* %incdec.ptr26, i8** %inptr0, align 4, !tbaa !2
  %82 = load i8, i8* %81, align 1, !tbaa !39
  %conv27 = zext i8 %82 to i32
  store i32 %conv27, i32* %y, align 4, !tbaa !32
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cred, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 2
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %89 = load i32, i32* %y, align 4, !tbaa !32
  %90 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add31 = add nsw i32 %89, %90
  %arrayidx32 = getelementptr inbounds i8, i8* %88, i32 %add31
  %91 = load i8, i8* %arrayidx32, align 1, !tbaa !39
  %92 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i8, i8* %92, i32 1
  store i8 %91, i8* %arrayidx33, align 1, !tbaa !39
  %93 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %94 = load i32, i32* %y, align 4, !tbaa !32
  %95 = load i32, i32* %cblue, align 4, !tbaa !32
  %add34 = add nsw i32 %94, %95
  %arrayidx35 = getelementptr inbounds i8, i8* %93, i32 %add34
  %96 = load i8, i8* %arrayidx35, align 1, !tbaa !39
  %97 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %97, i32 0
  store i8 %96, i8* %arrayidx36, align 1, !tbaa !39
  %98 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr37 = getelementptr inbounds i8, i8* %98, i32 3
  store i8* %add.ptr37, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %99 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %99, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %100 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %100, i32 0, i32 27
  %101 = load i32, i32* %output_width38, align 8, !tbaa !19
  %and = and i32 %101, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %102 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %103 = load i8, i8* %102, align 1, !tbaa !39
  %conv39 = zext i8 %103 to i32
  store i32 %conv39, i32* %cb, align 4, !tbaa !32
  %104 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %105 = load i8, i8* %104, align 1, !tbaa !39
  %conv40 = zext i8 %105 to i32
  store i32 %conv40, i32* %cr, align 4, !tbaa !32
  %106 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %107 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx41 = getelementptr inbounds i32, i32* %106, i32 %107
  %108 = load i32, i32* %arrayidx41, align 4, !tbaa !32
  store i32 %108, i32* %cred, align 4, !tbaa !32
  %109 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %110 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx42 = getelementptr inbounds i32, i32* %109, i32 %110
  %111 = load i32, i32* %arrayidx42, align 4, !tbaa !37
  %112 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %113 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx43 = getelementptr inbounds i32, i32* %112, i32 %113
  %114 = load i32, i32* %arrayidx43, align 4, !tbaa !37
  %add44 = add nsw i32 %111, %114
  %shr45 = ashr i32 %add44, 16
  store i32 %shr45, i32* %cgreen, align 4, !tbaa !32
  %115 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %116 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx46 = getelementptr inbounds i32, i32* %115, i32 %116
  %117 = load i32, i32* %arrayidx46, align 4, !tbaa !32
  store i32 %117, i32* %cblue, align 4, !tbaa !32
  %118 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %119 = load i8, i8* %118, align 1, !tbaa !39
  %conv47 = zext i8 %119 to i32
  store i32 %conv47, i32* %y, align 4, !tbaa !32
  %120 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %121 = load i32, i32* %y, align 4, !tbaa !32
  %122 = load i32, i32* %cred, align 4, !tbaa !32
  %add48 = add nsw i32 %121, %122
  %arrayidx49 = getelementptr inbounds i8, i8* %120, i32 %add48
  %123 = load i8, i8* %arrayidx49, align 1, !tbaa !39
  %124 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i8, i8* %124, i32 2
  store i8 %123, i8* %arrayidx50, align 1, !tbaa !39
  %125 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %126 = load i32, i32* %y, align 4, !tbaa !32
  %127 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add51 = add nsw i32 %126, %127
  %arrayidx52 = getelementptr inbounds i8, i8* %125, i32 %add51
  %128 = load i8, i8* %arrayidx52, align 1, !tbaa !39
  %129 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i8, i8* %129, i32 1
  store i8 %128, i8* %arrayidx53, align 1, !tbaa !39
  %130 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %131 = load i32, i32* %y, align 4, !tbaa !32
  %132 = load i32, i32* %cblue, align 4, !tbaa !32
  %add54 = add nsw i32 %131, %132
  %arrayidx55 = getelementptr inbounds i8, i8* %130, i32 %add54
  %133 = load i8, i8* %arrayidx55, align 1, !tbaa !39
  %134 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i8, i8* %134, i32 0
  store i8 %133, i8* %arrayidx56, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %135 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  %136 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  %137 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #4
  %138 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  %143 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %144 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  %149 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extbgrx_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %30, i32 0
  %31 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %32 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %33, i8** %inptr0, align 4, !tbaa !2
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %34, i32 1
  %35 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %37, i8** %inptr1, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %38, i32 2
  %39 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %41, i8** %inptr2, align 4, !tbaa !2
  %42 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %42, i32 0
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %outptr, align 4, !tbaa !2
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 27
  %45 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %45, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %46 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %46, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %47 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !39
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %49 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !39
  %conv9 = zext i8 %50 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %51 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %52 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %53, i32* %cred, align 4, !tbaa !32
  %54 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %55 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %54, i32 %55
  %56 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %57 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %58 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %57, i32 %58
  %59 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %56, %59
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %60 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %61 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %62, i32* %cblue, align 4, !tbaa !32
  %63 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !39
  %conv16 = zext i8 %64 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %65 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %66 = load i32, i32* %y, align 4, !tbaa !32
  %67 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %66, %67
  %arrayidx18 = getelementptr inbounds i8, i8* %65, i32 %add17
  %68 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %69, i32 2
  store i8 %68, i8* %arrayidx19, align 1, !tbaa !39
  %70 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !32
  %72 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %71, %72
  %arrayidx21 = getelementptr inbounds i8, i8* %70, i32 %add20
  %73 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %74 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8, i8* %74, i32 1
  store i8 %73, i8* %arrayidx22, align 1, !tbaa !39
  %75 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %76 = load i32, i32* %y, align 4, !tbaa !32
  %77 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %76, %77
  %arrayidx24 = getelementptr inbounds i8, i8* %75, i32 %add23
  %78 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %79, i32 0
  store i8 %78, i8* %arrayidx25, align 1, !tbaa !39
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %80, i32 3
  store i8 -1, i8* %arrayidx26, align 1, !tbaa !39
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %81, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %82 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8* %incdec.ptr27, i8** %inptr0, align 4, !tbaa !2
  %83 = load i8, i8* %82, align 1, !tbaa !39
  %conv28 = zext i8 %83 to i32
  store i32 %conv28, i32* %y, align 4, !tbaa !32
  %84 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %85 = load i32, i32* %y, align 4, !tbaa !32
  %86 = load i32, i32* %cred, align 4, !tbaa !32
  %add29 = add nsw i32 %85, %86
  %arrayidx30 = getelementptr inbounds i8, i8* %84, i32 %add29
  %87 = load i8, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %88, i32 2
  store i8 %87, i8* %arrayidx31, align 1, !tbaa !39
  %89 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %90 = load i32, i32* %y, align 4, !tbaa !32
  %91 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add32 = add nsw i32 %90, %91
  %arrayidx33 = getelementptr inbounds i8, i8* %89, i32 %add32
  %92 = load i8, i8* %arrayidx33, align 1, !tbaa !39
  %93 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i8, i8* %93, i32 1
  store i8 %92, i8* %arrayidx34, align 1, !tbaa !39
  %94 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %95 = load i32, i32* %y, align 4, !tbaa !32
  %96 = load i32, i32* %cblue, align 4, !tbaa !32
  %add35 = add nsw i32 %95, %96
  %arrayidx36 = getelementptr inbounds i8, i8* %94, i32 %add35
  %97 = load i8, i8* %arrayidx36, align 1, !tbaa !39
  %98 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i8, i8* %98, i32 0
  store i8 %97, i8* %arrayidx37, align 1, !tbaa !39
  %99 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i8, i8* %99, i32 3
  store i8 -1, i8* %arrayidx38, align 1, !tbaa !39
  %100 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr39 = getelementptr inbounds i8, i8* %100, i32 4
  store i8* %add.ptr39, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %101 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %101, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 27
  %103 = load i32, i32* %output_width40, align 8, !tbaa !19
  %and = and i32 %103, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %104 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %105 = load i8, i8* %104, align 1, !tbaa !39
  %conv41 = zext i8 %105 to i32
  store i32 %conv41, i32* %cb, align 4, !tbaa !32
  %106 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %107 = load i8, i8* %106, align 1, !tbaa !39
  %conv42 = zext i8 %107 to i32
  store i32 %conv42, i32* %cr, align 4, !tbaa !32
  %108 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %109 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx43 = getelementptr inbounds i32, i32* %108, i32 %109
  %110 = load i32, i32* %arrayidx43, align 4, !tbaa !32
  store i32 %110, i32* %cred, align 4, !tbaa !32
  %111 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %112 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx44 = getelementptr inbounds i32, i32* %111, i32 %112
  %113 = load i32, i32* %arrayidx44, align 4, !tbaa !37
  %114 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %115 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx45 = getelementptr inbounds i32, i32* %114, i32 %115
  %116 = load i32, i32* %arrayidx45, align 4, !tbaa !37
  %add46 = add nsw i32 %113, %116
  %shr47 = ashr i32 %add46, 16
  store i32 %shr47, i32* %cgreen, align 4, !tbaa !32
  %117 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %118 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx48 = getelementptr inbounds i32, i32* %117, i32 %118
  %119 = load i32, i32* %arrayidx48, align 4, !tbaa !32
  store i32 %119, i32* %cblue, align 4, !tbaa !32
  %120 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %121 = load i8, i8* %120, align 1, !tbaa !39
  %conv49 = zext i8 %121 to i32
  store i32 %conv49, i32* %y, align 4, !tbaa !32
  %122 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %123 = load i32, i32* %y, align 4, !tbaa !32
  %124 = load i32, i32* %cred, align 4, !tbaa !32
  %add50 = add nsw i32 %123, %124
  %arrayidx51 = getelementptr inbounds i8, i8* %122, i32 %add50
  %125 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %126 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %126, i32 2
  store i8 %125, i8* %arrayidx52, align 1, !tbaa !39
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !32
  %129 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add53 = add nsw i32 %128, %129
  %arrayidx54 = getelementptr inbounds i8, i8* %127, i32 %add53
  %130 = load i8, i8* %arrayidx54, align 1, !tbaa !39
  %131 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %131, i32 1
  store i8 %130, i8* %arrayidx55, align 1, !tbaa !39
  %132 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %133 = load i32, i32* %y, align 4, !tbaa !32
  %134 = load i32, i32* %cblue, align 4, !tbaa !32
  %add56 = add nsw i32 %133, %134
  %arrayidx57 = getelementptr inbounds i8, i8* %132, i32 %add56
  %135 = load i8, i8* %arrayidx57, align 1, !tbaa !39
  %136 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i8, i8* %136, i32 0
  store i8 %135, i8* %arrayidx58, align 1, !tbaa !39
  %137 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i8, i8* %137, i32 3
  store i8 -1, i8* %arrayidx59, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %138 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  %143 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %144 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  %149 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  %152 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %153 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  %154 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxbgr_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %30, i32 0
  %31 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %32 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %33, i8** %inptr0, align 4, !tbaa !2
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %34, i32 1
  %35 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %37, i8** %inptr1, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %38, i32 2
  %39 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %41, i8** %inptr2, align 4, !tbaa !2
  %42 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %42, i32 0
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %outptr, align 4, !tbaa !2
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 27
  %45 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %45, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %46 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %46, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %47 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !39
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %49 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !39
  %conv9 = zext i8 %50 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %51 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %52 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %53, i32* %cred, align 4, !tbaa !32
  %54 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %55 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %54, i32 %55
  %56 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %57 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %58 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %57, i32 %58
  %59 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %56, %59
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %60 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %61 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %62, i32* %cblue, align 4, !tbaa !32
  %63 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !39
  %conv16 = zext i8 %64 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %65 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %66 = load i32, i32* %y, align 4, !tbaa !32
  %67 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %66, %67
  %arrayidx18 = getelementptr inbounds i8, i8* %65, i32 %add17
  %68 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %69, i32 3
  store i8 %68, i8* %arrayidx19, align 1, !tbaa !39
  %70 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !32
  %72 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %71, %72
  %arrayidx21 = getelementptr inbounds i8, i8* %70, i32 %add20
  %73 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %74 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8, i8* %74, i32 2
  store i8 %73, i8* %arrayidx22, align 1, !tbaa !39
  %75 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %76 = load i32, i32* %y, align 4, !tbaa !32
  %77 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %76, %77
  %arrayidx24 = getelementptr inbounds i8, i8* %75, i32 %add23
  %78 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %79, i32 1
  store i8 %78, i8* %arrayidx25, align 1, !tbaa !39
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %80, i32 0
  store i8 -1, i8* %arrayidx26, align 1, !tbaa !39
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %81, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %82 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8* %incdec.ptr27, i8** %inptr0, align 4, !tbaa !2
  %83 = load i8, i8* %82, align 1, !tbaa !39
  %conv28 = zext i8 %83 to i32
  store i32 %conv28, i32* %y, align 4, !tbaa !32
  %84 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %85 = load i32, i32* %y, align 4, !tbaa !32
  %86 = load i32, i32* %cred, align 4, !tbaa !32
  %add29 = add nsw i32 %85, %86
  %arrayidx30 = getelementptr inbounds i8, i8* %84, i32 %add29
  %87 = load i8, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %88, i32 3
  store i8 %87, i8* %arrayidx31, align 1, !tbaa !39
  %89 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %90 = load i32, i32* %y, align 4, !tbaa !32
  %91 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add32 = add nsw i32 %90, %91
  %arrayidx33 = getelementptr inbounds i8, i8* %89, i32 %add32
  %92 = load i8, i8* %arrayidx33, align 1, !tbaa !39
  %93 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i8, i8* %93, i32 2
  store i8 %92, i8* %arrayidx34, align 1, !tbaa !39
  %94 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %95 = load i32, i32* %y, align 4, !tbaa !32
  %96 = load i32, i32* %cblue, align 4, !tbaa !32
  %add35 = add nsw i32 %95, %96
  %arrayidx36 = getelementptr inbounds i8, i8* %94, i32 %add35
  %97 = load i8, i8* %arrayidx36, align 1, !tbaa !39
  %98 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i8, i8* %98, i32 1
  store i8 %97, i8* %arrayidx37, align 1, !tbaa !39
  %99 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i8, i8* %99, i32 0
  store i8 -1, i8* %arrayidx38, align 1, !tbaa !39
  %100 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr39 = getelementptr inbounds i8, i8* %100, i32 4
  store i8* %add.ptr39, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %101 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %101, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 27
  %103 = load i32, i32* %output_width40, align 8, !tbaa !19
  %and = and i32 %103, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %104 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %105 = load i8, i8* %104, align 1, !tbaa !39
  %conv41 = zext i8 %105 to i32
  store i32 %conv41, i32* %cb, align 4, !tbaa !32
  %106 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %107 = load i8, i8* %106, align 1, !tbaa !39
  %conv42 = zext i8 %107 to i32
  store i32 %conv42, i32* %cr, align 4, !tbaa !32
  %108 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %109 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx43 = getelementptr inbounds i32, i32* %108, i32 %109
  %110 = load i32, i32* %arrayidx43, align 4, !tbaa !32
  store i32 %110, i32* %cred, align 4, !tbaa !32
  %111 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %112 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx44 = getelementptr inbounds i32, i32* %111, i32 %112
  %113 = load i32, i32* %arrayidx44, align 4, !tbaa !37
  %114 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %115 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx45 = getelementptr inbounds i32, i32* %114, i32 %115
  %116 = load i32, i32* %arrayidx45, align 4, !tbaa !37
  %add46 = add nsw i32 %113, %116
  %shr47 = ashr i32 %add46, 16
  store i32 %shr47, i32* %cgreen, align 4, !tbaa !32
  %117 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %118 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx48 = getelementptr inbounds i32, i32* %117, i32 %118
  %119 = load i32, i32* %arrayidx48, align 4, !tbaa !32
  store i32 %119, i32* %cblue, align 4, !tbaa !32
  %120 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %121 = load i8, i8* %120, align 1, !tbaa !39
  %conv49 = zext i8 %121 to i32
  store i32 %conv49, i32* %y, align 4, !tbaa !32
  %122 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %123 = load i32, i32* %y, align 4, !tbaa !32
  %124 = load i32, i32* %cred, align 4, !tbaa !32
  %add50 = add nsw i32 %123, %124
  %arrayidx51 = getelementptr inbounds i8, i8* %122, i32 %add50
  %125 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %126 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %126, i32 3
  store i8 %125, i8* %arrayidx52, align 1, !tbaa !39
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !32
  %129 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add53 = add nsw i32 %128, %129
  %arrayidx54 = getelementptr inbounds i8, i8* %127, i32 %add53
  %130 = load i8, i8* %arrayidx54, align 1, !tbaa !39
  %131 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %131, i32 2
  store i8 %130, i8* %arrayidx55, align 1, !tbaa !39
  %132 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %133 = load i32, i32* %y, align 4, !tbaa !32
  %134 = load i32, i32* %cblue, align 4, !tbaa !32
  %add56 = add nsw i32 %133, %134
  %arrayidx57 = getelementptr inbounds i8, i8* %132, i32 %add56
  %135 = load i8, i8* %arrayidx57, align 1, !tbaa !39
  %136 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i8, i8* %136, i32 1
  store i8 %135, i8* %arrayidx58, align 1, !tbaa !39
  %137 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i8, i8* %137, i32 0
  store i8 -1, i8* %arrayidx59, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %138 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  %143 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %144 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  %149 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  %152 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %153 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  %154 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @extxrgb_h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %30, i32 0
  %31 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %32 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %33, i8** %inptr0, align 4, !tbaa !2
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %34, i32 1
  %35 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %37, i8** %inptr1, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %38, i32 2
  %39 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %41, i8** %inptr2, align 4, !tbaa !2
  %42 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %42, i32 0
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %outptr, align 4, !tbaa !2
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 27
  %45 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %45, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %46 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %46, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %47 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !39
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %49 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !39
  %conv9 = zext i8 %50 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %51 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %52 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %53, i32* %cred, align 4, !tbaa !32
  %54 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %55 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %54, i32 %55
  %56 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %57 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %58 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %57, i32 %58
  %59 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %56, %59
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %60 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %61 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %62, i32* %cblue, align 4, !tbaa !32
  %63 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !39
  %conv16 = zext i8 %64 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %65 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %66 = load i32, i32* %y, align 4, !tbaa !32
  %67 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %66, %67
  %arrayidx18 = getelementptr inbounds i8, i8* %65, i32 %add17
  %68 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %69, i32 1
  store i8 %68, i8* %arrayidx19, align 1, !tbaa !39
  %70 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !32
  %72 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %71, %72
  %arrayidx21 = getelementptr inbounds i8, i8* %70, i32 %add20
  %73 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %74 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8, i8* %74, i32 2
  store i8 %73, i8* %arrayidx22, align 1, !tbaa !39
  %75 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %76 = load i32, i32* %y, align 4, !tbaa !32
  %77 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %76, %77
  %arrayidx24 = getelementptr inbounds i8, i8* %75, i32 %add23
  %78 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %79, i32 3
  store i8 %78, i8* %arrayidx25, align 1, !tbaa !39
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %80, i32 0
  store i8 -1, i8* %arrayidx26, align 1, !tbaa !39
  %81 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %81, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %82 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr27 = getelementptr inbounds i8, i8* %82, i32 1
  store i8* %incdec.ptr27, i8** %inptr0, align 4, !tbaa !2
  %83 = load i8, i8* %82, align 1, !tbaa !39
  %conv28 = zext i8 %83 to i32
  store i32 %conv28, i32* %y, align 4, !tbaa !32
  %84 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %85 = load i32, i32* %y, align 4, !tbaa !32
  %86 = load i32, i32* %cred, align 4, !tbaa !32
  %add29 = add nsw i32 %85, %86
  %arrayidx30 = getelementptr inbounds i8, i8* %84, i32 %add29
  %87 = load i8, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i8, i8* %88, i32 1
  store i8 %87, i8* %arrayidx31, align 1, !tbaa !39
  %89 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %90 = load i32, i32* %y, align 4, !tbaa !32
  %91 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add32 = add nsw i32 %90, %91
  %arrayidx33 = getelementptr inbounds i8, i8* %89, i32 %add32
  %92 = load i8, i8* %arrayidx33, align 1, !tbaa !39
  %93 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i8, i8* %93, i32 2
  store i8 %92, i8* %arrayidx34, align 1, !tbaa !39
  %94 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %95 = load i32, i32* %y, align 4, !tbaa !32
  %96 = load i32, i32* %cblue, align 4, !tbaa !32
  %add35 = add nsw i32 %95, %96
  %arrayidx36 = getelementptr inbounds i8, i8* %94, i32 %add35
  %97 = load i8, i8* %arrayidx36, align 1, !tbaa !39
  %98 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i8, i8* %98, i32 3
  store i8 %97, i8* %arrayidx37, align 1, !tbaa !39
  %99 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds i8, i8* %99, i32 0
  store i8 -1, i8* %arrayidx38, align 1, !tbaa !39
  %100 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr39 = getelementptr inbounds i8, i8* %100, i32 4
  store i8* %add.ptr39, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %101 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %101, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width40 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 27
  %103 = load i32, i32* %output_width40, align 8, !tbaa !19
  %and = and i32 %103, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %104 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %105 = load i8, i8* %104, align 1, !tbaa !39
  %conv41 = zext i8 %105 to i32
  store i32 %conv41, i32* %cb, align 4, !tbaa !32
  %106 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %107 = load i8, i8* %106, align 1, !tbaa !39
  %conv42 = zext i8 %107 to i32
  store i32 %conv42, i32* %cr, align 4, !tbaa !32
  %108 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %109 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx43 = getelementptr inbounds i32, i32* %108, i32 %109
  %110 = load i32, i32* %arrayidx43, align 4, !tbaa !32
  store i32 %110, i32* %cred, align 4, !tbaa !32
  %111 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %112 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx44 = getelementptr inbounds i32, i32* %111, i32 %112
  %113 = load i32, i32* %arrayidx44, align 4, !tbaa !37
  %114 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %115 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx45 = getelementptr inbounds i32, i32* %114, i32 %115
  %116 = load i32, i32* %arrayidx45, align 4, !tbaa !37
  %add46 = add nsw i32 %113, %116
  %shr47 = ashr i32 %add46, 16
  store i32 %shr47, i32* %cgreen, align 4, !tbaa !32
  %117 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %118 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx48 = getelementptr inbounds i32, i32* %117, i32 %118
  %119 = load i32, i32* %arrayidx48, align 4, !tbaa !32
  store i32 %119, i32* %cblue, align 4, !tbaa !32
  %120 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %121 = load i8, i8* %120, align 1, !tbaa !39
  %conv49 = zext i8 %121 to i32
  store i32 %conv49, i32* %y, align 4, !tbaa !32
  %122 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %123 = load i32, i32* %y, align 4, !tbaa !32
  %124 = load i32, i32* %cred, align 4, !tbaa !32
  %add50 = add nsw i32 %123, %124
  %arrayidx51 = getelementptr inbounds i8, i8* %122, i32 %add50
  %125 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %126 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds i8, i8* %126, i32 1
  store i8 %125, i8* %arrayidx52, align 1, !tbaa !39
  %127 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %128 = load i32, i32* %y, align 4, !tbaa !32
  %129 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add53 = add nsw i32 %128, %129
  %arrayidx54 = getelementptr inbounds i8, i8* %127, i32 %add53
  %130 = load i8, i8* %arrayidx54, align 1, !tbaa !39
  %131 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds i8, i8* %131, i32 2
  store i8 %130, i8* %arrayidx55, align 1, !tbaa !39
  %132 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %133 = load i32, i32* %y, align 4, !tbaa !32
  %134 = load i32, i32* %cblue, align 4, !tbaa !32
  %add56 = add nsw i32 %133, %134
  %arrayidx57 = getelementptr inbounds i8, i8* %132, i32 %add56
  %135 = load i8, i8* %arrayidx57, align 1, !tbaa !39
  %136 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds i8, i8* %136, i32 3
  store i8 %135, i8* %arrayidx58, align 1, !tbaa !39
  %137 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i8, i8* %137, i32 0
  store i8 -1, i8* %arrayidx59, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %138 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  %143 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %144 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  %149 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  %152 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %153 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  %154 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v1_merged_upsample_internal(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %30, i32 0
  %31 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %32 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %33, i8** %inptr0, align 4, !tbaa !2
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %34, i32 1
  %35 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %37, i8** %inptr1, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %38, i32 2
  %39 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %41, i8** %inptr2, align 4, !tbaa !2
  %42 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %42, i32 0
  %43 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %43, i8** %outptr, align 4, !tbaa !2
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %44, i32 0, i32 27
  %45 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %45, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %46 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %46, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %47 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !39
  %conv = zext i8 %48 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %49 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8, i8* %49, align 1, !tbaa !39
  %conv9 = zext i8 %50 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %51 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %52 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %51, i32 %52
  %53 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %53, i32* %cred, align 4, !tbaa !32
  %54 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %55 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %54, i32 %55
  %56 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %57 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %58 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %57, i32 %58
  %59 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %56, %59
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %60 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %61 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %62, i32* %cblue, align 4, !tbaa !32
  %63 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %63, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %64 = load i8, i8* %63, align 1, !tbaa !39
  %conv16 = zext i8 %64 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %65 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %66 = load i32, i32* %y, align 4, !tbaa !32
  %67 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %66, %67
  %arrayidx18 = getelementptr inbounds i8, i8* %65, i32 %add17
  %68 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %69 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i8, i8* %69, i32 0
  store i8 %68, i8* %arrayidx19, align 1, !tbaa !39
  %70 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %71 = load i32, i32* %y, align 4, !tbaa !32
  %72 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %71, %72
  %arrayidx21 = getelementptr inbounds i8, i8* %70, i32 %add20
  %73 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %74 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i8, i8* %74, i32 1
  store i8 %73, i8* %arrayidx22, align 1, !tbaa !39
  %75 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %76 = load i32, i32* %y, align 4, !tbaa !32
  %77 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %76, %77
  %arrayidx24 = getelementptr inbounds i8, i8* %75, i32 %add23
  %78 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %79 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i8, i8* %79, i32 2
  store i8 %78, i8* %arrayidx25, align 1, !tbaa !39
  %80 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %80, i32 3
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  %81 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr26 = getelementptr inbounds i8, i8* %81, i32 1
  store i8* %incdec.ptr26, i8** %inptr0, align 4, !tbaa !2
  %82 = load i8, i8* %81, align 1, !tbaa !39
  %conv27 = zext i8 %82 to i32
  store i32 %conv27, i32* %y, align 4, !tbaa !32
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cred, align 4, !tbaa !32
  %add28 = add nsw i32 %84, %85
  %arrayidx29 = getelementptr inbounds i8, i8* %83, i32 %add28
  %86 = load i8, i8* %arrayidx29, align 1, !tbaa !39
  %87 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i8, i8* %87, i32 0
  store i8 %86, i8* %arrayidx30, align 1, !tbaa !39
  %88 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %89 = load i32, i32* %y, align 4, !tbaa !32
  %90 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add31 = add nsw i32 %89, %90
  %arrayidx32 = getelementptr inbounds i8, i8* %88, i32 %add31
  %91 = load i8, i8* %arrayidx32, align 1, !tbaa !39
  %92 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i8, i8* %92, i32 1
  store i8 %91, i8* %arrayidx33, align 1, !tbaa !39
  %93 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %94 = load i32, i32* %y, align 4, !tbaa !32
  %95 = load i32, i32* %cblue, align 4, !tbaa !32
  %add34 = add nsw i32 %94, %95
  %arrayidx35 = getelementptr inbounds i8, i8* %93, i32 %add34
  %96 = load i8, i8* %arrayidx35, align 1, !tbaa !39
  %97 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds i8, i8* %97, i32 2
  store i8 %96, i8* %arrayidx36, align 1, !tbaa !39
  %98 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr37 = getelementptr inbounds i8, i8* %98, i32 3
  store i8* %add.ptr37, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %99 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %99, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %100 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width38 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %100, i32 0, i32 27
  %101 = load i32, i32* %output_width38, align 8, !tbaa !19
  %and = and i32 %101, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %102 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %103 = load i8, i8* %102, align 1, !tbaa !39
  %conv39 = zext i8 %103 to i32
  store i32 %conv39, i32* %cb, align 4, !tbaa !32
  %104 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %105 = load i8, i8* %104, align 1, !tbaa !39
  %conv40 = zext i8 %105 to i32
  store i32 %conv40, i32* %cr, align 4, !tbaa !32
  %106 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %107 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx41 = getelementptr inbounds i32, i32* %106, i32 %107
  %108 = load i32, i32* %arrayidx41, align 4, !tbaa !32
  store i32 %108, i32* %cred, align 4, !tbaa !32
  %109 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %110 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx42 = getelementptr inbounds i32, i32* %109, i32 %110
  %111 = load i32, i32* %arrayidx42, align 4, !tbaa !37
  %112 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %113 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx43 = getelementptr inbounds i32, i32* %112, i32 %113
  %114 = load i32, i32* %arrayidx43, align 4, !tbaa !37
  %add44 = add nsw i32 %111, %114
  %shr45 = ashr i32 %add44, 16
  store i32 %shr45, i32* %cgreen, align 4, !tbaa !32
  %115 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %116 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx46 = getelementptr inbounds i32, i32* %115, i32 %116
  %117 = load i32, i32* %arrayidx46, align 4, !tbaa !32
  store i32 %117, i32* %cblue, align 4, !tbaa !32
  %118 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %119 = load i8, i8* %118, align 1, !tbaa !39
  %conv47 = zext i8 %119 to i32
  store i32 %conv47, i32* %y, align 4, !tbaa !32
  %120 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %121 = load i32, i32* %y, align 4, !tbaa !32
  %122 = load i32, i32* %cred, align 4, !tbaa !32
  %add48 = add nsw i32 %121, %122
  %arrayidx49 = getelementptr inbounds i8, i8* %120, i32 %add48
  %123 = load i8, i8* %arrayidx49, align 1, !tbaa !39
  %124 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i8, i8* %124, i32 0
  store i8 %123, i8* %arrayidx50, align 1, !tbaa !39
  %125 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %126 = load i32, i32* %y, align 4, !tbaa !32
  %127 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add51 = add nsw i32 %126, %127
  %arrayidx52 = getelementptr inbounds i8, i8* %125, i32 %add51
  %128 = load i8, i8* %arrayidx52, align 1, !tbaa !39
  %129 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx53 = getelementptr inbounds i8, i8* %129, i32 1
  store i8 %128, i8* %arrayidx53, align 1, !tbaa !39
  %130 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %131 = load i32, i32* %y, align 4, !tbaa !32
  %132 = load i32, i32* %cblue, align 4, !tbaa !32
  %add54 = add nsw i32 %131, %132
  %arrayidx55 = getelementptr inbounds i8, i8* %130, i32 %add54
  %133 = load i8, i8* %arrayidx55, align 1, !tbaa !39
  %134 = load i8*, i8** %outptr, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds i8, i8* %134, i32 2
  store i8 %133, i8* %arrayidx56, align 1, !tbaa !39
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %135 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #4
  %136 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #4
  %137 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #4
  %138 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #4
  %139 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #4
  %140 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #4
  %141 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #4
  %142 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #4
  %143 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #4
  %144 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #4
  %145 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #4
  %146 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #4
  %147 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #4
  %148 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  %149 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v1_merged_upsample_565D_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %d0 = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %rgb = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 34
  %32 = load i32, i32* %output_scanline, align 4, !tbaa !40
  %and = and i32 %32, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %33 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %33, i32* %d0, align 4, !tbaa !37
  %34 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %38, i32 0
  %39 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx3 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %41, i8** %inptr0, align 4, !tbaa !2
  %42 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %42, i32 1
  %43 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %44 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx5 = getelementptr inbounds i8*, i8** %43, i32 %44
  %45 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %45, i8** %inptr1, align 4, !tbaa !2
  %46 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %46, i32 2
  %47 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %48 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %47, i32 %48
  %49 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %49, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8*, i8** %50, i32 0
  %51 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %51, i8** %outptr, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr9, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv10 = zext i8 %58 to i32
  store i32 %conv10, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx11, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx13 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx13, align 4, !tbaa !37
  %add = add nsw i32 %64, %67
  %shr14 = ashr i32 %add, 16
  store i32 %shr14, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx15, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr16 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr16, i8** %inptr0, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv17 = zext i8 %72 to i32
  store i32 %conv17, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add18 = add nsw i32 %74, %75
  %76 = load i32, i32* %d0, align 4, !tbaa !37
  %and19 = and i32 %76, 255
  %add20 = add nsw i32 %add18, %and19
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 %add20
  %77 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %conv22 = zext i8 %77 to i32
  store i32 %conv22, i32* %r, align 4, !tbaa !32
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add23 = add nsw i32 %79, %80
  %81 = load i32, i32* %d0, align 4, !tbaa !37
  %and24 = and i32 %81, 255
  %shr25 = ashr i32 %and24, 1
  %add26 = add nsw i32 %add23, %shr25
  %arrayidx27 = getelementptr inbounds i8, i8* %78, i32 %add26
  %82 = load i8, i8* %arrayidx27, align 1, !tbaa !39
  %conv28 = zext i8 %82 to i32
  store i32 %conv28, i32* %g, align 4, !tbaa !32
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add29 = add nsw i32 %84, %85
  %86 = load i32, i32* %d0, align 4, !tbaa !37
  %and30 = and i32 %86, 255
  %add31 = add nsw i32 %add29, %and30
  %arrayidx32 = getelementptr inbounds i8, i8* %83, i32 %add31
  %87 = load i8, i8* %arrayidx32, align 1, !tbaa !39
  %conv33 = zext i8 %87 to i32
  store i32 %conv33, i32* %b, align 4, !tbaa !32
  %88 = load i32, i32* %d0, align 4, !tbaa !37
  %and34 = and i32 %88, 255
  %shl = shl i32 %and34, 24
  %89 = load i32, i32* %d0, align 4, !tbaa !37
  %shr35 = ashr i32 %89, 8
  %and36 = and i32 %shr35, 16777215
  %or = or i32 %shl, %and36
  store i32 %or, i32* %d0, align 4, !tbaa !37
  %90 = load i32, i32* %r, align 4, !tbaa !32
  %and37 = and i32 %90, 248
  %91 = load i32, i32* %g, align 4, !tbaa !32
  %shr38 = lshr i32 %91, 5
  %or39 = or i32 %and37, %shr38
  %92 = load i32, i32* %g, align 4, !tbaa !32
  %shl40 = shl i32 %92, 11
  %and41 = and i32 %shl40, 57344
  %or42 = or i32 %or39, %and41
  %93 = load i32, i32* %b, align 4, !tbaa !32
  %shl43 = shl i32 %93, 5
  %and44 = and i32 %shl43, 7936
  %or45 = or i32 %or42, %and44
  store i32 %or45, i32* %rgb, align 4, !tbaa !37
  %94 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr46 = getelementptr inbounds i8, i8* %94, i32 1
  store i8* %incdec.ptr46, i8** %inptr0, align 4, !tbaa !2
  %95 = load i8, i8* %94, align 1, !tbaa !39
  %conv47 = zext i8 %95 to i32
  store i32 %conv47, i32* %y, align 4, !tbaa !32
  %96 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %97 = load i32, i32* %y, align 4, !tbaa !32
  %98 = load i32, i32* %cred, align 4, !tbaa !32
  %add48 = add nsw i32 %97, %98
  %99 = load i32, i32* %d0, align 4, !tbaa !37
  %and49 = and i32 %99, 255
  %add50 = add nsw i32 %add48, %and49
  %arrayidx51 = getelementptr inbounds i8, i8* %96, i32 %add50
  %100 = load i8, i8* %arrayidx51, align 1, !tbaa !39
  %conv52 = zext i8 %100 to i32
  store i32 %conv52, i32* %r, align 4, !tbaa !32
  %101 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %102 = load i32, i32* %y, align 4, !tbaa !32
  %103 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add53 = add nsw i32 %102, %103
  %104 = load i32, i32* %d0, align 4, !tbaa !37
  %and54 = and i32 %104, 255
  %shr55 = ashr i32 %and54, 1
  %add56 = add nsw i32 %add53, %shr55
  %arrayidx57 = getelementptr inbounds i8, i8* %101, i32 %add56
  %105 = load i8, i8* %arrayidx57, align 1, !tbaa !39
  %conv58 = zext i8 %105 to i32
  store i32 %conv58, i32* %g, align 4, !tbaa !32
  %106 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %107 = load i32, i32* %y, align 4, !tbaa !32
  %108 = load i32, i32* %cblue, align 4, !tbaa !32
  %add59 = add nsw i32 %107, %108
  %109 = load i32, i32* %d0, align 4, !tbaa !37
  %and60 = and i32 %109, 255
  %add61 = add nsw i32 %add59, %and60
  %arrayidx62 = getelementptr inbounds i8, i8* %106, i32 %add61
  %110 = load i8, i8* %arrayidx62, align 1, !tbaa !39
  %conv63 = zext i8 %110 to i32
  store i32 %conv63, i32* %b, align 4, !tbaa !32
  %111 = load i32, i32* %d0, align 4, !tbaa !37
  %and64 = and i32 %111, 255
  %shl65 = shl i32 %and64, 24
  %112 = load i32, i32* %d0, align 4, !tbaa !37
  %shr66 = ashr i32 %112, 8
  %and67 = and i32 %shr66, 16777215
  %or68 = or i32 %shl65, %and67
  store i32 %or68, i32* %d0, align 4, !tbaa !37
  %113 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl69 = shl i32 %113, 16
  %114 = load i32, i32* %r, align 4, !tbaa !32
  %and70 = and i32 %114, 248
  %115 = load i32, i32* %g, align 4, !tbaa !32
  %shr71 = lshr i32 %115, 5
  %or72 = or i32 %and70, %shr71
  %116 = load i32, i32* %g, align 4, !tbaa !32
  %shl73 = shl i32 %116, 11
  %and74 = and i32 %shl73, 57344
  %or75 = or i32 %or72, %and74
  %117 = load i32, i32* %b, align 4, !tbaa !32
  %shl76 = shl i32 %117, 5
  %and77 = and i32 %shl76, 7936
  %or78 = or i32 %or75, %and77
  %or79 = or i32 %shl69, %or78
  store i32 %or79, i32* %rgb, align 4, !tbaa !37
  %118 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv80 = trunc i32 %118 to i16
  %119 = load i8*, i8** %outptr, align 4, !tbaa !2
  %120 = bitcast i8* %119 to i16*
  %arrayidx81 = getelementptr inbounds i16, i16* %120, i32 1
  store i16 %conv80, i16* %arrayidx81, align 2, !tbaa !41
  %121 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr82 = ashr i32 %121, 16
  %conv83 = trunc i32 %shr82 to i16
  %122 = load i8*, i8** %outptr, align 4, !tbaa !2
  %123 = bitcast i8* %122 to i16*
  %arrayidx84 = getelementptr inbounds i16, i16* %123, i32 0
  store i16 %conv83, i16* %arrayidx84, align 2, !tbaa !41
  %124 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %124, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %125 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %125, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %126 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width85 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %126, i32 0, i32 27
  %127 = load i32, i32* %output_width85, align 8, !tbaa !19
  %and86 = and i32 %127, 1
  %tobool = icmp ne i32 %and86, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %128 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %129 = load i8, i8* %128, align 1, !tbaa !39
  %conv87 = zext i8 %129 to i32
  store i32 %conv87, i32* %cb, align 4, !tbaa !32
  %130 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %131 = load i8, i8* %130, align 1, !tbaa !39
  %conv88 = zext i8 %131 to i32
  store i32 %conv88, i32* %cr, align 4, !tbaa !32
  %132 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %133 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx89 = getelementptr inbounds i32, i32* %132, i32 %133
  %134 = load i32, i32* %arrayidx89, align 4, !tbaa !32
  store i32 %134, i32* %cred, align 4, !tbaa !32
  %135 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %136 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx90 = getelementptr inbounds i32, i32* %135, i32 %136
  %137 = load i32, i32* %arrayidx90, align 4, !tbaa !37
  %138 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %139 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx91 = getelementptr inbounds i32, i32* %138, i32 %139
  %140 = load i32, i32* %arrayidx91, align 4, !tbaa !37
  %add92 = add nsw i32 %137, %140
  %shr93 = ashr i32 %add92, 16
  store i32 %shr93, i32* %cgreen, align 4, !tbaa !32
  %141 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %142 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx94 = getelementptr inbounds i32, i32* %141, i32 %142
  %143 = load i32, i32* %arrayidx94, align 4, !tbaa !32
  store i32 %143, i32* %cblue, align 4, !tbaa !32
  %144 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %145 = load i8, i8* %144, align 1, !tbaa !39
  %conv95 = zext i8 %145 to i32
  store i32 %conv95, i32* %y, align 4, !tbaa !32
  %146 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %147 = load i32, i32* %y, align 4, !tbaa !32
  %148 = load i32, i32* %cred, align 4, !tbaa !32
  %add96 = add nsw i32 %147, %148
  %149 = load i32, i32* %d0, align 4, !tbaa !37
  %and97 = and i32 %149, 255
  %add98 = add nsw i32 %add96, %and97
  %arrayidx99 = getelementptr inbounds i8, i8* %146, i32 %add98
  %150 = load i8, i8* %arrayidx99, align 1, !tbaa !39
  %conv100 = zext i8 %150 to i32
  store i32 %conv100, i32* %r, align 4, !tbaa !32
  %151 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %152 = load i32, i32* %y, align 4, !tbaa !32
  %153 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add101 = add nsw i32 %152, %153
  %154 = load i32, i32* %d0, align 4, !tbaa !37
  %and102 = and i32 %154, 255
  %shr103 = ashr i32 %and102, 1
  %add104 = add nsw i32 %add101, %shr103
  %arrayidx105 = getelementptr inbounds i8, i8* %151, i32 %add104
  %155 = load i8, i8* %arrayidx105, align 1, !tbaa !39
  %conv106 = zext i8 %155 to i32
  store i32 %conv106, i32* %g, align 4, !tbaa !32
  %156 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %157 = load i32, i32* %y, align 4, !tbaa !32
  %158 = load i32, i32* %cblue, align 4, !tbaa !32
  %add107 = add nsw i32 %157, %158
  %159 = load i32, i32* %d0, align 4, !tbaa !37
  %and108 = and i32 %159, 255
  %add109 = add nsw i32 %add107, %and108
  %arrayidx110 = getelementptr inbounds i8, i8* %156, i32 %add109
  %160 = load i8, i8* %arrayidx110, align 1, !tbaa !39
  %conv111 = zext i8 %160 to i32
  store i32 %conv111, i32* %b, align 4, !tbaa !32
  %161 = load i32, i32* %r, align 4, !tbaa !32
  %and112 = and i32 %161, 248
  %162 = load i32, i32* %g, align 4, !tbaa !32
  %shr113 = lshr i32 %162, 5
  %or114 = or i32 %and112, %shr113
  %163 = load i32, i32* %g, align 4, !tbaa !32
  %shl115 = shl i32 %163, 11
  %and116 = and i32 %shl115, 57344
  %or117 = or i32 %or114, %and116
  %164 = load i32, i32* %b, align 4, !tbaa !32
  %shl118 = shl i32 %164, 5
  %and119 = and i32 %shl118, 7936
  %or120 = or i32 %or117, %and119
  store i32 %or120, i32* %rgb, align 4, !tbaa !37
  %165 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv121 = trunc i32 %165 to i16
  %166 = load i8*, i8** %outptr, align 4, !tbaa !2
  %167 = bitcast i8* %166 to i16*
  store i16 %conv121, i16* %167, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %168 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  %169 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  %170 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #4
  %171 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #4
  %172 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #4
  %173 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #4
  %174 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #4
  %175 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #4
  %176 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #4
  %177 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #4
  %178 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #4
  %179 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #4
  %180 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #4
  %181 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #4
  %182 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #4
  %183 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #4
  %184 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #4
  %185 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #4
  %186 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #4
  %187 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #4
  %188 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #4
  %189 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v1_merged_upsample_565D_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %d0 = alloca i32, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %rgb = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scanline = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 34
  %32 = load i32, i32* %output_scanline, align 4, !tbaa !40
  %and = and i32 %32, 3
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* @dither_matrix, i32 0, i32 %and
  %33 = load i32, i32* %arrayidx, align 4, !tbaa !37
  store i32 %33, i32* %d0, align 4, !tbaa !37
  %34 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #4
  %35 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #4
  %36 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #4
  %37 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #4
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8**, i8*** %38, i32 0
  %39 = load i8**, i8*** %arrayidx2, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx3 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %41, i8** %inptr0, align 4, !tbaa !2
  %42 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8**, i8*** %42, i32 1
  %43 = load i8**, i8*** %arrayidx4, align 4, !tbaa !2
  %44 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx5 = getelementptr inbounds i8*, i8** %43, i32 %44
  %45 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  store i8* %45, i8** %inptr1, align 4, !tbaa !2
  %46 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8**, i8*** %46, i32 2
  %47 = load i8**, i8*** %arrayidx6, align 4, !tbaa !2
  %48 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx7 = getelementptr inbounds i8*, i8** %47, i32 %48
  %49 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %49, i8** %inptr2, align 4, !tbaa !2
  %50 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i8*, i8** %50, i32 0
  %51 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %51, i8** %outptr, align 4, !tbaa !2
  %52 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %52, i32 0, i32 27
  %53 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %53, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %54 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %54, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %55, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %56 = load i8, i8* %55, align 1, !tbaa !39
  %conv = zext i8 %56 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %57 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr9 = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr9, i8** %inptr2, align 4, !tbaa !2
  %58 = load i8, i8* %57, align 1, !tbaa !39
  %conv10 = zext i8 %58 to i32
  store i32 %conv10, i32* %cr, align 4, !tbaa !32
  %59 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %60 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx11, align 4, !tbaa !32
  store i32 %61, i32* %cred, align 4, !tbaa !32
  %62 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %63 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %65 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %66 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx13 = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx13, align 4, !tbaa !37
  %add = add nsw i32 %64, %67
  %shr14 = ashr i32 %add, 16
  store i32 %shr14, i32* %cgreen, align 4, !tbaa !32
  %68 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %69 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx15 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx15, align 4, !tbaa !32
  store i32 %70, i32* %cblue, align 4, !tbaa !32
  %71 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr16 = getelementptr inbounds i8, i8* %71, i32 1
  store i8* %incdec.ptr16, i8** %inptr0, align 4, !tbaa !2
  %72 = load i8, i8* %71, align 1, !tbaa !39
  %conv17 = zext i8 %72 to i32
  store i32 %conv17, i32* %y, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cred, align 4, !tbaa !32
  %add18 = add nsw i32 %74, %75
  %76 = load i32, i32* %d0, align 4, !tbaa !37
  %and19 = and i32 %76, 255
  %add20 = add nsw i32 %add18, %and19
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 %add20
  %77 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %conv22 = zext i8 %77 to i32
  store i32 %conv22, i32* %r, align 4, !tbaa !32
  %78 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %79 = load i32, i32* %y, align 4, !tbaa !32
  %80 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add23 = add nsw i32 %79, %80
  %81 = load i32, i32* %d0, align 4, !tbaa !37
  %and24 = and i32 %81, 255
  %shr25 = ashr i32 %and24, 1
  %add26 = add nsw i32 %add23, %shr25
  %arrayidx27 = getelementptr inbounds i8, i8* %78, i32 %add26
  %82 = load i8, i8* %arrayidx27, align 1, !tbaa !39
  %conv28 = zext i8 %82 to i32
  store i32 %conv28, i32* %g, align 4, !tbaa !32
  %83 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %84 = load i32, i32* %y, align 4, !tbaa !32
  %85 = load i32, i32* %cblue, align 4, !tbaa !32
  %add29 = add nsw i32 %84, %85
  %86 = load i32, i32* %d0, align 4, !tbaa !37
  %and30 = and i32 %86, 255
  %add31 = add nsw i32 %add29, %and30
  %arrayidx32 = getelementptr inbounds i8, i8* %83, i32 %add31
  %87 = load i8, i8* %arrayidx32, align 1, !tbaa !39
  %conv33 = zext i8 %87 to i32
  store i32 %conv33, i32* %b, align 4, !tbaa !32
  %88 = load i32, i32* %d0, align 4, !tbaa !37
  %and34 = and i32 %88, 255
  %shl = shl i32 %and34, 24
  %89 = load i32, i32* %d0, align 4, !tbaa !37
  %shr35 = ashr i32 %89, 8
  %and36 = and i32 %shr35, 16777215
  %or = or i32 %shl, %and36
  store i32 %or, i32* %d0, align 4, !tbaa !37
  %90 = load i32, i32* %r, align 4, !tbaa !32
  %shl37 = shl i32 %90, 8
  %and38 = and i32 %shl37, 63488
  %91 = load i32, i32* %g, align 4, !tbaa !32
  %shl39 = shl i32 %91, 3
  %and40 = and i32 %shl39, 2016
  %or41 = or i32 %and38, %and40
  %92 = load i32, i32* %b, align 4, !tbaa !32
  %shr42 = lshr i32 %92, 3
  %or43 = or i32 %or41, %shr42
  store i32 %or43, i32* %rgb, align 4, !tbaa !37
  %93 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr44 = getelementptr inbounds i8, i8* %93, i32 1
  store i8* %incdec.ptr44, i8** %inptr0, align 4, !tbaa !2
  %94 = load i8, i8* %93, align 1, !tbaa !39
  %conv45 = zext i8 %94 to i32
  store i32 %conv45, i32* %y, align 4, !tbaa !32
  %95 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %96 = load i32, i32* %y, align 4, !tbaa !32
  %97 = load i32, i32* %cred, align 4, !tbaa !32
  %add46 = add nsw i32 %96, %97
  %98 = load i32, i32* %d0, align 4, !tbaa !37
  %and47 = and i32 %98, 255
  %add48 = add nsw i32 %add46, %and47
  %arrayidx49 = getelementptr inbounds i8, i8* %95, i32 %add48
  %99 = load i8, i8* %arrayidx49, align 1, !tbaa !39
  %conv50 = zext i8 %99 to i32
  store i32 %conv50, i32* %r, align 4, !tbaa !32
  %100 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %101 = load i32, i32* %y, align 4, !tbaa !32
  %102 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add51 = add nsw i32 %101, %102
  %103 = load i32, i32* %d0, align 4, !tbaa !37
  %and52 = and i32 %103, 255
  %shr53 = ashr i32 %and52, 1
  %add54 = add nsw i32 %add51, %shr53
  %arrayidx55 = getelementptr inbounds i8, i8* %100, i32 %add54
  %104 = load i8, i8* %arrayidx55, align 1, !tbaa !39
  %conv56 = zext i8 %104 to i32
  store i32 %conv56, i32* %g, align 4, !tbaa !32
  %105 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %106 = load i32, i32* %y, align 4, !tbaa !32
  %107 = load i32, i32* %cblue, align 4, !tbaa !32
  %add57 = add nsw i32 %106, %107
  %108 = load i32, i32* %d0, align 4, !tbaa !37
  %and58 = and i32 %108, 255
  %add59 = add nsw i32 %add57, %and58
  %arrayidx60 = getelementptr inbounds i8, i8* %105, i32 %add59
  %109 = load i8, i8* %arrayidx60, align 1, !tbaa !39
  %conv61 = zext i8 %109 to i32
  store i32 %conv61, i32* %b, align 4, !tbaa !32
  %110 = load i32, i32* %d0, align 4, !tbaa !37
  %and62 = and i32 %110, 255
  %shl63 = shl i32 %and62, 24
  %111 = load i32, i32* %d0, align 4, !tbaa !37
  %shr64 = ashr i32 %111, 8
  %and65 = and i32 %shr64, 16777215
  %or66 = or i32 %shl63, %and65
  store i32 %or66, i32* %d0, align 4, !tbaa !37
  %112 = load i32, i32* %r, align 4, !tbaa !32
  %shl67 = shl i32 %112, 8
  %and68 = and i32 %shl67, 63488
  %113 = load i32, i32* %g, align 4, !tbaa !32
  %shl69 = shl i32 %113, 3
  %and70 = and i32 %shl69, 2016
  %or71 = or i32 %and68, %and70
  %114 = load i32, i32* %b, align 4, !tbaa !32
  %shr72 = lshr i32 %114, 3
  %or73 = or i32 %or71, %shr72
  %shl74 = shl i32 %or73, 16
  %115 = load i32, i32* %rgb, align 4, !tbaa !37
  %or75 = or i32 %shl74, %115
  store i32 %or75, i32* %rgb, align 4, !tbaa !37
  %116 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv76 = trunc i32 %116 to i16
  %117 = load i8*, i8** %outptr, align 4, !tbaa !2
  %118 = bitcast i8* %117 to i16*
  %arrayidx77 = getelementptr inbounds i16, i16* %118, i32 0
  store i16 %conv76, i16* %arrayidx77, align 2, !tbaa !41
  %119 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr78 = ashr i32 %119, 16
  %conv79 = trunc i32 %shr78 to i16
  %120 = load i8*, i8** %outptr, align 4, !tbaa !2
  %121 = bitcast i8* %120 to i16*
  %arrayidx80 = getelementptr inbounds i16, i16* %121, i32 1
  store i16 %conv79, i16* %arrayidx80, align 2, !tbaa !41
  %122 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %122, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %123 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %123, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %124 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width81 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %124, i32 0, i32 27
  %125 = load i32, i32* %output_width81, align 8, !tbaa !19
  %and82 = and i32 %125, 1
  %tobool = icmp ne i32 %and82, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %126 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %127 = load i8, i8* %126, align 1, !tbaa !39
  %conv83 = zext i8 %127 to i32
  store i32 %conv83, i32* %cb, align 4, !tbaa !32
  %128 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %129 = load i8, i8* %128, align 1, !tbaa !39
  %conv84 = zext i8 %129 to i32
  store i32 %conv84, i32* %cr, align 4, !tbaa !32
  %130 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %131 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx85 = getelementptr inbounds i32, i32* %130, i32 %131
  %132 = load i32, i32* %arrayidx85, align 4, !tbaa !32
  store i32 %132, i32* %cred, align 4, !tbaa !32
  %133 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %134 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx86 = getelementptr inbounds i32, i32* %133, i32 %134
  %135 = load i32, i32* %arrayidx86, align 4, !tbaa !37
  %136 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %137 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx87 = getelementptr inbounds i32, i32* %136, i32 %137
  %138 = load i32, i32* %arrayidx87, align 4, !tbaa !37
  %add88 = add nsw i32 %135, %138
  %shr89 = ashr i32 %add88, 16
  store i32 %shr89, i32* %cgreen, align 4, !tbaa !32
  %139 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %140 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx90 = getelementptr inbounds i32, i32* %139, i32 %140
  %141 = load i32, i32* %arrayidx90, align 4, !tbaa !32
  store i32 %141, i32* %cblue, align 4, !tbaa !32
  %142 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %143 = load i8, i8* %142, align 1, !tbaa !39
  %conv91 = zext i8 %143 to i32
  store i32 %conv91, i32* %y, align 4, !tbaa !32
  %144 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %145 = load i32, i32* %y, align 4, !tbaa !32
  %146 = load i32, i32* %cred, align 4, !tbaa !32
  %add92 = add nsw i32 %145, %146
  %147 = load i32, i32* %d0, align 4, !tbaa !37
  %and93 = and i32 %147, 255
  %add94 = add nsw i32 %add92, %and93
  %arrayidx95 = getelementptr inbounds i8, i8* %144, i32 %add94
  %148 = load i8, i8* %arrayidx95, align 1, !tbaa !39
  %conv96 = zext i8 %148 to i32
  store i32 %conv96, i32* %r, align 4, !tbaa !32
  %149 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %150 = load i32, i32* %y, align 4, !tbaa !32
  %151 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add97 = add nsw i32 %150, %151
  %152 = load i32, i32* %d0, align 4, !tbaa !37
  %and98 = and i32 %152, 255
  %shr99 = ashr i32 %and98, 1
  %add100 = add nsw i32 %add97, %shr99
  %arrayidx101 = getelementptr inbounds i8, i8* %149, i32 %add100
  %153 = load i8, i8* %arrayidx101, align 1, !tbaa !39
  %conv102 = zext i8 %153 to i32
  store i32 %conv102, i32* %g, align 4, !tbaa !32
  %154 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %155 = load i32, i32* %y, align 4, !tbaa !32
  %156 = load i32, i32* %cblue, align 4, !tbaa !32
  %add103 = add nsw i32 %155, %156
  %157 = load i32, i32* %d0, align 4, !tbaa !37
  %and104 = and i32 %157, 255
  %add105 = add nsw i32 %add103, %and104
  %arrayidx106 = getelementptr inbounds i8, i8* %154, i32 %add105
  %158 = load i8, i8* %arrayidx106, align 1, !tbaa !39
  %conv107 = zext i8 %158 to i32
  store i32 %conv107, i32* %b, align 4, !tbaa !32
  %159 = load i32, i32* %r, align 4, !tbaa !32
  %shl108 = shl i32 %159, 8
  %and109 = and i32 %shl108, 63488
  %160 = load i32, i32* %g, align 4, !tbaa !32
  %shl110 = shl i32 %160, 3
  %and111 = and i32 %shl110, 2016
  %or112 = or i32 %and109, %and111
  %161 = load i32, i32* %b, align 4, !tbaa !32
  %shr113 = lshr i32 %161, 3
  %or114 = or i32 %or112, %shr113
  store i32 %or114, i32* %rgb, align 4, !tbaa !37
  %162 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv115 = trunc i32 %162 to i16
  %163 = load i8*, i8** %outptr, align 4, !tbaa !2
  %164 = bitcast i8* %163 to i16*
  store i16 %conv115, i16* %164, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %165 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #4
  %166 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #4
  %167 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  %168 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  %169 = bitcast i32* %d0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  %170 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #4
  %171 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #4
  %172 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #4
  %173 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #4
  %174 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #4
  %175 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #4
  %176 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #4
  %177 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #4
  %178 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #4
  %179 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #4
  %180 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #4
  %181 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #4
  %182 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #4
  %183 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #4
  %184 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #4
  %185 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #4
  %186 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v1_merged_upsample_565_be(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %rgb = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %34, i32 0
  %35 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %37, i8** %inptr0, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %38, i32 1
  %39 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %41, i8** %inptr1, align 4, !tbaa !2
  %42 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %42, i32 2
  %43 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %44 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %43, i32 %44
  %45 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %45, i8** %inptr2, align 4, !tbaa !2
  %46 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %46, i32 0
  %47 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %47, i8** %outptr, align 4, !tbaa !2
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 27
  %49 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %49, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %50 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %50, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !39
  %conv = zext i8 %52 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %53 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %53, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %54 = load i8, i8* %53, align 1, !tbaa !39
  %conv9 = zext i8 %54 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %55 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %56 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %55, i32 %56
  %57 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %57, i32* %cred, align 4, !tbaa !32
  %58 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %59 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %58, i32 %59
  %60 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %61 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %62 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %61, i32 %62
  %63 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %60, %63
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %64 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %65 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %64, i32 %65
  %66 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %66, i32* %cblue, align 4, !tbaa !32
  %67 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %67, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %68 = load i8, i8* %67, align 1, !tbaa !39
  %conv16 = zext i8 %68 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %69 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %70 = load i32, i32* %y, align 4, !tbaa !32
  %71 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %70, %71
  %arrayidx18 = getelementptr inbounds i8, i8* %69, i32 %add17
  %72 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %conv19 = zext i8 %72 to i32
  store i32 %conv19, i32* %r, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %74, %75
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 %add20
  %76 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %conv22 = zext i8 %76 to i32
  store i32 %conv22, i32* %g, align 4, !tbaa !32
  %77 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %78 = load i32, i32* %y, align 4, !tbaa !32
  %79 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %78, %79
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 %add23
  %80 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %conv25 = zext i8 %80 to i32
  store i32 %conv25, i32* %b, align 4, !tbaa !32
  %81 = load i32, i32* %r, align 4, !tbaa !32
  %and = and i32 %81, 248
  %82 = load i32, i32* %g, align 4, !tbaa !32
  %shr26 = lshr i32 %82, 5
  %or = or i32 %and, %shr26
  %83 = load i32, i32* %g, align 4, !tbaa !32
  %shl = shl i32 %83, 11
  %and27 = and i32 %shl, 57344
  %or28 = or i32 %or, %and27
  %84 = load i32, i32* %b, align 4, !tbaa !32
  %shl29 = shl i32 %84, 5
  %and30 = and i32 %shl29, 7936
  %or31 = or i32 %or28, %and30
  store i32 %or31, i32* %rgb, align 4, !tbaa !37
  %85 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds i8, i8* %85, i32 1
  store i8* %incdec.ptr32, i8** %inptr0, align 4, !tbaa !2
  %86 = load i8, i8* %85, align 1, !tbaa !39
  %conv33 = zext i8 %86 to i32
  store i32 %conv33, i32* %y, align 4, !tbaa !32
  %87 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %88 = load i32, i32* %y, align 4, !tbaa !32
  %89 = load i32, i32* %cred, align 4, !tbaa !32
  %add34 = add nsw i32 %88, %89
  %arrayidx35 = getelementptr inbounds i8, i8* %87, i32 %add34
  %90 = load i8, i8* %arrayidx35, align 1, !tbaa !39
  %conv36 = zext i8 %90 to i32
  store i32 %conv36, i32* %r, align 4, !tbaa !32
  %91 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %92 = load i32, i32* %y, align 4, !tbaa !32
  %93 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add37 = add nsw i32 %92, %93
  %arrayidx38 = getelementptr inbounds i8, i8* %91, i32 %add37
  %94 = load i8, i8* %arrayidx38, align 1, !tbaa !39
  %conv39 = zext i8 %94 to i32
  store i32 %conv39, i32* %g, align 4, !tbaa !32
  %95 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %96 = load i32, i32* %y, align 4, !tbaa !32
  %97 = load i32, i32* %cblue, align 4, !tbaa !32
  %add40 = add nsw i32 %96, %97
  %arrayidx41 = getelementptr inbounds i8, i8* %95, i32 %add40
  %98 = load i8, i8* %arrayidx41, align 1, !tbaa !39
  %conv42 = zext i8 %98 to i32
  store i32 %conv42, i32* %b, align 4, !tbaa !32
  %99 = load i32, i32* %rgb, align 4, !tbaa !37
  %shl43 = shl i32 %99, 16
  %100 = load i32, i32* %r, align 4, !tbaa !32
  %and44 = and i32 %100, 248
  %101 = load i32, i32* %g, align 4, !tbaa !32
  %shr45 = lshr i32 %101, 5
  %or46 = or i32 %and44, %shr45
  %102 = load i32, i32* %g, align 4, !tbaa !32
  %shl47 = shl i32 %102, 11
  %and48 = and i32 %shl47, 57344
  %or49 = or i32 %or46, %and48
  %103 = load i32, i32* %b, align 4, !tbaa !32
  %shl50 = shl i32 %103, 5
  %and51 = and i32 %shl50, 7936
  %or52 = or i32 %or49, %and51
  %or53 = or i32 %shl43, %or52
  store i32 %or53, i32* %rgb, align 4, !tbaa !37
  %104 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv54 = trunc i32 %104 to i16
  %105 = load i8*, i8** %outptr, align 4, !tbaa !2
  %106 = bitcast i8* %105 to i16*
  %arrayidx55 = getelementptr inbounds i16, i16* %106, i32 1
  store i16 %conv54, i16* %arrayidx55, align 2, !tbaa !41
  %107 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr56 = ashr i32 %107, 16
  %conv57 = trunc i32 %shr56 to i16
  %108 = load i8*, i8** %outptr, align 4, !tbaa !2
  %109 = bitcast i8* %108 to i16*
  %arrayidx58 = getelementptr inbounds i16, i16* %109, i32 0
  store i16 %conv57, i16* %arrayidx58, align 2, !tbaa !41
  %110 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %110, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %111 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %111, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %112 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width59 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %112, i32 0, i32 27
  %113 = load i32, i32* %output_width59, align 8, !tbaa !19
  %and60 = and i32 %113, 1
  %tobool = icmp ne i32 %and60, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %114 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %115 = load i8, i8* %114, align 1, !tbaa !39
  %conv61 = zext i8 %115 to i32
  store i32 %conv61, i32* %cb, align 4, !tbaa !32
  %116 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %117 = load i8, i8* %116, align 1, !tbaa !39
  %conv62 = zext i8 %117 to i32
  store i32 %conv62, i32* %cr, align 4, !tbaa !32
  %118 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %119 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx63 = getelementptr inbounds i32, i32* %118, i32 %119
  %120 = load i32, i32* %arrayidx63, align 4, !tbaa !32
  store i32 %120, i32* %cred, align 4, !tbaa !32
  %121 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %122 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx64 = getelementptr inbounds i32, i32* %121, i32 %122
  %123 = load i32, i32* %arrayidx64, align 4, !tbaa !37
  %124 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %125 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx65 = getelementptr inbounds i32, i32* %124, i32 %125
  %126 = load i32, i32* %arrayidx65, align 4, !tbaa !37
  %add66 = add nsw i32 %123, %126
  %shr67 = ashr i32 %add66, 16
  store i32 %shr67, i32* %cgreen, align 4, !tbaa !32
  %127 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %128 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx68 = getelementptr inbounds i32, i32* %127, i32 %128
  %129 = load i32, i32* %arrayidx68, align 4, !tbaa !32
  store i32 %129, i32* %cblue, align 4, !tbaa !32
  %130 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %131 = load i8, i8* %130, align 1, !tbaa !39
  %conv69 = zext i8 %131 to i32
  store i32 %conv69, i32* %y, align 4, !tbaa !32
  %132 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %133 = load i32, i32* %y, align 4, !tbaa !32
  %134 = load i32, i32* %cred, align 4, !tbaa !32
  %add70 = add nsw i32 %133, %134
  %arrayidx71 = getelementptr inbounds i8, i8* %132, i32 %add70
  %135 = load i8, i8* %arrayidx71, align 1, !tbaa !39
  %conv72 = zext i8 %135 to i32
  store i32 %conv72, i32* %r, align 4, !tbaa !32
  %136 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %137 = load i32, i32* %y, align 4, !tbaa !32
  %138 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add73 = add nsw i32 %137, %138
  %arrayidx74 = getelementptr inbounds i8, i8* %136, i32 %add73
  %139 = load i8, i8* %arrayidx74, align 1, !tbaa !39
  %conv75 = zext i8 %139 to i32
  store i32 %conv75, i32* %g, align 4, !tbaa !32
  %140 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %141 = load i32, i32* %y, align 4, !tbaa !32
  %142 = load i32, i32* %cblue, align 4, !tbaa !32
  %add76 = add nsw i32 %141, %142
  %arrayidx77 = getelementptr inbounds i8, i8* %140, i32 %add76
  %143 = load i8, i8* %arrayidx77, align 1, !tbaa !39
  %conv78 = zext i8 %143 to i32
  store i32 %conv78, i32* %b, align 4, !tbaa !32
  %144 = load i32, i32* %r, align 4, !tbaa !32
  %and79 = and i32 %144, 248
  %145 = load i32, i32* %g, align 4, !tbaa !32
  %shr80 = lshr i32 %145, 5
  %or81 = or i32 %and79, %shr80
  %146 = load i32, i32* %g, align 4, !tbaa !32
  %shl82 = shl i32 %146, 11
  %and83 = and i32 %shl82, 57344
  %or84 = or i32 %or81, %and83
  %147 = load i32, i32* %b, align 4, !tbaa !32
  %shl85 = shl i32 %147, 5
  %and86 = and i32 %shl85, 7936
  %or87 = or i32 %or84, %and86
  store i32 %or87, i32* %rgb, align 4, !tbaa !37
  %148 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv88 = trunc i32 %148 to i16
  %149 = load i8*, i8** %outptr, align 4, !tbaa !2
  %150 = bitcast i8* %149 to i16*
  store i16 %conv88, i16* %150, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %151 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  %152 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %153 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  %154 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  %155 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #4
  %156 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #4
  %157 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %158 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  %159 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #4
  %160 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #4
  %161 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #4
  %162 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #4
  %163 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #4
  %164 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #4
  %165 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #4
  %166 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #4
  %167 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  %168 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  %169 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #4
  %170 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #4
  %171 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #4
  ret void
}

; Function Attrs: alwaysinline nounwind
define internal void @h2v1_merged_upsample_565_le(%struct.jpeg_decompress_struct* %cinfo, i8*** %input_buf, i32 %in_row_group_ctr, i8** %output_buf) #3 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8***, align 4
  %in_row_group_ctr.addr = alloca i32, align 4
  %output_buf.addr = alloca i8**, align 4
  %upsample = alloca %struct.my_upsampler*, align 4
  %y = alloca i32, align 4
  %cred = alloca i32, align 4
  %cgreen = alloca i32, align 4
  %cblue = alloca i32, align 4
  %cb = alloca i32, align 4
  %cr = alloca i32, align 4
  %outptr = alloca i8*, align 4
  %inptr0 = alloca i8*, align 4
  %inptr1 = alloca i8*, align 4
  %inptr2 = alloca i8*, align 4
  %col = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  %Crrtab = alloca i32*, align 4
  %Cbbtab = alloca i32*, align 4
  %Crgtab = alloca i32*, align 4
  %Cbgtab = alloca i32*, align 4
  %r = alloca i32, align 4
  %g = alloca i32, align 4
  %b = alloca i32, align 4
  %rgb = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %input_buf, i8**** %input_buf.addr, align 4, !tbaa !2
  store i32 %in_row_group_ctr, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %upsample1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 85
  %2 = load %struct.jpeg_upsampler*, %struct.jpeg_upsampler** %upsample1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_upsampler* %2 to %struct.my_upsampler*
  store %struct.my_upsampler* %3, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %4 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  %8 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  %9 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  %10 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #4
  %11 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #4
  %12 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #4
  %13 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #4
  %14 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #4
  %15 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #4
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 65
  %17 = load i8*, i8** %sample_range_limit, align 4, !tbaa !38
  store i8* %17, i8** %range_limit, align 4, !tbaa !2
  %18 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #4
  %19 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_r_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %19, i32 0, i32 2
  %20 = load i32*, i32** %Cr_r_tab, align 4, !tbaa !33
  store i32* %20, i32** %Crrtab, align 4, !tbaa !2
  %21 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #4
  %22 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_b_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %22, i32 0, i32 3
  %23 = load i32*, i32** %Cb_b_tab, align 4, !tbaa !34
  store i32* %23, i32** %Cbbtab, align 4, !tbaa !2
  %24 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #4
  %25 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cr_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %25, i32 0, i32 4
  %26 = load i32*, i32** %Cr_g_tab, align 4, !tbaa !35
  store i32* %26, i32** %Crgtab, align 4, !tbaa !2
  %27 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #4
  %28 = load %struct.my_upsampler*, %struct.my_upsampler** %upsample, align 4, !tbaa !2
  %Cb_g_tab = getelementptr inbounds %struct.my_upsampler, %struct.my_upsampler* %28, i32 0, i32 5
  %29 = load i32*, i32** %Cb_g_tab, align 4, !tbaa !36
  store i32* %29, i32** %Cbgtab, align 4, !tbaa !2
  %30 = bitcast i32* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #4
  %31 = bitcast i32* %g to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #4
  %32 = bitcast i32* %b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #4
  %33 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #4
  %34 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8**, i8*** %34, i32 0
  %35 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %36 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i8*, i8** %35, i32 %36
  %37 = load i8*, i8** %arrayidx2, align 4, !tbaa !2
  store i8* %37, i8** %inptr0, align 4, !tbaa !2
  %38 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8**, i8*** %38, i32 1
  %39 = load i8**, i8*** %arrayidx3, align 4, !tbaa !2
  %40 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx4 = getelementptr inbounds i8*, i8** %39, i32 %40
  %41 = load i8*, i8** %arrayidx4, align 4, !tbaa !2
  store i8* %41, i8** %inptr1, align 4, !tbaa !2
  %42 = load i8***, i8**** %input_buf.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8**, i8*** %42, i32 2
  %43 = load i8**, i8*** %arrayidx5, align 4, !tbaa !2
  %44 = load i32, i32* %in_row_group_ctr.addr, align 4, !tbaa !32
  %arrayidx6 = getelementptr inbounds i8*, i8** %43, i32 %44
  %45 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %45, i8** %inptr2, align 4, !tbaa !2
  %46 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8*, i8** %46, i32 0
  %47 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %47, i8** %outptr, align 4, !tbaa !2
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 27
  %49 = load i32, i32* %output_width, align 8, !tbaa !19
  %shr = lshr i32 %49, 1
  store i32 %shr, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %50 = load i32, i32* %col, align 4, !tbaa !32
  %cmp = icmp ugt i32 %50, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %51 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr, i8** %inptr1, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !39
  %conv = zext i8 %52 to i32
  store i32 %conv, i32* %cb, align 4, !tbaa !32
  %53 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %incdec.ptr8 = getelementptr inbounds i8, i8* %53, i32 1
  store i8* %incdec.ptr8, i8** %inptr2, align 4, !tbaa !2
  %54 = load i8, i8* %53, align 1, !tbaa !39
  %conv9 = zext i8 %54 to i32
  store i32 %conv9, i32* %cr, align 4, !tbaa !32
  %55 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %56 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx10 = getelementptr inbounds i32, i32* %55, i32 %56
  %57 = load i32, i32* %arrayidx10, align 4, !tbaa !32
  store i32 %57, i32* %cred, align 4, !tbaa !32
  %58 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %59 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx11 = getelementptr inbounds i32, i32* %58, i32 %59
  %60 = load i32, i32* %arrayidx11, align 4, !tbaa !37
  %61 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %62 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %61, i32 %62
  %63 = load i32, i32* %arrayidx12, align 4, !tbaa !37
  %add = add nsw i32 %60, %63
  %shr13 = ashr i32 %add, 16
  store i32 %shr13, i32* %cgreen, align 4, !tbaa !32
  %64 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %65 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx14 = getelementptr inbounds i32, i32* %64, i32 %65
  %66 = load i32, i32* %arrayidx14, align 4, !tbaa !32
  store i32 %66, i32* %cblue, align 4, !tbaa !32
  %67 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr15 = getelementptr inbounds i8, i8* %67, i32 1
  store i8* %incdec.ptr15, i8** %inptr0, align 4, !tbaa !2
  %68 = load i8, i8* %67, align 1, !tbaa !39
  %conv16 = zext i8 %68 to i32
  store i32 %conv16, i32* %y, align 4, !tbaa !32
  %69 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %70 = load i32, i32* %y, align 4, !tbaa !32
  %71 = load i32, i32* %cred, align 4, !tbaa !32
  %add17 = add nsw i32 %70, %71
  %arrayidx18 = getelementptr inbounds i8, i8* %69, i32 %add17
  %72 = load i8, i8* %arrayidx18, align 1, !tbaa !39
  %conv19 = zext i8 %72 to i32
  store i32 %conv19, i32* %r, align 4, !tbaa !32
  %73 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %74 = load i32, i32* %y, align 4, !tbaa !32
  %75 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add20 = add nsw i32 %74, %75
  %arrayidx21 = getelementptr inbounds i8, i8* %73, i32 %add20
  %76 = load i8, i8* %arrayidx21, align 1, !tbaa !39
  %conv22 = zext i8 %76 to i32
  store i32 %conv22, i32* %g, align 4, !tbaa !32
  %77 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %78 = load i32, i32* %y, align 4, !tbaa !32
  %79 = load i32, i32* %cblue, align 4, !tbaa !32
  %add23 = add nsw i32 %78, %79
  %arrayidx24 = getelementptr inbounds i8, i8* %77, i32 %add23
  %80 = load i8, i8* %arrayidx24, align 1, !tbaa !39
  %conv25 = zext i8 %80 to i32
  store i32 %conv25, i32* %b, align 4, !tbaa !32
  %81 = load i32, i32* %r, align 4, !tbaa !32
  %shl = shl i32 %81, 8
  %and = and i32 %shl, 63488
  %82 = load i32, i32* %g, align 4, !tbaa !32
  %shl26 = shl i32 %82, 3
  %and27 = and i32 %shl26, 2016
  %or = or i32 %and, %and27
  %83 = load i32, i32* %b, align 4, !tbaa !32
  %shr28 = lshr i32 %83, 3
  %or29 = or i32 %or, %shr28
  store i32 %or29, i32* %rgb, align 4, !tbaa !37
  %84 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %incdec.ptr30 = getelementptr inbounds i8, i8* %84, i32 1
  store i8* %incdec.ptr30, i8** %inptr0, align 4, !tbaa !2
  %85 = load i8, i8* %84, align 1, !tbaa !39
  %conv31 = zext i8 %85 to i32
  store i32 %conv31, i32* %y, align 4, !tbaa !32
  %86 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %87 = load i32, i32* %y, align 4, !tbaa !32
  %88 = load i32, i32* %cred, align 4, !tbaa !32
  %add32 = add nsw i32 %87, %88
  %arrayidx33 = getelementptr inbounds i8, i8* %86, i32 %add32
  %89 = load i8, i8* %arrayidx33, align 1, !tbaa !39
  %conv34 = zext i8 %89 to i32
  store i32 %conv34, i32* %r, align 4, !tbaa !32
  %90 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %91 = load i32, i32* %y, align 4, !tbaa !32
  %92 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add35 = add nsw i32 %91, %92
  %arrayidx36 = getelementptr inbounds i8, i8* %90, i32 %add35
  %93 = load i8, i8* %arrayidx36, align 1, !tbaa !39
  %conv37 = zext i8 %93 to i32
  store i32 %conv37, i32* %g, align 4, !tbaa !32
  %94 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %95 = load i32, i32* %y, align 4, !tbaa !32
  %96 = load i32, i32* %cblue, align 4, !tbaa !32
  %add38 = add nsw i32 %95, %96
  %arrayidx39 = getelementptr inbounds i8, i8* %94, i32 %add38
  %97 = load i8, i8* %arrayidx39, align 1, !tbaa !39
  %conv40 = zext i8 %97 to i32
  store i32 %conv40, i32* %b, align 4, !tbaa !32
  %98 = load i32, i32* %r, align 4, !tbaa !32
  %shl41 = shl i32 %98, 8
  %and42 = and i32 %shl41, 63488
  %99 = load i32, i32* %g, align 4, !tbaa !32
  %shl43 = shl i32 %99, 3
  %and44 = and i32 %shl43, 2016
  %or45 = or i32 %and42, %and44
  %100 = load i32, i32* %b, align 4, !tbaa !32
  %shr46 = lshr i32 %100, 3
  %or47 = or i32 %or45, %shr46
  %shl48 = shl i32 %or47, 16
  %101 = load i32, i32* %rgb, align 4, !tbaa !37
  %or49 = or i32 %shl48, %101
  store i32 %or49, i32* %rgb, align 4, !tbaa !37
  %102 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv50 = trunc i32 %102 to i16
  %103 = load i8*, i8** %outptr, align 4, !tbaa !2
  %104 = bitcast i8* %103 to i16*
  %arrayidx51 = getelementptr inbounds i16, i16* %104, i32 0
  store i16 %conv50, i16* %arrayidx51, align 2, !tbaa !41
  %105 = load i32, i32* %rgb, align 4, !tbaa !37
  %shr52 = ashr i32 %105, 16
  %conv53 = trunc i32 %shr52 to i16
  %106 = load i8*, i8** %outptr, align 4, !tbaa !2
  %107 = bitcast i8* %106 to i16*
  %arrayidx54 = getelementptr inbounds i16, i16* %107, i32 1
  store i16 %conv53, i16* %arrayidx54, align 2, !tbaa !41
  %108 = load i8*, i8** %outptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %108, i32 4
  store i8* %add.ptr, i8** %outptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %109 = load i32, i32* %col, align 4, !tbaa !32
  %dec = add i32 %109, -1
  store i32 %dec, i32* %col, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %110 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width55 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %110, i32 0, i32 27
  %111 = load i32, i32* %output_width55, align 8, !tbaa !19
  %and56 = and i32 %111, 1
  %tobool = icmp ne i32 %and56, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %112 = load i8*, i8** %inptr1, align 4, !tbaa !2
  %113 = load i8, i8* %112, align 1, !tbaa !39
  %conv57 = zext i8 %113 to i32
  store i32 %conv57, i32* %cb, align 4, !tbaa !32
  %114 = load i8*, i8** %inptr2, align 4, !tbaa !2
  %115 = load i8, i8* %114, align 1, !tbaa !39
  %conv58 = zext i8 %115 to i32
  store i32 %conv58, i32* %cr, align 4, !tbaa !32
  %116 = load i32*, i32** %Crrtab, align 4, !tbaa !2
  %117 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx59 = getelementptr inbounds i32, i32* %116, i32 %117
  %118 = load i32, i32* %arrayidx59, align 4, !tbaa !32
  store i32 %118, i32* %cred, align 4, !tbaa !32
  %119 = load i32*, i32** %Cbgtab, align 4, !tbaa !2
  %120 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx60 = getelementptr inbounds i32, i32* %119, i32 %120
  %121 = load i32, i32* %arrayidx60, align 4, !tbaa !37
  %122 = load i32*, i32** %Crgtab, align 4, !tbaa !2
  %123 = load i32, i32* %cr, align 4, !tbaa !32
  %arrayidx61 = getelementptr inbounds i32, i32* %122, i32 %123
  %124 = load i32, i32* %arrayidx61, align 4, !tbaa !37
  %add62 = add nsw i32 %121, %124
  %shr63 = ashr i32 %add62, 16
  store i32 %shr63, i32* %cgreen, align 4, !tbaa !32
  %125 = load i32*, i32** %Cbbtab, align 4, !tbaa !2
  %126 = load i32, i32* %cb, align 4, !tbaa !32
  %arrayidx64 = getelementptr inbounds i32, i32* %125, i32 %126
  %127 = load i32, i32* %arrayidx64, align 4, !tbaa !32
  store i32 %127, i32* %cblue, align 4, !tbaa !32
  %128 = load i8*, i8** %inptr0, align 4, !tbaa !2
  %129 = load i8, i8* %128, align 1, !tbaa !39
  %conv65 = zext i8 %129 to i32
  store i32 %conv65, i32* %y, align 4, !tbaa !32
  %130 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %131 = load i32, i32* %y, align 4, !tbaa !32
  %132 = load i32, i32* %cred, align 4, !tbaa !32
  %add66 = add nsw i32 %131, %132
  %arrayidx67 = getelementptr inbounds i8, i8* %130, i32 %add66
  %133 = load i8, i8* %arrayidx67, align 1, !tbaa !39
  %conv68 = zext i8 %133 to i32
  store i32 %conv68, i32* %r, align 4, !tbaa !32
  %134 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %135 = load i32, i32* %y, align 4, !tbaa !32
  %136 = load i32, i32* %cgreen, align 4, !tbaa !32
  %add69 = add nsw i32 %135, %136
  %arrayidx70 = getelementptr inbounds i8, i8* %134, i32 %add69
  %137 = load i8, i8* %arrayidx70, align 1, !tbaa !39
  %conv71 = zext i8 %137 to i32
  store i32 %conv71, i32* %g, align 4, !tbaa !32
  %138 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %139 = load i32, i32* %y, align 4, !tbaa !32
  %140 = load i32, i32* %cblue, align 4, !tbaa !32
  %add72 = add nsw i32 %139, %140
  %arrayidx73 = getelementptr inbounds i8, i8* %138, i32 %add72
  %141 = load i8, i8* %arrayidx73, align 1, !tbaa !39
  %conv74 = zext i8 %141 to i32
  store i32 %conv74, i32* %b, align 4, !tbaa !32
  %142 = load i32, i32* %r, align 4, !tbaa !32
  %shl75 = shl i32 %142, 8
  %and76 = and i32 %shl75, 63488
  %143 = load i32, i32* %g, align 4, !tbaa !32
  %shl77 = shl i32 %143, 3
  %and78 = and i32 %shl77, 2016
  %or79 = or i32 %and76, %and78
  %144 = load i32, i32* %b, align 4, !tbaa !32
  %shr80 = lshr i32 %144, 3
  %or81 = or i32 %or79, %shr80
  store i32 %or81, i32* %rgb, align 4, !tbaa !37
  %145 = load i32, i32* %rgb, align 4, !tbaa !37
  %conv82 = trunc i32 %145 to i16
  %146 = load i8*, i8** %outptr, align 4, !tbaa !2
  %147 = bitcast i8* %146 to i16*
  store i16 %conv82, i16* %147, align 2, !tbaa !41
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %148 = bitcast i32* %rgb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #4
  %149 = bitcast i32* %b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #4
  %150 = bitcast i32* %g to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #4
  %151 = bitcast i32* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #4
  %152 = bitcast i32** %Cbgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #4
  %153 = bitcast i32** %Crgtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #4
  %154 = bitcast i32** %Cbbtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #4
  %155 = bitcast i32** %Crrtab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #4
  %156 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #4
  %157 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #4
  %158 = bitcast i8** %inptr2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #4
  %159 = bitcast i8** %inptr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #4
  %160 = bitcast i8** %inptr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #4
  %161 = bitcast i8** %outptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #4
  %162 = bitcast i32* %cr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #4
  %163 = bitcast i32* %cb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #4
  %164 = bitcast i32* %cblue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #4
  %165 = bitcast i32* %cgreen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #4
  %166 = bitcast i32* %cred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #4
  %167 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #4
  %168 = bitcast %struct.my_upsampler** %upsample to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #4
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { alwaysinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 452}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !8, i64 36, !8, i64 40, !8, i64 44}
!17 = !{!"jpeg_upsampler", !3, i64 0, !3, i64 4, !8, i64 8}
!18 = !{!16, !8, i64 8}
!19 = !{!7, !8, i64 112}
!20 = !{!7, !8, i64 120}
!21 = !{!16, !8, i64 40}
!22 = !{!7, !8, i64 312}
!23 = !{!16, !3, i64 4}
!24 = !{!16, !3, i64 12}
!25 = !{!7, !4, i64 44}
!26 = !{!7, !4, i64 88}
!27 = !{!12, !3, i64 4}
!28 = !{!16, !3, i64 32}
!29 = !{!16, !8, i64 36}
!30 = !{!7, !8, i64 116}
!31 = !{!16, !8, i64 44}
!32 = !{!8, !8, i64 0}
!33 = !{!16, !3, i64 16}
!34 = !{!16, !3, i64 20}
!35 = !{!16, !3, i64 24}
!36 = !{!16, !3, i64 28}
!37 = !{!13, !13, i64 0}
!38 = !{!7, !3, i64 324}
!39 = !{!4, !4, i64 0}
!40 = !{!7, !8, i64 140}
!41 = !{!10, !10, i64 0}
