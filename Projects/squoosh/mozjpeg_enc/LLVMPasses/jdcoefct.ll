; ModuleID = 'jdcoefct.c'
source_filename = "jdcoefct.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*)* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)*, i32, i32 }
%struct.jpeg_marker_reader = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { void (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { void (%struct.jpeg_decompress_struct*)*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { {}*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)* }
%struct.my_coef_controller = type { %struct.jpeg_d_coef_controller, i32, i32, i32, [10 x [64 x i16]*], i16*, [10 x %struct.jvirt_barray_control*], i32* }

; Function Attrs: nounwind
define hidden void @jinit_d_coef_controller(%struct.jpeg_decompress_struct* %cinfo, i32 %need_full_buffer) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %need_full_buffer.addr = alloca i32, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %ci = alloca i32, align 4
  %access_rows = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %buffer = alloca [64 x i16]*, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %need_full_buffer, i32* %need_full_buffer.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %2, i32 0, i32 0
  %3 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_decompress_struct* %4 to %struct.jpeg_common_struct*
  %call = call i8* %3(%struct.jpeg_common_struct* %5, i32 1, i32 120)
  %6 = bitcast i8* %call to %struct.my_coef_controller*
  store %struct.my_coef_controller* %6, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %7 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %8 = bitcast %struct.my_coef_controller* %7 to %struct.jpeg_d_coef_controller*
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 79
  store %struct.jpeg_d_coef_controller* %8, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !15
  %10 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %10, i32 0, i32 0
  %start_input_pass = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub, i32 0, i32 0
  store void (%struct.jpeg_decompress_struct*)* @start_input_pass, void (%struct.jpeg_decompress_struct*)** %start_input_pass, align 4, !tbaa !16
  %11 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %11, i32 0, i32 0
  %start_output_pass = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub2, i32 0, i32 2
  store void (%struct.jpeg_decompress_struct*)* @start_output_pass, void (%struct.jpeg_decompress_struct*)** %start_output_pass, align 4, !tbaa !19
  %12 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %coef_bits_latch = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %12, i32 0, i32 7
  store i32* null, i32** %coef_bits_latch, align 4, !tbaa !20
  %13 = load i32, i32* %need_full_buffer.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %14 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %access_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  store i32 0, i32* %ci, align 4, !tbaa !6
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 44
  %18 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !21
  store %struct.jpeg_component_info* %18, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %19 = load i32, i32* %ci, align 4, !tbaa !6
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 9
  %21 = load i32, i32* %num_components, align 4, !tbaa !22
  %cmp = icmp slt i32 %19, %21
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %22 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %22, i32 0, i32 3
  %23 = load i32, i32* %v_samp_factor, align 4, !tbaa !23
  store i32 %23, i32* %access_rows, align 4, !tbaa !6
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 45
  %25 = load i32, i32* %progressive_mode, align 4, !tbaa !25
  %tobool3 = icmp ne i32 %25, 0
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %for.body
  %26 = load i32, i32* %access_rows, align 4, !tbaa !6
  %mul = mul nsw i32 %26, 3
  store i32 %mul, i32* %access_rows, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then4, %for.body
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 1
  %28 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem5, align 4, !tbaa !8
  %request_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %28, i32 0, i32 5
  %29 = load %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)** %request_virt_barray, align 4, !tbaa !26
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = bitcast %struct.jpeg_decompress_struct* %30 to %struct.jpeg_common_struct*
  %32 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %32, i32 0, i32 7
  %33 = load i32, i32* %width_in_blocks, align 4, !tbaa !27
  %34 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %34, i32 0, i32 2
  %35 = load i32, i32* %h_samp_factor, align 4, !tbaa !28
  %call6 = call i32 @jround_up(i32 %33, i32 %35)
  %36 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %36, i32 0, i32 8
  %37 = load i32, i32* %height_in_blocks, align 4, !tbaa !29
  %38 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor7 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %38, i32 0, i32 3
  %39 = load i32, i32* %v_samp_factor7, align 4, !tbaa !23
  %call8 = call i32 @jround_up(i32 %37, i32 %39)
  %40 = load i32, i32* %access_rows, align 4, !tbaa !6
  %call9 = call %struct.jvirt_barray_control* %29(%struct.jpeg_common_struct* %31, i32 1, i32 1, i32 %call6, i32 %call8, i32 %40)
  %41 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %41, i32 0, i32 6
  %42 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 %42
  store %struct.jvirt_barray_control* %call9, %struct.jvirt_barray_control** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %43 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %44 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %44, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %45 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub10 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %45, i32 0, i32 0
  %consume_data = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub10, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*)* @consume_data, i32 (%struct.jpeg_decompress_struct*)** %consume_data, align 4, !tbaa !30
  %46 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub11 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %46, i32 0, i32 0
  %decompress_data = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub11, i32 0, i32 3
  store i32 (%struct.jpeg_decompress_struct*, i8***)* @decompress_data, i32 (%struct.jpeg_decompress_struct*, i8***)** %decompress_data, align 4, !tbaa !31
  %47 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image12 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %47, i32 0, i32 6
  %arraydecay = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image12, i32 0, i32 0
  %48 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub13 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %48, i32 0, i32 0
  %coef_arrays = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub13, i32 0, i32 4
  store %struct.jvirt_barray_control** %arraydecay, %struct.jvirt_barray_control*** %coef_arrays, align 4, !tbaa !32
  %49 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %50 = bitcast i32* %access_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  br label %if.end29

if.else:                                          ; preds = %entry
  %52 = bitcast [64 x i16]** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #3
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 1
  %55 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem14, align 4, !tbaa !8
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %55, i32 0, i32 1
  %56 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !33
  %57 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %58 = bitcast %struct.jpeg_decompress_struct* %57 to %struct.jpeg_common_struct*
  %call15 = call i8* %56(%struct.jpeg_common_struct* %58, i32 1, i32 1280)
  %59 = bitcast i8* %call15 to [64 x i16]*
  store [64 x i16]* %59, [64 x i16]** %buffer, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc20, %if.else
  %60 = load i32, i32* %i, align 4, !tbaa !6
  %cmp17 = icmp slt i32 %60, 10
  br i1 %cmp17, label %for.body18, label %for.end22

for.body18:                                       ; preds = %for.cond16
  %61 = load [64 x i16]*, [64 x i16]** %buffer, align 4, !tbaa !2
  %62 = load i32, i32* %i, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %61, i32 %62
  %63 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %63, i32 0, i32 4
  %64 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx19 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %64
  store [64 x i16]* %add.ptr, [64 x i16]** %arrayidx19, align 4, !tbaa !2
  br label %for.inc20

for.inc20:                                        ; preds = %for.body18
  %65 = load i32, i32* %i, align 4, !tbaa !6
  %inc21 = add nsw i32 %65, 1
  store i32 %inc21, i32* %i, align 4, !tbaa !6
  br label %for.cond16

for.end22:                                        ; preds = %for.cond16
  %66 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub23 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %66, i32 0, i32 0
  %consume_data24 = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub23, i32 0, i32 1
  store i32 (%struct.jpeg_decompress_struct*)* @dummy_consume_data, i32 (%struct.jpeg_decompress_struct*)** %consume_data24, align 4, !tbaa !30
  %67 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub25 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %67, i32 0, i32 0
  %decompress_data26 = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub25, i32 0, i32 3
  store i32 (%struct.jpeg_decompress_struct*, i8***)* @decompress_onepass, i32 (%struct.jpeg_decompress_struct*, i8***)** %decompress_data26, align 4, !tbaa !31
  %68 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub27 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %68, i32 0, i32 0
  %coef_arrays28 = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub27, i32 0, i32 4
  store %struct.jvirt_barray_control** null, %struct.jvirt_barray_control*** %coef_arrays28, align 4, !tbaa !32
  %69 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #3
  %70 = bitcast [64 x i16]** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  br label %if.end29

if.end29:                                         ; preds = %for.end22, %for.end
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem30 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %71, i32 0, i32 1
  %72 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem30, align 4, !tbaa !8
  %alloc_small31 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %72, i32 0, i32 0
  %73 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small31, align 4, !tbaa !12
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %75 = bitcast %struct.jpeg_decompress_struct* %74 to %struct.jpeg_common_struct*
  %call32 = call i8* %73(%struct.jpeg_common_struct* %75, i32 1, i32 128)
  %76 = bitcast i8* %call32 to i16*
  %77 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %workspace = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %77, i32 0, i32 5
  store i16* %76, i16** %workspace, align 4, !tbaa !34
  %78 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_input_pass(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 36
  store i32 0, i32* %input_iMCU_row, align 4, !tbaa !35
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_decompress_struct* %1)
  ret void
}

; Function Attrs: nounwind
define internal void @start_output_pass(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 79
  %2 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !15
  %3 = bitcast %struct.jpeg_d_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %4, i32 0, i32 0
  %coef_arrays = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub, i32 0, i32 4
  %5 = load %struct.jvirt_barray_control**, %struct.jvirt_barray_control*** %coef_arrays, align 4, !tbaa !32
  %cmp = icmp ne %struct.jvirt_barray_control** %5, null
  br i1 %cmp, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %do_block_smoothing = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 19
  %7 = load i32, i32* %do_block_smoothing, align 8, !tbaa !36
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 @smoothing_ok(%struct.jpeg_decompress_struct* %8)
  %tobool2 = icmp ne i32 %call, 0
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %land.lhs.true
  %9 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub4 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %9, i32 0, i32 0
  %decompress_data = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub4, i32 0, i32 3
  store i32 (%struct.jpeg_decompress_struct*, i8***)* @decompress_smooth_data, i32 (%struct.jpeg_decompress_struct*, i8***)** %decompress_data, align 4, !tbaa !31
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %if.then
  %10 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %10, i32 0, i32 0
  %decompress_data6 = getelementptr inbounds %struct.jpeg_d_coef_controller, %struct.jpeg_d_coef_controller* %pub5, i32 0, i32 3
  store i32 (%struct.jpeg_decompress_struct*, i8***)* @decompress_data, i32 (%struct.jpeg_decompress_struct*, i8***)** %decompress_data6, align 4, !tbaa !31
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then3
  br label %if.end7

if.end7:                                          ; preds = %if.end, %entry
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 38
  store i32 0, i32* %output_iMCU_row, align 4, !tbaa !37
  %12 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #3
  ret void
}

declare i32 @jround_up(i32, i32) #2

; Function Attrs: nounwind
define internal i32 @consume_data(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %MCU_col_num = alloca i32, align 4
  %blkn = alloca i32, align 4
  %ci = alloca i32, align 4
  %xindex = alloca i32, align 4
  %yindex = alloca i32, align 4
  %yoffset = alloca i32, align 4
  %start_col = alloca i32, align 4
  %buffer = alloca [4 x [64 x i16]**], align 16
  %buffer_ptr = alloca [64 x i16]*, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 79
  %2 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !15
  %3 = bitcast %struct.jpeg_d_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %xindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %start_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast [4 x [64 x i16]**]* %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #3
  %12 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %14 = load i32, i32* %ci, align 4, !tbaa !6
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 66
  %16 = load i32, i32* %comps_in_scan, align 8, !tbaa !38
  %cmp = icmp slt i32 %14, %16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 67
  %18 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %18
  %19 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  store %struct.jpeg_component_info* %19, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 1
  %21 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %access_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %21, i32 0, i32 8
  %22 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray, align 4, !tbaa !39
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %24 = bitcast %struct.jpeg_decompress_struct* %23 to %struct.jpeg_common_struct*
  %25 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %25, i32 0, i32 6
  %26 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %26, i32 0, i32 1
  %27 = load i32, i32* %component_index, align 4, !tbaa !40
  %arrayidx2 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 %27
  %28 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx2, align 4, !tbaa !2
  %29 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %29, i32 0, i32 36
  %30 = load i32, i32* %input_iMCU_row, align 4, !tbaa !35
  %31 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %31, i32 0, i32 3
  %32 = load i32, i32* %v_samp_factor, align 4, !tbaa !23
  %mul = mul i32 %30, %32
  %33 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor3 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %33, i32 0, i32 3
  %34 = load i32, i32* %v_samp_factor3, align 4, !tbaa !23
  %call = call [64 x i16]** %22(%struct.jpeg_common_struct* %24, %struct.jvirt_barray_control* %28, i32 %mul, i32 %34, i32 1)
  %35 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx4 = getelementptr inbounds [4 x [64 x i16]**], [4 x [64 x i16]**]* %buffer, i32 0, i32 %35
  store [64 x i16]** %call, [64 x i16]*** %arrayidx4, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %36 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %37, i32 0, i32 2
  %38 = load i32, i32* %MCU_vert_offset, align 4, !tbaa !41
  store i32 %38, i32* %yoffset, align 4, !tbaa !6
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc46, %for.end
  %39 = load i32, i32* %yoffset, align 4, !tbaa !6
  %40 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %40, i32 0, i32 3
  %41 = load i32, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !42
  %cmp6 = icmp slt i32 %39, %41
  br i1 %cmp6, label %for.body7, label %for.end48

for.body7:                                        ; preds = %for.cond5
  %42 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %42, i32 0, i32 1
  %43 = load i32, i32* %MCU_ctr, align 4, !tbaa !43
  store i32 %43, i32* %MCU_col_num, align 4, !tbaa !6
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc42, %for.body7
  %44 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 68
  %46 = load i32, i32* %MCUs_per_row, align 4, !tbaa !44
  %cmp9 = icmp ult i32 %44, %46
  br i1 %cmp9, label %for.body10, label %for.end44

for.body10:                                       ; preds = %for.cond8
  store i32 0, i32* %blkn, align 4, !tbaa !6
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc35, %for.body10
  %47 = load i32, i32* %ci, align 4, !tbaa !6
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 66
  %49 = load i32, i32* %comps_in_scan12, align 8, !tbaa !38
  %cmp13 = icmp slt i32 %47, %49
  br i1 %cmp13, label %for.body14, label %for.end37

for.body14:                                       ; preds = %for.cond11
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 67
  %51 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info15, i32 0, i32 %51
  %52 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx16, align 4, !tbaa !2
  store %struct.jpeg_component_info* %52, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %53 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %54 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %54, i32 0, i32 13
  %55 = load i32, i32* %MCU_width, align 4, !tbaa !45
  %mul17 = mul i32 %53, %55
  store i32 %mul17, i32* %start_col, align 4, !tbaa !6
  store i32 0, i32* %yindex, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc32, %for.body14
  %56 = load i32, i32* %yindex, align 4, !tbaa !6
  %57 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %57, i32 0, i32 14
  %58 = load i32, i32* %MCU_height, align 4, !tbaa !46
  %cmp19 = icmp slt i32 %56, %58
  br i1 %cmp19, label %for.body20, label %for.end34

for.body20:                                       ; preds = %for.cond18
  %59 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx21 = getelementptr inbounds [4 x [64 x i16]**], [4 x [64 x i16]**]* %buffer, i32 0, i32 %59
  %60 = load [64 x i16]**, [64 x i16]*** %arrayidx21, align 4, !tbaa !2
  %61 = load i32, i32* %yindex, align 4, !tbaa !6
  %62 = load i32, i32* %yoffset, align 4, !tbaa !6
  %add = add nsw i32 %61, %62
  %arrayidx22 = getelementptr inbounds [64 x i16]*, [64 x i16]** %60, i32 %add
  %63 = load [64 x i16]*, [64 x i16]** %arrayidx22, align 4, !tbaa !2
  %64 = load i32, i32* %start_col, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %63, i32 %64
  store [64 x i16]* %add.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  store i32 0, i32* %xindex, align 4, !tbaa !6
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc29, %for.body20
  %65 = load i32, i32* %xindex, align 4, !tbaa !6
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width24 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 0, i32 13
  %67 = load i32, i32* %MCU_width24, align 4, !tbaa !45
  %cmp25 = icmp slt i32 %65, %67
  br i1 %cmp25, label %for.body26, label %for.end31

for.body26:                                       ; preds = %for.cond23
  %68 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %68, i32 1
  store [64 x i16]* %incdec.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %69 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %69, i32 0, i32 4
  %70 = load i32, i32* %blkn, align 4, !tbaa !6
  %inc27 = add nsw i32 %70, 1
  store i32 %inc27, i32* %blkn, align 4, !tbaa !6
  %arrayidx28 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 %70
  store [64 x i16]* %68, [64 x i16]** %arrayidx28, align 4, !tbaa !2
  br label %for.inc29

for.inc29:                                        ; preds = %for.body26
  %71 = load i32, i32* %xindex, align 4, !tbaa !6
  %inc30 = add nsw i32 %71, 1
  store i32 %inc30, i32* %xindex, align 4, !tbaa !6
  br label %for.cond23

for.end31:                                        ; preds = %for.cond23
  br label %for.inc32

for.inc32:                                        ; preds = %for.end31
  %72 = load i32, i32* %yindex, align 4, !tbaa !6
  %inc33 = add nsw i32 %72, 1
  store i32 %inc33, i32* %yindex, align 4, !tbaa !6
  br label %for.cond18

for.end34:                                        ; preds = %for.cond18
  br label %for.inc35

for.inc35:                                        ; preds = %for.end34
  %73 = load i32, i32* %ci, align 4, !tbaa !6
  %inc36 = add nsw i32 %73, 1
  store i32 %inc36, i32* %ci, align 4, !tbaa !6
  br label %for.cond11

for.end37:                                        ; preds = %for.cond11
  %74 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %74, i32 0, i32 83
  %75 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy, align 4, !tbaa !47
  %decode_mcu = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %75, i32 0, i32 1
  %76 = load i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)** %decode_mcu, align 4, !tbaa !48
  %77 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %78 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer38 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %78, i32 0, i32 4
  %arraydecay = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer38, i32 0, i32 0
  %call39 = call i32 %76(%struct.jpeg_decompress_struct* %77, [64 x i16]** %arraydecay)
  %tobool = icmp ne i32 %call39, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.end37
  %79 = load i32, i32* %yoffset, align 4, !tbaa !6
  %80 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset40 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %80, i32 0, i32 2
  store i32 %79, i32* %MCU_vert_offset40, align 4, !tbaa !41
  %81 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %82 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_ctr41 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %82, i32 0, i32 1
  store i32 %81, i32* %MCU_ctr41, align 4, !tbaa !43
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.end37
  br label %for.inc42

for.inc42:                                        ; preds = %if.end
  %83 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %inc43 = add i32 %83, 1
  store i32 %inc43, i32* %MCU_col_num, align 4, !tbaa !6
  br label %for.cond8

for.end44:                                        ; preds = %for.cond8
  %84 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_ctr45 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %84, i32 0, i32 1
  store i32 0, i32* %MCU_ctr45, align 4, !tbaa !43
  br label %for.inc46

for.inc46:                                        ; preds = %for.end44
  %85 = load i32, i32* %yoffset, align 4, !tbaa !6
  %inc47 = add nsw i32 %85, 1
  store i32 %inc47, i32* %yoffset, align 4, !tbaa !6
  br label %for.cond5

for.end48:                                        ; preds = %for.cond5
  %86 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row49 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %86, i32 0, i32 36
  %87 = load i32, i32* %input_iMCU_row49, align 4, !tbaa !35
  %inc50 = add i32 %87, 1
  store i32 %inc50, i32* %input_iMCU_row49, align 4, !tbaa !35
  %88 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %88, i32 0, i32 64
  %89 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !50
  %cmp51 = icmp ult i32 %inc50, %89
  br i1 %cmp51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %for.end48
  %90 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_decompress_struct* %90)
  store i32 3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %for.end48
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %91, i32 0, i32 81
  %92 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !51
  %finish_input_pass = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %92, i32 0, i32 3
  %93 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %finish_input_pass, align 4, !tbaa !52
  %94 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %93(%struct.jpeg_decompress_struct* %94)
  store i32 4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end53, %if.then52, %if.then
  %95 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  %96 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #3
  %97 = bitcast [4 x [64 x i16]**]* %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #3
  %98 = bitcast i32* %start_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #3
  %99 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #3
  %100 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #3
  %101 = bitcast i32* %xindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #3
  %102 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #3
  %103 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #3
  %104 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #3
  %105 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  %106 = load i32, i32* %retval, align 4
  ret i32 %106
}

; Function Attrs: nounwind
define internal i32 @decompress_data(%struct.jpeg_decompress_struct* %cinfo, i8*** %output_buf) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %output_buf.addr = alloca i8***, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %last_iMCU_row = alloca i32, align 4
  %block_num = alloca i32, align 4
  %ci = alloca i32, align 4
  %block_row = alloca i32, align 4
  %block_rows = alloca i32, align 4
  %buffer = alloca [64 x i16]**, align 4
  %buffer_ptr = alloca [64 x i16]*, align 4
  %output_ptr = alloca i8**, align 4
  %output_col = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %inverse_DCT = alloca void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 79
  %2 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !15
  %3 = bitcast %struct.jpeg_d_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 64
  %6 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !50
  %sub = sub i32 %6, 1
  store i32 %sub, i32* %last_iMCU_row, align 4, !tbaa !6
  %7 = bitcast i32* %block_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %block_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast [64 x i16]*** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i8*** %output_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %output_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 35
  %18 = load i32, i32* %input_scan_number, align 8, !tbaa !54
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 37
  %20 = load i32, i32* %output_scan_number, align 8, !tbaa !55
  %cmp = icmp slt i32 %18, %20
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %while.cond
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 35
  %22 = load i32, i32* %input_scan_number2, align 8, !tbaa !54
  %23 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %23, i32 0, i32 37
  %24 = load i32, i32* %output_scan_number3, align 8, !tbaa !55
  %cmp4 = icmp eq i32 %22, %24
  br i1 %cmp4, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %lor.rhs
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %25, i32 0, i32 36
  %26 = load i32, i32* %input_iMCU_row, align 4, !tbaa !35
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 38
  %28 = load i32, i32* %output_iMCU_row, align 4, !tbaa !37
  %cmp5 = icmp ule i32 %26, %28
  br label %land.end

land.end:                                         ; preds = %land.rhs, %lor.rhs
  %29 = phi i1 [ false, %lor.rhs ], [ %cmp5, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %while.cond
  %30 = phi i1 [ true, %while.cond ], [ %29, %land.end ]
  br i1 %30, label %while.body, label %while.end

while.body:                                       ; preds = %lor.end
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 81
  %32 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !51
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %32, i32 0, i32 0
  %33 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !56
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %33(%struct.jpeg_decompress_struct* %34)
  %cmp6 = icmp eq i32 %call, 0
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %lor.end
  store i32 0, i32* %ci, align 4, !tbaa !6
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %35, i32 0, i32 44
  %36 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !21
  store %struct.jpeg_component_info* %36, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc44, %while.end
  %37 = load i32, i32* %ci, align 4, !tbaa !6
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %38, i32 0, i32 9
  %39 = load i32, i32* %num_components, align 4, !tbaa !22
  %cmp7 = icmp slt i32 %37, %39
  br i1 %cmp7, label %for.body, label %for.end47

for.body:                                         ; preds = %for.cond
  %40 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %40, i32 0, i32 12
  %41 = load i32, i32* %component_needed, align 4, !tbaa !57
  %tobool = icmp ne i32 %41, 0
  br i1 %tobool, label %if.end9, label %if.then8

if.then8:                                         ; preds = %for.body
  br label %for.inc44

if.end9:                                          ; preds = %for.body
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %42, i32 0, i32 1
  %43 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %access_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %43, i32 0, i32 8
  %44 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray, align 4, !tbaa !39
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %46 = bitcast %struct.jpeg_decompress_struct* %45 to %struct.jpeg_common_struct*
  %47 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %47, i32 0, i32 6
  %48 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 %48
  %49 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx, align 4, !tbaa !2
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 38
  %51 = load i32, i32* %output_iMCU_row10, align 4, !tbaa !37
  %52 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %52, i32 0, i32 3
  %53 = load i32, i32* %v_samp_factor, align 4, !tbaa !23
  %mul = mul i32 %51, %53
  %54 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor11 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %54, i32 0, i32 3
  %55 = load i32, i32* %v_samp_factor11, align 4, !tbaa !23
  %call12 = call [64 x i16]** %44(%struct.jpeg_common_struct* %46, %struct.jvirt_barray_control* %49, i32 %mul, i32 %55, i32 0)
  store [64 x i16]** %call12, [64 x i16]*** %buffer, align 4, !tbaa !2
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 38
  %57 = load i32, i32* %output_iMCU_row13, align 4, !tbaa !37
  %58 = load i32, i32* %last_iMCU_row, align 4, !tbaa !6
  %cmp14 = icmp ult i32 %57, %58
  br i1 %cmp14, label %if.then15, label %if.else

if.then15:                                        ; preds = %if.end9
  %59 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor16 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %59, i32 0, i32 3
  %60 = load i32, i32* %v_samp_factor16, align 4, !tbaa !23
  store i32 %60, i32* %block_rows, align 4, !tbaa !6
  br label %if.end22

if.else:                                          ; preds = %if.end9
  %61 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %61, i32 0, i32 8
  %62 = load i32, i32* %height_in_blocks, align 4, !tbaa !29
  %63 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor17 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %63, i32 0, i32 3
  %64 = load i32, i32* %v_samp_factor17, align 4, !tbaa !23
  %rem = urem i32 %62, %64
  store i32 %rem, i32* %block_rows, align 4, !tbaa !6
  %65 = load i32, i32* %block_rows, align 4, !tbaa !6
  %cmp18 = icmp eq i32 %65, 0
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.else
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor20 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 0, i32 3
  %67 = load i32, i32* %v_samp_factor20, align 4, !tbaa !23
  store i32 %67, i32* %block_rows, align 4, !tbaa !6
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %if.else
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %if.then15
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %idct = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %68, i32 0, i32 84
  %69 = load %struct.jpeg_inverse_dct*, %struct.jpeg_inverse_dct** %idct, align 8, !tbaa !58
  %inverse_DCT23 = getelementptr inbounds %struct.jpeg_inverse_dct, %struct.jpeg_inverse_dct* %69, i32 0, i32 1
  %70 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx24 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*]* %inverse_DCT23, i32 0, i32 %70
  %71 = load void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %arrayidx24, align 4, !tbaa !2
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* %71, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT, align 4, !tbaa !2
  %72 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %73 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx25 = getelementptr inbounds i8**, i8*** %72, i32 %73
  %74 = load i8**, i8*** %arrayidx25, align 4, !tbaa !2
  store i8** %74, i8*** %output_ptr, align 4, !tbaa !2
  store i32 0, i32* %block_row, align 4, !tbaa !6
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc41, %if.end22
  %75 = load i32, i32* %block_row, align 4, !tbaa !6
  %76 = load i32, i32* %block_rows, align 4, !tbaa !6
  %cmp27 = icmp slt i32 %75, %76
  br i1 %cmp27, label %for.body28, label %for.end43

for.body28:                                       ; preds = %for.cond26
  %77 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %78 = load i32, i32* %block_row, align 4, !tbaa !6
  %arrayidx29 = getelementptr inbounds [64 x i16]*, [64 x i16]** %77, i32 %78
  %79 = load [64 x i16]*, [64 x i16]** %arrayidx29, align 4, !tbaa !2
  %80 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %80, i32 0, i32 77
  %81 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !59
  %first_MCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %81, i32 0, i32 5
  %82 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx30 = getelementptr inbounds [10 x i32], [10 x i32]* %first_MCU_col, i32 0, i32 %82
  %83 = load i32, i32* %arrayidx30, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %79, i32 %83
  store [64 x i16]* %add.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  store i32 0, i32* %output_col, align 4, !tbaa !6
  %84 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master31 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %84, i32 0, i32 77
  %85 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master31, align 4, !tbaa !59
  %first_MCU_col32 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %85, i32 0, i32 5
  %86 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx33 = getelementptr inbounds [10 x i32], [10 x i32]* %first_MCU_col32, i32 0, i32 %86
  %87 = load i32, i32* %arrayidx33, align 4, !tbaa !6
  store i32 %87, i32* %block_num, align 4, !tbaa !6
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc, %for.body28
  %88 = load i32, i32* %block_num, align 4, !tbaa !6
  %89 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master35 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %89, i32 0, i32 77
  %90 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master35, align 4, !tbaa !59
  %last_MCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %90, i32 0, i32 6
  %91 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds [10 x i32], [10 x i32]* %last_MCU_col, i32 0, i32 %91
  %92 = load i32, i32* %arrayidx36, align 4, !tbaa !6
  %cmp37 = icmp ule i32 %88, %92
  br i1 %cmp37, label %for.body38, label %for.end

for.body38:                                       ; preds = %for.cond34
  %93 = load void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT, align 4, !tbaa !2
  %94 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %95 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %96 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %97 = bitcast [64 x i16]* %96 to i16*
  %98 = load i8**, i8*** %output_ptr, align 4, !tbaa !2
  %99 = load i32, i32* %output_col, align 4, !tbaa !6
  call void %93(%struct.jpeg_decompress_struct* %94, %struct.jpeg_component_info* %95, i16* %97, i8** %98, i32 %99)
  %100 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %100, i32 1
  store [64 x i16]* %incdec.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %101 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %101, i32 0, i32 9
  %102 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !60
  %103 = load i32, i32* %output_col, align 4, !tbaa !6
  %add = add i32 %103, %102
  store i32 %add, i32* %output_col, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body38
  %104 = load i32, i32* %block_num, align 4, !tbaa !6
  %inc = add i32 %104, 1
  store i32 %inc, i32* %block_num, align 4, !tbaa !6
  br label %for.cond34

for.end:                                          ; preds = %for.cond34
  %105 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size39 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %105, i32 0, i32 9
  %106 = load i32, i32* %DCT_scaled_size39, align 4, !tbaa !60
  %107 = load i8**, i8*** %output_ptr, align 4, !tbaa !2
  %add.ptr40 = getelementptr inbounds i8*, i8** %107, i32 %106
  store i8** %add.ptr40, i8*** %output_ptr, align 4, !tbaa !2
  br label %for.inc41

for.inc41:                                        ; preds = %for.end
  %108 = load i32, i32* %block_row, align 4, !tbaa !6
  %inc42 = add nsw i32 %108, 1
  store i32 %inc42, i32* %block_row, align 4, !tbaa !6
  br label %for.cond26

for.end43:                                        ; preds = %for.cond26
  br label %for.inc44

for.inc44:                                        ; preds = %for.end43, %if.then8
  %109 = load i32, i32* %ci, align 4, !tbaa !6
  %inc45 = add nsw i32 %109, 1
  store i32 %inc45, i32* %ci, align 4, !tbaa !6
  %110 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr46 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %110, i32 1
  store %struct.jpeg_component_info* %incdec.ptr46, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end47:                                        ; preds = %for.cond
  %111 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %111, i32 0, i32 38
  %112 = load i32, i32* %output_iMCU_row48, align 4, !tbaa !37
  %inc49 = add i32 %112, 1
  store i32 %inc49, i32* %output_iMCU_row48, align 4, !tbaa !37
  %113 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows50 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %113, i32 0, i32 64
  %114 = load i32, i32* %total_iMCU_rows50, align 8, !tbaa !50
  %cmp51 = icmp ult i32 %inc49, %114
  br i1 %cmp51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %for.end47
  store i32 3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %for.end47
  store i32 4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end53, %if.then52, %if.then
  %115 = bitcast void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #3
  %116 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #3
  %117 = bitcast i32* %output_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #3
  %118 = bitcast i8*** %output_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #3
  %119 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #3
  %120 = bitcast [64 x i16]*** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  %122 = bitcast i32* %block_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast i32* %block_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  %126 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = load i32, i32* %retval, align 4
  ret i32 %127
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal i32 @dummy_consume_data(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  ret i32 0
}

; Function Attrs: nounwind
define internal i32 @decompress_onepass(%struct.jpeg_decompress_struct* %cinfo, i8*** %output_buf) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %output_buf.addr = alloca i8***, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %MCU_col_num = alloca i32, align 4
  %last_MCU_col = alloca i32, align 4
  %last_iMCU_row = alloca i32, align 4
  %blkn = alloca i32, align 4
  %ci = alloca i32, align 4
  %xindex = alloca i32, align 4
  %yindex = alloca i32, align 4
  %yoffset = alloca i32, align 4
  %useful_width = alloca i32, align 4
  %output_ptr = alloca i8**, align 4
  %start_col = alloca i32, align 4
  %output_col = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %inverse_DCT = alloca void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 79
  %2 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !15
  %3 = bitcast %struct.jpeg_d_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %last_MCU_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %MCUs_per_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 68
  %7 = load i32, i32* %MCUs_per_row, align 4, !tbaa !44
  %sub = sub i32 %7, 1
  store i32 %sub, i32* %last_MCU_col, align 4, !tbaa !6
  %8 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 64
  %10 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !50
  %sub2 = sub i32 %10, 1
  store i32 %sub2, i32* %last_iMCU_row, align 4, !tbaa !6
  %11 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %xindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %useful_width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i8*** %output_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %start_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %output_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %22, i32 0, i32 2
  %23 = load i32, i32* %MCU_vert_offset, align 4, !tbaa !41
  store i32 %23, i32* %yoffset, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc61, %entry
  %24 = load i32, i32* %yoffset, align 4, !tbaa !6
  %25 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %25, i32 0, i32 3
  %26 = load i32, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !42
  %cmp = icmp slt i32 %24, %26
  br i1 %cmp, label %for.body, label %for.end63

for.body:                                         ; preds = %for.cond
  %27 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %27, i32 0, i32 1
  %28 = load i32, i32* %MCU_ctr, align 4, !tbaa !43
  store i32 %28, i32* %MCU_col_num, align 4, !tbaa !6
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc57, %for.body
  %29 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %30 = load i32, i32* %last_MCU_col, align 4, !tbaa !6
  %cmp4 = icmp ule i32 %29, %30
  br i1 %cmp4, label %for.body5, label %for.end59

for.body5:                                        ; preds = %for.cond3
  %31 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %31, i32 0, i32 4
  %arrayidx = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer, i32 0, i32 0
  %32 = load [64 x i16]*, [64 x i16]** %arrayidx, align 4, !tbaa !2
  %33 = bitcast [64 x i16]* %32 to i8*
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %blocks_in_MCU = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 70
  %35 = load i32, i32* %blocks_in_MCU, align 4, !tbaa !61
  %mul = mul i32 %35, 128
  call void @jzero_far(i8* %33, i32 %mul)
  %36 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %entropy = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %36, i32 0, i32 83
  %37 = load %struct.jpeg_entropy_decoder*, %struct.jpeg_entropy_decoder** %entropy, align 4, !tbaa !47
  %decode_mcu = getelementptr inbounds %struct.jpeg_entropy_decoder, %struct.jpeg_entropy_decoder* %37, i32 0, i32 1
  %38 = load i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)** %decode_mcu, align 4, !tbaa !48
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %40 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer6 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %40, i32 0, i32 4
  %arraydecay = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer6, i32 0, i32 0
  %call = call i32 %38(%struct.jpeg_decompress_struct* %39, [64 x i16]** %arraydecay)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body5
  %41 = load i32, i32* %yoffset, align 4, !tbaa !6
  %42 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset7 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %42, i32 0, i32 2
  store i32 %41, i32* %MCU_vert_offset7, align 4, !tbaa !41
  %43 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %44 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_ctr8 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %44, i32 0, i32 1
  store i32 %43, i32* %MCU_ctr8, align 4, !tbaa !43
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body5
  %45 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 77
  %47 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !59
  %first_iMCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %47, i32 0, i32 3
  %48 = load i32, i32* %first_iMCU_col, align 4, !tbaa !62
  %cmp9 = icmp uge i32 %45, %48
  br i1 %cmp9, label %land.lhs.true, label %if.end56

land.lhs.true:                                    ; preds = %if.end
  %49 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 77
  %51 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master10, align 4, !tbaa !59
  %last_iMCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %51, i32 0, i32 4
  %52 = load i32, i32* %last_iMCU_col, align 4, !tbaa !64
  %cmp11 = icmp ule i32 %49, %52
  br i1 %cmp11, label %if.then12, label %if.end56

if.then12:                                        ; preds = %land.lhs.true
  store i32 0, i32* %blkn, align 4, !tbaa !6
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc53, %if.then12
  %53 = load i32, i32* %ci, align 4, !tbaa !6
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 66
  %55 = load i32, i32* %comps_in_scan, align 8, !tbaa !38
  %cmp14 = icmp slt i32 %53, %55
  br i1 %cmp14, label %for.body15, label %for.end55

for.body15:                                       ; preds = %for.cond13
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 67
  %57 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 %57
  %58 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx16, align 4, !tbaa !2
  store %struct.jpeg_component_info* %58, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %59 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %59, i32 0, i32 12
  %60 = load i32, i32* %component_needed, align 4, !tbaa !57
  %tobool17 = icmp ne i32 %60, 0
  br i1 %tobool17, label %if.end19, label %if.then18

if.then18:                                        ; preds = %for.body15
  %61 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %61, i32 0, i32 15
  %62 = load i32, i32* %MCU_blocks, align 4, !tbaa !65
  %63 = load i32, i32* %blkn, align 4, !tbaa !6
  %add = add nsw i32 %63, %62
  store i32 %add, i32* %blkn, align 4, !tbaa !6
  br label %for.inc53

if.end19:                                         ; preds = %for.body15
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %idct = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 84
  %65 = load %struct.jpeg_inverse_dct*, %struct.jpeg_inverse_dct** %idct, align 8, !tbaa !58
  %inverse_DCT20 = getelementptr inbounds %struct.jpeg_inverse_dct, %struct.jpeg_inverse_dct* %65, i32 0, i32 1
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 0, i32 1
  %67 = load i32, i32* %component_index, align 4, !tbaa !40
  %arrayidx21 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*]* %inverse_DCT20, i32 0, i32 %67
  %68 = load void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %arrayidx21, align 4, !tbaa !2
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* %68, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT, align 4, !tbaa !2
  %69 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %70 = load i32, i32* %last_MCU_col, align 4, !tbaa !6
  %cmp22 = icmp ult i32 %69, %70
  br i1 %cmp22, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end19
  %71 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %71, i32 0, i32 13
  %72 = load i32, i32* %MCU_width, align 4, !tbaa !45
  br label %cond.end

cond.false:                                       ; preds = %if.end19
  %73 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_col_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %73, i32 0, i32 17
  %74 = load i32, i32* %last_col_width, align 4, !tbaa !66
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %72, %cond.true ], [ %74, %cond.false ]
  store i32 %cond, i32* %useful_width, align 4, !tbaa !6
  %75 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %76 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_index23 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %76, i32 0, i32 1
  %77 = load i32, i32* %component_index23, align 4, !tbaa !40
  %arrayidx24 = getelementptr inbounds i8**, i8*** %75, i32 %77
  %78 = load i8**, i8*** %arrayidx24, align 4, !tbaa !2
  %79 = load i32, i32* %yoffset, align 4, !tbaa !6
  %80 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %80, i32 0, i32 9
  %81 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !60
  %mul25 = mul nsw i32 %79, %81
  %add.ptr = getelementptr inbounds i8*, i8** %78, i32 %mul25
  store i8** %add.ptr, i8*** %output_ptr, align 4, !tbaa !2
  %82 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %83 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master26 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %83, i32 0, i32 77
  %84 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master26, align 4, !tbaa !59
  %first_iMCU_col27 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %84, i32 0, i32 3
  %85 = load i32, i32* %first_iMCU_col27, align 4, !tbaa !62
  %sub28 = sub i32 %82, %85
  %86 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_sample_width = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %86, i32 0, i32 16
  %87 = load i32, i32* %MCU_sample_width, align 4, !tbaa !67
  %mul29 = mul i32 %sub28, %87
  store i32 %mul29, i32* %start_col, align 4, !tbaa !6
  store i32 0, i32* %yindex, align 4, !tbaa !6
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc50, %cond.end
  %88 = load i32, i32* %yindex, align 4, !tbaa !6
  %89 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %89, i32 0, i32 14
  %90 = load i32, i32* %MCU_height, align 4, !tbaa !46
  %cmp31 = icmp slt i32 %88, %90
  br i1 %cmp31, label %for.body32, label %for.end52

for.body32:                                       ; preds = %for.cond30
  %91 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %91, i32 0, i32 36
  %92 = load i32, i32* %input_iMCU_row, align 4, !tbaa !35
  %93 = load i32, i32* %last_iMCU_row, align 4, !tbaa !6
  %cmp33 = icmp ult i32 %92, %93
  br i1 %cmp33, label %if.then36, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body32
  %94 = load i32, i32* %yoffset, align 4, !tbaa !6
  %95 = load i32, i32* %yindex, align 4, !tbaa !6
  %add34 = add nsw i32 %94, %95
  %96 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %96, i32 0, i32 18
  %97 = load i32, i32* %last_row_height, align 4, !tbaa !68
  %cmp35 = icmp slt i32 %add34, %97
  br i1 %cmp35, label %if.then36, label %if.end45

if.then36:                                        ; preds = %lor.lhs.false, %for.body32
  %98 = load i32, i32* %start_col, align 4, !tbaa !6
  store i32 %98, i32* %output_col, align 4, !tbaa !6
  store i32 0, i32* %xindex, align 4, !tbaa !6
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc, %if.then36
  %99 = load i32, i32* %xindex, align 4, !tbaa !6
  %100 = load i32, i32* %useful_width, align 4, !tbaa !6
  %cmp38 = icmp slt i32 %99, %100
  br i1 %cmp38, label %for.body39, label %for.end

for.body39:                                       ; preds = %for.cond37
  %101 = load void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT, align 4, !tbaa !2
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %103 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %104 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_buffer40 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %104, i32 0, i32 4
  %105 = load i32, i32* %blkn, align 4, !tbaa !6
  %106 = load i32, i32* %xindex, align 4, !tbaa !6
  %add41 = add nsw i32 %105, %106
  %arrayidx42 = getelementptr inbounds [10 x [64 x i16]*], [10 x [64 x i16]*]* %MCU_buffer40, i32 0, i32 %add41
  %107 = load [64 x i16]*, [64 x i16]** %arrayidx42, align 4, !tbaa !2
  %108 = bitcast [64 x i16]* %107 to i16*
  %109 = load i8**, i8*** %output_ptr, align 4, !tbaa !2
  %110 = load i32, i32* %output_col, align 4, !tbaa !6
  call void %101(%struct.jpeg_decompress_struct* %102, %struct.jpeg_component_info* %103, i16* %108, i8** %109, i32 %110)
  %111 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size43 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %111, i32 0, i32 9
  %112 = load i32, i32* %DCT_scaled_size43, align 4, !tbaa !60
  %113 = load i32, i32* %output_col, align 4, !tbaa !6
  %add44 = add i32 %113, %112
  store i32 %add44, i32* %output_col, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body39
  %114 = load i32, i32* %xindex, align 4, !tbaa !6
  %inc = add nsw i32 %114, 1
  store i32 %inc, i32* %xindex, align 4, !tbaa !6
  br label %for.cond37

for.end:                                          ; preds = %for.cond37
  br label %if.end45

if.end45:                                         ; preds = %for.end, %lor.lhs.false
  %115 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %MCU_width46 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %115, i32 0, i32 13
  %116 = load i32, i32* %MCU_width46, align 4, !tbaa !45
  %117 = load i32, i32* %blkn, align 4, !tbaa !6
  %add47 = add nsw i32 %117, %116
  store i32 %add47, i32* %blkn, align 4, !tbaa !6
  %118 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size48 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %118, i32 0, i32 9
  %119 = load i32, i32* %DCT_scaled_size48, align 4, !tbaa !60
  %120 = load i8**, i8*** %output_ptr, align 4, !tbaa !2
  %add.ptr49 = getelementptr inbounds i8*, i8** %120, i32 %119
  store i8** %add.ptr49, i8*** %output_ptr, align 4, !tbaa !2
  br label %for.inc50

for.inc50:                                        ; preds = %if.end45
  %121 = load i32, i32* %yindex, align 4, !tbaa !6
  %inc51 = add nsw i32 %121, 1
  store i32 %inc51, i32* %yindex, align 4, !tbaa !6
  br label %for.cond30

for.end52:                                        ; preds = %for.cond30
  br label %for.inc53

for.inc53:                                        ; preds = %for.end52, %if.then18
  %122 = load i32, i32* %ci, align 4, !tbaa !6
  %inc54 = add nsw i32 %122, 1
  store i32 %inc54, i32* %ci, align 4, !tbaa !6
  br label %for.cond13

for.end55:                                        ; preds = %for.cond13
  br label %if.end56

if.end56:                                         ; preds = %for.end55, %land.lhs.true, %if.end
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %123 = load i32, i32* %MCU_col_num, align 4, !tbaa !6
  %inc58 = add i32 %123, 1
  store i32 %inc58, i32* %MCU_col_num, align 4, !tbaa !6
  br label %for.cond3

for.end59:                                        ; preds = %for.cond3
  %124 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_ctr60 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %124, i32 0, i32 1
  store i32 0, i32* %MCU_ctr60, align 4, !tbaa !43
  br label %for.inc61

for.inc61:                                        ; preds = %for.end59
  %125 = load i32, i32* %yoffset, align 4, !tbaa !6
  %inc62 = add nsw i32 %125, 1
  store i32 %inc62, i32* %yoffset, align 4, !tbaa !6
  br label %for.cond

for.end63:                                        ; preds = %for.cond
  %126 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %126, i32 0, i32 38
  %127 = load i32, i32* %output_iMCU_row, align 4, !tbaa !37
  %inc64 = add i32 %127, 1
  store i32 %inc64, i32* %output_iMCU_row, align 4, !tbaa !37
  %128 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row65 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %128, i32 0, i32 36
  %129 = load i32, i32* %input_iMCU_row65, align 4, !tbaa !35
  %inc66 = add i32 %129, 1
  store i32 %inc66, i32* %input_iMCU_row65, align 4, !tbaa !35
  %130 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows67 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %130, i32 0, i32 64
  %131 = load i32, i32* %total_iMCU_rows67, align 8, !tbaa !50
  %cmp68 = icmp ult i32 %inc66, %131
  br i1 %cmp68, label %if.then69, label %if.end70

if.then69:                                        ; preds = %for.end63
  %132 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @start_iMCU_row(%struct.jpeg_decompress_struct* %132)
  store i32 3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end70:                                         ; preds = %for.end63
  %133 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %133, i32 0, i32 81
  %134 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !51
  %finish_input_pass = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %134, i32 0, i32 3
  %135 = load void (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*)** %finish_input_pass, align 4, !tbaa !52
  %136 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void %135(%struct.jpeg_decompress_struct* %136)
  store i32 4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end70, %if.then69, %if.then
  %137 = bitcast void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #3
  %138 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #3
  %139 = bitcast i32* %output_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #3
  %140 = bitcast i32* %start_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #3
  %141 = bitcast i8*** %output_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #3
  %142 = bitcast i32* %useful_width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #3
  %143 = bitcast i32* %yoffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #3
  %144 = bitcast i32* %yindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #3
  %145 = bitcast i32* %xindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #3
  %146 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #3
  %147 = bitcast i32* %blkn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #3
  %148 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #3
  %149 = bitcast i32* %last_MCU_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #3
  %150 = bitcast i32* %MCU_col_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #3
  %151 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #3
  %152 = load i32, i32* %retval, align 4
  ret i32 %152
}

; Function Attrs: nounwind
define internal void @start_iMCU_row(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 79
  %2 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !15
  %3 = bitcast %struct.jpeg_d_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comps_in_scan = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 66
  %5 = load i32, i32* %comps_in_scan, align 8, !tbaa !38
  %cmp = icmp sgt i32 %5, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %6, i32 0, i32 3
  store i32 1, i32* %MCU_rows_per_iMCU_row, align 4, !tbaa !42
  br label %if.end9

if.else:                                          ; preds = %entry
  %7 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %7, i32 0, i32 36
  %8 = load i32, i32* %input_iMCU_row, align 4, !tbaa !35
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 64
  %10 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !50
  %sub = sub i32 %10, 1
  %cmp2 = icmp ult i32 %8, %sub
  br i1 %cmp2, label %if.then3, label %if.else5

if.then3:                                         ; preds = %if.else
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 67
  %arrayidx = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info, i32 0, i32 0
  %12 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %12, i32 0, i32 3
  %13 = load i32, i32* %v_samp_factor, align 4, !tbaa !23
  %14 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row4 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %14, i32 0, i32 3
  store i32 %13, i32* %MCU_rows_per_iMCU_row4, align 4, !tbaa !42
  br label %if.end

if.else5:                                         ; preds = %if.else
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cur_comp_info6 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 67
  %arrayidx7 = getelementptr inbounds [4 x %struct.jpeg_component_info*], [4 x %struct.jpeg_component_info*]* %cur_comp_info6, i32 0, i32 0
  %16 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %arrayidx7, align 4, !tbaa !2
  %last_row_height = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %16, i32 0, i32 18
  %17 = load i32, i32* %last_row_height, align 4, !tbaa !68
  %18 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_rows_per_iMCU_row8 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %18, i32 0, i32 3
  store i32 %17, i32* %MCU_rows_per_iMCU_row8, align 4, !tbaa !42
  br label %if.end

if.end:                                           ; preds = %if.else5, %if.then3
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %19 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_ctr = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %19, i32 0, i32 1
  store i32 0, i32* %MCU_ctr, align 4, !tbaa !43
  %20 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %MCU_vert_offset = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %20, i32 0, i32 2
  store i32 0, i32* %MCU_vert_offset, align 4, !tbaa !41
  %21 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  ret void
}

; Function Attrs: nounwind
define internal i32 @smoothing_ok(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %smoothing_useful = alloca i32, align 4
  %ci = alloca i32, align 4
  %coefi = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %qtable = alloca %struct.JQUANT_TBL*, align 4
  %coef_bits = alloca i32*, align 4
  %coef_bits_latch = alloca i32*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 79
  %2 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !15
  %3 = bitcast %struct.jpeg_d_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %smoothing_useful to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  store i32 0, i32* %smoothing_useful, align 4, !tbaa !6
  %5 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %coefi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast %struct.JQUANT_TBL** %qtable to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32** %coef_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32** %coef_bits_latch to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %progressive_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 45
  %12 = load i32, i32* %progressive_mode, align 4, !tbaa !25
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef_bits2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 39
  %14 = load [64 x i32]*, [64 x i32]** %coef_bits2, align 8, !tbaa !69
  %cmp = icmp eq [64 x i32]* %14, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %15 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %coef_bits_latch3 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %15, i32 0, i32 7
  %16 = load i32*, i32** %coef_bits_latch3, align 4, !tbaa !20
  %cmp4 = icmp eq i32* %16, null
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 1
  %18 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %18, i32 0, i32 0
  %19 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !12
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %21 = bitcast %struct.jpeg_decompress_struct* %20 to %struct.jpeg_common_struct*
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 9
  %23 = load i32, i32* %num_components, align 4, !tbaa !22
  %mul = mul i32 %23, 24
  %call = call i8* %19(%struct.jpeg_common_struct* %21, i32 1, i32 %mul)
  %24 = bitcast i8* %call to i32*
  %25 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %coef_bits_latch6 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %25, i32 0, i32 7
  store i32* %24, i32** %coef_bits_latch6, align 4, !tbaa !20
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.end
  %26 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %coef_bits_latch8 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %26, i32 0, i32 7
  %27 = load i32*, i32** %coef_bits_latch8, align 4, !tbaa !20
  store i32* %27, i32** %coef_bits_latch, align 4, !tbaa !2
  store i32 0, i32* %ci, align 4, !tbaa !6
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 44
  %29 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !21
  store %struct.jpeg_component_info* %29, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc66, %if.end7
  %30 = load i32, i32* %ci, align 4, !tbaa !6
  %31 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %31, i32 0, i32 9
  %32 = load i32, i32* %num_components9, align 4, !tbaa !22
  %cmp10 = icmp slt i32 %30, %32
  br i1 %cmp10, label %for.body, label %for.end68

for.body:                                         ; preds = %for.cond
  %33 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %33, i32 0, i32 19
  %34 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_table, align 4, !tbaa !70
  store %struct.JQUANT_TBL* %34, %struct.JQUANT_TBL** %qtable, align 4, !tbaa !2
  %cmp11 = icmp eq %struct.JQUANT_TBL* %34, null
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %for.body
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %for.body
  %35 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtable, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %35, i32 0, i32 0
  %arrayidx = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 0
  %36 = load i16, i16* %arrayidx, align 4, !tbaa !71
  %conv = zext i16 %36 to i32
  %cmp14 = icmp eq i32 %conv, 0
  br i1 %cmp14, label %if.then46, label %lor.lhs.false16

lor.lhs.false16:                                  ; preds = %if.end13
  %37 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtable, align 4, !tbaa !2
  %quantval17 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %37, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval17, i32 0, i32 1
  %38 = load i16, i16* %arrayidx18, align 2, !tbaa !71
  %conv19 = zext i16 %38 to i32
  %cmp20 = icmp eq i32 %conv19, 0
  br i1 %cmp20, label %if.then46, label %lor.lhs.false22

lor.lhs.false22:                                  ; preds = %lor.lhs.false16
  %39 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtable, align 4, !tbaa !2
  %quantval23 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %39, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval23, i32 0, i32 8
  %40 = load i16, i16* %arrayidx24, align 4, !tbaa !71
  %conv25 = zext i16 %40 to i32
  %cmp26 = icmp eq i32 %conv25, 0
  br i1 %cmp26, label %if.then46, label %lor.lhs.false28

lor.lhs.false28:                                  ; preds = %lor.lhs.false22
  %41 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtable, align 4, !tbaa !2
  %quantval29 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %41, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval29, i32 0, i32 16
  %42 = load i16, i16* %arrayidx30, align 4, !tbaa !71
  %conv31 = zext i16 %42 to i32
  %cmp32 = icmp eq i32 %conv31, 0
  br i1 %cmp32, label %if.then46, label %lor.lhs.false34

lor.lhs.false34:                                  ; preds = %lor.lhs.false28
  %43 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtable, align 4, !tbaa !2
  %quantval35 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %43, i32 0, i32 0
  %arrayidx36 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval35, i32 0, i32 9
  %44 = load i16, i16* %arrayidx36, align 2, !tbaa !71
  %conv37 = zext i16 %44 to i32
  %cmp38 = icmp eq i32 %conv37, 0
  br i1 %cmp38, label %if.then46, label %lor.lhs.false40

lor.lhs.false40:                                  ; preds = %lor.lhs.false34
  %45 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtable, align 4, !tbaa !2
  %quantval41 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %45, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval41, i32 0, i32 2
  %46 = load i16, i16* %arrayidx42, align 4, !tbaa !71
  %conv43 = zext i16 %46 to i32
  %cmp44 = icmp eq i32 %conv43, 0
  br i1 %cmp44, label %if.then46, label %if.end47

if.then46:                                        ; preds = %lor.lhs.false40, %lor.lhs.false34, %lor.lhs.false28, %lor.lhs.false22, %lor.lhs.false16, %if.end13
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end47:                                         ; preds = %lor.lhs.false40
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef_bits48 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 39
  %48 = load [64 x i32]*, [64 x i32]** %coef_bits48, align 8, !tbaa !69
  %49 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx49 = getelementptr inbounds [64 x i32], [64 x i32]* %48, i32 %49
  %arraydecay = getelementptr inbounds [64 x i32], [64 x i32]* %arrayidx49, i32 0, i32 0
  store i32* %arraydecay, i32** %coef_bits, align 4, !tbaa !2
  %50 = load i32*, i32** %coef_bits, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i32, i32* %50, i32 0
  %51 = load i32, i32* %arrayidx50, align 4, !tbaa !6
  %cmp51 = icmp slt i32 %51, 0
  br i1 %cmp51, label %if.then53, label %if.end54

if.then53:                                        ; preds = %if.end47
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end54:                                         ; preds = %if.end47
  store i32 1, i32* %coefi, align 4, !tbaa !6
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc, %if.end54
  %52 = load i32, i32* %coefi, align 4, !tbaa !6
  %cmp56 = icmp sle i32 %52, 5
  br i1 %cmp56, label %for.body58, label %for.end

for.body58:                                       ; preds = %for.cond55
  %53 = load i32*, i32** %coef_bits, align 4, !tbaa !2
  %54 = load i32, i32* %coefi, align 4, !tbaa !6
  %arrayidx59 = getelementptr inbounds i32, i32* %53, i32 %54
  %55 = load i32, i32* %arrayidx59, align 4, !tbaa !6
  %56 = load i32*, i32** %coef_bits_latch, align 4, !tbaa !2
  %57 = load i32, i32* %coefi, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds i32, i32* %56, i32 %57
  store i32 %55, i32* %arrayidx60, align 4, !tbaa !6
  %58 = load i32*, i32** %coef_bits, align 4, !tbaa !2
  %59 = load i32, i32* %coefi, align 4, !tbaa !6
  %arrayidx61 = getelementptr inbounds i32, i32* %58, i32 %59
  %60 = load i32, i32* %arrayidx61, align 4, !tbaa !6
  %cmp62 = icmp ne i32 %60, 0
  br i1 %cmp62, label %if.then64, label %if.end65

if.then64:                                        ; preds = %for.body58
  store i32 1, i32* %smoothing_useful, align 4, !tbaa !6
  br label %if.end65

if.end65:                                         ; preds = %if.then64, %for.body58
  br label %for.inc

for.inc:                                          ; preds = %if.end65
  %61 = load i32, i32* %coefi, align 4, !tbaa !6
  %inc = add nsw i32 %61, 1
  store i32 %inc, i32* %coefi, align 4, !tbaa !6
  br label %for.cond55

for.end:                                          ; preds = %for.cond55
  %62 = load i32*, i32** %coef_bits_latch, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %62, i32 6
  store i32* %add.ptr, i32** %coef_bits_latch, align 4, !tbaa !2
  br label %for.inc66

for.inc66:                                        ; preds = %for.end
  %63 = load i32, i32* %ci, align 4, !tbaa !6
  %inc67 = add nsw i32 %63, 1
  store i32 %inc67, i32* %ci, align 4, !tbaa !6
  %64 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %64, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end68:                                        ; preds = %for.cond
  %65 = load i32, i32* %smoothing_useful, align 4, !tbaa !6
  store i32 %65, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end68, %if.then53, %if.then46, %if.then12, %if.then
  %66 = bitcast i32** %coef_bits_latch to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #3
  %67 = bitcast i32** %coef_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #3
  %68 = bitcast %struct.JQUANT_TBL** %qtable to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #3
  %69 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #3
  %70 = bitcast i32* %coefi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  %71 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #3
  %72 = bitcast i32* %smoothing_useful to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  %74 = load i32, i32* %retval, align 4
  ret i32 %74
}

; Function Attrs: nounwind
define internal i32 @decompress_smooth_data(%struct.jpeg_decompress_struct* %cinfo, i8*** %output_buf) #0 {
entry:
  %retval = alloca i32, align 4
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %output_buf.addr = alloca i8***, align 4
  %coef = alloca %struct.my_coef_controller*, align 4
  %last_iMCU_row = alloca i32, align 4
  %block_num = alloca i32, align 4
  %last_block_column = alloca i32, align 4
  %ci = alloca i32, align 4
  %block_row = alloca i32, align 4
  %block_rows = alloca i32, align 4
  %access_rows = alloca i32, align 4
  %buffer = alloca [64 x i16]**, align 4
  %buffer_ptr = alloca [64 x i16]*, align 4
  %prev_block_row = alloca [64 x i16]*, align 4
  %next_block_row = alloca [64 x i16]*, align 4
  %output_ptr = alloca i8**, align 4
  %output_col = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %inverse_DCT = alloca void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, align 4
  %first_row = alloca i32, align 4
  %last_row = alloca i32, align 4
  %workspace = alloca i16*, align 4
  %coef_bits = alloca i32*, align 4
  %quanttbl = alloca %struct.JQUANT_TBL*, align 4
  %Q00 = alloca i32, align 4
  %Q01 = alloca i32, align 4
  %Q02 = alloca i32, align 4
  %Q10 = alloca i32, align 4
  %Q11 = alloca i32, align 4
  %Q20 = alloca i32, align 4
  %num = alloca i32, align 4
  %DC1 = alloca i32, align 4
  %DC2 = alloca i32, align 4
  %DC3 = alloca i32, align 4
  %DC4 = alloca i32, align 4
  %DC5 = alloca i32, align 4
  %DC6 = alloca i32, align 4
  %DC7 = alloca i32, align 4
  %DC8 = alloca i32, align 4
  %DC9 = alloca i32, align 4
  %Al = alloca i32, align 4
  %pred = alloca i32, align 4
  %delta = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %coef1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 79
  %2 = load %struct.jpeg_d_coef_controller*, %struct.jpeg_d_coef_controller** %coef1, align 4, !tbaa !15
  %3 = bitcast %struct.jpeg_d_coef_controller* %2 to %struct.my_coef_controller*
  store %struct.my_coef_controller* %3, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %4 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 64
  %6 = load i32, i32* %total_iMCU_rows, align 8, !tbaa !50
  %sub = sub i32 %6, 1
  store i32 %sub, i32* %last_iMCU_row, align 4, !tbaa !6
  %7 = bitcast i32* %block_num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %last_block_column to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %block_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %access_rows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast [64 x i16]*** %buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast [64 x i16]** %prev_block_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast [64 x i16]** %next_block_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = bitcast i8*** %output_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #3
  %18 = bitcast i32* %output_col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i32* %first_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = bitcast i32* %last_row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = bitcast i16** %workspace to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = bitcast i32** %coef_bits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = bitcast %struct.JQUANT_TBL** %quanttbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = bitcast i32* %Q00 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = bitcast i32* %Q01 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #3
  %28 = bitcast i32* %Q02 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #3
  %29 = bitcast i32* %Q10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #3
  %30 = bitcast i32* %Q11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #3
  %31 = bitcast i32* %Q20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #3
  %32 = bitcast i32* %num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #3
  %33 = bitcast i32* %DC1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #3
  %34 = bitcast i32* %DC2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #3
  %35 = bitcast i32* %DC3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #3
  %36 = bitcast i32* %DC4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #3
  %37 = bitcast i32* %DC5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #3
  %38 = bitcast i32* %DC6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #3
  %39 = bitcast i32* %DC7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #3
  %40 = bitcast i32* %DC8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #3
  %41 = bitcast i32* %DC9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #3
  %42 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #3
  %43 = bitcast i32* %pred to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #3
  %44 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %workspace2 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %44, i32 0, i32 5
  %45 = load i16*, i16** %workspace2, align 4, !tbaa !34
  store i16* %45, i16** %workspace, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end13, %entry
  %46 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %46, i32 0, i32 35
  %47 = load i32, i32* %input_scan_number, align 8, !tbaa !54
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %48, i32 0, i32 37
  %49 = load i32, i32* %output_scan_number, align 8, !tbaa !55
  %cmp = icmp sle i32 %47, %49
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 81
  %51 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl, align 4, !tbaa !51
  %eoi_reached = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %51, i32 0, i32 5
  %52 = load i32, i32* %eoi_reached, align 4, !tbaa !72
  %tobool = icmp ne i32 %52, 0
  %lnot = xor i1 %tobool, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %53 = phi i1 [ false, %while.cond ], [ %lnot, %land.rhs ]
  br i1 %53, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_scan_number3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 35
  %55 = load i32, i32* %input_scan_number3, align 8, !tbaa !54
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_scan_number4 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 37
  %57 = load i32, i32* %output_scan_number4, align 8, !tbaa !55
  %cmp5 = icmp eq i32 %55, %57
  br i1 %cmp5, label %if.then, label %if.end9

if.then:                                          ; preds = %while.body
  %58 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #3
  %59 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %Ss = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %59, i32 0, i32 72
  %60 = load i32, i32* %Ss, align 8, !tbaa !73
  %cmp6 = icmp eq i32 %60, 0
  %61 = zext i1 %cmp6 to i64
  %cond = select i1 %cmp6, i32 1, i32 0
  store i32 %cond, i32* %delta, align 4, !tbaa !6
  %62 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %input_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %62, i32 0, i32 36
  %63 = load i32, i32* %input_iMCU_row, align 4, !tbaa !35
  %64 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %64, i32 0, i32 38
  %65 = load i32, i32* %output_iMCU_row, align 4, !tbaa !37
  %66 = load i32, i32* %delta, align 4, !tbaa !6
  %add = add i32 %65, %66
  %cmp7 = icmp ugt i32 %63, %add
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.then
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then8
  %67 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end9

if.end9:                                          ; preds = %cleanup.cont, %while.body
  %68 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %inputctl10 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %68, i32 0, i32 81
  %69 = load %struct.jpeg_input_controller*, %struct.jpeg_input_controller** %inputctl10, align 4, !tbaa !51
  %consume_input = getelementptr inbounds %struct.jpeg_input_controller, %struct.jpeg_input_controller* %69, i32 0, i32 0
  %70 = load i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)** %consume_input, align 4, !tbaa !56
  %71 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %call = call i32 %70(%struct.jpeg_decompress_struct* %71)
  %cmp11 = icmp eq i32 %call, 0
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end9
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup391

if.end13:                                         ; preds = %if.end9
  br label %while.cond

while.end:                                        ; preds = %cleanup, %land.end
  store i32 0, i32* %ci, align 4, !tbaa !6
  %72 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %72, i32 0, i32 44
  %73 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !21
  store %struct.jpeg_component_info* %73, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc380, %while.end
  %74 = load i32, i32* %ci, align 4, !tbaa !6
  %75 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %75, i32 0, i32 9
  %76 = load i32, i32* %num_components, align 4, !tbaa !22
  %cmp14 = icmp slt i32 %74, %76
  br i1 %cmp14, label %for.body, label %for.end383

for.body:                                         ; preds = %for.cond
  %77 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %77, i32 0, i32 12
  %78 = load i32, i32* %component_needed, align 4, !tbaa !57
  %tobool15 = icmp ne i32 %78, 0
  br i1 %tobool15, label %if.end17, label %if.then16

if.then16:                                        ; preds = %for.body
  br label %for.inc380

if.end17:                                         ; preds = %for.body
  %79 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %79, i32 0, i32 38
  %80 = load i32, i32* %output_iMCU_row18, align 4, !tbaa !37
  %81 = load i32, i32* %last_iMCU_row, align 4, !tbaa !6
  %cmp19 = icmp ult i32 %80, %81
  br i1 %cmp19, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.end17
  %82 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %82, i32 0, i32 3
  %83 = load i32, i32* %v_samp_factor, align 4, !tbaa !23
  store i32 %83, i32* %block_rows, align 4, !tbaa !6
  %84 = load i32, i32* %block_rows, align 4, !tbaa !6
  %mul = mul nsw i32 %84, 2
  store i32 %mul, i32* %access_rows, align 4, !tbaa !6
  store i32 0, i32* %last_row, align 4, !tbaa !6
  br label %if.end26

if.else:                                          ; preds = %if.end17
  %85 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %height_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %85, i32 0, i32 8
  %86 = load i32, i32* %height_in_blocks, align 4, !tbaa !29
  %87 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor21 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %87, i32 0, i32 3
  %88 = load i32, i32* %v_samp_factor21, align 4, !tbaa !23
  %rem = urem i32 %86, %88
  store i32 %rem, i32* %block_rows, align 4, !tbaa !6
  %89 = load i32, i32* %block_rows, align 4, !tbaa !6
  %cmp22 = icmp eq i32 %89, 0
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.else
  %90 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor24 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %90, i32 0, i32 3
  %91 = load i32, i32* %v_samp_factor24, align 4, !tbaa !23
  store i32 %91, i32* %block_rows, align 4, !tbaa !6
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %if.else
  %92 = load i32, i32* %block_rows, align 4, !tbaa !6
  store i32 %92, i32* %access_rows, align 4, !tbaa !6
  store i32 1, i32* %last_row, align 4, !tbaa !6
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then20
  %93 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %93, i32 0, i32 38
  %94 = load i32, i32* %output_iMCU_row27, align 4, !tbaa !37
  %cmp28 = icmp ugt i32 %94, 0
  br i1 %cmp28, label %if.then29, label %if.else38

if.then29:                                        ; preds = %if.end26
  %95 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor30 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %95, i32 0, i32 3
  %96 = load i32, i32* %v_samp_factor30, align 4, !tbaa !23
  %97 = load i32, i32* %access_rows, align 4, !tbaa !6
  %add31 = add nsw i32 %97, %96
  store i32 %add31, i32* %access_rows, align 4, !tbaa !6
  %98 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %98, i32 0, i32 1
  %99 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !8
  %access_virt_barray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %99, i32 0, i32 8
  %100 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray, align 4, !tbaa !39
  %101 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %102 = bitcast %struct.jpeg_decompress_struct* %101 to %struct.jpeg_common_struct*
  %103 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %103, i32 0, i32 6
  %104 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image, i32 0, i32 %104
  %105 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx, align 4, !tbaa !2
  %106 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row32 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %106, i32 0, i32 38
  %107 = load i32, i32* %output_iMCU_row32, align 4, !tbaa !37
  %sub33 = sub i32 %107, 1
  %108 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor34 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %108, i32 0, i32 3
  %109 = load i32, i32* %v_samp_factor34, align 4, !tbaa !23
  %mul35 = mul i32 %sub33, %109
  %110 = load i32, i32* %access_rows, align 4, !tbaa !6
  %call36 = call [64 x i16]** %100(%struct.jpeg_common_struct* %102, %struct.jvirt_barray_control* %105, i32 %mul35, i32 %110, i32 0)
  store [64 x i16]** %call36, [64 x i16]*** %buffer, align 4, !tbaa !2
  %111 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor37 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %111, i32 0, i32 3
  %112 = load i32, i32* %v_samp_factor37, align 4, !tbaa !23
  %113 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds [64 x i16]*, [64 x i16]** %113, i32 %112
  store [64 x i16]** %add.ptr, [64 x i16]*** %buffer, align 4, !tbaa !2
  store i32 0, i32* %first_row, align 4, !tbaa !6
  br label %if.end44

if.else38:                                        ; preds = %if.end26
  %114 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem39 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %114, i32 0, i32 1
  %115 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem39, align 4, !tbaa !8
  %access_virt_barray40 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %115, i32 0, i32 8
  %116 = load [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)** %access_virt_barray40, align 4, !tbaa !39
  %117 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %118 = bitcast %struct.jpeg_decompress_struct* %117 to %struct.jpeg_common_struct*
  %119 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %whole_image41 = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %119, i32 0, i32 6
  %120 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds [10 x %struct.jvirt_barray_control*], [10 x %struct.jvirt_barray_control*]* %whole_image41, i32 0, i32 %120
  %121 = load %struct.jvirt_barray_control*, %struct.jvirt_barray_control** %arrayidx42, align 4, !tbaa !2
  %122 = load i32, i32* %access_rows, align 4, !tbaa !6
  %call43 = call [64 x i16]** %116(%struct.jpeg_common_struct* %118, %struct.jvirt_barray_control* %121, i32 0, i32 %122, i32 0)
  store [64 x i16]** %call43, [64 x i16]*** %buffer, align 4, !tbaa !2
  store i32 1, i32* %first_row, align 4, !tbaa !6
  br label %if.end44

if.end44:                                         ; preds = %if.else38, %if.then29
  %123 = load %struct.my_coef_controller*, %struct.my_coef_controller** %coef, align 4, !tbaa !2
  %coef_bits_latch = getelementptr inbounds %struct.my_coef_controller, %struct.my_coef_controller* %123, i32 0, i32 7
  %124 = load i32*, i32** %coef_bits_latch, align 4, !tbaa !20
  %125 = load i32, i32* %ci, align 4, !tbaa !6
  %mul45 = mul nsw i32 %125, 6
  %add.ptr46 = getelementptr inbounds i32, i32* %124, i32 %mul45
  store i32* %add.ptr46, i32** %coef_bits, align 4, !tbaa !2
  %126 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %126, i32 0, i32 19
  %127 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_table, align 4, !tbaa !70
  store %struct.JQUANT_TBL* %127, %struct.JQUANT_TBL** %quanttbl, align 4, !tbaa !2
  %128 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quanttbl, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %128, i32 0, i32 0
  %arrayidx47 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 0
  %129 = load i16, i16* %arrayidx47, align 4, !tbaa !71
  %conv = zext i16 %129 to i32
  store i32 %conv, i32* %Q00, align 4, !tbaa !74
  %130 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quanttbl, align 4, !tbaa !2
  %quantval48 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %130, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval48, i32 0, i32 1
  %131 = load i16, i16* %arrayidx49, align 2, !tbaa !71
  %conv50 = zext i16 %131 to i32
  store i32 %conv50, i32* %Q01, align 4, !tbaa !74
  %132 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quanttbl, align 4, !tbaa !2
  %quantval51 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %132, i32 0, i32 0
  %arrayidx52 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval51, i32 0, i32 8
  %133 = load i16, i16* %arrayidx52, align 4, !tbaa !71
  %conv53 = zext i16 %133 to i32
  store i32 %conv53, i32* %Q10, align 4, !tbaa !74
  %134 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quanttbl, align 4, !tbaa !2
  %quantval54 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %134, i32 0, i32 0
  %arrayidx55 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval54, i32 0, i32 16
  %135 = load i16, i16* %arrayidx55, align 4, !tbaa !71
  %conv56 = zext i16 %135 to i32
  store i32 %conv56, i32* %Q20, align 4, !tbaa !74
  %136 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quanttbl, align 4, !tbaa !2
  %quantval57 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %136, i32 0, i32 0
  %arrayidx58 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval57, i32 0, i32 9
  %137 = load i16, i16* %arrayidx58, align 2, !tbaa !71
  %conv59 = zext i16 %137 to i32
  store i32 %conv59, i32* %Q11, align 4, !tbaa !74
  %138 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quanttbl, align 4, !tbaa !2
  %quantval60 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %138, i32 0, i32 0
  %arrayidx61 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval60, i32 0, i32 2
  %139 = load i16, i16* %arrayidx61, align 4, !tbaa !71
  %conv62 = zext i16 %139 to i32
  store i32 %conv62, i32* %Q02, align 4, !tbaa !74
  %140 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %idct = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %140, i32 0, i32 84
  %141 = load %struct.jpeg_inverse_dct*, %struct.jpeg_inverse_dct** %idct, align 8, !tbaa !58
  %inverse_DCT63 = getelementptr inbounds %struct.jpeg_inverse_dct, %struct.jpeg_inverse_dct* %141, i32 0, i32 1
  %142 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx64 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*]* %inverse_DCT63, i32 0, i32 %142
  %143 = load void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %arrayidx64, align 4, !tbaa !2
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* %143, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT, align 4, !tbaa !2
  %144 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %145 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds i8**, i8*** %144, i32 %145
  %146 = load i8**, i8*** %arrayidx65, align 4, !tbaa !2
  store i8** %146, i8*** %output_ptr, align 4, !tbaa !2
  store i32 0, i32* %block_row, align 4, !tbaa !6
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc377, %if.end44
  %147 = load i32, i32* %block_row, align 4, !tbaa !6
  %148 = load i32, i32* %block_rows, align 4, !tbaa !6
  %cmp67 = icmp slt i32 %147, %148
  br i1 %cmp67, label %for.body69, label %for.end379

for.body69:                                       ; preds = %for.cond66
  %149 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %150 = load i32, i32* %block_row, align 4, !tbaa !6
  %arrayidx70 = getelementptr inbounds [64 x i16]*, [64 x i16]** %149, i32 %150
  %151 = load [64 x i16]*, [64 x i16]** %arrayidx70, align 4, !tbaa !2
  %152 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %152, i32 0, i32 77
  %153 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master, align 4, !tbaa !59
  %first_MCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %153, i32 0, i32 5
  %154 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx71 = getelementptr inbounds [10 x i32], [10 x i32]* %first_MCU_col, i32 0, i32 %154
  %155 = load i32, i32* %arrayidx71, align 4, !tbaa !6
  %add.ptr72 = getelementptr inbounds [64 x i16], [64 x i16]* %151, i32 %155
  store [64 x i16]* %add.ptr72, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %156 = load i32, i32* %first_row, align 4, !tbaa !6
  %tobool73 = icmp ne i32 %156, 0
  br i1 %tobool73, label %land.lhs.true, label %if.else77

land.lhs.true:                                    ; preds = %for.body69
  %157 = load i32, i32* %block_row, align 4, !tbaa !6
  %cmp74 = icmp eq i32 %157, 0
  br i1 %cmp74, label %if.then76, label %if.else77

if.then76:                                        ; preds = %land.lhs.true
  %158 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  store [64 x i16]* %158, [64 x i16]** %prev_block_row, align 4, !tbaa !2
  br label %if.end80

if.else77:                                        ; preds = %land.lhs.true, %for.body69
  %159 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %160 = load i32, i32* %block_row, align 4, !tbaa !6
  %sub78 = sub nsw i32 %160, 1
  %arrayidx79 = getelementptr inbounds [64 x i16]*, [64 x i16]** %159, i32 %sub78
  %161 = load [64 x i16]*, [64 x i16]** %arrayidx79, align 4, !tbaa !2
  store [64 x i16]* %161, [64 x i16]** %prev_block_row, align 4, !tbaa !2
  br label %if.end80

if.end80:                                         ; preds = %if.else77, %if.then76
  %162 = load i32, i32* %last_row, align 4, !tbaa !6
  %tobool81 = icmp ne i32 %162, 0
  br i1 %tobool81, label %land.lhs.true82, label %if.else87

land.lhs.true82:                                  ; preds = %if.end80
  %163 = load i32, i32* %block_row, align 4, !tbaa !6
  %164 = load i32, i32* %block_rows, align 4, !tbaa !6
  %sub83 = sub nsw i32 %164, 1
  %cmp84 = icmp eq i32 %163, %sub83
  br i1 %cmp84, label %if.then86, label %if.else87

if.then86:                                        ; preds = %land.lhs.true82
  %165 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  store [64 x i16]* %165, [64 x i16]** %next_block_row, align 4, !tbaa !2
  br label %if.end90

if.else87:                                        ; preds = %land.lhs.true82, %if.end80
  %166 = load [64 x i16]**, [64 x i16]*** %buffer, align 4, !tbaa !2
  %167 = load i32, i32* %block_row, align 4, !tbaa !6
  %add88 = add nsw i32 %167, 1
  %arrayidx89 = getelementptr inbounds [64 x i16]*, [64 x i16]** %166, i32 %add88
  %168 = load [64 x i16]*, [64 x i16]** %arrayidx89, align 4, !tbaa !2
  store [64 x i16]* %168, [64 x i16]** %next_block_row, align 4, !tbaa !2
  br label %if.end90

if.end90:                                         ; preds = %if.else87, %if.then86
  %169 = load [64 x i16]*, [64 x i16]** %prev_block_row, align 4, !tbaa !2
  %arrayidx91 = getelementptr inbounds [64 x i16], [64 x i16]* %169, i32 0
  %arrayidx92 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx91, i32 0, i32 0
  %170 = load i16, i16* %arrayidx92, align 2, !tbaa !71
  %conv93 = sext i16 %170 to i32
  store i32 %conv93, i32* %DC3, align 4, !tbaa !6
  store i32 %conv93, i32* %DC2, align 4, !tbaa !6
  store i32 %conv93, i32* %DC1, align 4, !tbaa !6
  %171 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds [64 x i16], [64 x i16]* %171, i32 0
  %arrayidx95 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx94, i32 0, i32 0
  %172 = load i16, i16* %arrayidx95, align 2, !tbaa !71
  %conv96 = sext i16 %172 to i32
  store i32 %conv96, i32* %DC6, align 4, !tbaa !6
  store i32 %conv96, i32* %DC5, align 4, !tbaa !6
  store i32 %conv96, i32* %DC4, align 4, !tbaa !6
  %173 = load [64 x i16]*, [64 x i16]** %next_block_row, align 4, !tbaa !2
  %arrayidx97 = getelementptr inbounds [64 x i16], [64 x i16]* %173, i32 0
  %arrayidx98 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx97, i32 0, i32 0
  %174 = load i16, i16* %arrayidx98, align 2, !tbaa !71
  %conv99 = sext i16 %174 to i32
  store i32 %conv99, i32* %DC9, align 4, !tbaa !6
  store i32 %conv99, i32* %DC8, align 4, !tbaa !6
  store i32 %conv99, i32* %DC7, align 4, !tbaa !6
  store i32 0, i32* %output_col, align 4, !tbaa !6
  %175 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %175, i32 0, i32 7
  %176 = load i32, i32* %width_in_blocks, align 4, !tbaa !27
  %sub100 = sub i32 %176, 1
  store i32 %sub100, i32* %last_block_column, align 4, !tbaa !6
  %177 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master101 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %177, i32 0, i32 77
  %178 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master101, align 4, !tbaa !59
  %first_MCU_col102 = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %178, i32 0, i32 5
  %179 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx103 = getelementptr inbounds [10 x i32], [10 x i32]* %first_MCU_col102, i32 0, i32 %179
  %180 = load i32, i32* %arrayidx103, align 4, !tbaa !6
  store i32 %180, i32* %block_num, align 4, !tbaa !6
  br label %for.cond104

for.cond104:                                      ; preds = %for.inc, %if.end90
  %181 = load i32, i32* %block_num, align 4, !tbaa !6
  %182 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %master105 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %182, i32 0, i32 77
  %183 = load %struct.jpeg_decomp_master*, %struct.jpeg_decomp_master** %master105, align 4, !tbaa !59
  %last_MCU_col = getelementptr inbounds %struct.jpeg_decomp_master, %struct.jpeg_decomp_master* %183, i32 0, i32 6
  %184 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx106 = getelementptr inbounds [10 x i32], [10 x i32]* %last_MCU_col, i32 0, i32 %184
  %185 = load i32, i32* %arrayidx106, align 4, !tbaa !6
  %cmp107 = icmp ule i32 %181, %185
  br i1 %cmp107, label %for.body109, label %for.end

for.body109:                                      ; preds = %for.cond104
  %186 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %187 = load i16*, i16** %workspace, align 4, !tbaa !2
  %188 = bitcast i16* %187 to [64 x i16]*
  call void @jcopy_block_row([64 x i16]* %186, [64 x i16]* %188, i32 1)
  %189 = load i32, i32* %block_num, align 4, !tbaa !6
  %190 = load i32, i32* %last_block_column, align 4, !tbaa !6
  %cmp110 = icmp ult i32 %189, %190
  br i1 %cmp110, label %if.then112, label %if.end122

if.then112:                                       ; preds = %for.body109
  %191 = load [64 x i16]*, [64 x i16]** %prev_block_row, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds [64 x i16], [64 x i16]* %191, i32 1
  %arrayidx114 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx113, i32 0, i32 0
  %192 = load i16, i16* %arrayidx114, align 2, !tbaa !71
  %conv115 = sext i16 %192 to i32
  store i32 %conv115, i32* %DC3, align 4, !tbaa !6
  %193 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %arrayidx116 = getelementptr inbounds [64 x i16], [64 x i16]* %193, i32 1
  %arrayidx117 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx116, i32 0, i32 0
  %194 = load i16, i16* %arrayidx117, align 2, !tbaa !71
  %conv118 = sext i16 %194 to i32
  store i32 %conv118, i32* %DC6, align 4, !tbaa !6
  %195 = load [64 x i16]*, [64 x i16]** %next_block_row, align 4, !tbaa !2
  %arrayidx119 = getelementptr inbounds [64 x i16], [64 x i16]* %195, i32 1
  %arrayidx120 = getelementptr inbounds [64 x i16], [64 x i16]* %arrayidx119, i32 0, i32 0
  %196 = load i16, i16* %arrayidx120, align 2, !tbaa !71
  %conv121 = sext i16 %196 to i32
  store i32 %conv121, i32* %DC9, align 4, !tbaa !6
  br label %if.end122

if.end122:                                        ; preds = %if.then112, %for.body109
  %197 = load i32*, i32** %coef_bits, align 4, !tbaa !2
  %arrayidx123 = getelementptr inbounds i32, i32* %197, i32 1
  %198 = load i32, i32* %arrayidx123, align 4, !tbaa !6
  store i32 %198, i32* %Al, align 4, !tbaa !6
  %cmp124 = icmp ne i32 %198, 0
  br i1 %cmp124, label %land.lhs.true126, label %if.end169

land.lhs.true126:                                 ; preds = %if.end122
  %199 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i16, i16* %199, i32 1
  %200 = load i16, i16* %arrayidx127, align 2, !tbaa !71
  %conv128 = sext i16 %200 to i32
  %cmp129 = icmp eq i32 %conv128, 0
  br i1 %cmp129, label %if.then131, label %if.end169

if.then131:                                       ; preds = %land.lhs.true126
  %201 = load i32, i32* %Q00, align 4, !tbaa !74
  %mul132 = mul nsw i32 36, %201
  %202 = load i32, i32* %DC4, align 4, !tbaa !6
  %203 = load i32, i32* %DC6, align 4, !tbaa !6
  %sub133 = sub nsw i32 %202, %203
  %mul134 = mul nsw i32 %mul132, %sub133
  store i32 %mul134, i32* %num, align 4, !tbaa !74
  %204 = load i32, i32* %num, align 4, !tbaa !74
  %cmp135 = icmp sge i32 %204, 0
  br i1 %cmp135, label %if.then137, label %if.else150

if.then137:                                       ; preds = %if.then131
  %205 = load i32, i32* %Q01, align 4, !tbaa !74
  %shl = shl i32 %205, 7
  %206 = load i32, i32* %num, align 4, !tbaa !74
  %add138 = add nsw i32 %shl, %206
  %207 = load i32, i32* %Q01, align 4, !tbaa !74
  %shl139 = shl i32 %207, 8
  %div = sdiv i32 %add138, %shl139
  store i32 %div, i32* %pred, align 4, !tbaa !6
  %208 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp140 = icmp sgt i32 %208, 0
  br i1 %cmp140, label %land.lhs.true142, label %if.end149

land.lhs.true142:                                 ; preds = %if.then137
  %209 = load i32, i32* %pred, align 4, !tbaa !6
  %210 = load i32, i32* %Al, align 4, !tbaa !6
  %shl143 = shl i32 1, %210
  %cmp144 = icmp sge i32 %209, %shl143
  br i1 %cmp144, label %if.then146, label %if.end149

if.then146:                                       ; preds = %land.lhs.true142
  %211 = load i32, i32* %Al, align 4, !tbaa !6
  %shl147 = shl i32 1, %211
  %sub148 = sub nsw i32 %shl147, 1
  store i32 %sub148, i32* %pred, align 4, !tbaa !6
  br label %if.end149

if.end149:                                        ; preds = %if.then146, %land.lhs.true142, %if.then137
  br label %if.end166

if.else150:                                       ; preds = %if.then131
  %212 = load i32, i32* %Q01, align 4, !tbaa !74
  %shl151 = shl i32 %212, 7
  %213 = load i32, i32* %num, align 4, !tbaa !74
  %sub152 = sub nsw i32 %shl151, %213
  %214 = load i32, i32* %Q01, align 4, !tbaa !74
  %shl153 = shl i32 %214, 8
  %div154 = sdiv i32 %sub152, %shl153
  store i32 %div154, i32* %pred, align 4, !tbaa !6
  %215 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp155 = icmp sgt i32 %215, 0
  br i1 %cmp155, label %land.lhs.true157, label %if.end164

land.lhs.true157:                                 ; preds = %if.else150
  %216 = load i32, i32* %pred, align 4, !tbaa !6
  %217 = load i32, i32* %Al, align 4, !tbaa !6
  %shl158 = shl i32 1, %217
  %cmp159 = icmp sge i32 %216, %shl158
  br i1 %cmp159, label %if.then161, label %if.end164

if.then161:                                       ; preds = %land.lhs.true157
  %218 = load i32, i32* %Al, align 4, !tbaa !6
  %shl162 = shl i32 1, %218
  %sub163 = sub nsw i32 %shl162, 1
  store i32 %sub163, i32* %pred, align 4, !tbaa !6
  br label %if.end164

if.end164:                                        ; preds = %if.then161, %land.lhs.true157, %if.else150
  %219 = load i32, i32* %pred, align 4, !tbaa !6
  %sub165 = sub nsw i32 0, %219
  store i32 %sub165, i32* %pred, align 4, !tbaa !6
  br label %if.end166

if.end166:                                        ; preds = %if.end164, %if.end149
  %220 = load i32, i32* %pred, align 4, !tbaa !6
  %conv167 = trunc i32 %220 to i16
  %221 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx168 = getelementptr inbounds i16, i16* %221, i32 1
  store i16 %conv167, i16* %arrayidx168, align 2, !tbaa !71
  br label %if.end169

if.end169:                                        ; preds = %if.end166, %land.lhs.true126, %if.end122
  %222 = load i32*, i32** %coef_bits, align 4, !tbaa !2
  %arrayidx170 = getelementptr inbounds i32, i32* %222, i32 2
  %223 = load i32, i32* %arrayidx170, align 4, !tbaa !6
  store i32 %223, i32* %Al, align 4, !tbaa !6
  %cmp171 = icmp ne i32 %223, 0
  br i1 %cmp171, label %land.lhs.true173, label %if.end218

land.lhs.true173:                                 ; preds = %if.end169
  %224 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx174 = getelementptr inbounds i16, i16* %224, i32 8
  %225 = load i16, i16* %arrayidx174, align 2, !tbaa !71
  %conv175 = sext i16 %225 to i32
  %cmp176 = icmp eq i32 %conv175, 0
  br i1 %cmp176, label %if.then178, label %if.end218

if.then178:                                       ; preds = %land.lhs.true173
  %226 = load i32, i32* %Q00, align 4, !tbaa !74
  %mul179 = mul nsw i32 36, %226
  %227 = load i32, i32* %DC2, align 4, !tbaa !6
  %228 = load i32, i32* %DC8, align 4, !tbaa !6
  %sub180 = sub nsw i32 %227, %228
  %mul181 = mul nsw i32 %mul179, %sub180
  store i32 %mul181, i32* %num, align 4, !tbaa !74
  %229 = load i32, i32* %num, align 4, !tbaa !74
  %cmp182 = icmp sge i32 %229, 0
  br i1 %cmp182, label %if.then184, label %if.else199

if.then184:                                       ; preds = %if.then178
  %230 = load i32, i32* %Q10, align 4, !tbaa !74
  %shl185 = shl i32 %230, 7
  %231 = load i32, i32* %num, align 4, !tbaa !74
  %add186 = add nsw i32 %shl185, %231
  %232 = load i32, i32* %Q10, align 4, !tbaa !74
  %shl187 = shl i32 %232, 8
  %div188 = sdiv i32 %add186, %shl187
  store i32 %div188, i32* %pred, align 4, !tbaa !6
  %233 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp189 = icmp sgt i32 %233, 0
  br i1 %cmp189, label %land.lhs.true191, label %if.end198

land.lhs.true191:                                 ; preds = %if.then184
  %234 = load i32, i32* %pred, align 4, !tbaa !6
  %235 = load i32, i32* %Al, align 4, !tbaa !6
  %shl192 = shl i32 1, %235
  %cmp193 = icmp sge i32 %234, %shl192
  br i1 %cmp193, label %if.then195, label %if.end198

if.then195:                                       ; preds = %land.lhs.true191
  %236 = load i32, i32* %Al, align 4, !tbaa !6
  %shl196 = shl i32 1, %236
  %sub197 = sub nsw i32 %shl196, 1
  store i32 %sub197, i32* %pred, align 4, !tbaa !6
  br label %if.end198

if.end198:                                        ; preds = %if.then195, %land.lhs.true191, %if.then184
  br label %if.end215

if.else199:                                       ; preds = %if.then178
  %237 = load i32, i32* %Q10, align 4, !tbaa !74
  %shl200 = shl i32 %237, 7
  %238 = load i32, i32* %num, align 4, !tbaa !74
  %sub201 = sub nsw i32 %shl200, %238
  %239 = load i32, i32* %Q10, align 4, !tbaa !74
  %shl202 = shl i32 %239, 8
  %div203 = sdiv i32 %sub201, %shl202
  store i32 %div203, i32* %pred, align 4, !tbaa !6
  %240 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp204 = icmp sgt i32 %240, 0
  br i1 %cmp204, label %land.lhs.true206, label %if.end213

land.lhs.true206:                                 ; preds = %if.else199
  %241 = load i32, i32* %pred, align 4, !tbaa !6
  %242 = load i32, i32* %Al, align 4, !tbaa !6
  %shl207 = shl i32 1, %242
  %cmp208 = icmp sge i32 %241, %shl207
  br i1 %cmp208, label %if.then210, label %if.end213

if.then210:                                       ; preds = %land.lhs.true206
  %243 = load i32, i32* %Al, align 4, !tbaa !6
  %shl211 = shl i32 1, %243
  %sub212 = sub nsw i32 %shl211, 1
  store i32 %sub212, i32* %pred, align 4, !tbaa !6
  br label %if.end213

if.end213:                                        ; preds = %if.then210, %land.lhs.true206, %if.else199
  %244 = load i32, i32* %pred, align 4, !tbaa !6
  %sub214 = sub nsw i32 0, %244
  store i32 %sub214, i32* %pred, align 4, !tbaa !6
  br label %if.end215

if.end215:                                        ; preds = %if.end213, %if.end198
  %245 = load i32, i32* %pred, align 4, !tbaa !6
  %conv216 = trunc i32 %245 to i16
  %246 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx217 = getelementptr inbounds i16, i16* %246, i32 8
  store i16 %conv216, i16* %arrayidx217, align 2, !tbaa !71
  br label %if.end218

if.end218:                                        ; preds = %if.end215, %land.lhs.true173, %if.end169
  %247 = load i32*, i32** %coef_bits, align 4, !tbaa !2
  %arrayidx219 = getelementptr inbounds i32, i32* %247, i32 3
  %248 = load i32, i32* %arrayidx219, align 4, !tbaa !6
  store i32 %248, i32* %Al, align 4, !tbaa !6
  %cmp220 = icmp ne i32 %248, 0
  br i1 %cmp220, label %land.lhs.true222, label %if.end269

land.lhs.true222:                                 ; preds = %if.end218
  %249 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx223 = getelementptr inbounds i16, i16* %249, i32 16
  %250 = load i16, i16* %arrayidx223, align 2, !tbaa !71
  %conv224 = sext i16 %250 to i32
  %cmp225 = icmp eq i32 %conv224, 0
  br i1 %cmp225, label %if.then227, label %if.end269

if.then227:                                       ; preds = %land.lhs.true222
  %251 = load i32, i32* %Q00, align 4, !tbaa !74
  %mul228 = mul nsw i32 9, %251
  %252 = load i32, i32* %DC2, align 4, !tbaa !6
  %253 = load i32, i32* %DC8, align 4, !tbaa !6
  %add229 = add nsw i32 %252, %253
  %254 = load i32, i32* %DC5, align 4, !tbaa !6
  %mul230 = mul nsw i32 2, %254
  %sub231 = sub nsw i32 %add229, %mul230
  %mul232 = mul nsw i32 %mul228, %sub231
  store i32 %mul232, i32* %num, align 4, !tbaa !74
  %255 = load i32, i32* %num, align 4, !tbaa !74
  %cmp233 = icmp sge i32 %255, 0
  br i1 %cmp233, label %if.then235, label %if.else250

if.then235:                                       ; preds = %if.then227
  %256 = load i32, i32* %Q20, align 4, !tbaa !74
  %shl236 = shl i32 %256, 7
  %257 = load i32, i32* %num, align 4, !tbaa !74
  %add237 = add nsw i32 %shl236, %257
  %258 = load i32, i32* %Q20, align 4, !tbaa !74
  %shl238 = shl i32 %258, 8
  %div239 = sdiv i32 %add237, %shl238
  store i32 %div239, i32* %pred, align 4, !tbaa !6
  %259 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp240 = icmp sgt i32 %259, 0
  br i1 %cmp240, label %land.lhs.true242, label %if.end249

land.lhs.true242:                                 ; preds = %if.then235
  %260 = load i32, i32* %pred, align 4, !tbaa !6
  %261 = load i32, i32* %Al, align 4, !tbaa !6
  %shl243 = shl i32 1, %261
  %cmp244 = icmp sge i32 %260, %shl243
  br i1 %cmp244, label %if.then246, label %if.end249

if.then246:                                       ; preds = %land.lhs.true242
  %262 = load i32, i32* %Al, align 4, !tbaa !6
  %shl247 = shl i32 1, %262
  %sub248 = sub nsw i32 %shl247, 1
  store i32 %sub248, i32* %pred, align 4, !tbaa !6
  br label %if.end249

if.end249:                                        ; preds = %if.then246, %land.lhs.true242, %if.then235
  br label %if.end266

if.else250:                                       ; preds = %if.then227
  %263 = load i32, i32* %Q20, align 4, !tbaa !74
  %shl251 = shl i32 %263, 7
  %264 = load i32, i32* %num, align 4, !tbaa !74
  %sub252 = sub nsw i32 %shl251, %264
  %265 = load i32, i32* %Q20, align 4, !tbaa !74
  %shl253 = shl i32 %265, 8
  %div254 = sdiv i32 %sub252, %shl253
  store i32 %div254, i32* %pred, align 4, !tbaa !6
  %266 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp255 = icmp sgt i32 %266, 0
  br i1 %cmp255, label %land.lhs.true257, label %if.end264

land.lhs.true257:                                 ; preds = %if.else250
  %267 = load i32, i32* %pred, align 4, !tbaa !6
  %268 = load i32, i32* %Al, align 4, !tbaa !6
  %shl258 = shl i32 1, %268
  %cmp259 = icmp sge i32 %267, %shl258
  br i1 %cmp259, label %if.then261, label %if.end264

if.then261:                                       ; preds = %land.lhs.true257
  %269 = load i32, i32* %Al, align 4, !tbaa !6
  %shl262 = shl i32 1, %269
  %sub263 = sub nsw i32 %shl262, 1
  store i32 %sub263, i32* %pred, align 4, !tbaa !6
  br label %if.end264

if.end264:                                        ; preds = %if.then261, %land.lhs.true257, %if.else250
  %270 = load i32, i32* %pred, align 4, !tbaa !6
  %sub265 = sub nsw i32 0, %270
  store i32 %sub265, i32* %pred, align 4, !tbaa !6
  br label %if.end266

if.end266:                                        ; preds = %if.end264, %if.end249
  %271 = load i32, i32* %pred, align 4, !tbaa !6
  %conv267 = trunc i32 %271 to i16
  %272 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx268 = getelementptr inbounds i16, i16* %272, i32 16
  store i16 %conv267, i16* %arrayidx268, align 2, !tbaa !71
  br label %if.end269

if.end269:                                        ; preds = %if.end266, %land.lhs.true222, %if.end218
  %273 = load i32*, i32** %coef_bits, align 4, !tbaa !2
  %arrayidx270 = getelementptr inbounds i32, i32* %273, i32 4
  %274 = load i32, i32* %arrayidx270, align 4, !tbaa !6
  store i32 %274, i32* %Al, align 4, !tbaa !6
  %cmp271 = icmp ne i32 %274, 0
  br i1 %cmp271, label %land.lhs.true273, label %if.end320

land.lhs.true273:                                 ; preds = %if.end269
  %275 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx274 = getelementptr inbounds i16, i16* %275, i32 9
  %276 = load i16, i16* %arrayidx274, align 2, !tbaa !71
  %conv275 = sext i16 %276 to i32
  %cmp276 = icmp eq i32 %conv275, 0
  br i1 %cmp276, label %if.then278, label %if.end320

if.then278:                                       ; preds = %land.lhs.true273
  %277 = load i32, i32* %Q00, align 4, !tbaa !74
  %mul279 = mul nsw i32 5, %277
  %278 = load i32, i32* %DC1, align 4, !tbaa !6
  %279 = load i32, i32* %DC3, align 4, !tbaa !6
  %sub280 = sub nsw i32 %278, %279
  %280 = load i32, i32* %DC7, align 4, !tbaa !6
  %sub281 = sub nsw i32 %sub280, %280
  %281 = load i32, i32* %DC9, align 4, !tbaa !6
  %add282 = add nsw i32 %sub281, %281
  %mul283 = mul nsw i32 %mul279, %add282
  store i32 %mul283, i32* %num, align 4, !tbaa !74
  %282 = load i32, i32* %num, align 4, !tbaa !74
  %cmp284 = icmp sge i32 %282, 0
  br i1 %cmp284, label %if.then286, label %if.else301

if.then286:                                       ; preds = %if.then278
  %283 = load i32, i32* %Q11, align 4, !tbaa !74
  %shl287 = shl i32 %283, 7
  %284 = load i32, i32* %num, align 4, !tbaa !74
  %add288 = add nsw i32 %shl287, %284
  %285 = load i32, i32* %Q11, align 4, !tbaa !74
  %shl289 = shl i32 %285, 8
  %div290 = sdiv i32 %add288, %shl289
  store i32 %div290, i32* %pred, align 4, !tbaa !6
  %286 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp291 = icmp sgt i32 %286, 0
  br i1 %cmp291, label %land.lhs.true293, label %if.end300

land.lhs.true293:                                 ; preds = %if.then286
  %287 = load i32, i32* %pred, align 4, !tbaa !6
  %288 = load i32, i32* %Al, align 4, !tbaa !6
  %shl294 = shl i32 1, %288
  %cmp295 = icmp sge i32 %287, %shl294
  br i1 %cmp295, label %if.then297, label %if.end300

if.then297:                                       ; preds = %land.lhs.true293
  %289 = load i32, i32* %Al, align 4, !tbaa !6
  %shl298 = shl i32 1, %289
  %sub299 = sub nsw i32 %shl298, 1
  store i32 %sub299, i32* %pred, align 4, !tbaa !6
  br label %if.end300

if.end300:                                        ; preds = %if.then297, %land.lhs.true293, %if.then286
  br label %if.end317

if.else301:                                       ; preds = %if.then278
  %290 = load i32, i32* %Q11, align 4, !tbaa !74
  %shl302 = shl i32 %290, 7
  %291 = load i32, i32* %num, align 4, !tbaa !74
  %sub303 = sub nsw i32 %shl302, %291
  %292 = load i32, i32* %Q11, align 4, !tbaa !74
  %shl304 = shl i32 %292, 8
  %div305 = sdiv i32 %sub303, %shl304
  store i32 %div305, i32* %pred, align 4, !tbaa !6
  %293 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp306 = icmp sgt i32 %293, 0
  br i1 %cmp306, label %land.lhs.true308, label %if.end315

land.lhs.true308:                                 ; preds = %if.else301
  %294 = load i32, i32* %pred, align 4, !tbaa !6
  %295 = load i32, i32* %Al, align 4, !tbaa !6
  %shl309 = shl i32 1, %295
  %cmp310 = icmp sge i32 %294, %shl309
  br i1 %cmp310, label %if.then312, label %if.end315

if.then312:                                       ; preds = %land.lhs.true308
  %296 = load i32, i32* %Al, align 4, !tbaa !6
  %shl313 = shl i32 1, %296
  %sub314 = sub nsw i32 %shl313, 1
  store i32 %sub314, i32* %pred, align 4, !tbaa !6
  br label %if.end315

if.end315:                                        ; preds = %if.then312, %land.lhs.true308, %if.else301
  %297 = load i32, i32* %pred, align 4, !tbaa !6
  %sub316 = sub nsw i32 0, %297
  store i32 %sub316, i32* %pred, align 4, !tbaa !6
  br label %if.end317

if.end317:                                        ; preds = %if.end315, %if.end300
  %298 = load i32, i32* %pred, align 4, !tbaa !6
  %conv318 = trunc i32 %298 to i16
  %299 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx319 = getelementptr inbounds i16, i16* %299, i32 9
  store i16 %conv318, i16* %arrayidx319, align 2, !tbaa !71
  br label %if.end320

if.end320:                                        ; preds = %if.end317, %land.lhs.true273, %if.end269
  %300 = load i32*, i32** %coef_bits, align 4, !tbaa !2
  %arrayidx321 = getelementptr inbounds i32, i32* %300, i32 5
  %301 = load i32, i32* %arrayidx321, align 4, !tbaa !6
  store i32 %301, i32* %Al, align 4, !tbaa !6
  %cmp322 = icmp ne i32 %301, 0
  br i1 %cmp322, label %land.lhs.true324, label %if.end371

land.lhs.true324:                                 ; preds = %if.end320
  %302 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx325 = getelementptr inbounds i16, i16* %302, i32 2
  %303 = load i16, i16* %arrayidx325, align 2, !tbaa !71
  %conv326 = sext i16 %303 to i32
  %cmp327 = icmp eq i32 %conv326, 0
  br i1 %cmp327, label %if.then329, label %if.end371

if.then329:                                       ; preds = %land.lhs.true324
  %304 = load i32, i32* %Q00, align 4, !tbaa !74
  %mul330 = mul nsw i32 9, %304
  %305 = load i32, i32* %DC4, align 4, !tbaa !6
  %306 = load i32, i32* %DC6, align 4, !tbaa !6
  %add331 = add nsw i32 %305, %306
  %307 = load i32, i32* %DC5, align 4, !tbaa !6
  %mul332 = mul nsw i32 2, %307
  %sub333 = sub nsw i32 %add331, %mul332
  %mul334 = mul nsw i32 %mul330, %sub333
  store i32 %mul334, i32* %num, align 4, !tbaa !74
  %308 = load i32, i32* %num, align 4, !tbaa !74
  %cmp335 = icmp sge i32 %308, 0
  br i1 %cmp335, label %if.then337, label %if.else352

if.then337:                                       ; preds = %if.then329
  %309 = load i32, i32* %Q02, align 4, !tbaa !74
  %shl338 = shl i32 %309, 7
  %310 = load i32, i32* %num, align 4, !tbaa !74
  %add339 = add nsw i32 %shl338, %310
  %311 = load i32, i32* %Q02, align 4, !tbaa !74
  %shl340 = shl i32 %311, 8
  %div341 = sdiv i32 %add339, %shl340
  store i32 %div341, i32* %pred, align 4, !tbaa !6
  %312 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp342 = icmp sgt i32 %312, 0
  br i1 %cmp342, label %land.lhs.true344, label %if.end351

land.lhs.true344:                                 ; preds = %if.then337
  %313 = load i32, i32* %pred, align 4, !tbaa !6
  %314 = load i32, i32* %Al, align 4, !tbaa !6
  %shl345 = shl i32 1, %314
  %cmp346 = icmp sge i32 %313, %shl345
  br i1 %cmp346, label %if.then348, label %if.end351

if.then348:                                       ; preds = %land.lhs.true344
  %315 = load i32, i32* %Al, align 4, !tbaa !6
  %shl349 = shl i32 1, %315
  %sub350 = sub nsw i32 %shl349, 1
  store i32 %sub350, i32* %pred, align 4, !tbaa !6
  br label %if.end351

if.end351:                                        ; preds = %if.then348, %land.lhs.true344, %if.then337
  br label %if.end368

if.else352:                                       ; preds = %if.then329
  %316 = load i32, i32* %Q02, align 4, !tbaa !74
  %shl353 = shl i32 %316, 7
  %317 = load i32, i32* %num, align 4, !tbaa !74
  %sub354 = sub nsw i32 %shl353, %317
  %318 = load i32, i32* %Q02, align 4, !tbaa !74
  %shl355 = shl i32 %318, 8
  %div356 = sdiv i32 %sub354, %shl355
  store i32 %div356, i32* %pred, align 4, !tbaa !6
  %319 = load i32, i32* %Al, align 4, !tbaa !6
  %cmp357 = icmp sgt i32 %319, 0
  br i1 %cmp357, label %land.lhs.true359, label %if.end366

land.lhs.true359:                                 ; preds = %if.else352
  %320 = load i32, i32* %pred, align 4, !tbaa !6
  %321 = load i32, i32* %Al, align 4, !tbaa !6
  %shl360 = shl i32 1, %321
  %cmp361 = icmp sge i32 %320, %shl360
  br i1 %cmp361, label %if.then363, label %if.end366

if.then363:                                       ; preds = %land.lhs.true359
  %322 = load i32, i32* %Al, align 4, !tbaa !6
  %shl364 = shl i32 1, %322
  %sub365 = sub nsw i32 %shl364, 1
  store i32 %sub365, i32* %pred, align 4, !tbaa !6
  br label %if.end366

if.end366:                                        ; preds = %if.then363, %land.lhs.true359, %if.else352
  %323 = load i32, i32* %pred, align 4, !tbaa !6
  %sub367 = sub nsw i32 0, %323
  store i32 %sub367, i32* %pred, align 4, !tbaa !6
  br label %if.end368

if.end368:                                        ; preds = %if.end366, %if.end351
  %324 = load i32, i32* %pred, align 4, !tbaa !6
  %conv369 = trunc i32 %324 to i16
  %325 = load i16*, i16** %workspace, align 4, !tbaa !2
  %arrayidx370 = getelementptr inbounds i16, i16* %325, i32 2
  store i16 %conv369, i16* %arrayidx370, align 2, !tbaa !71
  br label %if.end371

if.end371:                                        ; preds = %if.end368, %land.lhs.true324, %if.end320
  %326 = load void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT, align 4, !tbaa !2
  %327 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %328 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %329 = load i16*, i16** %workspace, align 4, !tbaa !2
  %330 = load i8**, i8*** %output_ptr, align 4, !tbaa !2
  %331 = load i32, i32* %output_col, align 4, !tbaa !6
  call void %326(%struct.jpeg_decompress_struct* %327, %struct.jpeg_component_info* %328, i16* %329, i8** %330, i32 %331)
  %332 = load i32, i32* %DC2, align 4, !tbaa !6
  store i32 %332, i32* %DC1, align 4, !tbaa !6
  %333 = load i32, i32* %DC3, align 4, !tbaa !6
  store i32 %333, i32* %DC2, align 4, !tbaa !6
  %334 = load i32, i32* %DC5, align 4, !tbaa !6
  store i32 %334, i32* %DC4, align 4, !tbaa !6
  %335 = load i32, i32* %DC6, align 4, !tbaa !6
  store i32 %335, i32* %DC5, align 4, !tbaa !6
  %336 = load i32, i32* %DC8, align 4, !tbaa !6
  store i32 %336, i32* %DC7, align 4, !tbaa !6
  %337 = load i32, i32* %DC9, align 4, !tbaa !6
  store i32 %337, i32* %DC8, align 4, !tbaa !6
  %338 = load [64 x i16]*, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds [64 x i16], [64 x i16]* %338, i32 1
  store [64 x i16]* %incdec.ptr, [64 x i16]** %buffer_ptr, align 4, !tbaa !2
  %339 = load [64 x i16]*, [64 x i16]** %prev_block_row, align 4, !tbaa !2
  %incdec.ptr372 = getelementptr inbounds [64 x i16], [64 x i16]* %339, i32 1
  store [64 x i16]* %incdec.ptr372, [64 x i16]** %prev_block_row, align 4, !tbaa !2
  %340 = load [64 x i16]*, [64 x i16]** %next_block_row, align 4, !tbaa !2
  %incdec.ptr373 = getelementptr inbounds [64 x i16], [64 x i16]* %340, i32 1
  store [64 x i16]* %incdec.ptr373, [64 x i16]** %next_block_row, align 4, !tbaa !2
  %341 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %341, i32 0, i32 9
  %342 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !60
  %343 = load i32, i32* %output_col, align 4, !tbaa !6
  %add374 = add i32 %343, %342
  store i32 %add374, i32* %output_col, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %if.end371
  %344 = load i32, i32* %block_num, align 4, !tbaa !6
  %inc = add i32 %344, 1
  store i32 %inc, i32* %block_num, align 4, !tbaa !6
  br label %for.cond104

for.end:                                          ; preds = %for.cond104
  %345 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size375 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %345, i32 0, i32 9
  %346 = load i32, i32* %DCT_scaled_size375, align 4, !tbaa !60
  %347 = load i8**, i8*** %output_ptr, align 4, !tbaa !2
  %add.ptr376 = getelementptr inbounds i8*, i8** %347, i32 %346
  store i8** %add.ptr376, i8*** %output_ptr, align 4, !tbaa !2
  br label %for.inc377

for.inc377:                                       ; preds = %for.end
  %348 = load i32, i32* %block_row, align 4, !tbaa !6
  %inc378 = add nsw i32 %348, 1
  store i32 %inc378, i32* %block_row, align 4, !tbaa !6
  br label %for.cond66

for.end379:                                       ; preds = %for.cond66
  br label %for.inc380

for.inc380:                                       ; preds = %for.end379, %if.then16
  %349 = load i32, i32* %ci, align 4, !tbaa !6
  %inc381 = add nsw i32 %349, 1
  store i32 %inc381, i32* %ci, align 4, !tbaa !6
  %350 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr382 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %350, i32 1
  store %struct.jpeg_component_info* %incdec.ptr382, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end383:                                       ; preds = %for.cond
  %351 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_iMCU_row384 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %351, i32 0, i32 38
  %352 = load i32, i32* %output_iMCU_row384, align 4, !tbaa !37
  %inc385 = add i32 %352, 1
  store i32 %inc385, i32* %output_iMCU_row384, align 4, !tbaa !37
  %353 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %total_iMCU_rows386 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %353, i32 0, i32 64
  %354 = load i32, i32* %total_iMCU_rows386, align 8, !tbaa !50
  %cmp387 = icmp ult i32 %inc385, %354
  br i1 %cmp387, label %if.then389, label %if.end390

if.then389:                                       ; preds = %for.end383
  store i32 3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup391

if.end390:                                        ; preds = %for.end383
  store i32 4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup391

cleanup391:                                       ; preds = %if.end390, %if.then389, %if.then12
  %355 = bitcast i32* %pred to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %355) #3
  %356 = bitcast i32* %Al to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %356) #3
  %357 = bitcast i32* %DC9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %357) #3
  %358 = bitcast i32* %DC8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %358) #3
  %359 = bitcast i32* %DC7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %359) #3
  %360 = bitcast i32* %DC6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %360) #3
  %361 = bitcast i32* %DC5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %361) #3
  %362 = bitcast i32* %DC4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %362) #3
  %363 = bitcast i32* %DC3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %363) #3
  %364 = bitcast i32* %DC2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %364) #3
  %365 = bitcast i32* %DC1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %365) #3
  %366 = bitcast i32* %num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %366) #3
  %367 = bitcast i32* %Q20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %367) #3
  %368 = bitcast i32* %Q11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %368) #3
  %369 = bitcast i32* %Q10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %369) #3
  %370 = bitcast i32* %Q02 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %370) #3
  %371 = bitcast i32* %Q01 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %371) #3
  %372 = bitcast i32* %Q00 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %372) #3
  %373 = bitcast %struct.JQUANT_TBL** %quanttbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %373) #3
  %374 = bitcast i32** %coef_bits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %374) #3
  %375 = bitcast i16** %workspace to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %375) #3
  %376 = bitcast i32* %last_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %376) #3
  %377 = bitcast i32* %first_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %377) #3
  %378 = bitcast void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %inverse_DCT to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %378) #3
  %379 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %379) #3
  %380 = bitcast i32* %output_col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %380) #3
  %381 = bitcast i8*** %output_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %381) #3
  %382 = bitcast [64 x i16]** %next_block_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %382) #3
  %383 = bitcast [64 x i16]** %prev_block_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %383) #3
  %384 = bitcast [64 x i16]** %buffer_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %384) #3
  %385 = bitcast [64 x i16]*** %buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %385) #3
  %386 = bitcast i32* %access_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %386) #3
  %387 = bitcast i32* %block_rows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %387) #3
  %388 = bitcast i32* %block_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %388) #3
  %389 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %389) #3
  %390 = bitcast i32* %last_block_column to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %390) #3
  %391 = bitcast i32* %block_num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %391) #3
  %392 = bitcast i32* %last_iMCU_row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %392) #3
  %393 = bitcast %struct.my_coef_controller** %coef to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %393) #3
  %394 = load i32, i32* %retval, align 4
  ret i32 %394

unreachable:                                      ; preds = %cleanup
  unreachable
}

declare void @jcopy_block_row([64 x i16]*, [64 x i16]*, i32) #2

declare void @jzero_far(i8*, i32) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !4, i64 44, !7, i64 48, !7, i64 52, !10, i64 56, !7, i64 64, !7, i64 68, !4, i64 72, !7, i64 76, !7, i64 80, !7, i64 84, !4, i64 88, !7, i64 92, !7, i64 96, !7, i64 100, !7, i64 104, !7, i64 108, !7, i64 112, !7, i64 116, !7, i64 120, !7, i64 124, !7, i64 128, !7, i64 132, !3, i64 136, !7, i64 140, !7, i64 144, !7, i64 148, !7, i64 152, !7, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !7, i64 212, !3, i64 216, !7, i64 220, !7, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !7, i64 276, !7, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !11, i64 288, !11, i64 290, !7, i64 292, !4, i64 296, !7, i64 300, !3, i64 304, !7, i64 308, !7, i64 312, !7, i64 316, !7, i64 320, !3, i64 324, !7, i64 328, !4, i64 332, !7, i64 348, !7, i64 352, !7, i64 356, !4, i64 360, !7, i64 400, !7, i64 404, !7, i64 408, !7, i64 412, !7, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !3, i64 0}
!13 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!14 = !{!"long", !4, i64 0}
!15 = !{!9, !3, i64 428}
!16 = !{!17, !3, i64 0}
!17 = !{!"", !18, i64 0, !7, i64 20, !7, i64 24, !7, i64 28, !4, i64 32, !3, i64 72, !4, i64 76, !3, i64 116}
!18 = !{!"jpeg_d_coef_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16}
!19 = !{!17, !3, i64 8}
!20 = !{!17, !3, i64 116}
!21 = !{!9, !3, i64 216}
!22 = !{!9, !7, i64 36}
!23 = !{!24, !7, i64 12}
!24 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!25 = !{!9, !7, i64 220}
!26 = !{!13, !3, i64 20}
!27 = !{!24, !7, i64 28}
!28 = !{!24, !7, i64 8}
!29 = !{!24, !7, i64 32}
!30 = !{!17, !3, i64 4}
!31 = !{!17, !3, i64 12}
!32 = !{!17, !3, i64 16}
!33 = !{!13, !3, i64 4}
!34 = !{!17, !3, i64 72}
!35 = !{!9, !7, i64 148}
!36 = !{!9, !7, i64 80}
!37 = !{!9, !7, i64 156}
!38 = !{!9, !7, i64 328}
!39 = !{!13, !3, i64 32}
!40 = !{!24, !7, i64 4}
!41 = !{!17, !7, i64 24}
!42 = !{!17, !7, i64 28}
!43 = !{!17, !7, i64 20}
!44 = !{!9, !7, i64 348}
!45 = !{!24, !7, i64 52}
!46 = !{!24, !7, i64 56}
!47 = !{!9, !3, i64 444}
!48 = !{!49, !3, i64 4}
!49 = !{!"jpeg_entropy_decoder", !3, i64 0, !3, i64 4, !7, i64 8}
!50 = !{!9, !7, i64 320}
!51 = !{!9, !3, i64 436}
!52 = !{!53, !3, i64 12}
!53 = !{!"jpeg_input_controller", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20}
!54 = !{!9, !7, i64 144}
!55 = !{!9, !7, i64 152}
!56 = !{!53, !3, i64 0}
!57 = !{!24, !7, i64 48}
!58 = !{!9, !3, i64 448}
!59 = !{!9, !3, i64 420}
!60 = !{!24, !7, i64 36}
!61 = !{!9, !7, i64 356}
!62 = !{!63, !7, i64 12}
!63 = !{!"jpeg_decomp_master", !3, i64 0, !3, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !4, i64 20, !4, i64 60, !7, i64 100}
!64 = !{!63, !7, i64 16}
!65 = !{!24, !7, i64 60}
!66 = !{!24, !7, i64 68}
!67 = !{!24, !7, i64 64}
!68 = !{!24, !7, i64 72}
!69 = !{!9, !3, i64 160}
!70 = !{!24, !3, i64 76}
!71 = !{!11, !11, i64 0}
!72 = !{!53, !7, i64 20}
!73 = !{!9, !7, i64 400}
!74 = !{!14, !14, i64 0}
