; ModuleID = 'jquant1.c'
source_filename = "jquant1.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.my_cquantizer = type { %struct.jpeg_color_quantizer, i8**, i32, i8**, i32, [4 x i32], i32, [4 x [16 x i32]*], [4 x i16*], i32 }

@base_dither_matrix = internal constant [16 x [16 x i8]] [[16 x i8] c"\00\C00\F0\0C\CC<\FC\03\C33\F3\0F\CF?\FF", [16 x i8] c"\80@\B0p\8CL\BC|\83C\B3s\8FO\BF\7F", [16 x i8] c" \E0\10\D0,\EC\1C\DC#\E3\13\D3/\EF\1F\DF", [16 x i8] c"\A0`\90P\ACl\9C\\\A3c\93S\AFo\9F_", [16 x i8] c"\08\C88\F8\04\C44\F4\0B\CB;\FB\07\C77\F7", [16 x i8] c"\88H\B8x\84D\B4t\8BK\BB{\87G\B7w", [16 x i8] c"(\E8\18\D8$\E4\14\D4+\EB\1B\DB'\E7\17\D7", [16 x i8] c"\A8h\98X\A4d\94T\ABk\9B[\A7g\97W", [16 x i8] c"\02\C22\F2\0E\CE>\FE\01\C11\F1\0D\CD=\FD", [16 x i8] c"\82B\B2r\8EN\BE~\81A\B1q\8DM\BD}", [16 x i8] c"\22\E2\12\D2.\EE\1E\DE!\E1\11\D1-\ED\1D\DD", [16 x i8] c"\A2b\92R\AEn\9E^\A1a\91Q\ADm\9D]", [16 x i8] c"\0A\CA:\FA\06\C66\F6\09\C99\F9\05\C55\F5", [16 x i8] c"\8AJ\BAz\86F\B6v\89I\B9y\85E\B5u", [16 x i8] c"*\EA\1A\DA&\E6\16\D6)\E9\19\D9%\E5\15\D5", [16 x i8] c"\AAj\9AZ\A6f\96V\A9i\99Y\A5e\95U"], align 16
@__const.select_ncolors.RGB_order = private unnamed_addr constant [3 x i32] [i32 1, i32 0, i32 2], align 4
@rgb_green = internal constant [17 x i32] [i32 -1, i32 -1, i32 1, i32 -1, i32 -1, i32 -1, i32 1, i32 1, i32 1, i32 1, i32 2, i32 2, i32 1, i32 1, i32 2, i32 2, i32 -1], align 16
@rgb_red = internal constant [17 x i32] [i32 -1, i32 -1, i32 0, i32 -1, i32 -1, i32 -1, i32 0, i32 0, i32 2, i32 2, i32 3, i32 1, i32 0, i32 2, i32 3, i32 1, i32 -1], align 16
@rgb_blue = internal constant [17 x i32] [i32 -1, i32 -1, i32 2, i32 -1, i32 -1, i32 -1, i32 2, i32 2, i32 0, i32 0, i32 1, i32 3, i32 2, i32 0, i32 1, i32 3, i32 -1], align 16

; Function Attrs: nounwind
define hidden void @jinit_1pass_quantizer(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 1
  %2 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %2, i32 0, i32 0
  %3 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %5 = bitcast %struct.jpeg_decompress_struct* %4 to %struct.jpeg_common_struct*
  %call = call i8* %3(%struct.jpeg_common_struct* %5, i32 1, i32 88)
  %6 = bitcast i8* %call to %struct.my_cquantizer*
  store %struct.my_cquantizer* %6, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %7 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %8 = bitcast %struct.my_cquantizer* %7 to %struct.jpeg_color_quantizer*
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 87
  store %struct.jpeg_color_quantizer* %8, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %10 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %10, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub, i32 0, i32 0
  store void (%struct.jpeg_decompress_struct*, i32)* @start_pass_1_quant, void (%struct.jpeg_decompress_struct*, i32)** %start_pass, align 4, !tbaa !15
  %11 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %11, i32 0, i32 0
  %finish_pass = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub2, i32 0, i32 2
  %finish_pass3 = bitcast {}** %finish_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @finish_pass_1_quant, void (%struct.jpeg_decompress_struct*)** %finish_pass3, align 4, !tbaa !18
  %12 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub4 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %12, i32 0, i32 0
  %new_color_map = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub4, i32 0, i32 3
  %new_color_map5 = bitcast {}** %new_color_map to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @new_color_map_1_quant, void (%struct.jpeg_decompress_struct*)** %new_color_map5, align 4, !tbaa !19
  %13 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %13, i32 0, i32 8
  %arrayidx = getelementptr inbounds [4 x i16*], [4 x i16*]* %fserrors, i32 0, i32 0
  store i16* null, i16** %arrayidx, align 4, !tbaa !2
  %14 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %odither = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %14, i32 0, i32 7
  %arrayidx6 = getelementptr inbounds [4 x [16 x i32]*], [4 x [16 x i32]*]* %odither, i32 0, i32 0
  store [16 x i32]* null, [16 x i32]** %arrayidx6, align 4, !tbaa !2
  %15 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %15, i32 0, i32 29
  %16 = load i32, i32* %out_color_components, align 8, !tbaa !20
  %cmp = icmp sgt i32 %16, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 0
  %18 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !21
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %18, i32 0, i32 5
  store i32 55, i32* %msg_code, align 4, !tbaa !22
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err7 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err7, align 8, !tbaa !21
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 6
  %i = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx8 = getelementptr inbounds [8 x i32], [8 x i32]* %i, i32 0, i32 0
  store i32 4, i32* %arrayidx8, align 4, !tbaa !24
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err9 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err9, align 8, !tbaa !21
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 0
  %23 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !25
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = bitcast %struct.jpeg_decompress_struct* %24 to %struct.jpeg_common_struct*
  call void %23(%struct.jpeg_common_struct* %25)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %desired_number_of_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 23
  %27 = load i32, i32* %desired_number_of_colors, align 8, !tbaa !26
  %cmp10 = icmp sgt i32 %27, 256
  br i1 %cmp10, label %if.then11, label %if.end20

if.then11:                                        ; preds = %if.end
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err12 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 0
  %29 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err12, align 8, !tbaa !21
  %msg_code13 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %29, i32 0, i32 5
  store i32 57, i32* %msg_code13, align 4, !tbaa !22
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 0
  %31 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err14, align 8, !tbaa !21
  %msg_parm15 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %31, i32 0, i32 6
  %i16 = bitcast %union.anon* %msg_parm15 to [8 x i32]*
  %arrayidx17 = getelementptr inbounds [8 x i32], [8 x i32]* %i16, i32 0, i32 0
  store i32 256, i32* %arrayidx17, align 4, !tbaa !24
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err18 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 0
  %33 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err18, align 8, !tbaa !21
  %error_exit19 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %33, i32 0, i32 0
  %34 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit19, align 4, !tbaa !25
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %36 = bitcast %struct.jpeg_decompress_struct* %35 to %struct.jpeg_common_struct*
  call void %34(%struct.jpeg_common_struct* %36)
  br label %if.end20

if.end20:                                         ; preds = %if.then11, %if.end
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @create_colormap(%struct.jpeg_decompress_struct* %37)
  %38 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @create_colorindex(%struct.jpeg_decompress_struct* %38)
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 21
  %40 = load i32, i32* %dither_mode, align 8, !tbaa !27
  %cmp21 = icmp eq i32 %40, 2
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.end20
  %41 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @alloc_fs_workspace(%struct.jpeg_decompress_struct* %41)
  br label %if.end23

if.end23:                                         ; preds = %if.then22, %if.end20
  %42 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_1_quant(%struct.jpeg_decompress_struct* %cinfo, i32 %is_pre_scan) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %is_pre_scan.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %arraysize = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %is_pre_scan, i32* %is_pre_scan.addr, align 4, !tbaa !28
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i32* %arraysize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_colormap = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %6, i32 0, i32 1
  %7 = load i8**, i8*** %sv_colormap, align 4, !tbaa !29
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %colormap = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %8, i32 0, i32 33
  store i8** %7, i8*** %colormap, align 8, !tbaa !30
  %9 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_actual = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %9, i32 0, i32 2
  %10 = load i32, i32* %sv_actual, align 4, !tbaa !31
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %actual_number_of_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 32
  store i32 %10, i32* %actual_number_of_colors, align 4, !tbaa !32
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 21
  %13 = load i32, i32* %dither_mode, align 8, !tbaa !27
  switch i32 %13, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb4
    i32 2, label %sw.bb19
  ]

sw.bb:                                            ; preds = %entry
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 29
  %15 = load i32, i32* %out_color_components, align 8, !tbaa !20
  %cmp = icmp eq i32 %15, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb
  %16 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %16, i32 0, i32 0
  %color_quantize = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)* @color_quantize3, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)** %color_quantize, align 4, !tbaa !33
  br label %if.end

if.else:                                          ; preds = %sw.bb
  %17 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %17, i32 0, i32 0
  %color_quantize3 = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub2, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)* @color_quantize, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)** %color_quantize3, align 4, !tbaa !33
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 29
  %19 = load i32, i32* %out_color_components5, align 8, !tbaa !20
  %cmp6 = icmp eq i32 %19, 3
  br i1 %cmp6, label %if.then7, label %if.else10

if.then7:                                         ; preds = %sw.bb4
  %20 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub8 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %20, i32 0, i32 0
  %color_quantize9 = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub8, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)* @quantize3_ord_dither, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)** %color_quantize9, align 4, !tbaa !33
  br label %if.end13

if.else10:                                        ; preds = %sw.bb4
  %21 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub11 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %21, i32 0, i32 0
  %color_quantize12 = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub11, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)* @quantize_ord_dither, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)** %color_quantize12, align 4, !tbaa !33
  br label %if.end13

if.end13:                                         ; preds = %if.else10, %if.then7
  %22 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %row_index = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %22, i32 0, i32 6
  store i32 0, i32* %row_index, align 4, !tbaa !34
  %23 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %is_padded = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %23, i32 0, i32 4
  %24 = load i32, i32* %is_padded, align 4, !tbaa !35
  %tobool = icmp ne i32 %24, 0
  br i1 %tobool, label %if.end15, label %if.then14

if.then14:                                        ; preds = %if.end13
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @create_colorindex(%struct.jpeg_decompress_struct* %25)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %if.end13
  %26 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %odither = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %26, i32 0, i32 7
  %arrayidx = getelementptr inbounds [4 x [16 x i32]*], [4 x [16 x i32]*]* %odither, i32 0, i32 0
  %27 = load [16 x i32]*, [16 x i32]** %arrayidx, align 4, !tbaa !2
  %cmp16 = icmp eq [16 x i32]* %27, null
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @create_odither_tables(%struct.jpeg_decompress_struct* %28)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  br label %sw.epilog

sw.bb19:                                          ; preds = %entry
  %29 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %pub20 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %29, i32 0, i32 0
  %color_quantize21 = getelementptr inbounds %struct.jpeg_color_quantizer, %struct.jpeg_color_quantizer* %pub20, i32 0, i32 1
  store void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)* @quantize_fs_dither, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)** %color_quantize21, align 4, !tbaa !33
  %30 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %on_odd_row = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %30, i32 0, i32 9
  store i32 0, i32* %on_odd_row, align 4, !tbaa !36
  %31 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %31, i32 0, i32 8
  %arrayidx22 = getelementptr inbounds [4 x i16*], [4 x i16*]* %fserrors, i32 0, i32 0
  %32 = load i16*, i16** %arrayidx22, align 4, !tbaa !2
  %cmp23 = icmp eq i16* %32, null
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %sw.bb19
  %33 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @alloc_fs_workspace(%struct.jpeg_decompress_struct* %33)
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %sw.bb19
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 27
  %35 = load i32, i32* %output_width, align 8, !tbaa !37
  %add = add i32 %35, 2
  %mul = mul i32 %add, 2
  store i32 %mul, i32* %arraysize, align 4, !tbaa !38
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end25
  %36 = load i32, i32* %i, align 4, !tbaa !28
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components26 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 29
  %38 = load i32, i32* %out_color_components26, align 8, !tbaa !20
  %cmp27 = icmp slt i32 %36, %38
  br i1 %cmp27, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %39 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors28 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %39, i32 0, i32 8
  %40 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx29 = getelementptr inbounds [4 x i16*], [4 x i16*]* %fserrors28, i32 0, i32 %40
  %41 = load i16*, i16** %arrayidx29, align 4, !tbaa !2
  %42 = bitcast i16* %41 to i8*
  %43 = load i32, i32* %arraysize, align 4, !tbaa !38
  call void @jzero_far(i8* %42, i32 %43)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 0
  %46 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !21
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %46, i32 0, i32 5
  store i32 48, i32* %msg_code, align 4, !tbaa !22
  %47 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err30 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %47, i32 0, i32 0
  %48 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err30, align 8, !tbaa !21
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %48, i32 0, i32 0
  %49 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !25
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %51 = bitcast %struct.jpeg_decompress_struct* %50 to %struct.jpeg_common_struct*
  call void %49(%struct.jpeg_common_struct* %51)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %for.end, %if.end18, %if.end
  %52 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %arraysize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  ret void
}

; Function Attrs: nounwind
define internal void @finish_pass_1_quant(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define internal void @new_color_map_1_quant(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %0, i32 0, i32 0
  %1 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !21
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %1, i32 0, i32 5
  store i32 46, i32* %msg_code, align 4, !tbaa !22
  %2 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %2, i32 0, i32 0
  %3 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !21
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %3, i32 0, i32 0
  %4 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !25
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %6 = bitcast %struct.jpeg_decompress_struct* %5 to %struct.jpeg_common_struct*
  call void %4(%struct.jpeg_common_struct* %6)
  ret void
}

; Function Attrs: nounwind
define internal void @create_colormap(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %colormap = alloca i8**, align 4
  %total_colors = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %nci = alloca i32, align 4
  %blksize = alloca i32, align 4
  %blkdist = alloca i32, align 4
  %ptr = alloca i32, align 4
  %val = alloca i32, align 4
  %_mp = alloca i32*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i8*** %colormap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %total_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %nci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %blksize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %blkdist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %Ncolors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %15, i32 0, i32 5
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %Ncolors, i32 0, i32 0
  %call = call i32 @select_ncolors(%struct.jpeg_decompress_struct* %14, i32* %arraydecay)
  store i32 %call, i32* %total_colors, align 4, !tbaa !28
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 29
  %17 = load i32, i32* %out_color_components, align 8, !tbaa !20
  %cmp = icmp eq i32 %17, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %if.then
  %18 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !21
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 6
  %i2 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arraydecay3 = getelementptr inbounds [8 x i32], [8 x i32]* %i2, i32 0, i32 0
  store i32* %arraydecay3, i32** %_mp, align 4, !tbaa !2
  %21 = load i32, i32* %total_colors, align 4, !tbaa !28
  %22 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %22, i32 0
  store i32 %21, i32* %arrayidx, align 4, !tbaa !28
  %23 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %Ncolors4 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %23, i32 0, i32 5
  %arrayidx5 = getelementptr inbounds [4 x i32], [4 x i32]* %Ncolors4, i32 0, i32 0
  %24 = load i32, i32* %arrayidx5, align 4, !tbaa !28
  %25 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i32, i32* %25, i32 1
  store i32 %24, i32* %arrayidx6, align 4, !tbaa !28
  %26 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %Ncolors7 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %26, i32 0, i32 5
  %arrayidx8 = getelementptr inbounds [4 x i32], [4 x i32]* %Ncolors7, i32 0, i32 1
  %27 = load i32, i32* %arrayidx8, align 4, !tbaa !28
  %28 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds i32, i32* %28, i32 2
  store i32 %27, i32* %arrayidx9, align 4, !tbaa !28
  %29 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %Ncolors10 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %29, i32 0, i32 5
  %arrayidx11 = getelementptr inbounds [4 x i32], [4 x i32]* %Ncolors10, i32 0, i32 2
  %30 = load i32, i32* %arrayidx11, align 4, !tbaa !28
  %31 = load i32*, i32** %_mp, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %31, i32 3
  store i32 %30, i32* %arrayidx12, align 4, !tbaa !28
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err13 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 0
  %33 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err13, align 8, !tbaa !21
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %33, i32 0, i32 5
  store i32 94, i32* %msg_code, align 4, !tbaa !22
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err14, align 8, !tbaa !21
  %emit_message = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 1
  %36 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message, align 4, !tbaa !39
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %38 = bitcast %struct.jpeg_decompress_struct* %37 to %struct.jpeg_common_struct*
  call void %36(%struct.jpeg_common_struct* %38, i32 1)
  %39 = bitcast i32** %_mp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #3
  br label %do.cond

do.cond:                                          ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end

if.else:                                          ; preds = %entry
  %40 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err15 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %40, i32 0, i32 0
  %41 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err15, align 8, !tbaa !21
  %msg_code16 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %41, i32 0, i32 5
  store i32 95, i32* %msg_code16, align 4, !tbaa !22
  %42 = load i32, i32* %total_colors, align 4, !tbaa !28
  %43 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err17 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %43, i32 0, i32 0
  %44 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err17, align 8, !tbaa !21
  %msg_parm18 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %44, i32 0, i32 6
  %i19 = bitcast %union.anon* %msg_parm18 to [8 x i32]*
  %arrayidx20 = getelementptr inbounds [8 x i32], [8 x i32]* %i19, i32 0, i32 0
  store i32 %42, i32* %arrayidx20, align 4, !tbaa !24
  %45 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err21 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %45, i32 0, i32 0
  %46 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err21, align 8, !tbaa !21
  %emit_message22 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %46, i32 0, i32 1
  %47 = load void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*, i32)** %emit_message22, align 4, !tbaa !39
  %48 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %49 = bitcast %struct.jpeg_decompress_struct* %48 to %struct.jpeg_common_struct*
  call void %47(%struct.jpeg_common_struct* %49, i32 1)
  br label %if.end

if.end:                                           ; preds = %if.else, %do.end
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %50, i32 0, i32 1
  %51 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %51, i32 0, i32 2
  %52 = load i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_sarray, align 4, !tbaa !40
  %53 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %54 = bitcast %struct.jpeg_decompress_struct* %53 to %struct.jpeg_common_struct*
  %55 = load i32, i32* %total_colors, align 4, !tbaa !28
  %56 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components23 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %56, i32 0, i32 29
  %57 = load i32, i32* %out_color_components23, align 8, !tbaa !20
  %call24 = call i8** %52(%struct.jpeg_common_struct* %54, i32 1, i32 %55, i32 %57)
  store i8** %call24, i8*** %colormap, align 4, !tbaa !2
  %58 = load i32, i32* %total_colors, align 4, !tbaa !28
  store i32 %58, i32* %blkdist, align 4, !tbaa !28
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc47, %if.end
  %59 = load i32, i32* %i, align 4, !tbaa !28
  %60 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components25 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %60, i32 0, i32 29
  %61 = load i32, i32* %out_color_components25, align 8, !tbaa !20
  %cmp26 = icmp slt i32 %59, %61
  br i1 %cmp26, label %for.body, label %for.end49

for.body:                                         ; preds = %for.cond
  %62 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %Ncolors27 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %62, i32 0, i32 5
  %63 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx28 = getelementptr inbounds [4 x i32], [4 x i32]* %Ncolors27, i32 0, i32 %63
  %64 = load i32, i32* %arrayidx28, align 4, !tbaa !28
  store i32 %64, i32* %nci, align 4, !tbaa !28
  %65 = load i32, i32* %blkdist, align 4, !tbaa !28
  %66 = load i32, i32* %nci, align 4, !tbaa !28
  %div = sdiv i32 %65, %66
  store i32 %div, i32* %blksize, align 4, !tbaa !28
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc44, %for.body
  %67 = load i32, i32* %j, align 4, !tbaa !28
  %68 = load i32, i32* %nci, align 4, !tbaa !28
  %cmp30 = icmp slt i32 %67, %68
  br i1 %cmp30, label %for.body31, label %for.end46

for.body31:                                       ; preds = %for.cond29
  %69 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %70 = load i32, i32* %i, align 4, !tbaa !28
  %71 = load i32, i32* %j, align 4, !tbaa !28
  %72 = load i32, i32* %nci, align 4, !tbaa !28
  %sub = sub nsw i32 %72, 1
  %call32 = call i32 @output_value(%struct.jpeg_decompress_struct* %69, i32 %70, i32 %71, i32 %sub)
  store i32 %call32, i32* %val, align 4, !tbaa !28
  %73 = load i32, i32* %j, align 4, !tbaa !28
  %74 = load i32, i32* %blksize, align 4, !tbaa !28
  %mul = mul nsw i32 %73, %74
  store i32 %mul, i32* %ptr, align 4, !tbaa !28
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc41, %for.body31
  %75 = load i32, i32* %ptr, align 4, !tbaa !28
  %76 = load i32, i32* %total_colors, align 4, !tbaa !28
  %cmp34 = icmp slt i32 %75, %76
  br i1 %cmp34, label %for.body35, label %for.end43

for.body35:                                       ; preds = %for.cond33
  store i32 0, i32* %k, align 4, !tbaa !28
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc, %for.body35
  %77 = load i32, i32* %k, align 4, !tbaa !28
  %78 = load i32, i32* %blksize, align 4, !tbaa !28
  %cmp37 = icmp slt i32 %77, %78
  br i1 %cmp37, label %for.body38, label %for.end

for.body38:                                       ; preds = %for.cond36
  %79 = load i32, i32* %val, align 4, !tbaa !28
  %conv = trunc i32 %79 to i8
  %80 = load i8**, i8*** %colormap, align 4, !tbaa !2
  %81 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx39 = getelementptr inbounds i8*, i8** %80, i32 %81
  %82 = load i8*, i8** %arrayidx39, align 4, !tbaa !2
  %83 = load i32, i32* %ptr, align 4, !tbaa !28
  %84 = load i32, i32* %k, align 4, !tbaa !28
  %add = add nsw i32 %83, %84
  %arrayidx40 = getelementptr inbounds i8, i8* %82, i32 %add
  store i8 %conv, i8* %arrayidx40, align 1, !tbaa !24
  br label %for.inc

for.inc:                                          ; preds = %for.body38
  %85 = load i32, i32* %k, align 4, !tbaa !28
  %inc = add nsw i32 %85, 1
  store i32 %inc, i32* %k, align 4, !tbaa !28
  br label %for.cond36

for.end:                                          ; preds = %for.cond36
  br label %for.inc41

for.inc41:                                        ; preds = %for.end
  %86 = load i32, i32* %blkdist, align 4, !tbaa !28
  %87 = load i32, i32* %ptr, align 4, !tbaa !28
  %add42 = add nsw i32 %87, %86
  store i32 %add42, i32* %ptr, align 4, !tbaa !28
  br label %for.cond33

for.end43:                                        ; preds = %for.cond33
  br label %for.inc44

for.inc44:                                        ; preds = %for.end43
  %88 = load i32, i32* %j, align 4, !tbaa !28
  %inc45 = add nsw i32 %88, 1
  store i32 %inc45, i32* %j, align 4, !tbaa !28
  br label %for.cond29

for.end46:                                        ; preds = %for.cond29
  %89 = load i32, i32* %blksize, align 4, !tbaa !28
  store i32 %89, i32* %blkdist, align 4, !tbaa !28
  br label %for.inc47

for.inc47:                                        ; preds = %for.end46
  %90 = load i32, i32* %i, align 4, !tbaa !28
  %inc48 = add nsw i32 %90, 1
  store i32 %inc48, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end49:                                        ; preds = %for.cond
  %91 = load i8**, i8*** %colormap, align 4, !tbaa !2
  %92 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_colormap = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %92, i32 0, i32 1
  store i8** %91, i8*** %sv_colormap, align 4, !tbaa !29
  %93 = load i32, i32* %total_colors, align 4, !tbaa !28
  %94 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_actual = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %94, i32 0, i32 2
  store i32 %93, i32* %sv_actual, align 4, !tbaa !31
  %95 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  %96 = bitcast i32* %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #3
  %97 = bitcast i32* %blkdist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #3
  %98 = bitcast i32* %blksize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #3
  %99 = bitcast i32* %nci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #3
  %100 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #3
  %101 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #3
  %102 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #3
  %103 = bitcast i32* %total_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #3
  %104 = bitcast i8*** %colormap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #3
  %105 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #3
  ret void
}

; Function Attrs: nounwind
define internal void @create_colorindex(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %indexptr = alloca i8*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %nci = alloca i32, align 4
  %blksize = alloca i32, align 4
  %val = alloca i32, align 4
  %pad = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i8** %indexptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %nci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %blksize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %val to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %pad to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dither_mode = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %12, i32 0, i32 21
  %13 = load i32, i32* %dither_mode, align 8, !tbaa !27
  %cmp = icmp eq i32 %13, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 510, i32* %pad, align 4, !tbaa !28
  %14 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %is_padded = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %14, i32 0, i32 4
  store i32 1, i32* %is_padded, align 4, !tbaa !35
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %pad, align 4, !tbaa !28
  %15 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %is_padded2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %15, i32 0, i32 4
  store i32 0, i32* %is_padded2, align 4, !tbaa !35
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 1
  %17 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %17, i32 0, i32 2
  %18 = load i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_sarray, align 4, !tbaa !40
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %20 = bitcast %struct.jpeg_decompress_struct* %19 to %struct.jpeg_common_struct*
  %21 = load i32, i32* %pad, align 4, !tbaa !28
  %add = add nsw i32 256, %21
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 29
  %23 = load i32, i32* %out_color_components, align 8, !tbaa !20
  %call = call i8** %18(%struct.jpeg_common_struct* %20, i32 1, i32 %add, i32 %23)
  %24 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %24, i32 0, i32 3
  store i8** %call, i8*** %colorindex, align 4, !tbaa !41
  %25 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_actual = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %25, i32 0, i32 2
  %26 = load i32, i32* %sv_actual, align 4, !tbaa !31
  store i32 %26, i32* %blksize, align 4, !tbaa !28
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc36, %if.end
  %27 = load i32, i32* %i, align 4, !tbaa !28
  %28 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %28, i32 0, i32 29
  %29 = load i32, i32* %out_color_components3, align 8, !tbaa !20
  %cmp4 = icmp slt i32 %27, %29
  br i1 %cmp4, label %for.body, label %for.end38

for.body:                                         ; preds = %for.cond
  %30 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %Ncolors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %30, i32 0, i32 5
  %31 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %Ncolors, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx, align 4, !tbaa !28
  store i32 %32, i32* %nci, align 4, !tbaa !28
  %33 = load i32, i32* %blksize, align 4, !tbaa !28
  %34 = load i32, i32* %nci, align 4, !tbaa !28
  %div = sdiv i32 %33, %34
  store i32 %div, i32* %blksize, align 4, !tbaa !28
  %35 = load i32, i32* %pad, align 4, !tbaa !28
  %tobool = icmp ne i32 %35, 0
  br i1 %tobool, label %if.then5, label %if.end8

if.then5:                                         ; preds = %for.body
  %36 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex6 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %36, i32 0, i32 3
  %37 = load i8**, i8*** %colorindex6, align 4, !tbaa !41
  %38 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx7 = getelementptr inbounds i8*, i8** %37, i32 %38
  %39 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %39, i32 255
  store i8* %add.ptr, i8** %arrayidx7, align 4, !tbaa !2
  br label %if.end8

if.end8:                                          ; preds = %if.then5, %for.body
  %40 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex9 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %40, i32 0, i32 3
  %41 = load i8**, i8*** %colorindex9, align 4, !tbaa !41
  %42 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  store i8* %43, i8** %indexptr, align 4, !tbaa !2
  store i32 0, i32* %val, align 4, !tbaa !28
  %44 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %45 = load i32, i32* %i, align 4, !tbaa !28
  %46 = load i32, i32* %nci, align 4, !tbaa !28
  %sub = sub nsw i32 %46, 1
  %call11 = call i32 @largest_input_value(%struct.jpeg_decompress_struct* %44, i32 %45, i32 0, i32 %sub)
  store i32 %call11, i32* %k, align 4, !tbaa !28
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %if.end8
  %47 = load i32, i32* %j, align 4, !tbaa !28
  %cmp13 = icmp sle i32 %47, 255
  br i1 %cmp13, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond12
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body14
  %48 = load i32, i32* %j, align 4, !tbaa !28
  %49 = load i32, i32* %k, align 4, !tbaa !28
  %cmp15 = icmp sgt i32 %48, %49
  br i1 %cmp15, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %50 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !28
  %52 = load i32, i32* %val, align 4, !tbaa !28
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %val, align 4, !tbaa !28
  %53 = load i32, i32* %nci, align 4, !tbaa !28
  %sub16 = sub nsw i32 %53, 1
  %call17 = call i32 @largest_input_value(%struct.jpeg_decompress_struct* %50, i32 %51, i32 %inc, i32 %sub16)
  store i32 %call17, i32* %k, align 4, !tbaa !28
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %54 = load i32, i32* %val, align 4, !tbaa !28
  %55 = load i32, i32* %blksize, align 4, !tbaa !28
  %mul = mul nsw i32 %54, %55
  %conv = trunc i32 %mul to i8
  %56 = load i8*, i8** %indexptr, align 4, !tbaa !2
  %57 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx18 = getelementptr inbounds i8, i8* %56, i32 %57
  store i8 %conv, i8* %arrayidx18, align 1, !tbaa !24
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %58 = load i32, i32* %j, align 4, !tbaa !28
  %inc19 = add nsw i32 %58, 1
  store i32 %inc19, i32* %j, align 4, !tbaa !28
  br label %for.cond12

for.end:                                          ; preds = %for.cond12
  %59 = load i32, i32* %pad, align 4, !tbaa !28
  %tobool20 = icmp ne i32 %59, 0
  br i1 %tobool20, label %if.then21, label %if.end35

if.then21:                                        ; preds = %for.end
  store i32 1, i32* %j, align 4, !tbaa !28
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc32, %if.then21
  %60 = load i32, i32* %j, align 4, !tbaa !28
  %cmp23 = icmp sle i32 %60, 255
  br i1 %cmp23, label %for.body25, label %for.end34

for.body25:                                       ; preds = %for.cond22
  %61 = load i8*, i8** %indexptr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i8, i8* %61, i32 0
  %62 = load i8, i8* %arrayidx26, align 1, !tbaa !24
  %63 = load i8*, i8** %indexptr, align 4, !tbaa !2
  %64 = load i32, i32* %j, align 4, !tbaa !28
  %sub27 = sub nsw i32 0, %64
  %arrayidx28 = getelementptr inbounds i8, i8* %63, i32 %sub27
  store i8 %62, i8* %arrayidx28, align 1, !tbaa !24
  %65 = load i8*, i8** %indexptr, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds i8, i8* %65, i32 255
  %66 = load i8, i8* %arrayidx29, align 1, !tbaa !24
  %67 = load i8*, i8** %indexptr, align 4, !tbaa !2
  %68 = load i32, i32* %j, align 4, !tbaa !28
  %add30 = add nsw i32 255, %68
  %arrayidx31 = getelementptr inbounds i8, i8* %67, i32 %add30
  store i8 %66, i8* %arrayidx31, align 1, !tbaa !24
  br label %for.inc32

for.inc32:                                        ; preds = %for.body25
  %69 = load i32, i32* %j, align 4, !tbaa !28
  %inc33 = add nsw i32 %69, 1
  store i32 %inc33, i32* %j, align 4, !tbaa !28
  br label %for.cond22

for.end34:                                        ; preds = %for.cond22
  br label %if.end35

if.end35:                                         ; preds = %for.end34, %for.end
  br label %for.inc36

for.inc36:                                        ; preds = %if.end35
  %70 = load i32, i32* %i, align 4, !tbaa !28
  %inc37 = add nsw i32 %70, 1
  store i32 %inc37, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end38:                                        ; preds = %for.cond
  %71 = bitcast i32* %pad to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #3
  %72 = bitcast i32* %val to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast i32* %blksize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  %74 = bitcast i32* %nci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %75 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  %77 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast i8** %indexptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  %79 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  ret void
}

; Function Attrs: nounwind
define internal void @alloc_fs_workspace(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %arraysize = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i32* %arraysize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %6, i32 0, i32 27
  %7 = load i32, i32* %output_width, align 8, !tbaa !37
  %add = add i32 %7, 2
  %mul = mul i32 %add, 2
  store i32 %mul, i32* %arraysize, align 4, !tbaa !38
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4, !tbaa !28
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 29
  %10 = load i32, i32* %out_color_components, align 8, !tbaa !20
  %cmp = icmp slt i32 %8, %10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 1
  %12 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_large = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %12, i32 0, i32 1
  %13 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_large, align 4, !tbaa !42
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = bitcast %struct.jpeg_decompress_struct* %14 to %struct.jpeg_common_struct*
  %16 = load i32, i32* %arraysize, align 4, !tbaa !38
  %call = call i8* %13(%struct.jpeg_common_struct* %15, i32 1, i32 %16)
  %17 = bitcast i8* %call to i16*
  %18 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %18, i32 0, i32 8
  %19 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [4 x i16*], [4 x i16*]* %fserrors, i32 0, i32 %19
  store i16* %17, i16** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4, !tbaa !28
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #3
  %22 = bitcast i32* %arraysize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #3
  %23 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @color_quantize3(%struct.jpeg_decompress_struct* %cinfo, i8** %input_buf, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %pixcode = alloca i32, align 4
  %ptrin = alloca i8*, align 4
  %ptrout = alloca i8*, align 4
  %colorindex0 = alloca i8*, align 4
  %colorindex1 = alloca i8*, align 4
  %colorindex24 = alloca i8*, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !28
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i8** %ptrin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i8** %ptrout to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i8** %colorindex0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %8, i32 0, i32 3
  %9 = load i8**, i8*** %colorindex, align 4, !tbaa !41
  %arrayidx = getelementptr inbounds i8*, i8** %9, i32 0
  %10 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %10, i8** %colorindex0, align 4, !tbaa !2
  %11 = bitcast i8** %colorindex1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %12, i32 0, i32 3
  %13 = load i8**, i8*** %colorindex2, align 4, !tbaa !41
  %arrayidx3 = getelementptr inbounds i8*, i8** %13, i32 1
  %14 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %14, i8** %colorindex1, align 4, !tbaa !2
  %15 = bitcast i8** %colorindex24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex5 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %16, i32 0, i32 3
  %17 = load i8**, i8*** %colorindex5, align 4, !tbaa !41
  %arrayidx6 = getelementptr inbounds i8*, i8** %17, i32 2
  %18 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %18, i8** %colorindex24, align 4, !tbaa !2
  %19 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %22, i32 0, i32 27
  %23 = load i32, i32* %output_width, align 8, !tbaa !37
  store i32 %23, i32* %width, align 4, !tbaa !28
  store i32 0, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %entry
  %24 = load i32, i32* %row, align 4, !tbaa !28
  %25 = load i32, i32* %num_rows.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %24, %25
  br i1 %cmp, label %for.body, label %for.end26

for.body:                                         ; preds = %for.cond
  %26 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %27 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx7 = getelementptr inbounds i8*, i8** %26, i32 %27
  %28 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %28, i8** %ptrin, align 4, !tbaa !2
  %29 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %30 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx8 = getelementptr inbounds i8*, i8** %29, i32 %30
  %31 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %31, i8** %ptrout, align 4, !tbaa !2
  %32 = load i32, i32* %width, align 4, !tbaa !28
  store i32 %32, i32* %col, align 4, !tbaa !28
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %for.body
  %33 = load i32, i32* %col, align 4, !tbaa !28
  %cmp10 = icmp ugt i32 %33, 0
  br i1 %cmp10, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond9
  %34 = load i8*, i8** %colorindex0, align 4, !tbaa !2
  %35 = load i8*, i8** %ptrin, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %35, i32 1
  store i8* %incdec.ptr, i8** %ptrin, align 4, !tbaa !2
  %36 = load i8, i8* %35, align 1, !tbaa !24
  %conv = zext i8 %36 to i32
  %arrayidx12 = getelementptr inbounds i8, i8* %34, i32 %conv
  %37 = load i8, i8* %arrayidx12, align 1, !tbaa !24
  %conv13 = zext i8 %37 to i32
  store i32 %conv13, i32* %pixcode, align 4, !tbaa !28
  %38 = load i8*, i8** %colorindex1, align 4, !tbaa !2
  %39 = load i8*, i8** %ptrin, align 4, !tbaa !2
  %incdec.ptr14 = getelementptr inbounds i8, i8* %39, i32 1
  store i8* %incdec.ptr14, i8** %ptrin, align 4, !tbaa !2
  %40 = load i8, i8* %39, align 1, !tbaa !24
  %conv15 = zext i8 %40 to i32
  %arrayidx16 = getelementptr inbounds i8, i8* %38, i32 %conv15
  %41 = load i8, i8* %arrayidx16, align 1, !tbaa !24
  %conv17 = zext i8 %41 to i32
  %42 = load i32, i32* %pixcode, align 4, !tbaa !28
  %add = add nsw i32 %42, %conv17
  store i32 %add, i32* %pixcode, align 4, !tbaa !28
  %43 = load i8*, i8** %colorindex24, align 4, !tbaa !2
  %44 = load i8*, i8** %ptrin, align 4, !tbaa !2
  %incdec.ptr18 = getelementptr inbounds i8, i8* %44, i32 1
  store i8* %incdec.ptr18, i8** %ptrin, align 4, !tbaa !2
  %45 = load i8, i8* %44, align 1, !tbaa !24
  %conv19 = zext i8 %45 to i32
  %arrayidx20 = getelementptr inbounds i8, i8* %43, i32 %conv19
  %46 = load i8, i8* %arrayidx20, align 1, !tbaa !24
  %conv21 = zext i8 %46 to i32
  %47 = load i32, i32* %pixcode, align 4, !tbaa !28
  %add22 = add nsw i32 %47, %conv21
  store i32 %add22, i32* %pixcode, align 4, !tbaa !28
  %48 = load i32, i32* %pixcode, align 4, !tbaa !28
  %conv23 = trunc i32 %48 to i8
  %49 = load i8*, i8** %ptrout, align 4, !tbaa !2
  %incdec.ptr24 = getelementptr inbounds i8, i8* %49, i32 1
  store i8* %incdec.ptr24, i8** %ptrout, align 4, !tbaa !2
  store i8 %conv23, i8* %49, align 1, !tbaa !24
  br label %for.inc

for.inc:                                          ; preds = %for.body11
  %50 = load i32, i32* %col, align 4, !tbaa !28
  %dec = add i32 %50, -1
  store i32 %dec, i32* %col, align 4, !tbaa !28
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  br label %for.inc25

for.inc25:                                        ; preds = %for.end
  %51 = load i32, i32* %row, align 4, !tbaa !28
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.end26:                                        ; preds = %for.cond
  %52 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  %53 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #3
  %54 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #3
  %55 = bitcast i8** %colorindex24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #3
  %56 = bitcast i8** %colorindex1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #3
  %57 = bitcast i8** %colorindex0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #3
  %58 = bitcast i8** %ptrout to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #3
  %59 = bitcast i8** %ptrin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  %60 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #3
  %61 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #3
  ret void
}

; Function Attrs: nounwind
define internal void @color_quantize(%struct.jpeg_decompress_struct* %cinfo, i8** %input_buf, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %colorindex = alloca i8**, align 4
  %pixcode = alloca i32, align 4
  %ci = alloca i32, align 4
  %ptrin = alloca i8*, align 4
  %ptrout = alloca i8*, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  %nc = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !28
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i8*** %colorindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %5, i32 0, i32 3
  %6 = load i8**, i8*** %colorindex2, align 4, !tbaa !41
  store i8** %6, i8*** %colorindex, align 4, !tbaa !2
  %7 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i8** %ptrin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i8** %ptrout to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 27
  %15 = load i32, i32* %output_width, align 8, !tbaa !37
  store i32 %15, i32* %width, align 4, !tbaa !28
  %16 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 29
  %18 = load i32, i32* %out_color_components, align 8, !tbaa !20
  store i32 %18, i32* %nc, align 4, !tbaa !28
  store i32 0, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %entry
  %19 = load i32, i32* %row, align 4, !tbaa !28
  %20 = load i32, i32* %num_rows.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %19, %20
  br i1 %cmp, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  %21 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %22 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds i8*, i8** %21, i32 %22
  %23 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %23, i8** %ptrin, align 4, !tbaa !2
  %24 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %25 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx3 = getelementptr inbounds i8*, i8** %24, i32 %25
  %26 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %26, i8** %ptrout, align 4, !tbaa !2
  %27 = load i32, i32* %width, align 4, !tbaa !28
  store i32 %27, i32* %col, align 4, !tbaa !28
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc15, %for.body
  %28 = load i32, i32* %col, align 4, !tbaa !28
  %cmp5 = icmp ugt i32 %28, 0
  br i1 %cmp5, label %for.body6, label %for.end16

for.body6:                                        ; preds = %for.cond4
  store i32 0, i32* %pixcode, align 4, !tbaa !28
  store i32 0, i32* %ci, align 4, !tbaa !28
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body6
  %29 = load i32, i32* %ci, align 4, !tbaa !28
  %30 = load i32, i32* %nc, align 4, !tbaa !28
  %cmp8 = icmp slt i32 %29, %30
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %31 = load i8**, i8*** %colorindex, align 4, !tbaa !2
  %32 = load i32, i32* %ci, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx10, align 4, !tbaa !2
  %34 = load i8*, i8** %ptrin, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %34, i32 1
  store i8* %incdec.ptr, i8** %ptrin, align 4, !tbaa !2
  %35 = load i8, i8* %34, align 1, !tbaa !24
  %conv = zext i8 %35 to i32
  %arrayidx11 = getelementptr inbounds i8, i8* %33, i32 %conv
  %36 = load i8, i8* %arrayidx11, align 1, !tbaa !24
  %conv12 = zext i8 %36 to i32
  %37 = load i32, i32* %pixcode, align 4, !tbaa !28
  %add = add nsw i32 %37, %conv12
  store i32 %add, i32* %pixcode, align 4, !tbaa !28
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %38 = load i32, i32* %ci, align 4, !tbaa !28
  %inc = add nsw i32 %38, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !28
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  %39 = load i32, i32* %pixcode, align 4, !tbaa !28
  %conv13 = trunc i32 %39 to i8
  %40 = load i8*, i8** %ptrout, align 4, !tbaa !2
  %incdec.ptr14 = getelementptr inbounds i8, i8* %40, i32 1
  store i8* %incdec.ptr14, i8** %ptrout, align 4, !tbaa !2
  store i8 %conv13, i8* %40, align 1, !tbaa !24
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %41 = load i32, i32* %col, align 4, !tbaa !28
  %dec = add i32 %41, -1
  store i32 %dec, i32* %col, align 4, !tbaa !28
  br label %for.cond4

for.end16:                                        ; preds = %for.cond4
  br label %for.inc17

for.inc17:                                        ; preds = %for.end16
  %42 = load i32, i32* %row, align 4, !tbaa !28
  %inc18 = add nsw i32 %42, 1
  store i32 %inc18, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  %43 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #3
  %44 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #3
  %45 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #3
  %46 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #3
  %47 = bitcast i8** %ptrout to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #3
  %48 = bitcast i8** %ptrin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #3
  %49 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %50 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast i8*** %colorindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  %52 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #3
  ret void
}

; Function Attrs: nounwind
define internal void @quantize3_ord_dither(%struct.jpeg_decompress_struct* %cinfo, i8** %input_buf, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %pixcode = alloca i32, align 4
  %input_ptr = alloca i8*, align 4
  %output_ptr = alloca i8*, align 4
  %colorindex0 = alloca i8*, align 4
  %colorindex1 = alloca i8*, align 4
  %colorindex24 = alloca i8*, align 4
  %dither0 = alloca i32*, align 4
  %dither1 = alloca i32*, align 4
  %dither2 = alloca i32*, align 4
  %row_index = alloca i32, align 4
  %col_index = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !28
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i8** %input_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i8** %output_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i8** %colorindex0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %8, i32 0, i32 3
  %9 = load i8**, i8*** %colorindex, align 4, !tbaa !41
  %arrayidx = getelementptr inbounds i8*, i8** %9, i32 0
  %10 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  store i8* %10, i8** %colorindex0, align 4, !tbaa !2
  %11 = bitcast i8** %colorindex1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %12, i32 0, i32 3
  %13 = load i8**, i8*** %colorindex2, align 4, !tbaa !41
  %arrayidx3 = getelementptr inbounds i8*, i8** %13, i32 1
  %14 = load i8*, i8** %arrayidx3, align 4, !tbaa !2
  store i8* %14, i8** %colorindex1, align 4, !tbaa !2
  %15 = bitcast i8** %colorindex24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex5 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %16, i32 0, i32 3
  %17 = load i8**, i8*** %colorindex5, align 4, !tbaa !41
  %arrayidx6 = getelementptr inbounds i8*, i8** %17, i32 2
  %18 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %18, i8** %colorindex24, align 4, !tbaa !2
  %19 = bitcast i32** %dither0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32** %dither1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i32** %dither2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = bitcast i32* %row_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = bitcast i32* %col_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #3
  %25 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #3
  %26 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 27
  %28 = load i32, i32* %output_width, align 8, !tbaa !37
  store i32 %28, i32* %width, align 4, !tbaa !28
  store i32 0, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc46, %entry
  %29 = load i32, i32* %row, align 4, !tbaa !28
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %29, %30
  br i1 %cmp, label %for.body, label %for.end47

for.body:                                         ; preds = %for.cond
  %31 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %row_index7 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %31, i32 0, i32 6
  %32 = load i32, i32* %row_index7, align 4, !tbaa !34
  store i32 %32, i32* %row_index, align 4, !tbaa !28
  %33 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %34 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx8 = getelementptr inbounds i8*, i8** %33, i32 %34
  %35 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %35, i8** %input_ptr, align 4, !tbaa !2
  %36 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %37 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx9 = getelementptr inbounds i8*, i8** %36, i32 %37
  %38 = load i8*, i8** %arrayidx9, align 4, !tbaa !2
  store i8* %38, i8** %output_ptr, align 4, !tbaa !2
  %39 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %odither = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %39, i32 0, i32 7
  %arrayidx10 = getelementptr inbounds [4 x [16 x i32]*], [4 x [16 x i32]*]* %odither, i32 0, i32 0
  %40 = load [16 x i32]*, [16 x i32]** %arrayidx10, align 4, !tbaa !2
  %41 = load i32, i32* %row_index, align 4, !tbaa !28
  %arrayidx11 = getelementptr inbounds [16 x i32], [16 x i32]* %40, i32 %41
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx11, i32 0, i32 0
  store i32* %arraydecay, i32** %dither0, align 4, !tbaa !2
  %42 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %odither12 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %42, i32 0, i32 7
  %arrayidx13 = getelementptr inbounds [4 x [16 x i32]*], [4 x [16 x i32]*]* %odither12, i32 0, i32 1
  %43 = load [16 x i32]*, [16 x i32]** %arrayidx13, align 4, !tbaa !2
  %44 = load i32, i32* %row_index, align 4, !tbaa !28
  %arrayidx14 = getelementptr inbounds [16 x i32], [16 x i32]* %43, i32 %44
  %arraydecay15 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx14, i32 0, i32 0
  store i32* %arraydecay15, i32** %dither1, align 4, !tbaa !2
  %45 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %odither16 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %45, i32 0, i32 7
  %arrayidx17 = getelementptr inbounds [4 x [16 x i32]*], [4 x [16 x i32]*]* %odither16, i32 0, i32 2
  %46 = load [16 x i32]*, [16 x i32]** %arrayidx17, align 4, !tbaa !2
  %47 = load i32, i32* %row_index, align 4, !tbaa !28
  %arrayidx18 = getelementptr inbounds [16 x i32], [16 x i32]* %46, i32 %47
  %arraydecay19 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx18, i32 0, i32 0
  store i32* %arraydecay19, i32** %dither2, align 4, !tbaa !2
  store i32 0, i32* %col_index, align 4, !tbaa !28
  %48 = load i32, i32* %width, align 4, !tbaa !28
  store i32 %48, i32* %col, align 4, !tbaa !28
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc, %for.body
  %49 = load i32, i32* %col, align 4, !tbaa !28
  %cmp21 = icmp ugt i32 %49, 0
  br i1 %cmp21, label %for.body22, label %for.end

for.body22:                                       ; preds = %for.cond20
  %50 = load i8*, i8** %colorindex0, align 4, !tbaa !2
  %51 = load i8*, i8** %input_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %51, i32 1
  store i8* %incdec.ptr, i8** %input_ptr, align 4, !tbaa !2
  %52 = load i8, i8* %51, align 1, !tbaa !24
  %conv = zext i8 %52 to i32
  %53 = load i32*, i32** %dither0, align 4, !tbaa !2
  %54 = load i32, i32* %col_index, align 4, !tbaa !28
  %arrayidx23 = getelementptr inbounds i32, i32* %53, i32 %54
  %55 = load i32, i32* %arrayidx23, align 4, !tbaa !28
  %add = add nsw i32 %conv, %55
  %arrayidx24 = getelementptr inbounds i8, i8* %50, i32 %add
  %56 = load i8, i8* %arrayidx24, align 1, !tbaa !24
  %conv25 = zext i8 %56 to i32
  store i32 %conv25, i32* %pixcode, align 4, !tbaa !28
  %57 = load i8*, i8** %colorindex1, align 4, !tbaa !2
  %58 = load i8*, i8** %input_ptr, align 4, !tbaa !2
  %incdec.ptr26 = getelementptr inbounds i8, i8* %58, i32 1
  store i8* %incdec.ptr26, i8** %input_ptr, align 4, !tbaa !2
  %59 = load i8, i8* %58, align 1, !tbaa !24
  %conv27 = zext i8 %59 to i32
  %60 = load i32*, i32** %dither1, align 4, !tbaa !2
  %61 = load i32, i32* %col_index, align 4, !tbaa !28
  %arrayidx28 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx28, align 4, !tbaa !28
  %add29 = add nsw i32 %conv27, %62
  %arrayidx30 = getelementptr inbounds i8, i8* %57, i32 %add29
  %63 = load i8, i8* %arrayidx30, align 1, !tbaa !24
  %conv31 = zext i8 %63 to i32
  %64 = load i32, i32* %pixcode, align 4, !tbaa !28
  %add32 = add nsw i32 %64, %conv31
  store i32 %add32, i32* %pixcode, align 4, !tbaa !28
  %65 = load i8*, i8** %colorindex24, align 4, !tbaa !2
  %66 = load i8*, i8** %input_ptr, align 4, !tbaa !2
  %incdec.ptr33 = getelementptr inbounds i8, i8* %66, i32 1
  store i8* %incdec.ptr33, i8** %input_ptr, align 4, !tbaa !2
  %67 = load i8, i8* %66, align 1, !tbaa !24
  %conv34 = zext i8 %67 to i32
  %68 = load i32*, i32** %dither2, align 4, !tbaa !2
  %69 = load i32, i32* %col_index, align 4, !tbaa !28
  %arrayidx35 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx35, align 4, !tbaa !28
  %add36 = add nsw i32 %conv34, %70
  %arrayidx37 = getelementptr inbounds i8, i8* %65, i32 %add36
  %71 = load i8, i8* %arrayidx37, align 1, !tbaa !24
  %conv38 = zext i8 %71 to i32
  %72 = load i32, i32* %pixcode, align 4, !tbaa !28
  %add39 = add nsw i32 %72, %conv38
  store i32 %add39, i32* %pixcode, align 4, !tbaa !28
  %73 = load i32, i32* %pixcode, align 4, !tbaa !28
  %conv40 = trunc i32 %73 to i8
  %74 = load i8*, i8** %output_ptr, align 4, !tbaa !2
  %incdec.ptr41 = getelementptr inbounds i8, i8* %74, i32 1
  store i8* %incdec.ptr41, i8** %output_ptr, align 4, !tbaa !2
  store i8 %conv40, i8* %74, align 1, !tbaa !24
  %75 = load i32, i32* %col_index, align 4, !tbaa !28
  %add42 = add nsw i32 %75, 1
  %and = and i32 %add42, 15
  store i32 %and, i32* %col_index, align 4, !tbaa !28
  br label %for.inc

for.inc:                                          ; preds = %for.body22
  %76 = load i32, i32* %col, align 4, !tbaa !28
  %dec = add i32 %76, -1
  store i32 %dec, i32* %col, align 4, !tbaa !28
  br label %for.cond20

for.end:                                          ; preds = %for.cond20
  %77 = load i32, i32* %row_index, align 4, !tbaa !28
  %add43 = add nsw i32 %77, 1
  %and44 = and i32 %add43, 15
  store i32 %and44, i32* %row_index, align 4, !tbaa !28
  %78 = load i32, i32* %row_index, align 4, !tbaa !28
  %79 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %row_index45 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %79, i32 0, i32 6
  store i32 %78, i32* %row_index45, align 4, !tbaa !34
  br label %for.inc46

for.inc46:                                        ; preds = %for.end
  %80 = load i32, i32* %row, align 4, !tbaa !28
  %inc = add nsw i32 %80, 1
  store i32 %inc, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.end47:                                        ; preds = %for.cond
  %81 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #3
  %82 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #3
  %83 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #3
  %84 = bitcast i32* %col_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #3
  %85 = bitcast i32* %row_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #3
  %86 = bitcast i32** %dither2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #3
  %87 = bitcast i32** %dither1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #3
  %88 = bitcast i32** %dither0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #3
  %89 = bitcast i8** %colorindex24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #3
  %90 = bitcast i8** %colorindex1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #3
  %91 = bitcast i8** %colorindex0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #3
  %92 = bitcast i8** %output_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #3
  %93 = bitcast i8** %input_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #3
  %94 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #3
  %95 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #3
  ret void
}

; Function Attrs: nounwind
define internal void @quantize_ord_dither(%struct.jpeg_decompress_struct* %cinfo, i8** %input_buf, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %input_ptr = alloca i8*, align 4
  %output_ptr = alloca i8*, align 4
  %colorindex_ci = alloca i8*, align 4
  %dither = alloca i32*, align 4
  %row_index = alloca i32, align 4
  %col_index = alloca i32, align 4
  %nc = alloca i32, align 4
  %ci = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !28
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i8** %input_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i8** %output_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i8** %colorindex_ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32** %dither to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %row_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %col_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 29
  %12 = load i32, i32* %out_color_components, align 8, !tbaa !20
  store i32 %12, i32* %nc, align 4, !tbaa !28
  %13 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #3
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 27
  %18 = load i32, i32* %output_width, align 8, !tbaa !37
  store i32 %18, i32* %width, align 4, !tbaa !28
  store i32 0, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc27, %entry
  %19 = load i32, i32* %row, align 4, !tbaa !28
  %20 = load i32, i32* %num_rows.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %19, %20
  br i1 %cmp, label %for.body, label %for.end29

for.body:                                         ; preds = %for.cond
  %21 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %22 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds i8*, i8** %21, i32 %22
  %23 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %24 = load i32, i32* %width, align 4, !tbaa !28
  %mul = mul i32 %24, 1
  call void @jzero_far(i8* %23, i32 %mul)
  %25 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %row_index2 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %25, i32 0, i32 6
  %26 = load i32, i32* %row_index2, align 4, !tbaa !34
  store i32 %26, i32* %row_index, align 4, !tbaa !28
  store i32 0, i32* %ci, align 4, !tbaa !28
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc22, %for.body
  %27 = load i32, i32* %ci, align 4, !tbaa !28
  %28 = load i32, i32* %nc, align 4, !tbaa !28
  %cmp4 = icmp slt i32 %27, %28
  br i1 %cmp4, label %for.body5, label %for.end23

for.body5:                                        ; preds = %for.cond3
  %29 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %30 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds i8*, i8** %29, i32 %30
  %31 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  %32 = load i32, i32* %ci, align 4, !tbaa !28
  %add.ptr = getelementptr inbounds i8, i8* %31, i32 %32
  store i8* %add.ptr, i8** %input_ptr, align 4, !tbaa !2
  %33 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %34 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx7 = getelementptr inbounds i8*, i8** %33, i32 %34
  %35 = load i8*, i8** %arrayidx7, align 4, !tbaa !2
  store i8* %35, i8** %output_ptr, align 4, !tbaa !2
  %36 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %36, i32 0, i32 3
  %37 = load i8**, i8*** %colorindex, align 4, !tbaa !41
  %38 = load i32, i32* %ci, align 4, !tbaa !28
  %arrayidx8 = getelementptr inbounds i8*, i8** %37, i32 %38
  %39 = load i8*, i8** %arrayidx8, align 4, !tbaa !2
  store i8* %39, i8** %colorindex_ci, align 4, !tbaa !2
  %40 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %odither = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %40, i32 0, i32 7
  %41 = load i32, i32* %ci, align 4, !tbaa !28
  %arrayidx9 = getelementptr inbounds [4 x [16 x i32]*], [4 x [16 x i32]*]* %odither, i32 0, i32 %41
  %42 = load [16 x i32]*, [16 x i32]** %arrayidx9, align 4, !tbaa !2
  %43 = load i32, i32* %row_index, align 4, !tbaa !28
  %arrayidx10 = getelementptr inbounds [16 x i32], [16 x i32]* %42, i32 %43
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx10, i32 0, i32 0
  store i32* %arraydecay, i32** %dither, align 4, !tbaa !2
  store i32 0, i32* %col_index, align 4, !tbaa !28
  %44 = load i32, i32* %width, align 4, !tbaa !28
  store i32 %44, i32* %col, align 4, !tbaa !28
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc, %for.body5
  %45 = load i32, i32* %col, align 4, !tbaa !28
  %cmp12 = icmp ugt i32 %45, 0
  br i1 %cmp12, label %for.body13, label %for.end

for.body13:                                       ; preds = %for.cond11
  %46 = load i8*, i8** %colorindex_ci, align 4, !tbaa !2
  %47 = load i8*, i8** %input_ptr, align 4, !tbaa !2
  %48 = load i8, i8* %47, align 1, !tbaa !24
  %conv = zext i8 %48 to i32
  %49 = load i32*, i32** %dither, align 4, !tbaa !2
  %50 = load i32, i32* %col_index, align 4, !tbaa !28
  %arrayidx14 = getelementptr inbounds i32, i32* %49, i32 %50
  %51 = load i32, i32* %arrayidx14, align 4, !tbaa !28
  %add = add nsw i32 %conv, %51
  %arrayidx15 = getelementptr inbounds i8, i8* %46, i32 %add
  %52 = load i8, i8* %arrayidx15, align 1, !tbaa !24
  %conv16 = zext i8 %52 to i32
  %53 = load i8*, i8** %output_ptr, align 4, !tbaa !2
  %54 = load i8, i8* %53, align 1, !tbaa !24
  %conv17 = zext i8 %54 to i32
  %add18 = add nsw i32 %conv17, %conv16
  %conv19 = trunc i32 %add18 to i8
  store i8 %conv19, i8* %53, align 1, !tbaa !24
  %55 = load i32, i32* %nc, align 4, !tbaa !28
  %56 = load i8*, i8** %input_ptr, align 4, !tbaa !2
  %add.ptr20 = getelementptr inbounds i8, i8* %56, i32 %55
  store i8* %add.ptr20, i8** %input_ptr, align 4, !tbaa !2
  %57 = load i8*, i8** %output_ptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i8, i8* %57, i32 1
  store i8* %incdec.ptr, i8** %output_ptr, align 4, !tbaa !2
  %58 = load i32, i32* %col_index, align 4, !tbaa !28
  %add21 = add nsw i32 %58, 1
  %and = and i32 %add21, 15
  store i32 %and, i32* %col_index, align 4, !tbaa !28
  br label %for.inc

for.inc:                                          ; preds = %for.body13
  %59 = load i32, i32* %col, align 4, !tbaa !28
  %dec = add i32 %59, -1
  store i32 %dec, i32* %col, align 4, !tbaa !28
  br label %for.cond11

for.end:                                          ; preds = %for.cond11
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %60 = load i32, i32* %ci, align 4, !tbaa !28
  %inc = add nsw i32 %60, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !28
  br label %for.cond3

for.end23:                                        ; preds = %for.cond3
  %61 = load i32, i32* %row_index, align 4, !tbaa !28
  %add24 = add nsw i32 %61, 1
  %and25 = and i32 %add24, 15
  store i32 %and25, i32* %row_index, align 4, !tbaa !28
  %62 = load i32, i32* %row_index, align 4, !tbaa !28
  %63 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %row_index26 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %63, i32 0, i32 6
  store i32 %62, i32* %row_index26, align 4, !tbaa !34
  br label %for.inc27

for.inc27:                                        ; preds = %for.end23
  %64 = load i32, i32* %row, align 4, !tbaa !28
  %inc28 = add nsw i32 %64, 1
  store i32 %inc28, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.end29:                                        ; preds = %for.cond
  %65 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #3
  %66 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #3
  %67 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #3
  %68 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #3
  %69 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #3
  %70 = bitcast i32* %col_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  %71 = bitcast i32* %row_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #3
  %72 = bitcast i32** %dither to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast i8** %colorindex_ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  %74 = bitcast i8** %output_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #3
  %75 = bitcast i8** %input_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #3
  %76 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #3
  ret void
}

; Function Attrs: nounwind
define internal void @create_odither_tables(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %odither = alloca [16 x i32]*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %nci = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast [16 x i32]** %odither to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %nci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %8 = load i32, i32* %i, align 4, !tbaa !28
  %9 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %9, i32 0, i32 29
  %10 = load i32, i32* %out_color_components, align 8, !tbaa !20
  %cmp = icmp slt i32 %8, %10
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  %11 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %Ncolors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %11, i32 0, i32 5
  %12 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %Ncolors, i32 0, i32 %12
  %13 = load i32, i32* %arrayidx, align 4, !tbaa !28
  store i32 %13, i32* %nci, align 4, !tbaa !28
  store [16 x i32]* null, [16 x i32]** %odither, align 4, !tbaa !2
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %14 = load i32, i32* %j, align 4, !tbaa !28
  %15 = load i32, i32* %i, align 4, !tbaa !28
  %cmp3 = icmp slt i32 %14, %15
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %16 = load i32, i32* %nci, align 4, !tbaa !28
  %17 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %Ncolors5 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %17, i32 0, i32 5
  %18 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds [4 x i32], [4 x i32]* %Ncolors5, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx6, align 4, !tbaa !28
  %cmp7 = icmp eq i32 %16, %19
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  %20 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %odither8 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %20, i32 0, i32 7
  %21 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx9 = getelementptr inbounds [4 x [16 x i32]*], [4 x [16 x i32]*]* %odither8, i32 0, i32 %21
  %22 = load [16 x i32]*, [16 x i32]** %arrayidx9, align 4, !tbaa !2
  store [16 x i32]* %22, [16 x i32]** %odither, align 4, !tbaa !2
  br label %for.end

if.end:                                           ; preds = %for.body4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %j, align 4, !tbaa !28
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %j, align 4, !tbaa !28
  br label %for.cond2

for.end:                                          ; preds = %if.then, %for.cond2
  %24 = load [16 x i32]*, [16 x i32]** %odither, align 4, !tbaa !2
  %cmp10 = icmp eq [16 x i32]* %24, null
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %for.end
  %25 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %26 = load i32, i32* %nci, align 4, !tbaa !28
  %call = call [16 x i32]* @make_odither_array(%struct.jpeg_decompress_struct* %25, i32 %26)
  store [16 x i32]* %call, [16 x i32]** %odither, align 4, !tbaa !2
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %for.end
  %27 = load [16 x i32]*, [16 x i32]** %odither, align 4, !tbaa !2
  %28 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %odither13 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %28, i32 0, i32 7
  %29 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx14 = getelementptr inbounds [4 x [16 x i32]*], [4 x [16 x i32]*]* %odither13, i32 0, i32 %29
  store [16 x i32]* %27, [16 x i32]** %arrayidx14, align 4, !tbaa !2
  br label %for.inc15

for.inc15:                                        ; preds = %if.end12
  %30 = load i32, i32* %i, align 4, !tbaa !28
  %inc16 = add nsw i32 %30, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  %31 = bitcast i32* %nci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  %32 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #3
  %34 = bitcast [16 x i32]** %odither to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #3
  %35 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #3
  ret void
}

; Function Attrs: nounwind
define internal void @quantize_fs_dither(%struct.jpeg_decompress_struct* %cinfo, i8** %input_buf, i8** %output_buf, i32 %num_rows) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %output_buf.addr = alloca i8**, align 4
  %num_rows.addr = alloca i32, align 4
  %cquantize = alloca %struct.my_cquantizer*, align 4
  %cur = alloca i32, align 4
  %belowerr = alloca i32, align 4
  %bpreverr = alloca i32, align 4
  %bnexterr = alloca i32, align 4
  %delta = alloca i32, align 4
  %errorptr = alloca i16*, align 4
  %input_ptr = alloca i8*, align 4
  %output_ptr = alloca i8*, align 4
  %colorindex_ci = alloca i8*, align 4
  %colormap_ci = alloca i8*, align 4
  %pixcode = alloca i32, align 4
  %nc = alloca i32, align 4
  %dir = alloca i32, align 4
  %dirnc = alloca i32, align 4
  %ci = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  %width = alloca i32, align 4
  %range_limit = alloca i8*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i8** %output_buf, i8*** %output_buf.addr, align 4, !tbaa !2
  store i32 %num_rows, i32* %num_rows.addr, align 4, !tbaa !28
  %0 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %cquantize1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 87
  %2 = load %struct.jpeg_color_quantizer*, %struct.jpeg_color_quantizer** %cquantize1, align 4, !tbaa !14
  %3 = bitcast %struct.jpeg_color_quantizer* %2 to %struct.my_cquantizer*
  store %struct.my_cquantizer* %3, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %4 = bitcast i32* %cur to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %belowerr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %bpreverr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %bnexterr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i16** %errorptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i8** %input_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i8** %output_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast i8** %colorindex_ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #3
  %13 = bitcast i8** %colormap_ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #3
  %14 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #3
  %15 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #3
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 29
  %17 = load i32, i32* %out_color_components, align 8, !tbaa !20
  store i32 %17, i32* %nc, align 4, !tbaa !28
  %18 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #3
  %19 = bitcast i32* %dirnc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #3
  %20 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #3
  %21 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #3
  %22 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #3
  %23 = bitcast i32* %width to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #3
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %output_width = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %24, i32 0, i32 27
  %25 = load i32, i32* %output_width, align 8, !tbaa !37
  store i32 %25, i32* %width, align 4, !tbaa !28
  %26 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #3
  %27 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %sample_range_limit = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %27, i32 0, i32 65
  %28 = load i8*, i8** %sample_range_limit, align 4, !tbaa !43
  store i8* %28, i8** %range_limit, align 4, !tbaa !2
  store i32 0, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc56, %entry
  %29 = load i32, i32* %row, align 4, !tbaa !28
  %30 = load i32, i32* %num_rows.addr, align 4, !tbaa !28
  %cmp = icmp slt i32 %29, %30
  br i1 %cmp, label %for.body, label %for.end58

for.body:                                         ; preds = %for.cond
  %31 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %32 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds i8*, i8** %31, i32 %32
  %33 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %34 = load i32, i32* %width, align 4, !tbaa !28
  %mul = mul i32 %34, 1
  call void @jzero_far(i8* %33, i32 %mul)
  store i32 0, i32* %ci, align 4, !tbaa !28
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc51, %for.body
  %35 = load i32, i32* %ci, align 4, !tbaa !28
  %36 = load i32, i32* %nc, align 4, !tbaa !28
  %cmp3 = icmp slt i32 %35, %36
  br i1 %cmp3, label %for.body4, label %for.end52

for.body4:                                        ; preds = %for.cond2
  %37 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %38 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx5 = getelementptr inbounds i8*, i8** %37, i32 %38
  %39 = load i8*, i8** %arrayidx5, align 4, !tbaa !2
  %40 = load i32, i32* %ci, align 4, !tbaa !28
  %add.ptr = getelementptr inbounds i8, i8* %39, i32 %40
  store i8* %add.ptr, i8** %input_ptr, align 4, !tbaa !2
  %41 = load i8**, i8*** %output_buf.addr, align 4, !tbaa !2
  %42 = load i32, i32* %row, align 4, !tbaa !28
  %arrayidx6 = getelementptr inbounds i8*, i8** %41, i32 %42
  %43 = load i8*, i8** %arrayidx6, align 4, !tbaa !2
  store i8* %43, i8** %output_ptr, align 4, !tbaa !2
  %44 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %on_odd_row = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %44, i32 0, i32 9
  %45 = load i32, i32* %on_odd_row, align 4, !tbaa !36
  %tobool = icmp ne i32 %45, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body4
  %46 = load i32, i32* %width, align 4, !tbaa !28
  %sub = sub i32 %46, 1
  %47 = load i32, i32* %nc, align 4, !tbaa !28
  %mul7 = mul i32 %sub, %47
  %48 = load i8*, i8** %input_ptr, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds i8, i8* %48, i32 %mul7
  store i8* %add.ptr8, i8** %input_ptr, align 4, !tbaa !2
  %49 = load i32, i32* %width, align 4, !tbaa !28
  %sub9 = sub i32 %49, 1
  %50 = load i8*, i8** %output_ptr, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i8, i8* %50, i32 %sub9
  store i8* %add.ptr10, i8** %output_ptr, align 4, !tbaa !2
  store i32 -1, i32* %dir, align 4, !tbaa !28
  %51 = load i32, i32* %nc, align 4, !tbaa !28
  %sub11 = sub nsw i32 0, %51
  store i32 %sub11, i32* %dirnc, align 4, !tbaa !28
  %52 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %52, i32 0, i32 8
  %53 = load i32, i32* %ci, align 4, !tbaa !28
  %arrayidx12 = getelementptr inbounds [4 x i16*], [4 x i16*]* %fserrors, i32 0, i32 %53
  %54 = load i16*, i16** %arrayidx12, align 4, !tbaa !2
  %55 = load i32, i32* %width, align 4, !tbaa !28
  %add = add i32 %55, 1
  %add.ptr13 = getelementptr inbounds i16, i16* %54, i32 %add
  store i16* %add.ptr13, i16** %errorptr, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %for.body4
  store i32 1, i32* %dir, align 4, !tbaa !28
  %56 = load i32, i32* %nc, align 4, !tbaa !28
  store i32 %56, i32* %dirnc, align 4, !tbaa !28
  %57 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %fserrors14 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %57, i32 0, i32 8
  %58 = load i32, i32* %ci, align 4, !tbaa !28
  %arrayidx15 = getelementptr inbounds [4 x i16*], [4 x i16*]* %fserrors14, i32 0, i32 %58
  %59 = load i16*, i16** %arrayidx15, align 4, !tbaa !2
  store i16* %59, i16** %errorptr, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %60 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %colorindex = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %60, i32 0, i32 3
  %61 = load i8**, i8*** %colorindex, align 4, !tbaa !41
  %62 = load i32, i32* %ci, align 4, !tbaa !28
  %arrayidx16 = getelementptr inbounds i8*, i8** %61, i32 %62
  %63 = load i8*, i8** %arrayidx16, align 4, !tbaa !2
  store i8* %63, i8** %colorindex_ci, align 4, !tbaa !2
  %64 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %sv_colormap = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %64, i32 0, i32 1
  %65 = load i8**, i8*** %sv_colormap, align 4, !tbaa !29
  %66 = load i32, i32* %ci, align 4, !tbaa !28
  %arrayidx17 = getelementptr inbounds i8*, i8** %65, i32 %66
  %67 = load i8*, i8** %arrayidx17, align 4, !tbaa !2
  store i8* %67, i8** %colormap_ci, align 4, !tbaa !2
  store i32 0, i32* %cur, align 4, !tbaa !28
  store i32 0, i32* %bpreverr, align 4, !tbaa !28
  store i32 0, i32* %belowerr, align 4, !tbaa !28
  %68 = load i32, i32* %width, align 4, !tbaa !28
  store i32 %68, i32* %col, align 4, !tbaa !28
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %if.end
  %69 = load i32, i32* %col, align 4, !tbaa !28
  %cmp19 = icmp ugt i32 %69, 0
  br i1 %cmp19, label %for.body20, label %for.end

for.body20:                                       ; preds = %for.cond18
  %70 = load i32, i32* %cur, align 4, !tbaa !28
  %71 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %72 = load i32, i32* %dir, align 4, !tbaa !28
  %arrayidx21 = getelementptr inbounds i16, i16* %71, i32 %72
  %73 = load i16, i16* %arrayidx21, align 2, !tbaa !44
  %conv = sext i16 %73 to i32
  %add22 = add nsw i32 %70, %conv
  %add23 = add nsw i32 %add22, 8
  %shr = ashr i32 %add23, 4
  store i32 %shr, i32* %cur, align 4, !tbaa !28
  %74 = load i8*, i8** %input_ptr, align 4, !tbaa !2
  %75 = load i8, i8* %74, align 1, !tbaa !24
  %conv24 = zext i8 %75 to i32
  %76 = load i32, i32* %cur, align 4, !tbaa !28
  %add25 = add nsw i32 %76, %conv24
  store i32 %add25, i32* %cur, align 4, !tbaa !28
  %77 = load i8*, i8** %range_limit, align 4, !tbaa !2
  %78 = load i32, i32* %cur, align 4, !tbaa !28
  %arrayidx26 = getelementptr inbounds i8, i8* %77, i32 %78
  %79 = load i8, i8* %arrayidx26, align 1, !tbaa !24
  %conv27 = zext i8 %79 to i32
  store i32 %conv27, i32* %cur, align 4, !tbaa !28
  %80 = load i8*, i8** %colorindex_ci, align 4, !tbaa !2
  %81 = load i32, i32* %cur, align 4, !tbaa !28
  %arrayidx28 = getelementptr inbounds i8, i8* %80, i32 %81
  %82 = load i8, i8* %arrayidx28, align 1, !tbaa !24
  %conv29 = zext i8 %82 to i32
  store i32 %conv29, i32* %pixcode, align 4, !tbaa !28
  %83 = load i32, i32* %pixcode, align 4, !tbaa !28
  %conv30 = trunc i32 %83 to i8
  %conv31 = zext i8 %conv30 to i32
  %84 = load i8*, i8** %output_ptr, align 4, !tbaa !2
  %85 = load i8, i8* %84, align 1, !tbaa !24
  %conv32 = zext i8 %85 to i32
  %add33 = add nsw i32 %conv32, %conv31
  %conv34 = trunc i32 %add33 to i8
  store i8 %conv34, i8* %84, align 1, !tbaa !24
  %86 = load i8*, i8** %colormap_ci, align 4, !tbaa !2
  %87 = load i32, i32* %pixcode, align 4, !tbaa !28
  %arrayidx35 = getelementptr inbounds i8, i8* %86, i32 %87
  %88 = load i8, i8* %arrayidx35, align 1, !tbaa !24
  %conv36 = zext i8 %88 to i32
  %89 = load i32, i32* %cur, align 4, !tbaa !28
  %sub37 = sub nsw i32 %89, %conv36
  store i32 %sub37, i32* %cur, align 4, !tbaa !28
  %90 = load i32, i32* %cur, align 4, !tbaa !28
  store i32 %90, i32* %bnexterr, align 4, !tbaa !28
  %91 = load i32, i32* %cur, align 4, !tbaa !28
  %mul38 = mul nsw i32 %91, 2
  store i32 %mul38, i32* %delta, align 4, !tbaa !28
  %92 = load i32, i32* %delta, align 4, !tbaa !28
  %93 = load i32, i32* %cur, align 4, !tbaa !28
  %add39 = add nsw i32 %93, %92
  store i32 %add39, i32* %cur, align 4, !tbaa !28
  %94 = load i32, i32* %bpreverr, align 4, !tbaa !28
  %95 = load i32, i32* %cur, align 4, !tbaa !28
  %add40 = add nsw i32 %94, %95
  %conv41 = trunc i32 %add40 to i16
  %96 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds i16, i16* %96, i32 0
  store i16 %conv41, i16* %arrayidx42, align 2, !tbaa !44
  %97 = load i32, i32* %delta, align 4, !tbaa !28
  %98 = load i32, i32* %cur, align 4, !tbaa !28
  %add43 = add nsw i32 %98, %97
  store i32 %add43, i32* %cur, align 4, !tbaa !28
  %99 = load i32, i32* %belowerr, align 4, !tbaa !28
  %100 = load i32, i32* %cur, align 4, !tbaa !28
  %add44 = add nsw i32 %99, %100
  store i32 %add44, i32* %bpreverr, align 4, !tbaa !28
  %101 = load i32, i32* %bnexterr, align 4, !tbaa !28
  store i32 %101, i32* %belowerr, align 4, !tbaa !28
  %102 = load i32, i32* %delta, align 4, !tbaa !28
  %103 = load i32, i32* %cur, align 4, !tbaa !28
  %add45 = add nsw i32 %103, %102
  store i32 %add45, i32* %cur, align 4, !tbaa !28
  %104 = load i32, i32* %dirnc, align 4, !tbaa !28
  %105 = load i8*, i8** %input_ptr, align 4, !tbaa !2
  %add.ptr46 = getelementptr inbounds i8, i8* %105, i32 %104
  store i8* %add.ptr46, i8** %input_ptr, align 4, !tbaa !2
  %106 = load i32, i32* %dir, align 4, !tbaa !28
  %107 = load i8*, i8** %output_ptr, align 4, !tbaa !2
  %add.ptr47 = getelementptr inbounds i8, i8* %107, i32 %106
  store i8* %add.ptr47, i8** %output_ptr, align 4, !tbaa !2
  %108 = load i32, i32* %dir, align 4, !tbaa !28
  %109 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %add.ptr48 = getelementptr inbounds i16, i16* %109, i32 %108
  store i16* %add.ptr48, i16** %errorptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body20
  %110 = load i32, i32* %col, align 4, !tbaa !28
  %dec = add i32 %110, -1
  store i32 %dec, i32* %col, align 4, !tbaa !28
  br label %for.cond18

for.end:                                          ; preds = %for.cond18
  %111 = load i32, i32* %bpreverr, align 4, !tbaa !28
  %conv49 = trunc i32 %111 to i16
  %112 = load i16*, i16** %errorptr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds i16, i16* %112, i32 0
  store i16 %conv49, i16* %arrayidx50, align 2, !tbaa !44
  br label %for.inc51

for.inc51:                                        ; preds = %for.end
  %113 = load i32, i32* %ci, align 4, !tbaa !28
  %inc = add nsw i32 %113, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !28
  br label %for.cond2

for.end52:                                        ; preds = %for.cond2
  %114 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %on_odd_row53 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %114, i32 0, i32 9
  %115 = load i32, i32* %on_odd_row53, align 4, !tbaa !36
  %tobool54 = icmp ne i32 %115, 0
  %116 = zext i1 %tobool54 to i64
  %cond = select i1 %tobool54, i32 0, i32 1
  %117 = load %struct.my_cquantizer*, %struct.my_cquantizer** %cquantize, align 4, !tbaa !2
  %on_odd_row55 = getelementptr inbounds %struct.my_cquantizer, %struct.my_cquantizer* %117, i32 0, i32 9
  store i32 %cond, i32* %on_odd_row55, align 4, !tbaa !36
  br label %for.inc56

for.inc56:                                        ; preds = %for.end52
  %118 = load i32, i32* %row, align 4, !tbaa !28
  %inc57 = add nsw i32 %118, 1
  store i32 %inc57, i32* %row, align 4, !tbaa !28
  br label %for.cond

for.end58:                                        ; preds = %for.cond
  %119 = bitcast i8** %range_limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #3
  %120 = bitcast i32* %width to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #3
  %121 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #3
  %122 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #3
  %123 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #3
  %124 = bitcast i32* %dirnc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #3
  %125 = bitcast i32* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  %126 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast i32* %pixcode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = bitcast i8** %colormap_ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #3
  %129 = bitcast i8** %colorindex_ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #3
  %130 = bitcast i8** %output_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #3
  %131 = bitcast i8** %input_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #3
  %132 = bitcast i16** %errorptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #3
  %133 = bitcast i32* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #3
  %134 = bitcast i32* %bnexterr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #3
  %135 = bitcast i32* %bpreverr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #3
  %136 = bitcast i32* %belowerr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #3
  %137 = bitcast i32* %cur to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #3
  %138 = bitcast %struct.my_cquantizer** %cquantize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #3
  ret void
}

declare void @jzero_far(i8*, i32) #2

; Function Attrs: nounwind
define internal [16 x i32]* @make_odither_array(%struct.jpeg_decompress_struct* %cinfo, i32 %ncolors) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %ncolors.addr = alloca i32, align 4
  %odither = alloca [16 x i32]*, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %num = alloca i32, align 4
  %den = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %ncolors, i32* %ncolors.addr, align 4, !tbaa !28
  %0 = bitcast [16 x i32]** %odither to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast i32* %k to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = bitcast i32* %num to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = bitcast i32* %den to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %5, i32 0, i32 1
  %6 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %6, i32 0, i32 0
  %7 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %8 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %9 = bitcast %struct.jpeg_decompress_struct* %8 to %struct.jpeg_common_struct*
  %call = call i8* %7(%struct.jpeg_common_struct* %9, i32 1, i32 1024)
  %10 = bitcast i8* %call to [16 x i32]*
  store [16 x i32]* %10, [16 x i32]** %odither, align 4, !tbaa !2
  %11 = load i32, i32* %ncolors.addr, align 4, !tbaa !28
  %sub = sub nsw i32 %11, 1
  %mul = mul nsw i32 512, %sub
  store i32 %mul, i32* %den, align 4, !tbaa !38
  store i32 0, i32* %j, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %12 = load i32, i32* %j, align 4, !tbaa !28
  %cmp = icmp slt i32 %12, 16
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %k, align 4, !tbaa !28
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %k, align 4, !tbaa !28
  %cmp2 = icmp slt i32 %13, 16
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %14 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx = getelementptr inbounds [16 x [16 x i8]], [16 x [16 x i8]]* @base_dither_matrix, i32 0, i32 %14
  %15 = load i32, i32* %k, align 4, !tbaa !28
  %arrayidx4 = getelementptr inbounds [16 x i8], [16 x i8]* %arrayidx, i32 0, i32 %15
  %16 = load i8, i8* %arrayidx4, align 1, !tbaa !24
  %conv = zext i8 %16 to i32
  %mul5 = mul nsw i32 2, %conv
  %sub6 = sub nsw i32 255, %mul5
  %mul7 = mul nsw i32 %sub6, 255
  store i32 %mul7, i32* %num, align 4, !tbaa !38
  %17 = load i32, i32* %num, align 4, !tbaa !38
  %cmp8 = icmp slt i32 %17, 0
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body3
  %18 = load i32, i32* %num, align 4, !tbaa !38
  %sub10 = sub nsw i32 0, %18
  %19 = load i32, i32* %den, align 4, !tbaa !38
  %div = sdiv i32 %sub10, %19
  %sub11 = sub nsw i32 0, %div
  br label %cond.end

cond.false:                                       ; preds = %for.body3
  %20 = load i32, i32* %num, align 4, !tbaa !38
  %21 = load i32, i32* %den, align 4, !tbaa !38
  %div12 = sdiv i32 %20, %21
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub11, %cond.true ], [ %div12, %cond.false ]
  %22 = load [16 x i32]*, [16 x i32]** %odither, align 4, !tbaa !2
  %23 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx13 = getelementptr inbounds [16 x i32], [16 x i32]* %22, i32 %23
  %24 = load i32, i32* %k, align 4, !tbaa !28
  %arrayidx14 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx13, i32 0, i32 %24
  store i32 %cond, i32* %arrayidx14, align 4, !tbaa !28
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %25 = load i32, i32* %k, align 4, !tbaa !28
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %k, align 4, !tbaa !28
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %26 = load i32, i32* %j, align 4, !tbaa !28
  %inc16 = add nsw i32 %26, 1
  store i32 %inc16, i32* %j, align 4, !tbaa !28
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  %27 = load [16 x i32]*, [16 x i32]** %odither, align 4, !tbaa !2
  %28 = bitcast i32* %den to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #3
  %29 = bitcast i32* %num to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #3
  %30 = bitcast i32* %k to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #3
  %31 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #3
  %32 = bitcast [16 x i32]** %odither to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #3
  ret [16 x i32]* %27
}

; Function Attrs: nounwind
define internal i32 @select_ncolors(%struct.jpeg_decompress_struct* %cinfo, i32* %Ncolors) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %Ncolors.addr = alloca i32*, align 4
  %nc = alloca i32, align 4
  %max_colors = alloca i32, align 4
  %total_colors = alloca i32, align 4
  %iroot = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %changed = alloca i32, align 4
  %temp = alloca i32, align 4
  %RGB_order = alloca [3 x i32], align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32* %Ncolors, i32** %Ncolors.addr, align 4, !tbaa !2
  %0 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 29
  %2 = load i32, i32* %out_color_components, align 8, !tbaa !20
  store i32 %2, i32* %nc, align 4, !tbaa !28
  %3 = bitcast i32* %max_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #3
  %4 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %desired_number_of_colors = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %4, i32 0, i32 23
  %5 = load i32, i32* %desired_number_of_colors, align 8, !tbaa !26
  store i32 %5, i32* %max_colors, align 4, !tbaa !28
  %6 = bitcast i32* %total_colors to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast i32* %iroot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i32* %changed to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = bitcast [3 x i32]* %RGB_order to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %12) #3
  %13 = bitcast [3 x i32]* %RGB_order to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 bitcast ([3 x i32]* @__const.select_ncolors.RGB_order to i8*), i32 12, i1 false)
  %14 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %14, i32 0, i32 11
  %15 = load i32, i32* %out_color_space, align 4, !tbaa !45
  %arrayidx = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_green, i32 0, i32 %15
  %16 = load i32, i32* %arrayidx, align 4, !tbaa !28
  %arrayidx1 = getelementptr inbounds [3 x i32], [3 x i32]* %RGB_order, i32 0, i32 0
  store i32 %16, i32* %arrayidx1, align 4, !tbaa !28
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space2 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 11
  %18 = load i32, i32* %out_color_space2, align 4, !tbaa !45
  %arrayidx3 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_red, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx3, align 4, !tbaa !28
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %RGB_order, i32 0, i32 1
  store i32 %19, i32* %arrayidx4, align 4, !tbaa !28
  %20 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space5 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %20, i32 0, i32 11
  %21 = load i32, i32* %out_color_space5, align 4, !tbaa !45
  %arrayidx6 = getelementptr inbounds [17 x i32], [17 x i32]* @rgb_blue, i32 0, i32 %21
  %22 = load i32, i32* %arrayidx6, align 4, !tbaa !28
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %RGB_order, i32 0, i32 2
  store i32 %22, i32* %arrayidx7, align 4, !tbaa !28
  store i32 1, i32* %iroot, align 4, !tbaa !28
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %23 = load i32, i32* %iroot, align 4, !tbaa !28
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %iroot, align 4, !tbaa !28
  %24 = load i32, i32* %iroot, align 4, !tbaa !28
  store i32 %24, i32* %temp, align 4, !tbaa !38
  store i32 1, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.body
  %25 = load i32, i32* %i, align 4, !tbaa !28
  %26 = load i32, i32* %nc, align 4, !tbaa !28
  %cmp = icmp slt i32 %25, %26
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %27 = load i32, i32* %iroot, align 4, !tbaa !28
  %28 = load i32, i32* %temp, align 4, !tbaa !38
  %mul = mul nsw i32 %28, %27
  store i32 %mul, i32* %temp, align 4, !tbaa !38
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !28
  %inc8 = add nsw i32 %29, 1
  store i32 %inc8, i32* %i, align 4, !tbaa !28
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %do.cond

do.cond:                                          ; preds = %for.end
  %30 = load i32, i32* %temp, align 4, !tbaa !38
  %31 = load i32, i32* %max_colors, align 4, !tbaa !28
  %cmp9 = icmp sle i32 %30, %31
  br i1 %cmp9, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %32 = load i32, i32* %iroot, align 4, !tbaa !28
  %dec = add nsw i32 %32, -1
  store i32 %dec, i32* %iroot, align 4, !tbaa !28
  %33 = load i32, i32* %iroot, align 4, !tbaa !28
  %cmp10 = icmp slt i32 %33, 2
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %do.end
  %34 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %34, i32 0, i32 0
  %35 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !21
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %35, i32 0, i32 5
  store i32 56, i32* %msg_code, align 4, !tbaa !22
  %36 = load i32, i32* %temp, align 4, !tbaa !38
  %37 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err11 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %37, i32 0, i32 0
  %38 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err11, align 8, !tbaa !21
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %38, i32 0, i32 6
  %i12 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx13 = getelementptr inbounds [8 x i32], [8 x i32]* %i12, i32 0, i32 0
  store i32 %36, i32* %arrayidx13, align 4, !tbaa !24
  %39 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err14 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %39, i32 0, i32 0
  %40 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err14, align 8, !tbaa !21
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %40, i32 0, i32 0
  %41 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !25
  %42 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %43 = bitcast %struct.jpeg_decompress_struct* %42 to %struct.jpeg_common_struct*
  call void %41(%struct.jpeg_common_struct* %43)
  br label %if.end

if.end:                                           ; preds = %if.then, %do.end
  store i32 1, i32* %total_colors, align 4, !tbaa !28
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc20, %if.end
  %44 = load i32, i32* %i, align 4, !tbaa !28
  %45 = load i32, i32* %nc, align 4, !tbaa !28
  %cmp16 = icmp slt i32 %44, %45
  br i1 %cmp16, label %for.body17, label %for.end22

for.body17:                                       ; preds = %for.cond15
  %46 = load i32, i32* %iroot, align 4, !tbaa !28
  %47 = load i32*, i32** %Ncolors.addr, align 4, !tbaa !2
  %48 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx18 = getelementptr inbounds i32, i32* %47, i32 %48
  store i32 %46, i32* %arrayidx18, align 4, !tbaa !28
  %49 = load i32, i32* %iroot, align 4, !tbaa !28
  %50 = load i32, i32* %total_colors, align 4, !tbaa !28
  %mul19 = mul nsw i32 %50, %49
  store i32 %mul19, i32* %total_colors, align 4, !tbaa !28
  br label %for.inc20

for.inc20:                                        ; preds = %for.body17
  %51 = load i32, i32* %i, align 4, !tbaa !28
  %inc21 = add nsw i32 %51, 1
  store i32 %inc21, i32* %i, align 4, !tbaa !28
  br label %for.cond15

for.end22:                                        ; preds = %for.cond15
  br label %do.body23

do.body23:                                        ; preds = %do.cond41, %for.end22
  store i32 0, i32* %changed, align 4, !tbaa !28
  store i32 0, i32* %i, align 4, !tbaa !28
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc38, %do.body23
  %52 = load i32, i32* %i, align 4, !tbaa !28
  %53 = load i32, i32* %nc, align 4, !tbaa !28
  %cmp25 = icmp slt i32 %52, %53
  br i1 %cmp25, label %for.body26, label %for.end40

for.body26:                                       ; preds = %for.cond24
  %54 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %out_color_space27 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %54, i32 0, i32 11
  %55 = load i32, i32* %out_color_space27, align 4, !tbaa !45
  %cmp28 = icmp eq i32 %55, 2
  br i1 %cmp28, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body26
  %56 = load i32, i32* %i, align 4, !tbaa !28
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %RGB_order, i32 0, i32 %56
  %57 = load i32, i32* %arrayidx29, align 4, !tbaa !28
  br label %cond.end

cond.false:                                       ; preds = %for.body26
  %58 = load i32, i32* %i, align 4, !tbaa !28
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %57, %cond.true ], [ %58, %cond.false ]
  store i32 %cond, i32* %j, align 4, !tbaa !28
  %59 = load i32, i32* %total_colors, align 4, !tbaa !28
  %60 = load i32*, i32** %Ncolors.addr, align 4, !tbaa !2
  %61 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx30 = getelementptr inbounds i32, i32* %60, i32 %61
  %62 = load i32, i32* %arrayidx30, align 4, !tbaa !28
  %div = sdiv i32 %59, %62
  store i32 %div, i32* %temp, align 4, !tbaa !38
  %63 = load i32*, i32** %Ncolors.addr, align 4, !tbaa !2
  %64 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx31 = getelementptr inbounds i32, i32* %63, i32 %64
  %65 = load i32, i32* %arrayidx31, align 4, !tbaa !28
  %add = add nsw i32 %65, 1
  %66 = load i32, i32* %temp, align 4, !tbaa !38
  %mul32 = mul nsw i32 %66, %add
  store i32 %mul32, i32* %temp, align 4, !tbaa !38
  %67 = load i32, i32* %temp, align 4, !tbaa !38
  %68 = load i32, i32* %max_colors, align 4, !tbaa !28
  %cmp33 = icmp sgt i32 %67, %68
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %cond.end
  br label %for.end40

if.end35:                                         ; preds = %cond.end
  %69 = load i32*, i32** %Ncolors.addr, align 4, !tbaa !2
  %70 = load i32, i32* %j, align 4, !tbaa !28
  %arrayidx36 = getelementptr inbounds i32, i32* %69, i32 %70
  %71 = load i32, i32* %arrayidx36, align 4, !tbaa !28
  %inc37 = add nsw i32 %71, 1
  store i32 %inc37, i32* %arrayidx36, align 4, !tbaa !28
  %72 = load i32, i32* %temp, align 4, !tbaa !38
  store i32 %72, i32* %total_colors, align 4, !tbaa !28
  store i32 1, i32* %changed, align 4, !tbaa !28
  br label %for.inc38

for.inc38:                                        ; preds = %if.end35
  %73 = load i32, i32* %i, align 4, !tbaa !28
  %inc39 = add nsw i32 %73, 1
  store i32 %inc39, i32* %i, align 4, !tbaa !28
  br label %for.cond24

for.end40:                                        ; preds = %if.then34, %for.cond24
  br label %do.cond41

do.cond41:                                        ; preds = %for.end40
  %74 = load i32, i32* %changed, align 4, !tbaa !28
  %tobool = icmp ne i32 %74, 0
  br i1 %tobool, label %do.body23, label %do.end42

do.end42:                                         ; preds = %do.cond41
  %75 = load i32, i32* %total_colors, align 4, !tbaa !28
  %76 = bitcast [3 x i32]* %RGB_order to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %76) #3
  %77 = bitcast i32* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #3
  %78 = bitcast i32* %changed to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #3
  %79 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #3
  %80 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #3
  %81 = bitcast i32* %iroot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #3
  %82 = bitcast i32* %total_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #3
  %83 = bitcast i32* %max_colors to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #3
  %84 = bitcast i32* %nc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #3
  ret i32 %75
}

; Function Attrs: nounwind
define internal i32 @output_value(%struct.jpeg_decompress_struct* %cinfo, i32 %ci, i32 %j, i32 %maxj) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %ci.addr = alloca i32, align 4
  %j.addr = alloca i32, align 4
  %maxj.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %ci, i32* %ci.addr, align 4, !tbaa !28
  store i32 %j, i32* %j.addr, align 4, !tbaa !28
  store i32 %maxj, i32* %maxj.addr, align 4, !tbaa !28
  %0 = load i32, i32* %j.addr, align 4, !tbaa !28
  %mul = mul nsw i32 %0, 255
  %1 = load i32, i32* %maxj.addr, align 4, !tbaa !28
  %div = sdiv i32 %1, 2
  %add = add nsw i32 %mul, %div
  %2 = load i32, i32* %maxj.addr, align 4, !tbaa !28
  %div1 = sdiv i32 %add, %2
  ret i32 %div1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: nounwind
define internal i32 @largest_input_value(%struct.jpeg_decompress_struct* %cinfo, i32 %ci, i32 %j, i32 %maxj) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %ci.addr = alloca i32, align 4
  %j.addr = alloca i32, align 4
  %maxj.addr = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %ci, i32* %ci.addr, align 4, !tbaa !28
  store i32 %j, i32* %j.addr, align 4, !tbaa !28
  store i32 %maxj, i32* %maxj.addr, align 4, !tbaa !28
  %0 = load i32, i32* %j.addr, align 4, !tbaa !28
  %mul = mul nsw i32 2, %0
  %add = add nsw i32 %mul, 1
  %mul1 = mul nsw i32 %add, 255
  %1 = load i32, i32* %maxj.addr, align 4, !tbaa !28
  %add2 = add nsw i32 %mul1, %1
  %2 = load i32, i32* %maxj.addr, align 4, !tbaa !28
  %mul3 = mul nsw i32 2, %2
  %div = sdiv i32 %add2, %mul3
  ret i32 %div
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 460}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !3, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !4, i64 32, !8, i64 48, !4, i64 52, !4, i64 68, !8, i64 84}
!17 = !{!"jpeg_color_quantizer", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12}
!18 = !{!16, !3, i64 8}
!19 = !{!16, !3, i64 12}
!20 = !{!7, !8, i64 120}
!21 = !{!7, !3, i64 0}
!22 = !{!23, !8, i64 20}
!23 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!24 = !{!4, !4, i64 0}
!25 = !{!23, !3, i64 0}
!26 = !{!7, !8, i64 96}
!27 = !{!7, !4, i64 88}
!28 = !{!8, !8, i64 0}
!29 = !{!16, !3, i64 16}
!30 = !{!7, !3, i64 136}
!31 = !{!16, !8, i64 20}
!32 = !{!7, !8, i64 132}
!33 = !{!16, !3, i64 4}
!34 = !{!16, !8, i64 48}
!35 = !{!16, !8, i64 28}
!36 = !{!16, !8, i64 84}
!37 = !{!7, !8, i64 112}
!38 = !{!13, !13, i64 0}
!39 = !{!23, !3, i64 4}
!40 = !{!12, !3, i64 8}
!41 = !{!16, !3, i64 24}
!42 = !{!12, !3, i64 4}
!43 = !{!7, !3, i64 324}
!44 = !{!10, !10, i64 0}
!45 = !{!7, !4, i64 44}
