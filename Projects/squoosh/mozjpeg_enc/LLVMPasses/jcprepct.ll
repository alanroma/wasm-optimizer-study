; ModuleID = 'jcprepct.c'
source_filename = "jcprepct.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_compress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_destination_mgr*, i32, i32, i32, i32, double, i32, i32, i32, %struct.jpeg_component_info*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], [16 x i8], [16 x i8], [16 x i8], i32, %struct.jpeg_scan_info*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8, i8, i8, i16, i16, i32, i32, i32, i32, i32, i32, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, %struct.jpeg_comp_master*, %struct.jpeg_c_main_controller*, %struct.jpeg_c_prep_controller*, %struct.jpeg_c_coef_controller*, %struct.jpeg_marker_writer*, %struct.jpeg_color_converter*, %struct.jpeg_downsampler*, %struct.jpeg_forward_dct*, %struct.jpeg_entropy_encoder*, %struct.jpeg_scan_info*, i32 }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_destination_mgr = type { i8*, i32, void (%struct.jpeg_compress_struct*)*, i32 (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_comp_master = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [4 x [64 x double]], [4 x [64 x double]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float }
%struct.jpeg_c_main_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32)* }
%struct.jpeg_c_prep_controller = type { void (%struct.jpeg_compress_struct*, i32)*, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* }
%struct.jpeg_c_coef_controller = type { void (%struct.jpeg_compress_struct*, i32)*, i32 (%struct.jpeg_compress_struct*, i8***)* }
%struct.jpeg_marker_writer = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i32, i32)*, {}* }
%struct.jpeg_color_converter = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)* }
%struct.jpeg_downsampler = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, i32 }
%struct.jpeg_forward_dct = type { void (%struct.jpeg_compress_struct*)*, void (%struct.jpeg_compress_struct*, %struct.jpeg_component_info*, i8**, [64 x i16]*, i32, i32, i32, [64 x i16]*)* }
%struct.jpeg_entropy_encoder = type { {}*, i32 (%struct.jpeg_compress_struct*, [64 x i16]**)*, void (%struct.jpeg_compress_struct*)* }
%struct.jpeg_scan_info = type { i32, [4 x i32], i32, i32, i32, i32 }
%struct.my_prep_controller = type { %struct.jpeg_c_prep_controller, [10 x i8**], i32, i32, i32, i32 }

; Function Attrs: nounwind
define hidden void @jinit_c_prep_controller(%struct.jpeg_compress_struct* %cinfo, i32 %need_full_buffer) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %need_full_buffer.addr = alloca i32, align 4
  %prep = alloca %struct.my_prep_controller*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %need_full_buffer, i32* %need_full_buffer.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #3
  %2 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #3
  %3 = load i32, i32* %need_full_buffer.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %4, i32 0, i32 0
  %5 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !8
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %5, i32 0, i32 5
  store i32 4, i32* %msg_code, align 4, !tbaa !12
  %6 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %6, i32 0, i32 0
  %7 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err1, align 8, !tbaa !8
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %7, i32 0, i32 0
  %8 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !15
  %9 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %10 = bitcast %struct.jpeg_compress_struct* %9 to %struct.jpeg_common_struct*
  call void %8(%struct.jpeg_common_struct* %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %11, i32 0, i32 1
  %12 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !16
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %12, i32 0, i32 0
  %13 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !17
  %14 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %15 = bitcast %struct.jpeg_compress_struct* %14 to %struct.jpeg_common_struct*
  %call = call i8* %13(%struct.jpeg_common_struct* %15, i32 1, i32 64)
  %16 = bitcast i8* %call to %struct.my_prep_controller*
  store %struct.my_prep_controller* %16, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %17 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %18 = bitcast %struct.my_prep_controller* %17 to %struct.jpeg_c_prep_controller*
  %19 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %prep2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %19, i32 0, i32 56
  store %struct.jpeg_c_prep_controller* %18, %struct.jpeg_c_prep_controller** %prep2, align 4, !tbaa !19
  %20 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %20, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_c_prep_controller, %struct.jpeg_c_prep_controller* %pub, i32 0, i32 0
  store void (%struct.jpeg_compress_struct*, i32)* @start_pass_prep, void (%struct.jpeg_compress_struct*, i32)** %start_pass, align 4, !tbaa !20
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %downsample = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 60
  %22 = load %struct.jpeg_downsampler*, %struct.jpeg_downsampler** %downsample, align 4, !tbaa !23
  %need_context_rows = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %22, i32 0, i32 2
  %23 = load i32, i32* %need_context_rows, align 4, !tbaa !24
  %tobool3 = icmp ne i32 %23, 0
  br i1 %tobool3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %24 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %pub5 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %24, i32 0, i32 0
  %pre_process_data = getelementptr inbounds %struct.jpeg_c_prep_controller, %struct.jpeg_c_prep_controller* %pub5, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* @pre_process_context, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)** %pre_process_data, align 4, !tbaa !26
  %25 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  call void @create_context_buffer(%struct.jpeg_compress_struct* %25)
  br label %if.end11

if.else:                                          ; preds = %if.end
  %26 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %pub6 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %26, i32 0, i32 0
  %pre_process_data7 = getelementptr inbounds %struct.jpeg_c_prep_controller, %struct.jpeg_c_prep_controller* %pub6, i32 0, i32 1
  store void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)* @pre_process_data, void (%struct.jpeg_compress_struct*, i8**, i32*, i32, i8***, i32*, i32)** %pre_process_data7, align 4, !tbaa !26
  store i32 0, i32* %ci, align 4, !tbaa !6
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 15
  %28 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !27
  store %struct.jpeg_component_info* %28, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %29 = load i32, i32* %ci, align 4, !tbaa !6
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %30, i32 0, i32 13
  %31 = load i32, i32* %num_components, align 4, !tbaa !28
  %cmp = icmp slt i32 %29, %31
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %32 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem8 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %32, i32 0, i32 1
  %33 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem8, align 4, !tbaa !16
  %alloc_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %33, i32 0, i32 2
  %34 = load i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_sarray, align 4, !tbaa !29
  %35 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %36 = bitcast %struct.jpeg_compress_struct* %35 to %struct.jpeg_common_struct*
  %37 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %37, i32 0, i32 7
  %38 = load i32, i32* %width_in_blocks, align 4, !tbaa !30
  %mul = mul nsw i32 %38, 8
  %39 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %39, i32 0, i32 41
  %40 = load i32, i32* %max_h_samp_factor, align 8, !tbaa !32
  %mul9 = mul nsw i32 %mul, %40
  %41 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %41, i32 0, i32 2
  %42 = load i32, i32* %h_samp_factor, align 4, !tbaa !33
  %div = sdiv i32 %mul9, %42
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 42
  %44 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !34
  %call10 = call i8** %34(%struct.jpeg_common_struct* %36, i32 1, i32 %div, i32 %44)
  %45 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %45, i32 0, i32 1
  %46 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf, i32 0, i32 %46
  store i8** %call10, i8*** %arrayidx, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %47 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  %48 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %48, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end11

if.end11:                                         ; preds = %for.end, %if.then4
  %49 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #3
  %50 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #3
  %51 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass_prep(%struct.jpeg_compress_struct* %cinfo, i32 %pass_mode) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %pass_mode.addr = alloca i32, align 4
  %prep = alloca %struct.my_prep_controller*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i32 %pass_mode, i32* %pass_mode.addr, align 4, !tbaa !35
  %0 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %prep1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 56
  %2 = load %struct.jpeg_c_prep_controller*, %struct.jpeg_c_prep_controller** %prep1, align 4, !tbaa !19
  %3 = bitcast %struct.jpeg_c_prep_controller* %2 to %struct.my_prep_controller*
  store %struct.my_prep_controller* %3, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %4 = load i32, i32* %pass_mode.addr, align 4, !tbaa !35
  %cmp = icmp ne i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 0
  %6 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !8
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %6, i32 0, i32 5
  store i32 4, i32* %msg_code, align 4, !tbaa !12
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %err2 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 0
  %8 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err2, align 8, !tbaa !8
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %8, i32 0, i32 0
  %9 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !15
  %10 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %11 = bitcast %struct.jpeg_compress_struct* %10 to %struct.jpeg_common_struct*
  call void %9(%struct.jpeg_common_struct* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 8
  %13 = load i32, i32* %image_height, align 8, !tbaa !36
  %14 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %rows_to_go = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %14, i32 0, i32 2
  store i32 %13, i32* %rows_to_go, align 4, !tbaa !37
  %15 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %15, i32 0, i32 3
  store i32 0, i32* %next_buf_row, align 4, !tbaa !38
  %16 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %this_row_group = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %16, i32 0, i32 4
  store i32 0, i32* %this_row_group, align 4, !tbaa !39
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 42
  %18 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !34
  %mul = mul nsw i32 2, %18
  %19 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_stop = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %19, i32 0, i32 5
  store i32 %mul, i32* %next_buf_stop, align 4, !tbaa !40
  %20 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #3
  ret void
}

; Function Attrs: nounwind
define internal void @pre_process_context(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i32* %in_row_ctr, i32 %in_rows_avail, i8*** %output_buf, i32* %out_row_group_ctr, i32 %out_row_groups_avail) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %in_row_ctr.addr = alloca i32*, align 4
  %in_rows_avail.addr = alloca i32, align 4
  %output_buf.addr = alloca i8***, align 4
  %out_row_group_ctr.addr = alloca i32*, align 4
  %out_row_groups_avail.addr = alloca i32, align 4
  %prep = alloca %struct.my_prep_controller*, align 4
  %numrows = alloca i32, align 4
  %ci = alloca i32, align 4
  %buf_height = alloca i32, align 4
  %inrows = alloca i32, align 4
  %row = alloca i32, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i32* %in_row_ctr, i32** %in_row_ctr.addr, align 4, !tbaa !2
  store i32 %in_rows_avail, i32* %in_rows_avail.addr, align 4, !tbaa !6
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32* %out_row_group_ctr, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  store i32 %out_row_groups_avail, i32* %out_row_groups_avail.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %prep1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 56
  %2 = load %struct.jpeg_c_prep_controller*, %struct.jpeg_c_prep_controller** %prep1, align 4, !tbaa !19
  %3 = bitcast %struct.jpeg_c_prep_controller* %2 to %struct.my_prep_controller*
  store %struct.my_prep_controller* %3, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %4 = bitcast i32* %numrows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %buf_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %7, i32 0, i32 42
  %8 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !34
  %mul = mul nsw i32 %8, 3
  store i32 %mul, i32* %buf_height, align 4, !tbaa !6
  %9 = bitcast i32* %inrows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  br label %while.cond

while.cond:                                       ; preds = %if.end73, %entry
  %10 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  %11 = load i32, i32* %10, align 4, !tbaa !6
  %12 = load i32, i32* %out_row_groups_avail.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %11, %12
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %13 = load i32*, i32** %in_row_ctr.addr, align 4, !tbaa !2
  %14 = load i32, i32* %13, align 4, !tbaa !6
  %15 = load i32, i32* %in_rows_avail.addr, align 4, !tbaa !6
  %cmp2 = icmp ult i32 %14, %15
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %16 = load i32, i32* %in_rows_avail.addr, align 4, !tbaa !6
  %17 = load i32*, i32** %in_row_ctr.addr, align 4, !tbaa !2
  %18 = load i32, i32* %17, align 4, !tbaa !6
  %sub = sub i32 %16, %18
  store i32 %sub, i32* %inrows, align 4, !tbaa !6
  %19 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_stop = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %19, i32 0, i32 5
  %20 = load i32, i32* %next_buf_stop, align 4, !tbaa !40
  %21 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %21, i32 0, i32 3
  %22 = load i32, i32* %next_buf_row, align 4, !tbaa !38
  %sub3 = sub nsw i32 %20, %22
  store i32 %sub3, i32* %numrows, align 4, !tbaa !6
  %23 = load i32, i32* %numrows, align 4, !tbaa !6
  %24 = load i32, i32* %inrows, align 4, !tbaa !6
  %cmp4 = icmp ult i32 %23, %24
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %25 = load i32, i32* %numrows, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %26 = load i32, i32* %inrows, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %25, %cond.true ], [ %26, %cond.false ]
  store i32 %cond, i32* %numrows, align 4, !tbaa !6
  %27 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %27, i32 0, i32 59
  %28 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert, align 8, !tbaa !41
  %color_convert = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %28, i32 0, i32 1
  %29 = load void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert, align 4, !tbaa !42
  %30 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %31 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %32 = load i32*, i32** %in_row_ctr.addr, align 4, !tbaa !2
  %33 = load i32, i32* %32, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8*, i8** %31, i32 %33
  %34 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %34, i32 0, i32 1
  %arraydecay = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf, i32 0, i32 0
  %35 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row5 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %35, i32 0, i32 3
  %36 = load i32, i32* %next_buf_row5, align 4, !tbaa !38
  %37 = load i32, i32* %numrows, align 4, !tbaa !6
  call void %29(%struct.jpeg_compress_struct* %30, i8** %add.ptr, i8*** %arraydecay, i32 %36, i32 %37)
  %38 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %rows_to_go = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %38, i32 0, i32 2
  %39 = load i32, i32* %rows_to_go, align 4, !tbaa !37
  %40 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_height = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %40, i32 0, i32 8
  %41 = load i32, i32* %image_height, align 8, !tbaa !36
  %cmp6 = icmp eq i32 %39, %41
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %cond.end
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %if.then7
  %42 = load i32, i32* %ci, align 4, !tbaa !6
  %43 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %43, i32 0, i32 13
  %44 = load i32, i32* %num_components, align 4, !tbaa !28
  %cmp8 = icmp slt i32 %42, %44
  br i1 %cmp8, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  %45 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #3
  store i32 1, i32* %row, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %for.body
  %46 = load i32, i32* %row, align 4, !tbaa !6
  %47 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor10 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %47, i32 0, i32 42
  %48 = load i32, i32* %max_v_samp_factor10, align 4, !tbaa !34
  %cmp11 = icmp sle i32 %46, %48
  br i1 %cmp11, label %for.body12, label %for.end

for.body12:                                       ; preds = %for.cond9
  %49 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf13 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %49, i32 0, i32 1
  %50 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf13, i32 0, i32 %50
  %51 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %52 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf14 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %52, i32 0, i32 1
  %53 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx15 = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf14, i32 0, i32 %53
  %54 = load i8**, i8*** %arrayidx15, align 4, !tbaa !2
  %55 = load i32, i32* %row, align 4, !tbaa !6
  %sub16 = sub nsw i32 0, %55
  %56 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %56, i32 0, i32 7
  %57 = load i32, i32* %image_width, align 4, !tbaa !44
  call void @jcopy_sample_rows(i8** %51, i32 0, i8** %54, i32 %sub16, i32 1, i32 %57)
  br label %for.inc

for.inc:                                          ; preds = %for.body12
  %58 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  %59 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #3
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %60 = load i32, i32* %ci, align 4, !tbaa !6
  %inc18 = add nsw i32 %60, 1
  store i32 %inc18, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end19, %cond.end
  %61 = load i32, i32* %numrows, align 4, !tbaa !6
  %62 = load i32*, i32** %in_row_ctr.addr, align 4, !tbaa !2
  %63 = load i32, i32* %62, align 4, !tbaa !6
  %add = add i32 %63, %61
  store i32 %add, i32* %62, align 4, !tbaa !6
  %64 = load i32, i32* %numrows, align 4, !tbaa !6
  %65 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row20 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %65, i32 0, i32 3
  %66 = load i32, i32* %next_buf_row20, align 4, !tbaa !38
  %add21 = add nsw i32 %66, %64
  store i32 %add21, i32* %next_buf_row20, align 4, !tbaa !38
  %67 = load i32, i32* %numrows, align 4, !tbaa !6
  %68 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %rows_to_go22 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %68, i32 0, i32 2
  %69 = load i32, i32* %rows_to_go22, align 4, !tbaa !37
  %sub23 = sub i32 %69, %67
  store i32 %sub23, i32* %rows_to_go22, align 4, !tbaa !37
  br label %if.end47

if.else:                                          ; preds = %while.body
  %70 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %rows_to_go24 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %70, i32 0, i32 2
  %71 = load i32, i32* %rows_to_go24, align 4, !tbaa !37
  %cmp25 = icmp ne i32 %71, 0
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.else
  br label %while.end

if.end27:                                         ; preds = %if.else
  %72 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row28 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %72, i32 0, i32 3
  %73 = load i32, i32* %next_buf_row28, align 4, !tbaa !38
  %74 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_stop29 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %74, i32 0, i32 5
  %75 = load i32, i32* %next_buf_stop29, align 4, !tbaa !40
  %cmp30 = icmp slt i32 %73, %75
  br i1 %cmp30, label %if.then31, label %if.end46

if.then31:                                        ; preds = %if.end27
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc41, %if.then31
  %76 = load i32, i32* %ci, align 4, !tbaa !6
  %77 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components33 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %77, i32 0, i32 13
  %78 = load i32, i32* %num_components33, align 4, !tbaa !28
  %cmp34 = icmp slt i32 %76, %78
  br i1 %cmp34, label %for.body35, label %for.end43

for.body35:                                       ; preds = %for.cond32
  %79 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf36 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %79, i32 0, i32 1
  %80 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx37 = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf36, i32 0, i32 %80
  %81 = load i8**, i8*** %arrayidx37, align 4, !tbaa !2
  %82 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width38 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %82, i32 0, i32 7
  %83 = load i32, i32* %image_width38, align 4, !tbaa !44
  %84 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row39 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %84, i32 0, i32 3
  %85 = load i32, i32* %next_buf_row39, align 4, !tbaa !38
  %86 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_stop40 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %86, i32 0, i32 5
  %87 = load i32, i32* %next_buf_stop40, align 4, !tbaa !40
  call void @expand_bottom_edge(i8** %81, i32 %83, i32 %85, i32 %87)
  br label %for.inc41

for.inc41:                                        ; preds = %for.body35
  %88 = load i32, i32* %ci, align 4, !tbaa !6
  %inc42 = add nsw i32 %88, 1
  store i32 %inc42, i32* %ci, align 4, !tbaa !6
  br label %for.cond32

for.end43:                                        ; preds = %for.cond32
  %89 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_stop44 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %89, i32 0, i32 5
  %90 = load i32, i32* %next_buf_stop44, align 4, !tbaa !40
  %91 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row45 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %91, i32 0, i32 3
  store i32 %90, i32* %next_buf_row45, align 4, !tbaa !38
  br label %if.end46

if.end46:                                         ; preds = %for.end43, %if.end27
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.end
  %92 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row48 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %92, i32 0, i32 3
  %93 = load i32, i32* %next_buf_row48, align 4, !tbaa !38
  %94 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_stop49 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %94, i32 0, i32 5
  %95 = load i32, i32* %next_buf_stop49, align 4, !tbaa !40
  %cmp50 = icmp eq i32 %93, %95
  br i1 %cmp50, label %if.then51, label %if.end73

if.then51:                                        ; preds = %if.end47
  %96 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %downsample = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %96, i32 0, i32 60
  %97 = load %struct.jpeg_downsampler*, %struct.jpeg_downsampler** %downsample, align 4, !tbaa !23
  %downsample52 = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %97, i32 0, i32 1
  %98 = load void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)** %downsample52, align 4, !tbaa !45
  %99 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %100 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf53 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %100, i32 0, i32 1
  %arraydecay54 = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf53, i32 0, i32 0
  %101 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %this_row_group = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %101, i32 0, i32 4
  %102 = load i32, i32* %this_row_group, align 4, !tbaa !39
  %103 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %104 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  %105 = load i32, i32* %104, align 4, !tbaa !6
  call void %98(%struct.jpeg_compress_struct* %99, i8*** %arraydecay54, i32 %102, i8*** %103, i32 %105)
  %106 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  %107 = load i32, i32* %106, align 4, !tbaa !6
  %inc55 = add i32 %107, 1
  store i32 %inc55, i32* %106, align 4, !tbaa !6
  %108 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor56 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %108, i32 0, i32 42
  %109 = load i32, i32* %max_v_samp_factor56, align 4, !tbaa !34
  %110 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %this_row_group57 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %110, i32 0, i32 4
  %111 = load i32, i32* %this_row_group57, align 4, !tbaa !39
  %add58 = add nsw i32 %111, %109
  store i32 %add58, i32* %this_row_group57, align 4, !tbaa !39
  %112 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %this_row_group59 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %112, i32 0, i32 4
  %113 = load i32, i32* %this_row_group59, align 4, !tbaa !39
  %114 = load i32, i32* %buf_height, align 4, !tbaa !6
  %cmp60 = icmp sge i32 %113, %114
  br i1 %cmp60, label %if.then61, label %if.end63

if.then61:                                        ; preds = %if.then51
  %115 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %this_row_group62 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %115, i32 0, i32 4
  store i32 0, i32* %this_row_group62, align 4, !tbaa !39
  br label %if.end63

if.end63:                                         ; preds = %if.then61, %if.then51
  %116 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row64 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %116, i32 0, i32 3
  %117 = load i32, i32* %next_buf_row64, align 4, !tbaa !38
  %118 = load i32, i32* %buf_height, align 4, !tbaa !6
  %cmp65 = icmp sge i32 %117, %118
  br i1 %cmp65, label %if.then66, label %if.end68

if.then66:                                        ; preds = %if.end63
  %119 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row67 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %119, i32 0, i32 3
  store i32 0, i32* %next_buf_row67, align 4, !tbaa !38
  br label %if.end68

if.end68:                                         ; preds = %if.then66, %if.end63
  %120 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row69 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %120, i32 0, i32 3
  %121 = load i32, i32* %next_buf_row69, align 4, !tbaa !38
  %122 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor70 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %122, i32 0, i32 42
  %123 = load i32, i32* %max_v_samp_factor70, align 4, !tbaa !34
  %add71 = add nsw i32 %121, %123
  %124 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_stop72 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %124, i32 0, i32 5
  store i32 %add71, i32* %next_buf_stop72, align 4, !tbaa !40
  br label %if.end73

if.end73:                                         ; preds = %if.end68, %if.end47
  br label %while.cond

while.end:                                        ; preds = %if.then26, %while.cond
  %125 = bitcast i32* %inrows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #3
  %126 = bitcast i32* %buf_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #3
  %127 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #3
  %128 = bitcast i32* %numrows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #3
  %129 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #3
  ret void
}

; Function Attrs: nounwind
define internal void @create_context_buffer(%struct.jpeg_compress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %prep = alloca %struct.my_prep_controller*, align 4
  %rgroup_height = alloca i32, align 4
  %ci = alloca i32, align 4
  %i = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %true_buffer = alloca i8**, align 4
  %fake_buffer = alloca i8**, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %prep1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 56
  %2 = load %struct.jpeg_c_prep_controller*, %struct.jpeg_c_prep_controller** %prep1, align 4, !tbaa !19
  %3 = bitcast %struct.jpeg_c_prep_controller* %2 to %struct.my_prep_controller*
  store %struct.my_prep_controller* %3, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %4 = bitcast i32* %rgroup_height to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %5, i32 0, i32 42
  %6 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !34
  store i32 %6, i32* %rgroup_height, align 4, !tbaa !6
  %7 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #3
  %9 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #3
  %10 = bitcast i8*** %true_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #3
  %11 = bitcast i8*** %fake_buffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #3
  %12 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %12, i32 0, i32 1
  %13 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !16
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %13, i32 0, i32 0
  %14 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !17
  %15 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %16 = bitcast %struct.jpeg_compress_struct* %15 to %struct.jpeg_common_struct*
  %17 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %17, i32 0, i32 13
  %18 = load i32, i32* %num_components, align 4, !tbaa !28
  %mul = mul nsw i32 %18, 5
  %19 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %mul2 = mul nsw i32 %mul, %19
  %mul3 = mul i32 %mul2, 4
  %call = call i8* %14(%struct.jpeg_common_struct* %16, i32 1, i32 %mul3)
  %20 = bitcast i8* %call to i8**
  store i8** %20, i8*** %fake_buffer, align 4, !tbaa !2
  store i32 0, i32* %ci, align 4, !tbaa !6
  %21 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %21, i32 0, i32 15
  %22 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !27
  store %struct.jpeg_component_info* %22, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %entry
  %23 = load i32, i32* %ci, align 4, !tbaa !6
  %24 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components4 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %24, i32 0, i32 13
  %25 = load i32, i32* %num_components4, align 4, !tbaa !28
  %cmp = icmp slt i32 %23, %25
  br i1 %cmp, label %for.body, label %for.end27

for.body:                                         ; preds = %for.cond
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem5 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 1
  %27 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem5, align 4, !tbaa !16
  %alloc_sarray = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %27, i32 0, i32 2
  %28 = load i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)** %alloc_sarray, align 4, !tbaa !29
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %30 = bitcast %struct.jpeg_compress_struct* %29 to %struct.jpeg_common_struct*
  %31 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %31, i32 0, i32 7
  %32 = load i32, i32* %width_in_blocks, align 4, !tbaa !30
  %mul6 = mul nsw i32 %32, 8
  %33 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_h_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %33, i32 0, i32 41
  %34 = load i32, i32* %max_h_samp_factor, align 8, !tbaa !32
  %mul7 = mul nsw i32 %mul6, %34
  %35 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %h_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %35, i32 0, i32 2
  %36 = load i32, i32* %h_samp_factor, align 4, !tbaa !33
  %div = sdiv i32 %mul7, %36
  %37 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %mul8 = mul nsw i32 3, %37
  %call9 = call i8** %28(%struct.jpeg_common_struct* %30, i32 1, i32 %div, i32 %mul8)
  store i8** %call9, i8*** %true_buffer, align 4, !tbaa !2
  %38 = load i8**, i8*** %fake_buffer, align 4, !tbaa !2
  %39 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8*, i8** %38, i32 %39
  %40 = bitcast i8** %add.ptr to i8*
  %41 = load i8**, i8*** %true_buffer, align 4, !tbaa !2
  %42 = bitcast i8** %41 to i8*
  %43 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %mul10 = mul nsw i32 3, %43
  %mul11 = mul i32 %mul10, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %40, i8* align 1 %42, i32 %mul11, i1 false)
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %for.body
  %44 = load i32, i32* %i, align 4, !tbaa !6
  %45 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %44, %45
  br i1 %cmp13, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond12
  %46 = load i8**, i8*** %true_buffer, align 4, !tbaa !2
  %47 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %mul15 = mul nsw i32 2, %47
  %48 = load i32, i32* %i, align 4, !tbaa !6
  %add = add nsw i32 %mul15, %48
  %arrayidx = getelementptr inbounds i8*, i8** %46, i32 %add
  %49 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  %50 = load i8**, i8*** %fake_buffer, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx16 = getelementptr inbounds i8*, i8** %50, i32 %51
  store i8* %49, i8** %arrayidx16, align 4, !tbaa !2
  %52 = load i8**, i8*** %true_buffer, align 4, !tbaa !2
  %53 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx17 = getelementptr inbounds i8*, i8** %52, i32 %53
  %54 = load i8*, i8** %arrayidx17, align 4, !tbaa !2
  %55 = load i8**, i8*** %fake_buffer, align 4, !tbaa !2
  %56 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %mul18 = mul nsw i32 4, %56
  %57 = load i32, i32* %i, align 4, !tbaa !6
  %add19 = add nsw i32 %mul18, %57
  %arrayidx20 = getelementptr inbounds i8*, i8** %55, i32 %add19
  store i8* %54, i8** %arrayidx20, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond12

for.end:                                          ; preds = %for.cond12
  %59 = load i8**, i8*** %fake_buffer, align 4, !tbaa !2
  %60 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %add.ptr21 = getelementptr inbounds i8*, i8** %59, i32 %60
  %61 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %61, i32 0, i32 1
  %62 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx22 = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf, i32 0, i32 %62
  store i8** %add.ptr21, i8*** %arrayidx22, align 4, !tbaa !2
  %63 = load i32, i32* %rgroup_height, align 4, !tbaa !6
  %mul23 = mul nsw i32 5, %63
  %64 = load i8**, i8*** %fake_buffer, align 4, !tbaa !2
  %add.ptr24 = getelementptr inbounds i8*, i8** %64, i32 %mul23
  store i8** %add.ptr24, i8*** %fake_buffer, align 4, !tbaa !2
  br label %for.inc25

for.inc25:                                        ; preds = %for.end
  %65 = load i32, i32* %ci, align 4, !tbaa !6
  %inc26 = add nsw i32 %65, 1
  store i32 %inc26, i32* %ci, align 4, !tbaa !6
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end27:                                        ; preds = %for.cond
  %67 = bitcast i8*** %fake_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #3
  %68 = bitcast i8*** %true_buffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #3
  %69 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #3
  %70 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #3
  %71 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #3
  %72 = bitcast i32* %rgroup_height to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #3
  %73 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #3
  ret void
}

; Function Attrs: nounwind
define internal void @pre_process_data(%struct.jpeg_compress_struct* %cinfo, i8** %input_buf, i32* %in_row_ctr, i32 %in_rows_avail, i8*** %output_buf, i32* %out_row_group_ctr, i32 %out_row_groups_avail) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_compress_struct*, align 4
  %input_buf.addr = alloca i8**, align 4
  %in_row_ctr.addr = alloca i32*, align 4
  %in_rows_avail.addr = alloca i32, align 4
  %output_buf.addr = alloca i8***, align 4
  %out_row_group_ctr.addr = alloca i32*, align 4
  %out_row_groups_avail.addr = alloca i32, align 4
  %prep = alloca %struct.my_prep_controller*, align 4
  %numrows = alloca i32, align 4
  %ci = alloca i32, align 4
  %inrows = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_compress_struct* %cinfo, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  store i8** %input_buf, i8*** %input_buf.addr, align 4, !tbaa !2
  store i32* %in_row_ctr, i32** %in_row_ctr.addr, align 4, !tbaa !2
  store i32 %in_rows_avail, i32* %in_rows_avail.addr, align 4, !tbaa !6
  store i8*** %output_buf, i8**** %output_buf.addr, align 4, !tbaa !2
  store i32* %out_row_group_ctr, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  store i32 %out_row_groups_avail, i32* %out_row_groups_avail.addr, align 4, !tbaa !6
  %0 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %prep1 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %1, i32 0, i32 56
  %2 = load %struct.jpeg_c_prep_controller*, %struct.jpeg_c_prep_controller** %prep1, align 4, !tbaa !19
  %3 = bitcast %struct.jpeg_c_prep_controller* %2 to %struct.my_prep_controller*
  store %struct.my_prep_controller* %3, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %4 = bitcast i32* %numrows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #3
  %5 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #3
  %6 = bitcast i32* %inrows to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #3
  %7 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #3
  br label %while.cond

while.cond:                                       ; preds = %if.end46, %entry
  %8 = load i32*, i32** %in_row_ctr.addr, align 4, !tbaa !2
  %9 = load i32, i32* %8, align 4, !tbaa !6
  %10 = load i32, i32* %in_rows_avail.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %9, %10
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %11 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  %12 = load i32, i32* %11, align 4, !tbaa !6
  %13 = load i32, i32* %out_row_groups_avail.addr, align 4, !tbaa !6
  %cmp2 = icmp ult i32 %12, %13
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %14 = phi i1 [ false, %while.cond ], [ %cmp2, %land.rhs ]
  br i1 %14, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %15 = load i32, i32* %in_rows_avail.addr, align 4, !tbaa !6
  %16 = load i32*, i32** %in_row_ctr.addr, align 4, !tbaa !2
  %17 = load i32, i32* %16, align 4, !tbaa !6
  %sub = sub i32 %15, %17
  store i32 %sub, i32* %inrows, align 4, !tbaa !6
  %18 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %18, i32 0, i32 42
  %19 = load i32, i32* %max_v_samp_factor, align 4, !tbaa !34
  %20 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %20, i32 0, i32 3
  %21 = load i32, i32* %next_buf_row, align 4, !tbaa !38
  %sub3 = sub nsw i32 %19, %21
  store i32 %sub3, i32* %numrows, align 4, !tbaa !6
  %22 = load i32, i32* %numrows, align 4, !tbaa !6
  %23 = load i32, i32* %inrows, align 4, !tbaa !6
  %cmp4 = icmp ult i32 %22, %23
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %24 = load i32, i32* %numrows, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %25 = load i32, i32* %inrows, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %24, %cond.true ], [ %25, %cond.false ]
  store i32 %cond, i32* %numrows, align 4, !tbaa !6
  %26 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %cconvert = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %26, i32 0, i32 59
  %27 = load %struct.jpeg_color_converter*, %struct.jpeg_color_converter** %cconvert, align 8, !tbaa !41
  %color_convert = getelementptr inbounds %struct.jpeg_color_converter, %struct.jpeg_color_converter* %27, i32 0, i32 1
  %28 = load void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)*, void (%struct.jpeg_compress_struct*, i8**, i8***, i32, i32)** %color_convert, align 4, !tbaa !42
  %29 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %30 = load i8**, i8*** %input_buf.addr, align 4, !tbaa !2
  %31 = load i32*, i32** %in_row_ctr.addr, align 4, !tbaa !2
  %32 = load i32, i32* %31, align 4, !tbaa !6
  %add.ptr = getelementptr inbounds i8*, i8** %30, i32 %32
  %33 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %33, i32 0, i32 1
  %arraydecay = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf, i32 0, i32 0
  %34 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row5 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %34, i32 0, i32 3
  %35 = load i32, i32* %next_buf_row5, align 4, !tbaa !38
  %36 = load i32, i32* %numrows, align 4, !tbaa !6
  call void %28(%struct.jpeg_compress_struct* %29, i8** %add.ptr, i8*** %arraydecay, i32 %35, i32 %36)
  %37 = load i32, i32* %numrows, align 4, !tbaa !6
  %38 = load i32*, i32** %in_row_ctr.addr, align 4, !tbaa !2
  %39 = load i32, i32* %38, align 4, !tbaa !6
  %add = add i32 %39, %37
  store i32 %add, i32* %38, align 4, !tbaa !6
  %40 = load i32, i32* %numrows, align 4, !tbaa !6
  %41 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row6 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %41, i32 0, i32 3
  %42 = load i32, i32* %next_buf_row6, align 4, !tbaa !38
  %add7 = add nsw i32 %42, %40
  store i32 %add7, i32* %next_buf_row6, align 4, !tbaa !38
  %43 = load i32, i32* %numrows, align 4, !tbaa !6
  %44 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %rows_to_go = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %44, i32 0, i32 2
  %45 = load i32, i32* %rows_to_go, align 4, !tbaa !37
  %sub8 = sub i32 %45, %43
  store i32 %sub8, i32* %rows_to_go, align 4, !tbaa !37
  %46 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %rows_to_go9 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %46, i32 0, i32 2
  %47 = load i32, i32* %rows_to_go9, align 4, !tbaa !37
  %cmp10 = icmp eq i32 %47, 0
  br i1 %cmp10, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end
  %48 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row11 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %48, i32 0, i32 3
  %49 = load i32, i32* %next_buf_row11, align 4, !tbaa !38
  %50 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor12 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %50, i32 0, i32 42
  %51 = load i32, i32* %max_v_samp_factor12, align 4, !tbaa !34
  %cmp13 = icmp slt i32 %49, %51
  br i1 %cmp13, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %52 = load i32, i32* %ci, align 4, !tbaa !6
  %53 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %53, i32 0, i32 13
  %54 = load i32, i32* %num_components, align 4, !tbaa !28
  %cmp14 = icmp slt i32 %52, %54
  br i1 %cmp14, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %55 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf15 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %55, i32 0, i32 1
  %56 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf15, i32 0, i32 %56
  %57 = load i8**, i8*** %arrayidx, align 4, !tbaa !2
  %58 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %image_width = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %58, i32 0, i32 7
  %59 = load i32, i32* %image_width, align 4, !tbaa !44
  %60 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row16 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %60, i32 0, i32 3
  %61 = load i32, i32* %next_buf_row16, align 4, !tbaa !38
  %62 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor17 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %62, i32 0, i32 42
  %63 = load i32, i32* %max_v_samp_factor17, align 4, !tbaa !34
  call void @expand_bottom_edge(i8** %57, i32 %59, i32 %61, i32 %63)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %64 = load i32, i32* %ci, align 4, !tbaa !6
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %65 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor18 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %65, i32 0, i32 42
  %66 = load i32, i32* %max_v_samp_factor18, align 4, !tbaa !34
  %67 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row19 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %67, i32 0, i32 3
  store i32 %66, i32* %next_buf_row19, align 4, !tbaa !38
  br label %if.end

if.end:                                           ; preds = %for.end, %land.lhs.true, %cond.end
  %68 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row20 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %68, i32 0, i32 3
  %69 = load i32, i32* %next_buf_row20, align 4, !tbaa !38
  %70 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %max_v_samp_factor21 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %70, i32 0, i32 42
  %71 = load i32, i32* %max_v_samp_factor21, align 4, !tbaa !34
  %cmp22 = icmp eq i32 %69, %71
  br i1 %cmp22, label %if.then23, label %if.end29

if.then23:                                        ; preds = %if.end
  %72 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %downsample = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %72, i32 0, i32 60
  %73 = load %struct.jpeg_downsampler*, %struct.jpeg_downsampler** %downsample, align 4, !tbaa !23
  %downsample24 = getelementptr inbounds %struct.jpeg_downsampler, %struct.jpeg_downsampler* %73, i32 0, i32 1
  %74 = load void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)*, void (%struct.jpeg_compress_struct*, i8***, i32, i8***, i32)** %downsample24, align 4, !tbaa !45
  %75 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %76 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %color_buf25 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %76, i32 0, i32 1
  %arraydecay26 = getelementptr inbounds [10 x i8**], [10 x i8**]* %color_buf25, i32 0, i32 0
  %77 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %78 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  %79 = load i32, i32* %78, align 4, !tbaa !6
  call void %74(%struct.jpeg_compress_struct* %75, i8*** %arraydecay26, i32 0, i8*** %77, i32 %79)
  %80 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %next_buf_row27 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %80, i32 0, i32 3
  store i32 0, i32* %next_buf_row27, align 4, !tbaa !38
  %81 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  %82 = load i32, i32* %81, align 4, !tbaa !6
  %inc28 = add i32 %82, 1
  store i32 %inc28, i32* %81, align 4, !tbaa !6
  br label %if.end29

if.end29:                                         ; preds = %if.then23, %if.end
  %83 = load %struct.my_prep_controller*, %struct.my_prep_controller** %prep, align 4, !tbaa !2
  %rows_to_go30 = getelementptr inbounds %struct.my_prep_controller, %struct.my_prep_controller* %83, i32 0, i32 2
  %84 = load i32, i32* %rows_to_go30, align 4, !tbaa !37
  %cmp31 = icmp eq i32 %84, 0
  br i1 %cmp31, label %land.lhs.true32, label %if.end46

land.lhs.true32:                                  ; preds = %if.end29
  %85 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  %86 = load i32, i32* %85, align 4, !tbaa !6
  %87 = load i32, i32* %out_row_groups_avail.addr, align 4, !tbaa !6
  %cmp33 = icmp ult i32 %86, %87
  br i1 %cmp33, label %if.then34, label %if.end46

if.then34:                                        ; preds = %land.lhs.true32
  store i32 0, i32* %ci, align 4, !tbaa !6
  %88 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %88, i32 0, i32 15
  %89 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 4, !tbaa !27
  store %struct.jpeg_component_info* %89, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc43, %if.then34
  %90 = load i32, i32* %ci, align 4, !tbaa !6
  %91 = load %struct.jpeg_compress_struct*, %struct.jpeg_compress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components36 = getelementptr inbounds %struct.jpeg_compress_struct, %struct.jpeg_compress_struct* %91, i32 0, i32 13
  %92 = load i32, i32* %num_components36, align 4, !tbaa !28
  %cmp37 = icmp slt i32 %90, %92
  br i1 %cmp37, label %for.body38, label %for.end45

for.body38:                                       ; preds = %for.cond35
  %93 = load i8***, i8**** %output_buf.addr, align 4, !tbaa !2
  %94 = load i32, i32* %ci, align 4, !tbaa !6
  %arrayidx39 = getelementptr inbounds i8**, i8*** %93, i32 %94
  %95 = load i8**, i8*** %arrayidx39, align 4, !tbaa !2
  %96 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %width_in_blocks = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %96, i32 0, i32 7
  %97 = load i32, i32* %width_in_blocks, align 4, !tbaa !30
  %mul = mul i32 %97, 8
  %98 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  %99 = load i32, i32* %98, align 4, !tbaa !6
  %100 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %100, i32 0, i32 3
  %101 = load i32, i32* %v_samp_factor, align 4, !tbaa !46
  %mul40 = mul i32 %99, %101
  %102 = load i32, i32* %out_row_groups_avail.addr, align 4, !tbaa !6
  %103 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %v_samp_factor41 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %103, i32 0, i32 3
  %104 = load i32, i32* %v_samp_factor41, align 4, !tbaa !46
  %mul42 = mul i32 %102, %104
  call void @expand_bottom_edge(i8** %95, i32 %mul, i32 %mul40, i32 %mul42)
  br label %for.inc43

for.inc43:                                        ; preds = %for.body38
  %105 = load i32, i32* %ci, align 4, !tbaa !6
  %inc44 = add nsw i32 %105, 1
  store i32 %inc44, i32* %ci, align 4, !tbaa !6
  %106 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %106, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond35

for.end45:                                        ; preds = %for.cond35
  %107 = load i32, i32* %out_row_groups_avail.addr, align 4, !tbaa !6
  %108 = load i32*, i32** %out_row_group_ctr.addr, align 4, !tbaa !2
  store i32 %107, i32* %108, align 4, !tbaa !6
  br label %while.end

if.end46:                                         ; preds = %land.lhs.true32, %if.end29
  br label %while.cond

while.end:                                        ; preds = %for.end45, %land.end
  %109 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #3
  %110 = bitcast i32* %inrows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #3
  %111 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #3
  %112 = bitcast i32* %numrows to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #3
  %113 = bitcast %struct.my_prep_controller** %prep to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @jcopy_sample_rows(i8**, i32, i8**, i32, i32, i32) #2

; Function Attrs: nounwind
define internal void @expand_bottom_edge(i8** %image_data, i32 %num_cols, i32 %input_rows, i32 %output_rows) #0 {
entry:
  %image_data.addr = alloca i8**, align 4
  %num_cols.addr = alloca i32, align 4
  %input_rows.addr = alloca i32, align 4
  %output_rows.addr = alloca i32, align 4
  %row = alloca i32, align 4
  store i8** %image_data, i8*** %image_data.addr, align 4, !tbaa !2
  store i32 %num_cols, i32* %num_cols.addr, align 4, !tbaa !6
  store i32 %input_rows, i32* %input_rows.addr, align 4, !tbaa !6
  store i32 %output_rows, i32* %output_rows.addr, align 4, !tbaa !6
  %0 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #3
  %1 = load i32, i32* %input_rows.addr, align 4, !tbaa !6
  store i32 %1, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %row, align 4, !tbaa !6
  %3 = load i32, i32* %output_rows.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8**, i8*** %image_data.addr, align 4, !tbaa !2
  %5 = load i32, i32* %input_rows.addr, align 4, !tbaa !6
  %sub = sub nsw i32 %5, 1
  %6 = load i8**, i8*** %image_data.addr, align 4, !tbaa !2
  %7 = load i32, i32* %row, align 4, !tbaa !6
  %8 = load i32, i32* %num_cols.addr, align 4, !tbaa !6
  call void @jcopy_sample_rows(i8** %4, i32 %sub, i8** %6, i32 %7, i32 1, i32 %8)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #3
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 0}
!9 = !{!"jpeg_compress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20, !3, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !4, i64 40, !10, i64 48, !7, i64 56, !7, i64 60, !4, i64 64, !3, i64 68, !4, i64 72, !4, i64 88, !4, i64 104, !4, i64 120, !4, i64 136, !4, i64 152, !7, i64 168, !3, i64 172, !7, i64 176, !7, i64 180, !7, i64 184, !7, i64 188, !7, i64 192, !4, i64 196, !7, i64 200, !7, i64 204, !7, i64 208, !4, i64 212, !4, i64 213, !4, i64 214, !11, i64 216, !11, i64 218, !7, i64 220, !7, i64 224, !7, i64 228, !7, i64 232, !7, i64 236, !7, i64 240, !7, i64 244, !4, i64 248, !7, i64 264, !7, i64 268, !7, i64 272, !4, i64 276, !7, i64 316, !7, i64 320, !7, i64 324, !7, i64 328, !3, i64 332, !3, i64 336, !3, i64 340, !3, i64 344, !3, i64 348, !3, i64 352, !3, i64 356, !3, i64 360, !3, i64 364, !3, i64 368, !7, i64 372}
!10 = !{!"double", !4, i64 0}
!11 = !{!"short", !4, i64 0}
!12 = !{!13, !7, i64 20}
!13 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !7, i64 20, !4, i64 24, !7, i64 104, !14, i64 108, !3, i64 112, !7, i64 116, !3, i64 120, !7, i64 124, !7, i64 128}
!14 = !{!"long", !4, i64 0}
!15 = !{!13, !3, i64 0}
!16 = !{!9, !3, i64 4}
!17 = !{!18, !3, i64 0}
!18 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !14, i64 44, !14, i64 48}
!19 = !{!9, !3, i64 340}
!20 = !{!21, !3, i64 0}
!21 = !{!"", !22, i64 0, !4, i64 8, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60}
!22 = !{!"jpeg_c_prep_controller", !3, i64 0, !3, i64 4}
!23 = !{!9, !3, i64 356}
!24 = !{!25, !7, i64 8}
!25 = !{!"jpeg_downsampler", !3, i64 0, !3, i64 4, !7, i64 8}
!26 = !{!21, !3, i64 4}
!27 = !{!9, !3, i64 68}
!28 = !{!9, !7, i64 60}
!29 = !{!18, !3, i64 8}
!30 = !{!31, !7, i64 28}
!31 = !{!"", !7, i64 0, !7, i64 4, !7, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !7, i64 36, !7, i64 40, !7, i64 44, !7, i64 48, !7, i64 52, !7, i64 56, !7, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !3, i64 76, !3, i64 80}
!32 = !{!9, !7, i64 232}
!33 = !{!31, !7, i64 8}
!34 = !{!9, !7, i64 236}
!35 = !{!4, !4, i64 0}
!36 = !{!9, !7, i64 32}
!37 = !{!21, !7, i64 48}
!38 = !{!21, !7, i64 52}
!39 = !{!21, !7, i64 56}
!40 = !{!21, !7, i64 60}
!41 = !{!9, !3, i64 352}
!42 = !{!43, !3, i64 4}
!43 = !{!"jpeg_color_converter", !3, i64 0, !3, i64 4}
!44 = !{!9, !7, i64 28}
!45 = !{!25, !3, i64 4}
!46 = !{!31, !7, i64 12}
