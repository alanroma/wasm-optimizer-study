; ModuleID = 'jfdctint.c'
source_filename = "jfdctint.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: nounwind
define hidden void @jpeg_fdct_islow(i32* %data) #0 {
entry:
  %data.addr = alloca i32*, align 4
  %tmp0 = alloca i32, align 4
  %tmp1 = alloca i32, align 4
  %tmp2 = alloca i32, align 4
  %tmp3 = alloca i32, align 4
  %tmp4 = alloca i32, align 4
  %tmp5 = alloca i32, align 4
  %tmp6 = alloca i32, align 4
  %tmp7 = alloca i32, align 4
  %tmp10 = alloca i32, align 4
  %tmp11 = alloca i32, align 4
  %tmp12 = alloca i32, align 4
  %tmp13 = alloca i32, align 4
  %z1 = alloca i32, align 4
  %z2 = alloca i32, align 4
  %z3 = alloca i32, align 4
  %z4 = alloca i32, align 4
  %z5 = alloca i32, align 4
  %dataptr = alloca i32*, align 4
  %ctr = alloca i32, align 4
  store i32* %data, i32** %data.addr, align 4, !tbaa !2
  %0 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #2
  %1 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #2
  %2 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #2
  %3 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #2
  %4 = bitcast i32* %tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #2
  %5 = bitcast i32* %tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #2
  %6 = bitcast i32* %tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #2
  %7 = bitcast i32* %tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #2
  %8 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #2
  %9 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #2
  %10 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #2
  %11 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #2
  %12 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #2
  %13 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #2
  %14 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #2
  %15 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #2
  %16 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #2
  %17 = bitcast i32** %dataptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #2
  %18 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #2
  %19 = load i32*, i32** %data.addr, align 4, !tbaa !2
  store i32* %19, i32** %dataptr, align 4, !tbaa !2
  store i32 7, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %20 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp = icmp sge i32 %20, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %21 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %21, i32 0
  %22 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %23 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i32, i32* %23, i32 7
  %24 = load i32, i32* %arrayidx1, align 4, !tbaa !6
  %add = add nsw i32 %22, %24
  store i32 %add, i32* %tmp0, align 4, !tbaa !8
  %25 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i32, i32* %25, i32 0
  %26 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  %27 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i32, i32* %27, i32 7
  %28 = load i32, i32* %arrayidx3, align 4, !tbaa !6
  %sub = sub nsw i32 %26, %28
  store i32 %sub, i32* %tmp7, align 4, !tbaa !8
  %29 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %29, i32 1
  %30 = load i32, i32* %arrayidx4, align 4, !tbaa !6
  %31 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i32, i32* %31, i32 6
  %32 = load i32, i32* %arrayidx5, align 4, !tbaa !6
  %add6 = add nsw i32 %30, %32
  store i32 %add6, i32* %tmp1, align 4, !tbaa !8
  %33 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i32, i32* %33, i32 1
  %34 = load i32, i32* %arrayidx7, align 4, !tbaa !6
  %35 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds i32, i32* %35, i32 6
  %36 = load i32, i32* %arrayidx8, align 4, !tbaa !6
  %sub9 = sub nsw i32 %34, %36
  store i32 %sub9, i32* %tmp6, align 4, !tbaa !8
  %37 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i32, i32* %37, i32 2
  %38 = load i32, i32* %arrayidx10, align 4, !tbaa !6
  %39 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds i32, i32* %39, i32 5
  %40 = load i32, i32* %arrayidx11, align 4, !tbaa !6
  %add12 = add nsw i32 %38, %40
  store i32 %add12, i32* %tmp2, align 4, !tbaa !8
  %41 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds i32, i32* %41, i32 2
  %42 = load i32, i32* %arrayidx13, align 4, !tbaa !6
  %43 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds i32, i32* %43, i32 5
  %44 = load i32, i32* %arrayidx14, align 4, !tbaa !6
  %sub15 = sub nsw i32 %42, %44
  store i32 %sub15, i32* %tmp5, align 4, !tbaa !8
  %45 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %45, i32 3
  %46 = load i32, i32* %arrayidx16, align 4, !tbaa !6
  %47 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i32, i32* %47, i32 4
  %48 = load i32, i32* %arrayidx17, align 4, !tbaa !6
  %add18 = add nsw i32 %46, %48
  store i32 %add18, i32* %tmp3, align 4, !tbaa !8
  %49 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %49, i32 3
  %50 = load i32, i32* %arrayidx19, align 4, !tbaa !6
  %51 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx20 = getelementptr inbounds i32, i32* %51, i32 4
  %52 = load i32, i32* %arrayidx20, align 4, !tbaa !6
  %sub21 = sub nsw i32 %50, %52
  store i32 %sub21, i32* %tmp4, align 4, !tbaa !8
  %53 = load i32, i32* %tmp0, align 4, !tbaa !8
  %54 = load i32, i32* %tmp3, align 4, !tbaa !8
  %add22 = add nsw i32 %53, %54
  store i32 %add22, i32* %tmp10, align 4, !tbaa !8
  %55 = load i32, i32* %tmp0, align 4, !tbaa !8
  %56 = load i32, i32* %tmp3, align 4, !tbaa !8
  %sub23 = sub nsw i32 %55, %56
  store i32 %sub23, i32* %tmp13, align 4, !tbaa !8
  %57 = load i32, i32* %tmp1, align 4, !tbaa !8
  %58 = load i32, i32* %tmp2, align 4, !tbaa !8
  %add24 = add nsw i32 %57, %58
  store i32 %add24, i32* %tmp11, align 4, !tbaa !8
  %59 = load i32, i32* %tmp1, align 4, !tbaa !8
  %60 = load i32, i32* %tmp2, align 4, !tbaa !8
  %sub25 = sub nsw i32 %59, %60
  store i32 %sub25, i32* %tmp12, align 4, !tbaa !8
  %61 = load i32, i32* %tmp10, align 4, !tbaa !8
  %62 = load i32, i32* %tmp11, align 4, !tbaa !8
  %add26 = add nsw i32 %61, %62
  %shl = shl i32 %add26, 2
  %63 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx27 = getelementptr inbounds i32, i32* %63, i32 0
  store i32 %shl, i32* %arrayidx27, align 4, !tbaa !6
  %64 = load i32, i32* %tmp10, align 4, !tbaa !8
  %65 = load i32, i32* %tmp11, align 4, !tbaa !8
  %sub28 = sub nsw i32 %64, %65
  %shl29 = shl i32 %sub28, 2
  %66 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds i32, i32* %66, i32 4
  store i32 %shl29, i32* %arrayidx30, align 4, !tbaa !6
  %67 = load i32, i32* %tmp12, align 4, !tbaa !8
  %68 = load i32, i32* %tmp13, align 4, !tbaa !8
  %add31 = add nsw i32 %67, %68
  %mul = mul nsw i32 %add31, 4433
  store i32 %mul, i32* %z1, align 4, !tbaa !8
  %69 = load i32, i32* %z1, align 4, !tbaa !8
  %70 = load i32, i32* %tmp13, align 4, !tbaa !8
  %mul32 = mul nsw i32 %70, 6270
  %add33 = add nsw i32 %69, %mul32
  %add34 = add nsw i32 %add33, 1024
  %shr = ashr i32 %add34, 11
  %71 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx35 = getelementptr inbounds i32, i32* %71, i32 2
  store i32 %shr, i32* %arrayidx35, align 4, !tbaa !6
  %72 = load i32, i32* %z1, align 4, !tbaa !8
  %73 = load i32, i32* %tmp12, align 4, !tbaa !8
  %mul36 = mul nsw i32 %73, -15137
  %add37 = add nsw i32 %72, %mul36
  %add38 = add nsw i32 %add37, 1024
  %shr39 = ashr i32 %add38, 11
  %74 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx40 = getelementptr inbounds i32, i32* %74, i32 6
  store i32 %shr39, i32* %arrayidx40, align 4, !tbaa !6
  %75 = load i32, i32* %tmp4, align 4, !tbaa !8
  %76 = load i32, i32* %tmp7, align 4, !tbaa !8
  %add41 = add nsw i32 %75, %76
  store i32 %add41, i32* %z1, align 4, !tbaa !8
  %77 = load i32, i32* %tmp5, align 4, !tbaa !8
  %78 = load i32, i32* %tmp6, align 4, !tbaa !8
  %add42 = add nsw i32 %77, %78
  store i32 %add42, i32* %z2, align 4, !tbaa !8
  %79 = load i32, i32* %tmp4, align 4, !tbaa !8
  %80 = load i32, i32* %tmp6, align 4, !tbaa !8
  %add43 = add nsw i32 %79, %80
  store i32 %add43, i32* %z3, align 4, !tbaa !8
  %81 = load i32, i32* %tmp5, align 4, !tbaa !8
  %82 = load i32, i32* %tmp7, align 4, !tbaa !8
  %add44 = add nsw i32 %81, %82
  store i32 %add44, i32* %z4, align 4, !tbaa !8
  %83 = load i32, i32* %z3, align 4, !tbaa !8
  %84 = load i32, i32* %z4, align 4, !tbaa !8
  %add45 = add nsw i32 %83, %84
  %mul46 = mul nsw i32 %add45, 9633
  store i32 %mul46, i32* %z5, align 4, !tbaa !8
  %85 = load i32, i32* %tmp4, align 4, !tbaa !8
  %mul47 = mul nsw i32 %85, 2446
  store i32 %mul47, i32* %tmp4, align 4, !tbaa !8
  %86 = load i32, i32* %tmp5, align 4, !tbaa !8
  %mul48 = mul nsw i32 %86, 16819
  store i32 %mul48, i32* %tmp5, align 4, !tbaa !8
  %87 = load i32, i32* %tmp6, align 4, !tbaa !8
  %mul49 = mul nsw i32 %87, 25172
  store i32 %mul49, i32* %tmp6, align 4, !tbaa !8
  %88 = load i32, i32* %tmp7, align 4, !tbaa !8
  %mul50 = mul nsw i32 %88, 12299
  store i32 %mul50, i32* %tmp7, align 4, !tbaa !8
  %89 = load i32, i32* %z1, align 4, !tbaa !8
  %mul51 = mul nsw i32 %89, -7373
  store i32 %mul51, i32* %z1, align 4, !tbaa !8
  %90 = load i32, i32* %z2, align 4, !tbaa !8
  %mul52 = mul nsw i32 %90, -20995
  store i32 %mul52, i32* %z2, align 4, !tbaa !8
  %91 = load i32, i32* %z3, align 4, !tbaa !8
  %mul53 = mul nsw i32 %91, -16069
  store i32 %mul53, i32* %z3, align 4, !tbaa !8
  %92 = load i32, i32* %z4, align 4, !tbaa !8
  %mul54 = mul nsw i32 %92, -3196
  store i32 %mul54, i32* %z4, align 4, !tbaa !8
  %93 = load i32, i32* %z5, align 4, !tbaa !8
  %94 = load i32, i32* %z3, align 4, !tbaa !8
  %add55 = add nsw i32 %94, %93
  store i32 %add55, i32* %z3, align 4, !tbaa !8
  %95 = load i32, i32* %z5, align 4, !tbaa !8
  %96 = load i32, i32* %z4, align 4, !tbaa !8
  %add56 = add nsw i32 %96, %95
  store i32 %add56, i32* %z4, align 4, !tbaa !8
  %97 = load i32, i32* %tmp4, align 4, !tbaa !8
  %98 = load i32, i32* %z1, align 4, !tbaa !8
  %add57 = add nsw i32 %97, %98
  %99 = load i32, i32* %z3, align 4, !tbaa !8
  %add58 = add nsw i32 %add57, %99
  %add59 = add nsw i32 %add58, 1024
  %shr60 = ashr i32 %add59, 11
  %100 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx61 = getelementptr inbounds i32, i32* %100, i32 7
  store i32 %shr60, i32* %arrayidx61, align 4, !tbaa !6
  %101 = load i32, i32* %tmp5, align 4, !tbaa !8
  %102 = load i32, i32* %z2, align 4, !tbaa !8
  %add62 = add nsw i32 %101, %102
  %103 = load i32, i32* %z4, align 4, !tbaa !8
  %add63 = add nsw i32 %add62, %103
  %add64 = add nsw i32 %add63, 1024
  %shr65 = ashr i32 %add64, 11
  %104 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i32, i32* %104, i32 5
  store i32 %shr65, i32* %arrayidx66, align 4, !tbaa !6
  %105 = load i32, i32* %tmp6, align 4, !tbaa !8
  %106 = load i32, i32* %z2, align 4, !tbaa !8
  %add67 = add nsw i32 %105, %106
  %107 = load i32, i32* %z3, align 4, !tbaa !8
  %add68 = add nsw i32 %add67, %107
  %add69 = add nsw i32 %add68, 1024
  %shr70 = ashr i32 %add69, 11
  %108 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i32, i32* %108, i32 3
  store i32 %shr70, i32* %arrayidx71, align 4, !tbaa !6
  %109 = load i32, i32* %tmp7, align 4, !tbaa !8
  %110 = load i32, i32* %z1, align 4, !tbaa !8
  %add72 = add nsw i32 %109, %110
  %111 = load i32, i32* %z4, align 4, !tbaa !8
  %add73 = add nsw i32 %add72, %111
  %add74 = add nsw i32 %add73, 1024
  %shr75 = ashr i32 %add74, 11
  %112 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds i32, i32* %112, i32 1
  store i32 %shr75, i32* %arrayidx76, align 4, !tbaa !6
  %113 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %113, i32 8
  store i32* %add.ptr, i32** %dataptr, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %114 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec = add nsw i32 %114, -1
  store i32 %dec, i32* %ctr, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %115 = load i32*, i32** %data.addr, align 4, !tbaa !2
  store i32* %115, i32** %dataptr, align 4, !tbaa !2
  store i32 7, i32* %ctr, align 4, !tbaa !6
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc164, %for.end
  %116 = load i32, i32* %ctr, align 4, !tbaa !6
  %cmp78 = icmp sge i32 %116, 0
  br i1 %cmp78, label %for.body79, label %for.end166

for.body79:                                       ; preds = %for.cond77
  %117 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds i32, i32* %117, i32 0
  %118 = load i32, i32* %arrayidx80, align 4, !tbaa !6
  %119 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx81 = getelementptr inbounds i32, i32* %119, i32 56
  %120 = load i32, i32* %arrayidx81, align 4, !tbaa !6
  %add82 = add nsw i32 %118, %120
  store i32 %add82, i32* %tmp0, align 4, !tbaa !8
  %121 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx83 = getelementptr inbounds i32, i32* %121, i32 0
  %122 = load i32, i32* %arrayidx83, align 4, !tbaa !6
  %123 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds i32, i32* %123, i32 56
  %124 = load i32, i32* %arrayidx84, align 4, !tbaa !6
  %sub85 = sub nsw i32 %122, %124
  store i32 %sub85, i32* %tmp7, align 4, !tbaa !8
  %125 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds i32, i32* %125, i32 8
  %126 = load i32, i32* %arrayidx86, align 4, !tbaa !6
  %127 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx87 = getelementptr inbounds i32, i32* %127, i32 48
  %128 = load i32, i32* %arrayidx87, align 4, !tbaa !6
  %add88 = add nsw i32 %126, %128
  store i32 %add88, i32* %tmp1, align 4, !tbaa !8
  %129 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx89 = getelementptr inbounds i32, i32* %129, i32 8
  %130 = load i32, i32* %arrayidx89, align 4, !tbaa !6
  %131 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds i32, i32* %131, i32 48
  %132 = load i32, i32* %arrayidx90, align 4, !tbaa !6
  %sub91 = sub nsw i32 %130, %132
  store i32 %sub91, i32* %tmp6, align 4, !tbaa !8
  %133 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds i32, i32* %133, i32 16
  %134 = load i32, i32* %arrayidx92, align 4, !tbaa !6
  %135 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx93 = getelementptr inbounds i32, i32* %135, i32 40
  %136 = load i32, i32* %arrayidx93, align 4, !tbaa !6
  %add94 = add nsw i32 %134, %136
  store i32 %add94, i32* %tmp2, align 4, !tbaa !8
  %137 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx95 = getelementptr inbounds i32, i32* %137, i32 16
  %138 = load i32, i32* %arrayidx95, align 4, !tbaa !6
  %139 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds i32, i32* %139, i32 40
  %140 = load i32, i32* %arrayidx96, align 4, !tbaa !6
  %sub97 = sub nsw i32 %138, %140
  store i32 %sub97, i32* %tmp5, align 4, !tbaa !8
  %141 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx98 = getelementptr inbounds i32, i32* %141, i32 24
  %142 = load i32, i32* %arrayidx98, align 4, !tbaa !6
  %143 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i32, i32* %143, i32 32
  %144 = load i32, i32* %arrayidx99, align 4, !tbaa !6
  %add100 = add nsw i32 %142, %144
  store i32 %add100, i32* %tmp3, align 4, !tbaa !8
  %145 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i32, i32* %145, i32 24
  %146 = load i32, i32* %arrayidx101, align 4, !tbaa !6
  %147 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx102 = getelementptr inbounds i32, i32* %147, i32 32
  %148 = load i32, i32* %arrayidx102, align 4, !tbaa !6
  %sub103 = sub nsw i32 %146, %148
  store i32 %sub103, i32* %tmp4, align 4, !tbaa !8
  %149 = load i32, i32* %tmp0, align 4, !tbaa !8
  %150 = load i32, i32* %tmp3, align 4, !tbaa !8
  %add104 = add nsw i32 %149, %150
  store i32 %add104, i32* %tmp10, align 4, !tbaa !8
  %151 = load i32, i32* %tmp0, align 4, !tbaa !8
  %152 = load i32, i32* %tmp3, align 4, !tbaa !8
  %sub105 = sub nsw i32 %151, %152
  store i32 %sub105, i32* %tmp13, align 4, !tbaa !8
  %153 = load i32, i32* %tmp1, align 4, !tbaa !8
  %154 = load i32, i32* %tmp2, align 4, !tbaa !8
  %add106 = add nsw i32 %153, %154
  store i32 %add106, i32* %tmp11, align 4, !tbaa !8
  %155 = load i32, i32* %tmp1, align 4, !tbaa !8
  %156 = load i32, i32* %tmp2, align 4, !tbaa !8
  %sub107 = sub nsw i32 %155, %156
  store i32 %sub107, i32* %tmp12, align 4, !tbaa !8
  %157 = load i32, i32* %tmp10, align 4, !tbaa !8
  %158 = load i32, i32* %tmp11, align 4, !tbaa !8
  %add108 = add nsw i32 %157, %158
  %add109 = add nsw i32 %add108, 2
  %shr110 = ashr i32 %add109, 2
  %159 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx111 = getelementptr inbounds i32, i32* %159, i32 0
  store i32 %shr110, i32* %arrayidx111, align 4, !tbaa !6
  %160 = load i32, i32* %tmp10, align 4, !tbaa !8
  %161 = load i32, i32* %tmp11, align 4, !tbaa !8
  %sub112 = sub nsw i32 %160, %161
  %add113 = add nsw i32 %sub112, 2
  %shr114 = ashr i32 %add113, 2
  %162 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx115 = getelementptr inbounds i32, i32* %162, i32 32
  store i32 %shr114, i32* %arrayidx115, align 4, !tbaa !6
  %163 = load i32, i32* %tmp12, align 4, !tbaa !8
  %164 = load i32, i32* %tmp13, align 4, !tbaa !8
  %add116 = add nsw i32 %163, %164
  %mul117 = mul nsw i32 %add116, 4433
  store i32 %mul117, i32* %z1, align 4, !tbaa !8
  %165 = load i32, i32* %z1, align 4, !tbaa !8
  %166 = load i32, i32* %tmp13, align 4, !tbaa !8
  %mul118 = mul nsw i32 %166, 6270
  %add119 = add nsw i32 %165, %mul118
  %add120 = add nsw i32 %add119, 16384
  %shr121 = ashr i32 %add120, 15
  %167 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx122 = getelementptr inbounds i32, i32* %167, i32 16
  store i32 %shr121, i32* %arrayidx122, align 4, !tbaa !6
  %168 = load i32, i32* %z1, align 4, !tbaa !8
  %169 = load i32, i32* %tmp12, align 4, !tbaa !8
  %mul123 = mul nsw i32 %169, -15137
  %add124 = add nsw i32 %168, %mul123
  %add125 = add nsw i32 %add124, 16384
  %shr126 = ashr i32 %add125, 15
  %170 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx127 = getelementptr inbounds i32, i32* %170, i32 48
  store i32 %shr126, i32* %arrayidx127, align 4, !tbaa !6
  %171 = load i32, i32* %tmp4, align 4, !tbaa !8
  %172 = load i32, i32* %tmp7, align 4, !tbaa !8
  %add128 = add nsw i32 %171, %172
  store i32 %add128, i32* %z1, align 4, !tbaa !8
  %173 = load i32, i32* %tmp5, align 4, !tbaa !8
  %174 = load i32, i32* %tmp6, align 4, !tbaa !8
  %add129 = add nsw i32 %173, %174
  store i32 %add129, i32* %z2, align 4, !tbaa !8
  %175 = load i32, i32* %tmp4, align 4, !tbaa !8
  %176 = load i32, i32* %tmp6, align 4, !tbaa !8
  %add130 = add nsw i32 %175, %176
  store i32 %add130, i32* %z3, align 4, !tbaa !8
  %177 = load i32, i32* %tmp5, align 4, !tbaa !8
  %178 = load i32, i32* %tmp7, align 4, !tbaa !8
  %add131 = add nsw i32 %177, %178
  store i32 %add131, i32* %z4, align 4, !tbaa !8
  %179 = load i32, i32* %z3, align 4, !tbaa !8
  %180 = load i32, i32* %z4, align 4, !tbaa !8
  %add132 = add nsw i32 %179, %180
  %mul133 = mul nsw i32 %add132, 9633
  store i32 %mul133, i32* %z5, align 4, !tbaa !8
  %181 = load i32, i32* %tmp4, align 4, !tbaa !8
  %mul134 = mul nsw i32 %181, 2446
  store i32 %mul134, i32* %tmp4, align 4, !tbaa !8
  %182 = load i32, i32* %tmp5, align 4, !tbaa !8
  %mul135 = mul nsw i32 %182, 16819
  store i32 %mul135, i32* %tmp5, align 4, !tbaa !8
  %183 = load i32, i32* %tmp6, align 4, !tbaa !8
  %mul136 = mul nsw i32 %183, 25172
  store i32 %mul136, i32* %tmp6, align 4, !tbaa !8
  %184 = load i32, i32* %tmp7, align 4, !tbaa !8
  %mul137 = mul nsw i32 %184, 12299
  store i32 %mul137, i32* %tmp7, align 4, !tbaa !8
  %185 = load i32, i32* %z1, align 4, !tbaa !8
  %mul138 = mul nsw i32 %185, -7373
  store i32 %mul138, i32* %z1, align 4, !tbaa !8
  %186 = load i32, i32* %z2, align 4, !tbaa !8
  %mul139 = mul nsw i32 %186, -20995
  store i32 %mul139, i32* %z2, align 4, !tbaa !8
  %187 = load i32, i32* %z3, align 4, !tbaa !8
  %mul140 = mul nsw i32 %187, -16069
  store i32 %mul140, i32* %z3, align 4, !tbaa !8
  %188 = load i32, i32* %z4, align 4, !tbaa !8
  %mul141 = mul nsw i32 %188, -3196
  store i32 %mul141, i32* %z4, align 4, !tbaa !8
  %189 = load i32, i32* %z5, align 4, !tbaa !8
  %190 = load i32, i32* %z3, align 4, !tbaa !8
  %add142 = add nsw i32 %190, %189
  store i32 %add142, i32* %z3, align 4, !tbaa !8
  %191 = load i32, i32* %z5, align 4, !tbaa !8
  %192 = load i32, i32* %z4, align 4, !tbaa !8
  %add143 = add nsw i32 %192, %191
  store i32 %add143, i32* %z4, align 4, !tbaa !8
  %193 = load i32, i32* %tmp4, align 4, !tbaa !8
  %194 = load i32, i32* %z1, align 4, !tbaa !8
  %add144 = add nsw i32 %193, %194
  %195 = load i32, i32* %z3, align 4, !tbaa !8
  %add145 = add nsw i32 %add144, %195
  %add146 = add nsw i32 %add145, 16384
  %shr147 = ashr i32 %add146, 15
  %196 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx148 = getelementptr inbounds i32, i32* %196, i32 56
  store i32 %shr147, i32* %arrayidx148, align 4, !tbaa !6
  %197 = load i32, i32* %tmp5, align 4, !tbaa !8
  %198 = load i32, i32* %z2, align 4, !tbaa !8
  %add149 = add nsw i32 %197, %198
  %199 = load i32, i32* %z4, align 4, !tbaa !8
  %add150 = add nsw i32 %add149, %199
  %add151 = add nsw i32 %add150, 16384
  %shr152 = ashr i32 %add151, 15
  %200 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx153 = getelementptr inbounds i32, i32* %200, i32 40
  store i32 %shr152, i32* %arrayidx153, align 4, !tbaa !6
  %201 = load i32, i32* %tmp6, align 4, !tbaa !8
  %202 = load i32, i32* %z2, align 4, !tbaa !8
  %add154 = add nsw i32 %201, %202
  %203 = load i32, i32* %z3, align 4, !tbaa !8
  %add155 = add nsw i32 %add154, %203
  %add156 = add nsw i32 %add155, 16384
  %shr157 = ashr i32 %add156, 15
  %204 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx158 = getelementptr inbounds i32, i32* %204, i32 24
  store i32 %shr157, i32* %arrayidx158, align 4, !tbaa !6
  %205 = load i32, i32* %tmp7, align 4, !tbaa !8
  %206 = load i32, i32* %z1, align 4, !tbaa !8
  %add159 = add nsw i32 %205, %206
  %207 = load i32, i32* %z4, align 4, !tbaa !8
  %add160 = add nsw i32 %add159, %207
  %add161 = add nsw i32 %add160, 16384
  %shr162 = ashr i32 %add161, 15
  %208 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds i32, i32* %208, i32 8
  store i32 %shr162, i32* %arrayidx163, align 4, !tbaa !6
  %209 = load i32*, i32** %dataptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds i32, i32* %209, i32 1
  store i32* %incdec.ptr, i32** %dataptr, align 4, !tbaa !2
  br label %for.inc164

for.inc164:                                       ; preds = %for.body79
  %210 = load i32, i32* %ctr, align 4, !tbaa !6
  %dec165 = add nsw i32 %210, -1
  store i32 %dec165, i32* %ctr, align 4, !tbaa !6
  br label %for.cond77

for.end166:                                       ; preds = %for.cond77
  %211 = bitcast i32* %ctr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #2
  %212 = bitcast i32** %dataptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #2
  %213 = bitcast i32* %z5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #2
  %214 = bitcast i32* %z4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #2
  %215 = bitcast i32* %z3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #2
  %216 = bitcast i32* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #2
  %217 = bitcast i32* %z1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #2
  %218 = bitcast i32* %tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #2
  %219 = bitcast i32* %tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #2
  %220 = bitcast i32* %tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #2
  %221 = bitcast i32* %tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #2
  %222 = bitcast i32* %tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #2
  %223 = bitcast i32* %tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #2
  %224 = bitcast i32* %tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #2
  %225 = bitcast i32* %tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #2
  %226 = bitcast i32* %tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #2
  %227 = bitcast i32* %tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #2
  %228 = bitcast i32* %tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #2
  %229 = bitcast i32* %tmp0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #2
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long", !4, i64 0}
