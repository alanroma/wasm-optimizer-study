; ModuleID = 'jddctmgr.c'
source_filename = "jddctmgr.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.jpeg_decompress_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32, %struct.jpeg_source_mgr*, i32, i32, i32, i32, i32, i32, i32, double, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i8**, i32, i32, i32, i32, i32, [64 x i32]*, [4 x %struct.JQUANT_TBL*], [4 x %struct.JHUFF_TBL*], [4 x %struct.JHUFF_TBL*], i32, %struct.jpeg_component_info*, i32, i32, [16 x i8], [16 x i8], [16 x i8], i32, i32, i8, i8, i8, i16, i16, i32, i8, i32, %struct.jpeg_marker_struct*, i32, i32, i32, i32, i8*, i32, [4 x %struct.jpeg_component_info*], i32, i32, i32, [10 x i32], i32, i32, i32, i32, i32, %struct.jpeg_decomp_master*, %struct.jpeg_d_main_controller*, %struct.jpeg_d_coef_controller*, %struct.jpeg_d_post_controller*, %struct.jpeg_input_controller*, %struct.jpeg_marker_reader*, %struct.jpeg_entropy_decoder*, %struct.jpeg_inverse_dct*, %struct.jpeg_upsampler*, %struct.jpeg_color_deconverter*, %struct.jpeg_color_quantizer* }
%struct.jpeg_error_mgr = type { void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i32)*, void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*, i8*)*, void (%struct.jpeg_common_struct*)*, i32, %union.anon, i32, i32, i8**, i32, i8**, i32, i32 }
%struct.jpeg_common_struct = type { %struct.jpeg_error_mgr*, %struct.jpeg_memory_mgr*, %struct.jpeg_progress_mgr*, i8*, i32, i32 }
%union.anon = type { [8 x i32], [48 x i8] }
%struct.jpeg_memory_mgr = type { i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)*, i8** (%struct.jpeg_common_struct*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, i32, i32, i32)*, %struct.jvirt_sarray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, %struct.jvirt_barray_control* (%struct.jpeg_common_struct*, i32, i32, i32, i32, i32)*, {}*, i8** (%struct.jpeg_common_struct*, %struct.jvirt_sarray_control*, i32, i32, i32)*, [64 x i16]** (%struct.jpeg_common_struct*, %struct.jvirt_barray_control*, i32, i32, i32)*, void (%struct.jpeg_common_struct*, i32)*, {}*, i32, i32 }
%struct.jvirt_sarray_control = type opaque
%struct.jvirt_barray_control = type opaque
%struct.jpeg_progress_mgr = type { {}*, i32, i32, i32, i32 }
%struct.jpeg_source_mgr = type { i8*, i32, {}*, i32 (%struct.jpeg_decompress_struct*)*, void (%struct.jpeg_decompress_struct*, i32)*, i32 (%struct.jpeg_decompress_struct*, i32)*, {}* }
%struct.JQUANT_TBL = type { [64 x i16], i32 }
%struct.JHUFF_TBL = type { [17 x i8], [256 x i8], i32 }
%struct.jpeg_component_info = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.JQUANT_TBL*, i8* }
%struct.jpeg_marker_struct = type { %struct.jpeg_marker_struct*, i8, i32, i32, i8* }
%struct.jpeg_decomp_master = type { {}*, {}*, i32, i32, i32, [10 x i32], [10 x i32], i32 }
%struct.jpeg_d_main_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i32*, i32)* }
%struct.jpeg_d_coef_controller = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, {}*, i32 (%struct.jpeg_decompress_struct*, i8***)*, %struct.jvirt_barray_control** }
%struct.jpeg_d_post_controller = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)* }
%struct.jpeg_input_controller = type { i32 (%struct.jpeg_decompress_struct*)*, {}*, {}*, {}*, i32, i32 }
%struct.jpeg_marker_reader = type { {}*, i32 (%struct.jpeg_decompress_struct*)*, i32 (%struct.jpeg_decompress_struct*)*, i32, i32, i32, i32 }
%struct.jpeg_entropy_decoder = type { {}*, i32 (%struct.jpeg_decompress_struct*, [64 x i16]**)*, i32 }
%struct.jpeg_inverse_dct = type { {}*, [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*] }
%struct.jpeg_upsampler = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32*, i32, i8**, i32*, i32)*, i32 }
%struct.jpeg_color_deconverter = type { {}*, void (%struct.jpeg_decompress_struct*, i8***, i32, i8**, i32)* }
%struct.jpeg_color_quantizer = type { void (%struct.jpeg_decompress_struct*, i32)*, void (%struct.jpeg_decompress_struct*, i8**, i8**, i32)*, {}*, {}* }
%struct.my_idct_controller = type { %struct.jpeg_inverse_dct, [10 x i32] }

@start_pass.aanscales = internal constant [64 x i16] [i16 16384, i16 22725, i16 21407, i16 19266, i16 16384, i16 12873, i16 8867, i16 4520, i16 22725, i16 31521, i16 29692, i16 26722, i16 22725, i16 17855, i16 12299, i16 6270, i16 21407, i16 29692, i16 27969, i16 25172, i16 21407, i16 16819, i16 11585, i16 5906, i16 19266, i16 26722, i16 25172, i16 22654, i16 19266, i16 15137, i16 10426, i16 5315, i16 16384, i16 22725, i16 21407, i16 19266, i16 16384, i16 12873, i16 8867, i16 4520, i16 12873, i16 17855, i16 16819, i16 15137, i16 12873, i16 10114, i16 6967, i16 3552, i16 8867, i16 12299, i16 11585, i16 10426, i16 8867, i16 6967, i16 4799, i16 2446, i16 4520, i16 6270, i16 5906, i16 5315, i16 4520, i16 3552, i16 2446, i16 1247], align 16
@start_pass.aanscalefactor = internal constant [8 x double] [double 1.000000e+00, double 0x3FF63150B14861EF, double 0x3FF4E7AE914D6FCA, double 0x3FF2D062EF6C11AA, double 1.000000e+00, double 0x3FE92469C0A7BF3B, double 0x3FE1517A7BC720BB, double 0x3FD1A855DE72AB5D], align 16

; Function Attrs: nounwind
define hidden void @jinit_inverse_dct(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %idct = alloca %struct.my_idct_controller*, align 4
  %ci = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_idct_controller** %idct to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #4
  %2 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #4
  %3 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %3, i32 0, i32 1
  %4 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem, align 4, !tbaa !6
  %alloc_small = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %4, i32 0, i32 0
  %5 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small, align 4, !tbaa !11
  %6 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %7 = bitcast %struct.jpeg_decompress_struct* %6 to %struct.jpeg_common_struct*
  %call = call i8* %5(%struct.jpeg_common_struct* %7, i32 1, i32 84)
  %8 = bitcast i8* %call to %struct.my_idct_controller*
  store %struct.my_idct_controller* %8, %struct.my_idct_controller** %idct, align 4, !tbaa !2
  %9 = load %struct.my_idct_controller*, %struct.my_idct_controller** %idct, align 4, !tbaa !2
  %10 = bitcast %struct.my_idct_controller* %9 to %struct.jpeg_inverse_dct*
  %11 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %idct1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %11, i32 0, i32 84
  store %struct.jpeg_inverse_dct* %10, %struct.jpeg_inverse_dct** %idct1, align 8, !tbaa !14
  %12 = load %struct.my_idct_controller*, %struct.my_idct_controller** %idct, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_idct_controller, %struct.my_idct_controller* %12, i32 0, i32 0
  %start_pass = getelementptr inbounds %struct.jpeg_inverse_dct, %struct.jpeg_inverse_dct* %pub, i32 0, i32 0
  %start_pass2 = bitcast {}** %start_pass to void (%struct.jpeg_decompress_struct*)**
  store void (%struct.jpeg_decompress_struct*)* @start_pass, void (%struct.jpeg_decompress_struct*)** %start_pass2, align 4, !tbaa !15
  store i32 0, i32* %ci, align 4, !tbaa !18
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 44
  %14 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !19
  store %struct.jpeg_component_info* %14, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %ci, align 4, !tbaa !18
  %16 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %16, i32 0, i32 9
  %17 = load i32, i32* %num_components, align 4, !tbaa !20
  %cmp = icmp slt i32 %15, %17
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %mem3 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %18, i32 0, i32 1
  %19 = load %struct.jpeg_memory_mgr*, %struct.jpeg_memory_mgr** %mem3, align 4, !tbaa !6
  %alloc_small4 = getelementptr inbounds %struct.jpeg_memory_mgr, %struct.jpeg_memory_mgr* %19, i32 0, i32 0
  %20 = load i8* (%struct.jpeg_common_struct*, i32, i32)*, i8* (%struct.jpeg_common_struct*, i32, i32)** %alloc_small4, align 4, !tbaa !11
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %22 = bitcast %struct.jpeg_decompress_struct* %21 to %struct.jpeg_common_struct*
  %call5 = call i8* %20(%struct.jpeg_common_struct* %22, i32 1, i32 256)
  %23 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %23, i32 0, i32 20
  store i8* %call5, i8** %dct_table, align 4, !tbaa !21
  %24 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dct_table6 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %24, i32 0, i32 20
  %25 = load i8*, i8** %dct_table6, align 4, !tbaa !21
  call void @llvm.memset.p0i8.i32(i8* align 1 %25, i8 0, i32 256, i1 false)
  %26 = load %struct.my_idct_controller*, %struct.my_idct_controller** %idct, align 4, !tbaa !2
  %cur_method = getelementptr inbounds %struct.my_idct_controller, %struct.my_idct_controller* %26, i32 0, i32 1
  %27 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [10 x i32], [10 x i32]* %cur_method, i32 0, i32 %27
  store i32 -1, i32* %arrayidx, align 4, !tbaa !18
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %ci, align 4, !tbaa !18
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %ci, align 4, !tbaa !18
  %29 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %29, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %30 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #4
  %31 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #4
  %32 = bitcast %struct.my_idct_controller** %idct to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind
define internal void @start_pass(%struct.jpeg_decompress_struct* %cinfo) #0 {
entry:
  %cinfo.addr = alloca %struct.jpeg_decompress_struct*, align 4
  %idct = alloca %struct.my_idct_controller*, align 4
  %ci = alloca i32, align 4
  %i = alloca i32, align 4
  %compptr = alloca %struct.jpeg_component_info*, align 4
  %method = alloca i32, align 4
  %method_ptr = alloca void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, align 4
  %qtbl = alloca %struct.JQUANT_TBL*, align 4
  %ismtbl = alloca i32*, align 4
  %ifmtbl = alloca i32*, align 4
  %fmtbl = alloca float*, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  store %struct.jpeg_decompress_struct* %cinfo, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %0 = bitcast %struct.my_idct_controller** %idct to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #4
  %1 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %idct1 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %1, i32 0, i32 84
  %2 = load %struct.jpeg_inverse_dct*, %struct.jpeg_inverse_dct** %idct1, align 8, !tbaa !14
  %3 = bitcast %struct.jpeg_inverse_dct* %2 to %struct.my_idct_controller*
  store %struct.my_idct_controller* %3, %struct.my_idct_controller** %idct, align 4, !tbaa !2
  %4 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #4
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #4
  %6 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #4
  %7 = bitcast i32* %method to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #4
  store i32 0, i32* %method, align 4, !tbaa !18
  %8 = bitcast void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #4
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* null, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  %9 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #4
  store i32 0, i32* %ci, align 4, !tbaa !18
  %10 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %comp_info = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %10, i32 0, i32 44
  %11 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %comp_info, align 8, !tbaa !19
  store %struct.jpeg_component_info* %11, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc114, %entry
  %12 = load i32, i32* %ci, align 4, !tbaa !18
  %13 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %num_components = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %13, i32 0, i32 9
  %14 = load i32, i32* %num_components, align 4, !tbaa !20
  %cmp = icmp slt i32 %12, %14
  br i1 %cmp, label %for.body, label %for.end116

for.body:                                         ; preds = %for.cond
  %15 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %15, i32 0, i32 9
  %16 = load i32, i32* %DCT_scaled_size, align 4, !tbaa !23
  switch i32 %16, label %sw.default41 [
    i32 1, label %sw.bb
    i32 2, label %sw.bb2
    i32 3, label %sw.bb3
    i32 4, label %sw.bb4
    i32 5, label %sw.bb10
    i32 6, label %sw.bb11
    i32 7, label %sw.bb12
    i32 8, label %sw.bb13
    i32 9, label %sw.bb33
    i32 10, label %sw.bb34
    i32 11, label %sw.bb35
    i32 12, label %sw.bb36
    i32 13, label %sw.bb37
    i32 14, label %sw.bb38
    i32 15, label %sw.bb39
    i32 16, label %sw.bb40
  ]

sw.bb:                                            ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_1x1, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb2:                                           ; preds = %for.body
  %call = call i32 @jsimd_can_idct_2x2()
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb2
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jsimd_idct_2x2, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %sw.bb2
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_2x2, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb3:                                           ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_3x3, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb4:                                           ; preds = %for.body
  %call5 = call i32 @jsimd_can_idct_4x4()
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.then7, label %if.else8

if.then7:                                         ; preds = %sw.bb4
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jsimd_idct_4x4, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end9

if.else8:                                         ; preds = %sw.bb4
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_4x4, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end9

if.end9:                                          ; preds = %if.else8, %if.then7
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb10:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_5x5, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb11:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_6x6, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb12:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_7x7, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb13:                                          ; preds = %for.body
  %17 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %dct_method = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %17, i32 0, i32 17
  %18 = load i32, i32* %dct_method, align 8, !tbaa !24
  switch i32 %18, label %sw.default [
    i32 0, label %sw.bb14
    i32 1, label %sw.bb20
    i32 2, label %sw.bb26
  ]

sw.bb14:                                          ; preds = %sw.bb13
  %call15 = call i32 @jsimd_can_idct_islow()
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.then17, label %if.else18

if.then17:                                        ; preds = %sw.bb14
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jsimd_idct_islow, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end19

if.else18:                                        ; preds = %sw.bb14
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_islow, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end19

if.end19:                                         ; preds = %if.else18, %if.then17
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog

sw.bb20:                                          ; preds = %sw.bb13
  %call21 = call i32 @jsimd_can_idct_ifast()
  %tobool22 = icmp ne i32 %call21, 0
  br i1 %tobool22, label %if.then23, label %if.else24

if.then23:                                        ; preds = %sw.bb20
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jsimd_idct_ifast, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end25

if.else24:                                        ; preds = %sw.bb20
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_ifast, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end25

if.end25:                                         ; preds = %if.else24, %if.then23
  store i32 1, i32* %method, align 4, !tbaa !18
  br label %sw.epilog

sw.bb26:                                          ; preds = %sw.bb13
  %call27 = call i32 @jsimd_can_idct_float()
  %tobool28 = icmp ne i32 %call27, 0
  br i1 %tobool28, label %if.then29, label %if.else30

if.then29:                                        ; preds = %sw.bb26
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jsimd_idct_float, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end31

if.else30:                                        ; preds = %sw.bb26
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_float, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  br label %if.end31

if.end31:                                         ; preds = %if.else30, %if.then29
  store i32 2, i32* %method, align 4, !tbaa !18
  br label %sw.epilog

sw.default:                                       ; preds = %sw.bb13
  %19 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %19, i32 0, i32 0
  %20 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err, align 8, !tbaa !25
  %msg_code = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %20, i32 0, i32 5
  store i32 48, i32* %msg_code, align 4, !tbaa !26
  %21 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err32 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %21, i32 0, i32 0
  %22 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err32, align 8, !tbaa !25
  %error_exit = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %22, i32 0, i32 0
  %23 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit, align 4, !tbaa !28
  %24 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %25 = bitcast %struct.jpeg_decompress_struct* %24 to %struct.jpeg_common_struct*
  call void %23(%struct.jpeg_common_struct* %25)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end31, %if.end25, %if.end19
  br label %sw.epilog49

sw.bb33:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_9x9, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb34:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_10x10, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb35:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_11x11, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb36:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_12x12, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb37:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_13x13, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb38:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_14x14, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb39:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_15x15, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.bb40:                                          ; preds = %for.body
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* @jpeg_idct_16x16, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  store i32 0, i32* %method, align 4, !tbaa !18
  br label %sw.epilog49

sw.default41:                                     ; preds = %for.body
  %26 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err42 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %26, i32 0, i32 0
  %27 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err42, align 8, !tbaa !25
  %msg_code43 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %27, i32 0, i32 5
  store i32 7, i32* %msg_code43, align 4, !tbaa !26
  %28 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %DCT_scaled_size44 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %28, i32 0, i32 9
  %29 = load i32, i32* %DCT_scaled_size44, align 4, !tbaa !23
  %30 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err45 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %30, i32 0, i32 0
  %31 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err45, align 8, !tbaa !25
  %msg_parm = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %31, i32 0, i32 6
  %i46 = bitcast %union.anon* %msg_parm to [8 x i32]*
  %arrayidx = getelementptr inbounds [8 x i32], [8 x i32]* %i46, i32 0, i32 0
  store i32 %29, i32* %arrayidx, align 4, !tbaa !29
  %32 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err47 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %32, i32 0, i32 0
  %33 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err47, align 8, !tbaa !25
  %error_exit48 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %33, i32 0, i32 0
  %34 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit48, align 4, !tbaa !28
  %35 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %36 = bitcast %struct.jpeg_decompress_struct* %35 to %struct.jpeg_common_struct*
  call void %34(%struct.jpeg_common_struct* %36)
  br label %sw.epilog49

sw.epilog49:                                      ; preds = %sw.default41, %sw.bb40, %sw.bb39, %sw.bb38, %sw.bb37, %sw.bb36, %sw.bb35, %sw.bb34, %sw.bb33, %sw.epilog, %sw.bb12, %sw.bb11, %sw.bb10, %if.end9, %sw.bb3, %if.end, %sw.bb
  %37 = load void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr, align 4, !tbaa !2
  %38 = load %struct.my_idct_controller*, %struct.my_idct_controller** %idct, align 4, !tbaa !2
  %pub = getelementptr inbounds %struct.my_idct_controller, %struct.my_idct_controller* %38, i32 0, i32 0
  %inverse_DCT = getelementptr inbounds %struct.jpeg_inverse_dct, %struct.jpeg_inverse_dct* %pub, i32 0, i32 1
  %39 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx50 = getelementptr inbounds [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*], [10 x void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)*]* %inverse_DCT, i32 0, i32 %39
  store void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)* %37, void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %arrayidx50, align 4, !tbaa !2
  %40 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %component_needed = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %40, i32 0, i32 12
  %41 = load i32, i32* %component_needed, align 4, !tbaa !30
  %tobool51 = icmp ne i32 %41, 0
  br i1 %tobool51, label %lor.lhs.false, label %if.then54

lor.lhs.false:                                    ; preds = %sw.epilog49
  %42 = load %struct.my_idct_controller*, %struct.my_idct_controller** %idct, align 4, !tbaa !2
  %cur_method = getelementptr inbounds %struct.my_idct_controller, %struct.my_idct_controller* %42, i32 0, i32 1
  %43 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx52 = getelementptr inbounds [10 x i32], [10 x i32]* %cur_method, i32 0, i32 %43
  %44 = load i32, i32* %arrayidx52, align 4, !tbaa !18
  %45 = load i32, i32* %method, align 4, !tbaa !18
  %cmp53 = icmp eq i32 %44, %45
  br i1 %cmp53, label %if.then54, label %if.end55

if.then54:                                        ; preds = %lor.lhs.false, %sw.epilog49
  br label %for.inc114

if.end55:                                         ; preds = %lor.lhs.false
  %46 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %quant_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %46, i32 0, i32 19
  %47 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %quant_table, align 4, !tbaa !31
  store %struct.JQUANT_TBL* %47, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %48 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %cmp56 = icmp eq %struct.JQUANT_TBL* %48, null
  br i1 %cmp56, label %if.then57, label %if.end58

if.then57:                                        ; preds = %if.end55
  br label %for.inc114

if.end58:                                         ; preds = %if.end55
  %49 = load i32, i32* %method, align 4, !tbaa !18
  %50 = load %struct.my_idct_controller*, %struct.my_idct_controller** %idct, align 4, !tbaa !2
  %cur_method59 = getelementptr inbounds %struct.my_idct_controller, %struct.my_idct_controller* %50, i32 0, i32 1
  %51 = load i32, i32* %ci, align 4, !tbaa !18
  %arrayidx60 = getelementptr inbounds [10 x i32], [10 x i32]* %cur_method59, i32 0, i32 %51
  store i32 %49, i32* %arrayidx60, align 4, !tbaa !18
  %52 = load i32, i32* %method, align 4, !tbaa !18
  switch i32 %52, label %sw.default108 [
    i32 0, label %sw.bb61
    i32 1, label %sw.bb67
    i32 2, label %sw.bb82
  ]

sw.bb61:                                          ; preds = %if.end58
  %53 = bitcast i32** %ismtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #4
  %54 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dct_table = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %54, i32 0, i32 20
  %55 = load i8*, i8** %dct_table, align 4, !tbaa !21
  %56 = bitcast i8* %55 to i32*
  store i32* %56, i32** %ismtbl, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond62

for.cond62:                                       ; preds = %for.inc, %sw.bb61
  %57 = load i32, i32* %i, align 4, !tbaa !18
  %cmp63 = icmp slt i32 %57, 64
  br i1 %cmp63, label %for.body64, label %for.end

for.body64:                                       ; preds = %for.cond62
  %58 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %quantval = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %58, i32 0, i32 0
  %59 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx65 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval, i32 0, i32 %59
  %60 = load i16, i16* %arrayidx65, align 2, !tbaa !32
  %conv = zext i16 %60 to i32
  %61 = load i32*, i32** %ismtbl, align 4, !tbaa !2
  %62 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx66 = getelementptr inbounds i32, i32* %61, i32 %62
  store i32 %conv, i32* %arrayidx66, align 4, !tbaa !18
  br label %for.inc

for.inc:                                          ; preds = %for.body64
  %63 = load i32, i32* %i, align 4, !tbaa !18
  %inc = add nsw i32 %63, 1
  store i32 %inc, i32* %i, align 4, !tbaa !18
  br label %for.cond62

for.end:                                          ; preds = %for.cond62
  %64 = bitcast i32** %ismtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #4
  br label %sw.epilog113

sw.bb67:                                          ; preds = %if.end58
  %65 = bitcast i32** %ifmtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #4
  %66 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dct_table68 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %66, i32 0, i32 20
  %67 = load i8*, i8** %dct_table68, align 4, !tbaa !21
  %68 = bitcast i8* %67 to i32*
  store i32* %68, i32** %ifmtbl, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !18
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc79, %sw.bb67
  %69 = load i32, i32* %i, align 4, !tbaa !18
  %cmp70 = icmp slt i32 %69, 64
  br i1 %cmp70, label %for.body72, label %for.end81

for.body72:                                       ; preds = %for.cond69
  %70 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %quantval73 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %70, i32 0, i32 0
  %71 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx74 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval73, i32 0, i32 %71
  %72 = load i16, i16* %arrayidx74, align 2, !tbaa !32
  %conv75 = zext i16 %72 to i32
  %73 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx76 = getelementptr inbounds [64 x i16], [64 x i16]* @start_pass.aanscales, i32 0, i32 %73
  %74 = load i16, i16* %arrayidx76, align 2, !tbaa !32
  %conv77 = sext i16 %74 to i32
  %mul = mul nsw i32 %conv75, %conv77
  %add = add nsw i32 %mul, 2048
  %shr = ashr i32 %add, 12
  %75 = load i32*, i32** %ifmtbl, align 4, !tbaa !2
  %76 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx78 = getelementptr inbounds i32, i32* %75, i32 %76
  store i32 %shr, i32* %arrayidx78, align 4, !tbaa !18
  br label %for.inc79

for.inc79:                                        ; preds = %for.body72
  %77 = load i32, i32* %i, align 4, !tbaa !18
  %inc80 = add nsw i32 %77, 1
  store i32 %inc80, i32* %i, align 4, !tbaa !18
  br label %for.cond69

for.end81:                                        ; preds = %for.cond69
  %78 = bitcast i32** %ifmtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #4
  br label %sw.epilog113

sw.bb82:                                          ; preds = %if.end58
  %79 = bitcast float** %fmtbl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #4
  %80 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %dct_table83 = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %80, i32 0, i32 20
  %81 = load i8*, i8** %dct_table83, align 4, !tbaa !21
  %82 = bitcast i8* %81 to float*
  store float* %82, float** %fmtbl, align 4, !tbaa !2
  %83 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #4
  %84 = bitcast i32* %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #4
  store i32 0, i32* %i, align 4, !tbaa !18
  store i32 0, i32* %row, align 4, !tbaa !18
  br label %for.cond84

for.cond84:                                       ; preds = %for.inc105, %sw.bb82
  %85 = load i32, i32* %row, align 4, !tbaa !18
  %cmp85 = icmp slt i32 %85, 8
  br i1 %cmp85, label %for.body87, label %for.end107

for.body87:                                       ; preds = %for.cond84
  store i32 0, i32* %col, align 4, !tbaa !18
  br label %for.cond88

for.cond88:                                       ; preds = %for.inc102, %for.body87
  %86 = load i32, i32* %col, align 4, !tbaa !18
  %cmp89 = icmp slt i32 %86, 8
  br i1 %cmp89, label %for.body91, label %for.end104

for.body91:                                       ; preds = %for.cond88
  %87 = load %struct.JQUANT_TBL*, %struct.JQUANT_TBL** %qtbl, align 4, !tbaa !2
  %quantval92 = getelementptr inbounds %struct.JQUANT_TBL, %struct.JQUANT_TBL* %87, i32 0, i32 0
  %88 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx93 = getelementptr inbounds [64 x i16], [64 x i16]* %quantval92, i32 0, i32 %88
  %89 = load i16, i16* %arrayidx93, align 2, !tbaa !32
  %conv94 = uitofp i16 %89 to double
  %90 = load i32, i32* %row, align 4, !tbaa !18
  %arrayidx95 = getelementptr inbounds [8 x double], [8 x double]* @start_pass.aanscalefactor, i32 0, i32 %90
  %91 = load double, double* %arrayidx95, align 8, !tbaa !33
  %mul96 = fmul double %conv94, %91
  %92 = load i32, i32* %col, align 4, !tbaa !18
  %arrayidx97 = getelementptr inbounds [8 x double], [8 x double]* @start_pass.aanscalefactor, i32 0, i32 %92
  %93 = load double, double* %arrayidx97, align 8, !tbaa !33
  %mul98 = fmul double %mul96, %93
  %conv99 = fptrunc double %mul98 to float
  %94 = load float*, float** %fmtbl, align 4, !tbaa !2
  %95 = load i32, i32* %i, align 4, !tbaa !18
  %arrayidx100 = getelementptr inbounds float, float* %94, i32 %95
  store float %conv99, float* %arrayidx100, align 4, !tbaa !34
  %96 = load i32, i32* %i, align 4, !tbaa !18
  %inc101 = add nsw i32 %96, 1
  store i32 %inc101, i32* %i, align 4, !tbaa !18
  br label %for.inc102

for.inc102:                                       ; preds = %for.body91
  %97 = load i32, i32* %col, align 4, !tbaa !18
  %inc103 = add nsw i32 %97, 1
  store i32 %inc103, i32* %col, align 4, !tbaa !18
  br label %for.cond88

for.end104:                                       ; preds = %for.cond88
  br label %for.inc105

for.inc105:                                       ; preds = %for.end104
  %98 = load i32, i32* %row, align 4, !tbaa !18
  %inc106 = add nsw i32 %98, 1
  store i32 %inc106, i32* %row, align 4, !tbaa !18
  br label %for.cond84

for.end107:                                       ; preds = %for.cond84
  %99 = bitcast i32* %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #4
  %100 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #4
  %101 = bitcast float** %fmtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #4
  br label %sw.epilog113

sw.default108:                                    ; preds = %if.end58
  %102 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err109 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %102, i32 0, i32 0
  %103 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err109, align 8, !tbaa !25
  %msg_code110 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %103, i32 0, i32 5
  store i32 48, i32* %msg_code110, align 4, !tbaa !26
  %104 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %err111 = getelementptr inbounds %struct.jpeg_decompress_struct, %struct.jpeg_decompress_struct* %104, i32 0, i32 0
  %105 = load %struct.jpeg_error_mgr*, %struct.jpeg_error_mgr** %err111, align 8, !tbaa !25
  %error_exit112 = getelementptr inbounds %struct.jpeg_error_mgr, %struct.jpeg_error_mgr* %105, i32 0, i32 0
  %106 = load void (%struct.jpeg_common_struct*)*, void (%struct.jpeg_common_struct*)** %error_exit112, align 4, !tbaa !28
  %107 = load %struct.jpeg_decompress_struct*, %struct.jpeg_decompress_struct** %cinfo.addr, align 4, !tbaa !2
  %108 = bitcast %struct.jpeg_decompress_struct* %107 to %struct.jpeg_common_struct*
  call void %106(%struct.jpeg_common_struct* %108)
  br label %sw.epilog113

sw.epilog113:                                     ; preds = %sw.default108, %for.end107, %for.end81, %for.end
  br label %for.inc114

for.inc114:                                       ; preds = %sw.epilog113, %if.then57, %if.then54
  %109 = load i32, i32* %ci, align 4, !tbaa !18
  %inc115 = add nsw i32 %109, 1
  store i32 %inc115, i32* %ci, align 4, !tbaa !18
  %110 = load %struct.jpeg_component_info*, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.jpeg_component_info, %struct.jpeg_component_info* %110, i32 1
  store %struct.jpeg_component_info* %incdec.ptr, %struct.jpeg_component_info** %compptr, align 4, !tbaa !2
  br label %for.cond

for.end116:                                       ; preds = %for.cond
  %111 = bitcast %struct.JQUANT_TBL** %qtbl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #4
  %112 = bitcast void (%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32)** %method_ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #4
  %113 = bitcast i32* %method to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #4
  %114 = bitcast %struct.jpeg_component_info** %compptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #4
  %115 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #4
  %116 = bitcast i32* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #4
  %117 = bitcast %struct.my_idct_controller** %idct to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

declare void @jpeg_idct_1x1(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare i32 @jsimd_can_idct_2x2() #3

declare void @jsimd_idct_2x2(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_2x2(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_3x3(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare i32 @jsimd_can_idct_4x4() #3

declare void @jsimd_idct_4x4(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_4x4(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_5x5(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_6x6(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_7x7(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare i32 @jsimd_can_idct_islow() #3

declare void @jsimd_idct_islow(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_islow(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare i32 @jsimd_can_idct_ifast() #3

declare void @jsimd_idct_ifast(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_ifast(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare i32 @jsimd_can_idct_float() #3

declare void @jsimd_idct_float(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_float(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_9x9(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_10x10(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_11x11(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_12x12(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_13x13(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_14x14(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_15x15(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

declare void @jpeg_idct_16x16(%struct.jpeg_decompress_struct*, %struct.jpeg_component_info*, i16*, i8**, i32) #3

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 4}
!7 = !{!"jpeg_decompress_struct", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !8, i64 16, !8, i64 20, !3, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !4, i64 40, !4, i64 44, !8, i64 48, !8, i64 52, !9, i64 56, !8, i64 64, !8, i64 68, !4, i64 72, !8, i64 76, !8, i64 80, !8, i64 84, !4, i64 88, !8, i64 92, !8, i64 96, !8, i64 100, !8, i64 104, !8, i64 108, !8, i64 112, !8, i64 116, !8, i64 120, !8, i64 124, !8, i64 128, !8, i64 132, !3, i64 136, !8, i64 140, !8, i64 144, !8, i64 148, !8, i64 152, !8, i64 156, !3, i64 160, !4, i64 164, !4, i64 180, !4, i64 196, !8, i64 212, !3, i64 216, !8, i64 220, !8, i64 224, !4, i64 228, !4, i64 244, !4, i64 260, !8, i64 276, !8, i64 280, !4, i64 284, !4, i64 285, !4, i64 286, !10, i64 288, !10, i64 290, !8, i64 292, !4, i64 296, !8, i64 300, !3, i64 304, !8, i64 308, !8, i64 312, !8, i64 316, !8, i64 320, !3, i64 324, !8, i64 328, !4, i64 332, !8, i64 348, !8, i64 352, !8, i64 356, !4, i64 360, !8, i64 400, !8, i64 404, !8, i64 408, !8, i64 412, !8, i64 416, !3, i64 420, !3, i64 424, !3, i64 428, !3, i64 432, !3, i64 436, !3, i64 440, !3, i64 444, !3, i64 448, !3, i64 452, !3, i64 456, !3, i64 460}
!8 = !{!"int", !4, i64 0}
!9 = !{!"double", !4, i64 0}
!10 = !{!"short", !4, i64 0}
!11 = !{!12, !3, i64 0}
!12 = !{!"jpeg_memory_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32, !3, i64 36, !3, i64 40, !13, i64 44, !13, i64 48}
!13 = !{!"long", !4, i64 0}
!14 = !{!7, !3, i64 448}
!15 = !{!16, !3, i64 0}
!16 = !{!"", !17, i64 0, !4, i64 44}
!17 = !{!"jpeg_inverse_dct", !3, i64 0, !4, i64 4}
!18 = !{!8, !8, i64 0}
!19 = !{!7, !3, i64 216}
!20 = !{!7, !8, i64 36}
!21 = !{!22, !3, i64 80}
!22 = !{!"", !8, i64 0, !8, i64 4, !8, i64 8, !8, i64 12, !8, i64 16, !8, i64 20, !8, i64 24, !8, i64 28, !8, i64 32, !8, i64 36, !8, i64 40, !8, i64 44, !8, i64 48, !8, i64 52, !8, i64 56, !8, i64 60, !8, i64 64, !8, i64 68, !8, i64 72, !3, i64 76, !3, i64 80}
!23 = !{!22, !8, i64 36}
!24 = !{!7, !4, i64 72}
!25 = !{!7, !3, i64 0}
!26 = !{!27, !8, i64 20}
!27 = !{!"jpeg_error_mgr", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !8, i64 20, !4, i64 24, !8, i64 104, !13, i64 108, !3, i64 112, !8, i64 116, !3, i64 120, !8, i64 124, !8, i64 128}
!28 = !{!27, !3, i64 0}
!29 = !{!4, !4, i64 0}
!30 = !{!22, !8, i64 48}
!31 = !{!22, !3, i64 76}
!32 = !{!10, !10, i64 0}
!33 = !{!9, !9, i64 0}
!34 = !{!35, !35, i64 0}
!35 = !{!"float", !4, i64 0}
